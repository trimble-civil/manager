package no.mesta.mipss.util;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.query.StructHelper;
import oracle.jdbc.pool.OracleDataSource;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;

public class LocalTest {

    private static final Logger log = LoggerFactory.getLogger(LocalTest.class);
    private static EntityManager em;

    public static void prepareLocal() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mipss-test");
        em = new EntityManagerTransaction(emf);
        StructHelper.setLocalMode(true);
        BeanUtil.setBeanLookup(new LocalBeanLookup(em));

        try {
            InitialContext ix = new InitialContext();
            OracleDataSource ds = new OracleDataSource();
            ds.setServerName("vns-mes-t-db01.os.eon.no");
            ds.setPortNumber(1521);
            ds.setServiceName("pstyrt");
            ds.setPassword("mipss");
            ds.setUser("mipss");
            ds.setDriverType("thin");

            ix.bind("java:/mipssDS", ds);
        } catch (NamingException e) {
            log.error("Unable to create initial context", e);
        } catch (SQLException e) {
            log.error("Unable to create data source", e);
        }
    }

    public static EntityManager getEntityManager() {
        return em;
    }

    @Test
    public void testName() throws Exception {

    }
}
