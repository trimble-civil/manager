package no.mesta.mipss.util;

import no.mesta.mipss.common.BeanLookup;

import javax.persistence.EntityManager;
import java.lang.reflect.Field;
import java.util.HashMap;

public class LocalBeanLookup implements BeanLookup {

    private final EntityManager em;
    private final HashMap<String, Object> beanCache= new HashMap<String, Object>();

    public LocalBeanLookup(EntityManager em) {
        this.em = em;
    }

    @Override
    public <T> T lookup(String beanName, Class<T> bean) {
        if(beanCache.containsKey(beanName)) {
            return (T) beanCache.get(beanName);
        }

        try {
            Class c = getClassByName(bean);
            T t = (T) c.getConstructor().newInstance();

            Field[] declaredFields = c.getDeclaredFields();
            for(Field field : declaredFields) {
                if(field.getType() == EntityManager.class) {
                    field.setAccessible(true);
                    field.set(t, em);
                }
                if(field.getType().getName().contains("KonfigParam")) {
                    field.setAccessible(true);
                    field.set(t, lookup("KonfigParam", Class.forName("no.mesta.mipss.konfigparam.KonfigParam")));
                }
                if(field.getType().getName().contains("Rodegenerator")) {
                    field.setAccessible(true);
                    field.set(t, lookup("Rodegenerator", Class.forName("no.mesta.mipss.rodeservice.Rodegenerator")));
                }
                if(field.getType().getName().contains("NvdbWebserviceLocal")) {
                    field.setAccessible(true);
                    field.set(t, lookup("NvdbWebServiceLocal", Class.forName("no.mesta.mipss.webservice.NvdbWebservice")));
                }
                if(field.getType().getName().contains("AvvikServiceLocal")) {
                    field.setAccessible(true);
                    field.set(t, lookup("AvvikServiceLocal", Class.forName("no.mesta.mipss.mipssfelt.AvvikBean")));
                }
                if(field.getType().getName().contains("RskjemaService")) {
                    field.setAccessible(true);
                    field.set(t, lookup("RskjemaService", Class.forName("no.mesta.mipss.mipssfelt.RskjemaBean")));
                }

            }
            beanCache.put(beanName, t);
            return t;
        } catch (Exception e) {
            System.out.println("Error creating class of interface '"+beanName+"'");
            e.printStackTrace();
        }
        throw new IllegalArgumentException("Not yet able to create local bean of type '"+beanName+"'");
    }

    private <T> Class<?> getClassByName(Class<T> beanInterface) throws ClassNotFoundException {
        try {
            return Class.forName(beanInterface.getName()+"Bean");
        } catch(ClassNotFoundException e) {
            return Class.forName(beanInterface.getName().replace("Service", "Bean"));
        }
    }

    public <T> void addBean(String beanName, T beanImplementation) {
        beanCache.put(beanName, beanImplementation);
    }
}
