package no.mesta.mipss.util;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.naming.*;
import javax.naming.spi.InitialContextFactory;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class LocalInitialContextFactory implements InitialContextFactory {

    public static LocalInitialContext local;
    @Override
    public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
        if(local == null) {
            local = new LocalInitialContext();
        }
        return local;
    }
}

class LocalInitialContext implements Context {

    private final Map<String, Object> content = new HashMap<String, Object>();

    @Override
    public Object lookup(Name name) throws NamingException {
        return lookup(name.toString());
    }

    @Override
    public Object lookup(String name) throws NamingException {
        if(content.containsKey(name)) {
            return content.get(name);
        }
        throw new NamingException(name+" not found");
    }

    @Override
    public void bind(Name name, Object obj) throws NamingException {
        bind(name.toString(), obj);
    }

    @Override
    public void bind(String name, Object obj) throws NamingException {
        content.put(name, obj);
    }

    @Override
    public void rebind(Name name, Object obj) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void rebind(String name, Object obj) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void unbind(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void unbind(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void rename(Name oldName, Name newName) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void rename(String oldName, String newName) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void destroySubcontext(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void destroySubcontext(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Context createSubcontext(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Context createSubcontext(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Object lookupLink(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Object lookupLink(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public NameParser getNameParser(Name name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public NameParser getNameParser(String name) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Name composeName(Name name, Name prefix) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public String composeName(String name, String prefix) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Object addToEnvironment(String propName, Object propVal) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Object removeFromEnvironment(String propName) throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public Hashtable<?, ?> getEnvironment() throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public void close() throws NamingException {
        throw new NotImplementedException();
    }

    @Override
    public String getNameInNamespace() throws NamingException {
        throw new NotImplementedException();
    }
}