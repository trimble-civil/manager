package no.mesta.mipss.driftkontrakt.leverandor;

import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static no.mesta.mipss.driftkontrakt.leverandor.ArtikkelProdtypeTableModel.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ArtikkelProdtypeTableModelTest {

    private ArtikkelProdtypeTableModel model;

    @Before
    public void setUp() throws Exception {
        List<ArtikkelProdtype> articles = new ArrayList<>();
        articles.add(getArticle(true, false));
        articles.add(getArticle(false, true));

        model = new ArtikkelProdtypeTableModel(articles);
    }

    private ArtikkelProdtype getArticle(boolean broyteFlagg, boolean stroFlagg) {
        ArtikkelProdtype article = new ArtikkelProdtype();
        Prodtype prodtype = new Prodtype();
        prodtype.setBroyteFlagg(broyteFlagg);
        prodtype.setStroFlagg(stroFlagg);
        article.setProdtype(prodtype);

        GregorianCalendar gc = new GregorianCalendar();
        Date d = new Date();
        gc.setTime(d);

        gc.add(Calendar.DAY_OF_MONTH, -1);
        Date fromDate = gc.getTime();

        gc.add(Calendar.DAY_OF_MONTH, 2);
        Date toDate = gc.getTime();

        AgrArtikkelV agrArticle = new AgrArtikkelV();
        agrArticle.setArtikkelStatus("N");
        agrArticle.setFraDato(fromDate);
        agrArticle.setTilDato(toDate);
        article.setAgrArtikkelV(agrArticle);

        return article;
    }

    @Test
    public void willNotAllowEditingOfArticle() throws Exception {
        boolean actual = model.isCellEditable(0, COL_ARTIKKEL);
        assertFalse(actual);
    }

    @Test
    public void willNotAllowEditingOfUnit() throws Exception {
        boolean actual = model.isCellEditable(0, COL_UNIT);
        assertFalse(actual);
    }

    @Test
    public void willNotAllowEditingOfPrice() throws Exception {
        boolean actual = model.isCellEditable(0, COL_PRICE);
        assertFalse(actual);
    }

    @Test
    public void willAllowEditingOfCoproduction() throws Exception {
        boolean actual = model.isCellEditable(0, COL_COPRODUCTION);
        assertTrue(actual);
    }

    @Test
    public void willAllowEditingOfProductionType() throws Exception {
        boolean actual = model.isCellEditable(0, COL_PRODUCTION_TYPE);
        assertTrue(actual);
    }

    @Test
    public void willNotAllowEditingOfSpreadingProductForNonSpreadingProdType() throws Exception {
        boolean actual = model.isCellEditable(0, COL_SPREADING_PRODUCT);
        assertFalse(actual);
    }

    @Test
    public void willAllowEditingOfSpreadingProductForSpreadingProdtype() throws Exception {
        boolean actual = model.isCellEditable(1, COL_SPREADING_PRODUCT);
        assertTrue(actual);
    }
}