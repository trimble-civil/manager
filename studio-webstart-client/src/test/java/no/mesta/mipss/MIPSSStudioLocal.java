package no.mesta.mipss;

import no.mesta.mipss.util.LocalTest;

public class MIPSSStudioLocal {

    public static void main(String[] args) {
        LocalTest.prepareLocal();
        MIPSSStudio.main(null);
    }
}
