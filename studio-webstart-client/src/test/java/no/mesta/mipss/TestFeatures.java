package no.mesta.mipss;

import java.io.IOException;
import java.io.InputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.io.IOUtils;



public class TestFeatures {
	private static final int GATE = 538;
	public static void main(String[] args) throws IOException {
		InputStream is = TestFeatures.class.getResourceAsStream( "/sample-json.txt");
		String jsonTxt = IOUtils.toString( is );
    
		JSONObject json = (JSONObject)JSONSerializer.toJSON( jsonTxt );
		JSONArray o = (JSONArray)json.get("vegObjektTyper");
		for (int i=0;i<o.size();i++){
			JSONObject j = (JSONObject)o.get(i);
			System.out.println("id="+j.get("id"));
			System.out.println("navn="+j.get("navn"));
			System.out.println("beskrivelse="+j.get("beskrivelse"));
			System.out.println("");
		}
		
////		NvdbWebservice bean = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
////		List<Integer> values = new ArrayList<Integer>();
////		
////		for (int i=0;i<1000;i++){
////			values.add(Integer.valueOf(i));	
////		}
////		Veinettreflinkseksjon vr = new Veinettreflinkseksjon();
////		vr.setFra(0D);
////		vr.setTil(1D);
////		vr.setReflinkIdent(Long.valueOf(181027));
////		List<GetFeaturesWithinReflinkSectionsResult> ft = bean.getFeaturesWithinReflinkSectionsFromVeinettreflinkseksjon(GATE, values, Collections.singletonList(vr));
////		System.out.println(ft);
//		Calendar c = Calendar.getInstance();
//		
//		DriftsloggQueryParams p = new DriftsloggQueryParams();
//		p.setMonth(c.getTime());
//	
//		DriftsloggService bean = BeanUtil.lookup(DriftsloggService.BEAN_NAME, DriftsloggService.class);
//		List<Aktivitet> sokAktiviteter = bean.sokAktiviteter(p);
//		for (Aktivitet a:sokAktiviteter){
//			System.out.println(a.getPeriodeStatus());
//		}
//		System.out.println(sokAktiviteter);
	}
	
}
