package no.mesta.mipss;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.accesscontrol.AccessControlBean;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.MainFrame;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.FeltModule;
import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.felt.sak.detaljui.SakDetaljDialog;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.studio.client.gui.MainFrameImpl;
import no.mesta.mipss.studio.core.MipssStudioLoaderImpl;
import no.mesta.mipss.util.LocalTest;
import org.junit.Test;

import java.util.ArrayList;

public class SakDetaljDialogTest {
    public static void main(String[] args) {
        LocalTest.prepareLocal();

        final ArrayList<MenynodeDTO> menuList = new ArrayList<MenynodeDTO>();
        Bruker bruker = new Bruker();
        MainFrame mainFrame = new MainFrameImpl("SakDetaljTest", menuList);
        AccessControl accessControl = new AccessControlBean();
        IMipssStudioLoader loader = new MipssStudioLoaderImpl(mainFrame, bruker, accessControl, menuList);
        FeltModule feltModule = new FeltModule();
        feltModule.setLoader(loader);
        FeltController feltController = new FeltController(feltModule);
        SakController sakController = new SakController(feltController);
        Sak sak = (Sak)LocalTest.getEntityManager().createQuery("Select s from Sak s where s.guid='6367bd81-02b2-4430-a61d-1ba0b25fa98c'").getSingleResult();

        SakDetaljDialog dialog = new SakDetaljDialog(sakController, sak, true);
        dialog.setVisible(true);
    }

    @Test
    public void testName() throws Exception {

    }
}
