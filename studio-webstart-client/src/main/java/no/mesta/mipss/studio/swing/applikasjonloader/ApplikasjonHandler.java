package no.mesta.mipss.studio.swing.applikasjonloader;

import java.io.File;

import no.mesta.mipss.dto.ClassDTO;
import no.mesta.mipss.exceptions.ApplikasjonHandlerException;
import no.mesta.mipss.menunode.ApplikasjonLoader;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Denne klassen håndterer Applikasjoner og bibliotek, dvs. den har ansvaret for å hente 
 * applikasjoner og tilhørende bibliotek og å lage en ClassLoader som kan benyttes til å kalle
 * på klassene i applikasjonene som er lastet ned. 
 * 
 * Alle applikasjoner som skal kjøres lastes ned lokalt til MipssConstants.USER_HOME_APP. Denne mappen brukes 
 * som rammeverkets cache for applikasjoner. Applikasjonene i denne mappen puttes i en URLClassLoader. evt
 * eksterne bibliotek havner som en URLClassLoader som ligger under applikasjonenes URLClassLoader i classloader hierarkiet.
 * 
 * @author Harald A. Kulø
 */
public class ApplikasjonHandler {
    private Logger log = LoggerFactory.getLogger(ApplikasjonHandler.class);

//    private URLClassLoader applikasjonLoader;

//    private static ApplikasjonHandler instance;
    private ApplikasjonLoader loaderBean;
    
    public ApplikasjonHandler() {
    }
    
    public ClassDTO loadClass(String tilgangspunkt) throws ApplikasjonHandlerException{
        ClassDTO dto = new ClassDTO();
        try{
            Class<?> c= Class.forName(tilgangspunkt);
            dto.setClazz(c);
            return dto;
        } catch (ClassNotFoundException e) {
            Tilgangspunkt tp = loaderBean.getTilgangspunkt(tilgangspunkt);
            try {
                //Modulversjon modulKode = tp.getModulversjon();
                Modul modul = tp.getModul();//modulKode.getModul(); 
                
                log.debug("Laster modul:" + modul.getNavn());
                dto.setClazz(Class.forName(tp.getKlassenavn()));
                dto.setVisningsnavn(modul.getNavn());
                return dto;
            } catch (ClassNotFoundException f) {
                throw new ApplikasjonHandlerException("Klassen:"+tilgangspunkt+" ble ikke funnet..");
            }
        }
    } 
      
    /**
     * Returnerer true hvis biblioteket er en .dll eller en .exe fil
     * Dette er viktig å vite, ettersom et native-bibliotek ikke skal legges i en 
     * ClassLoader. 
     * @param name navnet på bibliotekfilen
     * @return true hvis det er et native bibliotek
     */
    @SuppressWarnings("unused")
	private boolean isNative(String name) {
        return name.endsWith(".dll")||name.endsWith(".exe");
    }

    /**
     * Lager en mappestruktur hvis den ikke finnes fra før av. 
     * @param dir mappen som skal lages
     * @return true hvis mappen finnes fra før eller blir hvis den ble laget. 
     */
    @SuppressWarnings("unused")
	private boolean createDir(String dir) {
        File file = new File(dir);
        if (file.exists()) {
            if (file.isDirectory()) {
                return true;
            } else {
                // en fil er i veien
                return false;
            }
        }
        // mappen finnes ikke, prøv å lag den.
        boolean success = new File(dir).mkdirs();
        return success;
    }

}
