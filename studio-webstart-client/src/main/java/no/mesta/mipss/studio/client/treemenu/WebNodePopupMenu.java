package no.mesta.mipss.studio.client.treemenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;

import no.mesta.mipss.core.IMipssStudioLoader;

/**
 * Lytter for web meny punkter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class WebNodePopupMenu extends MouseAdapter{
    private JPopupMenu menu;
//    private DefaultMutableTreeNode currentNode;
	
	public WebNodePopupMenu(IMipssStudioLoader loader){
		menu = new JPopupMenu();
		JMenuItem tabItem = new JMenuItem("Åpne ny sesjon i fane");
		tabItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
//				MenynodeDTO webapp = (MenynodeDTO)currentNode.getUserObject();
				//TODO fix back getMipssController().getApplikasjonPanelLoader().setBrowserApplication(webapp, true);
                        }
		});
		menu.add(tabItem);
	}
	
	public void mousePressed(java.awt.event.MouseEvent e) { 
		displayMenu(e);
    }
	
    public void mouseReleased(java.awt.event.MouseEvent e) {
        displayMenu(e);
    }
    
    public void mouseClicked(MouseEvent e) {
    	if (e.getClickCount()==1){
	    	JTree tree = (JTree)e.getSource();	
	    	int selRow = tree.getRowForLocation(e.getX(), e.getY());
	    	if (tree.isExpanded(selRow)){
	    		tree.collapseRow(selRow);
	    	}else{
	    		tree.expandRow(selRow);
	    	}
    	}
    	
    }
    
    private void displayMenu(MouseEvent e) {
    	if(e.isPopupTrigger()){   
    		if (e.getSource() instanceof JTree){
	    		JTree tree = (JTree)e.getSource();
    			int selRow = tree.getRowForLocation(e.getX(), e.getY());
//	    		TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
	    		if(selRow != -1) {
//	    			DefaultMutableTreeNode node = (DefaultMutableTreeNode)selPath.getLastPathComponent();
	    			/*
                                if (node.getUserObject() instanceof WebApplicationDTO){
	    				currentNode = node;
	    				menu.show(e.getComponent(), e.getX(), e.getY());
	    			}
                                */
	    		}
    		}
    	}
    }
}
