package no.mesta.mipss.studio.login;

import java.security.Principal;

public class MestaPrincipal implements Principal {

	private final String name;
	public MestaPrincipal(String name){
		this.name = name;
		
	}
	@Override
	public String getName() {
		return name;
	}

}
