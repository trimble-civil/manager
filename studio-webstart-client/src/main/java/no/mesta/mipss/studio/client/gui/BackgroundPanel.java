package no.mesta.mipss.studio.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.colorchooser.AbstractColorChooserPanel;

import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.ui.ColorChooserPanel;

@SuppressWarnings("serial")
public class BackgroundPanel extends JPanel{
	private Color color1 = new Color(123, 162, 231);
	private Color color2 = new Color(99, 117, 214);
	private JLabel label = new JLabel("\u03A0");
	private ColorChooserPanel chooserPanel;
//	private Image background = new ImageIcon(getClass().getResource("/images/mipss.png")).getImage();
//	private Image background = new ImageIcon(getClass().getResource("/images/blue.jpg")).getImage();
	private Image background = new ImageIcon(getClass().getResource("/images/summer_nature_background.jpg")).getImage();
	
	private Image mipss = new ImageIcon(getClass().getResource("/images/mipss2.png")).getImage();
	private Image studio = new ImageIcon(getClass().getResource("/images/studio.png")).getImage();
	private Image logo = new ImageIcon(getClass().getResource("/images/mesta_logo_shadow.png")).getImage();
	private int logoW = 1745/4;
	private int logoH = 412/4;
	private int x_pos=0;
	private int y_pos=0;
	private Thread thread;
	private boolean running;
	public BackgroundPanel(){
		label.addMouseListener(new PiMouseListener());
		label.setFont(new Font("Serif", Font.PLAIN, 12));
		label.setForeground(new Color(.9f, .9f, .9f, 0.5f));
		FlowLayout f = new FlowLayout();
		f.setAlignment(FlowLayout.RIGHT);
		setLayout(new BorderLayout());
		JPanel piPanel = new JPanel();
		piPanel.setOpaque(false);
		piPanel.setLayout(f);
		piPanel.add(label);
		add(piPanel, BorderLayout.SOUTH);
		logo = ImageUtil.scaleImage(new ImageIcon(logo), logoW, logoH);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		GradientPaint gradient = new GradientPaint(0,0,color1, 0, getHeight(), color2);
		g2.setPaint(gradient);
		g2.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		g.drawImage(mipss, 50,50,null);
		g.drawImage(studio, 50+mipss.getWidth(this)/2,50+mipss.getHeight(this)+20,null);
		
		g.drawImage(logo, getWidth()-logoW, getHeight()-logoH, logoW, logoH, null);
		/*int x = 0;
		int y = y_pos;
		for (int i=0;y<getHeight();i++){
			x = x_pos;
			if (i%2==0)
				x = -(background.getWidth(this)/2);
			
			while (x<getWidth()){
				g.drawImage(background, x, y, background.getWidth(this), background.getHeight(this), this); 
				x += background.getWidth(this)+20;
			}
			
			y+=background.getHeight(this)+20;
		}*/
	}
	
	/** EASTER EGGS... */
	
	
	private class Color1Action implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			color1 = chooserPanel.getCurrentColor();
		}
		
	}
	
	private class Color2Action implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			color2 = chooserPanel.getCurrentColor();
		}
		
	}
	public void fall(){
		new Thread(){
			public void run(){
				running = true;
				int turn = 30;
				int c = 0;
				int v = 1;
				int j = 5;
				boolean trn = false;
				while (running){
					c++;
					if (c<turn){
						y_pos-=v;
					}
					else if(y_pos<=getHeight()){
						if (!trn){
							v = 1;
						}
						trn = true;
						y_pos+=v;
					}else 
						running = false;
					try{
						Thread.sleep(12);
					}catch (InterruptedException e){}
					repaint();
					if (c%j==0)
						v++;
				}
			}
		}.start();
	}
	private class PiMouseListener extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent e) {
			JDialog dialog=null;
			JColorChooser chooser = new JColorChooser();
			chooserPanel = new ColorChooserPanel();
	        AbstractColorChooserPanel[] panels = {chooserPanel};
	        chooser.setChooserPanels(panels);
	        chooser.setPreviewPanel(new JPanel());
	        JButton button = new JButton();
			if (e.getButton()==MouseEvent.BUTTON3&&e.getModifiersEx()==MouseEvent.SHIFT_DOWN_MASK){
		        dialog = JColorChooser.createDialog(button,
		                                        "Velg en farge",
		                                        true,  //modal
		                                        chooser,
		                                        new Color1Action(),  //OK button handler
		                                        null); //no CANCEL button handler
		        dialog.setSize(280,240);
		        dialog.setVisible(true);
			}
			if (e.getButton()==MouseEvent.BUTTON3&&e.getModifiersEx()==MouseEvent.CTRL_DOWN_MASK){
		        dialog = JColorChooser.createDialog(button,
		                                        "Velg en farge",
		                                        true,  //modal
		                                        chooser,
		                                        new Color2Action(),  //OK button handler
		                                        null); //no CANCEL button handler
		        dialog.setSize(280,240);
		        dialog.setVisible(true);
			}
			if (e.getButton()==MouseEvent.BUTTON1&&e.getModifiersEx()==MouseEvent.ALT_DOWN_MASK){
				if (thread==null){
					thread = new Thread(){
						public void run(){
							running = true;
							while (running){
								try{
									Thread.sleep(5);
									x_pos--;
									repaint();
								}catch (InterruptedException e){}
							}
						}
					};
					thread.start();
				}
				else{
					running = false;
					thread=null;
				}
					
			}
			repaint();
		}
	}
}
