package no.mesta.mipss.studio.login;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.HashingUtils;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

public class ChangePasswordDialog{

	private JLabel oldPasswordLabel;
	private JLabel newPasswordLabel;
	private JLabel confirmNewPasswordLabel;
	
	private JPasswordField oldPasswordField;
	private JPasswordField newPasswordField;
	private JPasswordField confirmNewPasswordField;
	
	private JLabel brukernavnLabel;
	private String brukernavn;
	
	private AccessControl accessControlBean = BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class);

	public ChangePasswordDialog(String brukernavn) {
		this.brukernavn = brukernavn;
		initGUI();
	}	
	
	private void initGUI() {
		JPanel changePasswordPanel = new JPanel(new GridLayout(7, 1));
		
		brukernavnLabel = new JLabel("<html><b>Brukernavn: " + brukernavn + "</b></html>");
		
		changePasswordPanel.add(brukernavnLabel);
				
		oldPasswordLabel = new JLabel("Gammelt passord");
		newPasswordLabel = new JLabel("Nytt passord");
		confirmNewPasswordLabel = new JLabel("Bekreft nytt passord");
		
		oldPasswordField = new JPasswordField();
		newPasswordField = new JPasswordField();
		confirmNewPasswordField = new JPasswordField();
		
		changePasswordPanel.add(oldPasswordLabel);
		changePasswordPanel.add(newPasswordLabel);
		changePasswordPanel.add(confirmNewPasswordLabel);		

		changePasswordPanel.add(oldPasswordField);
		changePasswordPanel.add(newPasswordField);
		changePasswordPanel.add(confirmNewPasswordField);	
		
		changePasswordPanel.add(oldPasswordLabel);
		changePasswordPanel.add(oldPasswordField);
		changePasswordPanel.add(newPasswordLabel);
		changePasswordPanel.add(newPasswordField);
		changePasswordPanel.add(confirmNewPasswordLabel);
		changePasswordPanel.add(confirmNewPasswordField);
		
		int result = JOptionPane.showConfirmDialog(null, changePasswordPanel, 
		           "Endring av passord", JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {
			List<String> feilMeldinger = valider();
			if (feilMeldinger.isEmpty()) {				
				Bruker bruker = accessControlBean.hentBruker(brukernavn);
				bruker.setPassordHashed(new HashingUtils().toHash(new String(newPasswordField.getPassword())));
				bruker.setByttPassord(false);
				accessControlBean.oppdaterBruker(bruker, brukernavn);
				JOptionPane.showMessageDialog(null, "Passordet er endret");
			} else{
				
				StringBuilder sb = new StringBuilder();
				sb.append("<html>Følgende feil ble funnet:<br><br>");
				
				for (String feil : feilMeldinger) {
					sb.append("- " + feil + "<br>");
				}
				
 				JOptionPane.showMessageDialog(null, sb.toString());
 				initGUI();
			}
		}
	}


	private List<String> valider(){
		
		List<String> feilmeldinger = new ArrayList<String>();
		
		boolean sammePassword = new String(newPasswordField.getPassword()).equals(new String(confirmNewPasswordField.getPassword()));
		
		Bruker bruker = accessControlBean.hentBruker(brukernavn);
		
		boolean gammeltPassordStemmer = bruker.getPassordHashed().equals(new HashingUtils().toHash(new String(oldPasswordField.getPassword())));	
			
		if (!gammeltPassordStemmer) {
			feilmeldinger.add("Gammelt passord er feil.");
		}
			
		if (!sammePassword) {
			feilmeldinger.add("'Nytt passord' og 'Bekreft nytt passord' stemmer ikke overens med hverandre.");
		}
		
		if (!passordOK()) {
			feilmeldinger.add("Passordet du har skrevet inn tilfredstiller ikke kravene.<br>" +
					"Passordet må være på minst 8 tegn og bestå av minst en stor bokstav, en liten bokstav og ett siffer.<br>" +
					"Æ, Ø og Å, samt spesialtegn godtas ikke.");			
		}
				
		return feilmeldinger;
	}
	
	private boolean passordOK() {

		String password = new String(newPasswordField.getPassword());
		boolean hasDigits = false;
		boolean hasUpperCase = false;
		boolean hasLowerCase = false;
		boolean hasNoIllegalCharachters = false;
		
		if(password.matches("[a-zA-Z0-9]*")){
			hasNoIllegalCharachters = true;
			
			for (int i = 0; i < password.length(); i++) {
				Character c = password.charAt(i);
				if (Character.isDigit(c)) {
					hasDigits = true;
				}
				else {
					if (Character.isLowerCase(c)) {
						hasLowerCase = true;
					} else if (Character.isUpperCase(c)) {
						hasUpperCase = true;
					}
				}
			}
		}
		return (hasDigits && hasUpperCase && hasLowerCase && password.length() >= 8 && hasNoIllegalCharachters);
	}
}
