package no.mesta.mipss.studio.login;

import java.security.Principal;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import no.mesta.mipss.MIPSSStudio;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.logg.MipssLogger;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginModule {
    private LoginContext lc;
    private boolean loggedIn;
    private static final Logger logger = LoggerFactory.getLogger(LoginModule.class);
    private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");
 
    
    /**
     * Automatically verify the user on the LDAP
     * @throws LoginException
     */
    @SuppressWarnings("unchecked")
	private String autoLogin() throws LoginException {
        lc = new LoginContext("kerberosJaasNoPrompt", new LoginFrame());
        lc.login();
        String userName = System.getProperty("user.name");
        return userName;
    }
    
    /**
     * Display a login dialog to the user.
     * @throws LoginException
     */
    @SuppressWarnings("unchecked")
	private String displayLoginFrame() throws LoginException {
        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
        String username = "";
        if (loginFrame.getLoginType() == 1) {
            lc = new LoginContext("kerberosJaasNoPrompt", loginFrame);
            //it's potentially unsafe to use user.name as credential since any application can change 
            //this from within the runtime and on the command-line but since the LoginContext raises 
            //an exception if the username doesn't match with ldap, the execution will halt before 
            //we acquire the users name from System 
            username = System.getProperty("user.name");
        } else {
    		Boolean useAD = Boolean.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "useADLogIn").getVerdi());

        	if (useAD) {
        		lc = new LoginContext("kerberosJaas", loginFrame);
                username = loginFrame.getUsername();
                System.out.println(username);
			} else{				
				username = loginFrame.getUsername();
				lc = new LoginContext("noAD", loginFrame);            
	            System.out.println(username);
			}          
        }
        try{
        	lc.login();
        	new MipssLogger(username).logg(LoggType.LOGIN_MANUELL, "Bruker logget inn med innloggingsvindu");
        } catch (LoginException e){
        	new MipssLogger(username).logg(LoggType.LOGIN_FEILET, "Logg in feilet fra innloggingsvindu");
        	throw e;
        }
        return username;
    }

    public LoginContext getLoginContext() {
        return lc;
    }

    public Subject getSubject() {
        if(loggedIn) {
            return lc.getSubject();
        } else {
            return null;
        }
    }

    public void login(boolean manualLogin) {
    	logger.debug("login(" + manualLogin + ") start");
        loggedIn = false;
        
        String username = "";
        int retries = 0;
        while (retries < 3) {
            try {
                if (manualLogin) {
                    username = displayLoginFrame();
                } else {
                    String uname = autoLogin();
                    new MipssLogger(uname).logg(LoggType.LOGIN_AUTO, "Systemet logget inn brukeren automatisk");
                }
                loggedIn = true;
                break;                    
            } catch (LoginException e1) {
            	e1.printStackTrace();
                if(manualLogin) {
                	String msg = resources.getGuiString("studio.login.error");
                	String title = resources.getGuiString("studio.login.error.manualTitle");
                	MipssDialogue dialog = new MipssDialogue(null,IconResources.WARNING_ICON, title, msg, MipssDialogue.Type.WARNING, MipssDialogue.Button.OK);
                	dialog.askUser();
                } else {
                	String msg = resources.getGuiString("studio.login.error2");
                	String title = resources.getGuiString("studio.login.error.autoTitle");
                	logger.warn("auto login failed: " + username);
                	
                	new MipssLogger(username).logg(LoggType.LOGIN_FEILET, "Systemet klarte ikke å logge inn brukeren");
                    
                	MipssDialogue dialog = new MipssDialogue(null,IconResources.WARNING_ICON, title, msg, MipssDialogue.Type.WARNING, MipssDialogue.Button.OK);
                	dialog.askUser();                	
                }
                
                
                retries++;
                manualLogin = true;
            }
        }
        
        if(!loggedIn) {
            throw new RuntimeException("TODO noe bedre"); 
        }
    }

    /**
     * Log the user out from the LDAP server
     * @throws LoginException
     */
    public void logOut() throws LoginException {
    	if (!lc.getSubject().getPrincipals().isEmpty()){
	        String signatur = ((Principal) lc.getSubject().getPrincipals().toArray()[0]).getName();
			if (signatur.indexOf('@')!=-1){
				String bruker = signatur.substring(0, signatur.indexOf("@"));
				new MipssLogger(bruker).logg(LoggType.LOGG_UT, "px:"+MIPSSStudio.MOUSE_PX_COUNT+"/cn:"+MIPSSStudio.MOUSE_CL_COUNT);
			}else{
				new MipssLogger(signatur).logg(LoggType.LOGG_UT, "px:"+MIPSSStudio.MOUSE_PX_COUNT+"/cn:"+MIPSSStudio.MOUSE_CL_COUNT);
			}
    	}
    	lc.logout();
    	MIPSSStudio.MOUSE_CL_COUNT=0;
    	MIPSSStudio.MOUSE_PX_COUNT=0;
        loggedIn = false;
        
    }
}
