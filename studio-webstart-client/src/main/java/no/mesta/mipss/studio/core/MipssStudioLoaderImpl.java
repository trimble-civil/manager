package no.mesta.mipss.studio.core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.accesscontrol.PluginAccessException;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MainFrame;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.logg.MipssLogger;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginCacheKey;
import no.mesta.mipss.plugin.PluginLookupKey;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.pluginlist.PluginListModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.border.DropShadowBorder;

/**
 * Loaderen som håndterer åpning og lukking av plugins
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class MipssStudioLoaderImpl implements IMipssStudioLoader {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");
	private AccessControl accessControl = null;
	private Bruker bruker = null;
	private JXPanel busyPanel = null;
	private MipssPlugin dockedPlugin = null;
	private PluginListModel loadedPlugins = new PluginListModel();
	private Logger log = LoggerFactory.getLogger(MipssStudioLoaderImpl.class);
	private MainFrame mainFrame = null;
	private Map<PluginCacheKey, List<PluginMessage<?, ?>>> messages = new HashMap<PluginCacheKey, List<PluginMessage<?, ?>>>();
	private ArrayList<MenynodeDTO> menuList;
	private JXBusyLabel busyLabel;
	private Properties userSettings;
	private static final String USER_SETTINGS_FILE = System.getProperty("user.home")+File.separatorChar+".mipssSettings";
	
	/**
	 * Konstruktør
	 * 
	 * @param mainFrame
	 *            applikasjons frame
	 * @param bruker
	 *            pålogget bruker
	 */
	public MipssStudioLoaderImpl(MainFrame mainFrame, Bruker bruker, AccessControl accessControl, ArrayList<MenynodeDTO> menuList) {
		this.mainFrame = mainFrame;
		this.bruker = bruker;
		this.accessControl = accessControl;
		this.menuList=menuList;
	}
	
	public void loadProperties(){
		userSettings = new Properties();
		try {
			FileInputStream in= new FileInputStream(USER_SETTINGS_FILE);
			userSettings.load(in);
			in.close();
		} catch (FileNotFoundException e) {
//			e.printStackTrace();
			log.debug("No user settings present..");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void saveProperties(){
		try {
			FileOutputStream out= new FileOutputStream(USER_SETTINGS_FILE);
			userSettings.store(out, "---No Comment---");
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Properties getUserSettings(){
		loadProperties();
		return userSettings;
	}

	/** {@inheritDoc} */
	@Override
	public void activateMenuItem(MenynodeDTO menyNode) {
		mainFrame.activateMenuItem(menyNode);
	}

	/**
	 * @see no.mesta.mipss.core.IMipssStudioLoader#activatePlugin(PluginCacheKey)
	 * @param
	 */
	public void activatePlugin(PluginCacheKey key) {
		MipssPlugin plugin = loadedPlugins.get(key);

		if (plugin.isDocked()) {
			if (plugin == dockedPlugin) {
				return;
			}
			mainFrame.showPlugin(plugin, plugin.getDisplayName());
			dockedPlugin = plugin;
		}
	}

	/**
	 * Legger inn en beskjed for en plugin
	 * 
	 * @param plugin
	 * @param message
	 */
	private void addMessage(MipssPlugin plugin, PluginMessage<?, ?> message) {
		List<PluginMessage<?, ?>> pluginMessages = messages.get(plugin.getCacheKey());
		if (pluginMessages == null) {
			pluginMessages = new ArrayList<PluginMessage<?, ?>>();
			messages.put(plugin.getCacheKey(), pluginMessages);
		}

		pluginMessages.add(message);
	}

	/**
	 * Sjekker at der finnes et tilgangspunkt for meldingen og at brukeren har
	 * tilgang til modulen
	 * 
	 * @param message
	 * @return
	 */
	private boolean authorizeMessage(PluginMessage<?, ?> message) {
		Tilgangspunkt punkt = findEntryPoint(message);

		if (punkt == null) {
			log.warn("authorizeMessage(" + message + ") no entry point found");
			return false;
		}

		Modul modul = punkt.getModul();
		return hasAccess(bruker, modul);
	}

	/**
	 * Lukker en plugin
	 * 
	 * @param plugin
	 * @return true hvis den lukkes
	 */
	public boolean closePlugin(final MipssPlugin plugin) {
		killBusyLabel();
		new MipssLogger(getLoggedOnUserSign()).logg(LoggType.MODUL_AVSLUTT, plugin.getDisplayName());
		
		log.debug("closePlugin(" + plugin + ") start");
		mainFrame.clearMenuSelection();

		MipssDialogue dialogue = new MipssDialogue(getApplicationFrame(), IconResources.INFO_ICON, "Bekreft",
				resources.getGuiString("studio.pluginshutdown.notsaved"), MipssDialogue.Type.QUESTION,
				new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });

		plugin.readyForShutdown(dialogue);

		boolean shutdown = false;
		if (!dialogue.isUserAsked()) {
			shutdown = true;
		} else if (dialogue.isUserAsked() && dialogue.getPressedButton() == MipssDialogue.Button.OK) {

			shutdown = true;
		}

		if (shutdown) {
			if (dockedPlugin == plugin || dockedPlugin==null) {
				mainFrame.clearApplicationPanel();
				dockedPlugin = null;
			}

			SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
				protected Void doInBackground() {
					plugin.setStatus(MipssPlugin.Status.STOPPING);
					loadedPlugins.poke(plugin.getCacheKey());
					log.debug("closePlugin(" + plugin + ") initiating shutdown");
					plugin.shutdown();
					return null;
				}

				protected void done() {
					loadedPlugins.remove(plugin.getCacheKey());
					log.debug("closePlugin(" + plugin + ") unloaded");
				}
			};
			worker.execute();
			worker = null;
		}

		return shutdown;
	}

	/**
	 * Lager en instans av en klasse
	 * 
	 * @param clazz
	 * @param paramTypes
	 * @param paramValues
	 * @throws IllegalArgumentException
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T> T createInstance(Class<T> clazz, Class<?>[] paramTypes, Object[] paramValues) {
		T instance = null;

		try {
			if (paramTypes != null) {
				Constructor<?> c = clazz.getConstructor((Class<?>[]) paramTypes);
				instance = (T) c.newInstance(paramValues);
			} else {
				instance = clazz.newInstance();
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Could not create object", e);
		}

		return instance;
	}

	/**
	 * Lager en melding gitt et tilgangspunkt
	 * 
	 * @param target
	 * @param tilgangspunkt
	 */
	@SuppressWarnings("unchecked")
	private void createMessage(MipssPlugin target, Tilgangspunkt tilgangspunkt) {
		log.debug("createMessage(" + target + "," + tilgangspunkt + ") start");
		Class<PluginMessage> messageClass = getClazz(PluginMessage.class, tilgangspunkt.getMeldingType());
		PluginMessage<?, ?> message = createInstance(messageClass, null, null);
		message.setTargetPlugin(target);
		addMessage(target, message);
		log.debug("createMessage(" + target + "," + tilgangspunkt + ") done");
	}

	/**
	 * Initialiserer plugin objektet basert på menyDTO objektet
	 * 
	 * @param menuNode
	 * @return
	 */
	private MipssPlugin createMipssPlugin(MenynodeDTO menuNode) {
		log.debug("createMipssPlugin(" + menuNode + ") start");
		Tilgangspunkt tp = menuNode.getTilgangspunkt();

		MipssPlugin plugin = null;
		try {
			// instansier med parameter
			if (menuNode.getUrl() != null) {
				Class<?>[] types = { String.class };
				Object[] values = { menuNode.getUrl() };
				plugin = createPluginInstance(tp, types, values);
			} // ingen parameter
			else {
				plugin = createPluginInstance(tp, null, null);
			}

			if (plugin != null) {
				plugin.setMenynode(menuNode);
			}
		} catch (Exception e) {
			handleException(e, menuNode, "Menyen inneholdt ugyldig URL");
		}

		log.debug("createMipssPlugin(" + menuNode + ") finished");
		return plugin;
	}
	
	private MenynodeDTO getMenuNodeForTilgangspunkt(Tilgangspunkt tp){
		for (MenynodeDTO m:menuList){
			if (m.getTilgangspunkt().equals(tp))
				return m;
		}
		return null;
	}
	
	private MipssPlugin createPluginInstance(Tilgangspunkt tilgangspunkt, Class<?>[] paramTypes, Object[] paramValues) {
		log.debug("createPluginInstance(" + tilgangspunkt + "," + paramTypes + "," + paramValues + ") start");
		Modul modul = tilgangspunkt.getModul();

		MipssPlugin plugin = null;
		try {
			// Hent klasseobjektet til menypunktet
			Class<MipssPlugin> pluginClass = getClazz(MipssPlugin.class, tilgangspunkt.getKlassenavn());
			plugin = createInstance(pluginClass, paramTypes, paramValues);

			PluginCacheKey key = new PluginCacheKey(tilgangspunkt, pluginClass, paramValues);
			plugin.setCacheKey(key);
			plugin.setModul(modul);
			plugin.setTilgangspunkt(tilgangspunkt);
			if (plugin.getMenynode()==null){
				MenynodeDTO mdto = getMenuNodeForTilgangspunkt(tilgangspunkt);
				plugin.setMenynode(mdto);
				if (mdto!=null){
					mdto.setFjernValgt(true);
					activateMenuItem(mdto);
					mdto.setFjernValgt(false);
				}
			}
			
			if (tilgangspunkt.getMeldingType() != null) {
				createMessage(plugin, tilgangspunkt);
			}

			loadedPlugins.push(key, plugin);
		} catch (Exception e) {
			if (plugin != null) {
				handleException(e, modul, "Feil i pluginoppsett");
			} else {
				handleException(e, (Object) plugin, "Feil i pluginoppsett");
			}
		}

		log.debug("createPluginInstance(" + tilgangspunkt + "," + paramTypes + "," + paramValues + ") end with: "
				+ plugin);
		return plugin;
	}

	private void executeMessages(MipssPlugin plugin) {
		log.debug("executeMessages(" + plugin + ") start");
		List<PluginMessage<?, ?>> pluginMessages = messages.get(plugin.getCacheKey());
		if (pluginMessages == null || pluginMessages.size() == 0) {
			log.debug("executeMessages(" + plugin + ") done, nothing to do");
			return;
		}
		List<PluginMessage<?, ?>> toExecute = new ArrayList<PluginMessage<?, ?>>(pluginMessages);
		for (PluginMessage<?, ?> message : toExecute) {
			plugin.receiveMessage(message);
			if (message.isConsumed()) {
				pluginMessages.remove(message);
			}
		}
		log.debug("executeMessages(" + plugin + ") done");
	}

	/**
	 * Finner et tilgangspunkt basert på meldingstypen og klassen til target i
	 * meldingen
	 * 
	 * @param message
	 * @return
	 */
	private Tilgangspunkt findEntryPoint(PluginMessage<?, ?> message) {
		return findEntryPoint(message.getTargetPlugin().getClass().getName(), message.getClass().getName());
	}

	/**
	 * Finner et tilgangspunkt basert på meldingstypen og klassen til target i
	 * meldingen
	 * 
	 * @param pluginClass
	 * @param messageClass
	 * @return
	 */
	private Tilgangspunkt findEntryPoint(String pluginClass, String messageClass) {
		
		Tilgangspunkt tp = accessControl.finnMeldingspunkt(pluginClass, messageClass); //TODO er dette lurt? messageClass ble tidligere benyttet.. har det noe å si??.
		if (tp==null){
			tp = accessControl.finnMeldingspunkt(pluginClass, null);
		}
		return tp;
	}

	/** {@inheritDoc} */
	@Override
	public void flashMessage(String message, int priority) {
		mainFrame.flashMessage(message, priority);
	}

	/**
	 * Returnerer vinduet til applikasjonen
	 * 
	 * @return
	 */
	public JFrame getApplicationFrame() {
		return mainFrame.getApplicationFrame();
	}

	/**
	 * Henter ut klasse objektet fra et klassenavn
	 * 
	 * @param className
	 * @throws IllegalStateException
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T> Class<T> getClazz(Class<T> typeClazz, String className) {
		Class<?> clazz = null;

		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("Could not get class of type " + typeClazz.getName(), e);
		}
		return (Class<T>) clazz;
	}

	/** {@inheritDoc} */
	public Bruker getLoggedOnUser(boolean refresh) {
		if (refresh){
			bruker = accessControl.hentBruker(bruker.getSignatur(), true);
		}
		return bruker;
	}
	

	/** {@inheritDoc} */
	public String getLoggedOnUserSign() {
		return bruker.getSignatur();
	}

	/** {@inheritDoc} */
	public MipssPlugin getPlugin(Class<? extends MipssPlugin> pluginClass) {
		MipssPlugin plugin = loadedPlugins.find(new PluginLookupKey(pluginClass, null));

		if (plugin == null) {
			Tilgangspunkt tp = findEntryPoint(pluginClass.getName(), null);
			if (hasAccess(bruker, tp.getModul())) {
				plugin = getPlugin(tp, null, null);
				plugin.setStartInBackground(true);
				startPlugin(plugin);
			}
		}

		return plugin;
	}

	/**
	 * 
	 * @param tilgangspunkt
	 * @param paramTypes
	 * @param paramValues
	 * @return
	 */
	private MipssPlugin getPlugin(Tilgangspunkt tilgangspunkt, Class<?>[] paramTypes, Object[] paramValues) {
		log.debug("getPlugin(" + tilgangspunkt + "," + paramTypes + "," + paramValues + ") start");
		Class<MipssPlugin> pluginClass = getClazz(MipssPlugin.class, tilgangspunkt.getKlassenavn());
		PluginLookupKey searchKey = new PluginLookupKey(pluginClass, paramValues);
		MipssPlugin plugin = loadedPlugins.find(searchKey);

		if (plugin == null) {
			plugin = createPluginInstance(tilgangspunkt, paramTypes, paramValues);
		}

		log.debug("getPlugin(" + tilgangspunkt + "," + paramTypes + "," + paramValues + ") end with: " + plugin);
		return plugin;
	}
	
	/** {@inheritDoc} */
	public boolean isPluginRunning(Class<? extends MipssPlugin> pluginClass){
		MipssPlugin plugin = loadedPlugins.find(new PluginLookupKey(pluginClass, null));
		return plugin!=null;
		
	}
	/**
	 * @see no.mesta.mipss.core.IMipssStudioLoader#getRunningPluginsKeys()
	 * 
	 * @return
	 */
	public PluginListModel getRunningPluginsKeys() {
		return loadedPlugins;
	}

	private String getSourceName(Object source) {
		String sourceName = "ukjent";

		if (source != null) {
			if (source instanceof Modul) {
				sourceName = ((Modul) source).getNavn();
			} else if (source instanceof MipssPlugin) {
				MipssPlugin plugin = (MipssPlugin) source;
				sourceName = plugin.getModul().getNavn() + ": " + plugin.getDisplayName();
			}
		}

		return sourceName;
	}

	/**
	 * Behandler feil som oppstår
	 * 
	 * @param e
	 * @param source
	 * @param userMessage
	 */
	public void handleException(Throwable e, Object source, String userMessage) {
		String sourceName = getSourceName(source);
		handleException(e, source, sourceName, userMessage);
	}

	/**
	 * Behandler en feil som har oppstått
	 * 
	 * @param e
	 * @param source
	 * @param sourceName
	 * @param userMessage
	 */
	public void handleException(Throwable e, Object source, String sourceName, String userMessage) {
		if (sourceName == null) {
			sourceName = getSourceName(source);
		}

		log.error("Error occured in application. Source: " + sourceName + "source:{" + source + "}", e);
		MipssDialogue dialogue = new MipssDialogue(getApplicationFrame(), IconResources.WARNING_ICON,"Systemfeil", resources.getGuiString("studio.errormessage", new Object[]{sourceName, userMessage}),
				MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
		dialogue.askUser();
	}

	/**
	 * Undersøker med serveren (accessControl) om brukeren har tilgang til
	 * modulen
	 * 
	 * @param bruker
	 * @param modul
	 * @return
	 */
	private Boolean hasAccess(Bruker bruker, Modul modul) {
		boolean access = true;
		try {
			accessControl.hasAccess(bruker.getSignatur(), modul.getId());
		} catch (PluginAccessException e) {
			access = false;
		}

		return access;
	}

	public void logException(Throwable e, Object source, String developerMessage) {
		String sourceName = getSourceName(source);
		flashMessage("En feil er blitt logget", -1);
		log.error("Error occured in application. Source: " + sourceName, e);
	}

	/**
	 * Initialiserer en plugin basert på et menypunkt, og sender den til
	 * starting
	 * 
	 * @param menuNode
	 */
	public void openMenuNode(MenynodeDTO menuNode) {
		log.debug("openMenuNode(" + menuNode + ") start");
		Tilgangspunkt tp = menuNode.getTilgangspunkt();
		Object[] values = (menuNode.getUrl() == null ? null : new Object[] { menuNode.getUrl() });
		Class<MipssPlugin> pluginClass = getClazz(MipssPlugin.class, tp.getKlassenavn());
		MipssPlugin plugin = loadedPlugins.find(new PluginLookupKey(pluginClass, values));

		if ((plugin != null && !plugin.isDocked()) || plugin == null) {
			log.debug("openMenuNode(" + menuNode + ") no suitable plugin found, creating new instance");
			plugin = createMipssPlugin(menuNode);
			String modul = plugin.getModul().getNavn() + ": " + plugin.getDisplayName();
			if (plugin != null) {
				plugin.setMenynode(menuNode);
				loadedPlugins.poke(plugin.getCacheKey());
			}
			flashMessage("Starter \"" + modul + "\"...", 1);
			startPlugin(plugin);
		} else if (plugin != null && plugin.getStatus().equals(MipssPlugin.Status.STARTING)) {
			String modul = plugin.getModul().getNavn() + ": " + plugin.getDisplayName();
			flashMessage("Vennligst vent, modul \"" + modul + "\" starter...", 2);
			plugin.setTilgangspunkt(tp);
			plugin.setMenynode(menuNode);
			if (tp.getMeldingType() != null) {
				createMessage(plugin, tp);
				executeMessages(plugin);
			}
		} else {
			String modul = plugin.getModul().getNavn() + ": " + plugin.getDisplayName();
			dockedPlugin = plugin;
			plugin.setTilgangspunkt(tp);
			plugin.setMenynode(menuNode);
			mainFrame.showPlugin(plugin, plugin.getDisplayName());
			flashMessage("Viser \"" + modul + "\"", 3);

			if (tp.getMeldingType() != null) {
				createMessage(plugin, tp);
				executeMessages(plugin);
			}
		}

		log.debug("openMenuNode(" + menuNode + ") finished");
	}

	/**
	 * Callback fra plugin når den er klar
	 * 
	 * @param plugin
	 */
	public void pluginReady(MipssPlugin plugin) {
		log.debug("pluginReady(" + plugin + ") start");

		PluginCacheKey readyKey = plugin.getCacheKey();
		MipssPlugin lastloadedPlugin = loadedPlugins.peek();
		PluginCacheKey lastStartedKey = lastloadedPlugin.getCacheKey();
		if (readyKey.equals(lastStartedKey) && !plugin.isStartInBackground()) {
			stopPluginLoaderPlaceHolder();
			dockedPlugin = plugin;
			mainFrame.showPlugin(plugin, plugin.getDisplayName());
		}

		plugin.setStatus(MipssPlugin.Status.RUNNING);
		loadedPlugins.poke(readyKey);

		executeMessages(plugin);
		killBusyLabel();
		
		log.debug("pluginReady(" + plugin + ") stop");
	}
	private void killBusyLabel(){
		if (busyLabel!=null){
			busyLabel.setBusy(false);
			busyLabel=null;
		}
	}
	/**
	 * Kalles når en plugin løses ut
	 * 
	 */
	public void popOut(MipssPlugin plugin) {
		new MipssLogger(getLoggedOnUserSign()).logg(LoggType.MODUL_FLOAT, plugin.getDisplayName());
		setStatusMessage("Løser ut " + plugin.getDisplayName());
		mainFrame.clearMenuSelection();
		if (dockedPlugin == plugin) {
			dockedPlugin = null;
		}

		plugin.setDocked(false);
		loadedPlugins.poke(plugin.getCacheKey());
		mainFrame.clearApplicationPanel();
		flashMessage("Løst ut " + plugin.getDisplayName(), 0);
	}

	/**
	 * Avviser brukeren for en gitt modul
	 * 
	 * @param modul
	 */
	private void rejectModuleAccess(Modul modul) {
		new MipssLogger(getLoggedOnUserSign()).logg(LoggType.MODUL_IKKE_TILGANG, modul.getNavn());
		String message = resources.getGuiString("studio.plugins.plugins.accessdenied", new Object[]{modul.getNavn()});
		String title = resources.getGuiString("studio.plugins.plugins.title");
		MipssDialogue dialogue = new MipssDialogue(getApplicationFrame(), IconResources.INFO_ICON, title,
				message, MipssDialogue.Type.INFO,
				new MipssDialogue.Button[] { MipssDialogue.Button.OK });
		dialogue.askUser();
	}

	/** {@inheritDoc} */
	public void sendMessage(PluginMessage<?, ?> message) {
		if (authorizeMessage(message)) {
			addMessage(message.getTargetPlugin(), message);
			executeMessages(message.getTargetPlugin());
		} else {
			rejectModuleAccess(message.getTargetPlugin().getModul());
			message.getTargetPlugin().shutdown();
		}
	}

	/**
	 * Setter en status melding i hovedvinduet
	 * 
	 * @param message
	 */
	public void setStatusMessage(String message) {
		mainFrame.setStatusMessage(message);
	}

	/**
	 * Starter en plugin, brukes til "inter plugin kommunikasjon"
	 * 
	 * @param tilgangspunkt
	 * @param paramTypes
	 * @param paramValues
	 */
	public void startAccessPoint(Tilgangspunkt tilgangspunkt, Class<?>[] paramTypes, Object[] paramValues) {
		log.debug("startAccessPoint(" + tilgangspunkt + "," + paramTypes + "," + paramValues + ") start");

		MipssPlugin plugin = getPlugin(tilgangspunkt, paramTypes, paramValues);
		startPlugin(plugin);
		log.debug("startAccessPoint(" + tilgangspunkt + "," + paramTypes + "," + paramValues + ") end with: " + plugin);
	}

	/**
	 * Starter en plugin etter å ha sjekket om brukeren har tilgang til å starte
	 * den.
	 * 
	 * @param plugin
	 */
	public void startPlugin(final MipssPlugin plugin) {
		log.debug("startPlugin(" + plugin + ") start");
		if (plugin != null) {
			// Plugin kan være null hvis tilgang har blitt avslått
			// i en tidligere tilgangskontroll
			// eller annen feil har oppstått i oppstart av plugin

			if (hasAccess(bruker, plugin.getModul())) {
				if (!plugin.isStartInBackground()) {
					startPluginLoaderPlaceHolder(plugin);
				}
				plugin.setStatus(MipssPlugin.Status.STARTING);
				loadedPlugins.poke(plugin.getCacheKey());
				plugin.init(this);
				new MipssLogger(getLoggedOnUserSign()).logg(LoggType.MODUL_START, plugin.getDisplayName());
			} else {
				loadedPlugins.remove(plugin.getCacheKey());
				rejectModuleAccess(plugin.getModul());
				flashMessage("Tilgang avvist", 99);
			}
		}
		log.debug("startPlugin(" + plugin + ") end");
	}

	/**
	 * Ventebilde mens plugin starter
	 * 
	 */
	private void startPluginLoaderPlaceHolder(MipssPlugin plugin) {
		String pluginName = plugin.getDisplayName();
		busyPanel = new JXPanel(new GridBagLayout());
		busyPanel.setOpaque(false);
		
		busyPanel.setPreferredSize(mainFrame.getApplicationPanelSize());
		busyLabel = new JXBusyLabel();
		DropShadowBorder border = new DropShadowBorder(Color.black, 10, .5f, 7, true, true, true, true);
		busyLabel.setBorder(border);
		busyLabel.setBusy(true);
		busyLabel.setOpaque(true);
		busyLabel.setBackground(Color.white);
		busyLabel.setText("Laster "+pluginName);
		busyLabel.setPreferredSize(new Dimension(200,70));
		busyPanel.add(busyLabel, new GridBagConstraints(0,0,1,1,1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10,10,10,10),0,0));
//		Dimension pos = mainFrame.getApplicationPanelSize();
//		busyLabel.setBounds((int) (pos.getWidth() / 2) - 60, (int) (pos.getHeight() / 2) - 60, 120, 120);
		mainFrame.showPanel(busyPanel, "Laster " + pluginName);
//		mainFrame.addPanel(busyPanel);
		flashMessage("Vennligst vent mens " + pluginName + " laster...", 0);
	}

	/**
	 * Avslutter ventebildet for plugin lasting
	 * 
	 */
	private void stopPluginLoaderPlaceHolder() {
//		mainFrame.clearApplicationPanel();
		busyPanel = null;
	}

	@Override
	public Driftkontrakt getSelectedDriftkontrakt() {
		return mainFrame.getSelectedDriftkontrakt();
	}

	@Override
	public Connection getConnection() {
		String jdbc = KonfigparamPreferences.getInstance().hentEnForApp("mipss.admin", "jdbc.url").getVerdi();
		String usr = KonfigparamPreferences.getInstance().hentEnForApp("mipss.admin", "jdbc.usr").getVerdi();
		String pwd = KonfigparamPreferences.getInstance().hentEnForApp("mipss.admin", "jdbc.pwd").getVerdi();
		Connection c=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			c = DriverManager.getConnection(jdbc, usr, pwd);
		} catch (SQLException e) {
			log.error("Problemer med oppkobling til databasen", e);
			e.printStackTrace();
		//	JOptionPane.showMessageDialog(null, "Klarte ikke å koble til oracle\n"+e.getMessage(), "OracleServerConnection", JOptionPane.ERROR_MESSAGE);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}
}
