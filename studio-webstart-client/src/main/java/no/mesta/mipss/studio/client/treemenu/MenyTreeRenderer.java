package no.mesta.mipss.studio.client.treemenu;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;

/**
 * TreeCellRenderer for tree menyen, denne bestemmer hvordan nodene skal se ut.
 *
 * @author harkul
 */
@SuppressWarnings("serial")
public class MenyTreeRenderer extends DefaultTreeCellRenderer {
    public Component getTreeCellRendererComponent(JTree tree, Object value, 
                                                  boolean selected, 
                                                  boolean expanded, 
                                                  boolean leaf, int row, 
                                                  boolean hasFocus) {

        Component temp = super.getTreeCellRendererComponent(tree, value, selected, expanded, 
                                               leaf, row, hasFocus);
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
        if (node.getUserObject() instanceof MenynodeDTO) {
            MenynodeDTO menu = (MenynodeDTO)((DefaultMutableTreeNode)value).getUserObject();
            
            if (menu.isFjernValgt()){
                this.setBackgroundNonSelectionColor(new Color(245, 200, 255));
            } 
            /*else if (menu.isStartet()){
                this.setBackgroundNonSelectionColor(new Color(200, 200, 255));
            }*/
            else{
                setBackgroundNonSelectionColor(Color.white);
            }
            ImageIcon icon = null;
            ImageIcon ikon = menu.getMenynodeIkon();
            if (ikon!=null){
                icon = ikon;
            }
            else if (menu.getUrl() != null) {
                icon = new ImageIcon(getClass().getResource("/images/webexport.png"));
            } else {
                if (node.getChildCount() == 0)
                    icon = new ImageIcon(getClass().getResource("/images/kpersonalizer.png"));
                else
                    icon = new ImageIcon(getClass().getResource("/images/kcmkwm.png"));
            }
            setIcon(icon);
            menu.setMenynodeIkon(icon);
            if (!menu.isValgbar()){
            	setBackground(Color.white);
            	setForeground(Color.lightGray);
            }
        }
        if (node.getUserObject() instanceof Menygruppe){
            ImageIcon icon = ((Menygruppe) node.getUserObject()).getImageIcon();
            if(icon != null) {
                setIcon(icon);
            }
//            setFont(new Font("Tahoma", Font.BOLD, 12));
            //log.debug(getFont());
        }
//        setOpenIcon(new ImageIcon(ImageUtil.scaleImage(IconResources.MIPSS_ICON, 12, 12)));
//        setClosedIcon(new ImageIcon(ImageUtil.scaleImage(IconResources.MIPSS_ICON, 12, 12)));
        getClosedIcon();
        return temp;
    }
}
