package no.mesta.mipss.studio.client.treemenu;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.dto.MenynodeDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Lytter til menyvalg brukeren gjør
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class MenyTreeListener implements TreeSelectionListener {
	private static int callcounter = 0;
	private static final Logger log = LoggerFactory.getLogger(MenyTreeListener.class);
	private IMipssStudioLoader loader = null;
	private TreePath path;
	/**
	 * Konstruktør
	 * 
	 * @param loader
	 */
	public MenyTreeListener(IMipssStudioLoader loader) {
		this.loader = loader;
	}

	/**
	 * Kjøres hver gang brukeren trykker i menyen
	 * 
	 * @param e
	 */
	public void valueChanged(TreeSelectionEvent e) {
		
		callcounter++;
		log.debug(callcounter + ":valueChanged(TreeSelectionEvent) e: " + e);

		JTree tree = (JTree) e.getSource();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if (tree.getModel() instanceof MenyTreeModel) {
			MenyTreeModel model = (MenyTreeModel) tree.getModel();
			if (model.isHasFjernValgt())
				model.resetFjernValgt();
		}
		
		Object userObject = node == null ? null :node.getUserObject();
		if (node == null) {
			tree.clearSelection();
		} else if (node.getUserObject() instanceof MenynodeDTO && !((MenynodeDTO) userObject).isFjernValgt()) {
			if (((MenynodeDTO)node.getUserObject()).isValgbar()){
				loader.openMenuNode((MenynodeDTO) node.getUserObject());
				path = e.getPath();
			}else{
				if (path!=null){
					tree.setSelectionPath(path);
				}else{
					tree.clearSelection();
				}
			}
		}else{
			path = e.getPath();
		}
		
	}
}
