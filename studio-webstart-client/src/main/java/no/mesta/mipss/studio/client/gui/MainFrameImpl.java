package no.mesta.mipss.studio.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Stack;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.TransferHandler;
import javax.swing.plaf.OptionPaneUI;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import no.mesta.mipss.MIPSSStudio;
import no.mesta.mipss.brukerpreferanser.BrukerpreferanserService;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.MainFrame;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.persistence.applikasjon.Brukerpreferanser;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.studio.client.treemenu.MenyTreeListener;
import no.mesta.mipss.studio.client.treemenu.MenyTreeModel;
import no.mesta.mipss.studio.client.treemenu.MenyTreeRenderer;
import no.mesta.mipss.studio.client.treemenu.WebNodePopupMenu;
import no.mesta.mipss.studio.login.ChangePasswordDialog;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.pluginlist.JMipssPluginButtonList;
import no.mesta.mipss.ui.pluginlist.JMipssPluginsList;
import no.mesta.mipss.ui.systeminfo.JSystemInfoMenuItem;
import no.mesta.mipss.ui.systeminfo.JThreadDumpMenuItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.KeyFrames;
import org.jdesktop.animation.timing.interpolation.KeyTimes;
import org.jdesktop.animation.timing.interpolation.KeyValues;
import org.jdesktop.animation.timing.interpolation.PropertySetter;
import org.jdesktop.swingx.JXTitledPanel;

/**
 * Hoved vinduet til MIPSS Studio
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class MainFrameImpl extends JFrame implements MainFrame{
	private static PropertyResourceBundleUtil resources = PropertyResourceBundleUtil
			.getPropertyBundle("mipssStudioText");
	private FloatPanel applicationPanel;
	private Message currentMessage;
	private IMipssStudioLoader loader;
	private static final Logger log = LoggerFactory.getLogger(MainFrameImpl.class);
	/** containeren til menyen */
	private JXTitledPanel menuContainer;
	private List<MenynodeDTO> menuList;
	/** panelet som inneholder menyen */
	private JPanel menuPanel;
	/** Treet som inneholder menyen */
	private JTree menuTree;
	private Stack<Message> messages = new Stack<Message>();
	private JSplitPane splitPane;
	private JPanel applicationContentPanel;
	private JLayeredPane applicationContainer;
	
	
	/** Label som viser frem statusmeldingen */
	private JLabel statusLabel;
	
	private JMipssContractPicker contractPicker;

	/**
	 * Creates a new MainFrame with the specified title
	 * 
	 * @param title
	 *            the window title
	 */
	public MainFrameImpl(String title, List<MenynodeDTO> menuList) {
		super(title);
		this.menuList = menuList;
	}

	public void clearApplicationPanel() {
		if (applicationPanel != null) {
			applicationPanel.dispose();
			applicationPanel = null;
		}
		doTransition();
	}
	private void doTransition(){		
		applicationContentPanel.removeAll();
		if (applicationPanel==null){
			applicationContentPanel.add(new BackgroundPanel());
		}else{
			applicationContentPanel.add(applicationPanel);
		}
		setVisible(true);
	}
	/** {@inheritDoc} */
	@Override
	public void clearMenuSelection() {
		menuTree.clearSelection();
	}

	/**
	 * Create a default JMenuBar
	 * 
	 * @return JMenuBar TODO should this be created and applied by the
	 *         individual sub applications or is it sufficient to create log off
	 *         and exit action menus directly in the mainframe?
	 */
	private JMenuBar createMenu() {
		JMenuBar menu = new JMenuBar();
		JSeparator sep = new JSeparator();

		JMenu fileMenu = new JMenu(resources.getGuiString("manager.menu.file"));
		fileMenu.setMnemonic('F');

		JMenuItem logoffItem = new JMenuItem(resources.getGuiString("manager.menu.logOff"));
		logoffItem.setMnemonic('L');
		logoffItem.addActionListener(new LogoffAction(this));
		fileMenu.add(logoffItem);

		fileMenu.add(sep);
		
		JMenuItem changePasswordItem = new JMenuItem(resources.getGuiString("manager.menu.changePassword"));
		changePasswordItem.addActionListener(new ChangePasswordAction(this, loader));
		fileMenu.add(changePasswordItem);

		fileMenu.add(sep);

		// meny for å printe ut alle kjørende tråder til console.
		JMenuItem threadMenu = new JThreadDumpMenuItem(loader);
		fileMenu.add(threadMenu);

		// Debug informasjon
		JMenuItem sysMenu = new JSystemInfoMenuItem(loader);
		fileMenu.add(sysMenu);

		fileMenu.add(sep);
		
		JMenuItem logMenu = new JMenuItem(resources.getGuiString("manager.menu.showLog"));
		logMenu.setMnemonic('o');
		logMenu.addActionListener(new ShowLogAction(this));
		fileMenu.add(logMenu);

        fileMenu.add(sep);

		JMenuItem aboutMenu = new JMenuItem(resources.getGuiString("manager.menu.about"));
		aboutMenu.setMnemonic('m');
		aboutMenu.addActionListener(e -> JOptionPane.showMessageDialog(
				MainFrameImpl.this,
				resources.getGuiString("manager.about.message"),
				resources.getGuiString("manager.about.title"),
				JOptionPane.PLAIN_MESSAGE));
		fileMenu.add(aboutMenu);

		fileMenu.add(sep);

		JMenuItem exitItem = new JMenuItem(resources.getGuiString("manager.menu.exit"));
		exitItem.setMnemonic('x');
		exitItem.setIcon(IconResources.CANCEL_ICON);
		exitItem.addActionListener(e -> {
            int rvl = JOptionPane.showConfirmDialog(
            		MainFrameImpl.this,
                    resources.getGuiString("studio.quit.message"), resources.getGuiString("studio.quit.title"),
                    JOptionPane.YES_NO_OPTION);
            if (rvl == JOptionPane.YES_OPTION)
                System.exit(0);
        });
		fileMenu.add(exitItem);

		menu.add(fileMenu);
		return menu;
	}

	/** {@inheritDoc} */
	@Override
	public void flashMessage(String msg, int priority) {}

	/**
	 * @see no.mesta.mipss.core.MainFrame#getApplicationFrame()
	 * @return
	 */
	public JFrame getApplicationFrame() {
		return this;
	}

	/**
	 * @see no.mesta.mipss.core.MainFrame#getApplicationPanelSize()
	 * 
	 * @return
	 */
	public Dimension getApplicationPanelSize() {
		return splitPane.getRightComponent().getSize();
	}

	public JPanel getMenuPanel() {
		return menuPanel;
	}

	public void init(IMipssStudioLoader loader) {
		this.loader = loader;
		contractPicker = new JMipssContractPicker(loader.getLoggedOnUser(false), false);
		
		Driftkontrakt prefertertDriftKontrakt = finnKontraktvalgFraPreferences(contractPicker);
		if(prefertertDriftKontrakt!=null){
			contractPicker.getModel().setSelectedItem(prefertertDriftKontrakt);
		}
		
		contractPicker.setBorder(null);
		//Contraktpicker skal lagre valgene brukeren utfører i databasen, implementerer lytter...
		contractPicker.addPropertyChangeListener("valgtKontrakt",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				lagreValgtKontrakt(evt);
			}
		});
		
		applicationContentPanel = new JPanel(new BorderLayout());
		applicationContainer = new JLayeredPane();

		JPanel cPanel = new JPanel();
		cPanel.setLayout(new BoxLayout(cPanel, BoxLayout.PAGE_AXIS));
		cPanel.add(Box.createRigidArea(new Dimension(0,5)));
		
		setSize(1600, 900);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setIconImage(IconResources.MIPSS_ICON.getImage());

		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		applicationContainer.setLayout(new BorderLayout());
		applicationContainer.add(applicationContentPanel);
		splitPane.setRightComponent(applicationContainer);
		clearApplicationPanel(); // Setter blank flate for hovedpanelet til høyre

		menuContainer = new JXTitledPanel("Studio-meny");
		menuContainer.setMinimumSize(new Dimension(200, 300));
		splitPane.setLeftComponent(menuContainer);
		add(splitPane);
		setJMenuBar(createMenu());

		// create the menu tree
		menuTree = initMenuTree(menuList);
		cPanel.setBackground(menuTree.getBackground());
		JPanel treePanel = new JPanel(new BorderLayout());
		treePanel.add(cPanel, BorderLayout.NORTH);
		treePanel.add(menuTree);
		setMenuPanel(treePanel);
		
		
		// Lag meny for kjørende plugins
		JMipssPluginsList pluginsList = new JMipssPluginsList(loader, "Moduler", IconResources.MODULE_ICON);

		// create a status panel
		JPanel statusPnl = new JPanel(new BorderLayout());
		statusLabel = new JLabel("..");
		statusPnl.add(BorderLayout.WEST, statusLabel);
		statusPnl.add(BorderLayout.EAST, pluginsList);
		statusPnl.setBorder(BorderFactory.createEtchedBorder());
		add(statusPnl, BorderLayout.PAGE_END);

		// show frame
		setVisible(true);
	}

	/**
	 * Forsøker å finne ut hvilken kontrakt i grunnlaget for pickeren, som skal settes til valgt
	 * @param contractPicker
	 * @return
	 */
	private Driftkontrakt finnKontraktvalgFraPreferences(JMipssContractPicker contractPicker) {
		List<Driftkontrakt> driftKontraktList = contractPicker.getModel().getKontrakter();
		
		BrukerpreferanserService brukerpreferanserService =	BeanUtil.lookup(BrukerpreferanserService.BEAN_NAME, BrukerpreferanserService.class);
		Brukerpreferanser brukerPreferanser = brukerpreferanserService.getPreferanserForBruker(loader.getLoggedOnUser(false));
		if(brukerPreferanser!=null && brukerPreferanser.getValgtKontraktId()!=null){
			for (Driftkontrakt driftkontrakt : driftKontraktList) {
				if(brukerPreferanser.getValgtKontraktId().equals(driftkontrakt.getId())){
					return driftkontrakt;
				}
			}
		}
		return null;
	}

	/**
	 * Metoden lagrer kontraktsvalget i tabellen for brukerpreferanser hver gang bruker velger noe i denne.
	 * @param evt
	 */
	protected void lagreValgtKontrakt(PropertyChangeEvent evt) {
		Driftkontrakt selectedContract = (Driftkontrakt)evt.getNewValue();
		
		Brukerpreferanser preferanse = new Brukerpreferanser();
		preferanse.setSignatur(loader.getLoggedOnUser(false).getSignatur());
		if(selectedContract!=null){
			preferanse.setValgtKontraktId(selectedContract.getId());
		}else{
			preferanse.setValgtKontraktId(null);
		}
		

		BrukerpreferanserService brukerpreferanserService =	BeanUtil.lookup(BrukerpreferanserService.BEAN_NAME, BrukerpreferanserService.class);
		brukerpreferanserService.setPreferanserForBruker(preferanse);
	}

	/**
	 * Creates the menu tree
	 * 
	 * @param menuList
	 * @return
	 */
	private JTree initMenuTree(List<MenynodeDTO> menuList) {
		final JTree tree = new JTree();
		tree.setName("menuTree");
		tree.setModel(new MenyTreeModel(menuList, tree));
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.addMouseListener(new WebNodePopupMenu(loader));
		tree.addTreeSelectionListener(new MenyTreeListener(loader));
		tree.setCellRenderer(new MenyTreeRenderer());
		tree.setRowHeight(20);
		tree.setRootVisible(false);
		tree.setBorder(BorderFactory.createEmptyBorder());
		
		return tree;
	}

	private void processMessage() {
		Message message = messages.pop();
		currentMessage = message;
		currentMessage.show();
	}

	private void processMessages() {
		if (!messages.isEmpty()) {
			if (currentMessage == null || currentMessage.permanent) {
				processMessage();
			} else {
				Message message = messages.peek();
				if (message.isMoreImportantOrEqual(currentMessage)) {
					currentMessage.cancelled = true;
					processMessage();
				}
			}
		} else {
			currentMessage = null;
		}
	}

	public void setMenuPanel(JPanel panel) {
		menuContainer.getContentContainer().removeAll();
		menuContainer.getContentContainer().setLayout(new BorderLayout());
		if (panel != null) {
			JScrollPane scroll = new JScrollPane(panel);
			scroll.setBorder(null);
			
			menuContainer.getContentContainer().add(contractPicker, BorderLayout.NORTH);
			menuContainer.getContentContainer().add(scroll);
			menuContainer.getContentContainer().add(new JMipssPluginButtonList(loader), BorderLayout.SOUTH);
			menuPanel = panel;
		}

		menuContainer.validate();
	}

	public void setStatusMessage(String statusMessage) {}

	public void showPanel(JPanel panel, String displayName) {
		applicationContentPanel.add(panel);
	}

	/**
	 * Viser en plugin i hoved vinduet
	 * 
	 * @param plugin
	 * @param displayName
	 */
	public void showPlugin(MipssPlugin plugin, String displayName) {
		if (plugin != null) {
			if (applicationPanel != null) {
				applicationPanel.dispose();
				applicationPanel = null;
			}
			applicationPanel = new FloatPanel(plugin, displayName, this, loader);
			doTransition();
		}
	}

	/**
	 * Action class to log off system
	 * 
	 * @author harkul TODO move this to another class
	 */
	public static class LogoffAction implements ActionListener {
		private MainFrame frame;

		public LogoffAction(MainFrame mainFrame) {
			this.frame = mainFrame;
		}

		public void actionPerformed(ActionEvent e) {
			int rvl = JOptionPane.showConfirmDialog(frame.getApplicationFrame(), resources
					.getGuiString("studio.logoff.text1"), resources.getGuiString("studio.logoff.text2"),
					JOptionPane.YES_NO_OPTION);
			if (rvl == JOptionPane.YES_OPTION) {
				if (frame != null)
					frame.getApplicationFrame().dispose();
				frame = null;
				try {
					Thread.sleep(500);
				} catch (InterruptedException ex) {
				}
				MIPSSStudio.main(new String[] { "restart" });
			}
		}
	}
	
	
	public static class ChangePasswordAction implements ActionListener {
		private MainFrame frame;
		private final IMipssStudioLoader mipssLoader;

		public ChangePasswordAction(MainFrame mainFrame, IMipssStudioLoader mipssLoader) {
			this.frame = mainFrame;
			this.mipssLoader = mipssLoader;
		}

		public void actionPerformed(ActionEvent e) {
			new ChangePasswordDialog(mipssLoader.getLoggedOnUser(false).getSignatur());
		}
	}
	
	
	/**
	 * Quick'n dirty...
	 * @author harkul
	 *
	 */
	private class ShowLogAction implements ActionListener{
		private final JFrame owner;
		private JTextArea textArea;
		public ShowLogAction(JFrame owner){
			this.owner = owner;
			
		}

		private void updateText(final String text){
			SwingUtilities.invokeLater(new Runnable(){
				@Override
				public void run() {
					textArea.append(text);
					textArea.setCaretPosition(textArea.getText().length()-1);
				}
			});
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			JDialog dialog = new JDialog(owner, "Mipss loggviser 2011");
			textArea = new JTextArea();
			JScrollPane scr = new JScrollPane(textArea);
			dialog.add(scr);
			OutputStream out = new OutputStream(){
				public void write(int i) throws IOException{
					updateText(String.valueOf((char)i));
				}
				public void write(byte[] i, int off, int len)throws IOException{
					updateText(new String(i, off, len));
				}
				public void write(byte[] b) throws IOException{
					write(b, 0, b.length);
				}
			};
			final PrintStream oldOut = System.out;
			final PrintStream oldErr = System.err;
			System.setOut(new PrintStream(out));
			System.setErr(new PrintStream(out));
			dialog.addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent e) {
					System.setOut(oldOut);
					System.setErr(oldErr);
					System.out.println("stdout, stderr set back to defaults..");
				}
			});
			JButton copy = new JButton(new AbstractAction("Kopier markert tekst"){
				@Override
				public void actionPerformed(ActionEvent e) {
					textArea.getTransferHandler().exportToClipboard(textArea, Toolkit.getDefaultToolkit().getSystemClipboard(), TransferHandler.COPY);
				}
			});
			JButton clear = new JButton(new AbstractAction("Fjern tekst"){
				@Override
				public void actionPerformed(ActionEvent e) {
					textArea.setText("");
				}
			});
			JPanel pnlBtn = new JPanel();
			pnlBtn.add(copy);
			pnlBtn.add(clear);
			dialog.setLayout(new BorderLayout());
			dialog.add(scr);
			dialog.add(pnlBtn, BorderLayout.SOUTH);
			dialog.setSize(300, 300);
			dialog.setLocationRelativeTo(owner);
			dialog.setVisible(true);
			System.out.println("Lytter på logg...");
		}
	}

	private class Message {
		boolean cancelled;
		long lifeTime;
		String message;
		boolean permanent;
		int priority;
		@SuppressWarnings("unused")
		public Message(String message, boolean permanent) {
			this.message = message;
			this.permanent = permanent;
		}

		@SuppressWarnings("unused")
		public Message(String message, int priority, long lifeTime) {
			this.message = message;
			this.priority = priority;
			this.lifeTime = lifeTime;
			this.permanent = false;
		}

		public void animate() {
			SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
				
				
				/** {@inheritDoc} */
				@Override
				protected Void doInBackground() throws Exception {
					long tl = lifeTime + 1000;
					float start = (float) 250 / tl;
					float middle = (float) lifeTime / tl;

					Color bg = new Color(0f,0f,0f,0f);
					KeyValues<Color> keyValues = KeyValues
							.create(Color.BLACK, Color.BLUE, Color.BLACK, Color.BLACK, bg);
					KeyTimes times = new KeyTimes(0f, start, (start * 2), (start * 2) + middle, 1f);
					KeyFrames frames = new KeyFrames(keyValues, times);
					Animator animator = PropertySetter.createAnimator((int) tl, statusLabel, "foreground", frames);
					statusLabel.setForeground(Color.BLACK);
					statusLabel.setText(message);
					animator.start();

					boolean stop = false;
					do {
						if (cancelled) {
							animator.cancel();
							stop = true;
						}
						Thread.sleep(2);
					} while (animator.isRunning() || stop);

					return null;
				}

				/** {@inheritDoc} */
				@Override
				protected void done() {
					processMessages();
					
					if(Message.this == currentMessage) {
						currentMessage = null;
					}
				}
			};
			worker.execute();
		}

		public void display() {
			statusLabel.setForeground(Color.BLACK);
			statusLabel.setText(message);
		}

		public boolean isMoreImportantOrEqual(Message message) {
			if (message == null) {
				return true;
			} else {
				return priority >= message.priority;
			}
		}

		public void show() {
			if (permanent) {
				display();
			} else {
				animate();
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void activateMenuItem(MenynodeDTO menyNode) {
		TreePath path = ((MenyTreeModel) menuTree.getModel()).getTreePath(menyNode);
		menuTree.setSelectionPath(path);
	}

	@Override
	public Driftkontrakt getSelectedDriftkontrakt() {
		return contractPicker.getModel().getSelectedItem();
	}
}
