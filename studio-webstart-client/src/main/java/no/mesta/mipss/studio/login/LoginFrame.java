package no.mesta.mipss.studio.login;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.KonfigparamPreferences;

@SuppressWarnings("serial")
public class LoginFrame extends JDialog implements CallbackHandler {
	private JLabel usrLabel;
	private JLabel pwdLabel;
	private JTextField usrField;
	private JPasswordField pwdField;
	private LoginBackground panel;
	
	private JLabel forgotPasswordLabel;
	
	private int type = 0;
		
	private static final int USRLBL_X = 90;
	private static final int USRLBL_Y = 95;
	private static final int USRFLD_X = 90;
	private static final int USRFLD_Y = 115;
	
	private static final int PWDLBL_X = 90;
	private static final int PWDLBL_Y = 135;
	private static final int PWDFLD_X = 90;
	private static final int PWDFLD_Y = 155;
	
	private static final int FORLBL_X = 250;
	private static final int FORLBL_Y = 177;
	
	private static final int DEFAULT_WIDTH = 235;
	private static final int DEFAULT_HEIGHT = 20;
	private Image desktop = null;
	
	int mouseX = -1;
	int mouseY = -1;
	
	private static PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");
	
	/**
	 * Creates a new instance of LoginFrame defaults to a modal JDialog
	 */
	public LoginFrame(){
		init();
		initUI();
		setModal(true);
	}
	/**
	 * Creates a new instance of LoginFrame
	 * @param modal true if this instance should be modal
	 */
	public LoginFrame(boolean modal){
		init();
		initUI();
		setModal(modal);
		setVisible(true);
	}
	/**
	 * Initialize the JDialog
	 */
	private void init(){
		setUndecorated(true);
		setSize(420,300);
		setLocationRelativeTo(null);
		
		addMouseMotionListener(new MouseMotionAdapter(){
			public void mouseDragged(MouseEvent e){				
				Point pos = LoginFrame.this.getLocationOnScreen();
	            if (mouseX!=-1&&mouseY!=-1)
	            	LoginFrame.this.setLocation(mouseX, mouseY);
				mouseX = pos.x+e.getX();
	            mouseY = pos.y+e.getY();
				repaint();
			}
		});
	}
	/**
	 * Returns the type of login
	 * 1 = Get the user from runtime
	 * 0 = Use specified user name and password
	 * @return
	 */
	public int getLoginType(){
		return type;
		
	}
	
	/**
	 * Create a screenshot of the desktop
	 */
	private void updateBackground(){
		Robot rbt=null;
		try {
			rbt = new Robot();
		} catch (AWTException e1) {
			e1.printStackTrace();
		}
		
	    Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        desktop = rbt.createScreenCapture(new Rectangle(0,0,(int)dim.getWidth(), (int)dim.getHeight()));
	}
	/**
	 * Initialize ui components
	 */
	private void initUI(){
		JPanel loginPanel = new JPanel();
		loginPanel.setLayout(null);
		loginPanel.setOpaque(false);
		
		usrLabel = new JLabel("Brukernavn:");
		usrLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		usrLabel.setForeground(Color.WHITE);
		usrLabel.setBounds(USRLBL_X, USRLBL_Y, 80, DEFAULT_HEIGHT);

		pwdLabel = new JLabel("Passord:");
		pwdLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		pwdLabel.setForeground(Color.WHITE);
		pwdLabel.setBounds(PWDLBL_X, PWDLBL_Y, 80, DEFAULT_HEIGHT);
		
		usrField = new JTextField();
		usrField.setBounds(USRFLD_X, USRFLD_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
		
		pwdField = new JPasswordField();
		pwdField.setBounds(PWDFLD_X, PWDFLD_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
		pwdField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (validateFields())
					dispose();
			}
		});
		
		forgotPasswordLabel = new JLabel(resources.getGuiString("studio.forgotPassword"));
		forgotPasswordLabel.setBounds(FORLBL_X, FORLBL_Y, 80, DEFAULT_HEIGHT);
		forgotPasswordLabel.setForeground(Color.WHITE);
		forgotPasswordLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				JOptionPane.showMessageDialog(LoginFrame.this, 
						resources.getGuiString("studio.forgotPasswordMessage", KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "supportEpost").getVerdi()), 
						resources.getGuiString("studio.forgotPassword"), 
						JOptionPane.INFORMATION_MESSAGE);				
			}
		});
	    
		loginPanel.add(usrLabel);
		loginPanel.add(pwdLabel);
		loginPanel.add(usrField);
		loginPanel.add(pwdField);
		loginPanel.add(forgotPasswordLabel);
		
		JButton okBtn = new JButton("Ok");
		okBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (validateFields())
					dispose();
			}
		});
		
		JButton cancelBtn = new JButton("Avbryt");
		cancelBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		});
		
		JButton autoBtn = new JButton("Citrixbruker");
		autoBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				type = 1;
				dispose();
			}
		});

		okBtn.setBounds(USRFLD_X+DEFAULT_WIDTH-135, PWDFLD_Y+50, 60, 20);
		cancelBtn.setBounds(USRFLD_X+DEFAULT_WIDTH-70, PWDFLD_Y+50, 70, 20);
		autoBtn.setBounds(USRFLD_X+DEFAULT_WIDTH-135, PWDFLD_Y+80, 135, 20);
		
		loginPanel.add(okBtn);
		loginPanel.add(cancelBtn);
		loginPanel.add(autoBtn);
		JLabel label = new JLabel("MIPSS v.A");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setForeground(Color.WHITE);
		label.setBounds(290, -8, 300, 40);
		loginPanel.add(label);
		
		panel = new LoginBackground();
		panel.add(loginPanel);
		
		getContentPane().add(panel);
		new Thread(panel).start();
	}
	/**
	 * Validate that the user has entered something in the username and password field
	 * @return true if both usrField and pwdField has values
	 */
	private boolean validateFields(){
		return usrField.getText().length()>0&&pwdField.getPassword().length>0;
	}
	/**
	 * get the username
	 * @return username
	 */
	public String getUsername(){
		return usrField.getText();
	}
	
	/**
	 * Class that creates the background for the login frame
	 * @author harkul
	 *
	 */
	class LoginBackground extends JPanel implements Runnable{
//		java.net.URL imgURL = getClass().getResource("/images/prodstyrsplash.png");
		private Image background = new ImageIcon(getClass().getResource("/images/prodstyrsplash.png")).getImage();
		
		public LoginBackground(){

		    setLayout(new BorderLayout());
		    setOpaque(true);
		    updateBackground();
		}
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			Point pos = LoginFrame.this.getLocationOnScreen();
            g.drawImage(desktop, -pos.x,-pos.y,this);
			g.drawImage(background, 0, 0, this);
		}
		public void run(){
			int i = 0;
			loop:
			while (true){
				try{
					Thread.sleep(5000);
					for (i=27;i<170;i++){
						Graphics g = this.getGraphics();
						if(g!=null){
							g.setColor(Color.white);
							g.fillRect(i, 78, 3, 3);
							g.fillRect(408+27-i, 190, 3,3);
							
							g.fillRect(66, i, 3, 3);
							g.fillRect(342, 230+27-i, 3, 3);
							
							repaint();
							try{
								Thread.sleep(1);
							}catch (InterruptedException e){
								e.printStackTrace();
							}
						}
						else{
							break loop;
						}
					}
				} catch (InterruptedException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		
		for (int i=0;i<callbacks.length;i++){
			if (callbacks[i] instanceof NameCallback) {
		 		NameCallback nc = (NameCallback) callbacks[i];
		 		nc.setName(usrField.getText());
			}
			else if (callbacks[i] instanceof PasswordCallback) {
		 		PasswordCallback pc = (PasswordCallback) callbacks[i];
		 		pc.setPassword(pwdField.getPassword());
			}
		}
		
	}
}