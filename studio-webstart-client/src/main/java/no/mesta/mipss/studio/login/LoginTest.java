package no.mesta.mipss.studio.login;
/**
 * -Djava.security.auth.login.config=C:\code\mipss-studio\studio-webstart-client\src\main\java\no\mesta\mipss\studio\login\jaas.conf 
 * -Duser.krb5ccname=$KRB5CCNAME  
 * -Djavax.security.auth.useSubjectCredsOnly=false 
 * -Dsun.security.krb5.debug=true
 * 
 * Expand “Local Computer Policy” > “Computer Configuration” > “Windows Settings” > “Security Settings” > “Local Policies” > “Security Options” > “Network security: Configure encryption types allowed for Kerberos”
Double click “Network security: Configure encryption types allowed for Kerberos”
Select “DES-CBC-MD5" & "DES-CBC-CRC”
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import com.sun.security.auth.callback.TextCallbackHandler;

public class LoginTest {
	public static void main(String[] args) throws LoginException {
		String domainName = "MESTA.LOCAL";
//		System.setProperty("java.security.krb5.realm", domainName);
//        System.setProperty("java.security.krb5.kdc", "mydc." + domainName);
        
		// let the security subsystem know the location of the login.conf file
        System.setProperty("java.security.auth.login.config", createJaasConf().getAbsolutePath());
        
		LoginContext lc = new LoginContext("SampleClient", new TextCallbackHandler());
		lc.login();
		Subject subject = lc.getSubject();
		System.out.println(subject);
	}
	
	private static File createJaasConf(){
		String tmpFileName = System.getProperty("java.io.tmpdir") + "loginConf1";
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(tmpFileName));
			out.write("SampleClient {");
	        out.write("com.sun.security.auth.module.Krb5LoginModule required useTicketCache=true doNotPrompt=true;");
	        out.write("};");
	        out.close();
        } catch (IOException e) {
			e.printStackTrace();
		}
        return new File(tmpFileName);
	}
}
 
//
//public class LoginTest {
// 
//    public static void main(String[] args) {
//        try {
//            String strDomainName = "MESTA.LOCAL";
//            String currentLoggedUserDomainName = getSignedOnUserSubject(strDomainName);
//            System.out.println("currentLoggedUserDomainName: " + currentLoggedUserDomainName);
// 
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
// 
//    public static String getSignedOnUserSubject(String domainName) throws Exception {
// 
//        File tmpFileForLoginConf = null;
//        try {
//            // create tmp file in which is the config to 'Krb5LoginModule'
//            String tmpFileName = System.getProperty("java.io.tmpdir") + "loginConf1";
//            BufferedWriter out = new BufferedWriter(new FileWriter(tmpFileName));
//            out.write("SignedOnUserLoginContext {");
//            out.newLine();
//            out.write("com.sun.security.auth.module.Krb5LoginModule required useTicketCache=true doNotPrompt=true;");
//            out.newLine();
//            out.write("};");
//            out.newLine();
//            out.close();
//            tmpFileForLoginConf = new File(tmpFileName);
// 
//            // get the Subject that represents the signed-on user
//            // this method throws LoginException: Unable to obtain Princpal Name for authentication on JRE7
//            Subject signedOnUserSubject = getSubject(domainName, tmpFileForLoginConf, "SignedOnUserLoginContext");
//            System.out.println(signedOnUserSubject);
//            // rest of code which is unnecessary on JRE7
//            // it checks if domainName from credentials is the same as in param 'domainName'
//            String ret = null;
//            Set<Object> set = signedOnUserSubject.getPrivateCredentials();
//            if (set != null && set.size() > 0) {
//                for (Object obj : set) {
//                    if (obj instanceof KerberosTicket) {
//                        KerberosTicket kt = (KerberosTicket) obj;
// 
//                        if (kt.getServer().getRealm().equalsIgnoreCase(domainName)) {
//                            // if domainName == kt.getServer().getRealm() that mean that the user is currently logged to it.
//                            ret = kt.getClient().getName();
//                            break;
//                        }
//                    }
//                }
//            }
// 
//            return ret;
//        } catch (javax.security.auth.login.LoginException e1) {
//            if (e1.getMessage() != null && e1.getMessage().equals("Unable to obtain Princpal Name for authentication ")) {
//                e1.printStackTrace();
//                return null;
//            }
//            throw e1;
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                if (tmpFileForLoginConf != null) {
//                    tmpFileForLoginConf.delete();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
// 
//    /**
//     * Gets javax.security.auth.Subject
//     */
//    public static Subject getSubject(String domainName, File fileLoginConf, String loginContex) throws LoginException {
// 
//        System.setProperty("java.security.krb5.realm", domainName);
//        System.setProperty("java.security.krb5.kdc", "mydc." + domainName);
// 
//        // let the security subsystem know the location of the login.conf file
//        System.setProperty("java.security.auth.login.config", fileLoginConf.getAbsolutePath());
// 
//        // create a LoginContext based on the entry in the login.conf file
//        LoginContext lc = new LoginContext(loginContex);
// 
//        // login (effectively populating the Subject)
//        lc.login();
// 
//        // get the Subject that represents the signed-on user
//        Subject signedOnUserSubject = lc.getSubject();
//        return signedOnUserSubject;
//    }
//}