package no.mesta.mipss.studio.core;

import java.io.File;

/**
 * Inneholder noen konstanter som benyttes i rammeverket
 */
public interface MipssConstants {
        /** rammeverkets home-mappe */
	public static final String APP_HOME = "Mipss";
        /** den innloggede brukers home-mappe f.eks d:/documents and settings/user/ */
	public static final String USER_HOME = System.getProperty("user.home")+File.separator+APP_HOME;
        /** rammeverkets applikasjon mappe */
	public static final String USER_HOME_APP = USER_HOME+File.separator+"app";
        /** rammeverkets bibliotek mappe */
	public static final String USER_HOME_LIB = USER_HOME+File.separator+"lib";
        /** filen som inneholder informasjon om hvilke applikasjoner/biblioteker og versjoner som er cachet lokalt */
        public static final String USER_APP_CACHE = USER_HOME+File.separator+"app.cache";
	
        /** Default realm */
        public static final String DEFAULT_REALM = "VEGPROD.NO";
        
        public static final String CONTROLLER_CLASS = "no.mesta.mipss.studio.swing.MipssController";
        public static final String CONTROLLER_CLASS_IMPL = "no.mesta.mipss.studio.swing.MipssControllerImpl";
}
