/**
 * 
 */
package no.mesta.mipss.studio.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.common.ResourceFetcher;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Ordner opp i properties, slik at de er på plass når studio starter
 * </p>
 * 
 * <p>
 * Leser verdiene fra mipss.properties.
 * </p>
 * 
 * <p>
 * Der finnes noen delegasjons metoder for mipss.properties:
 * </p>
 * 
 * <ul>
 * <li><b>!{app,nøkkel}:</b> Leser fra Konfigparam tabellen i databasen</li>
 * <li><b>${nøkkel}:</b> Leser fra en annen verdi i mipss.properties</li>
 * <li><b>&{nøkkel}:</b> Leser fra VM argumenter</li>
 * </ul>
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PropertySetup {
	private static final Logger logger = LoggerFactory.getLogger(PropertySetup.class);
	private final PropertyResourceBundleUtil util;

	/**
	 * Åpner også propertyfilen "mipss.properties"
	 * 
	 */
	public PropertySetup() {
		logger.debug("PropertySetup() constructed");
		util = PropertyResourceBundleUtil.getPropertyBundle("mipss");
	}

	/**
	 * Kopierer en ressurs til en fillokasjon
	 * 
	 * @param resource
	 * @param fileDest
	 */
	private void copyFile(String resource, String fileDest) {
		System.out.println(resource+" "+fileDest);
		logger.debug("copyFile(" + resource + "," + fileDest + ") start");
				
		if (needToCopy(fileDest)) {
			InputStream in = getInputStream(resource);
			logger.debug("in is: " + (in == null ? "null" : "not null"));

			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(fileDest);
			} catch (FileNotFoundException e) {
				throw new IllegalStateException(e);
			}

			try {
				IOUtils.copy(in, fos);
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}
		logger.debug("resource handled " + resource);
	}

	/**
	 * Bearbeider alle "files[n]" i mipss.properties
	 * 
	 */
	private void doFiles() {
		logger.debug("doFiles() start");

		String[] files = util.getStringArray("files[]");
		
		for (String file : files) {
			
			String src = getProperty(file + "-src");
			String dest = getProperty(file + "-dest");
			copyFile(src, dest);
		}
	}

	/**
	 * Bearbeider alle "properties[n]" i mipss.properties
	 * 
	 */
	private void doProperties() {
		logger.debug("doProperties() start");
		String[] properties = util.getStringArray("property[]");
		for (String property : properties) {
			String value = getProperty(property);
			logger.debug("doProperties() " + property + " = " + value);
			System.setProperty(property, value);
		}
	}

	/**
	 * Bearbeider alle filer og properties
	 * 
	 */
	public void execute() {
		logger.debug("execute() start");
		doFiles();
		doProperties();
	}

	/**
	 * Sjekker om en fil finnes
	 * 
	 * @param fileDest
	 * @return
	 */
	private boolean fileExists(String fileDest) {
		File file = new File(fileDest);
		return file.exists();
	}

	/**
	 * Henter en inputstream fra classpath
	 * 
	 * @param resource
	 * @return
	 */
	private InputStream getInputStream(String resource) {
		return ResourceFetcher.getResourceAsStream(resource);
	}

	/**
	 * Henter en property fra mipss.properties, hvis den er anmerket med
	 * !{app,nøkkel} hentes den i konfigparam
	 * 
	 * @param property
	 * @return
	 */
	private String getProperty(String property) {
		logger.debug("getProperty(" + property + ") start");
		String value = util.getProperty(property);

		if (StringUtils.startsWith(value, "!{")) {
			String app = value.substring(2, value.indexOf(","));
			String name = value.substring(value.indexOf(",") + 1, value.indexOf("}"));

			logger.debug("App and name: <" + app + "> <" + name + ">");
			Konfigparam config = KonfigparamPreferences.getInstance().hentEnForApp(app, name);
			value = config.getVerdi();
			
			if (app.equals("mipss.studio.login") && name.equals("krb5.conf.dest") ||
					app.equals("mipss.webstart") && name.equals("jaas.conf.dest")) {
				Boolean useHomeFolder = Boolean.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "useHomeFolder").getVerdi());

				if (useHomeFolder) {
					value = System.getProperty("user.home") + "\\" + value;
				}
			}
		}
		logger.debug(property + " is " + value);
		return value;
	}

	/**
	 * Sjekker om det er nødvendig å oppfriske filen
	 * 
	 * @param fileDest
	 * @return
	 */
	private boolean needToCopy(String fileDest) {
		logger.debug("needToCopy(" + fileDest + ")");
		boolean needsToCopy = false;

		if (fileExists(fileDest)) {
			int minutes = Integer.valueOf(getProperty("fileAgeMinutes"));
			Date date = CalendarUtil.getMinutesAgo(minutes);
			File file = new File(fileDest);
			if (FileUtils.isFileOlder(file, date)) {
				FileUtils.deleteQuietly(file);
				needsToCopy = true;
			} else {
				needsToCopy = false;
			}
		} else {
			needsToCopy = true;
		}

		logger.debug("needsToCopy = " + needsToCopy + " for " + fileDest);
		return needsToCopy;
	}

	/**
	 * Lokal testmetode
	 * 
	 * @param s
	 */
	public static void main(String[] s) {
		PropertySetup props = new PropertySetup();
		props.execute();
	}
}
