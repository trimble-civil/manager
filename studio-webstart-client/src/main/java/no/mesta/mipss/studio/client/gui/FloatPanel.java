package no.mesta.mipss.studio.client.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.ArrowButton;
import no.mesta.mipss.ui.CrossButton;
import no.mesta.mipss.ui.SquareButton;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.plaf.TitledPanelUI;

/**
 * Vindu for plugins.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class FloatPanel extends JXTitledPanel{
	private final IMipssStudioLoader loader;
	private final JFrame owner;
	private MipssPlugin plugin;
	private JComponent container;
	private JPanel panel;
	private JPanel oldPanel;

	/**
	 * Konstruktør
	 * 
	 * @param panel
	 * @param title
	 * @param owner
	 * @param loader
	 */
	public FloatPanel(JPanel panel, String title, JFrame owner, IMipssStudioLoader loader) {
		super(title);
		this.owner = owner;
		this.loader = loader;
		if (panel != null) {
			add(panel);
		}
	}

	/**
	 * Konstruktør
	 * 
	 * @param panel
	 * @param title
	 * @param owner
	 * @param loader
	 */
	public FloatPanel(JPanel panel, String title, JFrame owner, IMipssStudioLoader loader, Component component) {
		super(title);

		this.panel = panel;
		this.owner = owner;
		this.loader = loader;
		this.container = (JComponent)component;
		if (panel != null) {
			add(panel);
		}
	}
	
	public void setPanel(JPanel panel){
		this.oldPanel = this.panel;
		this.panel = panel;
	}
	/**
	 * Lager en ny instans av FloatPanel
	 * 
	 * @param plugin
	 * @param title
	 *            tittelen på panelet
	 * @param owner
	 *            eieren til panelet, denne blir satt som parent til dialoger
	 *            som blir opprettet av FloatPanel
	 * @param loader
	 */
	public FloatPanel(MipssPlugin plugin, String title, JFrame owner, IMipssStudioLoader loader) {
		super(title);
		this.loader = loader;
		this.plugin = plugin;
		this.owner = owner;
		
		if (plugin != null) {
			this.panel = (JPanel)plugin.getModuleGUI();
			if(plugin.isScrollable()) {
				this.panel = (JPanel)plugin.getModuleGUI();
				Dimension max = new Dimension(939,649);
				Dimension psize = this.panel.getPreferredSize();
				if (this.panel.getPreferredSize().height>max.height){
					psize.height=max.height;
				}
				if (this.panel.getPreferredSize().width>max.width){
					psize.width=max.width;
				}
				this.panel.setPreferredSize(psize);
				JScrollPane scr = new JScrollPane(this.panel);				
				scr.setBorder(null);
				add(scr);
			} else {
				add((JPanel)plugin.getModuleGUI());
			}
		}

		setName(plugin.getModul().getNavn());
		initUI();
	}
	
	public MipssPlugin getPlugin(){
		return plugin;
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
//		this.plugin = null;
		//TODO dette fører til en nullpointer når man forsøker å løse ut og lukke vinduet....
	}
	/**
	 * deleger
	 * 
	 * @return
	 */
	public TitledPanelUI getUI() {
		return super.getUI();
	}

	/**
	 * Initialiser komponentene som brukes av FloatPanel
	 */
	private void initUI() {
		Dimension d = new Dimension(17, 17);
		SquareButton closeButton = new CrossButton(d, "Lukk panel");
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (plugin != null) {
					loader.closePlugin(plugin);
				}
			}
		});

		ArrowButton floatButton = new ArrowButton(d, "Løs ut panel");

		floatButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FloatPanel panel = FloatPanel.this;
				if (plugin != null) {
					Container d = panel.getParent();
					
					String title = panel.getTitle() != null ? panel.getTitle() : plugin.getModul().getNavn();

					final JDialog newDialog = new JDialog(owner, title);
					newDialog.add((JPanel) plugin.getModuleGUI());

					Point p1 = panel.getLocation();
					Point p2 = d.getLocation();
					Point p3 = new Point(p2.x + 20, p1.y + p2.y + 20);
					newDialog.setLocation(p3);
					newDialog.setSize(panel.getSize());
					newDialog.setLocationRelativeTo(null);
					newDialog.setVisible(true);
					loader.popOut(plugin);
					newDialog.setVisible(true);

					newDialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
					newDialog.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							boolean shutdown = loader.closePlugin(plugin);
							if (shutdown) {
								newDialog.dispose();
							}
						}
					});
				}
			}
		});
		
		JPanel buttonPnl = new JPanel(new FlowLayout());
		buttonPnl.setOpaque(false);
		buttonPnl.add(floatButton);
		buttonPnl.add(closeButton);

		getUI().setRightDecoration(buttonPnl);
	}

	/**
	 * Setter tittelen på vinduet.
	 * 
	 * @param title
	 *            tittelen
	 */
	public void setTitle(String title) {
		super.setTitle(title);
	}
}
