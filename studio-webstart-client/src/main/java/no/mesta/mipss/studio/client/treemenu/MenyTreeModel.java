package no.mesta.mipss.studio.client.treemenu;

import java.util.Iterator;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TreeModel for menytreet. Denne modellen bygger opp nodene rekursivt basert på
 * en liste med MenynodeDTO objekter MenynodeDTO.tresti bestemmer hvor i treet
 * noden skal befinne seg.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class MenyTreeModel extends DefaultTreeModel {
	private static final Logger log = LoggerFactory.getLogger(MenyTreeModel.class);
	private static final String TOKEN = ";"; // TODO should be specified in the
	// database if the tree path is
	// stored there */
	private boolean hasFjernValgt = false;
	private JTree tree;

	/**
	 * Creates a new instance of MenuTreeModel
	 * 
	 * @param menuList
	 *            an array that represents the values to create the tree from
	 */
	public MenyTreeModel(List<MenynodeDTO> menuList, JTree tree) {
		super(new DefaultMutableTreeNode("root"));
		this.tree = tree;

		Iterator<MenynodeDTO> it = menuList.iterator();
		while (it.hasNext()) {
			MenynodeDTO menu = it.next();
			DefaultMutableTreeNode root = ((DefaultMutableTreeNode) getRoot());
			DefaultMutableTreeNode groupRoot = findGruppe(root, menu);
			if (groupRoot == null) {
				groupRoot = createGroupRoot(root, menu);
			}
			DefaultMutableTreeNode subRoot = findNode(groupRoot, menu.getTresti().split(TOKEN), menu.toString());
			// get the level for the subRoot, this level will represent the
			// start index
			// in the treePath array defined in menu
			int level = subRoot.getLevel();
			createTreeNode(subRoot, menu, level);
		}
	}

	/**
	 * Lager en ny gruppe
	 * 
	 * @param root
	 * @param menynode
	 */
	private DefaultMutableTreeNode createGroupRoot(DefaultMutableTreeNode root, MenynodeDTO menynode) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(menynode.getGruppe());
		root.add(node);
		return node;
	}

	/**
	 * Recursively create a new tree node from root to the specified path. The
	 * level to start creating nodes from the MeduDTO's tresti is specified in
	 * the index counter. The value of index that is larger than zero represents
	 * the depth in the tree.
	 * 
	 * @param root
	 *            the root for this path
	 * @param menuItem
	 * @param index
	 *            counter variable used to determine which value in the
	 *            MenuDTO's tresti array to create legal values go from 0 to
	 *            menuItem.tresti.length;
	 */
	private void createTreeNode(DefaultMutableTreeNode root, MenynodeDTO menuItem, int index) {
		// log.debug(menuItem.getTresti().equals(menuItem.getVisningsnavn()));

		if (menuItem.getTresti().trim().equals(menuItem.getVisningsnavn().trim()) && index == 0) {
			root.add(new DefaultMutableTreeNode(menuItem));
			return;
		}
		// log.debug(menuItem.getTresti().equals(menuItem.getVisningsnavn()));
		String[] treePath = menuItem.getTresti().split(TOKEN);
		// add menu to current node
		if (index == treePath.length) {
			root.add(new DefaultMutableTreeNode(menuItem));
			return;
		}
		// this means that the current root is the actual destination for the
		// menuItem, since
		// the index has surpassed the length of the treePath
		else if (index == treePath.length + 1) {
			root.setUserObject(menuItem);
		} else {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(treePath[index++]);
			root.add(node);
			// recurse with the current node as root
			createTreeNode(node, menuItem, index);
		}
	}

	private DefaultMutableTreeNode findChild(DefaultMutableTreeNode treeNode, MenynodeDTO node) {
		if (treeNode == null) {
			return null;
		} else {
			Object userObject = treeNode.getUserObject();
			if (node.equals(userObject)) {
				return treeNode;
			} else {
				return findChild(treeNode.getNextNode(), node);
			}

		}
	}

	/**
	 * Søk etter en gruppe i treet
	 * 
	 * @param root
	 * @param menyNode
	 * @return
	 */
	private DefaultMutableTreeNode findGruppe(DefaultMutableTreeNode root, MenynodeDTO menyNode) {
		int c = root.getChildCount();
		for (int i = 0; i < c; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) root.getChildAt(i);
			if (node.getUserObject() instanceof Menygruppe) {
				Menygruppe n = (Menygruppe) node.getUserObject();
				log.trace(n.getNavn() + " " + menyNode.getGruppe().getNavn());
				if (n.getNavn().equals(menyNode.getGruppe().getNavn()))
					return node;
			}
		}
		return null;
	}

	/**
	 * Finner et MenynodeDTO objekt som har valgt tilgangspunkt
	 * 
	 * @param root
	 *            noden som skal undersøkes
	 * @param tilgangspunkt
	 *            klassenavnet på tilgangspunktet
	 */
	private MenynodeDTO findNode(DefaultMutableTreeNode root, String tilgangspunkt) {
		int c = root.getChildCount();
		if (root.getUserObject() instanceof MenynodeDTO) {
			MenynodeDTO dto = (MenynodeDTO) root.getUserObject();
			if (dto.getKlassenavn().equals(tilgangspunkt)) {
				return dto;
			}
		}
		for (int i = 0; i < c; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) root.getChildAt(i);
			MenynodeDTO dto = findNode(node, tilgangspunkt);
			if (dto != null) {
				TreePath path = new TreePath(node.getPath());
				tree.expandPath(path);
				setHasFjernValgt(true);
				return dto;
			}
		}
		return null;
	}

	/**
	 * Recursively find a node in the tree. The node returned can be any node,
	 * counting from the root and to the end of the tresti array. The user
	 * object in the MutableTreeNodes will determine if the node is on the path
	 * specified in tresti
	 * 
	 * @param root
	 *            the current node to search for the path
	 * @param treePath
	 *            the path to search for
	 * @return the node that matches the tresti and/or userObject, tree root if
	 *         no match is found
	 */
	private DefaultMutableTreeNode findNode(DefaultMutableTreeNode root, String[] treePath, Object userObject) {
		int c = root.getChildCount();
		// EOL for treePath, return the node that matched so far.
		if (root.getPath().length - 1 == treePath.length) {
			// check if the userObject matches any of the children to the last
			// parent node in the path
			for (int i = 0; i < c; i++) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) root.getChildAt(i);
				if (node.getUserObject().equals(userObject)) {
					return node;
				}
			}
			// no match on child nodes, return current root.
			return root;
		}
		// check all child nodes and recurse on child node if match is found
		for (int i = 0; i < c; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) root.getChildAt(i);
			if (node.getUserObject().toString().equals(treePath[node.getPath().length - 2])) {
				// recurse with the node as root.
				return findNode(node, treePath, userObject);
			}
		}
		return root;
	}

	/**
	 * Finner et MenynodeDTO objekt som har valgt tilgangspunkt
	 * 
	 * @param tilgangspunkt
	 *            klassenavnet på tilgangspunktet
	 */
	public MenynodeDTO findNode(String tilgangspunkt) {
		return findNode((DefaultMutableTreeNode) getRoot(), tilgangspunkt);
	}

	/**
	 * Gir "addressen" til et gitt MenynodeDTO
	 * 
	 * @param node
	 * @return
	 */
	public TreePath getTreePath(MenynodeDTO node) {
		DefaultMutableTreeNode treeNode = findNode(findGruppe((DefaultMutableTreeNode) getRoot(), node), node
				.getTresti().split(TOKEN), node.toString());
		if (treeNode != null) {
			DefaultMutableTreeNode child = findChild(treeNode, node);

			TreePath path = new TreePath(child.getPath());
			return path;
		} else {
			return null;
		}
	}

	/**
	 * returnerer true dersom en node i treet er markert
	 */
	public boolean isHasFjernValgt() {
		return hasFjernValgt;
	}

	/**
	 * Fjerner markeringen til noden
	 */
	public void resetFjernValgt() {
		resetFjernValgt(((DefaultMutableTreeNode) getRoot()));
		setHasFjernValgt(false);
	}

	/**
	 * Fjerner markeringen til noden
	 */
	private void resetFjernValgt(DefaultMutableTreeNode root) {
		int c = root.getChildCount();
		if (root.getUserObject() instanceof MenynodeDTO) {
			((MenynodeDTO) root.getUserObject()).setFjernValgt(false);
		}
		for (int i = 0; i < c; i++) {
			resetFjernValgt((DefaultMutableTreeNode) root.getChildAt(i));
		}
	}

	/**
	 * Setter hasFjernValgt, true dersom en node i treet er markert
	 */
	public void setHasFjernValgt(boolean hasFjernValgt) {
		this.hasFjernValgt = hasFjernValgt;
	}
}
