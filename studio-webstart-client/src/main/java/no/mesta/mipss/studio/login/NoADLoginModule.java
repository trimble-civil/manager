package no.mesta.mipss.studio.login;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.omg.PortableInterceptor.SUCCESSFUL;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.HashingUtils;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

public class NoADLoginModule implements LoginModule{

	private Subject subject;
	private CallbackHandler callbackHandler;
	private Map sharedState;
	private Map options;
	private boolean succeeded = false;

	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
				this.subject = subject;
				this.callbackHandler = callbackHandler;
				this.sharedState = sharedState;
				this.options = options;		
				
				succeeded = false;
	}

	@Override
	public boolean login() throws LoginException {
		System.out.println("Login Module - login called");
		if (callbackHandler == null) {
			throw new LoginException("Oops, callbackHandler is null");
		}

		Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("name:");
		callbacks[1] = new PasswordCallback("password:", false);

		try {
			callbackHandler.handle(callbacks);
		} catch (IOException e) {
			throw new LoginException("Oops, IOException calling handle on callbackHandler");
		} catch (UnsupportedCallbackException e) {
			throw new LoginException("Oops, UnsupportedCallbackException calling handle on callbackHandler");
		}

		NameCallback nameCallback = (NameCallback) callbacks[0];
		PasswordCallback passwordCallback = (PasswordCallback) callbacks[1];

		String name = nameCallback.getName();
		String password = new String(passwordCallback.getPassword());
		
		
		
		AccessControl accessControl = BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class);
		try{
			Bruker authBruker = accessControl.authBruker(name, new HashingUtils().toHash(password));
			if (authBruker!=null){
				MestaPrincipal mestaPrincipal = new MestaPrincipal(name);
				Set<Principal> principals = subject.getPrincipals();
				principals.add(mestaPrincipal);
				succeeded = true;
				return succeeded;
			}
			throw new FailedLoginException("Sorry! No login for you.");
		} catch (LoginException e){
			succeeded=false;
			throw new FailedLoginException("Sorry! No login for you.");
		}
//		
//		if ("larnym@mesta.no".equals(name) && "passord".equals(password)) {
//			System.out.println("Success! You get to log in!");
//			succeeded = true;
//			return succeeded;
//		} else {
//			System.out.println("Failure! You don't get to log in");
//			succeeded = false;
//			throw new FailedLoginException("Sorry! No login for you.");
//		}
	}

	@Override
	public boolean commit() throws LoginException {
		System.out.println("NoADLoginModule Module - commit called");
		return succeeded;
	}

	@Override
	public boolean abort() throws LoginException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean logout() throws LoginException {
		// TODO Auto-generated method stub
		return false;
	}

}
