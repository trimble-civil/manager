package no.mesta.mipss;

import javassist.CannotCompileException;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public class BeansBindingHack {

	@SuppressWarnings("rawtypes")
	public static void doHack(){
		try {
            ClassPool cp = ClassPool.getDefault();
            cp.insertClassPath(new ClassClassPath(BeansBindingHack.class));
            CtClass cc = cp.get("org.jdesktop.beansbinding.ELProperty");
            CtMethod m = cc.getDeclaredMethod("getBeanInfo");
            m.setBody("{"+
                    //"assert $1 != null;" +
                    "try {" +
                    "return java.beans.Introspector.getBeanInfo($1.getClass());" +
                    "} catch (java.beans.IntrospectionException ie) {" +
                    "throw new org.jdesktop.beansbinding.PropertyResolutionException(\"Exception while introspecting \" + $1.getClass().getName(), ie);" +
                    "} }");
            cp.toClass(cc, BeansBindingHack.class.getClassLoader(), BeansBindingHack.class.getProtectionDomain());
            cc = cp.get("org.jdesktop.beansbinding.BeanProperty");
            m = cc.getDeclaredMethod("getBeanInfo");
            m.setBody("{"+
                    //"assert $1 != null;" +
                    "try {" +
//                    "System.out.println(\"getBeanInfo()\");"+
                    "return java.beans.Introspector.getBeanInfo($1.getClass());" +
                    "} catch (java.beans.IntrospectionException ie) {" +
                    "throw new org.jdesktop.beansbinding.PropertyResolutionException(\"Exception while introspecting \" + $1.getClass().getName(), ie);" +
                    "} }");
            cp.toClass(cc, BeansBindingHack.class.getClassLoader(), BeansBindingHack.class.getProtectionDomain());
        } catch (NotFoundException ex) {
            ex.printStackTrace();
        } catch (CannotCompileException ex) {
            ex.printStackTrace();
        }
	}
}
