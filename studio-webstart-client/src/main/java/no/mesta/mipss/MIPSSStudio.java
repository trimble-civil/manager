package no.mesta.mipss;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MainFrame;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.menunode.ApplikasjonLoader;
import no.mesta.mipss.persistence.applikasjon.Menypunkt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.studio.client.gui.MainFrameImpl;
import no.mesta.mipss.studio.core.MipssStudioLoaderImpl;
import no.mesta.mipss.studio.core.PropertySetup;
import no.mesta.mipss.studio.login.ChangePasswordDialog;
import no.mesta.mipss.studio.login.LoginModule;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.security.Principal;
import java.util.*;
import java.util.List;

/**
 * Mipss oppstartklasse
 * 
 * @author Harald A. Kulø
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MIPSSStudio {
	private static final Logger log = LoggerFactory.getLogger(MIPSSStudio.class);
	private static PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");

	private AccessControl accessControl;
	private LoginModule loginModule;
	private MainFrame mainFrame;
	
	public static long MOUSE_PX_COUNT=0;
	public static long MOUSE_CL_COUNT=0;
	static{
		BeansBindingHack.doHack();
		Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener(){
			@Override
			public void eventDispatched(AWTEvent event) {
				MouseEvent e = (MouseEvent)event;
				if (e.getID()==MouseEvent.MOUSE_RELEASED){
					MOUSE_CL_COUNT++;
				}
			}
			
		}, AWTEvent.MOUSE_EVENT_MASK); 
		Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener(){
			private MouseEvent prev;
			@Override
			public void eventDispatched(AWTEvent event) {
				if (prev!=null){
					MouseEvent e = (MouseEvent)event;
					Point p = new Point(prev.getX(), prev.getY());
					Point p2 = new Point (e.getX(), e.getY());
					MOUSE_PX_COUNT+=p.distance(p2);
					prev = e;
				}
				else{
					prev = (MouseEvent)event;
				}
			}
			
		}, AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}
	private MIPSSStudio() {
		Locale.setDefault(new Locale("no", "NO"));
		try {
			UIManager.setLookAndFeel(new WindowsLookAndFeel());
		} catch (Exception e) {
			return;
		}
		ToolTipManager.sharedInstance().setDismissDelay(20000);
		
		PropertySetup setup = new PropertySetup();
		setup.execute();
		
		setUpSwingLocale();
	}

	/**
	 * AccessControl modulen som vi bruker til å hente brukeren med rettigheter
	 * fra databasen
	 * 
	 * @return
	 */
	private AccessControl getAccessControl() {
		if (accessControl == null) {
			accessControl = BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class);
		}

		return accessControl;
	}

	/**
	 * Henter brukeren fra databasen basert på brukernavnet som er pålogget
	 * 
	 * @param subject
	 * @return
	 */
	private Bruker getUser(Subject subject) {
		Bruker bruker =null;
		try{
			String signatur = ((Principal) subject.getPrincipals().toArray()[0]).getName();
			bruker = getAccessControl().hentBruker(signatur.substring(0, signatur.indexOf("@")));		
		} catch (Throwable t){
			t.printStackTrace();
		}
		return bruker;
	}
	/**
	 * Henter brukeren fra databasen basert på brukernavnet som er pålogget
	 * 
	 * @param subject
	 * @return
	 */
	private Bruker getUserNoAD(Subject subject) {
		Bruker bruker =null;
		try{
			String signatur = ((Principal) subject.getPrincipals().toArray()[0]).getName();
			bruker = getAccessControl().hentBruker(signatur);
		} catch (Throwable t){
			t.printStackTrace();
		}
		return bruker;
	}

	/**
	 * Avviser tilgang til studio
	 * 
	 * @param retry
	 * @param reason
	 */
	private void rejectStudioAccess(boolean retry, String reason) {
		log.warn("rejectStudioAccess(" + retry + "," + reason + ")");
		MipssDialogue dialogue = new MipssDialogue(null, IconResources.INFO_ICON, "Tilgangskontroll", resources
				.getGuiString("studio.plugins.accessdenied")
				+ reason + "</html>", MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
		dialogue.askUser();
		MIPSSStudio.main(new String[]{"restart"});
	}

	/**
	 * En key hook som log av systemet hvis man trykker ctrl + alt + x
	 * flytt til en annen klasse
	 */
	private void setLogoffKeyHook() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_X) {
					int rvl = JOptionPane.showConfirmDialog(mainFrame.getApplicationFrame(), resources
							.getGuiString("studio.logoff.text1"), resources.getGuiString("studio.logoff.text2"),
							JOptionPane.YES_NO_OPTION);
					if (rvl == JOptionPane.YES_OPTION) {
						if (mainFrame != null)
							mainFrame.getApplicationFrame().dispose();
						mainFrame = null;
						try {
							Thread.sleep(500);
						} catch (InterruptedException ex) {
							ex = null;
						}
						try {
							loginModule.logOut();
						} catch (LoginException e1) {
							e1.printStackTrace();
						}
						KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(this);
						MIPSSStudio.main(new String[] { "restart" });
					}
					return true;
				}
				return false;
			}
		});
	}

	private void setUpSwingLocale() {
		ResourceBundle bundle = PropertyResourceBundleUtil.getPropertyBundle("swingLocale", new Locale("no", "NO"));
		Enumeration<String> keys = bundle.getKeys();

		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			UIManager.put(key, bundle.getString(key));
		}
	}

	public void start(String[] args) {
        log.debug("Starting MIPSS Studio, arges="+args);
		Boolean useAD = Boolean.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "useADLogIn").getVerdi());
		// litt dirty, men vi får se på det senere...
		if (args != null && args.length > 0) {
			if (args[0] == "restart") {
				loginModule = new LoginModule();
				loginModule.login(true);
			}
		} else {
			loginModule = new LoginModule();
			if (useAD) {
                log.debug("Using AD");
				loginModule.login(false);
			} else{
                log.debug("Not using AD");
				loginModule.login(true);
			}
		}
		
		Subject subject = loginModule.getSubject();
        log.debug("Subject="+subject);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					loginModule.logOut();
				} catch (LoginException e) {
					log.error("Log out failed", e);
				}
			}
		});
		setLogoffKeyHook();
		// bruker er ikke verifisert
		if (subject == null) {
			rejectStudioAccess(false, "Feil i LDAP verifisering");
			log.warn("No access");
			System.exit(0);
		}

		final Bruker bruker = useAD?getUser(subject):getUserNoAD(subject);

		if (bruker == null) {
			rejectStudioAccess(true, "Bruker er ikke lagt til i MIPSS-databasen");
			return;
		}
		
		// brukeren er verifisert hent applikasjoner.
		final ArrayList<MenynodeDTO> menyList = new ArrayList<MenynodeDTO>();
		try {
			ApplikasjonLoader loader = BeanUtil.lookup(ApplikasjonLoader.BEAN_NAME, ApplikasjonLoader.class);
			List<Menypunkt> menypunkter = loader.getMenypunktAll();
			
			// bygg menynoder slik at menyen kan vises frem i en trestruktur
			String p = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "personligMenytre").getVerdi();
			Boolean personligTre  = Boolean.valueOf(p);
			for (Menypunkt m : menypunkter) {
				if (personligTre){
					if (bruker.hasModuleReadAccess(m.getTilgangspunkt().getModul())||bruker.hasModuleWriteAccess(m.getTilgangspunkt().getModul())){
						menyList.add(new MenynodeDTO(m));
					}
				}else{
					menyList.add(new MenynodeDTO(m));
				}
			} 
		
			
		} catch (Exception e) {
			log.error("Exception in start", e);
			reportError("Det oppstod en feil i kontakt med server(meny)");
			System.exit(100);
		}
		
		keepAlive();
		// Applikasjonene er hentet, start GUI
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				String tittel = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "tittel").getVerdi();
				mainFrame = new MainFrameImpl(tittel+" (" + bruker.getNavn() + ")", menyList);
				MipssStudioLoaderImpl loader = new MipssStudioLoaderImpl(mainFrame, bruker, getAccessControl(), menyList);
				mainFrame.init(loader);
				mainFrame.flashMessage("Velkommen " + bruker.getNavn() + "(" + bruker.getSignatur() + ")", 0);
				accessControl = null;
				log.info("welcome!");
				log.info("java version:"+System.getProperty("java.version"));
				System.out.println("doing hack");
				
				String tilgangspunkt = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "tilgangspunktVedOppstart").getVerdi();
				if (tilgangspunkt != null){
					for (MenynodeDTO menynodeDTO : menyList) {
						if (menynodeDTO.getTilgangspunkt().getId() == Integer.parseInt(tilgangspunkt)) {
							loader.activateMenuItem(menynodeDTO);
						}
					}
				}								
				
				if (bruker.getByttPassord()) {
					JOptionPane.showMessageDialog(null, 
							"<html>Det er første gang du logger inn, eller du har fått beskjed om at du må bytte passord.<br>Trykk 'OK' for å registrere nytt passord</html>",
							"Du må bytte passord", JOptionPane.OK_OPTION);
					new ChangePasswordDialog(bruker.getSignatur());
				}			
			}
		});
	}

	private void keepAlive(){
		log.info("Starting keep-alive thread..");
		new Thread("Keep-alive thread"){
			public void run(){
				while (true){
					try{
						log.warn("ping? "+BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class).ping());
						Thread.sleep(20000);
					}catch (Exception e){
						log.trace("Exception by peer");
					}
				}
			}
		}.start();
	}
	public static void main(String[] args) {
        log.debug("Main method begin");
		String jdbc = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "oppdatering.jdbc").getVerdi();
        log.debug("Creating update thread");
		UpdateThread updateThread = new UpdateThread(jdbc);
        log.debug("Starting update thread");
		updateThread.start();

        log.debug("Creating MIPSSStudio()");
		MIPSSStudio mipssStudio = new MIPSSStudio();
		mipssStudio.start(args);
	}

    /**
	 * Enkel feildialog til brukeren
	 * 
	 * @param message
	 */
	private static void reportError(String message) {
		log.error("MSG[" + message + "]");
		MipssDialogue dialogue = new MipssDialogue(null, IconResources.ERROR_ICON_SMALL, "Applikasjonsfeil", resources
				.getGuiString("studio.plugins.error", new Object[] { message }), MipssDialogue.Type.ERROR,
				new MipssDialogue.Button[] { MipssDialogue.Button.OK });
		dialogue.askUser();
	}
}
