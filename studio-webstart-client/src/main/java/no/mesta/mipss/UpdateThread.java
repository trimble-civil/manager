package no.mesta.mipss;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.resources.images.IconResources;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXBusyLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateThread extends Thread{
	private static final Logger log = LoggerFactory.getLogger(UpdateThread.class);

	private boolean updating;
	private boolean running = true;
	private boolean notified;
	private boolean updated;
	
	private JDialog updateWaitDialog;
	private JDialog updateMessage;
	
	private static final int WAIT_TIME = 30*1000;
	
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");
	
	private String jdbc = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=mesdb002-scan.mesta.local)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=PSTYR.MESTA.LOCAL)(FAILOVER_MODE=(TYPE=select)(METHOD=basic))))";
	
	public UpdateThread(String jdbc){
		super("MIPSS update thread");
		this.jdbc = jdbc;
	}
	public void run(){
		while (running) {
			try(Connection c=getConnection()){
				if (!notified) {
					checkNotificationStatus(c);
				}
				if (!updated) {
					checkUpdatestatus(c);
				}
			}catch(SQLException e) {
				log.error("Unable to get connection", e);
			}

			try {
				Thread.sleep(WAIT_TIME);
			} catch (InterruptedException t) {
				t.printStackTrace();
			}
		}
	}
	
	private void checkUpdatestatus(Connection c){
		if (isUpdateReady(c)&&!updating){
			updating = true;
			updateWaitDialog = new JDialog();
			updateWaitDialog.setAlwaysOnTop(true);
			updateWaitDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			JXBusyLabel message = new JXBusyLabel();
			message.setText(props.getGuiString("studio.updateTekst"));
			message.setBusy(true);
			JButton kill = new JButton("Avslutt studio");
			kill.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
			JPanel m = new JPanel(new GridBagLayout());
			m.add(message, new GridBagConstraints(0,0,0,0,1.0,1.0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5),0,0));
			m.add(kill, new GridBagConstraints(0,1,0,0,1.0,0.0,GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(5,5,5,5),0,0));
			
			updateWaitDialog.add(m);
			updateWaitDialog.pack();
			updateWaitDialog.setLocationRelativeTo(null);
			updateWaitDialog.setVisible(true);
		}
		if (!isUpdateReady(c)&&updating){
			updated=true;
			updateWaitDialog.dispose();
			updateWaitDialog = null;
			JOptionPane.showMessageDialog(null, props.getGuiString("studio.doneUpdateTekst"));
		}
	}
	
	private void checkNotificationStatus(Connection c){
		String message = isNotificationReady(c);
		if (!StringUtils.isBlank(message)){
			updateMessage = new JDialog();
			updateMessage.setAlwaysOnTop(true);
			updateMessage.setModal(true);
			JLabel messagelbl = new JLabel(message, IconResources.YELLOW_HELMET_ICON, JLabel.LEADING);
			JPanel messagepnl = new JPanel(new GridBagLayout());
			messagepnl.add(messagelbl, new GridBagConstraints(0,0,0,0,1.0,1.0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(15,15,15,15),0,0));
			updateMessage.add(messagepnl);
			updateMessage.pack();
			updateMessage.setTitle(getNotificationHeader(c));
			updateMessage.setLocationRelativeTo(null);
			updateMessage.setVisible(true);
			notified = true;
			updateMessage.dispose();
			updateMessage = null;
		}
	}
	
	public void kill(){
		running = false;
		interrupt();
	}
	
	public boolean isUpdateReady(Connection c){
		try(PreparedStatement st = c.prepareStatement("select verdi from konfigparam where navn='oppdaterer' and applikasjon = 'studio.mipss'");
			ResultSet rs = st.executeQuery()) {
			if (rs.next()){
				String updating = rs.getString(1);
				if (Integer.valueOf(updating)==1){
					return true;
				}
			}
		}catch (Throwable t){
			t.printStackTrace();
			return false;
		}
		return false;
	}
	public String isNotificationReady(Connection c){
		try(PreparedStatement st = c.prepareStatement("select verdi from konfigparam where navn='oppdatering.melding' and applikasjon = 'studio.mipss'");
			ResultSet rs = st.executeQuery()) {
			if (rs.next()){
				String melding = rs.getString(1);
				return melding;
			}
		}catch (Throwable t){
			t.printStackTrace();
			return null;
		}
		return null;
	}
	
	public String getNotificationHeader(Connection c){
		try(PreparedStatement st = c.prepareStatement("select verdi from konfigparam where navn='oppdatering.melding.tittel' and applikasjon = 'studio.mipss'");
			ResultSet rs = st.executeQuery()) {
			if (rs.next()){
				String melding = rs.getString(1);
				return melding;
				
			}
		}catch (Throwable t){
			t.printStackTrace();
			return null;
		}
		return null;
	}
	private Connection getConnection() {
		String usr = "mipss";
		String pwd = "mipss";
		Connection c=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			c = DriverManager.getConnection(jdbc, usr, pwd);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("UpdateThread:"+e.getMessage());
		}
		return c;
	}
}