package no.mesta.mipss;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.resources.images.IconResources;

public class Pomodoro {
	private static final int POMODORO_RUNTIME = 1500;
	private static final int POMODORO_SHORT_BREAK = 300;
	private static final int POMODORO_LONG_BREAK = 600;
	
	private JFrame frame;
	private JLabel label;
	private JButton start;
	private JButton shortBreak;
	private JButton longBreak;
	private Boolean killKillKillPomodoro = Boolean.FALSE;
	
	private JPanel panel;
	private Dimension screen  = Toolkit.getDefaultToolkit().getScreenSize();
	private JPanel south;
	@SuppressWarnings("serial")
	public Pomodoro(){
		label = new JLabel();
		label.setFont(new Font("Tahoma", Font.BOLD, 58));
		start = new JButton(new StartPomodoroAction("Start", IconResources.PLAYER_PLAY, POMODORO_RUNTIME));
		Font f = new Font("Tahome", Font.BOLD, 22); 
		shortBreak = new JButton(new StartPomodoroAction("5min", null, POMODORO_SHORT_BREAK));
		longBreak = new JButton(new StartPomodoroAction("10min", null, POMODORO_LONG_BREAK));
		shortBreak.setFont(f);
		longBreak.setFont(f);
		
		panel = new JPanel(){
			Font f = new Font("Tahoma", Font.BOLD, 58);
			public void paint(Graphics gr){
				super.paint(gr);
				Graphics2D g = (Graphics2D)gr;
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setFont(f);
				g.setColor(Color.white);
				g.drawString(label.getText(), 65, 120);
			}
		};
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.add(Box.createHorizontalGlue());
		panel.add(new JLabel(IconResources.TOMATO));
		panel.add(Box.createHorizontalGlue());
		
		south = new JPanel();
		south.add(Box.createHorizontalGlue());
		south.add(start);
		south.add(shortBreak);
		south.add(longBreak);
		south.add(Box.createHorizontalGlue());
		
		frame = new JFrame();
		update(1500);
		frame.setAlwaysOnTop(true);
		frame.setLayout(new BorderLayout());
		frame.add(panel);
		frame.add(south, BorderLayout.SOUTH);
		frame.pack();
		
		
		frame.setBackground(Color.green);
		
		frame.setLocation((int)(screen.getWidth()-frame.getWidth()), 0);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(((e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) == InputEvent.CTRL_DOWN_MASK)
				&& ((e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) == InputEvent.ALT_DOWN_MASK)
				&& ((e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK) == InputEvent.SHIFT_DOWN_MASK)) {
					synchronized (this) {killKillKillPomodoro = Boolean.TRUE;}
				}
			}
		});
		
		frame.setVisible(true);
	}
	public static void main(String[] args) {
		new Pomodoro();
	}
	private void update(int current){
		int sec = current%60;
		int min = current/60;
		String s = sec>9?sec+"":"0"+sec;
		String m = min>9?min+"":"0"+min;
		label.setText(m+":"+s);
		panel.repaint();
		frame.setTitle(label.getText());
	}
	
	private void end(){
		start.setEnabled(true);
		shortBreak.setEnabled(true);
		longBreak.setEnabled(true);
		panel.setBackground(Color.green);
		frame.setSize(screen);
		frame.setLocation(0,0);
		JOptionPane.showMessageDialog(frame, "TIME's UP!", "The pomodoro is over", JOptionPane.INFORMATION_MESSAGE);
		panel.setBackground(south.getBackground());
		frame.pack();
		frame.setLocation((int)(screen.getWidth()-frame.getWidth()), 0);
	}
	
	@SuppressWarnings("serial")
	class StartPomodoroAction extends AbstractAction{
		private int runtime=1500;
		public StartPomodoroAction(String text, ImageIcon icon, int runtime){
			super(text, icon);
			this.runtime = runtime;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			synchronized (this) {killKillKillPomodoro = Boolean.FALSE;}
			switch(runtime){
				case POMODORO_RUNTIME: 
					panel.setBackground(new Color(255,200,200));
				break;
				case POMODORO_SHORT_BREAK: 
					panel.setBackground(new Color(200,255,200));
				break;
				case POMODORO_LONG_BREAK: 
					panel.setBackground(new Color(200,200,255));
				break;
			}
			
			
			new Thread(new PomodoroRunner(runtime)).start();
			start.setEnabled(false);
			shortBreak.setEnabled(false);
			longBreak.setEnabled(false);
		}
		
	}
	class PomodoroRunner implements Runnable{
		private int p = 1500;
		public PomodoroRunner(int length){
			p = length;
		}
		public void run(){
			while (p>=0){
				synchronized (this) {if(killKillKillPomodoro){break;}}
				update(p);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				p--;
				if (p==60){
					panel.setBackground(Color.red);
				}
			}
			end();
		}
	}
}
