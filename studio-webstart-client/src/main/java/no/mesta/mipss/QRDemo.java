package no.mesta.mipss;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.util.DateTransformer;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QRDemo {
	private boolean debug = false;
	
	@SuppressWarnings({ "unchecked", "rawtypes", "serial" })
	public static void main(String[] args) throws WriterException, IOException, ParseException {
		MultiFormatWriter writer = new MultiFormatWriter();
		Hashtable hints = new Hashtable();
        hints.put( EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q );
        int size = 100;
		DateTransformer d = new DateTransformer();
		SimpleDateFormat fo = new SimpleDateFormat("ddMMyyyy");
		Date date = fo.parse("01022012");
		String dateToString = d.dateToString(date);
        final BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(writer.encode(dateToString, BarcodeFormat.QR_CODE, size, size));
        JPanel pnl = new JPanel(){
        	public void paint(Graphics g){
        		Graphics2D gr = (Graphics2D)g;
        		gr.drawImage(bufferedImage, 0, 0, null);
        	}
        };
        pnl.setSize(size,size);
        JFrame f = new JFrame();
        f.setSize(size+10, size+50);
        f.setLayout(new BorderLayout());
        f.add(pnl);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        
        while(true){
	        try{
	        	String code = JOptionPane.showInputDialog("Trykk kode:");
	        	JOptionPane.showMessageDialog(null, "Gyldig til:"+d.stringToDate(code));
	        } catch (Throwable t){
	        	t.printStackTrace();
	        }
        }
//		QRDemo d = new QRDemo();
//		d.testCodes();
	}
	
	/**
	 * En test for å sjekke hvor mange koder som kan opprettes.
	 */
	public void testCodes(){
		QRDemo d = new QRDemo();
		int current = 0;
		int max = 9999999;
		
		int hitCount = 0;
		SimpleDateFormat f = new SimpleDateFormat("ddMMyyyy");
		Date now=null;
//		try {
			now = Clock.now();// f.parse("12122012");
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
		while (current<max){
			String c = String.valueOf(current);
			if (d.validate(c)){
				try{
					Date hit = d.stringToDate(c);
					if (hit.after(now)){
						System.out.println(c+", "+f.format(hit));
						hitCount++;
						
					}
				} catch (Throwable t){
					//IGNORE
				}
			}
			current++;
		}
		System.out.println("found a total of :"+hitCount);
	}
	
	/**
	 * Kalkulerer kontrollsiffer for tallet med luhns-algoritme (mod10)
	 * @param value
	 * @return
	 */
	private String calculateLuhn(String value){
		int sum = 0;
		for (int i=0; i<value.length(); i++ ){
			sum += Integer.parseInt(value.substring(i,i+1));
		}
		int[] delta = new int[]{0,1,2,3,4,-4,-3,-2,-1,0};
		for (int i=value.length()-1; i>=0; i-=2 ){		
			int deltaIndex = Integer.parseInt(value.substring(i,i+1));
			int  deltaValue = delta[deltaIndex];	
			sum += deltaValue;
		}
		int mod10 = sum % 10;
		mod10 = 10 - mod10;	
		if (mod10==10){		
			mod10=0;
		}
		return String.valueOf(mod10);
	 }
	
	/**
	 * Gjør en xor på v og key
	 * @param v
	 * @param key
	 * @return
	 */
	public int xorify(int v, int key){
		return v^key;
	}
	 /**
	  * Validerer tallet mot kontrollsifferet
	  * @param value
	  * @return
	  */
	public boolean validate(String value){
		int luhnDigit = Integer.parseInt(value.substring(value.length()-1,value.length()));
		String luhnLess = value.substring(0,value.length()-1);
		if (Integer.parseInt(calculateLuhn(luhnLess))==(luhnDigit)){
			return true;
		}	
		return false;
	}
	
	/**
	 * Encoder en java.util.Date. 
	 * 
	 * OBS: Denne algoritmen gir kun 1/5 så stor sjanse å cracke som å vinne i lotto.
	 *  
	 * @param date
	 * @return
	 */
	public String dateToString(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dd = c.get(Calendar.DAY_OF_MONTH);
		int mm = c.get(Calendar.MONTH);
		String y1 = String.valueOf(c.get(Calendar.YEAR));
		int yy = Integer.valueOf(y1.substring(2));
		if (debug)
			System.out.println("Before:"+dd+" "+mm+" "+yy);
		
		String d = toHex(dd);
		String m = toHex(mm);
		String y = toHex(yy);
		
		String hex = m+""+y+""+d;
		int parseInt = Integer.parseInt(hex, 16);
		
		String digit = calculateLuhn(String.valueOf(parseInt));
		if (debug){
			System.out.println("HEX-string:"+hex);
			System.out.println("Integer code:"+parseInt);
			System.out.println("Kontrollsiffer:"+digit);
		}
		return String.valueOf(parseInt)+digit;
	}
	
	/**
	 * Konverterer en integer til hex og venstre-padder med 0 slik at hex-verdien består av 2 tegn
	 * @param s
	 * @return
	 */
	public String toHex(int s){
		StringBuilder sb = new StringBuilder();
		int i = Integer.valueOf(s);
		if (i<=0x00F)sb.append("0");
		sb.append(Integer.toHexString(i));
		return sb.toString();
	}
	
	/**
	 * Konverterer en hex verdi til en int
	 * @param hex
	 * @return
	 */
	public int fromHex(String hex){
		return Integer.parseInt(hex, 16);
	}
	/**
	 * Dekoder en obfuskert dato til en java.util.Date
	 * @param str
	 * @return
	 */
	public Date stringToDate(String integerDate){
		//validates the number
		if (!validate(integerDate)){
			throw new RuntimeException("Someone tried to crack the app (Luhns_algo_failed_exception)");
		}
		//fjerner kontrollsiffer før dato transformeres videre
		integerDate = integerDate.substring(0, integerDate.length()-1);
		String hex = Integer.toHexString(Integer.valueOf(integerDate));
		if (debug){
			System.out.println("Integer code:"+integerDate);
			System.out.println("HEX-string:"+hex);
		}
		
		String[] values = new String[3];
		int idx = 0;
		while (hex.length()>0){
			int i = hex.length()>1?2:1;
			values[idx]=hex.substring(hex.length()-i, hex.length());
			hex = hex.substring(0, hex.length()-i);
			idx++;
		}
		
		int m = fromHex(values[2]);
		int y = fromHex(values[1]);
		int d = fromHex(values[0]);
		
		if (d!=1){ //datoen skal være den 1. i hver måned
			throw new RuntimeException("Someone tried to crack the app (Wrong_calculated_day_exception)");
		}
		if (debug)
			System.out.println("Result:"+d+" "+m+" "+y);
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, d);
		c.set(Calendar.MONTH, m);
		c.set(Calendar.YEAR, 2000+y);
		if (debug)
			System.out.println("valid to:"+c.getTime());
		
		Date oneYear = CalendarUtil.getYearsAgo(-1); //Koder skal kun aksepteres for ett år fremover i tid
		
		if (c.getTime().after(oneYear)){//If luhns "failes"...
			throw new RuntimeException("Someone tried to crack the app (Wrong_master_year_size_exception)");
		}
		return c.getTime();
	}
}