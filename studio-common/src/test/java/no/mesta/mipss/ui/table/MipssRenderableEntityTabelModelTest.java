package no.mesta.mipss.ui.table;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

public class MipssRenderableEntityTabelModelTest extends TestCase{
    public MipssRenderableEntityTabelModelTest() {
    }
    
    public void testModel() {
        List<Bruker> brukere = new ArrayList<Bruker>();
        Bruker b1 = new Bruker();
        Bruker b2 = new Bruker();
        b1.setNavn("Bruker 1");
        b2.setNavn("Bruker 2");
        b1.setSignatur("Signatur 1");
        b2.setSignatur("Signatur 2");
        brukere.add(b1);
        brukere.add(b2);
        /*
        MipssRenderableEntityTableModel<Bruker> model = new MipssRenderableEntityTableModel<Bruker>(Bruker.class, brukere, "navn", "signatur");
        
        assertEquals("row count", 2, model.getRowCount());
        assertEquals("column count", 2, model.getColumnCount());
        assertEquals("navn1", "Bruker 1", model.getValueAt(0,0));
        assertEquals("signatur1", "Signatur 1", model.getValueAt(0,1));
        assertEquals("navn2", "Bruker 2", model.getValueAt(1,0));
        assertEquals("signatur2", "Signatur 2", model.getValueAt(1,1));
        
        model.setValueAt("Nytt navn", 0,0);
        assertEquals("setValue", "Nytt navn", model.getValueAt(0,0));
        */
    }
}
