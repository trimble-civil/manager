package no.mesta.mipss.ui.table;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.persistence.Clock;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalendarUtilTest {
    private Calendar c;

    @Before
    public void setUp() {
        c = Calendar.getInstance();
        Locale.setDefault(new Locale("no"));
    }

    private void setTime(int year, int month, int day, int hour) {
        c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR_OF_DAY, hour);

        Clock.setClock(c.getTime());
    }

    private void checkDate(Date date, int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        assertEquals(year, cal.get(Calendar.YEAR));
        assertEquals(month, cal.get(Calendar.MONTH));
        assertEquals(day, cal.get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void testGetNextOccurenceOfDayOfWeekRegularWeek() {
        //henter neste (første) onsdag kl 0800
        setTime(2013, Calendar.JANUARY, 9, 8);
        Date next = CalendarUtil.getNextOccurrenceOfDayOfWeek(Calendar.WEDNESDAY, 8);

        checkDate(next, 2013, Calendar.FEBRUARY, 6);
    }

    @Test
    public void testGetNextOccurenceOfDayOfWeekSameDay() {
        setTime(2013, Calendar.FEBRUARY, 6, 7);
        //henter neste (første) onsdag kl 0800
        Date next = CalendarUtil.getNextOccurrenceOfDayOfWeek(Calendar.WEDNESDAY, 8);

        checkDate(next, 2013, Calendar.FEBRUARY, 6);
    }

    @Test
    public void testSameDayAfterEight() throws Exception {
        setTime(2013, Calendar.FEBRUARY, 6, 9);
        //henter neste (første) onsdag kl 0800
        Date next = CalendarUtil.getNextOccurrenceOfDayOfWeek(Calendar.WEDNESDAY, 8);
        checkDate(next, 2013, Calendar.MARCH, 6);
    }

    @Test
    public void testLeapYearEarlyFebruary() throws Exception {
        setTime(2016, Calendar.FEBRUARY, 1, 9);
        //henter neste (første) onsdag kl 0800
        Date next = CalendarUtil.getNextOccurrenceOfDayOfWeek(Calendar.WEDNESDAY, 8);

        checkDate(next, 2016, Calendar.FEBRUARY, 3);
    }

    @Test
	public void testLeapYearMarch(){
        setTime(2016, Calendar.FEBRUARY, 29, 9);
		//henter neste (første) onsdag kl 0800
		Date next = CalendarUtil.getNextOccurrenceOfDayOfWeek(Calendar.WEDNESDAY, 8);

        checkDate(next, 2016, Calendar.MARCH, 2);
	}
}
