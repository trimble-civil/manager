package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;

import java.awt.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MipssTableComboBoxRendererTest {

    private MipssTableComboBoxRenderer renderer;
    private JTable table;
    private IRenderableMipssEntity value;

    @Before
    public void setUp() throws Exception {
        renderer = new MipssTableComboBoxRenderer();
        table = mock(JTable.class);
        when(table.isCellEditable(0,0)).thenReturn(true);
        when(table.isCellEditable(1,0)).thenReturn(false);
        value = mock(IRenderableMipssEntity.class);
        when(value.getTextForGUI()).thenReturn("testValue");
    }

    @Test
    public void returnsComboBoxWhenCellIsEditable() throws Exception {
        Component component = renderer.getTableCellRendererComponent(table, value, false, false, 0, 0);
        assertTrue(component instanceof JComboBox);
        assertEquals(value, ((JComboBox)component).getSelectedItem());
    }

    @Test
    public void returnsLabelWhenCellIsNotEditable() throws Exception {
        Component component = renderer.getTableCellRendererComponent(table, value, false, false, 1, 0);
        assertTrue(component instanceof JLabel);
        assertEquals("testValue", ((JLabel)component).getText());
    }
}