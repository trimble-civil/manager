package no.mesta.mipss.ui.table;

import javax.swing.table.TableColumn;

import junit.framework.TestCase;

import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

public class MipssRenderableEntityTableColumnModelTest extends TestCase{
    public MipssRenderableEntityTableColumnModelTest() {
    }
    
    public void testModel() {
        MipssRenderableEntityTableColumnModel model = new MipssRenderableEntityTableColumnModel(Bruker.class, "navn", "signatur");
        
        assertEquals("column count", 2, model.getColumnCount());
        
        TableColumn tc = model.getColumn(0);
        assertEquals("identifier", "no.mesta.mipss.persistence.tilgangskontroll.Bruker.navn", tc.getIdentifier());
        
        int tc2index = model.getColumnIndex("no.mesta.mipss.persistence.tilgangskontroll.Bruker.signatur");
        TableColumn tc2 = model.getColumn(tc2index);
        assertEquals("header", "Signatur", (String) tc2.getHeaderValue());
    }
}
