package no.mesta.mipss.plugin;

import java.io.Serializable;

import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Nøkkel som kan benyttes til å cache instansierte kjørende plugins
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PluginCacheKey implements Serializable {
    private Tilgangspunkt tilgangspunkt;
	private Class<MipssPlugin> plugin;
    private Object[] arguments;
    private PluginLookupKey lookupKey;

    /**
     * Konstruktør
     * 
     * @param plugin
     * @param a
     */
	public PluginCacheKey(Tilgangspunkt tp, Class<MipssPlugin> plugin, Object[] a) {
        this.tilgangspunkt = tp;
        this.plugin = plugin;
        arguments = a;
        
        lookupKey = new PluginLookupKey(plugin, a);
    }

    /**
     * Tilgangspunktet som startet plugin
     * 
     * @return
     */
	public Class<MipssPlugin> getPluginClass() {
        return plugin;
    }

    /**
     * Argumentene til konstruktøren
     * 
     * @return
     */
    public Object[] getArguments() {
        return arguments;
    }

    /**
     * Tilgangspunktet den ble startet med
     * 
     * @return
     */
    public Tilgangspunkt getTilgangspunkt() {
        return tilgangspunkt;
    }

    /**
     * Søkenøkkel
     * 
     * @return
     */
    public PluginLookupKey getLookupKey() {
        return lookupKey;
    }

    /**
     * @see java.lang.Object#equals(Object)
     * 
     * @param o
     * @return
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof PluginCacheKey)) {
            return false;
        }

        PluginCacheKey other = (PluginCacheKey)o;

        return new EqualsBuilder().
            append(tilgangspunkt, other.getTilgangspunkt()).
            append(plugin, other.getPluginClass()).
            append(arguments, other.getArguments()).
            isEquals();
    }

    /**
     * @see java.lang.Object#hashCode()
     * 
     * @return
     */
    public int hashCode() {
        return new HashCodeBuilder(2005, 2095).
            append(tilgangspunkt).
            append(plugin).
            append(arguments).
            toHashCode();
    }

    /**
     * @see java.lang.Object#toString()
     * 
     * @return
     */
    public String toString() {
        return new ToStringBuilder(this).
            append("tilgangspunkt", tilgangspunkt).
            append("plugin", plugin).
            append("arguments", arguments).toString();
    }
}

