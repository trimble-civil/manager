package no.mesta.mipss.plugin;

import java.util.Map;


/**
 * Adapter klasse for IPluginMessage
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public abstract class PluginMessage<K,V> implements IPluginMessage<K,V,MipssPlugin>{
    private Map<K,V> parameteres = null;
    private MipssPlugin target = null;
    private MipssPlugin source = null;
    private boolean consumed;

    /** {@inheritDoc} */
    public void setTargetPlugin(MipssPlugin plugin) {
        if(target == null) {
            target = plugin;
        } else {
            throw new IllegalStateException("Cannot overwrite target plugin!");
        }
    }

    /** {@inheritDoc} */
    public MipssPlugin getTargetPlugin() {
        return target;
    }

    /** {@inheritDoc} */
    public void setSourcePlugin(MipssPlugin plugin) {
        if(source == null) {
            source = plugin;
        } else {
            throw new IllegalStateException("Cannot overwrite source plugin");
        }
    }

    /** {@inheritDoc} */
    public MipssPlugin getSourcePlugin() {
        return source;
    }

    /** {@inheritDoc} */
    public void setParameters(Map<K,V> params) {
        if(parameteres == null) {
            parameteres = params;
        } else {
            throw new IllegalStateException("Cannot overwrite parameters!");
        }
    }

    /** {@inheritDoc} */
    public Map<K,V> getParameters() {
        return parameteres;
    }
    
    /** {@inheritDoc} */
    public boolean isConsumed() {
        return consumed;
    }
    
    /** {@inheritDoc} */
    public void setConsumed(boolean flag) {
        consumed = flag;
    }
}
