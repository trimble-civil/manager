package no.mesta.mipss.plugin;

import java.awt.Component;

import java.util.Map;

/**
 * Message API for utveksling av kall mellom Plugins
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
interface IPluginMessage<K,V,P extends MipssPlugin> {
    
    /**
     * Plugin som skal motta denne meldingen
     * 
     * @param plugin
     */
    void setTargetPlugin(P plugin);
    
    /**
     * Plugin som mottar meldingen
     * 
     * @return
     */
    P getTargetPlugin();
    
    /**
     * Plugin som sender meldingen
     * 
     * @param plugin
     */
    void setSourcePlugin(P plugin);
    
    /**
     * Plugin som sendte meldingen
     * 
     * @return
     */
    P getSourcePlugin();
    
    /**
     * Utfører arbeidet som ligger i meldingen
     * 
     */
    void processMessage();
    
    /**
     * Parametere som meldingen trenger
     * 
     * @param params
     */
    void setParameters(Map<K,V> params);
    
    /**
     * Parametere som meldingen benytter
     * 
     * @return
     */
    Map<K,V> getParameters();
    
    /**
     * Resultat etter processMessage hvis ikke processMessage agerer direkte
     * 
     * @return
     */
    Map<K,V> getResult();
    
    /**
     * Evt gui. ADVARSEL: Kan strykes
     * 
     * @return
     */
    Component getMessageGUI();
    
    /**
     * Angir om en melding er behandlet eller ikke
     * 
     * @return
     */
    boolean isConsumed();
    
    /**
     * Angir om en melding er behandlet eller ikke
     * 
     * @param flag
     */
    void setConsumed(boolean flag);
}
