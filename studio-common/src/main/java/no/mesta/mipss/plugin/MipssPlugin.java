package no.mesta.mipss.plugin;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.BrukerTilgang;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Superklasse for alle plugins.
 * 
 * Sørger for at init patern for plugin start overholdes
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public abstract class MipssPlugin implements IMipssPlugin, PropertyChangeSource {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");

	private static final String ERROR_NO_ACCESS = "NO ACCESS";
	private static final String ERROR_READ_ONLY = "READ ONLY ACCESS";
	private static final String FIELD_DISPLAYNAME = "displayName";
	private static final String FIELD_ICON = "icon";
	private static final String FIELD_MODULENAME = "modulName";
	private final static Logger logger = LoggerFactory.getLogger(MipssPlugin.class);
	private PluginCacheKey cacheKey;
	private String displayName;
	private boolean docked = true;
	private Icon icon;
	protected IMipssStudioLoader loader;
	private MenynodeDTO menynode;
	private Modul modul;
	private boolean noAccess;
	protected PropertyChangeSupport props = new PropertyChangeSupport(this);
	private boolean readAccess;
	private boolean scrollable;
	private boolean startInBackground;
	private Status status = Status.NOT_STARTED;
	private Tilgangspunkt tilgangspunkt;
	private boolean writeAccess;

	/**
	 * Konstruktør
	 * 
	 */
	public MipssPlugin() {
		scrollable = true;
	}
	
	public MipssPlugin(boolean scrollable) {
		this.scrollable = scrollable;
	}
	
	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}
	
	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}
	
	public void enforceReadAccess() {
		logger.debug("enforceReadAccess() start");
		if (noAccess || (!readAccess && !writeAccess)) {
			throw new IllegalStateException(ERROR_NO_ACCESS);
		}
	}

	public void enforceWriteAccess() {
		logger.debug("enforceWriteAccess() banished: " + noAccess + " writeAccess: " + writeAccess + " start");
		if (noAccess || !writeAccess) {
			rejectWriteAccess();
			throw new IllegalStateException(ERROR_READ_ONLY);
		}
	}

	/**
	 * Nøkkel som benyttes til caching av plugin
	 * 
	 * @return
	 */
	public PluginCacheKey getCacheKey() {
		return cacheKey;
	}

	/**
	 * Visningsnavnet til plugin
	 * 
	 * @return
	 */
	public String getDisplayName() {
		logger.debug("getDisplayName() start");
		if (displayName == null) {
			if (menynode != null) {
				displayName = menynode.getVisningsnavn();
			} else if (modul != null) {
				displayName = modul.getNavn();
			} else if (tilgangspunkt != null) {
				if (tilgangspunkt.getModul() != null) {
					modul = tilgangspunkt.getModul();
					displayName = modul.getNavn();
				}
			} else {
				displayName = "<ukjent>";
			}
		}

		logger.debug("getDisplayName() finished");
		return displayName;
	}

	/**
	 * Ikon for modulen
	 * 
	 * @return
	 */
	public Icon getIcon() {
		logger.trace("getIcon() start");
		if (icon == null) {
			if (menynode != null) {
				if (menynode.getMenynodeIkon() != null) {
					logger.debug("getIcon() Icon image set");
					icon = menynode.getMenynodeIkon();
				}
			}
		}

		Icon currentIcon = null;
		if (status == Status.NOT_STARTED) {
			currentIcon = IconResources.PLUGIN_NOT_STARTED;
		} else if (status == Status.STARTING) {
			currentIcon = IconResources.PLUGIN_STARTING;
		} else if (status == Status.CACHED) {
			currentIcon = IconResources.PLUGIN_CACHED;
		} else if (status == Status.STOPPING) {
			currentIcon = IconResources.PLUGIN_STOPPING;
		} else {
			currentIcon = icon;
		}

		logger.trace("getIcon() finished");
		return currentIcon;
	}

	/**
	 * Hjelpemetode for plugins hvis de må snakke med loaderen.
	 * 
	 * @return
	 */
	public IMipssStudioLoader getLoader() {
		return loader;
	}

	/**
	 * Menypunktet som åpnet plugin
	 * 
	 * @return
	 */
	public MenynodeDTO getMenynode() {
		return menynode;
	}

	/**
	 * Den virtuelle modulen i MIPSS Studio denne plugin representerer
	 * 
	 * @return
	 */
	public Modul getModul() {
		return modul;
	}

	/**
	 * Fra IMipssPlugin Interface
	 * 
	 * @return
	 */
	public abstract JComponent getModuleGUI();

	/**
	 * Gir navnet til modulen
	 * 
	 * @return
	 */
	public String getModuleName() {
		return (modul == null ? "<ukjent>" : modul.getNavn());
	}

	/**
	 * Statusen denne plugin har
	 * 
	 * @return
	 */
	public MipssPlugin.Status getStatus() {
		return status;
	}

	/**
	 * Tilgangspunktet som startet plugin
	 * 
	 * @return
	 */
	public Tilgangspunkt getTilgangspunkt() {
		return tilgangspunkt;
	}

	public boolean hasNoAccess() {
		
		return noAccess;
	}
	
	private void rejectWriteAccess(){
		String message = resources.getGuiString("plugin.rejectWriteAccess", new Object[]{modul.getNavn()});
		String title = resources.getGuiString("plugin.rejectWriteAccess.title");
		MipssDialogue dialogue = new MipssDialogue((JFrame)SwingUtilities.windowForComponent(getModuleGUI()), IconResources.INFO_ICON, title, message, MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
		dialogue.askUser();
	}
	public boolean hasReadAccess() {
		return readAccess;
	}

	public boolean hasWriteAccess() {
		return writeAccess;
	}

	/**
	 * Init metoden for plugins. Sørger for at loaderen kalles når plugin er
	 * ferdig lastet
	 * 
	 * @param loader
	 */
	public void init(final IMipssStudioLoader loader) {
		logger.debug("init(" + loader + ") start");
		this.loader = loader;

		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			private boolean failed = false;

			protected Void doInBackground() {
				status = Status.STARTING;
				try {
					Bruker user = getLoader().getLoggedOnUser(false);
					Modul module = getModul();
					readAccess = user.hasModuleReadAccess(module);
					writeAccess = user.hasModuleWriteAccess(module);
					noAccess = user.isBanishedFromModule(module);

					initPluginGUI();
				} catch (Throwable t) {
					loader.handleException(t, (Object) MipssPlugin.this, "Oppstart feilet");
					failed = true;
				}
				return null;
			}

			protected void done() {
				if (!failed) {
					loader.pluginReady(MipssPlugin.this);
					status = Status.RUNNING;
				} else {
					loader.closePlugin(MipssPlugin.this);
				}
				logger.debug("init(" + loader + ") notified loader, returning");
			}
		};
		worker.execute();
		worker = null;
		logger.debug("init(" + loader + ") scheduled");
	}

	/**
	 * Må implementeres av plugins slik at de lager sitt GUI
	 * 
	 */
	protected abstract void initPluginGUI();

	/**
	 * Standard så er ikke plugins cahceable. Kan overlastes av plugin slik at
	 * den blir det.
	 * 
	 * @return
	 */
	public Boolean isCacheable() {
		return true;
	}

	/**
	 * Angir om plugin er løst ut fra hovedvinduet
	 * 
	 * @return
	 */
	public boolean isDocked() {
		return docked;
	}

	/**
	 * Angir om plugin ønsker å være i et scrollpane
	 * 
	 * @return
	 */
	public boolean isScrollable() {
		return scrollable;
	}

	/**
	 * Angir om plugin skal startes i bakgrunn eller ikke
	 * 
	 * @return
	 */
	public boolean isStartInBackground() {
		return startInBackground;
	}

	/**
	 * Denne kan overlastes hvis plugin vil undersøke med brukeren om stopping
	 * av plugin skal gjøres
	 * 
	 * @param dialog
	 */
	public void readyForShutdown(MipssDialogue dialog) {
	}

	/**
	 * <p>
	 * Overlast denne hvis plugin skal være bevvist meldinger.
	 * </p>
	 * 
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * @param message
	 */
	public void receiveMessage(PluginMessage<?, ?> message) {
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	/**
	 * Nøkkel som benyttes til caching av plugin
	 * 
	 * @param cacheKey
	 */
	public void setCacheKey(PluginCacheKey cacheKey) {
		this.cacheKey = cacheKey;
	}

	/**
	 * Tillater overskrivning av visningsnavnet for plugin
	 * 
	 * @param displayName
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Angir om plugin er løst ut fra hovedvinduet
	 * 
	 * @param docked
	 */
	public void setDocked(boolean docked) {
		this.docked = docked;
	}

	/**
	 * Ikon for modulen, null kan bli overstyrt hvis et tilknyttet menypunkt har
	 * ikon
	 * 
	 * @param icon
	 */
	public void setIcon(Icon icon) {
		Icon old = getIcon();
		this.icon = icon;
		props.firePropertyChange(FIELD_ICON, old, getIcon());
	}

	/**
	 * Menypunktet som åpnet plugin
	 * 
	 * @param menynode
	 */
	public void setMenynode(MenynodeDTO menynode) {
		String old = displayName;
		Icon oldIcon = icon;

		displayName = null; // hack for å få kalkulert nytt og bedre displayName
		icon = null; // hack: ditto
		this.menynode = menynode;

		props.firePropertyChange(FIELD_ICON, oldIcon, getIcon());
		props.firePropertyChange(FIELD_DISPLAYNAME, old, getDisplayName());
	}

	/**
	 * Den virtuelle modulen i MIPSS Studio denne plugin representerer
	 * 
	 * @param modul
	 */
	public void setModul(Modul modul) {
		String old = getDisplayName();
		String oldModuleName = getModuleName();
		this.modul = modul;
		props.firePropertyChange(FIELD_DISPLAYNAME, old, getDisplayName());
		props.firePropertyChange(FIELD_MODULENAME, oldModuleName, getModuleName());
	}

	/**
	 * Angir om plugin skal startes i bakgrunn eller ikke
	 * 
	 * @param startInBackground
	 */
	public void setStartInBackground(boolean startInBackground) {
		this.startInBackground = startInBackground;
	}

	/**
	 * Statusen denne plugin har
	 * 
	 * @param status
	 */
	public void setStatus(MipssPlugin.Status status) {
		this.status = status;
	}
	
	/**
	 * Tilgangspunktet som startet plugin
	 * 
	 * @param tilgangspunkt
	 */
	public void setTilgangspunkt(Tilgangspunkt tilgangspunkt) {
		String old = getDisplayName();
		this.tilgangspunkt = tilgangspunkt;
		props.firePropertyChange(FIELD_DISPLAYNAME, old, getDisplayName());
	}
	
	/**
	 * Alle plugins må ta standpunkt til hvordan de skal avsluttes
	 * 
	 */
	public abstract void shutdown();

	/**
	 * @see java.lang.Object#toString()
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return new ToStringBuilder((Object) this).append("name", (modul == null ? "null" : modul.getNavn())).toString();
	}

	/**
	 * Statuser en plugin kan ha under kjøring
	 * 
	 */
	public enum Status {
		CACHED, NOT_STARTED, RUNNING, STARTING, STOPPING
	}

    /**
     * For test purposes
     * @param loader
     */
    public void setLoader(IMipssStudioLoader loader) {
        this.loader = loader;
    }
}
