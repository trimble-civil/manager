package no.mesta.mipss.plugin;

import java.io.Serializable;

import javax.swing.JComponent;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.ui.dialogue.MipssDialogue;


/**
 * Plugin interface for MIPSS Studio Loaderen
 * 
 * Hvis en plugin har felter som ikke kan serialiseres må de merkes som
 * transiente felter!
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface IMipssPlugin extends Serializable{

    /**
     * Denne kalles på plugin av rammeverket når den skal fjernes fra minnet.
     * Typisk er dette på plugins som ikke er chacheable og brukeren har valgt
     * å åpne en ny plugin.
     * 
     * Operasjoner plugin da kan gjøre er å frigjøre resursser
     */
    public void shutdown();
    
    /**
     * Denne vil bli kjørt av MIPSS Studio Loaderen hvis den fastslår at plugin
     * bør avsluttes.
     * 
     * Dette vil da gi plugin en mulighet til å avgjøre om den er klar for det.
     * Om nødvendig vil kan den kommunisere med brukeren vha av dialog
     * komponentet som sendes inn.
     * 
     * Ved retur fra metoden vil MIPSS Studio Loader undersøke om brukeren har
     * blitt kommunisert med, og hvis brukeren ønsker å avbryte nedstengingen
     * av plugin vil loaderen overholde det.

     * @param dialog
     */
    public void readyForShutdown(MipssDialogue dialog);
    
    /**
     * Kalles når brukeren ønsker å laste plugin
     * 
     * Det er forventet at plugin i slutten av init kaller registerReadyPlugin
     * når init er ferdig. Kravet er da at plugin GUI er klart og responsivt 
     * til brukerens disposisjon
     * 
     * @param loader object for callbacks til loaderen fra plugin
     */
    public void init(IMipssStudioLoader loader);
    
    /**
     * Brukes av MIPSS Studio Loader for å hente GUI til en plugin.
     * 
     * @return
     */
    public JComponent getModuleGUI();
    
    /**
     * Angir om en plugin skal taes vare på når brukeren klikker seg bort fra
     * den.
     * 
     * Hvis plugin returnerer true vil den caches og hurtig kunne vises igjen
     * når brukeren klikker seg tilbake i menyen til pluginen
     * 
     * Hvis plugin returnerer false, vil plugin motta readyForShutdown og 
     * shutdown hvis MIPSS Studio Loader finner ut at en plugin ikke lenger skal
     * vises i GUI.
     * 
     * @return
     */
    public Boolean isCacheable();
    
    /**
     * Mottar og behandler en melding
     * 
     * @param message
     */
    public void receiveMessage(PluginMessage<?,?> message);
}
