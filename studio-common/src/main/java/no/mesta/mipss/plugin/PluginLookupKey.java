package no.mesta.mipss.plugin;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Søkenøkkel for å finne en kjørende plugin
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PluginLookupKey {
	private Class<? extends MipssPlugin> pluginClass = null;
    private Object[] arguments = null;
    
    /**
     * Konstruktør
     * 
     * @param pluginClass
     * @param arguments
     */
	public PluginLookupKey(Class<? extends MipssPlugin> pluginClass, Object[] arguments) {
        this.pluginClass = pluginClass;
        this.arguments = arguments;
    }

    /**
     * Plugin klassen
     * 
     * @return
     */
	public Class<? extends MipssPlugin> getPluginClass() {
        return pluginClass;
    }

    /**
     * Argumentene
     * 
     * @return
     */
    public Object[] getArguments() {
        return arguments;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pluginClass).append(arguments).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof PluginLookupKey)) {return false;}
        
        PluginLookupKey other = (PluginLookupKey) o;
        
        return new EqualsBuilder().append(pluginClass, other.getPluginClass()).append(arguments, other.getArguments()).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("pluginClass", pluginClass).
            append("arguments", arguments).
            toString();
    }
}
