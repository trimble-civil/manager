package no.mesta.mipss.ldap;

/**
 * Wrapper Exceptions som oppstår når det kommuniseres med LDAP/AD
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class LdapException extends Exception{

    /**
     * Konstruktør for Exception
     * 
     * @param message
     * @param cause
     */
    public LdapException(String message, Throwable cause) {
        super(message, cause);
    }
}
