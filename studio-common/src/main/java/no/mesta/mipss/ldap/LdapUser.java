package no.mesta.mipss.ldap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Wrapper en bruker som er hentet ut fra LDAP/AD
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class LdapUser {
    private String commonName = null;
    private String sAMAccountName = null;
    private String mail;
    private String mobile;
    
    /**
     * Konstruktør
     * 
     */
    public LdapUser() {
    }
    
    /**
     * Hurtig konstruktør
     * 
     * @param sAMAccountName
     * @param commonName
     */
    public LdapUser(String sAMAccountName, String commonName) {
        this.sAMAccountName = sAMAccountName;
        this.commonName = commonName;
    }

    public LdapUser(String sAMAccountName, String commonName, String mail, String mobile) {
        this.sAMAccountName = sAMAccountName;
        this.commonName = commonName;
		this.setMail(mail);
		this.setMobile(mobile);
    }
    /**
     * Det naturlige navnet til brukeren
     * 
     * @param commonName
     */
    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    /**
     * Det naturlige navnet til brukeren
     * 
     * @return
     */
    public String getCommonName() {
        return commonName;
    }

    /**
     * Brukernavnet i AD, microsoft spesifikt
     * 
     * @param sAMAccountName
     * @see <a href="http://msdn.microsoft.com/en-us/library/ms679635(VS.85).aspx">MSDN om SAM Account Name</a>
     */
    public void setSAMAccountName(String sAMAccountName) {
        this.sAMAccountName = sAMAccountName;
    }

    /**
     * Brukernavnet i AD, microsoft spesifikt
     * 
     * @return
     * @see <a href="http://msdn.microsoft.com/en-us/library/ms679635(VS.85).aspx">MSDN om SAM Account Name</a>
     */
    public String getSAMAccountName() {
        return sAMAccountName;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof LdapUser)) {return false;}
        if(o == this) {return true;}
        
        LdapUser other = (LdapUser) o;
        return new EqualsBuilder().
            append(sAMAccountName, other.getSAMAccountName()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(39,65).
            append(sAMAccountName).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return "LdapUser[" + sAMAccountName + ",CN=" + commonName + "]"; 
    }

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMail() {
		return mail;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobile() {
		return mobile;
	}
}
