package no.mesta.mipss.ldap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import no.mesta.mipss.core.KonfigparamPreferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>Brukes til å hente ut listen over brukere i LDAP/AD</p>
 * 
 * <p>Klassen konfigureres i ldapaccess.properties. Der settes search base og search
 * filter som styrer hvilke subjekter det søkes på.</p>
 * 
 * <p>Property filen styrer også hvilken LDAP/AD det gåes mot.</p>
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @see <a href="http://java.sun.com/products/jndi/tutorial/ldap/">Sun LDAP tutorial</a>
 */
public class LdapUserFactory {
	private static final Logger logger = LoggerFactory.getLogger(LdapUserFactory.class);
    private String searchBase;
    private String searchBaseX;
    private String searchFilter;
    
    private static LdapUserFactory access;
    private static LdapUserFactory searcher;
    private Map<String, LdapUser> users = new HashMap<String,LdapUser>();
    private PropertyResourceBundle bundle;
    
    /**
     * <p>Klassen er en factory.<br> 
     * Hent ut brukerne slik:<br>
     * <code><pre>List&lt;LdapUser&gt; users = LdapUserFactory.getInstance().getUsers();<pre></code>
     * </p>
     */
    private LdapUserFactory() {
    	searchFilter = KonfigparamPreferences.getInstance().hentEnForApp("mipss.studio.login", "searchFilter").getVerdi();
    	searchBaseX = KonfigparamPreferences.getInstance().hentEnForApp("mipss.studio.login", "searchBaseX").getVerdi();
    	searchBase = KonfigparamPreferences.getInstance().hentEnForApp("mipss.studio.login", "searchBase").getVerdi();
    }
    
    public LdapUserFactory(String searchFilter) {
    	searchBaseX = KonfigparamPreferences.getInstance().hentEnForApp("mipss.studio.login", "searchBaseX").getVerdi();
    	searchBase = KonfigparamPreferences.getInstance().hentEnForApp("mipss.studio.login", "searchBase").getVerdi();
    	this.searchFilter = searchFilter;
	}

	/**
     * Henter instancen av fabrikken.
     * 
     * @return
     * @throws LdapException kastes hvis det oppstår problemer i ldap kommunikasjonen
     */
    public static LdapUserFactory getInstance() throws LdapException {
        synchronized(LdapUserFactory.class) {
            if(access == null) {
                access = new LdapUserFactory();
                access.init();
            }
        }
        
        return access;
    }
    
    public static LdapUserFactory getInstanceForSearch() throws LdapException {
    	if (searcher==null){
    		searcher = new LdapUserFactory("(&(objectClass=user)(&(anr=<user>)))");
    		searcher.initForSearch();
    	}
    	return searcher;
    }
    
    /**
     * Initierer factory.
     * 
     * @throws LdapException
     */
    private void init() throws LdapException{
        LdapContext ctx = null;
        try {
            bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("ldapaccess");
            ctx = getLdapContext();
            search(ctx);
        } finally {
            try {
                ctx.close();
            } catch (Exception e) {
                // Ikke gjør noe;
            }
        }
    }
    
    private void initForSearch(){
    	bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("ldapaccess");
    }

    /**
     * Lager en LdapContext fra properties.
     * 
     * @return
     * @throws LdapException
     */
    public LdapContext  getLdapContext() throws LdapException {
        Hashtable<String, String> env = new Hashtable<String, String>(11);
        env.put(Context.INITIAL_CONTEXT_FACTORY, bundle.getString(Context.INITIAL_CONTEXT_FACTORY));
        env.put(Context.PROVIDER_URL, bundle.getString(Context.PROVIDER_URL));
        env.put(Context.SECURITY_PRINCIPAL, bundle.getString(Context.SECURITY_PRINCIPAL));
        env.put(Context.SECURITY_CREDENTIALS, bundle.getString(Context.SECURITY_CREDENTIALS));

         LdapContext ctx = null;
        // Lag konteksten
        try {
            ctx = new InitialLdapContext(env,null);
        }
        catch (NamingException e) {
            System.err.println(env);
            throw new LdapException("Could not initiate LdapContext", e);
        }

        Control[] ctls = null;
        // Hent ut svar sidevis
        try {
             ctls = new Control[]{new PagedResultsControl(10, Control.CRITICAL)};
        }
        catch (IOException e) {
            throw new LdapException("Could not initiate PagedResultsControl[]", e);
        }
        
        try {
            ctx.setRequestControls(ctls);
        }
        catch (NamingException e) {
            throw new LdapException("Could not set PagedResultsControl[]", e);
        }

        return ctx;
    }

    public List<LdapUser> search(String navn) throws LdapException, NamingException, IOException{
//    	String filter = "(objectClass=user)&(cn=<user>)".replaceAll("<user>", navn);//"(SAMAccountName=<user>)".replaceAll("<user>", navn);
    	String filter = "(&(objectClass=user)(cn=*<user>*))".replaceAll("<user>", navn); 
    	LdapContext ldapContext = getLdapContext();
		byte[] cookie;
		SearchControls ctls = new SearchControls();
		ctls. setReturningObjFlag (true);
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		List<LdapUser> ldapUsers = new ArrayList<LdapUser>();
		do{
		    NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, filter, ctls);
		    while (answer.hasMoreElements()) {
		        SearchResult sr = answer.next();
		        
		        Attributes atrs = sr.getAttributes();
		        String cn = (String) ((Attribute) atrs.get("cn")).get();
		        String uid = (String) ((Attribute) atrs.get("samaccountname")).get();
		        Attribute mailAttr = atrs.get("mail");
		        String mail = null;
		        if (mailAttr!=null){
		        	mail = (String) ((Attribute) atrs.get("mail")).get();	
		        }
		        Attribute mobileAttr = atrs.get("mobile");
		        String mobile = null;
		        if (mobileAttr!=null){
		        	mobile = (String) ((Attribute) atrs.get("mobile")).get();
		        }
		        
		        
		        LdapUser user = new LdapUser(uid, cn, mail, mobile);
		        ldapUsers.add(user);
		    }
		    // examine the response controls
		    cookie = parseControls(ldapContext.getResponseControls());
		    // pass the cookie back to the server for the next page
		    ldapContext.setRequestControls(new Control[]{new PagedResultsControl(10, cookie, Control.CRITICAL) });

		}while ((cookie != null) && (cookie.length != 0));
		
		ldapContext.close();
    	return ldapUsers;
    }
    /**
     * Utfører søket som er spesifisert i ldapaccess.properties
     * 
     * @param ctx
     * @throws LdapException
     */
    private void search(LdapContext ctx) throws LdapException{
        try {
			leggTilBrukereFraAD(ctx, searchBase);
//            leggTilBrukereFraAD(ctx, searchBaseX);
        } catch (NamingException e) {
            throw new LdapException("An error occurred during search", e);
        } catch (IOException e) {
            throw new LdapException("An error occurred during search paging", e);
        }
    }

    
	private void leggTilBrukereFraAD(LdapContext ctx, String searchBase) throws NamingException,IOException {
		byte[] cookie;
		SearchControls ctls = new SearchControls();
		ctls. setReturningObjFlag (true);
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		do{
		    NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, ctls);
		    while (answer.hasMoreElements()) {
		        SearchResult sr = answer.next();
		        
		        Attributes atrs = sr.getAttributes();
		        String cn = (String) ((Attribute) atrs.get("cn")).get();
		        String uid = (String) ((Attribute) atrs.get("samaccountname")).get();
		        
		        LdapUser user = new LdapUser(uid, cn);
		        users.put(user.getSAMAccountName(),user);
		    }
		    // examine the response controls
		    cookie = parseControls(ctx.getResponseControls());

		    // pass the cookie back to the server for the next page
		    ctx.setRequestControls(new Control[]{new PagedResultsControl(10, cookie, Control.CRITICAL) });

		}while ((cookie != null) && (cookie.length != 0));
	}

    /**
     * Benyttes til paginering av resultatlisten fra ldap/ad
     * 
     * @param controls
     * @return
     * @throws NamingException
     */
    private byte[] parseControls(Control[] controls) throws NamingException {
        byte[] cookie = null;

        if (controls != null) {

            for (int i = 0; i < controls.length; i++) {
                if (controls[i] instanceof PagedResultsResponseControl) {
                    PagedResultsResponseControl prrc = 
                        (PagedResultsResponseControl)controls[i];
                    cookie = prrc.getCookie();
                    // Neste side er truffet
                }
            }
        }
        return (cookie == null) ? new byte[0] : cookie;
    }

    /**
     * Listen av brukere som ble funnet i ldap
     * 
     * @return
     */
    public Map<String, LdapUser> getUsers() {
        return users;
    }
    
    // For debug only
    /**
     * Kun for debugging
     * 
     * @param arg
     */
    public static void main(String[] arg) {
        try {
            LdapUserFactory a = LdapUserFactory.getInstance();
            Map<String, LdapUser> users = a.getUsers();
            logger.info("Users: {}", users);
            logger.info("Fant: " + users.size() + " brukere");
        }
        catch (LdapException e) {
        	logger.error("Fikk feil: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
