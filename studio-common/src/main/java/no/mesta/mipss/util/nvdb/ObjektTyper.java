package no.mesta.mipss.util.nvdb;


public class ObjektTyper {
	private int id;
	private int antall;
	private Filter[] filter;
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setAntall(int antall) {
		this.antall = antall;
	}
	public int getAntall() {
		return antall;
	}
	public void setFilter(Filter[] filter) {
		this.filter = filter;
	}
	public Filter[] getFilter() {
		return filter;
	}

}
