package no.mesta.mipss.util.nvdb;


public class Kriterie {
	private Lokasjon lokasjon;
	private ObjektTyper[] objektTyper;

	public void setLokasjon(Lokasjon lokasjon) {
		this.lokasjon = lokasjon;
	}

	public Lokasjon getLokasjon() {
		return lokasjon;
	}

	public void setObjektTyper(ObjektTyper[] objektTyper) {
		this.objektTyper = objektTyper;
	}
	public ObjektTyper[] getObjektTyper() {
		return objektTyper;
	}
}
