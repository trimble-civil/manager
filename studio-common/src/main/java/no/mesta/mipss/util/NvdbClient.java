package no.mesta.mipss.util;

import java.awt.geom.Point2D;
import java.net.URL;
import java.net.URLConnection;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import no.mesta.mipss.util.nvdb.Filter;
import no.mesta.mipss.util.nvdb.Kriterie;
import no.mesta.mipss.util.nvdb.Lokasjon;
import no.mesta.mipss.util.nvdb.ObjektTyper;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

public class NvdbClient {

	private static final String NVDB_API_ROOT = "https://www.vegvesen.no/nvdb/api";
	private static final String VEGREFERANSE = "/vegreferanse";//?x={x}&y={y}&srid=UTM33
	private static final String SOK = "/sok";//?kriterie={kriterie}
	
	private static final String VEGKATEGORI = "Vegkategori";
	private static final String VEGNUMMER = "Vegnummer";
	private static final String HOVEDPARSELL = "Hovedparsell";
	private static final String VEG_REFERANSE = "vegReferanse";
	private static final String EGENSKAPER = "egenskaper";
	private static final String NAVN = "navn";
	private static final String VERDI = "verdi";
	private static final String LOKASJON = "lokasjon";
	private static final String KOMMUNE = "kommune";
	private static final String FYLKE = "fylke";
	private static final String NUMMER = "nummer";
	
	private static final String METER_VERDI = "meterVerdi";
	public static void sokVeireferanse(double north, double east) throws Exception{
		StringBuilder sb = new StringBuilder(NVDB_API_ROOT).append(VEGREFERANSE)
		.append("?x=").append(String.valueOf(east))
		.append("&y=").append(String.valueOf(north))
		.append("&srid=UTM33");
		
		URL url = new URL(sb.toString());
		URLConnection con = url.openConnection();
		con.setRequestProperty("Accept", "application/vnd.vegvesen.nvdb-v1+json");
		String body = IOUtils.toString(con.getInputStream());
		JSONObject json = (JSONObject)JSONSerializer.toJSON( body );
		
		System.out.println(json.toString(2));
		JSONObject vegref =(JSONObject)json.get(VEG_REFERANSE);
		JSONArray egenskaper =(JSONArray)vegref.get(EGENSKAPER);
		
		for (int i=0;i<egenskaper.size();i++){
			JSONObject egenskap = (JSONObject)egenskaper.get(i);
			if (VEGKATEGORI.equals(egenskap.getString(NAVN))){
				System.out.println(egenskap.get(VERDI));
			}
			if (VEGNUMMER.equals(egenskap.getString(NAVN))){
				System.out.println(egenskap.get(VERDI));
			}
			if (HOVEDPARSELL.equals(egenskap.getString(NAVN))){
				System.out.println(egenskap.get(VERDI));
			}
		}
		Object o = json.get(METER_VERDI);
		System.out.println(json.get(METER_VERDI));
		
		JSONObject lokasjon= (JSONObject)vegref.get(LOKASJON);
		JSONObject kommune = (JSONObject)lokasjon.get(KOMMUNE);
		JSONObject fylke = (JSONObject)lokasjon.get(FYLKE);
		Object oo = kommune.get(NUMMER);
		Object ooo = fylke.get(NUMMER);
		System.out.println(kommune.get(NUMMER)+" "+fylke.get(NUMMER));
		String snappedString = (String)json.get("punktPaVegReferanseLinjeUTM33");
		int start = snappedString.indexOf('(');
		int end = snappedString.lastIndexOf(')');
		String[] en = snappedString.substring(start+1, end).split(" ");
		System.out.println(en[0]+","+en[1]+"...");
		System.out.println(snappedString);
		
		
//		ClientRequest req = new ClientRequest(NVDB_API_ROOT+VEGREFERANSE);
//        
//		req.queryParameter("x", ""+east)
//		   .queryParameter("y", ""+north)
//		   .queryParameter("srid", "UTM33");
//		req.accept("application/vnd.vegvesen.nvdb-v1+json");
//		
//        ClientResponse<String> res = req.get(String.class);
//        JSONObject json = (JSONObject)JSONSerializer.toJSON( res.getEntity() );
//        System.out.println(json.toString(2));
//        System.out.println(res.getEntity());
	}
	
	public static void sokFartsgrense(double north, double east) throws Exception{
		//1. beregn areaId
		//2. hent areaId fra cache
		//3. id==null?hent veireferanse
		//4. hent vei
		//5. legg til areaId
		
        String vei = "EV18";
        
        JSONObject lokasjon = new JSONObject();
        lokasjon.put("kommune", new int[]{627});
        
        JSONObject veiref = new JSONObject();
        veiref.put("type", "veireferanse");
        veiref.put("operator", "ERLIK");
        JSONArray veiarray = new JSONArray();
        veiarray.add(vei);
        veiref.put("verdi", veiarray);
        
        JSONArray filter = new JSONArray();
        filter.add(veiref);
        
        JSONObject fartsgrense = new JSONObject();
        fartsgrense.put("id", 105);
        fartsgrense.put("antall", 10);
        fartsgrense.put("filter", filter);
        
        JSONArray objektTyper = new JSONArray();
        objektTyper.add(fartsgrense);
        
        JSONObject root = new JSONObject();
        root.element("lokasjon", lokasjon);
        root.put("objektTyper", objektTyper);
        
        System.out.println(root.toString(2));
        
        
//        Filter f = new Filter();
//        f.setType("vegreferanse");
//        f.setOperator("ERLIK");
//        f.setVerdi(new String[]{vei});
//        
//        ObjektTyper fartsgrense = new ObjektTyper();
//        fartsgrense.setId(105);
//        fartsgrense.setAntall(100);
//        fartsgrense.setFilter(new Filter[]{f});
//        ObjektTyper stikkrenne = new ObjektTyper();
//        stikkrenne.setId(79);
//        stikkrenne.setAntall(100);
//        
//        Lokasjon l = new Lokasjon();
//        l.setKommune(new int[]{220});
////        l.setBbox("247000,6627000,248000,6628000");
////        l.setSrid("UTM33");
//        
//        
//        Kriterie k = new Kriterie();
//        k.setLokasjon(l);
//        k.setObjektTyper(new ObjektTyper[]{fartsgrense, stikkrenne});
//        
//        JSONObject kriterie = JSONObject.fromObject(k);
//        System.out.println(kriterie.toString(2));
//        if (true){
//        	System.exit(0);
//        }
        
		ClientRequest req = new ClientRequest(NVDB_API_ROOT+SOK);
		req.queryParameter("kriterie",root.toString());
		req.accept("application/vnd.vegvesen.nvdb-v1+json");
		ClientResponse<String> res = req.get(String.class);
		JSONObject json = (JSONObject)JSONSerializer.toJSON( res.getEntity() );
        JSONArray results = (JSONArray)json.get("resultater");
        for (int i=0;i<results.size();i++){
        	JSONObject r = (JSONObject)results.get(i);
        	JSONArray ar = (JSONArray)r.get("vegObjekter");
        	for (int j=0;j<ar.size();j++){
        		JSONObject veg = (JSONObject)ar.get(j);
        		JSONArray egenskaper = (JSONArray)veg.get("egenskaper");
        		for (int kk=0;kk<egenskaper.size();kk++){
        			JSONObject egenskap = (JSONObject)egenskaper.get(kk);
        			JSONObject verdi = (JSONObject)egenskap.get("enumVerdi");
        			if (verdi!=null)
        			System.out.println(verdi.get("verdi"));
        		}
        		JSONObject lokasjon2 = (JSONObject)veg.get("lokasjon");
        		JSONArray vegrefs = (JSONArray)lokasjon2.get("vegReferanser");
        		for (int kk=0;kk<vegrefs.size();kk++){
        			JSONObject vegref = (JSONObject)vegrefs.get(kk);
        			System.out.println(
        					vegref.get("fylke").toString()+"/"+
        					vegref.get("kommune").toString()+" "+
        					vegref.get("kategori").toString()+
        					vegref.get("status").toString()+
        					vegref.get("nummer").toString()+" hp"+
        					vegref.get("hp").toString()+" "+
        					vegref.get("fraMeter").toString()+"-"+
        					vegref.get("tilMeter"));
        		}
        		String geometri = (String)lokasjon2.get("geometriUtm33");
        		int firstCoord = geometri.indexOf('(')+1;
        		int lastCoord = geometri.indexOf(')');
        		String coordStr = geometri.substring(firstCoord, lastCoord);
        		String[] coords = coordStr.split(" ");
        		for (String coord :coords){
        			
        		}
        	}
        }
        
        System.out.println(json.toString(2));

	}
	
	public static Point2D nearestPointOnLine(double ax, double ay, double bx, double by, double px, double py,
	        boolean clampToSegment) {
	    // Thanks StackOverflow!
	    // http://stackoverflow.com/questions/1459368/snap-point-to-a-line-java
	        

	    double apx = px - ax;
	    double apy = py - ay;
	    double abx = bx - ax;
	    double aby = by - ay;

	    double ab2 = abx * abx + aby * aby;
	    double ap_ab = apx * abx + apy * aby;
	    double t = ap_ab / ab2;
	    if (clampToSegment) {
	        if (t < 0) {
	            t = 0;
	        } else if (t > 1) {
	            t = 1;
	        }
	    }
	    Point2D dest = new Point2D.Double();
	    dest.setLocation(ax + abx * t, ay + aby * t);
	    return dest;
	}
	public static void main(String[] args) throws Exception {
		sokFartsgrense(6643322, 246769);
//		sokVeireferanse(6643322, 246769);
//		Point2D p = new Point2D.Double(247083, 6627045);
//		double[][] doubles = new double[][]{{247083, 6627051}, {247077, 6627039}, {247076, 6627027}, {247079,6627008}, {247082,6626991}, {247086,6626972},
//				{247087,6626957},{247088,6626940},{247087,6626928},{247084, 6626914},{247079,6626903},{247071,6626888}, {247063,6626873}, {247059,6626868}, 
//				{247056,6626864}, {247053,6626861}, {247051,6626857}};
//		
//		double px = -1;
//		double py = -1;
//		double shortest = -1;
//		int index = 0;
//		for (int i=0;i<doubles.length;i++){
//			double x = doubles[i][0];
//			double y = doubles[i][1];
//			if (px==-1){
//				px=x;
//				py=y;
//				continue;
//			}
//			Point2D po = nearestPointOnLine(px, py, x, y, p.getX(), p.getY(), true);
//			double dist = p.distance(po);
//			System.out.println("distance="+dist+" "+po);
//			if (shortest==-1||dist<shortest){
//				shortest = dist;
//				index=i;
//			}
//		}
//		System.out.println("shortest found at index "+index+" distance:"+shortest);
		
//		Point2D np1 = nearestPointOnLine(247083, 6627051, 247077, 6627039, p.getX(), p.getY(), true);
//		Point2D np2 = nearestPointOnLine(247077, 6627039, 247076, 6627027, p.getX(), p.getY(), true);
//		Point2D np3 = nearestPointOnLine(247077, 6627039, 247076, 6627027, p.getX(), p.getY(), true);
//		System.out.println(p.distance(np1)+" "+p.distance(np2));
//		System.out.println(np1+" "+np2);
		
	}
	
}
