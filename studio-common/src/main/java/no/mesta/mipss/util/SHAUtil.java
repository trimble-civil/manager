package no.mesta.mipss.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHAUtil {
	private static final String ALGORITHM = "SHA-256";
	/**
	 * Beregner en SHA hash
	 *   
	 * @param text verdien det skal beregnes en hash fra
	 * @return en byte array med SHA-256 data
	 */
	public byte[] toSHABytes(String text){
		try {
			MessageDigest md = MessageDigest.getInstance(ALGORITHM);
			md.update(text.getBytes());
			return md.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Konverterer en byte array til HEX
	 * 
	 * @param data bytene som skal konverteres
	 * @return HEX streng representasjon av byte arrayen
	 */
	public String bytesToHEX(byte[] data){
		StringBuffer sb = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
         sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
	}
	
	/**
	 * Beregner en SHA hash
	 * 
	 * @param text verdien det skal bereges hash fra
	 * @return HEX-verdien av hashen
	 */
	public String toSHA(String text){
		byte[] dataBytes = toSHABytes(text);
		return bytesToHEX(dataBytes);
	}
	
	public static void main(String[] args) {
		SHAUtil u = new SHAUtil();
		String sha = u.toSHA("!");
		System.out.println(sha);
		
	}
}
