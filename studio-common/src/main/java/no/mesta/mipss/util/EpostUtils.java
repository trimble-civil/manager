package no.mesta.mipss.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.KonfigparamPreferences;

public class EpostUtils {
	
	private static KonfigparamPreferences konfig = KonfigparamPreferences.getInstance();

    /**
     * Verify the specified text. 
     *
     * @param textToVerify The text that is to be verified.
     * @return boolean true when valid, false when invalid
     *
     */        
    public static boolean verifyEpost(String epostString) {
    	//valider epostadressen, validerer ikke om eposten har riktig topdomene (.no, .org etc)
        Pattern p = Pattern.compile("([a-zA-Z\\-/\\d_\\\\\\.]+@([a-zA-Z\\-\\d_\\\\\\.]+)"+
        							"(\\.[a-zA-Z\\d]{2,4}))|([a-zA-Z/\\d_\\\\\\.]+@)"+
									"[\\d]{1,3}\\.[\\d]{1,3}\\.[\\d]{1,3}\\.[\\d]{1,3}");
        Matcher m = p.matcher(epostString);
        if (m.matches()) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public static String getSupportEpostAdresse(){
    	String epostAdresse = konfig.hentEnForApp("studio.mipss", "supportEpost").getVerdi();
    	
    	if (epostAdresse == null || epostAdresse.equals("")) {
			return PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText").getGuiString("defaultEmail");
		}
    	
    	return epostAdresse;
    }
}
