package no.mesta.mipss.util.vinterdriftsklasser;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.rapportfunksjoner.Vinterdrift;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class VinterdriftsklasserRapport {

    private static final String RAPPORT_TEMPLATE = "vinterdriftsrapport";

    private File file;
    private XSSFWorkbook workbook;
    private ExcelPOIHelper excel;

    public VinterdriftsklasserRapport(Driftkontrakt kontrakt, List<Vinterdrift> data, Date fraDato, Date tilDato){
        String periodeText = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT) + " - " + MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
        String rappDatoText = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

        Calendar c = Calendar.getInstance();
        c.setTime(fraDato);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("kontrakt", kontrakt);
        map.put("periode", periodeText);
        map.put("dato", rappDatoText);
        map.put("vinterdata", data);

        file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
        workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map);
        excel = new ExcelPOIHelper(workbook);
    }

    /**
     * Lukker arbeidsboken og åpner den i Excel
     */
    public void finalizeAndOpen(){
        RapportHelper.finalizeAndOpen(workbook, file);
    }

}