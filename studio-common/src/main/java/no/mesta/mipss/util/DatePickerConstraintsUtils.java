package no.mesta.mipss.util;

import java.util.Date;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.persistence.Clock;

import org.jdesktop.swingx.JXDatePicker;

public class DatePickerConstraintsUtils {
	private static KonfigparamPreferences konfig = KonfigparamPreferences.getInstance();
	private static final String KONFIG_MAX_DAYS = "maxDayForReportHistory";
	private static final String KONFIG_APP = "studio.mipss";
	
	/**
	 * Setter JXDatePicker til å kun tillate valg av datoer mellom [studio.mipss|maxDayForReportHistory] og til i dag
	 * @param loader
	 * @param datePickers
	 */
	public static void setNotBeforeDate(IMipssStudioLoader loader, JXDatePicker... datePickers){
		if (!UserUtils.isAdmin(loader.getLoggedOnUser(true))){
			String antallDager = konfig.hentEnForApp(KONFIG_APP, KONFIG_MAX_DAYS).getVerdi();
			Integer days = Integer.valueOf(antallDager);
			Date notBeforeDate = CalendarUtil.getDaysAgo(Clock.now(), days);
			
			for (JXDatePicker p:datePickers){
				p.getMonthView().setLowerBound(notBeforeDate);
				p.getMonthView().setUpperBound(Clock.now());
			}
		}
	}
	
	/**
	 * Setter JXDatePicker til å kun tillate valg av datoer mellom @code{notBeforeDate} til til i dag
	 * @param notBeforeDate
	 * @param datePickers
	 */
	public static void setNotBeforeDate(Date notBeforeDate, JXDatePicker... datePickers){
			for (JXDatePicker p:datePickers){
				p.getMonthView().setLowerBound(notBeforeDate);
				p.getMonthView().setUpperBound(Clock.now());
			}
	}
}
