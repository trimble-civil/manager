package no.mesta.mipss.util.ukjentproduksjon;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.UkjentStroprodukt;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;

public class UkjentStroproduktRapport {

	private static final String RAPPORT_TEMPLATE = "ukjent_stroprodukt";
	private File file;
	private XSSFWorkbook workbook;
	private ExcelPOIHelper excel;
	
	public UkjentStroproduktRapport(List<UkjentStroprodukt> data, Date fraDato, Date tilDato, Long kjoretoyId) {

		String periodeText = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.DATE_FORMAT)+" - "+MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.DATE_FORMAT);
		String rappDatoText = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ukjentStroproduktData", data);
		map.put("dato", rappDatoText);
		map.put("periode", periodeText);
		
		file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
		workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map);
		excel = new ExcelPOIHelper(workbook);
	}
	
	/**
	 * Lukker arbeidsboken og åpner den i Excel
	 */
	public void finalizeAndOpen(){
		RapportHelper.finalizeAndOpen(workbook, file);
	}
}