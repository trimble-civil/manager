package no.mesta.mipss.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Window;
import java.io.File;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.ResourceBundle;

import javax.ejb.EJBException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.text.JTextComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre_Status;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrelogg;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;


public class KjoretoyUtils {
    public static enum DialogElection {LAGRE, FORKAST, FORTSETT, JA, NEI};
    private static MipssKjoretoy mipssKjoretoyBean;
    private static MaintenanceContract maintenanceContract;
    
    private static final ResourceBundle bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
    /**
     * verktøysklasse kun for statiske metoder.
     */
    private KjoretoyUtils() {
    }
    
    public static String stringContentOf(Object content) {
        if (content == null) {
            return "";
        }
        if (content instanceof String) {
            return (String) content;
        }
        else {
            return String.valueOf(content);
        }
    }
    
    /**
     * Formatterer en string med formateringskoder (%x) med de leverte parameternerne.
     * Tar høyde for at eventuelle endringer i properties filer bryter med forventede
     * formateringskoder.
     * @param format er strengen med formateringskoder.
     * @param params verdiene som skal inn i den ferdig formatterte teksten.
     * @return den ferdigformatterte teksten, eller uformatert tekst hvis formatet var feil 
     * eller tom streng hvis formatet var null.
     */
    public static String formatString(String format, Object... params) {
        String result = null;
        try {
            result = String.format(format, params);
        }
        catch (IllegalFormatException ife) {
            result = format;
        }
        catch (NullPointerException npe) {
            result = "";
        }
        return result;
    }
    
    public static ResourceBundle getBundle () {
        return bundle;
    }
    
    /**
     * Returnerer teksten som tilsvarer key i ResourceBundle.
     * @param key
     * @return
     */
    public static String getPropertyString(String key) {
    	return bundle.getString(key);
    }
    
    /**
     * Endrer tekstfremvisningen av den leverte JLbel til Bold uten å endre
     * andre tekstatributter.
     * @param label
     */
    public static void setBold(JComponent comp) {
        comp.setFont(new Font(comp.getFont().getFamily(), Font.BOLD, comp.getFont().getSize()));
    }
    
    /**
     * Presenterer brukeren et dialogvindu som viser en informativ tekst
     * etter en Exception.
     * Uniformerer feilmeldingen for mer spesifiserte fatale feil.
     * @param e
     */
    public static void showException(Throwable e, String infoMessage) {
    	/*
        if(e instanceof UndeclaredExceptionTypeException) {
            displayExceptionDialogBox(bundle.getString(infoMessage + '\n' + "errormessage.wrongVersion."));
        }
        else */
//    	if (e instanceof EJBException) {
//            displayExceptionDialogBox(infoMessage + '\n' + bundle.getString("errormessage.noDbConnection"));
//        }
//        else {
//            displayExceptionDialogBox("Legg til håndtering av: \n" + infoMessage + '\n' + e.getClass());
//            e.printStackTrace();
//        }
        ErrorHandler.showError(e, "Feil", "En feil oppstod");
    }
    
    /**
     * Viser et modalt dialogvindu med tre faste valg.
     * @param parent
     * @param info
     * @param title
     * @return
     */
    public static DialogElection showLagreForkastDialog(Component caller, String info, String title) {
        Object[] options = {bundle.getString("dialog.button.lagre"),
                            bundle.getString("dialog.button.forkast"),
                            bundle.getString("dialog.button.fortsett")};
                            
        Component parent = getContainerWindow(caller);
        
        // mulige returverdier mappes direkte til DialogElection
        int n = JOptionPane.showOptionDialog(parent, info, title,
            JOptionPane.YES_NO_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[2]);
        
        switch(n) {
            case 0:
                return DialogElection.LAGRE;
            case 1:
            case -1:    // hvis dialogvinduet stenges fra tittle bar eller Escape
                return DialogElection.FORKAST;
            default:
                return DialogElection.FORTSETT;
        }
    }
    
    /**
     * Apner nytt dialogvindu med to faste alternativer: forkast eller fortsett.
     * @param parent
     * @param info
     * @param title
     * @return
     */
    public static DialogElection showForkastFortsettDialog(Component caller, String info, String title) {
    	Component parent = getContainerWindow(caller);
    	
        Object[] options = {bundle.getString("dialog.button.forkast"),
                            bundle.getString("dialog.button.fortsett")};
                            
        // mulige returverdier mappes direkte til DialogElection
        int n = JOptionPane.showOptionDialog(parent, info, title,
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[1]);
        
        switch(n) {
            case 0:
            case -1:    // hvis dialogvinduet stenges fra tittle bar eller Escape
                return DialogElection.FORKAST;
            default:
                return DialogElection.FORTSETT;
        }
    }
    
    /**
     * Viser et modalt dialogvindu med to alternativer (slett og forkast)
     * @param parent
     * @param info
     * @param title
     * @return
     */
    public static DialogElection showSlettForkastDialog(Component caller, String info, String title) {
        Object[] options = {bundle.getString("dialog.button.ja"),
                            bundle.getString("dialog.button.nei")};
                            
        Component parent = getContainerWindow(caller);
        
        int n = JOptionPane.showOptionDialog(parent, info, title,
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[1]);
        return (n == 0)? DialogElection.JA : DialogElection.NEI;              
    }

    /**
     * Presenterer brukeren et dialogvindu som viser den leverte meldingen.
     * For bruk for mer generelle Exception hvor kalleren vet hvorfor problemene oppstår.
     * @param message meldingen som presenters for brukeren.
     */
    public static void showException(String message) {
        displayExceptionDialogBox(message);
    }
    
    /**
     * Presenterer et dialogvindu med kun ett valg og informasjon om rettelsen brukeren 
     * må foreta.
     * @param info
     */
    public static void showCorectionInfo(String info) {
    	JOptionPane.showMessageDialog(null, info, "Vennigst korriger", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     * Viser dialiogvinduet med feilmeldingen.
     * @param message
     */
    private static void displayExceptionDialogBox(String message) {
        JOptionPane.showMessageDialog(null, message, "Uventet hendelse", JOptionPane.ERROR_MESSAGE);
    }
    
    public static String getDateFormat() {
        return "dd.MM.yyyy";
    }
    
    public static String getTimeFormat() {
        return "hh:mm";
    }
    
    /**
     * Initialiserer den leverte tabellen med felles brukeregenskaper.
     * Verktøysmetode som har to grunner: unngå kodeduplisering og - viktigst - 
     * få til en ensartet brukeropplevelse når det gjelder tabeller.
     * @param table
     */
    public static void initDefaultTableSettings (JTable table) {
        // sortering ++
        table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        table.setFillsViewportHeight(true);
        table.setAutoCreateRowSorter(true);

        // kun én linje kan velges av gangen
        table.setRowSelectionAllowed(true);
        table.setColumnSelectionAllowed(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        // felles look and feel
        table.getParent().setBackground(Color.WHITE);
        table.repaint();
    }
    
    /**
     * Setter den leverte componenten til å avvise redigering og focus.
     * Dette gir en bedre presentasjon av componenten.
     * @param c
     * @return
     */
    public static JTextComponent setReadOnly(JTextComponent c) {
        c.setEditable(false);
        c.setFocusable(false);
        return c;
    }

    /**
     * Instantierer sesjonsbønnen MipssKjoretoy kun én gang og returnerer den.
     * @return bønnen.
     */
	public static MipssKjoretoy getKjoretoyBean() {
		if(mipssKjoretoyBean != null) {
			return mipssKjoretoyBean;
		}
		mipssKjoretoyBean = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
		// create the JPA session bean
        return mipssKjoretoyBean;
	}
    
	public static MaintenanceContract getMaintenanceContractBean() {
		if(maintenanceContract != null) {
			return maintenanceContract;
		} else {
			maintenanceContract = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
			return maintenanceContract;
		}
	}
	
    /**
     * Går bakover i parent hierarkiet inntil den finner en parent som er {@code Window}.
     * @param containee
     * @return
     */
    private static Window getContainerWindow(Component containee) {
        Component parent = containee;

        while (!(parent instanceof Window)){
            if (parent.getParent() == null) {
                break;
            }
            parent = parent.getParent();
        }
        
        return (Window) parent;
    }
    
    /**
     * logger innholdet i det leverte kjøretøyet.
     * @param logger
     * @param mipssEntity
     */
	public static void debugKjoretoy(Logger logger, Kjoretoy mipssEntity) {
		logger.debug("------------Kjøretøy start------------------------");
		logger.debug("regnr        "  + mipssEntity.getRegnr());
		logger.debug("maskinnummer "  + mipssEntity.getMaskinnummer());
		logger.debug("ekst. navn   "  + mipssEntity.getEksterntNavn());
		logger.debug("nr. el. navn "  + mipssEntity.getNummerEllerNavn());
		logger.debug("fritekst     "  + mipssEntity.getFritekst());
		logger.debug("type         "  + mipssEntity.getKjoretoytype());
		logger.debug("modell       "  + mipssEntity.getKjoretoymodell());
		if(mipssEntity.getKjoretoymodell() != null) {
			logger.debug("år           "  + mipssEntity.getKjoretoymodell().getAar());
		}
		if(mipssEntity.getSelskapList() != null) {
			for (Selskap s : mipssEntity.getSelskapList()) {
				logger.debug("	selskap "  + s);
			}
		}
		else {
			logger.debug("selskaper     null");
		}
		logger.debug("slettet     "  + mipssEntity.getSlettetFlagg());
		logger.debug("------------Kjøretøy end-------------------------");
	}
	
	/**
	 * Lager en kopi av en kjøretøysordre
	 * 
	 * @param kjoretoyordre
	 * @return
	 */
	public static Kjoretoyordre lagKopiAvOrdre(Kjoretoyordre kjoretoyordre) {
		Kjoretoyordre kjoretoyordreKopi = new Kjoretoyordre();
		kjoretoyordreKopi.setId(kjoretoyordre.getId());
		kjoretoyordreKopi.merge(kjoretoyordre);
		kjoretoyordreKopi.setKjoretoyordre_StatusList(new ArrayList<Kjoretoyordre_Status>());
		kjoretoyordreKopi.setKjoretoyordreloggList(new ArrayList<Kjoretoyordrelogg>());
		
		for(Kjoretoyordre_Status kjoretoyordre_Status : kjoretoyordre.getKjoretoyordre_StatusList()) {
			Kjoretoyordre_Status kjoretoyordre_StatusKopi = new Kjoretoyordre_Status();
			kjoretoyordre_StatusKopi.setKjoretoyordrestatus(kjoretoyordre_Status.getKjoretoyordrestatus());
			kjoretoyordre_StatusKopi.setKjoretoyordre(kjoretoyordre_Status.getKjoretoyordre());
			try {
				kjoretoyordre_StatusKopi.merge(kjoretoyordre_Status);
			} catch(Exception e) {}
			kjoretoyordreKopi.getKjoretoyordre_StatusList().add(kjoretoyordre_StatusKopi);
		}
		
		for(Kjoretoyordrelogg kjoretoyordrelogg : kjoretoyordre.getKjoretoyordreloggList()) {
			Kjoretoyordrelogg kjoretoyordreloggKopi = new Kjoretoyordrelogg();
			kjoretoyordreloggKopi.setKjoretoyordre(kjoretoyordrelogg.getKjoretoyordre());
			kjoretoyordreloggKopi.setOpprettetDato(kjoretoyordrelogg.getOpprettetDato());
			try {
				kjoretoyordreloggKopi.merge(kjoretoyordrelogg);
			} catch(Exception e) {}
			kjoretoyordreKopi.getKjoretoyordreloggList().add(kjoretoyordreloggKopi);
		}
		
		return kjoretoyordreKopi;
	}
}
