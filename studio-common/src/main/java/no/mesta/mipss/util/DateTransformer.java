package no.mesta.mipss.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.persistence.Clock;

/**
 * Klasse for å transformere en dato til et heltatt presentert i en String og tilbake. 
 * 
 * Formatet på datoen er ddMMyy. Hver angivelse konverteres til HEX som en String og 
 * konkateneres i en buffer i rekkefølgen MMyydd. Resultatet konverteres fra radix 16 til en 
 * Integer og det kalkuleres et kontrollsiffer (mod10) som legges på. Resultatet er en 7-siffret 
 * kode som kan konverteres tilbake til en java.util.Date.
 * 
 * 
 * Datoer skal kun være den 1. i hver måned og kun koder for datoer mindre enn ett år fremover i tid validerer.
 * 
 * Innenfor 7 siffer er det inntill 4 duplikater i løpet av ett år. 
 *  
 * 
 * @author harkul
 *
 */
public class DateTransformer {
	private boolean debug = true;

	
	/**
	 * En test for å sjekke hvor mange koder som kan opprettes.
	 */
	public void testCodes(){
		DateTransformer d = new DateTransformer();
		int current = 0;
		int max = 9999999;
		
		int hitCount = 0;
		SimpleDateFormat f = new SimpleDateFormat("ddMMyyyy");
		Date now=Clock.now();
		while (current<max){
			String c = String.valueOf(current);
			if (d.validate(c)){
				try{
					Date hit = d.stringToDate(c);
					if (hit.after(now)){
						System.out.println(c+", "+f.format(hit));
						hitCount++;
						
					}
				} catch (Throwable t){
					//IGNORE
				}
			}
			current++;
		}
		System.out.println("found a total of :"+hitCount);
	}
	public static void main(String[] args) throws ParseException {
		DateTransformer d = new DateTransformer();
//		String code = d.dateToString(new SimpleDateFormat("ddMMyy").parse("010212"));
//		System.out.println("\n\nDECODE:"+code);
		Date stringToDate = d.stringToDate("1996737");
		System.out.println(stringToDate);
//		686066
	}
	
	/**
	 * Kalkulerer kontrollsiffer for tallet med luhns-algoritme (mod10)
	 * @param value
	 * @return
	 */
	private String calculateLuhn(String value){
		int sum = 0;
		for (int i=0; i<value.length(); i++ ){
			sum += Integer.parseInt(value.substring(i,i+1));
		}
		int[] delta = new int[]{0,1,2,3,4,-4,-3,-2,-1,0};
		for (int i=value.length()-1; i>=0; i-=2 ){		
			int deltaIndex = Integer.parseInt(value.substring(i,i+1));
			int  deltaValue = delta[deltaIndex];	
			sum += deltaValue;
		}
		int mod10 = sum % 10;
		mod10 = 10 - mod10;	
		if (mod10==10){		
			mod10=0;
		}
		return String.valueOf(mod10);
	 }
	
	/**
	 * Gjør en xor på v og key
	 * @param v
	 * @param key
	 * @return
	 */
	public int xorify(int v, int key){
		return v^key;
	}
	 /**
	  * Validerer tallet mot kontrollsifferet
	  * @param value
	  * @return
	  */
	public boolean validate(String value){
		int luhnDigit = Integer.parseInt(value.substring(value.length()-1,value.length()));
		String luhnLess = value.substring(0,value.length()-1);
		if (Integer.parseInt(calculateLuhn(luhnLess))==(luhnDigit)){
			return true;
		}	
		return false;
	}
	
	/**
	 * Encoder en java.util.Date. 
	 * 
	 * OBS: Denne algoritmen gir kun 1/5 så stor sjanse å cracke som å vinne i lotto.
	 *  
	 * @param date
	 * @return
	 */
	public String dateToString(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dd = c.get(Calendar.DAY_OF_MONTH);
		int mm = c.get(Calendar.MONTH);
		String y1 = String.valueOf(c.get(Calendar.YEAR));
		int yy = Integer.valueOf(y1.substring(2));
		if (debug)
			System.out.println("Before:"+dd+" "+mm+" "+yy);
		
		String d = toHex(dd);
		String m = toHex(mm);
		String y = toHex(yy);
		
		StringBuilder hex = new StringBuilder();
		hex.append(m);
		hex.append(y);
		hex.append(d);
		int parseInt = Integer.parseInt(hex.toString(), 16);
		
		String digit = calculateLuhn(String.valueOf(parseInt));
		if (debug){
			System.out.println("HEX-string:"+hex);
			System.out.println("Integer code:"+parseInt);
			System.out.println("Kontrollsiffer:"+digit);
		}
		//trekk fra dagens dagnummer(i året) fra resultatet slik at
		//"koden" kun kan benyttes samme dag den blir generert
		int doy = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
		int result = Integer.parseInt(String.valueOf(parseInt)+digit);
		return String.valueOf(result-doy);
	}
	
	/**
	 * Konverterer en integer til hex og venstre-padder med 0 slik at hex-verdien består av 2 tegn
	 * @param s
	 * @return
	 */
	public String toHex(int s){
		StringBuilder sb = new StringBuilder();
		int i = Integer.valueOf(s);
		if (i<=0x00F)sb.append("0");
		sb.append(Integer.toHexString(i));
		return sb.toString();
	}
	
	/**
	 * Konverterer en hex verdi til en int
	 * @param hex
	 * @return
	 */
	public int fromHex(String hex){
		if (hex==null)//januar
			return 0;
		return Integer.parseInt(hex, 16);
	}
	/**
	 * Dekoder en obfuskert dato til en java.util.Date
	 * @param str
	 * @return
	 */
	public Date stringToDate(String integerDate){
		//"koden" kan kun benyttes samme dag den ble generert
		int doy = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
		int date = Integer.parseInt(integerDate);
		integerDate = String.valueOf(date+doy);
		
		//validates the number
		if (!validate(integerDate)){
			throw new RuntimeException("Someone tried to crack the app (Luhns_algo_failed_exception)");
		}
		//fjerner kontrollsiffer før dato transformeres videre
		integerDate = integerDate.substring(0, integerDate.length()-1);
		String hex = Integer.toHexString(Integer.valueOf(integerDate));
		if (debug){
			System.out.println("Integer code:"+integerDate);
			System.out.println("HEX-string:"+hex);
		}
		
		String[] values = new String[3];
		int idx = 0;
		while (hex.length()>0){
			int i = hex.length()>1?2:1;
			values[idx]=hex.substring(hex.length()-i, hex.length());
			hex = hex.substring(0, hex.length()-i);
			idx++;
		}
		
		int m = fromHex(values[2]);
		int y = fromHex(values[1]);
		int d = fromHex(values[0]);
		
		if (d!=1){ //datoen skal være den 1. i hver måned
			throw new RuntimeException("Someone tried to crack the app (Wrong_calculated_day_exception)");
		}
		if (debug)
			System.out.println("Result:"+d+" "+m+" "+y);
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, d);
		c.set(Calendar.MONTH, m);
		c.set(Calendar.YEAR, 2000+y);
		if (debug)
			System.out.println("valid to:"+c.getTime());
		
		Date oneYear = CalendarUtil.getYearsAgo(-1); //Koder skal kun aksepteres for ett år fremover i tid
		
		if (c.getTime().after(oneYear)){//If luhns "failes"...
			throw new RuntimeException("Someone tried to crack the app (Wrong_master_year_size_exception)");
		}
		return c.getTime();
	}
}
