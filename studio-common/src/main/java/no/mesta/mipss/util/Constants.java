package no.mesta.mipss.util;


/**
 * Innehodler sentrale variabler for interumenterings-plugin.
 */
public class Constants {
    /**
     * Properties-fil for internasjonaliserte tekster
     */
    public static final String BUNDLE_NAME = "Instrumentation";
    
}
