package no.mesta.mipss.util.nvdb;


public class Lokasjon {
	private int[] kommune;
	private String bbox;
	private String srid;
	public void setKommune(int[] kommune) {
		this.kommune = kommune;
	}
	public int[] getKommune() {
		return kommune;
	}
	public void setBbox(String bbox) {
		this.bbox = bbox;
	}
	public String getBbox() {
		return bbox;
	}
	public void setSrid(String srid) {
		this.srid = srid;
	}
	public String getSrid() {
		return srid;
	}
}
