package no.mesta.mipss.util.nvdb;

public class Filter {
	private String type;
	private String operator;
	private String[] verdi;
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperator() {
		return operator;
	}
	public void setVerdi(String[] verdi) {
		this.verdi = verdi;
	}
	public String[] getVerdi() {
		return verdi;
	}

}
