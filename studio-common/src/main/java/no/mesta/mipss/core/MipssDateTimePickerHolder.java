package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXDatePicker;

public class MipssDateTimePickerHolder implements PropertyChangeListener, ChangeListener {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
    private JXDatePicker date;
    private JSpinner time;
    
    public MipssDateTimePickerHolder(JXDatePicker date, JSpinner time) {
        this.date = date;
        this.time = time;
        time.addChangeListener(this);
        date.addPropertyChangeListener(this);
    }
    
    private Date getDate(Date dag, Date kl){
    	if (dag==null)return null;
        Calendar dagCal = Calendar.getInstance();
        dagCal.setTime(dag);
        
        Calendar klCal = Calendar.getInstance();
        klCal.setTime(kl);
        
        dagCal.set(Calendar.HOUR_OF_DAY, klCal.get(Calendar.HOUR_OF_DAY));
        dagCal.set(Calendar.MINUTE, klCal.get(Calendar.MINUTE));
        dagCal.set(Calendar.SECOND, klCal.get(Calendar.SECOND));
        return dagCal.getTime();
    }
    
    public Date getDate(){
        Date dag = date.getDate();
        Date kl = (Date)time.getValue();
        return getDate(dag, kl);
    }
    public void setDate(Date newDate){
    	date.setDate(newDate);
    	if (newDate!=null)
    		time.setValue(newDate);
    	props.firePropertyChange("date", null, getDate());
    }
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		props.firePropertyChange("date", null, getDate());
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		props.firePropertyChange("date", null, getDate());
		
	}
    
	/**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + l + ")");
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + propName + ", " + l + ")");
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + l + ")");
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + propName + ", " + l + ")");
        props.removePropertyChangeListener(propName, l);
    }

	
}
