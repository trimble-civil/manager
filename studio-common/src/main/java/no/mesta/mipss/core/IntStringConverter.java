package no.mesta.mipss.core;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Converter;

public class IntStringConverter extends Converter<Integer, String> {
	private static final Logger logger = LoggerFactory.getLogger(IntegerStringConverter.class);

	public IntStringConverter() {
	}


	@Override
	public String convertForward(Integer value) {
		if (value == null) {
			return null;
		} else {
			return String.valueOf(value);
		}
	}

	@Override
	public Integer convertReverse(String value) {
		logger.trace("convertReverse(" + value + ") start");
		int intValue;
		if (StringUtils.isBlank(value)) {
			intValue = 0;
		} else {
			try {
				intValue = Integer.valueOf(value);
			} catch (NumberFormatException e) {
				intValue = 0;
				logger.error("ConvertRevers exception: ",e);
			}
		}

		return intValue;
	}
}
