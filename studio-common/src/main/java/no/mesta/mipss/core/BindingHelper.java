package no.mesta.mipss.core;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.beansbinding.Property;

/**
 * Gjør enkle binding oppgaver
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class BindingHelper {
    public static AutoBinding.UpdateStrategy READ = AutoBinding.UpdateStrategy.READ;
    public static AutoBinding.UpdateStrategy READ_WRITE = AutoBinding.UpdateStrategy.READ_WRITE;
    
    /**
     * Lager en BeanProperty eller ELProperty
     * 
     * @param beanField
     * @return
     */
    public static<S,V> Property<S,V> getProperty(String beanField) {
        if (beanField.startsWith("$")) {
            return ELProperty.create(beanField);
        } else {
            return BeanProperty.create(beanField);
        }
    }
    
    /**
     * Lager en binding mellom to objekters felter
     * 
     * @param src
     * @param srcField
     * @param target
     * @param targetField
     * @return
     */
    public static<SS, SV, TT, TV> Binding<SS, SV, TT, TV> createbinding(SS src, String srcField, TT target, String targetField) {
    	Property<SS,SV> sourceProperty = getProperty(srcField);
    	Property<TT,TV> targetProperty = getProperty(targetField);
        return Bindings.createAutoBinding(READ, src, sourceProperty, target, targetProperty);
    }
    
    /**
     * Lager en binding mellom to objekters felter med en gitt strategi
     * 
     * @param src
     * @param srcField
     * @param target
     * @param targetField
     * @param strategy
     * @return
     */
    public static<SS, SV, TT, TV> Binding<SS, SV, TT, TV> createbinding(SS src, String srcField, TT target, String targetField, AutoBinding.UpdateStrategy strategy) {
    	Property<SS,SV> sourceProperty = getProperty(srcField);
    	Property<TT,TV> targetProperty = getProperty(targetField);
        return Bindings.createAutoBinding(strategy, src, sourceProperty, target, targetProperty);
    }
    
    /**
     * Lager en konverter
     * 
     * @return
     */
    public static StringLongConverter createStringLongConverter() {
        return new StringLongConverter();
    }
}
