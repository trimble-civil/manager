package no.mesta.mipss.core;

import no.mesta.mipss.common.PropertyChangeSource;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <D>
 */
public class NullIntegerValidator<D extends PropertyChangeSource> extends BlankFieldValidatorAdapter<D, Integer> {

	public NullIntegerValidator(D detail, String fieldName) {
		super(detail, fieldName);
	}

	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(Integer value) {
		logger.trace("calculateBlank(" + value + ") start");
		return value == null;
	}
}
