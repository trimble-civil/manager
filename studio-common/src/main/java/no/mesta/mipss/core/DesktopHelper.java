package no.mesta.mipss.core;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class DesktopHelper {
	public static void openFile(File file) {
		if(Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			if(desktop.isSupported(Desktop.Action.OPEN)) {
				try {
					desktop.open(file);
				} catch (IOException e) {
					throw new IllegalStateException(e);
				}
			}
		}
	}
	
	public static void openNewMail(String address) {
		if(Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			if(desktop.isSupported(Desktop.Action.MAIL)) {
		        try {
		            if (address.length() > 0) {
		            	URI uriMailTo = new URI("mailto", address, null);
		                desktop.mail(uriMailTo);
		            } else {
		                desktop.mail();
		            }
		        } catch(IOException ioe) {
		            throw new IllegalStateException(ioe);
		        } catch(URISyntaxException use) {
		        	throw new IllegalStateException(use);
		        }
			}
		}
	}
}
