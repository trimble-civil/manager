package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import no.mesta.mipss.common.PropertyChangeSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Property;

/**
 * Adapter for null/blank validatorer
 * 
 * @author @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <D>
 * @param <T>
 */
public abstract class BlankFieldValidatorAdapter<D extends PropertyChangeSource, T>
		implements IBlankFieldValidator<T>, PropertyChangeListener {
	private final D detail;
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private final String fieldName;

	protected final PropertyChangeSupport props = new PropertyChangeSupport(this);

	public BlankFieldValidatorAdapter(D detail, String fieldName) {
		this.detail = detail;
		this.fieldName = fieldName;
		detail.addPropertyChangeListener(fieldName, this);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}
	/** {@inheritDoc} */
	@Override
	public abstract boolean calculateBlank(T value);
	
	public D getDetail() {
		return detail;
	}

	public String getFieldName() {
		return fieldName;
	}

	public T getValue() {
		Property<D, T> property = PropertyUtil.createProperty(getFieldName());
		return property.getValue(getDetail());
	}

	/** {@inheritDoc} */
	@Override
	public boolean isBlank() {
		return calculateBlank(getValue());
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public void propertyChange(PropertyChangeEvent e) {
		T oldValue = (T) e.getOldValue();
		T newValue = (T) e.getNewValue();
		
		boolean oldBlank = calculateBlank(oldValue);
		boolean newBlank = calculateBlank(newValue);
		
		props.firePropertyChange("blank", oldBlank, newBlank);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
}
