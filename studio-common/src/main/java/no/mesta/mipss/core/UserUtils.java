package no.mesta.mipss.core;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.RolleBruker;

public class UserUtils {

	
	public static boolean isAdmin(Bruker bruker){
		List<RolleBruker> rList = bruker.getRolleBrukerList();
		for (RolleBruker r:rList){
			if (r.getRolle().getNavn().indexOf("Admin")!=-1){
				return true;
			}
		}
		return false;
	}
	public static boolean isBrukerInRolle(Bruker bruker, String rollenavn){
		List<RolleBruker> rList = bruker.getRolleBrukerList();
		for (RolleBruker r:rList){
			if (StringUtils.equalsIgnoreCase(r.getRolle().getNavn(), rollenavn)){
				return true;
			}
		}
		return false;
	}
}
