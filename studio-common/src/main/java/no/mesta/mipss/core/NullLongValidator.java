package no.mesta.mipss.core;

import no.mesta.mipss.common.PropertyChangeSource;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <D>
 */
public class NullLongValidator<D extends PropertyChangeSource> extends BlankFieldValidatorAdapter<D, Long> {

	public NullLongValidator(D detail, String fieldName) {
		super(detail, fieldName);
	}

	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(Long value) {
		logger.trace("calculateBlank(" + value + ") start");
		return value == null;
	}
}
