package no.mesta.mipss.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdesktop.beansbinding.Converter;

/**
 * Brukes i beansbindings. Konverterer dato til string, og tilbake
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class DateStringConverter extends Converter<Date, String> {
    private Logger logger = LoggerFactory.getLogger(DateStringConverter.class);
    private SimpleDateFormat simpleDateFormat;
    
    /**
     * Konstruktoer
     * 
     * @param mask datoformatet, typisk dd.MM.yyyy
     */
    public DateStringConverter(String mask) {
        simpleDateFormat = new SimpleDateFormat(mask);
    }

    /** {@inheritDoc} */
    public String convertForward(Date value) {
        return simpleDateFormat.format(value);
    }

    /** {@inheritDoc} */
    public Date convertReverse(String value) {
        Date date = null;

        try {
            simpleDateFormat.parse(value);
        } catch (ParseException e) {
            logger.debug("ConvertReverse PE", e);
        }
        return date;
    }
}
