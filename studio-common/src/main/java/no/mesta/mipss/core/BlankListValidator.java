package no.mesta.mipss.core;

import java.util.List;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * Sier om en string er blank eller ikke
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 * @param <D> objektet feltet er medlem av
 * @param <E> objekttypen i listen
 */
public class BlankListValidator<D extends PropertyChangeSource, E extends MipssEntityBean<? extends IRenderableMipssEntity>>
		extends BlankFieldValidatorAdapter<D, List<E>> {

	public BlankListValidator(D detail, String fieldName) {
		super(detail, fieldName);
	}

	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(List<E> value) {
		if (logger.isTraceEnabled()) {
			logger.trace("calculateBlank(" + value + ") start");
		}
		return value == null || value.isEmpty();
	}
}
