package no.mesta.mipss.core;

import java.util.Date;
import java.util.List;

import no.mesta.mipss.persistence.IkkeoverlappendePeriode;

public class IkkeoverlappendePeriodeValidator {
	public boolean sjekkDatoerIPeriode(IkkeoverlappendePeriode ikkeoverlappendePeriode) {
		Date nyFraDato = CalendarUtil.round(ikkeoverlappendePeriode.getFraDato(), true);
		Date nyTilDato = CalendarUtil.round(ikkeoverlappendePeriode.getTilDato(), false);
		
		if(nyFraDato != null && nyTilDato != null && after(nyFraDato, nyTilDato)) {
			return false;
		}
		
		return true;
	}
	
	public boolean sjekkPeriodeMotEksisterendePerioder(boolean nyPeriode, List<? extends IkkeoverlappendePeriode> eksisterendePerioder, IkkeoverlappendePeriode ikkeoverlappendePeriode) {
		Date nyFraDato = CalendarUtil.round(ikkeoverlappendePeriode.getFraDato(), true);
		Date nyTilDato = CalendarUtil.round(ikkeoverlappendePeriode.getTilDato(), false);
		
		for(IkkeoverlappendePeriode eksPeriode : eksisterendePerioder) {
			if (nyPeriode&&CalendarUtil.isOverlappende(nyFraDato, nyTilDato, eksPeriode.getFraDato(), eksPeriode.getTilDato(), false)){
				return false;
			}
		}
		
		return true;
	}
	
	private boolean after(Date date1, Date date2) {
		return date1.after(date2) || date1.equals(date2);
	}
}
