package no.mesta.mipss.core;

import java.awt.Color;

/**
 * Klasse som konverterer mellom fargeformater
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ColorUtil {

	
	/**
	 * Konverter fra web farger (f.eks 0x0000ff) til java.awt.Color
	 * 
	 * @param webColor
	 * @return
	 */
	public static Color fromHexString(String webColor){
		if (webColor==null)
			return Color.white;
//			return new Color(100, 50, 254);
		return new Color(Integer.decode(webColor));
	}
	
	public static Color setAlpha(Color color, int alpha){
		Color c = new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha);
		return c;
	}
	public static Color fromHexString(String webColor, boolean hasAlpha){
		return new Color(Integer.decode(webColor), hasAlpha);
	}
	
	public static Color mergeColors(Color col1, Color col2){
		Color color = new Color(col1.getRed()|col2.getRed(), 
								col1.getGreen()|col2.getGreen(),
								col1.getBlue()|col2.getBlue());
		return color;
	}
	 public static String toHexString(Color color) {
	        return "0x"+(""+Integer.toHexString(color.getRGB())).substring(2);        
	    }
	public static Color mergeAllColors(Color... colors){
		int i =0;
		int r=0, g=0, b=0;
		for (Color c: colors){
			if (i==0){
				r = c.getRed();
				g = c.getGreen();
				b = c.getBlue();
			}else{
				r |=c.getRed();
				g |=c.getGreen();
				b |=c.getBlue();
			}
		}
		Color c = new Color  (r,g,b);
		return c;
	}
}
