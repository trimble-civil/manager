package no.mesta.mipss.core;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.MipssListCellRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modell for kontrakter i loaderen. Inneholder en null verdi også som
 * representeres av <code>NoContract</code>.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssContractModel implements ComboBoxModel {
	public static final String ALL_CONTRACTS = "< alle kontrakter >";
	private List<Driftkontrakt> kontrakter;
	private static final Logger logger = LoggerFactory.getLogger(MipssContractModel.class);
	public static final String SELECT_CONTRACT = "< velg en kontrakt >";
	@SuppressWarnings("unused")
	private String kontraktString;
	private final List<ListDataListener> listener = new ArrayList<ListDataListener>();
	private final NoContract NO_CONTRACT;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private VetoableChangeSupport vetos = new VetoableChangeSupport(this);
	private Driftkontrakt valgtKontrakt;

	public MipssContractModel(List<Driftkontrakt> kontrakter) {
		this(null, kontrakter);
	}

	public MipssContractModel(String kontraktString, List<Driftkontrakt> kontrakter) {
		Collections.sort(kontrakter);
		this.kontrakter = kontrakter;
		if (kontraktString == null) {
			kontraktString = SELECT_CONTRACT;
		}
		this.kontraktString = kontraktString;
		NO_CONTRACT = new NoContract(kontraktString);
		this.kontrakter.add(0, NO_CONTRACT);
	}

	/** {@inheritDoc} */
	public void addListDataListener(ListDataListener l) {
		listener.add(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	public ListCellRenderer createCellRenderer() {
		return new Renderer();
	}

	/** {@inheritDoc} */
	public Driftkontrakt getElementAt(int index) {
		return kontrakter.get(index);
	}

	/**
	 * Henter ut alle kontraktene i modellen
	 * 
	 * @return
	 */
	public List<Driftkontrakt> getKontrakter() {
		return kontrakter;
	}

	/** {@inheritDoc} */
	public Driftkontrakt getSelectedItem() {
		if (valgtKontrakt == NO_CONTRACT) {
			return null;
		} else {
			return valgtKontrakt;
		}
	}

	/** {@inheritDoc} */
	public int getSize() {
		return kontrakter.size();
	}

	public boolean isRealContract() {
		return valgtKontrakt != NO_CONTRACT && valgtKontrakt != null;
	}
	
	public boolean isNoContract(Driftkontrakt k) {
		return k == NO_CONTRACT;
	}

	/** {@inheritDoc} */
	public void removeListDataListener(ListDataListener l) {
		listener.remove(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	/**
	 * Setter alle kontraktene i modellen
	 * 
	 * @param kontrakter
	 */
	public void setKontrakter(List<Driftkontrakt> kontrakter) {
		logger.debug("setKontrakter");
		this.kontrakter = kontrakter;
		Collections.sort(this.kontrakter);
	}

	/** {@inheritDoc} */
	public void setSelectedItem(Object anItem) {
		boolean isVetoed = false;
		Driftkontrakt old = valgtKontrakt;
		Driftkontrakt now = (Driftkontrakt) anItem;
		if (anItem == null) {
			anItem = NO_CONTRACT;
		} else if (anItem == NO_CONTRACT) {
			now = null;
		}

		valgtKontrakt = (Driftkontrakt) anItem;
		
		try {
			vetos.fireVetoableChange("selectedItem", old, now);
			props.firePropertyChange("valgtKontrakt", old, now);
		} catch (PropertyVetoException e) {
			isVetoed = true;
		}
		
		if(isVetoed) {
			valgtKontrakt = old;
			for(ListDataListener listDataListener : listener) {
				listDataListener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, kontrakter.size()));
			}
		} else {
			props.firePropertyChange("selectedItem", old, now);
		}
	}

	/**
	 * Placeholder for en "ikke-kontrakt". Skal bare være en instans av denne i
	 * applikasjonen
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class NoContract extends Driftkontrakt implements Comparable<Driftkontrakt> {
		private String k = null;

		public NoContract(String s) {
			k = s;
		}

		/** {@inheritDoc} */
		@Override
		public int compareTo(Driftkontrakt other) {
			return -1;
		}

		/** {@inheritDoc} */
		@Override
		public boolean equals(Object o) {
			if (o == this) {
				return true;
			}
			return false;
		}

		/** {@inheritDoc} */
		@Override
		public String getTextForGUI() {
			return k;
		}

		/** {@inheritDoc} */
		@Override
		public int hashCode() {
			return k.hashCode();
		}
	}

	/**
	 * Renderer for kontrakt lister
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class Renderer extends MipssListCellRenderer<Driftkontrakt> {

		/** {@inheritDoc} */
		@Override
		public Component getListCellRendererComponent(JList list, // the list
				Object value, // value to display
				int index, // cell index
				boolean isSelected, // is the cell selected
				boolean cellHasFocus) // does the cell have focus
		{
			if (value == null) {
				super.getListCellRendererComponent(list, (Object)NO_CONTRACT, index, isSelected, cellHasFocus);
			} else {
				super.<MipssListCellRenderer<Driftkontrakt>>getListCellRendererComponent(list, (Driftkontrakt)value, index, isSelected, cellHasFocus);
			}

			return this;
		}
	}
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#addVetoableChangeListener(VetoableChangeListener)
	 * @param l
	 */
	public void addVetoableChangeListener(VetoableChangeListener listener) {
		vetos.addVetoableChangeListener(listener);
	}
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#addVetoableChangeListener(String,
	 *      VetoableChangeListener)
	 * @param l
	 */
	public void addVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		vetos.addVetoableChangeListener(propertyName, listener);
	}
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#removeVetoableChangeListener(VetoableChangeListener)
	 * @param l
	 */
	public void removeVetoableChangeListener(VetoableChangeListener listener) {
		vetos.removeVetoableChangeListener(listener);
	}
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#removeVetoableChangeListener(String,
	 *      VetoableChangeListener)
	 * @param l
	 */
	public void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		vetos.removeVetoableChangeListener(propertyName, listener);
	}
}
