package no.mesta.mipss.core;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;

/**
 * Interface for funksjoner i hovedvinduet
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public interface MainFrame {

	/**
	 * Viser et gitt menynode
	 * 
	 * @param menyNode
	 */
	public void activateMenuItem(MenynodeDTO menyNode);

	/**
	 * Fjerner visningen av plugin
	 * 
	 */
	public void clearApplicationPanel();

	/**
	 * Fjerner markeringen fra menyvalget
	 * 
	 */
	public void clearMenuSelection();

	/**
	 * Viser en melding en stund, og så fader den ut
	 * 
	 * @param message
	 * @param priority
	 */
	public void flashMessage(String message, int priority);

	/**
	 * Hovedvinduet til MIPSS Studio
	 * 
	 * @return
	 */
	public JFrame getApplicationFrame();

	/**
	 * Returnerer størelsen på panelet for plugins
	 * 
	 * @return
	 */
	public Dimension getApplicationPanelSize();

	/**
	 * Get the panel that contains the access menu
	 * 
	 * @return menuPanel
	 */
	public JPanel getMenuPanel();

	/**
	 * Åpner hovedvinduet til MIPSS Studio
	 * 
	 * @param loader
	 */
	public void init(IMipssStudioLoader loader);

	/**
	 * Set the left component on the window. This is the component where the
	 * user will find its applications
	 * 
	 * @param panel
	 *            AccessMenu panel
	 */
	public void setMenuPanel(JPanel panel);

	/**
	 * Set the application status message
	 * 
	 * @param statusMessage
	 */
	public void setStatusMessage(String statusMessage);

	/**
	 * Viser et GUI panel
	 * 
	 * @param panel
	 * @param displayName
	 */
	public void showPanel(JPanel panel, String displayName);

	/**
	 * Viser plugin
	 * 
	 * @param plugin
	 * @param displayName
	 * 
	 */
	public void showPlugin(MipssPlugin plugin, String displayName);
	
	public Driftkontrakt getSelectedDriftkontrakt();
	
}
