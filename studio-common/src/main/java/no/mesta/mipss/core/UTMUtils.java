package no.mesta.mipss.core;

import java.awt.geom.Point2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse som konverterer til og fra UTM33 fra Lat/long
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 * Oprinnelig kode fra kværnemotoren
 *
 */
public class UTMUtils {
    private static final Logger log = LoggerFactory.getLogger(UTMUtils.class);
    public enum Hemisphere {
        North,
        South;
    }

    private Hemisphere m_Hemisphere = Hemisphere.North;
    private int m_UtmZone = 33;
    private double m_e2;
    private double m_n;
    private double m_e_note_2;
    long m_a;
    
    public static void main (String[] args){
        UTMUtils u = new UTMUtils();
        Point2D.Double p = new Point2D.Double();
        String lat="59.767552";

        String lon = "6.078816";

        
        u.convert2UTM(lat, lon, p);
        System.out.println(p);
        log.debug("Point: {}", p);
    }
    public UTMUtils() {

        // Initialzing variables used by Convert2UTM(), Convert2LLA()
        m_a = 6378137; // WGS-84 6378388;  Internasjonal
        double f = 1 / 298.257223563; // WGS-84 (1.0/297);Internasjonal
        m_e2 = f * (2 - f);
        m_e_note_2 = m_e2 / (1 - m_e2);
        m_n = f / (2 - f);
    }

    // Se: http://www.uaex.edu/Other_Areas/publications/HTML/FSA-1031.asp
    // Denne har ikke noe med selve formelen å gjøre, men er kun forklaring.
    // ---------------------------------------------------------------------

    public boolean convert2UTM(String latitude, String longitude, 
                               Point2D utm) {

        double lat = getUtmCoordinate(latitude);
        double lon = getUtmCoordinate(longitude);
        return convert2UTM(lat, lon, utm);

    }

    public boolean convert2UTM(double latitude, double longitude, 
                               Point2D utm) {
        int nUtmUnit = 6; // 1 UTM zone is equal to 6 degrees

        if (latitude == 0 || longitude == 0)
            return false;

        double m_dX = latitude;
        double m_dY = longitude;

        if (m_UtmZone < 1 || m_UtmZone > 60)
            return false;

        double lambda0 = 0;

        if (m_UtmZone > 0 && m_UtmZone < 31)
            lambda0 = 
                    (Math.PI * ((m_UtmZone * nUtmUnit + 180) - (nUtmUnit / 2))) / 
                    180;
        else
            lambda0 = 
                    (Math.PI * ((m_UtmZone * nUtmUnit - 180) - (nUtmUnit / 2))) / 
                    180;

        m_dY = m_dY - lambda0;

        double W = Math.sqrt(1 - m_e2 * Math.pow(Math.sin(m_dX), 2));
        double cos2DX = Math.cos(2 * m_dX);

        double theta = 
            m_dX - ((3.0 / 2) * m_n - (31.0 / 24) * Math.pow(m_n, 3) - 
                    ((15.0 / 8) * Math.pow(m_n, 2) - 
                     (435.0 / 128) * Math.pow(m_n, 4)) * cos2DX + 
                    (35.0 / 12) * Math.pow(m_n, 3) * Math.pow(cos2DX, 2) - 
                    (315.0 / 64) * Math.pow(m_n, 4) * Math.pow(cos2DX, 3)) * 
            Math.sin(2 * m_dX);

        double cosDX = Math.cos(m_dX);
        double sinDY = Math.sin(m_dY);

        double eta = 0.5 * Math.log((1 + cosDX * sinDY) / (1 - cosDX * sinDY));
        double ksi = Math.atan(Math.tan(m_dX) / Math.cos(m_dY)) - m_dX;

        double N = m_a / W;
        double delta_gamma = 
            N * eta * (1 + (m_e_note_2 / 6) * (Math.pow(cosDX, 2)) * 
                       Math.pow(eta, 2));
        double delta_X = 
            N * ksi * (1 + (m_e_note_2 / 6) * (Math.pow(cosDX, 2)) * 
                       Math.pow(eta, 2));

        double S = 
            (m_a / (1 + m_n)) * (1 + (1 / 4) * Math.pow(m_n, 2) + (1 / 64) * 
                                 Math.pow(m_n, 4)) * theta;

        if (m_Hemisphere == Hemisphere.North)
            m_dX = 0.9996 * (delta_X + S);
        else if (m_Hemisphere == Hemisphere.South)
            m_dX = m_dX - 10.0e6;

        m_dY = 0.9996 * delta_gamma + 500000;

        utm.setLocation(m_dX, m_dY);
        return true;
    }
    
    public double getUtmCoordinate(double utm){
    	return utm /180 * Math.PI;
    }
    
    public double getUtmCoordinate(String latLongCoordinate) {
        // m_LatitudeStr:    5923.9876N
        // m_LongitudeStr:  01042.1234E
        //log.trace("Get utm coordinate:"+latLongCoordinate);
        String tmp = latLongCoordinate.replaceAll("E", "").replaceAll("N", "").replace(',','.');
        tmp = tmp.replaceAll("W", "");
        
        double coord = Double.parseDouble(tmp);
        return coord / 180 * Math.PI;
    }
}
