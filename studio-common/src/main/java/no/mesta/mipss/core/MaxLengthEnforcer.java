package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import javax.swing.text.JTextComponent;

import no.mesta.mipss.common.VetoableChangeSource;

import org.apache.commons.lang.StringUtils;

public class MaxLengthEnforcer implements VetoableChangeListener{
	private final int maxLength;
	private final JTextComponent textField;
	private final VetoableChangeSource beanInstance;
	private final String beanField;
	
	public MaxLengthEnforcer(VetoableChangeSource beanInstance, JTextComponent textField, String beanField, int length) {
		this.beanField = beanField;
		this.beanInstance = beanInstance;
		this.maxLength = length;
		this.textField = textField;		
	}
	
	public void start() {
		beanInstance.addVetoableChangeListener(beanField, this);
	}

	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		if(evt.getSource() == beanInstance && StringUtils.equals(beanField, evt.getPropertyName())) {
			String oldString = (String) evt.getOldValue();
			String newString = (String) evt.getNewValue();
			if(newString != null && newString.length() > maxLength) {
				textField.setText(oldString);
				throw new PropertyVetoException("Read only!", evt);
			}
		}		
	}
}
