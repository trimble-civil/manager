package no.mesta.mipss.core;

public class PasswordGeneratorUtils {
	
	public String generatePassword(){
		String vokaler = "aeiou";
		String konsonanter = "bdfghjklmnprstv";
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(konsonanter.toUpperCase().charAt((int) (Math.random()*konsonanter.length())));
		for (int i = 0; i < 5; i++) {
			if (i%2 == 0) {
				sb.append(vokaler.toLowerCase().charAt((int) (Math.random()*vokaler.length())));	

			} else{
				sb.append(konsonanter.toLowerCase().charAt((int) (Math.random()*konsonanter.length())));	
			}
		}
		
		for (int i = 0; i < 2; i++) {
			sb.append((int)(Math.random()*10));
		}
		
		return sb.toString();
	}
}
