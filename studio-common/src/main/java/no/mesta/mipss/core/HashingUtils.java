package no.mesta.mipss.core;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utilklasse for å lage en hash av en string med salting.
 * 
 * @author harkul
 *
 */
public class HashingUtils {
	public static final String SALT = "mestamipss256";
	
	public static void main(String[] args) {
		System.out.println(new HashingUtils().toHash("123"));
	}
	public String toHash(String text){
		text = addSalt(text);
		byte[] bytes=null;
		MessageDigest md=null;
		try {
			bytes = text.getBytes("UTF-8");
			md = MessageDigest.getInstance("SHA-256");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}	catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		md.update(bytes);
		byte[] sha = md.digest();
		return hashToString(sha);
	}
	
	private String hashToString(byte[] sha){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sha.length; i++) {
			sb.append(Integer.toString( ( sha[i] & 0xff ) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	private String addSalt(String text){
		StringBuilder sb = new StringBuilder();
		sb.append(SALT);
		sb.append(text);
		return sb.toString();
	}
}
