/**
 * 
 */
package no.mesta.mipss.core;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Converter;

/**
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 */
public class IntegerStringConverter extends Converter<Integer, String> {
	private static final Logger logger = LoggerFactory.getLogger(IntegerStringConverter.class);

	public IntegerStringConverter() {
	}


	@Override
	public String convertForward(Integer value) {
		if (value == null) {
			return null;
		} else {
			return String.valueOf(value);
		}
	}

	@Override
	public Integer convertReverse(String value) {
		logger.trace("convertReverse(" + value + ") start");
		Integer intValue;
		if (StringUtils.isBlank(value)) {
			intValue = null;
		} else {
			try {
				intValue = Integer.valueOf(value);
			} catch (NumberFormatException e) {
				intValue = null;
				logger.error("ConvertReverse NFE: ",e);
			}
		}

		return intValue;
	}
}
