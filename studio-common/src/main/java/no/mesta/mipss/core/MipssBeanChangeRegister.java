package no.mesta.mipss.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Holder oversikt over feltene i ei boenne
 * 
 * Denne stoetter ogsaa property change.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssBeanChangeRegister implements IBeanFieldChange, IDisposable {
    private static final Logger logger = LoggerFactory.getLogger(MipssBeanChangeRegister.class);
    private Map<String, Boolean> registeredFields = new HashMap<String, Boolean>();
    
    /**
     * Konstruktoer
     * 
     */
    public MipssBeanChangeRegister() {
        
    }
    
    /**
     * Konstruktoer
     * 
     * @param fields
     */
    public MipssBeanChangeRegister(String[] fields) {
        for(String field:fields) {
            registeredFields.put(field, false);
        }
    }

    /**
     * Legger til et felt i boenna
     * 
     * @param field
     */
    public void addField(String field) {
        put(field, false);
    }
    
    /**
     * Setter alle endringsflaggene til FALSE.
     */
    public void clearChanges() {
        Map<String, Boolean> nyMap = new HashMap<String, Boolean>();
        
        for(String k : registeredFields.keySet()) {
            nyMap.put(k, false);
        }
        
        registeredFields = nyMap;
    }
        
    /**
     * Logger til debug en beskjed
     * 
     * @param msg
     */
    protected void debug(String msg) {
        if(logger != null) {
            if(logger.isDebugEnabled()) {
                logger.debug(msg);
            }
        }
    }
    
    
    @Override
	public void dispose() {
		if(registeredFields != null) {
			registeredFields.clear();
		}
	}
    
    /**
     * Returnerer om boenna har endret seg
     * 
     * @return
     */
    public Boolean isChanged() {
        for(Boolean b:registeredFields.values()) {
            if(b) {
                return true;
            }
        }
        
        return false;        
    }
    
    /**
     * Undersoeker et gitt felt i boenna
     * 
     * @param field
     * @return
     */
    public Boolean isFieldChanged(String field) {
        return registeredFields.get(field);
    }

    /**
     * Setter om et felt i boenna er endret
     * 
     * @param field
     * @param changed
     */
    public void put(String field, Boolean changed) {
        registeredFields.put(field, changed);
    }

	/**
     * Notifiserer om et felt i boenna har endret seg
     * 
     * @param beanField
     * @param changed
     */
    public void setFieldChanged(Object instance, String beanField, boolean changed, boolean forceRemoval) {
        registeredFields.put(beanField, changed);
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
    	ToStringBuilder builder = new ToStringBuilder(this);
    	
    	Iterator<String> it = registeredFields.keySet().iterator();
    	while(it.hasNext()) {
    		String key = it.next();
    		builder.append(key, registeredFields.get(key));
    	}
    	
    	return builder.toString();
    }
}
