package no.mesta.mipss.core;

import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mipssutils.MipssServerUtils;

/**
 * Henter objekter og GUID'er
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ServerUtils implements MipssServerUtils{
	private static final MipssServerUtils bean = BeanUtil.lookup(MipssServerUtils.BEAN_NAME, MipssServerUtils.class);
	private static final ServerUtils utils = new ServerUtils();
	
	private ServerUtils() {}

	public void clearEM(){
		bean.clearEM();
	}
	/** {@inheritDoc} */
	@Override
	public String fetchGuid() {
		return bean.fetchGuid();
	}
	
	public static ServerUtils getInstance() {
		return utils;
	}

	/** {@inheritDoc} */
	@Override
	public <T> List<T> hentObjekter(Class<T> klasse, List<String> nokkler) {
		return bean.hentObjekter(klasse, nokkler);
	}
	
	public String hentSekvens(String navn){
		return bean.hentSekvens(navn);
	}

	@Override
	public Object hentMaskeKontFriksjon() {
		
		return bean.hentMaskeKontFriksjon();
	}
}
