package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import org.jdesktop.beansbinding.BindingGroup;

public class SaveStatusMonitor<T> implements PropertyChangeListener {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private Class<?> clazz;
	private T beanInstance;
	private MipssBeanChangeRegister mipssBeanChangeRegister;
	private Map<String, BeanFieldListener> beanFieldListeners = new HashMap<String, BeanFieldListener>();
	private Object oldValue;
	private Boolean overriddenChange;

	public SaveStatusMonitor(Class<?> clazz, Object beanInstanceSource, String beanInstanceSourceField, BindingGroup bindingGroup) {
		this.clazz = clazz;
		mipssBeanChangeRegister = new MipssBeanChangeRegister();
		bindingGroup.addBinding(BindingHelper.createbinding(beanInstanceSource, beanInstanceSourceField, this, "beanInstance"));
	}
	
	public void registerField(String field) {
		mipssBeanChangeRegister.addField(field);
		registerListener(field);
	}
	
	private void registerListener(String field) {
		BeanFieldListener listener = beanFieldListeners.get(field);
		if(listener != null) {
			throw new IllegalStateException("Two listeners on the same property");
		} else {
			if(getBeanInstance() != null) {
				BindingGroup bg = new BindingGroup();
				listener = new BeanFieldListener(getBeanInstance(), field, bg, mipssBeanChangeRegister);
				bg.bind();
				listener.addPropertyChangeListener("changed", this);
			}
			beanFieldListeners.put(field, listener);
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(evt.getPropertyName().equals("changed")) {
			Object newValue = isChanged();
			props.firePropertyChange("changed", oldValue, newValue);
			oldValue = newValue;
		}
	}
	
	public boolean isChanged() {
		return mipssBeanChangeRegister.isChanged() || (overriddenChange != null ? overriddenChange : false);
	}
	
	public T getBeanInstance() {
		return beanInstance;
	}
	
	public void setBeanInstance(T beanInstance) {
		this.beanInstance = beanInstance;
		rebindFields();
	}
	
	private void rebindFields() {
		Set<String> fields = beanFieldListeners.keySet();
		for(String field : fields) {
			BeanFieldListener beanFieldListener = beanFieldListeners.get(field);
			if(beanFieldListener != null) {
				beanFieldListener.removePropertyChangeListener(this);
				beanFieldListeners.put(field, null);
			}
			registerListener(field);
		}
	}
	
	public void reset() {
        Iterator<BeanFieldListener> lIt = beanFieldListeners.values().iterator();
        while(lIt.hasNext()) {
            BeanFieldListener listener = lIt.next();
            if(listener != null) {
            	listener.reset();
            }
        }
        
        overriddenChange = null;
        mipssBeanChangeRegister.clearChanges();
        props.firePropertyChange("fieldsChanged", oldValue, Boolean.FALSE);
        oldValue = Boolean.FALSE;
    }
	
	public Boolean getOverriddenChange() {
		return overriddenChange;
	}

	public void setOverriddenChange(Boolean overriddenChange) {
		Object oldValue = this.overriddenChange;
		this.overriddenChange = overriddenChange;
		propertyChange(new PropertyChangeEvent(this, "changed", oldValue, overriddenChange));
	}
	
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
