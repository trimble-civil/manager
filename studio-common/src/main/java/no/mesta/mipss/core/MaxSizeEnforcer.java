package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import javax.swing.text.JTextComponent;

import no.mesta.mipss.common.VetoableChangeSource;

import org.apache.commons.lang.StringUtils;

public class MaxSizeEnforcer<T extends Number> implements VetoableChangeListener{
	private final T maxSize;
	private final JTextComponent textField;
	private final VetoableChangeSource beanInstance;
	private final String beanField;
	
	public MaxSizeEnforcer(VetoableChangeSource beanInstance, JTextComponent textField, String beanField, T size) {
		this.beanField = beanField;
		this.beanInstance = beanInstance;
		this.maxSize = size;
		this.textField = textField;		
	}
	
	public void start() {
		beanInstance.addVetoableChangeListener(beanField, this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		if(evt.getSource() == beanInstance && StringUtils.equals(beanField, evt.getPropertyName())) {
			T oldV = (T) evt.getOldValue();
			T newV = (T) evt.getNewValue();
			boolean unsafe;
			if(oldV instanceof Integer) {
				unsafe = ((Integer) newV) > ((Integer) maxSize);
			} else {
				unsafe = ((Long) newV) > ((Long) maxSize);
			}
			if(oldV != null && unsafe) {
				textField.setText(String.valueOf(oldV));
				throw new PropertyVetoException("Too big!", evt);
			}
		}		
	}
}
