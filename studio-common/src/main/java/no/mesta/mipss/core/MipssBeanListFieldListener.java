package no.mesta.mipss.core;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Brukes til å lytte på de to "picklist" listene, slik at UserDialog
 * vet hvilke roller og selskaper som er valgt.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssBeanListFieldListener<T extends IRenderableMipssEntity> implements ListSelectionListener {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private List<T> originalList;
    private List<T> watchList;
    private IBeanFieldChange callback;
    private String field;
    
    /**
     * Konstruktør
     * 
     * @param liste listen som skal overvåkes
     * @param callback endringshåndtereren som skal informeres
     */
    public MipssBeanListFieldListener(List<T> liste, String field, IBeanFieldChange callback) {
    	log.debug("MipssBeanListFieldListener(" + liste + ", " + field + ") start");
        if(liste != null) {
        	originalList = new ArrayList<T>(liste);
        } else {
        	originalList = null;
        }
        
        this.watchList = liste;
        this.callback = callback;
        this.field = field;
    }
    
    /**
     * Henter ut valgene fra listene
     * 
     * @param e
     */
    public void valueChanged(ListSelectionEvent e) {
        boolean theSame = originalList != null ? originalList.equals(watchList) : watchList == null;
        log.debug("watch = " + watchList);
        log.debug("orginal = " + originalList);
        log.debug("valueChanged(e),  callback: " + !theSame + ", e: " + e);
        callback.setFieldChanged(null, field, !theSame, false);
    }
}

