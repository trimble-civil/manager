package no.mesta.mipss.core;

import java.sql.Connection;
import java.util.Properties;

import javax.swing.JFrame;

import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginCacheKey;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.ui.pluginlist.PluginListModel;

/**
 * Interface for aa kunne kommunisere med MIPSS Studio Loader.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface IMipssStudioLoader {

	/**
	 * Viser et gitt menynode
	 * 
	 * @param menyNode
	 */
	void activateMenuItem(MenynodeDTO menyNode);

	/**
	 * Legger en plugin i applikasjons vinduet hvis den ikke er loest ut
	 * 
	 * @param key
	 */
	void activatePlugin(PluginCacheKey key);

	/**
	 * Kalles naar en plugin skal stenges
	 * 
	 * @param plugin
	 * @return true hvis plugin lukkes
	 */
	boolean closePlugin(MipssPlugin plugin);

	/**
	 * Viser en melding en kort stund, legger den i kø hvis den har lavere pri
	 * en melding som vises
	 * 
	 * @param message
	 * @param priority
	 */
	void flashMessage(String message, int priority);

	/**
	 * Returnerer JFrame for applikasjonen. Praktisk hvis man feks vil koble en
	 * modal dialog til applikasjonsviduet.
	 * 
	 * @return
	 */
	JFrame getApplicationFrame();

	/**
	 * Brukerobjektet til paalogget bruker
	 * 
	 * @return
	 */
	Bruker getLoggedOnUser(boolean refresh);

	/**
	 * Brukernavnet til paalogget bruker
	 * 
	 * @return
	 */
	String getLoggedOnUserSign();

	/**
	 * Henter en plugininstans fra lastede plugins, eller laster den og
	 * returnerer den
	 * 
	 * @param pluginClass
	 * @return
	 */
	MipssPlugin getPlugin(Class<? extends MipssPlugin> pluginClass);

	/**
	 * Sjekker om en plugin kjører
	 * 
	 * @param pluginClass klassen som definerer pluginen
	 * 
	 * @return true hvis plugin er startet, false hvis plugin ikke kjører
	 */
	boolean isPluginRunning(Class<? extends MipssPlugin> pluginClass);

	/**
	 * Returnerer en liste over plugins som kjoerer
	 * 
	 * @return
	 */
	PluginListModel getRunningPluginsKeys();

	/**
	 * Haandterer en feilmeldning, viser den til brukeren
	 * 
	 * @param e
	 * @param source
	 * @param userMessage
	 */
	void handleException(Throwable e, Object source, String userMessage);

	/**
	 * Haandterer en feilmeldning, viser den til brukeren
	 * 
	 * @param e
	 * @param source
	 * @param sourceName
	 * @param userMessage
	 */
	void handleException(Throwable e, Object source, String sourceName, String userMessage);

	/**
	 * Logger kun en feilmelding i bakhaand
	 * 
	 * @param e
	 * @param source
	 * @param developerMessage
	 */
	void logException(Throwable e, Object source, String developerMessage);

	/**
	 * Lar meny handleren aapne meny punkter og plugins
	 * 
	 * @param menuNode
	 */
	void openMenuNode(MenynodeDTO menuNode);

	/**
	 * Kalles av en plugin naar den er ferdig med init
	 * 
	 * @param plugin
	 */
	void pluginReady(MipssPlugin plugin);

	/**
	 * Kalles naar en plugin loeses ut
	 * 
	 * @param plugin
	 */
	void popOut(MipssPlugin plugin);

	/**
	 * Sender og utfoerer en melding forutsatt at brukeren har tilgang og at det
	 * finnes et tilgangspunkt
	 * 
	 * @param message
	 */
	void sendMessage(PluginMessage<?, ?> message);

	/**
	 * Setter en beskjed i GUI (status linje nederst under meny og hovedpanelet)
	 * 
	 * @param message
	 */
	void setStatusMessage(String message);

	/**
	 * MIPSS Studio Loader vil da laste tilgangspunktet som en plugin og vise
	 * det i GUI
	 * 
	 * @param tilgangspunkt
	 * @param paramTypes
	 * @param paramValues
	 */
	void startAccessPoint(Tilgangspunkt tilgangspunkt, Class<?>[] paramTypes, Object[] paramValues);

	/**
	 * Starter en plugin
	 * 
	 * @param plugin
	 */
	void startPlugin(final MipssPlugin plugin);
	
	Driftkontrakt getSelectedDriftkontrakt();
	
	Connection getConnection();
	Properties getUserSettings();
	void saveProperties();
}
