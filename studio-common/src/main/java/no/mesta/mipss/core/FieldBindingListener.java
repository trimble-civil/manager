package no.mesta.mipss.core;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingListener;
import org.jdesktop.beansbinding.PropertyStateEvent;

/**
 * Lytter på selve bindingen/frikoplinegen mellom to objekter.
 * Brukes foreløpig kun til å blanke GUI komponeneten når bindingenopphører.
 * 
 * Det er en fordel at GUI komponentene setter sine egne navn (bruker {@code setName()}):
 * All debugging identifiserer GUI komponentene ved navn.
 * 
 * @author jorge
 */
public class FieldBindingListener implements BindingListener {
    private Logger logger = LoggerFactory.getLogger(FieldBindingListener.class);

    public void bindingBecameBound(Binding binding) {}
    
    /**
     * Seter {@code null} i verdien til GUI komponenten.
     * @param binding
     */
    public void bindingBecameUnbound(Binding binding) {
        Object target = binding.getTargetObject();
        Object param;
        if (target instanceof JCheckBox || target instanceof JButton) {
            param = Boolean.valueOf(false);
        }
        else {
            param = null;
        }
        try {
            binding.getTargetProperty().setValue(target, param);
        }
        catch(UnsupportedOperationException e) {
            logger.debug(e.getMessage() + ": " + binding.getName());
        }
    }
    
    public void syncFailed(Binding binding, Binding.SyncFailure syncFailure) {
        logger.debug("sync failed because: " + syncFailure + " for target " + ((Component)binding.getTargetObject()).getName());
    }
    
    public void synced(Binding binding) {
        logger.debug("synced " + ((Component)binding.getTargetObject()).getName());
    }
    
    public void sourceChanged(Binding binding, PropertyStateEvent propertyStateEvent) {
        logger.debug("source changed for target " + ((Component)binding.getTargetObject()).getName());
    }
    
    public void targetChanged(Binding binding, PropertyStateEvent propertyStateEvent) {
        logger.debug("target changed " + ((Component)binding.getTargetObject()).getName());
    }
}
