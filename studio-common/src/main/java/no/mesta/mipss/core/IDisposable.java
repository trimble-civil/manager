package no.mesta.mipss.core;

/**
 * Interface for objekter som må rydde opp etter seg
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface IDisposable {
	
	/**
	 * Metode for å rydde opp i minnet etter et objekt
	 * 
	 */
	public void dispose();
}
