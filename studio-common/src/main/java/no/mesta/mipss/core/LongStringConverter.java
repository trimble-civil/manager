/**
 * 
 */
package no.mesta.mipss.core;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Converter;

/**
 * @author jorsor
 * 
 */
public class LongStringConverter extends Converter<Long, String> {
	private static final Logger logger = LoggerFactory.getLogger(LongStringConverter.class);
	private boolean blankIsZero;
	private boolean nullIsZero;

	public LongStringConverter() {
		this(false, false);
	}

	public LongStringConverter(boolean blankIsZero, boolean nullIsZero) {
		this.blankIsZero = blankIsZero;
		this.nullIsZero = nullIsZero;
	}

	@Override
	public String convertForward(Long value) {
		if (value == null) {
			return null;
		} else {
			return String.valueOf(value);
		}
	}

	@Override
	public Long convertReverse(String value) {
		logger.trace("convertReverse(" + value + ") start");
		Long longValue;

		if (StringUtils.isBlank(value)) {
			if ((value == null && nullIsZero) || blankIsZero) {
				longValue = Long.valueOf(0L);
			} else {
				longValue = null;
			}
		} else {
			try {
				longValue = Long.valueOf(value);
			} catch (NumberFormatException e) {
				longValue = null;
				logger.warn("ConvertReverse NFE", e);
			}
		}

		return longValue;
	}

}
