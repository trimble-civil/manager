package no.mesta.mipss.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.commons.lang.StringUtils;

import org.jdesktop.beansbinding.Converter;

/**
 * <p>Konverterer mellom en <code>String</code> og <code>Double</code>,
 * <code>null</code> inn gir <code>null</code> ut - ellers konverteres det.</p>
 * 
 * <p>Metoden <code>convertForward(String)</code>
 * kan gi <code>NumberFormatException</code>, da gir også convertForward
 * <code>null</code></p>
 * 
 * @see java.lang.Double#valueOf(String)
 * @see java.lang.String#valueOf(Long)
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class StringDoubleConverter extends Converter<String,Double> {
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    private String failedDoubleConvert;
    
    /**
     * Konstruktør
     */
    public StringDoubleConverter() {
    }

    /** {@inheritDoc} */
    public Double convertForward(String value) {
        Double doubleValue = null;
        boolean errors = false;
        if(StringUtils.isBlank(value)) {
            doubleValue = null;
        } else {     
            try {
                doubleValue = Double.valueOf(value);
            } catch (NumberFormatException e) {
                errors = true;
            }
        }

        String old = failedDoubleConvert;
        if(errors) {
            failedDoubleConvert = "Ikke et flyttall: \"" + value + "\"";
            props.firePropertyChange("failedDoubleConvert", old, failedDoubleConvert);
        } else {
            failedDoubleConvert = null;
            props.firePropertyChange("failedDoubleConvert", old, failedDoubleConvert);
        }
        
        return doubleValue;
    }

    /** {@inheritDoc} */
    public String convertReverse(Double value) {
        if(value != null) {
            return String.valueOf(value);
        } else {
            return null;
        }
    }
    
    /**
     * Sist feilet String til Long konverteringsverdi
     * 
     * @return
     */
    public String getFailedDoubleConvert() {
        return failedDoubleConvert;
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
