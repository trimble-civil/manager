package no.mesta.mipss.core;

import no.mesta.mipss.persistence.Clock;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Tidens mester, kan manipulere tid
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class CalendarUtil {

	/**
	 * Returnerer en dato som er skrudd tilbake. Negative tall vil skrutiden
	 * fremover
	 * 
	 * @param anchor
	 * @param months
	 * @param days
	 * @param hours
	 * @return
	 */
	public static Date getAgo(Date anchor, int months, int days, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.HOUR_OF_DAY, -hours);
		calendar.add(Calendar.MONTH, -months);
		calendar.add(Calendar.DAY_OF_YEAR, -days);
		return calendar.getTime();
	}
	
	/**
	 * Returnerer en dato som er skrudd tilbake. Negative tall vil skrutiden
	 * fremover
	 * 
	 * @param anchor
	 * @param minute
	 * @return
	 */
	public static Date getAgo(Date anchor, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.MINUTE, -minute);
		return calendar.getTime();
	}

	/**
	 * Returnerer en dato som er skrudd tilbake. Negative tall vil skrutiden
	 * fremover
	 * 
	 * @param months
	 * @param days
	 * @param hours
	 * @return
	 */
	public static Date getAgo(int months, int days, int hours) {
		return getAgo(Clock.now(), months, days, hours);
	}

	/**
	 * Trekker fra dagene fra anker dato, negative tall her vil returnere en
	 * dato som er fremover i tid fra anker
	 * 
	 * @param anchor
	 * @param days
	 * @return
	 */
	public static Date getDaysAgo(Date anchor, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.DAY_OF_YEAR, -days);
		return calendar.getTime();
	}

	/**
	 * Trekker fra dagene fra anker dato, negative tall her vil returnere en
	 * dato som er fremover i tid fra anker
	 * 
	 * @param days
	 * @return
	 */
	public static Date getDaysAgo(int days) {
		return getDaysAgo(Clock.now(), days);
	}

	/**
	 * Returnerer en dato som er skrudd antall timer tilbake. Et negativt antall
	 * timer vil skru datoen frem
	 * 
	 * @param anchor
	 * @param hours
	 * @return
	 */
	public static Date getHoursAgo(Date anchor, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.HOUR_OF_DAY, -hours);
		return calendar.getTime();
	}

	/**
	 * Returnerer en dato som er skrudd antall timer tilbake. Et negativt antall
	 * timer vil skru datoen frem
	 * 
	 * @param hours
	 * @return
	 */
	public static Date getHoursAgo(int hours) {
		return getHoursAgo(Clock.now(), hours);
	}

	public static Date getMinutesAgo(Date anchor, int minutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.MINUTE, -minutes);
		return calendar.getTime();
	}

	public static Date getMinutesAgo(int minutes) {
		return getMinutesAgo(Clock.now(), minutes);
	}

	public static Date add(Date anchor, int field, int amount){
		Calendar c = Calendar.getInstance();
		c.setTime(anchor);
		c.add(field, amount);
		return c.getTime();
	}

	/**
	 * Returnerer en dato som er skrudd antall måneder tilbake. Et negativt
	 * antall måneder vil skru datoen frem.
	 * 
	 * @param anchor
	 * @param months
	 * @return
	 */
	public static Date getMonthsAgo(Date anchor, int months) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.MONTH, -months);
		return calendar.getTime();
	}

	public static Date getWeeksAgo(Date anchor, int weeks){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(anchor);
		calendar.add(Calendar.WEEK_OF_YEAR, -weeks);
		return calendar.getTime();
	}
	
	public static Date getWeeksAgo(int weeks){
		return getWeeksAgo(Clock.now(), weeks);
	}

	/**
	 * Returnerer en dato som er skrudd antall måneder tilbake. Et negativt
	 * antall måneder vil skru datoen frem.
	 * 
	 * @param months
	 * @return
	 */
	public static Date getMonthsAgo(int months) {
		return getMonthsAgo(Clock.now(), months);
	}

	/**
	 * Returnerer en dato somer skrudd ett ar tilbake i tid. Negative tall vil
	 * skru tiden fremover.
	 * 
	 * @param years
	 * @return skrudd dato
	 */
	public static Date getYearsAgo(int years) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(Clock.now());
		calendar.add(Calendar.YEAR, -years);
		return calendar.getTime();
	}
	
	public static Date getMidnight(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	public static Date getDayBefore(Date date) {
	    return instantToDate(Instant.ofEpochMilli(date.getTime()).minus(1, ChronoUnit.DAYS));
    }

	public static Date getYesterday(boolean atMidnight) {
		Date yesterday = instantToDate(Instant.now().minus(1, ChronoUnit.DAYS));
		return (atMidnight) ? round(yesterday, true) : yesterday;
	}

	public static Date getTomorrow(boolean atMidnight) {
		Date tomorrow = instantToDate(Instant.now().plus(1, ChronoUnit.DAYS));
		return (atMidnight) ? round(tomorrow, true) : tomorrow;
	}

	private static Date instantToDate(Instant instant) {
		ZonedDateTime atZone = instant.atZone(ZoneId.systemDefault());
		return new Date(atZone.toInstant().toEpochMilli());
	}

	/**
	 * Rund av klokkeslettet
	 * @param date
	 * @param first true hvis klokken skal være 00:00:00, false for 23:59:59
	 * @return
	 */
	public static Date round(Date date, boolean first){
		if(date == null) {
			return null;
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, first?0:59);
		cal.set(Calendar.MINUTE, first?0:59);
		cal.set(Calendar.HOUR_OF_DAY, first?0:23);
		return new Date(cal.getTimeInMillis());
	}
	
	public static Date toNearestFirstInMonth(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int i = c.get(Calendar.DAY_OF_MONTH);
		c.set(Calendar.DAY_OF_MONTH, 1);
		if (i>15){
			c.add(Calendar.MONTH, 1);
		}
		return c.getTime();
	}

	/**
	 * Sjekker om to perioder overlapper. Håndterer null-verdier. 
	 * 
	 *  f.eks to1==null && to2==null vil representere samme tidspunkt en gang i fremtiden.
	 *        from1==null && from2==null representerer samme tidspunkt en gang i fortiden. 
	 * 
	 * @throws RuntimeException hvis to1<from1 eller to2<from2.
	 * 
	 * @param from1 periode 1-fra (skal være før to1 i tid)
	 * @param to1 periode 1-til 
	 * @param from2 periode 2-fra (skal være før to2 i tid);
	 * @param to2 periode 2-til
	 * @param includeEndValue true hvis selve til-verdiene skal være med i sammenligningen. 
	 * 		
	 * @return true hvis periodene overlapper. 
	 */
	public static boolean isOverlappende(Date from1, Date to1, Date from2, Date to2, boolean includeEndValue){
		long start1 = from1!=null?from1.getTime():Long.MIN_VALUE;
		long end1 = to1!=null?to1.getTime():Long.MAX_VALUE;
		
		long start2 = from2!=null?from2.getTime():Long.MIN_VALUE;
		long end2 = to2!=null?to2.getTime():Long.MAX_VALUE;
		
		if (end1<start1)
			throw new RuntimeException("Til-dato (1) kan ikke være mindre enn fra-dato(1)");
		if (end2<start2)
			throw new RuntimeException("Til-dato (2) kan ikke være mindre enn fra-dato(2)");
		
		if (includeEndValue){
			return start1 <= end2 && start2 <= end1;
		}
		return start1 < end2 && start2 < end1;
	}

	public static int getWeekForDate(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.WEEK_OF_YEAR);
	}
	
	public static int getYearForDate(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.YEAR);
	}
	
	public static Date getFirstTimeInWeek(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
		c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        return c.getTime();
	}

	public static Date getLastTimeInWeek(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(getFirstTimeInWeek(date));
        c.add(Calendar.DATE, 7);
        c.add(Calendar.MILLISECOND, -1);
        return c.getTime();
	}
	
	public static Date getFirstDateOfFirstWeekInMonth(Long mnd, Long aar){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, aar.intValue());
		c.set(Calendar.MONTH, mnd.intValue());
		c.set(Calendar.DAY_OF_MONTH, 1);
		int firstWeekInMonth = c.get(Calendar.WEEK_OF_YEAR);
		c.clear();
		c.set(Calendar.YEAR, aar.intValue());
		c.set(Calendar.WEEK_OF_YEAR, firstWeekInMonth);
		return c.getTime();
	}

	private Date getLastSundayInMonth(Long mnd, Long aar){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mnd.intValue()+1);
		c.set(Calendar.DAY_OF_MONTH, 0); //siste dag i måneden (mnd)
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH)-6);//tar ikke med siste uken dersom den går over månedskiftet
		int weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
		c.clear();
		c.set(Calendar.YEAR, aar.intValue());
		c.set(Calendar.WEEK_OF_YEAR, weekOfYear);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		return c.getTime();
	}
	
	public static Date getNextOccurrenceOfDayOfWeek(int dayOfWeek, int hourOfDay){
		Calendar c = Calendar.getInstance();
		c.setTime(Clock.now());
		c.set(Calendar.HOUR_OF_DAY, hourOfDay);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.getTime();//trigger cal update
		c.set(Calendar.DAY_OF_WEEK, dayOfWeek);
		c.getTime();//trigger cal update
		
		Calendar cc = Calendar.getInstance();
		cc.setTime(Clock.now());
		//dagen var i forrige måned, hent neste uke.
		if (c.get(Calendar.MONTH)<cc.get(Calendar.MONTH)){
			c.add(Calendar.WEEK_OF_YEAR, 1);
			c.getTime();//trigger cal update
		}
		
		Date firstWednesdayInMonth = c.getTime();
		if (firstWednesdayInMonth.after(Clock.now())){//datoen har ikke intruffet enda
			return firstWednesdayInMonth;
		}
		//Datoen har intruffet hent første forekomst i neste måned av ukedagen
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
		
		return c.getTime();
	}
	
	public static String getDisplayNameForMonth(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("NO", "no"));
	}
	
	public static String getDisplayNameForWeek(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		String weekday = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, new Locale("NO", "no"));
		return weekday.substring(0, 1).toUpperCase() + weekday.substring(1);
	}

	public static Date getFirstDateCurrentMonth() {
		return getFirstDateGivenMonth(new Date());
	}

	public static Date getLastDateCurrentMonth() {
		return getLastDateGivenMonth(new Date());
	}

	public static ZonedDateTime convertDateToZonedDateTime(Date d) {
		LocalDate local = LocalDate.from(ZonedDateTime.ofInstant(d.toInstant(), ZoneOffset.systemDefault()));
		return local.atStartOfDay(ZoneOffset.systemDefault());
	}

	public static Date getFirstDateGivenMonth(Date month) {
		ZonedDateTime start = convertDateToZonedDateTime(month).with(TemporalAdjusters.firstDayOfMonth());
		return Date.from(start.toInstant());
	}

	public static Date getLastDateGivenMonth(Date month) {
		ZonedDateTime end = convertDateToZonedDateTime(month).with(TemporalAdjusters.lastDayOfMonth());
		return Date.from(end.toInstant());
	}

	public static Date getFirstTimeInMonth(Date month) {
		return getFirstDateGivenMonth(month);
	}

	public static Date getLastTimeInMonth(Date month) {
		ZonedDateTime zoned = convertDateToZonedDateTime(month);
		zoned = zoned.with(TemporalAdjusters.lastDayOfMonth()).withHour(23).withMinute(59).withSecond(59).withNano(999);
		return Date.from(zoned.toInstant());
	}

}