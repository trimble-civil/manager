package no.mesta.mipss.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.commons.lang.StringUtils;

import org.jdesktop.beansbinding.Converter;

/**
 * <p>Konverterer mellom en <code>String</code> og <code>Long</code>,
 * <code>null</code> inn gir <code>null</code> ut - ellers konverteres det.</p>
 * 
 * <p>Metoden <code>convertForward(String)</code>
 * kan gi <code>NumberFormatException</code>, da gir også convertForward
 * <code>null</code></p>
 * 
 * @see java.lang.Long#valueOf(String)
 * @see java.lang.String#valueOf(Long)
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class StringLongConverter extends Converter<String,Long> {
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    private String failedLongConvert ;
    
    /**
     * Konstruktør
     */
    public StringLongConverter() {
    }

    /** {@inheritDoc} */
    public Long convertForward(String value) {
        Long longValue = null;
        boolean errors = false;
        if(StringUtils.isBlank(value)) {
            longValue = null;
        } else {     
            try {
                longValue = Long.valueOf(value);
            } catch (NumberFormatException e) {
                errors = true;
            }
        }

        String old = failedLongConvert;
        if(errors) {
            failedLongConvert = "Ikke et heltall: \"" + value + "\"";
            props.firePropertyChange("failedLongConvert", old, failedLongConvert);
        } else {
            failedLongConvert = null;
            props.firePropertyChange("failedLongConvert", old, failedLongConvert);
        }
        
        return longValue;
    }

    /** {@inheritDoc} */
    public String convertReverse(Long value) {
        if(value != null) {
            return String.valueOf(value);
        } else {
            return null;
        }
    }
    
    /**
     * Sist feilet String til Long konverteringsverdi
     * 
     * @return
     */
    public String getFailedLongConvert() {
        return failedLongConvert;
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
