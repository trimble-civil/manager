package no.mesta.mipss.core;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;

/**
 * Sier om en string er blank eller ikke
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 * @param <D>
 */
public class PunktveirefValidator<D extends PropertyChangeSource> extends BlankFieldValidatorAdapter<D, Punktveiref> {
	private static Logger logger = LoggerFactory.getLogger(PunktveirefValidator.class);
	private static final MipssField BEAN = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
	private boolean oldBlank;
	private static final Date notValidateBefore;
	private static final String APP = "studio.mipss.field";
	private static final String APP_VALUE = "veirefIgnoreMonths";

	static {
		Konfigparam param = KonfigparamPreferences.getInstance().hentEnForApp(APP, APP_VALUE);
		if (param != null && param.getVerdi() != null) {
			Integer monthsBack = Integer.valueOf(param.getVerdi());

			notValidateBefore = CalendarUtil.getMonthsAgo(monthsBack);
			logger.debug("notValidateBefore = " + notValidateBefore);
		} else {
			notValidateBefore = null;
		}
	}

	public PunktveirefValidator(D detail, String fieldName) {
		super(detail, fieldName);

		Binding<D, String, PunktveirefValidator<D>, String> b = BindingHelper.createbinding(detail,
				"${veiref.textForGUI}", this, "veiText");
		b.bind();
	}

	public void setVeiText(String t) {
		logger.trace("setVeiText(" + t + ") start");
		props.firePropertyChange("blank", oldBlank, isBlank());
	}

	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(Punktveiref value) {
		logger.trace("calculateBlank(" + value + ") start");
		oldBlank = !isValid(value);
		return oldBlank;
	}

	/**
	 * Validerer et punktveiref objekt. Lik kode som i JRoadDialogue
	 * 
	 * @param veiref
	 * @return
	 */
	public static boolean isValid(Punktveiref veiref) {
		if (veiref == null) {
			return false;
		}

		if (veiref.getFraDato() != null && notValidateBefore != null && notValidateBefore.after(veiref.getFraDato())) {
			return true;
		}

		if (veiref.getFylkesnummer() == 0 || veiref.getFylkesnummer() == 13) {
			return false;
		}

		if (veiref.getVeinummer() == 0) {
			return false;
		}

		if (StringUtils.isBlank(veiref.getVeikategori()) || StringUtils.isBlank(veiref.getVeistatus())) {
			return false;
		}

		if (veiref.getHp() == 0) {
			return false;
		}

		if (StringUtils.equals("K", veiref.getVeikategori())) {
			if (veiref.getKommunenummer() == 0) {
				return false;
			}
		} else {
			if (veiref.getKommunenummer() > 0) {
				return false;
			}
		}

		return BEAN.validatePunktveiref(veiref);
	}
}
