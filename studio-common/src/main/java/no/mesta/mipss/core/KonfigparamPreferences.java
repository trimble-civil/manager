package no.mesta.mipss.core;

import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.KonfigparamPK;
import no.mesta.mipss.persistence.applikasjon.Konfigparamlogg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Gir tilgang til konfig param
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class KonfigparamPreferences implements KonfigParam {

    private static Logger log = LoggerFactory.getLogger(KonfigparamPreferences.class);

	private static KonfigParam konfigParam;
    private static KonfigparamPreferences konfigparamPreferences= null;
    
    /**
     * Denne klassen er en singleton
     * 
     */
    private KonfigparamPreferences() {
    }
    
    /**
     * Gir tilgang til KonfigparamPreferences
     * 
     * @return
     */
    public static KonfigparamPreferences getInstance() {
        if(konfigparamPreferences == null) {
            synchronized(KonfigparamPreferences.class) {
                log.debug("Creating KonfigparamPreferences");
                konfigparamPreferences = new KonfigparamPreferences();
            }
        }
        
        return konfigparamPreferences;
    }
    
    /** {@inheritDoc} */
    @Override
	public List<String> hentApplikasjonListe() {
		return getKonfigParam().hentApplikasjonListe();
	}
    
    /** {@inheritDoc} */
    public List<Konfigparam> hentAlleKonfigparam() {
        return getKonfigParam().hentAlleKonfigparam();
    }
    
    /** {@inheritDoc} */
    public List<Konfigparam> hentKonfigForApplikasjon(String app) {
        return getKonfigParam().hentKonfigForApplikasjon(app);
    }

    /** {@inheritDoc} */
    public Konfigparam lagreKonfig(Konfigparam konfigparam) {
        return getKonfigParam().lagreKonfig(konfigparam);
    }

    /** {@inheritDoc} */
    public Konfigparam oppdaterKonfig(Konfigparam konfigparam) {
        return getKonfigParam().oppdaterKonfig(konfigparam);
    }

    /** {@inheritDoc} */
    public void slettKonfig(String app, String navn) {
        getKonfigParam().slettKonfig(app, navn);
    }

    /** {@inheritDoc} */
    public Konfigparam hentEnForApp(String app, String navn) {
        log.debug("hentEnForApp("+app+","+navn+")");
        return getKonfigParam().hentEnForApp(app, navn);
    }
    
    /** {@inheritDoc} */
    public Konfigparam henEnForApp(KonfigparamPK key) {
    	return getKonfigParam().hentEnForApp(key.getApplikasjon(), key.getNavn());	
	}

    /**
     * Henter konfigurasjon
     * 
     * @param app
     * @param navn
     * @return
     */
    public Konfigparam hentBrukerEllerGenerellKonfig(String app, String navn) {
        Konfigparam konfig = hentEnForApp(app, navn);
        return konfig;
    }
    
    /**
     * Gir ut bønna som kan slå opp i databasen for KONFIGPARAM
     * 
     * @return
     */
    public KonfigParam getKonfigParam() {
        if(konfigParam == null) {
            log.debug("Looking up KonfigParamBean");
            konfigParam = BeanUtil.lookup(KonfigParam.BEAN_NAME, KonfigParam.class);
        }
        
        return konfigParam;
    }

	@Override
	public List<Konfigparamlogg> hentLoggForKonfigparam(Konfigparam p) {
		return getKonfigParam().hentLoggForKonfigparam(p);
	}

	@Override
	public String hentVerdiForApp(String app, String navn) {
		return konfigParam.hentEnForApp(app, navn).getVerdi();
	}
}
