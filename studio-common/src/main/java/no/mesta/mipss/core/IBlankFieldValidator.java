package no.mesta.mipss.core;

import no.mesta.mipss.common.PropertyChangeSource;

/**
 * Metoder i en null/blank validator
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <T>
 */
public interface IBlankFieldValidator<T> extends PropertyChangeSource{
	
	/**
	 * Avgjør om en verdi regnes som blank
	 * 
	 * @param value
	 * @return
	 */
	public boolean calculateBlank(T value);
	
	/**
	 * Avgjør om et felt er blankt
	 * 
	 * @return
	 */
	public boolean isBlank();
}
