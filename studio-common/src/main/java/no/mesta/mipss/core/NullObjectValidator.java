package no.mesta.mipss.core;

import no.mesta.mipss.common.PropertyChangeSource;

/**
 * Sier om en string er blank eller ikke
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 * @param <D>
 */
public class NullObjectValidator<D extends PropertyChangeSource, O>
		extends BlankFieldValidatorAdapter<D, O> {

	public NullObjectValidator(D detail, String fieldName) {
		super(detail, fieldName);
	}

	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(O value) {
		logger.trace("calculateBlank(" + value + ") start");
		return value == null;
	}
}
