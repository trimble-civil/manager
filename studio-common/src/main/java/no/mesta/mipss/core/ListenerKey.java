package no.mesta.mipss.core;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
/**
 * Klasse for å registere felt lyttere
 * 
 */

public class ListenerKey {
	Class<?> clazz;
	String field;
	Object instance;

	/**
	 * Konstruktør
	 * 
	 * @param clazz
	 * @param field
	 */
	public ListenerKey(Class<?> clazz, String field, Object instance) {
		this.clazz = clazz;
		this.field = field;
		this.instance = instance;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o.getClass().equals(ListenerKey.class))) {
			return false;
		}

		ListenerKey other = (ListenerKey) o;

		return new EqualsBuilder().append(field, other.field).append(clazz, other.clazz).append(instance,
				other.instance).isEquals();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(field).append(clazz).append(instance).toHashCode();
	}
}
