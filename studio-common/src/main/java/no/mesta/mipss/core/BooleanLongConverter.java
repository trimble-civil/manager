/**
 * 
 */
package no.mesta.mipss.core;

import org.jdesktop.beansbinding.Converter;

/**
 * Konverterer mellom boolean og Long. Til bruk når GUI-komponenten har et boolean felt
 * og MIPSS entiteten har et Long felt.
 * @author jorge
 *
 */
public class BooleanLongConverter extends Converter<Boolean, Long> {

	@Override
	public Long convertForward(Boolean value) {
		if(value == null) {
			return Long.valueOf(0);
		}
		else if(value == true) {
            return Long.valueOf(1);
        } 
		else {
            return Long.valueOf(0);   
        }
	}

	@Override
	public Boolean convertReverse(Long value) {
		if(value == null) {
            return false;
        } 
		else if(value == 0) {
            return false;
        } 
		else {
            return true;
        }
	}

}
