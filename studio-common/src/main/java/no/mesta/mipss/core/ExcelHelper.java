package no.mesta.mipss.core;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.CellView;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.read.biff.BiffException;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCell;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.ImageUtil;

public class ExcelHelper {
	private WritableFont times10font = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, true);
	private WritableCellFormat times10format = new WritableCellFormat(times10font);
	private WritableCellFormat headerFormat = new WritableCellFormat();
	private WritableFont titleFont = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD, true);
	private WritableCellFormat titleFormat = new WritableCellFormat(titleFont);
	
	private Map<Class<?>, WritableCellFormat> formats = new HashMap<Class<?>, WritableCellFormat>();
	private File tempFile;
	private String dateFormat;
	
	public ExcelHelper(){
		try {
			headerFormat.setBackground(Colour.GRAY_25);
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Lager en ny arkfane, 0-basert indeks (nr)
	 * @param title
	 * @param workbook
	 * @param nr
	 * @return
	 */
	public WritableSheet createNewSheet(String title, WritableWorkbook workbook, int nr){
		WritableSheet sheet = workbook.createSheet(title, nr);
		return sheet;
	}
	/**
	 * Henter ut et faneark fra workbook
	 * @param index
	 * @param workbook
	 * @return
	 */
	public WritableSheet getSheet(int index, WritableWorkbook workbook){
		return workbook.getSheet(index);
	}
	
	/**
	 * 
	 * @param sheet
	 * @param row
	 * @param headers
	 */
	public void writeHeaders(WritableSheet sheet, int row, int cIndex, boolean autoadjustWidth, String... headers){
		for (String title:headers){
			addCell(sheet, new Label(cIndex, row, title, times10format));
			if (autoadjustWidth)
				sheet.setColumnView(cIndex, title.length());
			cIndex++;
		}
	}
	
	
	public void writeTitle(WritableSheet sheet, int row, String title){
		addCell(sheet, new Label(0, row, title, titleFormat));
	}
	
	public int createReportHeader(WritableSheet sheet, Map<String,String> values, int row, int totalSize){
		for (String key:values.keySet()){
			addCell(sheet, new Label(0, row, key, headerFormat));
			addCell(sheet, new Label(1, row, values.get(key), headerFormat));
			mergeCells(sheet, 1, row, totalSize, row);
			row++;
		}
		return row;
	}
	
	public int createReportHeader(WritableSheet sheet, Map<String,String> values, List<String> valuesOrder, int row, int totalSize){
		for (String key:valuesOrder){
			addCell(sheet, new Label(0, row, key, headerFormat));
			addCell(sheet, new Label(1, row, values.get(key), headerFormat));
			mergeCells(sheet, 1, row, totalSize, row);
			row++;
		}
		return row;
	}
	
	public void addImage(WritableSheet sheet, Image image, int column, int row, int colSize, int rowSize){
		byte[] imageBytes = ImageUtil.imageToBytes(image);
		sheet.addImage(new WritableImage(column, row, colSize, rowSize, imageBytes));
	}
	
	public DateTime getDateTimeCell(int row, int col, Date data){
		WritableCellFormat dateFormat = formats.get(Date.class);
		if (dateFormat == null) {
			if (this.dateFormat==null){
				this.dateFormat = MipssDateFormatter.SHORT_TIME_FORMAT;
			}
			DateFormat customDateFormat = new DateFormat(this.dateFormat);
			dateFormat = new WritableCellFormat(customDateFormat);
			formats.put(Date.class, dateFormat);
		}
		return new DateTime(col, row, (Date) data, dateFormat);
	}
	
	public Label getStringCell(int row, int col, String data, CellFormat format){
		return new Label(col, row, data, format);
	}
	
	public Formula getFormulaCell(int row, int col, String formula, CellFormat format){
		Formula f = new Formula(col, row, formula, format);
		
		return f;
	}
	
	public jxl.write.Number getDoubleCell(int row, int col, Double data, CellFormat format){
		return new jxl.write.Number(col, row, data, format);
	}
	
	public jxl.write.Number getIntegerCell(int row, int col, Integer data, CellFormat format){
		return new jxl.write.Number(col, row, data, format);
	}
	
	public jxl.write.Number getLongCell(int row, int col, Long data, CellFormat format){
		return new jxl.write.Number(col, row, data, format);
	}
	
	public void writeData(WritableSheet sheet, int row, int col, Object data, boolean wrapText){
		writeData(sheet, row, col, data);
		Cell cell = sheet.getCell(col, row);
		if (cell!=null){
			try {
				if (cell.getCellFormat()!=null){
					WritableCellFormat cf = new WritableCellFormat(cell.getCellFormat());
					cf.setWrap(true);
				}
			} catch (WriteException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void writeData(WritableSheet sheet, int row, int col, Object data, Colour background, String tooltip){
		if (data!=null)
			writeData(sheet, row, col, data);
		else{
			Label label = new Label(col, row, "");
			addCell(sheet, label);
		}
		Cell cell = sheet.getCell(col, row);
		if (cell instanceof WritableCell){
			WritableCell wcell = (WritableCell)cell;
			WritableCellFormat cf = new WritableCellFormat();
			if (wcell.getCellFormat()!=null)
				cf = new WritableCellFormat(wcell.getCellFormat());
			if (tooltip!=null&&!tooltip.equals("")){
				WritableCellFeatures wcf = new WritableCellFeatures();
				if (wcell.getCellFeatures()!=null)
					wcf = new WritableCellFeatures(wcell.getCellFeatures());
				
				wcf.setComment(tooltip);
				wcell.setCellFeatures(wcf);
			}
			try {
				cf.setBackground(background);
				if (background==Colour.WHITE)
					cf.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_25);
				
			} catch (WriteException e) {
				e.printStackTrace();
			}
			wcell.setCellFormat(cf);
		}
	}
	public void writeData(WritableSheet sheet, int row, int col, Object data){
		int colWidth = 0;
		if (data != null) {
			Class<?> clazz = data.getClass();
			if (clazz.equals(String.class)) {
				Label label = new Label(col, row, (String) data);
				
				addCell(sheet, label);
			} else if (clazz.equals(Long.class)) {
				WritableCellFormat floatFormat = formats.get(Long.class);
				if (floatFormat == null) {
					floatFormat = new WritableCellFormat(NumberFormats.INTEGER);
					formats.put(Long.class, floatFormat);
				}
				addCell(sheet, new jxl.write.Number(col, row, (Long) data, floatFormat));
			} else if (clazz.equals(Double.class)) {
				WritableCellFormat floatFormat = formats.get(Double.class);
				if (floatFormat == null) {
					floatFormat = new WritableCellFormat(NumberFormats.FLOAT);
					formats.put(Double.class, floatFormat);
				}
				addCell(sheet, new jxl.write.Number(col, row, (Double) data, floatFormat));
			} else if (clazz.equals(Integer.class)) {
				WritableCellFormat integerFormat = formats.get(Integer.class);
				if (integerFormat == null) {
					integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
					formats.put(Integer.class, integerFormat);
				}
				addCell(sheet, new jxl.write.Number(col, row, (Integer) data, integerFormat));
			} 
			else if (clazz.equals(Date.class)) {
				WritableCellFormat dateFormat = formats.get(Date.class);
				if (dateFormat == null) {
					if (this.dateFormat==null){
						this.dateFormat = MipssDateFormatter.SHORT_TIME_FORMAT;
					}
					DateFormat customDateFormat = new DateFormat(this.dateFormat);
					dateFormat = new WritableCellFormat(customDateFormat);
					formats.put(Date.class, dateFormat);
				}

				addCell(sheet, new DateTime(col, row, (Date) data, dateFormat));
			} else if (clazz.equals(Boolean.class)) {
				WritableCellFormat integerFormat = formats.get(Integer.class);
				if (integerFormat == null) {
					integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
					formats.put(Integer.class, integerFormat);
				}
				Boolean value = (Boolean) data;
				Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
				addCell(sheet, new jxl.write.Number(col, row, number , integerFormat));
			} else{
				try{
					WritableCellFormat integerFormat = formats.get(Integer.class);
					if (integerFormat == null) {
						integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
						formats.put(Integer.class, integerFormat);
					}
					addCell(sheet, new jxl.write.Number(col, row, Integer.parseInt(data.toString()), integerFormat));
				}catch (NumberFormatException e){
					try{
						Double.parseDouble(data.toString());
						WritableCellFormat floatFormat = formats.get(Double.class);
						if (floatFormat == null) {
							floatFormat = new WritableCellFormat(NumberFormats.FLOAT);
							formats.put(Double.class, floatFormat);
						}
						addCell(sheet, new jxl.write.Number(col, row, Double.parseDouble(data.toString()), floatFormat));
					}catch (NumberFormatException ex){
						try{
							addCell(sheet, new Label(col, row, data.toString()));
						} catch (Throwable e1){
						}
					}
				}
				
			}
			
			colWidth = computeNewColWidth(colWidth, data);
		}
		
		CellView cellView = sheet.getColumnView(col);
		if (colWidth > (cellView.getSize() / 256)) {
			sheet.setColumnView(col, colWidth);
		}
	}
	public void setColumnView(WritableSheet sheet, int col, String width){
		sheet.setColumnView(col, computeNewColWidth(0, width));
	}
	private int computeNewColWidth(int colWidth, Object data) {
		int width = 0;
		if (data instanceof String) {
			width = ((String) data).length();
			width = width > 200 ? 200 : width;
		} else if (data instanceof Long) {
			width = String.valueOf((Long) data).length();
		} else if (data instanceof Double) {
			width = String.valueOf((Double) data).length();
		} else if (data instanceof Integer) {
			width = String.valueOf((Integer) data).length();
		} else if (data instanceof Date) {
			width = MipssDateFormatter.SHORT_DATE_TIME_FORMAT.length();
		} else if (data instanceof Boolean) {
			width = 2;
		}

		return Math.max(colWidth, width);
	}
	/**
	 * Legger til en celle i arket
	 * @param sheet
	 * @param cell
	 */
	public void addCell(WritableSheet sheet, WritableCell cell) {
		try {
			
			sheet.addCell(cell);
		} catch (RowsExceededException e) {
			throw new IllegalStateException(e);
		} catch (WriteException e) {
			throw new IllegalStateException(e);
		}
	}
	
	public void mergeCells(WritableSheet sheet, int col1, int row1, int col2, int row2){
		try {
			sheet.mergeCells(col1, row1, col2, row2);
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Opprett en ny, tom workbook
	 * @return
	 */
	public WritableWorkbook createNewWorkbook(String filename){
		tempFile = createTempFile(filename, ".xls");
		WritableWorkbook workbook = createWorkbook(tempFile);
		return workbook;
	}
	
	public File getExcelFile(){
		return tempFile;
	}
	
	/**
	 * Oppretter en ny workbook
	 * @param tempFile
	 * @return
	 */
	private WritableWorkbook createWorkbook(File tempFile) {
		this.tempFile = tempFile;
		WritableWorkbook workbook = null;
		try {
			workbook = Workbook.createWorkbook(tempFile);
			
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return workbook;
	}
	public WritableWorkbook createWorkbook(File tempFile, InputStream is){
		this.tempFile = tempFile;
		WritableWorkbook workbook = null;
		try {
			workbook = Workbook.createWorkbook(tempFile, Workbook.getWorkbook(is));
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return workbook;
	}
	
	/**
	 * lager en tempfil
	 * @param prefix
	 * @param suffix
	 * @return
	 */
	private File createTempFile(String prefix, String suffix) {
		File tempFile = null;
		try {
			tempFile = File.createTempFile(prefix, suffix);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return tempFile;
	}
	
	/**
	 * Skriver til disk og lukker 
	 * @param workbook
	 */
	public void closeWorkbook(WritableWorkbook workbook) {
		try {
			workbook.write();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		try {
			workbook.close();
		} catch (WriteException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	public void setDateFormat(String dateFormat){
		this.dateFormat = dateFormat;
	}
	public String getDateFormat(){
		return this.dateFormat;
	}
	
	public InputStream getExcelTemplate(String fileName){
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(fileName);
		if (url != null) {
			try{
			URLConnection connection = url.openConnection();
			if (connection != null) {
			    connection.setUseCaches(false);
			    return connection.getInputStream();
			}
			} catch (Exception e){
				throw new RuntimeException(e);
			}
		}
		return null;
	}
}
