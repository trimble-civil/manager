package no.mesta.mipss.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.event.DocumentEvent;

import javax.swing.event.DocumentListener;

import javax.swing.text.JTextComponent;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Lytter på et text felt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class FieldListener<T> implements DocumentListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private String originalValue;
    private JTextComponent textField;
    private Method method;
    private String beanField;
    private IBeanFieldChange callback;
    private T bean;

    public FieldListener(T bean, String originalValue, JTextComponent textField,
                         String beanField, IBeanFieldChange callback) {
        this.originalValue = originalValue;
        this.beanField = beanField;
        this.textField = textField;
        this.callback = callback;
        this.bean = bean;

        try {
            method = 
                    bean.getClass().getMethod("set" + beanField, new Class<?>[] { String.class });
        } catch (NoSuchMethodException e) {
            log.debug("FieldListener(), e: " + e);
        }
    }

    public void changeValue(DocumentEvent e) {
        String value = textField.getText();

        try {
            method.invoke(bean, value);
        } catch (IllegalAccessException iae) {
            log.debug("changeValue(e), e:" + iae);
        } catch (InvocationTargetException ite) {
            log.debug("changeValue(e), e:" + ite);
        }

        callback.setFieldChanged(bean, beanField, !StringUtils.equals(originalValue, value), false);
    }

    public void insertUpdate(DocumentEvent e) {
        changeValue(e);
    }

    public void removeUpdate(DocumentEvent e) {
        changeValue(e);
    }

    public void changedUpdate(DocumentEvent e) {
        // Ikke brukt av felter
    }
}
