package no.mesta.mipss.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.MipssObservableList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Property;
import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;

/**
 * Lytter til et felt på en bønne og notifiserer når verdien ikke er lik den
 * orginale verdien.
 * 
 * Denne klassen spesialbehandler Lister. Hvis feltet den skal lytte på er av
 * typen java.util.List vil den om nødvendig gjøre listen om til ObservableList
 * og sette den tilbake på bønna (derfor må bønna den lytter på ikke fyre i set
 * metoden hvis det settes en ObservableList). Når listen er konvertert så
 * registerer den seg som lytter på listen. For hver event på listen beregner på
 * nytt changed status(calculateChanged) og fyrer det til sine lyttere samt til
 * IBeanFieldChange callback.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class BeanFieldListener implements ObservableListListener, IDisposable {
	private String beanField;
	private Object beanInstance;
	private Binding<Object, Object, BeanFieldListener, Object> binding;
	private final BindingGroup bindingGroup;
	private IBeanFieldChange callback;
	private Object currentValue;
	private Logger logger = LoggerFactory.getLogger(BeanFieldListener.class);
	private Object originalValue;
	private PropertyChangeSupport props = new PropertyChangeSupport((Object) this);

	/**
	 * Konstruktør, setter opp binning mellom bønna og denne klassen slik at den
	 * vet når verdien har endret seg.
	 * 
	 * @param clazz
	 * @param beanInstance
	 * @param beanField
	 * @param bindingGroup
	 * @param callback
	 */
	public BeanFieldListener(Object beanInstance, String beanField, BindingGroup bindingGroup, IBeanFieldChange callback) {
		this.beanInstance = beanInstance;
		this.beanField = beanField;
		this.callback = callback;
		this.bindingGroup = bindingGroup;

		setOrigAndCurrent();

		createBinding();

//		debug("created, originalValue=" + originalValue + ", currentValue=" + currentValue);
	}

	public BeanFieldListener(Object beanInstance, String beanField, BindingGroup bindingGroup, IBeanFieldChange callback, Object originalValue) {
		this.beanInstance = beanInstance;
		this.beanField = beanField;
		this.callback = callback;
		this.bindingGroup = bindingGroup;
		this.originalValue = originalValue;
		currentValue = originalValue;
		createBinding();
	}
	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
//		debug("addPropertyChangeListener(" + l + ")");
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
//		debug("addPropertyChangeListener(" + propName + "," + l + ")");
		props.addPropertyChangeListener(propName, l);
	}

	/**
	 * Sjekker om feltet har endret seg
	 * 
	 * @param property
	 * @return
	 */
	private boolean calculateChanged(Object property) {
//		debug("{" + originalValue + "}.calculateChanged(" + property + ")"); TODO denne debuggen fører til treghet i rammeverket!!
		if (property instanceof String){
			if (originalValue==null&&"".equals(property)){
				return true;
			}
		}
		return (originalValue == null ? !(originalValue == property) : !originalValue.equals(property));
	}

	/**
	 * Sjekker om en liste endrer seg
	 * 
	 * @param list
	 */
	private void calculateListChange(List<?> list) {
		boolean old = isChanged();
		currentValue = copyList((List<?>) list);
		boolean changed = isChanged();
		fireChanged(old, changed);
	}

	/**
	 * Lager en grunn kopi av en liste
	 * 
	 * @param list
	 * @return
	 */
	private Object copyList(List<?> list) {
		List<Object> copiedList = new ArrayList<Object>();
		for (Object o : list) {
			copiedList.add(o);
		}

		return copiedList;
	}

	private void createBinding() {
		binding = BindingHelper.createbinding(beanInstance, beanField, this, "targetProperty", BindingHelper.READ);
		bindingGroup.addBinding(binding);
	}

	/**
	 * Logger debug beskjed med instansinfo
	 * 
	 * @param s
	 */
	private void debug(String s) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("{" + beanInstance.getClass().getName() + "/" + beanField + "}." + s);
//		}
	}

	@Override
	public void dispose() {
		currentValue = null;
		originalValue = null;
		binding = null;

		if (props != null && props.getPropertyChangeListeners() != null) {
			for (PropertyChangeListener l : props.getPropertyChangeListeners()) {
				props.removePropertyChangeListener(l);
			}
			props = null;
		}
	}

	/** {@inheritDoc} */
	@Override
	public void finalize() {
//		debug("finalize() - good bye");
	}

	/**
	 * Sender melding til lytterne til "changed" at feltet denne lytter på har
	 * endret seg
	 * 
	 * @param old
	 * @param changed
	 */
	private void fireChanged(boolean old, boolean changed) {
		callback.setFieldChanged(beanInstance, beanField, changed, false);
//		debug("fireChanged() for beanField " + beanField + ": old = " + old + ", changed = " + changed);
		props.firePropertyChange("changed", old, changed);
	}

	public Binding<Object, Object, BeanFieldListener, Object> getBinding() {
		return binding;
	}

	/**
	 * Bruker enten BeanProperty eller en ELProperty for å hente den orogonale
	 * verdien i den lyttede egenskapen.
	 * 
	 * @param beanInstance
	 * @param beanField
	 * @return den opprinnelige verdien.
	 */
	private Object getOriginalValue(Object beanInstance, String beanField) {
		debug(beanInstance + " " + beanField);
		Property<Object, Object> property = PropertyUtil.createProperty(beanField);
		Object value;
		try {
			value = property.getValue(beanInstance);
		} catch (Throwable t) {
			value = null;
		}
		return value;
	}

	/**
	 * Sier om feltet klassen lytter til er endret
	 * 
	 * @return
	 */
	public boolean isChanged() {
		return calculateChanged(currentValue);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public void listElementPropertyChanged(ObservableList observableList, int i) {
//		debug("listElementPropertyChanged()");
		calculateListChange(observableList);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public void listElementReplaced(ObservableList observableList, int i, Object object) {
//		debug("listElementReplaced()");
		calculateListChange(observableList);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public void listElementsAdded(ObservableList observableList, int i, int i1) {
//		debug("listElementsAdded()");
		calculateListChange(observableList);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public void listElementsRemoved(ObservableList observableList, int i, List list) {
//		debug("listElementsRemoved()");
		calculateListChange(observableList);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
//		debug("removePropertyChangeListener(" + l + ")");
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
//		debug("removePropertyChangeListener(" + propName + "," + l + ")");
		props.removePropertyChangeListener(propName, l);
	}

	public void reset() {
		boolean old = isChanged();
		unbind();
		setOrigAndCurrent();
		createBinding();
		binding.bind();
		binding.refreshAndNotify();
		boolean now = isChanged();
		fireChanged(old, now);
	}
	
	public void reset(boolean force){
		boolean old = isChanged();
		unbind();
		setOrigAndCurrent();
		createBinding();
		binding.bind();
		binding.refreshAndNotify();
		boolean now = isChanged();
		fireChanged(old, now, force);
	}

	private void fireChanged(boolean old, boolean changed, boolean force) {
		callback.setFieldChanged(beanInstance, beanField, changed, force);
//			debug("fireChanged() for beanField " + beanField + ": old = " + old + ", changed = " + changed);
		props.firePropertyChange("changed", old, changed);
	}

	@SuppressWarnings("unchecked")
	private void setOrigAndCurrent() {
		if (originalValue != null) {
			if (originalValue instanceof ObservableList) {
				((ObservableList) originalValue).removeObservableListListener(this);
			}
		}

		originalValue = getOriginalValue(beanInstance, beanField);

		// Sørg for å ha en liste av de opprinnelige verdiene
		if (originalValue instanceof List) {
			if (!(originalValue instanceof ObservableList)) {
				originalValue = new MipssObservableList((List) originalValue);
				Property<Object, Object> p = PropertyUtil.createProperty(beanField);
				p.setValue(beanInstance, originalValue);
			}
			((ObservableList) originalValue).addObservableListListener((ObservableListListener) this);

			debug("BeanFieldListener(), listening to a list");
			originalValue = copyList((List<?>) originalValue);
			currentValue = copyList((List<?>) originalValue);
		} else {
			currentValue = originalValue;
		}
	}

	/**
	 * Callback fra beansbindingen
	 * 
	 * @param property
	 */
	public void setTargetProperty(Object property) {
//		debug("setTargetProperty(" + property + ")");
		if (!(originalValue instanceof List)) {
			boolean old = isChanged();
			boolean changed = calculateChanged(property);
			currentValue = property;
			fireChanged(old, changed);
		}
	}
	//TODO kanskje private
	public void unbind() {
		bindingGroup.removeBinding(binding);
		binding.unbind();
		binding = null;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("beanField", beanField).append("originalValue", originalValue).append(
				"currentValue", currentValue).toString();
	}
}
