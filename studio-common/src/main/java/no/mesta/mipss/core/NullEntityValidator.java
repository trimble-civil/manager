package no.mesta.mipss.core;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * Sier om en string er blank eller ikke
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 * @param <D>
 */
public class NullEntityValidator<D extends PropertyChangeSource, E extends MipssEntityBean<? extends IRenderableMipssEntity>>
		extends BlankFieldValidatorAdapter<D, E> {

	public NullEntityValidator(D detail, String fieldName) {
		super(detail, fieldName);
	}

	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(E value) {
		logger.trace("calculateBlank(" + value + ") start");
		return value == null;
	}
}
