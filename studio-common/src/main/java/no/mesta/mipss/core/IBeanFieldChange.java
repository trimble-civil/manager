package no.mesta.mipss.core;

public interface IBeanFieldChange {
    public void setFieldChanged(Object instance, String beanField, boolean changed, boolean forceRemoval);
}
