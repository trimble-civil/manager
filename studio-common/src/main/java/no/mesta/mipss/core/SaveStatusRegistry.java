package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import org.jdesktop.beansbinding.BindingGroup;

import no.mesta.mipss.common.PropertyChangeSource;

public class SaveStatusRegistry implements PropertyChangeListener, PropertyChangeSource {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private List<SaveStatusMonitor<?>> monitorList = new ArrayList<SaveStatusMonitor<?>>();
	private BindingGroup bindingGroup = new BindingGroup();
	private boolean isChanged = false;
	
	public void addMonitor(SaveStatusMonitor<?> monitor) {
		monitorList.add(monitor);
		monitor.addPropertyChangeListener(this);
	}

	public void removeMonitor(SaveStatusMonitor<?> monitor) {
		monitorList.remove(monitor);
		monitor.removePropertyChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(evt.getPropertyName().equals("changed")) {
			setChanged(getGlobalChange());
		}
	}
	
	private boolean getGlobalChange() {
		Boolean isChanged = Boolean.FALSE;
		
		for(SaveStatusMonitor<?> monitor : monitorList) {
			if(monitor.isChanged()) {
				isChanged = Boolean.TRUE;
				break;
			}
		}
		return isChanged;
	}
	
	public boolean isChanged() {
		return isChanged;
	}
	
	public void setChanged(boolean changed) {
		Object oldValue = isChanged;
		isChanged = changed;
		props.firePropertyChange("changed", oldValue, changed);
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
	
	public void reset() {
		for(SaveStatusMonitor<?> monitor : monitorList) {
			monitor.reset();
		}
		setChanged(getGlobalChange());
	}
	
	/**
	 * Binds all registered bindings
	 */
	public void doBind() {
		bindingGroup.bind();
	}

	public BindingGroup getBindingGroup() {
		return bindingGroup;
	}
}
