package no.mesta.mipss.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JDateTimePicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.Converter;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

/**
 * Knytter sammen bønne og GUI.
 * <p>
 * Alle create*() metodene velger om de skal bruke ELProperty eller BeanProperty
 * mot instance (lyttekilden eller MIPSS entitetene) basert på det første tegnet
 * i {@code instanceField}. Hvis dette tegnet er "$" opprettes det en
 * ELProperty.
 * </p>
 * Mot GUI objektene opprettes det alltid en BeanProperty.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class FieldToolkit implements PropertyChangeListener, IDisposable {
	private static final String DATE = "date";
	private static final String ENABLE_FIELDS = "enableFields";
	private static final String ENABLED = "enabled";
	private static final String FIELDS_CHANGED = "fieldsChanged";
	private static final Logger logger = LoggerFactory.getLogger(FieldToolkit.class);
	private static final String SELECTED = "selected";
	private static final String TEXT = "text";
	private BindingGroup bindingGroup;
	private boolean bound;
	private boolean enableFields = true;
	private Map<ListenerKey, BeanFieldListener> listeners = new HashMap<ListenerKey, BeanFieldListener>();
	private boolean oldIsFieldsChanged;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private Map<Class<?>, MipssBeanChangeRegister> registers = new HashMap<Class<?>, MipssBeanChangeRegister>();

	/**
	 * Privat konstruktør
	 * 
	 */
	private FieldToolkit() {
		bindingGroup = new BindingGroup();
	}

	/**
	 * Delegatmetode for bindingsgruppa i toolkitet
	 * 
	 * @param binding
	 */
	public void addBinding(Binding<?,?,?,?> binding) {
		bindingGroup.addBinding(binding);
	}
	
	/**
	 * Legger til lytter på på et felt for å se om det endres fra orginalverdi
	 * 
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 */
	public <T> void addCangeListener(Class<T> clazz, Object instance, String instanceField) {
		MipssBeanChangeRegister register = registerBean(clazz, instanceField);
		BeanFieldListener changeListener = new BeanFieldListener(instance, instanceField, bindingGroup,
				(IBeanFieldChange) register);
		registerListener(clazz, changeListener, new ListenerKey(clazz, instanceField, instance));
		//Binding<?, ?, ?, ?> b = changeListener.getBinding();
		//b.bind();
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		logger.debug("addPropertyChangeListener(" + l + ")");
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		logger.debug("addPropertyChangeListener(" + propName + "," + l + ")");
		props.addPropertyChangeListener(propName, l);
	}

	/**
	 * Kjøres etter at alle komponenter er laget
	 * 
	 */
	public void bind() {
		logger.debug("bind()");
		bindingGroup.bind();
		oldIsFieldsChanged = isFieldsChanged();
		bound = true;
	}

	/**
	 * Binder sammen de to nevnte felt i {@code instance} hhv. {@code
	 * uiInstance}.
	 * 
	 * Bruker ELProperty for entitetens PropertyListener hvis useEL er {@code
	 * true}.
	 * 
	 * @param instance
	 *            kilden i bindingen
	 * @param uiInstance
	 *            mål i bindingen
	 * @param instanceField
	 *            Hvis denne strengen har "$" som sitt første tegn, bruker man
	 *            en {@code ELProperty}.
	 * @param uiField
	 *            felt i målinstansen
	 * @param converter
	 */
	@SuppressWarnings("unchecked")
	private <S,T> Binding<S, ?, T, ?> bind(S instance, T uiInstance, String instanceField, String uiField,
			Converter converter) {
		Binding<S, ?, T, ?> binding = BindingHelper.createbinding(instance, instanceField, uiInstance, uiField,
				BindingHelper.READ_WRITE);

		if (converter != null) {
			binding.setConverter(converter);
		}
		// lyt på endringer i selve bindingen
		binding.addBindingListener(new FieldBindingListener());
		bindingGroup.addBinding(binding);

		Binding<FieldToolkit, Boolean, T, Boolean> enableDisableField = BindingHelper.createbinding(this, ENABLE_FIELDS, uiInstance, ENABLED);
		bindingGroup.addBinding(enableDisableField);

		return binding;
	}

	public <S, T> Binding<S, ?, T, ?> bind(S source, T target, String sourceField, String targetField,
			UpdateStrategy strategy) {
		Binding<S, ?, T, ?> b = BindingHelper.createbinding(source, sourceField, target, targetField, strategy);
		addBinding(b);
		return b;
	}

	/**
	 * Legger til en binding
	 * 
	 * @param binding
	 */
	@SuppressWarnings("unchecked")
	public void bindAndAdd(Binding binding) {
		binding.bind();

		bindingGroup.addBinding(binding);
	}

	/**
	 * Binder en eksisterende JCheckBox.
	 * 
	 * @param box
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param converter
	 * @return
	 */
	public JCheckBox bindCheckBox(JCheckBox box, Class<?> clazz, Object instance, String instanceField,
			Converter<?, ?> converter) {
		bind(instance, box, instanceField, SELECTED, converter);

		registerCangeListener(clazz, instance, instanceField);
		return box;
	}

	public JDateTimePicker bindDatePicker(JDateTimePicker datePicker, Class<?> clazz, Object instance,
			String instanceField) {
		bind(instance, datePicker, instanceField, DATE, (Converter<?, ?>) null);

		registerCangeListener(clazz, instance, instanceField);
		return datePicker;
	}

	/**
	 * Binder en eksisterende DatePicker til en entitet.
	 * 
	 * @param datePicker
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param dateFormat
	 * @return
	 */
	public JMipssDatePicker bindDatePicker(JMipssDatePicker datePicker, Class<?> clazz, Object instance, String instanceField,
			String dateFormat) {
		datePicker.setFormats(dateFormat);
		datePicker.setLinkPanel(null);

		bind(instance, datePicker, instanceField, DATE, (Converter<?, ?>) null);

		registerCangeListener(clazz, instance, instanceField);
		return datePicker;
	}

	/**
	 * Binder en JDateTimePicker til en entitet.
	 * 
	 * @param dateTimePicker
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 * @return
	 */
	public JDateTimePicker bindDateTimePicker(JDateTimePicker dateTimePicker, Class<?> clazz, Object instance,
			String instanceField) {
		bind(instance, dateTimePicker, instanceField, DATE, (Converter<?, ?>) null);

		registerCangeListener(clazz, instance, instanceField);
		return dateTimePicker;
	}

	/**
	 * Binder en eksisterende ComboBox med en entitetinstans.
	 * 
	 * @param comboBox
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param converter
	 * @param listClazz
	 * @param list
	 * @return den leverte ComboBox.
	 */
	public <T extends IRenderableMipssEntity> JComboBox bindDropDown(JComboBox comboBox, Class<?> clazz,
			Object instance, String instanceField, Converter<?, ?> converter, Class<T> listClazz, List<T> list) {
		MipssListCellRenderer<T> renderer = new MipssListCellRenderer<T>();
		return bindDropDown(comboBox, clazz, instance, instanceField, converter, listClazz, list, renderer);
	}

	private <T extends IRenderableMipssEntity> JComboBox bindDropDown(JComboBox comboBox, Class<?> clazz,
			Object instance, String instanceField, Converter<?, ?> converter, Class<T> listClazz, List<T> list,
			MipssListCellRenderer<T> renderer) {
		comboBox.setRenderer((ListCellRenderer) renderer);
		ComponentSizeResources.setComponentSizes(comboBox);

		registerCangeListener(clazz, instance, instanceField);

		JComboBoxBinding<T, List<T>, JComboBox> cb = SwingBindings.createJComboBoxBinding(
				AutoBinding.UpdateStrategy.READ_WRITE, list, comboBox);
		bindingGroup.addBinding(cb);

		bind(instance, comboBox, instanceField, "selectedItem", converter);

		return comboBox;
	}
	
	public <T extends IRenderableMipssEntity> Binding<Object, ?, JComboBox, ?> getBindDropDown(JComboBox comboBox, Class<?> clazz,
			Object instance, String instanceField, Converter<?, ?> converter) {
		MipssListCellRenderer<T> renderer = new MipssListCellRenderer<T>();
		return getBindDropDown(comboBox, clazz, instance, instanceField, converter, renderer);
	}

	private <T extends IRenderableMipssEntity> Binding<Object, ?, JComboBox, ?> getBindDropDown(JComboBox comboBox, Class<?> clazz,
			Object instance, String instanceField, Converter<?, ?> converter, MipssListCellRenderer<T> renderer) {
		comboBox.setRenderer((ListCellRenderer) renderer);
		ComponentSizeResources.setComponentSizes(comboBox);

		registerCangeListener(clazz, instance, instanceField);

//		JComboBoxBinding<T, List<T>, JComboBox> cb = SwingBindings.createJComboBoxBinding(
//				AutoBinding.UpdateStrategy.READ_WRITE, list, comboBox);
//		bindingGroup.addBinding(cb);

		return bind(instance, comboBox, instanceField, "selectedItem", converter);
	}

	/**
	 * Binder en eksisterende ComboBox med en entitetinstans og gir en valgfri
	 * tekst til visning i comboboxen hvis det ikke finnes data
	 * 
	 * @param comboBox
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 * @param converter
	 * @param listClazz
	 * @param list
	 * @return den leverte ComboBox.
	 */
	public <T extends IRenderableMipssEntity> JComboBox bindDropDown(JComboBox comboBox, Class<?> clazz,
			Object instance, String instanceField, Converter<?, ?> converter, Class<T> listClazz, List<T> list,
			String emptyListText) {
		MipssListCellRenderer<T> renderer = new MipssListCellRenderer<T>(emptyListText);
		return bindDropDown(comboBox, clazz, instance, instanceField, converter, listClazz, list, renderer);
	}

	@SuppressWarnings("unchecked")
	public JButton bindReactiveButton(JButton button) {
		Binding binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, this, BeanProperty
				.create(FIELDS_CHANGED), button, BeanProperty.create(ENABLED));
		binding.addBindingListener(new FieldBindingListener());
		bindingGroup.addBinding(binding);

		return button;
	}

	/**
	 * Binder den leverte {@code JTextArea} til en entitetsinstans. Brukes for
	 * areaer som allerede er ferdig posisjonert i panelen.
	 * 
	 * @param area
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @return
	 */
	public JTextArea bindTextInputArea(JTextArea area, Class<?> clazz, Object instance, String instanceField) {
		bind(instance, area, instanceField, TEXT, (Converter<?, ?>)null);

		registerCangeListener(clazz, instance, instanceField);
		return area;
	}

	/**
	 * Binder den leverte {@code JTextField} til en entitetinstans. Brukes fior
	 * felt som allerede e posisjonert i panelen.
	 * 
	 * @param field
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param converter
	 * @return det samme {@code JTextField} objektet som ble levert.
	 */
	public JTextField bindTextInputField(JTextField field, Class<?> clazz, Object instance, String instanceField,
			Converter<?, ?> converter) {
		bind(instance, field, instanceField, TEXT, converter);

		registerCangeListener(clazz, instance, instanceField);
		return field;
	}

	/**
	 * Fjerner minnen om alle feltendringer.
	 */
	public void clearFieldChanges() {
		for (MipssBeanChangeRegister r : registers.values()) {
			r.clearChanges();
		}
	}

	public <T> Binding<FieldToolkit, ?, T, ?> controlEnabledField(T target) {
		Binding<FieldToolkit, ?, T, ?> b = bind(this, target, ENABLE_FIELDS, ENABLED, BindingHelper.READ);
		return b;
	}

	/**
	 * Oppretter en ny {@code JCheckBox} og binder den.
	 * 
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param converter
	 * @return
	 */
	public JCheckBox createCheckBox(Class<?> clazz, Object instance, String instanceField, Converter<?, ?> converter) {
		JCheckBox box = new JCheckBox();

		return bindCheckBox(box, clazz, instance, instanceField, converter);
	}

	public JCheckBox createCheckBox(Class<?> clazz, Object instance, String instanceField, Converter<?, ?> converter,
			String title) {
		JCheckBox box = new JCheckBox(title);

		return bindCheckBox(box, clazz, instance, instanceField, converter);
	}

	/**
	 * Oppretter en ny JXDatePicker og binder den.
	 * 
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param dateFormat
	 * @return
	 */
	public JMipssDatePicker createDatePicker(Class<?> clazz, Object instance, String instanceField, String dateFormat) {
		JMipssDatePicker datePicker = new JMipssDatePicker(null, null);
		return bindDatePicker(datePicker, clazz, instance, instanceField, dateFormat);
	}

	public JDateTimePicker createDateTimePicker(Class<?> clazz, Object instance, String instanceField, String dateFormat) {
		JDateTimePicker datePicker = new JDateTimePicker(Clock.now(), dateFormat);
		return bindDateTimePicker(datePicker, clazz, instance, instanceField);
	}

	/**
	 * Lager en dropdown som oppdaterer ei bønne
	 * 
	 * @param clazz
	 *            classen til instansen
	 * @param instance
	 *            instansen for bønna
	 * @param instanceField
	 *            feltet på bønna som skal oppdateres. Beordrer bruk av
	 *            ELProperty hvis den starter med "$".
	 * @param converter
	 *            evt konvertor som skal brukes
	 * @param listClazz
	 *            klassen til verdiene i listen
	 * @param list
	 *            listen over verdier som vises i dropdown
	 * @return
	 */
	public <T extends IRenderableMipssEntity> JComboBox createDropDown(Class<?> clazz, Object instance,
			String instanceField, Converter<?, ?> converter, Class<T> listClazz, List<T> list) {
		JComboBox comboBox = new JComboBox();
		return bindDropDown(comboBox, clazz, instance, instanceField, converter, listClazz, list);
	}

	public <T extends IRenderableMipssEntity> JComboBox createDropDown(Class<?> clazz, Object instance,
			String instanceField, Converter<?, ?> converter, Class<T> listClazz, List<T> list,
			MipssListCellRenderer<T> renderer) {
		JComboBox comboBox = new JComboBox();
		return bindDropDown(comboBox, clazz, instance, instanceField, converter, listClazz, list, renderer);
	}

	public JButton createReactiveButton(Action action) {
		JButton button = new JButton(action);
		return bindReactiveButton(button);
	}

	/**
	 * Oppretter en ny {@code JTextArea} og binder den.
	 * 
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @return
	 */
	public JTextArea createTextInputArea(Class<?> clazz, Object instance, String instanceField) {
		JTextArea area = new JTextArea();
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		return bindTextInputArea(area, clazz, instance, instanceField);
	}

	/**
	 * Oppretter en ny {@JTextField} og binder den.
	 * 
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 *            Beordrer bruk av ELProperty hvis den starter med "$".
	 * @param converter
	 * @return
	 */
	public JTextField createTextInputField(Class<?> clazz, Object instance, String instanceField,
			Converter<?, ?> converter) {
		JTextField field = new JTextField();
		return bindTextInputField(field, clazz, instance, instanceField, converter);
	}

	@Override
	public void dispose() {
		if(bindingGroup != null) {
			try {
				bindingGroup.unbind();
				bindingGroup = null;
			} catch (Throwable t) {
				t = null;
			}
		}
		
		if(registers != null) {
			Iterator<Class<?>> it = registers.keySet().iterator();
			while(it.hasNext()) {
				MipssBeanChangeRegister register = registers.get(it.next());
				register.dispose();
			}
			
			registers.clear();
			registers = null;
		}
		
		if(listeners != null) {
			Iterator<ListenerKey> it = listeners.keySet().iterator();
			while(it.hasNext()) {
				BeanFieldListener listener = listeners.get(it.next());
				listener.dispose();
			}

			listeners.clear();
			listeners = null;
		}

		if(props != null && props.getPropertyChangeListeners() != null) {
			for(PropertyChangeListener l:props.getPropertyChangeListeners()) {
				props.removePropertyChangeListener(l);
			}
			props = null;
		}
	}

	/**
	 * Returnerer om en bønne som det lyttes på er endret
	 * 
	 * @param clazz
	 * @return
	 */
	public boolean isBeanChanged(Class<?> clazz) {
		MipssBeanChangeRegister register = registers.get(clazz);
		return register.isChanged();
	}

	public boolean isBound() {
		return bound;
	}
	
	/**
	 * Returnerer om feltene er redigerbare
	 * 
	 * @return
	 */
	public boolean isEnableFields() {
		return enableFields;
	}
		
	/**
	 * Returnerer om et gitt felt er endret
	 * 
	 * @param clazz
	 * @param field
	 * @return
	 */
	public boolean isFieldChanged(Class<?> clazz, String field) {
		MipssBeanChangeRegister register = registers.get(clazz);
		return register.isFieldChanged(field);
	}

	/**
	 * Returnerer om feltene som overvåkes er endret
	 * 
	 * @return
	 */
	public boolean isFieldsChanged() {
		Iterator<Class<?>> it = registers.keySet().iterator();
		while (it.hasNext()) {
			Class<?> clazz = it.next();
			if (isBeanChanged(clazz)) {
				return true;
			}
		}
		return false;
	}

	/** {@inheritDoc} */
	public void propertyChange(PropertyChangeEvent evt) {
		logger.debug("propertyChange(" + evt + ")");
		boolean now = isFieldsChanged();
		logger.debug("old = " + oldIsFieldsChanged + ", now = " + now);
		props.firePropertyChange(FIELDS_CHANGED, oldIsFieldsChanged, now);
		oldIsFieldsChanged = now;
	}

	/**
	 * Henter ut registeret for bønna og registerer feltet i registeret
	 * 
	 * @param clazz
	 * @param field
	 * @return
	 */
	private MipssBeanChangeRegister registerBean(Class<?> clazz, String field) {
		MipssBeanChangeRegister register = registers.get(clazz);

		if (register == null) {
			register = new MipssBeanChangeRegister();
			registers.put(clazz, register);
		}

		register.addField(field);

		return register;
	}

	/**
	 * Lytter på på et felt for å se om det endres fra orginalverdi
	 * 
	 * @param clazz
	 * @param instance
	 * @param instanceField
	 */
	public <T> void registerCangeListener(Class<T> clazz, Object instance, String instanceField) {
		MipssBeanChangeRegister register = registerBean(clazz, instanceField);
		registerListener(clazz, new BeanFieldListener(instance, instanceField, bindingGroup,
				(IBeanFieldChange) register), new ListenerKey(clazz, instanceField, instance));
	}

	/**
	 * Registerer lytteren
	 * 
	 * @param clazz
	 * @param listener
	 * @param key
	 */
	private void registerListener(Class<?> clazz, BeanFieldListener listener, ListenerKey key) {
		BeanFieldListener registeredListener = listeners.get(key);

		if (registeredListener == null) {
			listeners.put(key, listener);
		} else {
			throw new IllegalStateException("Duplicate listener on field");
		}

		listener.addPropertyChangeListener("changed", this);
	}

	/**
	 * Fjerner en binding fra gruppen
	 * 
	 * @param binding
	 */
	public void removeBinding(Binding<?,?,?,?> binding) {
		bindingGroup.removeBinding(binding);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		logger.debug("removePropertyChangeListener(" + l + ")");
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		logger.debug("removePropertyChangeListener(" + propName + "," + l + ")");
		props.removePropertyChangeListener(propName, l);
	}

	/**
	 * Nulstiller toolkit uten å fjerne verdier
	 * 
	 */
	public void reset() {
		Iterator<ListenerKey> lIt = listeners.keySet().iterator();
		while (lIt.hasNext()) {
			BeanFieldListener listener = listeners.get(lIt.next());
			try{
			listener.reset();
			} catch (Throwable t){
				//TODO do nothing...for now
			}
		}

		Iterator<Class<?>> rIt = registers.keySet().iterator();
		while (rIt.hasNext()) {
			MipssBeanChangeRegister register = registers.get(rIt.next());
			register.clearChanges();
		}

		boolean now = isFieldsChanged();
		props.firePropertyChange(FIELDS_CHANGED, oldIsFieldsChanged, now);
		oldIsFieldsChanged = now;
	}

	public void resetValue(Class<?> clazz, Object instance, String field) {
		ListenerKey key = new ListenerKey(clazz, field, instance);
		listeners.get(key).reset();
		registers.get(clazz).setFieldChanged(instance, field, false, false);	
	}

	/**
	 * Sett om feltene skal være redigerbar eller ikke
	 * 
	 * @param f
	 */
	public void setEnableFields(boolean f) {
		boolean old = enableFields;
		enableFields = f;
		props.firePropertyChange(ENABLE_FIELDS, old, f);
	}

	/**
	 * Opphever eksisterende bindinger og fjerner dem.
	 */
	public void unbindAndRemove() {
		// fjern bindingene og reinstatier gruppen
		try {
			bindingGroup.unbind();
		} finally {
			bindingGroup = new BindingGroup();
		}

		// reinstantier registers og listeners
		registers = new HashMap<Class<?>, MipssBeanChangeRegister>();
		listeners = new HashMap<ListenerKey, BeanFieldListener>();
	}

	/**
	 * Fjerner en binding
	 * 
	 * @param binding
	 */
	@SuppressWarnings("unchecked")
	public void unbindAndRemove(Binding binding) {
		bindingGroup.removeBinding(binding);

		if (binding != null && binding.isBound()) {
			binding.unbind();
		}
	}

	/**
	 * Create metode, denne skal brukes
	 * 
	 * @return
	 */
	public static FieldToolkit createToolkit() {
		FieldToolkit toolkit = new FieldToolkit();
		return toolkit;
	}

		
}
