package no.mesta.mipss.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Converter;

/**
 * Konverterer mellom en Long og Boolean, 0 = false, 1 = true. Alt annet enn 0
 * er true.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class LongBooleanConverter extends Converter<Long,Boolean> {
    private static final Logger logger = LoggerFactory.getLogger(LongBooleanConverter.class);
    
    /**
     * Konstruktør
     */
    public LongBooleanConverter() {
    }

    /** {@inheritDoc} */
    public Boolean convertForward(Long value) {
    	logger.debug("forward: " + value);
    	
        if(value == null) {
            return false;
        } else if(value == 0) {
            return false;
        } else {
            return true;
        }
    }

    /** {@inheritDoc} */
    public Long convertReverse(Boolean value) {
    	logger.debug("reverse: " + value);
    	
    	if(value == null) {
    		return Long.valueOf(0);
    	}
        if(value == true) {
            return Long.valueOf(1);
        } else {
            return Long.valueOf(0);   
        }
    }
}
