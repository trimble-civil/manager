package no.mesta.mipss.core;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.xmlpull.v1.XmlPullParserException;

import android.content.res.AXmlResourceParser;

public class AndroidManifestReader {
	private static final String VERSION_CODE = "versionCode";
	private static final String VERSION_NAME = "versionName";
	private InputStream manifest;
	private String versionCode;
	private String versionName;
	
	public String getVersionCode(){
		return versionCode;
	}
	public String getVersionName(){
		return versionName;
	}
	
	/**
	 * Inputstream til AndroidManifest.xml
	 * @param manifest
	 */
	public AndroidManifestReader(InputStream manifest){
		this.manifest = manifest;
		try {
			readManifest();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public AndroidManifestReader(String apkFilename){
		ZipFile zip = null;
		try {
			if (apkFilename.endsWith(".apk") || apkFilename.endsWith(".zip")) {
				zip = new ZipFile(apkFilename);
				ZipEntry mft = zip.getEntry("AndroidManifest.xml");
				manifest = zip.getInputStream(mft);
			} else {
				manifest = new FileInputStream(apkFilename);
			}
			readManifest();
			manifest.close();
			if (zip != null) {
				zip.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Henter versionCode og versionName fra manifestet
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readManifest() throws XmlPullParserException, IOException{
		AXmlResourceParser parser = new AXmlResourceParser();
		parser.open(manifest);
		
		parser.next();
		parser.next();
		
		for(int i = 0; i != parser.getAttributeCount(); i++){  
			String attributeName = parser.getAttributeName(i);
			if (VERSION_CODE.equals(attributeName)){
				versionCode = String.valueOf(parser.getAttributeValueData(i));
			}
			if (VERSION_NAME.equals(attributeName)){
				versionName = parser.getAttributeValue(i);
			}
      	}
	}

	public static void main(String[] args) throws Exception {
		
		String fileName = "c:/MIPSS_Felt.apk";
		InputStream is = null;
		ZipFile zip = null;
		
		if (fileName.endsWith(".apk") || fileName.endsWith(".zip")) {
			zip = new ZipFile(fileName);
			ZipEntry mft = zip.getEntry("AndroidManifest.xml");
			is = zip.getInputStream(mft);
		} else {
			is = new FileInputStream(fileName);
		}
		AndroidManifestReader dc = new AndroidManifestReader(is);
		System.out.println(dc.getVersionCode());
		System.out.println(dc.getVersionName());
		is.close();
		if (zip != null) {
			zip.close();
		}
	}
}