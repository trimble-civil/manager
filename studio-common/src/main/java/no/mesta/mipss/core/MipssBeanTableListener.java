package no.mesta.mipss.core;

import java.util.HashMap;
import java.util.Map;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Brukes til å lytte på tabeller, slik at man vet om den har endret seg, og
 * slik at man kan håndtere det (eks undo).
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class MipssBeanTableListener implements TableModelListener {
	private IBeanFieldChange callback;
	private int columns = 0;
	private String field;
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private Map<Cell, Object> originalData = new HashMap<Cell, Object>();
	private int rows = 0;

	/**
	 * Konstruktør
	 * 
	 * @param model
	 *            data som skal overvåkes
	 * @param field
	 *            felte i bønna data vedlikeholder
	 * @param callback
	 *            endringshåndtereren som skal informeres
	 */
	public MipssBeanTableListener(TableModel model, String field, IBeanFieldChange callback) {
		rows = model.getRowCount();
		columns = model.getColumnCount();
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < columns; c++) {
				Cell cell = new Cell(r, c);
				originalData.put(cell, model.getValueAt(r, c));
			}
		}
		this.callback = callback;
		this.field = field;
	}

	private boolean checkAllColumns(int firstRow, int lastRow, TableModel model) {
		for (int r = firstRow; r <= lastRow; r++) {
			for (int c = 0; c < columns; c++) {
				Cell cell = new Cell(r, c);
				Object originalValue = originalData.get(cell);
				if (originalValue != null && !originalValue.equals(model.getValueAt(r, c))) {
					return true;
				} else if (originalValue == null && model.getValueAt(r, c) != null) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean checkCell(int firstRow, int lastRow, int column, TableModel model) {
		for (int r = firstRow; r <= lastRow; r++) {
			Cell cell = new Cell(r, column);
			Object originalValue = originalData.get(cell);
			if (!originalValue.equals(model.getValueAt(r, column))) {
				return true;
			}
		}

		return false;
	}

	public void tableChanged(TableModelEvent e) {
		boolean changed = false;

		int firstRow = e.getFirstRow();
		int lastRow = e.getLastRow();
		TableModel model = (TableModel) e.getSource();

		log.trace("t: " + e.getType() + ", fr:" + e.getFirstRow() + ", lr:" + e.getLastRow() + ", c:" + e.getColumn());

		if (TableModelEvent.ALL_COLUMNS == e.getColumn()) {
			changed = checkAllColumns(firstRow, lastRow, model);
		} else {
			changed = checkCell(firstRow, lastRow, e.getColumn(), model);
		}
		callback.setFieldChanged(null, field, changed, false);
	}

	class Cell {
		int column = -1;
		int row = -1;

		public Cell(int r, int c) {
			row = r;
			column = c;
		}

		@Override
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			if (!(o.getClass().equals(Cell.class))) {
				return false;
			}
			if (o == this) {
				return true;
			}

			Cell other = (Cell) o;
			return new EqualsBuilder().append(row, other.row).append(column, other.column).isEquals();
		}

		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(row).append(column).toHashCode();
		}
	}
}
