package no.mesta.mipss.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Converter;

/**
 * <p>Konverterer mellom en <code>String</code> og <code>Double</code>,
 * <code>null</code> inn gir <code>null</code> ut - ellers konverteres det.</p>
 * 
 * <p>Metoden <code>convertForward(String)</code>
 * kan gi <code>NumberFormatException</code>, da gir også convertForward
 * <code>null</code></p>
 * 
 * @see java.lang.Double#valueOf(String)
 * @see java.lang.String#valueOf(Long)
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class DoubleStringConverter extends Converter<Double, String> {
	private static final Logger logger = LoggerFactory.getLogger(DoubleStringConverter.class);
    private String failedDoubleConvert;
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    private boolean commaDelimiter=false;
	private DecimalFormat format;

    
    public void setCommaDelimiter(boolean commaDelimiter){
    	this.commaDelimiter = commaDelimiter;
    }
    /**
     * Konstruktør
     */
    public DoubleStringConverter() {
    	DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
    	symbols.setDecimalSeparator(',');
    	format = new DecimalFormat("#.###", symbols);

    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }
    
    /** {@inheritDoc} */
    public String convertForward(Double value) {
    	logger.debug("convertForward(" + value + ")");
        if(value != null) {
        	if (commaDelimiter){
        		return format.format(value);
        	}
            return String.valueOf(value);
        } else {
            return null;
        }
    }
    /** {@inheritDoc} */
    public Double convertReverse(String value) {
    	logger.debug("convertReverse(" + value + ")");
        Double doubleValue = null;
        boolean errors = false;
        if(StringUtils.isBlank(value)) {
            doubleValue = null;
        } else {     
            try {
            	if (commaDelimiter){
            		doubleValue = format.parse(value).doubleValue();
            	}
                doubleValue = Double.valueOf(value);
            } catch (NumberFormatException e) {
                errors = true;
            } catch (ParseException e) {
            	errors=true;
			}
        }

        String old = failedDoubleConvert;
        if(errors) {
            failedDoubleConvert = "Ikke et flyttall: \"" + value + "\"";
            props.firePropertyChange("failedDoubleConvert", old, failedDoubleConvert);
        } else {
            failedDoubleConvert = null;
            props.firePropertyChange("failedDoubleConvert", old, failedDoubleConvert);
        }
        
        return doubleValue;
    }

    /**
     * Sist feilet String til Long konverteringsverdi
     * 
     * @return
     */
    public String getFailedDoubleConvert() {
        return failedDoubleConvert;
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
