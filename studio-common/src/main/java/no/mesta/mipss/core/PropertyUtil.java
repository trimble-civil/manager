package no.mesta.mipss.core;

import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.beansbinding.Property;

/**
 * Hjelper med aa knytte seg til boenne felter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PropertyUtil {
    
    /**
     * Lager enten en EL eller Bean Property
     * 
     * @param field
     * @return
     */
    public static<S,V> Property<S,V> createProperty(String field) {
        if(StringUtils.startsWith(field, "$")) {
            return ELProperty.create(field);
        } else {
            return BeanProperty.create(field);
        }
    }
    
    /**
     * Henter ut metoden til bønnefeltet
     * 
     * @param field
     * @return
     */
    public static String getBeanField(String prefix, String field) {
        String beanField = prefix + field.substring(0,1).toUpperCase() + field.substring(1);
        return beanField;
    }

    /**
     * Henter ut typen for et felt
     * 
     * @param clazz
     * @param field
     * @return
     */
    public static Class<?> getBeanFieldType(Class<?> clazz, String field) {
    	if (field.startsWith("$")){
    		//EL-property
    		Property<Object, Object> prop = createProperty(field);
    		return prop.getClass();
    		
    	}
        Method m = null;
        Class<?> type = null;
        try {
            m = clazz.getMethod(getBeanField("get", field), (Class<?>[]) null);
            type = m.getReturnType();
        } catch(Exception e) {
            try {
                m = clazz.getMethod(getBeanField("is", field), (Class<?>[]) null);
                type = m.getReturnType();
            } catch (Exception e2) {
                throw new IllegalArgumentException("no method get/is " + field, e);
            }
        }
        
        return type;
    }
    
    /**
     * Binner sammen to felter
     */
    @SuppressWarnings("unchecked")
	public static void bindObjects(Object source, Object target, String sourceField, String targetField) {
        Property sourceProperty = createProperty(sourceField);
        Property targetProperty = createProperty(targetField);
        
        Binding binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, source, sourceProperty, target, targetProperty);
        binding.bind();
    }
}
