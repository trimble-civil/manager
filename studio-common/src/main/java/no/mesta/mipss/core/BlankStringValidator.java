package no.mesta.mipss.core;

import no.mesta.mipss.common.PropertyChangeSource;

import org.apache.commons.lang.StringUtils;

/**
 * Sier om en string er blank eller ikke
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <D>
 */
public class BlankStringValidator<D extends PropertyChangeSource> extends BlankFieldValidatorAdapter<D, String>{

	public BlankStringValidator(D detail, String fieldName) {
		super(detail, fieldName);
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean calculateBlank(String value) {
		logger.trace("calculateBlank(" + value + ") start");
		return StringUtils.isBlank(value);
	}
}
