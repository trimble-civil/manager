package no.mesta.mipss.core;

import java.awt.geom.Point2D;
import java.text.DecimalFormat;

import no.mesta.mipss.persistence.mipssfield.Punktstedfest;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;

public class CoordinateUtils {

	public static Point2D.Double convertToUTM32(double x, double y) throws Exception{
		
		CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");	
		CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32632");
		MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
		Coordinate c = JTS.transform(new Coordinate(x, y), null, transform);
		return new Point2D.Double(c.x, c.y);//TODO denne må testes hvis den tas i bruk
	}
	
	public static Point2D.Double convertFromUTM33toWGS84(double x, double y) throws Exception{
		CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");
		CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:4326");
		MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
		Coordinate c = JTS.transform(new Coordinate(x, y), null, transform);
		return new Point2D.Double(c.y, c.x);//koordinatets x verdi er punktets y verdi.. merkelig...
	}
	public static String convertFromXYToWGS84String(Double x, Double y){
		Punktstedfest ps = new Punktstedfest();
		ps.setX(x);
		ps.setY(y);
		return convertFromStedfestingToWGS84String(ps);
	}
	public static String convertFromStedfestingToWGS84String(Punktstedfest ps){
		Point2D.Double wgs84 =null;
		StringBuilder sb = new StringBuilder();
		if (ps.getSnappetX()!=null&&ps.getSnappetY()!=null){
			try {
				wgs84 = CoordinateUtils.convertFromUTM33toWGS84(ps.getSnappetX(), ps.getSnappetY());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if (ps.getX()!=null&&ps.getY()!=null){
			try {
				wgs84 = CoordinateUtils.convertFromUTM33toWGS84(ps.getX(), ps.getY());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (wgs84!=null){
			DecimalFormat f = new DecimalFormat("###.#####");
			sb.append("lat:");
			sb.append(f.format(wgs84.y));
			sb.append(" lon:");
			sb.append(f.format(wgs84.x));
		}
		return sb.toString();
		
	}
}