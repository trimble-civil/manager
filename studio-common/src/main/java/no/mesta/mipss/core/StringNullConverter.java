package no.mesta.mipss.core;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Converter;

/**
 * BeansBinding converter som sørger for at tomme strenger er null
 * @author harkul
 *
 */
public class StringNullConverter extends Converter<String,String> {

	public String convert(String value){
		if (StringUtils.isBlank(value))
			return null;
		return value;
	}
	@Override
	public String convertForward(String value) {
		return convert(value);
	}

	@Override
	public String convertReverse(String value) {
		return convert(value);
	}

}
