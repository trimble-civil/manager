package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.Map;

import no.mesta.mipss.plugin.PluginMessage;

public class LevHarUlagredeEndringerMessage extends PluginMessage<String, Boolean> {
	private Boolean ulagredeEndringer;
	
	public Boolean getUlagredeEndringer() {
		return ulagredeEndringer;
	}

	public void setUlagredeEndringer(Boolean ulagredeEndringer) {
		this.ulagredeEndringer = ulagredeEndringer;
	}

	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, Boolean> getResult() {
		return null;
	}

	@Override
	public void processMessage() {
		setConsumed(true);
	}
}
