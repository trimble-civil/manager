package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.Map;

import no.mesta.mipss.plugin.PluginMessage;

public class RefreshLeverandorMessage extends PluginMessage<String, String> {
	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, String> getResult() {
		return null;
	}

	@Override
	public void processMessage() {
		setConsumed(true);
	}
}
