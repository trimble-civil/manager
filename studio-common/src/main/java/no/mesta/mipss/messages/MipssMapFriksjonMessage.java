package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.plugin.PluginMessage;

public class MipssMapFriksjonMessage extends PluginMessage<String,String>{
	private final List<Friksjonsdetalj> friksjonList;

	public MipssMapFriksjonMessage(List<Friksjonsdetalj> friksjonList){
		this.friksjonList = friksjonList;
		
	}
	
	public List<Friksjonsdetalj> getFriksjonList(){
		return friksjonList;
	}
	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, String> getResult() {
		return null;
	}

	@Override
	public void processMessage() {
		setConsumed(true);
	}

	

}
