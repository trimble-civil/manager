package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.Date;
import java.util.Map;

import no.mesta.mipss.plugin.PluginMessage;

public class OpenDetaljertProdrapportMessage extends PluginMessage<String, String>{
	private Long driftkontraktId;
	private Long kjoretoyId;
	private Date fraDato;
	private Date tilDato;
	
	public OpenDetaljertProdrapportMessage(Long driftkontraktId, Long kjoretoyId, Date fraDato, Date tilDato){
		this.driftkontraktId = driftkontraktId;
		this.kjoretoyId = kjoretoyId;
		this.fraDato = fraDato;
		this.tilDato = tilDato;
	}
	@Override
	public void processMessage() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<String, String> getResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getMessageGUI() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the driftkontraktId
	 */
	public Long getDriftkontraktId() {
		return driftkontraktId;
	}

	/**
	 * @return the kjoretoyId
	 */
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	
	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}
	
	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}
}
