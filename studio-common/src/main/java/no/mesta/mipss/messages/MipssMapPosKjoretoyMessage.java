package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.Date;
import java.util.Map;

import no.mesta.mipss.plugin.PluginMessage;

public class MipssMapPosKjoretoyMessage extends PluginMessage<String,String>{

	private final Date gmtTidspunkt;
	private final Long kjoretoyId;
	private final String leverandor;
	private final String eksterntNavn;

	public MipssMapPosKjoretoyMessage(Long kjoretoyId, Date gmtTidspunkt, String leverandor, String eksterntNavn){
		this.kjoretoyId = kjoretoyId;
		this.gmtTidspunkt = gmtTidspunkt;
		this.leverandor = leverandor;
		this.eksterntNavn = eksterntNavn;
	}

	
	public Date getGmtTidspunkt() {
		return gmtTidspunkt;
	}


	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	
	public String getLeverandor(){
		return leverandor;
	}

	public String getEksterntNavn(){
		return eksterntNavn;
	}

	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, String> getResult() {
		return null;
	}

	@Override
	public void processMessage() {
		
	}
}
