package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.plugin.PluginMessage;

public class MipssMapFriksjonStrekningMessage extends PluginMessage<String,String>{

	private List<Prodstrekning> prodstrekningList;

	public MipssMapFriksjonStrekningMessage(List<Prodstrekning> prodstrekningList){
		this.prodstrekningList = prodstrekningList;
	}
	
	public List<Prodstrekning> getProdstrekningList(){
		return prodstrekningList;
	}
	@Override
	public Component getMessageGUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> getResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void processMessage() {
		// TODO Auto-generated method stub
		
	}
	
}
