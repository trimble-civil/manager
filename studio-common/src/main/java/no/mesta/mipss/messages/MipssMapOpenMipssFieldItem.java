package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.plugin.PluginMessage;

public class MipssMapOpenMipssFieldItem extends PluginMessage<String,String> {

	private List<? extends MipssFieldDetailItem> items;

	public MipssMapOpenMipssFieldItem(final List<? extends MipssFieldDetailItem> items) {
    	this.items = items;
    }
	
	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, String> getResult() {
		return null;
	}

	@Override
	public void processMessage() {
     
	}
	
	public List<? extends MipssFieldDetailItem> getItems(){
		return items;
	}
}
/*
if (item instanceof PunktregSok) {
addDetail((PunktregSok) item, saveAction);
} else if (item instanceof InspeksjonSokView) {
addDetail((InspeksjonSokView) item);
} else if (item instanceof HendelseSokView) {
addDetail((HendelseSokView) item, saveAction);
} else {
throw new IllegalArgumentException("Unknown detail object" + item);
}
*/