package no.mesta.mipss.messages;

import java.awt.Component;
import java.util.Map;

import no.mesta.mipss.plugin.PluginMessage;

public class AapneKjoretoyMessage extends PluginMessage<String, String> {
	private Long kjoretoyId;
	
	public AapneKjoretoyMessage(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, String> getResult() {
		return null;
	}

	@Override
	public void processMessage() {
		setConsumed(true);
	}
}
