package no.mesta.mipss.exceptions;

/**
 * Denne exception kastes naar et kall til rammeverket eller kall fra en applikasjon til en 
 * annen feiler. 
 * @author Harald A. Kuloe
 */
public class FrameworkException extends Exception {

    public FrameworkException() {
    }

    public FrameworkException(String message) {
        super(message);
    }

    public FrameworkException(Throwable cause) {
        super(cause);
    }

    public FrameworkException(String message, Throwable cause) {
        super(message, cause);
    }
}
