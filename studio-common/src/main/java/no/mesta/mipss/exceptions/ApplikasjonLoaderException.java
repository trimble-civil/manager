package no.mesta.mipss.exceptions;

/**
 * Kastes når lastingen av en applikasjon feiler. 
 * @author Harald A. Kulø
 */
public class ApplikasjonLoaderException extends Exception {

    public ApplikasjonLoaderException() {
    }

    public ApplikasjonLoaderException(String message) {
        super(message);
    }

    public ApplikasjonLoaderException(Throwable cause) {
        super(cause);
    }

    public ApplikasjonLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

}
