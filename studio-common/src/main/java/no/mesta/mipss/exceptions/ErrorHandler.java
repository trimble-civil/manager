package no.mesta.mipss.exceptions;

import java.util.logging.Level;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

public class ErrorHandler {

	public static void showError(Throwable t, String feilmelding){
        ErrorInfo info = new ErrorInfo("Feil ved henting av data", feilmelding, null, "Advarsel", t, Level.SEVERE, null);
        JXErrorPane.showDialog(null, info);
	}
	public static void showError(Throwable t, String tittel, String feilmelding){
        ErrorInfo info = new ErrorInfo(tittel, feilmelding, null, "Advarsel", t, Level.SEVERE, null);
        JXErrorPane.showDialog(null, info);
	}
}