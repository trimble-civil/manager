package no.mesta.mipss.exceptions;

/**
 * Kastes når en applikasjon ikke kan håndteres av ApplikasjonHandler, f.eks hvis en
 * feil oppstår ved henting fra databasen, eller hvis lagring til lokal fil feiler. 
 * @author Harald A. Kulø
 */
public class ApplikasjonHandlerException extends Exception {
    public ApplikasjonHandlerException() {
    }

    public ApplikasjonHandlerException(String message) {
        super(message);
    }

    public ApplikasjonHandlerException(Throwable cause) {
        super(cause);
    }

    public ApplikasjonHandlerException(String message, Throwable cause) {
        super(message, cause);
    }
}
