package no.mesta.mipss.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLHelper {

	
	public static Document createDocument(InputStream is) throws SAXException, IOException, ParserConfigurationException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        String xml = IOUtils.toString(is, "UTF8");
        Document document = builder.parse(new ByteArrayInputStream(xml.getBytes("UTF8")));
        return document;
	}
	
	public static String getTagValue(String tagName, Node node){
		if (node.getNodeType() == Node.ELEMENT_NODE) {
    		Element e = (Element)node;
			NodeList nodeList = e.getElementsByTagName(tagName).item(0).getChildNodes();
			Node item = nodeList.item(0);
			if (item!=null)
				return item.getNodeValue();
			return null;
		}
		return null;
	}
	
	public static NodeList getElementsByTagName(Document document, String tagName){
		Element rootElement = document.getDocumentElement();
        NodeList liste = rootElement.getElementsByTagName(tagName);
        return liste;
	}
	
}
