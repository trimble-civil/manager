package no.mesta.mipss.dto;

public class ClassDTO {
    private Class clazz;
    private String visningsnavn;
    
    public ClassDTO() {
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setVisningsnavn(String visningsnavn) {
        this.visningsnavn = visningsnavn;
    }

    public String getVisningsnavn() {
        return visningsnavn;
    }
}
