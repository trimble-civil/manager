package no.mesta.mipss.dto;

import java.io.Serializable;

import javax.swing.ImageIcon;

import no.mesta.mipss.persistence.applikasjon.Menygruppe;
import no.mesta.mipss.persistence.applikasjon.Menypunkt;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;

/**
 * Representerer en node i menystukturen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class MenynodeDTO implements Serializable {
	private boolean fjernValgt;
	private Menygruppe gruppe;
	private String klassenavn;
	private ImageIcon menynodeIkon;
	private boolean startet;
	private Tilgangspunkt tilgangspunkt;
	private String tresti;
	private String url;
	private String visningsnavn;
	private boolean valgbar;
	private boolean synlig;

	/**
	 * Konstruktør
	 * 
	 * @param node
	 */
	public MenynodeDTO(Menypunkt node) {
		valgbar = node.getValgbarFlagg();
		synlig = node.getSynligFlagg();
		
		setUrl(node.getUrl());
		setVisningsnavn(node.getVisningsnavn());
		if (node.getIkon() != null) {
			menynodeIkon = new ImageIcon(node.getIkon().getIkonLob());
		}
		setTresti(node.getTresti());
		Menygruppe gruppe = node.getMenygruppe();
		if (gruppe != null) {
			setGruppe(gruppe);
		}
		tilgangspunkt = node.getTilgangspunkt();
		if (tilgangspunkt != null) {
			setKlassenavn(tilgangspunkt.getKlassenavn());
		}

	}

	public boolean equals(Object o) {
		return (o instanceof MenynodeDTO && ((MenynodeDTO) o).getVisningsnavn().equals(visningsnavn));
	}

	public Menygruppe getGruppe() {
		return gruppe;
	}

	public String getKlassenavn() {
		return klassenavn;
	}

	public ImageIcon getMenynodeIkon() {
		return menynodeIkon;
	}

	public Tilgangspunkt getTilgangspunkt() {
		return tilgangspunkt;
	}

	public String getTresti() {
		return tresti;
	}

	public String getUrl() {
		return url;
	}

	public String getVisningsnavn() {
		return visningsnavn;
	}

	/**
	 * Hvis denne er true, vil ikke MenyTreeListener aktivisere menypunktet,
	 * bare gjøre det valgt.
	 * 
	 * @return
	 */
	public boolean isFjernValgt() {
		return fjernValgt;
	}

	public boolean isStartet() {
		return startet;
	}

	/**
	 * Hvis denne er true, vil ikke MenyTreeListener aktivisere menypunktet,
	 * bare gjøre det valgt.
	 * 
	 * @param fjernValgt
	 */
	public void setFjernValgt(boolean fjernValgt) {
		this.fjernValgt = fjernValgt;
	}

	public void setGruppe(Menygruppe gruppe) {
		this.gruppe = gruppe;
	}

	public void setKlassenavn(String className) {
		this.klassenavn = className;
	}

	public void setMenynodeIkon(ImageIcon menynodeIkon) {
		this.menynodeIkon = menynodeIkon;
	}

	public void setStartet(boolean startet) {
		this.startet = startet;
	}

	public void setTilgangspunkt(Tilgangspunkt tilgangspunkt) {
		this.tilgangspunkt = tilgangspunkt;
	}

	public void setTresti(String treePath) {
		this.tresti = treePath;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setVisningsnavn(String applicationName) {
		this.visningsnavn = applicationName;
	}

	/**
	 * Benyttes i rendereringen av tremeny, så må være GUI kompatibel
	 * 
	 * @return
	 */
	public String toString() {
		return getVisningsnavn();
	}

	public boolean isSynlig() {
		return synlig;
	}
	public boolean isValgbar(){
		return valgbar;
	}
}
