package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class DisabledRightAlignCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    public DisabledRightAlignCellRenderer() {
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
    	Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if (renderer instanceof JLabel){
        	renderer.setEnabled(false);
        	((JLabel) renderer).setHorizontalAlignment(JLabel.RIGHT);
        }
        return this;
    }
    
    @Override
    protected void setValue(Object value) {
    	if ((value != null) && (value instanceof Number)) {
    		Locale myLocale = Locale.getDefault(); 

			NumberFormat numberFormat = NumberFormat.getInstance(myLocale);
			numberFormat.setMaximumFractionDigits(2);
			numberFormat.setMinimumFractionDigits(0);
			numberFormat.setMinimumIntegerDigits(1);
			
			value = numberFormat.format(value);
        }
        super.setValue(value);
    }
}