package no.mesta.mipss.ui.picturepanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXImageView;

import sun.awt.image.ToolkitImage;


/**
 * En dialog som viser frem bilder. Inneholder div funksjonalitet for visning av bildene.
 * 
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class JPictureDialogue extends JDialog {
	private static final PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	
    private static final Logger logger = LoggerFactory.getLogger(JPictureDialogue.class);
    private JXImageView imageView;
    private MipssPictureModel model;
    private Dimension imageSize;
	private int currentImageIndex;
	private BufferedImage activeImage;
    private double currentScale=1;
    private boolean isFitting = false;
    private JTextPane beskrivelse;
    
    public JPictureDialogue(JFrame owner, URL imageUrl){
    	InputStream is;
		try {
			is = imageUrl.openStream();
			byte[] imageBytes = IOUtils.toByteArray(is);
			DefaultMipssPictureModel model = new DefaultMipssPictureModel();
	    	Bilde b = new Bilde();
	    	b.setBildeLob(imageBytes);
	    	model.addBilde(b);
	    	init(owner, model);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public JPictureDialogue(JFrame owner, final MipssPictureModel model){
    	super(owner, false);
    	init(owner, model);
        
    }
    public JPictureDialogue(JDialog owner, final MipssPictureModel model){
    	super(owner, false);
    	init(owner, model);
        
    }
	private void init(Window owner, final MipssPictureModel model) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    	setTitle(resources.getGuiString("picturedialog.title"));
    	JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(550,550));
        setMinimumSize(new Dimension(200,300));
        panel.setLayout(new BorderLayout());
        
    	setBackground(Color.WHITE);
    	this.model = model;
    	
    	imageView = new JXImageView();
        imageView.setDragEnabled(false);
        imageView.setEditable(true);
        imageView.setBorder(BorderFactory.createLineBorder(Color.black));
        imageView.addMouseWheelListener(new ZoomMouseWheelListener());
        
        beskrivelse = new JTextPane();
        beskrivelse.setEditable(false);
              
        showBilde(0);

        panel.add(imageView);
        JPanel southPanel = new JPanel();
        southPanel.setLayout(new BorderLayout());
        southPanel.add(new Thumbs(), BorderLayout.SOUTH);
        JPanel buttonPanel = new JPanel();
        JButton prev = new JButton(new PrevAction(IconResources.GO_PREVIOUS_ICON));
        JButton next = new JButton(new NextAction(IconResources.GO_NEXT_ICON));
        JButton clockwise = new JButton(new RotateC(IconResources.ROTATECW_ICON));
        JButton counterclockwise = new JButton(new RotateCC(IconResources.ROTATECCW_ICON));
        JButton zoomIn = new JButton(new ZoomInAction(IconResources.ZOOM_IN_ICON));
        JButton zoomOut = new JButton(new ZoomOutAction(IconResources.ZOOM_OUT_ICON));
        final FitAction fitAction = new  FitAction(IconResources.FITWINDOW_ICON);
        JButton fit = new JButton(fitAction);
        JButton actual = new JButton(new ActualPixelsAction(IconResources.ACTUAL_PIXELS_ICON));
        JButton save = new JButton(new SaveAction(IconResources.SAVE_ICON));
       
        prev.setToolTipText(resources.getGuiString("picturedialog.prev.text"));
        next.setToolTipText(resources.getGuiString("picturedialog.next.text"));
        clockwise.setToolTipText(resources.getGuiString("picturedialog.clockwise.text"));
        counterclockwise.setToolTipText(resources.getGuiString("picturedialog.counterclockwise.text"));
        zoomIn.setToolTipText(resources.getGuiString("picturedialog.zoomIn.text"));
        zoomOut.setToolTipText(resources.getGuiString("picturedialog.zoomOut.text"));
        fit.setToolTipText(resources.getGuiString("picturedialog.fit.text"));
        actual.setToolTipText(resources.getGuiString("picturedialog.actual.text"));
        save.setToolTipText(resources.getGuiString("picturedialog.save.text"));
        
        
        buttonPanel.add(prev);
        buttonPanel.add(next);
        buttonPanel.add(zoomIn);
        buttonPanel.add(zoomOut);
        buttonPanel.add(clockwise);
        buttonPanel.add(counterclockwise);
        buttonPanel.add(fit);
        buttonPanel.add(actual);
        buttonPanel.add(save);
        southPanel.add(beskrivelse, BorderLayout.NORTH);
        southPanel.add(buttonPanel, BorderLayout.CENTER);
        panel.add(southPanel, BorderLayout.SOUTH);
        
        setContentPane(panel);
        setLocationRelativeTo(owner);
        this.addComponentListener(new ComponentListener(){
        	@Override
			public void componentResized(ComponentEvent e) {}
        	@Override
			public void componentShown(ComponentEvent e) {}
			@Override
			public void componentHidden(ComponentEvent e) {}
			@Override
			public void componentMoved(ComponentEvent e) {
				if (isFitting)
					fitAction.actionPerformed(null);
			}
        });
        pack();
        logger.debug("JPictureDialogue() ccreated");
        fitAction.actionPerformed(null);
	}
    
    private void saveImage(File file, BufferedImage saveImage){
    	JDialog pd = new JDialog(this);
    	pd.setTitle("Lagrer...");
    	JProgressBar bar = new JProgressBar();
    	pd.add(bar);
    	pd.setSize(300,50);
    	pd.setLocationRelativeTo(null);
    	pd.setVisible(true);
    	SaveWorker w = new SaveWorker(pd, bar, file, saveImage);
    	w.execute();
    	pd.setModal(true);
    }
    
    public static void main(String[] args) {
		DefaultMipssPictureModel model = new DefaultMipssPictureModel();
		Bilde b = new Bilde();

		model.addBilde(create(IconResources.HENDELSE_ICON, "A"));
		
		model.addBilde(create(IconResources.LOGO, "B"));
		model.addBilde(create(IconResources.LOGO, "G"));
		model.addBilde(create(IconResources.LOGO, "J"));
		
		JPictureDialogue dialog = new JPictureDialogue((JFrame)null, model);
		dialog.pack();
		dialog.setVisible(true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
    private static Bilde create(ImageIcon icon, String guid){
    	Bilde b = new Bilde();
    	b.setGuid(guid);
    	b.setBildeLob(ImageUtil.imageToBytes((ToolkitImage)icon.getImage()));
    	return b;
    }
    private double calculateScale(int itemWidth, int itemHeight, int targetWidth, int targetHeight) {
        double scale = 1;
        
        if(itemWidth > itemHeight) {
            scale = (double)targetWidth/ (double)itemWidth;
        } else {
            scale = (double)targetHeight/(double)itemHeight;
        }
        
        return scale;
    }
    
    public void showBilde(int index) {
        Bilde b = model.getBilde(index);
        this.currentImageIndex = index;
		try {
			this.activeImage = ImageIO.read(new ByteArrayInputStream(b.getBildeLob()));
		} catch (Throwable e) {
			e.printStackTrace();
			this.activeImage = b.getImage();
		}
//        this.activeImage = new ImageIcon(b.getBildeLob());
        imageSize = new Dimension(activeImage.getWidth(), activeImage.getHeight());
        imageView.setImage(activeImage);
        imageView.setMinimumSize(new Dimension((int)imageSize.getWidth()/2, (int) imageSize.getHeight() / 2));
        imageView.setPreferredSize(imageSize);
        imageView.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        setTitle(resources.getGuiString("picturedialog.title")+" "+ b.getFilnavn());
        
        beskrivelse.setText(b.getBeskrivelse());
        
        new FitAction(null).actionPerformed(null);
    }
        
    /**
     * Et panel med thumbnails av bilder som er tilgjengelig
     * 
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    class Thumbs extends JPanel {
        private JScrollPane scroller;
        private int thumbwidth = 120, thumbheight = 120;
        
        public Thumbs() {
            setBackground(Color.WHITE);
            setOpaque(true);
            setBorder(BorderResources.createComponentBorder());
            scroller = new JScrollPane();
            
            initScroller();

            setLayout(new BorderLayout());
            add(scroller, BorderLayout.CENTER);
        }
        
        private void initScroller() {
            Box box = Box.createHorizontalBox();
            box.setOpaque(true);
            box.setBackground(Color.WHITE);

            Dimension sMin = new Dimension((int) imageSize.getWidth(), thumbheight + 20);
            Dimension sMax = new Dimension(Integer.MAX_VALUE, (int) thumbheight + 20);
            Dimension sPre = sMin;
            ComponentSizeResources.setComponentSizes(scroller, sMin, sPre, sMax);

            box.add(Box.createHorizontalStrut(10));
            Dimension thumbSize = new Dimension(thumbwidth, thumbheight);
            for(Bilde b:model.getBilder()) {
                JXImageView  thumb = new JXImageView();
                BufferedImage image = null;
                if (b.getSmaabildeLob()!=null){
                	image = b.getSmaaImage();
                }else{
                	image = b.getImage();
                }
                thumb.setImage(image);
                thumb.setScale(calculateScale(image.getWidth(), image.getHeight(), thumbwidth, thumbheight));
                thumb.setDragEnabled(false);
                thumb.setEditable(false);
                thumb.addMouseListener(new ClickThumbListener(thumb, b));
                ComponentSizeResources.setComponentSize(thumb, thumbSize);
                box.add(thumb);
                box.add(Box.createHorizontalStrut(10));
            }
            
            scroller.setBorder(null);
            scroller.setViewportView(box);
        }

		
    }
    
    private void zoomIn(){
    	double scale = imageView.getScale();
		scale+=0.5;
		currentScale = scale;
		imageView.setScale(scale);
		isFitting = false;
    }
    
    private void zoomOut(){
    	double scale = imageView.getScale();
		if (scale<0.6)
			scale-=0.1;
		else 
			scale -=0.5;
		
		if (scale>0.1){
			currentScale = scale;
			imageView.setScale(scale);
			isFitting = false;
		}
    }
    
    /**
     * Sørger for at det byttes til det bildet som det klikkes på
     * 
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    class ClickThumbListener implements MouseListener {
        private Bilde b;
		private JXImageView thumb;
        
        /**
         * Konstruktør
         * 
         * @param b
         */
        public ClickThumbListener(JXImageView thumb, Bilde b) {
            this.b = b;
            this.thumb = thumb;
        }

        /** {@inheritDoc} */
        public void mouseClicked(MouseEvent e) {
            
//            computeWindowSize();
        }
        @Override
		public void mouseEntered(MouseEvent e) {
//			thumb.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, new Color(0f,0f,0.9f, 0.3f), new Color(0f,0f,0.5f, 0.3f)));
        	thumb.setScale(thumb.getScale()+0.05);
		}

		@Override
		public void mouseExited(MouseEvent e) {
//			thumb.setBorder(null);
			thumb.setScale(thumb.getScale()-0.05);
			
		}

        /** {@inheritDoc} */
        public void mouseReleased(MouseEvent e) {
        	int index = model.getBildeIndex(b);
            showBilde(index);
        }

		@Override
		public void mousePressed(MouseEvent e) {
			
		}
    }
    /**
     * Filterklasse for å kun vise frem bildefiler av typen: gif, png og jpg i lagre dialogen
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class BildeFilter extends FileFilter{

		@Override
		public boolean accept(File f) {
			if (f.isDirectory())
				return true;
			int extIndex = f.getName().lastIndexOf('.');
			if (extIndex!=-1){
				String ext = f.getName().substring(extIndex+1);
				return ext.equals("gif") || ext.equals("png") || ext.equals("jpg");
			}
			return false;
		}

		@Override
		public String getDescription() {
			return "Bilder";
		}
    	
    }
    
    /**
     * Action klasse for 'forrige' knappen velger forrige bilde i listen dersom index ikke er 0
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class PrevAction extends AbstractAction{
    	public PrevAction(ImageIcon icon){
    		super("", icon);
    	}
    	
    	@Override
		public void actionPerformed(ActionEvent e) {
			if (currentImageIndex!=0){
				showBilde(--currentImageIndex);
				
			}
		}
    }
    /**
     * Actionklasse for 'neste' knappen, velger neste bilde i listen dersom index ikke er større enn modellens antall bilder
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class NextAction extends AbstractAction{
    	
    	public NextAction(ImageIcon icon){
    		super("", icon);
    	}
		@Override
		public void actionPerformed(ActionEvent e) {
			if (currentImageIndex<model.size()-1){
				showBilde(++currentImageIndex);
				
			}
		}
 	
    }
    /**
     * Actionklasse for 'roter med klokken' knappen.
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class RotateC extends AbstractAction{
    	public RotateC(ImageIcon icon){
    		super("", icon);
    	}
		@Override
		public void actionPerformed(ActionEvent e) {
			imageView.getRotateClockwiseAction().actionPerformed(e);
			imageView.setScale(currentScale);
			if (isFitting){
				new FitAction(null).actionPerformed(null);
			}
		}
    }
    /**
     * Actionklasse for 'roter mot klokken' knappen.
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class RotateCC extends AbstractAction{
    	public RotateCC(ImageIcon icon){
    		super("", icon);
    	}
    	@Override
		public void actionPerformed(ActionEvent e) {
			imageView.getRotateCounterClockwiseAction().actionPerformed(e);
			imageView.setScale(currentScale);
			if (isFitting){
				new FitAction(null).actionPerformed(null);
			}
		}
    }
    /**
     * Actionklasse for 'Zoom in' knappen. 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class ZoomInAction extends AbstractAction{
    	public ZoomInAction(ImageIcon icon){
    		super("", icon);
    	}
    	@Override
		public void actionPerformed(ActionEvent e) {
			zoomIn();
		}
    }
    /**
     * Actionklasse for 'Zoom ut' knappen, zoomer ut så lenge det er mulig å se bildet på skjermen. 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class ZoomOutAction extends AbstractAction{
    	public ZoomOutAction(ImageIcon icon){
    		super("", icon);
    	}
    	@Override
		public void actionPerformed(ActionEvent e) {
			zoomOut();
				
		}
    }
    /**
     * Actionklasse for 'beste tilpasning' knappen. Tilpasser bildet til størrelsen på imageview panelet. 
     * Dersom bildet er rotert må bredden til imageview deles med høyden til bildet og høyden til imageview 
     * deles med bredden til bilde for å få riktig skaleringsfaktor. Deretter velges minste skaleringsfaktor for
     * å sørge for at hele bildet vises innenfor rammene til imageview. 
     * 
     * Etter at denne klassen har kjørt vil bildet tilpasse seg endringer i dialogen. (auto-fit)
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class FitAction extends AbstractAction{
    	public FitAction(ImageIcon icon){
    		super("", icon);
    	}
    	@Override
		public void actionPerformed(ActionEvent e) {
    		autoFit();
		}
    }
    private void autoFit(){
    	double scale = 1;
		double width = imageView.getImage().getWidth(null);
		double height = imageView.getImage().getHeight(null);
		if (height>width){
			double s2 = ((double)imageView.getWidth()-10)/(double)activeImage.getHeight();
			double s1 = ((double)imageView.getHeight()-10)/(double)activeImage.getWidth();
			scale = Math.min(s2, s1);
		}
		else{
			double s2 = ((double)imageView.getWidth()-10)/(double)activeImage.getWidth();
			double s1 = ((double)imageView.getHeight()-10)/(double)activeImage.getHeight();

			scale = Math.min(s2, s1);
		}
		imageView.setScale(scale);
		currentScale = scale;
		imageView.setImageLocation(new Point2D.Double(imageView.getWidth()/2,imageView.getHeight()/2));
		isFitting = true;
    }
    /**
     * Actionklasse for 'Forhåndsvisning av bilde' knappen. Setter skalering til 1. 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class ActualPixelsAction extends AbstractAction{
    	public ActualPixelsAction(ImageIcon icon){
    		super("", icon);
    	}
    	@Override
		public void actionPerformed(ActionEvent e) {
			imageView.setScale(1.0);
			imageView.setImageLocation(new Point2D.Double(imageView.getWidth()/2,imageView.getHeight()/2));
			currentScale = 1;
			isFitting = false;
		}
    }
    /**
     * Actionklasse for 'lagre' knappen. Viser en FileChooser slik at brukeren kan lagre bildet til disk. 
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class SaveAction extends AbstractAction{
    	public SaveAction(ImageIcon icon){
    		super("", icon);
    	}
    	@Override
		public void actionPerformed(ActionEvent e) {
    		
    		String filnavn = null;
    		Bilde b = model.getBilde(currentImageIndex);
    		if (b!=null){
    			filnavn = b.getFilnavn();
    		}
    		if (filnavn==null||"".equals(filnavn)){
    			String date = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
    			date = date.replace(":", "");
    			date = date.replace(" ", "_");
    			date = date.replace(".", "");
    			filnavn = "bilde"+date+".jpg";
    		}
			JFileChooser chooser = new JFileChooser("H:");
			chooser.setSelectedFile(new File(filnavn));
			chooser.setFileFilter(new BildeFilter());
			int rvl = chooser.showSaveDialog(JPictureDialogue.this);
			if (rvl == JFileChooser.APPROVE_OPTION){
				File selectedFile = chooser.getSelectedFile();
				saveImage(selectedFile, activeImage);
			}
		}
    }
    
    class SaveWorker extends SwingWorker{
    	private JDialog dialog;
    	private JProgressBar bar;
    	private File file;
		private final BufferedImage image;
    	
    	public SaveWorker(JDialog dialog, JProgressBar bar, File file, BufferedImage image){
    		this.dialog = dialog;
    		this.bar = bar;
    		this.file = file;
			this.image = image;
    	}
    	
		@Override
		protected Object doInBackground() throws Exception {
			bar.setIndeterminate(true);;
			bar.setString("Vennligst vent, lagrer bilde...");
			bar.setStringPainted(true);
	    		ImageUtil.saveCompressedImage(image, file);
//				saved = ImageIO.write(image,"PNG",file);
			return true;
		}
		@Override
		protected void done(){
			dialog.dispose();
		}
		
    }
    /**
     * Lytterklasse som zoomer inn og ut bildet når brukeren beveger på musehjulet.
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class ZoomMouseWheelListener implements MouseWheelListener{

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			int rot = e.getWheelRotation();
			if (rot<0){
				zoomIn();
			}
			if (rot>0){
				zoomOut();
			}
				
		}
    	
    }
}
