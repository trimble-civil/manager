package no.mesta.mipss.ui;

import java.util.EventListener;


/**
 * Lytter interface for klasser som skal lytte på MippsBeanTableModel
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @see no.mesta.mipss.ui.beantable.MipssBeanTableModel
 */
public interface MipssBeanLoaderListener extends EventListener {

    /**
     * Forteller en lytter at lasting av data har startet i modellen
     * 
     * @param event
     */
    public void loadingStarted(MipssBeanLoaderEvent event);
    
    /**
     * Forteller en lytter at lasting av data er ferdig i modellen
     * 
     * @param event
     */
    public void loadingFinished(MipssBeanLoaderEvent event);
}
