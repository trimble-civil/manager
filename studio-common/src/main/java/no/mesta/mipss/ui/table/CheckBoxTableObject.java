package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

public interface CheckBoxTableObject extends IRenderableMipssEntity{

	public void setValgt(Boolean valgt);
	public Boolean getValgt();
	
}
