package no.mesta.mipss.ui;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * Lager den typen rammer vi vil ha
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class BorderResources {
    
    /**
     * Lager en ramme med tittel
     * 
     * @param title
     * @return
     */
    public static Border createComponentTitleBorder(String title) {
         return BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), title, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP));
    }
    
    /**
     * Lager en enkel ramme
     * 
     * @return
     */
    public static Border createComponentBorder() {
        return BorderFactory.createEtchedBorder();
    }
}
