package no.mesta.mipss.ui.roadpicker;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IntStringConverter;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.ui.DefaultButtonPanel;
import no.mesta.mipss.ui.VeiAttributtPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXHeader;

/**
 * Velger veisøkeparametere.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JRoadSelectDialog extends JDialog{
	private static final Logger logger = LoggerFactory.getLogger(JRoadSelectDialog.class);
    private IntStringConverter lsb = new IntStringConverter();
    private Long meterFra;
    private Long meterTil;
    private boolean set;
    private VeiAttributtPanel veiAttributtPanel;
    private RoadPickerPanel veivelgerPanel;
    private VeiInfo veiSok = new VeiInfo();
    private BindingGroup group;
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	private Driftkontrakt driftkontrakt;
	private boolean enableMeter;

	public JRoadSelectDialog() {
    }
    
    /**
     * Konstruktør
     * 
     * @param owner
     * @param enableMeter
     * @param enableMeterIntervall
     */
    public JRoadSelectDialog(JFrame owner, boolean enableMeter, boolean enableMeterIntervall) {
        super(owner, true);
        super.setTitle(props.getGuiString("sokEtterVei"));
        initGui(enableMeter, "VelgVeiPanel.header.title", "VelgVeiPanel.header.description");
    }
    
    /**
     * Konstruktør
     * 
     * @param owner
     * @param enableMeter
     * @param enableMeterIntervall
     */
    public JRoadSelectDialog(JDialog owner, boolean enableMeter, boolean enableMeterIntervall) {
        super(owner, true);
		this.enableMeter = enableMeter;
        super.setTitle(props.getGuiString("velgVei"));
        initGui(enableMeter, "VelgVeiPanel.header.title2", "VelgVeiPanel.header.description2");
    }

	private void initGui(boolean enableMeter, String headerTitleKey, String headerDescriptionKey) {
		this.enableMeter = enableMeter;
		veivelgerPanel = new RoadPickerPanel(null, enableMeter);
        veivelgerPanel.setVeiInfo(veiSok);
        
        veiAttributtPanel = new VeiAttributtPanel(enableMeter);
        bind();        
        JTabbedPane tab = new JTabbedPane();
        tab.addTab("Alle veier", veiAttributtPanel);
        tab.addTab("Kontraktveier", veivelgerPanel);
        
        setLayout(new BorderLayout());
        add(getHeader(props, headerTitleKey, headerDescriptionKey), BorderLayout.NORTH);
        add(tab, BorderLayout.CENTER);
        add(DefaultButtonPanel.getDefaultButtonPanel(new OkAction("", null), new ResetAction("Nullstill", null), new CancelAction("Avbryt", null)), BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
	}
    
	private JXHeader getHeader(PropertyResourceBundleUtil props, String headerTitleKey, String headerDescriptionKey){
		JXHeader header = new JXHeader();
		header.setTitle(props.getGuiString(headerTitleKey));
		header.setDescription(props.getGuiString(headerDescriptionKey));
		return header;
	}
	
    @SuppressWarnings("unchecked")
	private void bind() {
        group = new BindingGroup();
        Binding fb = BindingHelper.createbinding(veiSok, "fylkesnummer", veiAttributtPanel.getFylkeField(), "text", BindingHelper.READ_WRITE);
        fb.setConverter(lsb);
        group.addBinding(fb);
        
        Binding kb = BindingHelper.createbinding(veiSok, "kommunenummer", veiAttributtPanel.getKommuneField(), "text", BindingHelper.READ_WRITE);
        kb.setConverter(lsb);
        group.addBinding(kb);

        Binding vkb = BindingHelper.createbinding(veiSok, "veikategori", veiAttributtPanel.getKategoriField(), "text", BindingHelper.READ_WRITE);
        group.addBinding(vkb);
        
        Binding vsb = BindingHelper.createbinding(veiSok, "veistatus", veiAttributtPanel.getStatusField(), "text", BindingHelper.READ_WRITE);
        group.addBinding(vsb);

        Binding vnb = BindingHelper.createbinding(veiSok, "veinummer", veiAttributtPanel.getNummerField(), "text", BindingHelper.READ_WRITE);
        vnb.setConverter(lsb);
        group.addBinding(vnb);

        Binding hb = BindingHelper.createbinding(veiSok, "hp", veiAttributtPanel.getHpField(), "text", BindingHelper.READ_WRITE);
        hb.setConverter(lsb);
        group.addBinding(hb);
        
        if (enableMeter){
	        Binding meter = BindingHelper.createbinding(veiSok, "meter", veiAttributtPanel.getMeterField(), "text", BindingHelper.READ_WRITE);
	        hb.setConverter(lsb);
	        group.addBinding(meter);
        }
        group.bind();
    }
    
    public Long getMeterFra() {
        return meterFra;
    }

    public Long getMeterTil() {
        return meterTil;
    }

    public VeiInfo getVeiSok() {
        return veiSok;
    }

    public boolean isSet() {
        return set;
    }
    
    public void setVeiSok(VeiInfo veiInfo) {
    	logger.debug("setVeiSok(" + veiInfo + ") start");
    	group.unbind();
    	veiSok = veiInfo;
    	veivelgerPanel.setVeiInfo(veiInfo);
    	bind();
    }
    
    public void setDriftkontrakt(Driftkontrakt driftkontrakt){
    	this.driftkontrakt = driftkontrakt;
    	veivelgerPanel.setDriftkontrakt(driftkontrakt);
    }
    public Driftkontrakt getDriftkontrakt(){
    	return this.driftkontrakt;
    }
    @Override
    protected void finalize() throws Throwable {
    	super.finalize();
    	
    	if(group != null) {
    		group.unbind();
    		group = null;
    		veiSok = null;
    	}
    }
    
    /**
	 * Actionklasse for avbrytknappen
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	private class CancelAction extends AbstractAction{
		public CancelAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			set = false;
            JRoadSelectDialog.this.setVisible(false);
		}
	}
	
    /**
     * Actionklasse for ok knappen
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    private class OkAction extends AbstractAction{
		public OkAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			set = true;
            JRoadSelectDialog.this.setVisible(false);
		}
	}
    
	/**
     * Actionklasse for nullstill knappen
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
	private class ResetAction extends AbstractAction{
		public ResetAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			set = true;
			if (veiSok!=null){
				veiSok.clearPropertyChangeListeners();
			}
	    	setVeiSok(new VeiInfo());//veiSok = null;
		}
	}
}
