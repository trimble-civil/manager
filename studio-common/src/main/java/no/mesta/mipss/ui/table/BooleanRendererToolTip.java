package no.mesta.mipss.ui.table;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.resources.images.IconResources;

/**
 * Lett modifisert til å kunne inneholde tooltiptekster.
 * @author OLEHEL
 *
 */
public class BooleanRendererToolTip extends DefaultTableCellRenderer implements TableCellRenderer {

	private ImageIcon icon;
	private final boolean revert;
	private String tooltipText;
	public BooleanRendererToolTip() {
		this(IconResources.OK2_ICON, false,"");
	}
	
	public BooleanRendererToolTip(ImageIcon icon, boolean revert, String toolTipText){
		this.icon = icon;
		this.revert = revert;
		this.setTooltipText(toolTipText);
	}

	/** {@inheritDoc} */
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(component.getBackground());
		panel.setForeground(component.getForeground());
		
		if (value != null && value instanceof Boolean) {
			Boolean b = (Boolean) value;
			boolean add = false;
			if (revert){
				if (!b){
					add = true;
				}
			}else{
				if (b) {
					add = true;
				}
			}
			if (add){
				JLabel iconLabel = new JLabel(icon);
				panel.setToolTipText(tooltipText);
				panel.add(iconLabel, BorderLayout.CENTER);
			}
		}

		return panel;
	}

	public void setTooltipText(String tooltipText) {
		this.tooltipText = tooltipText;
	}

	public String getTooltipText() {
		return tooltipText;
	}
}