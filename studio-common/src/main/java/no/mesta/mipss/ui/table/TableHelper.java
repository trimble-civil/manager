package no.mesta.mipss.ui.table;

import java.awt.Color;

import javax.swing.UIManager;
import javax.swing.table.TableColumn;

public class TableHelper {

	public static Color getCellBackground() {
		return UIManager.getColor("Table.background");
	}
	public static Color getCellForeground() {
		return UIManager.getColor("Table.foreground");
	}
	public static Color getSelectedBackground() {
		return UIManager.getColor("Table.selectionBackground");
	}
	public static Color getSelectedDefaultForeground() {
		return UIManager.getColor("Table.selectionForeground");
	}
	public static Color getSelectedForeground() {
		Color c = new Color(200, 0, 0);
		return c;
	}
	public static void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}
}
