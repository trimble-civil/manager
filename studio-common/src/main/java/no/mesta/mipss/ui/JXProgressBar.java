package no.mesta.mipss.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Polygon;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class JXProgressBar extends JPanel{
	private int x;
	private int size;
	private boolean running;
	
	private Thread move;
	
	private Color transparent = new Color(1, 1, 1, 0f);

	private BufferedImage image;
	private Graphics2D ig;
	private String text="";
	private Color blue =  new Color(82,116,184);
	private Color white = new Color(132,166,234);
	
	private void createBufferedImage(boolean color){
		size = getHeight();
		int type = color?BufferedImage.TYPE_INT_ARGB:BufferedImage.TYPE_BYTE_GRAY;
		image = new BufferedImage(size*2, size, type);
		
		ig = image.createGraphics();
		Polygon p1 = new Polygon();
		p1.addPoint(0, 0);
		p1.addPoint(size, 0);
		p1.addPoint(0, size);
		ig.setPaint(blue);
		ig.fill(p1);
		
		Polygon p2 = new Polygon();
		p2.addPoint(size, 0);
		p2.addPoint(size*2, 0);
		p2.addPoint(size, size);
		p2.addPoint(0, size);
		ig.setPaint(white);
		ig.fill(p2);
		
		Polygon p3 = new Polygon();
		p3.addPoint(size*2, 0);
		p3.addPoint(size*2, size);
		p3.addPoint(size, size);
		ig.setPaint(blue);
		ig.fill(p3);
		
		LinearGradientPaint shade = new LinearGradientPaint(0, getHeight()/2, 0, 0, new float[]{0,1}, new Color[]{ new Color(0,0,0,0.4f), transparent});
		ig.setPaint(shade);
		ig.fillRect(0, 0, getWidth(), getHeight()/2);
		ig.setPaint(new Color(0,0,0,0.4f));
		ig.fillRect(0, getHeight()/2, getWidth(), getHeight()/2);
	}
	
	public void setString(String string){
		this.text = string;
	}
	public void paint(Graphics gr){
		Graphics2D g = (Graphics2D)gr;
		if (size!=getHeight()){
			createBufferedImage(isIndeterminate());
		}
		
		int c = getWidth()/size+1;
		for (int i=0;i<c;i++){
			int xx = i*size*2+x; 
			g.drawImage(image, xx, 0, this);
		}
		int w = 3;
		LinearGradientPaint top = new LinearGradientPaint(0, 0, 0, w, new float[]{0,1}, new Color[]{Color.white, transparent});
		g.setPaint(top);
		g.fillRect(0, 0, getWidth(), w);
		LinearGradientPaint bottom = new LinearGradientPaint(0, getHeight()-w, 0, getHeight(), new float[]{0,1}, new Color[]{transparent, Color.white});
		g.setPaint(bottom);
		g.fillRect(0, getHeight()-w, getWidth(), w);
		LinearGradientPaint left = new LinearGradientPaint(0, 0, w, 0, new float[]{0,1}, new Color[]{Color.white, transparent});
		g.setPaint(left);
		g.fillRect(0, 0, w, getHeight());
		LinearGradientPaint right = new LinearGradientPaint(getWidth()-w, 0, getWidth(), 0, new float[]{0,1}, new Color[]{transparent, Color.white});
		g.setPaint(right);
		g.fillRect(getWidth()-w, 0, w, getHeight());
		g.setColor(Color.darkGray);
		g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 5, 5);
		drawString(g);
	}
	
	private void drawString(Graphics2D g){
		Font old = g.getFont();
		int stringWidth = g.getFontMetrics().stringWidth(text);
		int sx = getWidth()/2-stringWidth/2;
		int sy = getHeight()/2+4;
		g.setPaint(Color.white);
		g.drawString(text, sx, sy);
		g.setFont(old);
	}
	public void setIndeterminate(boolean indeterminate){
		if (indeterminate){
			
			start();
		}else
			stop();
		size=0;
	}
	public boolean isIndeterminate(){
		return running;
	}
	public void start(){
		running = true;
		if (move ==null){
			move = new Thread(){
				public void run(){
					while (running){
						if (x<=-(size*2))
							x = 0;
						else{
							x--;
						}
						try{
							Thread.sleep(10);
						}catch (InterruptedException e){
							e.printStackTrace();
						}
						repaint();
					}
				}
			};
			move.start();
		}
	}
	
	public void stop(){
		if (move!=null){
			running = false;
			move = null;
		}
	}
	public static void main(String[] args) {
		final JXProgressBar bar = new JXProgressBar();
		
		JFrame f = new JFrame();
		f.add(bar);
		f.setSize(300,150);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
//		JButton start = new JButton("start");
//		JButton stop = new JButton("stop");
//		start.addActionListener(new ActionListener(){
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				bar.setIndeterminate(true);
//			}
//		});
//		stop.addActionListener(new ActionListener(){
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				bar.setIndeterminate(false);
//			}
//		});
//		
//		JPanel p = new JPanel();
//		p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
//		p.add(Box.createHorizontalGlue());
//		p.add(start);
//		p.add(Box.createRigidArea(new Dimension(5, 0)));
//		p.add(stop);
//		p.add(Box.createHorizontalGlue());
//	
//		
//		JPanel b = new JPanel();
//		b.setLayout(new BoxLayout(b, BoxLayout.LINE_AXIS));
//		b.setMaximumSize(new Dimension(150,35));
//		b.setMinimumSize(new Dimension(150,25));
//		b.add(bar);
//		
//		JPanel m = new JPanel();
//		m.setLayout(new BoxLayout(m, BoxLayout.PAGE_AXIS));
//		m.add(p);
//		m.add(Box.createRigidArea(new Dimension(0,10)));
//		m.add(b);
////		m.add(Box.createVerticalGlue());
//		
//		f.add(m);
//		f.pack();
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		f.setLocationRelativeTo(null);
//		f.setVisible(true);
	}
}
