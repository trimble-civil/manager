package no.mesta.mipss.ui.table;

import javax.swing.JTextArea;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.renderer.CellContext;
import org.jdesktop.swingx.renderer.ComponentProvider;
/**
 * use a JTextArea as rendering component.
 */
@SuppressWarnings("serial")
public class TextAreaProvider extends ComponentProvider<JTextArea> {
	 
    @SuppressWarnings("unchecked")
	@Override
    protected void configureState(CellContext context) {
        JXTable table = (JXTable) context.getComponent();
        int columnWidth = table.getColumn(context.getColumn()).getWidth();
        rendererComponent.setSize(columnWidth, Short.MAX_VALUE);
    }

    @Override
    protected JTextArea createRendererComponent() {
        JTextArea area = new JTextArea();
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setOpaque(true);
        return area;
    }

    @SuppressWarnings("unchecked")
	@Override
    protected void format(CellContext context) {
        rendererComponent.setText(getValueAsString(context));
    }
}