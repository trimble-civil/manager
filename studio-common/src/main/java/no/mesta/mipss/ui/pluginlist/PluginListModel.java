package no.mesta.mipss.ui.pluginlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginCacheKey;
import no.mesta.mipss.plugin.PluginLookupKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modell som holder listen over lastede pluginKeys
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PluginListModel {
    private static final Logger logger = LoggerFactory.getLogger(PluginListModel.class);
    private List<PluginListModelListener> listeners = new ArrayList<PluginListModelListener>();
    private Stack<PluginCacheKey> pluginKeys = new Stack<PluginCacheKey>();
	private Map<PluginCacheKey, MipssPlugin> plugins = new HashMap<PluginCacheKey, MipssPlugin>();
    
    /**
     * Konstruktør
     */
    public PluginListModel() {
    }
    
    /**
     * Legger til en lytter til modellen
     * 
     * @param listener
     */
    public void addPluginListModelListener(PluginListModelListener listener) {
        listeners.add(listener);
    }
    
    /**
     * Søker frem en passende plugin
     * 
     * @param searchKey
     * @return
     */
	public MipssPlugin find(PluginLookupKey searchKey) {
        logger.trace("find(" + searchKey + ") start");
        Iterator<PluginCacheKey> keyIterator = pluginKeys.iterator();
        MipssPlugin plugin = null;
        
        while(keyIterator.hasNext()) {
            PluginCacheKey key = keyIterator.next();
            PluginLookupKey target = key.getLookupKey();
            if(searchKey.equals(target)) {
                plugin = plugins.get(key);
                if(plugin.isDocked()) {
                    break;
                } else {
                    plugin = null;
                }
            }
        }
        
        logger.trace("find(" + searchKey + ") end with: " + plugin);
        return plugin;
    }
    
    /**
     * Notifiserer lyttere om at modellen har endret seg
     * 
     */
    private void fireModelChanged(PluginListModelEvent event) {
        for(PluginListModelListener listener:listeners) {
            listener.modelChanged(event);
        }
    }
    
    /**
     * Uthenting uavhengig av orden
     * 
     * @param key
     * @return
     */
	public MipssPlugin get(PluginCacheKey key) {
       return plugins.get(key); 
    }
    
    /**
     * Returnerer en liste over alle plugins
     * 
     * @return
     */
	public List<MipssPlugin> getPlugins() {
        return new ArrayList<MipssPlugin>(plugins.values());
    }
    
    /**
     * Viser den sist  innlagte nøkkelen i modellen
     * 
     * @return
     */
	public MipssPlugin peek() {
        PluginCacheKey key = pluginKeys.peek();
        return plugins.get(key);
    }
    
    /**
     * Fortell modellen at plugin som nøkkelen viser til har endret seg
     * 
     * @param key
     */
    public void poke(PluginCacheKey key) {
        synchronized(pluginKeys) {
            if(pluginKeys.contains(key)) {
                PluginListModelEvent event = new PluginListModelEvent(this);
                event.setType(PluginListModelEvent.Type.ITEM_CHANGE);
                event.setChangedKey(key);
                fireModelChanged(event);
            }
        }
    }
    
    /**
     * Fjerner en nøkkel fra modellen
     * 
     * @return
     */
	public MipssPlugin pop() {
        MipssPlugin plugin = null;
        
        synchronized(pluginKeys) {
            PluginCacheKey key = pluginKeys.pop();
            plugin = plugins.remove(key);
            PluginListModelEvent event = new PluginListModelEvent(this);
            event.setRemovedKey(key);
            event.setType(PluginListModelEvent.Type.ITEM_REMOVE);
            fireModelChanged(event);
        }
        
        return plugin;
    }
    
    /**
     * Legger inn en ny nøkkel for en plugin inn i modellen
     * 
     * @param key
     */
	public void push(PluginCacheKey key, MipssPlugin plugin) {
        synchronized(pluginKeys) {
            pluginKeys.push(key);
            plugins.put(key, plugin);
            PluginListModelEvent event = new PluginListModelEvent(this);
            event.setNewKey(key);
            event.setType(PluginListModelEvent.Type.ITEM_ADD);
            fireModelChanged(event);
        }
    }
    
    /**
     * Fjerner en nøkkel fra listen uavhengig av posisjon
     * 
     * @param key
     */
    public void remove(PluginCacheKey key) {
        synchronized(pluginKeys) {
            pluginKeys.remove(key);
            plugins.remove(key);
            PluginListModelEvent event = new PluginListModelEvent(this);
            event.setRemovedKey(key);
            event.setType(PluginListModelEvent.Type.ITEM_REMOVE);
            fireModelChanged(event);
        }
    }
    
    /**
     * Fjerner en lytter fra modellen
     * 
     * @param listener
     */
    public void removePluginListModelListener(PluginListModelListener listener) {
        listeners.remove(listener);
    }
    
    /**
     * Antallet plugins i modellen
     * 
     * @return
     */
    public int size() {
        return pluginKeys.size();
    }
}
