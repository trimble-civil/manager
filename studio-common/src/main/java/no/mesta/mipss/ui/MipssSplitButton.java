package no.mesta.mipss.ui;

import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;


public class MipssSplitButton extends DropDownButton{
    
    private JPopupMenu popupMenu;
    
    public MipssSplitButton() {
        popupMenu = new JPopupMenu();
        
    }    
    
    protected JPopupMenu getPopupMenu() {
        return popupMenu;
    }
    
    public void addMenuItem(String title, ActionListener action, ImageIcon icon){
        JMenuItem item = new JMenuItem(title, icon);
        item.addActionListener(action);
        
        popupMenu.add(item);
    }
    public void addMenuItem(String title, String tooltip, ActionListener action, ImageIcon icon){
        JMenuItem item = new JMenuItem(title, icon);
        item.addActionListener(action);
        item.setToolTipText(tooltip);
        
        popupMenu.add(item);
    }

	public void addMenuItem(String title, ActionListener action) {
		JMenuItem item = new JMenuItem(title);
        item.addActionListener(action);
        
        popupMenu.add(item);
	}
}
