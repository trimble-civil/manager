package no.mesta.mipss.ui;

import javax.swing.JPanel;

import no.mesta.mipss.core.IDisposable;

/**
 * Panel som bør benyttes som superklasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public abstract class JAbstractPanel extends JPanel implements IDisposable{

    /** {@inheritDoc} */
    public abstract void dispose();
    
    /**
     * Vindus tittel som vinduer eller faner som skal vise panelet kan bruke
     * 
     * @return
     */
    public abstract String getWindowTitle();
}
