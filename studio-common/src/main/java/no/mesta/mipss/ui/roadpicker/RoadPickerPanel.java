package no.mesta.mipss.ui.roadpicker;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang.builder.EqualsBuilder;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.service.veinett.VeirefVO;
import no.mesta.mipss.ui.DocumentFilter;

@SuppressWarnings("serial")
public class RoadPickerPanel extends JPanel implements PropertyChangeSource{

	private VeinettService veinettService;
	private List<VeirefVO> veirefList;
	
	private DefaultComboBoxModel veiModel;
	private DefaultComboBoxModel hpModel;
	
	private VeirefVO veirefFra;
	private VeiInfo veiInfo;
	private JTextField meterField;
	private final boolean enableMeter;
	private JComboBox veiCombo;
	private JComboBox hpCombo;
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		Driftkontrakt d = new Driftkontrakt();
		d.setId(Long.valueOf(137));
		f.add(new RoadPickerPanel(null, true));
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	
	public RoadPickerPanel(Driftkontrakt driftkontrakt, boolean enableMeter){
		this.enableMeter = enableMeter;
		/*if (driftkontrakt==null){
			throw new IllegalArgumentException("Driftkontrakt kan ikke være null");
		}*/
		veinettService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		initGui();
		setDriftkontrakt(driftkontrakt);
	}
	
	public void setVeiInfo(VeiInfo veiInfo){
		veiCombo.setSelectedItem(null);
		hpCombo.setSelectedItem(null);
		this.veiInfo = veiInfo;
		if (veiInfo!=null&&enableMeter)
			meterField.setText(veiInfo.getMeter()+"");
		if (veiInfo!=null){
			int size = veiModel.getSize();
			for (int i=0;i<size;i++){
				VeirefVO v = (VeirefVO)veiModel.getElementAt(i);
				if (isEqual(veiInfo, v)){
					veiCombo.setSelectedItem(v);
					hpCombo.setSelectedItem(v);
				}
			}
			if (veiInfo.isBlank()){
				VeirefVO v = (VeirefVO)veiCombo.getSelectedItem();
				HpWrapper w = (HpWrapper)hpCombo.getSelectedItem();
				if (v!=null){
					veiInfo.setFylkesnummer(v.getFylkesnummer().intValue());
					veiInfo.setKommunenummer(v.getKommunenummer().intValue());
					veiInfo.setVeikategori(v.getVeitype().charAt(0)+"");
					veiInfo.setVeinummer(v.getVeinummer().intValue());
					veiInfo.setVeistatus(v.getVeitype().charAt(1)+"");
				}
				if (w!=null){
					veiInfo.setHp(w.getVeiref().getHp().intValue());
				}
			}
		}
	}
	
	private boolean isEqual(VeiInfo v, VeirefVO vo){
		return new EqualsBuilder()
		.append(v.getFylkesnummer(), vo.getFylkesnummer().intValue())
		.append(v.getKommunenummer(), vo.getKommunenummer().intValue())
		.append(v.getHp(), vo.getHp().intValue())
		.append(v.getVeikategori(), vo.getVeitype().charAt(0)+"")
		.append(v.getVeistatus(), vo.getVeitype().charAt(1)+"")
		.append(v.getVeinummer(), vo.getVeinummer().intValue())
		.isEquals();
	}
	
	private void initGui() {
		veiModel = new DefaultComboBoxModel();
		
		veiCombo = new JComboBox();
		veiCombo.setModel(veiModel);
		veiCombo.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				VeirefVO v = (VeirefVO)e.getItem();
				hpModel.removeAllElements();
				for (VeirefVO vo:veirefList){
					if (vo.equals(v)){
						hpModel.addElement(new HpWrapper(vo));
					}
				}
				if (veiInfo!=null){
					veiInfo.setFylkesnummer(v.getFylkesnummer().intValue());
					veiInfo.setKommunenummer(v.getKommunenummer().intValue());
					veiInfo.setVeikategori(v.getVeitype().charAt(0)+"");
					veiInfo.setVeinummer(v.getVeinummer().intValue());
					veiInfo.setVeistatus(v.getVeitype().charAt(1)+"");
					if (veiInfo.getMeter()<v.getMinMeter())
						veiInfo.setMeter(v.getMinMeter().intValue());
				}
			}
		});
		
		hpModel = new DefaultComboBoxModel();
		hpCombo = new JComboBox();
		hpCombo.setModel(hpModel);
		hpCombo.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				VeirefVO old = veirefFra;
				HpWrapper w = (HpWrapper)e.getItem();
				veirefFra = w.getVeiref();
				firePropertyChange("veirefFra", old, veirefFra);
				
				if (veiInfo!=null){
					veiInfo.setHp(w.getVeiref().getHp().intValue());
					
					if (veiInfo.getMeter()<w.getVeiref().getMinMeter()){
						System.out.println("ny meter...");
						veiInfo.setMeter(w.getVeiref().getMinMeter().intValue());
					}
					if (enableMeter)
						meterField.setText(veiInfo.getMeter()+"");
				}
			}
		});
		
		if (enableMeter){
			meterField = new JTextField();
			meterField.setDocument(new DocumentFilter(7));
			meterField.getDocument().addDocumentListener(new DocumentListener(){
				@Override
				public void changedUpdate(DocumentEvent e) {}

				@Override
				public void insertUpdate(DocumentEvent e) {
					if (veiInfo!=null){
						 if (!meterField.getText().equals("")){
								 veiInfo.setMeter(Integer.valueOf(meterField.getText()));
						 }
					 }
				}
				@Override
				public void removeUpdate(DocumentEvent e) {
					if (veiInfo!=null){
						 if (!meterField.getText().equals("")){
								 veiInfo.setMeter(Integer.valueOf(meterField.getText()));
						 }
					 }
				}
			});
			
			if (veiInfo!=null)
				meterField.setText(veiInfo.getMeter()+"");
		}
		JLabel veiLabel = new JLabel("Vei:");
		JLabel fraHpLabel = new JLabel("HP:");
		JLabel meterLabel = new JLabel("Meter:");
		
		setLayout(new GridBagLayout());
		
		add(veiLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10,5,5,5), 0,0));
		add(veiCombo, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10,5,5,5), 0,0));
		
		double weightY = enableMeter?0.0:1.0;
		int con = enableMeter?GridBagConstraints.WEST:GridBagConstraints.NORTHWEST;
		add(fraHpLabel, new GridBagConstraints(0,1, 1,1, 0.0,weightY, con, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
		add(hpCombo, new GridBagConstraints(1,1, 1,1, 1.0,weightY, con, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,5), 0,0));
		if (enableMeter){
			add(meterLabel, new GridBagConstraints(0,2, 1,1, 0.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0,0));
			add(meterField, new GridBagConstraints(1,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,5), 0,0));
		}
	}
	
	public void setDriftkontrakt(Driftkontrakt driftkontrakt){
		if (driftkontrakt!=null){
			veirefList = veinettService.getVeirefListForDriftkontrakt(driftkontrakt.getId());
			List<VeirefVO> veirefVOList = new ArrayList<VeirefVO>();
			veiModel.removeAllElements();
			for (VeirefVO v:veirefList){
				if (!veirefVOList.contains(v)){
					veirefVOList.add(v);
					veiModel.addElement(v);
				}
			}
		}
	}
	
	/**
	 * Klasse som benyttes som modell for HP-comboboxene
	 * @author harkul
	 *
	 */
	class HpWrapper{
		private VeirefVO veiref;
		public HpWrapper(VeirefVO veiref){
			this.veiref = veiref;
		}
		public VeirefVO getVeiref(){
			return veiref;
		}
		public String toString(){
			return String.valueOf(veiref.getHp());
		}
	}
}
