package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.jdesktop.beansbinding.Property;

import no.mesta.mipss.core.PropertyUtil;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

/**
 * Rendrer entiteter i tabell iht interfacet IRenderableMipssEntity
 * 
 * @author lareid
 *
 * @param <T>
 */
public class RenderableMipssEntityTableCellRenderer<T extends IRenderableMipssEntity> extends DefaultTableCellRenderer {
	
	private String field;
	
	public RenderableMipssEntityTableCellRenderer(){
		
	}
	public RenderableMipssEntityTableCellRenderer(String field){
		this.field = field;
	}
	
	private Object getValueFromField(Object entity, String field) {
		if (field.startsWith("$")){
			Property<Object, Object> property = PropertyUtil.createProperty(field);
			return property.getValue(entity);
		}
		Object returnValue = null;
		try {
			Method m = entity.getClass().getMethod(getBeanField("get", field));
			returnValue = m.invoke(entity, (Object[]) null);
		} catch (IllegalAccessException iae) {
			iae.printStackTrace();
		} catch (InvocationTargetException ite) {
			ite.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	private String getBeanField(String prefix, String field) {
		String beanField = prefix + field.substring(0, 1).toUpperCase() + field.substring(1);
		return beanField;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		String text = "";
		if(value != null) {
			IRenderableMipssEntity renderableMipssEntity = (IRenderableMipssEntity)value;
			if (field!=null){
				Object val = getValueFromField(value, field);
				if (val!=null)
					text= val.toString();
			}else{
				text = renderableMipssEntity.getTextForGUI();
			}
		}
		return super.getTableCellRendererComponent(table, text, isSelected, hasFocus, row, column);
	}
}
