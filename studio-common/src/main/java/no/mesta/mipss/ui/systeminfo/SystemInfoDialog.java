package no.mesta.mipss.ui.systeminfo;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URI;

import javax.naming.InitialContext;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.resources.images.IconResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Vindu for å vise systeminformasjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SystemInfoDialog extends JDialog {
    private Logger logger = LoggerFactory.getLogger(SystemInfoDialog.class);
    
    private IMipssStudioLoader loader;
    private JLabel dbName;
    private JLabel dbPlatform;
    private JLabel dbHost;
    private JLabel serverHost;
    private JLabel server;
    private JLabel serverPingtime;
    private JLabel maxMemoryCounter;
    private JLabel freeMemoryCounter;
    private JLabel totalMemoryCounter;
    private JLabel threadCountCounter;
    private JLabel pluginCounter;
    private JLabel fileEncoding;
    private JLabel jreVersion;
	private long pingTime;
	
	private boolean running;

	private JPanel panel;

	private JLabel dbSchema; 
	
	
    /**
     * Konstruktør
     * 
     * @param loader
     */
    public SystemInfoDialog(IMipssStudioLoader loader) {
        super(loader.getApplicationFrame(), "Systeminformasjon", false);
        this.loader = loader;

		initGui();
		startWorker();
        setResizable(false);
        setLocationRelativeTo(null);
        pack();
    }
    
	private void startWorker() {
		running = true;
		SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>(){

			@Override
			protected Void doInBackground() throws Exception {
				while (running) {
		            SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							updateSystemInfo();
						}
					});
					Thread.sleep(5000);
		        }
				return null;
			}
			
		};
		sw.execute();
	}

	private void initGui() {
		dbName = new JLabel(getDbName());
		dbPlatform = new JLabel(getDbPlatform());
		dbHost = new JLabel(getDbHost());
		dbSchema = new JLabel(getDbSchema());
		server = new JLabel(getServer());
		serverHost = new JLabel(getServerHostname());
		serverPingtime = new JLabel(getPingtime());
        maxMemoryCounter = new JLabel(getMaxMemory());
        freeMemoryCounter = new JLabel(getFreeMemory());
        totalMemoryCounter = new JLabel(getTotalMemory());
        threadCountCounter = new JLabel(getThreadCount());
        pluginCounter = new JLabel(getPluginCount());
        fileEncoding = new JLabel(System.getProperty("file.encoding"));
        jreVersion = new JLabel(System.getProperty("java.version"));

        setIconImage(IconResources.MODULE_ICON.getImage());
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(11, 11, 11, 11));
        panel.setLayout(new GridLayout(13, 2, 10, 5));
        panel.add(new JLabel("Databasenavn:"));
        panel.add(dbName);
        panel.add(new JLabel("Database-plattform:"));
        panel.add(dbPlatform);
        panel.add(new JLabel("Database-hostname:"));
        panel.add(dbHost);
        panel.add(new JLabel("Server-hostname:"));
        panel.add(serverHost);
        panel.add(new JLabel("JNDI-Provider URL:"));
        panel.add(server);
        panel.add(new JLabel("Server-pingtid:"));
        panel.add(serverPingtime);
        panel.add(new JLabel("Max minne avsatt til VM:"));
        panel.add(maxMemoryCounter);
        panel.add(new JLabel("Fritt minne i VM:"));
        panel.add(freeMemoryCounter);
        panel.add(new JLabel("Totalt minne i bruk i VM:"));
        panel.add(totalMemoryCounter);
        panel.add(new JLabel("Antall tråder:"));
        panel.add(threadCountCounter);
        panel.add(new JLabel("Antall lastede plugins:"));
        panel.add(pluginCounter);
        panel.add(new JLabel("File encoding:"));
        panel.add(fileEncoding);
        panel.add(new JLabel("Java versjon:"));
        panel.add(jreVersion);
        JPanel content = new JPanel(new BorderLayout());
        content.add(panel);
        content.add(new JButton(getCopyAction()), BorderLayout.SOUTH );
        add(content);
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowListener() {
            public void windowOpened(WindowEvent e) {
            }

            public void windowClosing(WindowEvent e) {
            }

            public void windowClosed(WindowEvent e) {
            }

            public void windowIconified(WindowEvent e) {
            }

            public void windowDeiconified(WindowEvent e) {
            }

            public void windowActivated(WindowEvent e) {
            }

            public void windowDeactivated(WindowEvent e) {
            }
        });
	}
    private String getDbSchema() {
		return BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class).getDbSchema();

	}

	private String getDbHost() {
		return BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class).getDbHost();
	}

	private String getDbPlatform() {
		return BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class).getDbPlatform();
	}

	private String getDbName() {
		return BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class).getDbName();
	}

	private AbstractAction getCopyAction(){
    	return new AbstractAction("Kopier") {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				StringBuilder sb = new StringBuilder();
				for (int i=0;i<panel.getComponentCount();i++){
					if (panel.getComponent(i) instanceof JLabel){
						JLabel l = (JLabel)panel.getComponent(i);
						sb.append(l.getText());
						if ((i!=0&&i%2!=0)||i==1)
							sb.append("\n");
						else
							sb.append("\t");
					}
				}
				Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
				StringSelection content = new StringSelection( sb.toString() );
				c.setContents(content, null);
			}
		};
    }
    
    @Override
    public void dispose(){
    	running = false;
    	super.dispose();
    }
    /**
     * Oppdaterer alle info felter
     * 
     */
    public void updateSystemInfo() {
    	dbName.setText(getDbName());
		dbPlatform.setText(getDbPlatform());
		dbHost.setText(getDbHost());
		dbSchema.setText(getDbSchema());
    	serverHost.setText(getServerHostname());
    	server.setText(getServer());
    	serverPingtime.setText(getPingtime());
        maxMemoryCounter.setText(getMaxMemory());
        freeMemoryCounter.setText(getFreeMemory());
        totalMemoryCounter.setText(getTotalMemory());
        threadCountCounter.setText(getThreadCount());
        pluginCounter.setText(getPluginCount());
    }

    public String getPingtime(){
    	return pingTime+" millisekunder";
    }
    public String getServerHostname(){
    	long start = System.currentTimeMillis();
    	String s =  BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class).ping();
    	pingTime = System.currentTimeMillis()-start;
    	return s;
    }
    public String getServer() {
    	try {
    		URI uri = new URI(new InitialContext().getEnvironment().get(InitialContext.PROVIDER_URL).toString()); 
			return uri.getHost() + ":" + uri.getPort();
		} catch (Throwable t) {
			return "Feil ved henting";
		}
    }
    
    /**
     * Returnerer max minne som VM vil forsøke å bruke
     * 
     * @return
     */
    public String getMaxMemory() {
        return formatBytes(Runtime.getRuntime().maxMemory());
    }

    /**
     * Returnerer fritt minne i VM
     * 
     * @return
     */
    public String getFreeMemory() {
        return formatBytes(Runtime.getRuntime().freeMemory());
    }

    /**
     * Returnerer antallet tråder
     * 
     * @return
     */
    public String getThreadCount() {
        logger.debug("getThreadCount() start");
        ThreadGroup root = findRootThreadGroup();
        int count = countThreads(root, 0);
        
        logger.debug("getThreadCount() return: " + count);
        return String.valueOf(count);
    }

    /**
     * Antallet plugins som er lastet
     * 
     * @return
     */
    public String getPluginCount() {
        int count = loader.getRunningPluginsKeys().size();
        return String.valueOf(count);
    }

    /**
     * Finner root gruppen for tråder
     * 
     * @return
     */
    public ThreadGroup findRootThreadGroup() {
        ThreadGroup root = Thread.currentThread().getThreadGroup().getParent();
        while (root.getParent() != null) {
            root = root.getParent();
        }

        return root;
    }

    /**
     * Teller rekursivt alle kjørende tråder
     * 
     * @param group
     * @param level
     */
    public int countThreads(ThreadGroup group, int level) {
        logger.debug("countThreads(" + group.getName() + "," + level + ") start");
        
        // Get threads in `group'
        int numThreads = group.activeCount();

        // Get thread subgroups of `group'
        int numGroups = group.activeGroupCount();
        ThreadGroup[] groups = new ThreadGroup[numGroups * 2];
        numGroups = group.enumerate(groups, false);

        // Recursively visit each subgroup
        for (int i = 0; i < numGroups; i++) {
            numThreads += countThreads(groups[i], level + 1);
        }

        logger.debug("countThreads(" + group.getName() + "," + level + ") return: " + numThreads);
        return numThreads;
    }

    /**
     * Returnerer totalt minne i bruk i VM
     * 
     * @return
     */
    public String getTotalMemory() {
        long mem = 
            Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        return formatBytes(mem);
    }

    /**
     * Formaterer bytes informasjon (bytes, kB, MB og GB)
     * 
     * @param bytes
     * @return
     */
    public String formatBytes(long bytes) {
        long kb = bytes / 1000;
        long mb = bytes / (long)(Math.pow(1000, 2));
        long gb = bytes / (long)(Math.pow(1000, 3));
        if (bytes < 1000) {
            return bytes + " bytes";
        } else if (kb < 1000) {
            return kb + " kB (" + bytes + " bytes)";
        } else if (mb < 1000) {
            return mb + " MB (" + kb + " kB)";
        } else {
            return gb + " GB (" + mb + " MB)";
        }
    }
}
