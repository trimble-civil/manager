package no.mesta.mipss.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class MipssProgressDialog extends JDialog{
	
	private final String message;

	public MipssProgressDialog(String message, JFrame owner){
		super(owner);
		this.message = message;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JXBusyLabel busyLabel = new JXBusyLabel();
		busyLabel.setText(message);
		busyLabel.setBusy(true);
		
		add(busyLabel, new GridBagConstraints(0,0,1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(20,20,20,20), 0,0));
		pack();
		setLocationRelativeTo(getOwner());
	}

}
