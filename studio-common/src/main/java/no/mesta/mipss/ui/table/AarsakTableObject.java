package no.mesta.mipss.ui.table;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;

/**
 * Viser årsak i en tabell hvor den kan velges
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class AarsakTableObject implements CheckBoxTableObject, Comparable<AarsakTableObject>, PropertyChangeSource {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_NONE = BUNDLE.getGuiString("aarsakPanel.none");
	private Hendelseaarsak aarsak;
	private transient PropertyChangeSupport props;
	private boolean valgt;

	public AarsakTableObject(Hendelseaarsak aarsak) {
		this.aarsak = aarsak;
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(AarsakTableObject o) {
		if (o == null) {
			return 1;
		}

		if (aarsak == null) {
			return -1;
		}

		return aarsak.compareTo(o.aarsak);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof AarsakTableObject)) {
			return false;
		}

		AarsakTableObject other = (AarsakTableObject) o;

		return new EqualsBuilder().append(aarsak, other.aarsak).isEquals();
	}

	/**
	 * @return the aarsak
	 */
	public Hendelseaarsak getAarsak() {
		return aarsak;
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		return aarsak == null ? GUI_NONE : aarsak.getTextForGUI();
	}

	/**
	 * @return the valgt
	 */
	public Boolean getValgt() {
		return valgt;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(aarsak).toHashCode();
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	/**
	 * @param aarsak
	 *            the aarsak to set
	 */
	public void setAarsak(Hendelseaarsak aarsak) {
		Hendelseaarsak old = this.aarsak;
		this.aarsak = aarsak;
		getProps().firePropertyChange("aarsak", old, aarsak);
	}

	/**
	 * @param valgt
	 *            the valgt to set
	 */
	public void setValgt(Boolean valgt) {
		Boolean old = this.valgt;
		this.valgt = valgt;
		getProps().firePropertyChange("valgt", old, valgt);
	}
}
