package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

public class KjoretoyNyEierDialog extends JDialog implements PropertyChangeListener  {
	private Kjoretoyeier nyEier;
	
	private JPanel pnlEier = new JPanel();
	private JLabel lblNavn = new JLabel();
	private JTextField txtNavn = new JTextField();
	private JLabel lblAdresse = new JLabel();
	private JTextField txtAdresse = new JTextField();
	private JLabel lblPostnummer = new JLabel();
	private JFormattedTextField txtPostnummer = new JFormattedTextField();
	private JLabel lblPoststed = new JLabel();
	private JTextField txtPoststed = new JTextField();
	private JLabel lblTelefon1 = new JLabel();
	private JFormattedTextField txtTelefon1 = new JFormattedTextField();
	private JLabel lblTelefon2 = new JLabel();
	private JFormattedTextField txtTelefon2 = new JFormattedTextField();
	private JLabel lblEpost = new JLabel();
	private JTextField txtEpost = new JTextField();
	private JCheckBox chkUEFlagg = new JCheckBox();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	
	public KjoretoyNyEierDialog(JDialog parentDialog) {
		super(parentDialog, KjoretoyUtils.getPropertyString("kjoretoyseier.dialog.nyEier"), true);
		nyEier = new Kjoretoyeier();
		nyEier.setUeFlagg(Long.valueOf(0));
		nyEier.setSendeEpostFlagg(0L);
		init();
	}
	
	public KjoretoyNyEierDialog(JFrame parentFrame) {
		super(parentFrame, KjoretoyUtils.getPropertyString("kjoretoyseier.dialog.nyEier"), true);
		nyEier = new Kjoretoyeier();
		nyEier.setUeFlagg(Long.valueOf(0));
		nyEier.setSendeEpostFlagg(0L);
		init();
	}
	
	public KjoretoyNyEierDialog(JDialog parentDialog, Kjoretoyeier kjoretoyeier) {
		super(parentDialog, KjoretoyUtils.getPropertyString("kjoretoyseier.dialog.redigerEier"), true);
		this.nyEier = kjoretoyeier;
		init();
	}
	
	public KjoretoyNyEierDialog(JFrame parentFrame, Kjoretoyeier kjoretoyeier) {
		super(parentFrame, KjoretoyUtils.getPropertyString("kjoretoyseier.dialog.redigerEier"), true);
		this.nyEier = kjoretoyeier;
		init();
	}
	
	private void init() {
		initGui();
		initButtons();
	}
	
	public Kjoretoyeier showDialog() {
		nyEier.addPropertyChangeListener(this);
		
		initTextFields();
		this.setLocationRelativeTo(null);
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
		
		if(nyEier != null) {
			nyEier.removePropertyChangeListener(this);
		}
		
		return nyEier;
	}
	
	private void initButtons() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nyEier = null;
				dispose();
			}
		});
		
		btnOk.setEnabled(false);
	}
	
	@SuppressWarnings("unchecked")
	private void initTextFields() {
		BindingGroup bindingGroup = new BindingGroup();
		LongStringConverter longStringConverter = new LongStringConverter();

		Binding b = BindingHelper.createbinding(nyEier, "postnummer", txtPostnummer, "text", BindingHelper.READ_WRITE);
		b.setConverter(longStringConverter);
		bindingGroup.addBinding(b);
		b = BindingHelper.createbinding(nyEier, "tlfnummer1", txtTelefon1, "text", BindingHelper.READ_WRITE);
		b.setConverter(longStringConverter);
		bindingGroup.addBinding(b);
		b = BindingHelper.createbinding(nyEier, "tlfnummer2", txtTelefon2, "text", BindingHelper.READ_WRITE);
		b.setConverter(longStringConverter);
		bindingGroup.addBinding(b);
		
		BindingHelper.createbinding(nyEier, "navn", txtNavn, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(nyEier, "adresse", txtAdresse, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(nyEier, "poststed", txtPoststed, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(nyEier, "epostAdresse", txtEpost, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(nyEier, "ueFlaggBoolean", chkUEFlagg, "selected", BindingHelper.READ_WRITE).bind();
		
		txtPostnummer.setDocument(new DocumentFilter(4, "0123456789", false));
		txtTelefon1.setDocument(new DocumentFilter(8, "0123456789", false));
		txtTelefon2.setDocument(new DocumentFilter(8, "0123456789", false));
		
		bindingGroup.bind();
	}
	
	private void initGui() {
		Dimension dim = new Dimension(150, 19);
		
		btnOk.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.button.ok"));
		btnAvbryt.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.button.avbryt"));
		
		lblNavn.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.navn"));
		lblAdresse.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.adresse"));
		lblPostnummer.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.postnummer"));
		lblPoststed.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.poststed"));
		lblTelefon1.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.telefon1"));
		lblTelefon2.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.telefon2"));
		lblEpost.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.epost"));
		chkUEFlagg.setText(KjoretoyUtils.getBundle().getString("kjoretoyseier.lbl.ueFlagg"));
		
		txtNavn.setMinimumSize(dim);
		txtNavn.setPreferredSize(dim);
		txtAdresse.setMinimumSize(dim);
		txtAdresse.setPreferredSize(dim);
		txtPostnummer.setMinimumSize(dim);
		txtPostnummer.setPreferredSize(dim);
		txtPoststed.setMinimumSize(dim);
		txtPoststed.setPreferredSize(dim);
		txtTelefon1.setMinimumSize(dim);
		txtTelefon1.setPreferredSize(dim);
		txtTelefon2.setMinimumSize(dim);
		txtTelefon2.setPreferredSize(dim);
		txtEpost.setMinimumSize(dim);
		txtEpost.setPreferredSize(dim);
		
		pnlEier.setLayout(new GridBagLayout());
		pnlEier.add(lblNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtNavn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(lblAdresse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtAdresse, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(lblPostnummer, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtPostnummer, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(lblPoststed, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtPoststed, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(lblTelefon1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtTelefon1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(lblTelefon2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtTelefon2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(lblEpost, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlEier.add(txtEpost, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlEier.add(chkUEFlagg, new GridBagConstraints(0, 7, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		this.setLayout(new GridBagLayout());
		this.add(pnlEier, new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(btnOk, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 11, 11, 5), 0, 0));
		this.add(btnAvbryt, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 11, 11), 0, 0));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		btnOk.setEnabled(nyEier.getNavn() != null && nyEier.getNavn().trim().length() > 0);
	}
}
