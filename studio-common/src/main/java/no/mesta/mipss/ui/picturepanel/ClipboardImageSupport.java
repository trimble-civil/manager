package no.mesta.mipss.ui.picturepanel;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import org.jdesktop.swingx.JXImageView;

public class ClipboardImageSupport extends TransferHandler implements Transferable {
	private static final DataFlavor flavors[] = { DataFlavor.imageFlavor };
	private Image image;
	private JXImageView source;
	public boolean canImport(JComponent comp, DataFlavor flavor[]) {
		if (!(comp instanceof JXImageView)) {
			return false;
		}
		for (int i = 0, n = flavor.length; i < n; i++) {
			for (int j = 0, m = flavors.length; j < m; j++) {
				if (flavor[i].equals(flavors[j])) {
					return true;
				}
			}
		}
		return false;
	}

	public Transferable createTransferable(JComponent comp) {
		// Clear
		source = null;
		image = null;

		if (comp instanceof JXImageView) {
			JXImageView view = (JXImageView) comp;
			image = view.getImage();
			if (image != null) {
				source = view;
				return this;
			}
		}
		return null;
	}

	public int getSourceActions(JComponent c) {
		return TransferHandler.COPY;
	}

	// Transferable
	public Object getTransferData(DataFlavor flavor) {
		if (isDataFlavorSupported(flavor)) {
			return image;
		}
		return null;
	}

	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	public boolean importData(JComponent comp, Transferable t) {
		if (comp instanceof JXImageView) {
			JXImageView view = (JXImageView) comp;
			if (t.isDataFlavorSupported(flavors[0])) {
				try {
					image = (Image) t.getTransferData(flavors[0]);
					view.setImage(image);
					return true;
				} catch (UnsupportedFlavorException ignored) {
				} catch (IOException ignored) {
				}
			}
		}
		return false;
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return flavor.equals(DataFlavor.imageFlavor);
	}
}