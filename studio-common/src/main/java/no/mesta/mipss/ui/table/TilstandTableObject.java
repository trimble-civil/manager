package no.mesta.mipss.ui.table;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;

/**
 * Valgbar entitet i tabell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class TilstandTableObject implements CheckBoxTableObject, PropertyChangeSource, Comparable<TilstandTableObject> {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_NONE = BUNDLE.getGuiString("tilstandPanel.none");
	private transient PropertyChangeSupport props;
	private Avviktilstand tilstand;
	private boolean valgt;

	public TilstandTableObject(Avviktilstand tilstand) {
		this.tilstand = tilstand;
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(TilstandTableObject o) {
		if (o == null) {
			return 1;
		}

		if (tilstand == null) {
			return -1;
		}

		return tilstand.compareTo(o.tilstand);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof TilstandTableObject)) {
			return false;
		}
		
		TilstandTableObject other = (TilstandTableObject) o;
		
		return new EqualsBuilder().append(tilstand, other.tilstand).isEquals();
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		return tilstand == null ? GUI_NONE : tilstand.getTextForGUI();
	}

	/**
	 * @return the tilstand
	 */
	public Avviktilstand getTilstand() {
		return tilstand;
	}

	/**
	 * @return the valgt
	 */
	public Boolean getValgt() {
		return valgt;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(tilstand).toHashCode();
	}
	
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);

	}
	
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	/**
	 * @param tilstand
	 *            the tilstand to set
	 */
	public void setTilstand(Avviktilstand tilstand) {
		Avviktilstand old = this.tilstand;
		this.tilstand = tilstand;
		getProps().firePropertyChange("tilstand", old, tilstand);
	}

	/**
	 * @param valgt
	 *            the valgt to set
	 */
	public void setValgt(Boolean valgt) {
		Boolean old = this.valgt;
		this.valgt = valgt;
		getProps().firePropertyChange("valgt", old, valgt);
	}
}
