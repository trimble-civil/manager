package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXBusyLabel;

/**
 * Placeholder for et panel mens det venter på data.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JLoadingPanel extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(JLoadingPanel.class);
	private final Class<?>[] constructor;
	private Map<Class<?>, Object> arguments = new HashMap<Class<?>, Object>();
	private final Set<Class<?>> allKeys = new HashSet<Class<?>>();
	private final Class<? extends JComponent> panel;
	private boolean loaded;

	public JLoadingPanel(Class<? extends JComponent> panel, Class<?>[] constructor ){
		this.constructor = constructor;
		this.panel = panel;
		setLayout(new FlowLayout());
		JXBusyLabel busy = new JXBusyLabel();
		add(busy);
		busy.setBusy(true);
		
		for(Class<?> clazz:constructor) {
			allKeys.add(clazz);
		}
		
		setBorder(BorderResources.createComponentBorder());
	}
	
	public void setArgument(Class<?> clazz, Object value) {
		logger.debug("setArgument(" + clazz + ", " + value + ") start");
		arguments.put(clazz, value);
		Set<Class<?>> isSetKeys = arguments.keySet();
		if(allKeys.equals(isSetKeys)) {
			load();
		}
	}
	
	private void load() {
		logger.debug("load() start");
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				removeAll();
				Border border = getBorder();
				setBorder(null);
		
				try {
					Constructor<?> c = panel.getConstructor(constructor);
					Object[] args = new Object[constructor.length];
					int index = 0;
					for(Class<?> clazz:constructor) {
						args[index] = arguments.get(clazz);
						index++;
					}
					JComponent component = (JComponent) c.newInstance(args);
					if(component.getMinimumSize() != null) {
						setMinimumSize(component.getMinimumSize());
					}
					
					if(component.getPreferredSize() != null) { 
						setPreferredSize(component.getPreferredSize());
					}
					
					if(component.getMaximumSize() != null) {
						setMaximumSize(component.getMaximumSize());
					}
					
					if(component.getBorder() == null) {
						component.setBorder(border);
					}
					
					setLayout(new BorderLayout());
					add(component, BorderLayout.CENTER);
					component.revalidate();
					loaded=true;
				} catch (Throwable e) {
					e.printStackTrace();
					logger.error("Throwable in run", e);
				}
			}
		});
	}
	public boolean isLoaded(){
		return loaded;
	}
}
