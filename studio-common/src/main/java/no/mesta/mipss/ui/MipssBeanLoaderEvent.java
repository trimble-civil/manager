package no.mesta.mipss.ui;

import java.util.EventObject;


/**
 * Event som lages naar en server bean er ferdig eller skal til aa gjøre noe :-)
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssBeanLoaderEvent extends EventObject {
    private boolean errors = false;
    private boolean consumed;
    
    /**
     * Konstruktoer
     * 
     * @param source
     */
    public MipssBeanLoaderEvent(Object source, boolean errors) {
        super((Object) source);
        this.errors = errors;
    }

    public void setConsumed(boolean consumed){
    	this.consumed = consumed;
    }
    public boolean isConsumed(){
    	return consumed;
    }
    /**
     * Angir om lastingen var vellykket
     * 
     * @return
     */
    public boolean isErrors() {
        return errors;
    }
}
