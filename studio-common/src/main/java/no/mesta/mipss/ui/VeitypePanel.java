package no.mesta.mipss.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public class VeitypePanel extends JPanel{

    private EventListenerList listenerList = new EventListenerList();
    protected transient ChangeEvent changeEvent = null;
	private JCheckBox evCheck;
	private JCheckBox rvCheck;
	private JCheckBox fvCheck;
	private JCheckBox kvCheck;

	public VeitypePanel(){
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		setBorder(BorderFactory.createTitledBorder("Vis som veityper"));
		evCheck = new JCheckBox("Vis EV");
		rvCheck = new JCheckBox("Vis RV");
		fvCheck = new JCheckBox("Vis FV");
		kvCheck = new JCheckBox("Vis KV");
		
		ActionListener l = new ActionListener(){
			public void actionPerformed(ActionEvent e){
				fireStateChanged();
			}
		};
		evCheck.addActionListener(l);
		rvCheck.addActionListener(l);
		fvCheck.addActionListener(l);
		kvCheck.addActionListener(l);
		
		JLabel evLabel = createColorLabel(Color.yellow);
		JLabel rvLabel = createColorLabel(Color.magenta);
		JLabel fvLabel = createColorLabel(Color.cyan);
		JLabel kvLabel = createColorLabel(Color.white);
		
		JButton okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				okButtonAction(e);
			}
		});
		
		add(evLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		add(rvLabel, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		add(fvLabel, new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		add(kvLabel, new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		
		add(evCheck, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		add(rvCheck, new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		add(fvCheck, new GridBagConstraints(1,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		add(kvCheck, new GridBagConstraints(1,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
		
		add(okButton, new GridBagConstraints(1,4, 1,1, 1.0,1.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
	}
	
	private JLabel createColorLabel(Color color){
		JLabel colorLabel = new JLabel("          ");
		colorLabel.setOpaque(true);
		colorLabel.setBackground(color);
		colorLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		return colorLabel;
	}
	public boolean isEv(){
		return evCheck.isSelected();
	}
	public boolean isRv(){
		return rvCheck.isSelected();
	}
	public boolean isFv(){
		return fvCheck.isSelected();
	}
	public boolean isKv(){
		return kvCheck.isSelected();
	}
	public void addChangeListener(ChangeListener l) {
        listenerList.add(ChangeListener.class, l);
    }
    protected void fireStateChanged(){
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -=2 ) {
            if (listeners[i] == ChangeListener.class) {
                if (changeEvent == null) {
                    changeEvent = new ChangeEvent(this);
                }
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }
    private void okButtonAction(ActionEvent e){
        Container parent = this;
        while (!(parent instanceof JDialog)){
            parent = parent.getParent();
        }
        ((JDialog)parent).setVisible(false);
    }
}
