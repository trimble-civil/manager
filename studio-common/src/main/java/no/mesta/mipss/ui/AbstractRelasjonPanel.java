package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * Panel for setting av relasjonsdata
 * 
 * @author lareid
 *
 * @param <T> Ralsjonsentitet
 */
public abstract class AbstractRelasjonPanel<T> extends JPanel {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private JDialog parentDialog;
	private boolean complete; //Default false
	
	/**
	 * Brukes internt
	 * @param parentDialog
	 */
	public void setParentDialog(JDialog parentDialog) {
		this.parentDialog = parentDialog;
	}
	
	/**
	 * Gir parent til panelet
	 * @return
	 */
	protected JDialog getParentDialog() {
		return parentDialog;
	}
	
	/**
	 * Gir dialogstørrelsen
	 * @return
	 */
	public abstract Dimension getDialogSize();
	
	/**
	 * Gir titelen til dialogvinduet
	 * @return
	 */
	public abstract String getTitle();
	
	/**
	 * returnerer ny relasjon opprettet i panelet.
	 * @return
	 */
	public abstract T getNyRelasjon();
	
	/**
	 * Sjekkes ved klikk på OK. Ny relasjon bli ikke lagt til om denne returnerer {@code false}.
	 * Metoden må håndtere tilbakemelding til bruker.
	 * 
	 * @return
	 */
	public abstract boolean isLegal();
	
	/**
	 * Er {@code true} om relasjonen er komplett og kan legges til
	 * @return
	 */
	public boolean isComplete() {
		return complete;
	}
	
	/**
	 * Må kalles for å fortelle tilstanden til relasjonen til omverdenen
	 * @param complete
	 */
	public void setComplete(Boolean complete) {
		Boolean old = this.complete;
		this.complete = complete;
		props.firePropertyChange("complete", old, complete);
	}
	
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
