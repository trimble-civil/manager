package no.mesta.mipss.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.TilstandTableObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * Panel med tabell for valgbare entiteter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JTilstandPanel extends JPanel implements ItemListener, PropertyChangeListener {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_ALL = BUNDLE.getGuiString("tilstandPanel.all");
	private static final String GUI_TITLE = BUNDLE.getGuiString("tilstandPanel.title");
	private static final Logger logger = LoggerFactory.getLogger(JTilstandPanel.class);
	private JCheckBox alleCheck;
	private Set<TilstandTableObject> allStates;
	private JCheckTablePanel checkTable;
	private String[] columns = new String[] { "valgt", "textForGUI" };
	private TilstandTableObject ingen;
	private JXTable table;
	private MipssRenderableEntityTableModel<TilstandTableObject> tableModel;

	private Set<TilstandTableObject> valgte = new HashSet<TilstandTableObject>();

	public JTilstandPanel() {
		initGui();
	}

	public Long[] getAlleValgte() {
		logger.debug("getAlleValgte() start");

		return setToArray(valgte);
	}
	private JXTable getTable() {
		allStates = new TreeSet<TilstandTableObject>();
		List<Avviktilstand> alle = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class)
				.hentAllePunktregtilstandtype();// module.getMipssFieldBean().hentAlleProsesser();

		ingen = new TilstandTableObject(null);
		for (Avviktilstand p : alle) {
			TilstandTableObject tto = new TilstandTableObject(p);
			tto.addPropertyChangeListener("valgt", this);
			allStates.add(tto);
		}
		allStates.add(ingen);

		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				TilstandTableObject.class, columns);
		tableModel = new MipssRenderableEntityTableModel<TilstandTableObject>(TilstandTableObject.class,
				new int[] { 0 }, columns);
		tableModel.getEntities().clear();
		tableModel.getEntities().addAll(allStates);
		JXTable table = new JXTable(tableModel, columnModel);
		table.getColumn(0).setMaxWidth(30);

		return table;
	}
	private void initGui() {
		table = getTable();

		alleCheck = new JCheckBox(GUI_ALL);
		checkTable = new JCheckTablePanel<TilstandTableObject>(table, tableModel, alleCheck);
		/*
		Box components = BoxUtil.createVerticalBox(0, BoxUtil.createHorizontalBox(0, checkTable), BoxUtil
				.createHorizontalBox(0, alleCheck, Box.createHorizontalGlue()));
		
		ComponentSizeResources.setComponentSize(this, new Dimension(200, 200));

		components.setBorder(BorderFactory.createTitledBorder(GUI_TITLE));

		setLayout(new BorderLayout());
		add(components, BorderLayout.CENTER);
		*/
		
		setBorder(BorderFactory.createTitledBorder(GUI_TITLE));
		setLayout(new GridBagLayout());
		add(checkTable, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(alleCheck, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
	}
	public boolean isAlleValgt() {
		return alleCheck.isSelected();
	}

	public boolean isIngenValgt() {
		return ingen.getValgt();
	}

	/** {@inheritDoc} */
	@Override
	public void itemStateChanged(ItemEvent e) {
		boolean isSet = e.getStateChange() == ItemEvent.SELECTED;

		if (e.getSource() == alleCheck) {
			firePropertyChange("alleValgt", !isSet, isSet);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		logger.debug("propertyChange(" + evt + ") start for " + property);
		TilstandTableObject source = (TilstandTableObject) evt.getSource();
		if (StringUtils.equals(property, "valgt") && source == ingen) {
			Boolean oldValue = (Boolean) evt.getOldValue();
			Boolean newValue = (Boolean) evt.getNewValue();
			firePropertyChange("ingenValgt", oldValue, newValue);
		} else if (StringUtils.equals(property, "valgt")) {
			Set<TilstandTableObject> old = new HashSet<TilstandTableObject>(valgte);
			if (source.getValgt() == true) {
				valgte.add(source);
			} else {
				valgte.remove(source);
			}

			Long[] oldValues = setToArray(old);
			Long[] nowValues = setToArray(valgte);

			logger.debug("fire property change, old = " + Arrays.toString(oldValues) + ", now = "
					+ Arrays.toString(nowValues));
			firePropertyChange("alleValgte", oldValues, nowValues);
		}
	}

	public void setSelected(boolean selected){
		checkTable.setSelected(selected);
	}

	private Long[] setToArray(Set<TilstandTableObject> set) {
		Long[] ids = new Long[set.size()];
		Iterator<TilstandTableObject> it = set.iterator();
		int i = 0;
		boolean noneHit = false;
		while (it.hasNext()) {
			TilstandTableObject tilstand = (TilstandTableObject) it.next();
			if (tilstand.getTilstand() != null) {
				ids[i] = tilstand.getTilstand().getId();
				i++;
			} else {
				noneHit = true;
			}
		}

		if (noneHit && ids.length > 0) {
			ids = Arrays.copyOf(ids, ids.length - 1);
		}

		return ids;
	}
}
