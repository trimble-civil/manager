package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.MipssListCellRenderer;

/**
 * Comboboxeditor for tabeller. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class MipssTableComboBoxEditor extends AbstractCellEditor {

	private final List<? extends IRenderableMipssEntity> entityList;

	public MipssTableComboBoxEditor(List<? extends IRenderableMipssEntity> list){
		this.entityList = list;	
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		JComboBox comboBox = (JComboBox) super.getTableCellEditorComponent(table, value, isSelected, row, column);
		comboBox.removeAllItems();
			
		for(IRenderableMipssEntity entity : entityList) {
			comboBox.addItem(entity);
		}

		comboBox.setSelectedItem(value);
		comboBox.setRenderer(new MipssListCellRenderer<>());

		return comboBox;
	}

}