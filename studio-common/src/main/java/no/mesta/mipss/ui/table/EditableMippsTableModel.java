package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.ELProperty;


/**
 * Enkel read-only modell for visning av felt i MIPSS entiteter.
 * Constructor-kallet bestemmer kolonnenavn, MIPSS-felt og om kolonner skal kunne redigeres.
 * 
 * {@code field} verdien i en kolonne kan være:
 * <ul>
 *  <li>en String som stater med et dollartegn ($). Da blir den oppfattet som et sammensatt 
 *  property-navn i et objekt eller en JPA entitet og blir ekstrahert ved hjelp ev en ELProperty.</li>
 *  
 *  <li>en String uten "$" som første tegn. Da blir den oppfattet som et direkte property navn i en 
 *  JPA entitet eller et annet objekt. Verdien blir da hentet via reflection.</li>
 *  
 *  <li>Et Swing objekt. Da blir verdien oppfattet som grunnobjekt og blir klonet for hver kolonne.
 *  Det støttes for tiden kun én kolonne av slike objekter</li>
 * </ul>
 * 
 * @author jorge
 */
public class EditableMippsTableModel<T> extends AbstractTableModel {
	private static final long serialVersionUID = -976748530603062550L;
	Logger log = LoggerFactory.getLogger(this.getClass());
    private List<T> entities;
    private Object[] fields;
    private Map<String, Object> callers = new HashMap<String, Object>();
    private List<Component> rows = new ArrayList<Component>();
    private String[] titles;
    private boolean[] columnEditable;
    
    /**
     * Oppretter en ny instans med sin grunntype, sin entitetsliste og sine kolonnetitler.
     * @param clazz 
     * @param entities
     * @param initColumns
     */
    @SuppressWarnings("unchecked")
	public EditableMippsTableModel(Class<T> clazz, List<T> entities, List<MipssTableColumn> initColumns) {
        this.entities = entities;
        
        titles = new String [initColumns.size()];
        fields = new Object[initColumns.size()];
        columnEditable = new boolean[initColumns.size()];
        
        for (int i = 0; i < initColumns.size(); i++) {
            MipssTableColumn c = initColumns.get(i);
            titles[i] = c.title;
            fields[i] = c.field;
            columnEditable[i] = c.isEditable;
        }
        
        try{
            for(Object o : fields) {
                if (o instanceof String) {
                    String s = (String) o;
                    if (s.startsWith("$")) {
                        ELProperty prop = ELProperty.create(s);
                        callers.put(s, (Object)prop);
                    }
                    else {
                        Method m = clazz.getMethod(getBeanField(s), (Class<?>[])null);
                        callers.put(s, m);
                    }
                }
                // ignorer alle andre typer: de håndteres for seg
            }
        } catch (NoSuchMethodException nme) {
            log.error("MipssRenderableEntityTableModel(clazz,entities,columns...), e:" + nme);
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return titles[columnIndex];
    }

    public int getRowCount() {
        if (entities == null) {
            return 0;
        }
        return entities.size();
    }

    public int getColumnCount() {
        return titles.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return getValueFromField(entities.get(rowIndex), fields[columnIndex], rowIndex);
    }
    
    public Class<?> getColumnClass(int columnIndex) {
        return getReturnTypeOfField(fields[columnIndex]);
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnEditable[columnIndex];
    }
    
    @SuppressWarnings("unchecked")
	private Class<?> getReturnTypeOfField(Object field) {
        if (field instanceof String) {
            Object caller = callers.get((String) field);
            if (caller instanceof Method) {
                try {
                    return ((Method)caller).getReturnType();
                }
                catch (NullPointerException e) {
                    log.error("Kan ikke finne feltet " + field);
                    throw e;
                }
            }
            else if (caller instanceof ELProperty) {
            	if(entities != null && entities.size() > 0) {
            		try {
            			return ((ELProperty)caller).getWriteType(entities.get(0));
            		}
            		catch (Exception e) {
						log.debug("Kan ikke hente kolonnetype for feltet " + field + ": " + e);
						return String.class;
					}
            	}
            	else {
            		return String.class;
            	}
            }
            else {
            	log.warn("Metodekaller er hverken Method eller ELPropety for felt: " + field);
                throw new IllegalStateException("Metodekaller er hverken Method eller ELPropety for felt: " + field);
            }
        }      
        else if (field instanceof Component){
            return field.getClass();
        }
        else {
            throw new IllegalArgumentException("Feltdefinisjonen er hverken String eller Component: " + field.getClass());
        }
    }
    
    /**
     * Henter ut navnet til getter-metoden
     */
    private String getBeanField(String field) {
        return "get" + field.substring(0,1).toUpperCase() + field.substring(1);
    }

    /**
     * Henter verdien fra feltet hvis feild er en String. Ellers leveres field-objektet. 
     * @param entity
     * @param field
     * @return
     */
    @SuppressWarnings("unchecked")
	private Object getValueFromField(T entity, Object field, int rowIndex) {
        if (field instanceof String) {
            String f = (String) field;
            Object returnValue = null;
            Object caller = callers.get(f);
            if (caller instanceof Method) {
                try {
                    returnValue = ((Method)caller).invoke(entity, (Object[]) null);
                } catch (Exception iae) {
                    log.warn("getValueFromField. Entity=" + entity + " field=" + field + ". Exception: " + iae);
                    returnValue = "";
                } 
            }
            else if(caller instanceof ELProperty){
            	try {
            		returnValue = ((ELProperty)caller).getValue(entity);
            	}
            	catch (Exception e) {
					log.debug("kan ikke hente verdien fra " + field + ": " + e);
					returnValue = "";
				}
            }
            log.trace("getValueFromField(" + f + "), value:" + returnValue);
            return returnValue;
        }
        else if (field instanceof Component){
            log.trace("Henter klonen for " + field.getClass());
            return getCloneFromField(field, rowIndex);
        }
        else {
            throw new IllegalArgumentException("Feltdefinisjonen er hverken String eller Component: " + field.getClass());
        }
    }

    /**
     * kloner Swing objektet og legger det til rows listen for senere visning.
     * @param field objektet som skal klones
     * @param rowIndex radindeksen for å hente/legge til objektet.
     * @return
     */
    private Component getCloneFromField(Object field, int rowIndex) {
        Component c = null;
        
        if (rows.size() <= rowIndex) {
            try {
                Class<?> toClone = field.getClass();
                c = (Component)toClone.newInstance();
            }
            catch(InstantiationException e) {
                log.error("Feltobjekt av type " + field.getClass() + " kan ikke klones");
                return null;
            }
            catch(IllegalAccessException iae) {
                log.error("Feltobjekt av type " + field.getClass() + " kan ikke klones");
                return null;
            }
            rows.add(rowIndex, c);
            log.trace("returnerer ny klone i posisjon[" + rowIndex + "]: "+ c);
        }
        else {
            c = rows.get(rowIndex);
            log.trace("returnerer gammel klone i posisjon[" + rowIndex + "]: "+ c);
        }
        
        return c;
    }

    /**
     * Legger til eller erstatter datalisten som tabellen viser.
     * @param ents
     */
    public void setEntities(List<T> ents) {
        this.entities = ents;
        fireTableDataChanged();
    }

    /**
     * returnerer entitetene som vises på tabellen.
     */
    public List<T> getEntities() {
        return entities;
    }
    
    /**
     * Dedikert initialiseringsstruktur for EditableMippsTableModel.
     * Innholdet i feltet {@code field} kan være enString eller et hvilket annet Swing objekt som
     * kan sendes til renderer.
     */
    public static class MipssTableColumn {
        private String title;
        private Object field;
        private boolean isEditable;
        
        public MipssTableColumn(String title, Object field, boolean isEditable) {
            this.title = title;
            this.field = field;
            this.isEditable = isEditable;
        }
    }
}
