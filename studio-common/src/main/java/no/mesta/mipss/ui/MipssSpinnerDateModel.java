package no.mesta.mipss.ui;

import java.util.Calendar;
import java.util.Date;
import no.mesta.mipss.persistence.Clock;
import javax.swing.SpinnerDateModel;

/**
 * SpinnerDateModell som takler null som parametere. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class MipssSpinnerDateModel extends SpinnerDateModel{
	public MipssSpinnerDateModel(Date value, Comparable start, Comparable end, int calendarField){
		Calendar  c = Calendar.getInstance();
		c.setTime(Clock.now());
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		if (value!=null)
			super.setValue(value);
		else
			super.setValue(c.getTime());
		super.setStart(start);
		super.setEnd(end);
		super.setCalendarField(calendarField);
		
	}
	@Override
	public void setValue(Object value){
		if (value!=null)
			super.setValue(value);
	}
}
