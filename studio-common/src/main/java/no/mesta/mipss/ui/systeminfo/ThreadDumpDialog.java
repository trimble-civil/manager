package no.mesta.mipss.ui.systeminfo;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.resources.images.IconResources;


public class ThreadDumpDialog extends JDialog{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");

    private JLabel label;
    private JTextArea textArea;
    private JScrollPane textScroll;
    private JButton closeButton;
    
    private JPanel centerPanel;

	private boolean running;
    
    public ThreadDumpDialog(IMipssStudioLoader loader) {
        super(loader.getApplicationFrame(), "", false);
        this.setTitle(resources.getGuiString("studio.runningThreads"));
        setIconImage(IconResources.THREADS_ICON.getImage());
        setLayout(new BorderLayout());
        
        setSize(400,300);
        setLocationRelativeTo(null);
        
        label = new JLabel(resources.getGuiString("studio.threadname"));
        textArea = new JTextArea();
        textScroll = new JScrollPane(textArea);
        
        closeButton = new JButton("Lukk");
        closeButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ThreadDumpDialog.this.dispose();     
            }
        });
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(closeButton);
        
        centerPanel = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));
        centerPanel.add(label);
        centerPanel.add(Box.createRigidArea(new Dimension(0,5)));
        centerPanel.add(textScroll);
        
        add(centerPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.PAGE_END);
        init();
        startWorker();
    }
    
    private void init(){
        textArea.setText(getThreads());
    }
    
    private void startWorker() {
		running = true;
		SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>(){

			@Override
			protected Void doInBackground() throws Exception {
				while (running) {
		            SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							init();
						}
					});
					Thread.sleep(2000);
		        }
				return null;
			}
			
		};
		sw.execute();
	}

    @Override 
    public void dispose(){
    	running = false;
    	super.dispose();
    }
    /**
     * Debug for å finne alle kjørende tråder
     */
    private String getThreads(){
        // Find the root thread group
        ThreadGroup root = Thread.currentThread().getThreadGroup().getParent();
        while (root.getParent() != null) {
            root = root.getParent();
        }
        StringBuffer threadNames = new StringBuffer();
        // Visit each thread group
        visit(root, 0, threadNames);    
        return threadNames.toString();
    }
    /**
     * Debug for å rekursivt finne alle kjørende tråder
     * @param group
     * @param level
     */
    public void visit(ThreadGroup group, int level, StringBuffer threadNames) {
        // Get threads in `group'
        int numThreads = group.activeCount();
        Thread[] threads = new Thread[numThreads*2];
        numThreads = group.enumerate(threads, false);
    
        // Enumerate each thread in `group'
        for (int i=0; i<numThreads; i++) {
            // Get thread
            Thread thread = threads[i];
            threadNames.append(thread);
            threadNames.append("\n");
            //log.debug(thread);
        }
    
        // Get thread subgroups of `group'
        int numGroups = group.activeGroupCount();
        ThreadGroup[] groups = new ThreadGroup[numGroups*2];
        numGroups = group.enumerate(groups, false);
    
        // Recursively visit each subgroup
        for (int i=0; i<numGroups; i++) {
            visit(groups[i], level+1, threadNames);
        }
    }
}
