package no.mesta.mipss.ui.taskpane;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXCollapsiblePane;

@SuppressWarnings("serial")
public class TaskToggleButton extends JPanel{

	private final JXCollapsiblePane contentPane;
	
		
	public TaskToggleButton(JXCollapsiblePane contentPane){
		this.contentPane = contentPane;
		setOpaque(false);
		setLayout(new BorderLayout());
		add(new TaskToggleButtonComponent());
	}
	
	private class TaskToggleButtonComponent extends JLabel{
		private boolean border = false;
		private boolean down;
		private boolean prevDown;
		private int x = 10;
		private int y = 13;
		private Rectangle rect = new Rectangle(x-5, y-5, x+6, y+3);
		
		private Color downColor = new Color(138, 190, 245);
		private Stroke stroke = new BasicStroke(1);
		
		public TaskToggleButtonComponent(){
			initListeners();
			Dimension size = new Dimension(30,30);
			setMinimumSize(size);
			setPreferredSize(size);
			setMaximumSize(size);
		}
		
		private void initListeners() {
			addMouseListener(new MouseAdapter(){
				@Override
				public void mouseEntered(MouseEvent e) {
					border = true;
					if (prevDown){
						down = true;
					}
					repaint();
				}
				@Override
				public void mouseExited(MouseEvent e) {
					border = false;
					prevDown = down;
					down = false;
					repaint();
				}
				@Override
				public void mousePressed(MouseEvent e){
					down = true;
					repaint();
				}
				@Override
				public void mouseReleased(MouseEvent e){
					down = false;
					prevDown = false;
					repaint();
				}
				public void mouseClicked(MouseEvent e){
					contentPane.setCollapsed(!contentPane.isCollapsed());
					repaint();
				}
			});
			
		}
		@Override
		public void paint(Graphics gr){
			Graphics2D g = (Graphics2D)gr;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			if (down){
				g.setPaint(downColor);
				g.fill(rect);
			}
			
	    	g.setPaint(Color.black);
	    	g.setStroke(stroke);
	        if (contentPane.isCollapsed()) {
	            drawLineCollapsed(g);
	            g.translate(0, 4);
	            drawLineCollapsed(g);
	            g.translate(0, -4);
	        } else {
	        	drawLineExpanded(g);
	        	g.translate(0, 4);
	        	drawLineExpanded(g);
	        	g.translate(0, -4);
	        }
	        if (border){
	        	g.setPaint(Color.gray);
	        	g.draw(rect);
	        }
		}

		private void drawLineExpanded(Graphics2D g) {
			g.drawLine(x, y+3, x+3, y);
			g.drawLine(x+3, y, x+6, y+3);
		}

		private void drawLineCollapsed(Graphics2D g) {
			g.drawLine(x, y, x+3, y+3);
			g.drawLine(x+3, y+3, x+6, y);
		}
	}
}
