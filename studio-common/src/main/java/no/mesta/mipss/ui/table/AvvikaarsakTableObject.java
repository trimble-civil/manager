package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.mipssfield.Avvikaarsak;

@SuppressWarnings("serial")
public class AvvikaarsakTableObject implements CheckBoxTableObject{
	private final Avvikaarsak type;
	private boolean valgt;

	public AvvikaarsakTableObject(Avvikaarsak type){
		this.type = type;
	}
	
	public Long getId(){
		return type.getId();
	}
	public String getStatus(){
		return type.getNavn();
	}
	@Override
	public String getTextForGUI() {
		return type.getTextForGUI();
	}

	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
		
	}

	@Override
	public Boolean getValgt() {
		return valgt;
	}

}