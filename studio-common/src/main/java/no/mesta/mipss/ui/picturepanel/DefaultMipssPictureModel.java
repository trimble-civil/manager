package no.mesta.mipss.ui.picturepanel;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

/**
 * Standardimplementasjon av en MipssPictureModel uten MipssBeanLoaderListener, forutsetter
 * at alle bilder er ferdig lastet. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class DefaultMipssPictureModel implements MipssPictureModel {

	private List<Bilde> bildeList;
	private List<MipssPictureModelListener> listenerList;
	
	public DefaultMipssPictureModel(){
		bildeList =  new ArrayList<Bilde>();
		listenerList = new ArrayList<MipssPictureModelListener>();
	}
	
	@Override
	public void addBilde(Bilde b) {
		bildeList.add(b);
	}

	@Override
	public void addMipssBeanLoaderListener(MipssBeanLoaderListener l) {}
	@Override
	public void removeMipssBeanLoaderListener(MipssBeanLoaderListener l) {	}

	@Override
	public void addMipssPictureModelListener(MipssPictureModelListener l) {
		listenerList.add(l);
	}
	@Override
	public void removeMipssPictureModelListener(MipssPictureModelListener l) {
		listenerList.remove(l);
	}
	
	@Override
	public Bilde getBilde(int index) {
		return bildeList.get(index);
	}

	@Override
	public int getBildeIndex(Bilde b) {
		return bildeList.indexOf(b);
	}

	@Override
	public List<Bilde> getBilder() {
		return bildeList;
	}

	@Override
	public boolean isLoadingData() {
		return false;
	}

	@Override
	public void removeBilde(Bilde b) {
		bildeList.remove(b);
	}

	@Override
	public void setBilder(List<Bilde> bilder) {
		this.bildeList = bilder;
		
	}

	@Override
	public int size() {
		return bildeList.size();
	}

}
