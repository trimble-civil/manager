package no.mesta.mipss.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;

import org.jdesktop.beansbinding.Binding;

/**
 * Dialog for setting av relasjonsdata
 * @author lareid 
 *
 * @param <T> Relasjon som skal settes
 */
@SuppressWarnings("serial")
public class LeggTilRelasjonDialog<T> extends JDialog {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlContent = new JPanel();
    private JPanel pnlButtons = new JPanel();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    private GridBagLayout gridBagLayout5 = new GridBagLayout();
    private JButton btnOk = new JButton();
    private JButton btnAvbryt = new JButton();
    
    private AbstractRelasjonPanel<T> abstractRelasjonPanel;
    private boolean okKlikket;
	
    /**
     * Kontruktør
     * @param parent Dialogens parent frame
     * @param abstractRelasjonPanel Panel hvor ny relasjon opprettes
     */
	public LeggTilRelasjonDialog(JFrame parent, AbstractRelasjonPanel<T> abstractRelasjonPanel) {
		super(parent, true);
		this.abstractRelasjonPanel = abstractRelasjonPanel;
		abstractRelasjonPanel.setParentDialog(this);
		
		initGui(abstractRelasjonPanel);
		initButtons();
	}
	
    /**
     * Kontruktør
     * @param parent Dialogens parent dialog
     * @param abstractRelasjonPanel Panel hvor ny relasjon opprettes
     */
	public LeggTilRelasjonDialog(JDialog parent, AbstractRelasjonPanel<T> abstractRelasjonPanel) {
		super(parent, true);
		this.abstractRelasjonPanel = abstractRelasjonPanel;
		abstractRelasjonPanel.setParentDialog(this);
		
		initGui(abstractRelasjonPanel);
		initButtons();
	}
	
	/**
	 * Viser dialogen modalt. Returerer valgt relasjon. Returnerer {@code null} om brukeren avbryter.
	 * @return T
	 */
	public T showDialog() {
		okKlikket = false;
		setTitle(abstractRelasjonPanel.getTitle());
		setSize(abstractRelasjonPanel.getDialogSize());
		setLocationRelativeTo(null);
		setVisible(true);
		pack();
		
		return okKlikket ? abstractRelasjonPanel.getNyRelasjon() : null;
	}
	
	@SuppressWarnings("unchecked")
	private void initButtons() {
		btnOk.setAction(new AbstractAction(BUNDLE.getGuiString("relasjonPanel.button.ok")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(abstractRelasjonPanel.isLegal()) {
					okKlikket = true;
					dispose();
				}
			}});
		btnAvbryt.setAction(new AbstractAction(BUNDLE.getGuiString("relasjonPanel.button.avbryt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				okKlikket = false;
				dispose();
			}});
		
		Binding binding = BindingHelper.createbinding(abstractRelasjonPanel, "complete", btnOk, "enabled");
		binding.bind();
	}
	
	private void initGui(AbstractRelasjonPanel<T> abstractRelasjonPanel) {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlContent.setLayout(gridBagLayout4);
        pnlButtons.setLayout(gridBagLayout5);
        pnlContent.add(abstractRelasjonPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        pnlMain.add(pnlContent,  new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,  new Insets(0, 0, 0, 0), 0, 0));
        pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST,   GridBagConstraints.NONE,  new Insets(0, 0, 0, 0), 0, 0));
        pnlButtons.add(btnAvbryt,  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,  GridBagConstraints.NONE,  new Insets(0, 5, 0, 0), 0, 0));
        pnlMain.add(pnlButtons, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  new Insets(0, 11, 11, 11), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
}
