package no.mesta.mipss.ui;

import java.awt.Toolkit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
/**
 * 
 * Klasse for å sette restriksjoner på innput til JComponents som bruker Documents.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class DocumentFilter extends PlainDocument{
	private String validCharacters = "0123456789";
	private int maxLength = 4;
	private boolean forceUppercase = false;
	/**
	 * Lager et filter som gir kun mulighet til å taste inn maxLength antall tall fra 0-9
	 * @param maxLength
	 */
	public DocumentFilter(int maxLength){
		super();
		this.maxLength = maxLength;
		
	}
	/**
	 * Lager et filter som kun gir mulighet til å taste inn maxLength antall av validCharachters
	 * @param maxLength
	 * @param validCharacters
	 */
	public DocumentFilter(int maxLength, String validCharacters, boolean forceUppercase){
		super();
		this.maxLength = maxLength;
		this.validCharacters = validCharacters;
		this.forceUppercase = forceUppercase;
		
	}
	
	/**
	 * Lager et filter som kun gir mulighet til å taste inn fire siffer
	 */
	public DocumentFilter(){
		super();
		
	}
	
	/**
	 * Setter alle bokstaver til uppercase
	 * @param forceUppercase the forceUppercase to set
	 */
	public void setForceUppercase(boolean forceUppercase) {
		this.forceUppercase = forceUppercase;
	}
	
	@Override
	public void insertString(int offset, String input, AttributeSet as)
		throws BadLocationException {
		String newValue = "";
		if (forceUppercase){
			input = input.toUpperCase();
		}
		for (int i = 0; i < input.length(); i++){
			if (validCharacters==null || validCharacters.indexOf(input.charAt(i))!=-1){
				if (getLength()+input.length()<= maxLength){
					newValue += ""+input.charAt(i);
				}else{
					Toolkit.getDefaultToolkit().beep(); 
				}
			}
		}
		
		if (input!=null){
			super.insertString(offset, newValue, as);
		}
		
	}
}