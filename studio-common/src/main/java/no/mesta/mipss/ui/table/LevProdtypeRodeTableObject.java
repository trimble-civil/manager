package no.mesta.mipss.ui.table;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;

@SuppressWarnings("serial")
public class LevProdtypeRodeTableObject implements IRenderableMipssEntity {
	private Timestamp fraDato;
	private Timestamp tilDato;

	private Leverandor leverandor;
	private Rode rode;
	private List<Prodtype> prodtypeList = new ArrayList<Prodtype>();
	
	public LevProdtypeRodeTableObject() {
		
	}

	public Timestamp getFraDato() {
		return fraDato;
	}

	public void setFraDato(Timestamp fraDato) {
		this.fraDato = fraDato;
	}

	public Timestamp getTilDato() {
		return tilDato;
	}

	public void setTilDato(Timestamp tilDato) {
		this.tilDato = tilDato;
	}

	public Leverandor getLeverandor() {
		return leverandor;
	}
	public String getLeverandorNr(){
		return leverandor.getNr();
	}
	public String getLeverandorNavn(){
		return leverandor.getNavn();
	}

	public void setLeverandor(Leverandor leverandor) {
		this.leverandor = leverandor;
	}

	public Rode getRode() {
		return rode;
	}

	public void setRode(Rode rode) {
		this.rode = rode;
	}

	public List<Prodtype> getProdtypeList() {
		return prodtypeList;
	}

	public void setProdtypeList(List<Prodtype> prodtypeList) {
		this.prodtypeList = prodtypeList;
	}
	
	public String getProdtyperForGui() {
		String s = "";
		for(Iterator<Prodtype> it = prodtypeList.iterator(); it.hasNext();) {
			s += it.next().getTextForGUI();
			if(it.hasNext()) s += ", ";
		}
		return s;
	}

	@Override
	public String getTextForGUI() {
		return null;
	}
}
