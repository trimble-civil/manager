package no.mesta.mipss.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.jdesktop.swingx.JXBusyLabel;

/**
 * Scroll pane som følger med om modellen til tabellen den viser er opptatt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JMipssBeanScrollPane extends JScrollPane implements MipssBeanLoaderListener {
	public boolean busyModel = false;
	private MipssBeanTableModel tableModel;
	private JMipssBeanTable table;
	private JPanel busyPanel;
	private JXBusyLabel label;

	/**
	 * Konstruktør
	 * 
	 * @param table
	 * @param tableModel
	 */
	public JMipssBeanScrollPane(JMipssBeanTable table, MipssBeanTableModel tableModel) {
		super(table);
		this.table = table;
		this.tableModel = tableModel;
		tableModel.addTableLoaderListener(this);

		busyPanel = new JPanel(new GridBagLayout());
		label = new JXBusyLabel();
		busyPanel.add(label, new GridBagConstraints(0, 0, 1, 1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		setWheelScrollingEnabled(true);
		setBusy(tableModel.isLoadingData());
	}

	/**
	 * Setter på scrolleren om den er busy eller ikke
	 * 
	 * @param busy
	 */
	private void setBusy(boolean busy) {
		busyModel = busy;
		table.setVisible(!busy);
		busyPanel.setVisible(busy);
		label.setBusy(busy);

		if (!busy) {
			setViewportView(table);
		} else {
			setViewportView(busyPanel);
		}
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		setBusy(true);
	}

	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		if (event.isErrors()) {
			MipssDialogue dialogue = new MipssDialogue(null, IconResources.ERROR_ICON, "En feil oppstod",
					"<html>Beklager, spørringen feilet.</html>", MipssDialogue.Type.ERROR,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK });
			dialogue.askUser();
		}
		setBusy(false);
	}
}
