package no.mesta.mipss.ui;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;

import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * En implementasjon av <code>BasicButtonUI</code> delegerer med en firkant og gir
 * muligheter for å designe en knapp ved bruk av <code>Shape</code> objekter
 * @author Harald A. Kulø
 */
public class SquareButtonUI extends BasicButtonUI {
    public static final int COLOR_BLUE = 0;
    public static final int COLOR_RED = 1;

    /** Fargen på <code>Shape</code> når de ikke er valgt*/
    private Color defaultShapeColor = new Color(255, 255, 255);
    /** Fargen på <code>Shape</code> når de er valgt, COLOR_BLUE*/
    private Color blueSelectedShapeColor = new Color(120, 162, 216);
    
    /** Fargen på <code>Shape</code> når de er valgt, COLOR_RED */
    private Color redSelectedShapeColor = new Color(223, 154, 135);
    
    
    private int COLOR_SCHEME;
    /** 
     * Lager en ny instans av SquareButtonUI. 
     */
    public SquareButtonUI(int COLOR_SCHEME) {
        this.COLOR_SCHEME = COLOR_SCHEME;
    }

    protected void installDefaults(AbstractButton b) {
        super.installDefaults(b);
        b.setBorder(new EmptyBorder(0, 0, 0, 0));
    }

    public static ComponentUI createUI(JComponent c, int COLOR_SCHEME) {
        if (c instanceof SquareButton) {
            return new SquareButtonUI(COLOR_SCHEME);
        }

        try {
            throw new IllegalArgumentException("SquareButtonUI kan bare brukes på en instans av SquareButton");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Tegner denne komponenten med <code>SquareButton</code> sine <code>Shape</code> objekter
     * med fargene som stemmer overens med om knappen er trykket eller ikke
     * 
     * @param g <code>Graphics</code> konteksten som skal tegnes på
     * @param c <code>JComponent</code> som skal tegnes
     * 
     * @see javax.swing.plaf.basic.BasicButtonUI#paint(java.awt.Graphics, javax.swing.JComponent)
     */
    public void paint(Graphics g, JComponent c) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int w = c.getWidth() - 1;
        int h = c.getHeight() - 1;
        int x = 0;
        int y = 0;
        GradientPaint gradientBackground = null;
        Color selectedShapeColor = null;
        switch (COLOR_SCHEME){
            case COLOR_BLUE: 
                gradientBackground = new GradientPaint(x, y, new Color(0, 56, 130), w, h, new Color(0, 93, 194));
                selectedShapeColor = blueSelectedShapeColor;
            break;
            
            case COLOR_RED:  
                gradientBackground = new GradientPaint(x, y, new Color(121, 39, 17), w, h, new Color(199, 69, 32));
                selectedShapeColor = redSelectedShapeColor;
            break;
            
            default:                
                gradientBackground = new GradientPaint(x, y, new Color(0, 56, 130), w, h, new Color(0, 93, 194));
                selectedShapeColor = blueSelectedShapeColor; break;
        }

        AbstractButton absButton = (AbstractButton)c;
        ButtonModel absButtonModel = absButton.getModel();
        boolean isPressed = absButtonModel.isArmed() && absButtonModel.isPressed() || absButtonModel.isRollover();
        
        //tegn bakgrunnen til knappen
        Rectangle square = new Rectangle(x, y, w, h);
        if (isPressed) {
            g2d.setPaint(gradientBackground);
            g2d.fill(square);
        }

        //tegn omriss av knapp
        g2d.setColor(defaultShapeColor);
        g2d.drawRoundRect(x, y, w, h, 5, 5);

        // tegn shape objektene til knappen
        Shape[] shapes = ((SquareButton)c).getShapes();
        if (shapes != null) {
            if (isPressed)
                g2d.setColor(selectedShapeColor);
            else
                g2d.setColor(defaultShapeColor);

            for (int i = 0; i < shapes.length; i++) {
                g2d.draw(shapes[i]);
            }
        }
    }
}
