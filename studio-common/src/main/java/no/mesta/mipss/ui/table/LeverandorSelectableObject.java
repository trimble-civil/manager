package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.kontrakt.Leverandor;

@SuppressWarnings("serial")
public class LeverandorSelectableObject implements CheckBoxTableObject {

	private final Leverandor leverandor;
	private Boolean valgt;
	
	public LeverandorSelectableObject(Leverandor leverandor){
		this.leverandor = leverandor;
		valgt = Boolean.FALSE;
		
	}
	@Override
	public Boolean getValgt() {
		return valgt;
	}

	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
		
	}
	public String getNr(){
		return leverandor.getNr();
	}
	
	public String getNavn(){
		return leverandor.getNavn();
	}
	public Leverandor getLeverandor(){
		return leverandor;
	}
	@Override
	public String getTextForGUI() {
		return "";
	}

}
