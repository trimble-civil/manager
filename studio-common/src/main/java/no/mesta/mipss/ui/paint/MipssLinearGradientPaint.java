package no.mesta.mipss.ui.paint;

import java.awt.Color;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;

public class MipssLinearGradientPaint implements Paint{
	 private Color color1;
     private Color color2;
     
     
     public MipssLinearGradientPaint(Color color1, Color color2){
          this.color1 = color1;
          this.color2 = color2;
     }
       
     public PaintContext createContext(ColorModel cm, Rectangle deviceBounds, Rectangle2D userBounds, AffineTransform xform, RenderingHints hints){
//        float y1 = (float)userBounds.getY();
//        float y2 = (float)(userBounds.getHeight()/2);
        float height = (float)userBounds.getHeight();
    	 
//    	float x1 = (float)userBounds.getX();
//        float x2 = (float)userBounds.getY();
//        
        LinearGradientPaint gr = new LinearGradientPaint(0, 0, 0, height, new float[]{0,0.5f,1}, new Color[]{color1, color2, color1});
        
//        GradientPaint painter = new GradientPaint(x1, x2, color1, y1, y2, color2);
        
        return gr.createContext(cm, deviceBounds, userBounds, xform, hints);
     }

     public int getTransparency()
     {
        return OPAQUE;
     }  
}
