package no.mesta.mipss.ui;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.VetoableChangeSource;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.Clock;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXDatePicker;

/**
 * Viser en datovelger med klokkeslett også
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author Harald A. Kulø
 */
@SuppressWarnings("serial")
public class JMipssDatePicker extends JXDatePicker  {
	public enum TIME_FORMAT{
		NONE, 
		START_OF_DAY,
		END_OF_DAY
	}
	private TIME_FORMAT timeFormat = TIME_FORMAT.NONE;
	
    private static final Logger logger = LoggerFactory.getLogger(JMipssDatePicker.class);
    private Date date;
    private String format;
	@SuppressWarnings("unchecked")
	private Binding notBeforeBinding;
	private Date notBeforeDate;

	public JMipssDatePicker() {
        this(Clock.now());
    }
    
	public JMipssDatePicker(Locale locale){
		this(null, locale);
	}
    
    /**
     * Initialiserer til en gitt dato
     * 
     * @param date
     */
    public JMipssDatePicker(Date date) {
    	this(date, null);
    }
    public JMipssDatePicker(Date date, Locale loc){
    	super(date, loc);
        this.date = date;
        getEditor().addKeyListener(new KeyHandler());
        
        enforceNotBeforeDate();
        setFormats(new String[]{"dd.MM.yyyy"});
        getEditor().addFocusListener(new FocusAdapter(){
        	public void focusLost(FocusEvent e){
        		//TODO implement for å sette dato når editoren mister fokus...
        	}
        });
    }
    
    @Override
    public void setDate(Date date){
    	Date old = getDate();
    	
    	try{
    		fireVetoableChange("date", old, date);
    		super.setDate(date);
    	}catch (PropertyVetoException e){
    		System.out.println("PropertyVeto @ JMipssDatePicker.setDate...");
    	}
    }
    /**
     * Returnerer datoen til datepickeren. Dersom TIME_FORMAT er spesifisert
     * vil denne bli benyttet til å sette klokkeslettet til Date objektet. 
     */
    @Override
    public Date getDate(){
    	if (timeFormat == TIME_FORMAT.NONE)
    		return super.getDate();
    	
    	Date d = super.getDate();
    	if (d==null)
    		return d;
    	Calendar c = Calendar.getInstance();
    	c.setTime(d);
    	
    	if (timeFormat==null)
    		return d;
    	switch(timeFormat){
    		case START_OF_DAY:
    			c.set(Calendar.HOUR_OF_DAY, 0);
    			c.set(Calendar.MINUTE, 0);
    			c.set(Calendar.SECOND, 0);
    			break;
    		case END_OF_DAY: 
    			c.set(Calendar.HOUR_OF_DAY, 23);
    			c.set(Calendar.MINUTE, 59);
    			c.set(Calendar.SECOND, 59);
    			break;
    	}
    	return c.getTime();
    }
    
    public void setTimeFormat(TIME_FORMAT timeFormat){
    	this.timeFormat = timeFormat;
    }
    /**
     * Parrer denne datovelgeren med en annen. Å parre to 
     * datovelgere betyr i praksis at denne datovelgeren
     * ALLTID vil inneholde en dato som er før eller lik
     * den andre datovelgeren.
     * 
     * @param other
     */
    public void pairWith(final JMipssDatePicker other){
    	
    	addPropertyChangeListener(new PropertyChangeListener(){
    		public void propertyChange(PropertyChangeEvent evt) {
    			String property = evt.getPropertyName();
    			if(StringUtils.equals("date", property)) {
    				Date now = (Date) evt.getNewValue();
    				if (other.getDate()!=null){
    					if (now!=null){
		    				if (other.getDate().before(now)){
		    					other.setDate(now);
		    				}
    					}
    				}
    			}
    		}	
    	});
    	other.addPropertyChangeListener(new PropertyChangeListener(){
    		public void propertyChange(PropertyChangeEvent evt) {
    			String property = evt.getPropertyName();
    			if(StringUtils.equals("date", property)) {
    				Date now = (Date) evt.getNewValue();
    				if (getDate()!=null){
    					if (now!=null){
		    				if (getDate().after(now)){
		    					setDate(now);
		    				}
    					}
    				}
    			}
    		}	
    	});
    }
	/**
     * Sørger for at dato ikke kan settes til før notBeforeDate
     * 
     */
    public void enforceNotBeforeDate() {
    	super.addPropertyChangeListener("date", new NotBeforeListener());
    }

    /** {@inheritDoc} */
	@Override
	protected void finalize() throws Throwable {
		logger.debug("finalize()");

		Throwable err = null;

		try {
			super.finalize();
		} catch (Throwable t) {
			err = t;
		}

		if (notBeforeBinding != null) {
			try {
				if (notBeforeBinding.isBound()) {
					notBeforeBinding.unbind();
				}
				notBeforeBinding = null;
			} catch (Throwable t) {
				t = null;
			}
		}

		if (err != null) {
			throw err;
		}
	}
    
    /**
	 * @return the notBeforeDate
	 */
	public Date getNotBeforeDate() {
		return notBeforeDate;
	}
    

    /**
	 * @param notBeforeDate the notBeforeDate to set
	 */
	public void setNotBeforeDate(Date notBeforeDate) {
		Date old = this.notBeforeDate;
		this.notBeforeDate = notBeforeDate;
		super.firePropertyChange("notBeforeDate", old, notBeforeDate);
	}

	/**
	 * Setter en kilde som datovelgeren skal lytte på, som angir tidligst mulige
	 * dato
	 * 
	 * @param source
	 * @param field
	 */
	public void setNotBeforeSource(Object source, String field) {
		logger.debug("setNotBeforeSource(" + source + "," + field + ")");
		notBeforeBinding = BindingHelper.createbinding(source, field, this, "notBeforeDate");
		notBeforeBinding.bind();
	}
    
    /**
     * Lytter på tastaturet
     * 
     * @author Harald A. Kulø
     *
     */
    private class KeyHandler implements KeyListener{
    	/** {@inheritDoc} */
		@Override
		public void keyPressed(KeyEvent e) {}
		
		/** {@inheritDoc} */
		@Override
		public void keyReleased(KeyEvent e) {}
		/** {@inheritDoc} */
		@Override
		public void keyTyped(KeyEvent e) {
			if (e.getKeyChar()==KeyEvent.VK_ENTER){
				String txt = getEditor().getText();
				SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
				Date d = null;
				try {
					d = format.parse(txt);
					setDate(d);
				} catch (ParseException e1) {
					logger.error("failed to parse date..");
					setDate(date);
				}
			}	
		}
    	
    }
    
    /**
     * Lytter på dato for å sjekke at den ikke settes til "før" datoen
     * 
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    private class NotBeforeListener implements PropertyChangeListener {
    	
    	/** {@inheritDoc} */
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			String property = evt.getPropertyName();
			if(StringUtils.equals("date", property)) {
				Date old = (Date) evt.getOldValue();
				Date now = (Date) evt.getNewValue();
				if(notBeforeDate != null && now != null) {
					if(now.before(notBeforeDate)) {
						setDate(old);
					}else{
					}
				}
			}
		}	
    }
}
