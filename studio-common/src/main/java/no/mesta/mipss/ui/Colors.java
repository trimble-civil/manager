package no.mesta.mipss.ui;

import java.awt.*;

public class Colors {
    public static final Color WARNING_COLOR = new Color(255,215,215);
    public static final Color OK_COLOR = new Color(208,247,220);
    public static final Color ERROR_COLOR = new Color(208, 50, 50);
    public static final Color VALIDATION_HIGHLIGHT_COLOR = new Color(255,255,51);

    public static final Color BOOTSTRAP_BLUE = new Color(91, 192, 222);
    public static final Color BOOTSTRAP_GREEN = new Color(92, 184, 92);

    public static final Color TABLE_ROW_HIGHLIGHT_COLOR = new Color(208, 223, 244);
    public static final Color TABLE_GRID_COLOR = new Color(230,230,230);

    public static final Color RETURNED = new Color(255, 68, 68);
    public static final Color SUBMITTED = new Color(255, 187, 51);
    public static final Color PROCESSED = new Color(51, 181, 229);
    public static final Color SENT_IO = new Color(153, 204, 0);

    public static final Color PRISTINE_ACTIVITY = new Color(226,239,217);
    public static final Color DIRTY_ACTIVITY = new Color(255, 242, 204);
    public static final Color MANUAL_WEB_ACTIVITY = new Color(251, 228, 213);
    public static final Color MANUAL_MANAGER_ACTIVITY = new Color(221, 240, 255);

}
