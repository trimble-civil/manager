package no.mesta.mipss.ui.pluginlist;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import java.util.List;

import javax.swing.Icon;
import javax.swing.JButton;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import javax.swing.SwingUtilities;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.plugin.MipssPlugin;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Lager en knapp som viser listen over kjørende plugins
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JMipssPluginsList extends JButton implements PluginListModelListener {
	private IMipssStudioLoader loader = null;
	private Logger logger = LoggerFactory.getLogger(JMipssPluginsList.class);
	private JPopupMenu menu = new JPopupMenu();
	private PluginListModel model = null;
	private List<Binding<?, ?, ?, ?>> bindings = new ArrayList<Binding<?, ?, ?, ?>>();

	/**
	 * Konstruktør
	 * 
	 * @param text
	 * @param icon
	 */
	public JMipssPluginsList(IMipssStudioLoader loader, String text, Icon icon) {
		super(text, icon);
		this.loader = loader;
		model = loader.getRunningPluginsKeys();
		model.addPluginListModelListener(this);

		populateMenu();

		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Point p = new Point(getXpos(), getYpos());
				menu.show(JMipssPluginsList.this, (int) p.getX(), (int) p.getY());
				p.setLocation(getXpos(), getYpos());
				SwingUtilities.convertPointToScreen(p, JMipssPluginsList.this);
				menu.setLocation(p);
			}

			private int getXpos() {
				return -(menu.getWidth() - JMipssPluginsList.this.getWidth());
			}

			private int getYpos() {
				return -menu.getHeight();
			}
		});
	}

	/**
	 * @see no.mesta.mipss.ui.pluginlist.PluginListModelListener#modelChanged(PluginListModelEvent)
	 * 
	 * @param event
	 */
	public void modelChanged(PluginListModelEvent event) {
		for (Binding<?, ?, ?, ?> b : bindings) {
			b.unbind();
		}
		bindings.clear();

		populateMenu();
	}

	/**
	 * Fyller ut menyen
	 * 
	 */
	private void populateMenu() {
		logger.debug("populateMenu() start");
		menu.removeAll();

		List<MipssPlugin> loadedPlugins = model.getPlugins();
		List<MipssPlugin> plugins = new ArrayList<MipssPlugin>();
		for (MipssPlugin plugin : loadedPlugins) {
			if (plugin.isDocked()) {
				plugins.add(plugin);
			}
		}

		MipssPlugin[] sortedPlugins = new MipssPlugin[plugins.size()];

		if (sortedPlugins.length == 0) {
			setEnabled(false);
			return;
		}

		setEnabled(true);
		sortedPlugins = plugins.toArray(sortedPlugins);
		Arrays.sort(sortedPlugins, new PluginComparator());

		for (MipssPlugin plugin : sortedPlugins) {
			JMenuItem item = new JMenuItem(plugin.getModuleName());
			Binding<MipssPlugin, String, JMenuItem, String> b = BindingHelper.createbinding(plugin, "moduleName", item,
					"text");
			b.bind();
			bindings.add(b);

			if (plugin.getIcon() != null) {
				item.setIcon(plugin.getIcon());
			}
			Binding<MipssPlugin, Icon, JMenuItem, Icon> b2 = BindingHelper.createbinding(plugin, "icon", item, "icon");
			b2.bind();
			bindings.add(b2);

			if (plugin.getStatus() != MipssPlugin.Status.RUNNING) {
				item.setEnabled(false);
			}

			item.addActionListener(new ItemListener(plugin));

			menu.add(item);
		}
		logger.debug("populateMenu() finish");
	}

	/** {@inheritDoc} */
	@Override
	protected void finalize() throws Throwable {
		for (Binding<?, ?, ?, ?> b : bindings) {
			try {
				b.unbind();
			} catch (Throwable t) {
				t = null;
			}
		}
		bindings.clear();
		bindings = null;
	}

	/**
	 * Action listener for meny items
	 * 
	 */
	class ItemListener implements ActionListener {
		private MipssPlugin plugin = null;

		public ItemListener(MipssPlugin plugin) {
			this.plugin = plugin;
		}

		/**
		 * Sørger for at plugin som item viser til vises i docken
		 * 
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			MenynodeDTO menyNode = plugin.getMenynode();
			if(menyNode != null) {
				menyNode.setFjernValgt(true);
				loader.activateMenuItem(menyNode);
				menyNode.setFjernValgt(false);
			}
			loader.activatePlugin(plugin.getCacheKey());
		}
	}

	/**
	 * Brukes til å sortere plugins
	 * 
	 */
	class PluginComparator implements Comparator<MipssPlugin> {

		/**
		 * @see java.util.Comparator#compare(T,T)
		 * 
		 * @param p1
		 * @param p2
		 * @return
		 */
		public int compare(MipssPlugin p1, MipssPlugin p2) {
			return new CompareToBuilder().append(p1.getDisplayName(), p2.getDisplayName()).toComparison();
		}
	}
}
