package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.Line2D;

public class ArrowButton extends SquareButton{
    
    public ArrowButton(Dimension d, String tooltip) {
        super(SquareButtonUI.COLOR_BLUE);
        Line2D lr1 = new Line2D.Double(5, 3, d.width - 4, 3);
        Line2D lr2 = new Line2D.Double(5, 4, d.width - 4, 4);
        Line2D lr3 = new Line2D.Double(d.width-4, 3, d.width-4, d.height - 7);
        Line2D lr4 = new Line2D.Double(d.width-5, 3, d.width-5, d.height - 7);
        
        Line2D lr5 = new Line2D.Double(d.width-5, 3, 4, d.height - 6);
        Line2D lr6 = new Line2D.Double(d.width-4, 3, 4, d.height - 5);
        Line2D lr7 = new Line2D.Double(d.width-4, 4, 5, d.height - 5);
        
        Line2D lr8 = new Line2D.Double(7, d.height-4, d.width-4, d.height - 4);
        Line2D lr9 = new Line2D.Double(7, d.height-3, d.width-4, d.height - 3);
        
        Shape[] s2 = {lr1, lr2, lr3, lr4, lr5, lr6, lr7, lr8, lr9};
        setShapes(s2);
        setToolTipText(tooltip);
        setPreferredSize(d);
    }
}
