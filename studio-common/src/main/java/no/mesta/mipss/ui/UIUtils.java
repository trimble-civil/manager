package no.mesta.mipss.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Box.Filler;
/**
 * Utilklasse for vanlige UI operasjoner i modulen
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class UIUtils {
	
	public static JPanel getHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		return panel;
	}
	
	public static JPanel getVertPanel(){
		JPanel panel =new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		return panel;
	}
	
	public static JPanel getTwoComponentPanelHoriz(JComponent c1, JComponent c2, Dimension space){
		JPanel p = getVertPanel();
		p.add(c1);
		p.add(Box.createRigidArea(space));
		p.add(c2);
		return p;
	}
	
	public static JPanel getTwoComponentPanelVert(JComponent c1, JComponent c2, boolean glue){
		JPanel p = getVertPanel();
		JPanel p1 = getHorizPanel();
		p1.add(c1);
		p1.add(Box.createHorizontalGlue());
		JPanel p2 = getHorizPanel();
		p2.add(c2);
		if(glue)
			p2.add(Box.createHorizontalGlue());
		p.add(p1);
		p.add(p2);
		return p;
	}
	
	public static Component getHStrut(int width){
		return new Filler(new Dimension(width,0), new Dimension(width,0), 
				  new Dimension(width, 0));
	}
	
	public static Component getVStrut(int height){
		return new Filler(new Dimension(0,height), new Dimension(0,height), 
				  new Dimension(0, height));
	}
	
	public static void setComponentSize(Dimension s, JComponent c){
		c.setPreferredSize(s);
		c.setMaximumSize(s);
		c.setMinimumSize(s);
	}
	
	
}
