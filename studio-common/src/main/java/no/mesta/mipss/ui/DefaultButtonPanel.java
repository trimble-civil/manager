package no.mesta.mipss.ui;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Klasse for å lage knapperader
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class DefaultButtonPanel {

	/**
	 * Henter ut standard knapperad
	 * @param okAction action for hva som skjer ved klikk på ok
	 * @param cancelAction action for hva som skjer ved klikk på avbryt
	 * @param applyAction action for hva som skjer ved klikk på bruk
	 * @return
	 */
	public static JPanel getDefaultButtonPanel(AbstractAction okAction, AbstractAction cancelAction, AbstractAction applyAction){
		if (okAction.getValue(Action.NAME).equals(""))
			okAction.putValue(Action.NAME, "Ok");
		if (cancelAction.getValue(Action.NAME).equals(""))
			cancelAction.putValue(Action.NAME, "Avbryt");
		if (applyAction.getValue(Action.NAME).equals(""))
			applyAction.putValue(Action.NAME, "Bruk");
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(new JButton(okAction));
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(new JButton(cancelAction));
		buttonPanel.add(Box.createHorizontalStrut(5));
		buttonPanel.add(new JButton(applyAction));
		return buttonPanel;
	}
	
	
}
