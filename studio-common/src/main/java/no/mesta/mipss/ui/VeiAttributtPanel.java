package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
/**
 * Panel som inneholder attributtene en vei har, samtlige felter har ett document som sørger for at
 * kun gyldige verdier kan settes på feltene.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class VeiAttributtPanel extends JPanel{
	
	private JFormattedTextField fylkeField;
	private JFormattedTextField kommuneField;
	private JFormattedTextField kategoriField;
	private JFormattedTextField statusField;
	private JFormattedTextField nummerField;
	private JFormattedTextField hpField;
	private JFormattedTextField meterField;
	private final boolean meter;
	
	public VeiAttributtPanel(boolean meter){
		this.meter = meter;
		setLayout(new GridBagLayout());
		initGui();
	}
	
	private void initGui(){
		PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
		JLabel fylkeLabel = new JLabel(props.getGuiString("VelgVeiPanel.fylkeLabel"));
		JLabel kommuneLabel = new JLabel(props.getGuiString("VelgVeiPanel.kommuneLabel"));
		JLabel kategoriLabel = new JLabel(props.getGuiString("VelgVeiPanel.kategoriLabel"));
		JLabel statusLabel = new JLabel(props.getGuiString("VelgVeiPanel.statusLabel"));
		JLabel nummerLabel = new JLabel(props.getGuiString("VelgVeiPanel.nummerLabel"));
		JLabel hpLabel = new JLabel(props.getGuiString("VelgVeiPanel.hpLabel"));
		
		Dimension labelSize = new Dimension(75, 20);
		setSize(fylkeLabel, labelSize);
		setSize(kommuneLabel, labelSize);
		setSize(kategoriLabel, labelSize);
		setSize(statusLabel, labelSize);
		setSize(nummerLabel, labelSize);
		setSize(hpLabel, labelSize);
		
		fylkeField = new JFormattedTextField();
		fylkeField.setDocument(new DocumentFilter(2, "0123456789", true));
		fylkeField.setToolTipText(props.getGuiString("fylkeField.tooltip"));
		kommuneField = new JFormattedTextField();
		kommuneField.setDocument(new DocumentFilter(2, "0123456789", true));
		kommuneField.setToolTipText(props.getGuiString("kommuneField.tooltip"));
		kategoriField = new JFormattedTextField();
		kategoriField.setDocument(new DocumentFilter(1, "RKEF", true));
		kategoriField.setToolTipText(props.getGuiString("kategoriField.tooltip"));
		statusField = new JFormattedTextField();
		statusField.setDocument(new DocumentFilter(1, "VS", true));
		statusField.setToolTipText(props.getGuiString("statusField.tooltip"));
		nummerField = new JFormattedTextField();
		nummerField.setDocument(new DocumentFilter(5, "0123456789", true));
		nummerField.setToolTipText(props.getGuiString("nummerField.tooltip"));
		hpField = new JFormattedTextField();
		hpField.setDocument(new DocumentFilter(3, "0123456789", true));
		hpField.setToolTipText(props.getGuiString("hpField.tooltip"));
		
		
		add(fylkeLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
		add(fylkeField, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));
		
		add(kommuneLabel, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
		add(kommuneField, new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));
		
		add(kategoriLabel, new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
		add(kategoriField, new GridBagConstraints(1,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));
		
		add(statusLabel, new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
		add(statusField, new GridBagConstraints(1,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));
		
		add(nummerLabel, new GridBagConstraints(0,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
		add(nummerField, new GridBagConstraints(1,4, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));
		
		add(hpLabel, new GridBagConstraints(0,5, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
		add(hpField, new GridBagConstraints(1,5, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));
		
		if (meter){
			JLabel meterLabel = new JLabel(props.getGuiString("VelgVeiPanel.meterLabel"));
			setSize(meterLabel, labelSize);
			meterField = new JFormattedTextField();
			meterField.setDocument(new DocumentFilter(5, "0123456789", true));
			add(meterLabel, new GridBagConstraints(0,6, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5), 0,0));
			add(meterField, new GridBagConstraints(1,6, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0,0));

		}
		/*
		
		JPanel main = getMainPanel();
		//main.add(getHeader(props));
		//main.add(Box.createVerticalStrut(5));
		main.add(getPanel(fylkeLabel, fylkeField));
		main.add(getPanel(kommuneLabel, kommuneField));
		main.add(getPanel(kategoriLabel, kategoriField));
		main.add(getPanel(statusLabel, statusField));
		main.add(getPanel(nummerLabel, nummerField));
		main.add(getPanel(hpLabel, hpField));
		
		main.setPreferredSize(new Dimension(120, 220));
		add(main);*/
	}

	/**
	 * Renser alle feltene
	 */
	public void clear(){
		 fylkeField.setText(null);
		 kommuneField.setText(null);
		 kategoriField.setText(null);
		 statusField.setText(null);
		 nummerField.setText(null);
		 hpField.setText(null);
	}
	private JPanel getPanel(JLabel label, JTextField text){
		JPanel panel = getHorizPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(0,10,5,10));
		panel.add(label);
		panel.add(text);

		return panel;
	}
	
	private void setSize(JComponent c, Dimension size){
		c.setPreferredSize(size);
		c.setMaximumSize(size);
		c.setMinimumSize(size);
	}
	private JPanel getMainPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		return panel;
	}
	
	
	private JPanel getHorizPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		return panel;
	}
	
	
	/**
	 * @return the fylkeField
	 */
	public JTextField getFylkeField() {
		return fylkeField;
	}
	/**
	 * @return the kommuneField
	 */
	public JTextField getKommuneField() {
		return kommuneField;
	}
	/**
	 * @return the kategoriField
	 */
	public JTextField getKategoriField() {
		return kategoriField;
	}
	/**
	 * @return the statusField
	 */
	public JTextField getStatusField() {
		return statusField;
	}
	/**
	 * @return the nummerField
	 */
	public JTextField getNummerField() {
		return nummerField;
	}
	/**
	 * @return the hpField
	 */
	public JTextField getHpField() {
		return hpField;
	}
	public JTextField getMeterField() {
		return meterField;
	}
}
