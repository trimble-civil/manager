package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;
import no.mesta.mipss.ui.table.AarsakTableObject;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * Panel med tabell for valgbare entiteter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JAarsakPanel extends JPanel implements ItemListener, PropertyChangeListener {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_ALL = BUNDLE.getGuiString("aarsakPanel.all");
	private static final String GUI_TITLE = BUNDLE.getGuiString("aarsakPanel.title");
	private static final Logger logger = LoggerFactory.getLogger(JAarsakPanel.class);
	private Set<AarsakTableObject> allCauses;
	private JCheckBox alleCheck;
	private String[] columns = new String[] { "valgt", "textForGUI" };
	private AarsakTableObject ingen;
	private JXTable table;
	private MipssRenderableEntityTableModel<AarsakTableObject> tableModel;
	private Set<AarsakTableObject> valgte = new HashSet<AarsakTableObject>();

	public JAarsakPanel() {
		initGui();
	}

	public Long[] getAlleValgte() {
		logger.debug("getAlleValgte() start");

		return setToArray(valgte);
	}

	private JXTable getProsessTable() {
		allCauses = new TreeSet<AarsakTableObject>();
		List<Hendelseaarsak> all = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class).hentAlleHendelsesAarsaker();

		ingen = new AarsakTableObject(null);
		for (Hendelseaarsak p : all) {
			AarsakTableObject ato = new AarsakTableObject(p);
			ato.addPropertyChangeListener("valgt", this);
			allCauses.add(ato);
		}
		allCauses.add(ingen);

		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				AarsakTableObject.class, columns);
		tableModel = new MipssRenderableEntityTableModel<AarsakTableObject>(AarsakTableObject.class, new int[] { 0 },
				columns);
		tableModel.getEntities().clear();
		tableModel.getEntities().addAll(allCauses);
		JXTable table = new JXTable(tableModel, columnModel);
		table.getColumn(0).setMaxWidth(30);

		return table;
	}
	private JCheckTablePanel checkPanel;
	public void setSelected(boolean selected){
		checkPanel.setSelected(selected);
	}
	private void initGui() {
		table = getProsessTable();

		alleCheck = new JCheckBox(GUI_ALL);
		checkPanel = new JCheckTablePanel<AarsakTableObject>(table, tableModel, alleCheck);

		Box components = BoxUtil.createVerticalBox(0, BoxUtil.createHorizontalBox(0, checkPanel), BoxUtil
				.createHorizontalBox(0, alleCheck, Box.createHorizontalGlue()));
		ComponentSizeResources.setComponentSize(this, new Dimension(200, 200));

		components.setBorder(BorderFactory.createTitledBorder(GUI_TITLE));

		setLayout(new BorderLayout());
		add(components, BorderLayout.CENTER);
	}

	public boolean isAlleValgt() {
		return alleCheck.isSelected();
	}

	public boolean isIngenValgt() {
		return ingen.getValgt();
	}

	/** {@inheritDoc} */
	@Override
	public void itemStateChanged(ItemEvent e) {
		boolean isSet = e.getStateChange() == ItemEvent.SELECTED;

		if (e.getSource() == alleCheck) {
			firePropertyChange("alleValgt", !isSet, isSet);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		logger.debug("propertyChange(" + evt + ") start for " + property);
		AarsakTableObject source = (AarsakTableObject) evt.getSource();
		if (StringUtils.equals(property, "valgt") && source == ingen) {
			Boolean oldValue = (Boolean) evt.getOldValue();
			Boolean newValue = (Boolean) evt.getNewValue();
			firePropertyChange("ingenValgt", oldValue, newValue);
		} else if (StringUtils.equals(property, "valgt")) {
			Set<AarsakTableObject> old = new HashSet<AarsakTableObject>(valgte);
			if (source.getValgt() == true) {
				valgte.add(source);
			} else {
				valgte.remove(source);
			}

			Long[] oldValues = setToArray(old);
			Long[] nowValues = setToArray(valgte);

			logger.debug("fire property change, old = " + Arrays.toString(oldValues) + ", now = "
					+ Arrays.toString(nowValues));
			firePropertyChange("alleValgte", oldValues, nowValues);
		}
	}

	private Long[] setToArray(Set<AarsakTableObject> set) {
		Long[] ids = new Long[set.size()];
		Iterator<AarsakTableObject> it = set.iterator();
		int i = 0;
		boolean noneHit = false;
		while (it.hasNext()) {
			AarsakTableObject aarsak = (AarsakTableObject) it.next();
			if (aarsak.getAarsak() != null) {
				ids[i] = aarsak.getAarsak().getId();
				i++;
			} else {
				noneHit = true;
			}
		}

		if (noneHit && ids.length > 0) {
			ids = Arrays.copyOf(ids, ids.length - 1);
		}

		return ids;
	}
}
