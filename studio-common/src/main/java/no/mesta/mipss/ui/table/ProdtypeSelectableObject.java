package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;

@SuppressWarnings("serial")
public class ProdtypeSelectableObject implements CheckBoxTableObject  {
	private Prodtype prodtype;
	private Boolean valgt;
	
	public ProdtypeSelectableObject(Prodtype prodtype) {
		this.prodtype = prodtype;
		valgt = Boolean.FALSE;
	}
	
	public String getNavn() {
		return prodtype.getNavn();
	}
	
	public String getSesong() {
		return prodtype.getSesong().getSesong();
	}
	
	public Prodtype getProdtype() {
		return prodtype;
	}

	public Boolean getValgt() {
		return valgt;
	}

	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}

	@Override
	public String getTextForGUI() {
		return prodtype.getTextForGUI();
	}
}
