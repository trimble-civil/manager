package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.text.NumberFormatter;

public class BoldNumberCellEditor extends DefaultCellEditor {
    public BoldNumberCellEditor(){
        super(new JFormattedTextField());
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        JFormattedTextField editor = (JFormattedTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);

        if (value!=null){
            
            Locale myLocale = Locale.getDefault(); 

            NumberFormat numberFormat = NumberFormat.getInstance(myLocale);
            numberFormat.setMaximumFractionDigits(3);
            numberFormat.setMinimumFractionDigits(0);
            numberFormat.setMinimumIntegerDigits(1);

            editor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(
                            new NumberFormatter(numberFormat)));

            editor.setHorizontalAlignment(SwingConstants.RIGHT);
            editor.setValue(value);
        }
        return editor;
    }
}
