package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.util.Formatter;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * Rendrer Double i celler som har java.lang.Double
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class DoubleRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
	public static final String DEFAULT_FORMAT = "%1$,1.3f";
	public static final String SHORT_FORMAT = "%1$,1.1f";
	public static final String MEDIUM_FORMAT = "%1$,1.2f";
	private final String format;
	private boolean transparentBackground = false;
	
    public DoubleRenderer() {
        this(DEFAULT_FORMAT);
    }
    
    public void setTransparentBackground(boolean transparentBackground){
    	this.transparentBackground = transparentBackground;
    }
    
    public DoubleRenderer(String format) {
    	if(format == null) {
    		this.format = DEFAULT_FORMAT;
    	} else {
    		this.format = format;
    	} 	
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
    	Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
		Box panel = Box.createHorizontalBox();
		panel.setBackground(c.getBackground());
		panel.setForeground(c.getForeground());
		panel.add(Box.createHorizontalGlue());
		panel.setOpaque(true);

        if(value != null && value instanceof Double) {
        	Formatter formatter = new Formatter();
        	formatter = formatter.format(format, (Double) value);
            
        	JLabel label = new JLabel(formatter.toString());
        	label.setBackground(c.getBackground());
        	label.setForeground(c.getForeground());
        	label.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
        	if (!transparentBackground){
        		label.setOpaque(true);
        	}
            panel.add(label);
        }
        
        return panel;
    }
}