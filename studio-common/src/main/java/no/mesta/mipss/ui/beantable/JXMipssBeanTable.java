package no.mesta.mipss.ui.beantable;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

import javax.swing.table.TableModel;

import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXGlassBox;
import org.jdesktop.swingx.JXTable;


/**
 * Identisk med JMipssBeanTable med unntaket at den arver fra JXTable
 * Tabell som vet om modellen er opptatt med aa laste data eller ikke
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kuloe</a>
 */
@Deprecated //endringer i JMipssBeanTable gjør denne klassen overflødig
public class JXMipssBeanTable extends JXTable implements MipssBeanLoaderListener{
    

    private MipssBeanTableModel model = null;
    private boolean busyModel = true;
    private JXGlassBox busyBox = null;
    
    /**
     * Konstruktoer
     * 
     * @param model
     * @param columnModel
     */
    public JXMipssBeanTable(MipssBeanTableModel model, MipssRenderableEntityTableColumnModel columnModel) {
        super((TableModel) model, columnModel);
        this.model = model;
        model.addTableLoaderListener(this);
        busyModel = model.isLoadingData();
        
    }
        
    /** {@inheritDoc} */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if(busyModel) {
        }
    }

    /** {@inheritDoc} */
    public void loadingStarted(MipssBeanLoaderEvent event) {
        busyModel = true;
        setEnabled(false);
        repaint();
    }
    
    /** {@inheritDoc} */
    public void loadingFinished(MipssBeanLoaderEvent event) {
        busyModel = false;
        setEnabled(true);
        repaint();
    }
    
    /** {@inheritDoc} */
    @Override
    public void processMouseEvent(MouseEvent event) {
        //Dette er et hack slik at andre komponenter kan velge raden
        //som vises paa et musepunkt :)
        super.processMouseEvent(event);
    }
}
