package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.util.EpostUtils;

/**
 * Default celleditor som validerer at brukeren taster en riktig formattert epost adresse.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class EmailEditor extends DefaultCellEditor{
	private JFormattedTextField text;
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	
	public EmailEditor(){
		super(new JFormattedTextField());
		text = (JFormattedTextField) super.getComponent();
		text.setFocusLostBehavior(JFormattedTextField.PERSIST);
		text.setFormatterFactory(new DefaultFormatterFactory(new EmailFormatter()));
		
		text.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "validate");
		text.getActionMap().put("validate", new ValidateAction());
		
	}
	
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        JFormattedTextField text = (JFormattedTextField)super.getTableCellEditorComponent(table, value, isSelected, row, column);
        text.setValue(value);
        
        return text;
    }
    
    @Override
    public Object getCellEditorValue() {
        JFormattedTextField ftf = (JFormattedTextField)getComponent();
        Object o = ftf.getValue();
        //TODO valider e-post når dataene leses(?)...
        return o;
    }
    
    
    /**
     * Overskriv for å sjekke at det er tastet
     * inn en gyldig epostadresse. Set verdien
     * hvis alt er ok, eller si i fra hvis noe er galt.
     */
    @Override
    public boolean stopCellEditing() {
        JFormattedTextField text = (JFormattedTextField)getComponent();
        if (text.isEditValid()) {
            try {
                text.commitEdit();
            } catch (java.text.ParseException exc) { }
	    
        } else { //ugyldig epost
            if (!showWarning()) { //editer videre
	        return false; 
	    } 
        }
        return super.stopCellEditing();
    }

    /** 
     * Lets the user know that the text they entered is 
     * bad. Returns true if the user elects to revert to
     * the last good value.  Otherwise, returns false, 
     * indicating that the user wants to continue editing.
     * 
     * @return true hvis bruker vil sette tilbake til forrige riktige tilstand,
     * false hvis adressen skal editeres videre
     */
    protected boolean showWarning() {
        Toolkit.getDefaultToolkit().beep();
        
        String title = props.getGuiString("emailEditor.warning.title");
        String message = props.getGuiString("emailEditor.warning.message");
		
        Object[] options = {"Rediger", "Tilbakestill"};
        int value = JOptionPane.showOptionDialog(SwingUtilities.getWindowAncestor(text),
            message,title,
            JOptionPane.YES_NO_OPTION,
            JOptionPane.ERROR_MESSAGE,
            null,options, options[1]);
	    
        if (value == 1) { 
            text.setValue(text.getValue());
            return true;
        }
        return false;
    }
    

    /**
     * Formatter klasse som her blir brukt til å sjekke om 
     * eposten har riktig format. Kaster en parseException for
     * å indikere at eposten ikke er riktig formatert. 
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class EmailFormatter extends DefaultFormatter{
    	 public Object stringToValue(String string) throws ParseException {
    		 if (string==null||string.equals(""))
    			 return null;
    		 string = string.trim();
    		 if (!EpostUtils.verifyEpost(string)){
    			 throw new ParseException("Not a valid e-mail adress", 0);
    		 }
    		 return string;
    	 }
    }
    /**
     * Actionklasse for validering av epost
     * 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    class ValidateAction extends AbstractAction{
    	@Override
    	public void actionPerformed(ActionEvent e) {
        	if (!text.isEditValid()) { 
        		if (showWarning()) { 
        			text.postActionEvent(); 
        		}
            } else try {              
                text.commitEdit();    
                text.postActionEvent(); 
            } catch (java.text.ParseException exc) { }
        }
    }

}
