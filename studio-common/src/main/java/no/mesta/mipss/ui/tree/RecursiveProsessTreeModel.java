package no.mesta.mipss.ui.tree;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.mipssfield.Prosess;

@SuppressWarnings("serial")
public class RecursiveProsessTreeModel extends DefaultTreeModel implements PropertyChangeSource{

	protected DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Prosesser");
	private final boolean visIngenProsesser;
	private boolean ingenProsesser;
	private boolean velgUnderprosesserAutomatisk;
	private boolean fjernProsessForUnderprosess;
	private boolean singleSelectionMode;
	protected final JTree tree;
	private Prosess node;


	public RecursiveProsessTreeModel(JTree tree, boolean visIngenProsesser,  boolean velgUnderprosesserAutomatisk, boolean fjernProsessForUnderprosess) {
		super(null);
		this.tree = tree;
		this.visIngenProsesser = visIngenProsesser;
		this.setVelgUnderprosesserAutomatisk(velgUnderprosesserAutomatisk);
		tree.setCellEditor(new CheckBoxNodeEditor(tree));
		tree.setCellRenderer(new CheckBoxNodeRenderer());
		tree.setEditable(true);
		
		setRoot(rootNode);
	}
	
	public void selectAll(boolean selected) {
		ingenProsesser = selected;
		selectAll(rootNode, selected);
		fireTreeNodesChanged(this, new Object[]{rootNode}, null, null);
	}
	
	private void selectAll(DefaultMutableTreeNode node, boolean selected){
		Object o = node.getUserObject();
		if (o instanceof SelectableProsessNode){
			SelectableProsessNode pn = (SelectableProsessNode)o;
			pn.setSelected(selected);
		}
		
		int c = node.getChildCount();
		for (int i=0;i<c;i++){
			DefaultMutableTreeNode child = (DefaultMutableTreeNode)node.getChildAt(i);
			selectAll(child, selected);
		}
	}
	public void setProsesser(List<Prosess> prosesser) {
		setRoot(null);
		rootNode.removeAllChildren();
		
		if(prosesser != null) {
			for(Prosess prosess: prosesser) {
				rootNode.add(createTree(prosess));
			}				
		}
		setRoot(rootNode);
	}
	
	
	
	private DefaultMutableTreeNode createTree(Prosess p){
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(new SelectableProsessNode(p));
		for (Prosess pp:p.getProsesser()){
			node.add(createTree(pp));
		}
		return node;
	}
	
	public void setSelectedProsessId(Long prosessId){
		DefaultMutableTreeNode node = findNode(prosessId);
		if (node!=null){
			if (singleSelectionMode){
				selectAll(false);
			}
			SelectableProsessNode selectedNode = (SelectableProsessNode)node.getUserObject();
			selectedNode.setSelected(true);
			nodeSelected(selectedNode.getProsess(), true);
			tree.expandPath(new TreePath(node.getPath()));
			tree.scrollPathToVisible(new TreePath(node.getPath()));
			
		}
	}
	private DefaultMutableTreeNode findNode(Long prosessId){
		return findNode(rootNode, prosessId);
	}
	
	private DefaultMutableTreeNode findNode(DefaultMutableTreeNode root, Long prosessId){
		if (root.getUserObject() instanceof SelectableProsessNode){
			if (((SelectableProsessNode)root.getUserObject()).getProsess().getId().equals(prosessId))
				return root;
		}
		int c = root.getChildCount();
		for (int i=0;i<c;i++){
			DefaultMutableTreeNode node = findNode((DefaultMutableTreeNode)root.getChildAt(i), prosessId);
			if (node !=null && (node.getUserObject() instanceof SelectableProsessNode)){
				if (((SelectableProsessNode)node.getUserObject()).getProsess().getId().equals(prosessId))
					return node;
			}
		}
		//not there...
		return null;
	}
	/**
	 * Blir kalt hver gang en node blir valgt i treet
	 * 
	 * @param node
	 * @param selected
	 */
	protected void nodeSelected(Prosess node, boolean selected) {
		Prosess old = this.node;
		this.node = node;
		props.firePropertyChange("selectedNode", old, node);
	}
	public Prosess getSelectedNode(){
		return node;
	}
	
//	public Long[] getProsessId() {
//		Set<Long> selectedProsessIds = new HashSet<Long>();
//		getSelectedSubtree(rootNode, selectedProsessIds); 
//		return selectedProsessIds.toArray(new Long[]{});
//	}
	
	public Long[] getProsessId() {
		Set<Long> selectedProsessIds = new HashSet<Long>();
		getSelectedSubtree(rootNode, selectedProsessIds, SokModus.HENT_PROSESS); 
		return selectedProsessIds.toArray(new Long[]{});
	}
	
	public Long[] getUnderprosessId() {
		Set<Long> selectedProsessIds = new HashSet<Long>();
		getSelectedSubtree(rootNode, selectedProsessIds, SokModus.HENT_UNDERPROSESS); 
		return selectedProsessIds.toArray(new Long[]{});
	}
	
	public Set<Prosess> getValgteProsesser(){
		Set<Prosess> valgteProsesser = new HashSet<Prosess>();
		getSelectedSubtreeProsess(rootNode, valgteProsesser, null);
		return valgteProsesser;
	}
	public Boolean getIngenProsesser() {
		return ingenProsesser;
	}
	private enum SokModus {HENT_PROSESS, HENT_UNDERPROSESS};
	
	private void getSelectedSubtree(TreeNode treeNode, Set<Long> selectedIds, SokModus sokModus) {
		if(treeNode instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treeNode;
            Object userObject = defaultMutableTreeNode.getUserObject();
            if(userObject instanceof SelectableProsessNode) {
            	SelectableProsessNode selectableProsessNode =(SelectableProsessNode)userObject;
            	if(selectableProsessNode.getSelected()) {
            		Prosess prosess = selectableProsessNode.getProsess();
//            		if (prosess.getEierprosess()==null){
//            			if(sokModus == SokModus.HENT_PROSESS) {
            				selectedIds.add(prosess.getId());
//            			}
//            		} 
//            		else {
//            			if(sokModus == SokModus.HENT_UNDERPROSESS) {
//            				selectedIds.add(prosess.getId());
//            			}
//            		}


            	}
            }
		}
		
		for(int i = 0; i < treeNode.getChildCount(); i++) {
			getSelectedSubtree(treeNode.getChildAt(i), selectedIds, sokModus);
		}
	}
	
	private void getSelectedSubtreeProsess(TreeNode treeNode, Set<Prosess> valgteProsesser, SokModus sokModus) {
		if(treeNode instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treeNode;
            Object userObject = defaultMutableTreeNode.getUserObject();
            if(userObject instanceof SelectableProsessNode) {
            	SelectableProsessNode selectableProsessNode =(SelectableProsessNode)userObject;
            	if(selectableProsessNode.getSelected()) {
            		Prosess prosess = selectableProsessNode.getProsess();
            		valgteProsesser.add(prosess);
            	}
            }
		}
		for(int i = 0; i < treeNode.getChildCount(); i++) {
			getSelectedSubtreeProsess(treeNode.getChildAt(i), valgteProsesser, sokModus);
		}
	}
	public void setSingleSelectionMode(boolean singleSelectionMode) {
		this.singleSelectionMode = singleSelectionMode;
	}

	public boolean isSingleSelectionMode() {
		return singleSelectionMode;
	}
	public void setVelgUnderprosesserAutomatisk(boolean velgUnderprosesserAutomatisk) {
		this.velgUnderprosesserAutomatisk = velgUnderprosesserAutomatisk;
	}

	public boolean isVelgUnderprosesserAutomatisk() {
		return velgUnderprosesserAutomatisk;
	}
	//	private void getSelectedSubtree(TreeNode treeNode, Set<Long> selectedIds) {
//		if(treeNode instanceof DefaultMutableTreeNode) {
//			DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treeNode;
//            Object userObject = defaultMutableTreeNode.getUserObject();
//            if(userObject instanceof SelectableProsessNode) {
//            	SelectableProsessNode selectableProsessNode =(SelectableProsessNode)userObject;
//            	if(selectableProsessNode.getSelected()) {
//            		Prosess prosess = selectableProsessNode.getProsess();
//            		selectedIds.add(prosess.getId());
//            	}
//            }
//		}
//		
//		for(int i = 0; i < treeNode.getChildCount(); i++) {
//			getSelectedSubtree(treeNode.getChildAt(i), selectedIds);
//		}
//	}
	protected class SelectableProsessNode {
		private Prosess prosess;
		private Boolean selected;
		
		public SelectableProsessNode(Prosess prosess) {
			this.prosess = prosess;
			selected = Boolean.FALSE;
		}
		
		public String getText() {
			return prosess.getTextForGUI();
		}

		public Boolean getSelected() {
			return selected;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}

		public Prosess getProsess() {
			return prosess;
		}
	}
	private class CheckBoxNodeRendererComponent extends JCheckBox {
		private DefaultMutableTreeNode treeNode;
		private SelectableProsessNode selectableProsessNode;
		
		public SelectableProsessNode getSelectableProsessNode() {
			return selectableProsessNode;
		}

		public void setSelectableProsessNode(SelectableProsessNode selectableProsessNode) {
			this.selectableProsessNode = selectableProsessNode;
		}

		public DefaultMutableTreeNode getTreeNode() {
			return treeNode;
		}

		public void setTreeNode(DefaultMutableTreeNode treeNode) {
			this.treeNode = treeNode;
		}
	}
	private class CheckBoxNodeRenderer implements TreeCellRenderer {
		private DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
		private CheckBoxNodeRendererComponent chkRenderer = new CheckBoxNodeRendererComponent();
		private Color selectionForeground;
		private Color selectionBackground;
		private Color textForeground;
		private Color textBackground;

		protected CheckBoxNodeRendererComponent getCheckBoxNodeRendererComponent() {
			return chkRenderer;
	  	}

		public CheckBoxNodeRenderer() {
			Font fontValue;
	    	fontValue = UIManager.getFont("Tree.font");
	    	if (fontValue != null) {
	    		chkRenderer.setFont(fontValue);
	    	}
	    	Boolean booleanValue = (Boolean) UIManager.get("Tree.drawsFocusBorderAroundIcon");
	    	chkRenderer.setFocusPainted((booleanValue != null) && (booleanValue.booleanValue()));

	    	selectionForeground = UIManager.getColor("Tree.selectionForeground");
	    	selectionBackground = UIManager.getColor("Tree.selectionBackground");
	    	textForeground = UIManager.getColor("Tree.textForeground");
	    	textBackground = UIManager.getColor("Tree.textBackground");
	  	}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Component returnValue;
			
			String stringValue = tree.convertValueToText(value, selected, expanded, leaf, row, false);
			chkRenderer.setText(stringValue);
			chkRenderer.setSelected(false);

			chkRenderer.setEnabled(tree.isEnabled());

			if (selected) {
				chkRenderer.setForeground(selectionForeground);
				chkRenderer.setBackground(selectionBackground);
			} else {
				chkRenderer.setForeground(textForeground);
				chkRenderer.setBackground(textBackground);
			}

			if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
				Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
				chkRenderer.setTreeNode((DefaultMutableTreeNode) value);
				if (userObject instanceof SelectableProsessNode) {
					SelectableProsessNode node = (SelectableProsessNode) userObject;
					chkRenderer.setSelectableProsessNode(node);
					chkRenderer.setText(node.getText());
					chkRenderer.setSelected(node.getSelected());
					returnValue = chkRenderer;
				} else if(visIngenProsesser) {
					chkRenderer.setSelectableProsessNode(null);
					chkRenderer.setText("Ingen prosesser");
					chkRenderer.setSelected(ingenProsesser);
					returnValue = chkRenderer;
				} else {
					returnValue = defaultRenderer.getTreeCellRendererComponent(tree, stringValue, selected, expanded, leaf, row, hasFocus);
				}
	        } else {
	        	returnValue = defaultRenderer.getTreeCellRendererComponent(tree, stringValue, selected, expanded, leaf, row, hasFocus);
	        }
			
			return returnValue;
		}
	}
	
	private class CheckBoxNodeEditor extends AbstractCellEditor implements TreeCellEditor {
		CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
		JTree tree;

		public CheckBoxNodeEditor(JTree tree) {
			this.tree = tree;
		}

		public Object getCellEditorValue() {
			CheckBoxNodeRendererComponent checkbox = renderer.getCheckBoxNodeRendererComponent();
			checkbox.setEnabled(false);
			SelectableProsessNode checkBoxNode = checkbox.getSelectableProsessNode();
			if(checkBoxNode != null) {
				//Sprer evt valget til underprosesser
				if(isVelgUnderprosesserAutomatisk() && checkBoxNode.getProsess() instanceof Prosess) {
					selectAll(checkbox.getTreeNode(), checkbox.isSelected());
					fireTreeNodesChanged(this, new Object[]{rootNode}, null, null);
				}
				if (singleSelectionMode){
					selectAll(false);
				}
				checkBoxNode.setSelected(checkbox.isSelected());
				nodeSelected(checkBoxNode.getProsess(), checkbox.isSelected());
			} else if(visIngenProsesser) {
				ingenProsesser = checkbox.isSelected();
			}
			checkbox.setEnabled(true);
			return checkBoxNode;
		}

		public boolean isCellEditable(EventObject event) {
			boolean returnValue = false;
			if (event instanceof MouseEvent) {
				MouseEvent mouseEvent = (MouseEvent) event;
				TreePath path = tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());
				if (path != null) {
					Object node = path.getLastPathComponent();
			        if ((node != null) && (node instanceof DefaultMutableTreeNode)) {
			            DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
			            Object userObject = treeNode.getUserObject();
			            returnValue = userObject instanceof SelectableProsessNode;
			            
			            if(treeNode.equals(rootNode) && visIngenProsesser) {
			            	returnValue = true;
			            }
			        }
				}
			}
			return returnValue;
		}

		public Component getTreeCellEditorComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row) {
			Component editor = renderer.getTreeCellRendererComponent(tree, value, true, expanded, leaf, row, true);

			ItemListener itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent itemEvent) {
					if (stopCellEditing()) {
						fireEditingStopped();
					}
				}
			};
			
			if (editor instanceof JCheckBox) {
				((JCheckBox) editor).addItemListener(itemListener);
			}

			return editor;
		}
	}
	
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
		
	}

	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
		
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
		
	}

	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
		
	}

	
}
