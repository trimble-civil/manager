package no.mesta.mipss.ui.picturepanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXImageView;

/**
 * Et panel med thumbnails av bilder som er tilgjengelig
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JThumbPanel extends JPanel implements MipssBeanLoaderListener {
	private static final Logger logger = LoggerFactory.getLogger(JThumbPanel.class);
	private int imageIndex = -1;
	private final MipssPictureModel model;
	private final JScrollPane scroller = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_NEVER,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

	public JThumbPanel(MipssPictureModel model) {
		this.model = model;
		scroller.setBorder(null);
		scroller.setOpaque(true);
		scroller.setBackground(Color.WHITE);
		setOpaque(true);

		if (!model.isLoadingData() && model.size() > 0) {
			initScroller();
		}
		model.addMipssBeanLoaderListener(this);

		setLayout(new BorderLayout());
		add(scroller, BorderLayout.CENTER);
	}

	private String bildeTooltip(Bilde b) {
		String dato = MipssDateFormatter.formatDate(b.getNyDato(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT);
		return dato + " " + b.getFilnavn();
	}

	private double calculateScale(int itemWidth, int itemHeight, int targetWidth, int targetHeight) {
		double scale = 1;

		if (itemWidth > itemHeight) {
			scale = (double) targetWidth / (double) itemWidth;
		} else {
			scale = (double) targetHeight / (double) itemHeight;
		}

		return scale;
	}

	public int getImageIndex() {
		return imageIndex;
	}

	private void initScroller() {
		Box box = Box.createHorizontalBox();
		box.setOpaque(true);
		box.setBackground(Color.WHITE);

		int index = 0;
		for (Bilde b : model.getBilder()) {
			ImageIcon image = new ImageIcon(b.getSmaabildeLob());
			int imageWidth = image.getIconWidth();
			int thumbHeight = getHeight() - 19;
			int thumbWidth = imageWidth / (image.getIconHeight() / thumbHeight);
			Dimension thumbSize = new Dimension(thumbWidth, thumbHeight);
			final JXImageView thumb = new JXImageView();
			thumb.setImage(image.getImage());
			thumb.setScale(calculateScale(image.getIconWidth(), image.getIconHeight(), thumbWidth, thumbHeight));
			thumb.setDragEnabled(false);
			thumb.setEditable(false);
			thumb.setToolTipText(bildeTooltip(b));
			final int thumbIndex = index;
			thumb.addMouseListener(new MouseAdapter() {
				/** {@inheritDoc} */
				@Override
				public void mouseClicked(MouseEvent e) {
					setImageIndex(thumbIndex);
				}
				
				/** {@inheritDoc} */
		        @Override
				public void mouseEntered(MouseEvent e) {
		        	thumb.setScale(thumb.getScale()+0.05);
				}

				/** {@inheritDoc} */
				@Override
				public void mouseExited(MouseEvent e) {
					thumb.setScale(thumb.getScale()-0.05);
					
				}
			});
			ComponentSizeResources.setComponentSize(thumb, thumbSize);
			box.add(thumb);
			index++;
		}

		scroller.setViewportView(box);
	}

	/** {@inheritDoc} */
	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		logger.debug("loadingFinished(" + event + ")");
		initScroller();
	}

	/** {@inheritDoc} */
	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		scroller.setViewportView(null);
	}

	public void setImageIndex(int i) {
		int old = imageIndex;
		imageIndex = i;
		firePropertyChange("imageIndex", old, i);
	}
}
