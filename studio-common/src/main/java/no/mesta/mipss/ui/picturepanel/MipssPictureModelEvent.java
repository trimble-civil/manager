package no.mesta.mipss.ui.picturepanel;

import java.util.EventObject;

/**
 * Event når bilde modellen endrer seg
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssPictureModelEvent extends EventObject {
    public MipssPictureModelEvent(Object source) {
        super(source);
    }
}
