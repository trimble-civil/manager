package no.mesta.mipss.ui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.Box.Filler;

/**
 * Lager box gui komponenter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class BoxUtil {

    /**
     * Lager en horisontal box av komponenter
     * 
     * @param strut
     * @param components
     * @return
     */
    public static Box createHorizontalBox(int strut, Component... components) {
        Box box = Box.createHorizontalBox();
        
        int i = 0;
    	if(strut > 0) {
    		box.add(BoxUtil.createHorizontalStrut(strut));
    	}

    	for(Component c:components) {	
            box.add(c);
            
            if((i > 0 || (i == 0 && components.length == 1)) && strut > 0) {
                box.add(BoxUtil.createHorizontalStrut(strut));
            }
            i++;
        }
        return box;        
    }

    /**
     * Lager en vertikal box av komponenter
     * 
     * @param strut
     * @param components
     * @return
     */
    public static Box createVerticalBox(int strut, Component... components) {
        Box box = Box.createVerticalBox();
        
        int i = 0;
    	if(strut > 0) {
    		box.add(BoxUtil.createVerticalStrut(strut));
    	}
        for(Component c:components) {
            box.add(c);
            
            if((i > 0 || (i == 0 && components.length == 1)) && strut > 0) {
                box.add(BoxUtil.createVerticalStrut(strut));
            }
            i++;
        }
        return box;        
    }
    
    /**
     * Lager en horizontal placeholder
     * 
     * @param width
     * @return
     */
    public static Component createHorizontalStrut(int width) {
    	return new Filler(new Dimension(width,0), new Dimension(width,0), 
  			  new Dimension(width, 0));    	
    }

    /**
     * Lager en vertikal placeholder
     * @param height
     * @return
     */
    public static Component createVerticalStrut(int height) {
    	return new Filler(new Dimension(0,height), new Dimension(0,height), 
    			  new Dimension(0, height));
    }    
}
