package no.mesta.mipss.ui.progressbar;

import java.awt.BorderLayout;
import java.awt.Point;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.ui.JXProgressBar;

/**
 * Panel som viser frem alle kjørende og ventende oppgaver 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProgressPanel extends JPanel{
	private List<ProgressWorkerPanel> uiList;
	private JPanel contentPanel; 
	private JDialog dialog;
	private JXProgressBar ownerBar;
	private boolean visible;
	private Popup popup;
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	
	public ProgressPanel(JXProgressBar ownerBar){
		this.ownerBar = ownerBar;
		setLayout(new BorderLayout());
		uiList = new Vector<ProgressWorkerPanel>();
		contentPanel = new JPanel();
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.PAGE_AXIS));
		add(new JLabel(props.getGuiString("progressPanel.contentPanel.label.default")), BorderLayout.NORTH);
		add(contentPanel);
		contentPanel.setBorder(BorderFactory.createTitledBorder(props.getGuiString("progressPanel.contentPanel.border")));
	}
	
	public void setDialog(JDialog dialog){
		this.dialog = dialog;
	}
	
	public void addProgress(ProgressWorkerPanel w){
		if (popup==null){
			
			Point lr = new Point(0,0);
			SwingUtilities.convertPointToScreen(lr, ownerBar);
			popup = new Popup(0, lr);
			
		}else{
			Point lr = new Point(0,0);
			SwingUtilities.convertPointToScreen(lr, ownerBar);
			popup.setLowerRight(lr);
		}
		uiList.add(w);
		popup.setSize(uiList.size());
		revalidateIt();
	}
	
	/**
	 * Tegner opp panelet på nytt
	 * Fjerner alle elementer og legger til de elementene som ligger i uiList 
	 * for å konstant holde den kjørende oppgaven øverst i panelet.
	 * 
	 */
	private void revalidateIt(){
			contentPanel.removeAll();
			contentPanel.add(Box.createVerticalStrut(10));
			for (ProgressWorkerPanel w:uiList){
				contentPanel.add(w);
			}
			if (uiList.isEmpty()){
				contentPanel.add(new JLabel(props.getGuiString("progressPanel.contentPanel.label.none")));
			}
			contentPanel.add(Box.createVerticalStrut(10));
			contentPanel.add(Box.createVerticalGlue());
			if (dialog!=null){
				dialog.pack();
			}
	}
	
	class Popup {
		private JWindow pop;
		private JLabel nyOppgaveLabel;
		private int displayTime  = 3000;
		private Point lowerRight = new Point(0,0);
		private int mx;
		private int dialogWidth;
		private int dialogHeight;
		Thread animator;
		public Popup(int size, Point lr){
			pop = new JWindow();
			nyOppgaveLabel = new JLabel();
			nyOppgaveLabel.setBorder(BorderFactory.createTitledBorder(props.getGuiString("progressPanel.nyOppgaveLabel.border")));
			setSize(size);
			pop.setAlwaysOnTop(true);
			dialogWidth = 200;
			dialogHeight = 70;
			mx = dialogHeight;
			//pop.setUndecorated(true);
			pop.add(nyOppgaveLabel);
			pop.setSize(dialogWidth, 0);
			pop.setVisible(true);
			nyOppgaveLabel.setOpaque(true);
			this.lowerRight = lr;
		}
		public void setLowerRight(Point lr){
			this.lowerRight = lr;
		}
		public void animate(){
			if (animator==null){
				animator = new Thread("GrowShrink Thread"){
					public void run(){
						int count = grow();
						if (count==-1){
							return;
						}
						if (lowerRight==null) //skal ikke skje.. meeeen....
							lowerRight = new Point(0,0);
						pop.setLocation(lowerRight.x-50, lowerRight.y-mx);
						pop.setSize(dialogWidth, mx);
						pop.setVisible(true);
						try{
							Thread.sleep(displayTime);
						}catch (Exception e){
							return;
						}
						shrink(count);
						pop.setSize(dialogWidth, 0);
						pop.setLocation(lowerRight.x-50, lowerRight.y);
					}
					
					private int grow(){
						int count = 0;
						while (count < mx){
							pop.invalidate();
							pop.setSize(dialogWidth, count);
							pop.setLocation(lowerRight.x-50, lowerRight.y-count);
							count+=2;
							try{
								Thread.sleep(3);
							}catch (Exception e){
								return -1;
							}
						}
						return count;
					}
					
					private void shrink(int count){
						while (count>0){
							try{
								pop.setSize(dialogWidth, count);
								pop.setLocation(lowerRight.x-50, lowerRight.y-count);
							} catch (Throwable e){
								return;
							}
							count-=2;
							try{
								Thread.sleep(3);
							}catch (Exception e){
								return;
							}
						}
					}
				};
				animator.start();
				
			}else{
				animator.interrupt();
				animator = null;
				animate();
			}
		}
		
		public void setSize(int size){
			nyOppgaveLabel.setText(props.getGuiString("progressPanel.nyOppgaveLabel.new", new Object[]{new Integer(size)}));
			animate();
		}
		
	}
	public void removeProgress(ProgressWorkerPanel w){
		uiList.remove(w);
		revalidateIt();
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public List<ProgressWorkerPanel> getUiList(){
		return uiList;
	}
	
}
