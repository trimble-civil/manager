package no.mesta.mipss.ui.table;

import no.mesta.mipss.common.MipssNumberFormatter;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class BoldCellRenderer extends DefaultTableCellRenderer {

    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if (renderer instanceof JLabel) {
        	JLabel label = (JLabel) renderer;
        	Font f = label.getFont();
        	Font bold = new Font(f.getName(), Font.BOLD, f.getSize());
        	label.setFont(bold);
        	label.setHorizontalAlignment(JLabel.RIGHT);
        }
        
        return this;
    }
    
    @Override
    protected void setValue(Object value) {
    	if ((value != null) && (value instanceof Number)) {
            value = MipssNumberFormatter.formatNumber((Number) value);
        }
        super.setValue(value);
    }

}