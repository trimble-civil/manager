package no.mesta.mipss.ui.editablelist;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.swingbinding.JListBinding;
import org.jdesktop.swingbinding.SwingBindings;


/**
 * Viser en liste over verdier. Denne listen er redigerbar
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JEditableList<C extends IRenderableMipssEntity> extends JComponent {
    private static final Logger logger = LoggerFactory.getLogger(JEditableList.class);
    private JList list;
    private ObservableList<C> values;
    private Object instance;
    private String valuesProperty;
    private IItemDialogue<C> dialogue;
    private ListCellRenderer itemRenderer;
    private FieldToolkit toolkit;
    private List<SmallButton> buttons = new ArrayList<SmallButton>();
    private boolean uniqueItems = true;
    private JListBinding<C, List<C>, JList> lb;
    private boolean addSupport = true;
    private boolean removeSupport = true;
    
    /**
     * Konstruktør
     * 
     * @param toolkit sørger for at programmet vet tilstanden til instansen som
     * det redigeres på
     * @param instance instansen som det redigeres på
     * @param valuesProperty verdien på instansen som det redigers på
     * @param dialogue dialogen som skal vises når det klikkes på +
     * @param itemRenderer ListCellRenderer som har ansvar for å vise verdiene
     * i listen
     */
    public JEditableList(FieldToolkit toolkit, PropertyChangeSource instance, String valuesProperty, IItemDialogue<C> dialogue, ListCellRenderer itemRenderer) {
        this(toolkit, null, instance, valuesProperty, dialogue, itemRenderer);
    }
    
    /**
     * Konstruktør
     * 
     * @param toolkit sørger for at programmet vet tilstanden til instansen som
     * det redigeres på
     * @param title overskriften til listen
     * @param instance instansen som det redigeres på
     * @param valuesProperty verdien på instansen som det redigers på
     * @param dialogue dialogen som skal vises når det klikkes på +
     * @param itemRenderer ListCellRenderer som har ansvar for å vise verdiene
     * i listen
     */
    public JEditableList(FieldToolkit toolkit, String title, PropertyChangeSource instance, String valuesProperty, IItemDialogue<C> dialogue, ListCellRenderer itemRenderer) {
        this(toolkit, title, instance, valuesProperty, dialogue, itemRenderer, true);
    }
    
    /**
     * Konstruktør
     * 
     * @param toolkit sørger for at programmet vet tilstanden til instansen som
     * det redigeres på
     * @param title overskriften til listen
     * @param instance instansen som det redigeres på
     * @param valuesProperty verdien på instansen som det redigers på
     * @param dialogue dialogen som skal vises når det klikkes på +
     * @param itemRenderer ListCellRenderer som har ansvar for å vise verdiene
     * i listen
     * @param unique sørger for at det kun er unike verdier i listen
     */
    public JEditableList(FieldToolkit toolkit, String title, PropertyChangeSource instance, String valuesProperty, IItemDialogue<C> dialogue, ListCellRenderer itemRenderer, boolean unique) {
        this(toolkit, title, instance, valuesProperty, dialogue, itemRenderer, unique, true, true);
    }

    /**
     * Konstruktør
     * 
     * @param toolkit sørger for at programmet vet tilstanden til instansen som
     * det redigeres på
     * @param title overskriften til listen
     * @param instance instansen som det redigeres på
     * @param valuesProperty verdien på instansen som det redigers på
     * @param dialogue dialogen som skal vises når det klikkes på +
     * @param itemRenderer ListCellRenderer som har ansvar for å vise verdiene
     * i listen
     * @param unique sørger for at det kun er unike verdier i listen
     */
    public JEditableList(final FieldToolkit toolkit, final String title, final PropertyChangeSource instance, final String valuesProperty, final IItemDialogue<C> dialogue, final ListCellRenderer itemRenderer, final boolean unique, final boolean addSupport, final boolean removeSupport) {
        this.instance = instance;
        this.valuesProperty = valuesProperty;
        this.dialogue = dialogue;
        this.itemRenderer = itemRenderer;
        this.toolkit = toolkit;
        this.uniqueItems = unique;
        this.addSupport = addSupport;
        this.removeSupport = removeSupport;
        
        setTitle(title);

        makeValuesObservable();

        setBackground(Color.WHITE);
        setLayout(new BoxLayout((Container) this, BoxLayout.PAGE_AXIS));        
        add(createAndSetupList());
        add(Box.createVerticalStrut(5));
        add(createAndSetupButtons());
        add(Box.createVerticalGlue());
        
        toolkit.registerCangeListener(instance.getClass(), instance, valuesProperty);        
    }
    
    /**
     * Binder på nytt
     * 
     */
    public void reset() {
        toolkit.unbindAndRemove(lb);
        makeValuesObservable();
        lb = SwingBindings.createJListBinding(AutoBinding.UpdateStrategy.READ, values, list);
        toolkit.bindAndAdd(lb);
    }
    
    /**
     * Setter tittelen på listen
     * 
     * @param title
     */
    public void setTitle(String title) {
        if(title != null) {        
            setBorder(BorderResources.createComponentTitleBorder(title));
        } else {
            setBorder(BorderResources.createComponentBorder());
        }
    }
    
    /**
     * Sørger for at listen kan observeres
     * 
     */
    @SuppressWarnings("unchecked")
	private void makeValuesObservable() {
        BeanProperty property = BeanProperty.create(valuesProperty);
        List<C> originalValues = (List<C>) property.getValue(instance);
        if(!(originalValues instanceof ObservableList)) {
            values = new MipssObservableList<C>(originalValues);
            property.setValue(instance, values);
        } else {
            values = (ObservableList<C>) originalValues;
        }
    }
    
    /**
     * Lager listen som vises
     * 
     * @return
     */
    private Component createAndSetupList() {
        list = new JList();
        list.setFocusable(false); // TODO: Finn bedre hack for å unngå Exception ved fjerning av elementer
        ComponentSizeResources.setComponentSizes(list);
        
        list.setCellRenderer(itemRenderer);

        lb = SwingBindings.createJListBinding(AutoBinding.UpdateStrategy.READ, values, list);
        
        toolkit.addBinding(lb);
        
        return new JScrollPane(list);
    }
    
    /**
     * Lagger + og - knappene
     * 
     * @return
     */
    private Box createAndSetupButtons() {
        Box buttons = Box.createHorizontalBox();
        buttons.add(BoxUtil.createHorizontalStrut(3));
        if(addSupport) {
        	buttons.add(new SmallButton(new AddAction()));
        }
        buttons.add(BoxUtil.createHorizontalStrut(10));
        if(removeSupport) {
        	buttons.add(new SmallButton(new RemoveAction()));
        }
        buttons.add(Box.createHorizontalGlue());
        buttons.setOpaque(false);
        return buttons;        
    }

    /** {@inheritDoc} */
    @Override
    public void setEnabled(boolean f) {
        super.setEnabled(f);
        list.setEnabled(f);
        for(SmallButton b:buttons) {
            b.setEnabled(f);
        }
    }
    
    
    /**
     * Åpner en dialog hvor brukeren kan legge til et estimat
     * 
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    class AddAction extends AbstractAction {

        public AddAction() {
            super("Legg til", IconResources.ADD_ITEM_ICON);
        }
        
        /** {@inheritDoc} */
        public void actionPerformed(ActionEvent e) {
            dialogue.doDialogue((JEditableList<C>) JEditableList.this);
            logger.debug("add button(" + dialogue.isCancelled() + "): " + dialogue.getNewValue());
            if(!dialogue.isCancelled()) {
                C c = dialogue.getNewValue();
                
                if(c != null) {
                    if(uniqueItems) {
                        if(values.contains(c)) {
                            int i = values.indexOf(c);
                            values.set(i, c);
                        } else {
                            values.add(c);
                        }
                    } else {
                        values.add(c);
                    }
                }
            }
            
            for(C c:values) {
                logger.debug("actionPerformed: {}", c);
            }
        }
    }

    /**
     * Fjerner et estimat fra listen
     * 
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    class RemoveAction extends AbstractAction {
    
        /**
         * Konstruktør
         */
        public RemoveAction() {
            super("Fjern", IconResources.REMOVE_ITEM_ICON);            
        }

        /** {@inheritDoc} */
        @SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent e) {
            C c = (C) list.getSelectedValue();
            list.clearSelection();
            
            if(c != null) {
                values.remove(c);
            }
        }
    }
    
    /**
     * Lager en liten knapp basert på en Action.
     * Benytter navnet i Action som tooltip, og fjerner text på knappen
     * 
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    class SmallButton extends JButton {
        
        /**
         * Konstruktør
         * 
         * @param a Action knappen skal baseres på
         */
        public SmallButton(Action a) {
            super(a);
            buttons.add(this);
            setText(null);
            setToolTipText((String) a.getValue(Action.NAME));
            setBorder(null);
            setFocusable(false);
            setOpaque(false);
        }
    }
}
