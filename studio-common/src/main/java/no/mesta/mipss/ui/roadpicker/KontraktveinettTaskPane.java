package no.mesta.mipss.ui.roadpicker;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.persistence.veinett.VeinettveireferanseHPVO;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.VeireferanseTableObject;
import no.mesta.mipss.ui.taskpane.LinkLabel;
import no.mesta.mipss.ui.taskpane.MipssTaskPane;
import no.mesta.mipss.ui.taskpane.MipssTaskPaneContainer;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class KontraktveinettTaskPane extends JScrollPane{

	private MipssTaskPaneContainer taskPaneContainer = new MipssTaskPaneContainer();
	private List<Veinettveireferanse> veinett;

	private Map<String, List<Veinettveireferanse>> veireferanser;
	private Map<String, MipssBeanTableModel<VeireferanseTableObject>> veiModels;

	private final boolean inkluderKm;
	
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private List<JCheckBox> alleCheckBoxList = new ArrayList<JCheckBox>();
	private final boolean showLink;
	private final boolean editableKm;

	
	public KontraktveinettTaskPane(boolean inkluderKm, boolean showLink, boolean editableKm){
		this.inkluderKm = inkluderKm;
		this.showLink = showLink;
		this.editableKm = editableKm;
		busyLabel.setText("Henter veinett...");
		veiModels = new HashMap<String, MipssBeanTableModel<VeireferanseTableObject>>();
	}
	
	public void setVeinett(List<Veinettveireferanse>  veinett){
		this.veinett=veinett;
		initVeireferanser();
		initGui();
	}
	
	/**
	 * Fordeler veireferansene på veikategori og sorterer dem på veinummer og hp
	 */
	private void initVeireferanser(){
		veireferanser = new HashMap<String, List<Veinettveireferanse>>();
		
		for (Veinettveireferanse vr:veinett){
			Veinettveireferanse v = inkluderKm?vr:createVeiref(vr);
			List<Veinettveireferanse> list = veireferanser.get(vr.getVeikategori().toUpperCase()+vr.getVeistatus().toUpperCase());
			if (list==null){
				list = new ArrayList<Veinettveireferanse>();
				veireferanser.put(vr.getVeikategori().toUpperCase()+vr.getVeistatus().toUpperCase(), list);
			}
			if (!list.contains(v)){
				list.add(v);
			}
		}
		for (String key:veireferanser.keySet()){
			List<Veinettveireferanse> list = veireferanser.get(key);
			Collections.sort(list, new Comparator<Veinettveireferanse>(){
				@Override
				public int compare(Veinettveireferanse o1, Veinettveireferanse o2) {
					if (o1.getVeinummer().equals(o2.getVeinummer())){
						if (o1.getHp().equals(o2.getHp())){
							if (o1.getFraKm()!=null&&o2.getFraKm()!=null)
								return o1.getFraKm()>o2.getFraKm()?1:0;
						}
						return o1.getHp()>o2.getHp()?1:0;
					}
					return o1.getVeinummer()>o2.getVeinummer()?1:0;
				}
			});
		}
	}
	/**
	 * Forhåndsvelg disse veireferansene, kun hvis de ligger i en av veimodellene. De som ikke er tilstede blir ignorert.
	 * 
	 * @param veiListe
	 */
	public void setSelectedVeireferanser(List<Veinettveireferanse> veiListe){
		for (String key:veiModels.keySet()){
			for (VeireferanseTableObject o:veiModels.get(key).getEntities()){
				if (veiListe.contains(o.getVeireferanse())){
					o.setValgt(Boolean.TRUE);
				}else{
					o.setValgt(Boolean.FALSE);
				}
			}
		}
		repaint();
	}
	/**
	 * Henter de valgte veireferansene fra gui
	 * @return
	 */
	public List<Veinettveireferanse> getValgte(){
		List<Veinettveireferanse> veiList = new ArrayList<Veinettveireferanse>();
		for (String key:veiModels.keySet()){
			for (VeireferanseTableObject o:veiModels.get(key).getEntities()){
				if (o.getValgt()){
					veiList.add(o.getVeireferanse());
				}
			}
		}
		return veiList;
	}
	
	
	/**
	 * Lager en veiref av en veinettveireferanse (annen equals metode enn veinettveireferanse)
	 * @param v
	 * @return
	 */
	private VeinettveireferanseHPVO createVeiref(Veinettveireferanse v){
		VeinettveireferanseHPVO vr = new VeinettveireferanseHPVO();
		vr.setFylkesnummer(v.getFylkesnummer());
		vr.setKommunenummer(v.getKommunenummer());
		vr.setVeistatus(v.getVeistatus());
		vr.setVeikategori(v.getVeikategori());
		vr.setVeinummer(v.getVeinummer());
		vr.setHp(v.getHp());
		return vr;
	}
	/**
	 * Lag guikomponenter
	 */
	private void initGui() {
//		setLayout(new GridBagLayout());
		
		Set<String> keySet = veireferanser.keySet();
		List<String> keys = new ArrayList<String>(keySet);
		Collections.sort(keys);
//		JCheckBox hp = new JCheckBox("HP");
//		JCheckBox km = new JCheckBox("KM");
//		JLabel label = new JLabel("Velg detaljnivå :");
//		JLabel periode = new JLabel("Tidsperiode:");
//		JMipssDatePicker fraDato = new JMipssDatePicker(CalendarUtil.getDaysAgo(Clock.now(), 30));
//		JMipssDatePicker tilDato =new JMipssDatePicker(Clock.now());
//		hp.setOpaque(false);
//		km.setOpaque(false);
//		JPanel panel = new JPanel(new GridBagLayout());
//		panel.setOpaque(false);
//		panel.add(label, new GridBagConstraints(0,0,1,1,0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
//		panel.add(hp, new GridBagConstraints(1,0,1,1,0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
//		panel.add(km, new GridBagConstraints(2,0,1,1,0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
//		panel.add(fraDato, new GridBagConstraints(3,0,1,1,0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
//		panel.add(tilDato, new GridBagConstraints(4,0,1,1,1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
//		taskPaneContainer.add(panel);
		taskPaneContainer.removeAllTaskPanes();
		for (String s:keys){
			taskPaneContainer.addTaskPane(createTaskPane(s, veireferanser.get(s), s));
		}
//		taskPaneContainer.collapseAll(true);
//		selectAll();
//		JPanel panel = new JPanel(new GridBagLayout());
//		panel.add(taskPaneContainer, new GridBagConstraints(0,0,1,1,1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		getVerticalScrollBar().setUnitIncrement(16);
	}
	
	/**
	 * Lager en task pane, legger på en checkbox og en tabell med veinettet
	 * @param name
	 * @param v
	 * @param key
	 * @return
	 */
	private MipssTaskPane createTaskPane(String name, List<Veinettveireferanse> v, String key){
		final MipssTaskPane pane = new MipssTaskPane();
		JLabel lblTitle = new JLabel(name);
		Font font = lblTitle.getFont();
		font = new Font(font.getFontName(), Font.BOLD, font.getSize()+3);
		lblTitle.setFont(font);
		pane.setTitleLeftComponent(lblTitle);
		final JCheckBox alleCheck = new JCheckBox("Alle valgt");
		alleCheck.setOpaque(false);
		
		LinkLabel label = new LinkLabel("Velg vei", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (alleCheck.isSelected()){
					alleCheck.doClick();
				}
				pane.setCollapsed(false);
				
			}
		});
		alleCheckBoxList.add(alleCheck);
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.LINE_AXIS));
		titlePanel.add(Box.createHorizontalStrut(30));
		titlePanel.add(alleCheck);
		titlePanel.add(Box.createHorizontalGlue());
		if (showLink)
			titlePanel.add(label);
		pane.setTitleContent(titlePanel);
		JScrollPane scr = new JScrollPane(createTable(v, alleCheck, key));
		pane.setContent(scr);
		return pane;
	}
	
	public void selectAll(){
		for (JCheckBox b:alleCheckBoxList){
			b.doClick();
		}
	}
	
	public void collapseAll(){
		taskPaneContainer.collapseAll(true);
	}
	
	/**
	 * Lager en check-tabell av veinettet
	 * @param veilist
	 * @param alleCheck
	 * @param key
	 * @return
	 */
	private JCheckTablePanel<VeireferanseTableObject> createTable(List<Veinettveireferanse> veilist, JCheckBox alleCheck, String key){
		List<VeireferanseTableObject> veiListTable =new ArrayList<VeireferanseTableObject>();
		for (Veinettveireferanse v:veilist){
			veiListTable.add(new VeireferanseTableObject(v));
		}
		List<String> properties = new ArrayList<String>();
		properties.add("valgt");
		properties.add("fylkenummer");
		properties.add("kommunenummer");
		properties.add("veikategori");
		properties.add("veistatus");
		properties.add("veinummer");
		properties.add("hp");
		if (inkluderKm){
			properties.add("fraKm");
			properties.add("tilKm");
		}
		int[] editableColumns = new int[inkluderKm&&editableKm?3:1];
		editableColumns[0]=0;
		if (inkluderKm&editableKm){
			editableColumns[1]=7;
			editableColumns[2]=8;
		}
		MipssBeanTableModel<VeireferanseTableObject> model = new MipssBeanTableModel<VeireferanseTableObject>(VeireferanseTableObject.class, editableColumns, 
				properties.toArray(new String[]{}));
		model.setEntities(veiListTable);
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(VeireferanseTableObject.class, model.getColumns());
		
		JMipssBeanTable<VeireferanseTableObject> table = new JMipssBeanTable<VeireferanseTableObject>(model, colModel);
		table.setDoWidthHacks(false);
		table.setFillsViewportWidth(true);
		table.setPreferredScrollableViewportSize(new Dimension(200,130));
		table.getColumn(0).setPreferredWidth(15);
		table.getColumn(1).setPreferredWidth(15);
		table.getColumn(2).setPreferredWidth(15);
		table.getColumn(3).setPreferredWidth(15);
		table.getColumn(4).setPreferredWidth(15);
		table.getColumn(5).setPreferredWidth(15);
		table.getColumn(6).setPreferredWidth(15);
		if (inkluderKm){
			table.getColumn(7).setPreferredWidth(15);
			table.getColumn(8).setPreferredWidth(15);
		}
		veiModels.put(key, model);
		return new JCheckTablePanel<VeireferanseTableObject>(table, model, alleCheck, "Henter kontraktveinett", false);
	}
	public void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		taskPaneContainer.setVisible(!busy);
		this.setViewportView(busy?busyLabel:taskPaneContainer);
		repaint();
	}
}
