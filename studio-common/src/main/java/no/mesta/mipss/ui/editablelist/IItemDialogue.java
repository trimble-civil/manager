package no.mesta.mipss.ui.editablelist;

import javax.swing.JComponent;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

/**
 * Metoder dialoger til en redigerbar liste må støtte. Dialogene må være modal.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface IItemDialogue<I extends IRenderableMipssEntity> {

	/**
	 * Modal visning av dialogen
	 * 
	 * @param list
	 * @return
	 */
	public void doDialogue(JComponent list);

	/**
	 * Etter at dialogen er kjørt vil listen hente verdien brukeren lagde/valgte
	 * via denne metoden. Skal ikke returnere null
	 * 
	 * @return
	 */
	public I getNewValue();

	/**
	 * Hvis brukeren avbrøt dialogen skal denne returnerer true slik at listen
	 * kan ignorere getNewValue
	 * 
	 * @return
	 */
	public boolean isCancelled();
}
