package no.mesta.mipss.ui.picturepanel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

/**
 * Standard implementasjon av en bilde modell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssDefaultPictureModel implements MipssPictureModel {
	private static final Logger logger = LoggerFactory.getLogger(MipssDefaultPictureModel.class);
	private Object beanInstance;
	private Class<?> beanInterface;
	private String beanMethod;
	private Class<?>[] beanMethodArguments;
	private Object[] beanMethodArgValues;
	private List<Bilde> bilder = new ArrayList<Bilde>();
	private boolean errors;
	private List<MipssBeanLoaderListener> loaderListeners = new ArrayList<MipssBeanLoaderListener>();
	private boolean loadingData = false;
	private Method method;
	private List<MipssPictureModelListener> pictureListeners = new ArrayList<MipssPictureModelListener>();

	/**
	 * Konstruktør
	 * 
	 */
	public MipssDefaultPictureModel() {

	}

	/**
	 * Konstruktør
	 * 
	 * @param bilder
	 */
	public MipssDefaultPictureModel(List<Bilde> bilder) {
		if (bilder != null) {
			this.bilder = bilder;
		}
	}

	/** {@inheritDoc} */
	public void addBilde(Bilde b) {
		synchronized (bilder) {
			bilder.add(b);
			fireModelChanged(new MipssPictureModelEvent(this));
		}
	}

	/** {@inheritDoc} */
	public void addMipssBeanLoaderListener(MipssBeanLoaderListener l) {
		loaderListeners.add(l);
	}

	/** {@inheritDoc} */
	public void addMipssPictureModelListener(MipssPictureModelListener l) {
		pictureListeners.add(l);
	}

	/**
	 * Konstruerer et metodeobjekt som kan brukes til å laste data
	 * 
	 * @return Metode instansen for gitt <code>beanInteface</code>(interface),
	 *         <code>beanMethod</code>(navn) og <code>beanMethodArguments</code>
	 * @throws java.lang.IllegalStateException
	 *             hvis interface eller metode ikke er satt
	 * @throws java.lang.IllegalArgumentException
	 *             hvis interface ikke har metoden
	 * @see #setBeanMethodArguments(Class[])
	 * @see #setBeanMethod(String)
	 * @see #setBeanInterface(Class)
	 */
	private Method constructMethod() {
		if (beanInterface == null) {
			throw new IllegalStateException("No beanInterface set!");
		}

		if (beanMethod == null) {
			throw new IllegalStateException("No beanMethod set!");
		}

		try {
			method = beanInterface.getMethod(beanMethod, beanMethodArguments);
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException("No such method!", e);
		}

		return method;
	}

	/**
	 * Notifiserer alle lyttere om at loading er ferdig
	 * 
	 * @param e
	 */
	private void fireLoadingFinishedEvent(MipssBeanLoaderEvent e) {
		for (MipssBeanLoaderListener l : loaderListeners) {
			l.loadingFinished(e);
		}
	}

	/**
	 * Notifiserer alle lyttere om at loading har startet
	 * 
	 * @param e
	 */
	private void fireLoadingStartedEvent(MipssBeanLoaderEvent e) {
		for (MipssBeanLoaderListener l : loaderListeners) {
			l.loadingStarted(e);
		}
	}

	/**
	 * Notifisere om at modellen er endret
	 * 
	 * @param e
	 */
	private void fireModelChanged(MipssPictureModelEvent e) {
		for (MipssPictureModelListener l : pictureListeners) {
			l.modelChanged(e);
		}
	}

	/** {@inheritDoc} */
	public Bilde getBilde(int index) {
		return bilder.get(index);
	}

	/** {@inheritDoc} */
	public int getBildeIndex(Bilde b) {
		return bilder.indexOf(b);
	}

	/** {@inheritDoc} */
	public List<Bilde> getBilder() {
		return bilder;
	}

	public boolean isErrors() {
		return errors;
	}

	/** {@inheritDoc} */
	public boolean isLoadingData() {
		return loadingData;
	}

	/**
	 * Laster bilder til modellen. Kan også kaste exceptions kastet av
	 * <code>constructMethod()</code>
	 * 
	 * @throws java.lang.IllegalStateException
	 *             hvis beanInstance ikke er satt
	 * @see java.lang.reflect.InvocationTargetException
	 * @see java.lang.IllegalAccessException
	 * @see #constructMethod()
	 * @see #setBeanInstance(Object)
	 */
	public void loadBilder() {
		if (method == null) {
			method = constructMethod();
		}

		if (beanInstance == null) {
			throw new IllegalStateException("No beanInstance set!");
		}

		SwingWorker<List<Bilde>, Void> worker = new SwingWorker<List<Bilde>, Void>() {

			@SuppressWarnings("unchecked")
			protected List<Bilde> doInBackground() {
				errors = false;
				List<Bilde> bilder;
				try {
					bilder = (List<Bilde>) method.invoke(beanInstance, beanMethodArgValues);
				} catch (IllegalAccessException e) {
					errors = true;
					throw new IllegalArgumentException("No access to method!", e);
				} catch (InvocationTargetException e) {
					errors = true;
					throw new IllegalArgumentException("Method threw exception!", e);
				} catch (Throwable t) {
					errors = true;
					throw new IllegalStateException("Exception occured", t);
				}

				logger.debug("Found " + (bilder == null ? "null" : String.valueOf(bilder.size())) + " pictures");
				setBilder(bilder);
				return bilder;
			}

			@Override
			protected void done() {
				loadingDone();
			}
		};

		fireLoadingStartedEvent(new MipssBeanLoaderEvent(this, false));
		loadingData = true;
		worker.execute();
	}

	/**
	 * Kalles av tråden som laster data når den er ferdig
	 * 
	 */
	protected void loadingDone() {
		loadingData = false;
		fireLoadingFinishedEvent(new MipssBeanLoaderEvent(this, false));
	}

	/** {@inheritDoc} */
	public void removeBilde(Bilde b) {
		synchronized (bilder) {
			bilder.remove(b);
			fireModelChanged(new MipssPictureModelEvent(this));
		}
	}

	/** {@inheritDoc} */
	public void removeMipssBeanLoaderListener(MipssBeanLoaderListener l) {
		loaderListeners.remove(l);
	}

	/** {@inheritDoc} */
	public void removeMipssPictureModelListener(MipssPictureModelListener l) {
		pictureListeners.remove(l);
	}

	/**
	 * Setter bønne instansen som har metoden som laster dataene
	 * 
	 * @param beanInstance
	 */
	public void setBeanInstance(Object beanInstance) {
		this.beanInstance = beanInstance;
	}

	/**
	 * Interface for bønna som laster data
	 * 
	 * @param beanInteface
	 */
	public void setBeanInterface(Class<?> beanInteface) {
		this.beanInterface = beanInteface;
	}

	/**
	 * Navnet på bønne metoden
	 * 
	 * @param beanMethod
	 */
	public void setBeanMethod(String beanMethod) {
		this.beanMethod = beanMethod;
	}

	/**
	 * Setter argumenttypene for metode som laster data
	 * 
	 * @param beanMethodArguments
	 */
	public void setBeanMethodArguments(Class<?>[] beanMethodArguments) {
		this.beanMethodArguments = beanMethodArguments;
	}

	/**
	 * Setter metode argument verdiene
	 * 
	 * @param beanMethodArgValues
	 */
	public void setBeanMethodArgValues(Object[] beanMethodArgValues) {
		this.beanMethodArgValues = beanMethodArgValues;
	}

	/** {@inheritDoc} */
	public void setBilder(List<Bilde> bilder) {
		synchronized (this.bilder) {
			this.bilder = bilder;
			fireModelChanged(new MipssPictureModelEvent(this));
		}
	}

	/** {@inheritDoc} */
	public int size() {
		return bilder.size();
	}
}
