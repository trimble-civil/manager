package no.mesta.mipss.ui.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modell for MIPSS entiteter
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class MipssRenderableEntityModel<T extends IRenderableMipssEntity> implements ListModel{
    private List<ListDataListener> dataListeners = null;
    private List<T> entities = null;
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    /**
     * Lager en modell med listen over entiteter
     * 
     * @param entities
     */
    public MipssRenderableEntityModel(List<T> entities) {
        this.entities = entities;
        dataListeners = new ArrayList<ListDataListener>();
        sort();
    }

    /**
     * Legger til et element i modellen
     * 
     * @param element
     */
    public void addElement(T element) {
        List<T> l = new ArrayList<T>();
        l.add(element);
        addElements(l);
        sort();
    }
    private void sort(){
    	Collections.sort(entities, new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				return o1.getTextForGUI().compareTo(o2.getTextForGUI());
			}
		});
    }

    /**
     * Legger til en liste av elementer i modellen
     * 
     * @param elements
     */
    public void addElements(List<T> elements) {
        synchronized(entities) {
            int sizeBefore = entities.size();
            entities.addAll(elements);
            int sizeAfter = entities.size();
            fireContentChangedEvent(Math.max(sizeBefore, sizeAfter) - 1);
            log.debug("addElements(elements), before: " + sizeBefore + " , after: " + sizeAfter);
            sort();
        }
    }

    /**
     * Legger til en lytter paa modellen
     * 
     * @param l
     */
    public void addListDataListener(ListDataListener l) {
        dataListeners.add(l);
    }

    /**
     * Metode som benyttes til å notifisere lytterne naar innholdet endrer seg
     * 
     * @param object
     * @param size
     */
    private void fireContentChangedEvent(int size) {
        ListDataEvent event = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, size);
        for(ListDataListener l:dataListeners) {
            l.contentsChanged(event);
        }
    }
    
    /**
     * Returnerer en entitet paa en gitt plass i modellen
     * 
     * @param index
     * @return
     */
    public T getElementAt(int index) {
        return entities.get(index);
    }
    
    /**
     * Henter alle elementene som en liste fra modellen (kopi)
     * 
     * @return
     */
    public List<T> getElements() {
        List<T> elements = new ArrayList<T>(entities);

        return elements;
    }
    
    /**
     * Den opprinnelige listen med entiteter
     * 
     * @return
     */
    public List<T> getEntities() {
    	return entities;
    }
    
    /**
     * Returnerer antall entiteter i modellen
     * 
     * @return
     */
    public int getSize() {
        return (entities != null ? entities.size() : 0);
    }
    
    /**
     * Fjerner en liste med elementer fra modellen
     * 
     * @param elements
     */
    public void removeElements(List<T> elements) {
        synchronized(entities) {
            int sizeBefore = entities.size();
            boolean removed = entities.removeAll(elements);
            if(removed) {
                int sizeAfter = entities.size();
                fireContentChangedEvent(Math.max(sizeBefore, sizeAfter) - 1);
                log.debug("removeElements(elements), before: " + sizeBefore + " , after: " + sizeAfter);
                sort();
            }
            
        }
    }
    
    /**
     * Fjerner et element fra modellen, og notifiserer lytterene
     * 
     * @param item
     */
    public void removeItem(T item) {
        if(null == item || null == entities) {
            return;
        }
        
        synchronized(entities) {
            //int index = entities.indexOf(item);
            
            int sizeBefore = entities.size();
            boolean removed = entities.remove(item);
            if(removed) {
                int sizeAfter = entities.size();
                fireContentChangedEvent(Math.max(sizeBefore, sizeAfter) - 1);
                sort();
            }
        }
    }
    
    public void removeAll(){
    	synchronized (entities) {
    		entities.clear();
    		fireContentChangedEvent(0);
		}
    }
    /**
     * Fjerner en lytter fra modellen
     * 
     * @param l
     */
    public void removeListDataListener(ListDataListener l) {
        dataListeners.remove(l);
    }
}
