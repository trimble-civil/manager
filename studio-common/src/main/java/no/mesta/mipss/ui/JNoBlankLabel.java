package no.mesta.mipss.ui;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JLabel;

import no.mesta.mipss.core.IBlankFieldValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Label som blir gir rød tekst hvis validatoren sier at feltet er blankt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 * @param <T>
 */
public class JNoBlankLabel<T> extends JLabel implements PropertyChangeListener {
	private static final Logger logger = LoggerFactory.getLogger(JNoBlankLabel.class);
	private final IBlankFieldValidator<T> blankValidator;

	public JNoBlankLabel(final String txt, final IBlankFieldValidator<T> blankValidator) {
		super(txt);
		blankValidator.addPropertyChangeListener("blank", this);
		if (blankValidator.isBlank()) {
			warn();
		} else {
			ok();
		}
		this.blankValidator = blankValidator;
	}

	public IBlankFieldValidator<T> getValidator() {
		return blankValidator;
	}

	public boolean isOk() {
		return Color.BLACK.equals(getForeground());
	}

	public boolean isWarning() {
		return Color.RED.equals(getForeground());
	}

	public void ok() {
		logger.trace(getText() + " OK");
		boolean old = isOk();
		setForeground(Color.BLACK);
		firePropertyChange("ok", old, isOk());
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		logger.debug("propertyChange(" + evt + ") start");
		Boolean b = (Boolean) evt.getNewValue();
		if (Boolean.TRUE.equals(b)) {
			setForeground(Color.RED);
		} else {
			setForeground(Color.BLACK);
		}
	}

	public void warn() {
		logger.trace(getText() + " WARN");

		boolean old = isWarning();
		setForeground(Color.RED);
		firePropertyChange("warning", old, isWarning());
	}
}
