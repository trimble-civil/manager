package no.mesta.mipss.ui.tablerowtooltip;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdesktop.swingx.JXGlassBox;
import org.jdesktop.swingx.border.DropShadowBorder;


/**
 * Venter 2 sekunder, og hvis den fortsatt lever etter to sekunder så viser
 * den et tooltip
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JTableRowTooltipCountDown<T,V> extends SwingWorker<Void, Void> {
    private Logger logger = LoggerFactory.getLogger(JTableRowTooltipCountDown.class);
    @SuppressWarnings("unchecked")
	private TableMouseListener listener = null;
    private int row = -1;
    private Point point = null;
    private boolean killed = false;
    @SuppressWarnings("unchecked")
	private JMipssBeanTable source = null;
    private Class<?> viewClass = null;
    private Class<?> rowClass  = null;
    private long when = 0;

    /**
     * Konstruktør
     * 
     * @param l lytteren som styrer livet til tooltip nedtelleren
     * @param r raden som skal vises
     * @param p punktet hvor den skal vises
     * @param source
     */
    @SuppressWarnings("unchecked")
	public JTableRowTooltipCountDown(TableMouseListener l, int r, Point p, JMipssBeanTable source, Class<?> viewClass, Class<?> rowClass) {
        this.source = source;
        this.viewClass = viewClass;
        this.rowClass = rowClass;
        trace(":created");
        listener = l;
        row = r;
        point = p;
    }

    /** {@inheritDoc} */
    @Override
    protected Void doInBackground() {
        trace(":started");
        when = System.currentTimeMillis();
        long now = -1;
        do {
            try{
                Thread.yield();
            } catch (Exception e) {
                e = null;
            }
            now = System.currentTimeMillis();
        } while (((when + 2000L) > now) && !killed);

        if (killed || row == -1) {
            trace(":kill intercepted, or invalid row");
            return null;
        }

        // Finn glass
        JRootPane root = source.getRootPane();
        final Container glassPane = (Container)root.getGlassPane();

        // Åpne tooltip
        final JXGlassBox box = new JXGlassBox();
        T view = null;
        try {
            view = constructView();
        } catch (Exception e) {
            logger.error("Error constructing view", e);
            throw new IllegalArgumentException("View construct failed", e);
        }
        box.setLayout(new BorderLayout());
        box.add((Component) view, BorderLayout.CENTER);
        box.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));
        
        Point p = SwingUtilities.convertPoint(source, point.x, point.y, null);
        int x = p.x, y = p.y;
        box.showOnGlassPane(glassPane, x, y);
        int w = box.getWidth(), h = box.getHeight();
        Point o = new Point(glassPane.getWidth(), glassPane.getHeight());
        x = x - (w/2);
        y = y - (h/2); // LA STÅ: Dette virker, og det skjønner ikke jeg og Harald... :(
        x = (x + w > o.x ? o.x - w : x);
        y = (y + h > o.y ? o.y - h: y);
        box.setLocation(x,y);
        box.addMouseListener(new MouseListener() {
            /** {@inheritDoc} */
            public void mouseClicked(MouseEvent e) {
            }

            /** {@inheritDoc} */
            public void mousePressed(MouseEvent e) {
            }

            /** {@inheritDoc} */
            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            /** {@inheritDoc} */
            public void mouseExited(MouseEvent e) {
                box.setVisible(false);
                glassPane.remove(box);
            }
        });
        trace(":popped");
        return null;
    }

    @SuppressWarnings("unchecked")
	private T constructView() throws InstantiationException, 
                                     IllegalAccessException, 
                                     InvocationTargetException, 
                                     NoSuchMethodException {
        Constructor c = viewClass.getConstructor(rowClass);
        MipssBeanTableModel model = (MipssBeanTableModel) source.getModel();
        Object[] values = new Object[]{model.get(row)};
        T view = (T) c.newInstance(values);
        return view;
    }
    
    public void reset(int r, Point p) {
        row = r;
        point = p;
        when = System.currentTimeMillis();
    }
    
    /** {@inheritDoc} */
    @Override
    protected void done() {
        trace(":exit");
        if (!killed) {
            listener.clearCountDown();
        }
    }

    /**
     * Dreper denne slik at den ikke lager popup
     * 
     */
    public void kill() {
        trace(":killed");
        killed = true;
    }

    /**
     * Sender trace melding hvis trace er på
     * 
     */
    private void trace(String msg) {
        if (logger.isTraceEnabled()) {
            logger.trace(msg);
        }
    }
}
