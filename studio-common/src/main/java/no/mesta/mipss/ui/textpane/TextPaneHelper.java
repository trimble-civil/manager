package no.mesta.mipss.ui.textpane;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class TextPaneHelper {

	private final JTextPane pane;
	public TextPaneHelper(JTextPane pane){
		this.pane = pane;
	}
	
	public void appendInfo(String msg){
		StyledDocument doc = (StyledDocument)pane.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, Color.blue);
        try {
			doc.insertString(doc.getLength(), msg, style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		pane.setCaretPosition(doc.getLength());
	}
	public void appendWarning(String msg){
		StyledDocument doc = (StyledDocument)pane.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, new Color(200, 100, 0));
        try {
			doc.insertString(doc.getLength(), msg, style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		pane.setCaretPosition(doc.getLength());
	}
	public void appendError(StackTraceElement[] msg, String message){
		StyledDocument doc = (StyledDocument)pane.getDocument();
        Style style = doc.addStyle("StyleName", null);
        StyleConstants.setForeground(style, new Color(230, 0, 0));
        Style style2 = doc.addStyle("StyleName2", null);
        StyleConstants.setForeground(style2, new Color(0, 0, 220));
        try {
        	doc.insertString(doc.getLength(), message, style);
        	for (StackTraceElement e:msg){
        		
        		String m = "at "+e.getClassName()+"(";
        		String m2 = e.getFileName()+":"+e.getLineNumber();
        		String m3 = ")\n";
        		
        		doc.insertString(doc.getLength(), m, style);
        		doc.insertString(doc.getLength(), m2, style2);
        		doc.insertString(doc.getLength(), m3, style);
        	}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		pane.setCaretPosition(doc.getLength());
	}
}
