package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.common.MipssDateFormatter;

/**
 * Rendrer tabell celler som har dato
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class DateTimeRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
	private final String format;
	
    public DateTimeRenderer() {
        this(MipssDateFormatter.DATE_TIME_FORMAT);
    }
    
    public DateTimeRenderer(String format) {
    	if(format == null) {
    		this.format = MipssDateFormatter.DATE_TIME_FORMAT;
    	} else {
    		this.format = format;
    	}
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if(value == null || !(value instanceof Date)) {
            setText(null);
        } else {
            setText(MipssDateFormatter.formatDate((Date) value, format));
        }
        
        return this;
    }
}