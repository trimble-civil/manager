package no.mesta.mipss.ui.table;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JTable;

import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.valueobjects.Epostadresse;

public class EpostadresseTableHelper {
	
	/**
	 * Legger på renderer og lyttere slik at objekter av typen Epostadresse blir rendret rett og kan klikkes på
	 * 
	 * @param table
	 */
	public static void prepareTable(final JTable table) {
		table.setDefaultRenderer(Epostadresse.class, new EpostadresseTableCellRenderer());
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 1) {
					int row = table.rowAtPoint(e.getPoint());
					int col = table.columnAtPoint(e.getPoint());
					if(row >= 0 && col >= 0 && table.getColumnClass(col).equals(Epostadresse.class)) {
						Epostadresse epostadresse = (Epostadresse)table.getValueAt(row, col);
						if(epostadresse != null) {
							DesktopHelper.openNewMail(epostadresse.getAdresse());
						}
					}
				}
			}
		});
		table.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				int row = table.rowAtPoint(e.getPoint());
				int col = table.columnAtPoint(e.getPoint());
				if(row >= 0 && col >= 0 && table.getColumnClass(col).equals(Epostadresse.class) && table.getValueAt(row, col) != null) {
					table.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				} else {
					table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
	}
}
