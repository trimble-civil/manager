package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MipssContractModel;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.RolleBruker;
import no.mesta.mipss.plugin.MipssPlugin;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Setter kontrakt i plugin og loader
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JMipssContractPicker extends JPanel implements PropertyChangeListener, VetoableChangeListener {
	private static final Logger logger = LoggerFactory.getLogger(JMipssContractPicker.class);

	
	private MaintenanceContract contractBean;
	private JComboBox dropdown;
	private MipssContractModel model;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private VetoableChangeSupport vetos = new VetoableChangeSupport(this);

	/**
	 * Konstruktør for å vise alle kontrakter. Skal KUN brukes i moduler som er ment for 
	 * administrasjon av systemet. Ingen brukere skal ha tilgang til alle kontrakter utenom
	 * MIPSS prosjektet
	 * @param showLabel
	 */
	public JMipssContractPicker(boolean showLabel) {
			model = new MipssContractModel(MipssContractModel.SELECT_CONTRACT, getMaintenanceContractBean()
					.getDriftkontraktList());
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		JLabel label = new JLabel("Velg kontrakt");
		dropdown = new JComboBox(model);
		dropdown.setMaximumRowCount(25);
		dropdown.setRenderer(model.createCellRenderer());
		dropdown.setSelectedItem(null);
		dropdown.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);

		ComponentSizeResources.setComponentSizes(dropdown);
		if (showLabel) {
			add(label);
			add(BoxUtil.createHorizontalStrut(5));
		}
		add(dropdown);
	}
	
	public JMipssContractPicker(Bruker bruker, boolean showLabel) {
		model = createContractModel(bruker, null);
		
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		JLabel label = new JLabel("Velg kontrakt");
		model.addPropertyChangeListener("valgtKontrakt", this);
		dropdown = new JComboBox(model);
		dropdown.setMaximumRowCount(25);
		dropdown.setRenderer(model.createCellRenderer());
		dropdown.setSelectedItem(null);
		dropdown.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		
		
		ComponentSizeResources.setComponentSizes(dropdown);
		if (showLabel) {
			add(label);
			add(BoxUtil.createHorizontalStrut(5));
		}
		add(dropdown);
	}
	
	public JMipssContractPicker(MipssPlugin plugin, boolean showLabel) {
		synchronized (JMipssContractPicker.class) {
			JMipssContractPicker picker = null;//pickers.get(plugin.getClass());
			if (picker != null) {
				logger.debug("Not first picker for this plugin");
				model = picker.getModel();
			} else {
				logger.debug("First picker for this plugin");
				model = createContractModel(null, plugin);
			}

			model.addVetoableChangeListener("selectedItem", this);
			setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

			JLabel label = new JLabel("Velg kontrakt");

			dropdown = new JComboBox(model);
			dropdown.setMaximumRowCount(25);
			dropdown.setRenderer(model.createCellRenderer());
			if(plugin != null) {
				dropdown.setSelectedItem(plugin.getLoader().getSelectedDriftkontrakt());
			}
			dropdown.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);

			ComponentSizeResources.setComponentSizes(dropdown);
			if (showLabel) {
				add(label);
				add(Box.createRigidArea(new Dimension(10, 0)));
				add(Box.createHorizontalGlue());
			}
			add(dropdown);
		}
	}

	/**
	 * <p>
	 * Oppretter modellen til kontraktvelgeren
	 * </p>
	 * @param bruker kan være null hvis <b>plugin</b> ikke er null
	 * @param plugin kan være null hvis <b>bruker</b> ikke er null
	 * @return
	 */
	private MipssContractModel createContractModel(Bruker bruker, MipssPlugin plugin){
		if (bruker==null&&plugin==null){
			throw new IllegalArgumentException("Bruker og MipssPlugin kan ikke være null, benytt en annen konstruktør dersom kontraktvelgeren skal benyttes til en intern modul!");
		}
		Boolean begrensListe = Boolean.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("mipss.driftkontrakt", "begrensKontraktListe").getVerdi());
		if (bruker==null){
			bruker = plugin.getLoader().getLoggedOnUser(false);
		}
		if (begrensListe) {
			String rolleNavnAlleKontrakter = KonfigparamPreferences.getInstance().hentEnForApp("mipss.driftkontrakt", "rolleNavnForAlleKontrakter").getVerdi();
			List<RolleBruker> roller = bruker.getRolleBrukerList();
			
			//sjekk om bruker skal ha tilgang til alle kontakter
			boolean created = false;
			for (RolleBruker rb:roller){
				if (rb.getRolle().getNavn().equals(rolleNavnAlleKontrakter)){
					model = new MipssContractModel(MipssContractModel.SELECT_CONTRACT, getMaintenanceContractBean().getDriftkontraktList());
					created = true;
					break;
				}
			}
			if (!created)
				model = new MipssContractModel(MipssContractModel.SELECT_CONTRACT, getMaintenanceContractBean().getDriftkontraktList(bruker));
		} else {
			//kontraktlisten begrenses ikke 
			model = new MipssContractModel(MipssContractModel.SELECT_CONTRACT, getMaintenanceContractBean().getDriftkontraktList());
		}
		return model;
	}
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	public JComboBox getDropdown() {
		return dropdown;
	}
	@Override
	public void setEnabled(boolean enabled){
		if (dropdown!=null){
			dropdown.setEnabled(enabled);
		}
	}
	private MaintenanceContract getMaintenanceContractBean() {
		if (contractBean == null) {
			contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		}

		return contractBean;
	}

	public MipssContractModel getModel() {
		return model;
	}

	public Driftkontrakt getValgtKontrakt() {
		return model.getSelectedItem();
	}


	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		String propertyName = evt.getPropertyName();
		if (StringUtils.equals("selectedItem", propertyName)) {
			MipssContractModel src = (MipssContractModel) evt.getSource();
			Driftkontrakt kontrakt = src.getSelectedItem();
			Driftkontrakt old = (Driftkontrakt) evt.getOldValue();
			logger.debug("propertyChange() kontrakt endres fra " + old + " til " + kontrakt);

			if (model.isNoContract(kontrakt)) {
				kontrakt = null;
			}

			if (model.isNoContract(old)) {
				old = null;
			}

			if (old == null && kontrakt == null) {
				return;
			}

			vetos.fireVetoableChange("valgtKontrakt", old, kontrakt); // Kan evt
			// kaste
			// exception

			props.firePropertyChange("valgtKontrakt", old, kontrakt);
		}
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#addVetoableChangeListener(VetoableChangeListener)
	 * @param l
	 */
	public void addVetoableChangeListener(VetoableChangeListener listener) {
		vetos.addVetoableChangeListener(listener);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#addVetoableChangeListener(String,
	 *      VetoableChangeListener)
	 * @param l
	 */
	public void addVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		vetos.addVetoableChangeListener(propertyName, listener);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#removeVetoableChangeListener(VetoableChangeListener)
	 * @param l
	 */
	public void removeVetoableChangeListener(VetoableChangeListener listener) {
		vetos.removeVetoableChangeListener(listener);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#removeVetoableChangeListener(String,
	 *      VetoableChangeListener)
	 * @param l
	 */
	public void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		vetos.removeVetoableChangeListener(propertyName, listener);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		props.firePropertyChange("valgtKontrakt", evt.getOldValue(), evt.getNewValue());
	}
}
