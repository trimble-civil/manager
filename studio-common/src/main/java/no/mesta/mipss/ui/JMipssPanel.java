package no.mesta.mipss.ui;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;


@SuppressWarnings("serial")
public class JMipssPanel<T> extends JPanel{
		
	private Class<T> clazz;

	public JMipssPanel(Class<T> clazz) {	
		this.clazz = clazz;
		initializeKeyBindings();
	}

	private void initializeKeyBindings() {
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "forward");
        this.getActionMap().put("forward", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	JOptionPane.showMessageDialog(null, "F1 - SimpleName: " +  clazz.getSimpleName() + ", ClassName: " + clazz.getName());
                System.out.println("F1 - SimpleName: " +  clazz.getSimpleName() + ", ClassName: " + clazz.getName());
            }
        });		
	}
}
