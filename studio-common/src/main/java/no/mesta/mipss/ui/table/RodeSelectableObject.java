package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.kontrakt.Rode;

@SuppressWarnings("serial")
public class RodeSelectableObject implements CheckBoxTableObject {
	private Rode rode;
	private Boolean valgt;
	
	public RodeSelectableObject(Rode rode) {
		this.rode = rode;
		valgt = Boolean.FALSE;
	}
	
	public String getNavn() {
		return rode.getNavn();
	}

	public Boolean getValgt() {
		return valgt;
	}
	
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}
	
	public Rode getRode() {
		return rode;
	}
	
	@Override
	public String getTextForGUI() {
		return "";
	}
}
