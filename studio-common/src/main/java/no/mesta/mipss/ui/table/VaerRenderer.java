package no.mesta.mipss.ui.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.persistence.mipssfield.Vaer;

/**
 * Rendrer tabell celler som har v�r
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class VaerRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

    public VaerRenderer() {
        super();
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if(value == null || !(value instanceof Vaer)) {
            setText(null);
        } else {
            Vaer vaer = (Vaer) value;
            String txt = (vaer == null ? "" : vaer.getTextForGUI());
            setText(txt);
        }
        
        return this;
    }
}