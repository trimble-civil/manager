package no.mesta.mipss.ui.pluginlist;

import java.util.EventListener;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface PluginListModelListener extends EventListener{

    /**
     * Fyres når modellen endrer seg
     * 
     * @param event
     */
    public void modelChanged(PluginListModelEvent event);
}
