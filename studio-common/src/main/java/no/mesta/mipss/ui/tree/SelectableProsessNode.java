package no.mesta.mipss.ui.tree;

import no.mesta.mipss.persistence.mipssfield.Prosess;

public class SelectableProsessNode {
	private Prosess prosess;
	private Boolean selected;
	
	public SelectableProsessNode(Prosess prosess) {
		this.prosess = prosess;
		selected = Boolean.FALSE;
	}
	
	public String getText() {
		return prosess.getTextForGUI();
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Prosess getProsess() {
		return prosess;
	}
}