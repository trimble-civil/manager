package no.mesta.mipss.ui.table;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.persistence.dokarkiv.Bilde;

import org.jdesktop.swingx.JXImageView;

/**
 * Rendrer
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class BildeRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

	public BildeRenderer() {
	}

	/** {@inheritDoc} */
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(component.getBackground());
		panel.setForeground(component.getForeground());
		
		if (value != null && value instanceof Bilde) {
			Bilde b = (Bilde) value;

			if (b != null) {
				JXImageView view = new JXImageView();
				view.setDragEnabled(false);
				view.setEditable(false);
				view.setImage(b.getSmaaImage());
				panel.add(view, BorderLayout.CENTER);
			}
		}

		return panel;
	}
}