package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.driftkontrakt.bean.LevStedQueryFilter;
import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.util.KjoretoyUtils;

public class KjoretoyVelgLeverandorPanel extends AbstractRelasjonPanel<LevStedView> implements PropertyChangeListener {
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    
	private SokPanel<LevStedView> sokPanel;
	
	public KjoretoyVelgLeverandorPanel(MaintenanceContract maintenanceContractBean) {
		sokPanel = new SokPanel<LevStedView>(LevStedView.class,
												new LevStedQueryFilter(),
												maintenanceContractBean,
												"getLevStedViewListUnique",
												MaintenanceContract.class,
												false,
												"levNr",
												"levNavn",
												"orgNr",
												"adresse1",
												"postnr",
												"poststed");
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlMain.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
	private void updateComplete() {
		LevStedView levStedView = sokPanel.getSelectedEntity();
		setComplete(levStedView != null);
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(800,500);
	}

	@Override
	public LevStedView getNyRelasjon() {
		return sokPanel.getSelectedEntity();
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getPropertyString("leverandor.velgDialogTitle");
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateComplete();
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
