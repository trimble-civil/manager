package no.mesta.mipss.ui.process;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Valgbar entitet i tabell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ProsessTableObject implements CheckBoxTableObject, Comparable<ProsessTableObject>, PropertyChangeSource {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_NONE = BUNDLE.getGuiString("processPanel.none");
	private transient PropertyChangeSupport props;
	private Prosess prosess;
	private boolean valgt;

	public ProsessTableObject(Prosess prosess) {
		this.prosess = prosess;
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(ProsessTableObject o) {
		if(o == null) {
			return 1;
		}
		
		if(prosess == null) {
			return -1; // "ingen" valget
		}
		
		return prosess.compareTo(o.prosess);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof ProsessTableObject)) {
			return false;
		}
		
		ProsessTableObject other = (ProsessTableObject) o;
		
		return new EqualsBuilder().append(prosess, other.prosess).isEquals();
	}

	private PropertyChangeSupport getProps() {
		if(props == null) {
			props = new PropertyChangeSupport(this);
		}
		
		return props;
	}

	/**
	 * @return the prosess
	 */
	public Prosess getProsess() {
		return prosess;
	}
	
	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		return prosess == null ? GUI_NONE : prosess.getTextForGUI();
	}
	
	/**
	 * @return the valgt
	 */
	public Boolean getValgt() {
		return valgt;
	}
	
	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(prosess).toHashCode();
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	/**
	 * @param prosess
	 *            the prosess to set
	 */
	public void setProsess(Prosess prosess) {
		Prosess old = this.prosess;
		this.prosess = prosess;
		getProps().firePropertyChange("prosess", old, prosess);
	}

	/**
	 * @param valgt
	 *            the valgt to set
	 */
	public void setValgt(Boolean valgt) {
		Boolean old = this.valgt;
		this.valgt = valgt;
		getProps().firePropertyChange("valgt", old, valgt);
	}
}
