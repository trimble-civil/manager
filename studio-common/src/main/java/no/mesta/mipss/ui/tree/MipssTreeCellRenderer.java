package no.mesta.mipss.ui.tree;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class MipssTreeCellRenderer extends DefaultTreeCellRenderer{
	
	private ImageIcon image;
	
	public MipssTreeCellRenderer(){
		this(null);
	}
	public MipssTreeCellRenderer(ImageIcon image){
		this.image = image;
	}
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		Component temp = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
		if (node.getUserObject() instanceof IRenderableMipssEntity){
			IRenderableMipssEntity entity = (IRenderableMipssEntity)node.getUserObject();
			setText(entity.getTextForGUI());
		}
		if (node.getUserObject() instanceof String){
			setText((String)node.getUserObject());
		}
		if (image!=null)
			setIcon(image);
		return temp;
	}

}
