package no.mesta.mipss.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;


public class TabCloseIcon implements Icon {
	private Icon tabIcon;
	
	private JTabbedPane tabPane = null;
	private transient Rectangle iconPos = null;

	/**
	 * Creates a new instance of TabCloseIcon.
	 */
	public TabCloseIcon(Icon tabIcon) {
		this.tabIcon = tabIcon;
	}

	/**
	 * Creates a new instance of TabCloseIcon.
	 */
	public TabCloseIcon() {
		this(new ImageIcon(TabCloseIcon.class.getResource("/images/exit_over.png")));
	}

	
	public void paintIcon(final Component c, Graphics g, int x, int y) {
		if (null == tabPane) {
			tabPane = (JTabbedPane) c;
			tabPane.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// asking for isConsumed is *very* important, otherwise more
					// than one tab might get closed!
					if (!e.isConsumed()&& iconPos.contains(e.getX(), e.getY())) {
						if (tabPane.getComponentCount()==1){
							e.consume();
							return;
						}
						final int index = tabPane.getSelectedIndex();
						tabPane.remove(index);
						e.consume();
					}
				}
			});
			
//			tabPane.addChangeListener(new ChangeListener(){
//				public void stateChanged(ChangeEvent e) {
//					JTabbedPane pane = (JTabbedPane)e.getSource();
//		            // Get current tab
//		            int sel = pane.getSelectedIndex();
//		            
//		            tabIcon = new ImageIcon(TabCloseIcon.class.getResource("/images/exit_disabled.png"));
//		            int tabCount = pane.getTabCount();
//		            for (int i=0;i<tabCount;i++){
//		            	pane.setIconAt(i, tabIcon);
//		            }
//		            tabIcon = new ImageIcon(TabCloseIcon.class.getResource("/images/exit.png"));
//		            pane.setIconAt(sel, tabIcon);
//				}
//			});
		}
		iconPos = new Rectangle(x, y, getIconWidth(), getIconHeight());
		tabIcon.paintIcon(c, g, x, y);
		
		
	}

	/**
	 * just delegate
	 */
	public int getIconWidth() {
		return tabIcon.getIconWidth();
	}

	/**
	 * just delegate
	 */
	public int getIconHeight() {
		return tabIcon.getIconHeight();
	}

}