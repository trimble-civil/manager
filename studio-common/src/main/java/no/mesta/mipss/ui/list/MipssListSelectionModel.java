/**
 * 
 */
package no.mesta.mipss.ui.list;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

public class MipssListSelectionModel<T extends IRenderableMipssEntity> extends
		DefaultListSelectionModel {

	private JList source;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	
	public MipssListSelectionModel(JList list) {
		super();
		source = list;
		source.setSelectionModel(this);
	}

	@SuppressWarnings("unchecked")
	public T getSelectedValue() {
		return (T) source.getSelectedValue();
	}

	public void setSelectedValue(T value) {
		T old = getSelectedValue();
		source.setSelectedValue(value, true);
		props.firePropertyChange("selectedValue", old, value);
	}

	/** {@inheritDoc} */
	@Override
	public void setSelectionInterval(int index0, int index1) {
		T old = getSelectedValue();
		super.setSelectionInterval(index0, index1);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}

	/** {@inheritDoc} */
	@Override
	public void addSelectionInterval(int index0, int index1) {
		T old = getSelectedValue();
		super.addSelectionInterval(index0, index1);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}

	/** {@inheritDoc} */
	@Override
	public void removeSelectionInterval(int index0, int index1) {
		T old = getSelectedValue();
		super.removeSelectionInterval(index0, index1);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}
	
	/** {@inheritDoc} */
	@Override
	public void setAnchorSelectionIndex(int index) {
		T old = getSelectedValue();
		super.setAnchorSelectionIndex(index);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}
	
	/** {@inheritDoc} */
	@Override
	public void setLeadSelectionIndex(int index) {
		T old = getSelectedValue();
		super.setLeadSelectionIndex(index);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}
	
	/** {@inheritDoc} */
	@Override
	public void clearSelection() {
		T old = getSelectedValue();
		super.clearSelection();
		props.firePropertyChange("selectedValue", old, null);
	}
	
	/** {@inheritDoc} */
	@Override
	public void insertIndexInterval(int index, int length, boolean before) {
		T old = getSelectedValue();
		super.insertIndexInterval(index, length, before);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}
	
	/** {@inheritDoc} */
	@Override
	public void removeIndexInterval(int index0, int index1) {
		T old = getSelectedValue();
		super.removeIndexInterval(index0, index1);
		props.firePropertyChange("selectedValue", old, getSelectedValue());
	}

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}