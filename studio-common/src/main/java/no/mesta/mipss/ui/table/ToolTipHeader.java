package no.mesta.mipss.ui.table;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

@SuppressWarnings("serial")
public class ToolTipHeader extends JTableHeader {
	String[] toolTips;
	
	List<String> tooltips;
	   
    public ToolTipHeader(TableColumnModel model) {
      super(model);
      toolTips = new String [model.getColumnCount()];
    }
     
    public String getToolTipText(MouseEvent e) {
    	try {
    		int col  = columnAtPoint(e.getPoint());
    		int modelCol = getTable().convertColumnIndexToModel(col);
    		String retStr;
    		retStr = toolTips[modelCol];
    		if (retStr.length() < 1) {
    			retStr = super.getToolTipText(e);
    		}
    		return retStr;
    	} catch (NullPointerException ex) {
    		return "";
    	} catch (ArrayIndexOutOfBoundsException ex) {
    		return "";
    	}
    }  
    
    public void setToolTip(int columnIndex, String tooltip){
    	toolTips[columnIndex] = tooltip;
    }

}
