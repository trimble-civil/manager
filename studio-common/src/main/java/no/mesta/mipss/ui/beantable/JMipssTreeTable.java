package no.mesta.mipss.ui.beantable;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JViewport;
import javax.swing.event.ListSelectionEvent;
import javax.swing.tree.TreePath;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTableHeader;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.TreeTableModel;

@SuppressWarnings("serial")
public class JMipssTreeTable<T extends IRenderableMipssEntity> extends JXTreeTable implements PropertyChangeListener, MipssBeanLoaderListener{

	private final TreeTableModel model;
	private int rows = 0;
	private int[] selectedRowIndices;
	private Map<Integer, String> tooltips;
	
	public JMipssTreeTable(TreeTableModel model, Map<Integer, String> tooltips){
		super(model);
		this.model = model;
		this.tooltips = tooltips;
		addPropertyChangeListener("selectedRows", this);
	}
	public JMipssTreeTable(TreeTableModel model){
		this(model, null);
	}
	//Implement table header tool tips. 
	@Override
    protected JXTableHeader createDefaultTableHeader() {
        return new JXTableHeader(columnModel) {
            public String getToolTipText(MouseEvent e) {
            	if (tooltips==null)
            		return null;
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return tooltips.get(realIndex);
                
            }
        };
    }
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == this && StringUtils.equals("selectedRows", evt.getPropertyName())) {
			int[] old = (int[]) evt.getOldValue();
			int[] now = (int[]) evt.getNewValue();
			firePropertyChange("selectedEntities", getEntities(old), getEntities(now));
		}
	}
	
	/**
	 * Gir ut visse rader
	 * 
	 * @param rows
	 * @return
	 */
	public List<T> getEntities(int[] rows) {
		List<T> entities = new ArrayList<T>();
		if (rows != null && rows.length > 0) {
			for (int r : rows) {
				TreePath path = getPathForRow(r);
				if (path!=null){
					T entity = (T)path.getLastPathComponent();
					entities.add(entity);
				}
			}
		}

		return entities;
	}

	/**
	 * Gir ut valgte rader
	 * 
	 */
	public List<T> getSelectedEntities() {
		return getEntities(getSelectedRows());
	}
	
	public boolean getScrollableTracksViewportWidth() {
		return getParent() instanceof JViewport && (((JViewport) getParent()).getWidth() > getPreferredSize().width);
	}
	
	/** {@inheritDoc} */
	@Override
	public void valueChanged(ListSelectionEvent evt) {
		super.valueChanged(evt);
		if (!evt.getValueIsAdjusting()) {
			int old = rows;
			rows = getSelectedRowCount();
			int[] oldIndices = this.selectedRowIndices;
			selectedRowIndices = getSelectedRows();
			firePropertyChange("selectedRowIndices", oldIndices, selectedRowIndices);
			firePropertyChange("selectedRows", oldIndices, selectedRowIndices);
			firePropertyChange("noOfSelectedRows", old, rows);
		}
	}
	
	/**
	 * Antall valgte rader i tabellen
	 * 
	 * @return
	 */
	public int getNoOfSelectedRows() {
		return getSelectedRowCount();
	}

	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		setEnabled(true);
		repaint();
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		setEnabled(false);
		repaint();
	}
}
