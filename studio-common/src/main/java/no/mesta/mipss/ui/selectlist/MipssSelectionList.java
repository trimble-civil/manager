package no.mesta.mipss.ui.selectlist;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.list.JMipssList;
import no.mesta.mipss.ui.list.MipssRenderableEntityModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.decorator.SortOrder;

/**
 * Komponent for aa vise en "picklist" for brukeren, hvor brukeren kan flytte
 * elementer vha fire knapper fra en liste over alle tilgjengelige
 * valgmuligheter til en liste med sine valg.
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class MipssSelectionList<T extends IRenderableMipssEntity> extends JComponent {
	private ImageIcon fjernAlleIkon;
	private ImageIcon fjernEnIkon;
	private List<ListSelectionListener> listSelectionListeners = new ArrayList<ListSelectionListener>();
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private JMipssList<T> muligheterList;
	private MipssRenderableEntityModel<T> muligheterModell;
	private String tittel;
	private JMipssList<T> valgList;
	private MipssRenderableEntityModel<T> valgModell;
	private ImageIcon velgAlleIkon;
	private ImageIcon velgEnIkon;
	private final Class<T> clazz;

	/**
	 * Konstruktoer for komponentet
	 * 
	 * @param muligheter
	 *            listen over entiteter brukeren skal faa velge i
	 * @param tittel
	 *            tittelen til listen
	 */
	public MipssSelectionList(List<T> muligheter, List<T> valg, String tittel, Class<T> clazz) {
		log.debug("MipssSelectionList(muligheter,tittel), muligheter: "
				+ (muligheter == null ? "null" : muligheter.size() + " elementer") + ", tittel: " + tittel);
		this.tittel = tittel;
		this.clazz = clazz;
		
		List<T> mineMuligheter = new ArrayList<T>(muligheter);
		List<T> mineValg = new ArrayList<T>(valg);
		if (!mineValg.isEmpty()) {
			mineMuligheter.removeAll(mineValg);
			valgModell = new MipssRenderableEntityModel<T>(mineValg);
		} else {
			valgModell = new MipssRenderableEntityModel<T>(new ArrayList<T>());
		}
		muligheterModell = new MipssRenderableEntityModel<T>(mineMuligheter);
		valgModell.addListDataListener(new DataListener());

		try {
			initGUI();
		} catch (Exception e) {
			log.debug("initGUI(), e: ", e);
		}
		log.debug("MipssSelectionList(muligheter,tittel), ferdig");
	}

	/**
	 * Legger til en lytter paa listen
	 * 
	 * @param listener
	 */
	public void addListSelectionListener(ListSelectionListener listener) {
		listSelectionListeners.add(listener);
	}

	/**
	 * Toemmer valgene til brukeren, og notifiserer lytterne
	 */
	public void clearSelection() {
		synchronized (muligheterModell) {
			synchronized (valgModell) {
				List<T> alle = valgModell.getElements();
				valgModell.removeElements(alle);
				muligheterModell.addElements(alle);
				ListSelectionEvent event = new ListSelectionEvent((Object) this, 0, valgModell.getSize() - 1, false);
				for (ListSelectionListener l : listSelectionListeners) {
					l.valueChanged(event);
				}
			}
		}
	}

	/**
	 * Returnerer alle registrerte lyttere
	 * 
	 * @return
	 */
	public List<ListSelectionListener> getListSelectionListeners() {
		return listSelectionListeners;
	}

	/**
	 * Returnerer den valgte verdien til brukeren.
	 * 
	 * @return brukerens valgte verdi. Null hvis ikke valgt, eller flere er
	 *         valgt.
	 */
	public T getSelectedValue() {
		if (valgModell.getSize() == 1) {
			T object = valgModell.getElementAt(0);
			return object;
		} else {
			return null;
		}
	}

	/**
	 * Returnerer de valgte verdiene til brukeren
	 * 
	 * @return
	 */
	public List<T> getSelectedValues() {
		return valgModell.getEntities();
	}

	/**
	 * Lager GUI'et til dette komponentet. GUI'et bestaar av to lister og fire
	 * knapper
	 * 
	 * @throws Exception
	 */
	private void initGUI() throws Exception {
		velgEnIkon = IconResources.ADD_ONE_ICON;
		velgAlleIkon = IconResources.ADD_ALL_ICON;
		fjernEnIkon = IconResources.REMOVE_ONE_ICON;
		fjernAlleIkon = IconResources.REMOVE_ALL_ICON;
		JButton velgEnKnapp = new JButton(velgEnIkon);

		JButton velgAlleKnapp = new JButton(velgAlleIkon);
		JButton fjernEnKnapp = new JButton(fjernEnIkon);
		JButton fjernAlleKnapp = new JButton(fjernAlleIkon);

		muligheterList = new JMipssList<T>(muligheterModell, true, clazz);
		muligheterList.setCellRenderer(new MipssListCellRenderer<T>());
		muligheterList.setSortOrder(SortOrder.ASCENDING);

		valgList = new JMipssList<T>(valgModell, true, clazz);
		valgList.setCellRenderer(new MipssListCellRenderer<T>());
		valgList.setSortOrder(SortOrder.ASCENDING);

		final ValgListener muligheterListener = new ValgListener();
		muligheterList.addListSelectionListener(muligheterListener);
		final ValgListener valgListener = new ValgListener();
		valgList.addListSelectionListener(valgListener);
		muligheterList.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		valgList.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		// Action for velg en knapp. Flytter valgte elementer til høyre liste
		ActionListener velgEnAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.debug("actionPerformed(e) - start, velg en knapp");
				synchronized (muligheterModell) {
					synchronized (valgModell) {
						List<T> valg = muligheterListener.getValg();
						muligheterList.clearSelection();
						muligheterModell.removeElements(valg);
						valgModell.addElements(valg);
					}
				}
			}
		};
		velgEnKnapp.addActionListener(velgEnAction);

		// Action for velg alle knapp. Flytter alle elementer til høyre liste
		ActionListener velgAlleAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.debug("actionPerformed(e) - start, velg alle knapp");
				synchronized (muligheterModell) {
					synchronized (valgModell) {
						List<T> alle = muligheterModell.getElements();
						muligheterModell.removeElements(alle);
						valgModell.addElements(alle);
					}
				}
			}
		};
		velgAlleKnapp.addActionListener(velgAlleAction);

		// Action for fjern en knapp. Flytter et element til venstre liste
		ActionListener fjernEnAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.debug("actionPerformed(e) - start, fjern en knapp");
				synchronized (muligheterModell) {
					synchronized (valgModell) {
						List<T> valg = valgListener.getValg();
						valgList.clearSelection();
						valgModell.removeElements(valg);
						muligheterModell.addElements(valg);
					}
				}
			}
		};
		fjernEnKnapp.addActionListener(fjernEnAction);

		// Action for fjern alle knapp. Flytter alle elementer til venstre liste
		ActionListener fjernAlleAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.debug("actionPerformed(e) - start, fjern alle knapp");
				synchronized (muligheterModell) {
					synchronized (valgModell) {
						List<T> alle = valgModell.getElements();
						valgModell.removeElements(alle);
						muligheterModell.addElements(alle);
					}
				}
			}
		};
		fjernAlleKnapp.addActionListener(fjernAlleAction);

		JScrollPane scrollMuligheterListe = new JScrollPane((JXList) muligheterList);
		JScrollPane scrollValgListe = new JScrollPane((JXList) valgList);
		ComponentSizeResources.setComponentSizes(scrollMuligheterListe, new Dimension(150, 150), new Dimension(150, 150), new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		ComponentSizeResources.setComponentSizes(scrollValgListe, new Dimension(150, 150), new Dimension(150, 150), new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		scrollMuligheterListe.setBorder(new TitledBorder("Alle"));
		scrollValgListe.setBorder(new TitledBorder("Valgte"));

		ComponentSizeResources.setComponentSize(fjernEnKnapp, new Dimension(40, 23));
		ComponentSizeResources.setComponentSize(fjernAlleKnapp, new Dimension(40, 23));
		ComponentSizeResources.setComponentSize(velgAlleKnapp, new Dimension(40, 23));
		ComponentSizeResources.setComponentSize(velgEnKnapp, new Dimension(40, 23));
		Box buttonPanel = BoxUtil.createVerticalBox(5, Box.createVerticalGlue(), velgEnKnapp, velgAlleKnapp, fjernAlleKnapp, fjernEnKnapp, Box.createVerticalGlue());

		Box componentPanel = BoxUtil.createHorizontalBox(5, scrollMuligheterListe, buttonPanel, scrollValgListe);

		setLayout(new BorderLayout());
		add(componentPanel, BorderLayout.CENTER);
		setBorder(new TitledBorder(tittel));
	}

	/**
	 * Fjerner en lytter paa listen
	 * 
	 * @param listener
	 */
	public void removeListSelectionListener(ListSelectionListener listener) {
		listSelectionListeners.remove(listener);
	}

	/**
	 * Brukes til aa si i fra til MipssSelectionList at
	 * 
	 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
	 */
	class DataListener implements ListDataListener {

		/**
		 * @see #fireDataChanged()
		 * @param e
		 */
		public void contentsChanged(ListDataEvent e) {
			fireDataChanged();
		}

		/**
		 * Det er enklere aa bare sende en "altomfattende" selection
		 * notifikasjon til lytterne. Dette fordi listen med valg til brukeren i
		 * valgModell er komplett, og sortert. Saa index beregninger ikke
		 * praktisk.
		 * 
		 */
		private void fireDataChanged() {
			ListSelectionEvent event = new ListSelectionEvent((Object) MipssSelectionList.this, 0,
					valgModell.getSize() - 1, false);
			for (ListSelectionListener l : listSelectionListeners) {
				l.valueChanged(event);
			}
		}

		/**
		 * @see #fireDataChanged()
		 * @param e
		 */
		public void intervalAdded(ListDataEvent e) {
			fireDataChanged();
		}

		/**
		 * @see #fireDataChanged()
		 * @param e
		 */
		public void intervalRemoved(ListDataEvent e) {
			fireDataChanged();
		}
	}

	/**
	 * Brukes til aa lytte paa de to interne listene (muligheter og valg) slik
	 * at elementer kan flyttes mellom de to modellene til de to listene.
	 * 
	 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
	 */
	class ValgListener implements ListSelectionListener {
		Logger log = LoggerFactory.getLogger(this.getClass());
		List<T> valg = new ArrayList<T>();

		/**
		 * Valgene til brukeren i listen det lyttes paa
		 * 
		 * @return List av T, aldri null.
		 */
		public List<T> getValg() {
			log.debug("getValg() - returnerer: " + (valg == null ? "null" : valg.size() + " valg"));
			return valg;
		}

		/**
		 * Naar listen endrer seg henter den ut de nye valgene til brukeren
		 * 
		 * @param e
		 */
		@SuppressWarnings("unchecked")
		public void valueChanged(ListSelectionEvent e) {
			log.debug("valueChanged(), e:" + e);
			JMipssList<T> list = (JMipssList<T>) e.getSource();
			if (list.getSelectedValues() != null) {
				T[] objekter = list.getSelectedValues();
				valg = new ArrayList<T>();
				for (T objekt : objekter) {
					valg.add(objekt);
				}
			} else {
				valg = new ArrayList<T>();
				T objekt = list.getSelectedValue();
				valg.add(objekt);
			}
		}
	}
}
