package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.TrafiktiltakTableObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * Panel med tabell for valgbare entiteter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JTrafikktiltakPanel extends JPanel implements ItemListener, PropertyChangeListener {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_ALL = BUNDLE.getGuiString("tiltakPanel.all");
	private static final String GUI_TITLE = BUNDLE.getGuiString("tiltakPanel.title");
	private static final Logger logger = LoggerFactory.getLogger(JTrafikktiltakPanel.class);
	private Set<TrafiktiltakTableObject> allActions;
	private JCheckBox alleCheck;
	private String[] columns = new String[] { "valgt", "textForGUI" };
	private TrafiktiltakTableObject ingen;
	private JXTable table;
	private MipssRenderableEntityTableModel<TrafiktiltakTableObject> tableModel;
	private Set<TrafiktiltakTableObject> valgte = new HashSet<TrafiktiltakTableObject>();

	public JTrafikktiltakPanel() {
		initGui();
	}

	public Long[] getAlleValgte() {
		logger.debug("getAlleValgte() start");

		return setToArray(valgte);
	}

	private JXTable getTable() {
		allActions = new TreeSet<TrafiktiltakTableObject>();
		List<Trafikktiltak> alle = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class).hentAlleTrafikktiltaktyper();

		ingen = new TrafiktiltakTableObject(null);
		for (Trafikktiltak p : alle) {
			TrafiktiltakTableObject tto = new TrafiktiltakTableObject(p);
			tto.addPropertyChangeListener("valgt", this);
			allActions.add(tto);
		}
		allActions.add(ingen);

		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				TrafiktiltakTableObject.class, columns);
		tableModel = new MipssRenderableEntityTableModel<TrafiktiltakTableObject>(TrafiktiltakTableObject.class,
				new int[] { 0 }, columns);
		tableModel.getEntities().clear();
		tableModel.getEntities().addAll(allActions);
		JXTable table = new JXTable(tableModel, columnModel);
		table.getColumn(0).setMaxWidth(30);

		return table;
	}

	private void initGui() {
		table = getTable();

		JScrollPane scroll = new JScrollPane(table);

		alleCheck = new JCheckBox(GUI_ALL);
		alleCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox box = (JCheckBox) e.getSource();
				if (box.isSelected()) {
					table.setEnabled(false);
				} else
					table.setEnabled(true);

			}
		});

		Box components = BoxUtil.createVerticalBox(0, BoxUtil.createHorizontalBox(0, scroll), BoxUtil
				.createHorizontalBox(0, alleCheck, Box.createHorizontalGlue()));
		ComponentSizeResources.setComponentSize(this, new Dimension(200, 200));

		components.setBorder(BorderFactory.createTitledBorder(GUI_TITLE));

		setLayout(new BorderLayout());
		add(components, BorderLayout.CENTER);
	}

	public boolean isAlleValgt() {
		return alleCheck.isSelected();
	}

	public boolean isIngenValgt() {
		return ingen.getValgt();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		boolean isSet = e.getStateChange() == ItemEvent.SELECTED;

		if (e.getSource() == alleCheck) {
			firePropertyChange("alleValgt", !isSet, isSet);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		logger.debug("propertyChange(" + evt + ") start for " + property);
		TrafiktiltakTableObject source = (TrafiktiltakTableObject) evt.getSource();
		if (StringUtils.equals(property, "valgt") && source == ingen) {
			Boolean oldValue = (Boolean) evt.getOldValue();
			Boolean newValue = (Boolean) evt.getNewValue();
			firePropertyChange("ingenValgt", oldValue, newValue);
		} else if (StringUtils.equals(property, "valgt")) {
			Set<TrafiktiltakTableObject> old = new HashSet<TrafiktiltakTableObject>(valgte);
			if (source.getValgt() == true) {
				valgte.add(source);
			} else {
				valgte.remove(source);
			}

			Long[] oldValues = setToArray(old);
			Long[] nowValues = setToArray(valgte);

			logger.debug("fire property change, old = " + Arrays.toString(oldValues) + ", now = "
					+ Arrays.toString(nowValues));
			firePropertyChange("alleValgte", oldValues, nowValues);
		}
	}

	private Long[] setToArray(Set<TrafiktiltakTableObject> set) {
		Long[] ids = new Long[set.size()];
		Iterator<TrafiktiltakTableObject> it = set.iterator();
		int i = 0;
		boolean noneHit = false;
		while (it.hasNext()) {
			TrafiktiltakTableObject action = (TrafiktiltakTableObject) it.next();
			if (action.getTiltak() != null) {
				ids[i] = action.getTiltak().getId();
				i++;
			} else {
				noneHit = true;
			}
		}

		if (noneHit && ids.length > 0) {
			ids = Arrays.copyOf(ids, ids.length - 1);
		}

		return ids;
	}
}
