package no.mesta.mipss.ui.systeminfo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JMenuItem;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.resources.images.IconResources;


/**
 * Status komponent som kan brukes for debug av klienten
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JSystemInfoMenuItem extends JMenuItem {
    private SystemInfoDialog systemInfoDialog;
    /**
     * Konstruktoer
     * 
     */
    public JSystemInfoMenuItem(final IMipssStudioLoader loader) {
        super("Systeminformasjon", IconResources.MODULE_ICON);

        addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (systemInfoDialog==null){
	                    	systemInfoDialog = new SystemInfoDialog(loader);
	                    	systemInfoDialog.setVisible(true);
	                    	systemInfoDialog.addWindowListener(new WindowListener() {
	                    		@Override
								public void windowClosing(WindowEvent e) {
									systemInfoDialog.dispose();
									systemInfoDialog=null;
								}
								@Override
								public void windowOpened(WindowEvent e) {
								}
								@Override
								public void windowIconified(WindowEvent e) {
								}
								@Override
								public void windowDeiconified(WindowEvent e) {
								}
								@Override
								public void windowDeactivated(WindowEvent e) {
								}
								@Override
								public void windowClosed(WindowEvent e) {
								}
								@Override
								public void windowActivated(WindowEvent e) {
								}
							});
						}
                    }
                });
    }
}
