package no.mesta.mipss.ui.taskpane;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.jdesktop.swingx.VerticalLayout;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

public class MipssTaskPaneContainer extends JPanel{

	private List<MipssTaskPane> taskPanes = new ArrayList<MipssTaskPane>();
	public MipssTaskPaneContainer(){
		setLayout(new VerticalLayout(10));
	}
	
	public void addTaskPane(MipssTaskPane taskPane){
		taskPanes.add(taskPane);
		add(taskPane);
	}
	public void removeAllTaskPanes(){
		taskPanes.clear();
		removeAll();
	}
	public void collapseAll(boolean collapsed){
		for (MipssTaskPane p:taskPanes){
			p.setCollapsed(collapsed);
		}
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(new WindowsLookAndFeel());
		} catch (Exception e) {
			return;
		}
		JFrame f = new JFrame();
		f.setSize(200,200);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MipssTaskPaneContainer c = new MipssTaskPaneContainer();
		c.addTaskPane(new MipssTaskPane());
		c.addTaskPane(new MipssTaskPane());
		
		f.add(c);
		f.setVisible(true);
		f.pack();
	}
}
