package no.mesta.mipss.ui.beantable;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JViewport;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumn;

import jxl.CellView;
import jxl.Workbook;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;

/**
 * Tabell som vet om modellen er opptatt med aa laste data eller ikke
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */

public class JMipssBeanTable<T extends IRenderableMipssEntity> extends JXTable implements MipssBeanLoaderListener,
		PropertyChangeListener {
	
	public enum EXPORT_TYPE{
		ALL_ROWS,
		SELECTED_ROWS_ONLY
	}
	private static final Logger logger = LoggerFactory.getLogger(JMipssBeanTable.class);
	@SuppressWarnings("unused")
	private boolean busyModel = true;
	private boolean fillsViewportWidth;
	private MipssBeanTableModel<T> model;
	private int rows = 0;
	private int[] selectedRowIndices;
	private boolean doWidthHacks;
	private MipssTablePreferences userSettings;

	/**
	 * Konstruktoer
	 * 
	 * @param model
	 * @param columnModel
	 */
	public JMipssBeanTable(MipssBeanTableModel<T> model, MipssRenderableEntityTableColumnModel columnModel) {
		super(model, columnModel);
		this.model = model;
		
		model.addTableLoaderListener(this);
		busyModel = model.isLoadingData();
		addPropertyChangeListener("selectedRows", this);
		doWidthHacks = true;
	}
	
	/**
	 * Laster brukerinnstillinger på tabellen
	 */
	public void loadUserSettings(){
		try{
		List<TableColumn> columns = getColumns(true);
		List<ColumnPreferences> columnPreferences = userSettings.getTablePreferences();
		//ingen innstillinger? lagre defaults.
		if (userSettings.getDefaultPreferences()==null){
			saveDefaultSettings();
			return;
		}
		if (columnPreferences==null)
			return;
		
		//henter ut alle kolonnene og fjerner dem fra modellen  
		Map<Object,TableColumn> colMap = new HashMap<Object,TableColumn>();
		for (TableColumn t:columns){
			colMap.put(t.getIdentifier(), t);
			getColumnModel().removeColumn(t);
		}
		//noe er galt med settings fjern dem
		//TODO bruker må starte modulen på nytt for å få tilbake kolonnene...
		if (userSettings.getDefaultPreferences().size()==0&&colMap.size()>0 && columnPreferences.size()==0){
			removeColumnStateSupport();
			return;
		}
		
		//legger dem til i riktig rekkefølge og sett synlighet, bredder
		Collections.sort(columnPreferences);
		for (ColumnPreferences t:columnPreferences){
			TableColumnExt tc = (TableColumnExt)colMap.get(t.getId());
			getColumnModel().addColumn(tc);
			tc.setVisible(t.isVisible());
			tc.setPreferredWidth(t.getWidth());
			tc.setMinWidth(5);
			colMap.remove(t.getId());
		}
		//evt nye kolonner?
		if (!colMap.isEmpty()){
			Set<Object> keySet = colMap.keySet();
			for (Object o:keySet){
				TableColumnExt notInPrefs = (TableColumnExt)colMap.get(o);
				getColumnModel().addColumn(notInPrefs);
				notInPrefs.setVisible(true);
				notInPrefs.setPreferredWidth(10);
				notInPrefs.setMinWidth(5);
			}
		}
		}catch (Throwable t){
			logger.error("klarte ikke laste brukerinnstillinger, resetter", t);
			removeColumnStateSupport();
		}
		
	}

	/**
	 * Lagrer standardinnstillinger for tabellen
	 */
	private void saveDefaultSettings() {
		List<TableColumn> columns = getColumns(true);
		List<ColumnPreferences> defaults = new ArrayList<ColumnPreferences>();
		for (int i=0;i<columns.size();i++){
			TableColumnExt col = (TableColumnExt)columns.get(i);
			int idx = convertColumnIndexToView(col.getModelIndex());
			ColumnPreferences pref = new ColumnPreferences();
			pref.setId(col.getIdentifier());
			pref.setWidth(col.getPreferredWidth());
			pref.setVisible(col.isVisible());
			pref.setOrder(idx);
			defaults.add(pref);
		}
		userSettings.setDefaultPreferences(defaults);
		userSettings.saveDefaults();
	}
	
	/**
	 * Tilbakestiller brukerens innstillinger for tabellen
	 */
	public void resetColumnStateSupport(){
		if (userSettings!=null){
			userSettings.loadDefaults();
			userSettings.save();
			loadUserSettings();
		}
	}
	private void removeColumnStateSupport(){
		userSettings.remove();
	}
	@SuppressWarnings("serial")
	private AbstractAction getSaveUserDataAction(){
		return new AbstractAction("Lagre kolonneoppsett"){
			@Override
			public void actionPerformed(ActionEvent e) {
				saveUserSettings();
			}
		};
	}
	@SuppressWarnings("serial")
	private AbstractAction getRestoreUserDataAction(){
		return new AbstractAction("Tilbakestill kolonneoppsett"){
			@Override
			public void actionPerformed(ActionEvent e) {
				resetColumnStateSupport();
			}
		};
	}
	
	/**
	 * Legger til støtte for at brukeren skal kunne lagre innstillinger for tabellen. 
	 * Lagrer posisjon, bredde og synlighet for kolonnene
	 * 
	 * @param columnStateSupportName navnet på tabellen (lagres som en property)
	 * @param plugin som har en peker til loaderen for å hente ut brukerinnstillingene
	 */
	public void addColumnStateSupport(String columnStateSupportName, MipssPlugin plugin){
		getTableHeader().addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton()==MouseEvent.BUTTON3){
					JPopupMenu menu = new JPopupMenu();
					JMenuItem save = new JMenuItem(getSaveUserDataAction());
					JMenuItem restore = new JMenuItem(getRestoreUserDataAction());
					menu.add(save);
					menu.add(restore);
					menu.show(JMipssBeanTable.this, e.getX(), e.getY());
				}
			}
		});
		userSettings = new MipssTablePreferences(columnStateSupportName, plugin);
	}
	
	/**
	 * Lagrer brukerens innstillinger
	 */
	private void saveUserSettings() {
		List<ColumnPreferences> tp = new ArrayList<ColumnPreferences>();
		List<TableColumn> columns = getColumns(true);
		boolean[] visibleState = new boolean[columns.size()];

		for (int i=0;i<columns.size();i++){
			TableColumnExt col = (TableColumnExt)columns.get(i);
			visibleState[i]=col.isVisible();
			col.setVisible(true);
		}
		
		for (int i=0;i<columns.size();i++){
			TableColumnExt col = (TableColumnExt)columns.get(i);
			int idx = convertColumnIndexToView(col.getModelIndex());
			ColumnPreferences pref = new ColumnPreferences();
			pref.setId(col.getIdentifier());
			pref.setWidth(col.getPreferredWidth());
			pref.setVisible(visibleState[i]);
			pref.setOrder(idx);
			tp.add(pref);
		}
		userSettings.setTablePreferences(tp);
		userSettings.save();
		loadUserSettings();
	}
	
	public void setDoWidthHacks(boolean doWidthHacks) {
		this.doWidthHacks = doWidthHacks;
	}
	
	private void closeWorkbook(WritableWorkbook workbook) {
		try {
			workbook.write();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		try {
			workbook.close();
		} catch (WriteException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	private int computeNewColWidth(int colWidth, Object data) {
		int width = 0;
		if (data instanceof String) {
			width = ((String) data).length();
			width = width > 200 ? 200 : width;
		} else if (data instanceof Long) {
			width = String.valueOf((Long) data).length();
		} else if (data instanceof Double) {
			width = String.valueOf((Double) data).length();
		} else if (data instanceof Integer) {
			width = String.valueOf((Integer) data).length();
		} else if (data instanceof Date) {
			width = MipssDateFormatter.SHORT_DATE_TIME_FORMAT.length();
		} else if (data instanceof Boolean) {
			width = 2;
		}

		return Math.max(colWidth, width);
	}

	private File createTempFile(String prefix, String suffix) {
		File tempFile = null;
		try {
			tempFile = File.createTempFile(prefix, suffix);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return tempFile;
	}

	private WritableWorkbook createWorkbook(File tempFile) {
		WritableWorkbook workbook = null;
		try {
			workbook = Workbook.createWorkbook(tempFile);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return workbook;
	}

	private void excelAddCell(WritableSheet sheet, WritableCell cell) {
		try {
			sheet.addCell(cell);
		} catch (RowsExceededException e) {
			throw new IllegalStateException(e);
		} catch (WriteException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Eksporterer innholdet i denne tabellen til Excel format
	 * 
	 * @param exportType om alle dataene skal med eller om kun valgte rader skal med. 
	 * 					 dersom kun valgte rader skal med og ingen rader er markert, vil ingen rader skrives til Excelarket
	 * @return
	 */
	public File exportToExcel(EXPORT_TYPE exportType) {
		File tempFile = createTempFile("tabellEksport", ".xls");
		WritableWorkbook workbook = createWorkbook(tempFile);
		WritableSheet sheet = workbook.createSheet("Tittel", 0);
		writeExcelHeaders(sheet);
		writeExcelData(sheet, exportType);
		closeWorkbook(workbook);

		return tempFile;
	}

	/**
	 * Gir ut visse rader
	 * 
	 * @param rows
	 * @return
	 */
	public List<T> getEntities(int[] rows) {
		List<T> entities = new ArrayList<T>();
		if (rows != null && rows.length > 0) {
			for (int r : rows) {
				int row = convertRowIndexToModel(r);
				if(row >= 0 && row < model.getRowCount()) {
					entities.add((T) model.get(row));
				}
			}
		}

		return entities;
	}
	
	public void setModel(MipssBeanTableModel model){
		super.setModel(model);
		this.model = model;
	}

	
	public boolean getFillsViewportWidth() {
		return fillsViewportWidth;
	}

	/**
	 * Antall valgte rader i tabellen
	 * 
	 * @return
	 */
	public int getNoOfSelectedRows() {
		rows = getSelectedRowCount();

		return rows;
	}

	/**
	 * Avkobler fra JTable sin implementasjon som ikke endrer størrelsen på
	 * kolloner når AUTORESIZE_OFF er satt.
	 * 
	 */
	@Override
	public boolean getScrollableTracksViewportWidth() {
		if(doWidthHacks) {
			return getFillsViewportWidth() && getParent() instanceof JViewport
				&& (((JViewport) getParent()).getWidth() > getPreferredSize().width);
		} else {
			return super.getScrollableTracksViewportWidth();
		}
	}

	/**
	 * Gir ut valgte rader
	 * 
	 */
	public List<T> getSelectedEntities() {
		return getEntities(getSelectedRows());
	}

	public int[] getSelectedRowIndices() {
		return selectedRowIndices;
	}

	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		busyModel = false;
		setEnabled(true);
		repaint();
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		busyModel = true;
		setEnabled(false);
		repaint();
	}

	/** {@inheritDoc} */
	@Override
	public void processMouseEvent(MouseEvent event) {
		// Dette er et hack slik at andre komponenter kan velge raden
		// som vises paa et musepunkt :)
		super.processMouseEvent(event);
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == this && StringUtils.equals("selectedRows", evt.getPropertyName())) {
			int[] old = (int[]) evt.getOldValue();
			int[] now = (int[]) evt.getNewValue();
			firePropertyChange("selectedEntities", getEntities(old), getEntities(now));
		}
	}

	public void reSortTable() {
		TableModelEvent e = new TableModelEvent(model);
		this.tableChanged(e);
	}

	/**
	 * Logikk for høyreklikk valg i tabellen Sørger for at valgte rader i
	 * tabellen oppfører seg logisk:
	 * 
	 * <ol>
	 * <li>Hvis ingen rader er valg, velg raden under musen i det høyremusknapp
	 * trykkes</li>
	 * <li>Hvis en eller flere rader er valgt, og raden under musen er med i
	 * utvalget: Da skal utvalget beholdes</li>
	 * <li>Hvis en eller flere rader er valgt, og raden under musen ikke er med
	 * i utvalget: Velg da kun raden under musen</li>
	 * </ol>
	 * 
	 * Metoden lagrer utvalget i selection variabelen.
	 */
	public List<T> selectMouseSelection(MouseEvent e) {
		int[] rows = getSelectedRows();
		List<T> selection = null;

		// Sjekk om noe er valgt i tabellen
		if (rows.length == 0) {
			// Hvis ikke -> velg rad under muspeker
			selectRow(e);
			selection = getSelectedEntities();
		} else {
			// Hvis --> sjekk at raden under musa er med i utvalget
			selection = getSelectedEntities();
			int row = rowAtPoint(e.getPoint());
			if (row != -1) {
				T underMouse = (T) model.get(row);
				if (!selection.contains(underMouse)) {
					// Hvis ikke -> velg rad under muspeker
					selectRow(e);
					selection = getSelectedEntities();
				}
			}
		}

		return selection;
	}

	/**
	 * Setter valgt rad i tabellen til den som er under musepekeren i musevent
	 * 
	 */
	public void selectRow(MouseEvent e) {
		// Sørg for at raden der musa er klikkes på med venstre...
		int index = rowAtPoint(e.getPoint());
		if (index != -1) {
			setRowSelectionInterval(index, index);
		}
	}

	public void setFillsViewportWidth(boolean fillsViewportWidth) {
		boolean old = this.fillsViewportWidth;
		this.fillsViewportWidth = fillsViewportWidth;
		resizeAndRepaint();
		firePropertyChange("fillsViewportWidth", old, fillsViewportWidth);
	}

	/** {@inheritDoc} */
	@Override
	public void valueChanged(ListSelectionEvent evt) {
		logger.debug("valueChanged(" + evt + ") start");
		super.valueChanged(evt);
		if (!evt.getValueIsAdjusting()) {
			int old = rows;
			rows = getSelectedRowCount();
			int[] oldIndices = this.selectedRowIndices;
			selectedRowIndices = getSelectedRows();
			firePropertyChange("selectedRowIndices", oldIndices, selectedRowIndices);
			firePropertyChange("selectedRows", oldIndices, selectedRowIndices);
			firePropertyChange("noOfSelectedRows", old, rows);
		}

		logger.debug("valueChanged(" + evt + ") done with " + rows);
	}

	/**
	 * Skriv denne tabellen til Excelarket
	 * @param sheet Excelarket som skal skrives i
	 * @param exportType om ALLE data skal med eller om kun valgte rader skal med. 
	 */
	private void writeExcelData(WritableSheet sheet, EXPORT_TYPE exportType) {
		Map<Class<?>, WritableCellFormat> formats = new HashMap<Class<?>, WritableCellFormat>();

		MipssRenderableEntityTableColumnModel columnModel = (MipssRenderableEntityTableColumnModel) getColumnModel();
		for (TableColumn column : columnModel.getColumns(false)) {
			TableColumnExt columnEx = columnModel.getColumnExt(column.getIdentifier());

			if (!columnEx.isVisible()) {
				continue;
			}
			int col = columnModel.getColumnIndex(column.getIdentifier());
			int columnIndex = convertColumnIndexToModel(col);
			int colWidth = 0;
			Class<?> clazz = model.getColumnClass(columnIndex);
			int row = 1;
			for (int i = 1; i - 1 < model.getRowCount(); i++) {
				
				int rowIndex = convertRowIndexToModel(i - 1);
				if (exportType==EXPORT_TYPE.SELECTED_ROWS_ONLY){
					if (!this.isRowSelected(i-1)){
						continue;
					}
				}
				Object data = model.getValueAt(rowIndex, columnIndex);
				
				if (data != null) {
					if (clazz.equals(String.class)) {
						excelAddCell(sheet, new Label(col, row, (String) data));
					} else if (clazz.equals(Long.class)) {
						WritableCellFormat integerFormat = formats.get(Long.class);
						if (integerFormat == null) {
							integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
							formats.put(Long.class, integerFormat);
						}
						excelAddCell(sheet, new jxl.write.Number(col, row, (Long) data, integerFormat));
					} else if (clazz.equals(Double.class)) {
						WritableCellFormat floatFormat = formats.get(Double.class);
						if (floatFormat == null) {
							floatFormat = new WritableCellFormat(NumberFormats.FLOAT);
							formats.put(Double.class, floatFormat);
						}
						excelAddCell(sheet, new jxl.write.Number(col, row, (Double) data, floatFormat));
					} else if (clazz.equals(Integer.class)) {
						WritableCellFormat integerFormat = formats.get(Integer.class);
						if (integerFormat == null) {
							integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
							formats.put(Integer.class, integerFormat);
						}
						excelAddCell(sheet, new jxl.write.Number(col, row, (Integer) data, integerFormat));
						
					} 
					else if (clazz.equals(Date.class)) {
						WritableCellFormat dateFormat = formats.get(Date.class);
						if (dateFormat == null) {
							DateFormat customDateFormat = new DateFormat(MipssDateFormatter.SHORT_DATE_TIME_FORMAT);
							dateFormat = new WritableCellFormat(customDateFormat);
							formats.put(Date.class, dateFormat);
						}

						excelAddCell(sheet, new DateTime(col, row, (Date) data, dateFormat));
					} else if (clazz.equals(Boolean.class)) {
						WritableCellFormat integerFormat = formats.get(Integer.class);
						if (integerFormat == null) {
							integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
							formats.put(Integer.class, integerFormat);
						}
						Boolean value = (Boolean) data;
						Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
						excelAddCell(sheet, new jxl.write.Number(col, row, number , integerFormat));
					} else{
						try{
							WritableCellFormat integerFormat = formats.get(Integer.class);
							if (integerFormat == null) {
								integerFormat = new WritableCellFormat(NumberFormats.INTEGER);
								formats.put(Integer.class, integerFormat);
							}
							excelAddCell(sheet, new jxl.write.Number(col, row, Integer.parseInt(data.toString()), integerFormat));
						}catch (NumberFormatException e){
							try{
								Double.parseDouble(data.toString());
								WritableCellFormat floatFormat = formats.get(Double.class);
								if (floatFormat == null) {
									floatFormat = new WritableCellFormat(NumberFormats.FLOAT);
									formats.put(Double.class, floatFormat);
								}
								excelAddCell(sheet, new jxl.write.Number(col, row, Double.parseDouble(data.toString()), floatFormat));
							}catch (NumberFormatException ex){
								try{
									excelAddCell(sheet, new Label(col, row, data.toString()));
								} catch (Throwable e1){
									logger.error("Klarte ikke å eksportere data til excel:"+data);
								}
							}
						}
						
					}
					
					colWidth = computeNewColWidth(colWidth, data);
				}
				row++;
			}
			CellView cellView = sheet.getColumnView(col);
			if (colWidth > (cellView.getSize() / 256)) {
				sheet.setColumnView(col, colWidth);
			}
		}
	}

	private void writeExcelHeaders(WritableSheet sheet) {
		TableColumnModelExt columnModel = (TableColumnModelExt) getColumnModel();
		for (int columnIndex = 0; columnIndex < columnModel.getColumnCount(true); columnIndex++) {
			TableColumnExt tc = (TableColumnExt) columnModel.getColumn(columnIndex);
			if (tc.isVisible()) {
				WritableFont times10font = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, true);
				WritableCellFormat times10format = new WritableCellFormat(times10font);
				String title = (String) tc.getHeaderValue();
				title = removeHtmlFormatting(title);
				excelAddCell(sheet, new Label(columnIndex, 0, title, times10format));
				sheet.setColumnView(columnIndex, title.length());
			}
		}
	}
	
	public String[] getExcelHeaders(){
		List<String> excelHeaders = new ArrayList<String>();
		TableColumnModelExt columnModel = (TableColumnModelExt) getColumnModel();
		for (int columnIndex = 0; columnIndex < columnModel.getColumnCount(true); columnIndex++) {
			TableColumnExt tc = (TableColumnExt) columnModel.getColumn(columnIndex);
			if (tc.isVisible()) {
				String title = (String) tc.getHeaderValue();
				title = removeHtmlFormatting(title);
				excelHeaders.add(title);
			}
		}
		return excelHeaders.toArray(new String[]{});
	}
	private static String removeHtmlFormatting(String title){
		String htmlRemoved = title.replaceAll("\\<.*?\\>", "");
		return htmlRemoved;
	}
}
