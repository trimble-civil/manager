package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
/**
 * En JScrollPane som inneholder en tabell med funksjonalitet for å velge alle/ingen rader i modellen. Brukes av tabeller
 * som inneholder checkbokser som indikator på valg av rader.  JScrollPanen har tabellen som viewport
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 * @param <T>
 */
public class JCheckTablePanel<T extends CheckBoxTableObject> extends JScrollPane{
	private JXBusyLabel busyLabel;
	private JXTable table;
	private MipssRenderableEntityTableModel<T> model;
	private JCheckBox alleCheck;
	private boolean singleSelection;
	private TableModelListener singleSelectionListener;
	
	public JCheckTablePanel(final JXTable table, final MipssRenderableEntityTableModel<T> model, final JCheckBox alleCheck){
		this(table, model, alleCheck, false);
	}
	
	
	public JCheckTablePanel(final JXTable table, final MipssRenderableEntityTableModel<T> model, final JCheckBox alleCheck, String waitText, boolean singleSelection){
		this(table, model, alleCheck, singleSelection);
		busyLabel = new JXBusyLabel();
		busyLabel.setText(waitText);
	}
	public JCheckTablePanel(final JXTable table, final MipssRenderableEntityTableModel<T> model, final JCheckBox alleCheck, boolean singleSelection){
		super(table);
		this.table = table;
		this.singleSelection = singleSelection;
		this.alleCheck = alleCheck;
		setModel(model);
		init();
	}
	private void init(){
		table.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (!isAllTableObjectsSelected(model)){
					alleCheck.setSelected(false);
				}else
					alleCheck.setSelected(true);
			}
		});
		
		alleCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JCheckBox box =(JCheckBox)e.getSource();
				if (box.isSelected()){
					selectAllTableObjects(model, true);
				}else
					selectAllTableObjects(model, false);
				table.repaint();
			}
		});
		singleSelectionListener = new TableModelListener(){
			@Override
			public void tableChanged(TableModelEvent e) {
				selectAllTableObjects(model, false);
				T t = model.getEntities().get(e.getFirstRow());
				t.setValgt(true);
				table.repaint();
				
			}
		};
	}
	
	
	
	public void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		table.setVisible(!busy);
		this.setViewportView(busy?busyLabel:table);
		repaint();
	}
	
	/**
	 * Sjekker om alle radene i tabellen er valgt. 
	 * @param model
	 * @return
	 */
	private boolean isAllTableObjectsSelected(MipssRenderableEntityTableModel<? extends CheckBoxTableObject> model){
		if (model.getEntities().isEmpty())
			return false;
		for (CheckBoxTableObject o: model.getEntities()){
			if (!o.getValgt()){
				return false;
			}
		}
		return true;
	}
	public void setSelected(boolean selected){
		selectAllTableObjects(model, selected);
		alleCheck.setSelected(selected);
	}
	/**
	 * Sett alle radene i tabellen til valgt eller ikke valgt
	 * @param model
	 * @param selected
	 */
	private void selectAllTableObjects(MipssRenderableEntityTableModel<? extends CheckBoxTableObject> model, boolean selected){
		int rowCount = table.getRowCount();
		for (int i=0;i<rowCount;i++){
			int rowIndex = table.convertRowIndexToModel(i);
			CheckBoxTableObject o = model.get(rowIndex);
			o.setValgt(selected);
		}
		/*
		for (CheckBoxTableObject o: model.getEntities()){
			o.setValgt(selected);
		}*/
	}
	
	public List<T> getAlleValgte(){
		List<T> list = new ArrayList<T>();
		for (T o:model.getEntities()){
			if (o.getValgt())
				list.add(o);
		}
		return list;
	}
	
	public void setSingleSelection(boolean singleSelection){
		this.singleSelection = singleSelection;
		if (singleSelection){
			model.addTableModelListener(singleSelectionListener);
			table.setDefaultRenderer(Boolean.class, new DefaultTableRenderer(){
				@Override
				public Component getTableCellRendererComponent(JTable table,
						Object value, boolean isSelected, boolean hasFocus,
						int row, int column) {
					Component cr = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
					
					JRadioButton c = new JRadioButton();
					c.setOpaque(false);
					c.setBackground(cr.getBackground());
					c.setForeground(cr.getForeground());
					JPanel panel = new JPanel(new GridBagLayout());
					panel.setBackground(cr.getBackground());
					panel.setForeground(cr.getForeground());
					panel.add(c, new GridBagConstraints(0,0,0,0,1.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
					Boolean v = (Boolean)value;
					c.setSelected(v);
					return panel;
				}
			});

		}else{
			model.removeTableModelListener(singleSelectionListener);
			table.setDefaultRenderer(Boolean.class, new DefaultTableRenderer());
		}
	}
	
	public void setModel(final MipssRenderableEntityTableModel<T> model){
		this.model = model;
		if (singleSelection){
			model.addTableModelListener(new TableModelListener(){
	
				@Override
				public void tableChanged(TableModelEvent e) {
					selectAllTableObjects(model, false);
					T t = model.getEntities().get(e.getFirstRow());
					t.setValgt(true);
					table.repaint();
					
				}
			});
		}
	}
}
