package no.mesta.mipss.ui.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.persistence.mipssfield.Vind;

/**
 * Rendrer tabell celler som har vind
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class VindRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

    public VindRenderer() {
        super();
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if(value == null || !(value instanceof Vind)) {
            setText(null);
        } else {
            Vind vind = (Vind) value;
            String txt = (vind == null ? "" : vind.getTextForGUI());
            setText(txt);
        }
        
        return this;
    }
}