package no.mesta.mipss.ui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
/**
 * Klasse for å lage en drop down knapp som viser frem en kontekstmeny når man klikker på den
 * 
 * Kodens opprinnelse er fra jroller.com, 
 * bugfixes av:
 * Harald A. kulø
 */
public abstract class DropDownButton extends JToggleButton implements ChangeListener, PopupMenuListener, ActionListener, PropertyChangeListener{ 
    private final JToggleButton mainButton = this; 
    //private final ArrowButton arrowButton = new ArrowButton( new ImageIcon(getClass().getResource("/images/dropdown.png"))); 
    private final JButton arrowButton = new JButton(new ImageIcon(getClass().getResource("/images/dropdown.png"))); 
    private boolean popupVisible = false; 
 
    public DropDownButton(){ 
        mainButton.getModel().addChangeListener(this); 
        arrowButton.getModel().addChangeListener(this); 
        arrowButton.addActionListener(this); 
        arrowButton.setMargin(new Insets(4, 0, 4, 0)); 
        mainButton.addPropertyChangeListener("enabled", this); //NOI18N 
    } 
    public JButton getArrowButton(){
    	return arrowButton;
    }
    /*------------------------------[ PropertyChangeListener ]---------------------------------------------------*/ 
 
    public void propertyChange(PropertyChangeEvent evt){ 
        arrowButton.setEnabled(mainButton.isEnabled()); 
    } 
 
    /*------------------------------[ ChangeListener ]---------------------------------------------------*/ 
 
    public void stateChanged(ChangeEvent e){ 
        if(e.getSource()==mainButton.getModel()){ 
            if(popupVisible && !mainButton.getModel().isRollover()){ 
                mainButton.getModel().setRollover(true); 
                return; 
            } 
            arrowButton.getModel().setRollover(mainButton.getModel().isRollover()); 
            arrowButton.setSelected(mainButton.getModel().isArmed() && mainButton.getModel().isPressed()); 
        }else{ 
            if(popupVisible && !arrowButton.getModel().isSelected()){ 
                arrowButton.getModel().setSelected(true); 
                return; 
            } 
            mainButton.getModel().setRollover(arrowButton.getModel().isRollover()); 
        } 
    } 
 
    /*------------------------------[ ActionListener ]---------------------------------------------------*/ 
 
    public void actionPerformed(ActionEvent ae){ 
         JPopupMenu popup = getPopupMenu(); 
         popup.addPopupMenuListener(this); 
         popup.show(mainButton, 0, mainButton.getHeight()); 
     } 
 
    /*------------------------------[ PopupMenuListener ]---------------------------------------------------*/ 
 
    public void popupMenuWillBecomeVisible(PopupMenuEvent e){ 
        popupVisible = true; 
        mainButton.getModel().setRollover(true); 
        arrowButton.getModel().setSelected(true); 
    } 
 
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e){ 
        popupVisible = false; 
 
        mainButton.getModel().setRollover(false); 
        arrowButton.getModel().setSelected(false); 
        ((JPopupMenu)e.getSource()).removePopupMenuListener(this); // act as good programmer :)
    } 
 
    public void popupMenuCanceled(PopupMenuEvent e){ 
        popupVisible = false; 
    } 
    @Override
    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
        arrowButton.setPreferredSize(new Dimension(arrowButton.getPreferredSize().width, preferredSize.height));
    }
    /*------------------------------[ Other Methods ]---------------------------------------------------*/ 
 
    protected abstract JPopupMenu getPopupMenu(); 
 
    public JToggleButton addToToolBar(JToolBar toolbar){ 
            JToolBar tempBar = new JToolBar(); 
            tempBar.setAlignmentX(0.5f);
            tempBar.setRollover(true); 
            tempBar.add(mainButton); 
            tempBar.add(arrowButton); 
            tempBar.setFloatable(false); 
            toolbar.add(tempBar); 
            return mainButton; 
        } 
    public JPanel getToolbar(){
//    	JToolBar tempBar = new JToolBar(); 
//        tempBar.setAlignmentX(0.5f);
//        tempBar.setRollover(true);
//        
//        tempBar.add(mainButton); 
//        tempBar.add(arrowButton); 
//        tempBar.setFloatable(false);  
//        return tempBar;
    	JPanel panel = new JPanel();
        panel.add(mainButton);
        panel.add(arrowButton);
        return panel;
    }
    final class ArrowButton extends JButton{
        private Color darkShadow = new Color(50,50,50);
        private Color highlight = new Color(150,150,150);
        private Color shadow = new Color(100,100,100);
        private Image arrow = new ImageIcon(getClass().getResource("/images/dropdown.png")).getImage();
        
        public ArrowButton(ImageIcon arrow){
            super(arrow);
        }
        
        public void paint(Graphics g){
            super.paint(g);
            //g.drawImage(arrow, 0, 0, this);
            //paintTriangle(g, getWidth()/2, getHeight()/2, 3, isEnabled());
        }
        
        public void paintTriangle(Graphics g, int x, int y, int size, boolean isEnabled) {
            Color oldColor = g.getColor();
            int mid, i, j;

            j = 0;
            size = Math.max(size, 2);
            mid = (size / 2) - 1;
        
            g.translate(x, y);
            if(isEnabled)
                g.setColor(darkShadow);
            else
                g.setColor(shadow);

            if(!isEnabled)  {
                g.translate(1, 1);
                g.setColor(highlight);
                for(i = size-1; i >= 0; i--)   {
                    g.drawLine(mid-i, j, mid+i, j);
                    j++;
                }
                g.translate(-1, -1);
                g.setColor(shadow);
            }
            
            j = 0;
            for(i = size-1; i >= 0; i--)   {
                g.drawLine(mid-i, j, mid+i, j);
                j++;
            }
             
            g.translate(-x, -y);        
            g.setColor(oldColor);
        }
    }
}
