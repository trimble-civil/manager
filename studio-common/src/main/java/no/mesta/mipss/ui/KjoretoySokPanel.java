package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import no.mesta.driftkontrakt.bean.KjoretoygruppeService;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mipssmapserver.KjoretoyVO;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.KjoretoyTableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.RowPatternFilter;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.Filter;
import org.jdesktop.swingx.decorator.FilterPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class KjoretoySokPanel extends JPanel implements PropertyChangeSource{
	private static final Logger log = LoggerFactory.getLogger(KjoretoySokPanel.class);

	private static final PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	
	private JRadioButton kunKontraktRadio = new JRadioButton(resources.getGuiString("KjoretoyPanel.kunKontraktRadio.text"));
	private JRadioButton alleRadio = new JRadioButton(resources.getGuiString("KjoretoyPanel.alleRadio.text"));
	
	private JCheckTablePanel<KjoretoyTableObject> tableScroll;
	private MipssBeanTableModel<KjoretoyTableObject> kjoretoyTableModel;
	private String[] columns = new String[] { "valgt", "enhetId", "beskrivelse", "type", "modell", "leverandor", "regnr" };
	
	private JXTable kjoretoyTable;
	private JLabel sokLabel;
	private JTextField sokField;
	private JCheckBox velgAlleCheck;
	private JLabel antallKjoretoyLabel;
	private Driftkontrakt valgtKontrakt;
	private MipssComboBoxModel<Kjoretoygruppe> modelGruppe;
	private JComboBox cmbGruppe;
	private JRadioButton radioGrupper;
	
	private boolean velgAlleCheckVisible=true;
	private boolean singleSelection;
	private boolean loaded;
	
	private KjoretoygruppeService gruppeBean = BeanUtil.lookup(KjoretoygruppeService.BEAN_NAME, KjoretoygruppeService.class);

	private boolean visKjoretoygrupper=true;
	private boolean inkluderPDA = true;

	public KjoretoySokPanel(Driftkontrakt initialKontrakt) {
		initComponents();
		setValgtKontrakt(initialKontrakt);
		initGui();
		getKjoretoy();
	}
	public KjoretoySokPanel(Driftkontrakt initialKontrakt, boolean inkluderPDA) {
		this.inkluderPDA = inkluderPDA;
		initComponents();
		setValgtKontrakt(initialKontrakt);
		initGui();
		getKjoretoy();
	}

	public KjoretoySokPanel(Driftkontrakt initialKontrakt, boolean velgAlleCheckVisible, boolean singleSelection, boolean visKjoretoygrupper, String[] columns) {
		this.columns = columns;
		this.visKjoretoygrupper = visKjoretoygrupper;
		initComponents();
		
		setValgtKontrakt(initialKontrakt);
		this.velgAlleCheckVisible = velgAlleCheckVisible;
		this.singleSelection = singleSelection;
		setLayout(new BorderLayout());
		initGui();
		if (initialKontrakt!=null)
			getKjoretoy();
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		if (visKjoretoygrupper){
			add(radioGrupper,		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,0), 0,0));
			add(cmbGruppe,			new GridBagConstraints(1,0, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,20,0,5), 0,0));
		}
		add(kunKontraktRadio,	new GridBagConstraints(0,1, 2,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,0), 0,0));
		add(alleRadio, 			new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,0), 0,0));
		add(sokLabel,			new GridBagConstraints(2,2, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		add(sokField,			new GridBagConstraints(3,2, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		add(tableScroll, 	    new GridBagConstraints(0,3, 4,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
		if (velgAlleCheckVisible)
			add(velgAlleCheck, 	    new GridBagConstraints(0,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,00,5,0), 0,0));
		if (!singleSelection)//TODO singleSelection er ikke nødvendigvis et kriterie for å ikke vise antallKjoretoy
			add(antallKjoretoyLabel,new GridBagConstraints(2,4, 2,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,00,5,5), 0,0));
	}

	
	private void initComponents(){
		sokLabel = new JLabel(resources.getGuiString("KjoretoyPanel.sok.text"));
		antallKjoretoyLabel = new JLabel(resources.getGuiString("KjoretoyPanel.antallKjoretoyLabel.text"));
		sokField = new JTextField();
		Dimension fieldSize = new Dimension(130, 20);
		sokField.setPreferredSize(fieldSize);
		sokField.setMaximumSize(fieldSize);
		sokField.setMinimumSize(fieldSize);
		velgAlleCheck = new JCheckBox(resources.getGuiString("KjoretoyPanel.velgAlleCheck.text"));
		
		kjoretoyTable = createKjoretoyTable();
		tableScroll = new JCheckTablePanel<KjoretoyTableObject>(
				kjoretoyTable,
				kjoretoyTableModel,
				velgAlleCheck,
				resources.getGuiString("KjoretoyPanel.TableWaitScroller.title"), singleSelection);
		
		modelGruppe = new MipssComboBoxModel<Kjoretoygruppe>(new ArrayList<Kjoretoygruppe>(), true);
		cmbGruppe = new JComboBox(modelGruppe);
		fieldSize = new Dimension(153, 20);
		cmbGruppe.setPreferredSize(fieldSize);
		cmbGruppe.setMaximumSize(fieldSize);
		cmbGruppe.setMinimumSize(fieldSize);
		
		cmbGruppe.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				
				Kjoretoygruppe gruppe = (Kjoretoygruppe)cmbGruppe.getSelectedItem();
				if (gruppe==null){
					kjoretoyTable.setFilters(null);
					return;
				}
				List<String> kjoretoy = gruppeBean.getKjoretoynavn(gruppe.getId());
				String filter = "("+StringUtils.join(kjoretoy, "|")+")";
				Pattern pattern = Pattern.compile(filter.toString(),Pattern.CASE_INSENSITIVE);
				RowPatternFilter pf = new RowPatternFilter(pattern,kjoretoyTable.getColumnCount());
				FilterPipeline fp = new FilterPipeline(new Filter[] { pf });
				kjoretoyTable.setFilters(fp);
				velgAlleCheck.setEnabled(true);
				velgAlleCheck.setSelected(false);
			}
		});
		MipssListCellRenderer<Kjoretoygruppe> renderer = new MipssListCellRenderer<Kjoretoygruppe>();
        cmbGruppe.setRenderer(renderer);
        cmbGruppe.setPreferredSize(new Dimension(250,20));
        cmbGruppe.setEnabled(false);
		radioGrupper = new JRadioButton(resources.getGuiString("KjoretoyPanel.grupper"));
		
		addActionListenerRadio(kunKontraktRadio);
		addActionListenerRadio(alleRadio);
		alleRadio.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tableScroll.setSelected(false);
				JRadioButton b = (JRadioButton) e.getSource();
				if (b.isSelected()) {
					sokField.setEnabled(true);
					kjoretoyTable.setFilters(null);
					velgAlleCheck.setSelected(false);
					sokField.setText("");
				} else
					sokField.setEnabled(false);
			}
		});
		radioGrupper.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				tableScroll.setSelected(false);
				velgAlleCheck.setSelected(false);
				if (radioGrupper.isSelected()){
					cmbGruppe.setEnabled(true);
					sokField.setEnabled(false);
				}else{
					cmbGruppe.setEnabled(false);
					velgAlleCheck.setEnabled(true);
					sokField.setEnabled(true);
				}
			}
		});
		
		ButtonGroup group = new ButtonGroup();
		group.add(kunKontraktRadio);
		group.add(alleRadio);
		group.add(radioGrupper);
		kunKontraktRadio.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tableScroll.setSelected(false);
				velgAlleCheck.setSelected(false);
				sokField.setText("");
				kjoretoyTable.setFilters(null);
			}
		});
		sokField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				tableScroll.setSelected(false);
				velgAlleCheck.setSelected(false);
				String filter = sokField.getText();
				Pattern pattern = Pattern.compile(filter,Pattern.CASE_INSENSITIVE);
				RowPatternFilter pf = new RowPatternFilter(pattern,kjoretoyTable.getColumnCount());
				FilterPipeline fp = new FilterPipeline(new Filter[] { pf });
				kjoretoyTable.setFilters(fp);
			}
		});
		sokField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				tableScroll.setSelected(false);
				velgAlleCheck.setSelected(false);
				String filter = sokField.getText();
				Pattern pattern = Pattern.compile(filter,Pattern.CASE_INSENSITIVE);
				RowPatternFilter pf = new RowPatternFilter(pattern,kjoretoyTable.getColumnCount());
				FilterPipeline fp = new FilterPipeline(new Filter[] { pf });
				kjoretoyTable.setFilters(fp);
			}
		});
	}
	
	
	private JXTable createKjoretoyTable() {
		
		kjoretoyTableModel = new MipssBeanTableModel<KjoretoyTableObject>(KjoretoyTableObject.class, new int[] { 0 }, columns);
		MipssRenderableEntityTableColumnModel kjoretoyColumnModel = new MipssRenderableEntityTableColumnModel(KjoretoyTableObject.class, kjoretoyTableModel.getColumns());
		
		final JXTable table = new JXTable(kjoretoyTableModel,kjoretoyColumnModel) {
			@Override
			public boolean getScrollableTracksViewportWidth() {
				return true && getParent() instanceof JViewport && (((JViewport) getParent()).getWidth() > getPreferredSize().width);
			}
			protected boolean shouldSortOnChange(TableModelEvent e) {
				return false;
			}
		};
		table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				DefaultListSelectionModel m = (DefaultListSelectionModel)e.getSource();
				int index = m.getLeadSelectionIndex();
				KjoretoyTableObject kjoretoy = null;
				if (index!=-1&& index<getKjoretoyTable().getModel().getRowCount()){
					int modelIndex = getKjoretoyTable().convertRowIndexToModel(index);
					if (kjoretoyTableModel.getEntities().size()>index)
						kjoretoy = kjoretoyTableModel.getEntities().get(modelIndex);
				} 
				firePropertyChange("kjoretoy", kjoretoy);
			}
		});
		
		table.setFillsViewportHeight(true);
		setColumnWidth(table.getColumn(0), 40);
		setColumnWidth(table.getColumn(1), 60);
		setColumnWidth(table.getColumn(3), 60);
		setColumnWidth(table.getColumn(4), 60);
		table.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
		return table;
	}
	
	private void addActionListenerRadio(JRadioButton b) {
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getKjoretoy();
			}
		});
	}
	
	public void getKjoretoy() {
		if (kunKontraktRadio.isSelected()) {
			if (valgtKontrakt!=null){
				getKontraktKjoretoy();
				velgAlleCheck.setEnabled(true);
			}else{
				kunKontraktRadio.setSelected(false);
			}
		} else if (alleRadio.isSelected()) {
			velgAlleCheck.setSelected(false);
			velgAlleCheck.setEnabled(false);
			getAlleKjoretoy();
		}
	}
	
	private void getKontraktKjoretoy() {
		tableScroll.setBusy(true);
		final SwingWorker<List<KjoretoyTableObject>, Void> w = new SwingWorker<List<KjoretoyTableObject>, Void>() {
			@Override
			protected List<KjoretoyTableObject> doInBackground() throws Exception {
				try{
				MipssMap bean = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
				List<KjoretoyVO> kjoretoyList = bean.getKjoretoyPdaForKontrakt(getValgtKontrakt().getId());
				return convertKjoretoyVOToTableObjects(kjoretoyList);
				} catch (Throwable t){
					t.printStackTrace();
				}
				return null;
			}
			@Override
			protected void done(){
				tableScroll.setBusy(false);
				kjoretoyTableModel.getEntities().clear();
				try{
					List<KjoretoyTableObject> c = get();
					if(c != null) {
						kjoretoyTableModel.getEntities().addAll(c);
					} else {
						log.error("Unable to find vehicles");
					}
				}catch(Exception e){
					e.printStackTrace();//ignore...
				}
				antallKjoretoyLabel.setText(resources.getGuiString("KjoretoyPanel.antallKjoretoyLabel.text")+ kjoretoyTableModel.getEntities().size());
			}
		};
		w.execute();
		new Thread(){
			public void run(){
				try {
					w.get();
					loaded = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
		
	}
	private void getAlleKjoretoy() {
		tableScroll.setBusy(true);
		final SwingWorker<List<KjoretoyTableObject>, Void> w = new SwingWorker<List<KjoretoyTableObject>, Void>() {
			private List<KjoretoyTableObject> tableList;
			@Override
			protected List<KjoretoyTableObject> doInBackground() throws Exception {
				
				MipssMap bean = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
				List<KjoretoyVO> kjoretoyList = bean.getAllKjoretoyPda();
				
				return convertKjoretoyVOToTableObjects(kjoretoyList);
			}
			@Override
			public void done(){
				tableScroll.setBusy(false);
				kjoretoyTableModel.getEntities().clear();
				try {
					kjoretoyTableModel.getEntities().addAll(get());
				} catch (Exception e) {
					e.printStackTrace();//ignore
				} 
				antallKjoretoyLabel.setText(resources.getGuiString("KjoretoyPanel.antallKjoretoyLabel.text")+ kjoretoyTableModel.getEntities().size());
				loaded=true;
			}
		};
		w.execute();
		new Thread(){
			public void run(){
				try {
					w.get();
					loaded = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	private void firePropertyChange(String property, Object newValue){
		for (PropertyChangeListener listener:getPropertyChangeListeners()){
			PropertyChangeEvent e = new PropertyChangeEvent(this, property, null, newValue);
			listener.propertyChange(e);
		}
	}
	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}
	
	@Override
	public void setEnabled(boolean enabled){
		kunKontraktRadio.setEnabled(enabled);
		alleRadio.setEnabled(enabled);
		kjoretoyTable.setEnabled(enabled);
	}
	
	public void setValgtKontrakt(Driftkontrakt valgtKontrakt) {
		if (valgtKontrakt!=this.valgtKontrakt){
			kunKontraktRadio.setSelected(true);
		}
		this.valgtKontrakt = valgtKontrakt;
		if (valgtKontrakt!=null)
			updateGrupper();
	}

	private void updateGrupper() {
		List<Kjoretoygruppe> gr = gruppeBean.getKjoretoygruppe(getValgtKontrakt().getId());
		modelGruppe.setEntities(gr);
	}
	public Driftkontrakt getValgtKontrakt() {
		return valgtKontrakt;
	}
	public JXTable getKjoretoyTable() {
		return kjoretoyTable;
	}
	
	public void selectAll(boolean selected){
		if (selected){
			while (!loaded){
				Thread.yield();
			}
		}
		tableScroll.setSelected(selected);
	}
	public void setSingleSelection(boolean singleSelection){
		tableScroll.setSingleSelection(singleSelection);
	}
	/**
	 * Returnerer alle valgte kjøretøy, dersom velg alle er valgt returneres
	 * alle kjøretøy fra tabellen
	 * 
	 * @return
	 */
	public List<KjoretoyVO> getSelectedKjoretoy() {
		List<KjoretoyVO> kjoretoyList = new ArrayList<KjoretoyVO>();
		KjoretoyTableObject[] selected = kjoretoyTableModel.getEntities().toArray(new KjoretoyTableObject[] {});

		if (velgAlleCheck.isSelected()) {
			for (KjoretoyTableObject o : selected) {
				if (o.getKjoretoy() != null)
					kjoretoyList.add(new KjoretoyVO(o.getKjoretoy()));
				else if (o.getPda() != null) {
					kjoretoyList.add(new KjoretoyVO(o.getPda()));
				}
			}
		} else {
			for (KjoretoyTableObject o : selected) {
				if (o.getValgt().booleanValue()) {
					if (o.getKjoretoy() != null)
						kjoretoyList.add(new KjoretoyVO(o.getKjoretoy()));
					else if (o.getPda() != null) {
						kjoretoyList.add(new KjoretoyVO(o.getPda()));
					}
				}
			}
		}
		return kjoretoyList;
	}

	
	private List<KjoretoyTableObject> convertKjoretoyVOToTableObjects(List<KjoretoyVO> kjoretoyList) {
		List<KjoretoyTableObject> tableList = new ArrayList<KjoretoyTableObject>();
		for (KjoretoyVO p : kjoretoyList) {
			if (p.getKjoretoy() != null) {
				if (p.getKjoretoy().getId() != null)
					tableList.add(new KjoretoyTableObject(p.getKjoretoy()));
			} else if (p.getPda() != null &&inkluderPDA) {
				KjoretoyTableObject k = new KjoretoyTableObject(p.getPda());
				k.setPdaNavn(p.getPdaNavn());
				tableList.add(k);
			}
		}
		return tableList;
	}
	public JRadioButton getKunKontraktRadio() {
		return kunKontraktRadio;
	}
	public boolean isLoaded(){
		return loaded;
	}
	public MipssRenderableEntityTableModel<KjoretoyTableObject> getKjoretoyTableModel() {
		return kjoretoyTableModel;
	}
	public void setSanntidMode() {
		kunKontraktRadio.setSelected(Boolean.TRUE);
//		innenforTidRadio.setSelected(Boolean.FALSE);
		alleRadio.setSelected(Boolean.FALSE);
		
		kunKontraktRadio.setEnabled(Boolean.TRUE);
//		innenforTidRadio.setEnabled(Boolean.FALSE);
		alleRadio.setEnabled(Boolean.FALSE);
		
		getKjoretoy();
		
	}

	public void setPeriodeMode() {
		kunKontraktRadio.setSelected(Boolean.TRUE);
//		innenforTidRadio.setSelected(Boolean.FALSE);
		alleRadio.setSelected(Boolean.FALSE);
		
		kunKontraktRadio.setEnabled(Boolean.TRUE);
//		innenforTidRadio.setEnabled(Boolean.FALSE);
		alleRadio.setEnabled(Boolean.TRUE);
		
		getKjoretoy();
		
	}
}
