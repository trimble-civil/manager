package no.mesta.mipss.ui.table;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.resources.images.IconResources;

/**
 * Rendrer
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class BooleanRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

	private ImageIcon icon;
	private final boolean revert;
	public BooleanRenderer() {
		this(IconResources.OK2_ICON, false);
	}
	
	public BooleanRenderer(ImageIcon icon, boolean revert){
		this.icon = icon;
		this.revert = revert;
	}

	/** {@inheritDoc} */
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(component.getBackground());
		panel.setForeground(component.getForeground());
		
		if (value != null && value instanceof Boolean) {
			Boolean b = (Boolean) value;
			boolean add = false;
			if (revert){
				if (!b){
					add = true;
				}
			}else{
				if (b) {
					add = true;
				}
			}
			if (add)
				panel.add(new JLabel(icon), BorderLayout.CENTER);
		}

		return panel;
	}
}