package no.mesta.mipss.ui.roadpicker;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.rodeservice.Rodegenerator;

@SuppressWarnings("serial")
public class KontraktVeinettPanel extends JPanel{
	private PropertyResourceBundleUtil res = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	private Driftkontrakt kontrakt;
	private boolean inkluderKm;
	private boolean alleValgt;
	private boolean collapsed;
	private boolean showLink;
	private KontraktveinettTaskPane veinettPane;
	private final boolean editableKm;
	private List<Veinettveireferanse> veiListe;
	private JRadioButton btnKontraktveinett;
	private JRadioButton btnAktiveRoder;
	private boolean utenReflinkseksjoner = true;
	private boolean aktiveRoder = false;
	private boolean rodevalgAktiv = true;
	public KontraktVeinettPanel(Driftkontrakt kontrakt, boolean inkluderKm, boolean alleValgt, boolean collapsed, boolean showLink, boolean editableKm){
		this.kontrakt = kontrakt;
		this.inkluderKm = inkluderKm;
		this.alleValgt = alleValgt;
		this.collapsed = collapsed;
		this.showLink = showLink;
		this.editableKm = editableKm;
		initComponents();
		initGui();
		hentKontraktvei(true);
	}

	public KontraktVeinettPanel(Driftkontrakt kontrakt, boolean inkluderKm, boolean alleValgt, boolean collapsed, boolean showLink, boolean editableKm, boolean utenReflinkseksjoner){
		this.kontrakt = kontrakt;
		this.inkluderKm = inkluderKm;
		this.alleValgt = alleValgt;
		this.collapsed = collapsed;
		this.showLink = showLink;
		this.editableKm = editableKm;
		this.utenReflinkseksjoner = utenReflinkseksjoner;
		initComponents();
		initGui();
		if (!utenReflinkseksjoner){
			rodevalgAktiv=false;
		}
		hentKontraktvei(utenReflinkseksjoner);
	}
	
	private void hentKontraktvei(final boolean utenReflinkseksjoner){
		new SwingWorker<Void,Void>() {
			private List<Veinettveireferanse> veinett;
			@Override
			public Void doInBackground(){
				veinettPane.setBusy(true);
		    	Rodegenerator bean= BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
		    	try{
		    		if (utenReflinkseksjoner){
		    			if (aktiveRoder){
		    				veinett = bean.getVeinettForStroBroyt(kontrakt.getId());
		    			}else{
		    				veinett = bean.getVeinettForDriftkontraktRaskt(kontrakt.getVeinettId());
		    			}
		    		}else{
		    			if (aktiveRoder){
		    				
		    			}else{
		    				veinett=bean.getVeinettForDriftkontrakt(kontrakt.getId());
		    			}
		    		}
		    	} catch (Exception e){
		    		e.printStackTrace();
		    	}
				return null;
			}
			@Override
			public void done(){
				veinettPane.setVeinett(veinett);
		    	veinettPane.setBusy(false);
		    	if (veiListe!=null)
		    		veinettPane.setSelectedVeireferanser(veiListe);
		    	
		    	if (alleValgt)
		    		veinettPane.selectAll();
		    	if (collapsed)
		    		veinettPane.collapseAll();
			}
		}.execute();
	}
	private void initComponents(){
		veinettPane = new KontraktveinettTaskPane(inkluderKm, showLink, editableKm);
		btnAktiveRoder = new JRadioButton(res.getGuiString("label.aktiveRoder"));
		btnKontraktveinett = new JRadioButton(res.getGuiString("label.kontraktveinett"));
		btnAktiveRoder.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (btnAktiveRoder.isSelected()){
					aktiveRoder=true;
					hentKontraktvei(utenReflinkseksjoner);
				}
			}
		});
		
		btnKontraktveinett.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (btnKontraktveinett.isSelected()){
					aktiveRoder=false;
					hentKontraktvei(utenReflinkseksjoner);
				}
			}
			
		});
		ButtonGroup bg = new ButtonGroup();
		bg.add(btnAktiveRoder);
		bg.add(btnKontraktveinett);
		btnKontraktveinett.setSelected(true);
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		JPanel pnlRadio = new JPanel();
		pnlRadio.add(btnKontraktveinett);
		pnlRadio.add(btnAktiveRoder);
		if (rodevalgAktiv){
			add(pnlRadio, 	 new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		}
		add(veinettPane, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		
		setSize(560,550);
	}
	/**
	 * Setter valgte veireferanser.
	 * Hvis modellen ikke er lastet på forhånd vil ikke radene bli valgt før modellen lastes.
	 * @param veiListe
	 */
	public void setSelectedVeireferanser(List<Veinettveireferanse> veiListe){
		this.veiListe = veiListe;
		//kun hvis modellen er forhåndslastet
		veinettPane.setSelectedVeireferanser(veiListe);
	}
	public List<Veinettveireferanse> getValgte(){
		return veinettPane.getValgte();
	}
}
