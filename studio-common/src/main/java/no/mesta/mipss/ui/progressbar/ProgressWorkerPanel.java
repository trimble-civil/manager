package no.mesta.mipss.ui.progressbar;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Box.Filler;

import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JXProgressBar;

/**
 * Panel for å vise frem gui for utførende/ventende oppgaver
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProgressWorkerPanel extends JPanel{
	
	private JXProgressBar bar;
	private MipssProgressBarTask worker;
	private String label;
	private JButton cancel;
	private Thread cancelThread;
	private enum Status{
		NEW,
		CANCELLED, 
		RESTARTED
	}
	
	private Status status = Status.NEW;
	
	public ProgressWorkerPanel(String label){
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setOpaque(true);
		this.label = label;
		
		bar = new JXProgressBar();
		bar.setPreferredSize(new Dimension(300,25));
		bar.setMaximumSize(new Dimension(Short.MAX_VALUE,25));
		bar.setMinimumSize(new Dimension(200,25));
//		bar.setStringPainted(true);
		bar.setString(label+".. (Venter)");
		
		cancel = new JButton(new CancelAction("", IconResources.STOP_PROCESS_ICON));
		add(bar);
		add(getStrut(5));
		add(cancel);
//		setSize();
	}
	
	public Component getStrut(int w){
		return new Filler(new Dimension(w,0), new Dimension(w,0), 
				  new Dimension(w, 0));
	}
	public void setWorker(MipssProgressBarTask worker){
		this.worker = worker;
	}
	/**
	 * @return the bar
	 */
//	public JProgressBar getBar() {
//		return bar;
//	}
	
	public synchronized void setBarIndeterminate(boolean indeterminate){
		bar.setIndeterminate(indeterminate);
	}
	
	public synchronized void setBarString(String string){
		bar.setString(string);
	}
	/**
	 * @return the worker
	 */
	public MipssProgressBarTask getWorker() {
		return worker;
	}
	
	private void setSize(){
		Dimension size = new Dimension(300, 30);
		setPreferredSize(size);
		this.setMinimumSize(size);
		this.setMaximumSize(size);
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	
	class CancelAction extends AbstractAction{
		public CancelAction(String text, ImageIcon icon){
			super(text, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (status == Status.CANCELLED&&cancelThread!=null){
				cancelThread.interrupt();
				((JButton)e.getSource()).setIcon(IconResources.STOP_PROCESS_ICON);
				bar.setString(label+" (Gjenstartet)");
				status = Status.RESTARTED;
			}else
			if (worker!=null){
				worker.cancelTask();
				status = Status.CANCELLED;
				((JButton)e.getSource()).setIcon(IconResources.REFRESH_VIEW_ICON);
				bar.setString(label+" (Avbrutt av bruker)");
				cancelThread = new Thread("Avbryt progress Thread"){
					public void run(){
						try{
							Thread.sleep(3000);
							worker.getWorker().cancel(true);
						}catch (InterruptedException ex){
							
						}
					}
				};
				cancelThread.start();
			}
			
			
		}
		
	}
}
