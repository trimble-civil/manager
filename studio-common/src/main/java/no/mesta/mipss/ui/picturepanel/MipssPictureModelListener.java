package no.mesta.mipss.ui.picturepanel;

/**
 * Lytter til en bildemodell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface MipssPictureModelListener {

    /**
     * Fyres når modellen er oppdatert
     * 
     * @param e
     */
    public void modelChanged(MipssPictureModelEvent e);
}
