package no.mesta.mipss.ui.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;

import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

public abstract class AbstractMipssTreeTableModel extends AbstractTreeTableModel{

	private List<MipssBeanLoaderListener> listeners = new ArrayList<MipssBeanLoaderListener>();
	private boolean loadingData = false;
	private SwingWorker<?, ?> lastCalledWorker;
	
	/**
	 * I denne metoden skal lastingen av tabellen foregår. Den foregår i en egen tråd
	 * utenfor EDT, unngå detfor GUI-updates i denne metoden.
	 * 
	 * @param beanSourceController
	 * @param currentWorker 
	 */
	protected abstract void load(Object beanSourceController, SwingWorker currentWorker);
	
	
	/**
	 * En SwingWorker som kaller på load(..) og rapporterer evt feillasting.
	 *  
	 * @param beanSourceController
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private SwingWorker<?,?> getWorker(final Object beanSourceController){
		return new SwingWorker(){
			private boolean errors;
			
			@Override
			protected Object doInBackground() throws Exception {
				try{
					load(beanSourceController, this);
				} catch (Exception e){
					e.printStackTrace();
					errors = true;
				}
				return null;
			}
			@Override
            protected  void done() {
                loadingDone(errors);
            }
		};
	}
	
	/**
	 * Skal kalles når modellen skal lastes (evt på nytt)
	 */
	protected void startLoading(Object beanSourceController){
		synchronized (this) {
			loadingData = true;
			fireLoadingStartedEvent(new MipssBeanLoaderEvent(this, false));
	        

	        if(lastCalledWorker!=null){
	        	lastCalledWorker.cancel(true);
	        }
	        SwingWorker<?, ?> newWorker = getWorker(beanSourceController);
	        newWorker.execute();
	        lastCalledWorker = newWorker;
		}
	}
    /**
     * Kalles av tråden som laster data når den er ferdig
     * 
     */
    protected void loadingDone(boolean errors) {
        loadingData = false;
        fireLoadingFinishedEvent(new MipssBeanLoaderEvent(this, errors));
    }
    
	/**
     * Lytterne mottar beskjed når lasting har startet og stoppet
     * 
     * @param l
     */
    public void addTableLoaderListener(MipssBeanLoaderListener l) {
        listeners.add(l);
    }
    
    /**
     * Lytterne mottar beskjed når lasting har startet og stoppet
     * 
     * @param l
     */
    public void removeTableLoaderListener(MipssBeanLoaderListener l) {
        listeners.remove(l);
    }
    
    /**
     * Notifiserer alle lyttere om at loading har startet
     * 
     * @param e
     */
    private void fireLoadingStartedEvent(MipssBeanLoaderEvent e) {
        for(MipssBeanLoaderListener l:listeners) {
            l.loadingStarted(e);
        }
    }
    
    /**
     * Notifiserer alle lyttere om at loading er ferdig
     * 
     * @param e
     */
    private void fireLoadingFinishedEvent(MipssBeanLoaderEvent e) {
        for(MipssBeanLoaderListener l:listeners) {
            l.loadingFinished(e);
        }
    }
    
    /**
     * Kan kalles for å undersøke om modellen er opptatt
     * 
     * @return
     */
    public boolean isLoadingData() {
        return loadingData;
    }
	
}
