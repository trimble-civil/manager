package no.mesta.mipss.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class JPDADialog extends JDialog{
	private JPDAPanel pnlPDA;
	private final Driftkontrakt kontrakt;
	private boolean cancelled=true;

	public JPDADialog(JFrame owner, String title, Driftkontrakt kontrakt){
		super(owner, title, true);
		this.kontrakt = kontrakt;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		pnlPDA = new JPDAPanel(kontrakt);
		
		JButton nullstillButton = new JButton(getNullstillAction());
		JButton okButton = new JButton(getOkAction());
		JButton cancelButton = new JButton(getCancelAction());
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(nullstillButton,new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,10), 0,0));
		buttonPanel.add(okButton, 	  	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,10), 0,0));
		buttonPanel.add(cancelButton, 	new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0,0));
		
		add(pnlPDA, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(buttonPanel, new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		pack();
	}
	
	public boolean isCancelled(){
		return cancelled;
	}
	public Long[] getValgtePDAdfuIdent(){
		if (cancelled){
			return null;
		}
		return pnlPDA.getAlleValgte();
	}
	
	private Action getOkAction(){
		return new AbstractAction("Ok", IconResources.OK2_ICON){

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=false;
				JPDADialog.this.dispose();
			}
		};
	}
	private Action getCancelAction(){
		return new AbstractAction("Avbryt", IconResources.CANCEL_ICON){

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=true;
				JPDADialog.this.dispose();
			}
		};
	}
	private Action getNullstillAction(){
		return new AbstractAction("Nullstill", IconResources.RESET_ICON){

			@Override
			public void actionPerformed(ActionEvent e) {
				setSelectedPdaIds(new Long[]{});
			}
		};
	}

	public void setSelectedPdaIds(Long[] pdaDfuIdentList) {
		pnlPDA.setSelectedPdaIds(pdaDfuIdentList);
		
	}
}
