package no.mesta.mipss.ui;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Point2D;

import org.jdesktop.swingx.JXImageView;

@SuppressWarnings("serial")
public class JMipssImageView extends JXImageView{

	private boolean autoFit;
	
	public JMipssImageView() {
		addResizeListener();
	}

	public void setAutoFit(boolean autoFit){
		this.autoFit = autoFit;
	}
	
	public boolean getAutoFit(){
		return autoFit;
	}
	
	private void addResizeListener() {
		this.addComponentListener(new ComponentListener(){
        	@Override
			public void componentResized(ComponentEvent e) {
        		if (autoFit)
        			autoFit();
        	}
        	@Override
			public void componentShown(ComponentEvent e) {}
			@Override
			public void componentHidden(ComponentEvent e) {}
			@Override
			public void componentMoved(ComponentEvent e) {
				if (autoFit)
					autoFit();
			}
        });
		
	}
	
    public void autoFit(){
    	double scale = 1;
		double width = getImage().getWidth(null);
		double height = getImage().getHeight(null);
		if (height>width){
			double s2 = ((double)getWidth()-10)/(double)getImage().getHeight(this);
			double s1 = ((double)getHeight()-10)/(double)getImage().getWidth(this);
			scale = Math.max(s2, s1);
		}
		else{
			double s2 = ((double)getWidth()-10)/(double)getImage().getWidth(this);
			double s1 = ((double)getHeight()-10)/(double)getImage().getHeight(this);

			scale = Math.max(s2, s1);
		}
		setScale(scale);
		setImageLocation(new Point2D.Double(getWidth()/2,getHeight()/2));
		autoFit = true;
    }
}
