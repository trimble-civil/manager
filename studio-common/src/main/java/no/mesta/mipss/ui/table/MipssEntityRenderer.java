package no.mesta.mipss.ui.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

/**
 * Lager text av IRenderableMipssEntity
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssEntityRenderer extends DefaultTableCellRenderer {
    public MipssEntityRenderer() {
        super();
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if(value == null || !(value instanceof IRenderableMipssEntity)) {
            setText(null);
        } else {
        	IRenderableMipssEntity ent = (IRenderableMipssEntity) value;
            String txt = (ent == null ? "" : ent.getTextForGUI());
            setText(txt);
        }
        
        return this;
    }
}