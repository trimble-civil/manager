package no.mesta.mipss.ui.dialogue;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

import no.mesta.mipss.resources.buttons.ButtonToolkit;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;

/**
 * Modal dialog for bruk av MIPSS Studio Loader og andre
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssDialogue extends JDialog implements WindowListener {
	private int buttonC = 0;
	private final Box buttonPanel = Box.createVerticalBox();
	private Button[] buttons;
	private JButton[] jButtons;
	private Button pressedButton = Button.NOTHING;
	private boolean userAsked;

	public MipssDialogue(JFrame parent, ImageIcon icon, String title, String message, Type type, Button... buttons) {
		super(parent, title, true);
		if (buttons != null) {
			this.buttons = buttons;
			jButtons = new JButton[buttons.length];
		} else {
			this.buttons = new Button[0];
			jButtons = new JButton[0];
		}
		setIconImage(icon.getImage());

		ImageIcon typeIcon = null;
		switch (type) {
		case WARNING:
			typeIcon = IconResources.WARNING_ICON;
			break;
		case INFO:
			typeIcon = IconResources.INFO_ICON;
			break;
		case QUESTION:
			typeIcon = IconResources.QUESTION_ICON;
			break;
		case ERROR:
			typeIcon = IconResources.ERROR_ICON;
			break;
		default:
			throw new IllegalArgumentException("Unknown or illegal dialogue type");
		}

		for (Button b : this.buttons) {
			switch (b) {
			case OK:
				addButton(ButtonToolkit.createOkButton(), b);
				break;
			case SERIOUS_OK:
				addButton(ButtonToolkit.createSeriousOkButton(), b);
				break;
			case CANCEL:
				addButton(ButtonToolkit.createCancelButton(), b);
				break;
			case ABORT:
				addButton(ButtonToolkit.createAbortButton(), b);
				break;
			case YES:
				addButton(ButtonToolkit.createYesButton(), b);
				break;
			case NO:
				addButton(ButtonToolkit.createNoButton(), b);
				break;
			case SAVE:
				addButton(ButtonToolkit.createSaveButton(), b);
				break;
			default:
				throw new IllegalArgumentException("Unknown or illegal button type");
			}
		}
		layoutButtons();

		// Setter opp layout for dialogen
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		// BoxLayout layout = new BoxLayout(getContentPane(),
		// BoxLayout.PAGE_AXIS);
		// setLayout(layout);

		mainPanel.add(BoxUtil.createHorizontalBox(5, ButtonToolkit.createEmblem(typeIcon, false), BoxUtil
				.createHorizontalStrut(20), new JLabel(message)));
		mainPanel.add(BoxUtil.createVerticalStrut(15));
		mainPanel.add(BoxUtil.createHorizontalBox(5, Box.createHorizontalGlue(), buttonPanel));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(20, 10, 15, 5));
		add(mainPanel);
		pack();
		setLocationRelativeTo(parent);
		setResizable(false);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(this);
	}

	public MipssDialogue(JFrame parent, ImageIcon icon, String title, String message, Type type,
			ActionButton... actions) {
		this(parent, icon, title, message, type, (Button[]) null);
		this.buttons = new Button[actions.length];
		this.jButtons = new JButton[actions.length];
		
		for(ActionButton ab:actions) {
			addButton(new JButton(ab.getAction()), ab.getButton());
		}
		
		layoutButtons();
		pack();
	}

	private void addButton(JButton button, final Button b) {
		jButtons[buttonC++] = button;
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pressedButton = b;
				dispose();
			}
		});
	}

	public MipssDialogue.Button askUser() {
		userAsked = true;
		setVisible(true);
		return pressedButton;
	}

	public MipssDialogue.Button getPressedButton() {
		return pressedButton;
	}

	public boolean isUserAsked() {
		return userAsked;
	}

	private void layoutButtons() {
		buttonPanel.removeAll();
		Component[] components = new Component[jButtons.length + 1];
		components[0] = Box.createHorizontalGlue();
		int c = 1;
		for (Component component : jButtons) {
			components[c++] = component;
		}

		buttonPanel.add(BoxUtil.createHorizontalBox(10, components));
	}

	/** {@inheritDoc} */
	@Override
	public void windowActivated(WindowEvent e) {
	}

	/** {@inheritDoc} */
	@Override
	public void windowClosed(WindowEvent e) {
		removeWindowListener(this);
		buttons = null;
		jButtons = null;
	}

	/** {@inheritDoc} */
	@Override
	public void windowClosing(WindowEvent e) {
		dispose();
	}

	/** {@inheritDoc} */
	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	/** {@inheritDoc} */
	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	/** {@inheritDoc} */
	@Override
	public void windowIconified(WindowEvent e) {
	}

	/** {@inheritDoc} */
	@Override
	public void windowOpened(WindowEvent e) {
	}

	public enum Button {
		ABORT, CANCEL, NO, NOTHING, OK, SERIOUS_OK, YES, SAVE
	}

	public enum Type {
		ERROR, INFO, QUESTION, WARNING
	}
	
	public static class ActionButton {
		private final AbstractAction action;
		private final Button button;
		
		public ActionButton(AbstractAction action, Button button) {
			this.action = action;
			this.button = button;
		}
		
		public AbstractAction getAction() {
			return action;
		}
		
		public Button getButton() {
			return button;
		}
	}
}
