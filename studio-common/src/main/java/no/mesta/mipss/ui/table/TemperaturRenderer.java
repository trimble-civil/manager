package no.mesta.mipss.ui.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.persistence.mipssfield.Temperatur;

/**
 * Rendrer tabell celler som har temperatur
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class TemperaturRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

    public TemperaturRenderer() {
        super();
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if(value == null || !(value instanceof Temperatur)) {
            setText(null);
        } else {
            Temperatur temp = (Temperatur) value;
            String txt = (temp == null ? "" : temp.getTextForGUI());
            setText(txt);
        }
        
        return this;
    }
}