package no.mesta.mipss.ui.systeminfo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JMenuItem;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.resources.images.IconResources;

/**
 * MenuItem for å vise frem en dialog med alle kjørende tråder
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class JThreadDumpMenuItem extends JMenuItem{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("mipssStudioText");

    private ThreadDumpDialog dialog;
    
    public JThreadDumpMenuItem(final IMipssStudioLoader loader) {
        super("", IconResources.THREADS_ICON);
        this.setText(resources.getGuiString("studio.runningThreads"));
        addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	if (dialog==null){
	                dialog = new ThreadDumpDialog(loader);        
	                dialog.setVisible(true);
	                dialog.addWindowListener(new WindowListener() {
	            		@Override
						public void windowClosing(WindowEvent e) {
	            			dialog.dispose();
	            			dialog=null;
						}
						@Override
						public void windowOpened(WindowEvent e) {
						}
						@Override
						public void windowIconified(WindowEvent e) {
						}
						@Override
						public void windowDeiconified(WindowEvent e) {
						}
						@Override
						public void windowDeactivated(WindowEvent e) {
						}
						@Override
						public void windowClosed(WindowEvent e) {
						}
						@Override
						public void windowActivated(WindowEvent e) {
						}
					});
            	}
            }
        });
    }
}
