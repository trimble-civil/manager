package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class CurrencyRenderer extends DefaultTableCellRenderer {

	public Component getTableCellRendererComponent(JTable table, 
			Object value, 
			boolean isSelected, 
			boolean hasFocus, 
			int row, int column) {
		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		Box panel = Box.createHorizontalBox();
		panel.setBackground(c.getBackground());
		panel.setForeground(c.getForeground());
		panel.add(Box.createHorizontalGlue());
		panel.setOpaque(true);

		if (value != null && value instanceof Double) {
			Locale locale = new Locale("no", "NO");
			NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);		
			String convertedValue = formatter.format(value);

			JLabel label = new JLabel(convertedValue);
			label.setBackground(c.getBackground());
        	label.setForeground(c.getForeground());
			label.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
			label.setOpaque(false);
			panel.add(label);
		}

		return panel;
	}

}