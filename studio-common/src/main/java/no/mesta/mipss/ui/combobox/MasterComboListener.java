package no.mesta.mipss.ui.combobox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.List;

import javax.swing.JComboBox;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper klasse for ActionListeners i Comboboxer med en tilleggslinje for "Ikke valgt".
 * @author jorge
 */
public abstract class MasterComboListener<M extends IRenderableMipssEntity> implements ActionListener {
    private Logger logger = LoggerFactory.getLogger(MasterComboListener.class);
    private MipssComboBoxWrapper<M> master = null;
    
    
    public MasterComboListener(MipssComboBoxWrapper<M> masterComboWrapper) {
        master = masterComboWrapper;
    }
    
    public void actionPerformed(ActionEvent e) {
        int index;
        
        try {
            index = ((JComboBox) e.getSource()).getSelectedIndex();
        }
        catch (ClassCastException cce) {
            logger.error("MasterSlaveComboListener kan kun betjenne JComboBoxer" );
            throw cce;
        }
        if (index < 1) {
            // registrer "ingen valgt" i combowrapperen
            master.setMipssSelection(null);
            
            // Hvis valget var "IKKE VALGT" eller et NULL objekt
            serveNoselection();
        }
        else {
            List<M> subgrupper = master.getList();
            M o = subgrupper.get(index);
            
            // registrer valget i wrapperen
            master.setMipssSelection(o);
            serveSelection(o);
        }
    }
    
    /**
     * utfører svaret på at brukeren har valgt den første linjen i comboboxen, 
     * altså gatt tilbake til "ikke valgt".
     */
    protected abstract void serveNoselection();
    
    /**
     * Utfører svaret på at brukeen har valgt en gyldig entitet i comboboxen.
     * @param m entiteten brukeren hat valgt.
     */
    protected abstract void serveSelection(M m);
}
