package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;

public class EndreDfuKontraktDialog extends JDialog {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	private JLabel lblNavn = new JLabel();
	private JTextField txtNavn = new JTextField();
	
	private DfuKontrakt dfuKontrakt;
	private boolean okPressed = false;
	
	public EndreDfuKontraktDialog(JFrame parentFrame, DfuKontrakt dfuKontrakt) {
		super(parentFrame, BUNDLE.getGuiString("dialogTitle.endreDfuKontraktNavn"), true);
		this.dfuKontrakt = dfuKontrakt;
		
		initButtons();
		initGui();
	}
	
	public boolean showDialog() {
		txtNavn.setText(dfuKontrakt.getNavn());
		
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		if(okPressed) {
			dfuKontrakt.setNavn(txtNavn.getText());
		}
		
		return okPressed;
	}
	
	private void initButtons() {
		btnOk.setText(BUNDLE.getGuiString("button.ok"));
		btnAvbryt.setText(BUNDLE.getGuiString("button.avbryt"));
		
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okPressed = true;
				dispose();
			}
		});

		btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okPressed = false;
				dispose();
			}
		});	
	}
	
	private void initGui() {
		lblNavn.setText(BUNDLE.getGuiString("label.dfuKontraktNavn"));
		
		txtNavn.setMinimumSize(new Dimension(200, 19));
		txtNavn.setPreferredSize(new Dimension(200, 19));
		
		txtNavn.setDocument(new DocumentFilter(30, null, false));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(txtNavn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout()); 
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}
}
