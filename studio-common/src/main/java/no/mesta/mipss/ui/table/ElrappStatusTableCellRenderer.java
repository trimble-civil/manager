package no.mesta.mipss.ui.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.ui.Sphere;
import no.mesta.mipss.valueobjects.ElrappStatus;

@SuppressWarnings("serial")
public class ElrappStatusTableCellRenderer extends DefaultTableCellRenderer {
	private Sphere red;
	private Sphere yellow;
	private Sphere green;
	
	public ElrappStatusTableCellRenderer(){
		red = new Sphere(Color.red);
		yellow = new Sphere(Color.yellow);
		green = new Sphere(Color.green);
	}
	public static void main(String[] args) {
		JFrame f = new JFrame();
		Sphere s = new Sphere(Color.yellow, 512);
		JLabel l = new JLabel();
		l.setIcon(s.getIcon());
		f.add(l);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		if (value==null)
			label.setIcon(null);
		if (value instanceof ElrappStatus){
			ElrappStatus status = (ElrappStatus)value;
			
			label.setText("");
			label.setHorizontalAlignment(JLabel.CENTER);
			switch(status.getStatus()){
				case ENDRET: label.setIcon(yellow.getIcon());break;
				case IKKE_SENDT: label.setIcon(red.getIcon());break;
				case SENDT: label.setIcon(green.getIcon());break;
			}
			
		}
		return label;
	}
	
	
}