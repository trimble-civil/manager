package no.mesta.mipss.ui.combobox;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * ComboBox modell for MIPSS entiteter
 * 
 * @author Harald A. Kulø
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssComboBoxModel <T extends IRenderableMipssEntity> implements ComboBoxModel {
    private static final Logger log = LoggerFactory.getLogger(MipssComboBoxModel.class);
    protected EventListenerList listenerList = new EventListenerList();
    private PropertyChangeSupport props = new PropertyChangeSupport((Object)this);
    private List<T> entities;
    private T valgtEntity;
    private boolean supportNull;
    
    public MipssComboBoxModel(List<T> entities) {
        this(entities, false);
    }
    
    /**
     * Lager en comboboxmodell med entiteter
     * for bakoverkompatibilitet..
     * @param entities
     */
    public MipssComboBoxModel(List<T> entities, boolean supportNull){
        log.debug("Created(entities,nullsupport)");
        this.supportNull = supportNull;
        this.entities = entities;
        
        if(supportNull) {
            valgtEntity = null;
        } else {
        	if (!entities.isEmpty())
        		valgtEntity = entities.get(0);
        }
    }
    
    public List<T> getEntities() {
		return entities;
	}

	public void setEntities(List<T> entities) {
		Object old = this.entities;
		this.entities = entities;
		
		props.firePropertyChange("entities", old, entities);
		
        if(supportNull) {
        	setSelectedItem(null);
        } else {
        	if (!entities.isEmpty()) {
        		setSelectedItem(entities.get(0));
        	}else{
        		setSelectedItem(null);
        	}
        }
	}

	/**
     * Returnerer T på angitt index
     * @param index
     * @return
     */
    public T getElementAt(int index) {
        if (index >= getSize()) {
            return null;
        }
        
        if(supportNull) {
            if(index == 0) {
                return null;
            } else {
                index-=1;
            }
        }
        
        return entities.get(index);
    }
    
    /**
     * Returnerer størrelsen på listen
     * @return
     */
    public int getSize() {
        int size = entities.size();
        
        if(supportNull) {
            return size + 1;
        } else {
            return size;
        }
    }
    
    /**
     * Returnerer valgte entitet
     * @return
     */
    public T getSelectedItem() {
        return valgtEntity;
    }
    
    /**
     * Setter valgte entitet
     * @param anItem
     */
    @SuppressWarnings("unchecked")
	public void setSelectedItem(Object anItem) {
        log.debug("Setting selected item");
        T old = this.valgtEntity;
        valgtEntity = (T)anItem;
        fireContentsChanged((Object)this, -1, -1);
        props.firePropertyChange("selectedItem", old, valgtEntity);
        log.debug("done setting selected item");
    }
    
    /**
     * Legg til en lytter
     * @param l
     */
    public void addListDataListener(ListDataListener l) {
        listenerList.add(ListDataListener.class, l);
    }
    
    /**
     * Fjerner en lytter
     * @param l
     */
    public void removeListDataListener(ListDataListener l) {
        listenerList.remove(ListDataListener.class, l);
    }
    
    /**
     * Event som fyres når innholdet endres
     * @param source kilden til eventet
     * @param index0 fra index som ble endret
     * @param index1 til index som ble endret
     */
    protected void fireContentsChanged(Object source, int index0, int index1) {
        Object[] listeners = listenerList.getListenerList();
        ListDataEvent e = null;

        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == ListDataListener.class) {
                if (e == null) {
                    e = new ListDataEvent(source, ListDataEvent.CONTENTS_CHANGED, index0, index1);
                }
                ((ListDataListener)listeners[i + 1]).contentsChanged(e);
            }
        }
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
