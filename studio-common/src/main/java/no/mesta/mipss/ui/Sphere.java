package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RadialGradientPaint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.font.LineMetrics;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Sphere{
	private int size = 16;
	private ImageIcon icon;
	
	private Color highlight_max;
	private Color highlight_min;
	private final Color solid;
	private Color half;
	private Color highlight_min_half;
	private Color highlight_max_half;
	private String centerChar;
	
	public Sphere(Color solid, String centerChar, int size){
		this(solid, (Color)null, size);
		this.centerChar = centerChar;
	}
	public Sphere(Color solid, String centerChar){
		this(solid, centerChar, 16);
		
	}
	public Sphere(Color solid){
		this(solid, (Color)null, 16);
	}
	
	public Sphere(Color solid, Color half){
		this(solid, half, 16);
	}
	public Sphere(Color solid, Color half, int size){
		this.solid = solid;
		this.half = half;
		this.size = size;
		highlight_max = new Color(solid.getRed(), solid.getGreen(), solid.getBlue(), 255);
		highlight_min = new Color(solid.getRed(), solid.getGreen(), solid.getBlue(), 0);
		if (half!=null){
			highlight_max_half = new Color(half.getRed(), half.getGreen(), half.getBlue(), 255);
			highlight_min_half = new Color(half.getRed(), half.getGreen(), half.getBlue(), 0);
		}
	}
	public Sphere(Color solid, int size){
		this(solid, (Color)null, size);
	}
	public ImageIcon getIcon(){
		if (icon==null){
			BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
			paint(image.getGraphics());
			icon = new ImageIcon(image);
		}
		return icon;
	}
	
	private int getHeight(){
		return size;
	}
	private int getWidth(){
		return size;
	}
	
	protected void paint(Graphics g) {
		BufferedImage img = createSphere(highlight_max, highlight_min, solid);
        if (half!=null){
        	BufferedImage otherSphere = createSphere(highlight_max_half, highlight_min_half, half);
            TexturePaint pa = new TexturePaint(otherSphere, new Rectangle(0,0,getWidth(), getHeight()));
            Graphics2D go = (Graphics2D)img.getGraphics();
            go.setPaint(pa);
            go.fillPolygon(new int[]{getWidth(),0,getWidth()}, new int[]{0, getHeight(),getHeight()}, 3);
        }
        g.drawImage(img, 0, 0, null);
        if (centerChar!=null){
        	FontMetrics fm= g.getFontMetrics();
        	int stringWidth = fm.stringWidth(centerChar);
        	LineMetrics lm = fm.getLineMetrics(centerChar, g);
        	int height = (int)lm.getHeight();
        	int w = getWidth();
        	int h = getHeight();
        	Font font = g.getFont();
        	font = new Font(font.getName(), Font.BOLD, font.getSize());
        	g.setFont(font);
        	g.setColor(Color.white);
        	g.drawString(centerChar, (w/2)-(stringWidth/2), (h/2)+(height/2)-2);
        }
	}
	
	private BufferedImage createSphere(Color highlightMax, Color highlightMin, Color color){
		BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = (Graphics2D)img.getGraphics();
        
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // Fills the circle with solid blue color
        g2.setColor(color);
        g2.fillOval(0, 1, getWidth() - 1, getHeight() - 1);
        
        // Adds shadows at the top
        Paint p;
        p = new GradientPaint(0, 0, new Color(0.0f, 0.0f, 0.0f, 0.4f), 0, getHeight(), new Color(0.0f, 0.0f, 0.0f, 0.0f));
        g2.setPaint(p);
        g2.fillOval(0, 1, getWidth() - 1, getHeight() - 1);
        
        // Adds highlights at the bottom 
        p = new GradientPaint(0, 0, new Color(1.0f, 1.0f, 1.0f, 0.0f), 0, getHeight(), new Color(1.0f, 1.0f, 1.0f, 0.4f));
        g2.setPaint(p);
        g2.fillOval(0, 1, getWidth() - 1, getHeight() - 1);
        
        // Creates dark edges for 3D effect
        p = new RadialGradientPaint(new Point2D.Double(getWidth() / 2.0,
                getHeight() / 2.0), getWidth() / 2.0f,
                new float[] { 0.0f, 1.0f },
                new Color[] { color, new Color(0.0f, 0.0f, 0.0f, 0.8f) });
        g2.setPaint(p);
        g2.fillOval(0, 1, getWidth() - 1, getHeight() - 1);
        
        // Adds oval inner highlight at the bottom
        p = new RadialGradientPaint(new Point2D.Double(getWidth() / 2.0,
                getHeight() * 1.5), getWidth() / 2.3f,
                new Point2D.Double(getWidth() / 2.0, getHeight() * 1.75 + 6),
                new float[] { 0.0f, 0.8f },
                new Color[] { highlightMax, highlightMin },
                RadialGradientPaint.CycleMethod.NO_CYCLE,
                RadialGradientPaint.ColorSpaceType.SRGB,
                AffineTransform.getScaleInstance(1.0, 0.5));
        g2.setPaint(p);
        g2.fillOval(0, 1, getWidth() - 1, getHeight() - 1);
        
        // Adds oval specular highlight at the top left
        p = new RadialGradientPaint(new Point2D.Double(getWidth() / 2.0,
                getHeight() / 2.0), getWidth() / 1.4f,
                new Point2D.Double(45.0, 25.0),
                new float[] { 0.0f, 0.5f },
                new Color[] { new Color(1.0f, 1.0f, 1.0f, 0.4f),
                    new Color(1.0f, 1.0f, 1.0f, 0.1f) },
                RadialGradientPaint.CycleMethod.NO_CYCLE);
        g2.setPaint(p);
        g2.fillOval(0, 1, getWidth() - 1, getHeight() - 1);
        return img;
	}
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		JPanel p = new JPanel();
		p.setBackground(Color.white);
		p.setLayout(new BorderLayout());
		p.add(new JLabel(new Sphere(new Color(60, 60, 255),"R", 16).getIcon()));
		frame.add(p);
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}