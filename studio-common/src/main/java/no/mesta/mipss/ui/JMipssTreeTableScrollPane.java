package no.mesta.mipss.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.table.AbstractMipssTreeTableModel;

import org.jdesktop.swingx.JXBusyLabel;

/**
 * Scrollview for tree-tables.
 * 
 * Viser en busylabel når modellen til treetabellen lastes.
 * 
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class JMipssTreeTableScrollPane extends JScrollPane implements MipssBeanLoaderListener{
	public boolean busyModel = false;
	private AbstractMipssTreeTableModel tableModel;
	private JMipssTreeTable<? extends IRenderableMipssEntity> table;
	private JPanel busyPanel;
	private JXBusyLabel label;

	/**
	 * Konstruktør
	 * 
	 * @param table
	 * @param tableModel
	 */
	public JMipssTreeTableScrollPane(JMipssTreeTable<? extends IRenderableMipssEntity> table, AbstractMipssTreeTableModel tableModel) {
		super(table);
		this.table = table;
		this.tableModel = tableModel;
		tableModel.addTableLoaderListener(this);

		busyPanel = new JPanel(new GridBagLayout());
		label = new JXBusyLabel();
		busyPanel.add(label, new GridBagConstraints());
		setWheelScrollingEnabled(true);
		setBusy(tableModel.isLoadingData());
	}

	/**
	 * Setter på scrolleren om den er busy eller ikke
	 * 
	 * @param busy
	 */
	private void setBusy(boolean busy) {
		busyModel = busy;
		table.setVisible(!busy);
		busyPanel.setVisible(busy);
		label.setBusy(busy);

		if (!busy) {
			setViewportView(table);
		} else {
			setViewportView(busyPanel);
		}
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		setBusy(true);
	}

	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		if (event.isErrors()) {
			MipssDialogue dialogue = new MipssDialogue(null, IconResources.ERROR_ICON, "En feil oppstod",
					"<html>Beklager, spørringen feilet.</html>", MipssDialogue.Type.ERROR,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK });
			dialogue.askUser();
		}
		setBusy(false);
	}
}