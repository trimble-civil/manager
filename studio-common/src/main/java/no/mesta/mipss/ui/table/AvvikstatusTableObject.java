package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

@SuppressWarnings("serial")
public class AvvikstatusTableObject implements CheckBoxTableObject{
	private final AvvikstatusType type;
	private boolean valgt;

	public AvvikstatusTableObject(AvvikstatusType type){
		this.type = type;
	}
	
	public Long getId(){
		return type.getId();
	}
	public String getStatus(){
		return type.getStatus();
	}
	@Override
	public String getTextForGUI() {
		return type.getTextForGUI();
	}

	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
		
	}

	@Override
	public Boolean getValgt() {
		return valgt;
	}

}
