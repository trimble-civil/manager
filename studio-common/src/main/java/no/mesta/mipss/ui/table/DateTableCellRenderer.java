package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.common.MipssDateFormatter;

@SuppressWarnings("serial")
public class DateTableCellRenderer extends DefaultTableCellRenderer {
	private String dateFormat = MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT;
	public DateTableCellRenderer(){
		
	}
	public DateTableCellRenderer(String format){
		this.dateFormat = format;
	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		String text = "";
		if(value != null) {
			text = MipssDateFormatter.formatDate((Date)value, dateFormat);
		}
		return super.getTableCellRendererComponent(table, text, isSelected, hasFocus, row, column);
	}
}