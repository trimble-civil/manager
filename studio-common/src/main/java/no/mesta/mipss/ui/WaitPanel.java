package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXBusyLabel;

/**
 * Panel som viser frem en busylabel mens rapporten genereres 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class WaitPanel extends JPanel {
    private JXBusyLabel busyLabel = new JXBusyLabel();
    
    private String text;
    public WaitPanel(String text){
    	setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    	this.text = text;
    	initGui();
    }

	private void initGui() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.LINE_AXIS));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    	busyLabel.setBusy(true);
		mainPanel.add(busyLabel);
		mainPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    	mainPanel.add(new JLabel(text));
    	add(mainPanel);
	}
	
	public static JDialog displayWait(Window parent, String text){
		JDialog dialog = new JDialog(parent);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		WaitPanel panel = new WaitPanel(text);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		return dialog;
	}
}