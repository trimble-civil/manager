package no.mesta.mipss.ui;

import java.awt.Shape;

import javax.swing.JButton;
import javax.swing.plaf.ButtonUI;

/**
 * En implementasjon av <code>JButton</code> med et rund-firkantet design og muligheter for å legge
 * til <code>Shape</code> som et ikon.
 */
public class SquareButton extends JButton {

    /** Shape objektene til knappen */
    private Shape[] shapes;

    /** 
     * Lager en ny instans av Squarebutton 
     */
    public SquareButton(int COLOR_SCHEME) {
        super();
        setOpaque(false);
        setUI(SquareButtonUI.createUI(this, COLOR_SCHEME));
    }

    /**
     * Hent Shape arrayen til denne knappen.
     * 
     * @return en array som inneholder Shape objektene
     */
    public Shape[] getShapes() {
        return shapes;
    }

    /**
     * Sett shape objektene til knappen
     * 
     * @param shapes Shape objektene som skal legges til
     */
    public void setShapes(Shape[] shapes) {
        Shape[] oldShapes = this.shapes;
        this.shapes = shapes;
        firePropertyChange("shapes", oldShapes, shapes);
        repaint();
    }

    /**
     * Overskriv fra superklassen for å bruke en egendefinert <code>ButtonUI</code>
     */
    public void setUI(ButtonUI ui) {
        if (ui instanceof SquareButtonUI) {
            super.setUI(ui);
        }
    }

}
