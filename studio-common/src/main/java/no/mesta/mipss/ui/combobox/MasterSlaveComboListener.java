package no.mesta.mipss.ui.combobox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.List;

import javax.swing.JComboBox;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Kontrollerer at valg i master Combobox medfører at slave Combobox reinitialiseres for å vise
 * en ny liste.
 * Brukes i et hierarki av to Combobox'er hvor master Combobox styrer innholdet i slave Combobox.
 * 
 * Avhenger av {@code MipssComboBoxWrapper} som alltd har en linje med IKKE VALGT på toppen (index = 0).
 * @author jorge
 */
public abstract class MasterSlaveComboListener<M extends IRenderableMipssEntity, S extends IRenderableMipssEntity> implements ActionListener {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private MipssComboBoxWrapper<M> master = null;
    private MipssComboBoxWrapper<S> slave = null;
    private Class<M> masterClass = null;
    
    private int oldIndex;
    
    /**
     * Oppretter en instans med master og slave Comboboxer.
     * @param masterComboWrapper 
     * @param slaveComboWrapper
     * @param masterClass klassen til linjene i master Combobox.
     */
    public MasterSlaveComboListener(MipssComboBoxWrapper<M> masterComboWrapper, MipssComboBoxWrapper<S> slaveComboWrapper, Class<M> masterClass ) {
        master = masterComboWrapper;
        slave = slaveComboWrapper;
        this.masterClass = masterClass;
        reinitialize();
    }
    
    public void reinitialize() {
        oldIndex = -1;
    }

    /**
     * Utfører valgkommandoen.
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        int index = 0;
        
        try {
            index = ((JComboBox) e.getSource()).getSelectedIndex();
        }
        catch (ClassCastException cce) {
            logger.error("MasterSlaveComboListener kan kun betjenne JComboBoxer" );
            throw cce;
        }
        if (index == oldIndex) {
            return;
        }
        oldIndex = index;
        
        // hvis index = 0 -> man har valgt "IKKE VALGT"
        // hvis index = -1 -> har man valgt et NULL objekt
        if (index < 1) {
            // registrer valget i masterwrapper
            master.setMipssSelection(null);
            
            // velg "IKKE VALGT" i slaven og så tøm slavelisten.
            slave.setMipssSelection(null);
            slave.setList(null);
            clearSlave(slave);
        }
        else {
            // hent frem den valgte master
            Long id = new Long(0);
            List<M> masters = master.getList();
            M masterObj = masters.get(index);
            
            // registrer den i masterwrapperen
            master.setMipssSelection(masterObj);
            
            // hent ID fra valgt objekt
            try {
                Method m = masterClass.getMethod("getId", (Class<?>[])null);
                id = (Long) m.invoke(masterObj, (Object[])null);
            }
            catch (NoSuchMethodException nsme) {
                logger.error("Metoden getId() ikke i objektet " + masterObj.getClass());
                throw new IllegalStateException(nsme);
            } catch (IllegalAccessException iae) {
                 logger.error("Metoden getId() ikke kallt: " + iae.getCause());
                 throw  new IllegalStateException(iae);
            } catch (InvocationTargetException ite) {
                 logger.error("Metoden getId() ikke kallt: " + ite.getCause());
                throw  new IllegalStateException(ite);
            }
            reinitSlave(slave, id, index);
        }
    }
    
    /**
     * Reinitialiserer slave Comboboxen med en ny liste og en ny valgt linje.
     * @param slave Combobox wrapperen brukt i constructor kallet.
     * @param id nøkkel til objektet som skal velges i den nye listen
     * @param index av objektet valgt i master listen.
     */
    public abstract void reinitSlave(MipssComboBoxWrapper<S> slave, Long id, int index);
        //slave.setList(bean.getUtstyrmodellListFor(id));
        //slave.setMipssSelection(((KjoretoyUtstyr) mipssEntity).getUtstyrmodell());
        
    /**
     * 
     * @param slave
     */
    public abstract void clearSlave(MipssComboBoxWrapper<S> slave);
}
