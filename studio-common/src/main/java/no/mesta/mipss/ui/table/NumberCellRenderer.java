package no.mesta.mipss.ui.table;

import no.mesta.mipss.common.MipssNumberFormatter;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class NumberCellRenderer extends DefaultTableCellRenderer {

    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (renderer instanceof JLabel) {
            JLabel label = (JLabel) renderer;
            label.setHorizontalAlignment(JLabel.RIGHT);
        }

        return renderer;
    }
    
    @Override
    protected void setValue(Object value) {
    	if ((value != null) && (value instanceof Number)) {
            value = MipssNumberFormatter.formatNumberForcedDecimal((Number) value);
        }
        super.setValue(value);
    }

}