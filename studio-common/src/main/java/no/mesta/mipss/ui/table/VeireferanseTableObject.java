package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

public class VeireferanseTableObject implements CheckBoxTableObject {

	private boolean valgt;
	private Veinettveireferanse veiref;
	
	public VeireferanseTableObject(Veinettveireferanse veiref){
		this.veiref = veiref;
	}
	
	@Override
	public Boolean getValgt() {
		return valgt;
	}
	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}

	@Override
	public String getTextForGUI() {
		return null;
	}
	
	public Veinettveireferanse getVeireferanse(){
		return veiref;
	}
	
	public Long getFylkenummer(){
		return veiref.getFylkesnummer();
	}
	
	public Long getKommunenummer(){
		return veiref.getKommunenummer();
	}
	
	public String getVeikategori(){
		return veiref.getVeikategori();
	}
	public String getVeistatus(){
		return veiref.getVeistatus();
	}
	public Long getVeinummer(){
		return veiref.getVeinummer();
	}
	public Long getHp(){
		return veiref.getHp();
	}
	public Double getFraKm(){
		return veiref.getFraKm();
	}
	public Double getTilKm(){
		return veiref.getTilKm();
	}
	public void setFraKm(Double fraKm){
		veiref.setFraKm(fraKm);
	}
	public void setTilKm(Double tilKm){
		veiref.setTilKm(tilKm);
	}
}
