package no.mesta.mipss.ui.table;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

/**
 * Valgbar entitet i tabell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class DfuTableObject implements CheckBoxTableObject, Comparable<DfuTableObject> {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private final static String EMPTY = "";
	private static final String GUI_NONE = BUNDLE.getGuiString("pdaPanel.none");
	private final static String SPACE = " ";
	private final Driftkontrakt currentkontrakt;
	private final Dfuindivid dfuindivid;
	private final PropertyChangeSupport props = new PropertyChangeSupport(this);
	private boolean valgt;

	public DfuTableObject(Dfuindivid dfuindivid, Driftkontrakt currentkontrakt) {
		this.dfuindivid = dfuindivid;
		this.currentkontrakt = currentkontrakt;
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(DfuTableObject o) {
		if (o == null) {
			return 1;
		}
		
		if(dfuindivid == null) {
			return -1; // "ingen" valget
		}

		return new CompareToBuilder().append(getNavn(), o.getNavn()).append(dfuindivid, o.dfuindivid).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof DfuTableObject)) {
			return false;
		}

		DfuTableObject other = (DfuTableObject) o;

		return new EqualsBuilder().append(dfuindivid, other.dfuindivid).isEquals();
	}

	public Dfuindivid getEnhet() {
		return dfuindivid;
	}

	public Long getEnhetId() {
		return dfuindivid == null ? null : dfuindivid.getId();
	}

	public String getModell() {
		String modell = null;
		if (dfuindivid != null) {
			if (dfuindivid.getDfumodell() != null) {
				modell = dfuindivid.getDfumodell().getNavn();
			}
		} else {
			modell = GUI_NONE;
		}

		return modell;
	}

	public String getNavn() {
		String navn = null;
		if (dfuindivid != null) {
			List<DfuKontrakt> dfuKontraktList = dfuindivid.getDfuKontraktList();
			if (dfuKontraktList != null) {
				for (DfuKontrakt dk : dfuKontraktList) {
					if (dk.getKontraktId().longValue() == currentkontrakt.getId().longValue()) {
						navn = dk.getNavn();
					}
				}
			}
		}
		return navn;
	}

	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		final String id = getEnhetId() == null ? EMPTY : String.valueOf(getEnhetId());
		final String modell = getModell() == null ? EMPTY : getModell();
		final String name = getNavn() == null ? EMPTY : getNavn();
		final String guiText = id + SPACE + modell + SPACE + name;

		return dfuindivid == null ? GUI_NONE : guiText;
	}

	public String getType() {
		String type = null;
		if (dfuindivid != null) {
			type = dfuindivid.getDfumodell().getDfukategori().getNavn();
		}

		return type;
	}

	/** {@inheritDoc} */
	@Override
	public Boolean getValgt() {
		return valgt;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(dfuindivid).toHashCode();
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public void setValgt(Boolean valgt) {
		Boolean old = this.valgt;
		this.valgt = valgt;
		props.firePropertyChange("valgt", old, valgt);
	}
}
