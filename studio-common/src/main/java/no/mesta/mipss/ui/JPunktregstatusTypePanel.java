package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.ui.table.PunktregstatusTypeTableObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * Panel med tabell for valgbare entiteter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JPunktregstatusTypePanel extends JPanel implements ItemListener, PropertyChangeListener {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_ALL = BUNDLE.getGuiString("statusPanel.all");
	private static final String GUI_HISTORIC_SEARCH = BUNDLE.getGuiString("statusPanel.historic");
	private static final String GUI_TITLE = BUNDLE.getGuiString("statusPanel.title");
	private static final Logger logger = LoggerFactory.getLogger(JPunktregstatusTypePanel.class);
	private JCheckBox alleCheck;
	private Set<PunktregstatusTypeTableObject> allStatuses;
	private String[] columns = new String[] { "valgt", "textForGUI" };
	private JCheckBox historicCheck;
	private JXTable table;
	private MipssRenderableEntityTableModel<PunktregstatusTypeTableObject> tableModel;
	private Set<PunktregstatusTypeTableObject> valgte = new HashSet<PunktregstatusTypeTableObject>();

	public JPunktregstatusTypePanel() {
		initGui();
	}

	public String[] getAlleValgte() {
		logger.debug("getAlleValgte() start");

		return setToArray(valgte);
	}
	public Long[] getAlleValgteId() {
		logger.debug("getAlleValgteId() start");
		return setToArrayId(valgte);
	}

	private JXTable getTable() {
		allStatuses = new TreeSet<PunktregstatusTypeTableObject>();
		List<AvvikstatusType> all = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class).hentAlleStatusTyper();// module.getMipssFieldBean().hentAlleProsesser();

		for (AvvikstatusType p : all) {
			PunktregstatusTypeTableObject ptto = new PunktregstatusTypeTableObject(p);
			ptto.addPropertyChangeListener("valgt", this);
			allStatuses.add(ptto);
		}

		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				PunktregstatusTypeTableObject.class, columns);
		tableModel = new MipssRenderableEntityTableModel<PunktregstatusTypeTableObject>(
				PunktregstatusTypeTableObject.class, new int[] { 0 }, columns);
		tableModel.getEntities().clear();
		tableModel.getEntities().addAll(allStatuses);
		JXTable table = new JXTable(tableModel, columnModel);
		table.getColumn(0).setMaxWidth(30);

		return table;
	}
	private JCheckTablePanel checkPanel;
	public void setSelected(boolean selected){
		checkPanel.setSelected(selected);
	}
	
	private void initGui() {
		table = getTable();

		alleCheck = new JCheckBox(GUI_ALL);
		historicCheck = new JCheckBox(GUI_HISTORIC_SEARCH);
		alleCheck.addItemListener(this);
		historicCheck.addItemListener(this);

		checkPanel = new JCheckTablePanel<PunktregstatusTypeTableObject>(table, tableModel, alleCheck);
		/*
		Box components = BoxUtil.createVerticalBox(0, BoxUtil.createHorizontalBox(0, checkPanel), BoxUtil
				.createHorizontalBox(0, alleCheck, BoxUtil.createHorizontalStrut(5), historicCheck, Box
						.createHorizontalGlue()));
		ComponentSizeResources.setComponentSize(this, new Dimension(200, 200));

		components.setBorder(BorderFactory.createTitledBorder(GUI_TITLE));

		setLayout(new BorderLayout());
		add(components, BorderLayout.CENTER);
		*/
		setBorder(BorderFactory.createTitledBorder(GUI_TITLE));
		setLayout(new GridBagLayout());
		add(checkPanel, new GridBagConstraints(0,0, 2,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(alleCheck, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(historicCheck, new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));

	}

	public boolean isAllSelected() {
		return alleCheck.isSelected();
	}

	public boolean isHistoricSearch() {
		return historicCheck.isSelected();
	}

	/** {@inheritDoc} */
	@Override
	public void itemStateChanged(ItemEvent e) {
		boolean isSet = e.getStateChange() == ItemEvent.SELECTED;

		if (e.getSource() == historicCheck) {
			firePropertyChange("historicSearch", !isSet, isSet);
		} else if (e.getSource() == alleCheck) {
			firePropertyChange("allSelected", !isSet, isSet);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		logger.debug("propertyChange(" + evt + ") start for " + property );
		PunktregstatusTypeTableObject source = (PunktregstatusTypeTableObject) evt.getSource();
		if (StringUtils.equals(property, "valgt")) {
			Set<PunktregstatusTypeTableObject> old = new HashSet<PunktregstatusTypeTableObject>(valgte);
			if (source.getValgt() == true) {
				valgte.add(source);
			} else {
				valgte.remove(source);
			}

			String[] oldValues = setToArray(old);
			String[] nowValues = setToArray(valgte);

			logger.debug("fire property change, old = " + Arrays.toString(oldValues) + ", now = " + Arrays.toString(nowValues));
			firePropertyChange("alleValgte", oldValues, nowValues);
		}
	}

	private String[] setToArray(Set<PunktregstatusTypeTableObject> set) {
		String[] strings = new String[set.size()];
		Iterator<PunktregstatusTypeTableObject> it = set.iterator();
		int i = 0;
		while (it.hasNext()) {
			strings[i] = ((PunktregstatusTypeTableObject) it.next()).getStatus().getStatus();
			i++;
		}

		return strings;
	}
	private Long[] setToArrayId(Set<PunktregstatusTypeTableObject> set) {
		Long[] ids = new Long[set.size()];
		Iterator<PunktregstatusTypeTableObject> it = set.iterator();
		int i = 0;
		while (it.hasNext()) {
			ids[i] = ((PunktregstatusTypeTableObject) it.next()).getStatus().getId();
			i++;
		}

		return ids;
	}
}
