package no.mesta.mipss.ui.beantable;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import no.mesta.mipss.plugin.MipssPlugin;

public class MipssTablePreferences {
	/** navnet til konfigurasjonen (vanligvis navnet til tabellen) lagres som key i property*/
	private String name;
	/** Eiermodulen til tabellen*/
	private final MipssPlugin plugin;
	/** Alle kolonnenes egenskaper i tabellen*/
	private List<ColumnPreferences> tablePreferences; 
	/** Alle standardinnstillingene til kolonnene i tabellen*/
	private List<ColumnPreferences> defaultPreferences;
	
	/**
	 * Oppretter en ny Tabellkonfigurasjon
	 * 
	 * @param name navnet på konfigurasjonen 
	 * @param plugin 
	 */
	public MipssTablePreferences(String name, MipssPlugin plugin){
		this.name = name;
		this.plugin = plugin;
		load();
	}
	
	public void setTablePreferences(List<ColumnPreferences> tablePreferences){
		this.tablePreferences = tablePreferences;
	}
	/**
	 * Returnerer brukerdefinerte verdier
	 * @return
	 */
	public List<ColumnPreferences> getTablePreferences(){
		return tablePreferences;
	}
	
	public void setDefaultPreferences(List<ColumnPreferences> defaultPreferences) {
		this.defaultPreferences = defaultPreferences;
	}
	/**
	 * Returnerer standardverdiene
	 * @return
	 */
	public List<ColumnPreferences> getDefaultPreferences() {
		return defaultPreferences;
	}

	/**
	 * Laster verdier fra innstillinger, henter ut standardverdier og brukersatte verdier
	 */
	private void load(){
		String jsonString = plugin.getLoader().getUserSettings().getProperty(name);
		tablePreferences = loadPreferencesFromJSON(jsonString);
		String jsonStringDefaults = plugin.getLoader().getUserSettings().getProperty(name+".default");
		defaultPreferences= loadPreferencesFromJSON(jsonStringDefaults);
	}
	
	/**
	 * Leser json strengen og konverterer til javaobjeker i en liste
	 * @param jsonString
	 * @return
	 */
	private List<ColumnPreferences> loadPreferencesFromJSON(String jsonString) {
		if (jsonString==null){
			return null;
		}
		JSONArray json = JSONArray.fromObject(jsonString);
		List<ColumnPreferences> preferences = new ArrayList<ColumnPreferences>();
		for (int i=0;i<json.size();i++){
			JSONObject o = json.getJSONObject(i);
			ColumnPreferences tp = (ColumnPreferences)JSONObject.toBean(o, ColumnPreferences.class);
			preferences.add(tp);
		}
		return preferences;
		
	}
	
	/**
	 * Setter standardinnstillingene til brukeren
	 */
	public void loadDefaults(){
		tablePreferences = defaultPreferences;
	}
	
	/**
	 * Sletter egenskapene fra propertyfilen
	 */
	public void reset(){
		tablePreferences = null;
		save();
	}
	/**
	 * Lagrer standardinnstillinger for tabellen ved førstegangs bruk
	 */
	public void saveDefaults(){
		JSONArray json = JSONArray.fromObject(defaultPreferences);
		plugin.getLoader().getUserSettings().put(name+".default", json.toString());
		plugin.getLoader().saveProperties();
	}
	
	/**
	 * Fjerner denne konfigurasjonen fra settings
	 */
	public void remove(){
		plugin.getLoader().getUserSettings().remove(name+".default");
		plugin.getLoader().saveProperties();
		plugin.getLoader().getUserSettings().remove(name);
		plugin.getLoader().saveProperties();
	}
	/**
	 * Lagrer brukersatte verdier for tabellen
	 */
	public void save(){
		JSONArray json = JSONArray.fromObject(tablePreferences);
		plugin.getLoader().getUserSettings().put(name, json.toString());
		plugin.getLoader().saveProperties();
	}
}