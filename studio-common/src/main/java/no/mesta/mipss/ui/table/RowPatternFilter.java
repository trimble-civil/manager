package no.mesta.mipss.ui.table;

import java.util.regex.Pattern;

import org.jdesktop.swingx.decorator.PatternFilter;

/**
 * Klasse for å sette filter på en JXTable. 
 * 
 * Dette filteret bruker et regex pattern for å matche på en av verdiene i radene. Alle radene som inneholder
 * en kolonne med en match i patternet vises frem i tabellen når dette filteret er aktivt. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RowPatternFilter extends PatternFilter{

	private int columnCount = 0;
	private Pattern filter;
	
	/**
	 * Konstruktør
	 * @param filter regext pattern
	 * @param columnCount antall kolonner på tabellen
	 */
	public RowPatternFilter(Pattern filter, int columnCount){
		super(filter.pattern(), 0, 0);
		this.filter = filter;
		this.columnCount = columnCount;
	}
	
	@Override
	public boolean test(int row) {
        if (pattern == null) {
            return false;
        }
        boolean match = false;
        for (int i=0;i<columnCount;i++){
        	String txt = getInputString(row, i);
        	if (!isEmpty(txt)){
        		match = filter.matcher(txt).find();
        		if (match)
        			return true;
        	}
        }
        return match;
    }
	/**
	 * Returnerer true hvis strengen ikke inneholder text eller et null
	 * @param text
	 * @return
	 */
	private boolean isEmpty(String text) {
        return (text == null) || (text.length() == 0);
    }
}
