package no.mesta.mipss.ui.picturepanel;

import java.util.List;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

/**
 * Modell for å holde bilder
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface MipssPictureModel {

    /**
     * Antall bilder i modellen
     * 
     * @return
     */
    public int size();
    
    /**
     * Legger til et bilde i modellen
     * 
     * @param b
     */
    public void addBilde(Bilde b);
    
    /**
     * Fjerner et bilde i modellen
     * 
     * @param b
     */
    public void removeBilde(Bilde b);
    
    /**
     * Henter et bilde på en gitt index
     * 
     * @param index
     * @return
     */
    public Bilde getBilde(int index); 
    
    /**
     * Finner ut index til et gitt bilde
     * 
     * @param b
     * @return
     */
    public int getBildeIndex(Bilde b);
    
    /**
     * Legger til en lytter på modellen
     * 
     * @param l
     */
    public void addMipssPictureModelListener(MipssPictureModelListener l);
    
    /**
     * Fjerner en lytter til modellen
     * 
     * @param l
     */
    public void removeMipssPictureModelListener(MipssPictureModelListener l);
    
    /**
     * Legger til en bean loader lytter
     * 
     * @param l
     */
    public void addMipssBeanLoaderListener(MipssBeanLoaderListener l);
    
    /**
     * Fjerner en bean loader lytter
     * 
     * @param l
     */
    public void removeMipssBeanLoaderListener(MipssBeanLoaderListener l);
    
    /**
     * Setter alle bildene i modellen
     * 
     * @param bilder
     */
    public void setBilder(List<Bilde> bilder);
    
    /**
     * Henter alle bildene i modellen
     * 
     * @return
     */
    public List<Bilde> getBilder();
    
    /**
     * Arbeider modellen med å hente data?
     * 
     * @return
     */
    public boolean isLoadingData();
}
