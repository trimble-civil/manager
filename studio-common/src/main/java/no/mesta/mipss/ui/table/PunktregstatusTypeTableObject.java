package no.mesta.mipss.ui.table;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

/**
 * Viser status i en tabell hvor den kan velges
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PunktregstatusTypeTableObject implements CheckBoxTableObject, PropertyChangeSource, Comparable<PunktregstatusTypeTableObject> {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_NONE = BUNDLE.getGuiString("statusPanel.none");
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private AvvikstatusType status;
	private boolean valgt;

	public PunktregstatusTypeTableObject(AvvikstatusType status) {
		this.status = status;
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(PunktregstatusTypeTableObject o) {
		if(o == null) {
			return 1;
		}
		
		return status.compareTo(o.getStatus());
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof PunktregstatusTypeTableObject)) {
			return false;
		}
		
		PunktregstatusTypeTableObject other = (PunktregstatusTypeTableObject) o;
		
		return new EqualsBuilder().append(status, other.status).isEquals();
	}

	/**
	 * @return the status
	 */
	public AvvikstatusType getStatus() {
		return status;
	}

	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		return status == null ? GUI_NONE : status.getTextForGUI();
	}

	/**
	 * @return the valgt
	 */
	public Boolean getValgt() {
		return valgt;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(status).toHashCode();
	}
	
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}
	
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(AvvikstatusType status) {
		AvvikstatusType old = this.status;
		this.status = status;
		props.firePropertyChange("status", old, status);
	}

	/**
	 * @param valgt
	 *            the valgt to set
	 */
	public void setValgt(Boolean valgt) {
		Boolean old = this.valgt;
		this.valgt = valgt;
		props.firePropertyChange("valgt", old, valgt);
	}
}
