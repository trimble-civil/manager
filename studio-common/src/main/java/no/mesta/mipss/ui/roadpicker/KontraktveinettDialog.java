package no.mesta.mipss.ui.roadpicker;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class KontraktveinettDialog extends JDialog{

	private boolean cancelled = true;
	private KontraktVeinettPanel veinettPanel;
	private final Driftkontrakt kontrakt;
	private final boolean inkluderKm;
	private final boolean alleValgt;
	private final boolean collapsed;
	private final boolean showLink;
	private final boolean editableKm;
	private boolean utenReflinkseksjoner=true;

	public KontraktveinettDialog(Dialog owner, String title, Driftkontrakt kontrakt, boolean inkluderKm, boolean alleValgt, boolean collapsed, boolean showLink, boolean editableKm){
		super(owner, title, true);
		this.kontrakt = kontrakt;
		this.inkluderKm = inkluderKm;
		this.alleValgt = alleValgt;
		this.collapsed = collapsed;
		this.showLink = showLink;
		this.editableKm = editableKm;
		initGui();
	}
	public KontraktveinettDialog(Frame owner, String title, Driftkontrakt kontrakt, boolean inkluderKm, boolean alleValgt, boolean collapsed, boolean showLink,boolean editableKm){
		super(owner, title, true);
		this.kontrakt = kontrakt;
		this.inkluderKm = inkluderKm;
		this.alleValgt = alleValgt;
		this.collapsed = collapsed;
		this.showLink = showLink;
		this.editableKm = editableKm;
		initGui();
	}
	/**
	 * Ekstra konstruktør for kompatibilitet mot det som eksisterer i prod. 
	 * Kan joines med øvrig konstruktør ved neste release
	 */
	public KontraktveinettDialog(Dialog owner, String title, Driftkontrakt kontrakt, boolean inkluderKm, boolean alleValgt, boolean collapsed, boolean showLink,boolean editableKm, boolean utenReflinkseksjoner){
		super(owner, title, true);
		this.kontrakt = kontrakt;
		this.inkluderKm = inkluderKm;
		this.alleValgt = alleValgt;
		this.collapsed = collapsed;
		this.showLink = showLink;
		this.editableKm = editableKm;
		this.utenReflinkseksjoner = utenReflinkseksjoner;
		initGui();
	}
	
	
	private void initGui() {
		setLayout(new GridBagLayout());
		veinettPanel = new KontraktVeinettPanel(kontrakt, inkluderKm, alleValgt, collapsed, showLink, editableKm, utenReflinkseksjoner);
		
		JButton nullstillButton = new JButton(new NullstillAction("Nullstill", IconResources.RESET_ICON));
		JButton okButton = new JButton(new OkAction("Ok", IconResources.OK2_ICON));
		JButton cancelButton = new JButton(new CancelAction("Avbryt", IconResources.CANCEL_ICON));

		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(nullstillButton,new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,10), 0,0));
		buttonPanel.add(okButton, 	  	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,10), 0,0));
		buttonPanel.add(cancelButton, 	new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0,0));
		
		add(veinettPanel, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(buttonPanel, new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		setSize(560,550);
	}
	
	public void setSelectedVeireferanser(List<Veinettveireferanse> veiListe){
		veinettPanel.setSelectedVeireferanser(veiListe);
	}
	
	public List<Veinettveireferanse> getSelectedVeireferanser(){
		if (cancelled){
			return null;
		}
		return veinettPanel.getValgte();
	}
	public boolean isCancelled(){
		return cancelled;
	}
	class OkAction extends AbstractAction{

		public OkAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			cancelled=false;
			KontraktveinettDialog.this.dispose();
		}
	}

	class CancelAction extends AbstractAction{

		public CancelAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			cancelled = true;
			KontraktveinettDialog.this.dispose();
		}
		
	}
	class NullstillAction extends AbstractAction{

		public NullstillAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			setSelectedVeireferanser(new ArrayList<Veinettveireferanse>());
		}
		
	}
}
