package no.mesta.mipss.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.filter.AbstractQueryFilter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

/**
 * Lager et panel for søk basert på en entitet og et filter for entiteten
 * @author lareid
 *
 * @param <T>
 */
public class SokPanel<T extends IRenderableMipssEntity> extends JPanel {
	private static final Logger log = LoggerFactory.getLogger(SokPanel.class);
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlSok = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblIngenTreff = new JLabel();
    private JButton btnSok = new JButton();
    private JPanel pnlResultat = new JPanel();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    private JScrollPane scrlPaneResultat = new JScrollPane();
    private JXTable tblResultat = new JXTable();
    private JXBusyLabel lblBusy = new JXBusyLabel();;
    
    private Class<T> entityClass;
    private AbstractQueryFilter queryFilter;
    private MipssObservableList<T> resultatList = new MipssObservableList<T>();
    private BindingGroup bindingGroup;
    private Object beanInstance; 
    private String beanMethod;
    private Class<?> beanClass;
    private String[] beanColumns;
    private Method method;
    private boolean multipleSelection;
    
    /**
     * Kontruktør
     * 
     * @param entityClass Klassen som søkes opp
     * @param queryFilter Filter som skal inn i bønnemetoden
     * @param beanInstance bønnen med bønnemetoden
     * @param beanMethod bønnemetoden
     * @param beanClass bønneklassen
     * @param beanColumns kolonner som skal vises i tabell over treff
     */
    public SokPanel(Class<T> entityClass, 
    				AbstractQueryFilter queryFilter, 
    				Object beanInstance, 
    				String beanMethod, 
    				Class<?> beanClass, 
    				boolean multipleSelection, 
    				String... beanColumns) {
    	this.entityClass = entityClass;
    	this.queryFilter = queryFilter;
    	this.beanInstance = beanInstance;
    	this.beanMethod = beanMethod;
    	this.beanClass = beanClass;
    	this.multipleSelection = multipleSelection;
    	this.beanColumns = beanColumns;
    	
    	bindingGroup = new BindingGroup();
    	
    	initGui();
    	initSokefeltGui();
    	initButtons();
    	initTable();
    	
    	bindingGroup.bind();
    }
    
    public T getSelectedEntity() {
    	return tblResultat.getSelectedRow() >= 0 && tblResultat.getSelectedRow() < resultatList.size() 
    			? resultatList.get(tblResultat.convertRowIndexToModel(tblResultat.getSelectedRow())) 
    			: null;
    }
    
    public List<T> getSelectedEntityList() {
    	int[] selectedRows = tblResultat.getSelectedRows();
    	List<T> retList = new ArrayList<T>();
    	for(int i=0; i<selectedRows.length; i++) {
    		int selectedRow = selectedRows[i];
    		if(selectedRow >= 0 && selectedRow < resultatList.size()) {
        		T t = resultatList.get(tblResultat.convertRowIndexToModel(selectedRow));
        		retList.add(t);
        	}
    	}
    	return retList.size() > 0 ? retList : null;
    }
    
    /**
     * Tar inn et objekt og selecterer deette i listen
     * @param t
     */
    public void setSelectedEntity(T t) {
    	resultatList.clear();
    	resultatList.add(t);
    	tblResultat.getSelectionModel().setSelectionInterval(0, 0);
    }
    
    public void addListSelectionListener(ListSelectionListener l) {
    	tblResultat.getSelectionModel().addListSelectionListener(l);
    }
    
	private void initTable() {
		MipssBeanTableModel<T> model = new MipssBeanTableModel<T>(entityClass, beanColumns);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(entityClass, model.getColumns());
		model.setEntities(resultatList);
		tblResultat.setModel(model);
		tblResultat.setColumnModel(columnModel);
		if(multipleSelection) {
			tblResultat.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			tblResultat.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
	}
    
    private void initSokefeltGui() {
    	FocusListener defaultButtonListener = getSokbuttonFocusListener();
    	
    	String[] felt = queryFilter.getSearchableFields();
    	int i = 0;
    	for(; i < felt.length; i++) {
    		JLabel lbl = new JLabel(queryFilter.getLabelForField(felt[i]));
    		JTextField txt = new JTextField();
    		txt.setPreferredSize(new Dimension(120, 21));
    		txt.addFocusListener(defaultButtonListener);
    		bindingGroup.addBinding(BindingHelper.createbinding(queryFilter, felt[i], txt, "text", BindingHelper.READ_WRITE));
    		pnlSok.add(lbl, new GridBagConstraints(0, i, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    		pnlSok.add(txt, new GridBagConstraints(1, i, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    	}
    	
    	lblIngenTreff.setText(BUNDLE.getGuiString("sokPanel.ingenTreff"));
        lblIngenTreff.setForeground(Color.RED);
        lblIngenTreff.setVisible(false);
        
    	pnlSok.add(btnSok, new GridBagConstraints(2, --i, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    	pnlSok.add(lblIngenTreff, new GridBagConstraints(3, i, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    }
    
    private void initButtons() {
        btnSok.setText(BUNDLE.getGuiString("sokPanel.sokbutton"));
        btnSok.setAction(new AbstractAction(BUNDLE.getGuiString("sokPanel.sokbutton")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				callSokMethod();
			}
		});
    }
    
    private void initGui() {
    	this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlSok.setBorder(BorderFactory.createTitledBorder(BUNDLE.getGuiString("sokPanel.sokBorder")));
        pnlSok.setLayout(gridBagLayout3);
        pnlResultat.setLayout(gridBagLayout4);
        lblBusy.setText(BUNDLE.getGuiString("sokPanel.soker"));
        pnlResultat.setBorder(BorderFactory.createTitledBorder(BorderFactory.createTitledBorder(BUNDLE.getGuiString("sokPanel.resultatBorder"))));
        tblResultat.setPreferredScrollableViewportSize(new Dimension(0, 0));
        scrlPaneResultat.getViewport().add(tblResultat, null);
        pnlMain.add(pnlSok, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        pnlResultat.add(scrlPaneResultat, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(pnlResultat, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    
    private FocusListener getSokbuttonFocusListener() {
    	return new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				getRootPane().setDefaultButton(btnSok);
			}

			@Override
			public void focusLost(FocusEvent e) {
				getRootPane().setDefaultButton(null);
			}
		};
    }
    
    private void setBusy(boolean busy) {
    	setSearchEnabled(!busy);
    	lblBusy.setVisible(busy);
		lblBusy.setBusy(busy);
		tblResultat.setVisible(!busy);
		scrlPaneResultat.setViewportView(busy?lblBusy:tblResultat);
		scrlPaneResultat.repaint();
    }
    
    private void setIngenTreff(boolean ingenTreff) {
    	lblIngenTreff.setVisible(ingenTreff);
    }
    
    private void setSearchEnabled(boolean enabled) {
    	btnSok.setEnabled(enabled);
    }
    
    @SuppressWarnings("unchecked")
    private void callSokMethod() {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
				doSearch();
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				} 
    		}
    	}.start();
    }
    
    private Method constructSokMethod() {
    	Method method = null;
        if(beanClass == null) {
            throw new IllegalStateException("No beanInterface set!");
        }
        
        if(beanMethod == null) {
            throw new IllegalStateException("No beanMethod set!");
        }
        
        try {
            method = beanClass.getMethod(beanMethod, queryFilter.getClass());
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("No such method!", e);
        }
        
        return method;
    }
    
    @SuppressWarnings("unchecked")
    private void doSearch() {
    	setBusy(true);

		if(method == null) {
    		method = constructSokMethod();
    	}
    	
    	try {
			setIngenTreff(false);
			
			List<T> sokList = (List<T>)method.invoke(beanInstance, queryFilter);
            
            resultatList.clear();
			resultatList.addAll(sokList);
			
			setIngenTreff(resultatList.size() == 0);
        } catch (IllegalAccessException e) {
            log.error("No access to method", e);
        } catch (InvocationTargetException e) {
            log.error("Method threw exception", e);
            JOptionPane.showMessageDialog(this, BUNDLE.getGuiString("sokPanel.feilUndeSok"));
        }
		
		setBusy(false);
    }
}
