package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.ImageConsumer;
import java.text.ParseException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.MaskFormatter;

/**
 * Panel som viser frem en gui for å velge farge, 
 * Lyttere kan koble seg til dette panelet for å motta events slik:
 * 
 * panel.getColorSelectionModel().addChangeListener(new ChangeListener(){...});
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class ColorChooserPanel extends AbstractColorChooserPanel implements ChangeListener, ActionListener{
	
    private HSBImage palette;
    private HSBImage sliderPalette;
    
    private JSpinner red;
    private JSpinner green;
    private JSpinner blue;
    
    private JTextField hex;
    
    private JPanel previewPanel;
    
    private JLabel paletteLabel;
    private JLabel sliderPaletteLabel;
    
    private boolean isChanging;
    
    private Point paletteSelection = new Point();
    private Point paletteSliderSelection = new Point();
    
    private static final int PALETTE_DIMENSION = 100;
    
    private Color currentColor;
    
    /**
     * Konstruktør
     */
    public ColorChooserPanel() {
    }
    
    /**
     * Set fargen
     * @param currentColor
     */
    public void setCurrentColor(Color currentColor){
        if (currentColor!=null){
            this.currentColor = currentColor;
            getColorSelectionModel().setSelectedColor(currentColor);
                
        }
    }
    public Color getCurrentColor(){
    	return currentColor;
    }
    @Override
    protected void buildChooser() {
        setLayout(new BorderLayout());
        
        getColorSelectionModel().setSelectedColor(new Color(255,255,255));
        
        JComponent palette = buildColorPalette();
        add(palette, BorderLayout.WEST);
        
        JComponent controls = buildControls();
        add(controls, BorderLayout.EAST);
        
        buildPreview();
        add(previewPanel, BorderLayout.SOUTH);
    }
    
    @Override
    public String getDisplayName() {
        return null;
    }

    @Override
    public Icon getLargeDisplayIcon() {
        return null;
    }

    @Override
    public Icon getSmallDisplayIcon() {
        return null;
    }

    @Override
    public void updateChooser() {
        Color c = this.getColorSelectionModel().getSelectedColor();
        if(sliderPaletteLabel != null) {
            float[] hsb = new float[3];
            Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
            updatePanel(hsb[0], hsb[1], hsb[2]);
        }
        currentColor = c;
    }
    
    /**
     * Lager en JLabel som inneholder paletten 
     * @param image
     * @return
     */
    public JLabel createPaletteLabel(Image image) {
        paletteLabel = new JLabel() {
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.white);
                g.drawOval(paletteSelection.x - 4, paletteSelection.y - 4, 8, 8);
            }
        };
        paletteLabel.setIcon(new ImageIcon(image));
        paletteLabel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                float[] hsb = new float[3];
                palette.getHSBForLocation(e.getX(), e.getY(), hsb);
                updatePanel(hsb[0], hsb[1], hsb[2]);
            }
        });
        
        paletteLabel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                int labelWidth = paletteLabel.getWidth();

                int labelHeight = paletteLabel.getHeight();
                int x = e.getX();
                int y = e.getY();

                if (x >= labelWidth) {
                        x = labelWidth - 1;
                }

                if (y >= labelHeight) {
                        y = labelHeight - 1;
                }

                if (x < 0) {
                        x = 0;
                }

                if (y < 0) {
                        y = 0;
                }

                float[] hsb = new float[3];
                palette.getHSBForLocation(x, y, hsb);
                updatePanel(hsb[0], hsb[1], hsb[2]);
            }
        });
        
        return paletteLabel;
    }
    
    /**
     * Lager en JLabel som viser frem farger som en slider
     * @param image
     */
    public void createSliderPaletteLabel(Image image) {
        sliderPaletteLabel = new JLabel() {
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.WHITE);
                g.drawLine(0, paletteSliderSelection.y, this.getPreferredSize().width, paletteSliderSelection.y);
            }
        };
        sliderPaletteLabel.setIcon(new ImageIcon(image));
        
        sliderPaletteLabel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                paletteSliderSelection.y = e.getY();
                sliderPaletteLabel.repaint();
                Color c = getColorSelectionModel().getSelectedColor();
                
                float[] hsb = new float[3];
                Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
                hsb[0] = (float) ((double)e.getY()/(double)PALETTE_DIMENSION);
                
                updatePanel(hsb[0], hsb[1], hsb[2]);
            }
        });
        
        sliderPaletteLabel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                int Y = e.getY();
                if(Y > 0 && Y < PALETTE_DIMENSION) {
                    paletteSliderSelection.y = e.getY();
                    sliderPaletteLabel.repaint();
                    
                    Color c = getColorSelectionModel().getSelectedColor();
                    float[] hsb = new float[3];
                    Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
                    hsb[0] = (float) ((double)e.getY()/(double)PALETTE_DIMENSION);
                    
                    updatePanel(hsb[0], hsb[1], hsb[2]);
                }
            }
        });
    }
    
    /**
     * Lager en component som inneholder palett og slider
     * @return
     */
    private JComponent buildColorPalette() {
        palette = new HSBImage(ColorChooserPanel.HSBImage.HSQUARE, PALETTE_DIMENSION, PALETTE_DIMENSION, 255, 1.0f, 1.0f);
        sliderPalette = new HSBImage(ColorChooserPanel.HSBImage.HSLIDER, 16, PALETTE_DIMENSION, 0f, 1.0f, 1.0f);
        
        Image paletteImage = Toolkit.getDefaultToolkit().createImage(palette);
        Image sliderPaletteImage = Toolkit.getDefaultToolkit().createImage(sliderPalette);

        createPaletteLabel(paletteImage);
        
        createSliderPaletteLabel(sliderPaletteImage);
        
        JPanel panel = new JPanel();
        panel.add(paletteLabel);
        panel.add(sliderPaletteLabel);
        
        return panel;
    }
    
    /**
     * Lager komponenter som viser frem farger som tekst.
     * @return
     */
    private JComponent buildControls() {
        JPanel controls = new JPanel(new BorderLayout());
        
        JPanel rgbPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        rgbPanel.setLayout(layout);
        
        Color selectedColor = getColorSelectionModel().getSelectedColor();
        
        red = new JSpinner(new SpinnerNumberModel(selectedColor.getRed(), 0, 255, 1));
        green = new JSpinner(new SpinnerNumberModel(selectedColor.getGreen(), 0, 255, 1));
        blue = new JSpinner(new SpinnerNumberModel(selectedColor.getBlue(), 0, 255, 1));
        
        red.addChangeListener(this);
        green.addChangeListener(this);
        blue.addChangeListener(this);
        
        try {
                hex = new JFormattedTextField(new MaskFormatter("AAAAAA"));
        } catch (ParseException e) {
                e.printStackTrace();
        }
        hex.setPreferredSize(new Dimension(58,hex.getPreferredSize().height));
        hex.addActionListener(this);
        
        JLabel rLabel = new JLabel("R:");
        JLabel gLabel = new JLabel("G:");
        JLabel bLabel = new JLabel("B:");
        JLabel hexLabel = new JLabel("#");
        
        rgbPanel.add(rLabel);
        rgbPanel.add(red);
        rgbPanel.add(gLabel);
        rgbPanel.add(green);
        rgbPanel.add(bLabel);
        rgbPanel.add(blue);
        rgbPanel.add(hexLabel);
        rgbPanel.add(hex);
        
        // first row
        layout.putConstraint(SpringLayout.NORTH, rLabel, 4, SpringLayout.NORTH, controls);
        layout.putConstraint(SpringLayout.WEST, rLabel, 0, SpringLayout.WEST, controls);
        layout.putConstraint(SpringLayout.NORTH, red, 0, SpringLayout.NORTH, controls);
        layout.putConstraint(SpringLayout.WEST, red, 20, SpringLayout.WEST, rLabel);
        
        // second row
        layout.putConstraint(SpringLayout.NORTH, gLabel, 30, SpringLayout.NORTH, rLabel);
        layout.putConstraint(SpringLayout.WEST, gLabel, 0, SpringLayout.WEST, controls);
        layout.putConstraint(SpringLayout.NORTH, green, 30, SpringLayout.NORTH, red);
        layout.putConstraint(SpringLayout.WEST, green, 20, SpringLayout.WEST, gLabel);
        
        // third row
        layout.putConstraint(SpringLayout.NORTH, bLabel, 30, SpringLayout.NORTH, gLabel);
        layout.putConstraint(SpringLayout.WEST, bLabel, 0, SpringLayout.WEST, controls);
        layout.putConstraint(SpringLayout.NORTH, blue, 30, SpringLayout.NORTH, green);
        layout.putConstraint(SpringLayout.WEST, blue, 20, SpringLayout.WEST, bLabel);
        
        // the hex
        layout.putConstraint(SpringLayout.NORTH, hexLabel, 30, SpringLayout.NORTH, bLabel);
        layout.putConstraint(SpringLayout.WEST, hexLabel, 0, SpringLayout.WEST, controls);
        layout.putConstraint(SpringLayout.NORTH, hex, 30, SpringLayout.NORTH, blue);
        layout.putConstraint(SpringLayout.WEST, hex, 20, SpringLayout.WEST, hexLabel);
        
        // end of panel
        layout.putConstraint(SpringLayout.EAST, rgbPanel, 0, SpringLayout.EAST, hex);
        layout.putConstraint(SpringLayout.SOUTH, rgbPanel, 0, SpringLayout.SOUTH, hex);
        
        JPanel rgbWrapper = new JPanel();
        rgbWrapper.add(rgbPanel);
        controls.add(rgbWrapper, BorderLayout.CENTER);
        
        return controls;
    }
    
    /**
     * Lager forhåndsvisningspanel
     */
    private void buildPreview() {
        previewPanel = new JPanel();
        previewPanel.setBackground(getColorSelectionModel().getSelectedColor());
        previewPanel.setPreferredSize(new Dimension(this.getPreferredSize().width, 25));
    }
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(240,200);
        
        JColorChooser chooser = new JColorChooser();
        AbstractColorChooserPanel[] panels = {new ColorChooserPanel()};
        chooser.setChooserPanels(panels);
        chooser.setPreviewPanel(new JPanel());
        
        frame.add(chooser);
        
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Klasse for slider og palette bilder.
     */
    class HSBImage extends SyntheticImage {
        protected float h = .0f;
        protected float s = .0f;
        protected float b = .0f;

        protected float[] hsb = new float[3];

        protected boolean isDirty = true;

        protected int cachedY;
        protected int cachedColor;
        protected int type;

        private static final int HSQUARE = 0;
        private static final int HSLIDER = 3;
    
        /**
         * Konstruktør
         * @param type 
         * @param width
         * @param height
         * @param h
         * @param s
         * @param b
         */
        protected HSBImage(int type, int width, int height, float h, float s, float b) {
            super(width, height);
            setValues(type, h, s, b);
        }
        
        /**
         * Setter nye verdier på hsbbildet
         * @param type
         * @param h
         * @param s
         * @param b
         */
        public void setValues(int type, float h, float s, float b) {
            this.type = type;
            cachedY = -1;
            cachedColor = 0;
            setHue(h);
            setSaturation(s);
            setBrightness(b);
        }
        
        /**
         * Sett hue
         * @param hue
         */
        public final void setHue(float hue) {
            h = hue;
        }
        
        /**
         * Sett saturation
         * @param saturation
         */
        public final void setSaturation(float saturation) {
            s = saturation;
        }
        
        /**
         * Sett brightness
         * @param brightness
         */
        public final void setBrightness(float brightness) {
            b = brightness;
        }
        @Override
        protected boolean isStatic() {
            return false;
        }
        public synchronized void nextFrame() {
            isDirty = true;
            notifyAll();
        }
        @Override
        public synchronized void addConsumer(ImageConsumer ic) {
            isDirty = true;
            super.addConsumer(ic);
        }
        
        /**
         * Returnerer RGB verdien for en posisjon på bildet
         * @param x
         * @param y
         * @return
         */
        private int getRGBForLocation(int x, int y) {
            if (type >= HSLIDER && y == cachedY) {
                    return cachedColor;
            }

            getHSBForLocation(x, y, hsb);
            cachedY = y;
            cachedColor = Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]);
            
            return cachedColor;
        }
        
        /**
         * Returnerer HSB verdien for en posisjon på bildet
         * @param x
         * @param y
         * @param hsbArray
         */
        public void getHSBForLocation(int x, int y, float[] hsbArray) {
            switch (type) {
                case HSQUARE: {
                    float saturationStep = ((float) x) / width;
                    float brightnessStep = ((float) y) / height;
                    hsbArray[0] = h;
                    hsbArray[1] = s - saturationStep;
                    hsbArray[2] = b - brightnessStep;
                    break;
                }
                case HSLIDER: {
                    float step = 1.0f / ((float) height);
                    hsbArray[0] = y * step;
                    hsbArray[1] = s;
                    hsbArray[2] = b;
                    break;
                }
            }
        }

        @Override
        protected void computeRow(int y, int[] row) {
            if (y == 0) {
                synchronized (this) {
                    try {
                        while (!isDirty) {
                            wait();
                        }
                    } catch (InterruptedException ie) {
                    }
                    isDirty = false;
                }
            }

            if (aborted) {
                return;
            }

            for (int i = 0; i < row.length; ++i) {
                row[i] = getRGBForLocation(i, y);
            }
        }
    }
    
    /**
     * Fyrees av når tilstanden på ColorPickerPanel endres.
     * @param e
     */
    public void stateChanged(ChangeEvent e) {
        if(e.getSource() instanceof JSpinner) {
            int r = Integer.parseInt(red.getValue() + "");
            int g = Integer.parseInt(green.getValue() + "");
            int b = Integer.parseInt(blue.getValue() + "");
            
            float[] hsb = new float[3];
            Color.RGBtoHSB(r, g, b, hsb);
            
            updatePanel(hsb[0],hsb[1],hsb[2]);
        }
    }
    
    /**
     * Oppdaterer guikomponentene med nye fargeverdier
     * @param h hue
     * @param s saturation
     * @param b brightness
     */
    private void updatePanel(float h, float s, float b) {
        if(!isChanging) {
            isChanging = true;
            Color color = Color.getHSBColor(h, s, b);
            
            this.getColorSelectionModel().setSelectedColor(color);
            
            // the hex
            String h1 = Integer.toHexString(color.getRed());
            String h2 = Integer.toHexString(color.getGreen());
            String h3 = Integer.toHexString(color.getBlue());
            
            if (h1.equals("0"))
                    h1 = "00";
            if (h2.equals("0"))
                    h2 = "00";
            if (h3.equals("0"))
                    h3 = "00";
            
            if (h1.length()==1)
                    h1 = "0"+h1;
            if (h2.length()==1)
                    h2 = "0"+h2;
            if (h3.length()==1)
                    h3 = "0"+h3;
            
            if(h1.length() == 1) h1 = "0" + h1;
            if(h2.length() == 1) h2 = "0" + h2;
            if(h3.length() == 1) h3 = "0" + h3;
            
            String hexString = (h1 + h2 + h3).toUpperCase();
            hex.setText(hexString);
            
            palette.setHue(h);
            palette.nextFrame();
            
            int x = PALETTE_DIMENSION - (int) (s * PALETTE_DIMENSION);
            int y = PALETTE_DIMENSION - (int) (b * PALETTE_DIMENSION);
            
            paletteSelection.setLocation(x, y);
            paletteLabel.repaint();
            
            red.setValue(new Integer(color.getRed()));
            green.setValue(new Integer(color.getGreen()));
            blue.setValue(new Integer(color.getBlue()));
            
            previewPanel.setBackground(color);
            
            isChanging = false;
            
            if (currentColor!=null)
                    currentColor=color;
        }
    }
    
    /**
     * Action-metode som kaller updatePanel når en komponent har blitt endret. 
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == hex) {
            String color = hex.getText();
            
            int[] components = new int[3];
            for (int i = 0; i < components.length; i++)
                    components[i] = Integer.parseInt(color.substring(2 * i, 2 * i + 2), 16);
            
            float[] hsb = new float[3];
            Color.RGBtoHSB(components[0], components[1], components[2], hsb);
            updatePanel(hsb[0], hsb[1], hsb[2]);
        }
    }
}