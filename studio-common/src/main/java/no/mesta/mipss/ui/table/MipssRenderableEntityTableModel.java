package no.mesta.mipss.ui.table;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.PropertyUtil;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssObservableList;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Property;
import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;

/**
 * Tabellmodell som gjør om en liste av entiteter til celle data
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssRenderableEntityTableModel<T extends IRenderableMipssEntity> implements TableModel,
		PropertyChangeListener, ObservableListListener {
	private static final Logger log = LoggerFactory.getLogger(MipssRenderableEntityTableModel.class);
	private Class<T> clazz;
	private String[] columns;
	private int[] editableColumnIndices;
	private List<T> entities;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private Object sourceObject;
	private String sourcePropertyName;
	private boolean allowNullCellsEditable=true;
	private List<TableModelListener> tableModelListeners = new ArrayList<TableModelListener>();

	/**
	 * Konstruktøren
	 * 
	 * @param clazz
	 *            Klassen tabellen representerer, benyttes til reflection
	 * @param entities
	 *            Listen av data som skal wrappes som tabell data
	 * @param beanColumns
	 *            kollonnene i tabellen som velges, bønne felter
	 */
	public MipssRenderableEntityTableModel(Class<T> clazz, Collection<T> entities, int[] editableColumnIndices,
			String... beanColumns) {
		this(clazz, editableColumnIndices, beanColumns);
		this.entities = new ArrayList<T>(entities);

		for (Object e : this.entities) {
			listenToEntity(e);
		}
	}

	/**
	 * Konstruktøren
	 * 
	 * @param clazz
	 *            Klassen tabellen representerer, benyttes til reflection
	 * @param entities
	 *            Listen av data som skal wrappes som tabell data
	 * @param beanColumns
	 *            kollonnene i tabellen som velges, bønne felter
	 */
	public MipssRenderableEntityTableModel(Class<T> clazz, Collection<T> entities, String... beanColumns) {
		this(clazz, beanColumns);
		this.entities = new ArrayList<T>(entities);

		for (Object e : this.entities) {
			listenToEntity(e);
		}
	}

	/**
	 * Konstruktør
	 * 
	 * @param clazz
	 * @param beanColumns
	 */
	public MipssRenderableEntityTableModel(Class<T> clazz, int[] editableColumnIndices, String... beanColumns) {
		this(clazz, beanColumns);
		Arrays.sort(editableColumnIndices);
		this.editableColumnIndices = editableColumnIndices;
		this.entities = new ArrayList<T>();
	}
	/**
	 * Konstruktør
	 * 
	 * @param clazz
	 * @param beanColumns
	 */
	public MipssRenderableEntityTableModel(Class<T> clazz, int[] editableColumnIndices, boolean allowNullCellsEditable, String... beanColumns) {
		this(clazz, beanColumns);
		Arrays.sort(editableColumnIndices);
		this.allowNullCellsEditable = allowNullCellsEditable;
		this.editableColumnIndices = editableColumnIndices;
		this.entities = new ArrayList<T>();
	}
	/**
	 * Konstruktør
	 * 
	 * @param clazz
	 * @param beanColumns
	 */
	public MipssRenderableEntityTableModel(Class<T> clazz, String... beanColumns) {
		this.clazz = clazz;
		this.columns = new String[beanColumns.length];
		for (int i = 0; i < beanColumns.length; i++) {
			columns[i] = beanColumns[i];
		}

		this.entities = new ArrayList<T>();
	}

	public MipssRenderableEntityTableModel(String propertyName, Class<T> clazz, String... beanColumns) {
		this(clazz, beanColumns);
		setSourcePropertyName(propertyName);
	}
	
	public MipssRenderableEntityTableModel(String propertyName, Class<T> clazz, int[] editableColumnIndices, String... beanColumns) {
		this(clazz, beanColumns);
		this.editableColumnIndices = editableColumnIndices;
		setSourcePropertyName(propertyName);
	}

	/**
	 * Letter til et item i modellen
	 * 
	 * @param item
	 */
	public void addItem(T item) {
		synchronized (entities) {
			List<T> old = new ArrayList<T>(entities);
			List<T> newL = new ArrayList<T>(entities);
			newL.add(item);
			setEntities(newL);
			int size = entities.size();
			fireTableModelEvent(0, size, TableModelEvent.INSERT);
			fireEntitiesChanged(old, entities);
		}
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	/**
	 * Legger til en lytter
	 * 
	 * @param l
	 */
	public void addTableModelListener(TableModelListener l) {
		tableModelListeners.add(l);
	}

	/**
	 * Angir om modellen inneholder verdien eller ikke
	 * 
	 * @param item
	 * @return
	 */
	public boolean contains(T item) {
		return entities.contains(item);
	}

	/**
	 * Hjelpemetode til å melde til property lyttere at verdiene ang størelsen
	 * på modellen og inholdet har endret seg
	 * 
	 * @param old
	 * @param now
	 */
	protected void fireEntitiesChanged(List<T> old, List<T> now) {
		int oldSize = (old == null ? 0 : old.size());
		int nowSize = (now == null ? 0 : now.size());
		props.firePropertyChange("entities", old, now);
		props.firePropertyChange("size", oldSize, nowSize);
	}

	/**
	 * Sender en TableModel event til lytterne
	 * 
	 * @param firstRow
	 * @param lastRow
	 * @param eventType
	 */
	public void fireTableModelEvent(int firstRow, int lastRow, int eventType) {
		TableModelEvent tme = new TableModelEvent((TableModel) this, firstRow, lastRow, TableModelEvent.ALL_COLUMNS,
				eventType);
		for (TableModelListener tml : tableModelListeners) {
			tml.tableChanged(tme);
		}
	}

	/**
	 * Henter verdien på en gitt rad
	 * 
	 * @param index
	 * @return
	 */
	public T get(int index) {
		return entities.get(index);
	}

	/**
	 * Henter ut metoden til bønnefeltet
	 * 
	 * @param field
	 * @return
	 */
	private String getBeanField(String prefix, String field) {
		String beanField = prefix + field.substring(0, 1).toUpperCase() + field.substring(1);
		return beanField;
	}

	/**
	 * Returnerer klassen til entiteten som ble brukt i konstruktøren
	 * 
	 * @return
	 */
	public Class<T> getClazz() {
		return clazz;
	}

	/**
	 * Returnerer hvilken returntype et felt i bønna har
	 * 
	 * @param columnIndex
	 * @return
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return getReturnTypeOfField(columns[columnIndex]);
	}

	/**
	 * Returnerer antall kollonner i modellen
	 * 
	 * @return
	 */
	public int getColumnCount() {
		return columns.length;
	}

	/**
	 * Returnerer navnet på felter i bønna for den kollonnen
	 * 
	 * @param columnIndex
	 * @return
	 */
	public String getColumnName(int columnIndex) {
		return columns[columnIndex];
	}

	/**
	 * Returnerer kollonnene i modellen som ble brukt i konstruktøren
	 * 
	 * @return
	 */
	public String[] getColumns() {
		return columns;
	}

	/**
	 * Returnerer alle entitetene i modellen
	 * 
	 * @return
	 */
	public List<T> getEntities() {
		return entities;
	}

	/**
	 * Henter ut hvilken datatype et felt i en bønne er
	 * 
	 * @param field
	 * @return
	 */
	private Class<?> getReturnTypeOfField(String field) {
		return PropertyUtil.getBeanFieldType(clazz, field);
	}

	/**
	 * Returnerer antall rader i modellen
	 * 
	 * @return
	 */
	public int getRowCount() {
		return entities.size();
	}

	public int getSize() {
		return entities.size();
	}

	public Object getSourceObject() {
		return sourceObject;
	}

	public String getSourcePropertyName() {
		return sourcePropertyName;
	}

	/**
	 * Henter ut en verdi for en celle
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		// log.trace("getValueAt(" + rowIndex + ", " + columnIndex +
		// ") start ");
		Object value = null;
		try {
			value = getValueFromField(entities.get(rowIndex), columns[columnIndex]);
		} catch (ArrayIndexOutOfBoundsException aiobe) {
			log.trace("getValueAt(rowIndex, columnIdex) could be issue #855 in swingx, aiobe:" + aiobe);
			synchronized (this) {
				TableModelEvent tme = new TableModelEvent((TableModel) this);
				for (TableModelListener tml : tableModelListeners) {
					tml.tableChanged(tme);
				}
			}
		} catch (IndexOutOfBoundsException ex) {
			// flarp..
		}

		// log.trace("etValueAt(" + rowIndex + ", " + columnIndex +
		// "), ferdig: " + value);
		return value;
	}

	/**
	 * Benyttes for å hente ut et bønnefelt på en entitet
	 * 
	 * @param entity
	 * @param field
	 * @return
	 */
	private Object getValueFromField(T entity, String field) {
		if (field.startsWith("$")){
			//EL-Property
			Property<Object, Object> property = PropertyUtil.createProperty(field);
//			try{
				return property.getValue(entity);
//			} catch (Throwable t){ //mest sannsynlig null...
//				return null;
//			}
		}
		Object returnValue = null;
		try {
			Method m = entity.getClass().getMethod(getBeanField("get", field));
			returnValue = m.invoke(entity, (Object[]) null);
		} catch (IllegalAccessException iae) {
			log.debug("getValueFromField(entity, field), iae: " + iae);
		} catch (InvocationTargetException ite) {
			log.debug("getValueFromField(entity, field), iae: " + ite);
		} catch (SecurityException e) {
			log.debug("getValueFromField(entity, field), iae: " + e);
		} catch (NoSuchMethodException e) {
			log.debug("getValueFromField(entity, field), iae: " + e);
		}
		log.trace("getValueFromField(" + field + "), value:" + returnValue);
		return returnValue;
	}

	/**
	 * Avgjør om et felt er editerbart
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		boolean editable = false;

		if (editableColumnIndices == null)
			editable = false;
		else {

			Object cellValue = this.getValueAt(rowIndex, columnIndex);

			/*
			 * chriwii: If a field is null or blank, it should be editable. This
			 * solves the problem of primary key fields not being editable in
			 * NEW rows (where they should be). A side effect is that all other
			 * non-PK fields being null or blank will also always be editable,
			 * which is ok for the time being. harkul: this will propably result
			 * in an UnsupportedOperationException when using a custom
			 * tableobject that doesn't support the property represented in the
			 * model.
			 */
			if (isNullOrBlankString(cellValue)&&allowNullCellsEditable) {
				editable = true;
			} else {
				int binarySearch = Arrays.binarySearch(editableColumnIndices, columnIndex);
				if (binarySearch < 0) {
					editable = false;
				} else {
					editable = true;
				}
			}

		}

		return editable;
	}

	private boolean isNullOrBlankString(Object value) {
		boolean nullOrBlank = false;

		if (value == null) {
			nullOrBlank = true;
		} else {
			if (String.class.isAssignableFrom(value.getClass())) {
				nullOrBlank = StringUtils.isBlank((String) value);
			}
		}

		return nullOrBlank;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void listElementPropertyChanged(ObservableList list, int index) {
		fireTableModelEvent(index, index, TableModelEvent.UPDATE);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void listElementReplaced(ObservableList list, int index, Object value) {
		fireTableModelEvent(index, index, TableModelEvent.UPDATE);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void listElementsAdded(ObservableList list, int index, int value) {
		fireTableModelEvent(index, index, TableModelEvent.INSERT);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public void listElementsRemoved(ObservableList list, int index, List value) {
		fireTableModelEvent(index, index, TableModelEvent.DELETE);
	}

	@SuppressWarnings("unchecked")
	private void listen(List<T> entities) {
		if (entities instanceof MipssObservableList) {
			MipssObservableList<T> obsEnt = (MipssObservableList<T>) entities;
			obsEnt.addObservableListListener(this);
		}
	}

	/**
	 * Lytter til alle kollonnefeltene på en entitet
	 * 
	 * @param entity
	 */
	private void listenToEntity(Object entity) {
		if (entity instanceof PropertyChangeSource) {
			((PropertyChangeSource) entity).addPropertyChangeListener(this);
		}
	}

	@SuppressWarnings("unchecked")
	public void makeDataObservable(final Object source, final String propertyName) {
		if (source != null) {
			Property property = PropertyUtil.createProperty(propertyName);
			List<T> tmpList = (List<T>) property.getValue(source);

			if (tmpList != null && !(tmpList instanceof MipssObservableList)) {
				MipssObservableList<T> observableData = new MipssObservableList<T>(tmpList);
				property.setValue(source, observableData);
				setEntities(observableData);
			} else if (tmpList != null) {
				setEntities(tmpList);
			} else {
				setEntities(new ArrayList<T>());
			}
		} else {
			setEntities(new ArrayList<T>());
		}
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public void propertyChange(PropertyChangeEvent evt) {
		log.trace("propertyChange(" + evt + ")");
		if (sourceObject != null && evt.getSource() == sourceObject) {
			List<T> old = new ArrayList<T>(entities);
			unListen(entities);
			reDirect();
			listen(entities);
			for (TableModelListener tml : tableModelListeners) {
				tml.tableChanged(new TableModelEvent((TableModel) this));
			}

			fireEntitiesChanged(old, entities);
		} else {
			String column = evt.getPropertyName();
			T entity = (T) evt.getSource();

			int index = -1;
			boolean found = false;
			for (String c : columns) {
				index++;
				if (StringUtils.equals(c, column)) {
					found = true;
					break;
				}
			}

			if (!found) {
				log.trace("Did not find column: " + column);
				return;
			}

			int row = entities.indexOf(entity);
			TableModelEvent tableModelEvent = new TableModelEvent((TableModel) this, row, row, index,
					TableModelEvent.UPDATE);
			for (TableModelListener tml : tableModelListeners) {
				tml.tableChanged(tableModelEvent);
			}
		}
	}

	protected void reDirect() {
		makeDataObservable(getSourceObject(), getSourcePropertyName());
	}

	/**
	 * Fjerner et item fra modellen
	 * 
	 * @param item
	 */
	public boolean removeItem(T item) {
		boolean removed = false;
		synchronized (entities) {
			List<T> old = new ArrayList<T>(entities);
			removed = entities.remove(item);
			if (removed) {
				unListenToEntity(removed);
				// TableModelEvent tme = new TableModelEvent((TableModel)this,
				// 0, index, TableModelEvent.ALL_COLUMNS,
				// TableModelEvent.DELETE);

				// Problem med Sun bug: 4730055, som er duplicate av: 4178930
				// http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4730055
				fireTableModelEvent(0, entities.size(), TableModelEvent.DELETE);
				fireEntitiesChanged(old, entities);
			}
		}

		return removed;
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	/**
	 * Fjerner en lytter
	 * 
	 * @param l
	 */
	public void removeTableModelListener(TableModelListener l) {
		tableModelListeners.remove(l);
	}

	/**
	 * Brukes når feks en entitet har blitt lagret i DB og fått satt ID
	 * 
	 * @param oldItem
	 * @param newItem
	 */
	public void replaceItem(T oldItem, T newItem) {
		synchronized (entities) {
			List<T> old = new ArrayList<T>(entities);
			unListenToEntity(oldItem);
			listenToEntity(newItem);
			entities.remove(oldItem);
			entities.add(newItem);

			fireTableModelEvent(0, entities.size(), TableModelEvent.UPDATE);
			fireEntitiesChanged(old, entities);
		}
	}

	/**
	 * Erstatter alle data i tabellen
	 * 
	 * @param newEntities
	 */
	public void setEntities(List<T> newEntities) {
		log.debug("new entities collection is "
				+ (newEntities == null || newEntities.isEmpty() ? "empty" : "filled (" + newEntities.size()
						+ " entities)."));
		synchronized (entities) {
			unListen(entities);
			List<T> old = new ArrayList<T>(entities);
			for (Object e : entities) {
				unListenToEntity(e);
			}

			entities = newEntities;
			listen(entities);

			if (newEntities != null) {
				for (Object e : this.entities) {
					listenToEntity(e);
				}
			}

			for (TableModelListener tml : tableModelListeners) {
				tml.tableChanged(new TableModelEvent((TableModel) this));
			}

			fireEntitiesChanged(old, entities);
		}
	}

	public void setSourceObject(Object source) {
		Object old = this.sourceObject;
		unListen(entities);
		this.sourceObject = source;
		if (source instanceof PropertyChangeSource) {
			((PropertyChangeSource) source).addPropertyChangeListener(sourcePropertyName, this);
		}
		reDirect();
		listen(entities);
		props.firePropertyChange("sourceObject", old, sourceObject);
	}

	public void setSourcePropertyName(String sourcePropertyName) {
		String old = this.sourcePropertyName;
		this.sourcePropertyName = sourcePropertyName;
		props.firePropertyChange("sourcePropertyName", old, sourcePropertyName);
	}

	/**
	 * Setter verdien for en celle
	 * 
	 * @param aValue
	 * @param rowIndex
	 * @param columnIndex
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		setValueInField(entities.get(rowIndex), columns[columnIndex], aValue);
		TableModelEvent tme = new TableModelEvent((TableModel) this, rowIndex, rowIndex, columnIndex,
				TableModelEvent.UPDATE);
		for (TableModelListener tml : tableModelListeners) {
			tml.tableChanged(tme);
		}
	}

	/**
	 * Setter en verdi for en bønne
	 * 
	 * @param entity
	 * @param field
	 * @param value
	 */
	@SuppressWarnings("unchecked")
	private void setValueInField(T entity, String field, Object value) {
		Property property = PropertyUtil.createProperty(field);
		try {
			property.setValue(entity, value);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error setting value in field (" + field + ") value=" + value + " " + e);
		}

	}

	@SuppressWarnings("unchecked")
	private void unListen(List<T> entities) {
		if (entities instanceof MipssObservableList) {
			MipssObservableList<T> obsEnt = (MipssObservableList<T>) entities;
			obsEnt.removeObservableListListener(this);
		}
	}

	/**
	 * Fjerner lyttingen på en entitet
	 * 
	 * @param entity
	 */
	private void unListenToEntity(Object entity) {
		if (entity instanceof PropertyChangeSource) {
			((PropertyChangeSource) entity).removePropertyChangeListener(this);
		}
	}
}
