package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.kjoretoy.KjoretoyeierQueryFilter;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.util.KjoretoyUtils;

public class KjoretoyLeggTilEierPanel extends AbstractRelasjonPanel<Kjoretoyeier> {
	private JPanel pnlMain = new JPanel();
	private SokPanel<Kjoretoyeier> sokPanel;
	private JButton btnLeggTilNyEier = new JButton();
	private JLabel lblInfoTekst = new JLabel();
	private Kjoretoyeier valgtEier;
	
	public KjoretoyLeggTilEierPanel(Kjoretoy kjoretoy, MipssKjoretoy mipssKjoretoy) {
		sokPanel = new SokPanel<Kjoretoyeier>(Kjoretoyeier.class,
											  new KjoretoyeierQueryFilter(),
											  mipssKjoretoy,
											  "getKjoretoyeierList",
											  MipssKjoretoy.class,
											  false,
											  "navn",
											  "adresse",
											  "postnummer",
											  "poststed");
		
		initGui();
		initButtons();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void initGui() {
		lblInfoTekst.setText(KjoretoyUtils.getPropertyString("kjoretoyseier.lbl.info"));
		
		btnLeggTilNyEier.setText(KjoretoyUtils.getPropertyString("kjoretoyseier.button.nyEier"));
		
				
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblInfoTekst, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 0), 0, 0));
		pnlMain.add(sokPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(btnLeggTilNyEier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
	private void initButtons() {
		btnLeggTilNyEier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				KjoretoyNyEierDialog nyEierDialog = new KjoretoyNyEierDialog(getParentDialog());
				Kjoretoyeier kjoretoyeier = nyEierDialog.showDialog();
				if(kjoretoyeier != null) {
					valgtEier = kjoretoyeier;
					sokPanel.setSelectedEntity(valgtEier);
				}
			}
		});
	}
	
	private void updateComplete() {
		valgtEier = sokPanel.getSelectedEntity();
		setComplete(Boolean.valueOf(valgtEier != null));
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(700, 500);
	}

	@Override
	public Kjoretoyeier getNyRelasjon() {
		return valgtEier;
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getPropertyString("kjoretoyeier.dialogTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
