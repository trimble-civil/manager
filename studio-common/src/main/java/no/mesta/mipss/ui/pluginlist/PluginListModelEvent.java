package no.mesta.mipss.ui.pluginlist;

import java.util.EventObject;

import no.mesta.mipss.plugin.PluginCacheKey;

/**
 * Informasjon om en endring i PluginListModel
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PluginListModelEvent extends EventObject{
    private Type type = null;
    private PluginCacheKey newKey = null;
    private PluginCacheKey removedKey = null;
    private PluginCacheKey changedKey = null;
    
    /**
     * Konstruktør
     * 
     */
    public PluginListModelEvent(Object source) {
        super(source);
    }

    /**
     * Sier hva som har endret seg i modellen
     * 
     * @param type
     */
    public void setType(PluginListModelEvent.Type type) {
        this.type = type;
    }

    /**
     * Sier hva som har endret seg i modellen
     * 
     * @return
     */
    public PluginListModelEvent.Type getType() {
        return type;
    }

    /**
     * Hvis type = ITEM_ADD så bør denne ha en verdi
     * 
     * @param newKey
     */
    public void setNewKey(PluginCacheKey newKey) {
        this.newKey = newKey;
    }

    /**
     * Hvis type = ITEM_ADD så bør denne ha en verdi
     * 
     * @return
     */
    public PluginCacheKey getNewKey() {
        return newKey;
    }

    /**
     * Hvis type = ITEM_REMOVE så bør denne ha en verdi
     * 
     * @param removedKey
     */
    public void setRemovedKey(PluginCacheKey removedKey) {
        this.removedKey = removedKey;
    }

    /**
     * Hvis type = ITEM_REMOVE så bør denne ha en verdi
     * 
     * @return
     */
    public PluginCacheKey getRemovedKey() {
        return removedKey;
    }

    /**
     * Hvis type = ITEM_CHANGE så bør denne ha en verdi
     * 
     * @param changedKey
     */
    public void setChangedKey(PluginCacheKey changedKey) {
        this.changedKey = changedKey;
    }

    /**
     * Hvis type = ITEM_CHANGE så bør denne ha en verdi
     * 
     * @return
     */
    public PluginCacheKey getChangedKey() {
        return changedKey;
    }

    /**
     * ITEM_ADD = Et item er blitt lagt til<br>
     * ITEM_REMOVE = Et item er fjernet<br>
     * ITEM_CHANGE = Et item har endret på sine verdier<br>
     * ALL_ITEMS = Hele modellen har endret seg
     */
    public enum Type {ITEM_ADD, ITEM_REMOVE, ITEM_CHANGE, ALL_ITEMS}
}
