package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.Line2D;

/**
 * Et kryss, og no æ ø og å
 * 
 */
public class CrossButton extends SquareButton {

    public CrossButton(Dimension d, String tooltip) {
        super(SquareButtonUI.COLOR_RED);
        Line2D l1 = new Line2D.Double(3, 3, d.width - 4, d.height - 4);
        Line2D l2 = new Line2D.Double(3, 4, d.width - 5, d.height - 4);
        Line2D l3 = new Line2D.Double(4, 3, d.width - 4, d.height - 5);
        
        Line2D l4 = new Line2D.Double(d.width - 4, 3, 3, d.height - 4);
        Line2D l5 = new Line2D.Double(d.width - 5, 3, 3, d.height - 5);
        Line2D l6 = new Line2D.Double(d.width - 4, 4, 4, d.height - 4);

        Shape[] s1 = { l1, l2, l3, l4, l5, l6};
        setShapes(s1);
        setToolTipText(tooltip);
        setPreferredSize(d);
    }
}
