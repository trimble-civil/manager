package no.mesta.mipss.ui.combobox;

import java.awt.event.ActionListener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JComboBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.MipssListCellRenderer;


/**
 * Wrapper en JComboBox for enkel visning av valg i lister.
 * Brukes for å sette opp comboboxer som allerede er designet og plassert på panelet
 * av NetBeans e.l.
 * 
 * Setter comboboksen til å vise MIPSS entiteter og til å velge en entitet i listen.
 * Legger til en "ikke valgt" item øverst på listen og velger den hvis ingen virkelig item er valgt.
 * 
 * Bruker internt en Vector med en kopi av den leverte listen. det er denne vectoren som 
 * får lagt til den "ikke valgt" item. Avhenger av dedikerte Listeners som tar høyde for at den
 * første linjen i den viste listen ikke er koblet til en entitet.
 * 
 * Støtter skjeder av comboboxer:
 * Hvis man setter en slave combobox, vil listen i slaven tømmes hver gang man setter en liste i masteren.
 * 
 * @author jorge
 */
public class MipssComboBoxWrapper<T extends IRenderableMipssEntity> {
	private static final String properties = "MipssComboBoxWrapper";
	private Logger logger = LoggerFactory.getLogger(MipssComboBoxWrapper.class);
    private Vector<T> vector = null;
    private List<T> originalList = null;
    private JComboBox comboBox = null;
    private ResourceBundle bundle = null;
    private MipssComboBoxWrapper<?> slaveComboWrapper = null;
    private T mipssSelection = null;
    private PropertyChangeSupport props = null;
    private String ikkeValgt = null;
    private String ingenLinjer = null;
    private T oldMipssSelection;
    
    // støtter boolean property for å vise om man har valgt noe annet enn linjen med "ikke valgt"
    private boolean wasValidEntity = false;
    
    /**
     * Oppretter en enkel instans. Comboboxen kan ikke endres, listen kan settes om igjen.
     * @param list
     * @param combo
     */
    public MipssComboBoxWrapper(List<T> list, JComboBox combo) {
        if (combo == null) {
            throw new IllegalArgumentException("combobox kan ikke være null");
        }
        this.comboBox = combo;
        
        props = new PropertyChangeSupport((Object)this);
        bundle = java.util.ResourceBundle.getBundle(properties);

        setList(list);
        
        // init comboboxen med MIPSS renderer
        comboBox.setRenderer(new MipssListCellRenderer<T>());
    }
    
    /**
     * Oppretter en instans med de leverte tekstene
     * @param list
     * @param combo
     * @param ikkeValgt vises når ingen inlje er valgt.
     * @param ingenLinjer vises når listen er tom.
     */
    public MipssComboBoxWrapper(List<T> list, JComboBox combo, String ikkeValgt, String ingenLinjer) {
    	this(list, combo);
    	this.ikkeValgt = ikkeValgt;
    	this.ingenLinjer = ingenLinjer;
    	
    	// overstyr de allerde satte tekstene fra den andre constructor
    	setList(list);
    }
    
    /**
     * Oppretter en master instans med sin slave.
     * Initialiseringen omfatter også slaven: dens liste tømmes. 
     * @param list
     * @param combo
     * @param slave
     */
    public MipssComboBoxWrapper(List<T> list, JComboBox combo, MipssComboBoxWrapper<?> slave) {
        this(list, combo);
        slaveComboWrapper = slave;
        slaveComboWrapper.setList(null);
    }
    
    /**
     * Velger en av linjene i listen ut fra det angitte parameteret.
     * Tre alternativer er mulige:
     * <ul>
     * <li>Parameteret er NULL: den første "IKKE VALGT" linjen blir valgt.</li>
     * <li>Parameteret eksisterer i listen: den blir valgt</li>
     * <li>Parameteret er ikke NULL men er ikke med i listen: den første "IKKE VALGT" linjen blir valgt.</li>
     * </ul>
     * Kallet ignoreres hvis den interne listen er null eller tom.
     * @param selectable. hvis null, velges den første linjen som viser at registrering mangler.
     */
    public void setMipssSelection(T selectable) {
    	T oldSelection = getMipssSelection();
        wasValidEntity = (this.mipssSelection != null);

        boolean done = false;
        if (vector == null || vector.size() == 0) {
            this.mipssSelection = null;
        }
        else {
            this.mipssSelection = selectable;
            
            if (selectable == null) {
                comboBox.setSelectedIndex(0);
                done = true;
            }
            else {
                for (T t : vector) {
                    if (t.equals(selectable)) {
                    	if(t == null || !t.equals(comboBox.getSelectedItem())) {
                    		comboBox.setSelectedItem(t);
                    	}
                        done = true;
                        break;
                    }
                }
            }
            if (!done) {
                comboBox.setSelectedIndex(0);
            }
        }
        setValidSelection(this.mipssSelection != null);
        
        props.firePropertyChange("mipssSelection", oldSelection, getMipssSelection());
    }
    
    /**
     * Leverer tilbake det objektte i conboboxen som ble valgt sist.
     * @return det valgte objektet. Kan være null.
     */
    public T getMipssSelection() {
        return mipssSelection;
    }
    
    /**
     * Finner ut om brukeren har valgt en item i listen, eller om man har valgt "ikke valgt"
     * @return true hvis gyldig linje valgt.
     */
    public boolean isValidSelection() {
        return (mipssSelection != null);
    }
    
    /**
     * @return TRUE hvis comboboxen er tom for gyldige linjer
     */
    public boolean isEmpty() {
    	if((originalList == null)) {
    		return true;
    	}
    	if(originalList.size() == 0) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * pseudosetter for property {@code validSelection} som sender ut en Change event.
     * @param v
     */
    public void setValidSelection(boolean v) {
        props.firePropertyChange("validSelection", wasValidEntity, v);
    }
    
    /**
     * Henter ut comboboxen.
     * @return
     */
    public JComboBox getComboBox() {
        return comboBox;
    }

    /**
     * Henter den nåværende listen vist i Comboboxen.
     * @return listen. Kan ikke være null.
     */
    public List<T> getList() {
        return vector;
    }
    
    /**
     * Setter listen som vises i comboboxen. Listen får sin "ikke regisrert" item øverst.
     * @param list
     */
    public void setList(List<T> list) {
    	originalList = list;

        if (list == null || list.size() == 0) {
            //vis at listen er tom og slå av combon.
            vector = new Vector<T>();
            if(ingenLinjer == null) {
            	vector.add(0, (T) new PseudoEntity(bundle.getString("gen.ikkeEksisterende")));
            }
            else {
            	vector.add(0, (T) new PseudoEntity(ingenLinjer));
            }
            comboBox.setEnabled(false);
        }
        else {
            // omgjør listen til en egen vector og legg til noSelection
            vector = new Vector<T>(list);
            if(ikkeValgt == null) {
            	vector.add(0, (T) new PseudoEntity(bundle.getString("gen.ikkeValgt")));
            }
            else {
            	vector.add(0, (T) new PseudoEntity(ikkeValgt));
            }
            comboBox.setEnabled(true);
        }
        
        // på en fersk list har man ennå ikke valgt noen linje
        this.mipssSelection = null;
        
        // init comboboxen med MIPSS modell for den nye listen
        MipssComboBoxModel<T> model = new MipssComboBoxModel<T>(vector);
        
        model.addPropertyChangeListener("selectedItem", new PropertyChangeListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getNewValue() != null && !(evt.getNewValue() instanceof PseudoEntity)) {
					T t = (T)evt.getNewValue();
					setMipssSelection(t);
				} else {
					setMipssSelection(null);
				}
			}
        });
        
        comboBox.setModel(model);
        
        //reinit listener
        ActionListener[] listeners = comboBox.getActionListeners();
        for (ActionListener l : listeners) {
            if (l instanceof MasterSlaveComboListener) {
                ((MasterSlaveComboListener)l).reinitialize();
            }
        }
    }

    /**
     * Setter en avhengig Combobox som skal tømmes hver gang man setter en ny liste,
     * o skal fylles hver gang man vleger i denne combo.
     * @param slaveComboWrapper
     */
    public void setSlaveComboWrapper(MipssComboBoxWrapper slaveComboWrapper) {
        this.slaveComboWrapper = slaveComboWrapper;
    }

    public MipssComboBoxWrapper getSlaveComboWrapper() {
        return slaveComboWrapper;
    }
    
    /**
     * Fjerner den leverte {@code PropertyChangeListener}
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Fjerner den leverte {@code PropertyChangeListener} fra feltet i {@code propName}
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
    
    public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
    	props.addPropertyChangeListener(propName, l);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener l) {
    	props.addPropertyChangeListener(l);
    }

    /**
     * Implementerer kun GUI delen slik at den kan vises sammen med hvilken
     * som helst entitet som impleneterer {@code IRenderableMipssEntity},
     * dog ikke brueks mer enn til visning.
     * @author jorge
     */
    public static class PseudoEntity implements IRenderableMipssEntity {
		private static final long serialVersionUID = 8892632584653316257L;
		private String guiText = null;
        private PseudoEntity(String guiText) {
            this.guiText = guiText;
        }
        public String getTextForGUI() {
            return guiText;
        }
        /**
         * Sorterer alltid først. 
         */
        public int compareTo(Object o) {
            return -1;
        }
    }

	/**
	 * Henter den originale entiteteslisten som ble sendt inn til denne.
	 * @return the originalList
	 */
	public List<T> getOriginalList() {
		return originalList;
	}
}
