package no.mesta.mipss.ui.process;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingWorker;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.driftkontrakt.bean.ProsessHelper;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.ui.tree.RecursiveProsessTreeModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXBusyLabel;

/**
 * Panel med tabell for valgbare entiteter
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JProcessPanel extends JPanel implements ItemListener {
    private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
            .getPropertyBundle("mipssCommonText");
    private static final String GUI_ALL = BUNDLE.getGuiString("processPanel.all");
    private static final String GUI_TITLE = BUNDLE.getGuiString("processPanel.title");
    private static final String GUI_TOM_VALGTE = BUNDLE.getGuiString("processPanel.tomValgte");
    private static final Logger logger = LoggerFactory.getLogger(JProcessPanel.class);
    private JCheckBox alleCheck;
    private JScrollPane scrlPaneProsess;
    private JTree treeProsess;
    private RecursiveProsessTreeModel prosessTreeModel;
    private JXBusyLabel busyLabel = new JXBusyLabel();
    private JButton btnTomAlleValgte = new JButton();
    private boolean fjernProsessForUnderprosess;
    private boolean singleSelectionMode;
    private boolean processLoaded;
    private Long prosessId;
    private boolean kunGyldigForArbeid;

    public JProcessPanel(Object kontraktIdSource, String kontraktIdPropertyName, boolean fjernProsessForUnderprosess) {
        this.fjernProsessForUnderprosess = fjernProsessForUnderprosess;

        initGui();
        BindingHelper.createbinding(kontraktIdSource, kontraktIdPropertyName, this, "kontraktId", BindingHelper.READ).bind();
    }

    public JProcessPanel() {
        initGui();
    }

    public void setKunGyldigForArbeid(boolean kunGyldigForArbeid) {
        this.kunGyldigForArbeid = kunGyldigForArbeid;
    }

    public void setKontraktId(final Long kontraktId) {
        if (kontraktId != null) {
            final SwingWorker<Void, Void> w = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    busyLabel.setBusy(true);
                    scrlPaneProsess.setViewportView(busyLabel);
                    MaintenanceContract bean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
                    List<Prosess> kontraktProsess = bean.getProsessForKontrakt(kontraktId, kunGyldigForArbeid);
                    if (prosessId != null) {
                        kontraktProsess.add(bean.getProsessMedId(prosessId));
                    }
                    kontraktProsess = new ProsessHelper().createTree(kontraktProsess);
                    prosessTreeModel.setProsesser(kontraktProsess);
                    selectAll(alleCheck.isSelected());
                    busyLabel.setBusy(false);
                    scrlPaneProsess.setViewportView(treeProsess);
                    if (prosessId != null) {
                        prosessTreeModel.setSelectedProsessId(prosessId);
                    }
                    processLoaded = true;
                    return null;
                }
            };
            w.execute();
            new Thread() {
                public void run() {
                    try {
                        w.get();
                    } catch (Exception e) {
                        e.printStackTrace();
                        prosessTreeModel.setProsesser(null);
                        busyLabel.setBusy(false);
                        scrlPaneProsess.setViewportView(treeProsess);
                    }
                }
            }.start();
        } else {
            prosessTreeModel.setProsesser(null);
        }
    }

    public boolean isProsessLoaded() {
        return processLoaded;
    }

    private JScrollPane createTree() {
        treeProsess = new JTree();

        prosessTreeModel = new RecursiveProsessTreeModel(treeProsess, true, true, fjernProsessForUnderprosess);
        treeProsess.setModel(prosessTreeModel);
        JScrollPane scrlTree = new JScrollPane(treeProsess);
        scrlTree.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        return scrlTree;
    }

    public Binding getBindingToModel(Object o, String field) {
        return BindingHelper.createbinding(prosessTreeModel, "selectedNode", o, field);
    }

    public void setSelectedProsessId(Long prosessId) {
        this.prosessId = prosessId;
        if (processLoaded && prosessId != null) {
            prosessTreeModel.setSelectedProsessId(prosessId);
        }

    }

    public Long[] getAlleValgteProsesser() {
        logger.debug("getAlleValgteProsesser() start");

        return prosessTreeModel.getProsessId();
    }

    public Long[] getAlleValgteUnderprosesser() {
        logger.debug("getAlleValgteProsesser() start");

        return prosessTreeModel.getUnderprosessId();
    }

    public Set<Prosess> getValgteProsesser() {
        return prosessTreeModel.getValgteProsesser();
    }


    private void initGui() {
        alleCheck = new JCheckBox(GUI_ALL);
        btnTomAlleValgte.setText(GUI_TOM_VALGTE);

        btnTomAlleValgte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (alleCheck.isSelected()) {
                    alleCheck.setSelected(false);
                } else {
                    selectAll(false);
                }
            }
        });

        alleCheck.addItemListener(this);
        scrlPaneProsess = createTree();

        setBorder(BorderFactory.createTitledBorder(GUI_TITLE));
        setLayout(new GridBagLayout());
        add(scrlPaneProsess, new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(alleCheck, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(btnTomAlleValgte, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }

    public boolean isAlleValgt() {
        return alleCheck.isSelected();
    }

    public boolean isIngenValgt() {
        return prosessTreeModel.getIngenProsesser();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        boolean isSet = e.getStateChange() == ItemEvent.SELECTED;

        if (e.getSource() == alleCheck) {
            selectAll(alleCheck.isSelected());
            firePropertyChange("alleValgt", !isSet, isSet);
        }
    }

    public void selectAll(boolean selected) {
        prosessTreeModel.selectAll(selected);
    }

    public void setSelected(boolean selected) {
        alleCheck.setSelected(selected);
    }

    public void setSingleSelectionMode(boolean singleSelectionMode) {
        prosessTreeModel.setSingleSelectionMode(singleSelectionMode);
        alleCheck.setVisible(!singleSelectionMode);
        btnTomAlleValgte.setVisible(!singleSelectionMode);
    }

    public void setVelgUnderprosesserAutomatisk(boolean velgUnderprosesserAutomatisk) {
        prosessTreeModel.setVelgUnderprosesserAutomatisk(velgUnderprosesserAutomatisk);
    }
}
