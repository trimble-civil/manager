package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

/**
 * Abstrakt base-klasse for editering av objekter i tabell-celler.
 * 
 * Subklasser må implementere metoden {@link TableCellEditor#getTableCellEditorComponent(javax.swing.JTable, Object, boolean, int, int)} -
 * øvrige metoder i {@link TableCellEditor} interface delegeres til en default implementasjon.
 * 
 * @author Christian Wiik (Mesan)
 */
	
public abstract class AbstractCellEditor implements TableCellEditor {
	protected TableCellEditor defaultEditor;
	
	public AbstractCellEditor() {
		defaultEditor = new DefaultCellEditor(new JComboBox());		
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column){
		return defaultEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
	}
	
	/** Must be implemented by subclasses */
	@Override
	public Object getCellEditorValue() {
		return defaultEditor.getCellEditorValue();
	}
	
	@Override
	public void addCellEditorListener(CellEditorListener listener) {
		defaultEditor.addCellEditorListener(listener);
	}

	@Override
	public void cancelCellEditing() {
		defaultEditor.cancelCellEditing(); 
	}

	@Override
	public boolean isCellEditable(EventObject event) {
		return defaultEditor.isCellEditable(event);
	}

	@Override
	public void removeCellEditorListener(CellEditorListener listener) {
		defaultEditor.removeCellEditorListener(listener);
	}

	@Override
	public boolean shouldSelectCell(EventObject event) {
		return defaultEditor.shouldSelectCell(event);
	}

	@Override
	public boolean stopCellEditing() {
		return defaultEditor.stopCellEditing();
	}

}