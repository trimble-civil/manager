package no.mesta.mipss.ui.table;

import java.util.Date;

import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
@SuppressWarnings("serial")
public class AgrArtikkelSelectableObject implements CheckBoxTableObject {
	private AgrArtikkelV agr;
	private Boolean valgt;
	
	public AgrArtikkelSelectableObject(AgrArtikkelV agr){
		this.agr = agr;
		valgt = Boolean.FALSE;
	}
	public AgrArtikkelV getAgrArtikkel(){
		return agr;
	}
	public String getLevkontraktBeskrivelse(){
		return agr.getLevkontraktBeskrivelse();
	}
	public String getLevkontraktIdent(){
		return agr.getLevkontraktIdent();
	}
	public Date getFraDato(){
		return agr.getFraDato();
	}
	public Date getTilDato(){
		return agr.getTilDato();
	}
	@Override
	public String getTextForGUI() {
		return "";
	}

	public Boolean getValgt() {
		return valgt;
	}
	
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}

}
