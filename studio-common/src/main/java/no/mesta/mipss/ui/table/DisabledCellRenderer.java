package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class DisabledCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    public DisabledCellRenderer() {
    }
    
    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        
    	Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if (renderer instanceof JLabel){
        	renderer.setEnabled(false);
        }
        return this;
    }
}