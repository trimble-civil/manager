package no.mesta.mipss.ui.table;

import java.awt.Component;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.MipssListCellRenderer;

/**
 * Renderer for visning av <tt>IRenderableMipssEntity</tt> objekt i tabell-celle.
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link IRenderableMipssEntity#getTextForGUI() }
 */
public class MipssTableComboBoxRenderer implements TableCellRenderer {
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

		if (table.isCellEditable(row, column)) {
			IRenderableMipssEntity entity = (IRenderableMipssEntity) value;
			JComboBox comboBox = new JComboBox();

			if (entity != null) {
				comboBox.addItem(entity);
			}

			comboBox.setRenderer(new MipssListCellRenderer<>());
			return comboBox;
		} else {
			JLabel label = new JLabel();

			if(value!=null && value instanceof IRenderableMipssEntity) {
				label.setText(((IRenderableMipssEntity) value).getTextForGUI());
			}

			return label;
		}
	}

}