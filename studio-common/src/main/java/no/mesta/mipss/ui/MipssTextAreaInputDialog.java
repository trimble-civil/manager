package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class MipssTextAreaInputDialog extends JDialog {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText");
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JTextArea textArea = new JTextArea();
	private JScrollPane scrlPane = new JScrollPane();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	private String inputText;
	
	public MipssTextAreaInputDialog(JFrame parent, String title) {
		super(parent, title, true);
		
		initGui();
		initButtons();
	}
	
	public MipssTextAreaInputDialog(JDialog parent, String title) {
		super(parent, title, true);
		
		initGui();
		initButtons();
	}
	
	/**
	 * Returns null if user canceled
	 * @return
	 */
	public String getInput() {
		setLocationRelativeTo(null);
		setVisible(true);
		return inputText;
	}
	
	private void initButtons() {
		btnOk.setAction(new AbstractAction(BUNDLE.getGuiString("relasjonPanel.button.ok")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				inputText = textArea.getText();
				dispose();
			}
		});
		btnAvbryt.setAction(new AbstractAction(BUNDLE.getGuiString("relasjonPanel.button.avbryt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				inputText = null;
				dispose();
			}
		});
	}
	
	private void initGui() {
		this.setSize(new Dimension(300, 200));
		this.setLayout(new GridBagLayout());
		pnlMain.setLayout(new GridBagLayout());
		pnlButtons.setLayout(new GridBagLayout());
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrlPane.getViewport().add(textArea, null);
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(scrlPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 0, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 11, 11), 0, 0));
	}
}
