package no.mesta.mipss.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Setter størrelsen på komponenter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ComponentSizeResources {

	/**
	 * Skal ikke brukes
	 * 
	 */
	private ComponentSizeResources() {
	}

	/**
	 * Lager en størrelse for en tekst
	 * 
	 * @param fontMetrics
	 * @param string
	 * @return
	 */
	public static Dimension computeStringSize(FontMetrics fontMetrics, String string) {
		return new Dimension(fontMetrics.stringWidth(string), fontMetrics.getHeight());
	}

	/**
	 * Setter størrelsen på et komponent
	 * 
	 * @param component
	 * @param size
	 */
	public static void setComponentSize(Component component, Dimension size) {
		setComponentSizes(component, size, size, size);
	}

	/**
	 * Setter standard størelser på kjente komponenter
	 * 
	 * @param component
	 */
	public static void setComponentSizes(Component component) {
		Dimension min = null;
		Dimension preferred = null;
		Dimension max = null;

		if (component instanceof JComboBox || component instanceof JTextField) {
			max = new Dimension(800, 20);
			preferred = new Dimension(160, 20);
			min = new Dimension(50, 20);
		} else if (component instanceof JTextArea) {
			max = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
			preferred = new Dimension(400, 140);
			min = new Dimension(200, 80);
		} else if (component instanceof JList) {
			min = new Dimension(80, 80);
			preferred = new Dimension(160, 80);
			max = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
		} else if (component instanceof JTable) {
			min = new Dimension(80, 80);
			preferred = new Dimension(300, 200);
			max = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
		} else {
			// Ikke gjør no, returner
			return;
		}

		setComponentSizes(component, min, preferred, max);
	}

	/**
	 * Setter størrelsene på et komponent
	 * 
	 * @param component
	 * @param min
	 * @param preferred
	 * @param max
	 */
	public static void setComponentSizes(Component component, Dimension min, Dimension preferred, Dimension max) {
		component.setMaximumSize(max);
		component.setPreferredSize(preferred);
		component.setMinimumSize(min);
	}
}
