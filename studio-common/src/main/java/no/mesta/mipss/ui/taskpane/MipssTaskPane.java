package no.mesta.mipss.ui.taskpane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXCollapsiblePane;

@SuppressWarnings("serial")
public class MipssTaskPane extends JPanel{

	private JXCollapsiblePane contentPane;
	private TaskTitleBar titleBar;
	private JComponent content;
	
	public MipssTaskPane(){
		initComponents();
		initGui();
	}
	private void initGui() {
		setLayout(new BorderLayout());
		JPanel c = new JPanel(new BorderLayout());
		c.add(contentPane, BorderLayout.NORTH);
		JPanel m = new JPanel(new BorderLayout());
		m.add(titleBar, BorderLayout.NORTH);
		m.add(c, BorderLayout.CENTER);
		add(m, BorderLayout.CENTER);
	}
	private void initComponents(){
		contentPane = new JXCollapsiblePane();
		contentPane.setContentPane(createDefaultContentPane());
		titleBar = new TaskTitleBar(contentPane);
//		{
////			public void setBounds(int x, int y, int w, int h){
//				super.setBounds(x, y, w, h);
//				Dimension size = new Dimension(w-50, 200);
//				content.setPreferredSize(size);
//				content.setMaximumSize(size);
//			}
//		};
		
		
	}
	
	public void setTitleContent(JComponent components){
		
		titleBar.setContent(components);
	}
	public void setTitleLeftComponent(JComponent comp){
		titleBar.setLeftComponent(comp);
	}
	
	public void setContent(JComponent content){
		this.content = content;
		contentPane.getContentPane().add(content);
	}
	
	public void setCollapsed(boolean collapsed){
		contentPane.setCollapsed(collapsed);
	}
	public boolean isCollapsed(){
		return contentPane.isCollapsed();
	}
	private JPanel createDefaultContentPane(){
		JPanel pnl = new JPanel(new BorderLayout());
		pnl.setBackground(Color.white);
		pnl.setBorder(BorderFactory.createMatteBorder(0,1,1,1,Color.gray));
//		pnl.setPreferredSize(new Dimension(300, 100));
		return pnl;
	}
	
	
}
