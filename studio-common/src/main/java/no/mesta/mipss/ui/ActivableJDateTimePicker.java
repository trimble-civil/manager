/**
 * 
 */
package no.mesta.mipss.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import no.mesta.mipss.persistence.Clock;

/**
 * Utvider {@code JDateTimePicker} med en checkbox på venstresiden.
 * Selve DateTimePicker er slått av som standard. Den slås på når
 * checkboxen velges.
 * 
 * Hvis man velger å ha label for DateTimePicker, er denne label på kun 
 * når DateTimePicker er slåt på.
 *  
 * @author jorge
 *
 */
public class ActivableJDateTimePicker extends JDateTimePicker {
	private static final long serialVersionUID = -5657885421295795286L;
	private JCheckBox activate = null;
	private JLabel dateLabel = null;
	
	public ActivableJDateTimePicker() {
		super();
		initCheckbox("");
		initGuiLocal();
	}
	
	public ActivableJDateTimePicker(String activateLabel) {
		super();
		initCheckbox(activateLabel);
		initGuiLocal();
	}
	
	public ActivableJDateTimePicker(Date pickerDate, String activateLabel) {
		super(pickerDate);
		initCheckbox(activateLabel);
		initGuiLocal();
	}

	public ActivableJDateTimePicker(Date pickerDate, String format, String activateLabel){
		super(pickerDate, format);
		initCheckbox(activateLabel);
		initGuiLocal();
	}
	
	public ActivableJDateTimePicker(Date pickerDate, String format, String text, String activateLabel) {
		super(pickerDate, format);
		dateLabel = new JLabel(text);
		initCheckbox(activateLabel);
		initGuiLocal();
	}
	public ActivableJDateTimePicker(String text, String activateLabel) {
		super(Clock.now());
		dateLabel = new JLabel(text);
		initCheckbox(activateLabel);
		initGuiLocal();
	}
	

	
	/**
	 * Gjør hele komponenten utilgjengelig. 
	 * @param active
	 */
	public void setActivated(boolean active) {
		activate.setSelected(active);
		super.setEnabled(active);
		dateLabel.setEnabled(active);
	}
	
	/**
	 * binder checkboxen med label og dato- og tidkomponentenene.
	 * @param activateLabel
	 */
	private void initCheckbox(String activateLabel) {
		activate = new JCheckBox(activateLabel, false);

        activate.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox b = (JCheckBox)e.getSource();
				setEnabled(b.isSelected());
				dateLabel.setEnabled(b.isSelected());
			}
        });

	}
	
	/**
	 * Skaper cisningen på nytt.
	 * Ignorert hvis kallet kommer under contruction.
	 */
	@Override
    protected void initGui(){
		if(activate != null) {
			initGuiLocal();
		}
	}
	/**
     * Legger gui komponentene på panelet
     */
    private void initGuiLocal() {
    	// hvis ingen tekst satt, opprett tom label for å forenkle plasseringen
    	if(dateLabel == null) {
    		dateLabel = new JLabel();
    	}
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(activate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dateLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(time, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                .addComponent(time, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(dateLabel)
                .addComponent(activate))
        );
    }                                      
}
