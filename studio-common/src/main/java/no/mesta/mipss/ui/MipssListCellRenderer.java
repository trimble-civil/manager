package no.mesta.mipss.ui;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
/**
 * Cell renderer for lister (dropdowns og JList)
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class MipssListCellRenderer <T extends IRenderableMipssEntity> extends DefaultListCellRenderer implements ListCellRenderer<Object> {
    private String emptyText = null;

    public MipssListCellRenderer(String emptyText) {
        this.emptyText = emptyText;
    }

    public MipssListCellRenderer() {
        this("< ikke valgt >");
    }
    /**
     * Tegner inn MIPSS GUI tekst for lister
     *
     * @param list
     * @param value
     * @param index
     * @param isSelected
     * @param cellHasFocus
     * @return
     */
    @SuppressWarnings("unchecked")
	public Component getListCellRendererComponent(JList<T> list, T value,
                                                  int index, 
                                                  boolean isSelected, 
                                                  boolean cellHasFocus) {
        IRenderableMipssEntity entity = null;

        if(value != null && value instanceof IRenderableMipssEntity) {
            entity = value;
        }

        String text;
        if(entity != null) {
            text = entity.getTextForGUI();
        } else {
            text = emptyText;
        }

        return super.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JList<T> tList = (JList<T>) list;
        return getListCellRendererComponent(tList, (T)value, index, isSelected, cellHasFocus);
    }
}