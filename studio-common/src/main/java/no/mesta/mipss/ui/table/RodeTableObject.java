package no.mesta.mipss.ui.table;

import java.awt.Color;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Rode;

import org.jdesktop.swingx.color.ColorUtil;

public class RodeTableObject implements IRenderableMipssEntity, CheckBoxTableObject{
	public Rode rode;
	private Boolean vises;
	private boolean valgt;
	
	public RodeTableObject(Rode rode){
		this.rode = rode;
	}
	
	public void setRode(Rode rode){
		this.rode = rode;
	}
	
	public Color getGuiFarge(){
		return no.mesta.mipss.core.ColorUtil.fromHexString(rode.getVeinett().getGuiFarge());
	}
	
	public void setGuiFarge(Color color){
		String farge = ColorUtil.toHexString(color);
		rode.getVeinett().setGuiFarge(farge.replaceAll("#", "0x"));
	}
	
	public String getNavn(){
		return rode.getNavn();
	}
	
	public Boolean getVises(){
		return vises;
	}
	public void setVises(Boolean vises){
		this.vises = vises;
	}

	@Override
	public String getTextForGUI() {
		return "";
	}
	
	public Double getRodeLengde(){
		return rode.getRodeLengde();
	}
	public Rode getRode(){
		return rode;
	}

	@Override
	public Boolean getValgt() {
		return valgt;
	}

	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
		
	}
	
	public int hashCode(){
		return rode.hashCode();
	}
	public boolean equals(Object o){
		if (o instanceof RodeTableObject){
			return rode.equals(((RodeTableObject)o).getRode());
		}
		return false;
	}
	
}
