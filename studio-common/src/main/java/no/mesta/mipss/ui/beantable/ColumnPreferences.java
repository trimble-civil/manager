package no.mesta.mipss.ui.beantable;

/**
 * Bean som tar vare på en kolonnes innstillinger.<br>
 * <p>
 * {@code}Comparable{@code} på rekkefølgen: {@code}order{@code}
 * </p>
 * @author harkul
 *
 */
public class ColumnPreferences implements Comparable<ColumnPreferences>{
	/** kolonnens {@code}identifier{@code} */
	private Object id;
	/** hvilken index denne kolonnen har */
	private int order;
	/** om kolonnen er synlig*/
	private boolean visible;
	/** inneholder {@code}preferredSize{@code}*/
	private int width;
	
	@Override
	public int compareTo(ColumnPreferences o) {
		return (order<o.order? -1 : (order==o.order? 0 : 1));
	}
	
	/**
	 * Returnerer denne kolonnens id
	 * @return
	 */
	public Object getId() {
		return id;
	}
	
	/**
	 * Returnerer indexen til kolonnen i tabellen
	 * @return
	 */
	public int getOrder() {
		return order;
	}
	
	/**
	 * Returnerer hvor bred kolonnen ønsker å være 
	 * @return
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Returnerer synligheten til kolonnen
	 * @return
	 */
	public boolean isVisible() {
		return visible;
	}
	
	public void setId(Object id) {
		this.id = id;
	}
	
	public void setOrder(int order) {
		this.order = order;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public void setWidth(int width) {
		this.width = width;
	}
}