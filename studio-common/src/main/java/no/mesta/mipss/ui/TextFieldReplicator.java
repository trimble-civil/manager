package no.mesta.mipss.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;


/**
 * Brukes til å samholde innholdet mellom to tekst felter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class TextFieldReplicator implements FocusListener, ActionListener {
    private Component parentComponent;
    private JTextField srcField;
    private JTextField otherField;
    private Map<?, ?> beans;
    private String keyField;
    private String valueField;
    private Class<?> clazz;
    private boolean ignoreFocusLost = false;

    @SuppressWarnings("unchecked")
	private TextFieldReplicator(Component parentComponent, JTextField srcField, JTextField otherField, Class clazz, Map beans, String keyField, String valueField) {
        this.srcField = srcField;
        this.otherField = otherField;
        this.beans = beans;
        this.keyField = keyField;
        this.valueField = valueField;
        this.clazz = clazz;
        this.parentComponent = parentComponent;
    }

    /**
     * Sørger for at verdien i srcField som holdes i bønnene synkroniseres mellom feltene.
     * 
     * @param srcField
     * @param otherField
     * @param clazz
     * @param beans java.util.Map over bønnene.
     * @param field
     * @param other
     */
    @SuppressWarnings("unchecked")
	public static void decorateTextField(Component parentComponent, JTextField srcField, JTextField otherField , Class clazz, Map beans, String keyField, String valueField) {
        TextFieldReplicator listener = new TextFieldReplicator(parentComponent, srcField, otherField, clazz, beans, keyField, valueField);
        
        srcField.addActionListener(listener);
        srcField.addFocusListener(listener);
        
        otherField.addActionListener(listener);
        otherField.addFocusListener(listener);
    }

    /**
     * @see java.awt.event.FocusListener#focusGained(FocusEvent)
     * @param e
     */
    public void focusGained(FocusEvent e) {
        // Ignore
    }

    /**
     * @see java.awt.event.FocusListener#focusLost(FocusEvent)
     * @param e
     */
    public void focusLost(FocusEvent e) {
        if(!ignoreFocusLost) {
            fireChange((JTextField) e.getSource());
        }
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(ActionEvent)
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        fireChange((JTextField) e.getSource());
    }
    
    private void fireChange(JTextField src) {
        if(StringUtils.isEmpty(src.getText())) {
            return;
        }
        
        JTextField dst;
        boolean useKey = true;
        if(srcField.equals(src)) {
            dst = this.otherField;
        } else {
            dst = this.srcField;
            useKey = false;
        }
        String finalValue = src.getText();
        if(finalValue != null) {
            updatePairField(dst, finalValue, useKey);        
        }
    }

    /**
     * Oppdaterer destinasjonen med verdien som er oppgitt
     * 
     * @param dst feltet som skal oppdateres
     * @param txtFieldValue
     * @param useKey
     */
    private void updatePairField(JTextField dst, String txtFieldValue, boolean useKey) {
        String value;
        
        if(useKey) {
            Object bean = findBeanByKey(txtFieldValue);
            value = getFieldValue(bean, valueField);
            
        } else {
            ignoreFocusLost = true;
            Object[] beanz = findBeansByValue(txtFieldValue);
            if(beanz.length == 1) {
                Object bean = beanz[0];
                value = getFieldValue(bean, keyField);
            } else {
                // presenter bruker med flere valg
                BeanLabel[] options = new BeanLabel[beanz.length];
                for(int i = 0; i < beanz.length; i++) {
                    options[i] = new BeanLabel(beanz[i]);
                }
                BeanLabel selection = (BeanLabel) JOptionPane.showInputDialog(
                    parentComponent,
                    "Flere muligheter finnes for \"" + txtFieldValue + "\"",
                    "Velg entydig verdi",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    null
                    );
                value = selection.getBeanKey().toString();
            }
        }
        dst.setText(value);
        ignoreFocusLost = false;
    }
    
    /**
     * Finner bønna med den gitte nøkkelen
     * 
     * @param key
     * @return java.lang.Object
     */
    private Object findBeanByKey(Object key) {
        return beans.get(key);
    }
    
    /**
     * Finner de bønnene som har den oppgitte verdien
     * 
     * @param searchValue
     * @return java.lang.Object[]
     */
    private Object[] findBeansByValue(String searchValue) {
        List<?> beanz = new ArrayList<Object>(beans.values()); 
        List<Object> foundBeanz = new ArrayList<Object>();
        Iterator<?> it = beanz.iterator();
        while(it.hasNext()) {
            Object bean = it.next();
            String beanValue = getFieldValue(bean, valueField);
            if(StringUtils.equals(searchValue, beanValue)) {
                foundBeanz.add(bean);
            }
        }
        return foundBeanz.toArray();
    }
    
    /**
     * Returnerer verdien av et String felt i en bønne
     * 
     * @param bean
     * @param field
     * @return String verdi av field i bean
     * @throws java.lang.IllegalArgumentException hvis reflection feiler
     */
    private String getFieldValue(Object bean, String field) {
        try {
            Method m = clazz.getMethod("get" + field, (Class<?>[])null);
            return (String) m.invoke(bean,(Object[])null);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException(e);
        }
    }
    
    /**
     * Får å kunne ha et nøkkel+verdi valg i JOptionPane
     * 
     */
    class BeanLabel {
        private Object key;
        private String value;
        
        public BeanLabel(Object bean) {
            value = getFieldValue(bean, valueField);
            key = getFieldValue(bean, keyField);
        }
        
        public Object getBeanKey() {
            return key;
        }
        
        public String getValue() {
            return value;
        }
        
        @Override
        public String toString() {
            return "[" + key + "] " + value;
        }
    }
}
