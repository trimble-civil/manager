package no.mesta.mipss.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.table.DfuTableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * Panel med tabell for valgbare entiteter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JPDAPanel extends JPanel implements ItemListener, PropertyChangeListener {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_ALL = BUNDLE.getGuiString("pdaPanel.all");
	private static final String GUI_TITLE = BUNDLE.getGuiString("pdaPanel.title");
	private static final Logger logger = LoggerFactory.getLogger(JPDAPanel.class);
	private Set<DfuTableObject> allDfuer;
	private JCheckBox alleCheck;
	private String[] columns = new String[] { "valgt", "enhetId", "modell", "navn" };
	private DfuTableObject ingen;
	private Driftkontrakt kontrakt;
	private JXTable table;
	private MipssRenderableEntityTableModel<DfuTableObject> tableModel;
	private Set<DfuTableObject> valgte = new HashSet<DfuTableObject>();

	public JPDAPanel() {
		this(null);
	}

	public JPDAPanel(final Driftkontrakt kontrakt) {
		this.kontrakt = kontrakt;
		initGui();
	}

	private DfuTableObject fetchDfu(Long id) {
		DfuTableObject dfu = null;

		Iterator<DfuTableObject> it = allDfuer.iterator();
		while (it.hasNext()) {
			DfuTableObject search = it.next();
			if (search.getEnhetId() != null && search.equals(id)) {
				dfu = search;
				break;
			}
		}

		return dfu;
	}

	public Long[] getAlleValgte() {
		logger.debug("getAlleValgte() start");

		return setToArray(valgte);
//		List<Long> valgte = new ArrayList<Long>();
//		for (DfuTableObject b:allDfuer){
//			if (b.getValgt()!=null&&b.getValgt()){
//				valgte.add(b.getEnhetId());
//			}
//		}
//		return valgte.toArray(new Long[]{});
	}

	private Set<DfuTableObject> getEnheter() {
		allDfuer = new TreeSet<DfuTableObject>();
		List<Dfuindivid> alle = null;

		if (kontrakt != null) {
			alle = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class).getPdaForKontrakt(kontrakt.getId());
		} else {
			alle = new ArrayList<Dfuindivid>();
		}

		ingen = new DfuTableObject(null, kontrakt);
		ingen.addPropertyChangeListener("valgt", this);
		logger.debug("getEnheter() alle: " + alle);
		for (Dfuindivid p : alle) {
			DfuTableObject dto = new DfuTableObject(p, kontrakt);
			dto.addPropertyChangeListener("valgt", this);
			allDfuer.add(dto);
		}
		allDfuer.add(ingen);

		return allDfuer;
	}

	public Driftkontrakt getKontrakt() {
		return kontrakt;
	}

	private JXTable getTable() {
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				DfuTableObject.class, columns);
		tableModel = new MipssRenderableEntityTableModel<DfuTableObject>(DfuTableObject.class, new int[] { 0 }, columns);
		tableModel.getEntities().clear();
		tableModel.getEntities().addAll(getEnheter());
		JXTable table = new JXTable(tableModel, columnModel);
		table.getColumn(0).setMaxWidth(30);

		return table;
	}

	private void initGui() {
		table = getTable();

		JScrollPane scroll = new JScrollPane(table);

		alleCheck = new JCheckBox(GUI_ALL);
		alleCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox box = (JCheckBox) e.getSource();
				firePropertyChange("set", !isSet(), isSet());
				if (box.isSelected()) {
//					table.setEnabled(false);
					for (DfuTableObject b:allDfuer){
						if(b.getEnhetId()!=null){
							b.setValgt(Boolean.TRUE);
						}else{
							b.setValgt(Boolean.FALSE);//den tomme plassen i listen skal ikke være med
						}
					}
				} else {
					table.setEnabled(true);
					for (DfuTableObject b:allDfuer){
						b.setValgt(Boolean.FALSE);
					}
				}
				table.repaint();
			}
		});
		alleCheck.addItemListener(this);

		Box components = BoxUtil.createVerticalBox(0, BoxUtil.createHorizontalBox(0, scroll), BoxUtil
				.createHorizontalBox(0, alleCheck, Box.createHorizontalGlue()));
		Dimension min = new Dimension(250, 200);
		Dimension pref = min;
		Dimension max = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
		ComponentSizeResources.setComponentSizes(this, min, pref, max);

		components.setBorder(BorderFactory.createTitledBorder(GUI_TITLE));

		setLayout(new BorderLayout());
		add(components, BorderLayout.CENTER);
	}

	public boolean isAlleValgt() {
		return alleCheck.isSelected();
	}
	
	public boolean isIngenValgt() {
		return ingen.getValgt();
	}

	public boolean isSet() {
		Long[] valgte = getAlleValgte();
		return (valgte != null && valgte.length > 0) || isIngenValgt() || isAlleValgt();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		boolean isSet = e.getStateChange() == ItemEvent.SELECTED;

		if (e.getSource() == alleCheck) {
			firePropertyChange("alleValgt", !isSet, isSet);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		logger.debug("propertyChange(" + evt + ") start for " + property);
		DfuTableObject source = (DfuTableObject) evt.getSource();
		if (StringUtils.equals(property, "valgt") && source == ingen) {
			Boolean oldValue = (Boolean) evt.getOldValue();
			Boolean newValue = (Boolean) evt.getNewValue();
			firePropertyChange("ingenValgt", oldValue, newValue);
			firePropertyChange("set", true, false);
		} else if (StringUtils.equals(property, "valgt")) {
			Set<DfuTableObject> old = new HashSet<DfuTableObject>(valgte);
			if (source.getValgt() == true) {
				valgte.add(source);
			} else {
				valgte.remove(source);
			}

			Long[] oldValues = setToArray(old);
			Long[] nowValues = setToArray(valgte);

			logger.debug("fire property change, old = " + Arrays.toString(oldValues) + ", now = "
					+ Arrays.toString(nowValues));
			firePropertyChange("alleValgte", oldValues, nowValues);

			boolean oldSet = oldValues.length > 0;
			boolean newSet = nowValues.length > 0;
			firePropertyChange("set", oldSet, newSet);
		}
	}

	public void reset() {
		boolean oldSet = isSet();
		alleCheck.setSelected(false);
		for (DfuTableObject o : tableModel.getEntities()) {
			o.setValgt(false);
		}
		table.repaint();
		table.setEnabled(true);
		firePropertyChange("set", oldSet, isSet());
	}

	public void setAlleValgt(boolean f) {
		boolean old = isAlleValgt();
		boolean oldSet = isSet();
		alleCheck.setSelected(f);
		table.setEnabled(!f);
		firePropertyChange("alleValgt", old, isAlleValgt());
		firePropertyChange("set", oldSet, isSet());
	}

	public void setAlleValgte(Long[] set) {
		Long[] old = setToArray(valgte);
		boolean oldSet = isSet();
		
		valgte = new HashSet<DfuTableObject>();

		if (set != null) {
			for (Long id : set) {
				DfuTableObject dfu = fetchDfu(id);
				valgte.add(dfu);
			}
		}
		Long[] newValue = setToArray(valgte);
		
		firePropertyChange("set", oldSet, isSet());
		firePropertyChange("alleValgte", old, newValue);
	}

	public void setIngenValgt(boolean f) {
		boolean old = isIngenValgt();
		boolean oldSet = isSet();
		ingen.setValgt(f);
		firePropertyChange("ingenValgt", old, isIngenValgt());
		firePropertyChange("set", oldSet, isSet());
	}

	public void setKontrakt(Driftkontrakt kontrakt) {
		this.kontrakt = kontrakt;
		if (kontrakt != null) {
			tableModel.getEntities().clear();
			tableModel.getEntities().addAll(getEnheter());
		}
	}

	private Long[] setToArray(Set<DfuTableObject> set) {
		Long[] ids = new Long[set.size()];
		Iterator<DfuTableObject> it = set.iterator();
		int i = 0;
		while (it.hasNext()) {
			ids[i] = ((DfuTableObject) it.next()).getEnhetId();
			i++;
		}

		return ids;
	}

	public void setSelectedPdaIds(Long[] pdaDfuIdentList) {
		if (pdaDfuIdentList==null)
			return;
		Arrays.sort(pdaDfuIdentList);
		for (DfuTableObject b:allDfuer){
			if (b.getEnhetId()==null)
				continue;
			if(Arrays.binarySearch(pdaDfuIdentList, b.getEnhetId())>=0){
				b.setValgt(Boolean.TRUE);
			}else{
				b.setValgt(Boolean.FALSE);
			}
		}
		table.repaint();
	}
}
