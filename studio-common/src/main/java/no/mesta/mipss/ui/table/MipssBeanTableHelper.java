package no.mesta.mipss.ui.table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

/**
 * Denne klassen hjelper deg å lage en JMipssBeanTable og holder på modellen, kolonnemodellen og tabellen. 
 * 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class MipssBeanTableHelper<T extends IRenderableMipssEntity> {

	private final int[] editableColumns;
	private final Class<T> entityClass;
	private final Object beanInstance;
	private final Class<?> beanInterface;
	private final String beanMethod;
	@SuppressWarnings("unchecked")
	private final Class[] beanMethodArguments;
	private final String[] columns;
	
	private MipssBeanTableModel<T> model;
	private MipssRenderableEntityTableColumnModel colModel;
	private JMipssBeanTable<T> table;
	
	/**
	 * Oppretter en ny MipssBeanTableCreator
	 * @param editableColumns kolonne indexene som skal være editerbare, hvis denne er null er tabellen ikke editerbar
	 * @param entityClass klassen som er grunnlaget for dataene. 
	 * @param beanInstance instansen til bønnen som inneholder metoder for å hente data til tabellen
	 * @param beanInterface interfacet til bønnen
	 * @param beanMethod metoden som kalles for å hente data fra bønnen. (kalles gjennom loadData() på modellen)
	 * @param beanMethodArguments klassetypene som er parametere til bønnemetoden for henting av data
	 * @param columns navn på kolonnene som skal være med i tabellen. (kolonnene må ha samme navn som instansvariablene til entitietsklassen, og tilhørende get-metoder)
	 */
	@SuppressWarnings("unchecked")
	public MipssBeanTableHelper(int[] editableColumns, Class<T> entityClass,  
			Object beanInstance, Class<?> beanInterface, String beanMethod, Class[] beanMethodArguments, 
			String... columns){
				this.editableColumns = editableColumns;
				this.entityClass = entityClass;
				this.beanInstance = beanInstance;
				this.beanInterface = beanInterface;
				this.beanMethod = beanMethod;
				this.beanMethodArguments = beanMethodArguments;
				this.columns = columns;
	}
	
	/**
	 * Returnerer modellen, hvis denne er null instansieres den
	 * @return
	 */
	public MipssBeanTableModel<T> getModel(){
		if (model==null){
			if (editableColumns==null){
				model = new MipssBeanTableModel<T>(entityClass, columns);
			}else{
				model = new MipssBeanTableModel<T>(entityClass, editableColumns, columns);
			}
			model.setBeanInstance(beanInstance);
			model.setBeanInterface(beanInterface);
			model.setBeanMethod(beanMethod);
			model.setBeanMethodArguments(beanMethodArguments);
		}
		return model;
	}
	
	/**
	 * Returnerer kolonnemodellen, dersom denne er null instansieres den. Denne metoden instansierer også modellen
	 * @return
	 */
	public MipssRenderableEntityTableColumnModel getColumnModel(){
		if (colModel==null){
			colModel = new MipssRenderableEntityTableColumnModel(entityClass, getModel().getColumns());
		}
		return colModel;
	}
	
	/**
	 * Returnerer tabellen, dersom denne er null instansieres den. Denne metoden instansierer
	 * også kolonnemodellen og modellen
	 * @return
	 */
	public JMipssBeanTable<T> getTable(){
		if (table==null){
			table = new JMipssBeanTable<T>(getModel(), getColumnModel());
			table.setEditable(editableColumns!=null);
		}
		return table;
	}
}
