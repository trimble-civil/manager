package no.mesta.mipss.ui;

import no.mesta.mipss.resources.images.IconResources;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.PropertySetter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class SpeedometerPanel extends JPanel implements ChangeListener{

	private final Image background = IconResources.SPEEDOMETER.getImage();
	private final Image needle = IconResources.SPEEDOMETER_NEEDLE.getImage();
	private final Dimension size = new Dimension(180, 109);

	private final static int MAX_ANGLE = 90;
	private final static int MIN_ANGLE = -90;
	
	private BoundedRangeModel model;
	private double prevValue=1;
	private double value=0;
	private Animator currentAnimation;
	
	
	
	public static void main(String[] args) {

		JFrame f = new JFrame();
		f.setAlwaysOnTop(true);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpeedometerPanel sp = new SpeedometerPanel();
		
		final BoundedRangeModel m = new DefaultBoundedRangeModel();
		m.addChangeListener(sp);
		sp.setModel(m);		
		m.setMinimum(-10);
		m.setMaximum(100);
		m.setValue(50);
		
		
		f.setLayout(new BorderLayout());
		f.add(sp);
		f.pack();
		f.setVisible(true);
		
	}
	
	public SpeedometerPanel(){
		
	}
	
	public void setModel(BoundedRangeModel model){
		this.model = model;
	}

	
	@Override
	public void paint(Graphics g){
		Graphics2D gr = (Graphics2D)g;
		gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		double mmin = model.getMinimum();
		double mmax = model.getMaximum();
		
		double min = mmin<mmax?mmin:mmax;
		double max = mmax>mmin?mmax:mmin;
		value-= min;
		double f = max-min;
		min=0;
		max=f;

		double d = 180/(max-min);
		double ang = value*d-90;
		System.out.println(ang+ " "+d+" "+max+" "+min);
		
		gr.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		
		double fp = getWidth()*0.015;
		int x = getWidth()/2-(int)fp;
		int y = 26;
		gr.rotate(Math.toRadians(ang), x+needle.getWidth(this)/2, y+needle.getHeight(this)-3);
		
		gr.drawImage(needle, x, y, null);
	}
	
	public Dimension getPreferredSize(){
		return size;
	}

	private void animateToAngle(){
		if (currentAnimation==null||!currentAnimation.isRunning()){
			int traveltime = 500;
			currentAnimation = PropertySetter.createAnimator(traveltime,this, "value", new Double[] { prevValue, value});
			currentAnimation.start();
		}
	}
	
	public void setValue(double value){
		this.value = value;
		repaint();
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		value = ((BoundedRangeModel)e.getSource()).getValue();
		animateToAngle();
		prevValue = ((BoundedRangeModel)e.getSource()).getValue();
	}

}
