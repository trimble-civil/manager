package no.mesta.mipss.ui.pluginlist;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.dto.MenynodeDTO;
import no.mesta.mipss.plugin.MipssPlugin;

import org.apache.commons.lang.builder.CompareToBuilder;

@SuppressWarnings("serial")
public class JMipssPluginButtonList extends JPanel implements PluginListModelListener {
	
	private IMipssStudioLoader loader;
	private PluginListModel model;
	
	public JMipssPluginButtonList(IMipssStudioLoader loader){
		setBackground(Color.white);
		this.loader = loader;
		model = loader.getRunningPluginsKeys();
		model.addPluginListModelListener(this);
		populateMenu();
	}
	
	/**
	 * Fyller ut menyen
	 * 
	 */
	private void populateMenu() {
		removeButtons();
		
		
		List<MipssPlugin> loadedPlugins = model.getPlugins();
		List<MipssPlugin> plugins = new ArrayList<MipssPlugin>();
		for (MipssPlugin plugin : loadedPlugins) {
			if (plugin.isDocked()) {
				plugins.add(plugin);
			}
		}

		int height = plugins.size()/7+1;
		GridLayout layout = new GridLayout(height, 7);
		setLayout(layout);
		MipssPlugin[] sortedPlugins = new MipssPlugin[plugins.size()];

		if (sortedPlugins.length == 0) {
			setEnabled(false);
			return;
		}

		setEnabled(true);
		sortedPlugins = plugins.toArray(sortedPlugins);
		Arrays.sort(sortedPlugins, new PluginComparator());
		int padding = 7-plugins.size()%7;
		for (int i=0;i<padding;i++){
			JPanel p = new JPanel();
			p.setLayout(null);
			Dimension size = new Dimension(20, 20);
			p.setPreferredSize(size);
			p.setMaximumSize(size);
			p.setMinimumSize(size);
			p.setOpaque(false);
			add(p);
		}
		for (final MipssPlugin plugin : sortedPlugins) {
			JButton button = new JButton();
			button.setToolTipText(plugin.getDisplayName());
			if (plugin.getIcon() != null) {
				button.setIcon(plugin.getIcon());
			}
			if (plugin.getStatus() != MipssPlugin.Status.RUNNING) {
				button.setEnabled(false);
			}
			Dimension size = new Dimension(25, 25);
			button.setPreferredSize(size);
			button.setMaximumSize(size);
			button.setMinimumSize(size);
			button.addActionListener(new ItemListener(plugin));
			button.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){
					if (e.getModifiersEx()==MouseEvent.CTRL_DOWN_MASK){
						loader.closePlugin(plugin);
					}
				}
			});
			add(button);
		}
		
	}
	
	private void removeButtons(){
		for (Component c:getComponents()){
			remove(c);
		}
	}
	
	/**
	 * Action listener for meny items
	 * 
	 */
	class ItemListener implements ActionListener {
		private MipssPlugin plugin = null;

		public ItemListener(MipssPlugin plugin) {
			this.plugin = plugin;
		}

		/**
		 * Sørger for at plugin som item viser til vises i docken
		 * 
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			MenynodeDTO menyNode = plugin.getMenynode();
			if(menyNode != null) {
				menyNode.setFjernValgt(true);
				loader.activateMenuItem(menyNode);
				menyNode.setFjernValgt(false);
			}
			loader.activatePlugin(plugin.getCacheKey());
		}
	}

	
	/**
	 * Brukes til å sortere plugins
	 * 
	 */
	class PluginComparator implements Comparator<MipssPlugin> {

		/**
		 * @see java.util.Comparator#compare(T,T)
		 * 
		 * @param p1
		 * @param p2
		 * @return
		 */
		public int compare(MipssPlugin p1, MipssPlugin p2) {
			return new CompareToBuilder().append(p1.getDisplayName(), p2.getDisplayName()).toComparison();
		}
	}
	@Override
	public void modelChanged(PluginListModelEvent event) {
		populateMenu();
		revalidate();
		
	}
	
}
