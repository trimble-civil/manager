package no.mesta.mipss.ui.table;

import java.awt.Component;
import java.awt.Cursor;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.valueobjects.Epostadresse;

@SuppressWarnings("serial")
public class EpostadresseTableCellRenderer extends DefaultTableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lbl = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		if(value != null) {
			final Epostadresse epostadresse = (Epostadresse)value;
			lbl.setText("<html><a href=''>" + epostadresse.getAdresse() + "</a></html>");
		}
		
		return lbl;
	}
}
