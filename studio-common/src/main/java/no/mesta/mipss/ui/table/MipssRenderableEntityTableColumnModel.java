package no.mesta.mipss.ui.table;

import java.util.Enumeration;
import java.util.List;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import javax.swing.ListSelectionModel;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;


/**
 * Wrapper for DefaultTableColumnModel
 * 
 * @see javax.swing.table.DefaultTableColumnModel
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssRenderableEntityTableColumnModel implements TableColumnModelExt{
    private PropertyResourceBundle bundle = null;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private DefaultTableColumnModelExt tableColumnModel = new DefaultTableColumnModelExt();

	private final Class<?> clazz;

	private final String[] columns;
        
    /**
     * Standard konstruktør for klassen
     * 
     * @param clazz
     * @param columns
     */
    public MipssRenderableEntityTableColumnModel(Class<?> clazz, String... columns) {
        this.clazz = clazz;
		this.columns = columns;
		log.trace("MipssRenderableEntityTableColumnModel(" + clazz + ", " + columns + ") start");
        bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("tableColumns");
        
        int columnIndex = 0;
        for(String s:columns) {
            String identifier = clazz.getName() + "." + s;
            TableColumnExt tc = new TableColumnExt();
            String header;
            try{
                header = bundle.getString(identifier);
            } catch (MissingResourceException e) {
                header = "TODO: " + identifier;
            }
            
            tc.setIdentifier(identifier);
            tc.setModelIndex(columnIndex);
            tc.setHeaderValue(header);
            
            tableColumnModel.addColumn(tc);
            columnIndex++;
        }
    }

    /**
     * Legger til en ny kollonne i modellen
     * 
     * @see javax.swing.table.DefaultTableColumnModel#addColumn
     * @see javax.swing.table.TableColumnModel#addColumn
     * @param aColumn kollonnen som legges til
     */
    public void addColumn(TableColumn aColumn) {
        tableColumnModel.addColumn(aColumn);
    }

    /**
     * Legger til en lytter
     * 
     * @see javax.swing.table.DefaultTableColumnModel#addColumnModelListener
     * @see javax.swing.table.TableColumnModel#addColumnModelListener
     * @param x lytteren
     */
    public void addColumnModelListener(TableColumnModelListener x) {
        tableColumnModel.addColumnModelListener(x);
    }

    /**
     * Henter en gitt kollonne basert på index i modellen
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumn
     * @see javax.swing.table.TableColumnModel#getColumn
     * @param columnIndex
     * @return
     */
    public TableColumn getColumn(int columnIndex) {
        //log.trace("getColumn(" + columnIndex + ") start");
        TableColumn tc = tableColumnModel.getColumn(columnIndex);
        //log.trace("getColumn(" + columnIndex + ") ferdig: " + tc.getHeaderValue());
        return tc;
    }

    /**
     * Henter antallet kollonner
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumnCount
     * @see javax.swing.table.TableColumnModel#getColumnCount
     * @return antallet
     */
    public int getColumnCount() {
        return tableColumnModel.getColumnCount();
    }

    /** {@inheritDoc} */
	@Override
	public int getColumnCount(boolean includeHidden) {
		return tableColumnModel.getColumnCount(includeHidden);
	}

    /** {@inheritDoc} */
	@Override
	public TableColumnExt getColumnExt(int columnIndex) {
		return tableColumnModel.getColumnExt(columnIndex);
	}

    /** {@inheritDoc} */
	@Override
	public TableColumnExt getColumnExt(Object identifier) {
		return tableColumnModel.getColumnExt(identifier);
	}

    /**
     * Henter en gitt kollonne basert på navn
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumnIndex
     * @see javax.swing.table.TableColumnModel#getColumnIndex
     * @param columnIdentifier
     * @return
     */
    public int getColumnIndex(Object identifier) {
//        log.debug("getColumnIndex(" + columnIdentifier + ") start");
//        return tableColumnModel.getColumnIndex(columnIdentifier);
    	
    	if (identifier == null) {
		    throw new IllegalArgumentException("Identifier is null");
		}

		List<TableColumn> columns = getColumns(true);
		
		int index = 0;
		for (TableColumn tc:columns){
			if (identifier.equals(tc.getIdentifier()))
				return index;
			    index++;
		}
		throw new IllegalArgumentException("Identifier not found");
    }

    /**
     * Henter en gitt kollonne på en x posisjon i viewet
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumnIndexAtX
     * @see javax.swing.table.TableColumnModel#getColumnIndexAtX
     * @param xPosition
     * @return
     */
    public int getColumnIndexAtX(int xPosition) {
        return tableColumnModel.getColumnIndexAtX(xPosition);
    }

    /**
     * Henter verdien for margen
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumnMargin
     * @see javax.swing.table.TableColumnModel#getColumnMargin
     * @return
     */
    public int getColumnMargin() {
        return tableColumnModel.getColumnMargin();
    }

    /**
     * Henter en enumerasjon over alle kollonner i modellen
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumns
     * @see javax.swing.table.TableColumnModel#getColumns
     * @return
     */
    public Enumeration<TableColumn> getColumns() {
        return tableColumnModel.getColumns();
    }

    /** {@inheritDoc} */
	@Override
	public List<TableColumn> getColumns(boolean includeHidden) {
		return tableColumnModel.getColumns(includeHidden);
	}

    /**
     * Henter om det er log å velge kollonner
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getColumnSelectionAllowed
     * @see javax.swing.table.TableColumnModel#getColumnSelectionAllowed
     * @return
     */
    public boolean getColumnSelectionAllowed() {
        return tableColumnModel.getColumnSelectionAllowed();
    }

    /**
     * Henter antallet valgte kollonner
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getSelectedColumnCount
     * @see javax.swing.table.TableColumnModel#getSelectedColumnCount
     * @return
     */
    public int getSelectedColumnCount() {
        return tableColumnModel.getSelectedColumnCount();
    }

    /**
     * Henter valgte kollonner
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getSelectedColumns
     * @see javax.swing.table.TableColumnModel#getSelectedColumns
     * @return
     */
    public int[] getSelectedColumns() {
        return tableColumnModel.getSelectedColumns();
    }

    /**
     * Henter ut modellen for valg
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getSelectionModel
     * @see javax.swing.table.TableColumnModel#getSelectionModel
     * @return modellen
     */
    public ListSelectionModel getSelectionModel() {
        return tableColumnModel.getSelectionModel();
    }

    /**
     * Henter den totale bredden av alle kollonner
     * 
     * @see javax.swing.table.DefaultTableColumnModel#getTotalColumnWidth
     * @see javax.swing.table.TableColumnModel#getTotalColumnWidth
     * @return
     */
    public int getTotalColumnWidth() {
        return tableColumnModel.getTotalColumnWidth();
    }

    /**
     * Flytter en kollonne
     * 
     * @see javax.swing.table.DefaultTableColumnModel#moveColumn
     * @see javax.swing.table.TableColumnModel#moveColumn
     * @param columnIndex fra index
     * @param newIndex til index
     */
    public void moveColumn(int columnIndex, int newIndex) {
        tableColumnModel.moveColumn(columnIndex, newIndex);
    }

    /**
     * Fjerner en kollonne i modellen
     * 
     * @see javax.swing.table.DefaultTableColumnModel#removeColumn
     * @see javax.swing.table.TableColumnModel#removeColumn
     * @param column kollonnen som legges til
     */
    public void removeColumn(TableColumn column) {
        tableColumnModel.removeColumn(column);
    }

    /**
     * Fjerner en lytter
     * 
     * @see javax.swing.table.DefaultTableColumnModel#removeColumnModelListener
     * @see javax.swing.table.TableColumnModel#removeColumnModelListener
     * @param x lytteren
     */
    public void removeColumnModelListener(TableColumnModelListener x) {
        tableColumnModel.removeColumnModelListener(x);
    }

    /**
     * Setter margen for kollonnene
     * 
     * @see javax.swing.table.DefaultTableColumnModel#setColumnMargin
     * @see javax.swing.table.TableColumnModel#setColumnMargin
     * @param newMargin nye margen
     */
    public void setColumnMargin(int newMargin) {
        tableColumnModel.setColumnMargin(newMargin);
    }

    /**
     * Setter om det er lov å velge kollonner
     * 
     * @see javax.swing.table.DefaultTableColumnModel#setColumnSelectionAllowed
     * @see javax.swing.table.TableColumnModel#setColumnSelectionAllowed
     * @param flag
     */
    public void setColumnSelectionAllowed(boolean flag) {
        tableColumnModel.setColumnSelectionAllowed(flag);
    }

    /**
     * Setter en modell for valg
     * 
     * @see javax.swing.table.DefaultTableColumnModel#setSelectionModel
     * @see javax.swing.table.TableColumnModel#setSelectionModel
     * @param newModel den nye modellen
     */
    public void setSelectionModel(ListSelectionModel newModel) {
        tableColumnModel.setSelectionModel(newModel);
    }
}
