package no.mesta.mipss.ui.list;

import java.lang.reflect.Array;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.MipssListCellRenderer;

import org.jdesktop.swingx.JXList;

/**
 * En JList implementasjon.
 * 
 * Generics er brukt for å gjøre den typesikker mot en gitt entitet type.
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public class JMipssList<T extends IRenderableMipssEntity> extends JXList {
	private final Class<T> clazz;

	/**
	 * Hvis isSortEnabled = true vil listen være sortert.
	 * 
	 * @see org.jdesktop.swingx.JXList
	 * @param model
	 * @param isSortEnabled
	 */
	public JMipssList(MipssRenderableEntityModel<T> model, boolean isSortEnabled, Class<T> clazz) {
		super(model, isSortEnabled);
		this.clazz = clazz;
	}

	/**
	 * Lager en swingxlabs JXList uten sortering
	 * 
	 * @see org.jdesktop.swingx.JXList
	 * @param model
	 */
	public JMipssList(MipssRenderableEntityModel<T> model, Class<T> clazz) {
		super(model);
		this.clazz = clazz;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public T getSelectedValue() {
		return (T) super.getSelectedValue();
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public T[] getSelectedValues() {
		T[] t = (T[]) Array.newInstance(clazz, super.getSelectedValues().length);
		int i = 0;
		for (Object o : super.getSelectedValues()) {
			t[i] = (T) o;
			i++;
		}
		return t;
	}

	/**
	 * Her settes en mipssrenderer slik at getTextForGUI() benyttes
	 * 
	 * @param renderer
	 */
	public void setCellRenderer(MipssListCellRenderer<T> renderer) {
		super.setCellRenderer(renderer);
	}
}
