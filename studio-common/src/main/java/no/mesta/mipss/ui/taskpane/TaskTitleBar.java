package no.mesta.mipss.ui.taskpane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXCollapsiblePane;

@SuppressWarnings("serial")
public class TaskTitleBar extends JPanel{
	private int curve = 15;
    private Insets margin = new Insets(1,0,1,1);
    private Color grStart = new Color(250,250,250);
	private Color grEnd = new Color(178, 200, 255);
	private float[] fractions = new float[]{0,1};
	private Color[] colors = new Color[]{grStart, grEnd};
	
	public TaskTitleBar(JXCollapsiblePane contentPane){
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(100, 30));
		setPreferredSize(new Dimension(100, 30));
		
		add(new TaskToggleButton(contentPane), BorderLayout.EAST);
		
	}
	@Override
	public void paintComponent(Graphics gr){
		super.paintComponent( gr ); 
		GeneralPath bg = new GeneralPath();
	    bg.moveTo(margin.left, getHeight()-margin.bottom);
	    bg.lineTo(getWidth()-margin.right, getHeight()-margin.bottom);
	    bg.lineTo(getWidth()-margin.right, curve+margin.top);
	    bg.quadTo(getWidth()-margin.right, margin.right, getWidth()-margin.right-curve, margin.top);
	    bg.lineTo(curve+margin.left, margin.top);
	    bg.quadTo(margin.left, margin.top, margin.left, curve+margin.top);
		bg.closePath();
		Graphics2D g = (Graphics2D)gr;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setPaint(new LinearGradientPaint(0,0,0,getHeight(),fractions, colors));
		g.fill(bg);
		g.setPaint(Color.gray);
		g.draw(bg);
		
	}
	@Override
	public int getHeight(){
		return 30;
	}
	
	public void setContents(JComponent... components){
		JPanel panel = new JPanel(new FlowLayout());
		panel.setOpaque(false);
		for (JComponent c:components){
			c.setOpaque(false);
			panel.add(c);
		}
		add(panel, BorderLayout.CENTER);
	}
	public void setContent(JComponent contents){
		contents.setOpaque(false);
		add(contents, BorderLayout.CENTER);
	}
	
	public void setLeftComponent(JComponent component){
		JPanel pnl = new JPanel();
		pnl.setLayout(new BoxLayout(pnl, BoxLayout.LINE_AXIS));
		pnl.add(Box.createHorizontalStrut(15));
		pnl.add(component);
		pnl.setOpaque(false);
		add(pnl, BorderLayout.WEST);
	}
}
