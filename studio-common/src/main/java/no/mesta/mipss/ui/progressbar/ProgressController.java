package no.mesta.mipss.ui.progressbar;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Controllerklasse for å utføre oppgaver serielt, eller parallelt. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProgressController {
	
	private ExecutorService executorService =  Executors.newFixedThreadPool(1);
	private ProgressPanel panel;
	
	public ProgressController(ProgressPanel panel){
		this.panel = panel;
	}
	
	
	public Future addWorkerPanel(final ProgressWorkerPanel w){
			Future future = null;
			if (w.getWorker().getTaskType()==ExecutorTaskType.QUEUE){
				future = executorService.submit(w.getWorker().getWorker());
			}if (w.getWorker().getTaskType()==ExecutorTaskType.PARALELL){
				new Thread(){
					public void run(){
						w.getWorker().getWorker().execute();
					}
				}.start();
			}
			if (panel!=null){
				panel.addProgress(w);
			}
			
			return future;
	}
	public Future addWorker(final MipssProgressBarTask w){
		Future future = null;
		if (w.getTaskType()==ExecutorTaskType.QUEUE){
			future = executorService.submit(w.getWorker());
		}if (w.getTaskType()==ExecutorTaskType.PARALELL){
			new Thread(){
				public void run(){
					w.getWorker().execute();
				}
			}.start();
		}
		
		return future;
}
	
	public void removeWorkerPanel(ProgressWorkerPanel w){
		if (panel!=null){
			panel.removeProgress(w);
		}
	}
}
