package no.mesta.mipss.ui.popuplistmenu;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Lager en knapp som viser listen over menypunkter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JPopupListMenu extends JButton {
	private static final Logger logger = LoggerFactory.getLogger(JPopupListMenu.class);
	private final List<JMenuItem> items;
	private final JPopupMenu menu = new JPopupMenu();

	/**
	 * Konstruktør
	 * 
	 * @param text
	 * @param icon
	 */
	public JPopupListMenu(String text, Icon icon, List<JMenuItem> items) {
		super(text, icon);
		this.items = items;
		populateMenu();

		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Point p = new Point(getXpos(), getYpos());
				menu.show(JPopupListMenu.this, (int) p.getX(), (int) p.getY());
				p.setLocation(getXpos(), getYpos());
				SwingUtilities.convertPointToScreen(p, JPopupListMenu.this);
				menu.setLocation(p);
			}

			private int getYpos() {
				return -menu.getHeight();
			}

			private int getXpos() {
				return -(menu.getWidth() - JPopupListMenu.this.getWidth());
			}
		});
	}

	/**
	 * Fyller ut menyen
	 * 
	 */
	private void populateMenu() {
		logger.debug("populateMenu() start");
		menu.removeAll();

		setEnabled(true);

		for (JMenuItem item : items) {
			menu.add(item);
		}
		logger.debug("populateMenu() finish");
	}
}
