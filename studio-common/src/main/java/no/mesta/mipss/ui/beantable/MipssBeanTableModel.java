package no.mesta.mipss.ui.beantable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.SwingWorker;
import javax.swing.event.ListDataListener;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Holder data, men kan også laste de fra en bønne, via en oppgitt metode
 * 
 * Har støtte for EL-Property ved lesning av data.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssBeanTableModel<T extends IRenderableMipssEntity> extends MipssRenderableEntityTableModel<T>{
    private static final Logger log = LoggerFactory.getLogger(MipssBeanTableModel.class);

    private Object beanInstance = null;
    private String beanMethod = null;
    private Method method = null;
    private Class<?> beanInterface = null;
    private Class<?>[] beanMethodArguments = null;
    private Object[] beanMethodArgValues = null;
    private List<MipssBeanLoaderListener> listeners = new ArrayList<MipssBeanLoaderListener>();
    private boolean loadingData = false;
    /**
     * Konstruktør
     * 
     * @param clazz
     * @param beanColumns
     */
    public MipssBeanTableModel(Class<T> clazz, String... beanColumns) {
        super(clazz, beanColumns);
    }
    public MipssBeanTableModel(Class<T> clazz, int[] editableColumnIndices, boolean allowNullCellsEditable, String... beanColumns) {
    	super(clazz, editableColumnIndices, allowNullCellsEditable, beanColumns);
    }
    public MipssBeanTableModel(Class<T> clazz, int[] editableColumnIndices, String... beanColumns) {
        super(clazz, editableColumnIndices, beanColumns);
    }
    
    public MipssBeanTableModel(String propertyName, Class<T> clazz, String... beanColumns) {
    	super(propertyName, clazz, beanColumns);
    }
    
    public MipssBeanTableModel(String propertyName, Class<T> clazz, int[] editableColumnIndices, String... beanColumns) {
    	super(propertyName, clazz, editableColumnIndices, beanColumns);
    }
    
    /**
     * Laster data til tabellen. Kan også kaste exceptions kastet av 
     * <code>constructMethod()</code>
     * 
     * @throws java.lang.IllegalStateException hvis beanInstance ikke er satt
     * @see java.lang.reflect.InvocationTargetException
     * @see java.lang.IllegalAccessException
     * @see #constructMethod() 
     * @see #setBeanInstance(Object)
     */
    public void loadData() {
    	
        if(method == null) {
            method = constructMethod();
        }
        
        if(beanInstance == null) {
            throw new IllegalStateException("No beanInstance set!");
        }
        
        SwingWorker<List<T>, Void> worker = new SwingWorker<List<T>, Void>() {
            private boolean errors = false;
            
            @SuppressWarnings("unchecked")
			protected List<T> doInBackground() {
            	if (beanMethodArgValues==null&&beanMethodArguments!=null)
            		return null;
            	
                log.debug("loader begins(doInBackground()");
                List<T> entities = null;
                try {
                    log.debug("Will call: " + method + " with " + Arrays.toString(beanMethodArgValues));
                    entities = (List<T>)method.invoke(beanInstance, beanMethodArgValues);
                } catch (IllegalAccessException e) {
                	e.printStackTrace();
                    errors = true;
                    log.error("No access to method", e);
                } catch (InvocationTargetException e) {
                	e.printStackTrace();
                    errors = true;
                    log.error("Method threw exception", e);
                    log.error("Cause was: ", e.getCause());
                } catch (Throwable t){
                	t.printStackTrace();
                	errors = true;
                }

                if(errors) {                    
                    entities = new ArrayList<T>();
                    
                }
                
                setEntities(entities);
                log.debug("loader done(doInBackground()");
                loadingData = false;
//                loadingDone(errors);
                return entities;
            }
            
            @Override
            protected  void done() {
                loadingDone(errors);
                log.debug("Called loading done...");
            }
        };
        
        fireLoadingStartedEvent(new MipssBeanLoaderEvent(this, false));
        loadingData = true;
        worker.execute();
    }
    
    /**
     * Kalles av tråden som laster data når den er ferdig
     * 
     */
    protected void loadingDone(boolean errors) {
        loadingData = false;
        fireLoadingFinishedEvent(new MipssBeanLoaderEvent(this, errors));
    }
    
    /**
     * Konstruerer et metodeobjekt som kan brukes til å laste data
     * 
     * @return Metode instansen for gitt <code>beanInteface</code>(interface), 
     * <code>beanMethod</code>(navn) og <code>beanMethodArguments</code>
     * @throws java.lang.IllegalStateException hvis interface eller metode 
     * ikke er satt
     * @throws java.lang.IllegalArgumentException hvis interface ikke har
     * metoden
     * @see #setBeanMethodArguments(Class[])
     * @see #setBeanMethod(String)
     * @see #setBeanInterface(Class)
     */
    private Method constructMethod() {
        if(beanInterface == null) {
            throw new IllegalStateException("No beanInterface set!");
        }
        
        if(beanMethod == null) {
            throw new IllegalStateException("No beanMethod set!");
        }
        
        try {
            method = beanInterface.getMethod(beanMethod, beanMethodArguments);
        } catch (NoSuchMethodException e) {
            log.error("Unable to fnid method {}.{}", beanMethod, beanMethodArguments);
            throw new IllegalArgumentException("No such method!", e);
        }
        
        return method;
    }
    
    /**
     * Lytterne mottar beskjed når lasting har startet og stoppet
     * 
     * @param l
     */
    public void addTableLoaderListener(MipssBeanLoaderListener l) {
        listeners.add(l);
    }
    
    /**
     * Lytterne mottar beskjed når lasting har startet og stoppet
     * 
     * @param l
     */
    public void removeTableLoaderListener(MipssBeanLoaderListener l) {
        listeners.remove(l);
    }
    
    /**
     * Notifiserer alle lyttere om at loading har startet
     * 
     * @param e
     */
    private void fireLoadingStartedEvent(MipssBeanLoaderEvent e) {
        for(MipssBeanLoaderListener l:listeners) {
            l.loadingStarted(e);
        }
    }
    
    /**
     * Notifiserer alle lyttere om at loading er ferdig
     * 
     * @param e
     */
    private void fireLoadingFinishedEvent(MipssBeanLoaderEvent e) {
        for(MipssBeanLoaderListener l:listeners) {
            l.loadingFinished(e);
            if (e.isConsumed())
            	return;
        }
    }

    /**
     * Setter argumenttypene for metode som laster data
     * 
     * @param beanMethodArguments
     */
    public void setBeanMethodArguments(Class<?>[] beanMethodArguments) {
        this.beanMethodArguments = beanMethodArguments;
    }

    /**
     * Setter metode argument verdiene
     * 
     * @param beanMethodArgValues
     */
    public void setBeanMethodArgValues(Object[] beanMethodArgValues) {
        this.beanMethodArgValues = beanMethodArgValues;
    }
    
    /**
     * Setter bønne instansen som har metoden som laster dataene
     * 
     * @param beanInstance
     */
    public void setBeanInstance(Object beanInstance) {
        this.beanInstance = beanInstance;
    }

    /**
     * Navnet på bønne metoden
     * 
     * @param beanMethod
     */
    public void setBeanMethod(String beanMethod) {
        this.beanMethod = beanMethod;
    }

    /**
     * Interface for bønna som laster data
     * 
     * @param beanInteface
     */
    public void setBeanInterface(Class<?> beanInteface) {
        this.beanInterface = beanInteface;
    }

    /**
     * Kan kalles for å undersøke om modellen er opptatt
     * 
     * @return
     */
    public boolean isLoadingData() {
        return loadingData;
    }

}
