package no.mesta.mipss.ui.progressbar;

public enum ExecutorTaskType {
	
	/** definerer en task som skal utføres serielt i en kø */
	QUEUE,
	/** definerer en task som kan kjøres utenfor køen paralelt med andre tasker */
	PARALELL
	
}
