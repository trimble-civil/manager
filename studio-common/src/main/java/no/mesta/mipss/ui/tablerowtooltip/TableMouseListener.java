package no.mesta.mipss.ui.tablerowtooltip;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import no.mesta.mipss.ui.beantable.JMipssBeanTable;

import org.jdesktop.swingx.JXPanel;


/**
 * Lytter på en tabell og sier popper opp en rad avhengig tooltip hvis pekeren
 * hviler på raden
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class TableMouseListener<T,V extends JXPanel> implements MouseMotionListener, MouseListener, MouseWheelListener {
    private int row = -1;
    private JTableRowTooltipCountDown<T,V> countDown = null;
    private JMipssBeanTable<?> table = null;
    private Class<?> viewClass = null;
    private Class<?> rowClass = null;
    
    /**
     * Konstruktør
     * 
     * @param table
     */
    public TableMouseListener(JMipssBeanTable<?> table, Class<?> viewClass, Class<?> rowClass) {
        this.table = table;
        this.viewClass = viewClass;
        this.rowClass = rowClass;
    }
    
    /** {@inheritDoc} */
    public void mouseDragged(MouseEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if(countDown != null) {
            countDown.reset(row, e.getPoint());
        }
    }

    /** {@inheritDoc} */
    public void mouseMoved(MouseEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if (countDown == null) {
            if (row != -1) {
                countDown = 
                        new JTableRowTooltipCountDown<T,V>(this, row, e.getPoint(), table, viewClass, rowClass);
                countDown.execute();
            }
        } else if (countDown != null) {
            countDown.reset(row, e.getPoint());
        }
    }

    public void clearCountDown() {
        countDown = null;
    }

    /** {@inheritDoc} */
    public void mouseClicked(MouseEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if (countDown != null) {
            if(e.getClickCount() == 2) {
                countDown.kill();
                countDown = null;
            } else {
                countDown.reset(row, e.getPoint());
            }
        }
    }

    /** {@inheritDoc} */
    public void mousePressed(MouseEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if(countDown != null) {
            countDown.reset(row, e.getPoint());
        }
    }

    /** {@inheritDoc} */
    public void mouseReleased(MouseEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if(countDown != null) {
            countDown.reset(row, e.getPoint());
        }
    }

    /** {@inheritDoc} */
    public void mouseEntered(MouseEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if(countDown != null) {
            countDown.reset(row, e.getPoint());
        }
    }

    /** {@inheritDoc} */
    public void mouseExited(MouseEvent e) {
        if (countDown != null) {
            countDown.kill();
            countDown = null;
        }
    }

    /** {@inheritDoc} */
    public void mouseWheelMoved(MouseWheelEvent e) {
        row = table.rowAtPoint(e.getPoint());
        if(countDown != null) {
            countDown.reset(row, e.getPoint());
        }
    }
}
