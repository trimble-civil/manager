package no.mesta.mipss.ui.progressbar;

import javax.swing.SwingWorker;

public interface MipssProgressBarTask<T, E> {

	public abstract void setProgressController(
			ProgressController progressController);

	public abstract void setProgressWorkerPanel(
			ProgressWorkerPanel progressWorkerPanel);

	public abstract void cancelTask();

	public abstract boolean isTaskCancelled();

	public abstract ExecutorTaskType getTaskType();
	
	public SwingWorker<T,E> getWorker();

}