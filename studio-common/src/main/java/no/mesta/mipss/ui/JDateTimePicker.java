package no.mesta.mipss.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.Clock;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXDatePicker;

/**
 * En komponent for å velge dato og klokkeslett
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDateTimePicker extends JPanel implements PropertyChangeListener{
	private static final Logger logger = LoggerFactory.getLogger(JDateTimePicker.class);
	private static final long serialVersionUID = -7902610846392117831L;
	private Date currentDate;
	protected JMipssDatePicker date;
	private boolean enabled;
	private String format;
	@SuppressWarnings("unchecked")
	private Binding notBeforeBinding;
	private Date notBeforeDate;
	private JLabel textLabel;
	protected JSpinner time;

	private boolean nullDate;
	private boolean snapEndOfDay;
	private boolean snapStartOfDay;
	/**
	 * Oppretter en instans med tidsangivelsen for opprettelsen.
	 */
	public JDateTimePicker() {
		this(Clock.now());
	}
	/**
	 * Oppretter en instans med tidsangivelsen for opprettelsen.
	 */
	public JDateTimePicker(boolean snapEndOfDay, boolean snapStartOfDay) {
//		this(Clock.now());
		this.snapEndOfDay=snapEndOfDay;
		this.snapStartOfDay=snapStartOfDay;
		initPicker(null, null);
		initSpinner(null, null);
		initGui();
	}
	/**
	 * Lager en default JDateTimePicker med default formatering og uten label
	 * 
	 * @param pickerDate
	 */
	public JDateTimePicker(Date pickerDate) {
		currentDate = pickerDate;

		Calendar c = Calendar.getInstance();
		if (pickerDate!=null){
			c.setTime(pickerDate);
		}else{
			c=null;
		}
		initPicker(c, null);
		initSpinner(c, null);
		initGui();
	}

	/**
	 * Lager en JDateTimePicker med gitt dato og format
	 * 
	 * @param pickerDate
	 * @param format
	 *            kan være null.
	 */
	public JDateTimePicker(Date pickerDate, String format) {
		currentDate = pickerDate == null ? Clock.now() : pickerDate;
		this.format = format;

		Calendar c = Calendar.getInstance();
		c.setTime(pickerDate);
		initPicker(c, format);
		initSpinner(c, null);
		initGui();
	}

	/**
	 * Lager en JDateTimePicker med en textlabel
	 * 
	 * @param pickerDate
	 * @param text
	 */
	public JDateTimePicker(Date pickerDate, String format, String text) {
		this.format = format;
		currentDate = pickerDate;
		textLabel = new JLabel(text);

		Calendar c = Calendar.getInstance();
		c.setTime(pickerDate);
		initPicker(c, format);
		initSpinner(c, null);
		initGui();
	}

	/**
	 * Sørger for at dato ikke kan settes til før notBeforeDate
	 * 
	 */
	private void enforceNotBeforeDate() {
		super.addPropertyChangeListener("date", new NotBeforeListener());
	}

	/** {@inheritDoc} */
	@Override
	protected void finalize() throws Throwable {
		logger.debug("finalize()");

		Throwable err = null;

		try {
			super.finalize();
		} catch (Throwable t) {
			err = t;
		}

		if (notBeforeBinding != null) {
			try {
				if (notBeforeBinding.isBound()) {
					notBeforeBinding.unbind();
				}
				notBeforeBinding = null;
			} catch (Throwable t) {
				t = null;
			}
		}

		if (err != null) {
			throw err;
		}
	}

	private void fireChangeEvent(Date old) {
		logger.trace("sender event for datoendring. Old:" + old + ". new: " + currentDate);
		super.firePropertyChange("date", old, currentDate);
	}

	/**
	 * Returnerer datoen som er satt i både datepicker og jspinner
	 * 
	 * @return tidsangivelsen eller NULL hvis slått av.
	 */
	public Date getDate() {
		if (date.getDate() != null) {
			if (!super.isEnabled()) {
				return null;
			}
			Calendar dayCal = Calendar.getInstance();
			dayCal.setTime(date.getDate());
			Calendar hrCal = Calendar.getInstance();
			hrCal.setTime((Date) time.getValue());

			dayCal.set(Calendar.HOUR_OF_DAY, hrCal.get(Calendar.HOUR_OF_DAY));
			dayCal.set(Calendar.MINUTE, hrCal.get(Calendar.MINUTE));
			dayCal.set(Calendar.SECOND, hrCal.get(Calendar.SECOND));

			return dayCal.getTime();
		} else {
			return null;
		}
	}

	/**
	 * Returnerer en JXDatePicker
	 * 
	 * @return
	 */
	public JXDatePicker getDatePicker() {
		return date;
	}

	/**
	 * @return the notBeforeDate
	 */
	public Date getNotBeforeDate() {
		return notBeforeDate;
	}

	/**
	 * Returnerer en JSpinner
	 * 
	 * @return
	 */
	public JSpinner getTimeSpinner() {
		return time;
	}

	/**
	 * Legger gui komponentene på panelet
	 */
	protected void initGui() {
//		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
//		int height = 22;
//		int width = 158;
//
//		if (textLabel != null) {
//			width = 230;
//			add(textLabel);
//		}
//		add(date);
//		add(time);
//		setDateTimePickerSize(new Dimension(width, height));
//		Dimension is changed from 50 to 55 to fit better with
		time.setPreferredSize(new Dimension(55, 22));
		time.setMinimumSize(new Dimension(55, 22));
		
		setLayout(new GridBagLayout());
		int idx = 0;
		if (textLabel!=null){
			add(textLabel, new GridBagConstraints(idx++, 0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
		}
		add(date, new GridBagConstraints(idx++, 0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
		add(time, new GridBagConstraints(idx++, 0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
		enforceNotBeforeDate();
	}

	/**
	 * Initialiserer JXDatePicker Setter formatet på dato og datoen
	 * 
	 * @param calendar
	 */
	private void initPicker(Calendar calendar, String dateFormat) {
		if(date != null) {
			date.removePropertyChangeListener("date", this);
		}
		
		if (calendar != null) {
			date = new JMipssDatePicker(calendar.getTime());
		} else {
			date = new JMipssDatePicker();
			date.setDate(null);
		}
		date.addPropertyChangeListener("date", this);
		setPickerFormat(dateFormat);
		Dimension pickerSize = new Dimension(85, 22);
		date.setPreferredSize(pickerSize);
		date.setMaximumSize(pickerSize);
		date.setMinimumSize(pickerSize);

		date.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date old = currentDate;
				updateCurrentDate();
				fireChangeEvent(old);
			}
		});
		date.setEnabled(super.isEnabled());
	}

	/**
	 * Initialiserer JSpinner setter formatet til spinneren og setter datoen
	 * 
	 * @param c
	 * @param timeFormat
	 */
	private void initSpinner(Calendar c, String timeFormat) {
		Dimension spinnerSize = new Dimension(70, 22);

		if (c != null) {
			time = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
		} else {
			time = new JSpinner(new SpinnerDateModel(Clock.now(), null, null, Calendar.MINUTE));
		}
//		time.setPreferredSize(spinnerSize);
//		time.setMinimumSize(spinnerSize);
//		time.setMaximumSize(spinnerSize);

		if (c != null) {
			setSpinnerEditor(timeFormat);
			time.setEnabled(super.isEnabled());
		} else {
			setSpinnerEditor("");
			time.setEnabled(false);
		}

		time.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				Date old = currentDate;
				updateCurrentDate();
				fireChangeEvent(old);
			}

		});
		// TODO legg til en lytter som bytter dato på pickeren hvis
		// tiden ruller over fra 23 til 00 eller fra 00 til 23
	}

	/**
	 * Henter den av/på verdien som ble satt.
	 */
	@Override
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Setter ny tidsangivelse og reainitialiserer hele komponenten. Bruker
	 * formatet, hvis angitt fra før.
	 * 
	 * @param d
	 */
	public void setDate(Date d) {
		Date old = currentDate;
		if (old==null)
			nullDate = true;
		// husk om komponenten er slått på eller av
		boolean enabled = super.isEnabled();

		if (d != null) {
//			Calendar c = Calendar.getInstance();
//			c.setTime(d);
			super.removeAll();
//			initPicker(c, format);
//			initSpinner(c, format);
			date.setDate(d);
			time.setValue(d);
			initGui();
			
			updateCurrentDate();
			fireChangeEvent(old);

			// gjeninnsett av/på tilstanden
			setEnabled(enabled);
		} else {
			super.removeAll();
			initPicker(null, format);
			initSpinner(null, format);
			initGui();

			updateCurrentDate();
			fireChangeEvent(old);

			// gjeninnsett av/på tilstanden
			setEnabled(enabled);
		}
	}

	/**
	 * Setter størrelsen på datetime pickeren
	 * 
	 * @param size
	 */
	protected void setDateTimePickerSize(Dimension size) {
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
	}

	public void setEditable(boolean editable) {
		boolean old = date.isEditable();
		date.setEditable(editable);
		time.setEnabled(editable);
		super.firePropertyChange("editable", old, editable);
	}

	/**
	 * Sender videre den av/på verdien til dato- og tidsvisningen. Slår av
	 * visningen av dato og tid.
	 */
	@Override
	public void setEnabled(boolean enabled) {
		boolean old = this.enabled;
		this.enabled = enabled;

		if (enabled) {
			if (date.getDate() != null) {
				setPickerFormat(format);
				setSpinnerEditor(null);
			} else {
				setPickerFormat("");
				setSpinnerEditor("");
			}
		} else {
			setPickerFormat("");
			setSpinnerEditor("");
		}
		date.setEnabled(enabled);
		time.setEnabled(enabled);
		super.firePropertyChange("enabled", old, enabled);
		repaint();
	}

	/**
	 * @param notBeforeDate
	 *            the notBeforeDate to set
	 */
	public void setNotBeforeDate(Date notBeforeDate) {
		logger.debug("setNotBeforeDate(" + notBeforeDate + ") start");
		Date old = this.notBeforeDate;
		this.notBeforeDate = notBeforeDate;
		super.firePropertyChange("notBeforeDate", old, notBeforeDate);
	}

	/**
	 * Setter en kilde som datovelgeren skal lytte på, som angir tidligst mulige
	 * dato
	 * 
	 * @param source
	 * @param field
	 */
	public void setNotBeforeSource(Object source, String field) {
		logger.debug("setNotBeforeSource(" + source + "," + field + ")");
		notBeforeBinding = BindingHelper.createbinding(source, field, this, "notBeforeDate");
		notBeforeBinding.bind();
	}

	/**
	 * Setter picker formatet. Tåler null.
	 * 
	 * @param dateFormat
	 */
	private void setPickerFormat(String dateFormat) {
		if (dateFormat == null)
			dateFormat = MipssDateFormatter.SHORT_DATE_FORMAT;

		date.setFormats(new SimpleDateFormat(dateFormat));
	}

	/**
	 * setter sppinners editor med det leverte formatet.
	 * 
	 * @param format
	 */
	private void setSpinnerEditor(String timeFormat) {
		if (timeFormat == null)
			timeFormat = MipssDateFormatter.SHORT_TIME_FORMAT;

		time.setEditor(new JSpinner.DateEditor(time, timeFormat));
	}

	private void updateCurrentDate() {
		if (date.getDate() != null) {
			Calendar dayCal = Calendar.getInstance();
			dayCal.setTime(date.getDate());
			Calendar hrCal = Calendar.getInstance();
			hrCal.setTime((Date) time.getValue());
			
			dayCal.set(Calendar.HOUR_OF_DAY, hrCal.get(Calendar.HOUR_OF_DAY));
			dayCal.set(Calendar.MINUTE, hrCal.get(Calendar.MINUTE));
			dayCal.set(Calendar.SECOND, hrCal.get(Calendar.SECOND));

			currentDate = dayCal.getTime();
		} else {
			currentDate = null;
		}
	}

	/**
	 * Lytter på dato for å sjekke at den ikke settes til "før" datoen
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	private class NotBeforeListener implements PropertyChangeListener {

		/** {@inheritDoc} */
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			String property = evt.getPropertyName();
			if (StringUtils.equals("date", property)) {
				Date old = (Date) evt.getOldValue();
				Date now = (Date) evt.getNewValue();
				if (notBeforeDate != null && now != null && !now.equals(old)) {
					if (now.before(notBeforeDate)) {
						setDate(old);
					}
				}
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(StringUtils.equals("date", evt.getPropertyName()) && evt.getSource() == date) {
			
			Calendar dayCal = Calendar.getInstance();
			dayCal.setTime(date.getDate());
			Calendar hrCal = Calendar.getInstance();
			hrCal.setTime((Date) time.getValue());
			
			if (snapEndOfDay&&nullDate){
				dayCal.set(Calendar.HOUR_OF_DAY, 23);
				dayCal.set(Calendar.MINUTE, 59);
				dayCal.set(Calendar.SECOND, 59);
			}
			else if (snapStartOfDay&&nullDate){
				dayCal.set(Calendar.HOUR_OF_DAY, 0);
				dayCal.set(Calendar.MINUTE, 0);
				dayCal.set(Calendar.SECOND, 0);
			}
			else{
				dayCal.set(Calendar.HOUR_OF_DAY, hrCal.get(Calendar.HOUR_OF_DAY));
				dayCal.set(Calendar.MINUTE, hrCal.get(Calendar.MINUTE));
				dayCal.set(Calendar.SECOND, hrCal.get(Calendar.SECOND));
			}
			setDate(dayCal.getTime());
			nullDate = false;
		}
	}
}
