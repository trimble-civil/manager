/**
 * 
 */
package no.mesta.mipss.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.organisasjon.SelskapKjoretoy;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for valg av MESTA selskaper et kjøretøy skal knyttes til.
 * Denne klasen er under generaliserng ut fra klassen no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoySelskapViewPanel
 * fra plugin-kjoretoy.
 * 
 * Den er ikke ferdig.
 * 
 * @author jorge
 *
 */
public class KjoretoySelskapSelectionPanel extends JPanel {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<Selskap> selskaper = null;
    private MipssKjoretoy bean = null;
    private String title = null;
    private Kjoretoy kjoretoy = null;
    
    private CheckBoxListRenderer renderer = null;
    
    private JLabel lblTitle;
    private JPanel pnlMain;
    private JScrollPane scrlPaneSelskap;
    private JList selskapList;
    private boolean begrensetTilgangEndring;
    
    /**
     * Oppretter panelet med de leverte parametrene.
     * @param kjoretoy
     * @param title
     * @param bean
     */
    public KjoretoySelskapSelectionPanel(Kjoretoy kjoretoy, String title, MipssKjoretoy bean, boolean begrensetTilgangEndring) {
        super();
        this.kjoretoy = kjoretoy;
        this.bean = bean;
        this.title = title;
        this.begrensetTilgangEndring = begrensetTilgangEndring;
    }
    
    /**
     * Viser panelet. Hvis denne metoden ikke kalles, kommer dette panelet ikke opp.
     */
    public void showPanel() {
    	try {
            initComponentsPresentation();
            loadFromEntity();
        }
        catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
    
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	public void setBoldTitle(boolean bold) {
        lblTitle.setFont(new Font(lblTitle.getFont().getFamily(), Font.BOLD, lblTitle.getFont().getSize()));
	}

    private void initListeners() {
        // sett opp listener for å betjene brukerens valg
    	
    	selskapList.getSelectionModel().addListSelectionListener(new  ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent e) {
				ListSelectionModel lsm = (ListSelectionModel)e.getSource();

		        if (e.getValueIsAdjusting() || lsm.isSelectionEmpty()) {
		            logger.trace("Ignorerer hendelse");
		            return;
		        }

		        Selskap selected = selskaper.get(lsm.getMinSelectionIndex());
	             if(isAlreadySelected(selected)) {
	                 removeSelskapFromRelations(selected);
	             } else {
	                 addSelskapToRelations(selected);
	             }
	             redrawSelection();
	             
	             selskapList.getSelectionModel().clearSelection();
	             selskapList.repaint();
			}
			
			private boolean isAlreadySelected(Selskap s) {
				/*
	            List<SelskapKjoretoy> list = kjoretoy.getSelskapKjoretoyList();
	             if (list.size() > 0) {
	                 for (SelskapKjoretoy sk : list) {
	                     if (sk.getSelskapId() == s.getId()) {
	                         return true;
	                     }
	                 }
	             }
	             return false;
	             */
				List<Selskap> list = kjoretoy.getSelskapList();
				if (list.size() > 0) {
					for (Selskap se : list) {
	                     if (se.getSelskapskode().equals(s.getSelskapskode())) {
	                         return true;
	                     }
	                 }
				}
				return false;
	         }

    	});
    }
    
    private void loadFromEntity() {
        // henter selskapene kun én gang
        if (selskaper == null) {
            try {
                selskaper = bean.getSelskapeList();
                logger.debug("Antall selskaper: " + selskaper.size());
            }
            catch (Exception e) {
                logger.error("kan ikke hente selkasper: " + e);
                selskaper = new ArrayList<Selskap>();
                throw new IllegalStateException("kan ikke hente selkasper", e);
            }
            // init listen for å vise selskaper og merke de valgte
            selskapList.setListData(selskaper.toArray());
            initListeners();
        }
        //renderer = new CheckBoxListRenderer(kjoretoy.getSelskapKjoretoyList());
        renderer = new CheckBoxListRenderer(kjoretoy.getSelskapList());
        selskapList.setCellRenderer(renderer);

        selskapList.setFixedCellHeight(16);
        scrlPaneSelskap.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        if(begrensetTilgangEndring) {
        	selskapList.setEnabled(false);
        	selskapList.setBackground(lblTitle.getBackground());
        }
    }
    
    protected void redrawSelection() {
        //renderer.setItemsToMatch(kjoretoy.getSelskapKjoretoyList());
    	renderer.setItemsToMatch(kjoretoy.getSelskapList());
        selskapList.repaint();
    }
    
    /**
     * Legger til et SelskapKjoretoy objekt til kjøretøyet.
     * @param selskap er Selskapet man skal koble kjøretøyet til.
     */
    private void addSelskapToRelations(Selskap selskap) {
    	kjoretoy.addSelskap(selskap);
    }
    
    /**
     * Fjerner et SelskapKjoretoy objekt fra kjøretøyet.
     * @param selskap er Selskapet his koblig skal fjwernesd far kjøretøyet.
     */
    private void removeSelskapFromRelations(Selskap selskap) {
    	kjoretoy.removeSelskap(selskap);
    }
    
    /**
     * Oppretter et nytt SelskapKjoretoy objekt ut fra det leverte selskapet og
     * kjøretøyet som vises.
     * @param selskap
     * @return nytt SelskapKjoretoy.
     */
    private SelskapKjoretoy newSelskapKjoretoyFrom(Selskap selskap) {
        SelskapKjoretoy sk = new SelskapKjoretoy();
        sk.setKjoretoyId(kjoretoy.getId());
        sk.setSelskapskode(selskap.getSelskapskode());
        return sk;
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        
        
    }

    /**
     * Viser hver linje i listen som en Checkbox.
     * @author jorge
     */
    private class CheckBoxListRenderer  implements ListCellRenderer {
        private List<Selskap> itemsToMatch = null;
        
        public CheckBoxListRenderer(List<Selskap> itemsToMatch) {
            setItemsToMatch(itemsToMatch);
        }

        public Component getListCellRendererComponent(JList list, Object value, 
                                                      int index, 
                                                      boolean isSelected, 
                                                      boolean cellHasFocus) {
            // setter teksten i checkboxen
            Selskap selskap = (Selskap)value;
            JCheckBox renderer = new JCheckBox();
            renderer.setText(selskap.getTextForGUI());

            // er denne checkboxen valgt?
            if (itemsToMatch != null) {
                for (int i = 0; i < itemsToMatch.size(); i++) {
                    Selskap s = itemsToMatch.get(i);
                    if (selskap.getSelskapskode().equals(s.getSelskapskode())) {
                        renderer.setSelected(! renderer.isSelected());
                        break;
                    }
                }
            }
            // konfigurerer
            renderer.setOpaque(false);
            renderer.setEnabled(!begrensetTilgangEndring);
            return renderer;
        }

        /**
         * Oppdaterer listen som brukes til å slå CheckBox'ene av/på.
         * @return
         */
        public void setItemsToMatch(List<Selskap> itemsToMatch) {
            this.itemsToMatch = itemsToMatch;
        }
    }

    private void initComponents() {

        pnlMain = new JPanel();
        lblTitle = new JLabel();
        scrlPaneSelskap = new JScrollPane();
        selskapList = new JList();
        lblTitle.setText(title); // NOI18N
        scrlPaneSelskap.setViewportView(selskapList);
        
        scrlPaneSelskap.setMinimumSize(new Dimension(200, 100));
        scrlPaneSelskap.setPreferredSize(new Dimension(200, 100));

        this.setLayout(new GridBagLayout());
        this.add(lblTitle, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        this.add(scrlPaneSelskap, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }
}
