package no.mesta.mipss.ui;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

/**
 * Henter ut visningsnavnet fra en entitet for AutoCompleteDecorator
 * 
 * @see org.jdesktop.swingx.autocomplete.AutoCompleteDecorator
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class EntityToStringConverter<T extends IRenderableMipssEntity> extends ObjectToStringConverter {
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * @see org.jdesktop.swingx.autocomplete.ObjectToStringConverter#getPreferredStringForItem(Object)
	 * @param object
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getPreferredStringForItem(Object object) {
		if (object == null) {
			return null;
		} else if (!(object instanceof IRenderableMipssEntity)) {
			return null;
		}

		T entity = (T) object;
		log.debug("getPreferredStringForItem(object), object: " + entity);
		return entity.getTextForGUI();
	}
}
