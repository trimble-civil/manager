package no.mesta.mipss.ui.table;

import no.mesta.mipss.mipssmapserver.KjoretoyDTO;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

/**
 * For å vise frem to forskjellige objekter i samme tabell benyttes denne klassen som 
 * samlingsobjekt for de to objekttypene 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class KjoretoyTableObject implements CheckBoxTableObject{

	private KjoretoyDTO kjoretoy;
	private Dfuindivid pda;
	private boolean valgt;
	private String pdaNavn="";
	/**
	 * Lager en instans av KjoretoyTableObject med et kjoretoy
	 * @param kjoretoy 
	 */
	public KjoretoyTableObject(KjoretoyDTO kjoretoy){
		this.kjoretoy = kjoretoy;
		
	}
	
	/**
	 * Lager en instans av KjoretorTableObject med en pda
	 * @param pda
	 */
	public KjoretoyTableObject(Dfuindivid pda){
		this.pda = pda;
	}
	
	public String getEnhetId(){
		String enhet = "";
		if (pda!=null)
			enhet = ""+pda.getId();
		else
			enhet = kjoretoy.getNavn();
		if (enhet==null)
			enhet="";
		return enhet;
	}
	
	public String getBeskrivelse(){
		String beskrivelse = "";
		if (pda!=null){
			beskrivelse = getPdaNavn();
		} else{
			beskrivelse = kjoretoy.getGuiTekst();
		}
		
		if (beskrivelse == null){
			beskrivelse = "";
		}
		return beskrivelse;
	}
	
	public String getType(){
		String type = "";
		if (pda!=null)
			type = pda.getDfumodell().getDfukategori().getNavn();
		else{
			type = kjoretoy.getType();
		}
		if (type==null)
			type = "";
		return type;
	}
	
	public String getModell(){
		String modell = "";
		if (pda!=null){
			if (pda.getDfumodell()!=null)
				modell = pda.getDfumodell().getNavn();
		}
		else{
			modell = kjoretoy.getMerke();
		}
		if (modell==null)
			modell="";
		return modell;
	}
	
	public String getLeverandor(){
		String leverandor = "";
		if (pda!=null)
			leverandor = "";
		else{
			leverandor = kjoretoy.getLeverandorNavn();
		}
		if (leverandor == null)
			leverandor = "";
		return leverandor;
	}
	
	public String getRegnr(){
		String regnr = "";
		if (pda!=null)
			regnr = "";
		else
			regnr = kjoretoy.getRegnr();
		if (regnr==null)
			regnr = "";
		return regnr;
			
	}
	public Boolean getValgt(){
		return valgt;
	}
	public void setValgt(Boolean valgt){
		this.valgt = valgt;
	}
	
	@Override
	public String getTextForGUI() {
		return "";
	}
	
	public KjoretoyDTO getKjoretoy(){
		return kjoretoy;
	}
	public Dfuindivid getPda(){
		return pda;
	}

	public void setPdaNavn(String pdaNavn) {
		this.pdaNavn = pdaNavn;
	}

	public String getPdaNavn() {
		return pdaNavn;
	}

}
