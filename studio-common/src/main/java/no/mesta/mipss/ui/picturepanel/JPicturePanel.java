package no.mesta.mipss.ui.picturepanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.resources.buttons.ButtonToolkit;
import no.mesta.mipss.resources.images.MissingIcon;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXImageView;


/**
 * Panel for aa vise et bilde, og navigasjonsknapper til oeverige
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JPicturePanel extends JPanel implements MipssBeanLoaderListener {
    private static final Logger logger = LoggerFactory.getLogger(JPicturePanel.class);
    private JButton[] buttons;
    private JXBusyLabel busyLabel = new JXBusyLabel();
    private JPanel busyPanel;
    private JXImageView picturePanel;
    private MipssPictureModel model;
    private Image activeImage;
    private Image MISSING_IMAGE = (new MissingIcon(180,180)).getImage();
    private int width = 180;
    private int height = 180;
    private int activeImageIndex = -1;
    private JButton goFirstButton, goPreviousButton, 
        goNextButton, goLastButton;
    private JFrame owner;
    private Dimension dimension;
    
    /**
     * Konstruktoer
     * 
     * @param model
     * @param owner
     * @param title hvis null, ikke vis
     */
    public JPicturePanel(JFrame owner, MipssPictureModel model, String title) {
        this.model = model;
        this.owner = owner;
        
        model.addMipssBeanLoaderListener(this);
        
        if(title != null) {
            setBorder(BorderResources.createComponentTitleBorder("Bilder"));
        } else {
            setBorder(BorderResources.createComponentBorder());
        }
        setDimension(new Dimension(width + 30, height + 90));
        setMaximumSize(getDimension());
        setMinimumSize(getDimension());
        setPreferredSize(getDimension());
        goFirstButton = ButtonToolkit.createGoFirstButton();
        goFirstButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!JPicturePanel.this.model.isLoadingData() && activeImageIndex > 0) {
                    goFirst();
                    updatePicture();
                }
            }
        });   
        goPreviousButton = ButtonToolkit.createGoBackButton();
        goPreviousButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!JPicturePanel.this.model.isLoadingData() && activeImageIndex > 0) {
                    goDown();
                    updatePicture();
                }
            }
        });
        goNextButton = ButtonToolkit.createGoForwardButton();
        goNextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!JPicturePanel.this.model.isLoadingData() && activeImageIndex < JPicturePanel.this.model.size()-1) {
                    goUp();
                    updatePicture();
                }
            }
        });
        goLastButton = ButtonToolkit.createGoLastButton();
        goLastButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!JPicturePanel.this.model.isLoadingData() && activeImageIndex < JPicturePanel.this.model.size()-1) {
                    goLast();
                    updatePicture();
                }
            }
        });
        
        buttons = new JButton[]{goFirstButton, goPreviousButton, goNextButton, goLastButton};
        for(JButton b:buttons) {
            b.setBorder(null);
            b.setFocusable(false);
        }
        
        
        JPanel buttonPanel = createButtonPanel();
        picturePanel = createPicturePanel();
        busyPanel = createBusyPanel();

        setEnable(!model.isLoadingData());
        
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        // Slå på automatisk avstand mellom komponenter
        layout.setAutoCreateGaps(true);
        
        // Slå på automatisk avstand mellom komponenter og kanten, for komponenter
        // som rører kanten av kontaineren
        layout.setAutoCreateContainerGaps(true);
        
        // Lag sekvensiell gruppe for den horisontale aksen
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        
        // Den sekvensielle gruppen inneholder en parallell gruppe som holder
        // komponentene
        //
        // Innrykk benyttes for å forsterke grupperingen
        hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                 addComponent(busyPanel).addComponent(picturePanel).addComponent(buttonPanel));
        layout.setHorizontalGroup(hGroup);
        
        // Lag sekvensiell gruppe for den vertikale aksen
        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        
        // Den sekvensielle gruppen inneholder tre parallelle grupper som ordner
        // innholdet langs grunnlinjen.
         vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).
             addComponent(busyPanel));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).
            addComponent(picturePanel));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).
            addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, 40 , GroupLayout.PREFERRED_SIZE));
        layout.setVerticalGroup(vGroup);
    }
    
    /**
     * Lager busyPanelet som vises naar bilder lastes
     * 
     * @return
     */
    private JPanel createBusyPanel()  {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(busyLabel, BorderLayout.CENTER);
        panel.setVisible(true);
        busyLabel.setVisible(true);
        busyLabel.setBusy(true);
        return panel;
    }
    
    /**
     * Slaar av/paa GUI
     * 
     * @param f
     */
    private void setEnable(boolean f) {
        picturePanel.setVisible(f);
        busyLabel.setVisible(!f);
        busyLabel.setBusy(!f);
        busyPanel.setVisible(!f);
        if(!f || model.size() == 0) {
            for(JButton b:buttons) {
                b.setEnabled(false);
            }
        } else {
            resetButtons();
        }
    }
    
    /**
     * Lager panelet som viser det aktive bildet
     * 
     * @return
     */
    private JXImageView createPicturePanel() {
        JXImageView panel = new JXImageView();
        activeImage = MISSING_IMAGE;
        panel.setImage(activeImage);
        panel.setDragEnabled(false);
        panel.setEditable(false);
        panel.setMaximumSize(new Dimension(width, height));
        panel.setPreferredSize(new Dimension(width, height));
        panel.setMinimumSize(new Dimension(width, height));
        panel.setOpaque(true);
        panel.setBackground(this.getBackground());
        panel.addMouseListener(new PictureListenerer());
        
        return panel;
    }
    
    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        GroupLayout layout = new GroupLayout(buttonPanel);
        buttonPanel.setLayout(layout);
        // Slaa paa automatisk avstand mellom komponenter
        layout.setAutoCreateGaps(true);
        
        // Slaa paa automatisk avstand mellom komponenter og kanten, for komponenter
        // som roerer kanten av kontaineren
        layout.setAutoCreateContainerGaps(true);
        
        // Lag sekvensiell gruppe for den horisontale aksen
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        // Lag sekvensiell gruppe for den vertikale aksen
        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        // Lag paralellgruppe for den vertikale presentasjonen
        GroupLayout.ParallelGroup vpg = layout.createParallelGroup(GroupLayout.Alignment.BASELINE);

        for(JButton b:buttons) {
            hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(b));
            vpg.addComponent(b);
        }
        vGroup.addGroup(vpg);
        layout.setHorizontalGroup(hGroup);
        layout.setVerticalGroup(vGroup);
        
        return buttonPanel;
    }
    
    /**
     * Navigerer bilde index et steg ned
     * 
     */
    private void goDown() {
        if(!model.isLoadingData() && activeImageIndex > 0) {
            activeImageIndex--;
            resetButtons();
        }
    }
    
    private void resetButtons() {
        if(activeImageIndex == 0) {
            goFirstButton.setEnabled(false);
            goPreviousButton.setEnabled(false);            
            goLastButton.setEnabled(true);
            goNextButton.setEnabled(true);
        } else if(activeImageIndex+1 == model.size()) {
            goFirstButton.setEnabled(true);
            goPreviousButton.setEnabled(true);
            goLastButton.setEnabled(false);
            goNextButton.setEnabled(false);            
        } else if (activeImageIndex == -1 || model.size() == 0 || model.size() == 1) {
            goFirstButton.setEnabled(false);
            goPreviousButton.setEnabled(false);
            goLastButton.setEnabled(false);
            goNextButton.setEnabled(false);            
        } else {
            goFirstButton.setEnabled(true);
            goPreviousButton.setEnabled(true);
            goLastButton.setEnabled(true);
            goNextButton.setEnabled(true);            
        }
    }

    /**
     * Navigerer bildeindex et steg opp
     * 
     */
    private void goUp() {
        if(!model.isLoadingData() && activeImageIndex+1 < model.size()) {
            activeImageIndex++;
            resetButtons();
        }
    }

    /**
     * Stiller bildeindex til foerste bilde
     * 
     */
    private void goFirst() {
        if(!model.isLoadingData() && model.size() > 0) {
            activeImageIndex = 0;
            resetButtons();
        }
    }
    
    /**
     * Stiller bildeindex til siste bilde
     */
    private void goLast() {
        if(!model.isLoadingData() && model.size() > 0) {
            activeImageIndex = model.size() -1;
            resetButtons();
        }
    }
    
    private void updatePicture() {
        if(!model.isLoadingData() && model.size() > 0) {
            Bilde bilde = model.getBilde(activeImageIndex);
            ImageIcon image = new ImageIcon(bilde.getBildeLob());
            activeImage = image.getImage();
        } else if(!model.isLoadingData() && model.size() == 0) {
            activeImage = MISSING_IMAGE;
            activeImageIndex = -1;
        }
        picturePanel.setImage(activeImage);
        picturePanel.setScale(calculateScale());
    }
    
    private double calculateScale() {
        double scale = 1;
        
        int imageWidth = activeImage.getWidth(null);
        int imageHeight = activeImage.getHeight(null);
        if(imageWidth > imageHeight) {
            scale = (double)width/ (double)imageWidth;
        } else {
            scale = (double)height/(double)imageHeight;
        }
        
        return scale;
    }

    /** {@inheritDoc} */
    public void loadingStarted(MipssBeanLoaderEvent event) {
        setEnable(false);
    }

    /** {@inheritDoc} */
    public void loadingFinished(MipssBeanLoaderEvent event) {
        activeImageIndex = 0;
        updatePicture();
        setEnable(true);
    }

    public void setWidth(int width) {
        this.width = width;
        if(picturePanel != null) {
            picturePanel.setMaximumSize(new Dimension(width, height));
            picturePanel.setPreferredSize(new Dimension(width, height));
            picturePanel.setMinimumSize(new Dimension(width, height));
        }
    }

    public void setHeight(int height) {
        this.height = height;
        if(picturePanel != null) {
            picturePanel.setMaximumSize(new Dimension(width, height));
            picturePanel.setPreferredSize(new Dimension(width, height));
            picturePanel.setMinimumSize(new Dimension(width, height));
        }
    }

    /** {@inheritDoc} */
    public int getActiveImageIndex() {
        return activeImageIndex;
    }

    /** {@inheritDoc} */
    public Image getActiveImage() {
        return activeImage;
    }

    /** {@inheritDoc} */
    public MipssPictureModel getModel() {
        return model;
    }

    /** {@inheritDoc} */
    public void navigateToIndex(int index) {
        logger.debug("navigateToIndex(" + index + ")");
        if(!model.isLoadingData() && index+1 < model.size()) {
            activeImageIndex = index;
            updatePicture();
            resetButtons();
            repaint();
        }
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public Dimension getDimension() {
        return dimension;
    }

    class PictureListenerer implements MouseListener {

        public void mouseClicked(MouseEvent e) {
            if(!model.isLoadingData() && activeImageIndex >= 0 && model.size()>0) {
                if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    JPictureDialogue picture = new JPictureDialogue(JPicturePanel.this.owner, model);
                    picture.setVisible(true);
                }
            }
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }
    }
}
