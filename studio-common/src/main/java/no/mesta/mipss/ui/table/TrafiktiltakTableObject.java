package no.mesta.mipss.ui.table;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;

/**
 * Valgbar entitet i tabell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class TrafiktiltakTableObject implements CheckBoxTableObject, Comparable<TrafiktiltakTableObject>,
		PropertyChangeSource {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil
			.getPropertyBundle("mipssCommonText");
	private static final String GUI_NONE = BUNDLE.getGuiString("tiltakPanel.none");
	private transient PropertyChangeSupport props;
	private Trafikktiltak tiltak;
	private boolean valgt;

	public TrafiktiltakTableObject(Trafikktiltak tiltak) {
		this.tiltak = tiltak;
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public int compareTo(TrafiktiltakTableObject o) {
		if (o == null) {
			return 1;
		}

		if (tiltak == null) {
			return -1;
		}

		return tiltak.compareTo(o.tiltak);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof TrafiktiltakTableObject)) {
			return false;
		}

		TrafiktiltakTableObject other = (TrafiktiltakTableObject) o;

		return new EqualsBuilder().append(tiltak, other.tiltak).isEquals();
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		return tiltak == null ? GUI_NONE : tiltak.getTextForGUI();
	}

	/**
	 * @return the tiltak
	 */
	public Trafikktiltak getTiltak() {
		return tiltak;
	}

	/**
	 * @return the valgt
	 */
	public Boolean getValgt() {
		return valgt;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(tiltak).toHashCode();
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	/**
	 * @param tiltak
	 *            the tiltak to set
	 */
	public void setTiltak(Trafikktiltak tiltak) {
		Trafikktiltak old = this.tiltak;
		this.tiltak = tiltak;
		getProps().firePropertyChange("tiltak", old, tiltak);
	}

	/**
	 * @param valgt
	 *            the valgt to set
	 */
	public void setValgt(Boolean valgt) {
		Boolean old = this.valgt;
		this.valgt = valgt;
		getProps().firePropertyChange("valgt", old, valgt);
	}
}
