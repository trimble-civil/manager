package no.mesta.mipss.persistence.mipssfield;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

/**
 * Tester for funn
 * 
 * @see no.mesta.mipss.persistence.mipssfield.Avvik
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PunktregTest extends TestCase implements PropertyChangeListener {
	private static final String BILDE_PROPERTY =  "bilde";
	private static final Logger logger = LoggerFactory.getLogger(PunktregTest.class);
	private static final String STATUS_PROPERTY = "status";
	private static final String STATUSER_PROPERTY = "statuser";
	private static final String STATUSHISTORY_PROPERTY = "statusHistory";
	private List<String> expectedProperties;
	private List<String> firedProperties;
	private Avvik funn;
	
	
	
	/**
	 * Lager et status objekt
	 * 
	 * @return
	 */
	private AvvikStatus createStatus() {
		AvvikStatus status = new AvvikStatus();

		AvvikstatusType type = createStatusType();
		
		status.setAvvik(funn);
		status.setAvvikstatusType(type);
		return status;
	}
	
	/**
	 * Lager en liste av status objekt for tester
	 * 
	 * @return
	 */
	private List<AvvikStatus> createStatuser() {
		List<AvvikStatus> statuser = new ArrayList<AvvikStatus>();

		AvvikStatus status = createStatus();
		statuser.add(status);
		return statuser;
	}

	/**
	 * Lager et statusType objekt
	 * 
	 * @return
	 */
	private AvvikstatusType createStatusType() {
		AvvikstatusType type = new AvvikstatusType();
		type.setStatus("status");
		
		return type;
	}

	/** {@inheritDoc} */
	public void propertyChange(PropertyChangeEvent evt) {
		logger.debug("propertyChange(" + evt + ")");
		String property = evt.getPropertyName();

		if (property != null) {
			firedProperties.add(property);
		}
	}

	/**
	 * Nullstiller fyrte properties
	 * 
	 */
	private void resetFiredProperties() {
		firedProperties = new ArrayList<String>();
	}
	
	/** {@inheritDoc} */
	@Override
	public void setUp() throws Exception {
		super.setUp();

		funn = new Avvik();
		funn.setGuid("test");
		expectedProperties = new ArrayList<String>();
		firedProperties = new ArrayList<String>();
	}

	/** {@inheritDoc} */
	@Override
	public void tearDown() throws Exception {
		super.tearDown();

		funn = null;
		expectedProperties = null;
		firedProperties = null;
	}

	public void testAddStatus() {
		List<AvvikStatus> statuser = createStatuser();
		funn.setStatuser(statuser);
		
		AvvikStatus status = createStatus();
		AvvikstatusType type = createStatusType();
		type.setStatus("status2");
		status.setAvvikstatusType(type);
		resetFiredProperties();
		
		funn.addStatus(status);
		expectedProperties.add(STATUSER_PROPERTY);
		expectedProperties.add(STATUS_PROPERTY);
		expectedProperties.add(STATUSHISTORY_PROPERTY);
		expectedProperties.add(BILDE_PROPERTY);
	}
	
	/**
	 * Tester at setStatuser fyrer til forventede property lyttere
	 * 
	 */
	public void testStatusListener() {
		funn.addPropertyChangeListener(this);

		expectedProperties.add(STATUSER_PROPERTY);
		expectedProperties.add(STATUS_PROPERTY);
		expectedProperties.add(STATUSHISTORY_PROPERTY);
		List<AvvikStatus> statuser = createStatuser();
		funn.setStatuser(statuser);

		verifyFiredProperties();
	}

	/**
	 * Enkel hjelpemetode til å verifisere at lyttere har fyrt som forventet
	 * 
	 */
	private void verifyFiredProperties() {
		assertTrue("Fired properties not as expected", expectedProperties.equals(firedProperties));
	}
}
