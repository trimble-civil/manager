package no.mesta.mipss.persistence.kontrakt;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import org.junit.Before;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ArtikkelProdtypeTest extends TestCase {

    private ArtikkelProdtype articleProdtype;
    private AgrArtikkelV article;
    private Date articleStartDateToday;
    private Date articleStopDateToday;
    private Date articleStartDateBeforeToday;
    private Date articleStopDateAfterToday;
    private Date articleStopDateBeforeToday;

    @Before
    public void setUp() {
        articleProdtype = new ArtikkelProdtype();
        article = new AgrArtikkelV();
        articleProdtype.setAgrArtikkelV(article);

        Date now = new Date();

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(now);

        gc.set(Calendar.HOUR_OF_DAY, gc.get(Calendar.HOUR_OF_DAY) - 1);
        articleStartDateToday = gc.getTime();

        gc.set(Calendar.HOUR_OF_DAY, gc.get(Calendar.HOUR_OF_DAY) + 2);
        articleStopDateToday = gc.getTime();

        gc.setTime(now);

        gc.add(Calendar.DAY_OF_MONTH, -1);
        articleStartDateBeforeToday = gc.getTime();
        articleStopDateBeforeToday = gc.getTime();

        gc.add(Calendar.DAY_OF_MONTH, 2);
        articleStopDateAfterToday = gc.getTime();

        article.setFraDato(articleStartDateBeforeToday);
        article.setTilDato(articleStopDateAfterToday);
        article.setArtikkelStatus("N");
    }

    public void testIsUtgatt_validArticleDatesAndStatus_articleNotExpired() {
        boolean isExpired = articleProdtype.isUtgatt();

        assertFalse(isExpired);
    }

    public void testIsUtgatt_nullArticleDates_articleIsExpired() {
        article.setFraDato(null);
        article.setTilDato(null);

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_nullArticle_articleIsExpired() {
        articleProdtype.setAgrArtikkelV(null);

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_unusualArticleStatus_articleNotExpired() {
        article.setArtikkelStatus("n");

        boolean isExpired = articleProdtype.isUtgatt();

        assertFalse(isExpired);
    }

    public void testIsUtgatt_wrongArticleStatus_articleIsExpired() {
        article.setArtikkelStatus("C");

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_nullArticleFromDate_articleIsExpired() {
        article.setFraDato(null);

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_nullArticleToDate_articleIsExpired() {
        article.setTilDato(null);

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_articleDatesBeforeToday_articleIsExpired() {
        article.setFraDato(articleStartDateBeforeToday);
        article.setTilDato(articleStopDateBeforeToday);

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_nullArticleStatus_articleIsExpired() {
        article.setArtikkelStatus(null);

        boolean isExpired = articleProdtype.isUtgatt();

        assertTrue(isExpired);
    }

    public void testIsUtgatt_articleExpiresToday_articleNotExpired() {
        article.setFraDato(articleStartDateToday);
        article.setTilDato(articleStopDateToday);

        boolean isExpired = articleProdtype.isUtgatt();

        assertFalse(isExpired);
    }

}