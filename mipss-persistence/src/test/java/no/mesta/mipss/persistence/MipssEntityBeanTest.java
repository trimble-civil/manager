package no.mesta.mipss.persistence;

import java.lang.reflect.Method;
import java.util.List;

import javax.persistence.Column;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Avvik;

import org.apache.commons.lang.StringUtils;

/**
 * Tester MipssEntityBean
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssEntityBeanTest extends TestCase {

	public void testGetAnnotation() {
		Avvik funn = new Avvik();
		
		Object value = funn.getAnnotationValue("getBeskrivelse", Column.class, "length");
		Integer length = (Integer) value;
		assertTrue(length == 1000);
	}
	
	public void testIsSetter() {
		Method m = getTestMethod("setTest", new Class[]{Object.class});
		
		Avvik funn = new Avvik();
		boolean test = funn.isSetter(m);
		assertTrue(test);		
	}
	
	public void testIsGetter() {
		Method m = getTestMethod("getTest", null);

		Avvik funn = new Avvik();
		boolean test = funn.isGetter(m);
		assertTrue(test);		
	}
	
	public void testGetBeanName() {
		Method m = getTestMethod("setTest", new Class[]{Object.class});
		
		Avvik funn = new Avvik();
		String name = funn.getBeanName(m);
		boolean test = StringUtils.equals("test", name);
		assertTrue(test);
	}
	
	
	
	public void testGetColumnAnnotatedMethods() {
		Avvik funn = new Avvik();
		List<Method> methods = funn.getDBColumnMethods();
				
		assertTrue("Methods count", methods != null);// && methods.size() == 31);
	}
	
	public void testGetWritableBeanMethods() {
		Avvik funn = new Avvik();
		List<Method> methods = funn.getWritableBeanMethods();
		
		assertTrue("Methods count", methods != null);// && methods.size() == 31);
	}
	
	/**
	 * Brukes til å teste isSetter(Method) i MipssEntityBean
	 * 
	 * @see no.mesta.mipss.persistence.MipssEntityBeanTest#testIsSetter()
	 * @param o
	 */
	public void setTest(Object o) {
		o = null;
	}
	
	/**
	 * Brukes til å teste MipssEntityBean
	 * 
	 * @param o
	 * @return
	 */
	public Object getTest() {
		return null;		
	}

	/**
	 * Brukes til å hente ut metode som kan testes
	 * 
	 * @return
	 */
	private Method getTestMethod(String name, Class<?>[] params) {
		Method m = null;
		try {
			m = getClass().getMethod(name, params);
		} catch (SecurityException e) {
			fail("Security e");
		} catch (NoSuchMethodException e) {
			fail("No such method");
		}
		
		return m;
	}
}
