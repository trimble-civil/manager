package no.mesta.mipss.persistence;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

/**
 * Tester bilde util
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ImageUtilTest extends TestCase{
    public ImageUtilTest() {
    }
    
    public void testLoad() {
        Bilde b = ImageUtil.load("images/hovedlogo_sort.png");
        assertNotNull("Bilde is null", b);
    }
    
    public void testRotate() {
        Bilde b = ImageUtil.load("images/hovedlogo_sort.png");
        assertNotNull("Bilde is null", b);
        
//        BufferedImage bi = b.getImage();
//        assertNotNull("BufferedImage is null", bi);       
//        assertTrue("Width > 0", bi.getWidth() > 0);
//        assertNotNull("Raser is null", bi.getRaster());
//        
//        BufferedImage rotated = ImageUtil.rotate90(bi);
//        assertNotNull("Rotated image is null", rotated);        
    }
    
//    public static void main(String[] s) {
//        //Bilde b = ImageUtil.load("images/hovedlogo_sort.png");
//        Bilde b = ImageUtil.load("images/must_rotate.jpg");
//        BufferedImage bi = b.getImage();
//        BufferedImage rotated = ImageUtil.rotate90(bi);
//        
//        ImageIcon i = new ImageIcon(rotated);
//        JLabel l = new JLabel(i);
//        final JFrame f = new JFrame();
//        JPanel panel = new JPanel(new BorderLayout());
//        panel.add(l, BorderLayout.CENTER);
//        f.add(panel);
//        
//        SwingUtilities.invokeLater(new Runnable() {
//
//                    public void run() {
//                        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//                        f.pack();
//                        f.setLocationRelativeTo(null);;
//                        f.setVisible(true);                    }
//                });
//    }
}
