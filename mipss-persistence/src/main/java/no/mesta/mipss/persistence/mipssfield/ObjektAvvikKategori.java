package no.mesta.mipss.persistence.mipssfield;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Created by Skalstad2105 on 20.01.2016.
 */
@SuppressWarnings("serial")
@Entity(name = ObjektAvvikKategori.BEAN_NAME)
@SequenceGenerator(name = "objektavvikkategoriSeq", sequenceName = "OBJEKTAVVIKKATEGORI_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries({

        @NamedQuery(name = ObjektAvvikKategori.QUERY_FIND_ALL, query = "SELECT o FROM ObjektAvvikKategori o ORDER BY o.guiRekkefolge"),
        @NamedQuery(name = ObjektAvvikKategori.QUERY_FIND_VALID, query = "SELECT o FROM ObjektAvvikKategori o WHERE o.valgbarFlagg = 1 ORDER BY o.guiRekkefolge")})

@Table(name = "OBJEKTAVVIKKATEGORI")
public class ObjektAvvikKategori
        implements Serializable, IRenderableMipssEntity, Comparable<ObjektAvvikKategori> {
    public static final String BEAN_NAME = "ObjektAvvikKategori";
    public static final String QUERY_FIND_ALL = "ObjektAvvikKategori.findAll";
    public static final String QUERY_FIND_VALID = "ObjektAvvikKategori.findValid";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "objektavvikkategoriSeq")
    @Column(nullable = false)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "prosess_id", referencedColumnName = "ID", nullable = false)
    private Prosess prosess;
    @ManyToOne
    @JoinColumn(name = "kontrakt_id", referencedColumnName = "ID", nullable = false)
    private Driftkontrakt kontrakt;

    @Column(name="elrapp_id", nullable = false)
    private Long elrappId;
    private String navn;
    @Column(name = "OPPRETTET_AV")
    private String opprettetAv;
    @Column(name = "OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date opprettetDato;
    @Column(name = "ENDRET_AV")
    private String endretAv;
    @Column(name = "ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endretDato;
    @Column(name = "GUI_REKKEFOLGE")
    private Long guiRekkefolge;
    @Column(name = "VALGBAR_FLAGG")
    private Long valgbarFlagg;

    public ObjektAvvikKategori() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Prosess getProsess() {
        return prosess;
    }

    public Driftkontrakt getKontrakt() {
        return kontrakt;
    }

    public Long getElrappId() {
        return elrappId;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    public Long getGuiRekkefolge() {
        return guiRekkefolge;
    }

    public void setGuiRekkefolge(Long guiRekkefolge) {
        this.guiRekkefolge = guiRekkefolge;
    }

    public Long getValgbarFlagg() {
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Long valgbarFlagg) {
        this.valgbarFlagg = valgbarFlagg;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(7, 91).
                append(getId()).
                append(getNavn()).
                toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (null == o) {
            return false;
        }
        if (!(o instanceof ObjektAvvikKategori)) {
            return false;
        }
        ObjektAvvikKategori other = (ObjektAvvikKategori) o;
        return new EqualsBuilder().
                append(getId(), other.getId()).
                isEquals();
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    @Override
    public int compareTo(ObjektAvvikKategori o) {
        ObjektAvvikKategori other = (ObjektAvvikKategori) o;
        return new CompareToBuilder().append(getNavn(), other.getNavn()).toComparison();
    }

}
