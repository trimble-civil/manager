package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/***
 * Primary key klasse for <tt>Konfigparam</tt>.
 * 
 * @author Christian Wiik (Mesan)
 */
@SuppressWarnings("serial")
public class KonfigparamPK implements Serializable {
	private String applikasjon;
    private String navn;
	
	public KonfigparamPK() {};
	
	public KonfigparamPK(String applikasjon, String navn) {
		this.applikasjon = applikasjon;
		this.navn = navn;
	}
	
	public String getApplikasjon() {
		return applikasjon;
	}

	public String getNavn() {
		return navn;
	}
	
	public String toString() {
		return "Primary Key Class KonfigparamPK with values: applikasjon '" + applikasjon + "' and navn '" + navn + "'";
	}

	@Override
	public boolean equals(Object obj) {
		boolean equal = false;
		
		if(obj == null || obj.getClass() != this.getClass()) { equal = false; }
		else {
			if(obj == this) { equal = true; }
			else {
				KonfigparamPK other = (KonfigparamPK)obj;
				
				EqualsBuilder equalsBuilder = new EqualsBuilder();
				equalsBuilder.append(this.getApplikasjon(), other.getApplikasjon());
				equalsBuilder.append(this.getNavn(), other.getNavn());
				equal = equalsBuilder.isEquals(); 
			}
			
		}
		
		return equal;
	}
	
	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(73, 79);
		hashCodeBuilder.append(applikasjon);
		hashCodeBuilder.append(navn);
		return hashCodeBuilder.toHashCode();
	}
	
}