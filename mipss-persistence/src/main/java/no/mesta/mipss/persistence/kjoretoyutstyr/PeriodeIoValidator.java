/**
 * 
 */
package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.util.Date;

/**
 * Validerer periodeoverlappinger. Til bruk kun på klientsiden fordi
 * den ikke støtter sikker multithreading.
 * 
 * @author jorge
 *
 */
public class PeriodeIoValidator extends PeriodeIoMonitor {
	/**
	 * Ikke tillatt.
	 */
	@SuppressWarnings("unused")
	private PeriodeIoValidator() {}

	/**
	 * Oppretter en instans og lager lister av perioder ut fra portene som er i 
	 * bruk for hele kjøretøyet. brukt av klientapp for å slå av/på porters tilgjengelighet
	 * @param entity
	 */
	public PeriodeIoValidator(KjoretoyUtstyr entity) {
		if(entity == null || entity.getKjoretoy() == null || entity.getKjoretoy().getKjoretoyUtstyrList() == null) {
			return;
		}
	}

	/**
	 * Oppretter en instans og lager lister av perioder ut fra portene som er i 
	 * bruk for hele kjøretøyet bortsett fra den leverte ignoredIo.
	 * Brukes for å kontrollere tilgjegeligheten til portene etter at de er 
	 * lagt til kjøretøyet.
	 * @param entity
	 * @param ignoredIo
	 */
	public PeriodeIoValidator(KjoretoyUtstyr entity, KjoretoyUtstyrIo ignoredIo) {
		if(entity == null || entity.getKjoretoy() == null || entity.getKjoretoy().getKjoretoyUtstyrList() == null) {
			return;
		}
	}
	
	/**
	 * Kontrollerer om den angitte porten har en kollisjon innenfor det angitte tidsrommet.
	 * @param port
	 * @param from
	 * @param to
	 * @return True hvis ingen kollisjon.
	 */
	public boolean isOverlappingFree(Long port, Date from, Date to) {	
		return true;
	}
	
}
