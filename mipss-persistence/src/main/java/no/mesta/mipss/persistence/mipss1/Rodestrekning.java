package no.mesta.mipss.persistence.mipss1;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.kontrakt.Rode;

@SuppressWarnings("serial")
//@Entity
//@NamedQuery(name = "Rodestrekning.findAll", 
//    query = "select o from Rodestrekning o")
public class Rodestrekning implements Serializable {
//    private Timestamp datoOpprettet;
//    private Long fylkesnummer;
//    private Long hparsell;
//    private Long kmFra;
//    private Long kmTil;
//    private Long kommunenummer;
//    private Long linjeNr;
//    private String opprettetAv;
//    private Long rodestrekningId;
//    private Long slettet;
//    private String veifunksjon;
//    private String veikategori;
//    private Long veinummer;
//    private String veistatus;
//    private Rode rode;
//
//    public Rodestrekning() {
//    }
//
//    @Column(name="DATO_OPPRETTET", nullable = false)
//    public Timestamp getDatoOpprettet() {
//        return datoOpprettet;
//    }
//
//    public void setDatoOpprettet(Timestamp datoOpprettet) {
//        this.datoOpprettet = datoOpprettet;
//    }
//
//    public Long getFylkesnummer() {
//        return fylkesnummer;
//    }
//
//    public void setFylkesnummer(Long fylkesnummer) {
//        this.fylkesnummer = fylkesnummer;
//    }
//
//    @Column(nullable = false)
//    public Long getHparsell() {
//        return hparsell;
//    }
//
//    public void setHparsell(Long hparsell) {
//        this.hparsell = hparsell;
//    }
//
//    @Column(name="KM_FRA", nullable = false)
//    public Long getKmFra() {
//        return kmFra;
//    }
//
//    public void setKmFra(Long kmFra) {
//        this.kmFra = kmFra;
//    }
//
//    @Column(name="KM_TIL", nullable = false)
//    public Long getKmTil() {
//        return kmTil;
//    }
//
//    public void setKmTil(Long kmTil) {
//        this.kmTil = kmTil;
//    }
//
//    public Long getKommunenummer() {
//        return kommunenummer;
//    }
//
//    public void setKommunenummer(Long kommunenummer) {
//        this.kommunenummer = kommunenummer;
//    }
//
//    @Column(name="LINJE_NR", nullable = false)
//    public Long getLinjeNr() {
//        return linjeNr;
//    }
//
//    public void setLinjeNr(Long linjeNr) {
//        this.linjeNr = linjeNr;
//    }
//
//    @Column(name="OPPRETTET_AV", nullable = false)
//    public String getOpprettetAv() {
//        return opprettetAv;
//    }
//
//    public void setOpprettetAv(String opprettetAv) {
//        this.opprettetAv = opprettetAv;
//    }
//
//    @Id
//    @Column(name="RODESTREKNING_ID", nullable = false)
//    public Long getRodestrekningId() {
//        return rodestrekningId;
//    }
//
//    public void setRodestrekningId(Long rodestrekningId) {
//        this.rodestrekningId = rodestrekningId;
//    }
//
//
//    @Column(nullable = false)
//    public Long getSlettet() {
//        return slettet;
//    }
//
//    public void setSlettet(Long slettet) {
//        this.slettet = slettet;
//    }
//
//    public String getVeifunksjon() {
//        return veifunksjon;
//    }
//
//    public void setVeifunksjon(String veifunksjon) {
//        this.veifunksjon = veifunksjon;
//    }
//
//    @Column(nullable = false)
//    public String getVeikategori() {
//        return veikategori;
//    }
//
//    public void setVeikategori(String veikategori) {
//        this.veikategori = veikategori;
//    }
//
//    @Column(nullable = false)
//    public Long getVeinummer() {
//        return veinummer;
//    }
//
//    public void setVeinummer(Long veinummer) {
//        this.veinummer = veinummer;
//    }
//
//    @Column(nullable = false)
//    public String getVeistatus() {
//        return veistatus;
//    }
//
//    public void setVeistatus(String veistatus) {
//        this.veistatus = veistatus;
//    }
//
//    @ManyToOne
//    @JoinColumn(name = "RODE_ID", referencedColumnName = "RODE_ID")
//    public Rode getRode() {
//        return rode;
//    }
//
//    public void setRode(Rode rode) {
//        this.rode = rode;
//    }
//    
//    @Override
//    public boolean equals(Object o) {
//        if (o instanceof Rodestrekning){
//            Rodestrekning other = (Rodestrekning) o;
//            return getRodestrekningId().equals(other.getRodestrekningId());
//        }
//        return false;
//    }
//    
//    @Override
//    public int hashCode() {
//        return getRodestrekningId().hashCode();
//    }
}
