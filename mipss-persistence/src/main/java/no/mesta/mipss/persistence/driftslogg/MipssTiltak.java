package no.mesta.mipss.persistence.driftslogg;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

public class MipssTiltak implements IRenderableMipssEntity, Serializable{
	private Long rodeId;
	private String rode;
	private String navn;
	private Double timer;
	private Double tonn;
	private Double km;
	private String stroprodukt;
	private String stroproduktgruppe;
	private Double literLosningTotalt;
	private Double kgLosning;

	public MipssTiltak() {

	}

	public String getProdtype(){
		StringBuilder sb = new StringBuilder(navn);
		if (stroprodukt!=null){
			sb.append(" : ");
			sb.append(stroprodukt);
		}
		return sb.toString();
	}
	public Double getKm() {
		return km;
	}

	public void setKm(Double km) {
		this.km = km;
	}

	public Double getTonn() {
		return tonn;
	}

	public void setTonn(Double tonn) {
		this.tonn = tonn;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public Double getTimer() {
		return timer;
	}

	public void setTimer(Double timer) {
		this.timer = timer;
	}

	public String getStroprodukt() {
		return stroprodukt;
	}

	public void setStroprodukt(String stroprodukt) {
		this.stroprodukt = stroprodukt;
	}

	public String getTextForGUI() {
		return null;
	}

	public void setRode(String rode) {
		this.rode = rode;
	}

	public String getRode() {
		return rode;
	}

	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}

	public Long getRodeId() {
		return rodeId;
	}

	public String getStroproduktgruppe() {
		return stroproduktgruppe;
	}
	public void setStroproduktgruppe(String stroproduktgruppe){
		this.stroproduktgruppe = stroproduktgruppe;
	}

	public Double getLiterLosningTotalt() {
		return literLosningTotalt;
	}

	public void setLiterLosningTotalt(Double literLosningTotalt) {
		this.literLosningTotalt = literLosningTotalt;
	}

	public Double getKgLosning() {
		return kgLosning;
	}

	public void setKgLosning(Double kgLosning) {
		this.kgLosning = kgLosning;
	}
}
