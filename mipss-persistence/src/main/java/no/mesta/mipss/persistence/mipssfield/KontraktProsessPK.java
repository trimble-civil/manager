package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * PK klasse for KontraktProsess
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class KontraktProsessPK implements Serializable{
	private Long kontraktId;
	private Long prosessId;
	
	public KontraktProsessPK(){
		
	}
	public Long getKontraktId() {
		return kontraktId;
	}
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	public Long getProsessId() {
		return prosessId;
	}
	public void setProsessId(Long prosessId) {
		this.prosessId = prosessId;
	}
	
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("kontraktId", kontraktId).
            append("prosessId", prosessId).
            toString();
    }
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktProsessPK)) {return false;}
        
        KontraktProsessPK other = (KontraktProsessPK) o;
        
        return new EqualsBuilder().
            append(kontraktId, other.getKontraktId()).
            append(prosessId, other.getProsessId()).
            isEquals();
    }
	 
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2061,2039).append(kontraktId).append(prosessId).toHashCode();
    }
}
