package no.mesta.mipss.persistence.driftslogg;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@Table(name="PERIODESTATUS")
public class PeriodestatusType extends MipssEntityBean<PeriodestatusType>{

	@Id
	private Long id;
	@Column(name="STATUS")
	private String status;
	
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
}
