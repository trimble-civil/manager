package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.persistence.Temporal;

import javax.persistence.TemporalType;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "Selskap.findAll", query = "select o from Selskap o")
public class Selskap implements Serializable, IRenderableMipssEntity, Comparable<Selskap> {
	
	public static final String FIND_ALL = "Selskap.findAll";
	
    private String endretAv;
    private Date endretDato;
    private String selskapskode;
    private String navn;
    private String opprettetAv;
    private Date opprettetDato;
    private List<SelskapBruker> selskapBrukerList;
    private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
    
    public Selskap() {
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    @Id
    @Column(nullable = false)
    public String getSelskapskode() {
        return selskapskode;
    }

    public void setSelskapskode(String selskapskode) {
        this.selskapskode = selskapskode;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @OneToMany(mappedBy = "selskap")
    public List<SelskapBruker> getSelskapBrukerList() {
        return selskapBrukerList;
    }

    public void setSelskapBrukerList(List<SelskapBruker> selskapBrukerList) {
        this.selskapBrukerList = selskapBrukerList;
    }

    public SelskapBruker addSelskapBruker(SelskapBruker selskapBruker) {
        getSelskapBrukerList().add(selskapBruker);
        selskapBruker.setSelskap(this);
        return selskapBruker;
    }

    public SelskapBruker removeSelskapBruker(SelskapBruker selskapBruker) {
        getSelskapBrukerList().remove(selskapBruker);
        selskapBruker.setSelskap(null);
        return selskapBruker;
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    /**
     * Sammenligner navnet til et selskap, det er det som er GUI sorteringen
     * 
     * @param o
     * @return
     */
    public int compareTo(Selskap other) {
        return new CompareToBuilder().
            append(navn, other.getNavn()).
            toComparison();
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Selskap)) {return false;}
        
        Selskap other = (Selskap) o;
        return new EqualsBuilder().
            append(selskapskode, other.getSelskapskode()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(99,97).
            append(selskapskode).
            toHashCode();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("selskap", selskapskode).
            append("navn", navn).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            toString();
    }
    
    /**
     * Henter alle kjøretøy i et selskap.
     * @return
     */
    @ManyToMany(mappedBy="selskapList", fetch = FetchType.LAZY)
    public List<Kjoretoy> getKjoretoyList() {
        return kjoretoyList;
    }

    public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
        this.kjoretoyList = kjoretoyList;
    }
    
    /**
     * Legger til et kjoretoy i et mange-til-mange forhold.
     * VIKTIG: påse at denne metoden og den tilsvarende i Kjoretoy ikke kaller hverandre i løkke.
     * @param kjoretoy
     * @return det ferdig koblede kjoretoy
     */
    public Kjoretoy addKjoretoy(Kjoretoy kjoretoy) {
    	if(! getKjoretoyList().contains(kjoretoy)) {
    		getKjoretoyList().add(kjoretoy);
    	}
    	
    	return kjoretoy;
    }

    /**
     * Fjerner et kjoretoy fra et mange-til-mange forhold.
     * VIKTIG: påse at denne metoden og den tilsvarende i Kjoretoy ikke kaller hverandre i løkke.
     * @param kjoretoy
     * @return det frakoblede kjoretoy
     */
    public Kjoretoy removeKjoretoy(Kjoretoy kjoretoy) {
    	getKjoretoyList().remove(kjoretoy);
    	if(kjoretoy.getSelskapList().contains(this)) {
    		kjoretoy.removeSelskap(this);
    	}
    	return kjoretoy;
    }
    
}
