package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

public class Stroinfo implements Serializable {

	private Long measureId;
	private Double literLosning;
	private Double kgLosning;

	public static String[] getPropertiesArray() {
		return new String[] {
				"measureId",
				"literLosning",
				"kgLosning"
		};
	}

	public Long getMeasureId() {
		return measureId;
	}

	public void setMeasureId(Long measureId) {
		this.measureId = measureId;
	}

	public Double getLiterLosning() {
		return literLosning;
	}

	public void setLiterLosning(Double literLosning) {
		this.literLosning = literLosning;
	}

	public Double getKgLosning() {
		return kgLosning;
	}

	public void setKgLosning(Double kgLosning) {
		this.kgLosning = kgLosning;
	}

}