package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

public class PotentialDeviantActivity {

    private String contractorName;
    private String vehicleExternalName;
    private Long vehicleId;
    private Long activityId;
    private Long tripId;
    private Date fromDateTime;
    private Date toDateTime;
    private String rodeName;
    private String productionType;
    private String reporterComment;
    private String contractorComment;
    private Boolean manuallyCreated;
    private Boolean productionOutsideContract;
    private Boolean productionOutsideRode;
    private Boolean approvedBuild;
    private Double r12Planing;
    private Double r12SidePlough;
    private Double r12HeavyPlaning;
    private boolean negativeDeviation;
    private boolean positiveDeviation;

    public static String[] getPropertiesArray() {
        return new String[] {
            "contractorName",
            "vehicleExternalName",
            "vehicleId",
            "activityId",
            "tripId",
            "fromDateTime",
            "toDateTime",
            "rodeName",
            "productionType",
            "reporterComment",
            "contractorComment",
            "manuallyCreated",
            "productionOutsideContract",
            "productionOutsideRode",
            "approvedBuild",
            "r12Planing",
            "r12SidePlough",
            "r12HeavyPlaning"
        };
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getVehicleExternalName() {
        return vehicleExternalName;
    }

    public void setVehicleExternalName(String vehicleExternalName) {
        this.vehicleExternalName = vehicleExternalName;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Date getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public Date getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }

    public String getRodeName() {
        return rodeName;
    }

    public void setRodeName(String rodeName) {
        this.rodeName = rodeName;
    }

    public String getProductionType() {
        return productionType;
    }

    public void setProductionType(String productionType) {
        this.productionType = productionType;
    }

    public String getReporterComment() {
        return reporterComment;
    }

    public void setReporterComment(String reporterComment) {
        this.reporterComment = reporterComment;
    }

    public String getContractorComment() {
        return contractorComment;
    }

    public void setContractorComment(String contractorComment) {
        this.contractorComment = contractorComment;
    }

    public Boolean isManuallyCreated() {
        return manuallyCreated;
    }

    public void setManuallyCreated(Boolean manuallyCreated) {
        this.manuallyCreated = manuallyCreated;
    }

    public Boolean isProductionOutsideContract() {
        return productionOutsideContract;
    }

    public void setProductionOutsideContract(Boolean productionOutsideContract) {
        this.productionOutsideContract = productionOutsideContract;
    }

    public Boolean isProductionOutsideRode() {
        return productionOutsideRode;
    }

    public void setProductionOutsideRode(Boolean productionOutsideRode) {
        this.productionOutsideRode = productionOutsideRode;
    }

    public Boolean isApprovedBuild() {
        return approvedBuild;
    }

    public void setApprovedBuild(Boolean approvedBuild) {
        this.approvedBuild = approvedBuild;
    }

    public Double getR12Planing() {
        return r12Planing;
    }

    public void setR12Planing(Double r12Planing) {
        this.r12Planing = r12Planing;
    }

    public Double getR12SidePlough() {
        return r12SidePlough;
    }

    public void setR12SidePlough(Double r12SidePlough) {
        this.r12SidePlough = r12SidePlough;
    }

    public Double getR12HeavyPlaning() {
        return r12HeavyPlaning;
    }

    public void setR12HeavyPlaning(Double r12HeavyPlaning) {
        this.r12HeavyPlaning = r12HeavyPlaning;
    }

    public boolean hasNegativeDeviation() {
        return negativeDeviation;
    }

    public void setNegativeDeviation(boolean negativeDeviation) {
        this.negativeDeviation = negativeDeviation;
    }

    public boolean hasPositiveDeviation() {
        return positiveDeviation;
    }

    public void setPositiveDeviation(boolean positiveDeviation) {
        this.positiveDeviation = positiveDeviation;
    }

}