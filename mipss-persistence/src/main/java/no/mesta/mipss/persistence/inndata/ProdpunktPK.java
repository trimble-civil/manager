package no.mesta.mipss.persistence.inndata;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * N�kkel for prod punkt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class ProdpunktPK implements Serializable {
    public Long dfuId;
    public Date gmtTidspunkt;

    /**
     * Konstrukt�r
     * 
     */
    public ProdpunktPK() {
    }

    /**
     * Konstrukt�r
     * 
     * @param dfuId
     * @param gmtTidspunkt
     */
    public ProdpunktPK(Long dfuId, Timestamp gmtTidspunkt) {
        this.dfuId = dfuId;
        this.gmtTidspunkt = gmtTidspunkt;
    }

    /**
     * Dfu
     * 
     * @param dfuId
     */
    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    /**
     * Dfu
     * 
     * @return
     */
    public Long getDfuId() {
        return dfuId;
    }

    /**
     * Tid
     * 
     * @param gmtTidspunkt
     */
    public void setGmtTidspunkt(Date gmtTidspunkt) {
        this.gmtTidspunkt = gmtTidspunkt;
    }

    /**
     * Tid
     * 
     * @return
     */
    public Date getGmtTidspunkt() {
        return gmtTidspunkt;
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("dfuId", dfuId).
            append("gmtTidspunkt", gmtTidspunkt).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof ProdpunktPK)) {return false;}
        
        ProdpunktPK other = (ProdpunktPK) o;
        return new EqualsBuilder().append(dfuId, other.getDfuId()).
            append(gmtTidspunkt, other.getGmtTidspunkt()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2027,2073).append(dfuId).append(gmtTidspunkt).toHashCode();
    }
}
