package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

public class ApprovedActivity {

    private Long activityId;
    private String vendorName;
    private Integer vehicleId;
    private String vehicleName;
    private Integer rodeId;
    private String rodeName;
    private Date fromDateTime;
    private Date toDateTime;
    private Boolean manuallyCreated;
    private Double r12Planing;
    private Double r12SidePlough;
    private Double r12HeavyPlaning;
    private GritProductAmount gritProductAmount;
    private ActivityRodeInfo activityRodeInfo;
    private Boolean approvedBuild;
    private Boolean billIntention;
    private Double r12Intention;
    private Double totalProdStretchDistanceDriven;
    private String prodType;
    private Integer rodeConnectionTypeId;
    private double actualDuration;
    private boolean isCoproduction;
    private String gritType;

    public static String[] getPropertiesArray() {
        return new String[] {
            "activityId",
            "vendorName",
            "vehicleId",
            "vehicleName",
            "rodeId",
            "rodeName",
            "fromDateTime",
            "toDateTime",
            "manuallyCreated",
            "r12Planing",
            "r12SidePlough",
            "r12HeavyPlaning",
            "approvedBuild",
            "billIntention",
            "r12Intention",
            "prodType",
            "rodeConnectionTypeId"
        };
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Integer getRodeId() {
        return rodeId;
    }

    public void setRodeId(Integer rodeId) {
        this.rodeId = rodeId;
    }

    public String getRodeName() {
        return rodeName;
    }

    public void setRodeName(String rodeName) {
        this.rodeName = rodeName;
    }

    public Date getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public Date getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }

    public Boolean getManuallyCreated() {
        return manuallyCreated;
    }

    public void setManuallyCreated(Boolean manuallyCreated) {
        this.manuallyCreated = manuallyCreated;
    }

    public Boolean isManuallyCreated() {
        return getManuallyCreated();
    }

    public Double getR12Planing() {
        return r12Planing;
    }

    public void setR12Planing(Double r12Planing) {
        this.r12Planing = r12Planing;
    }

    public Double getR12SidePlough() {
        return r12SidePlough;
    }

    public void setR12SidePlough(Double r12SidePlough) {
        this.r12SidePlough = r12SidePlough;
    }

    public Double getR12HeavyPlaning() {
        return r12HeavyPlaning;
    }

    public void setR12HeavyPlaning(Double r12HeavyPlaning) {
        this.r12HeavyPlaning = r12HeavyPlaning;
    }

    public GritProductAmount getGritProductAmount() {
        return gritProductAmount;
    }

    public void setGritProductAmount(GritProductAmount gritProductAmount) {
        this.gritProductAmount = gritProductAmount;
    }

    public ActivityRodeInfo getActivityRodeInfo() {
        return activityRodeInfo;
    }

    public void setActivityRodeInfo(ActivityRodeInfo activityRodeInfo) {
        this.activityRodeInfo = activityRodeInfo;
    }

    public Boolean getApprovedBuild() {
        return approvedBuild;
    }

    public void setApprovedBuild(Boolean approvedBuild) {
        this.approvedBuild = approvedBuild;
    }

    public Boolean getBillIntention() {
        return billIntention;
    }

    public void setBillIntention(Boolean billIntention) {
        this.billIntention = billIntention;
    }

    public Double getR12Intention() {
        return r12Intention;
    }

    public void setR12Intention(Double r12Intention) {
        this.r12Intention = r12Intention;
    }

    public Double getTotalProdStretchDistanceDriven() {
        return totalProdStretchDistanceDriven;
    }

    public void addTotalProdStretchDistanceDriven(Double totalProdStretchDistanceDriven) {
        if (this.totalProdStretchDistanceDriven != null) {
            this.totalProdStretchDistanceDriven += totalProdStretchDistanceDriven;
        } else {
            this.totalProdStretchDistanceDriven = totalProdStretchDistanceDriven;
        }
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public Integer getRodeConnectionTypeId() {
        return rodeConnectionTypeId;
    }

    public void setRodeConnectionTypeId(Integer rodeConnectionTypeId) {
        this.rodeConnectionTypeId = rodeConnectionTypeId;
    }

    public double getActualDuration() {
        return actualDuration;
    }

    public void setActualDuration(double actualDuration) {
        this.actualDuration = actualDuration;
    }

    public boolean isCoproduction() {
        return isCoproduction;
    }

    public void setCoproduction(boolean coproduction) {
        isCoproduction = coproduction;
    }

    public String getGritType() {
        return gritType;
    }

    public void setGritType(String gritType) {
        this.gritType = gritType;
    }

    @Override
    public String toString() {
        return "ApprovedActivity:\nActivityId: " + activityId + "\nVendor name: " + vendorName + "\n" + activityRodeInfo.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApprovedActivity activity = (ApprovedActivity) o;

        if (!activityId.equals(activity.activityId)) return false;
        if (!vendorName.equals(activity.vendorName)) return false;
        return activityRodeInfo != null ? activityRodeInfo.equals(activity.activityRodeInfo) : activity.activityRodeInfo == null;

    }

    @Override
    public int hashCode() {
        int result = activityId.hashCode();
        result = 31 * result + vendorName.hashCode();
        result = 31 * result + (activityRodeInfo != null ? activityRodeInfo.hashCode() : 0);
        return result;
    }

}