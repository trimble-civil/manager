package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Avvikaarsak.QUERY_FIND_ALL, query = "SELECT a FROM Avvikaarsak a"),
	@NamedQuery(name = Avvikaarsak.QUERY_FIND_VALID, query = "SELECT a FROM Avvikaarsak a where a.valgbar = true")
    })
@SequenceGenerator(name = Avvikaarsak.ID_SEQ, sequenceName = "AVVIKAARSAK_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Avvikaarsak extends MipssEntityBean<Avvikaarsak> implements Serializable, IRenderableMipssEntity{

	public static final String ID_SEQ = "avvikaarsakIdSeq";
	public static final String QUERY_FIND_ALL = "avvikaarsak.findAll";
	public static final String QUERY_FIND_VALID = "avvikaarsak.findValid";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Avvik.ID_SEQ)
	@Column(nullable=false)
	private Long id;
	private String navn;
	@Column(name="GUI_REKKEFOLGE")
	private Long guiRekkefolge;
	@Column(name="VALGBAR_FLAGG")
	private Boolean valgbar;
	@Column(name="ELRAPP_ID")
	private String elrappId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}
	public void setGuiRekkefolge(Long guiRekkefolge) {
		Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}
	public Boolean getValgbar() {
		return valgbar;
	}
	public void setValgbar(Boolean valgbar) {
		Boolean old = this.valgbar;
		this.valgbar = valgbar;
		firePropertyChange("valgbar", old, valgbar);
	}

	public String getElrappId() {
		return elrappId;
	}

	public void setElrappId(String elrappId) {
		this.elrappId = elrappId;
	}

	/** {@inheritDoc} */
    public String getTextForGUI() {
        return getNavn();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Avvikaarsak)) {return false;}
        
        final Avvikaarsak other = (Avvikaarsak) o;
        return new EqualsBuilder().
            append(id, other.getId()).
            isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2063,2037).append(id).toHashCode();
    }

    /** {@inheritDoc} */
	public int compareTo(final Avvikaarsak o) {
		if(o == null) {
			return 1;
		}
		
		return new CompareToBuilder().append(navn, o.navn).toComparison();
	}
	
}

