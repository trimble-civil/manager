package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "kontraktloggIdSeq", sequenceName = "KONTRAKTLOGG_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQuery(name = "Kontraktlogg.findAll", 
    query = "select o from Kontraktlogg o")
public class Kontraktlogg implements Serializable, IRenderableMipssEntity, Comparable<Kontraktlogg> {
    private String fritekst;
    private Long id;
    private String opprettetAv;
    private Timestamp opprettetDato;
    private Long kontraktId;
    private Driftkontrakt driftkontrakt;

    public Kontraktlogg() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kontraktloggIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    public Timestamp getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Timestamp opprettetDato) {
        this.opprettetDato = opprettetDato;
    }
    
    @Column(name="KONTRAKT_ID", nullable = true, insertable = false, updatable = false)
    public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	@ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
    public Driftkontrakt getDriftkontrakt() {
        return driftkontrakt;
    }

    public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
        this.driftkontrakt = driftkontrakt;
        
        if(driftkontrakt != null) {
        	setKontraktId(driftkontrakt.getId());
        } else {
        	setKontraktId(null);
        }
    }
    
    // de neste metodene er håndkodet
    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kontraktlogg)) {
            return false;
        }
        Kontraktlogg other = (Kontraktlogg) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert på Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(79,31).
            append(getId()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getOpprettetAv() + " - " + getOpprettetDato();
    }

    /** {@inheritDoc} */
    public int compareTo(Kontraktlogg other) {
        return new CompareToBuilder().
            append(getOpprettetDato(), other.getOpprettetDato()).
            toComparison();
    }
}
