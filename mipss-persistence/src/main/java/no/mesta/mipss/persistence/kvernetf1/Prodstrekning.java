package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.VeiInfoUtil;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Prodstrekning
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Prodstrekning.QUERY_FIND_ALL, query = "SELECT p FROM Prodstrekning p"), 
    @NamedQuery(name = Prodstrekning.QUERY_FIND_LAZY, query= "SELECT p FROM Prodstrekning p where p.dfuId=:dfuId and p.fraTidspunkt >= :fraTidspunkt and p.tilTidspunkt <=:tilTidspunkt order by p.fraTidspunkt, p.tilTidspunkt"),
    @NamedQuery(name = Prodstrekning.QUERY_FIND, query= "SELECT DISTINCT p FROM Prodstrekning p JOIN FETCH p.prodreflinkseksjonList where p.dfuId=:dfuId and p.fraTidspunkt >= :fraTidspunkt and p.tilTidspunkt <=:tilTidspunkt order by p.fraTidspunkt, p.tilTidspunkt"),
    @NamedQuery(name = Prodstrekning.QUERY_FIND_ONE, query="SELECT p FROM Prodstrekning p where p.id = :id")
    })
public class Prodstrekning implements Serializable, IRenderableMipssEntity, Comparable<Prodstrekning> {
    public static transient Logger logger = LoggerFactory.getLogger(Prodstrekning.class);
    public static final String QUERY_FIND_ALL = "Prodstrekning.findAll";
    public static final String QUERY_FIND_LAZY = "Prodstrekning.findLazy";
    public static final String QUERY_FIND = "Prodstrekning.find";
    
    /**
     
     select * from prodstrekning 
     where dfuId=1 
     and fraTidspunkt >= trunc(cast(?fratidspunkt as date)) 
     and fraTidspunkt <= trunc(cast(?tiltidspunkt as date))+1 - (1/86400) 
     and fraTidspunkt >= ?fratidspunkt
     and tilTidspunkt <= ?tiltidspunkt
     order by fraTidspunkt
     
     */
    public static final String QUERY_FIND_ONE = "Prodstrekning.findOne";
    private Long id;
    private Long dfuId;
    private String inspeksjonGuid;
    private Date fraTidspunkt;
    private Date tilTidspunkt;
    private String digi;
    private Long fylkesnummer;
    private Long kommunenummer;
    private String veikategori;
    private String veistatus;
    private Long veinummer;
    private Long hp;
    private Double fraKm;
    private Double tilKm;
    private Long hastighetMin;
    private Long hastighetMax;
    private Long temperatur;
    private Long sistePunktReflinkIdent;
    private Long sistePunktReflinkPosisjon;
    private String opprinnelse;
    private Date opprettetDato;
    private Inspeksjon inspeksjon;
    private Dfuindivid dfu;
    private List<Prodreflinkseksjon> prodreflinkseksjonList = new ArrayList<Prodreflinkseksjon>();
    
    /**
     * Produksjonstypen til denne strekningen, må hentes ved siden av rammeverket 
     */
    private String prodtype;
    private String materiale;
    private Double mengde;
    
    
    private Prodreflinkseksjon reflinkseksjonTemp = new Prodreflinkseksjon();
    
    public void setReflinkId(int reflinkIdent){
    	reflinkseksjonTemp.setReflinkIdent(reflinkIdent);
	}
	
	public void setReflinkFra(double fra){
		reflinkseksjonTemp.setFra(fra);
	}
	
	public void setReflinkTil(double til){
		reflinkseksjonTemp.setTil(til);
	}
	
	@Transient
	public Prodreflinkseksjon getReflinkseksjonTemp(){
		return reflinkseksjonTemp;
	}
	
    /**
     * Konstrukt�r
     * 
     */
    public Prodstrekning() {
    }

    /**
     * Dfu
     * 
     * @return
     */
    @Column(name="DFU_ID", nullable = false, insertable = false, updatable = false)
    public Long getDfuId() {
        return dfuId;
    }

    /**
     * Dfu
     * 
     * @param dfuId
     */
    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    /**
     * Digi
     * 
     * @return
     */
    public String getDigi() {
        return digi;
    }

    /**
     * Digi
     * 
     * @param digi
     */
    public void setDigi(String digi) {
        this.digi = digi;
    }
    public void setDigiByte(byte[] digi){
    	String s = "";
    	for (Byte b:digi){
    		s+=pad(Integer.toHexString(0xFF&b));
    	}
    	setDigi(s);
    	
    }
	private String pad(String hx){
		int pads = 2-hx.length();
		String padded = "";
		for (int i=0;i<pads;i++){
			padded+="0";
		}
		padded+=hx;
		return padded;
		
	}
    /**
     * Fra km
     * 
     * @return
     */
    @Column(name="FRA_KM", nullable = false)
    public Double getFraKm() {
        return fraKm;
    }

    /**
     * Fra km
     * 
     * @param fraKm
     */
    public void setFraKm(Double fraKm) {
        this.fraKm = fraKm;
    }

    /**
     * Fra
     * 
     * @return
     */
    @Column(name="FRA_TIDSPUNKT")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getFraTidspunkt() {
        return fraTidspunkt;
    }

    /**
     * Fra
     * 
     * @param fraTidspunkt
     */
    public void setFraTidspunkt(Date fraTidspunkt) {
        this.fraTidspunkt = fraTidspunkt;
    }

    /**
     * Fylke
     * 
     * @return
     */
    @Column(nullable = false)
    public Long getFylkesnummer() {
        return fylkesnummer;
    }

    /**
     * Fylke
     * 
     * @param fylkesnummer
     */
    public void setFylkesnummer(Long fylkesnummer) {
        this.fylkesnummer = fylkesnummer;
    }

    /**
     * Fart
     * 
     * @return
     */
    @Column(name="HASTIGHET_MAX", nullable = false)
    public Long getHastighetMax() {
        return hastighetMax;
    }

    /**
     * Fart
     * 
     * @param hastighetMax
     */
    public void setHastighetMax(Long hastighetMax) {
        this.hastighetMax = hastighetMax;
    }

    /**
     * Fart
     * 
     * @return
     */
    @Column(name="HASTIGHET_MIN", nullable = false)
    public Long getHastighetMin() {
        return hastighetMin;
    }

    /**
     * Fart
     * 
     * @param hastighetMin
     */
    public void setHastighetMin(Long hastighetMin) {
        this.hastighetMin = hastighetMin;
    }

    /**
     * Hovedparcell
     * 
     * @return
     */
    @Column(nullable = false)
    public Long getHp() {
        return hp;
    }

    /**
     * Hovedparcell
     * 
     * @param hp
     */
    public void setHp(Long hp) {
        this.hp = hp;
    }

    /**
     * Id
     * 
     * @return
     */
    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Inspeksjon
     * 
     * @return
     */
    @Column(name="INSPEKSJON_GUID", insertable = false, updatable = false)
    public String getInspeksjonGuid() {
        return inspeksjonGuid;
    }

    /**
     * Inspeksjon
     * 
     * @param inspeksjonGuid
     */
    public void setInspeksjonGuid(String inspeksjonGuid) {
        this.inspeksjonGuid = inspeksjonGuid;
    }

    /**
     * Kommune
     * 
     * @return
     */
    @Column(nullable = false)
    public Long getKommunenummer() {
        return kommunenummer;
    }

    /**
     * Kommune
     * 
     * @param kommunenummer
     */
    public void setKommunenummer(Long kommunenummer) {
        this.kommunenummer = kommunenummer;
    }

    /**
     * N�r ble registreringen opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r ble registreringen opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Opprinnelse
     * 
     * @return
     */
    public String getOpprinnelse() {
        return opprinnelse;
    }

    /**
     * Opprinnelse
     * 
     * @param opprinnelse
     */
    public void setOpprinnelse(String opprinnelse) {
        this.opprinnelse = opprinnelse;
    }

    /**
     * Punkt reflink ident
     * 
     * @return
     */
    @Column(name="SISTE_PUNKT_REFLINK_IDENT")
    public Long getSistePunktReflinkIdent() {
        return sistePunktReflinkIdent;
    }

    /**
     * Punkt reflink ident
     * 
     * @param sistePunktReflinkIdent
     */
    public void setSistePunktReflinkIdent(Long sistePunktReflinkIdent) {
        this.sistePunktReflinkIdent = sistePunktReflinkIdent;
    }

    /**
     * Punkt reflink posisjon
     * 
     * @return
     */
    @Column(name="SISTE_PUNKT_REFLINK_POSISJON")
    public Long getSistePunktReflinkPosisjon() {
        return sistePunktReflinkPosisjon;
    }

    /**
     * Punkt reflink posisjon
     * 
     * @param sistePunktReflinkPosisjon
     */
    public void setSistePunktReflinkPosisjon(Long sistePunktReflinkPosisjon) {
        this.sistePunktReflinkPosisjon = sistePunktReflinkPosisjon;
    }

    /**
     * Temperatur
     * 
     * @return
     */
    public Long getTemperatur() {
        return temperatur;
    }

    /**
     * Temperatur
     * 
     * @param temperatur
     */
    public void setTemperatur(Long temperatur) {
        this.temperatur = temperatur;
    }

    /**
     * Til km
     * 
     * @return
     */
    @Column(name="TIL_KM", nullable = false)
    public Double getTilKm() {
        return tilKm;
    }

    /**
     * TIl km
     * 
     * @param tilKm
     */
    public void setTilKm(Double tilKm) {
        this.tilKm = tilKm;
    }

    /**
     * Til
     * 
     * @return
     */
    @Column(name="TIL_TIDSPUNKT")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTilTidspunkt() {
        return tilTidspunkt;
    }

    /**
     * Til
     * 
     * @param tilTidspunkt
     */
    public void setTilTidspunkt(Date tilTidspunkt) {
        this.tilTidspunkt = tilTidspunkt;
    }

    /**
     * Vei kat
     * 
     * @return
     */
    @Column(nullable = false)
    public String getVeikategori() {
        return veikategori;
    }

    /**
     * Vei kat
     * 
     * @param veikategori
     */
    public void setVeikategori(String veikategori) {
        this.veikategori = veikategori;
    }

    /**
     * Vei nr
     * 
     * @return
     */
    @Column(nullable = false)
    public Long getVeinummer() {
        return veinummer;
    }

    /**
     * Vei nr
     * 
     * @param veinummer
     */
    public void setVeinummer(Long veinummer) {
        this.veinummer = veinummer;
    }

    /**
     * Vei status
     * 
     * @return
     */
    @Column(nullable = false)
    public String getVeistatus() {
        return veistatus;
    }

    /**
     * Vei status
     * 
     * @param veistatus
     */
    public void setVeistatus(String veistatus) {
        this.veistatus = veistatus;
    }

    /**
     * Inspeksjon
     * 
     * @param inspeksjon
     */
    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
    }
    
    
    /**
     * Inspeksjon
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID")
    public Inspeksjon getInspeksjon() {
        return inspeksjon;
    }
    /**
     * Dfu
     * 
     * @param dfu
     */
    
    public void setDfu(Dfuindivid dfu) {
        this.dfu = dfu;
    }

    /**
     * Dfu
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "DFU_ID", referencedColumnName = "ID")
    public Dfuindivid getDfu() {
        return dfu;
    }
    
    @OneToMany(mappedBy = "prodstrekning")
    public List<Prodreflinkseksjon> getProdreflinkseksjonList() {
        return prodreflinkseksjonList;
    }

    public void setProdreflinkseksjonList(List<Prodreflinkseksjon> prodreflinkseksjonList) {
        this.prodreflinkseksjonList = prodreflinkseksjonList;
    }

    public Prodreflinkseksjon addProdreflinkseksjon(Prodreflinkseksjon prodreflinkseksjon) {
        getProdreflinkseksjonList().add(prodreflinkseksjon);
        prodreflinkseksjon.setProdstrekning(this);
        return prodreflinkseksjon;
    }

    public Prodreflinkseksjon removeProdreflinkseksjon(Prodreflinkseksjon prodreflinkseksjon) {
        getProdreflinkseksjonList().remove(prodreflinkseksjon);
        prodreflinkseksjon.setProdstrekning(null);
        return prodreflinkseksjon;
    }
    
    /**
     * Skriver ut veiinformasjon opp til HP
     * 
     * @return
     */
    @Transient
    public String getVei() {
        return VeiInfoUtil.buildVeiInfo(this).getVei();
    }
    @Transient
    public String getVeitype(){
    	String vs = "";
    	if (getVeikategori()!=null){
    		vs+=getVeikategori();
    	}
    	if (getVeistatus()!=null)
    		vs+=getVeistatus();
    	return vs;
    }
    
    @Transient
    public double getLengde(){
    	if (tilKm>fraKm){
    		return ((double)Math.round((tilKm-fraKm)*1000))/1000;
    	}
    	return ((double)Math.round((fraKm-tilKm)*1000))/1000;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("dfuId", dfuId).
            append("inspeksjonGuid", inspeksjonGuid).
            append("fraTidspunkt", fraTidspunkt).
            append("tilTidspunkt", tilTidspunkt).
            append("digi", digi).
            append("fylkesnummer", fylkesnummer).
            append("kommunenummer", kommunenummer).
            append("veikategori", veikategori).
            append("veistatus", veistatus).
            append("veinummer", veinummer).
            append("hp", hp).
            append("fraKm", fraKm).
            append("tilKm", tilKm).
            append("hastighetMin", hastighetMin).
            append("hastighetMax", hastighetMax).
            append("temperatur", temperatur).
            append("sistePunktReflinkIdent", sistePunktReflinkIdent).
            append("sistePunktReflinkPosisjon", sistePunktReflinkPosisjon).
            append("opprinnelse", opprinnelse).
            append("opprettetDato", opprettetDato).
            toString();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        trace("ME: {" + this + "}, OTHER: {" + o + "}");
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Prodstrekning)) {return false;}
        
        Prodstrekning other = (Prodstrekning) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2029,2071).append(id).toHashCode();
    }
    
    /**
     * Hjelp!
     * 
     * @param msg
     */
    private void trace(String msg) {
        //System.out.println(msg);
        
        if(logger == null) {
            logger = LoggerFactory.getLogger(Prodstrekning.class);
        }
        
        if(logger.isDebugEnabled()) {
            logger.debug(msg);
        }
    }

    /** {@inheritDoc} */
    public String getTextForGUI() {
        return VeiInfoUtil.buildVeiInfo(this).toString();
    }

    /** {@inheritDoc} */
    public int compareTo(Prodstrekning o) {
        return getTextForGUI().compareTo(o.getTextForGUI());
    }

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	@Transient
	/**
	 * Produksjonstypen til denne strekningen. Dette er et transient felt som må hentes ved siden av JPA. 
	 */
	public String getProdtype() {
		return prodtype;
	}
	
	public void setMateriale(String materiale){
		this.materiale = materiale;
	}
	/**
	 * Materiale til denne strekningen. Dette er et transient felt som må hentes ved siden av JPA. 
	 */
	@Transient
	public String getMateriale(){
		return materiale;
	}
	public void setMengde(Double mengde){
		this.mengde = mengde;
	}
	/**
	 * Mengde til denne strekningen. Dette er et transient felt som må hentes ved siden av JPA. 
	 */
	@Transient
	public Double getMengde(){
		return mengde;
	}
}
