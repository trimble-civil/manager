package no.mesta.mipss.persistence.driftslogg;

public class GritProductAmount {

    private Long activityId;
    private Integer gritProductId;
    private Double dryGrit;
    private Double wetGrit;

    public static String[] getPropertiesArray() {
        return new String[] {
                "activityId",
                "gritProductId",
                "dryGrit",
                "wetGrit"
        };
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getGritProductId() {
        return gritProductId;
    }

    public void setGritProductId(Integer gritProductId) {
        this.gritProductId = gritProductId;
    }

    public Double getWetGrit() {
        return wetGrit;
    }

    public void setWetGrit(Double wetGrit) {
        this.wetGrit = wetGrit;
    }

    public Double getDryGrit() {
        return dryGrit;
    }

    public void setDryGrit(Double dryGrit) {
        this.dryGrit = dryGrit;
    }

}