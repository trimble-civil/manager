package no.mesta.mipss.persistence.driftslogg;

import javax.persistence.*;
import java.io.Serializable;

@SequenceGenerator(name = TripGritProduct.ID_SEQ, sequenceName = "TRIP_GRIT_PRODUCT_ID_SEQ", initialValue = 1, allocationSize = 1)

@SuppressWarnings("serial")
@Entity
@Table(name="TRIP_GRIT_PRODUCT")
public class TripGritProduct implements Serializable {

    public static final String ID_SEQ = "tripGritProductIdSeq";

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TripGritProduct.ID_SEQ)
    private Long id;

    @Column(name = "NAME")
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}