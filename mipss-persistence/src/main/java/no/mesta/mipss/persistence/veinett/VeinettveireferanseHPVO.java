package no.mesta.mipss.persistence.veinett;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Klasse som implementerer en annen equals-metode for veinettveireferanse,
 * ønsker her at kun ned til HP skal væe avgjørende for om en veireferanse er
 * lik en annen
 */
@SuppressWarnings("serial")
public class VeinettveireferanseHPVO extends Veinettveireferanse {

	@Override
	public boolean equals(Object o) {
		VeinettveireferanseHPVO v = (VeinettveireferanseHPVO) o;
		return new EqualsBuilder()
		.append(getFylkesnummer(), v.getFylkesnummer())
		.append(getKommunenummer(), v.getKommunenummer())
		.append(getVeikategori(), v.getVeikategori())
		.append(getVeistatus(), v.getVeistatus())
		.append(getVeinummer(), v.getVeinummer())
		.append(getHp(), v.getHp())
		.isEquals();
	}

}
