package no.mesta.mipss.persistence.kontrakt;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.eclipse.persistence.indirection.IndirectList;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "leverandorIdSeq", sequenceName = "LEVERANDOR_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = Leverandor.FIND_ALL, query = "select o from Leverandor o"),
    @NamedQuery(name= Leverandor.FIND_BY_KJORETOY, query="select l from Leverandor l join l.kjoretoyList k where k.id=:kjoretoyId"),
    @NamedQuery(name= Leverandor.FIND_BY_DRIFTSKONTRAKT, query="select l from Leverandor l join l.kontraktLeverandorList k where k.kontraktId=:kontraktId")
})

@Table(name = "LEVERANDOR")

@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)

public class Leverandor extends MipssEntityBean<Leverandor> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity  {
    public static final String FIND_ALL = "Leverandor.findAll";
    public static final String FIND_BY_KJORETOY = "Leverandor.findByKjoretoy";
    public static final String FIND_BY_DRIFTSKONTRAKT= "Leverandor.findByDriftskontrakt";
    private String navn;
    private String nr;
    private Boolean valgbarFlagg;
    
    // felt lagt til for hånd
    private OwnedMipssEntity ownedMipssEntity;
    private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
    private List<LevProdtypeRode> levProdtypeRodeList = new ArrayList<LevProdtypeRode>();
    private List<LevProdtypeRode> slettedeLevProdtypeRodeList = new ArrayList<LevProdtypeRode>();
    private List<KontraktLeverandor> kontraktLeverandorList = new ArrayList<KontraktLeverandor>();
    private PropertyChangeSupport props;
    
    public Leverandor() {
        props = new PropertyChangeSupport(this);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        String old = this.navn;
        this.navn = navn;
        props.firePropertyChange("navn", old, navn);
    }

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "leverandorIdSeq")
    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
    	String old = this.nr;
        this.nr = nr;
        firePropertyChange("nr", old, nr);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
    	if(valgbarFlagg == null) {
    		valgbarFlagg = false;
    	}
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        
        props.firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }
    
    // de neste metodene er håndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("nummer", getNr()).
            append("navn", getNavn()).
//            append("geopunkt", getGeopunktId()).
            append("valgbar", getValgbarFlagg()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Leverandor)) {
            return false;
        }
        Leverandor other = (Leverandor) o;
        return new EqualsBuilder().
            append(getNr(), other.getNr()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getNr()).
//            append(getNavn()).
//            append(getGeopunktId()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Leverandor other = (Leverandor) o;
            return new CompareToBuilder().
                append(getNavn(), other.getNavn()).
                toComparison();
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }

    @OneToMany(mappedBy = "leverandor")
    public List<Kjoretoy> getKjoretoyList() {
        return kjoretoyList;
    }

    public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
        List<Kjoretoy> old = this.kjoretoyList;
        this.kjoretoyList = kjoretoyList;
        
        if(! (kjoretoyList instanceof IndirectList)) {
        	props.firePropertyChange("kjoretoyList", old, kjoretoyList);
        }
    }
    
    @OneToMany(mappedBy = "leverandor", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public List<LevProdtypeRode> getLevProdtypeRodeList() {
        return levProdtypeRodeList;
    }

    public void setLevProdtypeRodeList(List<LevProdtypeRode> levProdtypeRodeList) {
        List<LevProdtypeRode> old = this.levProdtypeRodeList;
        this.levProdtypeRodeList = levProdtypeRodeList;
        
        if(! (levProdtypeRodeList instanceof IndirectList)) {
        	if(old instanceof IndirectList) {
        		old = null;
        	}
        	props.firePropertyChange("levProdtypeRodeList", old, levProdtypeRodeList);
        }
    }
    
    @Transient
    public List<LevProdtypeRode> getSlettedeLevProdtypeRodeList() {
		return slettedeLevProdtypeRodeList;
	}

	public void setSlettedeLevProdtypeRodeList(List<LevProdtypeRode> slettedeLevProdtypeRodeList) {
		this.slettedeLevProdtypeRodeList = slettedeLevProdtypeRodeList;
	}

	@OneToMany(mappedBy = "leverandor", cascade = CascadeType.ALL)
    public List<KontraktLeverandor> getKontraktLeverandorList() {
        return kontraktLeverandorList;
    }

    public void setKontraktLeverandorList(List<KontraktLeverandor> kontraktLeverandorList) {
        List<KontraktLeverandor> old = this.kontraktLeverandorList;
        this.kontraktLeverandorList = kontraktLeverandorList;
        
        if(! (kontraktLeverandorList instanceof IndirectList)) {
        	props.firePropertyChange("kontraktLeverandorList", old, kontraktLeverandorList);
        }
    }
}
