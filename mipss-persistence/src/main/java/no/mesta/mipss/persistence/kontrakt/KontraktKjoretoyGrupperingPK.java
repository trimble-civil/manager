package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

@SuppressWarnings("serial")
public class KontraktKjoretoyGrupperingPK implements Serializable{

	private Long kjoretoygruppeId;
	private Long kontraktId;
	private Long kjoretoyId;

	public KontraktKjoretoyGrupperingPK(){
		
	}
	
	public KontraktKjoretoyGrupperingPK(Long kjoretoygruppeId, Long kontraktId, Long kjoretoyId){
		this.kjoretoygruppeId = kjoretoygruppeId;
		this.kontraktId = kontraktId;
		this.kjoretoyId = kjoretoyId;
		
	}

	public Long getKjoretoygruppeId() {
		return kjoretoygruppeId;
	}

	public Long getKontraktId() {
		return kontraktId;
	}

	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoygruppeId(Long kjoretoygruppeId) {
		this.kjoretoygruppeId = kjoretoygruppeId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
}
