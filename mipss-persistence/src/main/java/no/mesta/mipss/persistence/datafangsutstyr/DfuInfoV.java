package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = DfuInfoV.QUERY_FIND_ALL, query = "select o from DfuInfoV o")
@Table(name = "DFU_INFO_V")
public class DfuInfoV implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "DfuInfoV.findAll";
	
	private Long id;
	private String leverandorNavn;
	private String modellNavn;
	private String serienummer;
	private String versjon;
	private String simkortnummer;
	private String tlfnummer;
	private Date anskaffetDato;
	private String fritekst;
	private Date opprettetDato;
	private String opprettetAv;
	private Date endretDato;
	private String endretAv;
	private Long slettetFlagg;
	private String programvareversjon;
	private Date sisteLivstegnDato;
	private Date sistePosGmtTidspunkt;
	private Date sisteProdPosGmtTidspunkt;
	private Date statusOpprettetDato;
	private String statusNavn;
	private Date installasjonOpprettetDato;
	private Date installasjonAktivFraDato;
	private Date installasjonAktivTilDato;
	private String ignorerMaske;
	private Long kjoretoyId;
	private String regnr;
	private String maskinnummer;
	private String eksterntNavn;
	private String typeNavn;
	private String modell;
	private String kontraktTekst;
	
	public DfuInfoV() {
	}
	
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="LEVERANDOR_NAVN")
	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		this.leverandorNavn = leverandorNavn;
	}

	@Column(name="MODELL_NAVN")
	public String getModellNavn() {
		return modellNavn;
	}

	public void setModellNavn(String modellNavn) {
		this.modellNavn = modellNavn;
	}

	public String getSerienummer() {
		return serienummer;
	}

	public void setSerienummer(String serienummer) {
		this.serienummer = serienummer;
	}

	public String getVersjon() {
		return versjon;
	}

	public void setVersjon(String versjon) {
		this.versjon = versjon;
	}

	public String getSimkortnummer() {
		return simkortnummer;
	}

	public void setSimkortnummer(String simkortnummer) {
		this.simkortnummer = simkortnummer;
	}

	public String getTlfnummer() {
		return tlfnummer;
	}

	public void setTlfnummer(String tlfnummer) {
		this.tlfnummer = tlfnummer;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="ANSKAFFET_DATO")
	public Date getAnskaffetDato() {
		return anskaffetDato;
	}

	public void setAnskaffetDato(Date anskaffetDato) {
		this.anskaffetDato = anskaffetDato;
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="OPPRETTET_DATO")
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@Column(name="OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="ENDRET_DATO")
	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	@Column(name="ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	@Column(name="SLETTET_FLAGG")
	public Long getSlettetFlagg() {
		return slettetFlagg;
	}

	public void setSlettetFlagg(Long slettetFlagg) {
		this.slettetFlagg = slettetFlagg;
	}

	public String getProgramvareversjon() {
		return programvareversjon;
	}

	public void setProgramvareversjon(String programvareversjon) {
		this.programvareversjon = programvareversjon;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="SISTE_LIVSTEGN_DATO")
	public Date getSisteLivstegnDato() {
		return sisteLivstegnDato;
	}

	public void setSisteLivstegnDato(Date sisteLivstegnDato) {
		this.sisteLivstegnDato = sisteLivstegnDato;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="SISTE_POS_GMT_TIDSPUNKT")
	public Date getSistePosGmtTidspunkt() {
		return sistePosGmtTidspunkt;
	}

	public void setSistePosGmtTidspunkt(Date sistePosGmtTidspunkt) {
		this.sistePosGmtTidspunkt = sistePosGmtTidspunkt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="SISTE_PROD_POS_GMT_TIDSPUNKT")
	public Date getSisteProdPosGmtTidspunkt() {
		return sisteProdPosGmtTidspunkt;
	}

	public void setSisteProdPosGmtTidspunkt(Date sisteProdPosGmtTidspunkt) {
		this.sisteProdPosGmtTidspunkt = sisteProdPosGmtTidspunkt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="STATUS_OPPRETTET_DATO")
	public Date getStatusOpprettetDato() {
		return statusOpprettetDato;
	}

	public void setStatusOpprettetDato(Date statusOpprettetDato) {
		this.statusOpprettetDato = statusOpprettetDato;
	}

	@Column(name="STATUS_NAVN")
	public String getStatusNavn() {
		return statusNavn;
	}

	public void setStatusNavn(String statusNavn) {
		this.statusNavn = statusNavn;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="INSTALLASJON_OPPRETTET_DATO")
	public Date getInstallasjonOpprettetDato() {
		return installasjonOpprettetDato;
	}

	public void setInstallasjonOpprettetDato(Date installasjonOpprettetDato) {
		this.installasjonOpprettetDato = installasjonOpprettetDato;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="INSTALLASJON_AKTIV_FRA_DATO")
	public Date getInstallasjonAktivFraDato() {
		return installasjonAktivFraDato;
	}

	public void setInstallasjonAktivFraDato(Date installasjonAktivFraDato) {
		this.installasjonAktivFraDato = installasjonAktivFraDato;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="INSTALLASJON_AKTIV_TIL_DATO")
	public Date getInstallasjonAktivTilDato() {
		return installasjonAktivTilDato;
	}

	public void setInstallasjonAktivTilDato(Date installasjonAktivTilDato) {
		this.installasjonAktivTilDato = installasjonAktivTilDato;
	}

	@Column(name="IGNORER_MASKE")
	public String getIgnorerMaske() {
		return ignorerMaske;
	}

	public void setIgnorerMaske(String ignorerMaske) {
		this.ignorerMaske = ignorerMaske;
	}

	@Column(name="KJORETOY_ID")
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public String getRegnr() {
		return regnr;
	}

	public void setRegnr(String regnr) {
		this.regnr = regnr;
	}

	public String getMaskinnummer() {
		return maskinnummer;
	}

	public void setMaskinnummer(String maskinnummer) {
		this.maskinnummer = maskinnummer;
	}

	@Column(name="EKSTERNT_NAVN")
	public String getEksterntNavn() {
		return eksterntNavn;
	}

	public void setEksterntNavn(String eksterntNavn) {
		this.eksterntNavn = eksterntNavn;
	}

	@Column(name="TYPE_NAVN")
	public String getTypeNavn() {
		return typeNavn;
	}

	public void setTypeNavn(String typeNavn) {
		this.typeNavn = typeNavn;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	@Column(name="KONTRAKT_TEKST")
	public String getKontraktTekst() {
		return kontraktTekst;
	}

	public void setKontraktTekst(String kontraktTekst) {
		this.kontraktTekst = kontraktTekst;
	}

	public String getTextForGUI() {
		return getModellNavn();
	}
	
	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(1, 911).append(getId()).toHashCode();
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof DfuInfoV)) {
			return false;
		}
		DfuInfoV other = (DfuInfoV) o;
		return new EqualsBuilder().append(getId(), other.getId()).isEquals();
	}
	
	@Transient
	public String getTypeOgModell() {
		String ret = "";
		
		if(getTypeNavn() != null && getModell() != null) {
			ret = getTypeNavn() + ", " + getModell();
		} else if(getTypeNavn() != null) {
			ret = getTypeNavn();
		} else if(getModell() != null) {
			ret = getModell();
		}
		
		return ret;
	}
}
