package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class NokkeltallKjoretoy implements Serializable{
	private Long kontraktId;
	private Date maaned;
	private Long antKjoretoy;
	private Long antKjoretoyUtenLivstegn;
	private Date opprettetDato;
	
	
	public static final String[] getPropertiesArray(){
		return new String[]{"kontraktId", "maaned", "antKjoretoy", "antKjoretoyUtenLivstegn", "opprettetDato"};
	}

	
	/**
	 * @return the kontraktId
	 */
	public Long getKontraktId() {
		return kontraktId;
	}
	/**
	 * @param kontraktId the kontraktId to set
	 */
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	/**
	 * @return the maaned
	 */
	public Date getMaaned() {
		return maaned;
	}
	/**
	 * @param maaned the maaned to set
	 */
	public void setMaaned(Date maaned) {
		this.maaned = maaned;
	}
	/**
	 * @return the antKjoretoy
	 */
	public Long getAntKjoretoy() {
		return antKjoretoy;
	}
	/**
	 * @param antKjoretoy the antKjoretoy to set
	 */
	public void setAntKjoretoy(Long antKjoretoy) {
		this.antKjoretoy = antKjoretoy;
	}
	/**
	 * @return the antKjoretoyUtenLivstegn
	 */
	public Long getAntKjoretoyUtenLivstegn() {
		return antKjoretoyUtenLivstegn;
	}
	/**
	 * @param antKjoretoyUtenLivstegn the antKjoretoyUtenLivstegn to set
	 */
	public void setAntKjoretoyUtenLivstegn(Long antKjoretoyUtenLivstegn) {
		this.antKjoretoyUtenLivstegn = antKjoretoyUtenLivstegn;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

}
