package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * PublicKey for klassen AppBib
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class AppBibPK implements Serializable {
    public Long appId;
    public Long bibliotekId;

    /**
     * Konstrukt�r
     */
    public AppBibPK() {
    }

    /**
     * Konstrukt�r
     * 
     * @param appId
     * @param bibliotekId
     */
    public AppBibPK(Long appId, Long bibliotekId) {
        this.appId = appId;
        this.bibliotekId = bibliotekId;
    }

    /**
     * Applikasjon id
     * 
     * @param appId
     */
    public void setAppId(Long appId) {
        this.appId = appId;
    }

    /**
     * Applikasjon id
     * 
     * @return
     */
    public Long getAppId() {
        return appId;
    }

    /**
     * Bibliotek id
     * 
     * @param bibliotekId
     */
    public void setBibliotekId(Long bibliotekId) {
        this.bibliotekId = bibliotekId;
    }

    /**
     * Bibliotek id
     * 
     * @return
     */
    public Long getBibliotekId() {
        return bibliotekId;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof AppBibPK)) { return false;}
        if(o == this) { return true;}
        
        AppBibPK other = (AppBibPK) o;
        return new EqualsBuilder().
            append(appId, other.getAppId()).
            append(bibliotekId, other.getBibliotekId()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(19,981).
            append(appId).
            append(bibliotekId).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return getClass().getName() + "[" + appId + "," + bibliotekId + "]";
    }
}
