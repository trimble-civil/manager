package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@SuppressWarnings("serial")
@Entity
@Table(name = "dummy_eksisterer_ikke")
@SqlResultSetMappings( { @SqlResultSetMapping(name = SamPunktSok.RESULTSET_MAPPING_NAME, entities = { @EntityResult(entityClass = SamPunktSok.class, fields = {
		@FieldResult(name = "guid", column = "GUID"),	
		@FieldResult(name = "utmX", column = "UTM_X"),
		@FieldResult(name = "utmY", column = "UTM_Y"),
		@FieldResult(name = "hovedprosesskode", column = "HOVEDPROSESS_KODE"),
		@FieldResult(name = "prosesskode", column = "PROSESS_KODE"),
		@FieldResult(name = "prosessnavn", column = "PROSESS_NAVN"),
		@FieldResult(name = "trafikkfare", column = "TRAFIKKFARE_FLAGG"),
		@FieldResult(name = "beskrivelse", column = "BESKRIVELSE"),
		@FieldResult(name = "lukketDato", column = "LUKKET_DATO"),
		@FieldResult(name = "leverandor", column = "LEVERANDOR"),
		@FieldResult(name = "samType", column = "SAM_TYPE")
		}) }) })
public class SamPunktSok implements Serializable{

	public static final String RESULTSET_MAPPING_NAME = "SamPunktSokResult";
	private String guid;
	private Double utmX;
	private Double utmY;
	private String hovedprosesskode;
	private String prosesskode;
	private String prosessnavn;
	private Boolean trafikkfare;
	private String beskrivelse;
	private Date lukketDato;
	private String leverandor;
	private String samType;
	
	@Id
	@Column(name="GUID")
	public String getGuid(){
		return guid;
	}
	public void setGuid(String guid){
		this.guid = guid;
	}
	@Column(name="UTM_X")
	public Double getUtmX() {
		return utmX;
	}
	public void setUtmX(Double utmX) {
		this.utmX = utmX;
	}
	@Column(name="UTM_Y")
	public Double getUtmY() {
		return utmY;
	}
	public void setUtmY(Double utmY) {
		this.utmY = utmY;
	}
	@Column(name="HOVEDPROSESS_KODE")
	public String getHovedprosesskode() {
		return hovedprosesskode;
	}
	public void setHovedprosesskode(String hovedprosesskode) {
		this.hovedprosesskode = hovedprosesskode;
	}
	@Column(name="PROSESS_KODE")
	public String getProsesskode() {
		return prosesskode;
	}
	public void setProsesskode(String prosesskode) {
		this.prosesskode = prosesskode;
	}
	@Column(name="PROSESS_NAVN")
	public String getProsessnavn() {
		return prosessnavn;
	}
	public void setProsessnavn(String prosessnavn) {
		this.prosessnavn = prosessnavn;
	}
	@Column(name="TRAFIKKFARE_FLAGG")
	public Boolean getTrafikkfare() {
		return trafikkfare;
	}
	public void setTrafikkfare(Boolean trafikkfare) {
		this.trafikkfare = trafikkfare;
	}
	@Column(name="BESKRIVELSE")
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LUKKET_DATO")
	public Date getLukketDato() {
		return lukketDato;
	}
	public void setLukketDato(Date lukketDato) {
		this.lukketDato = lukketDato;
	}
	@Column(name="LEVERANDOR")
	public String getLeverandor() {
		return leverandor;
	}
	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}
	
	@Column(name="SAM_TYPE")
	public String getSamType(){
		return samType;
	}
	public void setSamType(String samType){
		this.samType = samType;
	}
	public String toString(){
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
			.append("utmX", utmX)
			.append("utmY", utmY)
			.append("hovedprosesskode", hovedprosesskode)
			.append("prosesskode", prosesskode)
			.append("prosessnavn", prosessnavn)
			.append("trafikkfare", trafikkfare)
			.append("beskrivelse", beskrivelse)
			.append("lukketDato", lukketDato)
			.append("leverandor", leverandor)
			.append("samType", samType)
			.toString();
	}
	
	
}
