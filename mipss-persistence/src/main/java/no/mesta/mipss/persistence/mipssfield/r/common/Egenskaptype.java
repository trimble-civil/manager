package no.mesta.mipss.persistence.mipssfield.r.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * Egenskaptype
 *
 * Definerer en type for en egenskap
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

@SuppressWarnings("serial")
@Entity(name = Egenskaptype.BEAN_NAME)
@Table(name="EGENSKAPTYPE")
public class Egenskaptype extends MipssEntityBean<Egenskaptype>{
	public static final String BEAN_NAME = "Egenskaptype";

	private String beskrivelse;
	@Id
	@Column(nullable = false)
	private Long id;
	private String navn;

	public String getBeskrivelse() {
		return this.beskrivelse;
	}

	public Long getId() {
		return this.id;
	}
	public String getNavn() {
		return this.navn;
	}

	@Override
	public String getTextForGUI(){
		return this.id+":"+this.navn;
	}
	public void setBeskrivelse(final String beskrivelse) {
		final String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}
	public void setId(final Long id) {
		final Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	public void setNavn(final String navn) {
		final String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
}
