package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = KjoretoyDok.FIND_ALL, query = "select o from KjoretoyDok o")
})

@Table(name = "KJORETOY_DOK")
@IdClass(KjoretoyDokPK.class)
public class KjoretoyDok implements Serializable {
	public static final String FIND_ALL = "KjoretoyDok.findAll";
	
    private Long dokumentId;
    private Long kjoretoyId;
    private Kjoretoy kjoretoy;

    public KjoretoyDok() {
    }

    @Id
    @Column(name="DOKUMENT_ID", nullable = false)
    public Long getDokumentId() {
        return dokumentId;
    }

    public void setDokumentId(Long dokumentId) {
        this.dokumentId = dokumentId;
    }

    @Id
    @Column(name="KJORETOY_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
        this.kjoretoy = kjoretoy;
    }
    // de neste metodene er håndkodet
     /**
      * Genererer en lesbar tekst med hovedlementene i entitybønnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         return new ToStringBuilder(this).
             append("kjoretoyId", getKjoretoyId()).
             append("dokumentId", getDokumentId()).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante nøklene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof KjoretoyDok)) {
             return false;
         }
         KjoretoyDok other = (KjoretoyDok) o;
         return new EqualsBuilder().
             append(getKjoretoyId(), other.getKjoretoyId()).
             append(getDokumentId(), other.getDokumentId()).
             isEquals();
     }

     /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
         return new HashCodeBuilder(7,91).
             append(getKjoretoyId()).
             append(getDokumentId()).
             toHashCode();
     }
}
