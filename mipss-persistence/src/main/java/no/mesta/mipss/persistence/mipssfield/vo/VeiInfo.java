package no.mesta.mipss.persistence.mipssfield.vo;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.Serializable;

import java.util.Arrays;

import no.mesta.mipss.common.PropertyChangeSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holder informasjon om et veipunkt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class VeiInfo implements Serializable, Cloneable, PropertyChangeSource {

	private static final Logger log = LoggerFactory.getLogger(VeiInfo.class);

	private static transient Logger logger = null;
	private int fylkesnummer;
	private int hp;
	private String kjorefelt;
	private int kommunenummer;
	private int meter;
	private transient PropertyChangeSupport props = new PropertyChangeSupport(this);
	private String veikategori;
	private int veinummer;
	private String veistatus;

	/**
	 * Konstruktør
	 * 
	 */
	public VeiInfo() {
	}

	/**
	 * Copy konstruktør
	 * 
	 * @param i
	 */
	public VeiInfo(VeiInfo i) {
		if (i != null) {
			fylkesnummer = i.getFylkesnummer();
			hp = i.getHp();
			kjorefelt = i.getKjorefelt();
			kommunenummer = i.getKommunenummer();
			meter = i.getMeter();
			veikategori = i.getVeikategori();
			veinummer = i.getVeinummer();
			veistatus = i.getVeistatus();
		}
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		log.trace("addPropertyChangeListener({})", l);
		getProps().addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		log("addPropertyChangeListener", propName, l);
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public Object clone() {
		return new VeiInfo(this);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof VeiInfo)) {
			return false;
		}

		VeiInfo other = (VeiInfo) o;

		return new EqualsBuilder().append(fylkesnummer, other.getFylkesnummer()).append(kommunenummer,
				other.getKommunenummer()).append(veikategori, other.getVeikategori()).append(veistatus,
				other.getVeistatus()).append(veinummer, other.getVeinummer()).append(hp, other.getHp()).append(meter,
				other.getMeter()).append(kjorefelt, other.getKjorefelt()).isEquals();
	}

	/**
	 * Fylke
	 * 
	 * @return
	 */
	public int getFylkesnummer() {
		return fylkesnummer;
	}

	/**
	 * Hovedparcell
	 * 
	 * @return
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * Kjørefelt
	 * 
	 * @return
	 */
	public String getKjorefelt() {
		return kjorefelt;
	}

	/**
	 * Kommune
	 * 
	 * @return
	 */
	public int getKommunenummer() {
		return kommunenummer;
	}

	/**
	 * Meter
	 * 
	 * @return
	 */
	public int getMeter() {
		return meter;
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/**
	 * Skriver ut alt opp til HP
	 * 
	 * @return
	 */
	public String getVei() {
		String roadInfo = "";

		if (!StringUtils.isBlank(getVeikategori())) { // Om det er kommunevei,
			// fylkesvei, riksvei og
			// europavei
			roadInfo += String.valueOf(getVeikategori());
		}

		if (!StringUtils.isBlank(getVeistatus())) { // "Alltid" V = Vei
			roadInfo += String.valueOf(getVeistatus());
		}

		if (getVeinummer() > 0) {
			String s = String.valueOf(getVeinummer());
			s = StringUtils.leftPad(s, 2, " ");
			roadInfo += s + " ";
		}

		return roadInfo;
	}

	/**
	 * Vei kat
	 * 
	 * @return
	 */
	public String getVeikategori() {
		return veikategori;
	}

	/**
	 * Vei nr
	 * 
	 * @return
	 */
	public int getVeinummer() {
		return veinummer;
	}

	/**
	 * Status
	 * 
	 * @return
	 */
	public String getVeistatus() {
		return veistatus;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(fylkesnummer).append(kommunenummer).append(veikategori).append(veistatus)
				.append(veinummer).append(hp).append(meter).append(kjorefelt).toHashCode();
	}

	public boolean isBlank() {
		if (fylkesnummer == 0 && hp == 0 && StringUtils.isBlank(kjorefelt) && kommunenummer == 0 && meter == 0 && StringUtils.isBlank(veikategori) 
				&& veinummer == 0 && StringUtils.isBlank(veistatus) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Logger en beskjed som debug
	 *
	 * @param msg
	 */
	private void log(String msg, Object... args) {
		if (logger == null) {
			logger = LoggerFactory.getLogger(VeiInfo.class);
		}

		logger.debug("@" + Integer.toHexString(hashCode()) + msg + "(" + Arrays.toString(args) + ")");
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		log.trace("removePropertyChangeListener({})", l);
		getProps().removePropertyChangeListener(l);
	}
	
	public void clearPropertyChangeListeners(){
		for (PropertyChangeListener l:getProps().getPropertyChangeListeners()){
			removePropertyChangeListener(l);
		}
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		log("removePropertyChangeListener", propName, l);
		getProps().removePropertyChangeListener(propName, l);
	}

	/**
	 * Fylke
	 * 
	 * @param fylkesnummer
	 */
	public void setFylkesnummer(int fylkesnummer) {
		int old = this.fylkesnummer;
		log("setFylkesnummer", fylkesnummer, old);
		this.fylkesnummer = fylkesnummer;
		getProps().firePropertyChange("fylkesnummer", old, fylkesnummer);
	}

	/**
	 * Hovedparcell
	 * 
	 * @param hp
	 */
	public void setHp(int hp) {
		int old = this.hp;
		log("setHp", hp, old);
		this.hp = hp;
		getProps().firePropertyChange("hp", old, hp);
	}

	/**
	 * Kjørefelt
	 * 
	 * @param kjorefelt
	 */
	public void setKjorefelt(String kjorefelt) {
		String old = this.kjorefelt;
		log("setKjorefelt", kjorefelt, old);
		this.kjorefelt = kjorefelt;
		getProps().firePropertyChange("kjorefelt", old, kjorefelt);
	}

	/**
	 * Kommune
	 * 
	 * @param kommunenummer
	 */
	public void setKommunenummer(int kommunenummer) {
		int old = this.kommunenummer;
		log("setKommunenummer", kommunenummer, old);
		this.kommunenummer = kommunenummer;
		getProps().firePropertyChange("kommunenummer", old, kommunenummer);
	}

	/**
	 * Meter
	 * 
	 * @param meter
	 */
	public void setMeter(int meter) {
		int old = this.meter;
		log("setMeter", meter, old);
		this.meter = meter;
		getProps().firePropertyChange("meter", old, meter);
	}

	/**
	 * Vei kat
	 * 
	 * @param veikategori
	 */
	public void setVeikategori(String veikategori) {
		String old = this.veikategori;
		log("setVeikategori", veikategori, old);
		this.veikategori = veikategori;
		getProps().firePropertyChange("veikategori", old, veikategori);
	}

	/**
	 * Vei nr
	 * 
	 * @param veinummer
	 */
	public void setVeinummer(int veinummer) {
		int old = this.veinummer;
		log("setVeinummer", veinummer, old);
		this.veinummer = veinummer;
		getProps().firePropertyChange("veinummer", old, veinummer);
	}

	/**
	 * Status
	 * 
	 * @param veistatus
	 */
	public void setVeistatus(String veistatus) {
		String old = this.veistatus;
		log("setVeistatus", veistatus, old);
		this.veistatus = veistatus;
		getProps().firePropertyChange("veistatus", old, veistatus);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		String roadInfo = "";

		// INFO: Padding. Fylke 2siffer, kommune 2, veinr 5, hp 3, meter 5
		if (getFylkesnummer() > 0) { // Vises
			// alltid
			String s = String.valueOf(getFylkesnummer());
			s = StringUtils.leftPad(s, 2, " ");
			roadInfo += s + " ";
		}

		if (getKommunenummer() != 0) { // Vises ikke på fylkesvei, riksvei
			// og europavei
			String s = String.valueOf(getKommunenummer());
			s = StringUtils.leftPad(s, 2, " ");
			roadInfo += s + " ";
		}

		roadInfo += getVei();

		if (getHp() > 0) {
			String s = String.valueOf(getHp());
			roadInfo += "hp" + s + " ";
		}

		if (!StringUtils.isBlank(roadInfo) && getMeter() >= 0) {
			String s = String.valueOf(getMeter());
			roadInfo += "m" + s + " ";
		}

		if (!StringUtils.isBlank(kjorefelt)) {
			roadInfo += "felt-" + getKjorefelt();
		}

		return roadInfo;
	}

	/**
	 * Builder pattern metode for setFylkesnummer
	 * 
	 * @param fylkesnummer
	 * @return
	 */
	public VeiInfo withFylkesnummer(int fylkesnummer) {
		setFylkesnummer(fylkesnummer);
		return this;
	}

	/**
	 * Builder pattern metode for setHp
	 * 
	 * @param hp
	 * @return
	 */
	public VeiInfo withHp(int hp) {
		setHp(hp);
		return this;
	}

	/**
	 * Builder pattern metode for setKjorefelt
	 * 
	 * @param kjorefelt
	 * @return
	 */
	public VeiInfo withKjorefelt(String kjorefelt) {
		setKjorefelt(kjorefelt);
		return this;
	}

	/**
	 * Builder pattern metode for setKommunenummer
	 * 
	 * @param kommunenummer
	 * @return
	 */
	public VeiInfo withKommunenummer(int kommunenummer) {
		setKommunenummer(kommunenummer);
		return this;
	}

	/**
	 * Builder pattern metode for setMeter
	 * 
	 * @param meter
	 * @return
	 */
	public VeiInfo withMeter(int meter) {
		setMeter(meter);
		return this;
	}

	/**
	 * Builder pattern metode for setVeikategori
	 * 
	 * @param veikategori
	 * @return
	 */
	public VeiInfo withVeikategori(String veikategori) {
		setVeikategori(veikategori);
		return this;
	}

	/**
	 * Builder pattern metode for setVeinummer
	 * 
	 * @param veinummer
	 * @return
	 */
	public VeiInfo withVeinummer(int veinummer) {
		setVeinummer(veinummer);
		return this;
	}

	/**
	 * Builder pattern metode for
	 * 
	 * @param status
	 * @return
	 */
	public VeiInfo withVeistatus(String status) {
		setVeistatus(status);
		return this;
	}
}
