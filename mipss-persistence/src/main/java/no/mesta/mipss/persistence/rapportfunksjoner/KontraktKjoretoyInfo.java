package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class KontraktKjoretoyInfo implements Serializable {
	private Date dfuSisteLivstegnDato;
	private Date dfuSistePosisjonDato;
	private Long sprUtenDatafangst;
	private String aktuellStroperiodeKlartekst;
	private Long ukjentStrometode;
	private Long ukjentProduksjon;
	private Date bestillingstatusDato;
	private String bestillingstatus;
	private String aktuellKonfigKlartekst;
	
	/**
	 * @return the dfuSisteLivstegnDato
	 */
	public Date getDfuSisteLivstegnDato() {
		return dfuSisteLivstegnDato;
	}
	/**
	 * @param dfuSisteLivstegnDato the dfuSisteLivstegnDato to set
	 */
	public void setDfuSisteLivstegnDato(Date dfuSisteLivstegnDato) {
		this.dfuSisteLivstegnDato = dfuSisteLivstegnDato;
	}
	/**
	 * @return the dfuSistePosisjonDato
	 */
	public Date getDfuSistePosisjonDato() {
		return dfuSistePosisjonDato;
	}
	/**
	 * @param dfuSistePosisjonDato the dfuSistePosisjonDato to set
	 */
	public void setDfuSistePosisjonDato(Date dfuSistePosisjonDato) {
		this.dfuSistePosisjonDato = dfuSistePosisjonDato;
	}
	/**
	 * @return the sprUtenDatafangst
	 */
	public Long getSprUtenDatafangst() {
		return sprUtenDatafangst;
	}
	/**
	 * @param sprUtenDatafangst the sprUtenDatafangst to set
	 */
	public void setSprUtenDatafangst(Long sprUtenDatafangst) {
		this.sprUtenDatafangst = sprUtenDatafangst;
	}
	/**
	 * @return the aktuellStroperiodeKlartekst
	 */
	public String getAktuellStroperiodeKlartekst() {
		return aktuellStroperiodeKlartekst;
	}
	/**
	 * @param aktuellStroperiodeKlartekst the aktuellStroperiodeKlartekst to set
	 */
	public void setAktuellStroperiodeKlartekst(String aktuellStroperiodeKlartekst) {
		this.aktuellStroperiodeKlartekst = aktuellStroperiodeKlartekst;
	}
	/**
	 * @return the ukjentStrometode
	 */
	public Long getUkjentStrometode() {
		return ukjentStrometode;
	}
	/**
	 * @param ukjentStrometode the ukjentStrometode to set
	 */
	public void setUkjentStrometode(Long ukjentStrometode) {
		this.ukjentStrometode = ukjentStrometode;
	}
	/**
	 * @return the ukjentProduksjon
	 */
	public Long getUkjentProduksjon() {
		return ukjentProduksjon;
	}
	/**
	 * @param ukjentProduksjon the ukjentProduksjon to set
	 */
	public void setUkjentProduksjon(Long ukjentProduksjon) {
		this.ukjentProduksjon = ukjentProduksjon;
	}
	/**
	 * @return the bestillingstatusDato
	 */
	public Date getBestillingstatusDato() {
		return bestillingstatusDato;
	}
	/**
	 * @param bestillingstatusDato the bestillingstatusDato to set
	 */
	public void setBestillingstatusDato(Date bestillingstatusDato) {
		this.bestillingstatusDato = bestillingstatusDato;
	}
	/**
	 * @return the bestillingstatus
	 */
	public String getBestillingstatus() {
		return bestillingstatus;
	}
	/**
	 * @param bestillingstatus the bestillingstatus to set
	 */
	public void setBestillingstatus(String bestillingstatus) {
		this.bestillingstatus = bestillingstatus;
	}
	/**
	 * @return the aktuellKonfigKlartekst
	 */
	public String getAktuellKonfigKlartekst() {
		return aktuellKonfigKlartekst;
	}
	/**
	 * @param aktuellKonfigKlartekst the aktuellKonfigKlartekst to set
	 */
	public void setAktuellKonfigKlartekst(String aktuellKonfigKlartekst) {
		this.aktuellKonfigKlartekst = aktuellKonfigKlartekst;
	}
	
}
