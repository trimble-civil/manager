package no.mesta.mipss.persistence.veinett;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = Veinettreflinkseksjon.ID_SEQ, sequenceName = "VEINETTREFLINKSEKSJON_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
@NamedQueries({
	@NamedQuery(name = Veinettreflinkseksjon.QUERY_FIND_ALL, query = "select o from Veinettreflinkseksjon o"),
	@NamedQuery(name = Veinettreflinkseksjon.QUERY_FIND, query = "select o from Veinettreflinkseksjon o where o.veinett.id=:veinettId"),
	@NamedQuery(name = Veinettreflinkseksjon.QUERY_FIND_EAGER, query = "select o from Veinettreflinkseksjon o join fetch o.veinettveireferanseList where o.veinett.id=:veinettId")
 })
 
public class Veinettreflinkseksjon implements Serializable, IOwnableMipssEntity {
    public static final String QUERY_FIND_ALL = "Veinettreflinkseksjon.findAll";
    public static final String QUERY_FIND = "Veinettreflinkseksjon.find";
    public static final String QUERY_FIND_EAGER = "Veinettreflinkseksjon.findEager";
    public static final String ID_SEQ = "veinettreflinkseksjonIdSeq";
    
    private Double fra;
    private Long id;
    private Long reflinkIdent;
    private Double til;
    private List<Veinettveireferanse> veinettveireferanseList = new ArrayList<Veinettveireferanse>();
    private Veinett veinett;
    
    //felt lagt til for hånd
    private OwnedMipssEntity ownedMipssEntity;
    public Veinettreflinkseksjon() {
    }

    public Veinettreflinkseksjon(Veinettreflinkseksjon v){
    	setReflinkIdent(v.getReflinkIdent());
    	setFra(v.getFra());
    	setTil(v.getTil());
    }

    public Double getFra() {
        return fra;
    }

    public void setFra(Double fra) {
        this.fra = fra;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Veinettreflinkseksjon.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="REFLINK_IDENT")
    public Long getReflinkIdent() {
        return reflinkIdent;
    }

    public void setReflinkIdent(Long reflinkIdent) {
        this.reflinkIdent = reflinkIdent;
    }

    public Double getTil() {
        return til;
    }

    public void setTil(Double til) {
        this.til = til;
    }


    @OneToMany(mappedBy = "veinettreflinkseksjon", cascade=CascadeType.ALL)
    public List<Veinettveireferanse> getVeinettveireferanseList() {
    	return veinettveireferanseList;
    }

    public void setVeinettveireferanseList(List<Veinettveireferanse> veinettveireferanseList) {
        this.veinettveireferanseList = veinettveireferanseList;
    }


    @ManyToOne
    @JoinColumn(name = "VEINETT_ID", referencedColumnName = "ID")
    public Veinett getVeinett() {
        return veinett;
    }

    public void setVeinett(Veinett veinett) {
        this.veinett = veinett;
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("reflinkIdent", reflinkIdent).
            append("fra", fra).
            append("til", til).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Veinettreflinkseksjon)) {return false;}
        
        Veinettreflinkseksjon other = (Veinettreflinkseksjon) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(741,751).append(id).toHashCode();
    }
}

