package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Knyttingen mellom et bibliotek og en applikasjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = AppBib.QUERY_FIND_ALL, query = "SELECT o FROM AppBib o")
})
@Table(name = "APP_BIB")
@IdClass(AppBibPK.class)
public class AppBib implements Serializable {
    public static final String QUERY_FIND_ALL = "AppBib.findAll";
    private Long appId;
    private Long bibliotekId;
    private String opprettetAv;
    private Date opprettetDato;
    private Applikasjon applikasjon;
    private Bibliotek bibliotek;

        /**
         * Konstruktør
         */
        public AppBib() {
        }

        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @see no.mesta.maven.mipssdeployer.persistence.AppBibPK#getAppId()
         * @return
         */
        @Id
        @Column(name="APP_ID", nullable = false, insertable = false, updatable = false)
        public Long getAppId() {
            return appId;
        }

        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @see no.mesta.maven.mipssdeployer.persistence.AppBibPK#setAppId(Long)
         * @param appId
         */
        public void setAppId(Long appId) {
            this.appId = appId;
        }

        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @see no.mesta.maven.mipssdeployer.persistence.AppBibPK#getBibliotekId()
         * @return
         */
        @Id
        @Column(name="BIBLIOTEK_ID", nullable = false, insertable = false, 
            updatable = false)
        public Long getBibliotekId() {
            return bibliotekId;
        }

        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @see no.mesta.maven.mipssdeployer.persistence.AppBibPK#setBibliotekId(Long)
         * @param bibliotekId
         */
        public void setBibliotekId(Long bibliotekId) {
            this.bibliotekId = bibliotekId;
        }

        /**
         * Hvem opprettet objektet
         * 
         * @return
         */
        @Column(name="OPPRETTET_AV")
        public String getOpprettetAv() {
            return opprettetAv;
        }

        /**
         * Hvem opprettet objektet
         * 
         * @param opprettetAv
         */
        public void setOpprettetAv(String opprettetAv) {
            this.opprettetAv = opprettetAv;
        }

        /**
         * Når ble objektet opprettet
         * 
         * @return
         */
        @Column(name="OPPRETTET_DATO")
        @Temporal(TemporalType.TIMESTAMP)
        public Date getOpprettetDato() {
            return opprettetDato;
        }

        /**
         * Når ble objektet opprettet
         * 
         * @param opprettetDato
         */
        public void setOpprettetDato(Date opprettetDato) {
            this.opprettetDato = opprettetDato;
        }

        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @return
         */
        @ManyToOne
        @JoinColumn(name = "APP_ID", referencedColumnName = "ID")
        public Applikasjon getApplikasjon() {
            return applikasjon;
        }
        
        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @param applikasjon
         */
        public void setApplikasjon(Applikasjon applikasjon) {
            this.applikasjon = applikasjon;
            
            if(applikasjon != null) {
                setAppId(applikasjon.getId());
            } else {
                setAppId(null);
            }
        }
        
        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @return
         */
        @ManyToOne
        @JoinColumn(name = "BIBLIOTEK_ID", referencedColumnName = "ID")
        public Bibliotek getBibliotek() {
            return bibliotek;
        }
        
        /**
         * Id, kompositt nøkkel med app og bib
         * 
         * @param bibliotek
         */
        public void setBibliotek(Bibliotek bibliotek) {
            this.bibliotek = bibliotek;
            
            if(bibliotek != null) {
                setBibliotekId(bibliotek.getId());
            } else {
                setBibliotekId(null);
            }
        }
        
        /**
         * @see java.lang.Object#equals(Object)
         * @param o
         * @return
         */
        @Override
        public boolean equals(Object o) {
            if(o == null) {return false;}
            if(!(o instanceof AppBib)) { return false;}
            if(o == this) { return true;}
            
            AppBib other = (AppBib) o;
            return new EqualsBuilder().
                append(appId, other.getAppId()).
                append(bibliotekId, other.getBibliotekId()).
                isEquals();
        }
        
        /**
         * @see java.lang.Object#hashCode()
         * @return
         */
        @Override
        public int hashCode() {
            return new HashCodeBuilder(21,979).
                append(appId).
                append(bibliotekId).
                toHashCode();
        } 
        
        /**
         * @see java.lang.Object#toString()
         * @return
         */
        @Override
        public String toString() {
            return new ToStringBuilder(this).
                append("appId", appId).
                append("bibliotekId", bibliotekId).
                toString();
        }
    }
