package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Rapp1 extends Rapp1Object implements Serializable{
	private Long rodeId;
	private String rodeNavn;
	private Long kjoretoyId;
	private String kjoretoyNavn;
	private Long paKontraktFlagg;
	private Long leverandorNr;
	private String leverandorNavn;
	private Long samproduksjonFlagg;
	private Date fraDato;
	private Date tilDato;
	private Double kjortKm;
	private Long kjortSekund;
	private Double rodelengde;
	
	private String vei;
	
	private List<R12Prodtype> prodtyper;
	private List<R12Stroprodukt> stroprodukter;
	
	public static String[] getPropertiesArray() {
		return new String[] { "rodeId", "rodeNavn", "kjoretoyId", "kjoretoyNavn", "paKontraktFlagg", "leverandorNr", "leverandorNavn", 
							  "samproduksjonFlagg", "fraDato", "tilDato", "kjortKm", "kjortSekund", "prodtyper", "stroprodukter", "rodelengde"};
	}
	
	public Long getRodeId() {
		return rodeId;
	}
	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}
	public String getRodeNavn() {
		return rodeNavn;
	}
	public void setRodeNavn(String rodeNavn) {
		this.rodeNavn = rodeNavn;
	}
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	public String getKjoretoyNavn() {
		return kjoretoyNavn;
	}
	public void setKjoretoyNavn(String kjoretoyNavn) {
		this.kjoretoyNavn = kjoretoyNavn;
	}
	public Long getPaKontraktFlagg() {
		return paKontraktFlagg;
	}
	public void setPaKontraktFlagg(Long paKontraktFlagg) {
		this.paKontraktFlagg = paKontraktFlagg;
	}
	public Long getLeverandorNr() {
		return leverandorNr;
	}
	public void setLeverandorNr(Long leverandorNr) {
		this.leverandorNr = leverandorNr;
	}
	public String getLeverandorNavn() {
		return leverandorNavn;
	}
	public void setLeverandorNavn(String leverandorNavn) {
		this.leverandorNavn = leverandorNavn;
	}
	public Long getSamproduksjonFlagg() {
		return samproduksjonFlagg;
	}
	public void setSamproduksjonFlagg(Long samproduksjonFlagg) {
		this.samproduksjonFlagg = samproduksjonFlagg;
	}
	public String getSamprodFlagg(){
		return samproduksjonFlagg!=null&&samproduksjonFlagg==1?"X":null;
	}
	public Date getFraDato() {
		return fraDato;
	}
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	public Date getTilDato() {
		return tilDato;
	}
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	public Double getKjortKm() {
		return kjortKm;
	}
	public void setKjortKm(Double kjortKm) {
		this.kjortKm = kjortKm;
	}
	public void addKjortSekund(Long kjortSekund){
		if (this.kjortSekund==null){
			this.kjortSekund=kjortSekund;
		}else{
			if (kjortSekund!=null){
				this.kjortSekund+=kjortSekund;
			}
		}
	}
	public void addKjortKm(Double kjortKm){
		if (this.kjortKm==null){
			this.kjortKm = kjortKm;
		}else{
			if (kjortKm!=null){
				this.kjortKm+=kjortKm;
			}
		}
	}
	public Long getKjortSekund() {
		return kjortSekund;
	}
	public double getKjortTimer(){
		return (double)kjortSekund/(double)86400;
	}
	public void setKjortSekund(Long kjortSekund) {
		this.kjortSekund = kjortSekund;
		this.sekund = kjortSekund;
	}
	public void setRodelengde(Double rodelengde) {
		this.rodelengde = rodelengde;
	}
	public Double getRodelengde() {
		return rodelengde;
	}
	public List<R12Prodtype> getProdtyper() {
		return prodtyper;
	}
	public void setProdtyper(List<R12Prodtype> prodtyper) {
		this.prodtyper = prodtyper;
	}
	public List<R12Stroprodukt> getStroprodukter() {
		return stroprodukter;
	}
	public void setStroprodukter(List<R12Stroprodukt> stroprodukter) {
		this.stroprodukter = stroprodukter;
	}
	public String getVei(){
		return vei;
	}
	public void setVei(String vei){
		this.vei = vei;
	}
	public Boolean getPaaKontrakt() {
		if (getPaKontraktFlagg()!=null){
			return getPaKontraktFlagg().intValue()==1;
		}
		return Boolean.FALSE;
	}

}
