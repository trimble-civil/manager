package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity

@NamedQueries({
    @NamedQuery(
        name = Utstyrprodusent.FIND_ALL, 
        query = "select o from Utstyrprodusent o")
})

public class Utstyrprodusent extends MipssEntityBean<Utstyrprodusent> implements Serializable, IRenderableMipssEntity {
    public static final String FIND_ALL = "Utstyrprodusent.findAll";
    
    private String fritekst;
    private String kontaktpersonNavn;
    private String navn;
    private Long tlfnummer;
    private String url;
    private List<Utstyrmodell> utstyrmodellList = new ArrayList<Utstyrmodell>();

    public Utstyrprodusent() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Column(name="KONTAKTPERSON_NAVN")
    public String getKontaktpersonNavn() {
        return kontaktpersonNavn;
    }

    public void setKontaktpersonNavn(String kontaktpersonNavn) {
    	String old = this.kontaktpersonNavn;
        this.kontaktpersonNavn = kontaktpersonNavn;
        firePropertyChange("kontaktpersonNavn", old, kontaktpersonNavn);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    public Long getTlfnummer() {
        return tlfnummer;
    }

    public void setTlfnummer(Long tlfnummer) {
    	Long old = this.tlfnummer;
        this.tlfnummer = tlfnummer;
        firePropertyChange("tlfnummer", old, tlfnummer);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
    	String old = this.url;
        this.url = url;
        firePropertyChange("url", old, url);
    }

    @OneToMany(mappedBy = "utstyrprodusent")
    public List<Utstyrmodell> getUtstyrmodellList() {
        return utstyrmodellList;
    }

    public void setUtstyrmodellList(List<Utstyrmodell> utstyrmodellList) {
        this.utstyrmodellList = utstyrmodellList;
    }

    public Utstyrmodell addUtstyrmodell(Utstyrmodell utstyrmodell) {
        getUtstyrmodellList().add(utstyrmodell);
        utstyrmodell.setUtstyrprodusent(this);
        return utstyrmodell;
    }

    public Utstyrmodell removeUtstyrmodell(Utstyrmodell utstyrmodell) {
        getUtstyrmodellList().remove(utstyrmodell);
        utstyrmodell.setUtstyrprodusent(null);
        return utstyrmodell;
    }

    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            append("kontaktperson", getKontaktpersonNavn()).
            append("url", getUrl()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Utstyrprodusent)) {
            return false;
        }
        Utstyrprodusent other = (Utstyrprodusent) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getNavn()).
            append(getKontaktpersonNavn()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Utstyrprodusent other = (Utstyrprodusent) o;
            return new CompareToBuilder().
                append(getNavn(), other.getNavn()).
                toComparison();
    }
}
