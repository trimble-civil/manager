package no.mesta.mipss.persistence.dokarkiv;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;
import no.mesta.mipss.persistence.applikasjon.Menypunkt;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Beskriver et ikon i databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = "IKON_SEQ", sequenceName = "IKON_ID_SEQ", allocationSize=1)
@NamedQueries({
    @NamedQuery(name = Ikon.QUERY_FIND_ALL, query = "SELECT i FROM Ikon i")
})
public class Ikon extends MipssEntityBean<Ikon> implements Serializable, IRenderableMipssEntity {
    public static final String QUERY_FIND_ALL = "Ikon.findAll";
    private Long id;
    private String filnavn;
    private Integer bredde;
    private Integer hoyde;
    private String opprettetAv;
    private Date opprettetDato;
    private String endretAv;
    private Date endretDato;
    private byte[] ikonLob;
    private List<Menygruppe> menygruppeList = new ArrayList<Menygruppe>();
    private List<Menypunkt> menypunktList = new ArrayList<Menypunkt>();
    private List<Dokformat> dokformatList = new ArrayList<Dokformat>();
    private Integer size;
    
    /**
     * Konstruktør
     * 
     */
    public Ikon() {
    }

    /**
     * Hvem endret ikonet
     * @return
     */
    @Column(name="ENDRET_AV", length=8)
    public String getEndretAv() {
        return endretAv;
    }

    /**
     * Hvem endret ikonet
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    /**
     * Når ble ikonet endret
     * 
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    /**
     * Når ble ikonet endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    /**
     * Ikonets filnavn
     * 
     * @return
     */
    @Column(nullable = false, length=255)
    public String getFilnavn() {
        return filnavn;
    }

    /**
     * Ikonets filnavn
     * 
     * @param filnavn
     */
    public void setFilnavn(String filnavn) {
    	String old = this.filnavn;
        this.filnavn = filnavn;
        firePropertyChange("filnavn", old, filnavn);
    }
    
    /**
     * Ikonets id
     * 
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IKON_SEQ")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Ikonets id
     * 
     * @param id
     */
    public void setId(Long id) {
        Long old = this.id;
    	this.id = id;
    	firePropertyChange("id", old, id);
    }

    /**
     * Ikonets binære bildeinnhold
     * 
     * @return
     */
    @Column(name="IKON_LOB", nullable = false)
    public byte[] getIkonLob() {
        return ikonLob;
    }

    /**
     * Ikonets binære bildeinnhold
     * 
     * @param ikonLob
     */
    public void setIkonLob(byte[] ikonLob) {
    	byte[] old = this.ikonLob;
        this.ikonLob = ikonLob;
        firePropertyChange("ikonLob", old, ikonLob);
    }

    /**
     * Hvem opprettet ikonet
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", length=8, nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Hvem opprettet ikonet
     * 
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    /**
     * Når ble ikonet opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * Når ble ikonet opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Liste over menygrupper som benytter ikonet
     * 
     * Relasjonen eies av Menygruppe.getIkon()
     * 
     * @return
     * @see no.mesta.mipss.persistence.applikasjon.Menygruppe#getIkon()
     */
    @OneToMany(mappedBy = "ikon")
    public List<Menygruppe> getMenygruppeList() {
        return menygruppeList;
    }

    /**
     * Liste over menygrupper som benytter ikonet
     * 
     * Relasjonen eies av Menygruppe.getIkon()
     * 
     * @param menygruppeList
     * @see no.mesta.mipss.persistence.applikasjon.Menygruppe#getIkon()
     */
    public void setMenygruppeList(List<Menygruppe> menygruppeList) {
        this.menygruppeList = menygruppeList;
    }

    /**
     * Legger til en menygruppe til dette ikonet
     * 
     * @param menygruppe
     * @see no.mesta.mipss.persistence.applikasjon.Menygruppe
     * @return
     */
    public Menygruppe addMenygruppe(Menygruppe menygruppe) {
        getMenygruppeList().add(menygruppe);
        menygruppe.setIkon(this);
        return menygruppe;
    }

    /**
     * Fjerner til en Menygruppe fra dette ikonet
     * 
     * @param menygruppe
     * @see no.mesta.mipss.persistence.applikasjon.Menygruppe
     * @return
     */
    public Menygruppe removeMenygruppe(Menygruppe menygruppe) {
        getMenygruppeList().remove(menygruppe);
        menygruppe.setIkon(null);
        return menygruppe;
    }

    /**
     * Liste over Menypunkt som benytter dette ikonet
     * 
     * Relasjonen eies av Menypunkt.getIkon()
     * 
     * @see no.mesta.mipss.persistence.applikasjon.Menypunkt#getIkon()
     * @return
     */
    @OneToMany(mappedBy = "ikon")
    public List<Menypunkt> getMenypunktList() {
        return menypunktList;
    }

    /**
     * Liste over Menypunkt som benytter dette ikonet
     * 
     * Relasjonen eies av Menypunkt.getIkon()
     * 
     * @see no.mesta.mipss.persistence.applikasjon.Menypunkt#getIkon()
     * @param menypunktList
     */
    public void setMenypunktList(List<Menypunkt> menypunktList) {
        this.menypunktList = menypunktList;
    }

    /**
     * Legger til et menypunkt til dette ikonet
     * 
     * @see no.mesta.mipss.persistence.applikasjon.Menypunkt
     * @param menypunkt
     * @return
     */
    public Menypunkt addMenypunkt(Menypunkt menypunkt) {
        getMenypunktList().add(menypunkt);
        menypunkt.setIkon(this);
        return menypunkt;
    }

    /**
     * Fjerner et Menypunkt fra dette ikonet
     * 
     * @see no.mesta.mipss.persistence.applikasjon.Menypunkt
     * @param menypunkt
     * @return
     */
    public Menypunkt removeMenypunkt(Menypunkt menypunkt) {
        getMenypunktList().remove(menypunkt);
        menypunkt.setIkon(null);
        return menypunkt;
    }

    /**
     * Liste over Dokformat som benytter dette Ikonet
     * 
     * Relasjonen eies av Dokformat.getIkon()
     * 
     * @see Dokformat#getIkon()
     * @return
     */
    @OneToMany(mappedBy = "ikon")
    public List<Dokformat> getDokformatList() {
        return dokformatList;
    }

    /**
     * Liste over Dokformat som benytter dette Ikonet
     * 
     * Relasjonen eies av Dokformat.getIkon()
     * 
     * @see Dokformat#getIkon()
     * @param dokformatList
     */
    public void setDokformatList(List<Dokformat> dokformatList) {
        this.dokformatList = dokformatList;
    }

    /**
     * Legger til et Dokformat som bruker dette Ikonet
     * 
     * @see Dokformat
     * @param dokformat
     * @return
     */
    public Dokformat addDokformat(Dokformat dokformat) {
        getDokformatList().add(dokformat);
        dokformat.setIkon(this);
        return dokformat;
    }

    /**
     * Fjerner et Dokformat som ikke lenger bruker dette Ikonet
     * 
     * @see Dokformat
     * @param dokformat
     * @return
     */
    public Dokformat removeDokformat(Dokformat dokformat) {
        getDokformatList().remove(dokformat);
        dokformat.setIkon(null);
        return dokformat;
    }

    /**
     * Ikonets bredde
     * 
     * @param bredde
     */
    public void setBredde(Integer bredde) {
    	Integer old = this.bredde;
        this.bredde = bredde;
        firePropertyChange("bredde", old, bredde);
    }

    /**
     * Ikonets bredde
     * 
     * @return
     */
    public Integer getBredde() {
        return bredde;
    }

    /**
     * Ikonets høyde
     * 
     * @param hoyde
     */
    public void setHoyde(Integer hoyde) {
    	Integer old = this.hoyde;
        this.hoyde = hoyde;
        firePropertyChange("hoyde", old, hoyde);
    }
    
    /**
     * Ikonets høyde
     * 
     * @return
     */
    public Integer getHoyde() {
        return hoyde;
    }

    /**
     * St�rrelsen på ikonet i bytes
     * 
     * @param size
     */
    public void setSize(Integer size) {
    	Integer old = this.size;
        this.size = size;
        firePropertyChange("size", old, size);
    }

    /**
     * St�rrelsen på ikonet i bytes
     * 
     * @return
     */
    @Transient
    public Integer getSize() {
        if(size == null) {
            if(ikonLob != null) {
                size = ikonLob.length;
            } else {
                size = 0;
            }
        }
        
        return size;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Ikon)) {return false;}
        if(o == this) {return true;}
        
        Ikon other = (Ikon) o;
        return new EqualsBuilder().
            append(id, other.getId()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3003,3997).
            append(id).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("filnavn", filnavn).
            append("bredde", bredde).
            append("hoyde", hoyde).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            append("ikonLob", (ikonLob == null ? "null" : ikonLob.length + " bytes")).
//            append("menygruppeList", (menygruppeList == null ? "null" : menygruppeList.size() + " items")).
//            append("menypunktList", (menypunktList == null ? "null" : menypunktList.size() + " items")).
//            append("dokformatList", (dokformatList == null ? "null" : dokformatList.size() + " items")).
            toString();
        
    }

	public String getTextForGUI() {		// 
		return filnavn;
	}
}
