/**
 * 
 */
package no.mesta.mipss.persistence.kjoretoyutstyr;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utvider PeriodeIoMonitor til å kunne brukes også for KjoretoyUtstyIo.
 * {@inheritDoc}
 * @author jorge
 *
 */
public class PeriodeIoMonitorKjoretoyUtstyrIO extends PeriodeIoMonitor {
	private Logger logger = LoggerFactory.getLogger(PeriodeIoMonitorKjoretoyUtstyrIO.class);
	
	public PeriodeIoMonitorKjoretoyUtstyrIO() {
		super();
	}
	
	/**
	 * Kontrollerer om det finnes overlappinger mellom {@code Utstyrperiode}r per port
	 * fro kjøretøyet som eier den leverte entiteten
	 * @param entity
	 */
	@PrePersist
	@PreUpdate
	public void monitorPeriodeKjoretoyUtstyrIO(KjoretoyUtstyrIo entity) {
		logger.debug("monitorering kalt");
		super.monitorPeriodeIo(entity.getKjoretoyUtstyr());
	}
	
	/**
	 * Skygger over superklassens metode for JPA lytting. 
	 * Dette er viktig for at TopLink ikke skal ta feil og kalle den.
	 */
	@Override
	public void monitorPeriodeIo(KjoretoyUtstyr entity) {}
}
