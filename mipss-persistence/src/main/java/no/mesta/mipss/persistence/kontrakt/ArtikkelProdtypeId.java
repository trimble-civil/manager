package no.mesta.mipss.persistence.kontrakt;

public class ArtikkelProdtypeId {

    private String leverandorNr;
    private long kontraktId;
    private String levkontraktId;
    private String artikkelId;

    public ArtikkelProdtypeId(String leverandorNr, long kontraktId, String levkontraktId, String artikkelId) {
        this.leverandorNr = leverandorNr;
        this.kontraktId = kontraktId;
        this.levkontraktId = levkontraktId;
        this.artikkelId = artikkelId;
    }
}
