package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.inndata.Prodpunkt;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Propunktinfo
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Prodpunktinfo.QUERY_FIND_ALL, query = "select o from Prodpunktinfo o"),
    @NamedQuery(name = Prodpunktinfo.QUERY_FIND_PRODPUNKTINFO, query= "SELECT DISTINCT p FROM Prodpunktinfo p JOIN FETCH p.prodpunkt WHERE p.dfuId=:dfuId AND p.gmtTidspunkt >=:fraTidspunkt AND p.gmtTidspunkt < :tilTidspunkt AND p.snappetX IS NOT NULL and p.snappetY IS NOT NULL order by p.gmtTidspunkt"),
    @NamedQuery(name = Prodpunktinfo.QUERY_FIND_SINGLEPRODPUNKTINFO, query= "SELECT p FROM Prodpunktinfo p WHERE p.dfuId=:dfuId AND p.gmtTidspunkt =:tidspunkt"),
    @NamedQuery(name = Prodpunktinfo.QUERY_FIND_LAST, query = "SELECT distinct p from Prodpunktinfo p " +
    														  "JOIN FETCH p.prodpunkt WHERE p.dfuId=:dfuId " +
    														  "AND p.gmtTidspunkt=(SELECT MAX(pi.gmtTidspunkt) " +
    														  "FROM Prodpunktinfo pi WHERE pi.dfuId=:dfuId " +
    														  "AND pi.gmtTidspunkt<:tilTidspunkt " +
    														  "AND pi.gmtTidspunkt>=:fraTidspunkt)"),
    @NamedQuery(name = Prodpunktinfo.QUERY_FIND_BY_INSPEKSJON_GUID, query = "SELECT p from Prodpunktinfo p where p.prodpunkt.inspeksjonGuid =:inspeksjonGuid order by p.gmtTidspunkt"),
    @NamedQuery(name = Prodpunktinfo.QUERY_FIND_BY_INSPEKSJON_GUID_SISTE, query = "SELECT p from Prodpunktinfo p where p.prodpunkt.inspeksjonGuid =:inspeksjonGuid and p.gmtTidspunkt = (SELECT MAX(pi.gmtTidspunkt) FROM Prodpunktinfo pi where pi.prodpunkt.inspeksjonGuid=:inspeksjonGuid)")
})

@IdClass(ProdpunktinfoPK.class)
public class Prodpunktinfo implements Serializable {
    public static final String QUERY_FIND_ALL = "Prodpunktinfo.findAll";
    public static final String QUERY_FIND_PRODPUNKTINFO = "Prodpunktinfo.findProdpunkt";
    public static final String QUERY_FIND_SINGLEPRODPUNKTINFO = "Prodpunktinfo.findSingle";
    public static final String QUERY_FIND_LAST = "Prodpunktinfo.findLast";
    public static final String QUERY_FIND_BY_INSPEKSJON_GUID = "Prodpunktinfo.findByInspeksjonGuid";
    public static final String QUERY_FIND_BY_INSPEKSJON_GUID_SISTE = "Prodpunktinfo.findByInspeksjonGuidSiste";
    private Long avstandForrigePunkt;
    private Long dfuId;
    private Long fylkesnummer;
    private Date gmtTidspunkt;
    private Long hp;
    private Long kommunenummer;
    private Long meter;
    private Date opprettetDato;
    private Long reflinkIdent;
    private Long reflinkPosisjon;
    private Double snappetX;
    private Double snappetY;
    private String veikategori;
    private Long veinummer;
    private String veistatus;
    private Prodpunkt prodpunkt;
    
    public Prodpunktinfo() {
    }

    @Column(name="AVSTAND_FORRIGE_PUNKT")
    public Long getAvstandForrigePunkt() {
        return avstandForrigePunkt;
    }

    public void setAvstandForrigePunkt(Long avstandForrigePunkt) {
        this.avstandForrigePunkt = avstandForrigePunkt;
    }

    @Id
    @Column(name="DFU_ID", nullable = false, insertable = false, updatable = false)
    public Long getDfuId() {
        return dfuId;
    }

    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    public Long getFylkesnummer() {
        return fylkesnummer;
    }

    public void setFylkesnummer(Long fylkesnummer) {
        this.fylkesnummer = fylkesnummer;
    }

    @Id
    @Column(name="GMT_TIDSPUNKT", nullable = false, insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getGmtTidspunkt() {
        return gmtTidspunkt;
    }

    public void setGmtTidspunkt(Date gmtTidspunkt) {
        this.gmtTidspunkt = gmtTidspunkt;
    }

    public Long getHp() {
        return hp;
    }

    public void setHp(Long hp) {
        this.hp = hp;
    }

    public Long getKommunenummer() {
        return kommunenummer;
    }

    public void setKommunenummer(Long kommunenummer) {
        this.kommunenummer = kommunenummer;
    }

    public Long getMeter() {
        return meter;
    }

    public void setMeter(Long meter) {
        this.meter = meter;
    }

    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @Column(name="REFLINK_IDENT")
    public Long getReflinkIdent() {
        return reflinkIdent;
    }

    public void setReflinkIdent(Long reflinkIdent) {
        this.reflinkIdent = reflinkIdent;
    }

    @Column(name="REFLINK_POSISJON")
    public Long getReflinkPosisjon() {
        return reflinkPosisjon;
    }

    public void setReflinkPosisjon(Long reflinkPosisjon) {
        this.reflinkPosisjon = reflinkPosisjon;
    }

    @Column(name="SNAPPET_X")
    public Double getSnappetX() {
        return snappetX;
    }

    public void setSnappetX(Double snappetX) {
        this.snappetX = snappetX;
    }

    @Column(name="SNAPPET_Y")
    public Double getSnappetY() {
        return snappetY;
    }

    public void setSnappetY(Double snappetY) {
        this.snappetY = snappetY;
    }

    public String getVeikategori() {
        return veikategori;
    }

    public void setVeikategori(String veikategori) {
        this.veikategori = veikategori;
    }

    public Long getVeinummer() {
        return veinummer;
    }

    public void setVeinummer(Long veinummer) {
        this.veinummer = veinummer;
    }

    public String getVeistatus() {
        return veistatus;
    }

    public void setVeistatus(String veistatus) {
        this.veistatus = veistatus;
    }
    
    @OneToOne
    @JoinColumns({
        @JoinColumn(name = "DFU_ID", referencedColumnName = "DFU_ID", nullable = false, insertable = true, updatable = true),
        @JoinColumn(name = "GMT_TIDSPUNKT", referencedColumnName = "GMT_TIDSPUNKT", nullable = false, insertable = true, updatable = true)
    })
    public Prodpunkt getProdpunkt() {
        return prodpunkt;
    }

    public void setProdpunkt(Prodpunkt prodpunkt) {
        this.prodpunkt = prodpunkt;
        
        if(prodpunkt != null) {
            setDfuId(prodpunkt.getDfuId());
            setGmtTidspunkt(prodpunkt.getGmtTidspunkt());
        } else {
            setDfuId(null);
            setGmtTidspunkt(null);
        }
    }
    @Transient
    public Date getCetTidspunkt(){
    	Calendar c = new GregorianCalendar(TimeZone.getTimeZone("CET"));
    	c.setTime(gmtTidspunkt);
    	int d = c.get(Calendar.DAY_OF_MONTH);
    	int m = c.get(Calendar.MONTH);
    	int y = c.get(Calendar.YEAR);
    	int hh = c.get(Calendar.HOUR_OF_DAY);
    	int mm = c.get(Calendar.MINUTE);
    	int ss = c.get(Calendar.SECOND);
    	int ms = c.get(Calendar.MILLISECOND);
    	Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    	cal.set(Calendar.DAY_OF_MONTH, d);
    	cal.set(Calendar.MONTH, m);
    	cal.set(Calendar.YEAR, y);
    	cal.set(Calendar.HOUR_OF_DAY, hh);
    	cal.set(Calendar.MINUTE, mm);
    	cal.set(Calendar.SECOND, ss);
    	cal.set(Calendar.MILLISECOND, ms);
		
    	return cal.getTime();
    }
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("snappetX", snappetX).
            append("snappetY", snappetY).
            append("reflinkIdent", reflinkIdent).
            append("reflinkPosisjon", reflinkPosisjon).
            append("fylkesnummer", fylkesnummer).
            append("kommunenummer", kommunenummer).
            append("veikategori", veikategori).
            append("veistatus", veistatus).
            append("veinummer", veinummer).
            append("hp", hp).
            append("meter", meter).
            append("avstandForrigePunkt", avstandForrigePunkt).
            append("opprettetDato", opprettetDato).
            toString();
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Prodpunktinfo)) {return false;}
        
        Prodpunktinfo other = (Prodpunktinfo) o;
        return new EqualsBuilder().append(dfuId, other.getDfuId()).append(gmtTidspunkt,other.getGmtTidspunkt()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(901,433).append(dfuId).append(gmtTidspunkt).toHashCode();
    }
}
