package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.applikasjon.Modul;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "BRUKER_TILGANG")
@IdClass(BrukerTilgangPK.class)
@NamedQueries( {
		@NamedQuery(name = BrukerTilgang.QUERY_BRUKERTILGANG_GET_FOR_USER, query = "SELECT bt FROM BrukerTilgang bt where LOWER(bt.signatur) = :signatur"),
		@NamedQuery(name = BrukerTilgang.QUERY_BRUKERTILGANG_DELETE_FOR_USER, query = "DELETE FROM BrukerTilgang bt where LOWER(bt.signatur) = :signatur") })
public class BrukerTilgang implements Serializable, Comparable<BrukerTilgang> {
	public static final String QUERY_BRUKERTILGANG_DELETE_FOR_USER = "BrukerTilgang.deleteForUser";
	public static final String QUERY_BRUKERTILGANG_GET_FOR_USER = "BrukerTilgang.getForUser";
	private Bruker bruker;
	private String endretAv;
	private Date endretDato;
	private Modul modul;
	private Long modulId;
	private String opprettetAv;
	private Date opprettetDato;
	private String signatur;
	private Tilgangsnivaa tilgangsnivaa;

	public BrukerTilgang() {
	}

	/** {@inheritDoc} */
	public int compareTo(BrukerTilgang o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(getTilgangsnivaa(), o.getTilgangsnivaa()).append(getBruker(),
				o.getBruker()).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof BrukerTilgang)) {
			return false;
		}
		if (o == this) {
			return true;
		}

		BrukerTilgang other = (BrukerTilgang) o;

		return new EqualsBuilder().append(signatur, other.getSignatur()).append(modulId, other.getModulId()).isEquals();
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "SIGNATUR", referencedColumnName = "SIGNATUR")
	public Bruker getBruker() {
		return bruker;
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "MODUL_ID", referencedColumnName = "ID")
	public Modul getModul() {
		return modul;
	}

	@Id
	@Column(name = "MODUL_ID", nullable = false, insertable = false, updatable = false)
	public Long getModulId() {
		return modulId;
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@Id
	@Column(nullable = false, insertable = false, updatable = false)
	public String getSignatur() {
		return signatur;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "NIVAA", referencedColumnName = "NIVAA")
	public Tilgangsnivaa getTilgangsnivaa() {
		return tilgangsnivaa;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(199, 93).append(signatur).append(modulId).toHashCode();
	}

	public void setBruker(Bruker bruker) {
		this.bruker = bruker;

		if (bruker != null) {
			setSignatur(bruker.getSignatur());
		} else {
			setSignatur(null);
		}
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	public void setModul(Modul modul) {
		this.modul = modul;

		if (modul != null) {
			setModulId(modul.getId());
		} else {
			setModulId(null);
		}
	}

	public void setModulId(Long modulId) {
		this.modulId = modulId;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}

	public void setTilgangsnivaa(Tilgangsnivaa tilgangsnivaa) {
		this.tilgangsnivaa = tilgangsnivaa;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("signatur", signatur).append("modulId", modulId).append("opprettetAv",
				opprettetAv).append("opprettetDato", opprettetDato).toString();
	}
}
