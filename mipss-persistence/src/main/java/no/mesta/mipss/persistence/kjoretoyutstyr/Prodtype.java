package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "prodtypeIdSeq", sequenceName = "PRODTYPE_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = Prodtype.FIND_ALL, query = "select o from Prodtype o")
})

@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)

public class Prodtype extends MipssEntityBean<Prodtype> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
    public static final String FIND_ALL = "Prodtype.findAll";
    private Boolean broyteFlagg;
    private Boolean intensjonFlagg;
    private Boolean dauFlagg;
    private String epostTekst;
    private String fritekst;
    private String guiFarge;
    private Long guiRekkefolge;
    private Long id;
    private String navn;
    private Boolean stroFlagg;
    private Sesong sesong;
    private List<Utstyrgruppe> utstyrgruppeList = new ArrayList<Utstyrgruppe>();
    
    // h�ndskrevet felt
    private OwnedMipssEntity ownedMipssEntity;

    public Prodtype() {
    }

    /**
     * For testing purposes
     */
    public Prodtype(Long id, String navn, Boolean dauFlagg) {
        this(id, navn, dauFlagg, null, null);
    }

    /**
     * For testing purposes
     */
    public Prodtype(Long id, String navn, Boolean dauFlagg, Boolean stroFlagg, Boolean broyteFlagg) {
        this.id = id;
        this.navn = navn;
        this.dauFlagg = dauFlagg;
        this.stroFlagg = stroFlagg;
        this.broyteFlagg = broyteFlagg;
    }

    @Column(name="DAU_BROYTE_FLAGG", nullable = false)
    public Boolean getBroyteFlagg() {
    	if(broyteFlagg == null) {
    		broyteFlagg = false;
    	}
        return broyteFlagg;
    }

    public void setBroyteFlagg(Boolean broyteFlagg) {
    	Boolean old = this.broyteFlagg;
        this.broyteFlagg = broyteFlagg;
        firePropertyChange("broyteFlagg", old, broyteFlagg);
    }
    
    @Column(name="INTENSJON_FLAGG", nullable = false)
    public Boolean getIntensjonFlagg() {
    	if(intensjonFlagg == null) {
    		intensjonFlagg = false;
    	}
        return intensjonFlagg;
    }

    public void setIntensjonFlagg(Boolean intensjonFlagg) {
    	Boolean old = this.intensjonFlagg;
        this.intensjonFlagg = intensjonFlagg;
        firePropertyChange("intensjonFlagg", old, intensjonFlagg);
    }

    @Column(name="DAU_FLAGG", nullable = false)
    public Boolean getDauFlagg() {
    	if(dauFlagg == null) {
    		dauFlagg = false;
    	}
        return dauFlagg;
    }

    public void setDauFlagg(Boolean dauFlagg) {
    	Boolean old = this.dauFlagg;
        this.dauFlagg = dauFlagg;
        firePropertyChange("dauFlagg", old, dauFlagg);
    }

    @Column(name="EPOST_TEKST", nullable = false)
    public String getEpostTekst() {
        return epostTekst;
    }

    public void setEpostTekst(String epostTekst) {
    	String old = this.epostTekst;
        this.epostTekst = epostTekst;
        firePropertyChange("epostTekst", old, epostTekst);
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Column(name="GUI_FARGE")
    public String getGuiFarge() {
        return guiFarge;
    }

    public void setGuiFarge(String guiFarge) {
    	String old = this.guiFarge;
        this.guiFarge = guiFarge;
        firePropertyChange("guiFarge", old, guiFarge);
    }

    @Column(name="GUI_REKKEFOLGE", nullable = false)
    public Long getGuiRekkefolge() {
        return guiRekkefolge;
    }

    public void setGuiRekkefolge(Long guiRekkefolge) {
    	Long old = this.guiRekkefolge;
        this.guiRekkefolge = guiRekkefolge;
        firePropertyChange("guiRekkefolge", old, guiRekkefolge);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prodtypeIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="STRO_FLAGG", nullable = false)
    public Boolean getStroFlagg() {
    	if(stroFlagg == null) {
    		stroFlagg = false;
    	}
        return stroFlagg;
    }

    public void setStroFlagg(Boolean stroFlagg) {
    	Boolean old = this.stroFlagg;
        this.stroFlagg = stroFlagg;
        firePropertyChange("stroFlagg", old, stroFlagg);
    }

    @ManyToOne
    @JoinColumn(name = "SESONG", referencedColumnName = "SESONG")
    public Sesong getSesong() {
        return sesong;
    }

    public void setSesong(Sesong sesong) {
    	Sesong old = this.sesong;
        this.sesong = sesong;
        firePropertyChange("sesong", old, sesong);
    }

    @OneToMany(mappedBy = "prodtype")
    public List<Utstyrgruppe> getUtstyrgruppeList() {
        return utstyrgruppeList;
    }

    public void setUtstyrgruppeList(List<Utstyrgruppe> utstyrgruppeList) {
        this.utstyrgruppeList = utstyrgruppeList;
    }

    public Utstyrgruppe addUtstyrgruppe(Utstyrgruppe utstyrgruppe) {
        getUtstyrgruppeList().add(utstyrgruppe);
        utstyrgruppe.setProdtype(this);
        return utstyrgruppe;
    }

    public Utstyrgruppe removeUtstyrgruppe(Utstyrgruppe utstyrgruppe) {
        getUtstyrgruppeList().remove(utstyrgruppe);
        utstyrgruppe.setProdtype(null);
        return utstyrgruppe;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("navn", getNavn()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Prodtype)) {
            return false;
        }
        Prodtype other = (Prodtype) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            append(getNavn()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Prodtype other = (Prodtype) o;
            return new CompareToBuilder().
                append(getNavn(), other.getNavn()).
                toComparison();
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
}
