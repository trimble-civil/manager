package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Beskriver et tilgangspunkt i databasen
 * 
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Tilgangspunkt.QUERY_FIND_ALL, query = "SELECT t FROM Tilgangspunkt t"),
    @NamedQuery(name = Tilgangspunkt.QUERY_FIND_MESSAGE_POINT, query = "SELECT t FROM Tilgangspunkt t WHERE t.klassenavn = :target AND t.meldingType = :message"),
    @NamedQuery(name = Tilgangspunkt.QUERY_FIND_MESSAGE_POINT_NULL, query = "SELECT t FROM Tilgangspunkt t WHERE t.klassenavn = :target AND t.meldingType is null")
})
public class Tilgangspunkt extends MipssEntityBean<Tilgangspunkt> implements Serializable {
    public static final String QUERY_FIND_ALL = "Tilgangspunkt.findAll";
    public static final String QUERY_FIND_MESSAGE_POINT = "Tilgangspunkt.finnMeldingsPunkt";
    public static final String QUERY_FIND_MESSAGE_POINT_NULL = "Tilgangspunkt.finnMeldingsPunktNull";
    
    private String endretAv;
    private Date endretDato;
    private Boolean fellesFlagg;
    private Long id;
    private String klassenavn;
    private String opprettetAv;
    private Date opprettetDato;
    private List<Menypunkt> menypunktList = new ArrayList<Menypunkt>();
    private Modul modul;
    private String meldingType;

    /**
     * Konstrukt�r
     * 
     */
    public Tilgangspunkt() {
    }

    /**
     * Hvem endret punktet sist
     * 
     * @return
     */
    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }
    
    /**
     * Hvem endret punktet sist
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
        String old = this.endretAv;
    	this.endretAv = endretAv;
    	firePropertyChange("endretAv", old, endretAv);
    }
    
	/** {@inheritDoc} */
	public String getTextForGUI() {
		return String.valueOf(id);
	}

    /**
     * N�r ble den endret
     * 
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    /**
     * N�r ble den endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
    	Date old = this.endretDato;
        this.endretDato = endretDato;
        firePropertyChange("endretDato", old, endretDato);
    }

    /**
     * 
     * @return
     */
    @Column(name="FELLES_FLAGG", nullable = false)
    public Boolean getFellesFlagg() {
        return fellesFlagg;
    }

    /**
     * 
     * @param fellesFlagg
     */
    public void setFellesFlagg(Boolean fellesFlagg) {
    	Boolean old = this.fellesFlagg;
        this.fellesFlagg = fellesFlagg;
        firePropertyChange("fellesFlagg", old, fellesFlagg);
    }

    /**
     * Unik id for punktet
     * 
     */
    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Unik id for punktet
     * 
     * @param id
     */
    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    /**
     * Klassen til punktet
     * 
     * @return
     */
    @Column(nullable = false)
    public String getKlassenavn() {
        return klassenavn;
    }

    /**
     * Klassen til punktet
     * 
     * @param klassenavn
     */
    public void setKlassenavn(String klassenavn) {
        String old = this.klassenavn;
    	this.klassenavn = klassenavn;
    	firePropertyChange("klassenavn", old, klassenavn);
    }

    /**
     * Hvem opprettet det
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Hvem opprettet det
     * 
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        String old = this.opprettetAv;
    	this.opprettetAv = opprettetAv;
    	firePropertyChange("opprettetAv", old, opprettetAv);
    }

    /**
     * N�r ble den opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r ble den opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
    	Date old = this.opprettetDato;
        this.opprettetDato = opprettetDato;
        firePropertyChange("opprettetDato", old, opprettetDato);
    }

    /**
     * Listen over menyer som benytter den
     * 
     * @return
     */
    @OneToMany(mappedBy = "tilgangspunkt")
    public List<Menypunkt> getMenypunktList() {
        return menypunktList;
    }

    /**
     * Listen over menyer som benytter den
     * 
     * @param menypunktList
     */
    public void setMenypunktList(List<Menypunkt> menypunktList) {
        this.menypunktList = menypunktList;
    }

    public Menypunkt addMenypunkt(Menypunkt menypunkt) {
        getMenypunktList().add(menypunkt);
        menypunkt.setTilgangspunkt(this);
        return menypunkt;
    }

    public Menypunkt removeMenypunkt(Menypunkt menypunkt) {
        getMenypunktList().remove(menypunkt);
        menypunkt.setTilgangspunkt(null);
        return menypunkt;
    }

    /**
     * Modulen dette er et tilgangspunkt for
     * 
     * @return
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MODUL_ID", referencedColumnName = "ID")
    public Modul getModul() {
        return modul;
    }
    
    /**
     * Modulen dette er et tilgangspunkt for
     * 
     * @param modulversjon
     */
    public void setModul(Modul modul) {
    	Modul old = this.modul;
        this.modul = modul;
        firePropertyChange("modul", old, modul);
    }

    /**
     * Meldingen som skal benyttes
     * 
     * @param meldingType
     */
    public void setMeldingType(String meldingType) {
    	String old = this.meldingType;
        this.meldingType = meldingType;
        firePropertyChange("meldingType", old, meldingType);
    }
    
    /**
     * Klasse som benyttes som melding til plugin
     * 
     * @return
     */
    @Column(name="MELDING_TYPE", nullable = true, length = 255)
    public String getMeldingType() {
        return meldingType;
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2001,2099).
            append(id).toHashCode();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Tilgangspunkt)) { return false;}
        
        Tilgangspunkt other = (Tilgangspunkt) o;
        
        return new EqualsBuilder().
            append(id, other.getId()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#toString
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("klassenavn", klassenavn).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            append("fellesFlagg", fellesFlagg).
            append("modul", modul).
            toString();
    }
}
