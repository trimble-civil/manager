package no.mesta.mipss.persistence;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Klasse for å hente relasjoner til en entitet
 * @author harkul
 *
 */
public class EagerFetcher {

	/**
	 * Henter ut de relasjonene som er markert med MipssFetch.
	 * 
	 * @param o entiteten som skal eager-fetches
	 */
	public static void fetch(Object o){
		List<Val> tst = new ArrayList<Val>();
		for (Method m:o.getClass().getMethods()){
			tst.addAll(getMethodsWithAnnotation(m));
		}
		for (Val v:tst){
			invoke(v.getGetter(), o, v.getMethod());
		}
	}
	
	/**
	 * Henter relasjoner for alle attributtene, standard kall til methoden size
	 * @param o entiteten som skal eagerfetches
	 * @param properties attributtene til entiteten
	 */
	public static void fetch(Object o, String... properties){
		for (String s:properties)	
			invoke(getMethodForProperty(o, s), o, "size");
	}
	/**
	 * Henter relasjoner for alle attributtesne
	 * @param o entiteten som skal eager-fetches
	 * @param propMeth key value der key er attributtnavn og value er metoden som kalles på objektet som returneres
	 * av kall til getter for attributt
	 */
	public static void fetch(Object o, Map<String, String> propMeth){
		for (String key:propMeth.keySet()){
			invoke(getMethodForProperty(o, key), 0, propMeth.get(key));
		}
	}
	
	private static Method getMethodForProperty(Object o, String property){
		try {
			Method m = o.getClass().getMethod(getGetter(property), null);
			return m;
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
	private static Method getMethod(Object o, String method){
		try {
			Method m = o.getClass().getMethod(method, null);
			return m;
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static String getGetter(String property){
		String methodName = "get"+property.substring(0,1).toUpperCase()+property.substring(1);
		return methodName;
	}
	/**
	 * Påkaller metodene for å tvinge en eager fetch av relasjonene
	 * @param m Metoden som skal påkalles for å hente første objekt
	 * @param instance instansen der metoden skal kalles
	 * @param method metoden som skal kalles på objektet fra forrige kall
	 */
	private static void invoke(Method m, Object instance, String method){
		try {
			Object o = m.invoke(instance);
			if (o!=null){
				Method met = o.getClass().getMethod(method);
				met.invoke(o);
			}
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("invoke failed", e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("invoke failed", e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("invoke failed", e);
		} catch (SecurityException e) {
			throw new RuntimeException("invoke failed", e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("invoke failed", e);
		}
	}
	
	
	/**
	 * Sjekker om metoden har en MipssFetch annotasjon, 
	 * hvis den har det, hentes method fra annotasjonen og legges sammen
	 * med m i en liste
	 * @param m Metoden som skal sjekkes
	 * @return Liste av Vel objekter som inneholder referanse til Metoden som gir objektet og metoden som skal kalles på det objektet
	 */
	private static List<Val> getMethodsWithAnnotation(Method m){
		List<Val> methodList = new ArrayList<Val>();
			for (Annotation a:m.getAnnotations()){
				
				if (a.annotationType().equals(MipssFetch.class)){
					MipssFetch f = (MipssFetch)a;
					Val v = new Val(m, f.method());
					methodList.add(v);
				}
			}
		return methodList;
	}
	
	
	
	
	private static class Val{
		private Method getter;
		private String method;
		
		public Val(Method getter, String method){
			this.getter = getter;
			this.method = method;
		}
		public Method getGetter(){
			return getter;
		}
		public String getMethod(){
			return method;
		}
	}
}
