package no.mesta.mipss.persistence.rapportfunksjoner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@SuppressWarnings("serial")
public class Rapp1Object implements Rapp1Entity{
	protected Date fraDato;
	protected Date tilDato;
	protected double km;
	protected long sekund;
	protected List<Rapp1> data;
	protected Map<Long, R12Prodtype> prodtypeMap;
	protected Map<Long, R12Stroprodukt> stroproduktMap;
	protected Double mengdeM2; //Klipping
	
	private Boolean samprod;
	private String samprodFlagg;
	
	public Rapp1Object(){}
	public Rapp1Object(List<Rapp1> data){
		this.data = data;
		initData();
	}
	
	private void initData() {
		for (Rapp1 r:data){
			Date fd = r.getFraDato();
			Date td = r.getTilDato();
			
			if (fraDato == null||fd.before(fraDato)){
				fraDato = fd;
			}
			if (tilDato==null||td.after(tilDato)){
				tilDato = td;
			}
			if (r.getSamproduksjonFlagg()!=null&&r.getSamproduksjonFlagg().intValue()==1){
				samprod = true;
				samprodFlagg = "X";
			}
			km += r.getKjortKm()!=null?r.getKjortKm():0;
			sekund += r.getKjortSekund();
			
			handleProdtyper(r.getProdtyper());
			handleStroprodukter(r.getStroprodukter());
		}
	}
	/**
	 * Leverandørens navn
	 * @return
	 */
	public String getNavn(){
		return data.get(0).getLeverandorNavn();
	}
	
	public String getKjoretoy(){
		return data.get(0).getKjoretoyNavn();
	}
	public String getRodenavn(){
		return data.get(0).getRodeNavn();
	}
	public String getVei(){
		return data.get(0).getVei();
	}
	public Double getRodelengde(){
		return data.get(0).getRodelengde();
	}
	
	/**
	 * True hvis det finnes samproduksjon for denne leverandøren
	 * @return
	 */
	public Boolean getSamprod(){
		return samprod;
	}
	
	public String getSamprodFlagg(){
		return samprodFlagg;
	}
	public void setSamprodFlagg(String samprodFlagg){
		this.samprodFlagg = samprodFlagg;
	}
	public double getTid(){
		return (double)sekund/(double)86400;
	}
	/**
	 * Første loggtidspunkt for et kjøretøy tilhørende denne 
	 * @return
	 */
	public Date getFraDato(){
		return fraDato;
	}
	
	/**
	 * Siste loggtidspunkt for et kjøretøy tilhørende denne 
	 * @return
	 */
	public Date getTilDato(){
		return tilDato;
	}
	/**
	 * Totalt kjørte kilometer for denne 
	 * @return
	 */
	public Double getKm(){
		return km;
	}
	
	/**
	 * Total kjørt sekund for denne 
	 * @return
	 */
	public Long getSekund(){
		return sekund;
	}
	
	/**
	 * Total mengdeM2
	 * @return
	 */
	public Double getMengdeM2(){
		return mengdeM2;
	}

	/**
	 * Henter alle prodtypene tilhørende denne , sorterer listen basert på <code>seq</code> i R12Prodtype
	 * @return
	 */
	public List<R12Prodtype> getProdtyper(){
		List<R12Prodtype> ptList = new ArrayList<R12Prodtype>();
		Comparator<R12Prodtype> comparator = new Comparator<R12Prodtype>(){
			public int compare(R12Prodtype o1, R12Prodtype o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}
		};
		Set<Long> keySet = prodtypeMap.keySet();
		for (Long l:keySet){
			R12Prodtype r12Prodtype = prodtypeMap.get(l);
			ptList.add(r12Prodtype);
		}
		Collections.sort(ptList, comparator);
		return ptList;
	}
	/**
	 * Legger til alle prodtyper i dennes liste. De prodtypene denne ikke har utført
	 * vil legge seg til med kun header-informasjon. Listen med alle sorterte prodtyper vil bli oppdatert med de
	 * data denne evt har produsert. 
	 * @param alleSortert
	 */
	public void organizeProdtyper(List<R12Prodtype> alleSortert){
		List<R12Prodtype> pList = new ArrayList<R12Prodtype>();
		for (R12Prodtype a:alleSortert){
			R12Prodtype r12Prodtype = prodtypeMap.get(a.getId());
			if (r12Prodtype==null){
				r12Prodtype = createEmpty(a);
			}else{
				a.addKjortKm(r12Prodtype.getKjortKm());
				a.addKjortSekund(r12Prodtype.getKjortSekund());
				a.addMengdeM2(r12Prodtype.getMengdeM2()); //KLIPPING
			}
			pList.add(r12Prodtype);
		}
	}
	
	/**
	 * Legger til alle strøprodukter i dennes liste. De strøprodukter denne ikke har lagt ut,
	 * vil legge seg til med kun header-informasjon. Listen med alle sorterte strøprodukter vil bli oppdatert med de
	 * data denne evt har produsert. 
	 * @param alleSortert
	 */
	public void organizeStroprodukt(List<R12Stroprodukt> alleSortert){
		List<R12Stroprodukt> sList = new ArrayList<R12Stroprodukt>();
		for (R12Stroprodukt s:alleSortert){
			R12Stroprodukt stro = stroproduktMap.get(s.getId());
			if (stro==null){
				stro = createEmpty(s);
			}else{
				s.addKjortKm(stro.getKjortKm());
				s.addKjortSekund(stro.getKjortSekund());
				s.addMengdeM3(stro.getMengdeM3());
				s.addMengdeTonn(stro.getMengdeTonn());
			}
			sList.add(stro);
		}
	}
	
	/**
	 * Lager en placeholder prodtype, med kun header-informasjon og uten data
	 * @param pt
	 * @return
	 */
	private R12Prodtype createEmpty(R12Prodtype pt){
		R12Prodtype p = new R12Prodtype();
		p.setHeader(pt.getHeader());
		p.setId(pt.getId());
		p.setSeq(pt.getSeq());
		return p;
	}
	/**
	 * Lager et placeholder strøprodukt, med kun header-informasjon og uten data.
	 * @param st
	 * @return
	 */
	private R12Stroprodukt createEmpty(R12Stroprodukt st){
		R12Stroprodukt s = new R12Stroprodukt();
		s.setHeader(st.getHeader());
		s.setId(st.getId());
		s.setSeq(st.getSeq());
		return s;
	}
	/**
	 * Henter alle stroproduktene tilhørende denne, sorterer listen basert på seq i R12Stroprodukt
	 * @return
	 */
	public List<R12Stroprodukt> getStroprodukter(){
		List<R12Stroprodukt> stList = new ArrayList<R12Stroprodukt>();
		Comparator<R12Stroprodukt> comparator = new Comparator<R12Stroprodukt>(){
			public int compare(R12Stroprodukt o1, R12Stroprodukt o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}
		};
		Set<Long> keySet = stroproduktMap.keySet();
		for (Long l:keySet){
			stList.add(stroproduktMap.get(l));
		}
		Collections.sort(stList, comparator);
		return stList;
	}
	/**
	 * Returnerer en komplett liste over samtlige strøprodukter for gjeldende rapport der denne
	 * utførte strøprodukter er fylt med data. De strøprodukter denne ikke har produsert leveres med
	 * header-informasjon
	 * @return
	 */
//	public List<R12Stroprodukt> getStroproduktList(){
//		return stroproduktList;
//	}
	
	/**
	 * Rådataene for denne leverandøren
	 * @return
	 */
	public List<Rapp1> getData(){
		return data;
	}
	/**
	 * Slår sammen totaler for prodtypene for denne
	 * @param ptList
	 */
	protected void handleProdtyper(List<R12Prodtype> ptList){
		if (prodtypeMap==null){
			prodtypeMap = new HashMap<Long, R12Prodtype>();
		}
		if (ptList!=null&&ptList.size()>0){
			for (R12Prodtype pt:ptList){
				R12Prodtype p = prodtypeMap.get(pt.getId());
				if (p==null){
					prodtypeMap.put(pt.getId(), pt.clone());
				}else{
					p.addKjortKm(pt.getKjortKm());
					p.addKjortSekund(pt.getKjortSekund());
					p.addMengdeM2(pt.getMengdeM2()); // KLIPPING
					//må slå sammen summene for sub-prodtyper
					if (pt.getProdtyper()!=null){
						for (R12Prodtype subtype:pt.getProdtyper()){
							p.mergeProdtype(subtype);
						}
					}
					
				}
				
			}
		}
	}
	
	/**
	 * Slår sammen totaler for strøproduktene for denne
	 * @param ptList
	 */
	protected void handleStroprodukter(List<R12Stroprodukt> ptList){
		if (stroproduktMap==null){
			stroproduktMap= new HashMap<Long, R12Stroprodukt>();
		}
		if (ptList!=null&&ptList.size()>0){
			for (R12Stroprodukt pt:ptList){
				R12Stroprodukt p = stroproduktMap.get(pt.getId());
				if (p==null){
					stroproduktMap.put(pt.getId(), pt.clone());
				}else{
					p.addKjortKm(pt.getKjortKm());
					p.addKjortSekund(pt.getKjortSekund());
					p.addMengdeM3(pt.getMengdeM3());
					p.addMengdeTonn(pt.getMengdeTonn());
				}
			}
		}
	}
	public List<Materiale> getMaterialer() {
		return null;
	}
	public Boolean getPaaKontrakt() {
		if (data!=null&&data.get(0)!=null){
			Long paKontraktFlagg = data.get(0).getPaKontraktFlagg();
			if (paKontraktFlagg!=null){
				return paKontraktFlagg.intValue()==1;
			}
		}
		return Boolean.FALSE;
	}
}
