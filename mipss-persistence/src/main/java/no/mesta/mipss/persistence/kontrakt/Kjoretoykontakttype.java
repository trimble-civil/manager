package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Kjoretoykontakttype.FIND_ALL, query = "select o from Kjoretoykontakttype o order by o.navn")

public class Kjoretoykontakttype extends MipssEntityBean<Kjoretoykontakttype> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kjoretoykontakttype.findAll";
    private String fritekst;
    private String navn;
    private Boolean kunEnKontaktFlagg;
    
    public Kjoretoykontakttype() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }
    
    @Column(name = "KUN_EN_KONTAKT_FLAGG")
    public Boolean getKunEnKontaktFlagg() {
		return kunEnKontaktFlagg;
	}

	public void setKunEnKontaktFlagg(Boolean kunEnKontaktFlagg) {
		this.kunEnKontaktFlagg = kunEnKontaktFlagg;
	}

	// de neste metodene er håndkodet
    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kjoretoykontakttype)) {
            return false;
        }
        Kjoretoykontakttype other = (Kjoretoykontakttype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert på Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,13).
            append(getNavn()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Kjoretoykontakttype other = (Kjoretoykontakttype) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
