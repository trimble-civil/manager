package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

public class ActivityRodeInfo {

    private Integer vehicleId;
    private Integer rodeId;
    private String winterClass;
    private Integer countyNumber;
    private String roadType;
    private Integer prodTypeId;
    private Integer gritProdId;
    private Boolean gritFlag;
    private Boolean plowFlag;
    private Date fromDateTime;
    private Date toDateTime;
    private Double distanceDriven;
    private Double prodStretchDistanceDriven;
    private Long measureId;
    private boolean coproduction;

    public static String[] getPropertiesArray() {
        return new String[] {
                "vehicleId",
                "rodeId",
                "winterClass",
                "countyNumber",
                "roadType",
                "prodTypeId",
                "gritProdId",
                "gritFlag",
                "plowFlag",
                "fromDateTime",
                "toDateTime",
                "distanceDriven",
                "prodStretchDistanceDriven",
                "measureId"
        };
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Integer getRodeId() {
        return rodeId;
    }

    public void setRodeId(Integer rodeId) {
        this.rodeId = rodeId;
    }

    public String getWinterClass() {
        return winterClass;
    }

    public void setWinterClass(String winterClass) {
        this.winterClass = winterClass;
    }

    public Integer getCountyNumber() {
        return countyNumber;
    }

    public void setCountyNumber(Integer countyNumber) {
        this.countyNumber = countyNumber;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public Integer getProdTypeId() {
        return prodTypeId;
    }

    public void setProdTypeId(Integer prodTypeId) {
        this.prodTypeId = prodTypeId;
    }

    public Integer getGritProdId() {
        return gritProdId;
    }

    public void setGritProdId(Integer gritProdId) {
        this.gritProdId = gritProdId;
    }

    public Boolean getGritFlag() {
        return gritFlag;
    }

    public void setGritFlag(Boolean gritFlag) {
        this.gritFlag = gritFlag;
    }

    public Boolean getPlowFlag() {
        return plowFlag;
    }

    public void setPlowFlag(Boolean plowFlag) {
        this.plowFlag = plowFlag;
    }

    public Date getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public Date getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }

    public Double getDistanceDriven() {
        return distanceDriven;
    }

    public void setDistanceDriven(Double distanceDriven) {
        this.distanceDriven = distanceDriven;
    }

    public Double getProdStretchDistanceDriven() {
        return prodStretchDistanceDriven;
    }

    public void setProdStretchDistanceDriven(Double prodStretchDistanceDriven) {
        this.prodStretchDistanceDriven = prodStretchDistanceDriven;
    }

    public Long getMeasureId() {
        return measureId;
    }

    public void setMeasureId(Long measureId) {
        this.measureId = measureId;
    }

    public boolean isCoproduction() {
        return coproduction;
    }

    public void setCoproduction(boolean coproduction) {
        this.coproduction = coproduction;
    }

    @Override
    public String toString() {
        return "ActivityRodeInfo: \nRodeId: " + rodeId + "\nWinter class: " + winterClass + "\nCount number: " + countyNumber + "\nRoad type: " + roadType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivityRodeInfo that = (ActivityRodeInfo) o;

        if (!rodeId.equals(that.rodeId)) return false;
        if (winterClass != null ? !winterClass.equals(that.winterClass) : that.winterClass != null) return false;
        if (countyNumber != null ? !countyNumber.equals(that.countyNumber) : that.countyNumber != null) return false;
        return roadType != null ? roadType.equals(that.roadType) : that.roadType == null;

    }

    @Override
    public int hashCode() {
        int result = rodeId.hashCode();
        result = 31 * result + (winterClass != null ? winterClass.hashCode() : 0);
        result = 31 * result + (countyNumber != null ? countyNumber.hashCode() : 0);
        result = 31 * result + (roadType != null ? roadType.hashCode() : 0);
        return result;
    }

}