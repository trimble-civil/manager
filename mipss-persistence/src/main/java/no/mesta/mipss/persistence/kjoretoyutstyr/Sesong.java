package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Sesong.FIND_ALL, query = "select o from Sesong o")
})
public class Sesong extends MipssEntityBean<Sesong> implements Serializable, IRenderableMipssEntity {
    public static final String FIND_ALL = "Sesong.findAll";
    
    private String sesong;
    private List<Prodtype> prodtypeList = new ArrayList<Prodtype>();

    public Sesong() {
    }

    @Id
    @Column(nullable = false)
    public String getSesong() {
        return sesong;
    }

    public void setSesong(String sesong) {
    	String old = this.sesong;
        this.sesong = sesong;
        firePropertyChange("sesong", old, sesong);
    }

    @OneToMany(mappedBy = "sesong")
    public List<Prodtype> getProdtypeList() {
        return prodtypeList;
    }

    public void setProdtypeList(List<Prodtype> prodtypeList) {
        this.prodtypeList = prodtypeList;
    }

    public Prodtype addProdtype(Prodtype prodtype) {
        getProdtypeList().add(prodtype);
        prodtype.setSesong(this);
        return prodtype;
    }

    public Prodtype removeProdtype(Prodtype prodtype) {
        getProdtypeList().remove(prodtype);
        prodtype.setSesong(null);
        return prodtype;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("sesong", getSesong()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Sesong)) {
            return false;
        }
        Sesong other = (Sesong) o;
        return new EqualsBuilder().
            append(getSesong(), other.getSesong()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getSesong()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getSesong();
    }

    public int compareTo(Object o) {
        Sesong other = (Sesong) o;
            return new CompareToBuilder().
                append(getSesong(), other.getSesong()).
                toComparison();
    }
}
