package no.mesta.mipss.persistence.kontrakt;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.valueobjects.Epostadresse;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
@Table(name = "KONTRAKT_LEVERANDOR")
@IdClass(KontraktLeverandorPK.class)
@NamedQueries({
    @NamedQuery(name = KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT, query = "SELECT o FROM KontraktLeverandor o where o.kontraktId = :id"),
    @NamedQuery(name = KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT_LEVERANDOR, query = "SELECT o FROM KontraktLeverandor o where o.kontraktId = :id and o.leverandorNr=:leverandorNr")
})
public class KontraktLeverandor extends MipssEntityBean<KontraktLeverandor> implements Serializable, IRenderableMipssEntity, MipssEpostEntity {
	public static final String QUERY_FIND_FOR_KONTRAKT = "KontraktLeverandor.findForKontrakt";
	public static final String QUERY_FIND_FOR_KONTRAKT_LEVERANDOR = "KontraktLeverandor.findForKontraktLeverandor";
	private static final String[] ID_NAMES = new String[] {"kontraktId", "leverandorNr", "fraDato"}; 
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	private Long kontraktId;
	private String leverandorNr;
	private Date fraDato;
	private Date tilDato;
	private String epostAdresse;
	private String kopiEpostAdresse;
	private Long tlfnummer;
	private Long sendeEpostFlagg;
	
    private String fritekst;
    
	private Leverandor leverandor;
	private Driftkontrakt driftkontrakt;
	
	private Boolean ekstraDagsrapport;
	private OwnedMipssEntity ownedMipssEntity;
	private Integer number;
	
	private List<Levkontrakt> levkontraktList = new ArrayList<Levkontrakt>();
	
	/** Ikke en del av entiteten */
	private Date inaktivDato;
	private Long antallKjoretoy;
	
	public KontraktLeverandor() {
	}
	@Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    } 
	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
	public void setSendeEpost(Boolean epostFlagg) {
		Boolean old = getSendeEpost();
		boolean vetoed = false;
		try {
			fireVetoableChange("sendeEpost", old, epostFlagg);
		} catch (PropertyVetoException e) {
			vetoed = true;
		}
		
		if (!vetoed){
			if(epostFlagg != null && epostFlagg) {
				setSendeEpostFlagg(1L);
			} else {
				setSendeEpostFlagg(0L);
			}
			firePropertyChange("sendeEpost", old, epostFlagg);
		}
	}
	
	@Transient
	public Boolean getSendeEpost() {
		return sendeEpostFlagg != null && sendeEpostFlagg == 1L;
	}
	
	@Id
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	@Id
	@Column(name = "LEVERANDOR_NR", nullable = false, insertable = false, updatable = false)
	public String getLeverandorNr() {
		return leverandorNr;
	}

	public void setLeverandorNr(String leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	@Id
	@Column(name = "FRA_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFraDato() {
		return fraDato;
	}

	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	
	/**
	 * Streng-representasjon av fraDato. Kan bindes til JTextField i ELProperty med flere ledd
	 * @return
	 */
	@Transient
	public String getFraDatoString() {
		return fraDato != null ? dateFormat.format(fraDato) : "";
	}
	
	@Column(name = "TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTilDato() {
		return tilDato;
	}

	public void setTilDato(Date tilDato) {
		Object oldValue = this.tilDato;
		this.tilDato = tilDato;
		getProps().firePropertyChange("tilDato", oldValue, tilDato);
	}

	@Column(name = "EPOST_ADRESSE")
	public String getEpostAdresse() {
		return epostAdresse;
	}

	@Transient
	public Epostadresse getEpost() {
		if(epostAdresse != null) {
			return new Epostadresse(epostAdresse);
		} else {
			return null;
		}
	}
	
	public void setEpostAdresse(String epostAdresse) {
		if (epostAdresse!=null&&epostAdresse.equals(""))
			epostAdresse=null;
		Object oldEpost = getEpost();
		Object oldValue = this.epostAdresse;
		this.epostAdresse = epostAdresse;
		getProps().firePropertyChange("epostAdresse", oldValue, epostAdresse);
		getProps().firePropertyChange("epost", oldEpost, getEpost());
	}

	@Column(name = "KOPI_EPOST_ADRESSE")
	public String getKopiEpostAdresse() {
		return kopiEpostAdresse;
	}

	@Transient
	public Epostadresse getKopiEpost() {
		if(kopiEpostAdresse != null) {
			return new Epostadresse(kopiEpostAdresse);
		} else {
			return null;
		}
	}
	
	public void setKopiEpostAdresse(String kopiEpostAdresse) {
		if (kopiEpostAdresse!=null&&kopiEpostAdresse.equals(""))
			kopiEpostAdresse=null;
		Object oldKopiEpost = getKopiEpost();
		Object oldValue = this.kopiEpostAdresse;
		this.kopiEpostAdresse = kopiEpostAdresse;
		getProps().firePropertyChange("kopiEpostAdresse", oldValue, kopiEpostAdresse);
		getProps().firePropertyChange("kopiEpost", oldKopiEpost, getKopiEpost());
	}

	@Column(name = "SENDE_EPOST_FLAGG")
	public Long getSendeEpostFlagg() {
		return sendeEpostFlagg;
	}

	public void setSendeEpostFlagg(Long sendeEpostFlagg) {
		Long oldValue = this.sendeEpostFlagg;
		this.sendeEpostFlagg = sendeEpostFlagg;
		firePropertyChange("sendeEpostFlagg", oldValue, ekstraDagsrapport);
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}

	@ManyToOne(cascade=CascadeType.MERGE)//(cascade={CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "LEVERANDOR_NR", referencedColumnName = "NR", nullable = false)
	public Leverandor getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(Leverandor leverandor) {
		this.leverandor = leverandor;
        
        if(leverandor != null) {
            setLeverandorNr(leverandor.getNr());
        } else {
        	setLeverandorNr(null);
        }
	}

	@ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
		
		if(driftkontrakt != null) {
			setKontraktId(driftkontrakt.getId());
		} else {
			setKontraktId(null);
		}
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		String ret = driftkontrakt != null ? driftkontrakt.getKontraktnavn() + ", " : "";
		ret += leverandor != null ? leverandor.getNavn() : "";
		return ret;
	}
	
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3,5).
            append(kontraktId).
            append(leverandorNr).
            append(fraDato).
            toHashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof KontraktLeverandor)) {return false;}
        if(o == this) { return true;}
        
        KontraktLeverandor other = (KontraktLeverandor) o;
        return new EqualsBuilder().
            append(kontraktId, other.getKontraktId()).
            append(leverandorNr, other.getLeverandorNr()).
            append(fraDato, other.getFraDato()).
            isEquals();
    }
    
    @Transient
	public String getNavn() {
		return leverandor!=null?leverandor.getNavn():null;
	}

	public String getType() {
		return resolvePropertyText("leverandor");
	}
	public Long getTlfnummer() {
        return tlfnummer;
    }

	 public void setTlfnummer(Long tlfnummer) {
	    	Long old = this.tlfnummer;
	        this.tlfnummer = tlfnummer;
	        try {
				fireVetoableChange("tlfnummer", old, tlfnummer);
				firePropertyChange("tlfnummer", old, tlfnummer);
			} catch (PropertyVetoException e) {
				this.tlfnummer = old;
			}
	    }
	@Transient
	public Boolean getEkstraDagsrapport() {
		return ekstraDagsrapport;
	}
	
	public void setEkstraDagsrapport(Boolean ekstraDagsrapport) {
		Boolean old = this.ekstraDagsrapport;
		this.ekstraDagsrapport = ekstraDagsrapport;
		try {
			fireVetoableChange("ekstraDagsrapport", old, ekstraDagsrapport);
			firePropertyChange("ekstraDagsrapport", old, ekstraDagsrapport);
		} catch (PropertyVetoException e) {
			this.ekstraDagsrapport = old;
		}
	}
	@Transient
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number){
		this.number = number;
	}
	public void setLevkontraktList(List<Levkontrakt> levkontraktList) {
		List<Levkontrakt> old = this.levkontraktList;
		this.levkontraktList = levkontraktList;
		try{
			fireVetoableChange("levkontraktList", old, levkontraktList);
			firePropertyChange("levkontraktList", old, levkontraktList);
		} catch (PropertyVetoException e){
			this.levkontraktList = old;
		}
	}
	
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumns({@JoinColumn(name="LEVERANDOR_NR", referencedColumnName="LEVERANDOR_NR"),
  				  @JoinColumn(name="DRIFTKONTRAKT_ID", referencedColumnName="KONTRAKT_ID"),
                  @JoinColumn(name="FRA_DATO", referencedColumnName="FRA_DATO")})
	public List<Levkontrakt> getLevkontraktList() {
		return levkontraktList;
	}
	public void setInaktivDato(Date inaktivDato) {
		this.inaktivDato = inaktivDato;
	}
	@Transient
	public Date getInaktivDato() {
		return inaktivDato;
	}
	public void setAntallKjoretoy(Long antallKjoretoy) {
		this.antallKjoretoy = antallKjoretoy;
	}
	@Transient
	public Long getAntallKjoretoy() {
		return antallKjoretoy;
	}
}
