package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class KontraktStabPK implements Serializable {
	private String funksjonNavn;
	private Long kontraktId;
	private String signatur;
	
	public KontraktStabPK() {
	}
	
	public KontraktStabPK(String funksjonNavn, Long kontraktId, String signatur) {
		this.funksjonNavn = funksjonNavn;
		this.kontraktId = kontraktId;
		this.signatur = signatur;
	}

	public String getFunksjonNavn() {
		return funksjonNavn;
	}

	public void setFunksjonNavn(String funksjonNavn) {
		this.funksjonNavn = funksjonNavn;
	}

	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(funksjonNavn).append(kontraktId).append(signatur).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktStabPK)) {return false;}
        
        KontraktStabPK other = (KontraktStabPK) o;
        
        return new EqualsBuilder().append(funksjonNavn, other.funksjonNavn).append(kontraktId, other.kontraktId).append(signatur, other.signatur).isEquals();
    }
}
