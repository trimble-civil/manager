package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.applikasjon.Modul;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ROLLE_TILGANG")
@IdClass(RolleTilgangPK.class)
@NamedQueries( {
		@NamedQuery(name = RolleTilgang.QUERY_ROLLETILGANG_GET_FOR_ROLE, query = "SELECT rt FROM RolleTilgang rt where rt.rolleId = :rolleId"),
		@NamedQuery(name = RolleTilgang.QUERY_ROLLETILGANG_DELETE_FOR_ROLE, query = "DELETE FROM RolleTilgang rt where rt.rolleId = :rolleId") })
public class RolleTilgang implements Serializable, Comparable<RolleTilgang> {
	public static final String QUERY_ROLLETILGANG_DELETE_FOR_ROLE = "RolleTilgang.deleteForRole";
	public static final String QUERY_ROLLETILGANG_GET_FOR_ROLE = "RolleTilgang.getForRole";

	private Modul modul;
	private Long modulId;
	private String opprettetAv;
	private Date opprettetDato;
	private Rolle rolle;
	private Long rolleId;
	private Tilgangsnivaa tilgangsnivaa;

	public RolleTilgang() {
	}

	/** {@inheritDoc} */
	public int compareTo(RolleTilgang o) {
		if (o == null) {
			return 1;
		}

		Tilgangsnivaa otherNivaa = o.getTilgangsnivaa();
		Tilgangsnivaa thisNivaa = getTilgangsnivaa();
		return new CompareToBuilder().append(thisNivaa, otherNivaa).append(getRolle(), o.getRolle()).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof RolleTilgang)) {
			return false;
		}
		if (o == this) {
			return true;
		}

		RolleTilgang other = (RolleTilgang) o;
		return new EqualsBuilder().append(rolleId, other.getRolleId()).append(modulId, other.getModulId()).isEquals();
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "MODUL_ID", referencedColumnName = "ID")
	public Modul getModul() {
		return modul;
	}

	@Id
	@Column(name = "MODUL_ID", nullable = false, insertable = false, updatable = false)
	public Long getModulId() {
		return modulId;
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "ROLLE_ID", referencedColumnName = "ID")
	public Rolle getRolle() {
		return rolle;
	}

	@Id
	@Column(name = "ROLLE_ID", nullable = false, insertable = false, updatable = false)
	public Long getRolleId() {
		return rolleId;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "NIVAA", referencedColumnName = "NIVAA", nullable = false)
	public Tilgangsnivaa getTilgangsnivaa() {
		return tilgangsnivaa;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(29, 31).append(rolleId).append(modulId).toHashCode();
	}

	public void setModul(Modul modul) {
		this.modul = modul;
		if (modul != null) {
			setModulId(modul.getId());
		} else {
			setModulId(null);
		}
	}

	public void setModulId(Long id) {
		modulId = id;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setRolle(Rolle rolle) {
		this.rolle = rolle;
		if (rolle != null) {
			setRolleId(rolle.getId());
		} else {
			setRolleId(null);
		}
	}

	public void setRolleId(Long id) {
		rolleId = id;
	}

	public void setTilgangsnivaa(Tilgangsnivaa tilgangsnivaa) {
		this.tilgangsnivaa = tilgangsnivaa;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("rolleId", rolleId).append("modulId", modulId).toString();
	}
}
