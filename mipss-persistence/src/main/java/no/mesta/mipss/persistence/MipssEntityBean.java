package no.mesta.mipss.persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.common.VetoableChangeSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.BeanProperty;

/**
 * Hjelpemetoder for entiteter slik at man kan finne litt ut om seg selv
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings({ "unchecked", "serial" })
public abstract class MipssEntityBean<T extends MipssEntityBean> implements PropertyChangeSource, VetoableChangeSource, Serializable, IRenderableMipssEntity {
	private static transient PropertyResourceBundleUtil BUNDLE;
	private static final String GET_CLASS = "getClass";
	public static final String GET_PREFIX = "get";
	public static final String IS_PREFIX = "is";
	public static final String SET_PREFIX = "set";
	private transient Logger logger;
	private transient PropertyChangeSupport props;
	private transient VetoableChangeSupport vetos;
	private boolean newEntity = false;

	/** {@inheritDoc} */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	public void addVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		getVetos().addVetoableChangeListener(propertyName, listener);
	}

	/** {@inheritDoc} */
	public void addVetoableChangeListener(VetoableChangeListener listener) {
		getVetos().addVetoableChangeListener(listener);
	}

	/**
	 * Fjerner alle lyttere på denne klassen
	 * 
	 */
	public void clearListeners() {
		for (VetoableChangeListener veto : getVetoableChangeListeners()) {
			getVetos().removeVetoableChangeListener(veto);
		}

		for (PropertyChangeListener change : getPropertyChangeListeners()) {
			getProps().removePropertyChangeListener(change);
		}
	}

	public String getTextForGUI(){
		return null;
	}
	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o.getClass().equals(getClass()))) {
			return false;
		}

		T other = (T) o;

		EqualsBuilder builder = new EqualsBuilder();

		for (String field : getIdNames()) {
			Object thisValue = getFieldValue(field);
			Object otherValue = other.getFieldValue(field);
			builder.append(thisValue, otherValue);
		}

		return builder.isEquals();
	}

	protected void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
		if (props != null && props.getPropertyChangeListeners() != null
				&& props.getPropertyChangeListeners().length > 0) {
			props.firePropertyChange(propertyName, oldValue, newValue);
		}
	}

	protected void firePropertyChange(String propertyName, int oldValue, int newValue) {
		if (props != null && props.getPropertyChangeListeners() != null
				&& props.getPropertyChangeListeners().length > 0) {
			props.firePropertyChange(propertyName, oldValue, newValue);
		}
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		if (props != null && props.getPropertyChangeListeners() != null
				&& props.getPropertyChangeListeners().length > 0) {
			props.firePropertyChange(propertyName, oldValue, newValue);
		}
	}

	protected void fireVetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		if (vetos != null && vetos.getVetoableChangeListeners() != null
				&& vetos.getVetoableChangeListeners().length > 0) {
			vetos.fireVetoableChange(evt);
		}
	}

	protected void fireVetoableChange(String propertyName, boolean oldValue, boolean newValue)
			throws PropertyVetoException {
		if (vetos != null && vetos.getVetoableChangeListeners() != null
				&& vetos.getVetoableChangeListeners().length > 0) {
			vetos.fireVetoableChange(propertyName, oldValue, newValue);
		}
	}

	protected void fireVetoableChange(String propertyName, int oldValue, int newValue) throws PropertyVetoException {
		if (vetos != null && vetos.getVetoableChangeListeners() != null
				&& vetos.getVetoableChangeListeners().length > 0) {
			vetos.fireVetoableChange(propertyName, oldValue, newValue);
		}
	}

	protected void fireVetoableChange(String propertyName, Object oldValue, Object newValue)
			throws PropertyVetoException {
		if (vetos != null && vetos.getVetoableChangeListeners() != null
				&& vetos.getVetoableChangeListeners().length > 0) {
			vetos.fireVetoableChange(propertyName, oldValue, newValue);
		}
	}

	/**
	 * Setter det første tegnet til liten bokstav
	 * 
	 * @param name
	 * @return
	 */
	public String firstLower(String name) {
		String s = name.substring(0, 1).toLowerCase();
		name = s + name.substring(1);

		return name;
	}

	/**
	 * Setter det første tegnet til stor bokstav
	 * 
	 * @param name
	 * @return
	 */
	public String firstUpper(String name) {
		String s = name.substring(0, 1).toUpperCase();
		name = s + name.substring(1);

		return name;
	}

	public Object getAnnotationValue(Method m, Class<? extends Annotation> anno, String fieldName) {
		Annotation a = m.getAnnotation(anno);
		
		if (a==null){//annotasjon er satt på attributt
			return null;
		}
		Class<? extends Annotation> clazz = a.getClass();
		try {
			Method field = clazz.getMethod(fieldName, (Class<?>[])null);
			return field.invoke(a, (Object[]) null);
		} catch (Exception e) {
			return null;
		}
	}
	public Object getAnnotationValue(Field f, Class<? extends Annotation> anno, String fieldName) {
		Annotation a = f.getAnnotation(anno);
		
		if (a==null){//annotasjon er satt på attributt
			return null;
		}
		Class<? extends Annotation> clazz = a.getClass();
		try {
			Method field = clazz.getMethod(fieldName, (Class<?>[])null);
			return field.invoke(a, (Object[]) null);
		} catch (Exception e) {
			return null;
		}
	}
	public Object getAnnotationValue(String method, Class<? extends Annotation> anno, String field) {
		Method m = getMethod(method, null);
		Object annotationValue = getAnnotationValue(m, anno, field);
		if (annotationValue==null){//annotasjon kan være på attributt
			String fieldName = method.replaceAll("get", "");
			fieldName = firstLower(fieldName);
			Field f = getField(fieldName);
			annotationValue = getAnnotationValue(f, anno, field);
		}
		return annotationValue;
	}
	public Field getField(String name){
		try {
			return getClass().getDeclaredField(name);
		} catch (SecurityException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Henter ut bønnefelt navnet for is, get og set metoder
	 * 
	 * @param m
	 * @return
	 * @throws java.lang.IllegalArgumentException
	 *             hvis det ikke er en is/get/set metode
	 */
	public String getBeanName(Method m) {
		String name = m.getName();
		if (!StringUtils.startsWith(name, SET_PREFIX) && !StringUtils.startsWith(name, GET_PREFIX)
				&& !StringUtils.startsWith(name, IS_PREFIX)) {
			throw new IllegalArgumentException("Can only compute bean methods!");
		}

		if (StringUtils.startsWith(name, GET_PREFIX) || StringUtils.startsWith(name, SET_PREFIX)) {
			name = name.substring(3);
		} else {
			name = name.substring(2);
		}

		return firstLower(name);
	}

	/**
	 * Henter ut alle db kollonner for klassen
	 * 
	 * @return
	 */
	@Transient
	public List<Method> getDBColumnMethods() {
		List<Method> allMethods = getMethods();
		List<Method> columnMethods = new ArrayList<Method>();
		for (Method m : allMethods) {
			if (isDbColumn(m)) {
				columnMethods.add(m);
			}
		}

		return columnMethods;
	}

	/**
	 * JPA Mapping navnet
	 * 
	 * @return
	 */
	@Transient
	public String getEntityName() {
		return getClass().getSimpleName();
	}

	/**
	 * Returnerer typen til feltet
	 * 
	 * @param field
	 * @return
	 */
	public Class<?> getFieldType(String field) {
		String name = GET_PREFIX + firstUpper(field);
		Method m = getMethod(name, null);

		return m.getReturnType();
	}

	/**
	 * Henter ut verdien til et felt
	 * 
	 * @param field
	 * @return
	 */
	public synchronized <V> V getFieldValue(String field) {
		BeanProperty<T, V> property = BeanProperty.create(field);
		try{
			return property.getValue((T) this);
		} catch (UnsupportedOperationException e){
			throw e;
		}
	}

	/**
	 * Returnerer get/is methoden for en setter
	 * 
	 * @param m
	 * @return
	 */
	public Method getGetter(Method m) {
		String name = m.getName();
		if (!StringUtils.startsWith(name, SET_PREFIX)) {
			throw new IllegalArgumentException("Can only test set methods!");
		}

		name = getBeanName(m);

		String isName = IS_PREFIX + firstUpper(name);
		String getName = GET_PREFIX + firstUpper(name);

		Method isMethod = getMethod(isName, null);
		Method getMethod = getMethod(getName, null);

		Method getter = null;
		if (isMethod != null && getMethod != null) {
			throw new IllegalStateException("Both " + isName + " and " + getName + " exists!");
		} else if (isMethod == null && getMethod == null) {
			getter = null;
		} else {
			if (isMethod != null) {
				getter = isMethod;
			} else {
				getter = getMethod;
			}
		}

		return getter;
	}

	/**
	 * Skal returnerer alle ID felter for klassen
	 * 
	 * @return
	 */
	@Transient
//	public abstract String[] getIdNames();
	
	public String[] getIdNames(){
		List<String> aList =new ArrayList<String>();
		for (Method m:getMethods()){
			Annotation[] annotations = m.getAnnotations();
			for (Annotation a:annotations){
				if (a.annotationType().equals(Id.class)){
					aList.add(getBeanName(m));
				}
			}
			
			String name = m.getName();
			if (StringUtils.startsWith(name, SET_PREFIX) || StringUtils.startsWith(name, GET_PREFIX) || StringUtils.startsWith(name, IS_PREFIX)) {
				String field = getBeanName(m); 
				try {
					Field f = getClass().getDeclaredField(field);
					for (Annotation a: f.getAnnotations()){
						if (a.annotationType().equals(Id.class)&&!aList.contains(field)){
							aList.add(getBeanName(m));
						}
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchFieldException e) {
					//not a corresponding field... ignore.
				}
				
			}
			
			
		}
		return aList.toArray(new String[]{});
	}

	/**
	 * Sluker exceptions og returnerer null hvis metoden ikke kan fremskaffes
	 * 
	 * @param name
	 * @param params
	 * @return
	 */
	public Method getMethod(String name, Class<?>[] params) {
		Method m = null;
		try {
			m = getClass().getMethod(name, params);
		} catch (SecurityException e) {
			e = null;
		} catch (NoSuchMethodException e) {
			e = null;
		}

		return m;
	}

	/**
	 * Gir ut en liste over alle metodene i klassen
	 * 
	 * @see java.lang.Class#getMethods()
	 * @return
	 */
	@Transient
	public List<Method> getMethods() {
		Method[] method = getClass().getMethods();
		List<Method> methods = new ArrayList<Method>();
		for (Method m : method) {
			methods.add(m);
		}

		return methods;
	}

	/**
	 * Alle lyttere
	 * 
	 * @return
	 */
	@Transient
	public PropertyChangeListener[] getPropertyChangeListeners() {
		return getProps().getPropertyChangeListeners();
	}

	protected PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/**
	 * Gir ut SQL tabell navnet, slik at man kan spørre i native queries
	 * 
	 * @return
	 */
	@Transient
	public String getTableName() {
		String tableName;

		if (getClass().isAnnotationPresent(Table.class)) {
			Table table = (Table) getClass().getAnnotation(Table.class);
			tableName = table.name();
		} else {
			tableName = getClass().getSimpleName();
		}

		return tableName;
	}

	/**
	 * Alle veto lyttere
	 * 
	 * @return
	 */
	@Transient
	public VetoableChangeListener[] getVetoableChangeListeners() {
		return getVetos().getVetoableChangeListeners();
	}

	protected VetoableChangeSupport getVetos() {
		if (vetos == null) {
			vetos = new VetoableChangeSupport(this);
		}

		return vetos;
	}

	/**
	 * Returnerer alle getMetoder som er skrivbare
	 * 
	 * @return
	 */
	@Transient
	public List<Method> getWritableBeanMethods() {
		List<Method> allMethods = getMethods();
		List<Method> writableMethods = new ArrayList<Method>();

		for (Method m : allMethods) {
			if (isGetter(m)) {
				Method writer = getWriteMethod(m);
				if (writer != null) {
					writableMethods.add(m);
				}
			}
		}

		return writableMethods;
	}

	/**
	 * Returerer navnet på alle skrivbare feltnavn
	 * 
	 * @return
	 */
	@Transient
	public List<String> getWritableFieldNames() {
		List<Method> methods = getWritableBeanMethods();
		List<String> names = new ArrayList<String>();

		for (Method m : methods) {
			String name = getBeanName(m);
			names.add(name);
		}

		return names;
	}
	/**
	 * Standard toStringmetode som baserer seg på alle skrivbare db-felter. 
	 * Tar ikke med relasjoner av typen:OneToMany, ManyToMany. Heller ikke 
	 * Embedded objekter
	 */
	@Override
	public String toString(){
		List<String> fields = getWritableFieldNamesWithoutRelationsAnd(new String[]{});
		ToStringBuilder s = new ToStringBuilder(this);
		for (String f:fields){
			Object fieldValue = getFieldValue(f);
			s.append(f, fieldValue);
		}
		return s.toString();
	}
	
	@Transient
	public List<String> getWritableFieldNamesWithoutRelationsAnd(String... ignore){
		List<Method> methods = getWritableBeanMethods();
		List<String> names = new ArrayList<String>();
		for (Method m:methods){
			boolean add = true;
			Annotation[] annotations = m.getAnnotations();
			for (Annotation a:annotations){
				if (a.annotationType().equals(OneToMany.class)||
					a.annotationType().equals(ManyToMany.class)||
					a.annotationType().equals(Embedded.class)||
					a.annotationType().equals(Transient.class)){
					add = false;
					break;
				}
				
			}
			if (!hasMethodCorespondingFieldAndCorrectAnnotationValues(m)){
				add = false;
			}
			String name = getBeanName(m);
			if (ignore!=null&&ignore.length>0){
				for (String s:ignore){
					if (s.equals(name)){
						add=false;
						break;
					}
				}
			}
			if (add){
				names.add(name);
			}
		}
		
		return names;
	}
	
	/**
	 * Sjekker om metoden har en tilsvarende property og om annotasjonene
	 * til propertyen er gyldige til å ta med i listen
	 * @param m
	 * @return
	 */
	private boolean hasMethodCorespondingFieldAndCorrectAnnotationValues(Method m){
		String name = m.getName();
		name = name.replaceAll("get", "");
		name = Character.toLowerCase(name.charAt(0))+name.substring(1);
		try {
			Field field = getClass().getDeclaredField(name);
			
			for (Annotation a:field.getAnnotations()){
				if (a.annotationType().equals(OneToMany.class)||
						a.annotationType().equals(ManyToMany.class)||
						a.annotationType().equals(Embedded.class)||
						a.annotationType().equals(Transient.class)){
					return false;
				}
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			//metoden har ikke en tilhørende property 
			return true;
		}
		return true;
	}

	/**
	 * Henter ut metoden for å skrive til et felt
	 * 
	 * @param m
	 * @return
	 */
	public Method getWriteMethod(Method m) {
		if (!isBeanGetMethod(m)) {
			throw new IllegalArgumentException("Not a is/get method!");
		}

		String name = getBeanName(m);
		name = SET_PREFIX + firstUpper(name);

		return getMethod(name, new Class<?>[] { m.getReturnType() });
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		for (String idField : getIdNames()) {
			Object value = getFieldValue(idField);
			builder.append(value);
		}

		return builder.toHashCode();
	}

	/**
	 * Lager en kommaseparert string av id'er til entiteten
	 * 
	 * @return
	 */
	private String idsToString() {
		String idString = "";

		int i = 0;
		int ids = getIdNames().length;
		for (String field : getIdNames()) {
			Object value = getFieldValue(field);
			idString += value;

			if (i < ids - 1) {
				idString += ", ";
			}

			i++;
		}

		return idString;
	}
	
	/**
	 * Angir om metoden er en is/get metode
	 * 
	 * @param m
	 * @return
	 */
	public boolean isBeanGetMethod(Method m) {
		String name = m.getName();
		return StringUtils.startsWith(name, GET_PREFIX) || StringUtils.startsWith(name, IS_PREFIX);
	}

	/**
	 * Angir om en get metode representerer en database kolonne
	 * 
	 * @param m
	 * @return
	 */
	public boolean isDbColumn(Method m) {
		String name = m.getName();

		if (StringUtils.equals(name, GET_CLASS)) {
			return false;
		}

		if (!StringUtils.startsWith(name, GET_PREFIX)) {
			return false;
		}

		Class<?>[] params = m.getParameterTypes();
		if (params.length > 0) {
			return false;
		}

		Class<?> returnType = m.getReturnType();
		if (Void.TYPE.equals(returnType)) {
			return false;
		}

		if (m.isAnnotationPresent(Transient.class)) {
			return false;
		}

		return true;
	}

	/**
	 * Angir om en bønne felt representerer en database kolonne
	 * 
	 * @param m
	 * @return
	 */
	public boolean isDbColumn(String fieldName) {
		String methodSuffix = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		
		Method m = getMethod(GET_PREFIX + methodSuffix, null);
		if(m == null) {
			m = getMethod(IS_PREFIX + methodSuffix, null);
		}
		
		if(m != null) {
			return isDbColumn(m);
		} else {
			return false;
		}
	}

	/**
	 * Sjekker om en metode er en getter
	 * 
	 * @param m
	 * @return
	 */
	public boolean isGetter(Method m) {
		if (!StringUtils.startsWith(m.getName(), GET_PREFIX) && !StringUtils.startsWith(m.getName(), IS_PREFIX)) {
			return false;
		}

		Class<?> returnType = m.getReturnType();
		if (Void.TYPE.equals(returnType)) {
			return false;
		}

		Class<?>[] parameterTypes = m.getParameterTypes();
		if (parameterTypes.length > 0) {
			return false;
		}

		return true;
	}

	/**
	 * Returnerer true hvis feltet er et id felt
	 * 
	 * @param field
	 * @return
	 */
	public boolean isIdField(String field) {
		for (String idField : getIdNames()) {
			if (StringUtils.equals(field, idField)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * <p>
	 * Sjekker at en metode er en setter metode(starter med "set") og at den
	 * returnerer Void, samt bare tar et argument.
	 * </p>
	 * 
	 * <p>
	 * Er public slik at den kan testes.
	 * </p>
	 * 
	 * @param m
	 *            metoden som skal testes
	 * @return
	 */
	public boolean isSetter(Method m) {
		if (!StringUtils.startsWith(m.getName(), SET_PREFIX)) {
			return false;
		}

		Class<?> returnType = m.getReturnType();
		if (!Void.TYPE.equals(returnType)) {
			return false;
		}

		Class<?>[] parameterTypes = m.getParameterTypes();
		if (parameterTypes == null || parameterTypes.length > 1) {
			return false;
		}

		return true;
	}

	/**
	 * Logger en beskjed som debug
	 *
	 * @param msg
	 */
	protected void log(String msg, Object... args) {
		if (logger == null) {
			logger = LoggerFactory.getLogger(getClass());
		}

		logger.debug("@" + Integer.toHexString(hashCode()) + "{" + idsToString() + "}." + msg + "("
				+ Arrays.toString(args) + ")");
	}

	/**
	 * Slår sammen alle felter fra den andre til denne instansen
	 * @param other
	 */
	public void mergeAll(T other) {
		List<String> fields = getWritableFieldNames();
		//sjekk at det er samme entitet
		for (String field : fields){
			if (isIdField(field)){
				Object otherId = other.getFieldValue(field);
				if (otherId==null){
					throw new IllegalArgumentException("Can not merge other entity with null id, use merge(T other) instead.");
				}
				Object thisId = getFieldValue(field);
				if (!(otherId.equals(thisId))){
					throw new IllegalArgumentException("Can not merge entity with another Id");
				}
			}
		}
		
		for (String field : fields) {
			Object value = other.getFieldValue(field);
			try {
				setFieldValue(field, value);
			} catch (Throwable t) {
				logger.warn("merge of field [" + field + "] gave " + t.getMessage());
			}
		}		
	}
	/**
	 * Kopierer alle db felter fra den andre til instansen, untatt id felter
	 * 
	 * @param other
	 */
	public void merge(T other) {
		if (other == null || !other.equals(this)) {
			throw new IllegalArgumentException("Merge not needed!");
		}

		mergeValues(other);
	}
	
	public void mergeValues(T other) {
		if (other == null) {
			throw new IllegalArgumentException("Merge not needed!");
		}

		List<String> fields = getWritableFieldNames();
		for (String field : fields) {
			if (!isIdField(field) && isDbColumn(field)) { // bare merge dbfelter, ignorer id felter og transiente felter
				Object value = other.getFieldValue(field);
				try {
					setFieldValue(field, value);
				} catch (Throwable t) {
					logger.warn("merge of field [" + field + "] gave " + t.getMessage());
				}
			}
		}		
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	public void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		getVetos().removeVetoableChangeListener(propertyName, listener);
	}

	/** {@inheritDoc} */
	public void removeVetoableChangeListener(VetoableChangeListener listener) {
		getVetos().removeVetoableChangeListener(listener);
	}

	/**
	 * Setter felt verdien
	 * 
	 * @param field
	 * @param value
	 */
	public synchronized void setFieldValue(String field, Object value) {
		BeanProperty<T, Object> property = BeanProperty.create(field);
		property.setValue((T) this, value);
	}

	@Transient
	private static PropertyResourceBundleUtil getBundle() {
		if (BUNDLE == null) {
			BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("persistanceTxts");
		}

		return BUNDLE;
	}

	public static String resolvePropertyText(String key) {
		return getBundle().getGuiString(key);
	}

	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
	@Transient
	public boolean isNewEntity() {
		return newEntity;
	}
}
