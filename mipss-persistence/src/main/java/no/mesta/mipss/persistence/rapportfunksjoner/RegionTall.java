package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class RegionTall implements Serializable {
	private Long regionId;
	private String regionnavn;
	private List<NokkeltallFelt> kontrakter;
	
	/**
	 * @return the regionId
	 */
	public Long getRegionId() {
		return regionId;
	}
	/**
	 * @return the regionnavn
	 */
	public String getRegionnavn() {
		return regionnavn;
	}
	/**
	 * @return the kontrakter
	 */
	public List<NokkeltallFelt> getKontrakter() {
		return kontrakter;
	}
	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	/**
	 * @param regionnavn the regionnavn to set
	 */
	public void setRegionnavn(String regionnavn) {
		this.regionnavn = regionnavn;
	}
	/**
	 * @param kontrakter the kontrakter to set
	 */
	public void setKontrakter(List<NokkeltallFelt> kontrakter) {
		this.kontrakter = kontrakter;
	}
	public boolean equals(Object other){
		if (!(other instanceof RegionTall)){
			return false;
		}
		RegionTall o = (RegionTall)other;
		return o.getRegionId().equals(getRegionId());
	}
	public void addNokkeltallFelt(NokkeltallFelt kontrakt) {
		if (kontrakter==null){
			kontrakter = new ArrayList<NokkeltallFelt>();
		}
		kontrakter.add(kontrakt);
	}
	
}
