package no.mesta.mipss.persistence.mipssfield;

import java.beans.PropertyVetoException;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Tiltak
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name="HENDELSE_TRAFIKKTILTAK_XV")
@NamedQueries( {
		@NamedQuery(name = HendelseTrafikktiltak.QUERY_FIND_ALL, query = "SELECT t FROM HendelseTrafikktiltak t"),
		@NamedQuery(name = HendelseTrafikktiltak.QUERY_FIND_FOR_INCIDENT, query = "SELECT t FROM HendelseTrafikktiltak t WHERE t.hendelseGuid = :hguid") })
@IdClass(HendelseTrafikktiltakPK.class)
public class HendelseTrafikktiltak extends MipssEntityBean<HendelseTrafikktiltak> implements Serializable, IRenderableMipssEntity,
		Comparable<HendelseTrafikktiltak>, PropertyChangeSource {
	private static final String BESKRIVELSE = "beskrivelse";
	private static final String ID_FIELD_HENDELSE = "hendelse";
	private static final String ID_FIELD_HENDELSE_GUID = "hendelseGuid";
	private static final String ID_FIELD_TYPE = "trafikktiltak";
	private static final String ID_FIELD_TYPE_ID = "trafikktiltakId";
	private static final String[] ID_NAMES = { ID_FIELD_HENDELSE, ID_FIELD_HENDELSE_GUID, ID_FIELD_TYPE,
			ID_FIELD_TYPE_ID };
	public static final String QUERY_FIND_ALL = "HendelseTrafikktiltak.findAll";
	public static final String QUERY_FIND_FOR_INCIDENT = "HendelseTrafikktiltak.findForIncident";
	
	
	@Column(name = "BESKRIVELSE", length = 255)
	private String beskrivelse;
	@ManyToOne
	@JoinColumn(name = "HENDELSE_GUID", referencedColumnName = "GUID", nullable = false, insertable = true, updatable = true)
	private Hendelse hendelse;
	@Id
	@Column(name = "HENDELSE_GUID", nullable = false, insertable = false, updatable = false)
	private String hendelseGuid;
	@ManyToOne
	@JoinColumn(name = "TILTAK_ID", referencedColumnName = "ID")
	private Trafikktiltak trafikktiltak;
	@Id
	@Column(name = "TILTAK_ID", nullable = false, insertable = false, updatable = false)
	private Long trafikktiltakId;

	/**
	 * Konstruktør
	 * 
	 */
	public HendelseTrafikktiltak() {
	}

	/** {@inheritDoc} */
	public int compareTo(HendelseTrafikktiltak o) {
		if (o == null) {
			return 1;
		}

		if (equals(o)) {
			return 0;
		}

		if (trafikktiltak == null) {
			return -1;
		}

		return trafikktiltak.compareTo(o.trafikktiltak);
	}
	/** {@inheritDoc} */
	@Override
	public boolean equals(final Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof HendelseTrafikktiltak)) {
			return false;
		}

		final HendelseTrafikktiltak other = (HendelseTrafikktiltak) o;
		return new EqualsBuilder().append(this.trafikktiltak.getId(), other.getTrafikktiltak().getId())
		.append(this.beskrivelse, other.beskrivelse).isEquals();
	}
	/**
	 * Beskrivelse
	 * 
	 * @return
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Hendelse
	 * 
	 * @return
	 */
	public Hendelse getHendelse() {
		return hendelse;
	}

	/**
	 * Hendelse
	 * 
	 * @return
	 */
	public String getHendelseGuid() {
		return hendelseGuid;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/** {@inheritDoc} */
	@Transient
	public String getTextForGUI() {
		return (getTrafikktiltak() != null ? getTrafikktiltak().getNavn() : "") + " "
				+ (getBeskrivelse() != null ? getBeskrivelse() : "");
	}

	/**
	 * Type
	 * 
	 * @return
	 */
	public Trafikktiltak getTrafikktiltak() {
		return trafikktiltak;
	}

	/**
	 * Type
	 * 
	 * @return
	 */
	public Long getTrafikktiltakId() {
		return trafikktiltakId;
	}

	/**
	 * Beskrivelse
	 * 
	 * @param beskrivelse
	 */
	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		
		try {
			fireVetoableChange(BESKRIVELSE, old, beskrivelse);
		} catch (PropertyVetoException e) {
			this.beskrivelse = old;
		}

		firePropertyChange(BESKRIVELSE, old, beskrivelse);
	}

	/**
	 * Hendelse
	 * 
	 * @param hendelse
	 */
	public void setHendelse(Hendelse hendelse) {
		Hendelse old = this.hendelse;
		this.hendelse = hendelse;
		if (hendelse != null) {
			setHendelseGuid(hendelse.getGuid());
		} else {
			setHendelseGuid(null);
		}
		firePropertyChange(ID_FIELD_HENDELSE, old, hendelse);
	}

	/**
	 * Hendelse
	 * 
	 * @param hendelsesGuid
	 */
	public void setHendelseGuid(String hendelsesGuid) {
		String old = this.hendelseGuid;
		this.hendelseGuid = hendelsesGuid;
		firePropertyChange(ID_FIELD_HENDELSE_GUID, old, hendelsesGuid);
	}

	/**
	 * Type
	 * 
	 * @param type
	 */
	public void setTrafikktiltak(Trafikktiltak trafikktiltak) {
		Trafikktiltak old = this.trafikktiltak;
		this.trafikktiltak = trafikktiltak;

		if (trafikktiltak != null) {
			setTrafikktiltakId(trafikktiltak.getId());
		} else {
			setTrafikktiltakId(null);
		}
		firePropertyChange(ID_FIELD_TYPE, old, trafikktiltak);
	}

	/**
	 * Type
	 * 
	 * @param typeId
	 */
	public void setTrafikktiltakId(Long trafikktiltakId) {
		Long old = this.trafikktiltakId;
		this.trafikktiltakId = trafikktiltakId;
		firePropertyChange(ID_FIELD_TYPE_ID, old, trafikktiltakId);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("hendelseGuid", hendelseGuid).append("trafikktiltakId", trafikktiltakId).append(
				BESKRIVELSE, beskrivelse).toString();
	}
}
