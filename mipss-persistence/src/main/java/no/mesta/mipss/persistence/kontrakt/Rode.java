package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.veinett.Veinett;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.eclipse.persistence.indirection.IndirectList;


/**
 * Rode
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Rode.QUERY_FIND_ALL, query = "select o from Rode o"),
	@NamedQuery(name = Rode.QUERY_FIND_BY_VEINETT, query = "select o from Rode o where o.veinett.id=:veinettId"),
	
	@NamedQuery(name = Rode.QUERY_FIND_BY_KONTRAKT, query = "select o from Rode o where o.kontraktId=:kontraktId"),
	@NamedQuery(name = Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE, query = "select o from Rode o where o.kontraktId=:kontraktId and o.rodetype.id=:rodetypeId"),
	@NamedQuery(name = Rode.QUERY_COUNT_BY_KONTRAKT_AND_TYPE, query = "select count(o) from Rode o where o.kontraktId=:kontraktId and o.rodetype.id=:rodetypeId"),
	@NamedQuery(name = Rode.QUERY_FIND_AKTIVE_BY_KONTRAKT_AND_TYPE, query = "select o from Rode o " +
																			"where o.kontraktId=:kontraktId " +
																			"and o.rodetype.id=:rodetypeId " +
																			"and (:now < o.gyldigTilDato or o.gyldigTilDato is null)")//and sysdate < rd.gyldig_til_dato
})
@SequenceGenerator(name = Rode.ID_SEQ, sequenceName = "RODE_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class Rode extends MipssEntityBean<Rode> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
    public static final String QUERY_FIND_ALL = "Rode.findAll";
    public static final String QUERY_FIND_BY_VEINETT= "Rode.findByVeinett";
    public static final String QUERY_FIND_BY_KONTRAKT = "Rode.findByKontakt";
    public static final String QUERY_FIND_BY_KONTRAKT_AND_TYPE = "Rode.findByKontraktAndType";
    public static final String QUERY_COUNT_BY_KONTRAKT_AND_TYPE = "Rode.countByKontraktAndType";
    public static final String QUERY_FIND_AKTIVE_BY_KONTRAKT_AND_TYPE = "Rode.findAktiveByKontraktAndType";
    
    public static final String ID_SEQ = "rodeIdSeq";
    
    private List<LevProdtypeRode> levProdtypeRodeList = new ArrayList<LevProdtypeRode>();

    private String beskrivelse;
    private Date gyldigFraDato;
    private Date gyldigTilDato;
    private Long id;
    private Long kontraktId;
    private String navn;
//    private Long veinettId;
    private Veinett veinett;
    private Rodetype rodetype;
    private Boolean utenforKontrakt;

    private Double rodeLengde;
    private OwnedMipssEntity ownedMipssEntity;
    public Rode() {
    }

    /**
     * For testing purposes
     */
    public Rode(long id, String name, boolean utenforKontrakt) {
        this.id = id;
        this.navn = name;
        this.utenforKontrakt = utenforKontrakt;
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Column(name="UTENFOR_KONTRAKT_FLAGG", nullable = false)
    public Boolean getUtenforKontrakt() {
        return utenforKontrakt;
    }

    public void setUtenforKontrakt(Boolean utenforKontrakt) {
        this.utenforKontrakt = utenforKontrakt;
    }

    @Column(name="GYLDIG_FRA_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getGyldigFraDato() {
        return gyldigFraDato;
    }

    public void setGyldigFraDato(Date gyldigFraDato) {
    	Date old = this.gyldigFraDato;
        this.gyldigFraDato = gyldigFraDato;
        firePropertyChange("gyldigFraDato", old, gyldigFraDato);
    }

    @Column(name="GYLDIG_TIL_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getGyldigTilDato() {
        return gyldigTilDato;
    }

    public void setGyldigTilDato(Date gyldigTilDato) {
    	Date old = this.gyldigTilDato;
        this.gyldigTilDato = gyldigTilDato;
        firePropertyChange("gyldigTilDato", old, gyldigTilDato);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Rode.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(name="KONTRAKT_ID", nullable = false)
    public Long getKontraktId() {
        return kontraktId;
    }

    public void setKontraktId(Long kontraktId) {
    	Long old = this.kontraktId;
        this.kontraktId = kontraktId;
        firePropertyChange("kontraktId", old, kontraktId);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @ManyToOne
    @JoinColumn(name = "TYPE_ID", referencedColumnName = "ID")
    public Rodetype getRodetype() {
        return rodetype;
    }

    public void setRodetype(Rodetype rodetype) {
    	Rodetype old = this.rodetype;
        this.rodetype = rodetype;
        firePropertyChange("rodetype", old, rodetype);
    }
    
    public void setVeinett(Veinett veinett){
    	Veinett old = this.veinett;
    	this.veinett = veinett;
    	firePropertyChange("veinett", old, veinett);
    }
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "VEINETT_ID", referencedColumnName = "ID")
    public Veinett getVeinett(){
    	return veinett;
    }
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            append("beskrivelse", beskrivelse).
            append("gyldigFraDato", gyldigFraDato).
            append("gyldigTilDato", gyldigTilDato).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Rode)) {return false;}
        
        Rode other = (Rode) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(9751,9721).append(id).toHashCode();
    }
    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

	public void setRodeLengde(Double rodeLengde) {
		Double old = this.rodeLengde;
		this.rodeLengde = rodeLengde;
		firePropertyChange("rodeLengde", old, rodeLengde);
	}
	@Transient
	public Double getRodeLengde() {
		return rodeLengde;
	}
	@OneToMany(mappedBy = "rode", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public List<LevProdtypeRode> getLevProdtypeRodeList() {
        return levProdtypeRodeList;
    }

    public void setLevProdtypeRodeList(List<LevProdtypeRode> levProdtypeRodeList) {
        List<LevProdtypeRode> old = this.levProdtypeRodeList;
        this.levProdtypeRodeList = levProdtypeRodeList;
        
        if(! (levProdtypeRodeList instanceof IndirectList)) {
        	firePropertyChange("levProdtypeRodeList", old, levProdtypeRodeList);
        }
    }
	@Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
}
