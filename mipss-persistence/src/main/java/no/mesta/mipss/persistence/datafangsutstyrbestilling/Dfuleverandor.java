package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

@SuppressWarnings("serial")
@NamedQueries(
		{@NamedQuery(name = Dfuleverandor.FIND_ALL, query = "select o from Dfuleverandor o")
})
@Entity
@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})
public class Dfuleverandor extends MipssEntityBean<Dfuleverandor> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "Dfuleverandor.findAll";
	private String navn;
	private String kontaktNavn;
	private String adresse;
	private String postNummer;
	private String postSted;
	private String tlfNummer1;
	private String tlfNummer2;
	private String epostAdresse;
	private OwnedMipssEntity ownedMipssEntity;
	
	@Id
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	
	public String getKontaktNavn() {
		return kontaktNavn;
	}
	
	public void setKontaktNavn(String kontaktNavn) {
		String old = this.kontaktNavn;
		this.kontaktNavn = kontaktNavn;
		firePropertyChange("kontaktNavn", old, kontaktNavn);
	}
	
	@Column(name = "ADRESSE")
	public String getAdresse() {
		return adresse;
	}
	
	public void setAdresse(String adresse) {
		String old = this.adresse;
		this.adresse = adresse;
		firePropertyChange("adresse", old, adresse);
	}
	
	public String getPostNummer() {
		return postNummer;
	}
	
	public void setPostNummer(String postNummer) {
		String old = this.postNummer;
		this.postNummer = postNummer;
		firePropertyChange("postNummer", old, postNummer);
	}
	
	public String getPostSted() {
		return postSted;
	}
	
	public void setPostSted(String postSted) {
		String old = this.postSted;
		this.postSted = postSted;
		firePropertyChange("postSted", old, postSted);
	}
	
	public String getTlfNummer1() {
		return tlfNummer1;
	}
	
	public void setTlfNummer1(String tlfNummer1) {
		String old = this.tlfNummer1;
		this.tlfNummer1 = tlfNummer1;
		firePropertyChange("tlfNummer1", old, tlfNummer1);
	}
	
	public String getTlfNummer2() {
		return tlfNummer2;
	}
	
	public void setTlfNummer2(String tlfNummer2) {
		String old = this.tlfNummer2;
		this.tlfNummer2 = tlfNummer2;
		firePropertyChange("tlfNummer2", old, tlfNummer2);
	}
	
	@Column(name = "EPOST_ADRESSE")
	public String getEpostAdresse() {
		return epostAdresse;
	}
	
	public void setEpostAdresse(String epostAdresse) {
		String old = this.epostAdresse;
		this.epostAdresse = epostAdresse;
		firePropertyChange("epostAdresse", old, epostAdresse);
	}

	@Transient
	public String getTextForGUI() {
		return this.getNavn();
	}

	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		
		if(ownedMipssEntity == null) {
			ownedMipssEntity = new OwnedMipssEntity();
		}
		
		return ownedMipssEntity;
	}

	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		this.ownedMipssEntity = ownedMipssEntity;
	}	
}