package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "driftregionIdSeq", sequenceName = "DRIFTREGION_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQuery(name = "Driftregion.findAll", 
    query = "select o from Driftregion o")
public class Driftregion extends MipssEntityBean<Driftregion> implements Serializable, IRenderableMipssEntity {
	
	public static final String FIND_ALL = "Driftregion.findAll";
    private String beskrivelse;
    private String endretAv;
    private Timestamp endretDato;
    private Long geoomraadeId;
    private Long id;
    private String navn;
    private String opprettetAv;
    private Timestamp opprettetDato;
    private List<Driftdistrikt> driftdistriktList = new ArrayList<Driftdistrikt>();

    public Driftregion() {
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name="ENDRET_DATO")
    public Timestamp getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Timestamp endretDato) {
        this.endretDato = endretDato;
    }

    @Column(name="GEOOMRAADE_ID")
    public Long getGeoomraadeId() {
        return geoomraadeId;
    }

    public void setGeoomraadeId(Long geoomraadeId) {
    	Long old = this.geoomraadeId;
        this.geoomraadeId = geoomraadeId;
        firePropertyChange("geoomraadeId", old, geoomraadeId);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "driftregionIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    public Timestamp getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Timestamp opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @OneToMany(mappedBy = "driftregion")
    public List<Driftdistrikt> getDriftdistriktList() {
        return driftdistriktList;
    }

    public void setDriftdistriktList(List<Driftdistrikt> driftdistriktList) {
        this.driftdistriktList = driftdistriktList;
    }

    public Driftdistrikt addDriftdistrikt(Driftdistrikt driftdistrikt) {
        getDriftdistriktList().add(driftdistrikt);
        driftdistrikt.setDriftregion(this);
        return driftdistrikt;
    }

    public Driftdistrikt removeDriftdistrikt(Driftdistrikt driftdistrikt) {
        getDriftdistriktList().remove(driftdistrikt);
        driftdistrikt.setDriftregion(null);
        return driftdistrikt;
    }
    
    // de neste metodene er håndkodet
    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Driftregion)) {
            return false;
        }
        Driftregion other = (Driftregion) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert på Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,13).
            append(getId()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Driftregion other = (Driftregion) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
