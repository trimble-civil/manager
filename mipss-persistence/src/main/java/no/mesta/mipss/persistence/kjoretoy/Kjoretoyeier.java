package no.mesta.mipss.persistence.kjoretoy;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.eclipse.persistence.indirection.IndirectList;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "kjoretoyeierIdSeq", sequenceName = "KJORETOYEIER_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
	@NamedQuery(name = Kjoretoyeier.FIND_ALL, query = "select e from Kjoretoyeier e order by e.navn"),
	@NamedQuery(name = Kjoretoyeier.FIND_BY_ID, query = "select e from Kjoretoyeier e where e.id =:id")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

public class Kjoretoyeier extends MipssEntityBean<Kjoretoyeier> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity, MipssEpostEntity, KjoretoyKontaktperson {
	public static final String FIND_ALL = "Kjoretoyeier.findAll";
	public static final String FIND_BY_ID = "Kjoretoyeier.findById";
	private static final String[] ID_NAMES = {"id"};
	
    private String adresse;
    private String epostAdresse;
    private Long id;
    private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
    private String kopiEpostAdresse;
    private String navn;
    private OwnedMipssEntity ownedMipssEntity;
    private Long postnummer;
    private String poststed;
    private Long sendeEpostFlagg;
    
    private Long tlfnummer1;
    
    private Long tlfnummer2;
    private Long ueFlagg;
    private Boolean ekstraDagsrapport;
    
    private final UUID uuid;
    
    public Kjoretoyeier() {
    	uuid = UUID.randomUUID();
    }
    public Kjoretoy addKjoretoy(Kjoretoy kjoretoy) {
        getKjoretoyList().add(kjoretoy);
        kjoretoy.setKjoretoyeier(this);
        return kjoretoy;
    }
   
    public int compareTo(Object o) {
        Kjoretoy other = (Kjoretoy) o;
        return new CompareToBuilder().
            append(id, other.getId()).
            toComparison();
    }
    /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante n�klene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof Kjoretoyeier)) {
             return false;
         }
         Kjoretoyeier other = (Kjoretoyeier) o;
         if(getId() != null && other.getId() != null) {
	         return new EqualsBuilder().
	             append(getId(), other.getId()).
	             isEquals();
         } else if(getId() == null && other.getId() == null) {
        	 return new EqualsBuilder().
             append(uuid, other.uuid).
             isEquals();
         } else {
        	 return false;
         }
     }

    public String getAdresse() {
        return adresse;
    }

    @Column(name="EPOST_ADRESSE")
    public String getEpostAdresse() {
        return epostAdresse;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoyeierIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    @Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

    @OneToMany(mappedBy = "kjoretoyeier", fetch = FetchType.LAZY)
    public List<Kjoretoy> getKjoretoyList() {
        return kjoretoyList;
    }

    @Column(name ="KOPI_EPOST_ADRESSE")
    public String getKopiEpostAdresse(){
    	return kopiEpostAdresse;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }

    @Column(nullable = false)
    public Long getPostnummer() {
        return postnummer;
    }

    public String getPoststed() {
        return poststed;
    }

    @Column(name="SENDE_EPOST_FLAGG", nullable=false)
    public Long getSendeEpostFlagg(){
    	return sendeEpostFlagg;
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    public Long getTlfnummer1() {
        return tlfnummer1;
    }

    public Long getTlfnummer2() {
        return tlfnummer2;
    }

    @Column(name="UE_FLAGG", nullable = false)
    public Long getUeFlagg() {
        return ueFlagg;
    }

    @Transient
    public Boolean getUeFlaggBoolean() {
        return ueFlagg != null && ueFlagg.longValue() > 0;
    }

    @Transient
    public String getWholeAdresse() {
        StringBuffer b = new StringBuffer();
        if (adresse != null && adresse.length() > 0) {
            b.append(adresse)
            .append(System.getProperty("line.separator"));
        }
        if (postnummer != null) {
            b.append(postnummer)
            .append(' ');
        }
        if (poststed != null && poststed.length() > 0) {
            b.append(poststed);
        }
        return b.toString();
    }

    /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
    	 if(getId() != null) {
	         return new HashCodeBuilder(17,91).
	             append(getId()).
	             toHashCode();
    	 } else {
    		 return new HashCodeBuilder(17,91).
             append(uuid).
             toHashCode();
    	 }
     }

    public Kjoretoy removeKjoretoy(Kjoretoy kjoretoy) {
        getKjoretoyList().remove(kjoretoy);
        kjoretoy.setKjoretoyeier(null);
        return kjoretoy;
    }

   

    public void setAdresse(String adresse) {
    	String old = this.adresse;
        this.adresse = adresse;
        
        firePropertyChange("adresse", old, adresse);
    }

    public void setEpostAdresse(String epostAdresse) {
    	if (epostAdresse!=null&&epostAdresse.equals(""))
    		epostAdresse=null;
    	String old = this.epostAdresse;
        this.epostAdresse = epostAdresse;
        
        firePropertyChange("epostAdresse", old, epostAdresse);
    }

    public void setId(Long id) {
        this.id = id;
    }

     public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
    	List<Kjoretoy> old = this.kjoretoyList;
        this.kjoretoyList = kjoretoyList;
        
        if(! (kjoretoyList instanceof IndirectList)) {
        	firePropertyChange("kjoretoyList", old, kjoretoyList);
        }
    }

     public void setKopiEpostAdresse(String kopiEpostAdresse){
     	String old = this.kopiEpostAdresse;
    	 if (kopiEpostAdresse!=null&&kopiEpostAdresse.equals(""))
     		kopiEpostAdresse=null;
    	this.kopiEpostAdresse = kopiEpostAdresse;
    	firePropertyChange("kopiEpostAdresse", old, kopiEpostAdresse);
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        
        firePropertyChange("navn", old, navn);
    }

    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    public void setPostnummer(Long postnummer) {
    	Long old = this.postnummer;
        this.postnummer = postnummer;
    	
    	firePropertyChange("postnummer", old, postnummer);
    }
    
    public void setPoststed(String poststed) {
    	String old = this.poststed;
        this.poststed = poststed;
        
        firePropertyChange("poststed", old, poststed);
    }
    
    public void setSendeEpostFlagg(Long sendeEpostFlagg){
    	Long old = this.sendeEpostFlagg;
    	this.sendeEpostFlagg = sendeEpostFlagg;
    	try {
			fireVetoableChange("sendeEpostFlagg", old, sendeEpostFlagg);
			firePropertyChange("sendeEpostFlagg", old, sendeEpostFlagg);
		} catch (PropertyVetoException e) {
			this.sendeEpostFlagg = old;
		}
    }

    public void setTlfnummer1(Long tlfnummer1) {
    	Long old = this.tlfnummer1;
        this.tlfnummer1 = tlfnummer1;
        
        firePropertyChange("tlfnummer1", old, tlfnummer1);
    }

    public void setTlfnummer2(Long tlfnummer2) {
    	Long old = this.tlfnummer2;
        this.tlfnummer2 = tlfnummer2;
    	
    	firePropertyChange("tlfnummer2", old, tlfnummer2);
    }

    public void setUeFlagg(Long ueFlagg) {
    	Long old = this.ueFlagg;
        this.ueFlagg = ueFlagg;
        
        firePropertyChange("ueFlagg", old, ueFlagg);
    }

    @Transient
    public void setUeFlaggBoolean(Boolean ueFlagg) {
    	Boolean old = this.getUeFlaggBoolean();
        this.ueFlagg = ueFlagg != null && ueFlagg.booleanValue() ? Long.valueOf(1) : Long.valueOf(0);
        
        firePropertyChange("ueFlaggBoolean", old, this.getUeFlaggBoolean());
    }

	// de neste metodene er h�ndskrevet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("navn", getNavn()).
            append("addresse", getAdresse()).
            append("postnummer", getPostnummer()).
            append("poststed", getPoststed()).
            append("telefon1", getTlfnummer1()).
            toString();
    }
    
    @Transient
	public Boolean getSendeEpost() {
		return Long.valueOf(getSendeEpostFlagg())==1;
	}
    public void setSendeEpost(Boolean epostFlagg) {
		Boolean old = getSendeEpost();
		boolean vetoed = false;
		try {
			fireVetoableChange("sendeEpost", old, epostFlagg);
		} catch (PropertyVetoException e) {
			vetoed = true;
		}
		
		if (!vetoed){
			if(epostFlagg != null && epostFlagg) {
				setSendeEpostFlagg(1L);
			} else {
				setSendeEpostFlagg(0L);
			}
			firePropertyChange("sendeEpost", old, epostFlagg);
		}
	}
	
    public String getType() {
		return resolvePropertyText("eier");
	}
	
	@Transient
	public Boolean getEkstraDagsrapport() {
		return ekstraDagsrapport;
	}
	
	public void setEkstraDagsrapport(Boolean ekstraDagsrapport) {
		Boolean old = this.ekstraDagsrapport;
		this.ekstraDagsrapport = ekstraDagsrapport;
		firePropertyChange("ekstraDagsrapport", old, ekstraDagsrapport);
		try {
			fireVetoableChange("ekstraDagsrapport", old, ekstraDagsrapport);
			firePropertyChange("ekstraDagsrapport", old, ekstraDagsrapport);
		} catch (PropertyVetoException e) {
			this.ekstraDagsrapport = old;
		}
	}
	public Integer getNumber() {
		return null;
	}
	@Transient
	public String getKontaktnavn() {
		return navn;
	}
	@Transient
	public String getKontakttypeNavn() {
		return "Eier";
	}
	@Transient
	public String getTelefonnummer() {
		if (tlfnummer1!=null)
			return tlfnummer1.toString();
		return null;
	}
	@Transient
	public String getAdressePoststed() {
		if (adresse!=null&&postnummer!=null){
			return adresse+", "+postnummer;
		}
		if (adresse!=null)
			return adresse;
		if (postnummer!=null)
			return postnummer.toString();
		return null;
	}
}
