package no.mesta.mipss.persistence.mipssfield.r.common;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * Egenskapverdi
 *
 * Definerer en verdi til en @see Egenskap
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

@SuppressWarnings("serial")
@Entity(name=Egenskapverdi.BEAN_NAME)
@Table(name="EGENSKAPVERDI")
@NamedQueries({
	@NamedQuery(name = Egenskapverdi.QUERY_FIND_BY_EGENSKAP, query = "SELECT e FROM Egenskapverdi e where e.egenskap.id=:id")
})
public class Egenskapverdi extends MipssEntityBean<Egenskapverdi>{

	public static final String BEAN_NAME = "Egenskapverdi";
	public static final String QUERY_FIND_BY_EGENSKAP = "Egenskap.findByEgenskap";

	@ManyToOne(cascade=CascadeType.REFRESH)
	@JoinColumn(name = "EGENSKAP_ID", referencedColumnName = "ID")
	private Egenskap egenskap;
	@Column(name="GUI_REKKEFOLGE")
	private Long guiRekkefolge;
	@Id
	@Column(nullable = false)
	private Long id;
	private String verdi;

	public Egenskap getEgenskap() {
		return this.egenskap;
	}

	public Long getGuiRekkefolge() {
		return this.guiRekkefolge;
	}

	public Long getId() {
		return this.id;
	}
	public String getVerdi() {
		return this.verdi;
	}

	public void setEgenskap(final Egenskap egenskap) {
		final Egenskap old = this.egenskap;
		this.egenskap = egenskap;
		firePropertyChange("egenskap", old, egenskap);
	}
	public void setGuiRekkefolge(final Long guiRekkefolge) {
		final Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}
	public void setId(final Long id) {
		final Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	public void setVerdi(final String verdi) {
		final String old = this.verdi;
		this.verdi = verdi;
		firePropertyChange("verdi", old, verdi);
	}
	@Override
	public String toString(){
		return this.verdi;
	}
}
