package no.mesta.mipss.persistence.kontrakt;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name=Samproduksjon.FIND_ALL,
                query="select s from Samproduksjon s")
})
@Entity
public class Samproduksjon implements IRenderableMipssEntity {

    public static final String FIND_ALL = "Samproduksjon.FindAll";

    @Id
    private int id;
    @Column
    private String navn;

    @Override
    public String getTextForGUI() {
        return navn;
    }

}