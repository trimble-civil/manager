package no.mesta.mipss.persistence;


public interface IOwnableMipssEntity {

    /**
     * Henter det @Embedded objektet som inneholder identifikasjon av de som 
     * opprettet og evt. endret entiteten.
     * Implementasjonen skal aldri returnere null.
     * @return {@code OwnedMipssEntity} instanse. Ikke null.
     */
    public OwnedMipssEntity getOwnedMipssEntity();
}
