package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA entitet generert i JDeveloper.
 * Mange-til-mage forhold mellom Kjoretoyordre og Kjoretoyordrestatus.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity(name="KjoretoyordreStatus")

@NamedQueries({
	@NamedQuery(name = Kjoretoyordre_Status.FIND_ALL, query = "select o from KjoretoyordreStatus o")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

@Table(name = "KJORETOYORDRE_STATUS")
@IdClass(Kjoretoyordre_StatusPK.class)

public class Kjoretoyordre_Status extends MipssEntityBean<Kjoretoyordre_Status> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity  {
	public static final String FIND_ALL = "Kjoretoyordre_Status.findAll";
	
    private String fritekst;
    private Long ordreId;
    private String statusIdent;
    private Kjoretoyordre kjoretoyordre;
    private Kjoretoyordrestatus kjoretoyordrestatus;

    // felt lagt til for h�nd
    private OwnedMipssEntity ownedMipssEntity;
    
    public Kjoretoyordre_Status() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    @Id
    @Column(name="ORDRE_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getOrdreId() {
        return ordreId;
    }

    public void setOrdreId(Long ordreId) {
        this.ordreId = ordreId;
    }

    @Id
    @Column(name="STATUS_IDENT", nullable = false, insertable = false, 
        updatable = false)
    public String getStatusIdent() {
        return statusIdent;
    }

    public void setStatusIdent(String statusIdent) {
        this.statusIdent = statusIdent;
    }

    @ManyToOne
    @JoinColumn(name = "ORDRE_ID", referencedColumnName = "ID")
    public Kjoretoyordre getKjoretoyordre() {
        return kjoretoyordre;
    }

    public void setKjoretoyordre(Kjoretoyordre kjoretoyordre) {
        this.kjoretoyordre = kjoretoyordre;
        
        if(kjoretoyordre != null) {
        	setOrdreId(kjoretoyordre.getId());
        } else {
        	setOrdreId(null);
        }
    }

    @ManyToOne
    @JoinColumn(name = "STATUS_IDENT", referencedColumnName = "IDENT")
    public Kjoretoyordrestatus getKjoretoyordrestatus() {
        return kjoretoyordrestatus;
    }

    public void setKjoretoyordrestatus(Kjoretoyordrestatus kjoretoyordrestatus) {
        this.kjoretoyordrestatus = kjoretoyordrestatus;
        
        if(kjoretoyordrestatus != null) {
        	setStatusIdent(kjoretoyordrestatus.getIdent());
        } else {
        	setStatusIdent(null);
        }
    }

    // lagt til for h�nd
    
    /** {@inheritDoc} */
    @Transient
	public String getTextForGUI() {
		return getStatusIdent() + " - " + getOrdreId();
	}

    /** {@inheritDoc} */
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    /** {@inheritDoc} */
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("status", getStatusIdent()).
            append("orde ID", getOrdreId()).
            toString();
    }

    /**
     * Sammenlikner Id-n�kkelen med o's Id-n�kkel.
     * @param o
     * @return true hvis n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kjoretoyordre_Status)) {
            return false;
        }
        Kjoretoyordre_Status other = (Kjoretoyordre_Status) o;
        return new EqualsBuilder().
            append(getStatusIdent(), other.getStatusIdent()).
            append(getOrdreId(), getOrdreId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun p� Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getStatusIdent()).
            append(getOrdreId()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	Kjoretoyordre_Status other = (Kjoretoyordre_Status) o;
		return new CompareToBuilder().
			append(getStatusIdent(), other.getStatusIdent()).
			append(getOrdreId(), other.getOrdreId()).
			toComparison();
    }
    
    @Transient
    public Date getOpprettetDato() {
    	return ownedMipssEntity.getOpprettetDato();
    }
}
