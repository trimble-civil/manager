package no.mesta.mipss.persistence.driftslogg;

import java.io.Serializable;
import java.util.Date;

public class PeriodeStatusPK implements Serializable{
	private Long periodeId;
	private Date opprettetDato;
	
	public PeriodeStatusPK(){
		
	}
	public PeriodeStatusPK(Long periodeId, Date opprettetDato){
		this.setPeriodeId(periodeId);
		this.setOpprettetDato(opprettetDato);
		
	}
	public void setPeriodeId(Long periodeId) {
		this.periodeId = periodeId;
	}
	public Long getPeriodeId() {
		return periodeId;
	}
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}

}
