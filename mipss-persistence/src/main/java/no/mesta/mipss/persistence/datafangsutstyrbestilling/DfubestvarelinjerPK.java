package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class DfubestvarelinjerPK {
	private Long vareId;
	private Long levbestId;
	
	public DfubestvarelinjerPK(Long vareId, Long levbestId) {
		this.vareId = vareId;
		this.levbestId = levbestId;
	}

	public DfubestvarelinjerPK() {
	}

	public Long getVareId() {
		return vareId;
	}

	public void setVareId(Long vareId) {
		this.vareId = vareId;
	}

	public Long getLevbestId() {
		return levbestId;
	}

	public void setLevbestId(Long levbestId) {
		this.levbestId = levbestId;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(vareId).append(levbestId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof DfubestvarelinjerPK)) {return false;}
        
        DfubestvarelinjerPK other = (DfubestvarelinjerPK) o;
        
        return new EqualsBuilder().append(vareId, other.vareId).append(levbestId, other.levbestId).isEquals();
    }
}
