package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * PK klasse for trafikktiltak
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class TrafikktiltakPK implements Serializable {
    private String hendelseGuid;
    private Long typeId;
    
    /**
     * Konstruktør
     * 
     */
    public TrafikktiltakPK() {
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof TrafikktiltakPK)) {return false;}
        
        TrafikktiltakPK other = (TrafikktiltakPK) o;
        return new EqualsBuilder().append(hendelseGuid, other.getHendelseGuid()).
            append(typeId, other.getTypeId()).isEquals();
    }

    /**
     * Hendelses guid
     * 
     * @return
     */
    public String getHendelseGuid() {
        return hendelseGuid;
    }

    /**
     * Type id
     * 
     * @return
     */
    public Long getTypeId() {
        return typeId;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(hendelseGuid).append(typeId).toHashCode();
    }
    
    /**
     * Hendelses guid
     * 
     * @param hendelseGuid
     */
    public void setHendelseGuid(String hendelseGuid) {
        this.hendelseGuid = hendelseGuid;
    }

    /**
     * Type id
     * 
     * @param typeId
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("hendelseGuid", hendelseGuid).
            append("typeId", typeId).
            toString();
    }
}
