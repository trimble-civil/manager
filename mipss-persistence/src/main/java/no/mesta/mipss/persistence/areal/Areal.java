package no.mesta.mipss.persistence.areal;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Areal
 * @author <a href="mailto:larse@mesan.no">Lars A. Eidsheim</a>
 *
 */
/**
 * @author lareid
 *
 */
@SuppressWarnings("serial")
@NamedQuery(name = "Areal.findAll", query = "select o from Areal o")
@Entity
@SequenceGenerator(name = Areal.ID_SEQ, sequenceName = "AREAL_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Areal extends MipssEntityBean<Areal> implements Serializable, IOwnableMipssEntity, IRenderableMipssEntity {
	public static final String ID_SEQ = "arealIdSeq";
	public static final String FIND_ALL = "Areal.findAll";
	private Long id;
//	private String typeNavn;
	private String navn;
	private String fritekst;
	private Double x1;
	private Double y1;
	private Double deltaX;
	private Double deltaY;
	private Long vinkelAlfa;
	private String url;
	private String guiFarge;
	private Long guiTykkelse;
	private String guiTekst;
	
	private Arealtype arealtype;
	
	private OwnedMipssEntity ownedMipssEntity = null;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	
	public Areal() {
		
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Areal.ID_SEQ)
    @Column(nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	
//	@Column(name="TYPE_NAVN", nullable = true, insertable = false, updatable = false)
//	public String getTypeNavn() {
//		return typeNavn;
//	}
//	
//	public void setTypeNavn(String typeNavn) {
//		this.typeNavn = typeNavn;
//	}
	
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	
	public String getFritekst() {
		return fritekst;
	}
	
	public void setFritekst(String fritekst) {
		String old = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", old, fritekst);
	}
	
	public Double getX1() {
		return x1;
	}
	
	public void setX1(Double x1) {
		Double old = this.x1;
		this.x1 = x1;
		firePropertyChange("x1", old, x1);
	}
	
	public Double getY1() {
		return y1;
	}
	
	public void setY1(Double y1) {
		Double old = this.y1;
		this.y1 = y1;
		firePropertyChange("y1", old, y1);
	}
	
	@Column(name = "DELTA_X")
	public Double getDeltaX() {
		return deltaX;
	}
	
	public void setDeltaX(Double deltaX) {
		Double old = this.deltaX;
		this.deltaX = deltaX;
		firePropertyChange("deltaX", old, deltaX);
	}
	
	@Column(name = "DELTA_Y")
	public Double getDeltaY() {
		return deltaY;
	}
	
	public void setDeltaY(Double deltaY) {
		Double old = this.deltaY;
		this.deltaY = deltaY;
		firePropertyChange("deltaY", old, deltaY);
	}
	
	@Column(name = "VINKEL_ALFA")
	public Long getVinkelAlfa() {
		return vinkelAlfa;
	}
	
	public void setVinkelAlfa(Long vinkelAlfa) {
		Long old = this.vinkelAlfa;
		this.vinkelAlfa = vinkelAlfa;
		firePropertyChange("vinkelAlfa", old, vinkelAlfa);
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		String old = this.url;
		this.url = url;
		firePropertyChange("url", old, url);
	}
	
	@Column(name="GUI_FARGE")
	public String getGuiFarge() {
		return guiFarge;
	}
	
	public void setGuiFarge(String guiFarge) {
		String old = this.guiFarge;
		this.guiFarge = guiFarge;
		firePropertyChange("guiFarge", old, guiFarge);
	}
	
	@Column(name="GUI_TYKKELSE")
	public Long getGuiTykkelse() {
		return guiTykkelse;
	}
	
	public void setGuiTykkelse(Long guiTykkelse) {
		Long old = this.guiTykkelse;
		this.guiTykkelse = guiTykkelse;
		firePropertyChange("guiTykkelse", old, guiTykkelse);
	}
	
	@Column(name="GUI_TEKST")
	public String getGuiTekst() {
		return guiTekst;
	}
	
	public void setGuiTekst(String guiTekst) {
		String old = this.guiTekst;
		this.guiTekst = guiTekst;
		firePropertyChange("guiTekst", old, guiTekst);
	}
	
	@ManyToOne
    @JoinColumn(name = "TYPE_NAVN", referencedColumnName = "NAVN")
	public Arealtype getArealtype() {
		return arealtype;
	}

	public void setArealtype(Arealtype arealtype) {
		Arealtype old = this.arealtype;
        this.arealtype = arealtype;
        
        props.firePropertyChange("arealtype", old, arealtype);
        
//        if(arealtype != null) {
//            setTypeNavn(arealtype.getNavn());
//        } else {
//        	setTypeNavn(null);
//        }
	}

	@Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Areal)) {return false;}
        
        Areal other = (Areal) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(731,741).append(id).toHashCode();
    }

	public String getTextForGUI() {
		return navn;
	}
}
