package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Punktveiref
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name="PUNKTVEIREF_XV")
@NamedQueries( { @NamedQuery(name = Punktveiref.QUERY_FIND_ALL, query = "SELECT p FROM Punktveiref p") })
@IdClass(PunktveirefPK.class)
public class Punktveiref extends MipssEntityBean<Punktveiref> implements Serializable, Comparable<Punktveiref>,
		IRenderableMipssEntity, Cloneable {
	private static final String FIELD_NAME_FRADATO = "fraDato";
	private static final String FIELD_NAME_FYLKESNUMMER = "fylkesnummer";
	private static final String FIELD_NAME_HP = "hp";
	private static final String FIELD_NAME_KJOREFELT = "kjorefelt";
	private static final String FIELD_NAME_KOMMUNENUMMER = "kommunenummer";
	private static final String FIELD_NAME_METER = "meter";
	private static final String FIELD_NAME_STEDFESTGUID = "stedfestGuid";
	private static final String FIELD_NAME_TEXTFORGUI = "textForGUI";
	private static final String FIELD_NAME_VEIKATEGORI = "veikategori";
	private static final String FIELD_NAME_VEINUMMER = "veinummer";
	private static final String FIELD_NAME_VEISTATUS = "veistatus";
	private static final String ID_NAME1 = FIELD_NAME_STEDFESTGUID;
	private static final String ID_NAME2 = FIELD_NAME_FRADATO;
	private static final String[] ID_NAMES = new String[] { ID_NAME1, ID_NAME2 };
	public static final String QUERY_FIND_ALL = "Punktveiref.findAll";
	private Date fraDato;
	private int fylkesnummer;
	private int hp;
	private String kjorefelt = " ";
	private int kommunenummer;
	private int meter;
	private Punktstedfest punktstedfest;
	private String stedfestGuid;
	private String veikategori;
	private int veinummer;
	private String veistatus;

	/**
	 * Konstruktør
	 * 
	 */
	public Punktveiref() {
	}

	public Punktveiref(Punktveiref vei) {
		fraDato = vei.getFraDato();
		fylkesnummer = vei.getFylkesnummer();
		hp = vei.getHp();
		kjorefelt = vei.getKjorefelt();
		kommunenummer = vei.getKommunenummer();
		meter = vei.getMeter();
		veikategori = vei.getVeikategori();
		veinummer = vei.getVeinummer();
		veistatus = vei.getVeistatus();
	}

	/** {@inheritDoc} */
	public int compareTo(Punktveiref o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(o.getFraDato(), getFraDato()).toComparison();
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(name = "FRA_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFraDato() {
		return fraDato;
	}

	/**
	 * Fylke
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getFylkesnummer() {
		return fylkesnummer;
	}

	/**
	 * Hovedparcell
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getHp() {
		return hp;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/**
	 * Kjørefelt
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public String getKjorefelt() {
		return kjorefelt;
	}

	/**
	 * Regner meter om til km
	 * 
	 * @return
	 */
	@Transient
	public Double getKm() {
		if (meter != 0) {
			return new Double((meter / 1000.0d));
		} else {
			return null;
		}
	}

	/**
	 * Kommune
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getKommunenummer() {
		return kommunenummer;
	}

	/**
	 * Meter
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getMeter() {
		return meter;
	}

	/**
	 * Stedfesting
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "STEDFEST_GUID", referencedColumnName = "GUID")
	public Punktstedfest getPunktstedfest() {
		return punktstedfest;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(name = "STEDFEST_GUID", nullable = false, insertable = false, updatable = false)
	public String getStedfestGuid() {
		return stedfestGuid;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		return VeiInfoUtil.getVeiStedfest(this);
	}

	/**
	 * Lager tekst med vei informasjon uten hp og meter
	 * 
	 * @return
	 */
	public String getVeiGUIText() {
		String fylke = (getFylkesnummer() > 0 ? String.valueOf(getFylkesnummer()) : "");
		fylke = StringUtils.leftPad(fylke, 2, " ");

		String kommune = (getKommunenummer() > 0 ? String.valueOf(getKommunenummer())
				: "");
		kommune = StringUtils.leftPad(kommune, 2, " ");

		String veinummer = (getVeinummer() > 0 ? String.valueOf(getVeinummer()) : "");
		veinummer = StringUtils.leftPad(veinummer, 2, " ");

		return fylke + " " + kommune + getVeikategori() + getVeistatus() + " " + veinummer;
	}

	/**
	 * Vei kat
	 * 
	 * @return
	 */
	@Column(nullable = false, length = 1)
	public String getVeikategori() {
		return veikategori;
	}

	/**
	 * Vei nr
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getVeinummer() {
		return veinummer;
	}

	/**
	 * Status
	 * 
	 * @return
	 */
	@Column(nullable = false, length = 1)
	public String getVeistatus() {
		return veistatus;
	}

	/**
	 * Id
	 * 
	 * @param fraDato
	 */
	public void setFraDato(Date fraDato) {
		Date old = this.fraDato;
		this.fraDato = fraDato;
		firePropertyChange(FIELD_NAME_FRADATO, old, fraDato);
	}

	/**
	 * Fylke
	 * 
	 * @param fylkesnummer
	 */
	public void setFylkesnummer(int fylkesnummer) {
		String oldTxt = getTextForGUI();
		int old = this.fylkesnummer;
		this.fylkesnummer = fylkesnummer;
		firePropertyChange(FIELD_NAME_FYLKESNUMMER, old, fylkesnummer);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Hovedparcell
	 * 
	 * @param hp
	 */
	public void setHp(int hp) {
		String oldTxt = getTextForGUI();
		int old = this.hp;
		this.hp = hp;
		firePropertyChange(FIELD_NAME_HP, old, hp);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Kjørefelt
	 * 
	 * @param kjorefelt
	 */
	public void setKjorefelt(String kjorefelt) {
		String oldTxt = getTextForGUI();
		String old = this.kjorefelt;
		this.kjorefelt = kjorefelt;
		firePropertyChange(FIELD_NAME_KJOREFELT, old, kjorefelt);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Kommune
	 * 
	 * @param kommunenummer
	 */
	public void setKommunenummer(int kommunenummer) {
		String oldTxt = getTextForGUI();
		int old = this.kommunenummer;
		this.kommunenummer = kommunenummer;
		firePropertyChange(FIELD_NAME_KOMMUNENUMMER, old, kommunenummer);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Meter
	 * 
	 * @param meter
	 */
	public void setMeter(int meter) {
		String oldTxt = getTextForGUI();
		int old = this.meter;
		this.meter = meter;
		firePropertyChange(FIELD_NAME_METER, old, meter);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Stedfesting
	 * 
	 * @param punktstedfest
	 */
	public void setPunktstedfest(Punktstedfest punktstedfest) {
		Punktstedfest old = this.punktstedfest;
		this.punktstedfest = punktstedfest;
		firePropertyChange("punktstedfest", old, punktstedfest);

		if (punktstedfest != null) {
			setStedfestGuid(punktstedfest.getGuid());
		} else {
			setStedfestGuid(null);
		}
	}

	/**
	 * Id
	 * 
	 * @param stedfestGuid
	 */
	public void setStedfestGuid(String stedfestGuid) {
		String old = this.stedfestGuid;
		this.stedfestGuid = stedfestGuid;
		firePropertyChange(FIELD_NAME_STEDFESTGUID, old, stedfestGuid);
	}

	/**
	 * Vei kat
	 * 
	 * @param veikategori
	 */
	public void setVeikategori(String veikategori) {
		String oldTxt = getTextForGUI();
		String old = this.veikategori;
		this.veikategori = veikategori;
		firePropertyChange(FIELD_NAME_VEIKATEGORI, old, veikategori);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Vei nr
	 * 
	 * @param veinummer
	 */
	public void setVeinummer(int veinummer) {
		String oldTxt = getTextForGUI();
		int old = this.veinummer;
		this.veinummer = veinummer;
		firePropertyChange(FIELD_NAME_VEINUMMER, old, veinummer);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Status
	 * 
	 * @param veistatus
	 */
	public void setVeistatus(String veistatus) {
		String oldTxt = getTextForGUI();
		String old = this.veistatus;
		this.veistatus = veistatus;
		firePropertyChange(FIELD_NAME_VEISTATUS, old, veistatus);
		firePropertyChange(FIELD_NAME_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(FIELD_NAME_STEDFESTGUID, stedfestGuid).append(FIELD_NAME_FRADATO,
				fraDato).append(FIELD_NAME_FYLKESNUMMER, fylkesnummer).append(FIELD_NAME_KOMMUNENUMMER, kommunenummer)
				.append(FIELD_NAME_VEIKATEGORI, veikategori).append(FIELD_NAME_VEISTATUS, veistatus).append(
						FIELD_NAME_VEINUMMER, veinummer).append(FIELD_NAME_HP, hp).append(FIELD_NAME_METER, meter)
				.append(FIELD_NAME_KJOREFELT, kjorefelt).toString();
	}
	
	@Override
	public Punktveiref clone(){
		Punktveiref v = new Punktveiref();
		v.setFylkesnummer(fylkesnummer);
		v.setHp(hp);
		v.setKjorefelt(kjorefelt);
		v.setKommunenummer(kommunenummer);
		v.setMeter(meter);
		v.setVeikategori(veikategori);
		v.setVeinummer(veinummer);
		v.setVeistatus(veistatus);
		return v;
	}
}
