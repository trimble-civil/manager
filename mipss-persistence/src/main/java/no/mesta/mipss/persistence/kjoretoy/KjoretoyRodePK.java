package no.mesta.mipss.persistence.kjoretoy;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class KjoretoyRodePK implements Serializable {
    private Long kjoretoyId;
    private Date fraTidspunkt;
    private Long rodeId;

    public  KjoretoyRodePK() {
    }

    public KjoretoyRodePK(Long kjoretoyId,Date fraTidspunkt,Long rodeId) {
        this.kjoretoyId = kjoretoyId;
        this.fraTidspunkt = fraTidspunkt;
        this.rodeId = rodeId;
    }

    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    public Date getFraDato() {
        return fraTidspunkt;
    }

    public void setFraDato(Date fraDato) {
        this.fraTidspunkt = fraDato;
    }

    public Long getRodeId() {
        return rodeId;
    }

    public void setRodeId(Long rodeId) {
        this.rodeId = rodeId;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kjoretoyId).append(fraTidspunkt).toHashCode();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KjoretoyRodePK)) {return false;}

        KjoretoyRodePK other = (KjoretoyRodePK) o;

        return new EqualsBuilder().append(kjoretoyId, other.kjoretoyId).append(rodeId,other.rodeId).append(fraTidspunkt, other.fraTidspunkt).isEquals();
    }
}
