package no.mesta.mipss.persistence.inndata;

import java.io.Serializable;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * PK for Prodpunktdetalj.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class ProdpunktdetaljPK implements Serializable {
	private Long dfuId;
	private Date gmtTidspunkt;

	/**
     * 
     */
	public ProdpunktdetaljPK() {
	}

	/**
	 * 
	 * @param dfuId
	 * @param gmtTidspunkt
	 */
	public ProdpunktdetaljPK(Long dfuId, Date gmtTidspunkt) {
		this.dfuId = dfuId;
		this.gmtTidspunkt = gmtTidspunkt;
	}

	/**
	 * set dfuId
	 * 
	 * @param dfuId
	 */
	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}

	/**
	 * set gmtTidspunkt
	 * 
	 * @param gmtTidspunkt
	 */
	public void setGmtTidspunkt(Date gmtTidspunkt) {
		this.gmtTidspunkt = gmtTidspunkt;
	}

	/**
	 * Returnerer dfuId
	 * 
	 * @return
	 */
	public Long getDfuId() {
		return dfuId;
	}

	/**
	 * Returnerer gmtTidspunkt
	 * 
	 * @return
	 */
	public Date getGmtTidspunkt() {
		return gmtTidspunkt;
	}

	/**
	 * @see java.lang.Object#toString()
	 * @see org.apache.commons.lang.builder.ToStringBuilder
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("dfuId", dfuId).append("gmtTidspunkt", gmtTidspunkt).toString();
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 * @see org.apache.commons.lang.builder.EqualsBuilder
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof ProdpunktdetaljPK)) {
			return false;
		}

		ProdpunktdetaljPK other = (ProdpunktdetaljPK) o;
		return new EqualsBuilder().append(dfuId, other.getDfuId()).append(gmtTidspunkt, other.getGmtTidspunkt())
				.isEquals();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 * @see org.apache.commons.lang.builder.HashCodeBuilder
	 * 
	 * @return
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(761, 771).append(dfuId).append(gmtTidspunkt).toHashCode();
	}
}
