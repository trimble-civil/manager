package no.mesta.mipss.persistence.inndata;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kvernetf1.Prodpunktinfo;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Prodpunkt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Prodpunkt.QUERY_FIND_ALL, query = "SELECT p FROM Prodpunkt p"),
    @NamedQuery(name = Prodpunkt.QUERY_FIND, query= "SELECT p FROM Prodpunkt p WHERE p.dfuId=:dfuId AND p.gmtTidspunkt >=:fraTidspunkt AND p.gmtTidspunkt < :tilTidspunkt AND p.breddegrad IS NOT NULL and p.lengdegrad IS NOT NULL")
    })
@IdClass(ProdpunktPK.class)
public class Prodpunkt implements Serializable {
    public static final String QUERY_FIND_ALL = "Prodpunkt.findAll";
    public static final String QUERY_FIND = "Prodpunkt.findPunkt";
    
    private Long dfuId;
    private Date gmtTidspunkt;
    private String inspeksjonGuid;
    private String breddegrad;
    private String lengdegrad;
    private Double hastighet;
    private Double retning;
    private Float temperatur;
    private String digi;
    private String ignorerMaske;
    private Date opprettetDato;
    private Dfuindivid dfu;
    private Inspeksjon inspeksjon;
    private Prodpunktinfo prodpunktinfo;
    
    /**
     * Konstruktør
     * 
     */
    public Prodpunkt() {
    }

    /**
     * Breddegrad
     * 
     * @return
     */
    public String getBreddegrad() {
        return breddegrad;
    }

    /**
     * Breddegrad
     * 
     * @param breddegrad
     */
    public void setBreddegrad(String breddegrad) {
        this.breddegrad = breddegrad;
    }

    /**
     * Id
     * 
     * @return
     */
    @Id
    @Column(name="DFU_ID", nullable = false, insertable = false, updatable = false)
    public Long getDfuId() {
        return dfuId;
    }

    /**
     * Id
     * 
     * @param dfuId
     */
    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    /**
     * Digi
     * 
     * @return
     */
    @Column(nullable = false)
    public String getDigi() {
        return digi;
    }
    
    @Transient 
    public String getDigiAndMask(){
    	if (getIgnorerMaske()!=null){
    		String im = getIgnorerMaske();
    		String di = getDigi();    		
    		Integer mask = Integer.parseInt(im, 16);
    		Integer digi = Integer.parseInt(di, 16);
    		int digiAndMask = digi&mask;
    		String dm = Integer.toHexString(digiAndMask);
    		while (dm.length()<4)
    			dm = 0+dm;
    		return dm;
    	}
    	return getDigi();
    }
    /**
     * Digi
     * 
     * @param digi
     */
    public void setDigi(String digi) {
        this.digi = digi;
    }

    /**
     * Tidspunkt
     * 
     * @return
     */
    @Id
    @Column(name="GMT_TIDSPUNKT", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getGmtTidspunkt() {
        return gmtTidspunkt;
    }

    /**
     * Tidspunkt
     * 
     * @param gmtTidspunkt
     */
    public void setGmtTidspunkt(Date gmtTidspunkt) {
        this.gmtTidspunkt = gmtTidspunkt;
    }

    /**
     * Hastighet
     * 
     * @return
     */
    public Double getHastighet() {
        return hastighet;
    }

    /**
     * Hastighet
     * 
     * @param hastighet
     */
    public void setHastighet(Double hastighet) {
        this.hastighet = hastighet;
    }

    /**
     * Maske
     * 
     * @return
     */
    @Column(name="IGNORER_MASKE")
    public String getIgnorerMaske() {
        return ignorerMaske;
    }

    /**
     * Maske
     * 
     * @param ignorerMaske
     */
    public void setIgnorerMaske(String ignorerMaske) {
        this.ignorerMaske = ignorerMaske;
    }

    /**
     * 
     * Inspeksjon
     * 
     * @return
     */
    @Column(name="INSPEKSJON_GUID", nullable = false, insertable = false, updatable = false) 
    public String getInspeksjonGuid() {
        return inspeksjonGuid;
    }

    /**
     * Inspeksjon
     * 
     * @param inspeksjonGuid
     */
    public void setInspeksjonGuid(String inspeksjonGuid) {
        this.inspeksjonGuid = inspeksjonGuid;
    }

    /**
     * Lengdegrad
     * 
     * @return
     */
    public String getLengdegrad() {
        return lengdegrad;
    }

    /**
     * Lengdegrad
     * 
     * @param lengdegrad
     */
    public void setLengdegrad(String lengdegrad) {
        this.lengdegrad = lengdegrad;
    }

    /**
     * N�r ble den registrert
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r ble den registrert
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Retning
     * 
     * @return
     */
    public Double getRetning() {
        return retning;
    }

    /**
     * Retning
     * 
     * @param retning
     */
    public void setRetning(Double retning) {
        this.retning = retning;
    }

    /**
     * Temp
     * 
     * @return
     */
    public Float getTemperatur() {
        return temperatur;
    }

    /**
     * Temp
     * 
     * @param temperatur
     */
    public void setTemperatur(Float temperatur) {
        this.temperatur = temperatur;
    }

    /**
     * Dfu
     * 
     * @param dfu
     */
    public void setDfu(Dfuindivid dfu) {
        this.dfu = dfu;
        
        if(dfu != null) {
            setDfuId(dfu.getId());
        } else {
            setDfuId(null);
        }
    }

    /**
     * Dfu
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "DFU_ID", referencedColumnName = "ID")
    public Dfuindivid getDfu() {
        return dfu;
    }

    /**
     * Inspeksjon
     * 
     * @param inspeksjon
     */
    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
    }

    /**
     * Inspeksjon
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID")
    public Inspeksjon getInspeksjon() {
        return inspeksjon;
    }
    
    
    @OneToOne
    @JoinColumns({
        @JoinColumn(name = "DFU_ID", referencedColumnName = "DFU_ID", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "GMT_TIDSPUNKT", referencedColumnName = "GMT_TIDSPUNKT", nullable = false, insertable = false, updatable = false)
    })
    public Prodpunktinfo getProdpunktinfo() {
        return prodpunktinfo;
    }

    public void setProdpunktinfo(Prodpunktinfo prodpunktinfo) {
        this.prodpunktinfo = prodpunktinfo;
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("dfuId", dfuId).
            append("gmtTidspunkt", gmtTidspunkt).
            append("inspeksjonGuid", inspeksjonGuid).
            append("breddegrad", breddegrad).
            append("lengdegrad", lengdegrad).
            append("hastighet", hastighet).
            append("retning", retning).
            append("temperatur", temperatur).
            append("digi", digi).
            append("ignorerMaske", ignorerMaske).
            append("opprettetDato", opprettetDato).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Prodpunkt)) {return false;}
        
        Prodpunkt other = (Prodpunkt) o;
        return new EqualsBuilder().append(dfuId, other.getDfuId()).
            append(gmtTidspunkt, other.getGmtTidspunkt()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2025,2075).append(dfuId).append(gmtTidspunkt).toHashCode();
    }
    
}
