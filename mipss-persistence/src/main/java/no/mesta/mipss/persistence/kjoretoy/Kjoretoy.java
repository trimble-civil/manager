package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.klipping.Klippeperiode;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.stroing.Stroperiode;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.eclipse.persistence.indirection.IndirectList;

/**
 * Generert opprinnelig i JDeveloper.
 * Beriket med flere mange-til-mange og en-til-mange forhold, propertyChangeListening og automatisert signering.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity(name = Kjoretoy.BEAN_NAME)

@SequenceGenerator(name = "kjoretoyIdSeq", sequenceName = "KJORETOY_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = Kjoretoy.FIND_ALL, query = "select o from Kjoretoy o order by o.regnr, o.maskinnummer, o.eksterntNavn"),
    @NamedQuery(name = Kjoretoy.FIND_ALL_EAGER, query = "select o from Kjoretoy o order by o.regnr, o.maskinnummer, o.eksterntNavn"),
    @NamedQuery(name = Kjoretoy.FIND_RICH, query = "select distinct k from Kjoretoy k left join fetch k.stroperiodeList where k.id = :id", 
    		hints={@QueryHint(name=QueryHints.REFRESH, value=HintValues.TRUE)}),
    @NamedQuery(name= Kjoretoy.FIND_BY_KONTRAKT, query="select k from Kjoretoy k JOIN k.kontraktKjoretoyList kkl where kkl.kontraktId=:id"),
    @NamedQuery(name= Kjoretoy.FIND_BY_LEVERANDOR, query="select k from Kjoretoy k where k.leverandor.nr=:nr"),
    @NamedQuery(name= Kjoretoy.FIND_BY_KONTRAKT_AND_KJORETOY, query="select k from Kjoretoy k JOIN k.kontraktKjoretoyList kkl where kkl.kontraktId=:id and kkl.kjoretoyId=:kid"),
    @NamedQuery(name= Kjoretoy.FIND_EAGER_BY_KONTRAKT, query="select distinct k from Kjoretoy k JOIN FETCH k.leverandor JOIN k.kontraktKjoretoyList kkl where kkl.kontraktId=:id")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

public class Kjoretoy extends MipssEntityBean<Kjoretoy> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	private static transient Logger logger = null;
	public static final String BEAN_NAME = "Kjoretoy";
	public static final String FIND_ALL = "Kjoretoy.findAll";
	public static final String FIND_ALL_EAGER = "Kjoretoy.findAllEager";
    public static final String FIND_RICH = "Kjoretoy.findRich";
    public static final String FIND_BY_KONTRAKT = "Kjoretoy.findByKontrakt";
    public static final String FIND_EAGER_BY_KONTRAKT = "Kjoretoy.findEagerByKontrakt";
    public static final String FIND_BY_KONTRAKT_AND_KJORETOY = "Kjoretoy.findByKontraktAndKjoretoy";
    public static final String FIND_BY_LEVERANDOR = "Kjoretoy.findByLeverandor";
    
    public static final String ID_IKKE_SATT = "Ikke satt";
    
    private static final String[] ID_NAMES = {"id"};
    
    private String eksterntNavn;
    private String fritekst;
    private String guitekst;
    private Long id;
    private Long maskinnummer;
    private String regnr;
    private Long slettetFlagg;
    private List<KjoretoyKontakttype> kjoretoyKontaktypeList = new ArrayList<KjoretoyKontakttype>();
    private List<Bilde> bildeList = new ArrayList<Bilde>();
    private Kjoretoymodell kjoretoymodell;
    private List<Dokument> dokumentList = new ArrayList<Dokument>();
    private Leverandor leverandor;
    private List<KjoretoyUtstyr> kjoretoyUtstyrList = new ArrayList<KjoretoyUtstyr>();
    private Kjoretoytype kjoretoytype;
    private List<Installasjon> installasjonList = new ArrayList<Installasjon>();
    private List<Kjoretoyordre> kjoretoyordreList = new ArrayList<Kjoretoyordre>();
    private List<Kjoretoylogg> kjoretoyloggList = new ArrayList<Kjoretoylogg>();
    private Kjoretoyeier kjoretoyeier;

    // Ikke generert fra databasen:
    private Installasjon currentInstallasjon;
    private List<KjoretoyUtstyr> currentUtstyrList;
    private List<Selskap> selskapList = new ArrayList<Selskap>();
    private List<KontraktKjoretoy> kontraktKjoretoyList = new ArrayList<KontraktKjoretoy>();
    private OwnedMipssEntity ownedMipssEntity;
    private List<Stroperiode> stroperiodeList = new ArrayList<Stroperiode>();
    private List<Klippeperiode> klippeperiodeList = new ArrayList<Klippeperiode>();
    private List<KjoretoyRode> kjoretoyRodeList = new ArrayList<KjoretoyRode>();

    public Kjoretoy() {
    	log("Kjoretoy-instans opprettet");
    }

    @Column(name="EKSTERNT_NAVN", nullable = false)
    public String getEksterntNavn() {
    	log("henter eksternt navn");
        return eksterntNavn;
    }

    public void setEksterntNavn(String eksterntNavn) {
    	log("setter eksterntnavn");
    	String old = this.eksterntNavn;
        this.eksterntNavn = eksterntNavn;
        
        getProps().firePropertyChange("eksterntNavn", old, eksterntNavn);
    }

    public String getFritekst() {
    	log("henter fritekst");
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	log("setter fritekst");
    	String old = this.fritekst;
        this.fritekst = fritekst;
        
        getProps().firePropertyChange("fritekst", old, fritekst);
    }

    @Column(nullable = false)
    public String getGuitekst() {
    	log("henter guitekst");
        return guitekst;
    }

    public void setGuitekst(String guitekst) {
    	log("setter guitekst");
    	String old = this.guitekst;
        this.guitekst = guitekst;
        
        getProps().firePropertyChange("guitekst", old, guitekst);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoyIdSeq")
    @Column(nullable = false)
    public Long getId() {
    	log("henter id: " + id);
        return id;
    }

    public void setId(Long id) {
    	log("setter id: " + id);
        this.id = id;
    }

    public Long getMaskinnummer() {
    	log("henter maskinnummer");
        return maskinnummer;
    }

    public void setMaskinnummer(Long maskinnummer) {
    	log("setter maskinnummer");
    	Long old = this.maskinnummer;
        this.maskinnummer = maskinnummer;
        
        getProps().firePropertyChange("maskinnummer", old, maskinnummer);
    }
    
    @Transient
    public Long getMaskinnummerValid() {
        return getMaskinnummer();
    }
    
    public void setMaskinnummerValid(Long maskinnummer) {
    	Object old = getMaskinnummer();
    	setMaskinnummer(maskinnummer);
        
        getProps().firePropertyChange("maskinnummerValid", old, maskinnummer);
        
        updateIdentification();
    }
    
    public String getRegnr() {
    	log("henter regnr");
        return regnr;
    }
    
    public void setRegnr(String regnr) {
    	log("setter regnr");
    	String old = this.regnr;
        this.regnr = regnr;
        
        getProps().firePropertyChange("regnr", old, regnr);
    }
    
    @Transient
    public String getRegnrValid() {
        return getRegnr();
    }
    
    public void setRegnrValid(String regnr) {
        Object old = getRegnr();
        regnr = regnr.replaceAll("\\s+", ""); //Fjerner alle mellomrom i regnr
        setRegnr(regnr);
    	
        getProps().firePropertyChange("regnrValid", old, regnr);
        
        updateIdentification();
    }

    @Column(name="SLETTET_FLAGG", nullable = false)
    public Long getSlettetFlagg() {
    	log("henter slettetFlagg");
        return slettetFlagg;
    }

    public void setSlettetFlagg(Long slettetFlagg) {
    	log("setter slettetFlag");
    	Long old = this.slettetFlagg;
        this.slettetFlagg = slettetFlagg;
        
        getProps().firePropertyChange("slettetFlagg", old, slettetFlagg);
    }

    @PrivateOwned
    @OneToMany(mappedBy = "kjoretoy", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<KjoretoyKontakttype> getKjoretoyKontaktypeList() {
    	log("henter kjoretoyKontakttypeList");
        return kjoretoyKontaktypeList;
    }

    public void setKjoretoyKontaktypeList(List<KjoretoyKontakttype> kjoretoyKontaktypeList) {
    	log("setter kjoretoyKontakttypeList med: " + kjoretoyKontaktypeList.size());
    	List<KjoretoyKontakttype> old = this.kjoretoyKontaktypeList;
        this.kjoretoyKontaktypeList = kjoretoyKontaktypeList;
        
        if(! (kjoretoyKontaktypeList instanceof IndirectList)) {
        	getProps().firePropertyChange("kjoretoyKontaktypeList", old, kjoretoyKontaktypeList);
        }
    }

    public KjoretoyKontakttype addKjoretoyKontaktype(KjoretoyKontakttype kjoretoyKontaktype) {
    	log("legegr til kjoretoykontakttype");
        getKjoretoyKontaktypeList().add(kjoretoyKontaktype);
        kjoretoyKontaktype.setKjoretoy(this);
        return kjoretoyKontaktype;
    }

    public KjoretoyKontakttype removeKjoretoyKontaktype(KjoretoyKontakttype kjoretoyKontaktype) {
    	log("fjerner kjoretoyKontakttype");
        getKjoretoyKontaktypeList().remove(kjoretoyKontaktype);
        kjoretoyKontaktype.setKjoretoy(null);
        return kjoretoyKontaktype;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "KJORETOY_BILDE", 
    	joinColumns = {@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")},
        inverseJoinColumns = {@JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID")})
    public List<Bilde> getBildeList() {
    	log("henter bildeList");
        return bildeList;
    }

    public void setBildeList(List<Bilde> bildeList) {
    	log("setter bildeList med: " + bildeList.size());
    	List<Bilde> old = this.bildeList;
        this.bildeList = bildeList;
        
        if(! (bildeList instanceof IndirectList)) {
        	getProps().firePropertyChange("bildeList", old, bildeList);
        }
    }

    public Bilde addBilde(Bilde bilde) {
    	log("legger til bilde");

        if(! getBildeList().contains(bilde)) {
        	getBildeList().add(bilde);
        }
        return bilde;
    }

    public Bilde removeKjoretoyBilde(Bilde bilde) {
    	log("fjerner bilde");
        getBildeList().remove(bilde);
        return bilde;
    }

    @ManyToOne
    @JoinColumn(name = "MODELL_ID", referencedColumnName = "ID")
    public Kjoretoymodell getKjoretoymodell() {
    	log("henter kjoretoymodell");
        return kjoretoymodell;
    }

    public void setKjoretoymodell(Kjoretoymodell kjoretoymodell) {
    	log("setter kjoretoymodell");
    	Kjoretoymodell old = this.kjoretoymodell;
        this.kjoretoymodell = kjoretoymodell;
        
        getProps().firePropertyChange("kjoretoymodell", old, kjoretoymodell);
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "KJORETOY_DOK",
    		joinColumns = {@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "DOKUMENT_ID", referencedColumnName = "ID")})
    public List<Dokument> getDokumentList() {
    	log("henter dokList");
        return dokumentList;
    }

    public void setDokumentList(List<Dokument> dokumentList) {
    	log("setter dokList med: " + dokumentList.size());
    	List<Dokument> old = this.dokumentList;
        this.dokumentList = dokumentList;
        
        if(! (dokumentList instanceof IndirectList)) {
        	getProps().firePropertyChange("dokumentList", old, dokumentList);
        }
    }

    public Dokument addDokument(Dokument dokument) {
    	log("legger til en dok");
    	
    	if(! getDokumentList().contains(dokument)) {
    		getDokumentList().add(dokument);
    	}
        return dokument;
    }

    public Dokument removeDokument(Dokument dokument) {
    	log("fjerner en dok");
        getDokumentList().remove(dokument);
        return dokument;
    }
    
    @ManyToOne(cascade=CascadeType.REFRESH)
    @JoinColumn(name = "LEVERANDOR_NR", referencedColumnName = "NR")
    public Leverandor getLeverandor() {
    	log("henter leverandor");
        return leverandor;
    }

    public void setLeverandor(Leverandor leverandor) {
    	log("setter leverandor");
    	Leverandor old = this.leverandor;
        this.leverandor = leverandor;
        
        getProps().firePropertyChange("leverandor", old, leverandor);
    }

    @OneToMany(mappedBy = "kjoretoy", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<KjoretoyUtstyr> getKjoretoyUtstyrList() {
    	log("henter utstyrList");
        return kjoretoyUtstyrList;
    }

    public void setKjoretoyUtstyrList(List<KjoretoyUtstyr> kjoretoyUtstyrList) {
    	log("setter utstyrList mad: " + kjoretoyUtstyrList.size());
    	List<KjoretoyUtstyr> old = this.kjoretoyUtstyrList;
        this.kjoretoyUtstyrList = kjoretoyUtstyrList;
        
        if(! (kjoretoyUtstyrList instanceof IndirectList)) {
        	getProps().firePropertyChange("kjoretoyUtstyrList", old, kjoretoyUtstyrList);
        }
    }

    public KjoretoyUtstyr addKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
    	log("legger til et utstyr");
        getKjoretoyUtstyrList().add(kjoretoyUtstyr);
        kjoretoyUtstyr.setKjoretoy(this);
        return kjoretoyUtstyr;
    }

    public KjoretoyUtstyr removeKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
    	log("fjerner et utstyr");
        getKjoretoyUtstyrList().remove(kjoretoyUtstyr);
        kjoretoyUtstyr.setKjoretoy(null);
        return kjoretoyUtstyr;
    }

    @ManyToOne
    @JoinColumn(name = "TYPE_NAVN", referencedColumnName = "NAVN")
    public Kjoretoytype getKjoretoytype() {
    	log("henter kjoretoytype");
        return kjoretoytype;
    }

    public void setKjoretoytype(Kjoretoytype kjoretoytype) {
    	log("setter kjoretoytype");
    	Kjoretoytype old = this.kjoretoytype;
        this.kjoretoytype = kjoretoytype;
        
        getProps().firePropertyChange("kjoretoytype", old, kjoretoytype);
    }

    @OneToMany(mappedBy = "kjoretoy", cascade = CascadeType.ALL)
    public List<Installasjon> getInstallasjonList() {
    	log("henter installsjonslist");
        return installasjonList;
    }

    public void setInstallasjonList(List<Installasjon> installasjonList) {
    	log("setter installasjonslist med: " + installasjonList.size());
    	List<Installasjon> old = this.installasjonList;
        this.installasjonList = installasjonList;
        
        if(! (installasjonList instanceof IndirectList)) {
        	getProps().firePropertyChange("installasjonList", old, installasjonList);
        }
    }

    public Installasjon addInstallasjon(Installasjon installasjon) {
    	log("legger til en installasjon");
        getInstallasjonList().add(installasjon);
        installasjon.setKjoretoy(this);
        return installasjon;
    }

    public Installasjon removeInstallasjon(Installasjon installasjon) {
    	log("fjerner en installsjon");
        getInstallasjonList().remove(installasjon);
        installasjon.setKjoretoy(null);
        return installasjon;
    }
    @PrivateOwned
    @OneToMany(mappedBy = "kjoretoy", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<Kjoretoyordre> getKjoretoyordreList() {
    	log("henter kjoretoyordreList");
        return kjoretoyordreList;
    }

    public void setKjoretoyordreList(List<Kjoretoyordre> kjoretoyordreList) {
    	log("setter kjoretoyordreList med: " + kjoretoyordreList.size());
    	List<Kjoretoyordre> old = this.kjoretoyordreList;
        this.kjoretoyordreList = kjoretoyordreList;
        
        if(! (kjoretoyordreList instanceof IndirectList)) {
        	getProps().firePropertyChange("kjoretoyordreList", old, kjoretoyordreList);
        }
    }

    public Kjoretoyordre addKjoretoyordre(Kjoretoyordre kjoretoyordre) {
    	log("legger til en ordre");
        getKjoretoyordreList().add(kjoretoyordre);
        kjoretoyordre.setKjoretoy(this);
        return kjoretoyordre;
    }

    public Kjoretoyordre removeKjoretoyordre(Kjoretoyordre kjoretoyordre) {
    	log("fjerner en ordre");
        getKjoretoyordreList().remove(kjoretoyordre);
        kjoretoyordre.setKjoretoy(null);
        return kjoretoyordre;
    }
    
    @PrivateOwned
    @OneToMany(mappedBy = "kjoretoy", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @OrderBy("id DESC")
    public List<Kjoretoylogg> getKjoretoyloggList() {
    	log("henter loggList");
        return kjoretoyloggList;
    }

    public void setKjoretoyloggList(List<Kjoretoylogg> kjoretoyloggList) {
    	log("setter loggList med " + kjoretoyloggList.size());
    	List<Kjoretoylogg> old = this.kjoretoyloggList;
        this.kjoretoyloggList = kjoretoyloggList;
        
        if(! (kjoretoyloggList instanceof IndirectList)) {
        	getProps().firePropertyChange("kjoretoyloggList", old, kjoretoyloggList);
        }
    }
 
    public Kjoretoylogg addKjoretoylogg(Kjoretoylogg kjoretoylogg) {
    	log("legger til en logg");
        getKjoretoyloggList().add(kjoretoylogg);
        kjoretoylogg.setKjoretoy(this);
        return kjoretoylogg;
    }

    public Kjoretoylogg removeKjoretoylogg(Kjoretoylogg kjoretoylogg) {
    	log("fjerner en logg");
        getKjoretoyloggList().remove(kjoretoylogg);
        kjoretoylogg.setKjoretoy(null);
        return kjoretoylogg;
    }

    @ManyToOne(cascade=CascadeType.PERSIST )
    @JoinColumn(name = "EIER_ID", referencedColumnName = "ID")
    public Kjoretoyeier getKjoretoyeier() {
    	log("henter kjoretoyeier");
        return kjoretoyeier;
    }

    public void setKjoretoyeier(Kjoretoyeier kjoretoyeier) {
    	log("setter kjoretoyeier");
    	Kjoretoyeier old = this.kjoretoyeier;
        this.kjoretoyeier = kjoretoyeier;
        
        getProps().firePropertyChange("kjoretoyeier", old, kjoretoyeier);
    }

    /**
     * Returneerer eksternt_navn
     */
    @Transient
    public String getNummerEllerNavn() {
    	return getEksterntNavn();
    }
    
    public void setNummerEllerNavn(String nummerEllerNavn) {
		String old = getNummerEllerNavn();
		
		setEksterntNavn(nummerEllerNavn);
		
		getProps().firePropertyChange("nummerEllerNavn", old, getNummerEllerNavn());
    }
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("Id", getId()).
            append("registreringsnummer", getRegnr()).
            append("maskinnummer", getMaskinnummer()).
            append("eksterntNavn", getEksterntNavn()).
            toString();
    }

    // de neste metodene er h�ndkodet
    /**
     * Sammenlikner to kjøretøy.
     * Tar høyde for sirkulære avhengigheter mellom instansene av relaterte entiteter.
     * Fanger ikke opp at en liste kan ha samme lengde men forskjellig innhold.
     * @param o
     * @return true hvis n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kjoretoy)) {
            return false;
        }
        
        Kjoretoy other = (Kjoretoy)o;
        EqualsBuilder comp = new EqualsBuilder().
        append(getId(), other.getId());
        
        return comp.isEquals();
        
        
        /*Kjoretoy other = (Kjoretoy)o;
        EqualsBuilder comp = new EqualsBuilder().
        append(getId(), other.getId()).
        append(getRegnr(), other.getRegnr()).
        append(getSlettetFlagg(), other.getSlettetFlagg()).
        append(getMaskinnummer(), other.getMaskinnummer()).
        append(getEksterntNavn(), other.getEksterntNavn()).
        append(getFritekst(), other.getFritekst());

        if((getBildeList() == null && other.getBildeList() != null) || (getBildeList() != null && other.getBildeList() == null)){
        	return false;
        }
        if(getBildeList() != null && other.getBildeList() != null) {
        	comp.append(getBildeList().size(), other.getBildeList().size());
        }
        		
        if((getDokumentList() == null && other.getDokumentList() != null) || (getDokumentList() != null && other.getDokumentList() == null)){
        	return false;
        }
        if(getDokumentList() != null && other.getDokumentList() != null) {
        	comp.append(getDokumentList().size(), other.getDokumentList().size());
        }
        
        if((getKjoretoyKontaktypeList() == null && other.getKjoretoyKontaktypeList() != null) || (getKjoretoyKontaktypeList() != null && other.getKjoretoyKontaktypeList() == null)){
        	return false;
        }
        if(getKjoretoyKontaktypeList() != null && other.getKjoretoyKontaktypeList() != null) {
        	comp.append(getKjoretoyKontaktypeList().size(), other.getKjoretoyKontaktypeList().size());
        }
        
        if((getKjoretoyloggList() == null && other.getKjoretoyloggList() != null) || (getKjoretoyloggList() != null && other.getKjoretoyloggList() == null)){
        	return false;
        }
        if(getKjoretoyloggList() != null && other.getKjoretoyloggList() != null) {
        	comp.append(getKjoretoyloggList().size(), other.getKjoretoyloggList().size());
        }
        
        if((getInstallasjonList() == null && other.getInstallasjonList() != null) || (getInstallasjonList() != null && other.getInstallasjonList() == null)){
        	return false;
        }
        if(getInstallasjonList() != null && other.getInstallasjonList() != null) {
        	comp.append(getInstallasjonList().size(), other.getInstallasjonList().size());
        }

        if((getKjoretoyordreList() == null && other.getKjoretoyordreList() != null) || (getKjoretoyordreList() != null && other.getKjoretoyordreList() == null)){
        	return false;
        }
        if(getKjoretoyordreList() != null && other.getKjoretoyordreList() != null) {
        	comp.append(getKjoretoyordreList().size(), other.getKjoretoyordreList().size());
        }

        if((getKjoretoyUtstyrList() == null && other.getKjoretoyUtstyrList() != null) || (getKjoretoyUtstyrList() != null && other.getKjoretoyUtstyrList() == null)){
        	return false;
        }
        if(getKjoretoyUtstyrList() != null && other.getKjoretoyUtstyrList() != null) {
        	comp.append(getKjoretoyUtstyrList().size(), other.getKjoretoyUtstyrList().size());
        }

        if((getSelskapList() == null && other.getSelskapList() != null) || (getSelskapList() != null && other.getSelskapList() == null)){
        	return false;
        }
        if(getSelskapList() != null && other.getSelskapList() != null) {
        	comp.append(getSelskapList().size(), other.getSelskapList().size());
        }

        if((getKjoretoyeier() == null && other.getKjoretoyeier() != null) || (getKjoretoyeier() != null && other.getKjoretoyeier() == null)) {
        	return false;
        }
        if(getKjoretoyeier() != null && other.getKjoretoyeier() != null) {
        	comp.append(getKjoretoyeier().getNavn(), other.getKjoretoyeier().getNavn());
        }

        if((getKjoretoymodell() == null && other.getKjoretoymodell() != null) || (getKjoretoymodell() != null && other.getKjoretoymodell() == null)) {
        	return false;
        }
        if(getKjoretoymodell() != null && other.getKjoretoymodell() != null) {
        	comp.append(getKjoretoymodell().getModell(), other.getKjoretoymodell().getModell());
        }

        if((getKjoretoytype() == null && other.getKjoretoytype() != null) || (getKjoretoytype() != null && other.getKjoretoytype() == null)) {
        	return false;
        }
        if(getKjoretoytype() != null && other.getKjoretoytype() != null) {
        	comp.append(getKjoretoytype().getNavn(), other.getKjoretoytype().getNavn());
        }

        if((getLeverandor() == null && other.getLeverandor() != null) || (getLeverandor() != null && other.getLeverandor() == null)) {
        	return false;
        }
        if(getLeverandor() != null && other.getLeverandor() != null) {
        	comp.append(getLeverandor().getNavn(), other.getLeverandor().getNavn());
        }

        if((getMaskinsenter() == null && other.getMaskinsenter() != null) || (getMaskinsenter() != null && other.getMaskinsenter() == null)) {
        	return false;
        }
        if(getMaskinsenter() != null && other.getMaskinsenter() != null) {
        	comp.append(getMaskinsenter().getNavn(), other.getMaskinsenter().getNavn());
        }

        return comp.isEquals();*/
    }

    /**
     * Genererer en hashkode basert kun p� Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getId()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getRelevantNavn();
    }

    public int compareTo(Object o) {
        Kjoretoy other = (Kjoretoy) o;
                return new CompareToBuilder().
                    append(id, other.getId()).
                    toComparison();
    }
    
    @Transient
    public String getRelevantNavn() {
        if (maskinnummer != null) {
            return String.valueOf(maskinnummer);
        }
        if (regnr != null && regnr.length() > 0) {
            return regnr;
        }
        if (eksterntNavn != null && eksterntNavn.length() > 0) {
            return eksterntNavn;
        }
        return String.valueOf(id);
    }
    
    @Transient
    public String getIdString() {
        return String.valueOf(getId());
    }
    
    @Transient
    public String getMaskinnummerString() {
        if (getMaskinnummer() == null) {
            return "";
        }
        return String.valueOf(getMaskinnummer());
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "SELSKAP_KJORETOY", 
        joinColumns = {@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")},
        inverseJoinColumns = {@JoinColumn(name = "SELSKAPSKODE", referencedColumnName = "SELSKAPSKODE")})    
    public List<Selskap> getSelskapList() {
    	log("henter selskapList");
        return selskapList;
    }

    public void setSelskapList(List<Selskap> selskapList) {
    	log("setter selskapList med: " + selskapList.size());
    	List<Selskap> old = this.selskapList;
        this.selskapList = selskapList;
        
        if(! (selskapList instanceof IndirectList)) {
        	getProps().firePropertyChange("selskapList", old, selskapList);
        }
    }

    /**
     * Legger til et selkap i et mange-til-mange forhold.
     * VIKTIG: p�se at denne metoden og den tilsvarende i Kjoretoy ikke kaller hverandre i l�kke.
     * @param selskap
     * @return det ferdigkoblede selkapet.
     */
    public Selskap addSelskap(Selskap selskap) {
    	log("legger til et selskap");
    	if(! getSelskapList().contains(selskap)) {
    		getSelskapList().add(selskap);
    	}
    	return selskap;
    }
    
    /**
     * Fjerner et selskap fra et mange-til-mange forhold.
     * VIKTIG: p�se at denne metoden og den tilsvarende i Kjoretoy ikke kaller hverandre i l�kke.
     * @param selskap
     * @return det frakoblede selskapet.
     */
    public Selskap removeSelskap(Selskap selskap) {
    	log("fjerner et selskap");
    	getSelskapList().remove(selskap);

    	return selskap;
    }
    @PrivateOwned
    @OneToMany(mappedBy = "kjoretoy", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<KontraktKjoretoy> getKontraktKjoretoyList() {
		return kontraktKjoretoyList;
	}


	public void setKontraktKjoretoyList(List<KontraktKjoretoy> kontraktKjoretoyList) {
		this.kontraktKjoretoyList = kontraktKjoretoyList;
	}

	@PrivateOwned
	@OneToMany(mappedBy = "kjoretoy", cascade = CascadeType.ALL)
	public List<Stroperiode> getStroperiodeList() {
		return stroperiodeList;
	}

	public void setStroperiodeList(List<Stroperiode> stroperiodeList) {
		this.stroperiodeList = stroperiodeList;
	}

	@PrivateOwned
	@OneToMany(mappedBy = "kjoretoy", cascade = CascadeType.ALL)
	public List<Klippeperiode> getKlippeperiodeList() {
		return klippeperiodeList;
	}

	public void setKlippeperiodeList(List<Klippeperiode> klippeperiodeList) {
		this.klippeperiodeList = klippeperiodeList;
	}

    @PrivateOwned
    @OneToMany(mappedBy = "kjoretoy", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<KjoretoyRode> getKjoretoyRodeList() {
        return kjoretoyRodeList;
    }

    public void setKjoretoyRodeList(List<KjoretoyRode> kjoretoyRodeList) {
        this.kjoretoyRodeList = kjoretoyRodeList;
    }

	@Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
    	log("henter ownedMipssEntity");
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
    	log("setter ownedMipssEntity");
        this.ownedMipssEntity = ownedMipssEntity;
    }

    private void log(String mssg) {
    	if(logger == null) {
    		logger = LoggerFactory.getLogger(Kjoretoy.class);
    	}
    	logger.trace("id=" + id + ". " + mssg);
    }

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	@Transient
	public boolean isComplete() {
		return getNummerEllerNavn() != null 
		&& getNummerEllerNavn().length() != 0 
		&& !getNummerEllerNavn().equals(Kjoretoy.ID_IKKE_SATT)
		&& (getMaskinnummerString() == null || getMaskinnummerString().trim().length() == 0 || getMaskinnummerString().trim().length() == 8);
	}
	
	/**
	 * Denne metoden sørger for at identifikasjonen til kjøretøyet er iht spec
	 * kjoretoyId vil alltid være være maskinnummer om dette er satt.
	 * Ellers vil det være regNr. Om hverken maskinNummer eller regNr er satt kan eksterntNavn være hva som helst.
	 * 
	 */
	@Transient
	private void updateIdentification() {
		String newEksterntnavn = null;
		
		if(maskinnummer != null) {
			newEksterntnavn = maskinnummer.toString();
		} else if(regnr != null && regnr.length() > 0) {
			newEksterntnavn = regnr;
		}
		
		//if(newEksterntnavn != null) {
			String oldNummerEllerNavn = getNummerEllerNavn();
			setEksterntNavn(newEksterntnavn);
			getProps().firePropertyChange("nummerEllerNavn", oldNummerEllerNavn, getNummerEllerNavn());
		//}
	}
	
	@Transient
	public Installasjon getAktivInstallasjon(Date date) {
		for(Installasjon ins : getInstallasjonList()) {
			if(ins.getAktivFraDato().before(date) && (ins.getAktivTilDato() == null || ins.getAktivTilDato().after(date))) {
				return ins;
			}
		}
		return null;
	}
}
