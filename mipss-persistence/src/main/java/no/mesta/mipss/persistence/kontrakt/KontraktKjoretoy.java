package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
@Table(name = "KONTRAKT_KJORETOY")
@IdClass(KontraktKjoretoyPK.class)

@NamedQueries({
    @NamedQuery(
        name = KontraktKjoretoy.FIND_ALL, 
        query = "select o from KontraktKjoretoy o"),
    @NamedQuery(
        name = KontraktKjoretoy.FIND_BY_KJORETOY, 
        query = "select o from KontraktKjoretoy o where o.kjoretoyId = :id"),
	@NamedQuery(
        name = KontraktKjoretoy.FIND_BY_KONTRAKT, 
        query = "select o from KontraktKjoretoy o where o.kontraktId = :id"),
    @NamedQuery(
            name = KontraktKjoretoy.FIND_BY_NOT_IN_GRUPPE, 
            query = "select o from KontraktKjoretoy o where o.kontraktId = :id and o.kjoretoyId not in(select k.kjoretoyId from KontraktKjoretoyGruppering k where k.kjoretoygruppeId=:gruppeId)"),
    @NamedQuery(
            name = KontraktKjoretoy.FIND_BY_KONTRAKT_LEVERANDOR, 
            query = "select o from KontraktKjoretoy o where o.kontraktId = :id and o.kjoretoy.leverandor.nr=:leverandorNr"),
    @NamedQuery(
            name = KontraktKjoretoy.FIND_BY_KONTRAKT_LEVERANDOR_ANTALL, 
            query = "select count(o) from KontraktKjoretoy o where o.kontraktId = :id and o.kjoretoy.leverandor.nr=:leverandorNr"),
    @NamedQuery(
    		name = KontraktKjoretoy.FIND_BY_KJORETOY_KONTRAKT,
    		query = "select o from KontraktKjoretoy o where o.kjoretoyId = :kjoretoyId and o.kontraktId = :kontraktId"
    )        
            
})

public class KontraktKjoretoy extends MipssEntityBean<KontraktKjoretoy> implements Serializable, IRenderableMipssEntity {
	private final static String[] ID_NAMES = {"kontraktId", "kjoretoyId"};
	public final static String FIND_ALL = "KontraktKjoretoy.findAll";
	public final static String FIND_BY_KJORETOY = "KontraktKjoretoy.findByKjoretoy";
	public final static String FIND_BY_KONTRAKT = "KontraktKjoretoy.findByKontrakt";
	public final static String FIND_BY_KONTRAKT_LEVERANDOR = "KontraktKjoretoy.findByKontraktLeverandor";
	public final static String FIND_BY_KONTRAKT_LEVERANDOR_ANTALL = "KontraktKjoretoy.findByKontraktLeverandorAntall";
	public static final String FIND_BY_NOT_IN_GRUPPE = "KontraktKjoretoy.findByNotInGruppe";
	public static final String FIND_BY_KJORETOY_KONTRAKT = "KontraktKjoretoy.findByKjoretoyKontrakt";
	
	private Long kontraktId;
	private Long kjoretoyId;
	private Long ansvarligFlagg;
	
	private Driftkontrakt driftkontrakt;
	private Kjoretoy kjoretoy;
	
	public KontraktKjoretoy() {
	}

	@Id
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	@Id
	@Column(name = "KJORETOY_ID", nullable = false, insertable = false, updatable = false)
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	@Column(name = "ANSVARLIG_FLAGG", nullable = false)
	public Long getAnsvarligFlagg() {
		return ansvarligFlagg;
	}

	public void setAnsvarligFlagg(Long ansvarligFlagg) {
		Object oldVal = this.ansvarligFlagg;
		Object oldValBoolean = getAnsvarligFlaggBoolean();
		this.ansvarligFlagg = ansvarligFlagg;
		
		getProps().firePropertyChange("ansvarligFlagg", oldVal, this.ansvarligFlagg);
		getProps().firePropertyChange("ansvarligFlaggBoolean", oldValBoolean, getAnsvarligFlaggBoolean());
	}
	
    public Boolean getAnsvarligFlaggBoolean() {
    	return ansvarligFlagg != null && ansvarligFlagg.longValue() == 1 ? Boolean.TRUE : Boolean.FALSE;
    }

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
		
		if(driftkontrakt != null) {
			setKontraktId(driftkontrakt.getId());
		} else {
			setKontraktId(null);
		}
	}

	@ManyToOne
	@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID", nullable = false)
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}

	public void setKjoretoy(Kjoretoy kjoretoy) {
		this.kjoretoy = kjoretoy;
		
		if(kjoretoy != null) {
			setKjoretoyId(kjoretoy.getId());
		} else {
			setKjoretoyId(null);
		}
	}
	
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public String getTextForGUI() {
		return driftkontrakt != null ? driftkontrakt.getKontraktnavn() : "";
	}
	
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3,5).
            append(kontraktId).
            append(kjoretoyId).
            toHashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof KontraktKjoretoy)) {return false;}
        if(o == this) { return true;}
        
        KontraktKjoretoy other = (KontraktKjoretoy) o;
        return new EqualsBuilder().
            append(kontraktId, other.getKontraktId()).
            append(kjoretoyId, other.getKjoretoyId()).
            isEquals();
    }
    
    @Transient
	public String getKjoretoynavn(){
		if (kjoretoy!=null)
			return kjoretoy.getEksterntNavn();
		return null;
	}
	@Transient
	public String getType(){
		if (kjoretoy!=null){
			if (kjoretoy.getKjoretoytype()!=null)
				return kjoretoy.getKjoretoytype().getNavn();
		}
		return null;
	}
	@Transient
	public String getMerke(){
		if (kjoretoy!=null){
			if (kjoretoy.getKjoretoymodell()!=null)
				return kjoretoy.getKjoretoymodell().getMerke();
		}
		return null;
	}
	@Transient
	public String getLeverandor(){
		if (kjoretoy!=null){
			if (kjoretoy.getLeverandor()!=null)
				return kjoretoy.getLeverandor().getNavn();
		}
		return null;
	}
	@Transient
	public String getRegnr(){
		if (kjoretoy!=null){
			return kjoretoy.getRegnr();
		}
		return null;
	}
}
