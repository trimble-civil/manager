package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

import javax.persistence.*;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import org.eclipse.persistence.annotations.ReadOnly;

@NamedQueries({
    @NamedQuery(name = Levkontrakt.FIND_BY_LEVERANDOR_NR, query = "select o from Levkontrakt o where o.leverandorNr=:leverandorNr")
})

@SuppressWarnings("serial")
@Entity
@Table(name = "LEVKONTRAKT")
public class Levkontrakt extends MipssEntityBean<Levkontrakt> implements IRenderableMipssEntity {
	public static final String FIND_BY_LEVERANDOR_NR = "Levkontrakt.findByLeverandorNr";
	@Id
	@Column(name="LEVKONTRAKT_IDENT")
	private String levkontraktIdent;
	@Column(name="DRIFTKONTRAKT_ID")
	private Long driftkontraktId;
	@ManyToOne
	@JoinColumn(name="DRIFTKONTRAKT_ID", updatable = false, insertable = false)
	private Driftkontrakt driftkontrakt;
	@Column(name="LEVERANDOR_NR")
	private String leverandorNr;
	@ManyToOne
	@JoinColumn(name="LEVERANDOR_NR", updatable = false, insertable = false)
	private Leverandor leverandor;
	@Column(name="FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fraDato;
	
	/**
	 * @return the levkontraktIdent
	 */
	public String getLevkontraktIdent() {
		return levkontraktIdent;
	}
	/**
	 * @param levkontraktIdent the levkontraktIdent to set
	 */
	public void setLevkontraktIdent(String levkontraktIdent) {
		String old = this.levkontraktIdent;
		this.levkontraktIdent = levkontraktIdent;
		firePropertyChange("levkontraktIdent", old, levkontraktIdent);
	}
	/**
	 * @return the driftkontraktId
	 */
	public Long getDriftkontraktId() {
		return driftkontraktId;
	}
	/**
	 * @param driftkontraktId the driftkontraktId to set
	 */
	public void setDriftkontraktId(Long driftkontraktId) {
		Long old = this.driftkontraktId;
		this.driftkontraktId = driftkontraktId;
		firePropertyChange("driftkontraktId", old, driftkontraktId);
	}
	/**
	 * @return the leverandorNr
	 */
	public String getLeverandorNr() {
		return leverandorNr;
	}
	/**
	 * @param leverandorNr the leverandorNr to set
	 */
	public void setLeverandorNr(String leverandorNr) {
		String old = this.leverandorNr;
		this.leverandorNr = leverandorNr;
		firePropertyChange("leverandorNr", old, leverandorNr);
	}
	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}
	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		Date old = this.fraDato;
		this.fraDato = fraDato;
		firePropertyChange("fraDato", old, fraDato);
	}

	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		Driftkontrakt old = this.driftkontrakt;
		this.driftkontrakt = driftkontrakt;
		firePropertyChange("driftkontrakt", old, driftkontrakt);
		setDriftkontraktId(driftkontrakt.getId());
	}

	public Leverandor getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(Leverandor leverandor) {
		Leverandor old = this.leverandor;
		this.leverandor = leverandor;
		firePropertyChange("leverandor", old, leverandor);
		setLeverandorNr(leverandor.getNr());
	}

	@Override
	public String getTextForGUI(){
		return levkontraktIdent;
	}
	
}
