package no.mesta.mipss.persistence.kjoretoy;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@Table(name = "dummy_eksisterer_ikke")
@SqlResultSetMappings({
	@SqlResultSetMapping(name="KontraktKjoretoyInfoResult", entities = {
		@EntityResult(entityClass = KontraktKjoretoyInfo.class, fields = {
			@FieldResult(name = "kjoretoyId", column = "KJORETOY_ID"),
			@FieldResult(name = "ansvarligFlagg", column = "ANSVARLIG_FLAGG"),
			@FieldResult(name = "regnr", column = "REGNR"),
			@FieldResult(name = "maskinnummer", column = "MASKINNUMMER"),
			@FieldResult(name = "eksterntNavn", column = "EKSTERNT_NAVN"),
			@FieldResult(name = "type", column = "TYPE"),
			@FieldResult(name = "merke", column = "MERKE"),
			@FieldResult(name = "modell", column = "MODELL"),
			@FieldResult(name = "aar", column = "AAR"),
			@FieldResult(name = "leverandorNr", column = "LEVERANDOR_NR"),
			@FieldResult(name = "leverandorNavn", column = "LEVERANDOR_NAVN"),
			@FieldResult(name = "leverandorValgbarFlagg", column = "LEVERANDOR_VALGBAR_FLAGG"),
			@FieldResult(name = "dfuId", column = "DFU_ID"),
			@FieldResult(name = "dfuTlfnummer", column = "DFU_TLFNUMMER"),
			@FieldResult(name = "dfustatus", column = "DFU_STATUSNAVN"),
			@FieldResult(name = "dfuSisteLivtegnDato", column = "DFU_SISTE_LIVSTEGN_DATO"),
			@FieldResult(name = "dfuSistePosisjonDato", column = "DFU_SISTE_POSISJON_DATO"),
			@FieldResult(name = "aktuellKonfigKlartekst", column = "AKTUELL_KONFIG_KLARTEKST"),
			@FieldResult(name = "aktuellStroperiodeKlartekst", column = "AKTUELL_STROPERIODE_KLARTEKST"),
			@FieldResult(name = "sprMedDatafangstFlagg", column = "SPR_MED_DATAFANGST_FLAGG"),
			@FieldResult(name = "sprUtenDatafangstFlagg", column = "SPR_UTEN_DATAFANGST_FLAGG"),
			@FieldResult(name = "dfuSisteProdPosisjonDato", column = "DFU_SISTE_PROD_POSISJON_DATO"),
			@FieldResult(name = "aktuellStroperiodeFraDato", column= "AKTUELL_STROPERIODE_FRA_DATO"),
			@FieldResult(name = "aktuellStroperiodeTilDato", column= "AKTUELL_STROPERIODE_TIL_DATO"),
			@FieldResult(name = "dfuSerienummer", column="DFU_SERIENUMMER"),
			@FieldResult(name = "bestillingStatus", column="BESTILLING_STATUS"),
			@FieldResult(name = "bestillingStatusDato", column="BESTILLINGSTATUS_DATO"),
			@FieldResult(name = "bestillingDato", column="BESTILLING_DATO"),
			@FieldResult(name = "ukjentProduksjon", column="UKJENT_PRODUKSJON_FLAGG"),
			@FieldResult(name = "ukjentStrometode", column="UKJENT_STROMETODE_FLAGG"),
			@FieldResult(name = "eierNavn", column="KJORETOY_EIER_NAVN"),
			@FieldResult(name = "aktuellKlippeperiodeTekst", column="AKTUELL_KLIPPEPERIODE_TEKST"),
			@FieldResult(name = "aktuellKlippeperiodeFraDato", column="AKTUELL_KLIPPEPERIODE_FRA_DATO"),
			@FieldResult(name = "aktuellKlippeperiodeTilDato", column="AKTUELL_KLIPPEPERIODE_TIL_DATO"),
			@FieldResult(name = "kantklippFlagg", column="KANTKLIPP_FLAGG")
		})
	})
})

public class KontraktKjoretoyInfo extends MipssEntityBean<KontraktKjoretoyInfo> implements IRenderableMipssEntity {
	private static final String[] ID_NAMES = {"kjoretoyId"};
	
	private Long kjoretoyId;
	private Long ansvarligFlagg;
	private String regnr;
	private Long maskinnummer;
	private String eksterntNavn;
	private String type;
	private String merke;
	private String modell;
	private Long aar;
	private String leverandorNr;
	private String leverandorNavn;
	private String eierNavn;
	private Long leverandorValgbarFlagg;
	private Long dfuId;
	//private String dfuSerienummer;
	private String dfuTlfnummer;
	private String dfustatus;
	private Date dfuSisteLivtegnDato;
	private Date dfuSistePosisjonDato;
	private String aktuellKonfigKlartekst;
	private String aktuellStroperiodeKlartekst;
	private Date aktuellStroperiodeFraDato;
	private Date aktuellStroperiodeTilDato;
	private Long sprMedDatafangstFlagg;
	private Long sprUtenDatafangstFlagg;
	private Date dfuSisteProdPosisjonDato;
	
	private String dfuSerienummer;
	private String bestillingStatus;
	private Date bestillingStatusDato;
	private Date bestillingDato;
	private Boolean ukjentProduksjon;
	private Boolean ukjentStrometode;
	
	//Klippeperiodeinfo
	private String aktuellKlippeperiodeTekst;
	private Date aktuellKlippeperiodeFraDato;
	private Date aktuellKlippeperiodeTilDato;
	private Long kantklippFlagg;
		
	public KontraktKjoretoyInfo() {
	}
	
	@Id
	@Column(name = "KJORETOY_ID")
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	
	@Column(name = "ANSVARLIG_FLAGG")
	public Long getAnsvarligFlagg() {
		return ansvarligFlagg;
	}

	public void setAnsvarligFlagg(Long ansvarligFlagg) {
		this.ansvarligFlagg = ansvarligFlagg;
	}

	public String getRegnr() {
		return regnr;
	}

	public void setRegnr(String regnr) {
		this.regnr = regnr;
	}

	public Long getMaskinnummer() {
		return maskinnummer;
	}

	public void setMaskinnummer(Long maskinnummer) {
		this.maskinnummer = maskinnummer;
	}
	
	@Column(name = "EKSTERNT_NAVN")	public String getEksterntNavn() {
		return eksterntNavn;
	}

	public void setEksterntNavn(String eksterntNavn) {
		this.eksterntNavn = eksterntNavn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMerke() {
		return merke;
	}

	public void setMerke(String merke) {
		this.merke = merke;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public Long getAar() {
		return aar;
	}

	public void setAar(Long aar) {
		this.aar = aar;
	}
	
	@Column(name = "LEVERANDOR_NR")
	public String getLeverandorNr() {
		return leverandorNr;
	}

	public void setLeverandorNr(String leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	@Column(name = "LEVERANDOR_NAVN")
	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		this.leverandorNavn = leverandorNavn;
	}

	@Column(name = "LEVERANDOR_VALGBAR_FLAGG")
	public Long getLeverandorValgbarFlagg(){
		return leverandorValgbarFlagg;
	}
	
	public void setLeverandorValgbarFlagg(Long leverandorValgbarFlagg){
		this.leverandorValgbarFlagg = leverandorValgbarFlagg;
	}
	@Column(name = "DFU_ID")
	public Long getDfuId() {
		return dfuId;
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}

	@Column(name = "DFU_TLFNUMMER")
	public String getDfuTlfnummer() {
		return dfuTlfnummer;
	}

	public void setDfuTlfnummer(String dfuTlfnummer) {
		this.dfuTlfnummer = dfuTlfnummer;
	}
//	@Column(name = "DFU_SERIENUMMER")
//	public String getDfuSerienummer() {
//		return dfuSerienummer;
//	}
//
//	public void setDfuSerienummer(String dfuSerienummer) {
//		this.dfuSerienummer = dfuSerienummer;
//	}

	@Column(name = "DFU_STATUSNAVN")
	public String getDfustatus() {
		return dfustatus;
	}
	
	public void setDfustatus(String dfustatus) {
		this.dfustatus = dfustatus;
	}

	@Column(name = "DFU_SISTE_LIVSTEGN_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDfuSisteLivtegnDato() {
		return dfuSisteLivtegnDato;
	}

	public void setDfuSisteLivtegnDato(Date dfuSisteLivtegnDato) {
		this.dfuSisteLivtegnDato = dfuSisteLivtegnDato;
	}

	@Column(name = "DFU_SISTE_POSISJON_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDfuSistePosisjonDato() {
		return dfuSistePosisjonDato;
	}

	public void setDfuSistePosisjonDato(Date dfuSistePosisjonDato) {
		this.dfuSistePosisjonDato = dfuSistePosisjonDato;
	}

	@Transient
	public Date getDfuSistePosisjonDatoCET() {
		return dfuSistePosisjonDato != null ? MipssDateFormatter.convertFromGMTtoCET(dfuSistePosisjonDato) : null;
	}
	
	@Column(name = "AKTUELL_KONFIG_KLARTEKST")
	public String getAktuellKonfigKlartekst() {
		return aktuellKonfigKlartekst;	
	}

	public void setAktuellKonfigKlartekst(String sisteKonfigKlartekst) {
		this.aktuellKonfigKlartekst = sisteKonfigKlartekst;
	}

	@Column(name = "AKTUELL_STROPERIODE_KLARTEKST")
	public String getAktuellStroperiodeKlartekst() {
		return aktuellStroperiodeKlartekst;
	}

	public void setAktuellStroperiodeKlartekst(String sisteStroperiodeKlartekst) {
		this.aktuellStroperiodeKlartekst = sisteStroperiodeKlartekst;
	}
	
	@Column(name = "AKTUELL_STROPERIODE_FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAktuellStroperiodeFraDato(){
		return aktuellStroperiodeFraDato;
	}
	public void setAktuellStroperiodeFraDato(Date aktuellStroperiodeFraDato){
		this.aktuellStroperiodeFraDato = aktuellStroperiodeFraDato;
		
	}

	@Column(name = "AKTUELL_STROPERIODE_TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAktuellStroperiodeTilDato(){
		return aktuellStroperiodeTilDato;
	}
	public void setAktuellStroperiodeTilDato(Date aktuellStroperiodeTilDato){
		this.aktuellStroperiodeTilDato = aktuellStroperiodeTilDato;
	}
	
	@Column(name = "SPR_MED_DATAFANGST_FLAGG")
	public Long getSprMedDatafangstFlagg() {
		return sprMedDatafangstFlagg;
	}

	public void setSprMedDatafangstFlagg(Long sprMedDatafangstFlagg) {
		this.sprMedDatafangstFlagg = sprMedDatafangstFlagg;
	}

	@Column(name = "SPR_UTEN_DATAFANGST_FLAGG")
	public Long getSprUtenDatafangstFlagg() {
		return sprUtenDatafangstFlagg;
	}

	public void setSprUtenDatafangstFlagg(Long sprUtenDatafangstFlagg) {
		this.sprUtenDatafangstFlagg = sprUtenDatafangstFlagg;
	}

	@Column(name = "DFU_SISTE_PROD_POSISJON_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDfuSisteProdPosisjonDato() {
		return dfuSisteProdPosisjonDato;
	}

	public void setDfuSisteProdPosisjonDato(Date dfuSisteProdPosisjonDato) {
		this.dfuSisteProdPosisjonDato = dfuSisteProdPosisjonDato;
	}

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public String getTextForGUI() {
		return eksterntNavn;
	}

	@Column(name="DFU_SERIENUMMER")
	public String getDfuSerienummer() {
		return dfuSerienummer;
	}

	public void setDfuSerienummer(String dfuSerienummer) {
		this.dfuSerienummer = dfuSerienummer;
	}

	@Column(name="BESTILLING_STATUS")
	public String getBestillingStatus() {
		return bestillingStatus;
	}

	public void setBestillingStatus(String bestillingStatus) {
		this.bestillingStatus = bestillingStatus;
	}
	@Column(name = "BESTILLINGSTATUS_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBestillingStatusDato() {
		return bestillingStatusDato;
	}

	public void setBestillingStatusDato(Date bestillingStatusDato) {
		this.bestillingStatusDato = bestillingStatusDato;
	}
	@Column(name = "BESTILLING_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBestillingDato() {
		return bestillingDato;
	}

	public void setBestillingDato(Date bestillingDato) {
		this.bestillingDato = bestillingDato;
	}
	@Column(name="UKJENT_PRODUKSJON_FLAGG")
	public Boolean getUkjentProduksjon() {
		return ukjentProduksjon;
	}

	public void setUkjentProduksjon(Boolean ukjentProduksjon) {
		this.ukjentProduksjon = ukjentProduksjon;
	}
	
	@Column(name="UKJENT_STROMETODE_FLAGG")
	public Boolean getUkjentStrometode() {
		return ukjentStrometode;
	}
	public void setUkjentStrometode(Boolean ukjentStrometode) {
		this.ukjentStrometode = ukjentStrometode;
	}

	public void setEierNavn(String eierNavn) {
		this.eierNavn = eierNavn;
	}
	@Column(name="KJORETOY_EIER_NAVN")
	public String getEierNavn() {
		return eierNavn;
	}

	@Column(name = "AKTUELL_KLIPPEPERIODE_TEKST")
	public String getAktuellKlippeperiodeTekst() {
		return aktuellKlippeperiodeTekst;
	}

	public void setAktuellKlippeperiodeTekst(String aktuellKlippeperiodeTekst) {
		this.aktuellKlippeperiodeTekst = aktuellKlippeperiodeTekst;
	}

	@Column(name = "AKTUELL_KLIPPEPERIODE_FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAktuellKlippeperiodeFraDato() {
		return aktuellKlippeperiodeFraDato;
	}

	public void setAktuellKlippeperiodeFraDato(Date aktuellKlippeperiodeFraDato) {
		this.aktuellKlippeperiodeFraDato = aktuellKlippeperiodeFraDato;
	}

	@Column(name = "AKTUELL_KLIPPEPERIODE_TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAktuellKlippeperiodeTilDato() {
		return aktuellKlippeperiodeTilDato;
	}

	public void setAktuellKlippeperiodeTilDato(Date aktuellKlippeperiodeTilDato) {
		this.aktuellKlippeperiodeTilDato = aktuellKlippeperiodeTilDato;
	}

	@Column(name = "KANTKLIPP_FLAGG")
	public Long getKantklippFlagg() {
		return kantklippFlagg;
	}

	public void setKantklippFlagg(Long kantklippFlagg) {
		this.kantklippFlagg = kantklippFlagg;
	}
}
