package no.mesta.mipss.persistence.kontrakt;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.kjoretoyutstyr.*;
import no.mesta.mipss.persistence.stroing.Stroprodukt;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity @IdClass(ArtikkelProdtypeId.class)
@NamedQueries({
        @NamedQuery(name = ArtikkelProdtype.FIND_BY_CONTRACT, query = "select o from ArtikkelProdtype o where o.kontraktId = :id")
})
@Table(name = "ARTIKKEL_PRODTYPE")
public class ArtikkelProdtype implements Serializable, IRenderableMipssEntity {

    public static final String FIND_BY_CONTRACT = "ArtikkelProdtype.Kontrakt";

    @Id
    @Column(name="LEVERANDOR_NR")
    private String leverandorNr;
    @Id
    @Column(name="KONTRAKT_ID")
    private long kontraktId;
    @Id
    @Column(name="LEVKONTRAKT_ID")
    private String levkontraktId;
    @Id
    @Column(name="ARTIKKEL_ID")
    private String artikkelId;

    @Column
    private String artikkel;
    @Column
    private String oppgjorsenhet;
    @Column
    private double enhetspris;
    @ManyToOne
    @JoinColumn(name="SAMPRODUKSJON_ID")
    private Samproduksjon samproduksjon;
    @ManyToOne
    @JoinColumn(name="PRODTYPE_ID")
    private Prodtype prodtype;
    @ManyToOne
    @JoinColumn(name="STROPRODUKT_ID")
    private Stroprodukt stroprodukt;

    @OneToOne
    @PrimaryKeyJoinColumns({
            @PrimaryKeyJoinColumn(name = "LEVERANDOR_NR", referencedColumnName = "LEVERANDOR_NR"),
            @PrimaryKeyJoinColumn(name = "LEVKONTRAKT_ID", referencedColumnName = "LEVKONTRAKT_IDENT"),
            @PrimaryKeyJoinColumn(name = "ARTIKKEL_ID", referencedColumnName = "ARTIKKEL_IDENT")
    })
    private AgrArtikkelV agrArtikkelV;

    public ArtikkelProdtype() {
    }

    public ArtikkelProdtype(String artikkelId, Prodtype prodtype, Stroprodukt stroprodukt) {
        this.artikkelId = artikkelId;
        this.prodtype = prodtype;
        this.stroprodukt = stroprodukt;
    }

    public boolean isUtgatt() {
        if (agrArtikkelV != null && agrArtikkelV.getArtikkelStatus() != null) {
            Date now = new Date();
            return !(
                    agrArtikkelV.getArtikkelStatus().equalsIgnoreCase("N") &&
                    agrArtikkelV.getFraDato() != null && agrArtikkelV.getFraDato().before(now) &&
                    agrArtikkelV.getTilDato() != null && agrArtikkelV.getTilDato().after(now)
            );
        }

        return true;
    }

    public AgrArtikkelV getAgrArtikkelV() {
        return agrArtikkelV;
    }

    public void setAgrArtikkelV(AgrArtikkelV agrArtikkelV) {
        this.agrArtikkelV = agrArtikkelV;
    }

    public String getLeverandorNr() {
        return leverandorNr;
    }

    public void setLeverandorNr(String leverandorNr) {
        this.leverandorNr = leverandorNr;
    }

    public long getKontraktId() {
        return kontraktId;
    }

    public void setKontraktId(long kontraktId) {
        this.kontraktId = kontraktId;
    }

    public String getLevkontraktId() {
        return levkontraktId;
    }

    public void setLevkontraktId(String levkontraktId) {
        this.levkontraktId = levkontraktId;
    }

    public String getArtikkelId() {
        return artikkelId;
    }

    public void setArtikkelId(String artikkelId) {
        this.artikkelId = artikkelId;
    }

    public String getArtikkel() {
        return artikkel;
    }

    public void setArtikkel(String artikkel) {
        this.artikkel = artikkel;
    }

    public String getOppgjorsenhet() {
        return oppgjorsenhet;
    }

    public void setOppgjorsenhet(String oppgjorsenhet) {
        this.oppgjorsenhet = oppgjorsenhet;
    }

    public double getEnhetspris() {
        return enhetspris;
    }

    public void setEnhetspris(double enhetspris) {
        this.enhetspris = enhetspris;
    }

    public Samproduksjon getSamproduksjon() {
        return samproduksjon;
    }

    public void setSamproduksjon(Samproduksjon samproduksjon) {
        this.samproduksjon = samproduksjon;
    }

    public Prodtype getProdtype() {
        return prodtype;
    }

    public void setProdtype(Prodtype prodtype) {
        this.prodtype = prodtype;
    }

    public Stroprodukt getStroprodukt() {
        return stroprodukt;
    }

    public void setStroprodukt(Stroprodukt stroprodukt) {
        this.stroprodukt = stroprodukt;
    }

    @Override
    public String getTextForGUI() {
        return null;
    }

}