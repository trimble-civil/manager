package no.mesta.mipss.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeListener;
import java.io.Serializable;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.PropertyChangeSource;

/**
 * Delklasse for alle entiteter som inneholder feltene {@code opprettetAv}, {@code opprettetDato}, 
 * {@code endretAv} og {@code endretDato}.
 * 
 * Disse feltene kan ikke oppdateres på GUI: de hentes fra databasen og settes automatisk rett før 
 *  persistering/oppdatering, derfor trenger man ikke å sende eventer på endringene de gjennomgår.
 * 
 * Klassen inkluderer også et eget felt for angivelse av brukerens identifikasjon for bruk på klientsiden.
 * @author jorge
 */
@SuppressWarnings("serial")
@Embeddable
public class OwnedMipssEntity implements Serializable, PropertyChangeSource {
	private static final String datoformat = "dd.MM.yyyy";
    private String signatur;
    
    private String endretAv;
    private Date endretDato;
    
    private String opprettetAv;
    private Date opprettetDato;
	private PropertyChangeSupport props;
    
    public OwnedMipssEntity() {}
    
    /**
     * Setter brukerens identifikasjon. Denne identifikasjonen lagres i feltene
     * OPPRETTET_AV og ENDRET_AV og kan ikke overstide dens lengde.
     * @param signatur
     */
    public void setUserSignatur(String signatur) {
    	String old = this.signatur;
        this.signatur = signatur;
        getProps().firePropertyChange("signatur", old, signatur);
    }

    /**
     * Henter brukerens signatur. 
     * @return signaturen eller "IkkeSatt" hvis feltet ikke var satt på forhånd.
     */
    @Transient
    public String getUserSignatur() {
        if (signatur == null) {
            signatur = "IkkeSatt";
        }
        return signatur;
    }
    
    /**
     * henter brukersignatur for siste endring.
     * @return
     */
    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    /**
     * Setter brukersignatur for opprettelsen.
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
    	String old = this.endretAv;
        this.endretAv = endretAv;
        getProps().firePropertyChange("endretAv", old, endretAv);
    }

    /**
     * henter dato for siste endring.
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }
    
    /**
     * setter dato for siste endring.
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
    	Date old = this.endretDato;
        this.endretDato = endretDato;
        getProps().firePropertyChange("endretDato", old, endretDato);
    }
    
    /**
     * @return endringsdato som tekst. teksten er tom hvis endirngsdato ikke er satt.
     */
    @Transient
    public String getEndretDatoString() {
    	SimpleDateFormat formatter = new SimpleDateFormat(datoformat);
    	if(endretDato == null) {
    		return "";
    	}
        return formatter.format(endretDato);
    }

    /**
     * henter brukersignatur for opprettelsen
     * @return
     */
    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Setter brukersignatur for opprettelsen
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
    	String old = opprettetAv;
        this.opprettetAv = opprettetAv;
        getProps().firePropertyChange("opprettetAv", old, opprettetAv);
    }

    /**
     * henter tidsstempel for opprettelsen
     * @return
     */
    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * @return opprettelsesdato som tekst. Hvis opprettelsesdato ikke er satt, 
     * returneres dagens dato.
     */
    @Transient
    public String getOpprettetDatoString() {
    	SimpleDateFormat formatter = new SimpleDateFormat(datoformat);
    	if(opprettetDato == null) {
    		return formatter.format(Clock.now());
    	}
        return formatter.format(opprettetDato);
    }

    /**
     * setter tidsstempel for opprettelsen
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
    	Date old = this.opprettetDato;
        this.opprettetDato = opprettetDato;
        getProps().firePropertyChange("opprettetDato", old, opprettetDato);
    }

    /** {@inheritDoc} */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}
	
	protected PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}
}
