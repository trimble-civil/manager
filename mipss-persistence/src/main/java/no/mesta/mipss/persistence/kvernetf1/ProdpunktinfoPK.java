package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Nøkkelklasse for prodpunkt info
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class ProdpunktinfoPK implements Serializable {
    private Long dfuId;
    private Date gmtTidspunkt;

    /**
     * Konstruktør
     * 
     */
    public ProdpunktinfoPK() {
    }

    /**
     * Konstruktør
     * 
     * @param dfuId
     * @param gmtTidspunkt
     */
    public ProdpunktinfoPK(Long dfuId, Date gmtTidspunkt) {
        this.dfuId = dfuId;
        this.gmtTidspunkt = gmtTidspunkt;
    }

    /**
     * Dfu
     * 
     * @param dfuId
     */
    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    /**
     * Dfu
     * 
     * @return
     */
    public Long getDfuId() {
        return dfuId;
    }

    /**
     * tidspunkt
     * 
     * @param gmtTidspunkt
     */
    public void setGmtTidspunkt(Date gmtTidspunkt) {
        this.gmtTidspunkt = gmtTidspunkt;
    }

    /**
     * tidspunkt
     * 
     * @return
     */
    public Date getGmtTidspunkt() {
        return gmtTidspunkt;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dfuId).append(gmtTidspunkt).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof ProdpunktinfoPK)) {return false;}
        
        ProdpunktinfoPK other = (ProdpunktinfoPK) o;
        
        return new EqualsBuilder().append(dfuId, other.dfuId).append(gmtTidspunkt, other.gmtTidspunkt).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("dfuId", dfuId).
            append("gmtTidspunkt", gmtTidspunkt).
            toString();
    }
}
