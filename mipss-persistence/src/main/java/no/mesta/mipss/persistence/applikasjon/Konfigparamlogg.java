package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Inneholder system og modulkonfigurasjon logg
 * 
 */
@SuppressWarnings("serial")
@Entity(name="Konfigparamlogg")
@Table(name="KONFIGPARAMLOGG")
@NamedQueries({
	@NamedQuery(name = Konfigparamlogg.QUERY_GET_HISTORY_FOR_APP_NAVN, query = "SELECT k FROM Konfigparamlogg k where k.applikasjon=:applikasjon and k.navn=:navn order by k.id")
})
public class Konfigparamlogg extends MipssEntityBean<Konfigparamlogg> implements Serializable, IRenderableMipssEntity, PropertyChangeSource {

    private static final Logger log = LoggerFactory.getLogger(Konfigparamlogg.class);

	public static final String QUERY_GET_HISTORY_FOR_APP_NAVN = "Konfigparamlogg.getHistoryForAppNavn";
    
    private String applikasjon;
    private String navn;
    private String verdi;
    private String beskrivelse;
    
    private String endretAv;
    private Date endretDato;
    private String opprettetAv;
    private Date opprettetDato;
    private String slettetAv;
    private Date slettetDato;
    
    private Integer version;
	private Long id; 
      
	public Konfigparamlogg() {
        log("Konfigparamlogg() constructed");
    }
	@Id
	@Column(nullable = false)
	public Long getId(){
		return id;
	}
    @Column(nullable = false)
    public String getApplikasjon() {
        return applikasjon;
    }
    
    public void setApplikasjon(String applikasjon) {
        log.trace("setApplikasjon({})", applikasjon);
        this.applikasjon = applikasjon;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }
    
    public void setNavn(String navn) {
        log.trace("setNavn({})", navn);
        this.navn = navn;
    }

    @Column(nullable = false)
    public String getVerdi() {
        return verdi;
    }
    
    public void setId(Long id){
    	this.id = id;
    }
    
    public void setVerdi(String verdi) {
        log.trace("setVerdi({})", verdi);
        this.verdi = verdi;
    }

    @Column(nullable = false)
    public String getBeskrivelse() {
        return beskrivelse;
    }
    
    public void setBeskrivelse(String beskrivelse) {
        log.trace("setBeskrivelse({})", beskrivelse);
        this.beskrivelse = beskrivelse;
    }
    
    @Column(name = "VERSJON")
	public Integer getVersion() {
		return version;
	}
	
	public void setVersion(Integer version) {
		this.version = version;
	}
    public String getTextForGUI() {
		return navn;
	}
    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }
    
    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }
    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }
    
    
    @Column(name="SLETTET_AV")
    public String getSlettetAv() {
        return slettetAv;
    }

    public void setSlettetAv(String slettetAv) {
        this.slettetAv = slettetAv;
    }
    @Column(name="SLETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSlettetDato() {
        return slettetDato;
    }

    public void setSlettetDato(Date slettetDato) {
        this.slettetDato = slettetDato;
    }
    
	/** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Konfigparamlogg)) {return false;}
        
        Konfigparamlogg other = (Konfigparamlogg) o;
        
        return new EqualsBuilder().
            append(id, other.getId()).
            isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
        	append("id", getId()).
            append("applikasjon", getApplikasjon()).
            append("navn", getNavn()).
            append("verdi", getVerdi()).
            append("beskrivelse", getBeskrivelse()).
            toString();        
    }
}
