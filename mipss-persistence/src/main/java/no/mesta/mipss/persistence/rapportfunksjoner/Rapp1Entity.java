package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.List;


public interface Rapp1Entity extends Serializable{

	 List<R12Prodtype> getProdtyper();
	 List<R12Stroprodukt> getStroprodukter();
	 List<Materiale> getMaterialer();
	 Boolean getPaaKontrakt();
}
