package no.mesta.mipss.persistence.driftslogg;

import java.io.Serializable;

public class DeviantActivity implements Serializable {

    private String contractorName;
    private String vehicleExternalName;
    private Long vehicleId;
    private Long activityId;
    private Long tripId;
    private String fromDateTime;
    private String toDateTime;
    private String rodeName;
    private String deviationMessage;
    private String prodType;
    private String gritType;
    private Double amount;
    private String unit;
    private String reporterComment;
    private String contractorComment;
    private boolean negativeDeviation;

    public DeviantActivity() {}

    public DeviantActivity(String contractorName, String vehicleExternalName, Long vehicleId, Long activityId,
            Long tripId, String fromDateTime, String toDateTime, String rodeName, String deviationMessage,
            String prodType, String gritType, Double amount, String unit, String reporterComment,
            String contractorComment, boolean negativeDeviation) {
        this.contractorName = contractorName;
        this.vehicleExternalName = vehicleExternalName;
        this.vehicleId = vehicleId;
        this.activityId = activityId;
        this.tripId = tripId;
        this.fromDateTime = fromDateTime;
        this.toDateTime = toDateTime;
        this.rodeName = rodeName;
        this.deviationMessage = deviationMessage;
        this.prodType = prodType;
        this.gritType = gritType;
        this.amount = amount;
        this.unit = unit;
        this.reporterComment = reporterComment;
        this.contractorComment = contractorComment;
        this.negativeDeviation = negativeDeviation;
    }

    public DeviantActivity(DeviantActivity activity) {
        this(
            activity.getContractorName(),
            activity.getVehicleExternalName(),
            activity.getVehicleId(),
            activity.getActivityId(),
            activity.getTripId(),
            activity.getFromDateTime(),
            activity.getToDateTime(),
            activity.getRodeName(),
            activity.getDeviationMessage(),
            activity.getProdType(),
            activity.getGritType(),
            activity.getAmount(),
            activity.getUnit(),
            activity.getReporterComment(),
            activity.getContractorComment(),
            activity.isNegativeDeviation()
        );
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getVehicleExternalName() {
        return vehicleExternalName;
    }

    public void setVehicleExternalName(String vehicleExternalName) {
        this.vehicleExternalName = vehicleExternalName;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public String getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(String fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public String getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(String toDateTime) {
        this.toDateTime = toDateTime;
    }

    public String getRodeName() {
        return rodeName;
    }

    public void setRodeName(String rodeName) {
        this.rodeName = rodeName;
    }

    public String getDeviationMessage() {
        return deviationMessage;
    }

    public void setDeviationMessage(String deviationMessage) {
        this.deviationMessage = deviationMessage;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public String getGritType() {
        return gritType;
    }

    public void setGritType(String gritType) {
        this.gritType = gritType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getReporterComment() {
        return reporterComment;
    }

    public void setReporterComment(String reporterComment) {
        this.reporterComment = reporterComment;
    }

    public String getContractorComment() {
        return contractorComment;
    }

    public void setContractorComment(String contractorComment) {
        this.contractorComment = contractorComment;
    }

    public boolean isNegativeDeviation() {
        return negativeDeviation;
    }

    public void setNegativeDeviation(boolean negativeDeviation) {
        this.negativeDeviation = negativeDeviation;
    }

}