package no.mesta.mipss.persistence.driftslogg;

/**
 * Created by Tommy Skara on 29.11.2016.
 */
public class ActivityRodeIdName {

    private Integer rodeId;
    private String rodeName;

    public static String[] getPropertiesArray() {
        return new String[]{
                "rodeId",
                "rodeName"
        };
    }

    public Integer getRodeId() {
        return rodeId;
    }

    public void setRodeId(Integer rodeId) {
        this.rodeId = rodeId;
    }

    public String getRodeName() {
        return rodeName;
    }

    public void setRodeName(String rodeName) {
        this.rodeName = rodeName;
    }

}