package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Forsikringsskade 
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name="FORSIKRINGSSKADE_XV")
@NamedQueries({
    @NamedQuery(name = Forsikringsskade.QUERY_FIND_ALL, query = "SELECT f FROM Forsikringsskade f"),
	@NamedQuery(name = Forsikringsskade.QUERY_FIND_ENTITY_INFO_BY_SAK, query = "SELECT new no.mesta.mipss.persistence.mipssfield.FeltEntityInfo(f.guid, f.refnummer, f.slettetDato) FROM Forsikringsskade f where f.sakGuid=:sakGuid"),
	@NamedQuery(name = Forsikringsskade.QUERY_HENT_MED_REFNUMMER, query = "SELECT f FROM Forsikringsskade f where f.refnummer=:refnummer"),
	@NamedQuery(name = Forsikringsskade.QUERY_HENT_MED_ELRAPP_ID, query = "SELECT f FROM Forsikringsskade f where f.elrappDokumentIdent=:elrappId and f.sak.kontraktId=:driftkontraktId")

    })
@NamedNativeQueries( { @NamedNativeQuery(name = Forsikringsskade.QUERY_GET_BILDER, query = 
		"SELECT " + "b.GUID, b.FILNAVN, b.BILDE_LOB, b.BESKRIVELSE, b.HOYDE, b.SMAABILDE_LOB, b.BREDDE, "
		+ "b.OPPRETTET_DATO, b.ENDRET_AV, b.OPPRETTET_AV, b.ENDRET_DATO FROM "
		+ "SKADE_BILDE sb JOIN Bilde b ON sb.BILDE_GUID = b.GUID WHERE sb.SKADE_GUID = ?1", resultSetMapping = "BildeResults"),
		
		@NamedNativeQuery(name = Forsikringsskade.QUERY_GET_BILDER_UTENRELASJONER, query = 
				"SELECT " + "b.GUID, b.FILNAVN, b.BILDE_LOB,  b.BESKRIVELSE,  b.HOYDE,  b.SMAABILDE_LOB,  b.BREDDE, "
				+ "b.OPPRETTET_DATO,  b.ENDRET_AV,  b.OPPRETTET_AV,  b.ENDRET_DATO  FROM "
				+ "SKADE_BILDE sb  JOIN Bilde b ON sb.BILDE_GUID = b.GUID  WHERE  sb.SKADE_GUID = ?1 " 
				+ "and not exists (select * from avvik_bilde where bilde_guid=b.guid) "
				+ "and not exists (select * from hendelse_bilde where bilde_guid=b.guid) "
				+ "and not exists (select * from skred_bilde where bilde_guid=b.guid) "
				+ " ", resultSetMapping = "BildeResults")})
@SqlResultSetMappings( { 
	@SqlResultSetMapping(name = "SkadeBildeResults", entities = { 
			@EntityResult(entityClass = Bilde.class, fields = {
				@FieldResult(name = "guid", column = "GUID"), @FieldResult(name = "filnavn", column = "FILNAVN"),
				@FieldResult(name = "bildeLob", column = "BILDE_LOB"), @FieldResult(name = "tekst", column = "BESKRIVELSE"),
				@FieldResult(name = "hoyde", column = "HOYDE"), @FieldResult(name = "smaabildeLob", column = "SMAABILDE_LOB"),
				@FieldResult(name = "nyDato", column = "OPPRETTET_DATO"), @FieldResult(name = "endrer", column = "ENDRET_AV"),
				@FieldResult(name = "oppretter", column = "OPPRETTET_AV"),
				@FieldResult(name = "endringsDato", column = "ENDRET_DATO"), @FieldResult(name = "bredde", column = "BREDDE") 
			}) 
		}) 
	})	
@SequenceGenerator(name = Forsikringsskade.REFNUMMER_ID_SEQ, sequenceName = "FORSIKRINGSSKADE_REFNUMMER_SEQ", initialValue = 1, allocationSize = 1)

public class Forsikringsskade extends MipssEntityBean<Forsikringsskade> implements Serializable, Cloneable,MipssFieldDetailItem {
	private static final Logger log = LoggerFactory.getLogger(Forsikringsskade.class);

    private static final String ID_NAME = "guid";
    private static final String[] ID_NAMES = new String[] { ID_NAME };
    public static final String QUERY_FIND_ALL = "Forsikringsskade.findAll";
	public static final String QUERY_GET_BILDER = "Forsikringsskade.getBilder";
	public static final String QUERY_GET_BILDER_UTENRELASJONER = "Forsikringsskade.getBilderUtenRelasjoner";
	public static final String QUERY_HENT_MED_REFNUMMER = "Forsikringsskade.hentMedRefnummer";
	public static final String QUERY_HENT_MED_ELRAPP_ID = "Forsikringsskade.hentMedElrappId";
    public static final String REFNUMMER_ID_SEQ = "forsikringsskadeRefnummerSeq";
	public static final String QUERY_FIND_ENTITY_INFO_BY_SAK = "Forsikringsskade.findGuidBySak";
	
	
	@Column(name="ANMELDT_FLAGG")
    private Boolean anmeldt;
	
	@Column(name="ENTREPENOR_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date entrepenorDato;
	@Column(name="ENTREPENOR_NAVN")
    private String entrepenorNavn;
	@Column(name="ESTIMAT_KOSTNAD")
    private Double estimatKostnad;
	@Column(name="FRITEKST")
    private String fritekst;
    @Id
    @Column(nullable = false)
    private String guid;
    @Column(name="MELDT_AV")
    private String meldtAv;
    @Column(name="MELDT_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date meldtDato;
    @Column(name="REPARERT_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reparertDato;
    @Column(name="SKADE_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date skadeDato;
    @OneToMany(mappedBy="skade", cascade = { CascadeType.ALL }, orphanRemoval=true)
    private List<SkadeKontakt> skadeKontakter = new ArrayList<SkadeKontakt>();
    @Column(name="SKADEVOLDER_BILMERKE")
    private String skadevolderBilmerke;
    @Column(name="SKADEVOLDER_HENGER")
    private String skadevolderHenger;
    @Column(name="SKADEVOLDER_REGNR")
    private String skadevolderRegnr;
    @Column(name="UTSATT_REP_KOMMENTAR")
    private String utsattRepKommentar;
    @Column(name="TILLEGGSOPPLYSNINGER")
	private String tilleggsopplysninger;
    
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Forsikringsskade.REFNUMMER_ID_SEQ)
	@Column(name="refnummer")
	private Long refnummer;
	@Column(name="RAPPORTERT_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date rapportertDato;
	@Column(name="ELRAPP_DOKUMENT_IDENT")
	private Long elrappDokumentIdent;
	@Column(name="ELRAPP_VERSJON")
	private Long elrappVersjon;
	@Column(name="SKJEMA_EMNE")
	private String skjemaEmne;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name="ENDRET_AV")
	private String endretAv;
	@Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endretDato;
	@Column(name="SLETTET_AV")
    private String slettetAv;
	@Column(name="SLETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date slettetDato;
    @Column(name="ANTATT_TIDSROM_FOR_SKADEN")
	private String antattTidsromForSkaden;
    
//	@ManyToMany(cascade=CascadeType.REFRESH)
//	@JoinTable(	name = "SKADE_BILDE_XV", 
//				joinColumns = 		 { @JoinColumn(name = "SKADE_GUID", referencedColumnName = "GUID") }, 		
//				inverseJoinColumns = { @JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID") })
//	private List<Bilde> bilder = new ArrayList<Bilde>();
	@OneToMany(mappedBy = "forsikringsskade", cascade = CascadeType.ALL, orphanRemoval=true)
	private List<ForsikringsskadeBilde> bilder = new ArrayList<ForsikringsskadeBilde>();
	
	@OneToOne
	@JoinColumn(name = "SAK_GUID", referencedColumnName = "GUID")
	private Sak sak;
	@Column(name = "SAK_GUID", nullable = true, insertable = false, updatable = false)
	private String sakGuid;
	@Transient
	private List<Skadekontakttype> kontaktList;
	
	/**
	 * Returnerer antall bilder tilknyttet funnet
	 * 
	 * @return
	 */
	@Transient
	public Integer getAntallBilder() {
		if (bilder == null) {
			return Integer.valueOf(0);
		} else {
			return Integer.valueOf(bilder.size());
		}
	}
	/**
	 * Legger til et bilde
	 * 
	 * @param b
	 */
	public void addBilde(ForsikringsskadeBilde b) {
		log.trace("addBilde({})",  b);
		List<ForsikringsskadeBilde> old = new ArrayList<ForsikringsskadeBilde>(getBilder());
		log.trace("addBilde({})", b);
		getBilder().add(b);
		firePropertyChange("bilder", old, getBilder());
	}
	
	public Sak getSak() {
		return sak;
	}

	public String getSakGuid() {
		return sakGuid;
	}

	public void setSak(Sak sak) {
		Sak old = this.sak;
		this.sak = sak;
		if (sak!=null){
			setSakGuid(sak.getGuid());
		}else{
			setSakGuid(null);
		}
		firePropertyChange("sak", old, sak);
			
	}

	public void removeBilde(ForsikringsskadeBilde b) {
		List<ForsikringsskadeBilde> old = new ArrayList<ForsikringsskadeBilde>(getBilder());
		log.trace("removeBilde({})", b);
		getBilder().remove(b);
		firePropertyChange("bilder", old, getBilder());
	}
	public void setSakGuid(String sakGuid) {
		String old = this.sakGuid;
		this.sakGuid = sakGuid;
		firePropertyChange("sakGuid", old, sakGuid);
	}
	
	public List<ForsikringsskadeBilde> getBilder() {
		return bilder;
	}
	public void setBilder(List<ForsikringsskadeBilde> bilder) {
		List<ForsikringsskadeBilde> old = this.bilder;
		this.bilder = bilder;
		firePropertyChange("bilder", old, bilder);
	}
    /**
     * Konstruktør
     * 
     */
    public Forsikringsskade() {
    }
  
    /**
     * Anmeldt
     * 
     * @return
     */
    public Boolean getAnmeldt() {
        return anmeldt;
    }

    @Transient
    public List<SkadeKontakt> getBergingsSelskaper(){
    	return getSkadeKontaktStringList(SkadeKontakt.BERGINSSELSKAP);
    }
    @Transient
    public SkadeKontakt getBerg1(){
    	SkadeKontakt berg1 = null;
    	List<SkadeKontakt> sl = getSkadeKontaktStringList(SkadeKontakt.BERGINSSELSKAP);
    	if (sl.size()>0){
    		berg1 = sl.get(0);
    	}
    	return berg1;
    }
    
    @Transient
    public SkadeKontakt getBerg2(){
    	SkadeKontakt berg2 = null;
    	List<SkadeKontakt> sl = getSkadeKontaktStringList(SkadeKontakt.BERGINSSELSKAP);
    	if (sl.size()>1){
    		berg2 = sl.get(1);
    	}
    	return berg2;
    }
   
    @Transient
    public String getNavnBerg1(){
    	SkadeKontakt berg1 = getBerg1();
    	return berg1==null?null:berg1.getNavn();
    }
    public void setNavnBerg1(String navnBerg1){
    	SkadeKontakt berg1 = getBerg1();
    	String old = berg1==null?null:berg1.getNavn();
    	if (berg1==null){
    		berg1 = new SkadeKontakt();
    		berg1.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg1);
    	}
    	berg1.setNavn(navnBerg1);
    	getProps().firePropertyChange("navnBerg1", old, navnBerg1);
    }
    @Transient
    public Long getTlfBerg1(){
    	SkadeKontakt berg1 = getBerg1();
    	return berg1==null?null:berg1.getTlfnummer();
    }
    public void setTlfBerg1(Long tlfBerg1){
    	SkadeKontakt berg1 = getBerg1();
    	Long old = berg1==null?null:berg1.getTlfnummer();
    	if (berg1==null){
    		berg1 = new SkadeKontakt();
    		berg1.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg1);
    	}
    	berg1.setTlfnummer(tlfBerg1);
    	getProps().firePropertyChange("tlfBerg1", old, tlfBerg1);
    }
    @Transient
    public String getKontaktNavnBerg1(){
    	SkadeKontakt berg1 = getBerg1();
    	return berg1==null?null:berg1.getKontaktNavn();
    }
    public void setKontaktNavnBerg1(String kontaktNavnBerg1){
    	SkadeKontakt berg1 = getBerg1();
    	String old = berg1==null?null:berg1.getKontaktNavn();
    	if (berg1==null){
    		berg1 = new SkadeKontakt();
    		berg1.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg1);
    	}
    	berg1.setKontaktNavn(kontaktNavnBerg1);
    	getProps().firePropertyChange("kontaktNavnBerg1", old, kontaktNavnBerg1);
    }
    @Transient
    public Long getKontaktTlfBerg1(){
    	SkadeKontakt berg1 = getBerg1();
    	return berg1==null?null:berg1.getKontaktTlfnummer();
    }
    public void setKontaktTlfBerg1(Long kontaktTlfBerg1){
    	SkadeKontakt berg1 = getBerg1();
    	Long old = berg1==null?null:berg1.getKontaktTlfnummer();
    	if (berg1==null){
    		berg1 = new SkadeKontakt();
    		berg1.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg1);
    	}
    	berg1.setKontaktTlfnummer(kontaktTlfBerg1);
    	getProps().firePropertyChange("kontaktTlfBerg1", old, kontaktTlfBerg1);
    }
    @Transient
    public String getResultatBerg1(){
    	SkadeKontakt berg1 = getBerg1();
    	return berg1==null?null:berg1.getResultat();
    }
    public void setResultatBerg1(String resultatBerg1){
    	SkadeKontakt berg1 = getBerg1();
    	String old = berg1==null?null:berg1.getResultat();
    	if (berg1==null){
    		berg1 = new SkadeKontakt();
    		berg1.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg1);
    	}
    	berg1.setResultat(resultatBerg1);
    	getProps().firePropertyChange("resultatBerg1", old, resultatBerg1);
    }
    @Transient
    public String getNavnBerg2(){
    	SkadeKontakt berg2 = getBerg2();
    	return berg2==null?null:berg2.getNavn();
    }
    public void setNavnBerg2(String navnBerg2){
    	SkadeKontakt berg2 = getBerg2();
    	String old = berg2==null?null:berg2.getNavn();
    	if (berg2==null){
    		berg2 = new SkadeKontakt();
    		berg2.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg2);
    	}
    	berg2.setNavn(navnBerg2);
    	getProps().firePropertyChange("navnBerg2", old, navnBerg2);
    }
    @Transient
    public Long getTlfBerg2(){
    	SkadeKontakt berg2 = getBerg2();
    	return berg2==null?null:berg2.getTlfnummer();
    }
    public void setTlfBerg2(Long tlfBerg2){
    	SkadeKontakt berg2 = getBerg2();
    	Long old = berg2==null?null:berg2.getTlfnummer();
    	if (berg2==null){
    		berg2 = new SkadeKontakt();
    		berg2.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg2);
    	}
    	berg2.setTlfnummer(tlfBerg2);
    	getProps().firePropertyChange("tlfBerg2", old, tlfBerg2);
    }
    @Transient
    public String getKontaktNavnBerg2(){
    	SkadeKontakt berg2 = getBerg2();
    	return berg2==null?null:berg2.getKontaktNavn();
    }
    public void setKontaktNavnBerg2(String kontaktNavnBerg2){
    	SkadeKontakt berg2 = getBerg2();
    	String old = berg2==null?null:berg2.getKontaktNavn();
    	if (berg2==null){
    		berg2 = new SkadeKontakt();
    		berg2.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg2);
    	}
    	berg2.setKontaktNavn(kontaktNavnBerg2);
    	getProps().firePropertyChange("kontaktNavnBerg2", old, kontaktNavnBerg2);
    }
    @Transient
    public Long getKontaktTlfBerg2(){
    	SkadeKontakt berg2 = getBerg2();
    	return berg2==null?null:berg2.getKontaktTlfnummer();
    }
    public void setKontaktTlfBerg2(Long kontaktTlfBerg2){
    	SkadeKontakt berg2 = getBerg2();
    	Long old = berg2==null?null:berg2.getKontaktTlfnummer();
    	if (berg2==null){
    		berg2 = new SkadeKontakt();
    		berg2.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg2);
    	}
    	berg2.setKontaktTlfnummer(kontaktTlfBerg2);
    	getProps().firePropertyChange("kontaktTlfBerg2", old, kontaktTlfBerg2);
    }
    @Transient
    public String getResultatBerg2(){
    	SkadeKontakt berg2 = getBerg2();
    	return berg2==null?null:berg2.getResultat();
    }
    public void setResultatBerg2(String resultatBerg2){
    	SkadeKontakt berg2 = getBerg2();
    	String old = berg2==null?null:berg2.getResultat();
    	if (berg2==null){
    		berg2 = new SkadeKontakt();
    		berg2.setType(getType(SkadeKontakt.BERGINSSELSKAP));
    		skadeKontakter.add(berg2);
    	}
    	berg2.setResultat(resultatBerg2);
    	getProps().firePropertyChange("resultatBerg2", old, resultatBerg2);
    }
   
    
    public void setAntattTidsromForSkaden(String antattTidsromForSkaden){
    	String old = this.antattTidsromForSkaden;
    	this.antattTidsromForSkaden = antattTidsromForSkaden;
    	firePropertyChange("antattTidsromForSkaden", old, antattTidsromForSkaden);
    }
    public String getAntattTidsromForSkaden(){
    	return this.antattTidsromForSkaden;
    }
    @Transient
    public Boolean getBroyteskade(){
    	return false;//TODO må antakelig gjøre noe her.. lese et flagg fra db e.l.
    }

    @Transient
    public String getEierAdresse(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.EIER_AV_KJORETOY);
    	String t = s==null?null:s.getAdresse();
    	return t;
    }
    public void setEierAdresse(String eierAdresse){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.EIER_AV_KJORETOY);
    	String old = s==null?null:s.getAdresse();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.EIER_AV_KJORETOY));
    		skadeKontakter.add(s);
    	}
    	s.setAdresse(eierAdresse);
    	getProps().firePropertyChange("eierAdresse", old, eierAdresse);
    }

    @Transient
    public String getEierAvKjoretoy(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.EIER_AV_KJORETOY);
    	String e = s==null?null:s.getNavn();
    	return e;
    }
    public void setEierAvKjoretoy(String eierAvKjoretoy){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.EIER_AV_KJORETOY);
    	String old = s==null?null:s.getNavn();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.EIER_AV_KJORETOY));
    		skadeKontakter.add(s);
    	}
    	s.setNavn(eierAvKjoretoy);
    	getProps().firePropertyChange("eierAvKjoretoy", old, eierAvKjoretoy);
    }

    @Transient
    public Long getEierTelefonNr(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.EIER_AV_KJORETOY);
    	return s==null?null:s.getTlfnummer();
    }
    
    public void setEierTelefonNr(Long eierTelefonNr){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.EIER_AV_KJORETOY);
    	Long old = s==null?null:s.getTlfnummer();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.EIER_AV_KJORETOY));
    		skadeKontakter.add(s);
    	}
    	s.setTlfnummer(eierTelefonNr);
    	getProps().firePropertyChange("eierTelefonNr", old, eierTelefonNr);
    }
    
    /**
     * Hvem endret registringen
     * @return
     */
    public String getEndretAv() {
        return endretAv;
    }
    
    public String getTilleggsopplysninger(){
    	return tilleggsopplysninger;
    }
    /**
     * Når ble registreringen endret
     * 
     * @return
     */
    public Date getEndretDato() {
        return endretDato;
    }
    /**
     * Entrepenør dato
     * 
     * @return
     */
    public Date getEntrepenorDato() {
        return entrepenorDato;
    }
    /**
     * Navn til entrepenør
     * 
     * @return
     */
    public String getEntrepenorNavn() {
        return entrepenorNavn;
    }
    /**
     * Estimatkostnad
     * 
     * @return
     */
    public Double getEstimatKostnad() {
        return estimatKostnad;
    }
    @Transient
    public String getForerNavn(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.FORER_AV_KJORETOY);
    	String f = s==null?null:s.getNavn();
    	return f;
    }
    public void setForerNavn(String forerNavn){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.FORER_AV_KJORETOY);
    	String old = s==null?null:s.getNavn();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.FORER_AV_KJORETOY));
    		skadeKontakter.add(s);
    	}
    	s.setNavn(forerNavn);
    	getProps().firePropertyChange("forerNavn", old, forerNavn);
    }
    @Transient
    public Long getForerTlf(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.FORER_AV_KJORETOY);
    	return s==null?null:s.getTlfnummer();
    }
    public void setForerTlf(Long forerTlf){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.FORER_AV_KJORETOY);
    	Long old = s==null?null:s.getTlfnummer();
    	if (s==null){
    		s=new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.FORER_AV_KJORETOY));
    		skadeKontakter.add(s);
    	}
    	s.setTlfnummer(forerTlf);
    	getProps().firePropertyChange("forerTlf", old, forerTlf);
    }
    @Transient
    public String getForsikringsselskap(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.FORSIKRINGSELSKAP);
    	String fs = s==null?null:s.getNavn();
    	return fs;
    }
    public void setForsikringsselskap(String forsikringsselskap){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.FORSIKRINGSELSKAP);
    	String old = s==null?null:s.getNavn();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.FORSIKRINGSELSKAP));
    		skadeKontakter.add(s);
    	}
    	s.setNavn(forsikringsselskap);
    	getProps().firePropertyChange("forsikringsselskap", old, forsikringsselskap);
    }
    /**
     * Fritekst
     * 
     * @return
     */
    public String getFritekst() {
        return fritekst;
    }
    /**
     * Id
     * 
     * @return
     */
    public String getGuid() {
        return guid;
    }
    
    @Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

    /**
     * Hvem meldte skaden
     * 
     * @return
     */
    public String getMeldtAv() {
        return meldtAv;
    }

    /**
     * Når ble skaden meldt
     * 
     * @return
     */
    public Date getMeldtDato() {
        return meldtDato;
    }

    @Transient 
    public SkadeKontakt getPolitiLensmann(){
    	SkadeKontakt k = getSkadeKontaktString(SkadeKontakt.LENSMANN);
    	if (k==null)
    		k = getSkadeKontaktString(SkadeKontakt.POLITI);
    	return k;
    }
    @Transient
    public String getPolitiKontaktNavn(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	return s==null?null:s.getKontaktNavn();
    }
    public void setPolitiKontaktNavn(String politiKontaktNavn){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	String old = s==null?null:s.getKontaktNavn();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.POLITI));
    		skadeKontakter.add(s);
    	}
    	s.setKontaktNavn(politiKontaktNavn);
    	getProps().firePropertyChange("politiKontaktNavn", old, politiKontaktNavn);
    }

    @Transient
    public Long getPolitiKontaktTlf(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	return s==null?null:s.getKontaktTlfnummer();
    }
    
    public void setPolitiKontaktTlf(Long politiKontaktTlf){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	Long old = s==null?null:s.getKontaktTlfnummer();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.POLITI));
    		skadeKontakter.add(s);
    	}
    	s.setKontaktTlfnummer(politiKontaktTlf);
    	getProps().firePropertyChange("politiKontaktTlf", old, politiKontaktTlf);
    }
    @Transient
    public String getPolitiKontaktResultat(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	return s==null?null:s.getResultat();
    }
    public void setPolitiKontaktResultat(String politiKontaktResultat){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	String old = s==null?null:s.getResultat();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.POLITI));
    		skadeKontakter.add(s);
    	}
    	s.setResultat(politiKontaktResultat);
    	getProps().firePropertyChange("politiKontaktResultat", old, politiKontaktResultat);
    }
    @Transient
    public Date getPolitiKontaktDato(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	return s==null?null:s.getDato();
    }
    public void setPolitiKontaktDato(Date politiKontaktDato){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.POLITI);
    	Date old = s==null?null:s.getDato();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.POLITI));
    		skadeKontakter.add(s);
    	}
    	s.setDato(politiKontaktDato);
    	getProps().firePropertyChange("politiKontaktDato", old, politiKontaktDato);
    }
    /**
     * Når ble skaden reparert
     * 
     * @return
     */
    public Date getReparertDato() {
        return reparertDato;
    }

    /**
     * Skade dato
     * 
     * @return
     */
    public Date getSkadeDato() {
        return skadeDato;
    }

    /**
     * Skade kontakter
     * 
     * @return
     */
    public List<SkadeKontakt> getSkadeKontakter() {
        return skadeKontakter;
    }

    private SkadeKontakt getSkadeKontaktString(String navn){
    	for (SkadeKontakt sk: skadeKontakter){
    		if (sk.getType()!=null)
	    		if (sk.getType().getNavn().equals(navn)){
	    			return sk;
	    		}
    	}
    	return null;
    }
    
    /**
     * Setter alle tilgjengelige typer for kontakter
     * @param kontaktList
     */
    public void setSkadekontakttyper(List<Skadekontakttype> kontaktList){
		this.kontaktList = kontaktList;
    }
	private Skadekontakttype getType(String typeNavn){
		for (Skadekontakttype sk:kontaktList){
			if (sk.getNavn().equals(typeNavn)){
				return sk;
			}
		}
		return null;
	}
    private List<SkadeKontakt> getSkadeKontaktStringList(String navn){
    	List<SkadeKontakt> list = new ArrayList<SkadeKontakt>();
    	for (SkadeKontakt sk:skadeKontakter){
    		if (sk.getType()!=null)
	    		if (sk.getType().getNavn().equals(navn)){
	    			list.add(sk);
	    		}
    	}
    	return list;
    }

    /**
     * Skadelvolder informasjon
     * 
     * @return
     */
    public String getSkadevolderBilmerke() {
        return skadevolderBilmerke;
    }

    /**
     * Skadelvolder informasjon
     * 
     * @return
     */
    public String getSkadevolderHenger() {
        return skadevolderHenger;
    }

    /**
     * Skadelvolder informasjon
     * 
     * @return
     */
    public String getSkadevolderRegnr() {
        return skadevolderRegnr;
    }

    /**
	 * Hack for å "jukse" til supbreports for Jasper
	 * 
	 * @return
	 */
	@Transient
	public Collection<Forsikringsskade> getThisForReport() {
		return Collections.singletonList(this);
	}
	@Transient
	public Forsikringsskade getForsikringsskadeForReport(){
		return this;
	}
    /**
     * Kommentar
     * 
     * @return
     */
    public String getUtsattRepKommentar() {
        return utsattRepKommentar;
    }

    @Transient
    public List<SkadeKontakt> getVitner(){
    	return getSkadeKontaktStringList(SkadeKontakt.VITNE);
    }

    @Transient
    public String getVitneNavn(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.VITNE);
    	return s==null?null:s.getNavn();
    }
    public void setVitneNavn(String vitneNavn){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.VITNE);
    	String old = s==null?null:s.getNavn();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.VITNE));
    		skadeKontakter.add(s);
    	}
    	s.setNavn(vitneNavn);
    	getProps().firePropertyChange("vitneNavn", old, vitneNavn);
    }
    public Date getVitneDato(){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.VITNE);
    	return s==null?null:s.getDato();
    }
    public void setVitneDato(Date vitneDato){
    	SkadeKontakt s = getSkadeKontaktString(SkadeKontakt.VITNE);
    	Date old = s==null?null:s.getDato();
    	if (s==null){
    		s = new SkadeKontakt();
    		s.setType(getType(SkadeKontakt.VITNE));
    		skadeKontakter.add(s);
    	}
    	s.setDato(vitneDato);
    	getProps().firePropertyChange("vitneDato", old, vitneDato);
    }
    /**
     * Anmeldt
     * 
     * @param anmeldtFlagg
     */
    public void setAnmeldt(Boolean anmeldt) {
    	Boolean old = this.anmeldt;
        this.anmeldt = anmeldt;
        firePropertyChange("anmeldt", old, anmeldt);
    }

    /**
     * Hvem endret registringen
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    public void setTilleggsopplysninger(String tilleggsopplysninger){
    	String old = this.tilleggsopplysninger;
    	this.tilleggsopplysninger = tilleggsopplysninger;
    	firePropertyChange("tilleggsopplysninger", old, tilleggsopplysninger);
    }
    /**
     * Når ble registreingen endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
    	Date old = this.endretDato;
        this.endretDato = endretDato;
        firePropertyChange("endretDato", old, endretDato);
    }

    /**
     * Entrepenør dato
     * 
     * @param entrepenorDato
     */
    public void setEntrepenorDato(Date entrepenorDato) {
    	Date old = this.entrepenorDato;
        this.entrepenorDato = entrepenorDato;
        firePropertyChange("entrepenorDato", old, entrepenorDato);
    }

    /**
     * Navn til entrepenør
     * 
     * @param entrepenorNavn
     */
    public void setEntrepenorNavn(String entrepenorNavn) {
    	String old = this.entrepenorNavn;
        this.entrepenorNavn = entrepenorNavn;
        firePropertyChange("entrepenorNavn", old, entrepenorNavn);
    }

    /**
     * Estimatkostnad
     * 
     * @param estimatKostnad
     */
    public void setEstimatKostnad(Double estimatKostnad) {
    	Double old = this.estimatKostnad;
        this.estimatKostnad = estimatKostnad;
        firePropertyChange("estimatKostnad", old, estimatKostnad);
    }

    /**
     * Fritekst
     * 
     * @param fritekst
     */
    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    /**
     * Id
     * 
     * @param guid
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * Hvem meldte skaden
     * 
     * @param meldtAv
     */
    public void setMeldtAv(String meldtAv) {
    	String old = this.meldtAv;
        this.meldtAv = meldtAv;
        firePropertyChange("meldtAv", old, meldtAv);
    }

    /**
     * Når ble skaden meldt
     * 
     * @param meldtDato
     */
    public void setMeldtDato(Date meldtDato) {
    	Date old = this.meldtDato;
        this.meldtDato = meldtDato;
        firePropertyChange("meldtDato", old, meldtDato);
    }
    /**
     * Når ble skaden reparert
     * 
     * @param reparertDato
     */
    public void setReparertDato(Date reparertDato) {
    	Date old = this.reparertDato;
        this.reparertDato = reparertDato;
        firePropertyChange("reparertDato", old, reparertDato);
    }

    /**
     * Skade dato
     * 
     * @param skadeDato
     */
    public void setSkadeDato(Date skadeDato) {
    	Date old = this.skadeDato;
        this.skadeDato = skadeDato;
        firePropertyChange("skadeDato", old, skadeDato);
    }

    /**
     * Skade kontakter
     * 
     * @param skadeKontakter
     */
    public void setSkadeKontakter(List<SkadeKontakt> skadeKontakter) {
        this.skadeKontakter = skadeKontakter;
    }

    /**
     * Skadelvolder informasjon
     * 
     * @param skadevolderBilmerke
     */
    public void setSkadevolderBilmerke(String skadevolderBilmerke) {
    	String old = this.skadevolderBilmerke;
        this.skadevolderBilmerke = skadevolderBilmerke;
        firePropertyChange("skadevolderBilmerke", old, skadevolderBilmerke);
    }

    /**
     * Skadelvolder informasjon
     * 
     * @param skadevolderHenger
     */
    public void setSkadevolderHenger(String skadevolderHenger) {
    	String old = this.skadevolderHenger;
        this.skadevolderHenger = skadevolderHenger;
        firePropertyChange("skadevolderHenger", old, skadevolderHenger);
    }

    /**
     * Skadelvolder informasjon
     * 
     * @param skadevolderRegnr
     */
    public void setSkadevolderRegnr(String skadevolderRegnr) {
    	String old = this.skadevolderRegnr;
        this.skadevolderRegnr = skadevolderRegnr;
        firePropertyChange("skadevolderRegnr", old, skadevolderRegnr);
    }
    
    /**
     * Kommentar
     * 
     * @param utsattRepKommentar
     */
    public void setUtsattRepKommentar(String utsattRepKommentar) {
    	String old = this.utsattRepKommentar;
        this.utsattRepKommentar = utsattRepKommentar;
        firePropertyChange("utsattRepKommentar", old, utsattRepKommentar);
    }

//    /** {@inheritDoc} */
//    @Override
//    public int hashCode() {
//        return new HashCodeBuilder(2033, 2067).append(guid).toHashCode();
//    }
//    
//    /** {@inheritDoc} */
//    @Override
//    public boolean equals(Object o) {
//        if(o == null) {return false;}
//        if(o == this) {return true;}
//        if(!(o instanceof Forsikringsskade)) {return false;}
//        
//        Forsikringsskade other = (Forsikringsskade) o;
//        
//        return new EqualsBuilder().append(guid, other.getGuid()).isEquals();
//    }
    
	public Long getRefnummer() {
		return refnummer;
	}

	public void setRefnummer(Long refnummer) {
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}

	public Date getRapportertDato() {
		return rapportertDato;
	}

	public void setRapportertDato(Date rapportertDato) {
		Date old = this.rapportertDato;
		this.rapportertDato = rapportertDato;
		firePropertyChange("rapportertDato", old, rapportertDato);
	}
	public Long getElrappDokumentIdent() {
		return elrappDokumentIdent;
	}
	
	public void setElrappDokumentIdent(Long elrappDokumentIdent) {
		Long old = elrappDokumentIdent;
		this.elrappDokumentIdent = elrappDokumentIdent;
		firePropertyChange("elrappDokumentIdent", old, elrappDokumentIdent);
	}
	public Long getElrappVersjon() {
		return elrappVersjon;
	}

	public void setElrappVersjon(Long elrappVersjon) {
		Long old = this.elrappVersjon;
		this.elrappVersjon = elrappVersjon;
		firePropertyChange("elrappVersjon", old, elrappVersjon);
	}
	public String getSkjemaEmne() {
		return skjemaEmne;
	}

	public void setSkjemaEmne(String skjemaEmne) {
		String old = this.skjemaEmne;
		this.skjemaEmne = skjemaEmne;
		firePropertyChange("skjemaEmne", old, skjemaEmne);
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}
	public String getSlettetAv() {
		return slettetAv;
	}

	public void setSlettetAv(String slettetAv) {
		this.slettetAv = slettetAv;
	}

	public Date getSlettetDato() {
		return slettetDato;
	}

	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}

	/** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("guid", guid).
            append("skadevolderRegnr", skadevolderRegnr).
            append("skadevolderHenger", skadevolderHenger).
            append("skadevolderBilmerke", skadevolderBilmerke).
            append("fritekst", fritekst).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            append("anmeldt", anmeldt).
            append("meldtAv", meldtAv).
            append("meldtDato", meldtDato).
            append("entrepenorDato", entrepenorDato).
            append("skadeDato", skadeDato).
            append("antattTidsromForSkaden", antattTidsromForSkaden).
            append("estimatKostnad", estimatKostnad).
            append("reparertDato", reparertDato).
            append("utsattRepKommentar", utsattRepKommentar).
            append("entrepenorNavn", entrepenorNavn).
            toString();
    }
}
