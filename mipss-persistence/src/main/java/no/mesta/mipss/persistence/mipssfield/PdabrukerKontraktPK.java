package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import no.mesta.mipss.persistence.kontrakt.KontraktStabPK;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class PdabrukerKontraktPK implements Serializable {
	private String signatur;
	private Long kontraktId;
	
	public PdabrukerKontraktPK() {
	}
	
	public PdabrukerKontraktPK(String signatur, Long kontraktId) {
		this.signatur = signatur;
		this.kontraktId = kontraktId;
	}
	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kontraktId).append(signatur).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktStabPK)) {return false;}
        
        PdabrukerKontraktPK other = (PdabrukerKontraktPK) o;
        
        return new EqualsBuilder().append(kontraktId, other.kontraktId).append(signatur, other.signatur).isEquals();
    }
}
