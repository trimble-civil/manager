package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * N�kkelklasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class KjoretoyordreloggPK implements Serializable {
    private Date opprettetDato;
    private Long ordreId;

    public KjoretoyordreloggPK() {
    }

    public KjoretoyordreloggPK(Date opprettetDato, Long ordreId) {
        this.opprettetDato = opprettetDato;
        this.ordreId = ordreId;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOrdreId(Long ordreId) {
        this.ordreId = ordreId;
    }

    public Long getOrdreId() {
        return ordreId;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(opprettetDato).append(ordreId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KjoretoyordreloggPK)) {return false;}
        
        KjoretoyordreloggPK other = (KjoretoyordreloggPK) o;
        
        return new EqualsBuilder().append(opprettetDato, other.opprettetDato).append(ordreId, other.ordreId).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("opprettetDato", opprettetDato).
            append("ordreId", ordreId).
            toString();
    }
}
