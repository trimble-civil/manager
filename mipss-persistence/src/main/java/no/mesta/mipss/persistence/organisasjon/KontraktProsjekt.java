package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.ICreatableMippsEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssEntityCreator;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA entitet for et mange-til-mange forhold. Generert i JDeveloper.
 * Både Prosjekt og Driftkontrakt har sine 
 * respektive ManyToMany forhold direkte til hverandre.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity

@NamedQueries ({
	@NamedQuery(name = KontraktProsjekt.FIND_ALL, query = "select o from KontraktProsjekt o"),
	@NamedQuery(name = KontraktProsjekt.QUERY_FIND_FOR_KONTRAKT, query = "SELECT o FROM KontraktProsjekt o where o.kontraktId = :id")
})

@EntityListeners({no.mesta.mipss.persistence.OpprettingMonitor.class})

@Table(name = "KONTRAKT_ARBEIDSORDRE")

@IdClass(KontraktProsjektPK.class)
public class KontraktProsjekt extends MipssEntityBean<KontraktProsjekt> implements Serializable, ICreatableMippsEntity, IRenderableMipssEntity {
	public static final String FIND_ALL = "ProsjektKontrakt.findAll";
	public static final String QUERY_FIND_FOR_KONTRAKT = "ProsjektKontrakt.findAllForKontrakt";
	private static final String[] ID_NAMES = {"kontraktId", "prosjektNr", "selskapskode"};
	
    private Long kontraktId;
    private String prosjektNr;
    private String selskapskode;
    private Long standardFlagg;
    private Driftkontrakt driftkontrakt;
    private MipssEntityCreator mipssEntityCreator;
    
    public KontraktProsjekt() {
    }

    @Id
    @Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
    public Long getKontraktId() {
        return kontraktId;
    }

    public void setKontraktId(Long kontraktId) {
        this.kontraktId = kontraktId;
    }

    @Id
    @Column(name="ARBEIDSORDRE_NR", nullable = false)
    public String getProsjektNr() {
        return prosjektNr;
    }

    public void setProsjektNr(String prosjektNr) {
        this.prosjektNr = prosjektNr;
    }
    
    @Id
    @Column(nullable = false)
    public String getSelskapskode() {
        return selskapskode;
    }

    public void setSelskapskode(String selskapskode) {
        this.selskapskode = selskapskode;
    }

    @Column(name="STANDARD_FLAGG")
    public Long getStandardFlagg() {
        return standardFlagg;
    }

    public void setStandardFlagg(Long standardFlagg) {
        Object oldValue = this.standardFlagg;
    	this.standardFlagg = standardFlagg;
    	getProps().firePropertyChange("standardFlagg", oldValue, standardFlagg);
    }

    public Boolean getStandardFlaggBoolean() {
    	return standardFlagg != null && standardFlagg.longValue() == 1 ? Boolean.TRUE : Boolean.FALSE;
    }
    
	@ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
    public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
		
		if(driftkontrakt != null) {
            setKontraktId(driftkontrakt.getId());
        } else {
        	setKontraktId(null);
        }
	}

	/**
     * Genererer en lesbar tekst med hovedlementene i entitybønnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("kontrakt id", getKontraktId()).
            append("arbeidsordre id", getProsjektNr()).
            append("selskapskode", getSelskapskode()).
            append("standard", getStandardFlagg()).
            toString();
    }

    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof KontraktProsjekt)) {
            return false;
        }
        KontraktProsjekt other = (KontraktProsjekt) o;
        return new EqualsBuilder().
            append(getKontraktId(), other.getKontraktId()).
            append(getProsjektNr(), other.getProsjektNr()).
            append(getSelskapskode(), other.getSelskapskode()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun på Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getKontraktId()).
            append(getProsjektNr()).
            append(getSelskapskode()).
            append(getStandardFlagg()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	KontraktProsjekt other = (KontraktProsjekt) o;
		return new CompareToBuilder().
			append(getProsjektNr(), other.getProsjektNr()).
			append(getStandardFlagg(), other.getStandardFlagg()).
			append(getKontraktId(), other.getKontraktId()).
			append(getSelskapskode(), other.getSelskapskode()).
			toComparison();
    }

	public MipssEntityCreator getCreator() {
    	if(mipssEntityCreator == null) {
    		mipssEntityCreator = new MipssEntityCreator();
    	}
		return mipssEntityCreator;
	}

	/**
	 * Henter en delklasse for opprettelsesinfo.
	 * {@inheritDoc}
	 * @see no.mesta.mipss.persistence.ICreatableMippsEntity#getMipssEntityCreator()
	 */
    @Embedded
	public MipssEntityCreator getMipssEntityCreator() {
    	if(mipssEntityCreator == null) {
    		mipssEntityCreator = new MipssEntityCreator();
    	}
		return mipssEntityCreator;
	}
    
	/**
	 * Setter en delklasse for opprettelsesinfo.
	 * @param creator setter en delklasse for opprettelsesinfo.
	 */
	public void setMipssEntityCreator(MipssEntityCreator mipssEntityCreator) {
		this.mipssEntityCreator = mipssEntityCreator;
	}
	
	public String getTextForGUI() {
		return getProsjektNr();
	}

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}
}
