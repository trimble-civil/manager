package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Entitetsbønne for brukerpreferanser, utvides etterhvert som ønskelige preferanser ønskes lagret for brukerne av systemet.
 * @author OLEHEL
 */
@SuppressWarnings("serial")
@Entity(name="Brukerpreferanse")
@Table(name="BRUKERPREFERANSER")
@NamedQueries({
    @NamedQuery(name = Brukerpreferanser.QUERY_GET_FOR_BRUKER, query = "SELECT p FROM Brukerpreferanse p WHERE p.signatur = :sig"),
})

public class Brukerpreferanser implements Serializable {
	private String signatur;
	private Long valgtKontraktId;
	
    public static final String QUERY_GET_FOR_BRUKER = "Brukerpreferanse.getForBruker";
	
   
	@Id
	@Column(nullable = false, updatable = false, insertable = true)
	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}

	@Column(name="VALGT_KONTRAKT_ID", updatable = true, insertable = true)
	public Long getValgtKontraktId() {
		return valgtKontraktId;
	}

	public void setValgtKontraktId(Long valgtKontraktId) {
		this.valgtKontraktId = valgtKontraktId;
	}

    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Brukerpreferanser)) {return false;}
        if(o == this) {return true;}
        
        Brukerpreferanser other = (Brukerpreferanser) o;
        
        return new EqualsBuilder().
            append(signatur, other.getSignatur()).
            append(valgtKontraktId, other.getValgtKontraktId()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(201,301).
            append(signatur).
            append(valgtKontraktId).
            toHashCode();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("signatur", signatur).
            append("valgtKontrakt", valgtKontraktId).
            toString();
    }	
}