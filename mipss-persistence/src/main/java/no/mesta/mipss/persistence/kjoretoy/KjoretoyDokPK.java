package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * N�kkel klasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class KjoretoyDokPK implements Serializable {
    private Long dokumentId;
    private Long kjoretoyId;

    public KjoretoyDokPK() {
    }

    public KjoretoyDokPK(Long dokumentId, Long kjoretoyId) {
        this.dokumentId = dokumentId;
        this.kjoretoyId = kjoretoyId;
    }

    public void setDokumentId(Long dokumentId) {
        this.dokumentId = dokumentId;
    }

    public Long getDokumentId() {
        return dokumentId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    public Long getKjoretoyId() {
        return kjoretoyId;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dokumentId).append(kjoretoyId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KjoretoyDokPK)) {return false;}
        
        KjoretoyDokPK other = (KjoretoyDokPK) o;
        
        return new EqualsBuilder().append(dokumentId, other.dokumentId).append(kjoretoyId, other.kjoretoyId).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("dokumentId", dokumentId).
            append("kjoretoyId", kjoretoyId).
            toString();
    }
}
