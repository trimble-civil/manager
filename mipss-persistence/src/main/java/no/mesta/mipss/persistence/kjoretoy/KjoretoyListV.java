package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * Denne entiteten henter st innhold fra et databaseview ved samme navn. 
 * Den er read-only. Den trenger derfor ingen PropertyChangeSupport.
 * 
 * Brukes for optimalisert søk av kjøretøy 
 * @author jorge
 */
@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "KjoretoyListV.findAll", query = "select o from KjoretoyListV o")
@Table(name = "KJORETOY_LIST_V")
public class KjoretoyListV implements Serializable, IRenderableMipssEntity {
    private Long aar;
    private String dfuStatus;
    private String eierNavn;
    private String eksterntNavn;
    private Date endretDato;
    private String endretAv;
    private Long id;
    private Long maskinnummer;
    private String merke;
    private String modell;
    private String opprettetAv;
    private String regnr;
    private Date sisteLivstegnDato;
    private String typeNavn;
    private String dfuSerienummer;
    private String dfuTlfnummer;
    private String ansKontraktnummer;
    private String ansKontraktnavn;
    private Long dfuId;
    private Long slettet;

    public KjoretoyListV() {
    }

    public Long getAar() {
        return aar;
    }

    public void setAar(Long aar) {
        this.aar = aar;
    }

    @Column(name="DFU_STATUS")
    public String getDfuStatus() {
        return dfuStatus;
    }

    public void setDfuStatus(String dfuStatus) {
        this.dfuStatus = dfuStatus;
    }

    @Column(name="EIER_NAVN")
    public String getEierNavn() {
        return eierNavn;
    }

    public void setEierNavn(String eierNavn) {
        this.eierNavn = eierNavn;
    }

    @Column(name="EKSTERNT_NAVN", nullable = false)
    public String getEksterntNavn() {
        return eksterntNavn;
    }

    public void setEksterntNavn(String eksterntNavn) {
        this.eksterntNavn = eksterntNavn;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMaskinnummer() {
        return maskinnummer;
    }

    public void setMaskinnummer(Long maskinnummer) {
        this.maskinnummer = maskinnummer;
    }

    public String getMaskinnummerString() {
        return maskinnummer != null ? maskinnummer.toString() : "";
    }

    public String getMerke() {
        return merke;
    }

    public void setMerke(String merke) {
        this.merke = merke;
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    public String getRegnr() {
        return regnr;
    }

    public void setRegnr(String regnr) {
        this.regnr = regnr;
    }

    @Column(name="SISTE_LIVSTEGN_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSisteLivstegnDato() {
        return sisteLivstegnDato;
    }

    public void setSisteLivstegnDato(Date sisteLivstegnDato) {
        this.sisteLivstegnDato = sisteLivstegnDato;
    }

    @Column(name="TYPE_NAVN")
    public String getTypeNavn() {
        return typeNavn;
    }

    public void setTypeNavn(String typeNavn) {
        this.typeNavn = typeNavn;
    }
    
    @Column(name="DFU_SERIENUMMER")
    public String getDfuSerienummer() {
		return dfuSerienummer;
	}

	public void setDfuSerienummer(String dfuSerienummer) {
		this.dfuSerienummer = dfuSerienummer;
	}
	
	@Column(name="DFU_TLFNUMMER")
	public String getDfuTlfnummer(){
		return dfuTlfnummer;
	}
	
	public void setDfuTlfnummer(String dfuTlfnummer){
		this.dfuTlfnummer = dfuTlfnummer;
	}

	@Column(name="ANS_KONTRAKTNUMMER")
	public String getAnsKontraktnummer() {
		return ansKontraktnummer;
	}

	public void setAnsKontraktnummer(String ansKontraktnummer) {
		this.ansKontraktnummer = ansKontraktnummer;
	}
	
	@Column(name="ANS_KONTRAKTNAVN")
	public String getAnsKontraktnavn() {
		return ansKontraktnavn;
	}

	public void setAnsKontraktnavn(String ansKontraktnavn) {
		this.ansKontraktnavn = ansKontraktnavn;
	}

	@Column(name="DFU_ID")
	public Long getDfuId() {
		return dfuId;
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}
	
	@Transient
	public String getAnsKontrakt() {
		String ret = "";
		
		if(ansKontraktnummer != null) {
			ret += ansKontraktnummer + " - ";
		}
		
		if(ansKontraktnavn != null) {
			ret += ansKontraktnavn;
		}
		
		return ret;
	}

	/**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof KjoretoyListV)) {
            return false;
        }
        KjoretoyListV other = (KjoretoyListV) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun på Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,23).
            append(getId()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getEksterntNavn();
    }

    public int compareTo(Object o) {
        KjoretoyListV other = (KjoretoyListV) o;
        return new CompareToBuilder().
            append(getId(), other.getId()).
            toComparison();
    }
    
    public String toString() {
        StringBuffer b = new StringBuffer("KjotretoyListV:");
        b.append(id)
            .append(" navn:")
            .append(getEksterntNavn())
            .append(" dfuStatus:")
            .append(dfuStatus)
            .append(" sisteLivstegnDato:")
            .append(sisteLivstegnDato);
        return b.toString();
    }

	public void setSlettet(Long slettet) {
		this.slettet = slettet;
	}
	@Column(name="SLETTET_FLAGG")
	public Long getSlettet() {
		return slettet;
	}
	
	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}
	@Column(name="ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}
}
