package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.persistence.indirection.IndirectList;


/**
 * Holder en instans fra KJORETOY_UTSTYR tabellen.
 * Implementeterer hierarkiet i forholdet Utstyrindivid -> Utstyrmodell -> Utstyrsubgruppe -> Utstyrgruppe.
 * <p>
 * Alle setter metodene fyrer av en hendelse som støtter BeansBinding. Dette skjer også for de 
 * relasjonssettere som brukes til å koble til andre objekter og objektlister. Det er kun nøkkelsetteren 
 * som ikke sender en hendelse.
 * </p>
 * Man sender en hendelse knyttet til feltet "kjoretoyUtstyrIoList" både når man erstatter hele listen og
 * når man legge til / fjerner en instans fra den.
 * 
 * @author jorge
 */
@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "kjoretoyUtstyrIdSeq", sequenceName = "KJORETOY_UTSTYR_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = KjoretoyUtstyr.FIND_ALL, query = "select o from KjoretoyUtstyr o"),
    @NamedQuery(name = KjoretoyUtstyr.FIND_ONE, query = "select u from KjoretoyUtstyr u where u.id = :id",
    		hints={@QueryHint(name=QueryHints.REFRESH, value=HintValues.TRUE)}),
    @NamedQuery(name = KjoretoyUtstyr.LIST_FOR_KJORETOY, query = "select o from KjoretoyUtstyr o where o.kjoretoyId = :id")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

@Table(name = "KJORETOY_UTSTYR")

public class KjoretoyUtstyr implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "KjoretoyUtstyr.findAll";
    public static final String FIND_ONE = "KjoretoyUtstyr.findOne";
    public static final String LIST_FOR_KJORETOY = "KjoretoyUtstyr.listForKjoretoy";
    
    private String fritekst;
    private Long id;
    private Long kjoretoyId;
    private Utstyrmodell utstyrmodell;
    private Utstyrsubgruppe utstyrsubgruppe;
    private Utstyrgruppe utstyrgruppe;
    private List<KjoretoyUtstyrIo> kjoretoyUtstyrIoList = new ArrayList<KjoretoyUtstyrIo>();

    // felt lagt til for hånd
    private OwnedMipssEntity ownedMipssEntity;
    private Kjoretoy kjoretoy;
    private PropertyChangeSupport props;
    private boolean completelyDefined = false;
    private transient Logger logger;
    
    private final UUID tmpUUID;
    
    public KjoretoyUtstyr() {
        props = new PropertyChangeSupport(this);
        tmpUUID = UUID.randomUUID();
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        String old = this.fritekst;
        this.fritekst = fritekst;
        
        props.firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoyUtstyrIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Setter entitetsnøkkelen. Trenger ikke å sende an endringshendelse.
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="KJORETOY_ID", nullable = false, insertable = false, updatable = false)
    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        Long old = this.kjoretoyId;
        this.kjoretoyId = kjoretoyId;
        
        props.firePropertyChange("kjoretoyId", old, kjoretoyId);
    }

    /**
     * Henter utstyyrsmodellen fra individet som første prioritet.
     * Fooretningsregel om dette bestememr at modellen skal lagre i tabellen når individet er satt.
     * @return utstyyrsmodellen eller null.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UTSTYRMODELL_ID", referencedColumnName = "ID")
    public Utstyrmodell getUtstyrmodell() {
    	return utstyrmodell;
    }
    
    /**
     * Henter utstyyrsmodellen fra individet, hvis det eksisterer.
     * Implementerer hierarkiet i informasjonen om et KJORETOY_UTSTYR:
     * Modellen hentes fra individet hvis det eksisterer. I så tilfelle 
     * ignorerer verdien i dette feltet, hvis noen ble hentet fra databasen.
     * Brukes for å komplett informasjon på brukergrensesnittet.
     * @return utstyyrsmodellen eller null.
     */
    @Transient
    public Utstyrmodell getUtstyrmodellForGui() {
        return utstyrmodell;
    }

    /**
     * Setter modellen i individet, hvis det eksisterer, ellers settes
     * den direkte i entiteten.
     * @param utstyrmodell
     */
    public void setUtstyrmodell(Utstyrmodell utstyrmodell) {
        Utstyrmodell old = this.utstyrmodell;
        this.utstyrmodell = utstyrmodell;
        
        props.firePropertyChange("utstyrmodell", old, utstyrmodell);
        setCompletelyDefined(false);
    }

    /**
     * Henter subgruppen direkte.
     * @return
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UTSTYRSUBGRUPPE_ID", referencedColumnName = "ID")
    public Utstyrsubgruppe getUtstyrsubgruppe() {
        return utstyrsubgruppe;
    }
    
    /**
     * Henter subgruppen fra utstyrsmodellen, hvis den eksisterer.
     * Implementerer hierarkiet i informasjonen om et KJORETOY_UTSTYR:
     * Subgruppen hentes fra modellen hvis den eksisterer. I så tilfelle 
     * ignorerer verdien i dette feltet, hvis noen ble hentet fra databasen.
     * Brukes for å komplett informasjon på brukergrensesnittet.
     * @return
     */
    @Transient
    public Utstyrsubgruppe getUtstyrsubgruppeForGui() {
        Utstyrmodell modell = getUtstyrmodellForGui();
        if (modell == null) {
        	log("modell ikke funnet: returnerer intern subgruppe: " + utstyrsubgruppe);
            return utstyrsubgruppe;
        }
        log("returnerer modellens subgruppe: " + modell.getUtstyrsubgruppe());
        return modell.getUtstyrsubgruppe();
    }

    /**
     * Setter {@code Utstyrsubgruppe} i entitetens {@code Utstyrsubgruppe} hvis
     * den eksisterer, ellers settes den direkte på entiteten.
     * @param utstyrsubgruppe
     */
    public void setUtstyrsubgruppe(Utstyrsubgruppe utstyrsubgruppe) {
        Utstyrsubgruppe old = getUtstyrsubgruppe();
        this.utstyrsubgruppe = utstyrsubgruppe;
        
        props.firePropertyChange("utstyrsubgruppe", old, utstyrsubgruppe);
        setCompletelyDefined(false);
    }

    /**
     * Henter gruppen direkte. 
     * @return
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UTSTYRGRUPPE_ID", referencedColumnName = "ID")
    public Utstyrgruppe getUtstyrgruppe() {
        return utstyrgruppe;
    }
    
    /**
     * Henter gruppen fra subgruppen, hvis den eksisterer.
     * Implementerer hierarkiet i informasjonen om et KJORETOY_UTSTYR:
     * Gruppen hentes fra subgruppen hvis den eksisterer. I så tilfelle 
     * ignoreres verdien i dette feltet, hvis noen ble hentet fra databasen.
     * Brukes for å komplett informasjon på brukergrensesnittet.
     * @return
     */
    @Transient
    public Utstyrgruppe getUtstyrgruppeForGui() {
        Utstyrsubgruppe subgruppe = getUtstyrsubgruppeForGui();
        if (subgruppe == null) {
        	log("subgruppe ikke funnet. Returnerer den inetrne gruppen: " + subgruppe);
            return utstyrgruppe;
        }
        log("returnerer subgruppens gruppe: " + subgruppe.getUtstyrgruppe());
        return subgruppe.getUtstyrgruppe();
    }

    /**
     * Setter [@code Utstyrgruppe} i entitetens {@code Utstyrsubgruppe}
     * hvis den eksisterer, ellers settes den direkte i entiteten.
     * @param utstyrgruppe
     */
    public void setUtstyrgruppe(Utstyrgruppe utstyrgruppe) {
        Utstyrgruppe old = getUtstyrgruppe();
        this.utstyrgruppe = utstyrgruppe;
        
        props.firePropertyChange("utstyrgruppe", old, utstyrgruppe);
        setCompletelyDefined(false);
    }

    /**
     * Henter listen over KjoretoyUtstyrIo for utstyret på kjøretøyet.
     * Listen blir oppdatert/sletett sammen med {@code KjoretoyUtstyr}
     * @return
     */
    @OneToMany(mappedBy = "kjoretoyUtstyr", fetch = FetchType.EAGER, cascade={CascadeType.ALL})
    @OrderBy("fraDato DESC")
    public List<KjoretoyUtstyrIo> getKjoretoyUtstyrIoList() {
        return kjoretoyUtstyrIoList;
    }

    /**
     * Erstatter alle KjoretoyUtstyrIo koblet til et {@KjoretoyUtstyr}
     * @param kjoretoyUtstyrIoList
     */
    public void setKjoretoyUtstyrIoList(List<KjoretoyUtstyrIo> kjoretoyUtstyrIoList) {
        List<KjoretoyUtstyrIo> old = this.kjoretoyUtstyrIoList;
        this.kjoretoyUtstyrIoList = kjoretoyUtstyrIoList;
        
        if(! (kjoretoyUtstyrIoList instanceof IndirectList)) {
        	props.firePropertyChange("kjoretoyUtstyrIoList", old, kjoretoyUtstyrIoList);
        }
    }

    /**
     * Legger til det leverte {@code KjoretoyUtstyrIo} entitet til den eksisterende listen.
     * Sender ut en hendelse kun hvis listen er clonable. Hendelsen kobles til "kjoretoyUtstyrIoList"
     * feltet.
     * @param kjoretoyUtstyrIo
     * @return
     */
    public KjoretoyUtstyrIo addKjoretoyUtstyrIo(KjoretoyUtstyrIo kjoretoyUtstyrIo) {
        List<KjoretoyUtstyrIo> old = getOldKjoretoyUtstyrIoList();
        
        getKjoretoyUtstyrIoList().add(kjoretoyUtstyrIo);
        kjoretoyUtstyrIo.setKjoretoyUtstyr(this);
        
        props.firePropertyChange("kjoretoyUtstyrIoList", old, kjoretoyUtstyrIoList);
        
        return kjoretoyUtstyrIo;
    }

    /**
     * Fjerner den leverte {@code KjoretoyUtstyrIo} fra KjoretoyUtstyrIo listen, hvis listen
     * inneholdt den.
     * Sender ut en hendelse kun hvis listen er clonable. Hendelsen kobles til "kjoretoyUtstyrIoList"
     * feltet.
     * @param kjoretoyUtstyrIo
     * @return
     */
    public KjoretoyUtstyrIo removeKjoretoyUtstyrIo(KjoretoyUtstyrIo kjoretoyUtstyrIo) {
        List<KjoretoyUtstyrIo> old = getOldKjoretoyUtstyrIoList();
        
        getKjoretoyUtstyrIoList().remove(kjoretoyUtstyrIo);
        kjoretoyUtstyrIo.setKjoretoyUtstyr(null);
        
        props.firePropertyChange("kjoretoyUtstyrIoList", old, kjoretoyUtstyrIoList);
        
        return kjoretoyUtstyrIo;
    }
    
    @Transient
    private List<KjoretoyUtstyrIo> getOldKjoretoyUtstyrIoList() {
        List<KjoretoyUtstyrIo> old;
        old = new ArrayList<KjoretoyUtstyrIo>(this.kjoretoyUtstyrIoList);

        return old;
    }
    
    // de neste metodene er håndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("kjoretoy id", getKjoretoyId()).
            append("gruppe", getUtstyrgruppe()).
            append("subgruppe", getUtstyrsubgruppe()).
            append("modell", getUtstyrmodell()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof KjoretoyUtstyr)) {
            return false;
        }
        
        KjoretoyUtstyr other = (KjoretoyUtstyr) o;
        if(getId() == null && other.getId() == null) {
        	return new EqualsBuilder().
            append(tmpUUID, other.tmpUUID).
            isEquals();
        } else if(getId() != null && other.getId() != null) {
        	return new EqualsBuilder().
        	append(getId(), other.getId()).
            isEquals();
        } else {
        	return false;
        }
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            toHashCode();
    }
        
    @Transient
    public String getTextForGUI() {
    	if(utstyrmodell != null) {
    		return utstyrmodell.getNavn();
    	} else if(utstyrsubgruppe != null) {
    		return utstyrsubgruppe.getNavn();
    	} else if(utstyrgruppe != null) {
    		return utstyrgruppe.getNavn();
    	} else {
    		return String.valueOf(getId());
    	}
    }

    public int compareTo(Object o) {
        KjoretoyUtstyr other = (KjoretoyUtstyr) o;
            return new CompareToBuilder().
                append(getId(), other.getId()).
                append(getKjoretoyId(), other.getKjoretoyId()).
                toComparison();
    }

    /**
     * Henter den kjøretøyinstansen koblet til en KjoretoyUtstyr instans.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    /**
     * Setter en ny kjøretøyinstans og oppdaterer filtet kjoretoyId.
     * @param kjoretoy
     */
    public void setKjoretoy(Kjoretoy kjoretoy) {
        Kjoretoy old = this.kjoretoy;
        this.kjoretoy = kjoretoy;
        
        if(kjoretoy != null) {
            setKjoretoyId(kjoretoy.getId());
        } 
        else {
            setKjoretoyId(null);
        }
        
        props.firePropertyChange("kjoretoy", old, kjoretoy);
    }

    /**
     * Legger til den leverte {@code PropertyChangeListener} for alle felt.
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Legger til den leverte {@code PropertyChangeListener} for feltet nevnt i {@code propName}
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Fjerner den leverte {@code PropertyChangeListener}
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Fjerner den leverte {@code PropertyChangeListener} fra feltet i {@code propName}
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    @Transient
    public boolean isCompletelyDefined() {
        return completelyDefined;
    }
    
    /**
     * Setter flagget som viser at en instans av denne klassen har fått alle tilhørende definisjoner.
     * Dette gjelder utstyrgruppe, -subgruppe og -modell samt -individ.
     * Dette avhenger av at de tilhørende definisjoner krever subnivåer med sine interne flagg. 
     * @param ready er tilstedet kun for å tilfredstille Java bean standard: den overskrives internt.
     */
    public void setCompletelyDefined(boolean ready) {
        boolean old = this.completelyDefined;
        ready = false;
        
        if((utstyrgruppe != null && (utstyrgruppe.getKreverSubgrFlagg() == false)) || 
            (utstyrsubgruppe != null && (utstyrsubgruppe.getKreverModellFlagg() == false)) || 
            (utstyrmodell != null && (utstyrmodell.getKreverIndividFlagg() == false))) {
            ready = true;
        }

        this.completelyDefined = ready;

        props.firePropertyChange("completelyDefined", old, ready);
    }
    
    @Transient
    public Sensortype getCurrentSensortype(Date datoForPort){
    	KjoretoyUtstyrIo kjoretoyUtstyrIo = getKjoretoyUtstyrIoForDate(datoForPort);
    	System.out.println("currentSensortype:"+getUtstyrmodell()+" x "+kjoretoyUtstyrIo+" -"+datoForPort);
    	if(kjoretoyUtstyrIo != null) {
    		return kjoretoyUtstyrIo.getSensortype();
    	} else {
    		return null;
    	}
    }
    
    @Transient
    public String getCurrentPort(Date datoForPort){
    	KjoretoyUtstyrIo kjoretoyUtstyrIo = getKjoretoyUtstyrIoForDate(datoForPort);
    	System.out.println("currentPort:"+getUtstyrmodell()+"-"+kjoretoyUtstyrIo+" -"+datoForPort);
    	if(kjoretoyUtstyrIo != null) {
    		return kjoretoyUtstyrIo.getBitNr().toString();
    	} else {
    		return null;
    	}
    }
    
    @Transient
    public Utstyrgruppe getUtstyrgruppeForReport(){
    	if (utstyrmodell!=null){
    		if (utstyrmodell.getUtstyrsubgruppe()!=null){
    			return utstyrmodell.getUtstyrsubgruppe().getUtstyrgruppe();
    		}
    	}
    	if (utstyrsubgruppe!=null){
    		return utstyrsubgruppe.getUtstyrgruppe();
    	}
    	return utstyrgruppe;
    	
    }
    
    @Transient
    public Utstyrsubgruppe getUtstyrsubgruppeForReport(){
    	if (utstyrmodell!=null){
    		return utstyrmodell.getUtstyrsubgruppe();
    	}
    	return utstyrsubgruppe;
    }
    
    @Transient
    public Utstyrmodell getUtstyrmodellForReport(){
    	return utstyrmodell;
    }
    
    public KjoretoyUtstyr getThisForReport(){
    	return this;
    }
    
    
    @Transient
    public KjoretoyUtstyrIo getKjoretoyUtstyrIoForDate(Date datoForPort) {
    	if(kjoretoyUtstyrIoList != null) {
    		for(KjoretoyUtstyrIo kjoretoyUtstyrIo : kjoretoyUtstyrIoList) {
    			if(kjoretoyUtstyrIo.getFraDato().before(datoForPort)) {
    				if(kjoretoyUtstyrIo.getTilDato() == null || kjoretoyUtstyrIo.getTilDato().after(datoForPort)) {
    					return kjoretoyUtstyrIo;
    				}
    			}
    		}
    		return null;
    	} else {
    		return null;
    	}
    }
    
    /**
     * sentralisert logger.
     * @param setter
     * @param oldO
     * @param newO
     */
    private void log(String msg) {
    	if(logger == null) {
    		logger = LoggerFactory.getLogger(KjoretoyUtstyr.class);
    	}
    	logger.trace(msg);
    }
    
    /**
     * Sjekker for likt utstyr
     * 
     * @param kjoretoyUtstyr
     * @return
     */
    public boolean harSammeUtstyr(final KjoretoyUtstyr kjoretoyUtstyr) {
    	return new EqualsBuilder().
        append(getUtstyrgruppe(), kjoretoyUtstyr.getUtstyrgruppe()).
        append(getUtstyrsubgruppe(), kjoretoyUtstyr.getUtstyrsubgruppe()).
        append(getUtstyrmodell(), kjoretoyUtstyr.getUtstyrmodell()).
        isEquals();
    }
}
