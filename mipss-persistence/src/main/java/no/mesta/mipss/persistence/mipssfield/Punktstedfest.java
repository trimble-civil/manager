package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.Resources;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Stedfesting av punkt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name="PUNKTSTEDFEST_XV")
@NamedQueries( { 
	@NamedQuery(name = Punktstedfest.QUERY_FIND_ALL, query = "SELECT p FROM Punktstedfest p"),
	@NamedQuery(name= Punktstedfest.QUERY_FIND_BY_AVVIK, query = "SELECT p FROM Punktstedfest p where p.avvikGuid=:guid")})
public class Punktstedfest extends MipssEntityBean<Punktstedfest> implements Serializable, IRenderableMipssEntity,
		Comparable<Punktstedfest>, PropertyChangeSource {
	public static final String QUERY_FIND_ALL = "Punktstedfest.findAll";
	public static final String QUERY_FIND_BY_AVVIK= "Punktstedfest.findByAvvik";
	
	private static final String EAST = resolvePropertyText("east");
	private static final String FIELD_SAK = "sak";
	private static final String FIELD_SAKGUID = "sakGuid";
	private static final String FIELD_AVVIK = "avvik";
	private static final String FIELD_AVVIKGUID = "avvikGuid";
	private static final String FIELD_REFLINKIDENT = "reflinkIdent";
	private static final String FIELD_REFLINKPOSISJON = "reflinkPosisjon";
	private static final String FIELD_SNAPPETH = "snappetH";
	private static final String FIELD_SNAPPETX = "snappetX";
	private static final String FIELD_SNAPPETY = "snappetY";
	private static final String FIELD_TEXTFORGUI = "textForGUI";
	private static final String FIELD_VEIREFERANSER = "veireferanser";
	private static final String FIELD_VEIREF = "veiref";
	private static final String FIELD_X = "x";
	private static final String FIELD_Y = "y";
	private static final String HEIGHT = resolvePropertyText("height");
	private static final String ID_FIELD = "guid";
	private static final String[] ID_NAMES = new String[] { ID_FIELD };
	private static final String NORTH = resolvePropertyText("north");
	private static final String UNKNOWN = resolvePropertyText("unknown");
	private static final String EQUAL = "=";
	private static final String COMMA = ", ";
	private String guid;
	private Avvik avvik;
	private String avvikGuid;
	private Long reflinkIdent;
	private Double reflinkPosisjon;
	private Double snappetH;
	private Double snappetX;
	private Double snappetY;
	private List<Punktveiref> veireferanser = new ArrayList<Punktveiref>();
	private Double x;
	private Double y;
	private Sak sak;
	private String sakGuid;

	/**
	 * Konstruktør
	 * 
	 */
	public Punktstedfest() {
		log("Punktstedfest()");
	}

	/**
	 * Konstruktør
	 * 
	 */
	public Punktstedfest(Punktstedfest sted) {
		log("Punktstedfest()");
		
		reflinkIdent = sted.getReflinkIdent();
		reflinkPosisjon = sted.getReflinkPosisjon();
		snappetH = sted.getSnappetH();
		snappetX = sted.getSnappetX();
		snappetY = sted.getSnappetY();
		x = sted.getX();
		y = sted.getY();
	}

	/**
	 * Legger til en veireferanse
	 * 
	 * @param punktveiref
	 * @return
	 */
	public Punktveiref addPunktveiref(Punktveiref punktveiref) {
		List<Punktveiref> old = new ArrayList<Punktveiref>(this.veireferanser);
		Punktveiref veiref = getVeiref();
		String oldHpText = veiref==null?null:getProdHpGUIText();
		getVeireferanser().add(punktveiref);
		punktveiref.setPunktstedfest(this);

		getProps().firePropertyChange(FIELD_VEIREFERANSER, old, getVeireferanser());
		getProps().firePropertyChange(FIELD_VEIREF, veiref, getVeiref());
		getProps().firePropertyChange("prodHpGUIText", oldHpText, getProdHpGUIText());
		return punktveiref;
	}

	@Override
	public int compareTo(Punktstedfest o) {
		if (o == null) {
			return 1;
		}

		Double thisx = getSnappetX() != null ? getSnappetX() : getX();
		Double otherx = o.getSnappetX() != null ? o.getSnappetX() : o.getX();

		Double thisy = getSnappetY() != null ? getSnappetY() : getY();
		Double othery = o.getSnappetY() != null ? o.getSnappetY() : o.getY();

		Double thish = getSnappetH();
		Double otherh = o.getSnappetH();

		return new CompareToBuilder().append(thisx, otherx).append(thisy, othery).append(thish, otherh).toComparison();
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(nullable = false)
	public String getGuid() {
		return guid;
	}

	/**
	 * Sak
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "SAK_GUID", referencedColumnName = "GUID")
	public Sak getSak() {
		return sak;
	}

	/**
	 * Sak
	 * 
	 * @return
	 */
	@Column(name = "SAK_GUID", nullable = true, insertable = false, updatable = false)
	public String getSakGuid() {
		return sakGuid;
	}

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/**
	 * Avvik
	 * 
	 * @return
	 */
	@OneToOne(optional = true)
	@JoinColumn(name = "AVVIK_GUID", referencedColumnName = "GUID")
	public Avvik getAvvik() {
		return avvik;
	}

	/**
	 * Avvik
	 * 
	 * @return
	 */
	@Column(name = "AVVIK_GUID", nullable = true, insertable = false, updatable = false)
	public String getAvvikGuid() {
		return avvikGuid;
	}

	/**
	 * Reflink
	 * 
	 * @return
	 */
	@Column(name = "REFLINK_IDENT")
	public Long getReflinkIdent() {
		return reflinkIdent;
	}

	/**
	 * Reflink
	 * 
	 * @return
	 */
	@Column(name = "REFLINK_POSISJON")
	public Double getReflinkPosisjon() {
		return reflinkPosisjon;
	}

	/**
	 * Snappet til vei
	 * 
	 * @return
	 */
	@Column(name = "SNAPPET_H")
	public Double getSnappetH() {
		return snappetH;
	}

	/**
	 * Snappet til vei
	 * 
	 * @return
	 */
	@Column(name = "SNAPPET_X")
	public Double getSnappetX() {
		return snappetX;
	}

	/**
	 * Snappet til vei
	 * 
	 * @return
	 */
	@Column(name = "SNAPPET_Y")
	public Double getSnappetY() {
		return snappetY;
	}

	@Override
	public String getTextForGUI() {
		Double xp = getSnappetX() != null ? getSnappetX() : getX();
		Double yp = getSnappetY() != null ? getSnappetY() : getY();
		Double hp = getSnappetH();
		String xTxt = xp != null ? String.valueOf(xp.intValue()) : UNKNOWN;
		String yTxt = yp != null ? String.valueOf(yp.intValue()) : UNKNOWN;
		String hTxt = hp != null ? String.valueOf(hp.intValue()) : UNKNOWN;
		String stedfestTxt = EAST + EQUAL + xTxt + COMMA + NORTH + EQUAL + yTxt + COMMA + HEIGHT + EQUAL + hTxt;
		return stedfestTxt;
	}

	/**
	 * Henter ut veiref med nyeste dato, hvis der ikke er noen returnerers
	 * {@code null}, se {@linkplain Punktveiref} og
	 * {@linkplain Punktveiref#compareTo(Punktveiref)}
	 * 
	 * @return
	 */
	@Transient
	public Punktveiref getVeiref() {
		List<Punktveiref> veier = getVeireferanser();
		if (veier == null || veier.size() == 0) {
			return null;
		}

		Punktveiref[] sorterte = new Punktveiref[veier.size()];
		sorterte = veier.toArray(sorterte);
		Arrays.sort(sorterte);
		

		return sorterte[0];
	}
	
	@Transient
	public String getProdHpGUIText() {
		return VeiInfoUtil.buildVeiInfo(getVeiref()).toString();
	}

	@Transient 
	public String getKoordinatString(){
		DecimalFormat f = new DecimalFormat("#0.000");
		String lat = getY()!=null?f.format(getY()):"??";
		String lon = getX()!=null?f.format(getX()):"??";
		
		String e = getSnappetX()!=null?getSnappetX().toString():"??";
		String n = getSnappetY()!=null?getSnappetY().toString():"??";
		StringBuilder sb = new StringBuilder();
		sb.append("lat:");
		sb.append(lat);
		sb.append(" lon:");
		sb.append(lon);
		sb.append(" (WGS84) \n");
		sb.append(Resources.getResource("east"));
		sb.append(":");
		sb.append(e);
		sb.append(" ");
		sb.append(Resources.getResource("north"));
		sb.append(":");
		sb.append(n);
		sb.append(" (UTM33)");
		return sb.toString();
		
	}
	/**
	 * Veireferansen
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "punktstedfest", fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@OrderBy("fraDato DESC")
	public List<Punktveiref> getVeireferanser() {
		return veireferanser;
	}

	/**
	 * Lengdegrad
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public Double getX() {
		return x;
	}

	/**
	 * Breddegrad
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public Double getY() {
		return y;
	}

	/**
	 * Fjerner en veireferanse
	 * 
	 * @param punktveiref
	 * @return
	 */
	public Punktveiref removePunktveiref(Punktveiref punktveiref) {
		List<Punktveiref> old = new ArrayList<Punktveiref>(getVeireferanser());
		Punktveiref veiref = getVeiref();
		getVeireferanser().remove(punktveiref);

		getProps().firePropertyChange(FIELD_VEIREFERANSER, old, getVeireferanser());
		getProps().firePropertyChange(FIELD_VEIREF, veiref, getVeiref());
		return punktveiref;
	}

	/**
	 * Id
	 * 
	 * @param guid
	 */
	public void setGuid(String guid) {
		String old = this.guid;
		this.guid = guid;
		getProps().firePropertyChange(ID_FIELD, old, guid);
	}

	/**
	 * Sak
	 * 
	 * @param sak
	 */
	public void setSak(Sak sak) {
		Sak old = this.sak;
		this.sak = sak;

		if (sak != null) {
			setSakGuid(sak.getGuid());
		} else {
			setSakGuid(null);
		}

		firePropertyChange(FIELD_SAK, old, sak);
	}

	/**
	 * sak
	 * 
	 * @param sakGuid
	 */
	public void setSakGuid(String sakGuid) {
		String old = this.sakGuid;
		this.sakGuid = sakGuid;
		firePropertyChange(FIELD_SAKGUID, old, sakGuid);
	}

	/**
	 * Avvik
	 * 
	 * @param 
	 */
	public void setAvvik(Avvik avvik) {
		Avvik old = this.avvik;
		this.avvik = avvik;

		if (avvik != null) {
			setAvvikGuid(avvik.getGuid());
		} else {
			setAvvikGuid(null);
		}
		firePropertyChange(FIELD_AVVIK, old, avvik);
	}

	/**
	 * Avvik
	 * 
	 * @param avvikGuid
	 */
	public void setAvvikGuid(String avvikGuid) {
		String old = this.avvikGuid;
		this.avvikGuid = avvikGuid;
		firePropertyChange(FIELD_AVVIKGUID, old, avvikGuid);
	}

	/**
	 * Reflink
	 * 
	 * @param reflinkIdent
	 */
	public void setReflinkIdent(Long reflinkIdent) {
		Long old = this.reflinkIdent;
		this.reflinkIdent = reflinkIdent;
		firePropertyChange(FIELD_REFLINKIDENT, old, reflinkIdent);
	}

	/**
	 * Reflink
	 * 
	 * @param reflinkPosisjon
	 */
	public void setReflinkPosisjon(Double reflinkPosisjon) {
		Double old = this.reflinkPosisjon;
		this.reflinkPosisjon = reflinkPosisjon;
		firePropertyChange(FIELD_REFLINKPOSISJON, old, reflinkPosisjon);
	}

	/**
	 * Snappet til vei
	 * 
	 * @param snappetH
	 */
	public void setSnappetH(Double snappetH) {
		String oldTxt = getTextForGUI();
		Double old = this.snappetH;
		this.snappetH = snappetH;
		firePropertyChange(FIELD_SNAPPETH, old, snappetH);
		firePropertyChange(FIELD_TEXTFORGUI, oldTxt, getTextForGUI());
	}

	/**
	 * Snappet til vei
	 * 
	 * @param snappetX
	 */
	public void setSnappetX(Double snappetX) {
		String oldTxt = getTextForGUI();
		Double old = this.snappetX;
		String oldKoord = getKoordinatString();
		this.snappetX = snappetX;
		firePropertyChange(FIELD_SNAPPETX, old, snappetX);
		firePropertyChange(FIELD_TEXTFORGUI, oldTxt, getTextForGUI());
		getProps().firePropertyChange("koordinatString", oldKoord, getKoordinatString());

	}

	/**
	 * Snappet til vei
	 * 
	 * @param snappetY
	 */
	public void setSnappetY(Double snappetY) {
		String oldTxt = getTextForGUI();
		Double old = this.snappetY;
		String oldKoord = getKoordinatString();
		this.snappetY = snappetY;
		firePropertyChange(FIELD_SNAPPETY, old, snappetY);
		firePropertyChange(FIELD_TEXTFORGUI, oldTxt, getTextForGUI());
		getProps().firePropertyChange("koordinatString", oldKoord, getKoordinatString());
	}

	/**
	 * Veireferansen
	 * 
	 * @param punktveirefList
	 */
	public void setVeireferanser(List<Punktveiref> punktveirefList) {
		List<Punktveiref> old = new ArrayList<Punktveiref>(this.veireferanser);
		Punktveiref veiref = getVeiref();
		this.veireferanser = punktveirefList;
		firePropertyChange(FIELD_VEIREFERANSER, old, punktveirefList);
		firePropertyChange(FIELD_VEIREF, veiref, getVeiref());
	}

	/**
	 * Lengdegrad
	 * 
	 * @param x
	 */
	public void setX(Double x) {
		String oldTxt = getTextForGUI();
		Double old = this.x;
		String oldKoord = getKoordinatString();
		this.x = x;
		firePropertyChange(FIELD_X, old, x);
		firePropertyChange(FIELD_TEXTFORGUI, oldTxt, getTextForGUI());
		getProps().firePropertyChange("koordinatString", oldKoord, getKoordinatString());
	}

	/**
	 * Breddegrad
	 * 
	 * @param y
	 */
	public void setY(Double y) {
		String oldTxt = getTextForGUI();
		Double old = this.y;
		String oldKoord = getKoordinatString();
		this.y = y;
		firePropertyChange(FIELD_Y, old, y);
		firePropertyChange(FIELD_TEXTFORGUI, oldTxt, getTextForGUI());
		getProps().firePropertyChange("koordinatString", oldKoord, getKoordinatString());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append(ID_FIELD, guid).append(FIELD_AVVIKGUID, avvikGuid).append(
				FIELD_SAKGUID, sakGuid).append(FIELD_Y, y).append(FIELD_X, x)
				.append(FIELD_SNAPPETX, snappetX).append(FIELD_SNAPPETY, snappetY).append(FIELD_SNAPPETH, snappetH)
				.append(FIELD_REFLINKIDENT, reflinkIdent).append(FIELD_REFLINKPOSISJON, reflinkPosisjon).toString();
	}
}
