package no.mesta.mipss.persistence.stroing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
@NamedQuery(name = Stroprodukt.QUERY_FIND_ALL, query = "select o from Stroprodukt o"),
@NamedQuery(name = Stroprodukt.QUERY_FIND_FORETTERHENGENDE, query = "select o from Stroprodukt o where o.forEtterhengendeFlagg=1")
})
@SequenceGenerator(name = "stroproduktIdSeq", sequenceName = "STROPRODUKT_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Stroprodukt extends MipssEntityBean<Stroprodukt> {
	public static final String QUERY_FIND_ALL = "Stroprodukt.findAll";
	public static final String QUERY_FIND_FORETTERHENGENDE = "Stroprodukt.findForEtterhengende";
	
	private Long id;
	private Long produktgruppeId;
	private String navn;
	private String beskrivelse;
	private Long guiRekkefolge;
	private String svvIdent;
	private Long flytAkkumulatorFlagg;
	private Long forEtterhengendeFlagg;
	private Long vaattFlagg;
	private Long driftsloggFlagg;
	private Long toKammerFlagg;

	private Stroproduktgruppe stroproduktgruppe;

	public Stroprodukt() {
	}

	public Stroprodukt(Long id, String navn, Long produktgruppeId) {
		this.id = id;
		this.navn = navn;
		this.produktgruppeId = produktgruppeId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stroproduktIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	@Column(name = "PRODUKTGRUPPE_ID", nullable = false, insertable = false, updatable = false)
	public Long getProduktgruppeId() {
		return produktgruppeId;
	}

	public void setProduktgruppeId(Long produktgruppeId) {
		Long old = this.produktgruppeId;
		this.produktgruppeId = produktgruppeId;
		
		firePropertyChange("produktgruppeId", old, produktgruppeId);
	}

	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		
		firePropertyChange("navn", old, navn);
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

	@Column(name = "GUI_REKKEFOLGE", nullable = false)
	public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}

	public void setGuiRekkefolge(Long guiRekkefolge) {
		Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}

	@Column(name = "SVV_IDENT")
	public String getSvvIdent() {
		return svvIdent;
	}

	public void setSvvIdent(String svvIdent) {
		String old = this.svvIdent;
		this.svvIdent = svvIdent;
		
		firePropertyChange("svvIdent", old, svvIdent);
	}

	@Column(name = "FLYT_AKKUMULATOR_FLAGG", nullable = false)
	public Long getFlytAkkumulatorFlagg() {
		return flytAkkumulatorFlagg;
	}

	public void setFlytAkkumulatorFlagg(Long flytAkkumulatorFlagg) {
		Long old = this.flytAkkumulatorFlagg;
		this.flytAkkumulatorFlagg = flytAkkumulatorFlagg;
		
		firePropertyChange("flytAkkumulatorFlagg", old, flytAkkumulatorFlagg);
	}

	@Column(name = "FOR_ETTERHENGENDE_FLAGG", nullable = false)
	public Long getForEtterhengendeFlagg() {
		return forEtterhengendeFlagg;
	}

	public void setForEtterhengendeFlagg(Long forEtterhengendeFlagg) {
		Long old = this.forEtterhengendeFlagg;
		this.forEtterhengendeFlagg = forEtterhengendeFlagg;
		
		firePropertyChange("forEtterhengendeFlagg", old, forEtterhengendeFlagg);
	}

	@ManyToOne
	@JoinColumn(name = "PRODUKTGRUPPE_ID", referencedColumnName = "ID", nullable = false)
	public Stroproduktgruppe getStroproduktgruppe() {
		return stroproduktgruppe;
	}

	public void setStroproduktgruppe(Stroproduktgruppe stroproduktgruppe) {
		this.stroproduktgruppe = stroproduktgruppe;
		
		if(stroproduktgruppe != null) {
			setProduktgruppeId(stroproduktgruppe.getId());
		} else {
			setProduktgruppeId(null);
		}
	}
	
	@Column(name = "VAATT_FLAGG", nullable = false)
	public Long getVaattFlagg() {
		return vaattFlagg;
	}

	public void setVaattFlagg(Long vaattFlagg) {
		Long old = this.vaattFlagg;
		this.vaattFlagg = vaattFlagg;
		firePropertyChange("vaattFlagg", old, vaattFlagg);
	}

	@Column(name = "DRIFTSLOGG_FLAGG", nullable = false)
	public Long getDriftsloggFlagg() {
		return driftsloggFlagg;
	}

	public void setDriftsloggFlagg(Long driftsloggFlagg) {
		Long old = this.vaattFlagg;
		this.driftsloggFlagg = driftsloggFlagg;
		firePropertyChange("driftsloggFlagg", old, driftsloggFlagg);
	}

	@Column(name = "TO_KAMMER_FLAGG", nullable = false)
	public Long getToKammerFlagg() {
		return toKammerFlagg;
	}

	public void setToKammerFlagg(Long toKammerFlagg) {
		Long old = this.toKammerFlagg;
		this.toKammerFlagg = toKammerFlagg;
		firePropertyChange("toKammerFlagg", old, toKammerFlagg);
	}

	@Override
	public String getTextForGUI() {
		return navn;
	}
}
