package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Nøkkel for prosjektkontrakt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class KontraktProsjektPK implements Serializable {
    private Long kontraktId;
    private String prosjektNr;
    private String selskapskode;

    /**
     * Konstruktør
     * 
     */
    public KontraktProsjektPK() {
    }

    /**
     * Konstruktør
     * 
     * @param kontraktId
     * @param prosjektId
     */
    public KontraktProsjektPK(Long kontraktId, String prosjektNr, String selskapkode) {
        this.kontraktId = kontraktId;
        this.prosjektNr = prosjektNr;
        this.selskapskode = selskapkode;
    }

    /**
     * Kontrakt id
     * 
     * @param kontraktId
     */
    public void setKontraktId(Long kontraktId) {
        this.kontraktId = kontraktId;
    }

    /**
     * Kontrakt id
     * 
     * @return
     */
    public Long getKontraktId() {
        return kontraktId;
    }

    /**
     * Prosjekt id
     * 
     * @param prosjektId
     */
    public void setProsjektNr(String prosjektNr) {
        this.prosjektNr = prosjektNr;
    }

    /**
     * Prosjekt id
     * 
     * @return
     */
    public String getProsjektNr() {
        return prosjektNr;
    }
    
    
    public void setSelskapskode(String selskapskode){
    	this.selskapskode = selskapskode;
    }
    
    public String getSelskapskode(){
    	return selskapskode;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
        	.append(kontraktId)
        	.append(prosjektNr)
        	.append(selskapskode)
        	.toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktProsjektPK)) {return false;}
        
        KontraktProsjektPK other = (KontraktProsjektPK) o;
        
        return new EqualsBuilder()
        	.append(kontraktId, other.kontraktId)
        	.append(prosjektNr, other.prosjektNr)
        	.append(selskapskode, other.getSelskapskode())
        	.isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("kontraktId", kontraktId).
            append("prosjektNr", prosjektNr).
            append("selskapskode", selskapskode).
            toString();
    }
}
