package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Dokument;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "utstyrsubgruppeIdSeq", sequenceName = "UTSTYRSUBGRUPPE_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = Utstyrsubgruppe.FIND_ALL, query = "select o from Utstyrsubgruppe o order by o.guiRekkefolge"),
    @NamedQuery(name = Utstyrsubgruppe.FIND_VALGBAR, query = "select o from Utstyrsubgruppe o where o.valgbarFlagg=true order by o.guiRekkefolge"),
    @NamedQuery(name = Utstyrsubgruppe.FOR_GRUPPE, query = "select o from Utstyrsubgruppe o where o.utstyrgruppe.id = :id and o.valgbarFlagg=true order by o.guiRekkefolge",
        hints={
            @QueryHint(name=QueryHints.REFRESH, value=HintValues.TRUE)
        }
    )
})

@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)

public class Utstyrsubgruppe extends MipssEntityBean<Utstyrsubgruppe> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity, Comparable<Utstyrsubgruppe> {
    public static final String FIND_ALL = "Utstyrsubgruppe.findAll";
    public static final String FIND_VALGBAR = "Utstyrsubgruppe.findValgbar";
    public static final String FOR_GRUPPE = "Utstyrsubgruppe.forGruppe";
    private String fritekst;
    private Long guiRekkefolge;
    private Long id;
    private Boolean kanInstrFlagg;
    private Boolean kreverModellFlagg;
    private String navn;
    private Boolean valgbarFlagg;
    private Boolean serieportFlagg;
    private List<KjoretoyUtstyr> kjoretoyUtstyrList = new ArrayList<KjoretoyUtstyr>();
    private Utstyrgruppe utstyrgruppe;
    private List<Utstyrmodell> utstyrmodellList = new ArrayList<Utstyrmodell>();

    // lagt til for h�nd
    private OwnedMipssEntity ownedMipssEntity;
    private List<Bilde> bildeList = new ArrayList<Bilde>();
    private List<Dokument> dokumentList = new ArrayList<Dokument>();

    public Utstyrsubgruppe() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Column(name="GUI_REKKEFOLGE", nullable = false)
    public Long getGuiRekkefolge() {
        return guiRekkefolge;
    }

    public void setGuiRekkefolge(Long guiRekkefolge) {
    	Long old = this.guiRekkefolge;
        this.guiRekkefolge = guiRekkefolge;
        firePropertyChange("guiRekkefolge", old, guiRekkefolge);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utstyrsubgruppeIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(name="KAN_INSTR_FLAGG", nullable = false)
    public Boolean getKanInstrFlagg() {
    	if(kanInstrFlagg == null) {
    		kanInstrFlagg = false;
    	}
        return kanInstrFlagg;
    }

    public void setKanInstrFlagg(Boolean kanInstrFlagg) {
    	Boolean old = this.kanInstrFlagg;
    	
        this.kanInstrFlagg = kanInstrFlagg;
        firePropertyChange("kanInstrFlagg", old, kanInstrFlagg);
    }

    @Column(name="KREVER_MODELL_FLAGG", nullable = false)
    public Boolean getKreverModellFlagg() {
    	if(kreverModellFlagg == null) {
    		kreverModellFlagg = false;
    	}
        return kreverModellFlagg;
    }

    public void setKreverModellFlagg(Boolean kreverModellFlagg) {
    	Boolean old = this.kreverModellFlagg;
        this.kreverModellFlagg = kreverModellFlagg;
        firePropertyChange("kreverModellFlagg", old, kreverModellFlagg);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
    	if(valgbarFlagg == null) {
    		valgbarFlagg = false;
    	}
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }
    
    @Column(name="SERIEPORT_FLAGG", nullable = false)
    public Boolean getSerieportFlagg() {
        return serieportFlagg;
    }

    public void setSerieportFlagg(Boolean serieportFlagg) {
    	Boolean old = this.serieportFlagg;
        this.serieportFlagg = serieportFlagg;
        firePropertyChange("serieportFlagg", old, serieportFlagg);
    }

    @OneToMany(mappedBy = "utstyrsubgruppe")
    public List<KjoretoyUtstyr> getKjoretoyUtstyrList() {
        return kjoretoyUtstyrList;
    }

    public void setKjoretoyUtstyrList(List<KjoretoyUtstyr> kjoretoyUtstyrList) {
        this.kjoretoyUtstyrList = kjoretoyUtstyrList;
    }

    public KjoretoyUtstyr addKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
        getKjoretoyUtstyrList().add(kjoretoyUtstyr);
        kjoretoyUtstyr.setUtstyrsubgruppe(this);
        return kjoretoyUtstyr;
    }

    public KjoretoyUtstyr removeKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
        getKjoretoyUtstyrList().remove(kjoretoyUtstyr);
        kjoretoyUtstyr.setUtstyrsubgruppe(null);
        return kjoretoyUtstyr;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UTSTYRGRUPPE_ID", referencedColumnName = "ID", nullable = false)
    public Utstyrgruppe getUtstyrgruppe() {
        return utstyrgruppe;
    }

    public void setUtstyrgruppe(Utstyrgruppe utstyrgruppe) {
    	Utstyrgruppe old = this.utstyrgruppe;
        this.utstyrgruppe = utstyrgruppe;
        firePropertyChange("utstyrgruppe", old, utstyrgruppe);
    }

    @OneToMany(mappedBy = "utstyrsubgruppe", fetch = FetchType.EAGER)
    @OrderBy("guiRekkefolge")
    public List<Utstyrmodell> getUtstyrmodellList() {
        return utstyrmodellList;
    }

    public void setUtstyrmodellList(List<Utstyrmodell> utstyrmodellList) {
        this.utstyrmodellList = utstyrmodellList;
    }

    public Utstyrmodell addUtstyrmodell(Utstyrmodell utstyrmodell) {
        getUtstyrmodellList().add(utstyrmodell);
        utstyrmodell.setUtstyrsubgruppe(this);
        return utstyrmodell;
    }

    public Utstyrmodell removeUtstyrmodell(Utstyrmodell utstyrmodell) {
        getUtstyrmodellList().remove(utstyrmodell);
        utstyrmodell.setUtstyrsubgruppe(null);
        return utstyrmodell;
    }

    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("navn", getNavn()).
            append("rekkefolge", getGuiRekkefolge()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Utstyrsubgruppe)) {
            return false;
        }
        Utstyrsubgruppe other = (Utstyrsubgruppe) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Utstyrsubgruppe o) {
        Utstyrsubgruppe other = (Utstyrsubgruppe) o;
        int comp = new CompareToBuilder().
	        append(getGuiRekkefolge(), other.getGuiRekkefolge()).
	        toComparison();
    
	    if(comp == 0) {
	    	comp = new CompareToBuilder().
		        append(getNavn(), other.getNavn()).
		        toComparison();
	    }
	    
	    return comp;
    }
    
    /**
     * stter bildelisten i et mange-til-mange forhold.
     * @param bildeList
     */
    public void setBildeList(List<Bilde> bildeList) {
        this.bildeList = bildeList;
    }

    public List<Bilde> getBildeList() {
        return bildeList;
    }

    public void setDokumentList(List<Dokument> dokumentList) {
        this.dokumentList = dokumentList;
    }

    public List<Dokument> getDokumentList() {
        return dokumentList;
    }   

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
}
