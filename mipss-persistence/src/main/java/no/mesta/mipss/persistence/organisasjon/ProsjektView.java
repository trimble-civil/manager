package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(
        name = ProsjektView.QUERY_FIND_ALL,
        query = "SELECT o FROM ProsjektView o"),
	@NamedQuery(
		name = ProsjektView.QUERY_FIND_BY_PROSJEKTNR,
        query = "SELECT o FROM ProsjektView o WHERE o.prosjektnr = :prosjektnr")
    })
@Table(name = "PROSJEKT_V")
@IdClass(ProsjektViewPK.class)
public class ProsjektView implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "ProsjektView.findAll";
	public static final String QUERY_FIND_BY_PROSJEKTNR = "ProsjektView.findByProsjekt";
	
	private String prosjektnr;
	private String prosjektnavn;
	private String beskrivelse;
	private Timestamp ansvarFradato;
	private Timestamp ansvarTildato;
	private String ansvar;
	private String selskapskode;
	
	public ProsjektView() {
	}
	
	@Id
	public String getProsjektnr() {
		return prosjektnr;
	}

	public void setProsjektnr(String prosjektnr) {
		this.prosjektnr = prosjektnr;
	}

	public String getProsjektnavn() {
		return prosjektnavn;
	}

	public void setProsjektnavn(String prosjektnavn) {
		this.prosjektnavn = prosjektnavn;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	@Column(name = "ANSVAR_FRADATO")
	public Timestamp getAnsvarFradato() {
		return ansvarFradato;
	}

	public void setAnsvarFradato(Timestamp ansvarFradato) {
		this.ansvarFradato = ansvarFradato;
	}

	@Column(name = "ANSVAR_TILDATO")
	public Timestamp getAnsvarTildato() {
		return ansvarTildato;
	}

	public void setAnsvarTildato(Timestamp ansvarTildato) {
		this.ansvarTildato = ansvarTildato;
	}

	public String getAnsvar() {
		return ansvar;
	}

	public void setAnsvar(String ansvar) {
		this.ansvar = ansvar;
	}

	@Id
	public String getSelskapskode() {
		return selskapskode;
	}

	public void setSelskapskode(String selskapskode) {
		this.selskapskode = selskapskode;
	}

	public String getTextForGUI() {
		return getProsjektnr() + " - " + getProsjektnavn();
	}
}
