/**
 * 
 */
package no.mesta.mipss.persistence.exceptions;

/**
 * Informerer om tidsoverlapping mellom to {@code IoListe} fro samme port fro
 * samme kjøretøy.
 * 
 * @author jorge
 * 
 */
@SuppressWarnings("serial")
public class IolisteOverlappingException extends RuntimeException {

	/**
	 * 
	 */
	public IolisteOverlappingException() {
		super();
	}

	/**
	 * @param message
	 */
	public IolisteOverlappingException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public IolisteOverlappingException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public IolisteOverlappingException(Throwable cause) {
		super(cause);
	}

}
