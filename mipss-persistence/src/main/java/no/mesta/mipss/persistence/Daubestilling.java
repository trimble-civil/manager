package no.mesta.mipss.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
@Table(name = "DAU_BESTILLING")
@SequenceGenerator(name = Daubestilling.ID_SEQ, sequenceName = "DAU_BESTILLING_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Daubestilling implements Serializable{
	public static final String ID_SEQ = "dauBestillingIdSeq";
	
	private Long id;
	
	private Long kontraktId;
	private Long rodeId;
	private Date periodeFra;
	private Date periodeTil;
	private String filnavn;
	private Date behandletDato;
	
	private Date opprettetDato;

	private String opprettetAv;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Daubestilling.ID_SEQ)
    @Column(nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="KONTRAKT_ID", nullable=false)
	
	public Long getKontraktId(){
		return kontraktId;
	}
	public void setKontraktId(Long kontraktId){
		this.kontraktId = kontraktId;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PERIODE_FRA", nullable=false)
	public Date getPeriodeFra(){
		return periodeFra;
	}
	public void setPeriodeFra(Date periodeFra){
		this.periodeFra = periodeFra;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PERIODE_TIL", nullable=false)
	public Date getPeriodeTil(){
		return periodeTil;
	}
	public void setPeriodeTil(Date periodeTil){
		this.periodeTil = periodeTil;
	}
	public String getFilnavn(){
		return filnavn;
	}
	public void setFilnavn(String filnavn){
		this.filnavn = filnavn;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEHANDLET_DATO")
	public Date getBehandletDato(){
		return behandletDato;
	}
	public void setBehandletDato(Date behandletDato){
		this.behandletDato = behandletDato;
	}
	@Column(name="RODE_ID", nullable=false)
	public Long getRodeId(){
		return rodeId;
	}
	public void setRodeId(Long rodeId){
		this.rodeId = rodeId;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPPRETTET_DATO")
	public Date getOpprettetDato(){
		return this.opprettetDato;
	}
	public void setOpprettetDato(Date opprettetDato){
		this.opprettetDato = opprettetDato;
	}
	@Column(name="OPPRETTET_AV")
	public String getOpprettetAv(){
		return this.opprettetAv;
	}
	public void setOpprettetAv(String opprettetAv){
		this.opprettetAv = opprettetAv;
	}
	
	
	public int hashCode(){
		return new HashCodeBuilder(4021, 4039).append(id).toHashCode();
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Daubestilling)) {
			return false;
		}

		Daubestilling other = (Daubestilling) o;
		return new EqualsBuilder().append(id, other.getId()).isEquals();
	}
	
}
