package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * En rad for et bibliotek i databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = "BIBLIOTEK_SEQ", sequenceName = "BIBLIOTEK_ID_SEQ", allocationSize=1)
@NamedQueries({
    @NamedQuery(name = Bibliotek.QUERY_FIND_ALL, query = "select o from Bibliotek o")
})
public class Bibliotek implements Serializable {
    public static final String QUERY_FIND_ALL = "Bibliotek.findAll";
    
    private String endretAv;
    private Date endretDato;
    private String filnavn;
    private String gjenstandNavn;
    private String gruppeNavn;
    private Long id;
    private byte[] jarfilLob;
    private String opprettetAv;
    private Date opprettetDato;
    private String versjon;
    private Integer win32Flagg;
    private List<AppBib> appBibList = new ArrayList<AppBib>();
    private Integer size;
    
    /**
     * Konstrukt�r
     * 
     */
    public Bibliotek() {
    }

    /**
     * Hvem endret biblioteket
     * 
     * @return
     */
    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    /**
     * Hvem endret biblioteket
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    /**
     * N�r ble biblioteket endret
     * 
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    /**
     * N�r ble biblioteket endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    /**
     * Filnavnet til biblioteket
     * 
     * @return
     */
    @Column(nullable = false)
    public String getFilnavn() {
        return filnavn;
    }

    /**
     * Filnavnet til biblioteket
     * 
     * @param filnavn
     */
    public void setFilnavn(String filnavn) {
        this.filnavn = filnavn;
    }

    /**
     * Maven artifactId
     * 
     * @return
     */
    @Column(name="GJENSTANDNAVN", nullable = false)
    public String getGjenstandNavn() {
        return gjenstandNavn;
    }

    /**
     * Maven artifactId
     * 
     * @param gjenstandNavn
     */
    public void setGjenstandNavn(String gjenstandNavn) {
        this.gjenstandNavn = gjenstandNavn;
    }

    /**
     * Maven groupId
     * 
     * @return
     */
    @Column(name="GRUPPENAVN", nullable = false)
    public String getGruppeNavn() {
        return gruppeNavn;
    }

    /**
     * Maven groupId
     * 
     * @param gruppeNavn
     */
    public void setGruppeNavn(String gruppeNavn) {
        this.gruppeNavn = gruppeNavn;
    }

    /**
     * Id i databasen
     * 
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BIBLIOTEK_SEQ")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Id i databasen
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Bibliotekets fil
     * 
     * @return
     */
    @Column(name="JARFIL_LOB", nullable = false)
    public byte[] getJarfilLob() {
        return jarfilLob;
    }

    /**
     * Bibliotekets fil
     * 
     * @param jarfilLob
     */
    public void setJarfilLob(byte[] jarfilLob) {
        this.jarfilLob = jarfilLob;
    }

    /**
     * Hvem opprettet biblioteket
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Hvem opprettet biblioteket
     * 
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    /**
     * N�r ble biblioteket opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r ble biblioteket opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Maven version
     * 
     * @return
     */
    @Column(nullable = false)
    public String getVersjon() {
        return versjon;
    }

    /**
     * Maven version
     * 
     * @param versjon
     */
    public void setVersjon(String versjon) {
        this.versjon = versjon;
    }

    /**
     * Angir om det er windows native eller ikke
     * 
     * @param win32Flagg
     */
    public void setWin32Flagg(Integer win32Flagg) {
        this.win32Flagg = win32Flagg;
    }

    /**
     * Angir om det er windows native eller ikke
     * 
     * @return
     */
    @Column(name = "WIN32_FLAGG", nullable = false)
    public Integer getWin32Flagg() {
        return win32Flagg;
    }

    /**
     * St�rrelsen p� jarfilen
     * 
     * @param size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * St�rrelsen p� jarfilen
     * 
     * @return
     */
    @Transient
    public Integer getSize() {
        if(size == null) {
            if(jarfilLob != null) {
                size = jarfilLob.length;
            } else {
                size = 0;
            }
        }
        
        return size;
    }
    
    /**
     * Liste over applikasjoner som benytter biblioteket
     * 
     * @return
     */
    @OneToMany(mappedBy = "bibliotek")
    public List<AppBib> getAppBibList() {
        return appBibList;
    }

    /**
     * Liste over applikasjoner som benytter biblioteket
     * 
     * @param appBibList
     */
    public void setAppBibList(List<AppBib> appBibList) {
        this.appBibList = appBibList;
    }

    /**
     * Legger til et bibliotek
     * 
     * @param appBib
     * @return
     */
    public AppBib addAppBib(AppBib appBib) {
        getAppBibList().add(appBib);
        appBib.setBibliotek(this);
        return appBib;
    }

    /**
     * Fjerner et bibliotek
     * 
     * @param appBib
     * @return
     */
    public AppBib removeAppBib(AppBib appBib) {
        getAppBibList().remove(appBib);
        appBib.setBibliotek(null);
        return appBib;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Bibliotek)) { return false;}
        if(o == this) { return true;}
        
        Bibliotek other = (Bibliotek) o;
        return new EqualsBuilder().
            append(gruppeNavn, other.getGruppeNavn()).
            append(gjenstandNavn, other.getGjenstandNavn()).
            append(versjon, other.getVersjon()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,983).
            append(gruppeNavn).
            append(gjenstandNavn).
            append(versjon).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("gruppeNavn", gruppeNavn).
            append("gjenstandNavn", gjenstandNavn).
            append("versjon", versjon).
            append("jarFilNavn", filnavn).
            append("jarfilLob", jarfilLob == null? "null":"not null").
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            toString();
    }
}
