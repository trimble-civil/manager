package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Ikon;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Holder en applikasjons rad fra databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = "APPLIKASJON_SEQ", sequenceName = "APPLIKASJON_ID_SEQ", allocationSize=1)
@NamedQueries({
    @NamedQuery(name = Applikasjon.QUERY_FIND_ALL, query = "SELECT a FROM Applikasjon a"),
    @NamedQuery(name = Applikasjon.QUERY_FIND_APPLIKASJONER_MAVEN, query = "SELECT a FROM Applikasjon a WHERE a.gruppeNavn = :gruppenavn AND a.gjenstandNavn = :gjenstandnavn")
})
public class Applikasjon implements Serializable {
    public static final String QUERY_FIND_ALL = "Applikasjon.findAll";
    public static final String QUERY_FIND_APPLIKASJONER_MAVEN = "Applikasjon.getApplikasjonMaven";
    
    private Long id;
    private Appstatus appstatus = Appstatus.NEW; // Alle nye applikasjoner starter p� "NEW"
    private Ikon ikon;
    private Long ikonId;
    private Bilde splashBilde;
    private String splashGuid;
    private String versjon;
    private String gruppeNavn;
    private String gjenstandNavn;
    private String opprettetAv;
    private Date opprettetDato;
    private String endretAv;
    private Date endretDato;
    private String beskrivelse;
    private String fritekst;
    private String hovedklasse;
    private String guiHint;
    private String navn;
    private List<AppBib> appBibList = new ArrayList<AppBib>();
    private List<AppMod> appModList = new ArrayList<AppMod>();

    /**
     * Konstrukt�r
     */
    public Applikasjon() {
    }

    /**
     * Hvem endret objektet
     * 
     * @return
     */
    @Column(name="ENDRET_AV", length=8)
    public String getEndretAv() {
        return endretAv;
    }

    /**
     * Hvem endret objektet
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    /**
     * N�r ble objektet endret
     * 
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    /**
     * N�r ble objektet endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    /**
     * Maven artifactId
     * 
     * @return
     */
    @Column(name="GJENSTANDNAVN", length=255, nullable = false)
    public String getGjenstandNavn() {
        return gjenstandNavn;
    }

    /**
     * Maven artifactId
     * 
     * @param gjenstandNavn
     */
    public void setGjenstandNavn(String gjenstandNavn) {
        this.gjenstandNavn = gjenstandNavn;
    }

    /**
     * Maven gruppeId
     * 
     * @return
     */
    @Column(name="GRUPPENAVN", length=255, nullable = false)
    public String getGruppeNavn() {
        return gruppeNavn;
    }

    /**
     * Maven groupId
     * 
     * @param gruppeNavn
     */
    public void setGruppeNavn(String gruppeNavn) {
        this.gruppeNavn = gruppeNavn;
    }

    /**
     * Id for applikasjon
     * 
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APPLIKASJON_SEQ")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Id for applikasjon
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Hvem opprettet applikasjonen
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", length=8)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Hvem opprettet applikasjonen
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    /**
     * N�r ble applikasjonen opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r ble applikasjonen opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Maven version
     * 
     * @return
     */
    @Column(nullable = false, length=20)
    public String getVersjon() {
        return versjon;
    }

    /**
     * Maven version
     * 
     * @param versjon
     */
    public void setVersjon(String versjon) {
        this.versjon = versjon;
    }

    /**
     * Tilh�rende biblioteker
     * 
     * @return
     */
    @OneToMany(mappedBy = "applikasjon")
    public List<AppBib> getAppBibList() {
        return appBibList;
    }
    
    /**
     * Tilh�rende biblioteker
     * 
     * @param appBibList
     */
    public void setAppBibList(List<AppBib> appBibList) {
        this.appBibList = appBibList;
    }

    /**
     * Legger til et bibliotek
     * 
     * @param appBib
     * @return
     */
    public AppBib addAppBib(AppBib appBib) {
        getAppBibList().add(appBib);
        appBib.setApplikasjon(this);
        return appBib;
    }

    /**
     * Fjerner et bibliotek
     * 
     * @param appBib
     * @return
     */
    public AppBib removeAppBib(AppBib appBib) {
        getAppBibList().remove(appBib);
        appBib.setApplikasjon(null);
        return appBib;
    }

    /**
     * Tilh�rende moduler
     * 
     * @return
     */
    @OneToMany(mappedBy = "applikasjon", cascade={CascadeType.ALL})
    public List<AppMod> getAppModList() {
        return appModList;
    }
    
    /**
     * Tilh�rende moduler
     * 
     * @param appModList
     */
    public void setAppModList(List<AppMod> appModList) {
        this.appModList = appModList;
    }

    /**
     * Legger til en modul
     * 
     * @param appMod
     * @return
     */
    public AppMod addAppMod(AppMod appMod) {
        getAppModList().add(appMod);
        appMod.setApplikasjon(this);
        return appMod;
    }

    /**
     * Fjerner en modul
     * 
     * @param appMod
     * @return
     */
    public AppMod removeAppMod(AppMod appMod) {
        getAppModList().remove(appMod);
        appMod.setApplikasjon(null);
        return appMod;
    }

    /**
     * Applkasjonens status
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "STATUS_VERDI", referencedColumnName = "VERDI")
    public Appstatus getAppstatus() {
        return appstatus;
    }

    /**
     * Applikasjonens status
     * 
     * @param appstatus
     */
    public void setAppstatus(Appstatus appstatus) {
        this.appstatus = appstatus;
    }

    /**
     * Ikonet til applikasjonen
     * 
     * @param ikon
     */
    public void setIkon(Ikon ikon) {
        this.ikon = ikon;
        
        if(ikon != null) {
            setIkonId(ikon.getId());
        } else {
            setIkonId(null);
        }
    }

    /**
     * Ikonet til applikasjonen
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "IKON_ID", referencedColumnName = "ID")
	public Ikon getIkon() {
        return ikon;
    }

    /**
     * Splashbildet til applikasjonen
     * 
     * @param splashBilde
     */
    public void setSplashBilde(Bilde splashBilde) {
        this.splashBilde = splashBilde;
        
        if(splashBilde != null) {
            setSplashGuid(splashBilde.getGuid());
        } else {
            setSplashGuid(null);
        }
    }

    /**
     * Splashbildet til applikasjonen
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID")
    public Bilde getSplashBilde() {
        return splashBilde;
    }

    /**
     * Beskrivelsen(kort)
     * 
     * @param beskrivelse
     */
    public void setBeskrivelse(String beskrivelse) {
        this.beskrivelse = beskrivelse;
    }

    /**
     * Beskrivelsen(kort)
     * 
     * @return
     */
    @Column(length=255)
    public String getBeskrivelse() {
        return beskrivelse;
    }

    /**
     * Lengre beksrivelse
     * 
     * @param fritekst
     */
    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    /**
     * Lengre status
     * 
     * @return
     */
    @Column(length=4000)
    public String getFritekst() {
        return fritekst;
    }

    /**
     * Klassen som skal kj�res
     * 
     * @param hovedklasse
     */
    public void setHovedklasse(String hovedklasse) {
        this.hovedklasse = hovedklasse;
    }

    /**
     * Klassen som skal kj�res
     * 
     * @return
     */
    @Column(length=255)
    public String getHovedklasse() {
        return hovedklasse;
    }

    /**
     * Gui hint(tooltip)
     * 
     * @param guiHint
     */
    public void setGuiHint(String guiHint) {
        this.guiHint = guiHint;
    }

    /**
     * Gui hint(tooltip)
     * 
     * @return
     */
    @Column(length=255)
    public String getGuiHint() {
        return guiHint;
    }

    /**
     * Ikonet til applikasjonen
     * 
     * @param ikonId
     */
    public void setIkonId(Long ikonId) {
        this.ikonId = ikonId;
    }

    /**
     * Ikonet til applikasjonen
     * 
     * @return
     */
    @Column(name="IKON_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getIkonId() {
        return ikonId;
    }

    /**
     * Splashbildet til applikasjonen
     * 
     * @param spashGuid
     */
    public void setSplashGuid(String splashGuid) {
        this.splashGuid = splashGuid;
    }

    /**
     * Splashbildet til applikasjonen
     * 
     * @return
     */
    @Column(name="BILDE_GUID", nullable = false, insertable = false, 
        updatable = false)
    public String getSplashGuid() {
        return splashGuid;
    }

    /**
     * Applikasjonens navn
     * 
     * @param navn
     */
    public void setNavn(String navn) {
        this.navn = navn;
    }

    /**
     * Applikasjonens navn
     * 
     * @return
     */
    @Column(nullable=false, length=255)
    public String getNavn() {
        return navn;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Applikasjon)) { return false;}
        if(o == this) { return true;}
        
        Applikasjon other = (Applikasjon) o;
        
        return new EqualsBuilder().
            append(id, other.getId()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3,997).
            append(id).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            append("appstatus", appstatus).
            append("ikon", (ikon == null ? "null" : "set to:" + ikon.getId())).
            append("splashBilde", (splashBilde == null ? "null" : "set to:" + splashBilde.getGuid())).
            append("versjon", versjon).
            append("gruppeNavn", gruppeNavn).
            append("gjenstandNavn", gjenstandNavn).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            append("beskrivelse", beskrivelse).
            append("fritekst", fritekst).
            append("hovedklasse", hovedklasse).
            append("guiHint", guiHint).
            append("appBibList", (appBibList == null ? "null" : appBibList.size() + " items")).
            append("appModList", (appModList == null ? "null" : appModList.size() + "items")).
            toString();
    }
}
