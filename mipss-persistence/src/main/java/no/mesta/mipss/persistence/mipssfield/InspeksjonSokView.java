package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JPA for db view for inspeksjoner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( { @NamedQuery(name = InspeksjonSokView.QUERY_FIND_ALL, query = "SELECT i FROM InspeksjonSokView i") })
@Table(name = "INSPEKSJON_SOK_V")
public class InspeksjonSokView extends MipssEntityBean<InspeksjonSokView> implements Serializable,
		IRenderableMipssEntity, Comparable<InspeksjonSokView>, MipssFieldDetailItem {

	private static final Logger log = LoggerFactory.getLogger(InspeksjonSokView.class);

	private static final String ALLE_PROSESSER_FLAGG = "alleProsesserFlagg";
	private static final String ALLE_PROSESSER_SATT = "alleProsesserSatt";
	private static final String ANTALL_AVVIK = "antallAvvik";
	private static final String ANTALL_HENDELSER = "antallHendelser";
	private static final String BESKRIVELSE = "beskrivelse";
	private static final String EMPTY_STRING = "";
	private static final String ETTERSLEP_FLAGG = "etterslepFlagg";
	private static final String EV_INSPISERT = "evInspisert";
	private static final String FRA = "fra";
	private static final String FV_INSPISERT = "fvInspisert";
	private static final String HP_TEKST = "hpTekst";
	private static final String ID_NAME = "guid";
	private static final String[] ID_NAMES = new String[] { ID_NAME };
	private static final String KONTRAKT_ID = "kontraktId";
	private static final String KV_INSPISERT = "kvInspisert";
	private static final String OPPRETTET_AV = "opprettetAv";
	private static final String OPPRETTET_DATO = "opprettetDato";
	private static final String PDA_DFU_IDENT = "pdaDfuIdent";
	private static final String PDA_DFU_NAVN = "pdaDfuNavn";
	private static final String PROSESSER = "prosesser";
	private static final String PROSESSER_TEKST = "prosesserTekst";
	public static final String QUERY_FIND_ALL = "InspeksjonSokView.findAll";
	private static final String REFNUMMER = "refnummer";
	private static final String RV_INSPISERT = "rvInspisert";
	private static final String SLETTET_AV = "slettetAv";
	private static final String SLETTET_DATO = "slettetDato";
	private static final String STREKNINGER = "strekninger";
	private static final String TEMPERATUR = "temperatur";
	private static final String TEMPERATUR_ID = "temperaturId";
	private static final String TIL = "til";
	private static final String VAER = "vaer";
	private static final String VAER_ID = "vaerId";
	private static final String VIND = "vind";
	private static final String VIND_ID = "vindId";
	
	private Integer antallAvvik;
	private Integer antallHendelser;
	private String beskrivelse;
	private Long etterslepFlagg;
	private Double evInspisert;
	private Date fra;
	private Double fvInspisert;
	private String guid;
	private String hpTekst;
	private Long kontraktId;
	private Double kvInspisert;
	private String opprettetAv;
	private Date opprettetDato;
	private Long pdaDfuIdent;
	private String pdaDfuNavn;
	private List<InspeksjonSokProsess> prosesser = new ArrayList<InspeksjonSokProsess>();
	private String prosesserTekst;
	private Long refnummer;
	private Double rvInspisert;
	private String slettetAv;
	private Date slettetDato;
	private List<ProdstrekningSok> strekninger = new ArrayList<ProdstrekningSok>();
	private String temperatur;
	private Long temperaturId;
	private Date til;
	private String vaer;
	private Long vaerId;
	private String vind;
	private Long vindId;

	/**
	 * Konstruktør
	 */
	public InspeksjonSokView() {
		log("InspeksjonSokView() constructed");
	}
	
	/** {@inheritDoc} */
	public int compareTo(InspeksjonSokView o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(fra, o.fra).append(til, o.til).append(opprettetAv, o.opprettetAv)
				.toComparison();
	}
	/**
	 * Om alle prosesser er satt?
	 * 
	 * @return
	 */

	/**
	 * Antall funn registrert på inspeksjonen
	 * 
	 * @return
	 */
	@Column(name = "ANTALL_AVVIK")
	public Integer getAntallAvvik() {
		return antallAvvik;
	}

	/**
	 * @return the antallHendelser
	 */
	@Column(name = "ANTALL_HENDELSER")
	public Integer getAntallHendelser() {
		return antallHendelser;
	}

	@Column(name = "BESKRIVELSE")
	public String getBeskrivelse(){
		return beskrivelse;
	}

	/**
	 * Verktøysmetode for å gjøre om db Long til flagg
	 * 
	 * @return
	 */
	@Transient
	public Boolean getEtterslep() {
		return (etterslepFlagg != null && etterslepFlagg.equals(1L) ? Boolean.valueOf(true) : Boolean.valueOf(false));
	}

	/**
	 * Angir om det er en etterslepsregistrering (0 = nei, 1 = ja)
	 * 
	 * @return
	 */
	@Column(name = "ETTERSLEP_FLAGG")
	public Long getEtterslepFlagg() {
		return etterslepFlagg;
	}

	/**
	 * @return the evInspisert
	 */
	@Column(name = "EV_INSP")
	public Double getEvInspisert() {
		return evInspisert;
	}

	/**
	 * Når gjelder inspeksjonen fra
	 * 
	 * @return
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFra() {
		return fra;
	}

	@Transient
	public String getFunnHendelserTekst() {
		return String.valueOf(antallAvvik) + "/" + String.valueOf(antallHendelser);
	}

	/**
	 * @return the fvInspisert
	 */
	@Column(name = "FV_INSP")
	public Double getFvInspisert() {
		return fvInspisert;
	}

	/**
	 * unik id for en inspeksjon
	 * 
	 * @return
	 */
	@Id
	public String getGuid() {
		return guid;
	}

	/**
	 * @return the hpTekst
	 */
	@Column(name = "HP_INSP_TXT")
	public String getHpTekst() {
		return hpTekst;
	}

	/** {@inheritDoc} */
	@Override
	@Transient
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/**
	 * Kontrakten inspeksjonen gjelder
	 * 
	 * @return
	 */
	@Column(name = "KONTRAKT_ID")
	public Long getKontraktId() {
		return kontraktId;
	}

	/**
	 * @return the kvInspisert
	 */
	@Column(name = "KV_INSP")
	public Double getKvInspisert() {
		return kvInspisert;
	}

	/**
	 * Hvem utførte inspeksjonen
	 * 
	 * @return
	 */
	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	/**
	 * Datoen for funnet
	 * 
	 * @return
	 */
	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@Column(name = "PDA_DFU_IDENT")
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}

	@Column(name = "PDA_DFU_NAVN")
	public String getPdaDfuNavn() {
		return pdaDfuNavn;
	}

	/*
	 * Tilknyttede prosesser
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "inspeksjonSok", fetch = FetchType.LAZY)
	public List<InspeksjonSokProsess> getProsesser() {
		return prosesser;
	}
	
	/**
	 * @return the prosesserTekst
	 */
	@Column(name = "PROSESS_INSP_TXT")
	public String getProsesserTekst() {
		return prosesserTekst;
	}

	@Column(name=REFNUMMER)
	public Long getRefnummer(){
		return refnummer;
	}

	/**
	 * @return the rvInspisert
	 */
	@Column(name = "RV_INSP")
	public Double getRvInspisert() {
		return rvInspisert;
	}

	@Column(name = "SLETTET_AV")
	public String getSlettetAv() {
		return slettetAv;
	}

	@Column(name = "SLETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSlettetDato() {
		return slettetDato;
	}

	/**
	 * Produksjon
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "inspeksjonSok", fetch = FetchType.LAZY)
	public List<ProdstrekningSok> getStrekninger() {
		return strekninger;
	}

	/**
	 * @return the temperatur
	 */
	@Column(name = "TEMPERATUR_NAVN")
	public String getTemperatur() {
		return temperatur;
	}

	/**
	 * Temperatur intervall
	 * 
	 * @return
	 */
	@Column(name = "TEMPERATUR_ID", nullable = false, insertable = false, updatable = false)
	public Long getTemperaturId() {
		return temperaturId;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		String fraTidspunkt = fra != null ? MipssDateFormatter.formatDate(fra,
				MipssDateFormatter.SHORT_DATE_TIME_FORMAT) : EMPTY_STRING;
		String tilTidspunkt = til != null ? MipssDateFormatter.formatDate(til,
				MipssDateFormatter.SHORT_DATE_TIME_FORMAT) : EMPTY_STRING;
		String av = opprettetAv != null ? opprettetAv : EMPTY_STRING;

		return fraTidspunkt + " - " + tilTidspunkt + " av " + av;
	}

	/**
	 * Når gjelder inspeksjonen til
	 * 
	 * @return
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTil() {
		return til;
	}

	/**
	 * @return the vaer
	 */
	@Column(name = "VAER_NAVN")
	public String getVaer() {
		return vaer;
	}

	/**
	 * Vær
	 * 
	 * @return
	 */
	@Column(name = "VAER_ID", nullable = false, insertable = false, updatable = false)
	public Long getVaerId() {
		return vaerId;
	}

	/**
	 * @return the vind
	 */
	@Column(name = "VIND_NAVN")
	public String getVind() {
		return vind;
	}

	/**
	 * Vind
	 * 
	 * @return
	 */
	@Column(name = "VIND_ID", nullable = false, insertable = false, updatable = false)
	public Long getVindId() {
		return vindId;
	}


	/**
	 * Antall funn registrert på inspeksjonen
	 * 
	 * @param antallAvvik
	 */
	public void setAntallAvvik(Integer antallAvvik) {
		Integer old = this.antallAvvik;
		this.antallAvvik = antallAvvik;
		firePropertyChange(ANTALL_AVVIK, old, antallAvvik);
	}

	/**
	 * @param antallHendelser
	 *            the antallHendelser to set
	 */
	public void setAntallHendelser(Integer antallHendelser) {
		Integer old = this.antallHendelser;
		this.antallHendelser = antallHendelser;
		firePropertyChange(ANTALL_HENDELSER, old, antallHendelser);
	}

	public void setBeskrivelse(String beskrivelse){
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange(BESKRIVELSE, old, beskrivelse);
	}

	/**
	 * Angir om det er en etterslepsregistrering (0 = nei, 1 = ja)
	 * 
	 * @param etterslepFlagg
	 */
	public void setEtterslepFlagg(Long etterslepFlagg) {
		Long old = this.etterslepFlagg;
		this.etterslepFlagg = etterslepFlagg;
		firePropertyChange(ETTERSLEP_FLAGG, old, etterslepFlagg);
	}

	/**
	 * @param evInspisert
	 *            the evInspisert to set
	 */
	public void setEvInspisert(Double evInspisert) {
		Double old = this.evInspisert;
		this.evInspisert = evInspisert;
		firePropertyChange(EV_INSPISERT, old, evInspisert);
	}

	/**
	 * Når gjelder inspeksjonen fra
	 * 
	 * @param d
	 */
	public void setFra(Date d) {
		Date old = fra;
		fra = d;
		firePropertyChange(FRA, old, fra);
	}

	/**
	 * @param fvInspisert
	 *            the fvInspisert to set
	 */
	public void setFvInspisert(Double fvInspisert) {
		Double old = this.fvInspisert;
		this.fvInspisert = fvInspisert;
		firePropertyChange(FV_INSPISERT, old, fvInspisert);
	}

	/**
	 * Unik id for en inspeksjon
	 * 
	 * @param g
	 */
	public void setGuid(String g) {
		log.trace("setGuid({})", g);
		String old = guid;
		guid = g;
		firePropertyChange(ID_NAME, old, guid);
	}

	/**
	 * @param hpTekst
	 *            the hpTekst to set
	 */
	public void setHpTekst(String hpTekst) {
		String old = this.hpTekst;
		this.hpTekst = hpTekst;
		firePropertyChange(HP_TEKST, old, hpTekst);
	}

	/**
	 * Kontrakten inspeksjonen gjelder
	 * 
	 * @param id
	 */
	public void setKontraktId(Long id) {
		Long old = this.kontraktId;
		kontraktId = id;
		firePropertyChange(KONTRAKT_ID, old, kontraktId);
	}

	/**
	 * @param kvInspisert
	 *            the kvInspisert to set
	 */
	public void setKvInspisert(Double kvInspisert) {
		Double old = this.kvInspisert;
		this.kvInspisert = kvInspisert;
		firePropertyChange(KV_INSPISERT, old, kvInspisert);
	}

	/**
	 * Hvem utførte inspeksjonen
	 * 
	 * @param av
	 */
	public void setOpprettetAv(String av) {
		String old = this.opprettetAv;
		opprettetAv = av;
		firePropertyChange(OPPRETTET_AV, old, opprettetAv);
	}

	/**
	 * Datoen for funnet
	 * 
	 * @param dato
	 */
	public void setOpprettetDato(Date dato) {
		Date old = this.opprettetDato;
		this.opprettetDato = dato;
		firePropertyChange(OPPRETTET_DATO, old, opprettetDato);
	}

	public void setPdaDfuIdent(Long pdaDfuIdent) {
		Long old = this.pdaDfuIdent;
		this.pdaDfuIdent = pdaDfuIdent;
		firePropertyChange(PDA_DFU_IDENT, old, pdaDfuIdent);
	}

	public void setPdaDfuNavn(String pdaDfuNavn) {
		String old = this.pdaDfuNavn;
		this.pdaDfuNavn = pdaDfuNavn;
		firePropertyChange(PDA_DFU_NAVN, old, pdaDfuNavn);
	}

	/**
	 * Tilknyttede prosesser
	 * 
	 * @param prosesser
	 */
	public void setProsesser(List<InspeksjonSokProsess> prosesser) {
		log.trace("setProsesser({})", prosesser);
		List<InspeksjonSokProsess> old = this.prosesser;
		this.prosesser = prosesser;
		firePropertyChange(PROSESSER, old, prosesser);
	}
	
	/**
	 * @param prosesserTekst
	 *            the prosesserTekst to set
	 */
	public void setProsesserTekst(String prosesserTekst) {
		String old = this.prosesserTekst;
		this.prosesserTekst = prosesserTekst;
		firePropertyChange(PROSESSER_TEKST, old, prosesserTekst);
	}
	
	public void setRefnummer(Long refnummer){
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}
	/**
	 * @param rvInspisert
	 *            the rvInspisert to set
	 */
	public void setRvInspisert(Double rvInspisert) {
		Double old = this.rvInspisert;
		this.rvInspisert = rvInspisert;
		firePropertyChange(RV_INSPISERT, old, rvInspisert);
	}

	public void setSlettetAv(String slettetAv) {
		String old = this.slettetAv;
		this.slettetAv = slettetAv;
		firePropertyChange(SLETTET_AV, old, slettetAv);
	}

	public void setSlettetDato(Date slettetDato) {
		Date old = this.slettetDato;
		this.slettetDato = slettetDato;
		firePropertyChange(SLETTET_DATO, old, slettetDato);
	}

	/**
	 * Produksjon
	 * 
	 * @param strekninger
	 */
	public void setStrekninger(List<ProdstrekningSok> strekninger) {
		log.trace("setStrekninger({})", strekninger);
		List<ProdstrekningSok> old = this.strekninger;
		this.strekninger = strekninger;
		firePropertyChange(STREKNINGER, old, strekninger);
	}

	/**
	 * @param temperatur
	 *            the temperatur to set
	 */
	public void setTemperatur(String temperatur) {
		String old = this.temperatur;
		this.temperatur = temperatur;
		firePropertyChange(TEMPERATUR, old, temperatur);
	}

	/**
	 * Temperatur intervall
	 * 
	 * @param temperaturId
	 */
	public void setTemperaturId(Long temperaturId) {
		log.trace("setTemperaturId({})", temperaturId);
		Long old = this.temperaturId;
		this.temperaturId = temperaturId;
		firePropertyChange(TEMPERATUR_ID, old, temperaturId);
	}

	/**
	 * Når gjelder inspeksjonen til
	 * 
	 * @param d
	 */
	public void setTil(Date d) {
		Date old = this.til;
		til = d;
		firePropertyChange(TIL, old, til);
	}

	/**
	 * @param vaer
	 *            the vaer to set
	 */
	public void setVaer(String vaer) {
		String old = this.vaer;
		this.vaer = vaer;
		firePropertyChange(VAER, old, vaer);
	}

	/**
	 * Vær
	 * 
	 * @param vaerId
	 */
	public void setVaerId(Long vaerId) {
		log.trace("setVaerId({})", vaerId);
		Long old = this.vaerId;
		this.vaerId = vaerId;
		firePropertyChange(VAER_ID, old, vaerId);
	}

	/**
	 * @param vind
	 *            the vind to set
	 */
	public void setVind(String vind) {
		String old = this.vind;
		this.vind = vind;
		firePropertyChange(VIND, old, vind);
	}

	/**
	 * Vind
	 * 
	 * @param vindId
	 */
	public void setVindId(Long vindId) {
		log.trace("setVindId({})", vindId);
		Long old = this.vindId;
		this.vindId = vindId;
		firePropertyChange(VIND_ID, old, vindId);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(REFNUMMER, refnummer).append(ID_NAME, guid).append(KONTRAKT_ID, kontraktId).append(OPPRETTET_DATO,
				opprettetDato).append(OPPRETTET_AV, opprettetAv).append(FRA, fra).append(TIL, til).append(
				ETTERSLEP_FLAGG, etterslepFlagg).append(ANTALL_AVVIK,
				antallAvvik).append(PDA_DFU_IDENT, pdaDfuIdent).append(PDA_DFU_NAVN, pdaDfuNavn).toString();
	}
}
