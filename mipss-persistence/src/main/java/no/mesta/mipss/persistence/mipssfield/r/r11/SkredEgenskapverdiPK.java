package no.mesta.mipss.persistence.mipssfield.r.r11;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
/**
 * PK-klasse for @see SkredEgenskapverdi
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class SkredEgenskapverdiPK implements Serializable{
	
	public Long verdiId;
	public String skredGuid;
	
	public SkredEgenskapverdiPK() {
	}

	/**
	 * Konstruktør
	 * 
	 * @param verdiId
	 * @param skredGuid
	 */
	public SkredEgenskapverdiPK(Long verdiId, String skredGuid) {
		this.verdiId = verdiId;
		this.skredGuid = skredGuid;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof SkredEgenskapverdiPK)) {
			return false;
		}

		SkredEgenskapverdiPK other = (SkredEgenskapverdiPK) o;

		return new EqualsBuilder().append(skredGuid, other.getSkredGuid())
								  .append(verdiId, other.getVerdiId()).isEquals();
	}

	public Long getVerdiId() {
		return verdiId;
	}

	public String getSkredGuid() {
		return skredGuid;
	}

	public void setVerdiId(Long verdiId) {
		this.verdiId = verdiId;
	}

	public void setSkredGuid(String skredGuid) {
		this.skredGuid = skredGuid;
	}
	
	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(skredGuid).append(verdiId).toHashCode();
	}
}
