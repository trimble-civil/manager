package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Nedbor
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Nedbor.QUERY_FIND_ALL, query = "select o from Nedbor o")
public class Nedbor implements Serializable, IRenderableMipssEntity {
    public static final String QUERY_FIND_ALL = "Nedbor.findAll";
    private Long id;
    private String navn;
    private Long nummer;

    public Nedbor() {
    }

    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public Long getNummer() {
        return nummer;
    }

    public void setNummer(Long nummer) {
        this.nummer = nummer;
    }

    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("nummer", nummer).
            append("navn", navn).
            toString();
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Fore)) {return false;}
        
        Fore other = (Fore) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(919,921).append(id).toHashCode();
    }

	public String getTextForGUI() {
		return getNavn();
	}
}
