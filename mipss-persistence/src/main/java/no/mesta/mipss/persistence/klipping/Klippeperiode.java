package no.mesta.mipss.persistence.klipping;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IkkeoverlappendePeriode;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.stroing.Stroperiode;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Klippeperiode.FIND_ALL, query = "select o from Klippeperiode o"),
	@NamedQuery(name = Klippeperiode.FIND_BY_KJORETOY, query = "select o from Klippeperiode o where o.kjoretoyId = :id")
})
@IdClass(KlippeperiodePK.class)
public class Klippeperiode extends MipssEntityBean<Klippeperiode> implements IkkeoverlappendePeriode{
	public static final String FIND_ALL = "Klippeperiode.findAll";
	public static final String FIND_BY_KJORETOY = "Klippeperiode.findByKjoretoy";
	@Id
	@Column(name = "KJORETOY_ID", nullable = false, insertable = false, updatable = false)
	private Long kjoretoyId;
	@Id
	@Column(name = "FRA_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fraDato;
	@Column(name = "TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tilDato;
	@Column(name = "KLIPPEBREDDE_CM", nullable = false)
	private Long klippebreddeCm;
	@Column(name = "OPPRETTET_AV")
	private String opprettetAv;
	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name = "ENDRET_AV")
	private String endretAv;
	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;
	@ManyToOne
	@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID", nullable = false)
	private Kjoretoy kjoretoy;
	/**
	 * @return the kjoretoyId
	 */
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	/**
	 * @param kjoretoyId the kjoretoyId to set
	 */
	public void setKjoretoyId(Long kjoretoyId) {
		Long old = this.kjoretoyId;
		this.kjoretoyId = kjoretoyId;
		firePropertyChange("kjoretoyId", old, kjoretoyId);
	}
	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}
	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		Date old = this.fraDato;
		this.fraDato = fraDato;
		firePropertyChange("fraDato", old, fraDato);
	}
	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}
	/**
	 * @param tilDato the tilDato to set
	 */
	public void setTilDato(Date tilDato) {
		Date old = this.tilDato;
		this.tilDato = tilDato;
		firePropertyChange("tilDato", old, tilDato);
	}
	/**
	 * @return the klippebreddeCm
	 */
	public Long getKlippebreddeCm() {
		return klippebreddeCm;
	}
	/**
	 * @param klippebreddeCm the klippebreddeCm to set
	 */
	public void setKlippebreddeCm(Long klippebreddeCm) {
		Long old = this.klippebreddeCm;
		this.klippebreddeCm = klippebreddeCm;
		firePropertyChange("klippebreddeCm", old, klippebreddeCm);
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	/**
	 * @return the endretAv
	 */
	public String getEndretAv() {
		return endretAv;
	}
	/**
	 * @param endretAv the endretAv to set
	 */
	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}
	/**
	 * @return the endretDato
	 */
	public Date getEndretDato() {
		return endretDato;
	}
	/**
	 * @param endretDato the endretDato to set
	 */
	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}
	/**
	 * @return the kjoretoy
	 */
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}
	/**
	 * @param kjoretoy the kjoretoy to set
	 */
	public void setKjoretoy(Kjoretoy kjoretoy) {
		Kjoretoy old = this.kjoretoy;
		this.kjoretoy = kjoretoy;
		
		if(kjoretoy != null) {
			setKjoretoyId(kjoretoy.getId());
		} else {
			setKjoretoyId(null);
		}
		
		firePropertyChange("kjoretoy", old, kjoretoy);
	}
	
}
