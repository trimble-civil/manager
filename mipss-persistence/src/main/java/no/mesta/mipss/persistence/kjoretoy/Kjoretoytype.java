package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Kjoretoytype.FIND_ALL, query = "select o from Kjoretoytype o order by o.navn"),
	@NamedQuery(name = Kjoretoytype.FIND_VALID, query = "select t from Kjoretoytype t where t.valgbarFlagg = 1 order by t.navn")
})

public class Kjoretoytype extends MipssEntityBean<Kjoretoytype> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kjoretoytype.findAll";
	public static final String FIND_VALID = "Kjoretoytype.findValid";
    private String fritekst;
    private String navn;
    private Boolean valgbarFlagg;
    private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();

    public Kjoretoytype() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }

    @OneToMany(mappedBy = "kjoretoytype")
    public List<Kjoretoy> getKjoretoyList() {
        return kjoretoyList;
    }

    public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
        this.kjoretoyList = kjoretoyList;
    }

    public Kjoretoy addKjoretoy(Kjoretoy kjoretoy) {
        getKjoretoyList().add(kjoretoy);
        kjoretoy.setKjoretoytype(this);
        return kjoretoy;
    }

    public Kjoretoy removeKjoretoy(Kjoretoy kjoretoy) {
        getKjoretoyList().remove(kjoretoy);
        kjoretoy.setKjoretoytype(null);
        return kjoretoy;
    }
    
    // de neste metodene er h�ndkodet
     /**
      * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         return new ToStringBuilder(this).
             append("navm", getNavn()).
             append("valgbar", getValgbarFlagg()).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante n�klene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof Kjoretoytype)) {
             return false;
         }
         Kjoretoytype other = (Kjoretoytype) o;
         return new EqualsBuilder().
             append(getNavn(), other.getNavn()).
             isEquals();
     }

     /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
         return new HashCodeBuilder(23,37).
             append(getNavn()).
             toHashCode();
     }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Kjoretoytype other = (Kjoretoytype) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
