package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class RolleTilgangPK implements Serializable {
    private Long modulId;
    private Long rolleId;

    public RolleTilgangPK() {
    }

    public RolleTilgangPK(Long modulId, Long rolleId) {
        this.modulId = modulId;
        this.rolleId = rolleId;
    }

    public void setModulId(Long modulId) {
        this.modulId = modulId;
    }

    public Long getModulId() {
        return modulId;
    }

    public void setRolleId(Long rolleId) {
        this.rolleId = rolleId;
    }

    public Long getRolleId() {
        return rolleId;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;};
        if(!(o instanceof RolleTilgangPK)) { return false;}
        if(o == this) { return true;}
        
        RolleTilgangPK other = (RolleTilgangPK) o;
        
        return new EqualsBuilder().
            append(rolleId, other.getRolleId()).
            append(modulId, other.getModulId()).
            isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(77,75).
            append(rolleId).
            append(modulId).
            toHashCode();
    }
}
