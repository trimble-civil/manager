package no.mesta.mipss.persistence.stroing;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@SequenceGenerator(name = "stroproduktgruppeIdSeq", sequenceName = "STROPRODUKTGRUPPE_ID_SEQ", initialValue = 1, allocationSize = 1)

@Entity
@NamedQuery(name = "Stroproduktgruppe.findAll", query = "select o from Stroproduktgruppe o")
public class Stroproduktgruppe extends MipssEntityBean<Stroproduktgruppe> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Stroproduktgruppe.findAll";
	private Long id;
	private String navn;
	private String beskrivelse;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stroproduktgruppeIdSeq")
	@Column(nullable = false)	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	
	public String getBeskrivelse() {
		return beskrivelse;
	}
	
	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

	public String getTextForGUI() {
		return navn;
	}
}