package no.mesta.mipss.persistence.mipssfield.r.r11;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Sak;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Skred
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

@SuppressWarnings("serial")
@Entity(name = Skred.BEAN_NAME)
@Table(name="SKRED_XV")

@NamedQueries( { 
	@NamedQuery(name = Skred.QUERY_FIND_ALL, query = "SELECT h FROM Skred h"),
	@NamedQuery(name = Skred.QUERY_FIND_ENTITY_INFO_BY_SAK,	query = "SELECT new no.mesta.mipss.persistence.mipssfield.FeltEntityInfo(s.guid, s.refnummer, s.slettetDato) FROM Skred s where s.sakGuid=:sakGuid"),
	@NamedQuery(name = Skred.QUERY_HENT_MED_REFNUMMER, query = "SELECT h FROM Skred h where h.refnummer=:refnummer"),
	@NamedQuery(name = Skred.QUERY_HENT_MED_ELRAPP_ID, query = "SELECT h FROM Skred h where h.elrappDokumentIdent=:elrappId and h.sak.kontraktId=:driftkontraktId")

    })
@NamedNativeQueries( { @NamedNativeQuery(name = Skred.QUERY_GET_BILDER, query = 
		  "SELECT b.GUID, b.FILNAVN, b.BILDE_LOB, b.BESKRIVELSE, b.HOYDE, b.SMAABILDE_LOB, b.BREDDE, b.OPPRETTET_DATO, "
		+ "b.ENDRET_AV, b.OPPRETTET_AV, b.ENDRET_DATO FROM SKRED_BILDE sb JOIN Bilde b ON sb.BILDE_GUID = b.GUID WHERE sb.SKRED_GUID = ?1", 
		resultSetMapping = "BildeResults"),
		
		@NamedNativeQuery(name = Skred.QUERY_GET_BILDER_UTENRELASJONER, query = 
			"SELECT " + "b.GUID, b.FILNAVN, b.BILDE_LOB,  b.BESKRIVELSE,  b.HOYDE,  b.SMAABILDE_LOB,  b.BREDDE, "
			+ "b.OPPRETTET_DATO,  b.ENDRET_AV,  b.OPPRETTET_AV,  b.ENDRET_DATO  FROM "
			+ "SKRED_BILDE sb  JOIN Bilde b ON sb.BILDE_GUID = b.GUID  WHERE  sb.SKRED_GUID = ?1 " 
			+ "and not exists (select * from skade_bilde where bilde_guid=b.guid) "
			+ "and not exists (select * from hendelse_bilde where bilde_guid=b.guid) "
			+ "and not exists (select * from avvik_bilde where bilde_guid=b.guid) "
			+ " ", resultSetMapping = "BildeResults") 
			
})

@SequenceGenerator(name = Skred.REFNUMMER_ID_SEQ, sequenceName = "SKRED_REFNUMMER_SEQ", initialValue = 1, allocationSize = 1)

public class Skred extends MipssEntityBean<Skred> implements Serializable, IRenderableMipssEntity, MipssFieldDetailItem{

	private static final Logger log = LoggerFactory.getLogger(Skred.class);
	
	public static final String BEAN_NAME = "Skred";
	public static final String QUERY_FIND_ALL = "Skred.findAll";
	public static final String QUERY_FIND_ENTITY_INFO_BY_SAK = "Skred.findGuidBySak";
	public static final String QUERY_GET_BILDER = "Skred.getBilder";
	public static final String QUERY_GET_BILDER_UTENRELASJONER = "Skred.getBilderUtenrelasjoner";
	public static final String QUERY_HENT_MED_REFNUMMER = "Skred.hentMedRefummer";
	public static final String QUERY_HENT_MED_ELRAPP_ID = "Skred.hentMedElrappId";
	public static final String REFNUMMER_ID_SEQ = "skredRefnummerSeq";
	
	@Column(name="AAPNET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date aapnetDato;
	
    @Column(name="ANSLAG_TOTAL_SKREDSTORRELSE")
	private Integer anslagTotalSkredstorrelse;
    
    @ManyToMany(cascade={CascadeType.REFRESH, CascadeType.PERSIST})
	@JoinTable(	name = "SKRED_BILDE_XV", 
				joinColumns = 		 { @JoinColumn(name = "SKRED_GUID", referencedColumnName = "GUID") }, 		
				inverseJoinColumns = { @JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID") })
	private List<Bilde> bilder = new ArrayList<Bilde>();
	
    @Column(name="ELRAPP_DOKUMENT_IDENT")
	private Long elrappDokumentIdent;
		
    @Column(name="ELRAPP_VERSJON")
	private Long elrappVersjon;
    
	@Column(name="ENDRET_AV")
	private String endretAv;
	
	@Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endretDato;
	
	@Column(name="GJELDER_GS_FLAGG")
	private Boolean gjelderGS;
	
	@Id
	@Column(nullable = false)
	private String guid;
	
	@Column(name="INGEN_NEDBOR_FLAGG")
	private Boolean ingenNedbor;
	
	@Column(name="KOMMENTAR")
    private String kommentar;
	
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name="RAPPORTERT_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date rapportertDato;
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Skred.REFNUMMER_ID_SEQ)
	@Column(name="refnummer")
	private Long refnummer;
	@OneToOne
	@JoinColumn(name = "SAK_GUID", referencedColumnName = "GUID")
	private Sak sak;
	@Column(name = "SAK_GUID", nullable = true, insertable = false, updatable = false)
	private String sakGuid;
	@Column(name="SKJEMA_EMNE")
	private String skjemaEmne;
	@Column(name="SKRED_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date skredDato;
	@OneToMany(mappedBy = "skred", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<SkredEgenskapverdi> skredEgenskapList= new ArrayList<SkredEgenskapverdi>();
	@Column(name="SLETTET_AV")
    private String slettetAv;
	@Column(name="SLETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date slettetDato;
	@Column(name="STENGT_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date stengtDato;
	
	
	@Column(name="STENGT_PGA_SKREDFARE_FLAGG")
	private Boolean stengtPgaSkredfare;
	
	@Column(name="TEMPERATUR")
	private Double temperatur;
	
	@OneToOne (mappedBy="skred", fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval=true)
	private VaerFraVaerstasjon vaerFraVaerstasjon = new VaerFraVaerstasjon();

	public void addBilde(Bilde b) {
		List<Bilde> old = new ArrayList<Bilde>(getBilder());
		getBilder().add(b);
		firePropertyChange("bilder", old, getBilder());
	}
	
	public Date getAapnetDato() {
		return aapnetDato;
	}

	public Integer getAnslagTotalSkredstorrelse(){
		return anslagTotalSkredstorrelse;
	}

	public List<Bilde> getBilder() {
		return bilder;
	}

	public Long getElrappDokumentIdent() {
		return elrappDokumentIdent;
	}
	
	public Long getElrappVersjon() {
		return elrappVersjon;
	}
	public String getEndretAv() {
		return endretAv;
	}
	
	public Date getEndretDato() {
		return endretDato;
	}

	
	public Boolean getGjelderGS() {
		return gjelderGS;
	}

	public String getGuid() {
		return guid;
	}
	
	public Boolean getIngenNedbor() {
		return ingenNedbor;
	}
	public String getKommentar() {
		return kommentar;
	}
	
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	
	public Date getRapportertDato() {
		return rapportertDato;
	}
	
	public Long getRefnummer() {
		return refnummer;
	}
	
	public Sak getSak() {
		return sak;
	}

	public String getSakGuid() {
		return sakGuid;
	}
	public String getSkjemaEmne() {
		return skjemaEmne;
	}

	public Date getSkredDato() {
		return skredDato;
	}
	
	public List<SkredEgenskapverdi> getSkredEgenskapList() {
		return skredEgenskapList;
	}
	public void addSkredEgenskap(SkredEgenskapverdi egenskap){
		List<SkredEgenskapverdi> old = new ArrayList<SkredEgenskapverdi>(getSkredEgenskapList());
		getSkredEgenskapList().add(egenskap);
		firePropertyChange("skredEgenskapList", old, getSkredEgenskapList());
	}
	
	public void removeSkredEgenskap(SkredEgenskapverdi egenskap){
		List<SkredEgenskapverdi> old = new ArrayList<SkredEgenskapverdi>(getSkredEgenskapList());
		getSkredEgenskapList().remove(egenskap);
		firePropertyChange("skredEgenskapList", old, getSkredEgenskapList());
	}
	public String getSlettetAv() {
		return slettetAv;
	}

	public Date getSlettetDato() {
		return slettetDato;
	}
	@Transient
	public Integer getAntallBilder(){
		if (bilder == null) {
			return Integer.valueOf(0);
		} else {
			return Integer.valueOf(bilder.size());
		}
	}
	public Date getStengtDato() {
		return stengtDato;
	}

	public Boolean getStengtPgaSkredfare() {
		return stengtPgaSkredfare;
	}

	public Double getTemperatur() {
		return temperatur;
	}

	@Transient
	public Skred getThisForReport(){
		return this;
	}
	
	public void removeBilde(Bilde b) {
		List<Bilde> old = new ArrayList<Bilde>(getBilder());
		log.trace("removeBilde", b);
		getBilder().remove(b);
		firePropertyChange("bilder", old, getBilder());
	}
    public VaerFraVaerstasjon getVaerFraVaerstasjon() {
		return vaerFraVaerstasjon;
	}

	public void setAapnetDato(Date aapnetDato) {
		Date old = this.aapnetDato;
		this.aapnetDato = aapnetDato;
		firePropertyChange("aapnetDato", old, aapnetDato);
	}

	public void setAnslagTotalSkredstorrelse(Integer anslagTotalSkredstorrelse){
		Integer old = this.anslagTotalSkredstorrelse;
		this.anslagTotalSkredstorrelse = anslagTotalSkredstorrelse;
		firePropertyChange("anslagTotalSkredstorrelse", old, anslagTotalSkredstorrelse);
	}

	public void setBilder(List<Bilde> bilder) {
		List<Bilde> old = this.bilder;
		this.bilder = bilder;
		firePropertyChange("bilder", old, bilder);
	}

	public void setElrappDokumentIdent(Long elrappDokumentIdent) {
		Long old = elrappDokumentIdent;
		this.elrappDokumentIdent = elrappDokumentIdent;
		firePropertyChange("elrappDokumentIdent", old, elrappDokumentIdent);
	}

	public void setElrappVersjon(Long elrappVersjon) {
		Long old = this.elrappVersjon;
		this.elrappVersjon = elrappVersjon;
		firePropertyChange("elrappVersjon", old, elrappVersjon);
	}

	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}

	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}
	
	public void setGjelderGS(Boolean gjelderGS) {
		Boolean old = this.gjelderGS;
		this.gjelderGS = gjelderGS;
		firePropertyChange("gjelderGS", old, gjelderGS);
	}


	public void setGuid(String guid) {
		String old = this.guid;
		this.guid = guid;
		firePropertyChange("guid", old, guid);
	}

	public void setIngenNedbor(Boolean ingenNedbor) {
		Boolean old = this.ingenNedbor;
		this.ingenNedbor = ingenNedbor;
		firePropertyChange("ingenNedbor", old, ingenNedbor);
	}

	public void setKommentar(String kommentar) {
		String old = this.kommentar;
		this.kommentar = kommentar;
		firePropertyChange("kommentar", old, kommentar);
	}

	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}
	
	public void setRapportertDato(Date rapportertDato) {
		Date old = this.rapportertDato;
		this.rapportertDato = rapportertDato;
		firePropertyChange("rapportertDato", old, rapportertDato);
	}
	public void setRefnummer(Long refnummer) {
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}

	public void setSak(Sak sak) {
		Sak old = this.sak;
		this.sak = sak;
		if (sak!=null){
			setSakGuid(sak.getGuid());
		}else{
			setSakGuid(null);
		}
		firePropertyChange("sak", old, sak);
			
	}
	public void setSakGuid(String sakGuid) {
		String old = this.sakGuid;
		this.sakGuid = sakGuid;
		firePropertyChange("sakGuid", old, sakGuid);
	}

	public void setSkjemaEmne(String skjemaEmne) {
		String old = this.skjemaEmne;
		this.skjemaEmne = skjemaEmne;
		firePropertyChange("skjemaEmne", old, skjemaEmne);
	}
	public void setSkredDato(Date skredDato) {
		Date old = this.skredDato;
		this.skredDato = skredDato;
		firePropertyChange("skredDato", old, skredDato);
	}

	public void setSkredEgenskapList(List<SkredEgenskapverdi> skredEgenskapList) {
		this.skredEgenskapList = skredEgenskapList;
	}
	public void setSlettetAv(String slettetAv) {
		this.slettetAv = slettetAv;
	}

	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}
	public void setStengtDato(Date stengtDato) {
		Date old = this.stengtDato;
		this.stengtDato = stengtDato;
		firePropertyChange("stengtDato", old, stengtDato);
	}

	public void setStengtPgaSkredfare(Boolean stengtPgaSkredfare) {
		Boolean old = this.stengtPgaSkredfare;
		this.stengtPgaSkredfare = stengtPgaSkredfare;
		firePropertyChange("stengtPgaSkredfare", old, stengtPgaSkredfare);
	}

	public void setTemperatur(Double temperatur) {
		Double old = this.temperatur;
		this.temperatur = temperatur;
		firePropertyChange("temperatur", old, temperatur);
	}

	public void setVaerFraVaerstasjon(VaerFraVaerstasjon vaerFraVaerstasjon) {
		VaerFraVaerstasjon old = this.vaerFraVaerstasjon;
		this.vaerFraVaerstasjon = vaerFraVaerstasjon;
		firePropertyChange("vaerFraVaerstasjon", old, vaerFraVaerstasjon);
	}
}
