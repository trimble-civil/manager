package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Holder forholdet mellom brukere og selskaper.
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "SELSKAP_BRUKER")
@IdClass(SelskapBrukerPK.class)
@NamedQueries({
    @NamedQuery(name = SelskapBruker.QUERY_SELSKAPBRUKER_GET_FOR_USER, query = "SELECT sb FROM SelskapBruker sb where LOWER(sb.signatur) = LOWER(:signatur)"),
    @NamedQuery(name = SelskapBruker.QUERY_SELSKAPBRUKER_DELETE_FOR_USER, query = "DELETE FROM SelskapBruker sb where LOWER(sb.signatur) = LOWER(:signatur)")
    })
public class SelskapBruker implements Serializable {
    public static final String QUERY_SELSKAPBRUKER_GET_FOR_USER = "SelskapBruker.getForUser";
    public static final String QUERY_SELSKAPBRUKER_DELETE_FOR_USER = "SelskapBruker.deleteForUser";
    
    private String opprettetAv;
    private Date opprettetDato;
    private String selskapskode;
    private String signatur;
    private Selskap selskap;
    private Bruker bruker;

    public SelskapBruker() {
    }

    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @Id
    @Column(nullable = false, insertable = false, 
        updatable = false)
    public String getSelskapskode() {
        return selskapskode;
    }

    public void setSelskapskode(String selskapskode) {
        this.selskapskode = selskapskode;
    }

    @Id
    @Column(nullable = false, insertable = false, updatable = false)
    public String getSignatur() {
        return signatur;
    }

    public void setSignatur(String signatur) {
        this.signatur = signatur;
    }

    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @JoinColumn(name = "SELSKAPSKODE", referencedColumnName = "SELSKAPSKODE")
    public Selskap getSelskap() {
        return selskap;
    }

    public void setSelskap(Selskap selskap) {
        this.selskap = selskap;
        
        if(selskap != null) {
            setSelskapskode(selskap.getSelskapskode());
        } else {
            setSelskapskode(null);
        }
    }

    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @JoinColumn(name = "SIGNATUR", referencedColumnName = "SIGNATUR")
    public Bruker getBruker() {
        return bruker;
    }

    public void setBruker(Bruker bruker) {
        this.bruker = bruker;
        
        if(bruker != null) {
            setSignatur(bruker.getSignatur());
        } else {
            setSignatur(null);
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof SelskapBruker)) { return false;}
        if(o == this) {return true;}
        
        SelskapBruker other = (SelskapBruker) o;
        
        return new EqualsBuilder().
            append(signatur, other.getSignatur()).
            append(selskapskode, other.getSelskapskode()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(101,19).
            append(signatur).
            append(selskapskode).
            toHashCode();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("signatur", signatur).
            append("selskapskode", selskapskode).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            toString();
    }
}
