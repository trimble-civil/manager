package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Holder en knytting mellom Applikasjon og Modul
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = AppMod.QUERY_FIND_ALL, query = "SELECT o FROM AppMod o")
})
@Table(name = "APP_MOD")
@IdClass(AppModPK.class)
public class AppMod implements Serializable {
    public static final String QUERY_FIND_ALL = "AppMod.findAll";
    
    private Long appId;
    private Long modulversjonId;
    private String opprettetAv;
    private Date opprettetDato;
    private Modulversjon modulversjon;
    private Applikasjon applikasjon;

    /**
     * Konstrukt�r
     */
    public AppMod() {
    }

    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.maven.mipssdeployer.persistence.AppModPK
     * @return
     */
    @Id
    @Column(name="APP_ID", nullable = false, insertable = false, updatable = false)
    public Long getAppId() {
        return appId;
    }
    
    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.maven.mipssdeployer.persistence.AppModPK
     * @param appId
     */
    public void setAppId(Long appId) {
        this.appId = appId;
    }

    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.maven.mipssdeployer.persistence.AppModPK
     * @return
     */
    @Id
    @Column(name="MODULVERSJON_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getModulversjonId() {
        return modulversjonId;
    }

    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.maven.mipssdeployer.persistence.AppModPK
     * @param modulversjonId
     */
    public void setModulversjonId(Long modulversjonId) {
        this.modulversjonId = modulversjonId;
    }

    /**
     * Hvem opprettet knyttingen
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Hvem opprettet knyttingen
     * 
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    /**
     * N�r ble knyttingen opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r ble knyttingen opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.mipss.persistence.applikasjon.AppModPK
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "MODULVERSJON_ID", referencedColumnName = "ID")
    public Modulversjon getModulversjon() {
        return modulversjon;
    }

    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.mipss.persistence.applikasjon.AppModPK
     * @param modulversjon
     */
    public void setModulversjon(Modulversjon modulversjon) {
        this.modulversjon = modulversjon;
        
        if(modulversjon != null) {
            setModulversjonId(modulversjon.getId());
        } else {
            setModulversjonId(null);
        }
    }

    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.mipss.persistence.applikasjon.AppModPK
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "APP_ID", referencedColumnName = "ID")
    public Applikasjon getApplikasjon() {
        return applikasjon;
    }
    
    /**
     * Del av den kompositte n�kkelen AppModPK
     * 
     * @see no.mesta.mipss.persistence.applikasjon.AppModPK
     * @param applikasjon
     */
    public void setApplikasjon(Applikasjon applikasjon) {
        this.applikasjon = applikasjon;
        
        if(applikasjon != null ) {
            setAppId(applikasjon.getId());
        } else {
            setAppId(null);
        }
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof AppMod)) { return false;}
        if(o == this) { return true;}
        
        AppMod other = (AppMod) o;
        return new EqualsBuilder().
            append(appId, other.getAppId()).
            append(modulversjonId, other.getModulversjonId()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(23, 977).
            append(appId).
            append(modulversjonId).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("appId", appId).
            append("modulversjonId", modulversjonId).
            toString();
    }
}
