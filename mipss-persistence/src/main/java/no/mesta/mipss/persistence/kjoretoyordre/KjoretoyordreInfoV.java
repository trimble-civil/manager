package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery( name = KjoretoyordreInfoV.QUERY_FIND_ALL, query = "select o from KjoretoyordreInfoV o"),
	@NamedQuery( name = KjoretoyordreInfoV.QUERY_FIND_BY_ID, query = "select distinct o from KjoretoyordreInfoV o where o.id = :id")})
@Table(name = "KJORETOYORDRE_INFO_V")
public class KjoretoyordreInfoV implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "KjoretoyordreInfoV.findAll";
	public static final String QUERY_FIND_BY_ID = "KjoretoyordreInfoV.findById";

	private Long id;
	private String typeNavn;
	private String signatur;
	private String fritekst;
	private String opprettetAv;
	private Date opprettetDato;
	private String endretAv;
	private Date endretDato;
	private String selskapskode;
	private String prosjektNr;
	private Long kjoretoyId;
	private String maskinnummer;
	private String regnr;
	private String eksterntNavn;
	private String kjoretoyTypeNavn;
	private String modell;
	private String navn;
	private String statusIdent;
	private String statusNavn;
	private String statusBeskrivelse;
	private String statusOpprettetAv;
	private Date statusOpprettetDato;
	private String serienummer;
	
	public KjoretoyordreInfoV() {
	}
	
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="TYPE_NAVN")
	public String getTypeNavn() {
		return typeNavn;
	}

	public void setTypeNavn(String typeNavn) {
		this.typeNavn = typeNavn;
	}

	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}

	@Column(name="OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="OPPRETTET_DATO")
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@Column(name="ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="ENDRET_DATO")
	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	public String getSelskapskode() {
		return selskapskode;
	}

	public void setSelskapskode(String selskapskode) {
		this.selskapskode = selskapskode;
	}

	@Column(name="PROSJEKT_NR")
	public String getProsjektNr() {
		return prosjektNr;
	}

	public void setProsjektNr(String prosjektNr) {
		this.prosjektNr = prosjektNr;
	}

	@Column(name="KJORETOY_ID")
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public String getMaskinnummer() {
		return maskinnummer;
	}

	public void setMaskinnummer(String maskinnummer) {
		this.maskinnummer = maskinnummer;
	}

	public String getRegnr() {
		return regnr;
	}

	public void setRegnr(String regnr) {
		this.regnr = regnr;
	}

	@Column(name="EKSTERNT_NAVN")
	public String getEksterntNavn() {
		return eksterntNavn;
	}

	public void setEksterntNavn(String eksterntNavn) {
		this.eksterntNavn = eksterntNavn;
	}

	@Column(name="KJORETOY_TYPE_NAVN")
	public String getKjoretoyTypeNavn() {
		return kjoretoyTypeNavn;
	}

	public void setKjoretoyTypeNavn(String kjoretoyTypeNavn) {
		this.kjoretoyTypeNavn = kjoretoyTypeNavn;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	@Column(name="STATUS_IDENT")
	public String getStatusIdent() {
		return statusIdent;
	}
	
	public void setStatusIdent(String statusIdent) {
		this.statusIdent = statusIdent;
	}

	@Column(name="STATUS_NAVN")
	public String getStatusNavn() {
		return statusNavn;
	}

	public void setStatusNavn(String statusNavn) {
		this.statusNavn = statusNavn;
	}

	@Column(name="STATUS_BESKRIVELSE")
	public String getStatusBeskrivelse() {
		return statusBeskrivelse;
	}

	public void setStatusBeskrivelse(String statusBeskrivelse) {
		this.statusBeskrivelse = statusBeskrivelse;
	}

	@Column(name="STATUS_OPPRETTET_AV")
	public String getStatusOpprettetAv() {
		return statusOpprettetAv;
	}

	public void setStatusOpprettetAv(String statusOpprettetAv) {
		this.statusOpprettetAv = statusOpprettetAv;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="STATUS_OPPRETTET_DATO")
	public Date getStatusOpprettetDato() {
		return statusOpprettetDato;
	}

	public void setStatusOpprettetDato(Date statusOpprettetDato) {
		this.statusOpprettetDato = statusOpprettetDato;
	}
	
	@Column(name="DFU_SERIENUMMER")
	public String getSerienummer(){
		return serienummer;
	}
	public void setSerienummer(String serienummer){
		this.serienummer = serienummer;
	}

	public String getTextForGUI() {
		return getId() + getTypeNavn();
	}
	
	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(1, 911).append(getId()).toHashCode();
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof KjoretoyordreInfoV)) {
			return false;
		}
		KjoretoyordreInfoV other = (KjoretoyordreInfoV) o;
		return new EqualsBuilder().append(getId(), other.getId()).isEquals();
	}
	
	@Transient
	public String getTypeOgModell() {
		String ret = "";
		
		if(getKjoretoyTypeNavn() != null && getModell() != null) {
			ret = getKjoretoyTypeNavn() + ", " + getModell();
		} else if(getKjoretoyTypeNavn() != null) {
			ret = getKjoretoyTypeNavn();
		} else if(getModell() != null) {
			ret = getModell();
		}
		
		return ret;
	}
}
