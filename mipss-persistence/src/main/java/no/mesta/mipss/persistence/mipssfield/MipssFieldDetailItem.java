package no.mesta.mipss.persistence.mipssfield;

/**
 * Type
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface MipssFieldDetailItem {

	String getGuid();
}
