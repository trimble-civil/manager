package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity(name = Dfubestvarelinjer.BEAN_NAME)
@Table(name = "DFUBESTVARELINJER")
@IdClass(DfubestvarelinjerPK.class)
public class Dfubestvarelinjer extends MipssEntityBean<Dfubestvarelinjer> implements MipssDfuvare {
	public static final String BEAN_NAME = "Dfubestvarelinjer";
	private Long vareId;
	private Long levbestId;
	private Long antall;
	
	private DfulevBestilling dfulevBestilling;
	private Dfuvarekatalog dfuvarekatalog;
	
	public Dfubestvarelinjer() {
	}

	@Id
	@Column(name="VARE_ID", nullable = true, insertable = false, updatable = false)
	public Long getVareId() {
		return vareId;
	}

	public void setVareId(Long vareId) {
		this.vareId = vareId;
	}

	@Id
	@Column(name="LEVBEST_ID", nullable = true, insertable = false, updatable = false)
	public Long getLevbestId() {
		return levbestId;
	}

	public void setLevbestId(Long levbestId) {
		this.levbestId = levbestId;
	}

	public Long getAntall() {
		return antall;
	}

	public void setAntall(Long antall) {
		if(antall != null && antall < 0) {
			throw new IllegalArgumentException("Antall kan ikke være negativ");
		}
		Object oldPris = getTotalPris();
		Object oldValue = this.antall;
		this.antall = antall;
		firePropertyChange("antall", oldValue, antall);
		firePropertyChange("totalPris", oldPris, getTotalPris());
	}
	
	@ManyToOne
	@JoinColumn(name="LEVBEST_ID", referencedColumnName="ID")
	public DfulevBestilling getDfulevBestilling() {
		return dfulevBestilling;
	}

	public void setDfulevBestilling(DfulevBestilling dfulevBestilling) {
		Object oldValue = this.dfulevBestilling;
		this.dfulevBestilling = dfulevBestilling;
		
		if(dfulevBestilling != null) {
			setLevbestId(dfulevBestilling.getId());
		} else {
			setLevbestId(null);
		}
		
		firePropertyChange("dfulevBestilling", oldValue, dfulevBestilling);
	}

	@ManyToOne
	@JoinColumn(name="VARE_ID", referencedColumnName="ID")
	public Dfuvarekatalog getDfuvarekatalog() {
		return dfuvarekatalog;
	}

	public void setDfuvarekatalog(Dfuvarekatalog dfuvarekatalog) {
		Object oldVarenummer = getVarenummer();
		Object oldBeskrivelse = getBeskrivelse();
		Object oldPris = getTotalPris();
		Object oldValue = this.dfuvarekatalog;
		this.dfuvarekatalog = dfuvarekatalog;
		
		if(dfuvarekatalog != null) {
			setVareId(dfuvarekatalog.getId());
		} else {
			setVareId(null);
		}
		
		firePropertyChange("dfuvarekatalog", oldValue, dfuvarekatalog);
		firePropertyChange("varenummer", oldVarenummer, getVarenummer());
		firePropertyChange("beskrivelse", oldBeskrivelse, getBeskrivelse());
		firePropertyChange("totalPris", oldPris, getTotalPris());
	}
	
	@Transient
	public Double getTotalPris() {
		if(dfuvarekatalog != null 
				&& dfuvarekatalog.getPris() != null 
				&& antall != null) {
			return dfuvarekatalog.getPris() * antall;
		} else {
			return 0.0;
		}
	}
	
	@Transient
	public void addToAntall(final Long l) {
		if(antall == null) {
			setAntall(l);
		} else {
			setAntall(getAntall() + l);
		}
	}
	
	@Transient
	public String getVarenummer() {
		return dfuvarekatalog != null ? dfuvarekatalog.getVarenummer() : "";
	}
	
	@Transient
	public String getBeskrivelse() {
		return dfuvarekatalog != null ? dfuvarekatalog.getBeskrivelse() : "";
	}
}
