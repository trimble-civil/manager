package no.mesta.mipss.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
public class BrukerLoggType extends MipssEntityBean<BrukerLoggType>{
	public enum LoggType{
		LOGIN_AUTO,
		LOGIN_MANUELL,
		LOGIN_FEILET,
		LOGG_UT,
		MODUL_START,
		MODUL_FLOAT,
		MODUL_AVSLUTT,
		MODUL_IKKE_TILGANG,
		HENT_RAPPORT,
		HENT_RAPPORT_FEIL,
		BRUKERSTOTTE
	}
	
	private String navn;
	private Boolean aktiv;
	
	@Id
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
	
	@Column(name="AKTIV_FLAGG")
	public Boolean getAktiv() {
		return aktiv;
	}
	public void setAktiv(Boolean aktiv) {
		this.aktiv = aktiv;
	}
}
