package no.mesta.mipss.persistence.veinett;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Entity
@Table(name="veinettlengde_v")
public class VeinettlengdeView {
	private Long veinettId;
	private Double lengdeKm;
	
	public void setLengdeKm(Double lengdeKm) {
		this.lengdeKm = lengdeKm;
	}
	
	@Column(name="lengde_km")
	public Double getLengdeKm() {
		return lengdeKm;
	}
	public void setVeinettId(Long veinettId) {
		this.veinettId = veinettId;
	}
	@Id
	@Column(name="veinett_id")
	public Long getVeinettId() {
		return veinettId;
	}
	
	public int hashCode(){
		return new HashCodeBuilder(2981, 2341).append(veinettId).toHashCode();
	}
	
	public boolean equals(Object o){
		if (o==this)
			return true;
		if (! (o instanceof VeinettlengdeView))
			return false;
		VeinettlengdeView other = (VeinettlengdeView)o;
		return new EqualsBuilder().append(other.getVeinettId(), getVeinettId()).isEquals();
	}
	
}
