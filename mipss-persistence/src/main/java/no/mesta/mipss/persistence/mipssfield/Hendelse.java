package no.mesta.mipss.persistence.mipssfield;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hendelse
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity(name = Hendelse.BEAN_NAME)
@Table(name = "HENDELSE_XV")
@NamedQueries({
        @NamedQuery(name = Hendelse.QUERY_FIND_ENTITY_INFO_BY_SAK,
                query = "SELECT new no.mesta.mipss.persistence.mipssfield.FeltEntityInfo(h.guid, h.refnummer, h.slettetDato) FROM Hendelse h where h.sakGuid=:sakGuid"),

        @NamedQuery(name = Hendelse.QUERY_FIND_ALL, query = "SELECT h FROM Hendelse h"),
        @NamedQuery(name = Hendelse.QUERY_HENT_MED_REFNUMMER, query = "SELECT h FROM Hendelse h where h.refnummer=:refnummer"),
        @NamedQuery(name = Hendelse.QUERY_HENT_MED_ELRAPPID, query = "SELECT h FROM Hendelse h where h.elrappDokumentIdent=:elrappId and h.sak.kontraktId=:driftkontraktId")
})

@NamedNativeQueries({@NamedNativeQuery(name = Hendelse.QUERY_GET_BILDER, query = "SELECT " + "b.GUID, "
        + "b.FILNAVN, " + "b.BILDE_LOB, " + "b.BESKRIVELSE, " + "b.HOYDE, " + "b.SMAABILDE_LOB, " + "b.BREDDE, "
        + "b.OPPRETTET_DATO, " + "b.ENDRET_AV, " + "b.OPPRETTET_AV, " + "b.ENDRET_DATO " + "FROM "
        + "HENDELSE_BILDE_XV hb " + "JOIN Bilde b ON hb.BILDE_GUID = b.GUID " + "WHERE " + "hb.HENDELSE_GUID = ?1", resultSetMapping = "BildeResults"),

        @NamedNativeQuery(name = Hendelse.QUERY_GET_BILDER_UTENRELASJONER, query =
                "SELECT " + "b.GUID, b.FILNAVN, b.BILDE_LOB,  b.BESKRIVELSE,  b.HOYDE,  b.SMAABILDE_LOB,  b.BREDDE, "
                        + "b.OPPRETTET_DATO,  b.ENDRET_AV,  b.OPPRETTET_AV,  b.ENDRET_DATO  FROM "
                        + "HENDELSE_BILDE sb  JOIN Bilde b ON sb.BILDE_GUID = b.GUID  WHERE  sb.HENDELSE_GUID = ?1 "
                        + "and not exists (select * from skade_bilde where bilde_guid=b.guid) "
                        + "and not exists (select * from avvik_bilde where bilde_guid=b.guid) "
                        + "and not exists (select * from skred_bilde where bilde_guid=b.guid) "
                        + " ", resultSetMapping = "BildeResults")
})

@SqlResultSetMappings({
        @SqlResultSetMapping(name = "BildeResults", entities = {
                @EntityResult(entityClass = Bilde.class, fields = {
                        @FieldResult(name = "guid", column = "GUID"), @FieldResult(name = "filnavn", column = "FILNAVN"),
                        @FieldResult(name = "bildeLob", column = "BILDE_LOB"), @FieldResult(name = "tekst", column = "BESKRIVELSE"),
                        @FieldResult(name = "hoyde", column = "HOYDE"), @FieldResult(name = "smaabildeLob", column = "SMAABILDE_LOB"),
                        @FieldResult(name = "nyDato", column = "OPPRETTET_DATO"), @FieldResult(name = "endrer", column = "ENDRET_AV"),
                        @FieldResult(name = "oppretter", column = "OPPRETTET_AV"),
                        @FieldResult(name = "endringsDato", column = "ENDRET_DATO"), @FieldResult(name = "bredde", column = "BREDDE")
                })
        })
})

@SequenceGenerator(name = Hendelse.ID_SEQ, sequenceName = "HENDELSE_REFNUMMER_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class Hendelse extends MipssEntityBean<Hendelse> implements Serializable, IOwnableMipssEntity,
        IRenderableMipssEntity, Comparable<Hendelse>, MipssFieldDetailItem, ProcessRelated {

    private static final Logger log = LoggerFactory.getLogger(Hendelse.class);

    public static final String BEAN_NAME = "Hendelse";
    private static final String EMPTY_STRING = "";

    private static final String FIELD_NAME_AARSAK = "aarsak";
    private static final String FIELD_NAME_AARSAKID = "aarsakId";
    private static final String FIELD_NAME_BESKRIVELSE = "beskrivelse";
    private static final String FIELD_NAME_BILDER = "bilder";
    private static final String FIELD_NAME_DATO = "dato";
    private static final String FIELD_NAME_HENVENDELSEDATO = "henvendelseDato";
    private static final String FIELD_NAME_HENVENDELSEFRA = "henvendelseFra";
    private static final String FIELD_NAME_HENVENDELSEMOTTATTAV = "henvendelseMottattAv";
    private static final String FIELD_NAME_OWNEDMIPSSENTITY = "ownedMipssEntity";
    private static final String FIELD_NAME_SLETTETAV = "slettetAv";
    private static final String FIELD_NAME_SLETTETDATO = "slettetDato";
    private static final String FIELD_NAME_TILTAK = "tiltak";
    private static final String FIELD_NAME_TILTAKSTEKSTER = "tiltakstekster";
    private static final String FIELD_NAME_SKJEMA_EMNE = "skjemaEmne";

    private static final String ID_NAME = "guid";
    private static final String[] ID_NAMES = new String[]{ID_NAME};
    public static final String ID_SEQ = "hendelseRefnummerSeq";
    public static final String QUERY_FIND_ALL = "Hendelse.findAll";
    public static final String QUERY_GET_BILDER = "Hendelse.getBilder";
    private static final String REFNUMMER = "refnummer";
    public static final String QUERY_HENT_MED_REFNUMMER = "hendelse.hentMedRefnummer";
    public static final String QUERY_HENT_MED_ELRAPPID = "hendelse.hentMedElrappId";
    public static final String QUERY_FIND_ENTITY_INFO_BY_SAK = "hendelse.findGuidBySak";
    public static final String QUERY_GET_BILDER_UTENRELASJONER = "hendelse.getBilderUtenRelasjoner";

    private Hendelseaarsak aarsak;
    private Long aarsakId;
    private String beskrivelse;
    private List<Bilde> bilder = new ArrayList<Bilde>();
    private Date dato;
    private String guid;
    private Date henvendelseDato;
    private String henvendelseFra;
    private String henvendelseMottattAv;
    private OwnedMipssEntity ownedMipssEntity = new OwnedMipssEntity();
    private Long refnummer;
    private String slettetAv;
    private Date slettetDato;
    private List<HendelseTrafikktiltak> tiltak = new ArrayList<HendelseTrafikktiltak>();
    private Long elrappDokumentIdent;
    private Date rapportertDato;
    private Long elrappVersjon;
    private String skjemaEmne;

    private Sak sak;
    private String sakGuid;

    private ArbeidType arbeidType;
    private ObjektType objektType;
    private ObjektAvvikType objektAvvikType;
    private ObjektAvvikKategori objektAvvikKategori;
    private ObjektStedTypeAngivelse objektStedTypeAngivelse;

    private String merknad;

    private Prosess prosess;
    private Date planlagtUtbedret;
    private String utbedringplan;
    private boolean utbedret;

    @ManyToOne
    @JoinColumn(name = "ARBEIDTYPE_ID", referencedColumnName = "ID")
    public ArbeidType getArbeidType() {
        return arbeidType;
    }

    public void setArbeidType(ArbeidType arbeidType) {
        ArbeidType old = this.arbeidType;
        this.arbeidType = arbeidType;
        firePropertyChange("arbeidType", old, arbeidType);
    }

    @ManyToOne
    @JoinColumn(name = "OBJEKTTYPE_ID", referencedColumnName = "ID")
    public ObjektType getObjektType() {
        return objektType;
    }

    public void setObjektType(ObjektType objektType) {
        ObjektType old = this.objektType;
        this.objektType = objektType;
        firePropertyChange("objektType", old, objektType);
    }

    @ManyToOne
    @JoinColumn(name = "OBJEKTAVVIKTYPE_ID", referencedColumnName = "ID")
    public ObjektAvvikType getObjektAvvikType() {
        return objektAvvikType;
    }

    public void setObjektAvvikType(ObjektAvvikType objektAvvikType) {
        ObjektAvvikType old = this.objektAvvikType;
        this.objektAvvikType = objektAvvikType;
        firePropertyChange("objektAvvikType", old, objektAvvikType);
    }

    @ManyToOne
    @JoinColumn(name = "OBJEKTAVVIKKATEGORI_ID", referencedColumnName = "ID")
    public ObjektAvvikKategori getObjektAvvikKategori() {
        return objektAvvikKategori;
    }

    public void setObjektAvvikKategori(ObjektAvvikKategori objektAvvikKategori) {
        this.objektAvvikKategori = objektAvvikKategori;
    }

    @ManyToOne
    @JoinColumn(name = "OBJEKTSTEDTYPEANGIVELSE_ID", referencedColumnName = "ID")
    public ObjektStedTypeAngivelse getObjektStedTypeAngivelse() {
        return objektStedTypeAngivelse;
    }

    public void setObjektStedTypeAngivelse(ObjektStedTypeAngivelse objektStedTypeAngivelse) {
        ObjektStedTypeAngivelse old = this.objektStedTypeAngivelse;
        this.objektStedTypeAngivelse = objektStedTypeAngivelse;
        firePropertyChange("objektStedTypeAngivelse", old, objektStedTypeAngivelse);
    }

    public boolean isUtbedret() {
        return utbedret;
    }

    public void setUtbedret(boolean utbedret) {
        boolean old = this.utbedret;
        this.utbedret = utbedret;
        firePropertyChange("utbedret", old, utbedret);
    }

    @OneToOne
    @JoinColumn(name = "SAK_GUID", referencedColumnName = "GUID")
    public Sak getSak() {
        return sak;
    }

    @Column(name = "SAK_GUID", nullable = true, insertable = false, updatable = false)
    public String getSakGuid() {
        return sakGuid;
    }

    @Transient
    public Punktveiref getVeiref() {
        if (sak != null)
            return sak.getVeiref();
        return null;
    }

    public void setSak(Sak sak) {
        Sak old = this.sak;
        this.sak = sak;
        if (sak != null) {
            setSakGuid(sak.getGuid());
        } else {
            setSakGuid(null);
        }
        firePropertyChange("sak", old, sak);

    }


    public void setSakGuid(String sakGuid) {
        String old = this.sakGuid;
        this.sakGuid = sakGuid;
        firePropertyChange("sakGuid", old, sakGuid);
    }

    @Column(name = "SKJEMA_EMNE")
    public String getSkjemaEmne() {
        return skjemaEmne;
    }

    public void setSkjemaEmne(String skjemaEmne) {
        String old = this.skjemaEmne;
        this.skjemaEmne = skjemaEmne;
        firePropertyChange(FIELD_NAME_SKJEMA_EMNE, old, skjemaEmne);
    }

    /**
     * Legger til et bilde
     *
     * @param b
     */
    public void addBilde(Bilde b) {
        log.trace("addBilde({})", b);
        List<Bilde> old = new ArrayList<Bilde>(getBilder());
        log.trace("addBilde({})", b);
        getBilder().add(b);
        firePropertyChange(FIELD_NAME_BILDER, old, getBilder());
    }

    /**
     * Legger til et tiltak til denne hendelsen
     *
     * @param t
     */
    public void addTrafikktiltak(HendelseTrafikktiltak t) {
        log.trace("addTrafikktiltak({})", t);
        List<HendelseTrafikktiltak> oldTiltak = new ArrayList<HendelseTrafikktiltak>(getTiltak());
        String oldTxt = getTiltakstekster();
        t.setHendelse(this);
        getTiltak().add(t);
        firePropertyChange(FIELD_NAME_TILTAKSTEKSTER, oldTxt, getTiltakstekster());
        firePropertyChange(FIELD_NAME_TILTAK, oldTiltak, getTiltak());
    }

    /**
     * {@inheritDoc}
     */
    public int compareTo(Hendelse o) {
        if (o == null) {
            return 1;
        }

        return new CompareToBuilder().append(dato, o.dato).append(henvendelseFra, o.henvendelseFra).toComparison();
    }

    @Column(name = "ELRAPP_DOKUMENT_IDENT")
    public Long getElrappDokumentIdent() {
        return elrappDokumentIdent;
    }

    public void setElrappDokumentIdent(Long elrappDokumentIdent) {
        Long old = this.elrappDokumentIdent;
        this.elrappDokumentIdent = elrappDokumentIdent;
        firePropertyChange("elrappDokumentIdent", old, elrappDokumentIdent);
    }

    @Column(name = "ELRAPP_VERSJON")
    public Long getElrappVersjon() {
        return elrappVersjon;
    }

    public void setElrappVersjon(Long elrappVersjon) {
        Long old = this.elrappVersjon;
        this.elrappVersjon = elrappVersjon;
        firePropertyChange("elrappVersjon", old, elrappVersjon);
    }

    @Column(name = "RAPPORTERT_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getRapportertDato() {
        return rapportertDato;
    }

    public void setRapportertDato(Date rapportertDato) {
        Date old = this.rapportertDato;
        this.rapportertDato = rapportertDato;
        firePropertyChange("rapportertDato", old, rapportertDato);
    }

    /**
     * Årsaken til hendelsen
     *
     * @return
     */
    @ManyToOne(optional = true)
    @JoinColumn(name = "AARSAK_ID", referencedColumnName = "ID")
    public Hendelseaarsak getAarsak() {
        return aarsak;
    }

    /**
     * Årsaken til hendelsen
     *
     * @return
     */
    @Column(name = "AARSAK_ID", nullable = true, insertable = false, updatable = false)
    public Long getAarsakId() {
        return aarsakId;
    }

    /**
     * Returnerer antall bilder tilknyttet funnet
     *
     * @return
     */
    @Transient
    public Integer getAntallBilder() {
        if (bilder == null) {
            return Integer.valueOf(0);
        } else {
            return Integer.valueOf(bilder.size());
        }
    }

    @Column(name = "BESKRIVELSE", length = 1000)
    public String getBeskrivelse() {
        return beskrivelse;
    }

    /**
     * Bilder knyttet til hendelsen
     *
     * @return
     */
    @ManyToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "HENDELSE_BILDE_XV",
            joinColumns = {@JoinColumn(name = "HENDELSE_GUID", referencedColumnName = "GUID")},
            inverseJoinColumns = {@JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID")})
    public List<Bilde> getBilder() {
        return bilder;
    }

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDato() {
        return dato;
    }

    @Id
    @Column(nullable = false)
    public String getGuid() {
        return guid;
    }

    @Column(name = "HENVENDELSE_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getHenvendelseDato() {
        return henvendelseDato;
    }

    @Column(name = "HENVENDELSE_FRA")
    public String getHenvendelseFra() {
        return henvendelseFra;
    }

    @Column(name = "HENVENDELSE_MOTTATT_AV")
    public String getHenvendelseMottattAv() {
        return henvendelseMottattAv;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getIdNames() {
        return ID_NAMES;
    }

    /**
     * {@inheritDoc}
     */
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        return ownedMipssEntity;
    }

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Hendelse.ID_SEQ)
    @Column(name = REFNUMMER)
    public Long getRefnummer() {
        return refnummer;
    }

    @Column(name = "SLETTET_AV")
    public String getSlettetAv() {
        return slettetAv;
    }

    @Column(name = "SLETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSlettetDato() {
        return slettetDato;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getTextForGUI() {
        String datoString = dato != null ? MipssDateFormatter.formatDate(dato,
                MipssDateFormatter.SHORT_DATE_TIME_FORMAT) : EMPTY_STRING;
        String av = getOwnedMipssEntity().getOpprettetAv() != null ? getOwnedMipssEntity().getOpprettetAv()
                : EMPTY_STRING;

        return datoString + " av " + av;
    }

    /**
     * Hack for å "jukse" til supbreports for Jasper
     *
     * @return
     */
    @Transient
    public Collection<Hendelse> getThisForReport() {
        return Collections.singletonList(this);
    }

    /**
     * Tiltak
     *
     * @return
     */
    @OneToMany(mappedBy = "hendelse", orphanRemoval = true)
    public List<HendelseTrafikktiltak> getTiltak() {
        return tiltak;
    }

    /**
     * Brukes i GUI og rapporter til å liste opp utførte tiltak
     *
     * @return
     */
    @Transient
    public String getTiltakstekster() {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for (HendelseTrafikktiltak t : getTiltak()) {
            if (t.getTrafikktiltak() != null && t.getTrafikktiltak().getNavn() != null) {
                sb.append(t.getTrafikktiltak().getNavn());
                if (i < tiltak.size() - 1) {
                    sb.append(", ");
                }
            }
            i++;
        }

        return sb.toString();
    }

    /**
     * Sletter et bilde
     *
     * @param b
     */
    public void removeBilde(Bilde b) {
        List<Bilde> old = new ArrayList<Bilde>(getBilder());
        log.trace("removeBilde({})", b);
        getBilder().remove(b);
        firePropertyChange(FIELD_NAME_BILDER, old, getBilder());
    }

    /**
     * Fjerner et tiltak fra denne hendelsen
     *
     * @param t
     */
    public void removeTrafikktiltak(HendelseTrafikktiltak t) {
        log.trace("removeTrafikktiltak({})", t);
        List<HendelseTrafikktiltak> oldTiltak = new ArrayList<HendelseTrafikktiltak>(getTiltak());
        String oldTxt = getTiltakstekster();
        getTiltak().remove(t);
        firePropertyChange(FIELD_NAME_TILTAKSTEKSTER, oldTxt, getTiltakstekster());
        firePropertyChange(FIELD_NAME_TILTAK, oldTiltak, getTiltak());
    }

    /**
     * Årsaken til hendelsen
     *
     * @param aarsak
     */
    public void setAarsak(Hendelseaarsak aarsak) {
        Hendelseaarsak old = this.aarsak;
        this.aarsak = aarsak;

        if (aarsak != null) {
            setAarsakId(aarsak.getId());
        } else {
            setAarsakId(null);
        }
        firePropertyChange(FIELD_NAME_AARSAK, old, aarsak);
    }

    /**
     * Årsaken til hendelsen
     *
     * @param aarsakId
     */
    public void setAarsakId(Long aarsakId) {
        Long old = this.aarsakId;
        this.aarsakId = aarsakId;
        firePropertyChange(FIELD_NAME_AARSAKID, old, aarsakId);
    }

    /**
     * Beskrivelse
     *
     * @param beskrivelse
     */
    public void setBeskrivelse(String beskrivelse) {
        String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;

        try {
            fireVetoableChange(FIELD_NAME_BESKRIVELSE, old, beskrivelse);
        } catch (PropertyVetoException e) {
            this.beskrivelse = old;
        }

        firePropertyChange(FIELD_NAME_BESKRIVELSE, old, beskrivelse);
    }

    /**
     * Bilder knyttet til hendelsen
     *
     * @param bilder
     */
    public void setBilder(List<Bilde> bilder) {
        List<Bilde> old = this.bilder;
        this.bilder = bilder;
        firePropertyChange(FIELD_NAME_BILDER, old, bilder);
    }

    public void setDato(Date dato) {
        Date old = this.dato;
        this.dato = dato;
        firePropertyChange(FIELD_NAME_DATO, old, dato);
    }

    public void setGuid(String guid) {
        String old = this.guid;
        this.guid = guid;
        firePropertyChange("guid", old, guid);
    }

    public void setHenvendelseDato(Date henvendelseDato) {
        Date old = this.henvendelseDato;
        this.henvendelseDato = henvendelseDato;
        firePropertyChange(FIELD_NAME_HENVENDELSEDATO, old, henvendelseDato);
    }

    public void setHenvendelseFra(String henvendelseFra) {
        String old = this.henvendelseFra;
        this.henvendelseFra = henvendelseFra;
        firePropertyChange(FIELD_NAME_HENVENDELSEFRA, old, henvendelseFra);
    }

    public void setHenvendelseMottattAv(String henvendelseMottattAv) {
        String old = this.henvendelseMottattAv;
        this.henvendelseMottattAv = henvendelseMottattAv;
        firePropertyChange(FIELD_NAME_HENVENDELSEMOTTATTAV, old, henvendelseMottattAv);
    }

    /**
     * @param ownedMipssEntity
     * @see #getOwnedMipssEntity()
     */
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        OwnedMipssEntity old = this.ownedMipssEntity;
        this.ownedMipssEntity = ownedMipssEntity;
        firePropertyChange(FIELD_NAME_OWNEDMIPSSENTITY, old, ownedMipssEntity);
    }

    public void setRefnummer(Long refnummer) {
        Long old = this.refnummer;
        this.refnummer = refnummer;
        firePropertyChange("refnummer", old, refnummer);
    }

    public void setSlettetAv(String slettetAv) {
        String oldValue = this.slettetAv;
        this.slettetAv = slettetAv;
        firePropertyChange(FIELD_NAME_SLETTETAV, oldValue, slettetAv);
    }

    public void setSlettetDato(Date slettetDato) {
        Date oldValue = this.slettetDato;
        this.slettetDato = slettetDato;
        firePropertyChange(FIELD_NAME_SLETTETDATO, oldValue, slettetDato);
    }

    /**
     * Tiltak
     *
     * @param tiltak
     */
    public void setTiltak(List<HendelseTrafikktiltak> tiltak) {
        String oldTxt = getTiltakstekster();
        List<HendelseTrafikktiltak> old = this.tiltak;
        this.tiltak = tiltak;
        firePropertyChange(FIELD_NAME_TILTAKSTEKSTER, oldTxt, getTiltakstekster());
        firePropertyChange(FIELD_NAME_TILTAK, old, tiltak);
    }


    public String getMerknad() {
        return merknad;
    }

    public void setMerknad(String merknad) {
        String old = this.merknad;
        this.merknad = merknad;
        firePropertyChange("merknad", old, merknad);
    }

    @ManyToOne
    @JoinColumn(name = "PROSESS_ID", referencedColumnName = "ID")
    public Prosess getProsess() {
        return prosess;
    }

    public void setProsess(Prosess prosess) {
        Prosess old = this.prosess;
        this.prosess = prosess;
        firePropertyChange("prosess", old, prosess);
        if (getProsessString() != null) {
            firePropertyChange("prosessString", null, getProsessString());
        }
    }

    @Override
    public Long getProsessId() {
        if(prosess != null) {
            return prosess.getId();
        }
        return null;
    }

    @Transient
    public String getProsessString(){
        if (getProsess() != null && getProsess().getProsessString() != null) {
            return getProsess().getProsessString();
        }
        return "";
    }

    @Column(name="PLANLAGT_UTBEDRET")
    @Temporal(TemporalType.DATE)
    public Date getPlanlagtUtbedret() {
        return planlagtUtbedret;
    }

    public void setPlanlagtUtbedret(Date planlagtUtbedret) {
        Date old = this.planlagtUtbedret;
        this.planlagtUtbedret = planlagtUtbedret;
        firePropertyChange("planlagtUtbedret", old, planlagtUtbedret);
    }

    public String getUtbedringplan() {
        return utbedringplan;
    }

    public void setUtbedringplan(String utbedringplan) {
        String old = this.utbedringplan;
        this.utbedringplan = utbedringplan;
        firePropertyChange("utbedringplan", old, utbedringplan);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append(REFNUMMER, refnummer).append(ID_NAME, guid)
                .append(FIELD_NAME_DATO, dato).append(FIELD_NAME_BESKRIVELSE, beskrivelse).toString();
    }

    /**
     * Setter Årsaken til hendelsen
     *
     * @param aarsak
     * @return
     */
    public Hendelse withAarsak(Hendelseaarsak aarsak) {
        setAarsak(aarsak);
        return this;
    }

    /**
     * Setter beskrivelsen
     *
     * @param b
     * @return
     */
    public Hendelse withBeskrivelse(String b) {
        setBeskrivelse(b);
        return this;
    }

    /**
     * Legger til et bilde
     *
     * @param b
     * @return
     */
    public Hendelse withBilde(Bilde b) {
        addBilde(b);
        return this;
    }

    /**
     * Setter dato
     *
     * @param d
     * @return
     */
    public Hendelse withDato(Date d) {
        setDato(d);
        return this;
    }

    /**
     * Setter id
     *
     * @param s
     * @return
     */
    public Hendelse withGUID(String s) {
        setGuid(s);
        return this;
    }


    /**
     * Legger til tiltak
     *
     * @param t
     * @return
     */
    public Hendelse withTrafikktiltak(HendelseTrafikktiltak t) {
        getTiltak().add(t);
        return this;
    }
}
