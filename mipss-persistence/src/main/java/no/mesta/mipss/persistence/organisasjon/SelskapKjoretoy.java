package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA entitet for et mange-til-mange forhold. Både Kjoretoy og Selskap har sine 
 * respektive ManyToMany forhold direkte til hverandre.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "SelskapKjoretoy.findAll", 
    query = "select o from SelskapKjoretoy o")
@Table(name = "SELSKAP_KJORETOY")
@IdClass(SelskapKjoretoyPK.class)
public class SelskapKjoretoy implements Serializable, IRenderableMipssEntity {
    private Long kjoretoyId;
    private String selskapskode;
    private Kjoretoy kjoretoy;

    public SelskapKjoretoy() {
    }

    @Id
    @Column(name="KJORETOY_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    @Id
    @Column(nullable = false)
    public String getSelskapskode() {
        return selskapskode;
    }

    public void setSelskapskode(String selskapskode) {
        this.selskapskode = selskapskode;
    }

    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
        this.kjoretoy = kjoretoy;
    }
    
    // de neste metodene er håndkodet
     /**
      * Genererer en lesbar tekst med hovedlementene i entitybønnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         return new ToStringBuilder(this).
             append("selskapskode", getSelskapskode()).
             append("kjoretoyId", getKjoretoyId()).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante nøklene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof SelskapKjoretoy)) {
             return false;
         }
         SelskapKjoretoy other = (SelskapKjoretoy) o;
         return new EqualsBuilder().
             append(getSelskapskode(), other.getSelskapskode()).
             append(getKjoretoyId(), other.getKjoretoyId()).
             isEquals();
     }

     /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
         return new HashCodeBuilder(23,41).
             append(getSelskapskode()).
             append(getKjoretoyId()).
             toHashCode();
     }

    public String getTextForGUI() {
        return String.valueOf(getKjoretoyId()) + "-" + getSelskapskode();
    }

    public int compareTo(Object o) {
        SelskapKjoretoy other = (SelskapKjoretoy) o;
        return new CompareToBuilder().
            append(getKjoretoyId(), other.getKjoretoyId()).
            append(getSelskapskode(), other.getSelskapskode()).
            toComparison();
    }
}
