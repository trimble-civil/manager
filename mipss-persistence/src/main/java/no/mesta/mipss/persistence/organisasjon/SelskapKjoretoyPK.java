package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SelskapKjoretoyPK implements Serializable {
    public Long kjoretoyId;
    public String selskapskode;

    public SelskapKjoretoyPK() {
    }

    public SelskapKjoretoyPK(Long kjoretoyId, String selskapskode) {
        this.kjoretoyId = kjoretoyId;
        this.selskapskode = selskapskode;
    }

    public boolean equals(Object other) {
        if (other instanceof SelskapKjoretoyPK) {
            final SelskapKjoretoyPK otherSelskapKjoretoyPK = (SelskapKjoretoyPK) other;
            final boolean areEqual = 
                (otherSelskapKjoretoyPK.kjoretoyId.equals(kjoretoyId) && otherSelskapKjoretoyPK.selskapskode.equals(selskapskode));
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }
}
