package no.mesta.mipss.persistence;

import java.util.Date;

public interface IkkeoverlappendePeriode {
	Date getFraDato();
	Date getTilDato();
}
