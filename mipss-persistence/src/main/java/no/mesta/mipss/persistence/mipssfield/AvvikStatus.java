package no.mesta.mipss.persistence.mipssfield;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Status for et funn.
 * 
 * Det skal alltid være en opprettetDato og avvik. Ved spesialtilfelle skal
 * lukketDato på Funnet også settes.
 * 
 * @see no.mesta.mipss.persistence.mipssfield.Avvik
 * @see no.mesta.mipss.persistence.mipssfield.AvvikStatus
 * @see no.mesta.mipss.persistence.mipssfield.listener.AvvikStatusListener
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( {
		@NamedQuery(name = AvvikStatus.QUERY_FIND_ALL, query = "SELECT p FROM AvvikStatus p"),
		@NamedQuery(name = AvvikStatus.QUERY_FIND_FOR_AVVIK, query = "SELECT p FROM AvvikStatus p WHERE p.avvikGuid = :pguid") })
@Table(name = "AVVIK_STATUS_XV")
@IdClass(AvvikStatusPK.class)
@EntityListeners(no.mesta.mipss.persistence.mipssfield.listener.AvvikStatusListener.class)
public class AvvikStatus extends MipssEntityBean<AvvikStatus> implements Serializable,
		Comparable<AvvikStatus>, IRenderableMipssEntity, PropertyChangeSource {

	private static final Logger log = LoggerFactory.getLogger(AvvikStatus.class);

	private static final String AVVIK = "avvik";
	private static final String TEXT_FOR_GUI = "textForGUI";
	private static final String OPPRETTET_DATO = "opprettetDato";
	private static final String OPPRETTET_AV = "opprettetAv";
	private static final String FRITEKST = "fritekst";
	private static final String ID_FIELD_AVVIK = AVVIK;
	private static final String ID_FIELD_AVVIK_GUID = "avvikGuid";
	private static final String ID_FIELD_AVVIKSTATUS_TYPE = "avvikstatusType";
	private static final String[] ID_NAMES = { ID_FIELD_AVVIK_GUID, ID_FIELD_AVVIK, ID_FIELD_AVVIKSTATUS_TYPE };
	public static final String QUERY_FIND_ALL = "AvvikStatus.findAll";
	public static final String QUERY_FIND_FOR_AVVIK = "AvvikStatus.findForAvvik";
	private String fritekst;
	private String opprettetAv;
	private Date opprettetDato;
	private Avvik avvik;
	private String avvikGuid;
	private AvvikstatusType avvikstatusType;
	private Inspeksjon inspeksjon;
	private String inspeksjonGuid;

	/**
	 * Konstruktør
	 * 
	 */
	public AvvikStatus() {
	}

	/**
	 * Inspeksjon
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID")
	public Inspeksjon getInspeksjon() {
		return inspeksjon;
	}	
	/**
	 * Inspeksjon
	 * 
	 * @return
	 */
	@Column(name = "INSPEKSJON_GUID", nullable = true, insertable = false, updatable = false)
	public String getInspeksjonGuid() {
		return inspeksjonGuid;
	}
	
	/**
	 * Tilhørende inspeksjon
	 * 
	 * @param inspeksjon
	 */
	public void setInspeksjon(Inspeksjon inspeksjon) {
		Inspeksjon old = this.inspeksjon;
		this.inspeksjon = inspeksjon;
		
		if(inspeksjon != null) {
			setInspeksjonGuid(inspeksjon.getGuid());
		} else {
			setInspeksjonGuid(null);
		}
		firePropertyChange("inspeksjon", old, inspeksjon);
	}

	/**
	 * Inspeksjon
	 * 
	 * @param inspeksjonGuid
	 */
	public void setInspeksjonGuid(String inspeksjonGuid) {
		log.trace("setInspeksjonGuid({})", inspeksjonGuid);
		String old = this.inspeksjonGuid;
		this.inspeksjonGuid = inspeksjonGuid;

		firePropertyChange("inspeksjon_guid", old, inspeksjonGuid);
	}
	/**
	 * <p>
	 * Benytter {@linkplain #getOpprettetDato()} til å sortere
	 * </p>
	 * 
	 * @param o
	 * @return
	 */
	public int compareTo(AvvikStatus o) {
		return new CompareToBuilder().append(getOpprettetDato(), o.getOpprettetDato()).toComparison();
	}

	/**
	 * Fritekst
	 * 
	 * @return
	 */
	@Column(name = "FRITEKST", length = 4000)
	public String getFritekst() {
		return fritekst;
	}

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/**
	 * Opprettet av
	 * 
	 * @return
	 */
	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	/**
	 * Opprettet
	 * 
	 * @return
	 */
	@Id
	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	/**
	 * Avvik
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "AVVIK_GUID", referencedColumnName = "GUID")
	public Avvik getAvvik() {
		return avvik;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(name = "AVVIK_GUID", nullable = false, insertable = false, updatable = false)
	public String getAvvikGuid() {
		return avvikGuid;
	}

	/**
	 * Status
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "STATUS_ID", referencedColumnName = "ID")
	public AvvikstatusType getAvvikstatusType() {
		return avvikstatusType;
	}


	@Override
	public String getTextForGUI() {
		String status = (avvikstatusType == null ? "" : avvikstatusType.getTextForGUI());
		String date = (getOpprettetDato() == null ? "" : " (" + MipssDateFormatter.formatDate(getOpprettetDato(), true)
				+ ")");
		return status + date;
	}

	/**
	 * Fritekst
	 * 
	 * @param fritekst
	 */
	public void setFritekst(String fritekst) {
		String old = this.fritekst;
		this.fritekst = fritekst;
		
		try {
			fireVetoableChange(FRITEKST, old, fritekst);
		} catch (PropertyVetoException e) {
			this.fritekst = old;
		}

		firePropertyChange(FRITEKST, old, fritekst);
	}

	/**
	 * Opprettet av
	 * 
	 * @param opprettetAv
	 */
	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange(OPPRETTET_AV, old, opprettetAv);
	}

	/**
	 * Opprettet
	 * 
	 * @param opprettetDato
	 */
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		String oldText = getTextForGUI();
		this.opprettetDato = opprettetDato;
		firePropertyChange(OPPRETTET_DATO, old, opprettetDato);
		firePropertyChange(TEXT_FOR_GUI, oldText, getTextForGUI());
	}

	/**
	 * Avvik
	 * 
	 * @param avvik
	 */
	public void setAvvik(Avvik avvik) {
		Avvik old = this.avvik;
		this.avvik = avvik;

		if (avvik != null) {
			setAvvikGuid(avvik.getGuid());
		} else {
			setAvvikGuid(null);
		}
		firePropertyChange(ID_FIELD_AVVIK, old, avvik);
	}

	/**
	 * Id
	 * 
	 * @param avvikGuid
	 */
	public void setAvvikGuid(String avvikGuid) {
		String old = this.avvikGuid;
		this.avvikGuid = avvikGuid;
		firePropertyChange(ID_FIELD_AVVIK_GUID, old, avvikGuid);
	}

	/**
	 * Status
	 * 
	 * @param avvikstatusType
	 */
	public void setAvvikstatusType(AvvikstatusType avvikstatusType) {
		log.trace("setAvvikstatusType({})", avvikstatusType);
		AvvikstatusType old = this.avvikstatusType;
		String oldText = getTextForGUI();
		this.avvikstatusType = avvikstatusType;

		firePropertyChange(ID_FIELD_AVVIKSTATUS_TYPE, old, avvikstatusType);
		firePropertyChange(TEXT_FOR_GUI, oldText, getTextForGUI());
	}


	@Override
	public String toString() {
		return new ToStringBuilder(this).append(ID_FIELD_AVVIK_GUID, avvikGuid)
				.append(FRITEKST, fritekst).append(OPPRETTET_AV, opprettetAv).append(OPPRETTET_DATO, opprettetDato)
				.toString();
	}
}
