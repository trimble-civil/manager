package no.mesta.mipss.persistence.driftslogg;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NamedQueries({
    @NamedQuery(name = Aktivitet.AKTIVITET_SOK, query = "select o from Aktivitet o where o.tilDatoTid >= :fraDato and o.fraDatoTid <= :tilDato "),
    @NamedQuery(name = Aktivitet.AKTIVITET_SOK_ARTIKKEL_LEVERANDOR, query = 			"select o from Aktivitet o where o.tilDatoTid >= :fraDato and o.fraDatoTid <= :tilDato and o.artikkelIdent =:ident and o.trip.period.levkontrakt.leverandorNr =:levNr and o.trip.period.levkontraktIdent=:levkontraktIdent"),
    @NamedQuery(name = Aktivitet.AKTIVITET_SOK_ARTIKKEL_LEVERANDOR_KUN_FRADATO, query = "select o from Aktivitet o where o.fraDatoTid >= :fraDato and o.fraDatoTid <= :tilDato and o.artikkelIdent =:ident and o.trip.period.levkontrakt.leverandorNr =:levNr and o.trip.period.levkontraktIdent=:levkontraktIdent"),
    @NamedQuery(name = Aktivitet.AKTIVITET_SOK_GODKJENT, query = "select o from Aktivitet o where o.fraDatoTid between :fraDato and :tilDato and o.trip.period.levkontrakt.driftkontraktId=:driftkontraktId" )//TODO join med periodestatus=godkjent
})
@SequenceGenerator(name = Aktivitet.ID_SEQ, sequenceName = "AKTIVITET_ID_SEQ", initialValue = 1, allocationSize = 1)

@SuppressWarnings("serial")
@Entity
@Table(name="AKTIVITET")
public class Aktivitet extends MipssEntityBean<Aktivitet> implements IRenderableMipssEntity, Warning {

	public static final String AKTIVITET_SOK = "Aktivitet.aktivitetSok";
	public static final String AKTIVITET_SOK_ARTIKKEL_LEVERANDOR = "Aktivitet.aktivitetSokArtikkelLeverandor";
	public static final String AKTIVITET_SOK_ARTIKKEL_LEVERANDOR_KUN_FRADATO = "Aktivitet.aktivitetSokArtikkelLeverandorKunStartDato";
	public static final String AKTIVITET_SOK_GODKJENT = "Aktivitet.aktivitetSokGodkjent";
	public static final String ID_SEQ = "aktivitetIdSeq";
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Aktivitet.ID_SEQ)
	private Long id;

	@Column(name = "ARTIKKEL_IDENT")
	private String artikkelIdent;

	@Column(name = "KJORETOY_ID")
	private Long kjoretoyId;

	@Column(name = "ANNET_KJORETOY")
	private String annetKjoretoy;

	@Column(name = "RODE_ID")
	private Long rodeId;

	@Column(name = "FRA_DATO_TID")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fraDatoTid;

	@Column(name = "TIL_DATO_TID")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tilDatoTid;

	private String enhet;

	private Double enhetsPris;

	@Column(name = "MELDT_MENGDE")
	private Double meldtMengde;

	@Column(name = "OMFORENT_MENGDE")
	private Double omforentMengde;

	@Column(name = "MELDT_KM")
	private Double meldtKm;

	@Column(name = "OMFORENT_KM")
	private Double omforentKm;

	@Column(name = "VAER_ID")
	private Long vaerId;

	@Column(name = "TEMPERATUR_ID")
	private Long temperaturId;

	@Column(name = "KOMMENTAR")
	private String kommentar;

	@Column(name = "INTERN_KOMMENTAR")
	private String internKommentar;

	@Column(name = "OPPRETTET_AV")
	private String opprettetAv;

	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;

	@Column(name = "ENDRET_AV")
	private String endretAv;
	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;

	@Column(name = "SENDT_AGRESSO_FLAGG")
	private Boolean sendtAgresso;

	@Column(name = "SENDT_AGRESSO_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendtAgressoDato;

	@OneToMany(mappedBy = "aktivitet", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	private List<Stroproduktmengde> stroprodukter = new ArrayList<>();

	@ManyToOne(cascade = CascadeType.PERSIST, fetch= FetchType.EAGER)
	@JoinColumn(name="TRIP_ID")
	private Trip trip;

	@Column(name = "MANUALLY_CREATED")
	private boolean manuallyCreated;

	@Column(name = "NO_PAYMENT")
	private boolean noPayment;

	@Column(name = "PRODUCTION_OUTSIDE_CONTRACT")
	private boolean productionOutsideContract;

	@Column(name = "PRODUCTION_OUTSIDE_RODE")
	private boolean productionOutsideRode;

	@Column(name = "ARTICLE_CHANGED")
	private boolean articleChanged;

	@Column(name = "PRODUCTION_TYPE")
	private String productionType;

	@Column(name = "R12_BROYTING")
	private Double r12_broyting;

	@Column(name = "R12_SIDEPLOG")
	private Double r12_sideplog;

	@Column(name = "R12_HOVLING")
	private Double r12_hovling;

	@Column(name = "R12_INTENSJON_KM")
	private Double r12_intention;

	@Column(name = "APPROVED_UE")
	private boolean approvedUE;

	@Column(name = "APPROVED_BUILD")
	private boolean approvedBuild;

	@Column(name = "FAKTURER_INTENSJON_FLAGG")
	private boolean approvedIntention;

	@Column(name = "CREATED_MANAGER")
	private boolean createdManager;

	@Column(name = "ACTUAL_DURATION")
	private Double actualDuration;

	@Transient
	private String viewKjoretoyNavn;

	@Transient
	private String viewRodeNavn;

	@Transient
	private String viewLeverandorNavn;

	@Transient
	private AgrArtikkelV viewArtikkel;

	@Transient
	private String idNames;

	//Benyttes for å kunne vise om en aktivitet ikke er overført pga leverandøren.
	@Transient
	private Boolean skalOverforesTilAgresso;

	// Used when adding a new activity to a trip.
	@Transient
	private String gritProductName;

	// Used when adding a new activity to a trip.
	@Transient
	private Double sum;

	// Used when adding a new activity to a trip.
	@Transient
	private Double dryGritAmount;

	// Used when adding a new activity to a trip.
	@Transient
	private Double wetGritAmount;

	// Used when adding a new activitiy to a trip.
	@Transient
	private Long tempId;

	public Aktivitet() {
	}

	public Aktivitet(Long kjoretoyId, Long rodeId, String productionType, boolean manuallyCreated, boolean productionOutsideContract, boolean productionOutsideRode, boolean noPayment) {
		this.kjoretoyId = kjoretoyId;
		this.rodeId = rodeId;
		this.productionType = productionType;
		this.manuallyCreated = manuallyCreated;
		this.productionOutsideContract = productionOutsideContract;
		this.productionOutsideRode = productionOutsideRode;
		this.noPayment = noPayment;
	}

	public Aktivitet(Aktivitet old) {
		this.id = old.id;
		this.artikkelIdent = old.artikkelIdent;
		this.viewArtikkel = old.viewArtikkel;
		this.kjoretoyId = old.kjoretoyId;
		this.annetKjoretoy = old.annetKjoretoy;
		this.rodeId = old.rodeId;
		this.fraDatoTid = old.fraDatoTid;
		this.tilDatoTid = old.tilDatoTid;
		this.enhet = old.enhet;
		this.enhetsPris = old.enhetsPris;
		this.meldtMengde = old.meldtMengde;
		this.omforentMengde = old.omforentMengde;
		this.meldtKm = old.meldtKm;
		this.omforentKm = old.omforentKm;
		this.vaerId = old.vaerId;
		this.temperaturId = old.temperaturId;
		this.kommentar = old.kommentar;
		this.internKommentar = old.internKommentar;
		this.opprettetAv = old.opprettetAv;
		this.opprettetDato = old.opprettetDato;
		this.endretAv = old.endretAv;
		this.endretDato = old.endretDato;
		this.sendtAgresso = old.sendtAgresso;
		this.sendtAgressoDato = old.sendtAgressoDato;
		this.stroprodukter = old.stroprodukter;
		this.manuallyCreated = old.manuallyCreated;
		this.noPayment = old.noPayment;
		this.productionOutsideContract = old.productionOutsideContract;
		this.productionOutsideRode = old.productionOutsideRode;
		this.articleChanged = old.articleChanged;
		this.productionType = old.productionType;
		this.r12_broyting = old.r12_broyting;
		this.r12_hovling = old.r12_hovling;
		this.r12_sideplog = old.r12_sideplog;
		this.r12_intention = old.r12_intention;
		this.approvedUE = old.approvedUE;
		this.approvedBuild = old.approvedBuild;
		this.approvedIntention = old.approvedIntention;
		this.createdManager = old.createdManager;
		this.viewKjoretoyNavn = old.viewKjoretoyNavn;
		this.viewRodeNavn = old.viewRodeNavn;
		this.viewLeverandorNavn = old.viewLeverandorNavn;
		this.trip = old.trip;

		if (old.viewArtikkel != null) {
			this.viewArtikkel = new AgrArtikkelV(old.getViewArtikkel());
		}

		this.skalOverforesTilAgresso = old.skalOverforesTilAgresso;
		this.gritProductName = old.gritProductName;
		this.sum = old.sum;
		this.dryGritAmount = old.dryGritAmount;
		this.wetGritAmount = old.wetGritAmount;
		this.tempId = old.tempId;
		this.actualDuration = old.actualDuration;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the artikkelIdent
	 */
	public String getArtikkelIdent() {
		return artikkelIdent;
	}


	/**
	 * @return the kjoretoyId
	 */
	public Long getKjoretoyId() {
		return kjoretoyId;
	}


	/**
	 * @return the annetKjoretoy
	 */
	public String getAnnetKjoretoy() {
		return annetKjoretoy;
	}


	/**
	 * @return the rodeId
	 */
	public Long getRodeId() {
		return rodeId;
	}


	/**
	 * @return the fraDatoTid
	 */
	public Date getFraDatoTid() {
		return fraDatoTid;
	}


	/**
	 * @return the tilDatoTid
	 */
	public Date getTilDatoTid() {
		return tilDatoTid;
	}


	/**
	 * @return the enhet
	 */
	public String getEnhet() {
		return enhet;
	}


	/**
	 * @return the enhetsPris
	 */
	public Double getEnhetsPris() {
		return enhetsPris;
	}


	/**
	 * @return the meldtMengde
	 */
	public Double getMeldtMengde() {
		return meldtMengde;
	}


	/**
	 * @return the omforentMengde
	 */
	public Double getOmforentMengde() {
		return omforentMengde;
	}


	/**
	 * @return the meldtKm
	 */
	public Double getMeldtKm() {
		return meldtKm;
	}


	/**
	 * @return the omforentKm
	 */
	public Double getOmforentKm() {
		return omforentKm;
	}


	/**
	 * @return the vaerId
	 */
	public Long getVaerId() {
		return vaerId;
	}


	/**
	 * @return the temperaturId
	 */
	public Long getTemperaturId() {
		return temperaturId;
	}


	/**
	 * @return the kommentar
	 */
	public String getKommentar() {
		return kommentar;
	}


	/**
	 * @return the internKommentar
	 */
	public String getInternKommentar() {
		return internKommentar;
	}


	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}


	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}


	/**
	 * @return the endretAv
	 */
	public String getEndretAv() {
		return endretAv;
	}


	/**
	 * @return the endretDato
	 */
	public Date getEndretDato() {
		return endretDato;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	/**
	 * @param artikkelIdent the artikkelIdent to set
	 */
	public void setArtikkelIdent(String artikkelIdent) {
		String old = this.artikkelIdent;
		this.artikkelIdent = artikkelIdent;
		firePropertyChange("artikkelIdent", old, artikkelIdent);
	}


	/**
	 * @param kjoretoyId the kjoretoyId to set
	 */
	public void setKjoretoyId(Long kjoretoyId) {
		Long old = this.kjoretoyId;
		this.kjoretoyId = kjoretoyId;
		firePropertyChange("kjoretoyId", old, kjoretoyId);
	}


	/**
	 * @param annetKjoretoy the annetKjoretoy to set
	 */
	public void setAnnetKjoretoy(String annetKjoretoy) {
		String old = this.annetKjoretoy;
		this.annetKjoretoy = annetKjoretoy;
		firePropertyChange("annetKjoretoy", old, annetKjoretoy);
	}


	/**
	 * @param rodeId the rodeId to set
	 */
	public void setRodeId(Long rodeId) {
		Long old = this.rodeId;
		this.rodeId = rodeId;
		firePropertyChange("rodeId", old, rodeId);
	}


	/**
	 * @param fraDatoTid the fraDatoTid to set
	 */
	public void setFraDatoTid(Date fraDatoTid) {
		Date old = this.fraDatoTid;
		this.fraDatoTid = fraDatoTid;
		firePropertyChange("fraDatoTid", old, fraDatoTid);
	}


	/**
	 * @param tilDatoTid the tilDatoTid to set
	 */
	public void setTilDatoTid(Date tilDatoTid) {
		Date old = this.tilDatoTid;
		this.tilDatoTid = tilDatoTid;
		firePropertyChange("tilDatoTid", old, tilDatoTid);
	}


	/**
	 * @param enhet the enhet to set
	 */
	public void setEnhet(String enhet) {
		String old = this.enhet;
		this.enhet = enhet;
		firePropertyChange("enhet", old, enhet);
	}


	/**
	 * @param enhetsPris the enhetsPris to set
	 */
	public void setEnhetsPris(Double enhetsPris) {
		Double old = this.enhetsPris;
		this.enhetsPris = enhetsPris;
		firePropertyChange("enhetsPris", old, enhetsPris);
	}


	/**
	 * @param meldtMengde the meldtMengde to set
	 */
	public void setMeldtMengde(Double meldtMengde) {
		Double old = this.meldtMengde;
		this.meldtMengde = meldtMengde;
		firePropertyChange("meldtMengde", old, meldtMengde);
	}


	/**
	 * @param omforentMengde the omforentMengde to set
	 */
	public void setOmforentMengde(Double omforentMengde) {
		Double old = this.omforentMengde;
		this.omforentMengde = omforentMengde;
		firePropertyChange("omforentMengde", old, omforentMengde);
	}


	/**
	 * @param meldtKm the meldtKm to set
	 */
	public void setMeldtKm(Double meldtKm) {
		Double old = this.meldtKm;
		this.meldtKm = meldtKm;
		firePropertyChange("meldtKm", old, meldtKm);
	}


	/**
	 * @param omforentKm the omforentKm to set
	 */
	public void setOmforentKm(Double omforentKm) {
		Double old = this.omforentKm;
		this.omforentKm = omforentKm;
		firePropertyChange("omforentKm", old, omforentKm);
	}

	public Double getGodkjentMengde() {
		return omforentMengde;
	}

	/**
	 * @param vaerId the vaerId to set
	 */
	public void setVaerId(Long vaerId) {
		Long old = this.vaerId;
		this.vaerId = vaerId;
		firePropertyChange("vaerId", old, vaerId);
	}


	/**
	 * @param temperaturId the temperaturId to set
	 */
	public void setTemperaturId(Long temperaturId) {
		Long old = this.temperaturId;
		this.temperaturId = temperaturId;
		firePropertyChange("temperaturId", old, temperaturId);
	}


	/**
	 * @param kommentar the kommentar to set
	 */
	public void setKommentar(String kommentar) {
		String old = this.kommentar;
		this.kommentar = kommentar;
		firePropertyChange("kommentar", old, kommentar);
	}


	/**
	 * @param internKommentar the internKommentar to set
	 */
	public void setInternKommentar(String internKommentar) {
		String old = this.internKommentar;
		this.internKommentar = internKommentar;
		firePropertyChange("internKommentar", old, internKommentar);
	}


	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}


	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}


	/**
	 * @param endretAv the endretAv to set
	 */
	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}


	/**
	 * @param endretDato the endretDato to set
	 */
	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}


	public String getTextForGUI() {
		return null;
	}


	public void setStroprodukter(List<Stroproduktmengde> stroprodukter) {
		List<Stroproduktmengde> old = this.stroprodukter;
		this.stroprodukter = stroprodukter;
		firePropertyChange("stroprodukter", old, stroprodukter);
	}


	public List<Stroproduktmengde> getStroprodukter() {
		return stroprodukter;
	}


	public Periode getPeriode() {
		return trip.getPeriod();
	}


	public void setViewRodeNavn(String viewRodeNavn) {
		this.viewRodeNavn = viewRodeNavn;
	}


	public String getViewRodeNavn() {
		return viewRodeNavn;
	}


	public void setViewKjoretoyNavn(String viewKjoretoyNavn) {
		this.viewKjoretoyNavn = viewKjoretoyNavn;
	}


	public String getViewKjoretoyNavn() {
		return viewKjoretoyNavn;
	}

	public String getPeriodeStatus() {
		Periode periode = trip != null ? trip.getPeriod() : null;
		if(periode != null) {
			PeriodeStatus status = periode != null ? periode.getStatus() : null;
			if (status == null) {
				return "Mangler status";
			}
			return status.getStatusNavn();
		}
		return "Mangler status";
	}


	public void setViewArtikkel(AgrArtikkelV viewArtikkel) {
		this.viewArtikkel = viewArtikkel;
	}


	public AgrArtikkelV getViewArtikkel() {
		return viewArtikkel;
	}

	/**
	 * Mengden som er grunnlaget for oppgjør med leverandør
	 * f.eks. dersom leverandør har oppgjør på timer brøyting vil denne vise meldt_mengde(TIMER)
	 *
	 * @return
	 */
	public Double getUtfortMengde() {

		if (getEnhet().toLowerCase().startsWith("km")) {
			return meldtKm;
		}

		if (meldtMengde != null)
			return meldtMengde;

		if (meldtKm != null)
			return meldtKm;
		return null;
	}

	/**
	 * Mendge2,
	 * f.eks: dersom leverandør har oppgjør på timer brøyting vil denne vise meldt_km(KM)
	 *
	 * @return
	 */
	public Double getUtfortMengde2() {
		if (getEnhet().toLowerCase().startsWith("tim")) {
			return meldtKm;
		}
		return null;
	}


	public void setSendtAgresso(Boolean sendtAgresso) {
		this.sendtAgresso = sendtAgresso;
	}


	public Boolean getSendtAgresso() {
		return sendtAgresso;
	}


	public Date getSendtAgressoDato() {
		return sendtAgressoDato;
	}


	public void setSendtAgressoDato(Date sendtAgressoDato) {
		this.sendtAgressoDato = sendtAgressoDato;
	}


	public void setViewLeverandorNavn(String viewLeverandorNavn) {
		this.viewLeverandorNavn = viewLeverandorNavn;
	}


	public String getViewLeverandorNavn() {
		return viewLeverandorNavn;
	}

	public String[] getIdNames() {
		return new String[]{"id"};
	}

	public Boolean getSkalOverforesTilAgresso() {
		return skalOverforesTilAgresso;
	}

	public void setSkalOverforesTilAgresso(Boolean skalOverforesTilAgresso) {
		this.skalOverforesTilAgresso = skalOverforesTilAgresso;
	}

	public Double getR12_hovling() {
		return r12_hovling;
	}

	public Double getR12_sideplog() {
		return r12_sideplog;
	}

	public Double getR12_broyting() {
		return r12_broyting;
	}

	public String getProductionType() {
		return productionType;
	}

	public boolean isArticleChanged() {
		return articleChanged;
	}

	public boolean isProductionOutsideRode() {
		return productionOutsideRode;
	}

	public boolean isProductionOutsideContract() {
		return productionOutsideContract;
	}

	public boolean hasNoPayment() {
		return noPayment;
	}

	public boolean isManuallyCreated() {
		return manuallyCreated;
	}

	public Trip getTrip() {
		return trip;
	}

	public boolean isApprovedBuild() {
		return approvedBuild;
	}

	public void setApprovedBuild(boolean approvedBuild) {
		boolean old = this.approvedBuild;
		this.approvedBuild = approvedBuild;
		firePropertyChange("approvedBuild", old, approvedBuild);
	}

	public void setApprovedUE(boolean approvedUE) {
		boolean old = this.approvedUE;
		this.approvedUE = approvedUE;
		firePropertyChange("approvedUE", old, approvedUE);
	}

	public boolean isApprovedUE() {
		return approvedUE;
	}

	public void setR12_hovling(Double r12_hovling) {
		Double old = this.r12_hovling;
		this.r12_hovling = r12_hovling;
		firePropertyChange("r12_hovling", old, r12_hovling);
	}

	public void setR12_sideplog(Double r12_sideplog) {
		Double old = this.r12_sideplog;
		this.r12_sideplog = r12_sideplog;
		firePropertyChange("r12_sideplog", old, r12_sideplog);
	}

	public void setR12_broyting(Double r12_broyting) {
		Double old = this.r12_broyting;
		this.r12_broyting = r12_broyting;
		firePropertyChange("r12_broyting", old, r12_broyting);
	}

	public Double getR12_intention() {
		return r12_intention;
	}

	public void setR12_intention(Double r12_intention) {
		Double old = this.r12_intention;
		this.r12_intention = r12_intention;
		firePropertyChange("r12_intention", old, r12_intention);
	}

	public boolean isApprovedIntention() {
		return approvedIntention;
	}

	public void setApprovedIntention(boolean approvedIntention) {
		boolean old = this.approvedIntention;
		this.approvedIntention = approvedIntention;
		firePropertyChange("approvedIntention", old, approvedIntention);
	}

	public int hashCode(){
		return new HashCodeBuilder().append(getId()).toHashCode();
	}

	public void setProductionType(String productionType) {
		String old = this.productionType;
		this.productionType = productionType;
		firePropertyChange("productionType", old, productionType);
	}

	public String getGritProductName() {
		return gritProductName;
	}

	public void setGritProductName(String gritProductName) {
		this.gritProductName = gritProductName;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

	public Double getDryGritAmount() {
		return dryGritAmount;
	}

	public void setDryGritAmount(Double dryGritAmount) {
		Double old = this.dryGritAmount;
		this.dryGritAmount = dryGritAmount;
		firePropertyChange("dryGritAmount", old, dryGritAmount);
	}

	public Double getWetGritAmount() {
		return wetGritAmount;
	}

	public void setWetGritAmount(Double wetGritAmount) {
		Double old = this.wetGritAmount;
		this.wetGritAmount = wetGritAmount;
		firePropertyChange("wetGritAmount", old, wetGritAmount);
	}

	public Long getTempId() {
		return tempId;
	}

	public void setTempId(Long tempId) {
		this.tempId = tempId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		Aktivitet aktivitet = (Aktivitet) o;

		if (id != null ? !id.equals(aktivitet.id) : aktivitet.id != null) return false;
		return tempId != null ? tempId.equals(aktivitet.tempId) : aktivitet.tempId == null;
	}

	public void setTrip(Trip trip) {
		Trip old = this.trip;
		this.trip = trip;
		firePropertyChange("trip", old, trip);
	}

	public boolean isCreatedManager() {
		return createdManager;
	}

	public void setCreatedManager(boolean createdManager) {
		boolean old = this.createdManager;
		this.createdManager = createdManager;
		firePropertyChange("createdManager", old, createdManager);
	}

	public void setProductionOutsideRode(boolean productionOutsideRode) {
		boolean old = this.productionOutsideRode;
		this.productionOutsideRode = productionOutsideRode;
		firePropertyChange("productionOutsideRode", old, productionOutsideRode);
	}

	public Double getActualDuration() {
		return actualDuration;
	}

	public void setActualDuration(Double actualDuration) {
		Double old = this.actualDuration;
		this.actualDuration = actualDuration;
		firePropertyChange("actualDuration", old, actualDuration);
	}

}