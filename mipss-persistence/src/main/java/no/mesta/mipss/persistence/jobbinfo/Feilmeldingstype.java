package no.mesta.mipss.persistence.jobbinfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;


@SuppressWarnings("serial")
@Entity
@Table(name="FEILMELDINGSTYPE")
public class Feilmeldingstype extends MipssEntityBean<Feilmelding> {
	
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="NAVN")
	private String navn;
	
	@Column(name="FRITEKST")
	private String fritekst;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}

}
