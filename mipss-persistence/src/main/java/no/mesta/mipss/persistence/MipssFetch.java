package no.mesta.mipss.persistence;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotasjon som benyttes til å merke relasjoner som skal hentes i kode. 
 * 
 * method angir hvilken metode på det returnerte objektet som skal kjøres for å hente relasjonen
 * @author harkul
 *
 */
@Target({METHOD, FIELD}) 
@Retention(RUNTIME)
public @interface MipssFetch {
	String method() default "size";
}

