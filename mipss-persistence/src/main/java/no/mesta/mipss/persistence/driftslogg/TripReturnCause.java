package no.mesta.mipss.persistence.driftslogg;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import javax.persistence.*;
import java.util.Date;

@Entity
@IdClass(TripReturnCausePK.class)
@Table(name="TRIP_RETURN_CAUSE")
public class TripReturnCause extends MipssEntityBean<MipssEntityBean> implements IRenderableMipssEntity {

    @Id
    @Column(name="TRIP_ID")
    private Long tripId;

    @Id
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date created;

    @Id
    @Column(name="SIGNATURE")
    private String signature;

    @Column(name="CAUSE")
    private String cause;

    public TripReturnCause() { }

    public TripReturnCause(Long tripId, Date created, String signature, String cause) {
        this.tripId = tripId;
        this.created = created;
        this.signature = signature;
        this.cause = cause;
    }

    public Long getTripId() {
        return tripId;
    }

    public Date getCreated() {
        return created;
    }

    public String getSignature() {
        return signature;
    }

    public String getCause() {
        return cause;
    }

}