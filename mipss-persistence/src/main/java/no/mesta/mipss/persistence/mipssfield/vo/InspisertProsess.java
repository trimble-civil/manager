package no.mesta.mipss.persistence.mipssfield.vo;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Prosess;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Angir om inspeksjonens prosesser, både de som er knyttet til via inspiserte
 * prosesser og via funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class InspisertProsess implements Serializable, IRenderableMipssEntity, Comparable<InspisertProsess> {
	private int antallFunn;
	private Inspeksjon inspeksjon;
	private Prosess prosess;
	
	public InspisertProsess() {
	}

	public InspisertProsess(Inspeksjon i, Prosess p) {
		inspeksjon = i;
		prosess = p;
	}
	
	

	/** {@inheritDoc} */
	public int compareTo(InspisertProsess o) {
		if (o == null) {
			return 1;
		}
		if (equals(o)) {
			return 0;
		}

		return getTextForGUI().compareTo(o.getTextForGUI());
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof InspisertProsess)) {
			return false;
		}

		InspisertProsess other = (InspisertProsess) o;

		return new EqualsBuilder().append(inspeksjon, other.getInspeksjon()).append(prosess, other.getProsess())
				.isEquals();
	}

	public Boolean getAdHoc() {
		if (inspeksjon == null) {
			throw new IllegalStateException("Not attatched to an inspection!");
		}

		
		return !inspeksjon.getProsesser().contains(prosess);
	}

	public int getAntallFunn() {
		return antallFunn;
	}

	public Inspeksjon getInspeksjon() {
		return inspeksjon;
	}

	public String getKode() {
		return prosess.getProsessKode();
	}

	public String getNavn() {
		return prosess.getNavn();
	}

	public Prosess getProsess() {
		return prosess;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		return prosess.getTextForGUI();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(inspeksjon).append(prosess).toHashCode();
	}

	public void setAntallFunn(int antall) {
		antallFunn = antall;
	}

	public void setInspeksjon(Inspeksjon inspeksjon) {
		this.inspeksjon = inspeksjon;
	}

	public void setProsess(Prosess prosess) {
		this.prosess = prosess;
	}
}
