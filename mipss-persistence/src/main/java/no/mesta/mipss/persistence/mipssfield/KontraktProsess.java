package no.mesta.mipss.persistence.mipssfield;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = KontraktProsess.QUERY_FIND_ALL, query = "SELECT k FROM KontraktProsess k"),
    @NamedQuery(name = KontraktProsess.QUERY_FIND_BY_KONTRAKT, query = "SELECT k FROM KontraktProsess k WHERE k.kontraktId = :id"),
    @NamedQuery(name = KontraktProsess.QUERY_FIND_BY_KONTRAKT_GYLDIG_FOR_ARBEID, query = "SELECT k FROM KontraktProsess k WHERE k.kontraktId = :id and k.gyldigForArbeid=true")
    })
@Table(name = "KONTRAKT_PROSESS")
@IdClass(KontraktProsessPK.class)
public class KontraktProsess extends MipssEntityBean<KontraktProsess> {
	public static final String QUERY_FIND_ALL = "KontraktProsess.findAll";
	public static final String QUERY_FIND_BY_KONTRAKT = "KontraktProsess.findByKontrakt";
	public static final String QUERY_FIND_BY_KONTRAKT_GYLDIG_FOR_ARBEID = "KontraktProsess.findByKontraktGyldigForArbeid";

	@Id
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	private Long kontraktId;
	@Id
	@Column(name = "PROSESS_ID", nullable = false, insertable = false, updatable = false)
	private Long prosessId;
	@Column(name="FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fraDato;
	@ManyToOne
    @JoinColumn(name = "PROSESS_ID", referencedColumnName = "ID")
	private Prosess prosess;
	@ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
	private Driftkontrakt driftkontrakt;
	
	@Column(name="GYLDIG_FOR_INSPEKSJON_FLAGG", nullable = false)
	private Boolean gyldigForInspeksjon;
	@Column(name="GYLDIG_FOR_ARBEID_FLAGG", nullable = false)
	private Boolean gyldigForArbeid;
	
//	@Column(name = "OPPRETTET_AV")
	@Transient
	private String opprettetAv;
	
//	@Column(name = "OPPRETTET_DATO")
//	@Temporal(TemporalType.TIMESTAMP)
	@Transient
	private Date opprettetDato;
	
//	@Column(name="ENDRET_AV")
	@Transient
	private String endretAv;
	
//	@Column(name="ENDRET_DATO")
//	@Temporal(TemporalType.TIMESTAMP)
	@Transient
	private Date endretDato;
	
	public Long getKontraktId() {
		return kontraktId;
	}
	public void setKontraktId(Long kontraktId) {
		Long old = this.kontraktId;
		this.kontraktId = kontraktId;
		firePropertyChange("kontraktId", old, kontraktId);
	}
	public Long getProsessId() {
		return prosessId;
	}
	public void setProsessId(Long prosessId) {
		Long old = this.prosessId;
		this.prosessId = prosessId;
		firePropertyChange("prosessId", old, prosessId);
	}
	public Date getFraDato() {
		return fraDato;
	}
	public void setFraDato(Date fraDato) {
		Date old = this.fraDato;
		this.fraDato = fraDato;
		firePropertyChange("fraDato", old, fraDato);
	}
	public Prosess getProsess() {
		return prosess;
	}
	public void setProsess(Prosess prosess) {
		Prosess old = this.prosess;
		this.prosess = prosess;
		firePropertyChange("prosess", old, prosess);
		if (prosess!=null){
			setProsessId(prosess.getId());
		}else{
			setProsessId(null);
		}
	}
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}
	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		Driftkontrakt old = this.driftkontrakt;
		this.driftkontrakt = driftkontrakt;
		firePropertyChange("driftkontrakt", old, driftkontrakt);
		
		if(this.driftkontrakt != null) {
			setKontraktId(driftkontrakt.getId());
		} else {
			setKontraktId(null);
		}
	}
	public Boolean getGyldigForInspeksjon() {
		return gyldigForInspeksjon;
	}
	public void setGyldigForInspeksjon(Boolean gyldigForInspeksjon) {
		Boolean old = this.gyldigForInspeksjon;
		this.gyldigForInspeksjon = gyldigForInspeksjon;
		firePropertyChange("gyldigForInspeksjon", old, gyldigForInspeksjon);
	}
	public Boolean getGyldigForArbeid() {
		return gyldigForArbeid;
	}
	public void setGyldigForArbeid(Boolean gyldigForArbeid) {
		Boolean old = this.gyldigForArbeid;
		this.gyldigForArbeid = gyldigForArbeid;
		firePropertyChange("gyldigForArbeid", old, gyldigForArbeid);
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}
	public String getEndretAv() {
		return endretAv;
	}
	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}
	public Date getEndretDato() {
		return endretDato;
	}
	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}
	@Override
	public String getTextForGUI(){
		return prosess.getTextForGUI();
	}
	@Override
	public String toString(){
		return getTextForGUI();
	}
	
	
}
