package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class KjoretoyloggPK implements Serializable{

	private Long id;
	private Date opprettetDato;

	public KjoretoyloggPK(){
		
	}
	public KjoretoyloggPK(Long id, Date opprettetDato){
		this.id = id;
		this.opprettetDato = opprettetDato;
		
	}
	public Long getId() {
		return id;
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
}
