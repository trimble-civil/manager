package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

import org.apache.commons.lang.builder.CompareToBuilder;

@SuppressWarnings("serial")
@Entity
@Table(name = "KONTRAKT_STAB")
@IdClass(KontraktStabPK.class)
@NamedQueries( {
		@NamedQuery(name = KontraktStab.QUERY_FIND_FOR_KONTRAKT, query = "SELECT o FROM KontraktStab o where o.kontraktId = :id"),
		@NamedQuery(name = KontraktStab.QUERY_FIND_FOR_USER, query = "SELECT o FROM KontraktStab o where o.signatur = :signatur"),
		@NamedQuery(name = KontraktStab.QUERY_FIND_BY_SIGNATUR, query = "SELECT o.driftkontrakt FROM KontraktStab o where o.signatur = :signatur") })
public class KontraktStab extends MipssEntityBean<KontraktStab> implements Serializable, IRenderableMipssEntity,
		Comparable<KontraktStab> {
	private static final String ID_ARBEIDSFUNKSJON = "arbeidsfunksjon";
	private static final String ID_BRUKER = "bruker";
	private static final String ID_DRIFTKONTRAKT = "driftkontrakt";
	private static final String[] ID_NAMES = new String[] { ID_BRUKER, ID_ARBEIDSFUNKSJON, ID_DRIFTKONTRAKT };
	public static final String QUERY_FIND_BY_SIGNATUR = "KontraktStab.findBySignatur";
	public static final String QUERY_FIND_FOR_KONTRAKT = "KontraktStab.findForKontrakt";
	public static final String QUERY_FIND_FOR_USER = "KontraktStab.findForUser";
	private Arbeidsfunksjon arbeidsfunksjon;
	private Bruker bruker;
	private Driftkontrakt driftkontrakt;
	private String funksjonNavn;
	private Long kontraktId;
	private String opprettetAv;
	private Date opprettetDato;
	private String signatur;

	public KontraktStab() {
	}

	/** {@inheritDoc} */
	public int compareTo(final KontraktStab o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(bruker, o.bruker).append(driftkontrakt, o.driftkontrakt).append(
				arbeidsfunksjon, o.arbeidsfunksjon).toComparison();
	}

	@ManyToOne
	@JoinColumn(name = "FUNKSJON_NAVN", referencedColumnName = "NAVN", nullable = false)
	public Arbeidsfunksjon getArbeidsfunksjon() {
		return arbeidsfunksjon;
	}

	@ManyToOne
	@JoinColumn(name = "SIGNATUR", referencedColumnName = "SIGNATUR", nullable = false)
	public Bruker getBruker() {
		return bruker;
	}

	@ManyToOne
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	@Id
	@Column(name = "FUNKSJON_NAVN", nullable = false, insertable = false, updatable = false)
	public String getFunksjonNavn() {
		return funksjonNavn;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	@Id
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getKontraktId() {
		return kontraktId;
	}

	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@Id
	@Column(nullable = false, insertable = false, updatable = false)
	public String getSignatur() {
		return signatur;
	}
	
	@Transient
	public String getElrappBruker(){
		return bruker.getElrappIdent();
	}
	public String getTextForGUI() {
		return (bruker != null ? bruker.getTextForGUI() : "") + ", "
				+ (driftkontrakt != null ? driftkontrakt.getTextForGUI() : "") + ", "
				+ (arbeidsfunksjon != null ? arbeidsfunksjon.getTextForGUI() : "");
	}

	public void setArbeidsfunksjon(final Arbeidsfunksjon arbeidsfunksjon) {
		this.arbeidsfunksjon = arbeidsfunksjon;

		if (arbeidsfunksjon != null) {
			setFunksjonNavn(arbeidsfunksjon.getNavn());
		} else {
			setFunksjonNavn(null);
		}
	}

	public void setBruker(final Bruker bruker) {
		this.bruker = bruker;

		if (bruker != null) {
			setSignatur(bruker.getSignatur());
		} else {
			setSignatur(null);
		}
	}

	public void setDriftkontrakt(final Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;

		if (driftkontrakt != null) {
			setKontraktId(driftkontrakt.getId());
		} else {
			setKontraktId(null);
		}
	}

	public void setFunksjonNavn(final String funksjonNavn) {
		this.funksjonNavn = funksjonNavn;
	}

	public void setKontraktId(final Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public void setOpprettetAv(final String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public void setOpprettetDato(final Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setSignatur(final String signatur) {
		this.signatur = signatur;
	}
}
