package no.mesta.mipss.persistence.dokarkiv;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity

@NamedQueries({
	@NamedQuery(name = Dokument.FIND_ALL, query = "select o from Dokument o"),
	@NamedQuery(name = Dokument.FIND_BY_DOKTYPE, query = "select o from Dokument o where o.doktype.navn=:doktypenavn" ),
	@NamedQuery(name = Dokument.FIND_FOR_KJORETOY, query = "select d from Dokument d where d.id in (select kd.dokumentId from KjoretoyDok kd where kd.kjoretoyId = :id)")
})

@SequenceGenerator(name = "dokumentIdSeq", sequenceName = "DOKUMENT_ID_SEQ", initialValue = 1, allocationSize = 1)

@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)

public class Dokument implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String DOKTYPE_BRUKERSTOTTE = "Brukerstotte";
	
	public static final String FIND_ALL = "Dokument.findAll";
	public static final String FIND_BY_DOKTYPE = "Dokument.findByDoktype";
	public static final String FIND_FOR_KJORETOY = "Dokument.finfForKjoretoy";
    private String filsti;
    private String fritekst;
    private Long id;
    private byte[] lob;
    private String navn;
    private Doktype doktype;
    private Dokformat dokformat;
    
    private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
    private OwnedMipssEntity ownedMipssEntity = new OwnedMipssEntity();
    
    public Dokument() {
    }

    public String getFilsti() {
        return filsti;
    }

    public void setFilsti(String filsti) {
        this.filsti = filsti;
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dokumentIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public byte[] getLob() {
        return lob;
    }

    public void setLob(byte[] lob) {
        this.lob = lob;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    @ManyToOne
    @JoinColumn(name = "DOKTYPE_NAVN", referencedColumnName = "NAVN")
    public Doktype getDoktype() {
        return doktype;
    }

    public void setDoktype(Doktype doktype) {
        this.doktype = doktype;
    }

    @ManyToOne
    @JoinColumn(name = "DOKFORMAT_NAVN", referencedColumnName = "NAVN")
    public Dokformat getDokformat() {
        return dokformat;
    }

    public void setDokformat(Dokformat dokformat) {
        this.dokformat = dokformat;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
        append(getId()).
        append(getNavn()).
        append(getDoktype()).
        append(getDokformat()).
        append(getFilsti()).
        toString();
    }
    
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Dokument)) {
            return false;
        }
        Dokument other = (Dokument) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            append(getNavn(), other.getNavn()).
            append(getFilsti(), other.getFilsti()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(11,23).
            append(getId()).
            toHashCode();
    }

    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Dokument other = (Dokument) o;
        return new CompareToBuilder().
            append(getId(), other.getId()).
            toComparison();
    }

    @ManyToMany(mappedBy="dokumentList", fetch = FetchType.LAZY)
	public List<Kjoretoy> getKjoretoyList() {
		return kjoretoyList;
	}

	public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
		this.kjoretoyList = kjoretoyList;
	}

    /** {@inheritDoc} */
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
    	if(ownedMipssEntity == null) {
    		ownedMipssEntity = new OwnedMipssEntity();
    	}
        return ownedMipssEntity;
    }
    
    /**
     * @see #getOwnedMipssEntity()
     * @param ownedMipssEntity
     */
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }


}
