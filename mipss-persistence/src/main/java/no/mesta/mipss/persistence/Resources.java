package no.mesta.mipss.persistence;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class Resources {
	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "persistanceTxts";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
	public static String getResource(String key, String... values){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key, values);
	}
}