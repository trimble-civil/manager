package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@SequenceGenerator(name = "dfulevBestillingIdSeq", sequenceName = "DFULEV_BESTILLING_ID_SEQ", allocationSize = 1, initialValue = 1)
@Entity(name = DfulevBestilling.BEAN_NAME)
@Table(name = "DFULEV_BESTILLING")
public class DfulevBestilling extends MipssEntityBean<DfulevBestilling> {
	public static final String BEAN_NAME = "DfulevBestilling";
	private Long id;
	private Long dfubestmasterId;
	private String leverandorNavn;
	private Date sendtDato;
	private String fritekst;
	private String opprettetAv;
	private Date opprettetDato;
	private Dfubestillingmaster dfubestillingmaster;
	private List<Dfubestvarelinjer> dfubestvarelinjerList = new ArrayList<Dfubestvarelinjer>();
	
	public DfulevBestilling() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dfulevBestillingIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="DFUBESTMASTER_ID", nullable = true, insertable = false, updatable = false)
	public Long getDfubestmasterId() {
		return dfubestmasterId;
	}

	public void setDfubestmasterId(Long dfubestmasterId) {
		this.dfubestmasterId = dfubestmasterId;
	}

	@Column(name="LEVERANDOR_NAVN")
	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		Object oldValue = this.leverandorNavn;
		this.leverandorNavn = leverandorNavn;
		firePropertyChange("leverandorNavn", oldValue, leverandorNavn);
	}

	@Column(name="SENDT_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSendtDato() {
		return sendtDato;
	}

	public void setSendtDato(Date sendtDato) {
		Object oldValue = this.sendtDato;
		this.sendtDato = sendtDato;
		firePropertyChange("sendtDato", oldValue, sendtDato);
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		Object oldValue = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", oldValue, fritekst);
	}

	@Column(name="OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@ManyToOne
	@JoinColumn(name="DFUBESTMASTER_ID", referencedColumnName="ID")
	public Dfubestillingmaster getDfubestillingmaster() {
		return dfubestillingmaster;
	}

	public void setDfubestillingmaster(Dfubestillingmaster dfubestillingmaster) {
		Object oldValue = this.dfubestillingmaster;
		this.dfubestillingmaster = dfubestillingmaster;
		
		if(dfubestillingmaster != null) {
			setDfubestmasterId(dfubestillingmaster.getId());
		} else {
			setDfubestmasterId(null);
		}

		firePropertyChange("dfubestillingmaster", oldValue, dfubestillingmaster);
	}

	@OneToMany(mappedBy = "dfulevBestilling", cascade = CascadeType.ALL)
	public List<Dfubestvarelinjer> getDfubestvarelinjerList() {
		return dfubestvarelinjerList;
	}

	public void setDfubestvarelinjerList(List<Dfubestvarelinjer> dfubestvarelinjerList) {
		Object oldValue = this.dfubestvarelinjerList;
		this.dfubestvarelinjerList = dfubestvarelinjerList;
		firePropertyChange("dfubestvarelinjerList", oldValue, dfubestvarelinjerList);
	}
}
