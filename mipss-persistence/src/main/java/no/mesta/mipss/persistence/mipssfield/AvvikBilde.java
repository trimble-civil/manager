package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Oversikt over bilder for Avvik
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( {
		@NamedQuery(name = AvvikBilde.QUERY_FIND_ALL, query = "SELECT p FROM AvvikBilde p"),
        @NamedQuery(name = AvvikBilde.QUERY_FIND_PUNKT_BILDER, query = "SELECT p FROM AvvikBilde p, Bilde b WHERE p.avvikGuid = :pguid AND p.bildeGuid = b.guid"),
		@NamedQuery(name = AvvikBilde.QUERY_FIND_FOR_AVVIK, query = "SELECT p FROM AvvikBilde p WHERE p.avvikGuid = :pguid") })
@Table(name = "AVVIK_BILDE_XV")
@IdClass(AvvikBildePK.class)
public class AvvikBilde extends MipssEntityBean<AvvikBilde> implements Serializable, Comparable<AvvikBilde> {

	private static final Logger log = LoggerFactory.getLogger(AvvikBilde.class);

	private static final String FIELD_STATUSID = "statusId";
	private static final String FIELD_STATUSTYPE = "statusType";
	private static final String ID_BILDE = "bilde";
	private static final String ID_BILDEGUID = "bildeGuid";
	private static final String ID_AVVIK = "avvik";
	private static final String ID_AVVIKGUID = "avvikGuid";
	private static final String[] NAMES = { ID_BILDE, ID_BILDEGUID, ID_AVVIK, ID_AVVIKGUID };
	public static final String QUERY_FIND_ALL = "AvvikBilde.findAll";
	public static final String QUERY_FIND_FOR_AVVIK = "AvvikBilde.findForAvvik";
	public static final String QUERY_FIND_PUNKT_BILDER = "AvvikBilde.findBilderForPunkt";
	private Bilde bilde;
	private String bildeGuid;
	private Avvik avvik;
	private String avvikGuid;
	private Long statusId;
	private AvvikstatusType statusType;

	/**
	 * Konstruktør
	 * 
	 */
	public AvvikBilde() {
		log("AvvikBilde() constructed");
	}

	/**
	 * <p>
	 * Benytter dato, så status til å sortere.
	 * </p>
	 * 
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * @param o
	 * @return
	 */
	public int compareTo(AvvikBilde o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(avvik, o.avvik).append(statusType, o.statusType).append(bilde,
				o.bilde).toComparison();
	}

	/**
	 * Bilde
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID")
	public Bilde getBilde() {
		return bilde;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(name = "BILDE_GUID", nullable = false, insertable = false, updatable = false)
	public String getBildeGuid() {
		return bildeGuid;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return NAMES;
	}

	/**
	 * Avvik
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "AVVIK_GUID", referencedColumnName = "GUID")
	public Avvik getAvvik() {
		return avvik;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(name = "AVVIK_GUID", nullable = false, insertable = false, updatable = false)
	public String getAvvikGuid() {
		return avvikGuid;
	}

	/**
	 * Status
	 * 
	 * @return
	 */
	@Column(name = "AVVIKSTATUS_ID", nullable = false, insertable = false, updatable = false)
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * Status
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "AVVIKSTATUS_ID", referencedColumnName = "ID")
	public AvvikstatusType getStatusType() {
		return statusType;
	}

	/**
	 * Bilde
	 * 
	 * @param bilde
	 */
	public void setBilde(Bilde bilde) {
		log.trace("setBilde({})", bilde);
		Bilde old = this.bilde;
		this.bilde = bilde;

		if (bilde != null) {
			setBildeGuid(bilde.getGuid());
		} else {
			setBildeGuid(null);
		}

		firePropertyChange(ID_BILDE, old, bilde);
	}

	/**
	 * Id
	 * 
	 * @param bildeGuid
	 */
	public void setBildeGuid(String bildeGuid) {
		log.trace("setBildeGuid({})", bildeGuid);
		String old = this.bildeGuid;
		this.bildeGuid = bildeGuid;
		firePropertyChange(ID_BILDEGUID, old, bildeGuid);
	}

	/**
	 * Avvik
	 * 
	 * @param avvik
	 */
	public void setAvvik(Avvik avvik) {
		log.trace("setAvvik({})", avvik);
		Avvik old = this.avvik;
		this.avvik = avvik;

		if (avvik != null) {
			setAvvikGuid(avvik.getGuid());
		} else {
			setAvvikGuid(null);
		}
		firePropertyChange(ID_AVVIK, old, avvik);
	}

	/**
	 * Id
	 * 
	 * @param avvikGuid
	 */
	public void setAvvikGuid(String avvikGuid) {
		log.trace("setAvvikGuid({})", avvikGuid);
		String old = this.avvikGuid;
		this.avvikGuid = avvikGuid;
		firePropertyChange(ID_AVVIKGUID, old, avvikGuid);
	}

	/**
	 * Status
	 * 
	 * @param statusId
	 */
	public void setStatusId(Long statusId) {
		log.trace("setStatusId({})", statusId);
		Long old = this.statusId;
		this.statusId = statusId;
		firePropertyChange(FIELD_STATUSID, old, statusId);
	}

	/**
	 * Status
	 * 
	 * @param statusType
	 */
	public void setStatusType(AvvikstatusType statusType) {
		log.trace("setStatusType({})", statusType);
		AvvikstatusType old = this.statusType;
		this.statusType = statusType;

		if (statusType != null) {
			setStatusId(statusType.getId());
		} else {
			setStatusId(null);
		}
		firePropertyChange(FIELD_STATUSTYPE, old, statusType);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(ID_BILDEGUID, bildeGuid).append(ID_AVVIKGUID, avvikGuid).append(
				FIELD_STATUSID, statusId).toString();
	}
}
