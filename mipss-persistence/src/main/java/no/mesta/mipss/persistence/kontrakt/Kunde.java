package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/*@Entity

@SequenceGenerator(name = "kundeIdSeq", sequenceName = "KUNDE_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQuery(name = "Kunde.findAll", query = "select o from Kunde o")*/
@SuppressWarnings("serial")
public class Kunde implements Serializable, IRenderableMipssEntity, Comparable<Kunde> {
    private String beskrivelse;
    private String endretAv;
    private Timestamp endretDato;
    private String fritekst;
    private Long id;
    private String navn;
    private String opprettetAv;
    private Timestamp opprettetDato;
    private List<Driftkontrakt> driftkontraktList;

    public Kunde() {
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
        this.beskrivelse = beskrivelse;
    }

    //@Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    //@Column(name="ENDRET_DATO")
    public Timestamp getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Timestamp endretDato) {
        this.endretDato = endretDato;
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    /*@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kundeIdSeq")
    @Column(nullable = false)*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //@Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    //@Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    //@Column(name="OPPRETTET_DATO", nullable = false)
    public Timestamp getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Timestamp opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    //@OneToMany(mappedBy = "kunde")
    public List<Driftkontrakt> getDriftkontraktList() {
        return driftkontraktList;
    }

    public void setDriftkontraktList(List<Driftkontrakt> driftkontraktList) {
        this.driftkontraktList = driftkontraktList;
    }

    /*public Driftkontrakt addDriftkontrakt(Driftkontrakt driftkontrakt) {
        getDriftkontraktList().add(driftkontrakt);
        driftkontrakt.setKunde(this);
        return driftkontrakt;
    }

    public Driftkontrakt removeDriftkontrakt(Driftkontrakt driftkontrakt) {
        getDriftkontraktList().remove(driftkontrakt);
        driftkontrakt.setKunde(null);
        return driftkontrakt;
    }*/
    
    // de neste metodene er håndkodet
    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kunde)) {
            return false;
        }
        Kunde other = (Kunde) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert på Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(79,31).
            append(getId()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    /** {@inheritDoc} */
    public int compareTo(Kunde other) {
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
