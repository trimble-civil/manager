package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssNumberFormatter;
import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kontrakt.kontakt.Kontraktkontakttype;
import no.mesta.mipss.valueobjects.Epostadresse;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = KontraktKontakttype.FIND_ALL, query = "select o from KontraktKontakttype o"),
	@NamedQuery(name = KontraktKontakttype.QUERY_FIND_FOR_KONTRAKT, query = "SELECT o FROM KontraktKontakttype o where o.kontraktId = :id")}
)
@Table(name = "KONTRAKT_KONTAKTTYPE")
@SequenceGenerator(name = KontraktKontakttype.ID_SEQ, sequenceName = "KONTRAKT_KONTAKTTYPE_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})
public class KontraktKontakttype extends MipssEntityBean<KontraktKontakttype> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "KontraktKontakttype.findAll";
	public static final String QUERY_FIND_FOR_KONTRAKT = "KontraktKontakttype.findAllForKontrakt";
	public static final String ID_SEQ = "kontraktKontakttypeIdSeq";
	private final static String[] ID_NAMES = {"kontraktId", "kontakttypeNavn"};
	
	private OwnedMipssEntity ownedMipssEntity = new OwnedMipssEntity();
	
	private Long id;
	private Long kontraktId;
	private String kontakttypeNavn;
	private String kontaktnavn;
	private Long tlfnummer1;
	private Long tlfnummer2;
	private String epostAdresse;
	private Long faksnummer;
	private String beskrivelse;
	
	private Driftkontrakt driftkontrakt;
	private Kontraktkontakttype kontraktkontakttype;
	
	public KontraktKontakttype() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = KontraktKontakttype.ID_SEQ)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	@Column(name = "KONTAKTTYPE_NAVN", nullable = false, insertable = false, updatable = false)
	public String getKontakttypeNavn() {
		return kontakttypeNavn;
	}

	public void setKontakttypeNavn(String kontakttypeNavn) {
		this.kontakttypeNavn = kontakttypeNavn;
	}

	public String getKontaktnavn() {
		return kontaktnavn;
	}
	
	public void setKontaktnavn(String kontaktnavn) {
		Object oldValue = this.kontaktnavn;
		this.kontaktnavn = kontaktnavn;
		getProps().firePropertyChange("kontaktnavn", oldValue, kontaktnavn);
	}

	public Long getTlfnummer1() {
		return tlfnummer1;
	}
	
	@Transient
	public String getTlfnummer1String() {
		return MipssNumberFormatter.formatPhoneNumber(tlfnummer1);
	}

	public void setTlfnummer1(Long tlfnummer1) {
		Object oldValue = this.tlfnummer1;
		this.tlfnummer1 = tlfnummer1;
		getProps().firePropertyChange("tlfnummer1", oldValue, tlfnummer1);
	}
	
	public Long getTlfnummer2() {
		return tlfnummer2;
	}

	@Transient
	public String getTlfnummer2String() {
		return MipssNumberFormatter.formatPhoneNumber(tlfnummer2);
	}
	
	public void setTlfnummer2(Long tlfnummer2) {
		Object oldValue = this.tlfnummer2;
		this.tlfnummer2 = tlfnummer2;
		getProps().firePropertyChange("tlfnummer2", oldValue, tlfnummer2);
	}
	
	@Column(name = "EPOST_ADRESSE")
	public String getEpostAdresse() {
		return epostAdresse;
	}

	@Transient
	public Epostadresse getEpost() {
		if(epostAdresse != null) {
			return new Epostadresse(epostAdresse);
		} else {
			return null;
		}
	}
	
	public void setEpostAdresse(String epostAdresse) {
		Object oldEpost = getEpost();
		Object oldValue = this.epostAdresse;
		this.epostAdresse = epostAdresse;
		getProps().firePropertyChange("epostAdresse", oldValue, epostAdresse);
		getProps().firePropertyChange("epost", oldEpost, getEpost());
	}

	public Long getFaksnummer() {
		return faksnummer;
	}

	@Transient
	public String getFaksnummerString() {
		return MipssNumberFormatter.formatPhoneNumber(faksnummer);
	}
	
	public void setFaksnummer(Long faksnummer) {
		Object oldValue = this.faksnummer;
		this.faksnummer = faksnummer;
		getProps().firePropertyChange("faksnummer", oldValue, faksnummer);
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		Object oldValue = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		getProps().firePropertyChange("beskrivelse", oldValue, beskrivelse);
	}

	@ManyToOne
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		Object oldValue = this.driftkontrakt;
		this.driftkontrakt = driftkontrakt;
		
		if(driftkontrakt != null) {
			setKontraktId(driftkontrakt.getId());
		} else {
			setKontraktId(null);
		}
		
		getProps().firePropertyChange("driftkontrakt", oldValue, driftkontrakt);
	}

	@ManyToOne
	@JoinColumn(name = "KONTAKTTYPE_NAVN", referencedColumnName = "NAVN", nullable = false)
	public Kontraktkontakttype getKontraktkontakttype() {
		return kontraktkontakttype;
	}

	public void setKontraktkontakttype(Kontraktkontakttype kontraktkontakttype) {
		Object oldValue = this.kontraktkontakttype;
		this.kontraktkontakttype = kontraktkontakttype;
		
		if(kontraktkontakttype != null) {
			setKontakttypeNavn(kontraktkontakttype.getNavn());
		} else {
			setKontakttypeNavn(null);
		}
		
		getProps().firePropertyChange("kontraktkontakttype", oldValue, kontraktkontakttype);
	}

	public String getTextForGUI() {
		return getKontaktnavn() + " (" + getKontakttypeNavn() + ")";
	}

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}
	
	/** {@inheritDoc} */
	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		return ownedMipssEntity;
	}
	
	/**
	 * @see #getOwnedMipssEntity()
	 * @param ownedMipssEntity
	 */
	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		log("setOwnedMipssEntity", ownedMipssEntity);
		this.ownedMipssEntity = ownedMipssEntity;
	}
	
    @Override
    public int hashCode() {
        return new HashCodeBuilder(3,5).
            append(kontraktId).
            append(kontakttypeNavn).
            toHashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof KontraktKontakttype)) {return false;}
        if(o == this) { return true;}
        
        KontraktKontakttype other = (KontraktKontakttype) o;
        EqualsBuilder equals = new EqualsBuilder().
            append(kontraktId, other.getKontraktId()).
            append(kontakttypeNavn, other.getKontakttypeNavn()).
            append(kontaktnavn, other.kontaktnavn);
        
        if (id!=null&&other.id!=null){
        	equals.append(id, other.id);
        }
        return equals.isEquals();
            
    }
}
