package no.mesta.mipss.persistence.rapportfunksjoner;

import java.util.Calendar;
import java.util.Date;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;

public class DatakvalitetHelper {

	private Date threeDaysAgo;
	private Date twoWeeks;
	private Date now;

	public DatakvalitetHelper(){
		now = Clock.now();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.roll(Calendar.DAY_OF_YEAR, -3);
		threeDaysAgo = cal.getTime();
		cal.roll(Calendar.DAY_OF_YEAR, -11);
		twoWeeks = cal.getTime();
	}
	/**
	 * 
	 * @param sisteLivstegn
	 * @return
	 */
	public boolean checkSisteLivstegn3d(Date sisteLivstegn){
		if (sisteLivstegn==null){
			return false;
		}
		return sisteLivstegn.before(threeDaysAgo)&&(!sisteLivstegn.before(twoWeeks));
	}
	/**
	 * 
	 * @param sisteLivstegn
	 * @return
	 */
	public boolean checkSisteLivstegn14d(Date sisteLivstegn){
		if (sisteLivstegn==null){
			return true;
		}
		return sisteLivstegn.before(twoWeeks);
	}
	
	public boolean checkSisteGPSPosisjon3d(Date sistePosisjonDato){
		if (sistePosisjonDato==null)
			return false;
		return sistePosisjonDato.before(threeDaysAgo)&&(!sistePosisjonDato.before(twoWeeks));
	}
	public boolean checkSisteGPSPosisjon14d(Date sistePosisjonDato){
		if (sistePosisjonDato==null)
			return true;
		return sistePosisjonDato.before(twoWeeks);
	}
	
	public boolean checkManuellSprederUtenGyldigStroperiode(KontraktKjoretoyInfo info){
		boolean sprUtenDatafangst = info.getSprUtenDatafangstFlagg()!=null?info.getSprUtenDatafangstFlagg().intValue()==1?true:false:false;
		if (!sprUtenDatafangst)
			return false;
		Date fraDato = info.getAktuellStroperiodeFraDato();
		Date tilDato = info.getAktuellStroperiodeTilDato();
		
		if (fraDato==null)
			return true;
		
		if (fraDato.after(now))
			return true;
		
		if (tilDato!=null&&tilDato.before(now))
			return true;
		
		return false;
		
	}
	public boolean checkUkjentProduksjon(Boolean ukjentProduksjon) {
		if (ukjentProduksjon==null)
			return false;
		return ukjentProduksjon.booleanValue();
	}
	public boolean checkUkjentStrometode(Boolean ukjentStrometode) {
		if (ukjentStrometode==null)
			return false;
		return ukjentStrometode.booleanValue();
	}
	public boolean checkIkkeProduksjonssatteBestillinger(KontraktKjoretoyInfo info) {
		String status = "Deler sendt";
		if (status.equals(info.getBestillingStatus())){
			if (info.getBestillingStatusDato()!=null&&info.getBestillingStatusDato().before(twoWeeks)){
				return true;
			}
		}
		return false;
	}
}
