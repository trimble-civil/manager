/**
 * 
 */
package no.mesta.mipss.persistence;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Delklasse for alle MIPSS entiteter som har felt med navn
 * {@code opprettetAv}, {@code opprettetDato}.
 * 
 * Datofeltene kan ikke oppdateres på GUI: de hentes fra databasen og settes automatisk rett før 
 * persistering/oppdatering, derfor trenger man ikke å sende eventer på endringene de gjennomgår.
 * 
 * Klassen inkluderer også et eget felt for angivelse av brukerens identifikasjon for bruk på klientsiden.

 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Embeddable
public class MipssEntityCreator implements Serializable {
	private static final String datoformat = "dd.MM.yyyy";
	
    private String signatur;
    
    private String opprettetAv;
    private Date opprettetDato;
    
    public MipssEntityCreator() {}
    
    /**
     * Setter brukerens identifikasjon. Denne identifikasjonen lagres i feltene
     * OPPRETTET_AV og ENDRET_AV og kan ikke overstide dens lengde.
     * @param signatur
     */
    public void setUserSignatur(String signatur) {
        this.signatur = signatur;
    }

    /**
     * Henter brukerens signatur. 
     * @return signaturen eller "IkkeSatt" hvis feltet ikke var satt på forhånd.
     */
    @Transient
    public String getUserSignatur() {
        if (signatur == null) {
            signatur = "IkkeSatt";
        }
        return signatur;
    }
    /**
     * henter brukersignatur for opprettelsen
     * @return
     */
    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Setter brukersignatur for opprettelsen
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    /**
     * henter tidsstempel for opprettelsen
     * @return
     */
    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * setter tidsstempel for opprettelsen
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }
    
    /**
     * @return opprettelsesdato som tekst. Hvis opprettelsesdato ikke er satt,
     * returneres dagens dato.
     */
    @Transient
    public String getOpprettetDatoString() {
    	SimpleDateFormat formatter = new SimpleDateFormat(datoformat);
    	if(opprettetDato == null) {
    		return formatter.format(Clock.now());
    	}
        return formatter.format(opprettetDato);
    }
}
