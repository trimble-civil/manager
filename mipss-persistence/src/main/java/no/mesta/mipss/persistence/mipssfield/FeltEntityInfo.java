package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
/**
 * Minimalt med informasjon om en mipss-felt entitet.
 * 
 * Mipss-felt entiteter:
 * Sak
 * Avvik
 * Hendelse
 * Forsikringsskade
 * Skred
 * 
 */
public class FeltEntityInfo implements Serializable{
	public static final String MAPPING_NAME = "FeltEntityInfo.defaultMapping";
	private String guid;
	private Long refnummer;
	private Date slettetDato;
	
	public FeltEntityInfo(String guid, Long refnummer, Date slettetDato){
		this.guid = guid;
		this.refnummer = refnummer;
		this.slettetDato = slettetDato;
	}
	public String getGuid() {
		return guid;
	}
	public Long getRefnummer() {
		return refnummer;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}
	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}
	public Date getSlettetDato() {
		return slettetDato;
	}

}
