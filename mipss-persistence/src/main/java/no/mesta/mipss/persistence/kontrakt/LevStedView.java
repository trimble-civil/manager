package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = LevStedView.QUERY_FIND_ALL, query = "SELECT o FROM LevStedView o"),
    @NamedQuery(name = LevStedView.QUERY_FIND_INAKTIVDATE, query = "SELECT o.levStedInaktivDato FROM LevStedView o where o.levNr=:leverandorNr")
    
    })
@Table(name = "LEV_STED_V")
@IdClass(LevStedViewPK.class)
public class LevStedView implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "LevStedView.findAll";
	public static final String QUERY_FIND_INAKTIVDATE = "LevStedView.findInaktivDate";
	
	private Long levId;
	private String levNr;
	private String levNavn;
	private String levAktiv;
	private String orgNr;
	private Long stedId;
	private String stedNavn;
	private String betalingssted;
	private String innkjopssted;
	private String adresse1;
	private String adresse2;
	private String adresse3;
	private String postnr;
	private String poststed;
	private String tlf;
	private String faksno;
	private Timestamp levStedInaktivDato;
	
	public LevStedView() {
	}
	
	@Id
	@Column(name = "LEV_ID")
	public Long getLevId() {
		return levId;
	}

	public void setLevId(Long levId) {
		this.levId = levId;
	}
	
	@Column(name = "LEV_NR")
	public String getLevNr() {
		return levNr;
	}

	public void setLevNr(String levNr) {
		this.levNr = levNr;
	}
	
	@Column(name = "LEV_NAVN")
	public String getLevNavn() {
		return levNavn;
	}

	public void setLevNavn(String levNavn) {
		this.levNavn = levNavn;
	}

	@Column(name = "LEV_AKTIV")
	public String getLevAktiv() {
		return levAktiv;
	}

	public void setLevAktiv(String levAktiv) {
		this.levAktiv = levAktiv;
	}

	@Column(name = "ORG_NR")
	public String getOrgNr() {
		return orgNr;
	}

	public void setOrgNr(String orgNr) {
		this.orgNr = orgNr;
	}

	@Id
	@Column(name = "STED_ID")
	public Long getStedId() {
		return stedId;
	}

	public void setStedId(Long stedId) {
		this.stedId = stedId;
	}

	@Column(name = "STED_NAVN")
	public String getStedNavn() {
		return stedNavn;
	}

	public void setStedNavn(String stedNavn) {
		this.stedNavn = stedNavn;
	}

	public String getBetalingssted() {
		return betalingssted;
	}

	public void setBetalingssted(String betalingssted) {
		this.betalingssted = betalingssted;
	}

	public String getInnkjopssted() {
		return innkjopssted;
	}

	public void setInnkjopssted(String innkjopssted) {
		this.innkjopssted = innkjopssted;
	}

	@Column(name = "ADRESSE_1")
	public String getAdresse1() {
		return adresse1;
	}

	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	@Column(name = "ADRESSE_2")
	public String getAdresse2() {
		return adresse2;
	}

	public void setAdresse2(String adresse2) {
		this.adresse2 = adresse2;
	}

	@Column(name = "ADRESSE_3")
	public String getAdresse3() {
		return adresse3;
	}

	public void setAdresse3(String adresse3) {
		this.adresse3 = adresse3;
	}

	public String getPostnr() {
		return postnr;
	}

	public void setPostnr(String postnr) {
		this.postnr = postnr;
	}

	public String getPoststed() {
		return poststed;
	}

	public void setPoststed(String poststed) {
		this.poststed = poststed;
	}

	public String getTlf() {
		return tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	public String getFaksno() {
		return faksno;
	}

	public void setFaksno(String faksno) {
		this.faksno = faksno;
	}

	@Column(name = "LEV_STED_INAKTIV_DATO")
	public Timestamp getLevStedInaktivDato() {
		return levStedInaktivDato;
	}

	public void setLevStedInaktivDato(Timestamp levStedInaktivDato) {
		this.levStedInaktivDato = levStedInaktivDato;
	}

	public String getTextForGUI() {
		return getLevNavn();
	}
	
}
