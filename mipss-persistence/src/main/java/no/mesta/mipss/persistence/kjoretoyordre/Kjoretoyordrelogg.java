package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA entitet generert i JDeveloper.
 * Kan ikke bruke MipssEntityCreator fordi opprettetDato er en del av n�kkelen.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity

@NamedQueries({
	@NamedQuery(name = Kjoretoyordrelogg.FIND_ALL, query = "select o from Kjoretoyordrelogg o order by o.opprettetDato")
})

@IdClass(KjoretoyordreloggPK.class)
public class Kjoretoyordrelogg extends MipssEntityBean<Kjoretoyordrelogg> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kjoretoyordrelogg.findAll";
	public static final String[] ID_NAMES = {"ordreId", "opprettetDato"};
	
    private String opprettetAv;
    private Date opprettetDato;
    private Long ordreId;
    private String tekst;
    private Kjoretoyordre kjoretoyordre;

    public Kjoretoyordrelogg() {
    }

    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Id
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
    	if(opprettetDato == null) {
    		opprettetDato = Clock.now();
    	}
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @Id
    @Column(name="ORDRE_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getOrdreId() {
        return ordreId;
    }

    public void setOrdreId(Long ordreId) {
        this.ordreId = ordreId;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    @ManyToOne
    @JoinColumn(name = "ORDRE_ID", referencedColumnName = "ID")
    public Kjoretoyordre getKjoretoyordre() {
        return kjoretoyordre;
    }

    public void setKjoretoyordre(Kjoretoyordre kjoretoyordre) {
        this.kjoretoyordre = kjoretoyordre;
        
        if(kjoretoyordre != null) {
        	setOrdreId(kjoretoyordre.getId());
        } else {
        	setOrdreId(null);
        }
    }

    @Transient
	public String getTextForGUI() {
		return getOpprettetDato() + " - " + getOpprettetAv();
	}
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("ordre i", getOrdreId()).
            append("Opprettet den", getOpprettetDato()).
            append("Opprettet av", getOpprettetAv()).
            toString();
    }

    /**
     * Sammenlikner Id-n�kkelen med o's Id-n�kkel.
     * @param o
     * @return true hvis n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kjoretoyordrelogg)) {
            return false;
        }
        Kjoretoyordrelogg other = (Kjoretoyordrelogg) o;
        return new EqualsBuilder().
            append(getOrdreId(), other.getOrdreId()).
            append(getOpprettetDato(), other.getOpprettetDato()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun p� Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getOrdreId()).
            append(getOpprettetDato()).
            append(getOpprettetAv()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	Kjoretoyordrelogg other = (Kjoretoyordrelogg) o;
		return new CompareToBuilder().
			append(getOpprettetDato(), other.getOpprettetDato()).
			toComparison();
    }

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

}
