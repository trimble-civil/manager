package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;

@SuppressWarnings("serial")
public class InspeksjonStatF implements Serializable{

	private String veikategori;
	private Double veinettLengde;
	private Double dekningsgrad;
	private Double inspKm;
	
	public static final String[] getPropertiesArray(){
		return new String[]{"veikategori", "veinettLengde", "inspKm"};
	}
	
	/**
	 * @return the veikategori
	 */
	public String getVeikategori() {
		return veikategori;
	}
	/**
	 * @return the veinettLengde
	 */
	public Double getVeinettLengde() {
		return veinettLengde;
	}
	/**
	 * @return the dekningsgrad
	 */
	public Double getDekningsgrad() {
		return dekningsgrad;
	}
	/**
	 * @param veikategori the veikategori to set
	 */
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}
	/**
	 * @param veinettLengde the veinettLengde to set
	 */
	public void setVeinettLengde(Double veinettLengde) {
		this.veinettLengde = veinettLengde;
	}
	/**
	 * @param dekningsgrad the dekningsgrad to set
	 */
	public void setDekningsgrad(Double dekningsgrad) {
		this.dekningsgrad = dekningsgrad;
	}

	public void setInspKm(Double inspKm) {
		this.inspKm = inspKm;
	}

	public Double getInspKm() {
		return inspKm;
	}
	
	
}
