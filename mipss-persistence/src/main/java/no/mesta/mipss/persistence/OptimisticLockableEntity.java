package no.mesta.mipss.persistence;

/**
 * Makes a class optimistically lockable using a <tt>VersionColumn</tt>.
 * 
 * @author Christian Wiik (Mesan)
 */
public interface OptimisticLockableEntity {
	
	VersionColumn getVersion();

}