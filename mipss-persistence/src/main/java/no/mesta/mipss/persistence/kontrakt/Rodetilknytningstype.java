package no.mesta.mipss.persistence.kontrakt;

import no.mesta.mipss.persistence.MipssEntityBean;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "rodetilknytningstypeIdSeq", sequenceName = "RODETILKNYTNINGSTYPE_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
        @NamedQuery(name = Rodetilknytningstype.FIND_BY_RODETILKNYTNINGSTYPE_ID, query = "select o from Rodetilknytningstype o where o.id = :id"),
        @NamedQuery(name = Rodetilknytningstype.FIND_ALL, query = "select o from Rodetilknytningstype o")
})
@Table(name = "RODETILKNYTNINGSTYPE")
public class Rodetilknytningstype extends MipssEntityBean<Rodetilknytningstype> {

    public static final String FIND_BY_RODETILKNYTNINGSTYPE_ID = "Rodetilknytningstype.findByRodetilknytningstypeId";
    public static final String FIND_ALL = "Rodetilknytningstype.findAll";

    private Long id;
    private String navn;
    private String beskrivelse;
    private String opprettetAv;
    private Date opprettetDato;
    private String endretAv;
    private Date endretDato;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rodetilknytningstypeIdSeq")
    @Column(name = "ID", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NAVN")
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name = "BESKRIVELSE")
    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
        String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Column(name = "OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name = "OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @Column(name = "ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name = "ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    @Override
    public String getTextForGUI(){
        return navn;
    }

}