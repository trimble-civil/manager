package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Ikon;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Menypunkt.QUERY_FIND_ALL, query = "SELECT m FROM Menypunkt m where m.synligFlagg=1")
})
public class Menypunkt extends MipssEntityBean<Menypunkt> implements Serializable {
    public static final String QUERY_FIND_ALL = "Menypunkt.findAll";
    
    private List<Brukermeny> brukermenyList;
    private String endretAv;
    private Date endretDato;
    private Long id;
    private Ikon ikon;
    private Menygruppe menygruppe;
    private String opprettetAv;
    private Date opprettetDato;
    private Boolean synligFlagg;
    private Tilgangspunkt tilgangspunkt;
    private String tresti;
    private String url;
    private Boolean valgbarFlagg;
    private String visningsnavn;

    /**
     * Konstruktør
     * 
     */
    public Menypunkt() {
    }
    
    public Brukermeny addBrukermeny(Brukermeny brukermeny) {
        getBrukermenyList().add(brukermeny);
        brukermeny.setMenypunkt(this);
        return brukermeny;
    }

    /**
     * @see java.lang.Object#equals(Object)
     * 
     * @param o
     * @return
     */
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Menypunkt)) {return false;}
        
        Menypunkt other = (Menypunkt) o;
        
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    /**
     * Listen over brukermenyer denne inngår i
     * 
     * @return
     */
    @OneToMany(mappedBy = "menypunkt")
    public List<Brukermeny> getBrukermenyList() {
        return brukermenyList;
    }
    /**
     * Hvem utførte endringen
     * 
     * @return
     */
    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }
    /**
     * Når ble den endret
     * 
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }
    /**
     * Unik id
     * 
     * @return
     */
    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Ikonet som benyttes
     * 
     * @return
     */
    @ManyToOne(cascade=CascadeType.REFRESH)
    @JoinColumn(name = "IKON_ID", referencedColumnName = "ID")
    public Ikon getIkon() {
        return ikon;
    }

    /**
     * Menygruppen
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "GRUPPE_ID", referencedColumnName = "ID")
    public Menygruppe getMenygruppe() {
        return menygruppe;
    }

    /**
     * Hvem opprettet den
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Når ble den opprettet
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }


    /**
     * Returnerer alltid true eller false aldri null
     * @return
     */
    @Transient
    public Boolean getSynlig(){
    	return (synligFlagg != null && synligFlagg.equals(1L) ? Boolean.valueOf(true) : Boolean.valueOf(false));
    }

    @Column(name = "SYNLIG_FLAGG", nullable = false)
	public Boolean getSynligFlagg() {
		return synligFlagg;
	}

    /**
     * Tilgangspunktet til menyen
     * 
     * @return
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TILGANGSPUNKT_ID", referencedColumnName = "ID")
    public Tilgangspunkt getTilgangspunkt() {
        return tilgangspunkt;
    }

    /**
     * Visningssti i menyen
     * 
     * @return
     */
    @Column(nullable = false)
    public String getTresti() {
        return tresti;
    }

    /**
     * URL for websider
     * 
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returnerer alltid true eller false aldri null
     * @return
     */
    @Transient
    public Boolean getValgbar(){
    	return (valgbarFlagg != null && valgbarFlagg.equals(1L) ? Boolean.valueOf(true) : Boolean.valueOf(false));
    }

    @Column(name = "VALGBAR_FLAGG", nullable = false)
	public Boolean getValgbarFlagg() {
		return valgbarFlagg;
	}

    /**
     * Visningsnavn
     * 
     * @return
     */
    @Column(nullable = false)
    public String getVisningsnavn() {
        return visningsnavn;
    }

    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2003,2097).
            append(id).toHashCode();
    }

    public Brukermeny removeBrukermeny(Brukermeny brukermeny) {
        getBrukermenyList().remove(brukermeny);
        brukermeny.setMenypunkt(null);
        return brukermeny;
    }

    /**
     * Listen over brukermenyer denne inngår i
     * 
     * @param brukermenyList
     */
    public void setBrukermenyList(List<Brukermeny> brukermenyList) {
        this.brukermenyList = brukermenyList;
    }

    /**
     * Hvem utførte endringen
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
    	String old = this.endretAv;
        this.endretAv = endretAv;
        firePropertyChange("endretAv", old, endretAv);
    }

    /**
     * Når ble den endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
    	Date old = this.endretDato;
        this.endretDato = endretDato;
        firePropertyChange("endretDato", old, endretDato);
    }

    /**
     * Unik id
     * 
     * @param id
     */
    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    /**
     * Ikonet som benyttes
     * 
     * @param ikon
     */
    public void setIkon(Ikon ikon) {
    	Ikon old = this.ikon;
        this.ikon = ikon;
        firePropertyChange("ikon", old, ikon);
    }

    /**
     * Menygruppen
     * 
     * @param menygruppe
     */
    public void setMenygruppe(Menygruppe menygruppe) {
    	Menygruppe old = this.menygruppe;
        this.menygruppe = menygruppe;
        firePropertyChange("menygruppe", old, menygruppe);
    }

    /**
     * Hvem opprettet den
     * 
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
    	String old = this.opprettetAv;
        this.opprettetAv = opprettetAv;
        firePropertyChange("opprettetAv", old, opprettetAv);
    }

    /**
     * Når ble den opprettet
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
    	Date old = opprettetDato;
        this.opprettetDato = opprettetDato;
        firePropertyChange("opprettetDato", old, opprettetDato);
    }

    public void setSynligFlagg(Boolean synligFlagg){
    	Boolean old = this.synligFlagg;
    	this.synligFlagg = synligFlagg;
    	firePropertyChange("synligFlagg", old, synligFlagg);
    }

    /**
     * Tilgangspunktet til menyen
     * 
     * @param tilgangspunkt
     */
    public void setTilgangspunkt(Tilgangspunkt tilgangspunkt) {
    	Tilgangspunkt old = this.tilgangspunkt;
        this.tilgangspunkt = tilgangspunkt;
        firePropertyChange("tilgangspunkt", old, tilgangspunkt);
    }

    /**
     * Visningsti i menyen
     * 
     * @param tresti
     */
    public void setTresti(String tresti) {
        String old = tresti;
    	this.tresti = tresti;
    	firePropertyChange("tresti", old, tresti);
    }

    /**
     * URL for websider
     * 
     * @param url
     */
    public void setUrl(String url) {
    	String old = this.url;
        this.url = url;
        firePropertyChange("url", old, url);
    }
    
    public void setValgbarFlagg(Boolean valgbarFlagg){
    	Boolean old = this.valgbarFlagg;
    	this.valgbarFlagg = valgbarFlagg;
    	firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }
    
    /**
     * Visningsnavn
     * 
     * @param visningsnavn
     */
    public void setVisningsnavn(String visningsnavn) {
    	String old = this.visningsnavn;
        this.visningsnavn = visningsnavn;
        firePropertyChange("visningsnavn", old, visningsnavn);
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("visningsnavn", visningsnavn).
            append("tresti", tresti).
            append("url", url).
            append("ikon", ikon).
            append("tilgangspunkt", tilgangspunkt).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            append("menygruppe", menygruppe).
            toString();
    }
}
