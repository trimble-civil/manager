package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Generert i JDeveloper. VIKTIG: Datamodellen er fortsatt under revidereing,
 * derfor ventes det med tilpasning av den genererte koden.
 * @author jorge
 *
 */
/*@Entity

@NamedQuery(name = Ansvarssted.FIND_ALL, query = "select o from Ansvarssted o order by o.navn")

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})*/

@SuppressWarnings("serial")
public class Ansvarssted implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "Ansvarssted.findAll";
	
    private Long id;
    private String navn;
    private Selskap selskap;
    private List<Prosjekt> prosjektList = new ArrayList<Prosjekt>();
    private OwnedMipssEntity ownedMipssEntity;

    public Ansvarssted() {
    }

    /*@Id
    @Column(nullable = false)*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //@Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    /*@ManyToOne
    @JoinColumn(name = "SELSKAP_ID", referencedColumnName = "ID")*/
    public Selskap getSelskap() {
        return selskap;
    }

    public void setSelskap(Selskap selskap) {
        this.selskap = selskap;
    }

    //@OneToMany(mappedBy = "ansvarssted")
    public List<Prosjekt> getProsjektList() {
        return prosjektList;
    }

    public void setProsjektList(List<Prosjekt> prosjektList) {
        this.prosjektList = prosjektList;
    }

    public Prosjekt addProsjekt(Prosjekt prosjekt) {
        getProsjektList().add(prosjekt);
        prosjekt.setAnsvarssted(this);
        return prosjekt;
    }

    public Prosjekt removeProsjekt(Prosjekt prosjekt) {
        getProsjektList().remove(prosjekt);
        prosjekt.setAnsvarssted(null);
        return prosjekt;
    }
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("Id", getId()).
            append("navn", getNavn()).
            toString();
    }

    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Ansvarssted)) {
            return false;
        }
        Ansvarssted other = (Ansvarssted) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun på Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getId()).
            append(getNavn()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	Ansvarssted other = (Ansvarssted) o;
		return new CompareToBuilder().
			append(getNavn(), other.getNavn()).
			toComparison();
    }

	/**
	 * @return ownedMipssEntity
	 */
	public OwnedMipssEntity getOwnedMipssEntity() {
		if(ownedMipssEntity == null) {
			ownedMipssEntity = new OwnedMipssEntity();
		}
		return ownedMipssEntity;
	}

	/**
	 * @param ownedMipssEntity the ownedMipssEntity to set
	 */
	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		this.ownedMipssEntity = ownedMipssEntity;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		return getNavn();
	}

}
