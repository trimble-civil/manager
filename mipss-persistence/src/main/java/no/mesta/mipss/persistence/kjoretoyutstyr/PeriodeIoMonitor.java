/**
 * 
 */
package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import no.mesta.mipss.persistence.exceptions.IolisteOverlappingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kontrollerer om det finnes overlappende perioder for portene som er i bruk i 
 * {@code Kjoretoy} hvor dette {@KjoretoyUtstyr} er montert.
 * 
 * Prosedyren går ut på å samle alle perioder for {@KjoretoyUtstyr} på kjøretøyet
 * etter portnumrene. Man kan da kontrollere at for en gitt port ikke skal finnes kolliderende 
 * perioder.
 * 
 * Denne klassen brukes kun på serversiden for å slå av alle Iolister som har overlappende perioder.
 * 
 * BUG: Vær oppmerksom på at det kan finnes forekomster hvor denne prosedyren 
 * finner falske overlappinger. Disse skal forekomme så skjeldent at man kan takle dem
 * på andre måter.
 * 
 * @author jorge
 *
 */
public class PeriodeIoMonitor {
	private Logger logger = LoggerFactory.getLogger(PeriodeIoMonitor.class);
	
	/**
	 * constructor brukt av JPA på serveren
	 */
	public PeriodeIoMonitor(){}
	
	/**
	 * Kontrollerer om det finnes overlappinger mellom {@code Utstyrperiode}r per port.
	 * @param entity
	 * @throws IllegalStateException hvis entity mangler sitt kjøretøy.
	 * @throws IolisteOverlappingException hvis en overlapping funnet.
	 */
	@PrePersist
	@PreUpdate
	public void monitorPeriodeIo(KjoretoyUtstyr entity) {
		
		if(entity.getKjoretoy() == null) {
			throw new IllegalStateException("Utstyrperiode mangler sitt Kjøretøy: " + entity);
		}

		// hvis det finnes definerte portnummer
		if(entity.getKjoretoy().getKjoretoyUtstyrList() != null) {

		}
	}
}
