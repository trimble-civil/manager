package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "Ioteknologi.findAll", 
    query = "select o from Ioteknologi o")
public class Ioteknologi extends MipssEntityBean<Ioteknologi> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Ioteknologi.findAll";
    private String fritekst;
    private String navn;
    private List<Ioliste> iolisteList = new ArrayList<Ioliste>();

    public Ioteknologi() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @OneToMany(mappedBy = "ioteknologi")
    public List<Ioliste> getIolisteList() {
        return iolisteList;
    }

    public void setIolisteList(List<Ioliste> iolisteList) {
        this.iolisteList = iolisteList;
    }

    public Ioliste addIoliste(Ioliste ioliste) {
        getIolisteList().add(ioliste);
        ioliste.setIoteknologi(this);
        return ioliste;
    }

    public Ioliste removeIoliste(Ioliste ioliste) {
        getIolisteList().remove(ioliste);
        ioliste.setIoteknologi(null);
        return ioliste;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Ioteknologi)) {
            return false;
        }
        Ioteknologi other = (Ioteknologi) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getNavn()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Ioteknologi other = (Ioteknologi) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
