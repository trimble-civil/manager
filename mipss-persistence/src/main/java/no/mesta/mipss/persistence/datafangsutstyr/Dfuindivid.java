package no.mesta.mipss.persistence.datafangsutstyr;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = "dfuindividIdSeq", sequenceName = "DFUINDIVID_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries( {
		@NamedQuery(name = Dfuindivid.QUERY_FIND_ALL, query = "select o from Dfuindivid o order by o.serienummer"),
		// @NamedQuery(name = Dfuindivid.FIND_FOR_KJORETOY, query =
		// "select o from Dfuindivid o where o.id = (select i.dfuId from Installasjon i where i.kjoretoy.id = :id)"),
		@NamedQuery(name = Dfuindivid.QUERY_FIND_PDA_BY_KONTRAKT, query = "SELECT dfu FROM Dfuindivid dfu "
				+ "JOIN dfu.dfuKontraktList dk " + "WHERE dk.kontraktId=:kontraktId"),
		@NamedQuery(name = Dfuindivid.FIND_FOR_KJORETOY, query = "select o from Dfuindivid o " +
																	"where o.id in " +
																	"(select i.dfuId from Installasjon i where i.kjoretoy.id = :id " +
																	"and (" +
	    																"(i.aktivTilDato>=:now " +
	    																"and i.aktivFraDato<=:now) " +
	    																"or " +
	    																"(i.aktivTilDato is null " +
	    																"and i.aktivFraDato <=:now)" +
    																"))"),
				
		@NamedQuery(name = Dfuindivid.FIND_BY_KONTRAKT, query = "select o.dfuindivid from DfuKontrakt o where o.kontraktId=:kontraktId"),
		@NamedQuery(name = Dfuindivid.FIND_FOR_ADMIN, query = "select distinct o from Dfuindivid o left join fetch o.dfuloggList left join fetch o.installasjonList left join fetch o.dfuKontraktList where o.id=:id"),
		@NamedQuery(name = Dfuindivid.FIND_BY_LEVERANDOR, query = "select o from Dfuindivid o join o.dfumodell m where m.leverandorNavn=:leverandorNavn")

})
public class Dfuindivid implements Serializable, IRenderableMipssEntity, Comparable<Dfuindivid>, PropertyChangeSource {
	public static final String FIND_BY_KONTRAKT = "Dfuindivid.findByKontrakt";
	public static final String FIND_FOR_KJORETOY = "Dfuindivid.findForKjoretoy";
	public static final String QUERY_FIND_ALL = "Dfuindivid.findAll";
	public static final String QUERY_FIND_PDA_BY_KONTRAKT = "Dfuindivid.findPdaByKontrakt";
	public static final String FIND_FOR_ADMIN = "Dfuindivid.findForAdmin";
	public static final String FIND_BY_LEVERANDOR = "Dfuindivid.findByLeverandor";
	private Date anskaffetDato;
	private Dfustatus currentStatus;
	private List<Dfu_Status> dfu_StatusList = new ArrayList<Dfu_Status>();
	private List<DfuKontrakt> dfuKontraktList = new ArrayList<DfuKontrakt>();
	private List<Dfulogg> dfuloggList = new ArrayList<Dfulogg>();
	private Dfumodell dfumodell;
	private String endretAv;
	private Date endretDato;
	private String fritekst;
	private Long id;
	private List<Installasjon> installasjonList = new ArrayList<Installasjon>();
	private String opprettetAv;
	private Date opprettetDato;
	private transient PropertyChangeSupport props;
	private String serienummer;
	private String simkortnummer;
	private Long slettetFlagg;
	private Long tlfnummer;
	private String versjon;
	private Dfuinfo dfuinfo;
	private String modellNavn;
	private Long programvareversjon;
	
	public Dfuindivid() {
	}

	@Column(name="MODELL_NAVN", nullable = true, insertable = false, updatable = false)
	public String getModellNavn() {
		return modellNavn;
	}

	public void setModellNavn(String modellNavn) {
		this.modellNavn = modellNavn;
	}

	public Dfu_Status addDfu_Status(Dfu_Status dfu_Status) {
		getDfu_StatusList().add(dfu_Status);
		dfu_Status.setDfuindivid(this);
		return dfu_Status;
	}

	public DfuKontrakt addDfuKontrakt(DfuKontrakt dfuKontrakt) {
		getDfuKontraktList().add(dfuKontrakt);
		dfuKontrakt.setDfuindivid(this);
		return dfuKontrakt;
	}

	public Dfulogg addDfulogg(Dfulogg dfulogg) {
		getDfuloggList().add(dfulogg);
		dfulogg.setDfuindivid(this);
		return dfulogg;
	}

	public Installasjon addInstallasjon(Installasjon installasjon) {
		getInstallasjonList().add(installasjon);
		installasjon.setDfuindivid(this);
		return installasjon;
	}

	/** {@inheritDoc} */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	public int compareTo(Dfuindivid o) {
		return new CompareToBuilder().append(serienummer, o.serienummer).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof Dfuindivid)) {
			return false;
		}
		Dfuindivid other = (Dfuindivid) o;
		return new EqualsBuilder().append(getId(), other.getId()).isEquals();
	}

	@Column(name = "ANSKAFFET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAnskaffetDato() {
		return anskaffetDato;
	}
	
	@Transient
	public String getAnskaffetDatoString(){
		return MipssDateFormatter.formatDate(anskaffetDato, false);
	}

	@Transient
	public Dfustatus getCurrentStatus() {
		return currentStatus;
	}

	@OneToMany(mappedBy = "dfuindivid", fetch = FetchType.EAGER)
	@OrderBy("opprettetDato")
	public List<Dfu_Status> getDfu_StatusList() {
		return dfu_StatusList;
	}
	
	@OneToOne(mappedBy = "dfuindivid", cascade = CascadeType.ALL)
	public Dfuinfo getDfuinfo(){
		return dfuinfo;
	}
	public void setDfuinfo(Dfuinfo dfuinfo){
		this.dfuinfo = dfuinfo;
	}
	@OneToMany(mappedBy = "dfuindivid", cascade = CascadeType.ALL)
	public List<DfuKontrakt> getDfuKontraktList() {
		return dfuKontraktList;
	}

	@OneToMany(mappedBy = "dfuindivid", cascade = CascadeType.ALL)
	public List<Dfulogg> getDfuloggList() {
		return dfuloggList;
	}

	@ManyToOne
	@JoinColumn(name = "MODELL_NAVN", referencedColumnName = "NAVN")
	public Dfumodell getDfumodell() {
		return dfumodell;
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	public String getFritekst() {
		return fritekst;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dfuindividIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	@OneToMany(mappedBy = "dfuindivid")
	public List<Installasjon> getInstallasjonList() {
		return installasjonList;
	}

	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	@Column(nullable = false)
	public String getSerienummer() {
		return serienummer;
	}

	public String getSimkortnummer() {
		return simkortnummer;
	}

	@Transient
	public String getSimkortnummerString() {
		return String.valueOf(getSimkortnummer());
	}
//
//	@Column(name = "SISTE_LIVSTEGN_DATO")
//	@Temporal(TemporalType.TIMESTAMP)
//	public Date getSisteLivstegnDato() {
//		return sisteLivstegnDato;
//	}

	@Column(name = "SLETTET_FLAGG", nullable = false)
	public Long getSlettetFlagg() {
		return slettetFlagg;
	}

	public String getTextForGUI() {
		return "S/N " + getSerienummer();
	}

	public Long getTlfnummer() {
		return tlfnummer;
	}

	@Transient
	public String getTlfnummerString() {
		return String.valueOf(getTlfnummer());
	}

	@Column(nullable = false)
	public String getVersjon() {
		return versjon;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(7, 91).append(getId()).append(getDfumodell()).append(getSerienummer()).toHashCode();
	}

	public Dfu_Status removeDfu_Status(Dfu_Status dfu_Status) {
		getDfu_StatusList().remove(dfu_Status);
		dfu_Status.setDfuindivid(null);
		return dfu_Status;
	}

	public DfuKontrakt removeDfuKontrakt(DfuKontrakt dfuKontrakt) {
		getDfuKontraktList().remove(dfuKontrakt);
		dfuKontrakt.setDfuindivid(null);
		return dfuKontrakt;
	}

	public Dfulogg removeDfulogg(Dfulogg dfulogg) {
		getDfuloggList().remove(dfulogg);
		dfulogg.setDfuindivid(null);
		return dfulogg;
	}


	public Installasjon removeInstallasjon(Installasjon installasjon) {
		getInstallasjonList().remove(installasjon);
		installasjon.setDfuindivid(null);
		return installasjon;
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	public void setAnskaffetDato(Date anskaffetDato) {
		Date oldValue = this.anskaffetDato;
		this.anskaffetDato = anskaffetDato;
		getProps().firePropertyChange("anskaffetDato", oldValue, anskaffetDato);
	}

	/**
	 * setter den nåværende status.
	 * 
	 * @param currentStatus
	 */
	public void setCurrentStatus(Dfustatus currentStatus) {
		Dfustatus oldValue = this.currentStatus;
		this.currentStatus = currentStatus;
		getProps().firePropertyChange("currentStatus", oldValue, currentStatus);
	}

	public void setDfu_StatusList(List<Dfu_Status> dfu_StatusList) {
//		List<Dfu_Status> oldValue = this.dfu_StatusList;
		this.dfu_StatusList = dfu_StatusList;
//		getProps().firePropertyChange("dfu_StatusList", oldValue, dfu_StatusList);
	}

	public void setDfuKontraktList(List<DfuKontrakt> dfuKontraktList) {
//		List<DfuKontrakt> oldValue = this.dfuKontraktList;
		this.dfuKontraktList = dfuKontraktList;
//		getProps().firePropertyChange("dfuKontraktList", oldValue, dfuKontraktList);
	}

	public void setDfuloggList(List<Dfulogg> dfuloggList) {
//		List<Dfulogg> oldValue = this.dfuloggList;
		this.dfuloggList = dfuloggList;
//		getProps().firePropertyChange("dfuloggList", oldValue, dfuloggList);
	}

	public void setDfumodell(Dfumodell dfumodell) {
		Dfumodell oldValue = this.dfumodell;
		this.dfumodell = dfumodell;
		
		if(dfumodell != null) {
			setModellNavn(dfumodell.getNavn());
		} else {
			setModellNavn(null);
		}
		
		getProps().firePropertyChange("dfumodell", oldValue, dfumodell);
	}

	public void setEndretAv(String endretAv) {
		String oldValue = this.endretAv;
		this.endretAv = endretAv;
		getProps().firePropertyChange("endretAv", oldValue, endretAv);
	}

	public void setEndretDato(Date endretDato) {
		Date oldValue = this.endretDato;
		this.endretDato = endretDato;
		getProps().firePropertyChange("endretDato", oldValue, endretDato);
	}

	public void setFritekst(String fritekst) {
		String oldValue = this.fritekst;
		this.fritekst = fritekst;
		getProps().firePropertyChange("fritekst", oldValue, fritekst);
	}

	public void setId(Long id) {
		Long oldValue = this.id;
		this.id = id;
		getProps().firePropertyChange("id", oldValue, id);
	}

	public void setInstallasjonList(List<Installasjon> installasjonList) {
//		List<Installasjon> oldValue = this.installasjonList;
		this.installasjonList = installasjonList;
//		getProps().firePropertyChange("installasjonList", oldValue, installasjonList);
	}

	public void setOpprettetAv(String opprettetAv) {
		String oldValue = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		getProps().firePropertyChange("opprettetAv", oldValue, opprettetAv);
	}

	public void setOpprettetDato(Date opprettetDato) {
		Date oldValue = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		getProps().firePropertyChange("opprettetDato", oldValue, opprettetDato);
	}

	public void setSerienummer(String serienummer) {
		String oldValue = this.serienummer;
		this.serienummer = serienummer;
		getProps().firePropertyChange("serienummer", oldValue, serienummer);
	}

	public void setSimkortnummer(String simkortnummer) {
		String oldValue = this.simkortnummer;
		this.simkortnummer = simkortnummer;
		getProps().firePropertyChange("simkortnummer", oldValue, simkortnummer);
	}

//	public void setSisteLivstegnDato(Date sisteLivstegnDato) {
//		Date oldValue = this.sisteLivstegnDato;
//		this.sisteLivstegnDato = sisteLivstegnDato;
//		getProps().firePropertyChange("sisteLivstegnDato", oldValue, sisteLivstegnDato);
//	}

	public void setSlettetFlagg(Long slettetFlagg) {
		Long oldValue = this.slettetFlagg;
		this.slettetFlagg = slettetFlagg;
		getProps().firePropertyChange("slettetFlagg", oldValue, slettetFlagg);
	}

	public void setTlfnummer(Long tlfnummer) {
		String oldString = getTlfnummerString();
		Long oldValue = this.tlfnummer;
		this.tlfnummer = tlfnummer;
		getProps().firePropertyChange("tlfnummer", oldValue, tlfnummer);
		getProps().firePropertyChange("tlfnummerString", oldString, getTlfnummerString());
	}

	public void setVersjon(String versjon) {
		String oldValue = this.versjon;
		this.versjon = versjon;
		getProps().firePropertyChange("versjon", oldValue, versjon);
	}

	public Long getProgramvareversjon() {
		return programvareversjon;
	}

	public void setProgramvareversjon(Long programvareversjon) {
		Long oldValue = this.programvareversjon;
		this.programvareversjon = programvareversjon;
		getProps().firePropertyChange("programvareversjon", oldValue, programvareversjon);
	}

	public void setSlettet(Boolean slettet) {
		Boolean old = getSlettet();

		if(slettet != null && slettet) {
			setSlettetFlagg(1L);
		} else {
			setSlettetFlagg(0L);
		}
		getProps().firePropertyChange("slettet", old, slettet);
	}
	
	@Transient
	public Boolean getSlettet() {
		return slettetFlagg != null && slettetFlagg == 1L;
	}
	
	/**
	 * Genererer en lesbar tekst med hovedlementene i entitybønnen.
	 * 
	 * @return en tekstlinje.
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", getId()).append("modell", getDfumodell()).append("serienummer",
				getSerienummer()).append("versjon", getVersjon()).append("simkort", getSimkortnummer()).append("tlf",
				getTlfnummer()).toString();
	}
}
