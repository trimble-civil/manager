package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Beskriver en modul rad i databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( { @NamedQuery(name = Modul.QUERY_FIND_ALL, query = "SELECT m FROM Modul m") })
public class Modul extends MipssEntityBean<Modul> implements Serializable, IRenderableMipssEntity, Comparable<Modul> {
	public static final String QUERY_FIND_ALL = "Modul.findAll";

	private String beskrivelse;
	private String endretAv;
	private Date endretDato;
	private Long id;
	private List<Modulversjon> modulversjonList = new ArrayList<Modulversjon>();
	private String navn;
	private String opprettetAv;
	private Date opprettetDato;

	/**
	 * Konstruktør
	 */
	public Modul() {
	}

	/**
	 * Legger til en modulimplementasjon
	 * 
	 * @param modulversjon
	 * @return
	 */
	public Modulversjon addModulversjon(Modulversjon modulversjon) {
		getModulversjonList().add(modulversjon);
		modulversjon.setModul(this);
		return modulversjon;
	}

	/** {@inheritDoc} */
	public int compareTo(Modul other) {
		if (other == null) {
			return 1;
		}

		return new CompareToBuilder().append(navn, other.getNavn()).toComparison();
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof Modul)) {
			return false;
		}
		if (o == this) {
			return true;
		}

		Modul other = (Modul) o;
		return new EqualsBuilder().append(id, other.getId()).isEquals();
	}

	/**
	 * Modulens status
	 * 
	 * @return
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Hvem endret modulen
	 * 
	 * @return
	 */
	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	/**
	 * Når ble modulen endret
	 * 
	 * @return
	 */
	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	/**
	 * Modulens id
	 * 
	 * @return
	 */
	@Id
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	/**
	 * Liste over konkrete modulimplementasjoner
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "modul")
	public List<Modulversjon> getModulversjonList() {
		return modulversjonList;
	}

	/**
	 * Modulens navn
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	/**
	 * Hvem opprettet modulen
	 * 
	 * @return
	 */
	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	/**
	 * Når ble modulen opprettet
	 * 
	 * @return
	 */
	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		return navn;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(1, 999).append(id).toHashCode();
	}

	/**
	 * Fjerner en modulimplementasjon
	 * 
	 * @param modulversjon
	 * @return
	 */
	public Modulversjon removeModulversjon(Modulversjon modulversjon) {
		getModulversjonList().remove(modulversjon);
		return modulversjon;
	}

	/**
	 * Modulens status
	 * 
	 * @param beskrivelse
	 */
	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

	/**
	 * Hvem endret modulen
	 * 
	 * @param endretAv
	 */
	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}

	/**
	 * Når ble modulen endret
	 * 
	 * @param endretDato
	 */
	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}

	/**
	 * Modulens id
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	/**
	 * Liste over konkrete modulimplementasjoner
	 * 
	 * @param modulversjonList
	 */
	public void setModulversjonList(List<Modulversjon> modulversjonList) {
		this.modulversjonList = modulversjonList;
	}

	/**
	 * Modulens navn
	 * 
	 * @param navn
	 */
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old,navn);
	}

	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}

	/**
	 * Når ble modulen opprettet
	 * 
	 * @param opprettetDato
	 */
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("navn", navn).append("beskrivelse", beskrivelse)
				.append("opprettetAv", opprettetAv).append("opprettetDato", opprettetDato).append("endretAv", endretAv)
				.append("endretDato", endretDato).toString();
	}
}
