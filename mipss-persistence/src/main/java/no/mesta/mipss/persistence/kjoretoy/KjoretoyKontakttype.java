package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Kjoretoykontakttype;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.valueobjects.Epostadresse;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = KjoretoyKontakttype.FIND_ALL, query = "select o from KjoretoyKontakttype o"),
	@NamedQuery(name = KjoretoyKontakttype.FIND_BY_KJORETOY, query = "select o from KjoretoyKontakttype o where o.kjoretoyId = :id")
})

@Table(name = "KJORETOY_KONTAKTTYPE")
@SequenceGenerator(name = KjoretoyKontakttype.ID_SEQ, sequenceName = "KJORETOY_KONTAKTTYPE_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

public class KjoretoyKontakttype extends MipssEntityBean<KjoretoyKontakttype> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity, Cloneable, KjoretoyKontaktperson {
	public static final String FIND_ALL = "KjoretoyKontakttype.findAll";
	public static final String FIND_BY_KJORETOY = "KjoretoyKontakttype.findByKjoretoy";
	public static final String ID_SEQ = "kjoretoyKontakttypeIdSeq";
	private static final String[] ID_NAMES = {"kjoretoyId", "kontakttypeNavn"};
	
	private Long id;
    private String beskrivelse;
    private String epostAdresse;
    private Long kjoretoyId;
    private String kontaktnavn;
    private String kontakttypeNavn;
    private Long tlfnummer1;
    private Long tlfnummer2;
    private Kjoretoy kjoretoy;

    private Kjoretoykontakttype kontakttype;
    private OwnedMipssEntity ownedMipssEntity;
    
    public KjoretoyKontakttype() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = KjoretoyKontakttype.ID_SEQ)
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
    	this.beskrivelse = beskrivelse;
    	
    	getProps().firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Column(name="EPOST_ADRESSE")
    public String getEpostAdresse() {
        return epostAdresse;
    }

	@Transient
	public Epostadresse getEpost() {
		if(epostAdresse != null) {
			return new Epostadresse(epostAdresse);
		} else {
			return null;
		}
	}
    
    public void setEpostAdresse(String epostAdresse) {
		Object oldEpost = getEpost();
		String old = this.epostAdresse;
        this.epostAdresse = epostAdresse;
        
        getProps().firePropertyChange("epostAdresse", old, epostAdresse);
		getProps().firePropertyChange("epost", oldEpost, getEpost());
    }

    @Column(name="KJORETOY_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    @Column(nullable = false)
    public String getKontaktnavn() {
        return kontaktnavn;
    }

    public void setKontaktnavn(String kontaktnavn) {
    	String old = this.kontaktnavn;
        this.kontaktnavn = kontaktnavn;
        
        getProps().firePropertyChange("kontaktnavn", old, kontaktnavn);
    }

    @Column(name="KONTAKTTYPE_NAVN", nullable = false, insertable = false, updatable = false)
    public String getKontakttypeNavn() {
        return kontakttypeNavn;
    }

    public void setKontakttypeNavn(String kontakttypeNavn) {
    	String old = this.kontakttypeNavn;
        this.kontakttypeNavn = kontakttypeNavn;
        
        getProps().firePropertyChange("kontakttypeNavn", old, kontakttypeNavn);
    }
    
    @ManyToOne
    @JoinColumn(name = "KONTAKTTYPE_NAVN", referencedColumnName = "NAVN", nullable = false)
    public Kjoretoykontakttype getKontakttype() {
        return kontakttype;
    }

    public void setKontakttype(Kjoretoykontakttype kontakttype) {
    	Kjoretoykontakttype old = this.kontakttype;
        this.kontakttype = kontakttype;
        
        if(kontakttype != null) {
        	this.kontakttypeNavn = kontakttype.getNavn();
        } else {
        	this.kontakttypeNavn = null;
        }
        
        getProps().firePropertyChange("kontakttype", old, kontakttype);
    }

    public Long getTlfnummer1() {
        return tlfnummer1;
    }

    public void setTlfnummer1(Long tlfnummer1) {
    	Long old = this.tlfnummer1;
        this.tlfnummer1 = tlfnummer1;
        
        getProps().firePropertyChange("tlfnummer1", old, tlfnummer1);
    }

    public Long getTlfnummer2() {
        return tlfnummer2;
    }

    public void setTlfnummer2(Long tlfnummer2) {
    	Long old = this.tlfnummer2;
        this.tlfnummer2 = tlfnummer2;
        
        getProps().firePropertyChange("tlfnummer2", old, tlfnummer2);
    }

    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
    	Kjoretoy old = this.kjoretoy;
        this.kjoretoy = kjoretoy;
        
        if(kjoretoy != null) {
        	this.kjoretoyId = kjoretoy.getId();
        } else {
        	this.kjoretoyId = null;
        }
        
        getProps().firePropertyChange("kjoretoy", old, kjoretoy);
    }
    
    // de neste metodene er h�ndskrevet
     /**
      * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         return new ToStringBuilder(this).
             append("kjoreotyId", getKjoretoyId()).
             append("type", getKontakttypeNavn()).
             append("navn", getKontaktnavn()).
             append("telefon1", getTlfnummer1()).
             append("telefon2", getTlfnummer2()).
             append("epost", getEpostAdresse()).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante n�klene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof KjoretoyKontakttype)) {
             return false;
         }
         KjoretoyKontakttype other = (KjoretoyKontakttype) o;
         return new EqualsBuilder().
             append(getId(), other.getId()).
             isEquals();
     }

     /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
         return new HashCodeBuilder(23,73).
             append(getId()).
             toHashCode();
     }

    public String getTextForGUI() {
        return getKontaktnavn();
    }

    public int compareTo(Object o) {
        KjoretoyKontakttype other = (KjoretoyKontakttype) o;
        return new CompareToBuilder().
            append(getKontaktnavn(), other.getKontaktnavn()).
            toComparison();
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }

    /**
     * kopierer felt mednikke PropertyChangeListeners.
     * @see java.lang.Object#clone()
     */
    public KjoretoyKontakttype clone() throws CloneNotSupportedException {
    	KjoretoyKontakttype ny = new KjoretoyKontakttype();
    	ny.setBeskrivelse(this.getBeskrivelse());
    	ny.setEpostAdresse(this.getEpostAdresse());
    	ny.setKjoretoyId(this.getKjoretoyId());
    	ny.setKontaktnavn(this.getKontaktnavn());
    	ny.setKontakttypeNavn(this.getKontakttypeNavn());
    	ny.setTlfnummer1(this.getTlfnummer1());
    	ny.setTlfnummer2(this.getTlfnummer2());
    	ny.setKjoretoy(this.getKjoretoy());
    	ny.setOwnedMipssEntity(this.getOwnedMipssEntity());
    	
    	return ny;
    }

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}
	
	@Transient
	public String getAdressePoststed() {
		return null;
	}
	
	@Transient
	public String getTelefonnummer() {
		if (tlfnummer1!=null)
			return tlfnummer1.toString();
		return null;
	}
}
