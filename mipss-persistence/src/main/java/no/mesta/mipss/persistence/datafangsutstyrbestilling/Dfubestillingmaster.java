package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;

@SuppressWarnings("serial")
@SequenceGenerator(name = "dfubestillingmasterIdSeq", sequenceName = "DFUBESTILLINGMASTER_ID_SEQ", allocationSize = 1, initialValue = 1)
@Entity(name = Dfubestillingmaster.BEAN_NAME)
@Table(name = "DFUBESTILLINGMASTER")
public class Dfubestillingmaster extends MipssEntityBean<Dfubestillingmaster> {
	public static final String BEAN_NAME = "Dfubestillingmaster";
	private Long id;
	private Long ordreId;
	private String signatur;
	private String leveringsNavn;
	private String leveringsAdresse;
	private Long leveringsPostnummer;
	private String leveringsPoststed;
	private Long leveringsTlfnummer;
	private String leveringsEpostAdresse;
	private String fritekst;
	private Date onsketDato;
	private String opprettetAv;
	private Date opprettetDato;
	private String endretAv;
	private Date endretDato;
	
	private Long bedriftspakkeFlagg;
	
	private Kjoretoyordre kjoretoyordre;
	private List<DfulevBestilling> dfulevBestillingList = new ArrayList<DfulevBestilling>();
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dfubestillingmasterIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="ORDRE_ID", nullable = true, insertable = false, updatable = false)
	public Long getOrdreId() {
		return ordreId;
	}

	public void setOrdreId(Long ordreId) {
		this.ordreId = ordreId;
	}

	@Column(nullable=false)
	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		Object oldValue = this.signatur;
		this.signatur = signatur;
		firePropertyChange("signatur", oldValue, signatur);
	}

	@Column(name="LEVERINGS_NAVN")
	public String getLeveringsNavn() {
		return leveringsNavn;
	}

	public void setLeveringsNavn(String leveringsNavn) {
		Object oldValue = this.leveringsNavn;
		this.leveringsNavn = leveringsNavn;
		firePropertyChange("leveringsNavn", oldValue, leveringsNavn);
	}

	@Column(name="LEVERINGS_ADRESSE")
	public String getLeveringsAdresse() {
		return leveringsAdresse;
	}

	public void setLeveringsAdresse(String leveringsAdresse) {
		Object oldValue = this.leveringsAdresse;
		this.leveringsAdresse = leveringsAdresse;
		firePropertyChange("leveringsAdresse", oldValue, leveringsAdresse);
	}

	@Column(name="LEVERINGS_POSTNUMMER")
	public Long getLeveringsPostnummer() {
		return leveringsPostnummer;
	}

	public void setLeveringsPostnummer(Long leveringsPostnummer) {
		Object oldValue = this.leveringsPostnummer;
		this.leveringsPostnummer = leveringsPostnummer;
		firePropertyChange("leveringsPostnummer", oldValue, leveringsPostnummer);
	}

	@Column(name="LEVERINGS_POSTSTED")
	public String getLeveringsPoststed() {
		return leveringsPoststed;
	}

	public void setLeveringsPoststed(String leveringsPoststed) {
		Object oldValue = this.leveringsPoststed;
		this.leveringsPoststed = leveringsPoststed;
		firePropertyChange("leveringsPoststed", oldValue, leveringsPoststed);
	}

	@Column(name="LEVERINGS_TLFNUMMER")
	public Long getLeveringsTlfnummer() {
		return leveringsTlfnummer;
	}

	public void setLeveringsTlfnummer(Long leveringsTlfnummer) {
		Object oldValue = this.leveringsTlfnummer;
		this.leveringsTlfnummer = leveringsTlfnummer;
		firePropertyChange("leveringsTlfnummer", oldValue, leveringsTlfnummer);
	}

	@Column(name="LEVERINGS_EPOST_ADRESSE")
	public String getLeveringsEpostAdresse() {
		return leveringsEpostAdresse;
	}

	public void setLeveringsEpostAdresse(String leveringsEpostAdresse) {
		Object oldValue = this.leveringsEpostAdresse;
		this.leveringsEpostAdresse = leveringsEpostAdresse;
		firePropertyChange("leveringsEpostAdresse", oldValue, leveringsEpostAdresse);
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		Object oldValue = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", oldValue, fritekst);
	}

	@Column(name="ONSKET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOnsketDato() {
		return onsketDato;
	}

	public void setOnsketDato(Date onsketDato) {
		Object oldValue = this.onsketDato;
		this.onsketDato = onsketDato;
		firePropertyChange("onsketDato", oldValue, onsketDato);
	}

	@Column(name="OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@Column(name="ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	@Column(name="ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	@ManyToOne
	@JoinColumn(name = "ORDRE_ID", referencedColumnName = "ID")
	public Kjoretoyordre getKjoretoyordre() {
		return kjoretoyordre;
	}

	public void setKjoretoyordre(Kjoretoyordre kjoretoyordre) {
		Object oldValue = this.kjoretoyordre;
		this.kjoretoyordre = kjoretoyordre;
		
		if (kjoretoyordre != null) {
			setOrdreId(kjoretoyordre.getId());
		} else {
			setOrdreId(null);
		}
		
		firePropertyChange("kjoretoyordre", oldValue, kjoretoyordre);
	}

	@OneToMany(mappedBy = "dfubestillingmaster", cascade = CascadeType.ALL)
	public List<DfulevBestilling> getDfulevBestillingList() {
		return dfulevBestillingList;
	}

	public void setDfulevBestillingList(List<DfulevBestilling> dfulevBestillingList) {
		Object oldValue = this.dfulevBestillingList;
		this.dfulevBestillingList = dfulevBestillingList;
		firePropertyChange("dfulevBestillingList", oldValue, dfulevBestillingList);
	}
	
	public DfulevBestilling getDfuLDfulevBestillingForBestilling() {
		return getDfulevBestillingList().get(0);
	}

	@Column(name="BEDRIFTSPAKKE_FLAGG")
	public Long getBedriftspakkeFlagg() {
		if(bedriftspakkeFlagg == null) {
			bedriftspakkeFlagg = 0L;
		}
		return bedriftspakkeFlagg;
	}

	public void setBedriftspakkeFlagg(Long bedriftspakkeFlagg) {
		Object oldValue = this.bedriftspakkeFlagg;
		this.bedriftspakkeFlagg = bedriftspakkeFlagg;
		firePropertyChange("bedriftspakkeFlagg", oldValue, bedriftspakkeFlagg);
	}
	
	@Transient
	public Boolean getBedriftspakkeFlaggBoolean() {
		return bedriftspakkeFlagg != null && bedriftspakkeFlagg == 1;
	}
	
	public void setBedriftspakkeFlaggBoolean(Boolean bedriftspakkeFlaggBoolean) {
		Object oldValue = this.getBedriftspakkeFlaggBoolean();
		setBedriftspakkeFlagg((bedriftspakkeFlaggBoolean == null || !bedriftspakkeFlaggBoolean) ? 0L : 1L);
		firePropertyChange("bedriftspakkeFlaggBoolean", oldValue, getBedriftspakkeFlaggBoolean());
	}
}
