package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;

@SuppressWarnings("serial")
public class R12Stroprodukt implements Serializable, Cloneable{
	private Long id;
	private String header;
	private Long seq;
	private Double kjortKm;
	private Long kjortSekund;
	private Double mengdeTonn;
	private Double mengdeM3;
	
	public static String[] getPropertiesArray() {
		return new String[] { "id", "header", "seq", "kjortKm", "kjortSekund", "mengdeTonn", "mengdeM3"};
	}
	
	public String toString(){
		return id+":"+header+":"+seq+":"+kjortKm;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public Double getKjortKm() {
		return kjortKm;
	}
	public void setKjortKm(Double kjortKm) {
		this.kjortKm = kjortKm;
	}
	public void addKjortKm(Double kjortKm){
		if (this.kjortKm==null){
			this.kjortKm = kjortKm;
		}else{
			if (kjortKm!=null){
				this.kjortKm+=kjortKm;
			}
		}
	}
	public Long getKjortSekund() {
		return kjortSekund;
	}
	public void setKjortSekund(Long kjortSekund) {
		this.kjortSekund = kjortSekund;
	}
	public void addKjortSekund(Long kjortSekund){
		if (this.kjortSekund==null){
			this.kjortSekund = kjortSekund;
		}else{
			if (kjortSekund!=null){
				this.kjortSekund += kjortSekund;
			}
		}
	}
	public Double getMengdeTonn() {
		return mengdeTonn;
	}
	public void setMengdeTonn(Double mengdeTonn) {
		this.mengdeTonn = mengdeTonn;
	}
	public void addMengdeTonn(Double mengdeTonn){
		if (this.mengdeTonn==null){
			this.mengdeTonn = mengdeTonn;
		}else{
			if (mengdeTonn!=null){
				this.mengdeTonn += mengdeTonn;
			}
		}
	}
	public void addMengdeM3(Double mengdeM3){
		if (this.mengdeM3==null){
			this.mengdeM3 = mengdeM3;
		}else{
			if (mengdeM3!=null){
				this.mengdeM3 += mengdeM3;
			}
		}
	}
	public Double getMengdeM3() {
		return mengdeM3;
	}
	public void setMengdeM3(Double mengdeM3) {
		this.mengdeM3 = mengdeM3;
	}
	public Double getTid(){
		if (kjortSekund==null)
			return null;
		return (double)kjortSekund/(double)86400;
	}
	
	public boolean equals(Object o){
		if (!(o instanceof R12Stroprodukt)){
			return false;
		}
		R12Stroprodukt other = (R12Stroprodukt)o;
		return new EqualsBuilder().append(id, other.id).isEquals();
	}
	@Override
	public R12Stroprodukt clone(){
		R12Stroprodukt st = new R12Stroprodukt();
		st.setId(id);
		st.setSeq(seq);
		st.setHeader(header);
		st.setKjortKm(kjortKm);
		st.setKjortSekund(kjortSekund);
		st.setMengdeM3(mengdeM3);
		st.setMengdeTonn(mengdeTonn);
		return st;
	}
}
