package no.mesta.mipss.persistence.mipssfield;

import java.util.List;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.vo.Hp;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;

/**
 * Hjelpeklasse for å arbeide med veier (punkter og strekninger)
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class VeiInfoUtil {

	/**
	 * Lager et tomt veiinfo objekt
	 * 
	 * @return
	 */
	public static VeiInfo buildVeiInfo() {
		return new VeiInfo();
	}

	public static VeiInfo buildVeiInfo(HendelseSokView hendelse) {
		return buildVeiInfo().withFylkesnummer(hendelse.getFylkesnummer()).withKommunenummer(
				hendelse.getKommunenummer()).withVeikategori(hendelse.getVeikategori()).withVeistatus(
				hendelse.getVeistatus()).withVeinummer(hendelse.getVeinummer()).withHp(hendelse.getFraHp()).withMeter(
				hendelse.getFraMeter()).withKjorefelt(hendelse.getKjorefelt());
	}

	/**
	 * Lager veiinfo objekt basert på en kværnet strekning
	 * 
	 * @param p
	 * @return
	 */
	public static VeiInfo buildVeiInfo(Prodstrekning p) {
		return buildVeiInfo()
			.withFylkesnummer(p.getFylkesnummer() != null ? p.getFylkesnummer().intValue() : 0)
			.withKommunenummer(p.getKommunenummer() != null ? p.getKommunenummer().intValue() : 0)
			.withVeikategori(p.getVeikategori() != null ? p.getVeikategori() : "")
			.withVeistatus(p.getVeistatus() != null ? p.getVeistatus() : "")
			.withVeinummer(p.getVeinummer() != null ? p.getVeinummer().intValue() : 0)
			.withHp(p.getHp() != null ? p.getHp().intValue() : 0);
	}

	/**
	 * Lager veiinfo objekt basert på en kværnet strekning
	 * 
	 * @param p
	 * @return
	 */
	public static VeiInfo buildVeiInfo(ProdstrekningSok p) {
		return buildVeiInfo()
		.withFylkesnummer(p.getFylkesnummer())
		.withKommunenummer(p.getKommunenummer())
		.withVeikategori(p.getVeikategori() != null ? p.getVeikategori() : "")
		.withVeistatus(p.getVeistatus() != null ? p.getVeistatus() : "")
		.withVeinummer(p.getVeinummer())
		.withHp(p.getHp());
	}

	/**
	 * Lager veiinfo objekt basert på en view rad
	 * 
	 * @param view
	 * @return
	 */
	public static VeiInfo buildVeiInfo(PunktregSok view) {
		return buildVeiInfo().withFylkesnummer(view.getFylkesnummer()).withKommunenummer(view.getKommunenummer())
				.withVeikategori(view.getVeikategori()).withVeistatus(view.getVeistatus()).withHp(view.getHp())
				.withVeinummer(view.getVeinummer()).withMeter(view.getMeter()).withKjorefelt(view.getKjorefelt());
	}

	/**
	 * Lager veiinfo objekt basert på en veireferanse
	 * 
	 * @param veiref
	 * @return
	 */
	public static VeiInfo buildVeiInfo(Punktveiref veiref) {
		return buildVeiInfo().withFylkesnummer(veiref.getFylkesnummer()).withHp(veiref.getHp()).withVeikategori(
				veiref.getVeikategori()).withVeistatus(veiref.getVeistatus()).withVeinummer(veiref.getVeinummer())
				.withKommunenummer(veiref.getKommunenummer()).withMeter(veiref.getMeter()).withKjorefelt(
						veiref.getKjorefelt());
	}

	public static String getProdHpGUIText(List<VeiInfo> hpStrekninger) {
		if (hpStrekninger == null || hpStrekninger.isEmpty()) {
			return "";
		} else {
			StringBuffer text = new StringBuffer();
			for (int i = 0; i < hpStrekninger.size(); i++) {
				VeiInfo v = hpStrekninger.get(i);
				Hp hp = new Hp(v);
				text.append(hp);
				if (i < hpStrekninger.size() - 1) {
					text.append(", ");
				}
			}

			return text.toString();
		}

	}

	/**
	 * Gir en GUI tekst for en veireferanse
	 * 
	 * @param view
	 * @return
	 */
	public static String getVeiStedfest(PunktregSok view) {
		return buildVeiInfo(view).toString();
	}

	/**
	 * Gir en GUI tekst for en veireferanse
	 * 
	 * @param veiref
	 * @return
	 */
	public static String getVeiStedfest(Punktveiref veiref) {
		return buildVeiInfo(veiref).toString();
	}

	/**
	 * Gir en GUI tekst for vei informasjon
	 * 
	 * @param info
	 * @return
	 */
	public static String getVeiStedfest(VeiInfo info) {
		return info.toString();
	}
}
