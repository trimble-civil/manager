package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
public class SkadeBildePK implements Serializable{
	public String bildeGuid;
    public String forsikringsskadeGuid;
    
    public SkadeBildePK(String bildeBuid, String forsikringsskadeGuid){
    	this.bildeGuid = bildeBuid;
    	this.forsikringsskadeGuid= forsikringsskadeGuid;
    }
    public SkadeBildePK(){
    	
    }
	public String getBildeGuid() {
		return bildeGuid;
	}
	public void setBildeGuid(String bildeGuid) {
		this.bildeGuid = bildeGuid;
	}
	public String getForsikringsskadeGuid() {
		return forsikringsskadeGuid;
	}
	public void setForsikringsskadeGuid(String forsikringsskadeGuid) {
		this.forsikringsskadeGuid = forsikringsskadeGuid;
	}
	
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("bildeGuid", bildeGuid).
            append("forsikringsskadeGuid", forsikringsskadeGuid).
            toString();
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(4009, 5091).append(bildeGuid).
            append(forsikringsskadeGuid).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof SkadeBildePK)) {return false;}
        
        SkadeBildePK other = (SkadeBildePK) o;
        
        return new EqualsBuilder().append(bildeGuid, other.getBildeGuid()).
            append(forsikringsskadeGuid, other.getForsikringsskadeGuid()).isEquals();
    }
}
