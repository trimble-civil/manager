package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "ROLLE_BRUKER")
@IdClass(RolleBrukerPK.class)
@NamedQueries( {
		@NamedQuery(name = RolleBruker.QUERY_ROLLEBRUKER_GET_FOR_USER, query = "SELECT rb FROM RolleBruker rb where LOWER(rb.signatur) = LOWER(:signatur)"),
		@NamedQuery(name = RolleBruker.QUERY_ROLLEBRUKER_DELETE_FOR_USER, query = "DELETE FROM RolleBruker rb where LOWER(rb.signatur) = LOWER(:signatur)"),
		@NamedQuery(name = RolleBruker.QUERY_ROLLEBRUKER_GET_FOR_ROLE, query = "SELECT rb FROM RolleBruker rb where rb.rolleId = :rolleId") })
public class RolleBruker implements Serializable, Comparable<RolleBruker> {
	public static final String QUERY_ROLLEBRUKER_DELETE_FOR_USER = "RolleBruker.DeleteForUser";
	public static final String QUERY_ROLLEBRUKER_GET_FOR_ROLE = "RolleBruker.getForRole";
	public static final String QUERY_ROLLEBRUKER_GET_FOR_USER = "RolleBruker.getForUser";

	private Bruker bruker;
	private String opprettetAv;
	private Date opprettetDato;
	private Rolle rolle;
	private Long rolleId;
	private String signatur;
	private String slettetAv;
	private Date slettetDato;

	public RolleBruker() {
	}

	/** {@inheritDoc} */
	public int compareTo(RolleBruker o) {
		if (o == null) {
			return 1;
		}

		Rolle otherRolle = o.getRolle();
		Rolle thisRolle = getRolle();
		return new CompareToBuilder().append(getBruker(), o.getBruker()).append(thisRolle, otherRolle).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof RolleBruker)) {
			return false;
		}
		if (o == this) {
			return true;
		}

		RolleBruker other = (RolleBruker) o;
		return new EqualsBuilder().append(rolleId, other.getRolleId()).append(signatur, other.getSignatur()).isEquals();
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "SIGNATUR", referencedColumnName = "SIGNATUR")
	public Bruker getBruker() {
		return bruker;
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "ROLLE_ID", referencedColumnName = "ID")
	public Rolle getRolle() {
		return rolle;
	}

	@Id
	@Column(name = "ROLLE_ID", nullable = false, insertable = false, updatable = false)
	public Long getRolleId() {
		return rolleId;
	}

	@Id
	@Column(name = "SIGNATUR", nullable = false, insertable = false, updatable = false)
	public String getSignatur() {
		return signatur;
	}

	@Column(name = "SLETTET_AV")
	public String getSlettetAv() {
		return slettetAv;
	}

	@Column(name = "SLETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSlettetDato() {
		return slettetDato;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(3, 5).append(rolleId).append(signatur).toHashCode();
	}

	public void setBruker(Bruker bruker) {
		this.bruker = bruker;
		if (bruker != null) {
			setSignatur(bruker.getSignatur());
		} else {
			setSignatur(null);
		}
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setRolle(Rolle rolle) {
		this.rolle = rolle;

		if (rolle != null) {
			setRolleId(rolle.getId());
		} else {
			setRolleId(null);
		}
	}

	public void setRolleId(Long rolleId) {
		this.rolleId = rolleId;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}

	public void setSlettetAv(String slettetAv) {
		this.slettetAv = slettetAv;
	}

	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("rolleId", rolleId).append("signatur", signatur).toString();
	}
}
