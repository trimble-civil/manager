package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Statuser en applikasjon kan ha
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Appstatus.QUERY_FIND_ALL, query = "SELECT s FROM Appstatus s")
})
public class Appstatus extends MipssEntityBean<Appstatus> implements Serializable, IRenderableMipssEntity {
	
    public static final String QUERY_FIND_ALL = "Appstatus.findAll";
    
    public static final Appstatus NEW = new Appstatus("NEW");
    public static final Appstatus PILOT = new Appstatus("PILOT");
    public static final Appstatus INSTALLED = new Appstatus("INSTALLED");
    public static final Appstatus RETIRED = new Appstatus("RETIRED");
    
    private String verdi;

    /**
     * Konstruktør
     * 
     */
    public Appstatus() {
    }

    /**
     * Konstruktør
     * 
     * @param verdi
     */
    public Appstatus(String verdi) {
    	this.verdi = verdi;
    }
    
    /**
     * Verdien er eneste felt, og unik
     * @return
     */
    @Id
    @Column(nullable = false)
    public String getVerdi() {
        return verdi;
    }

    /**
     * Verdien
     * 
     * @param verdi
     */
    public void setVerdi(String verdi) {
    	String old = this.verdi;
        this.verdi = verdi;
        firePropertyChange("verdi", old, verdi);
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Appstatus)) {return false;}
        if(o == this) { return true;}
        
        Appstatus other = (Appstatus) o;
        return new EqualsBuilder().
            append(verdi, other.getVerdi()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(5,995).
            append(verdi).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return verdi;
    }

	public String getTextForGUI() {
		return verdi;
	}

}
