package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

/**
 * JPA entitet generert i JDeveloper.
 * Grunndata for ordretyper.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Kjoretoyordretype.FIND_ALL, query = "select o from Kjoretoyordretype o order by o.guiRekkefolge"),
	@NamedQuery(name = Kjoretoyordretype.FIND_ALL_VALID, query = "select o from Kjoretoyordretype o where o.valgbarFlagg = 1 order by o.guiRekkefolge")
})

public class Kjoretoyordretype implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kjoretoyordretype.findAll";
	public static final String FIND_ALL_VALID = "Kjoretoyordretype.findAllValid";
	
    private String beskrivelse;
    private Long guiRekkefolge;
    private String navn;
    private Long valgbarFlagg;
    private List<Kjoretoyordre> kjoretoyordreList;

    public Kjoretoyordretype() {
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
        this.beskrivelse = beskrivelse;
    }

    @Column(name="GUI_REKKEFOLGE")
    public Long getGuiRekkefolge() {
        return guiRekkefolge;
    }

    public void setGuiRekkefolge(Long guiRekkefolge) {
        this.guiRekkefolge = guiRekkefolge;
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Long getValgbarFlagg() {
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Long valgbarFlagg) {
        this.valgbarFlagg = valgbarFlagg;
    }

    @OneToMany(mappedBy = "kjoretoyordretype")
    public List<Kjoretoyordre> getKjoretoyordreList() {
        return kjoretoyordreList;
    }

    public void setKjoretoyordreList(List<Kjoretoyordre> kjoretoyordreList) {
        this.kjoretoyordreList = kjoretoyordreList;
    }

    public Kjoretoyordre addKjoretoyordre(Kjoretoyordre kjoretoyordre) {
        getKjoretoyordreList().add(kjoretoyordre);
        kjoretoyordre.setKjoretoyordretype(this);
        return kjoretoyordre;
    }

    public Kjoretoyordre removeKjoretoyordre(Kjoretoyordre kjoretoyordre) {
        getKjoretoyordreList().remove(kjoretoyordre);
        kjoretoyordre.setKjoretoyordretype(null);
        return kjoretoyordre;
    }

    @Transient
    public String getTextForGUI() {
		return getNavn();
	}
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entitybønnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            append("rekkefølge", getGuiRekkefolge()).
            append("valgbar", getValgbarFlagg()).
            toString();
    }

    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kjoretoyordretype)) {
            return false;
        }
        Kjoretoyordretype other = (Kjoretoyordretype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun på Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getNavn()).
            append(getGuiRekkefolge()).
            append(getValgbarFlagg()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	Kjoretoyordretype other = (Kjoretoyordretype) o;
		return new CompareToBuilder().
			append(getNavn(), other.getNavn()).
			toComparison();
    }

}
