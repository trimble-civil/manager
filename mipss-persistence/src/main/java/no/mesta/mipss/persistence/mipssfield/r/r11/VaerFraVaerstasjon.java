package no.mesta.mipss.persistence.mipssfield.r.r11;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * VaerFraVaerstasjon
 *
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

@SuppressWarnings("serial")
@Entity(name = VaerFraVaerstasjon.BEAN_NAME)
@Table(name="VAER_FRA_VAERSTASJON_XV")
public class VaerFraVaerstasjon extends MipssEntityBean<VaerFraVaerstasjon>{
	public static final String BEAN_NAME = "VaerFraVaerstasjon";

	@Column(name="MM_NEDBOR_SISTE_3_DOGN")
	private Double mmNedborSiste3Dogn;
	@Column(name="REGN_MM_PR_DAG")
	private Double regnMmPrDag;
	@OneToOne
    @JoinColumn(name = "SKRED_GUID", referencedColumnName = "GUID")
	private Skred skred;
	@Id
	@Column(name="SKRED_GUID", nullable = false, insertable = false, updatable = false )
	private String skredGuid;
	@Column(name="SLUDD_MM_PR_DAG")
	private Double sluddMmPrDag;
	@Column(name="SNO_MM_PR_DAG")
	private Double snoMmPrDag;
	private Double temperatur;
	private String vindretning;

	@Column(name="VINDSTYRKE_M_PR_SEK")
	private Double vindstyrkeMPrSek;

	public Double getMmNedborSiste3Dogn() {
		return this.mmNedborSiste3Dogn;
	}
	public Double getRegnMmPrDag() {
		return this.regnMmPrDag;
	}
	public Skred getSkred() {
        return this.skred;
    }
	public String getSkredGuid() {
		return this.skredGuid;
	}

	public Double getSluddMmPrDag() {
		return this.sluddMmPrDag;
	}
	public Double getSnoMmPrDag() {
		return this.snoMmPrDag;
	}
	public Double getTemperatur() {
		return this.temperatur;
	}
	public String getVindretning() {
		return this.vindretning;
	}

    public Double getVindstyrkeMPrSek() {
		return this.vindstyrkeMPrSek;
	}

	public void setMmNedborSiste3Dogn(final Double mmNedborSiste3Dogn) {
		final Double old = this.mmNedborSiste3Dogn;
		this.mmNedborSiste3Dogn = mmNedborSiste3Dogn;
		firePropertyChange("mmNedborSiste3Dogn", old, mmNedborSiste3Dogn);
	}
	public void setRegnMmPrDag(final Double regnMmPrDag) {
		final Double old = this.regnMmPrDag;
		this.regnMmPrDag = regnMmPrDag;
		firePropertyChange("regnMmPrDag", old, regnMmPrDag);
	}
	public void setSkred(final Skred skred){
		final Skred old = this.skred;
		this.skred = skred;
		firePropertyChange("skred", old, skred);

		if (skred != null) {
			setSkredGuid(skred.getGuid());
		} else {
			setSkredGuid(null);
		}
	}
	public void setSkredGuid(final String skredGuid) {
		final String old = this.skredGuid;
		this.skredGuid = skredGuid;
		firePropertyChange("skredGuid", old, skredGuid);
	}
	public void setSluddMmPrDag(final Double sluddMmPrDag) {
		final Double old = this.sluddMmPrDag;
		this.sluddMmPrDag = sluddMmPrDag;
		firePropertyChange("sluddMmPrDag", old, sluddMmPrDag);
	}
	public void setSnoMmPrDag(final Double snoMmPrDag) {
		final Double old = this.snoMmPrDag;
		this.snoMmPrDag = snoMmPrDag;
		firePropertyChange("snoMmPrDag", old, snoMmPrDag);
	}
	public void setTemperatur(final Double temperatur) {
		final Double old = this.temperatur;
		this.temperatur = temperatur;
		firePropertyChange("temperatur", old, temperatur);
	}
	public void setVindretning(final String vindretning) {
		final String old = this.vindretning;
		this.vindretning = vindretning;
		firePropertyChange("vindretning", old, vindretning);
	}
	public void setVindstyrkeMPrSek(final Double vindstyrkeMPrSek) {
		final Double old = this.vindstyrkeMPrSek;
		this.vindstyrkeMPrSek = vindstyrkeMPrSek;
		firePropertyChange("vindstyrkeMPrSek", old, vindstyrkeMPrSek);
	}



}
