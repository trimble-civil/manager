package no.mesta.mipss.persistence;

import java.util.Date;

/**
 * Denne skal vite hva klokka er...
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haralk@mesan.no">Harald A. Kulø</a>
 */
public class Clock {
    private static Date now = null;
    private static long realTime = 0;
    private static boolean running;
    /**
     * Angir "nuh". I det den kalles, eller et fastsatt tidspunkt hvis klokka
     * har blitt "tuklet" med, vha setClock(Date).
     * 
     * @return
     */
    public static Date now() {
        if(now == null) {
            return new Date();
        } else {
        	if (running){
        		long d = new Date().getTime()-realTime;
        		Date date = new Date();
        		date.setTime(now.getTime()+d);
        		return date;
        	}else{
        		return now;
        	}
        }
    }
    /**
     * Settes til true dersom man vil at tiden skal løpe, tiden som har gått siden man satt 
     * klokken vil da legges til når man kaller now()
     * @param running
     */
    public static void setRunning(boolean running){
    	Clock.running = running;
    }
    /**
     * Kan benyttes i test øymed til å "tukle med tiden"
     * 
     * @param date
     */
    public static void setClock(Date date) {
        realTime = new Date().getTime();
    	now = date;
    }
}
