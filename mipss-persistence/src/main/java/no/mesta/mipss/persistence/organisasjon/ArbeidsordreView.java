package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(
        name = ArbeidsordreView.QUERY_FIND_ALL,
        query = "SELECT o FROM ArbeidsordreView o"),
	@NamedQuery(
		name = ArbeidsordreView.QUERY_FIND_BY_ARBEIDSORDRENR,
        query = "SELECT o FROM ArbeidsordreView o WHERE o.prosjektnr = :prosjektnr")
    })
@Table(name = "ARBEIDSORDRE_V")
public class ArbeidsordreView implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "ArbeidsordreView.findAll";
	public static final String QUERY_FIND_BY_ARBEIDSORDRENR = "ArbeidsordreView.findByArbeidsordre";
	
	private String arbeidsordrenr;
	private String arbeidsordrenavn;
	private String prosjektnr;
	private String prosjektnavn;
	private Date ansvarFradato;
	private Date ansvarTildato;
	private String ansvar;
	private String selskapskode;
	
	public ArbeidsordreView() {
	}
	@Id
	public String getArbeidsordrenr(){
		return arbeidsordrenr;
	}
	public void setArbeidsordrenr(String arbeidsordrenr){
		this.arbeidsordrenr = arbeidsordrenr;
	}
	public String getArbeidsordrenavn(){
		return arbeidsordrenavn;
	}
	public void setArbeidsordrenavn(String arbeidsordrenavn){
		this.arbeidsordrenavn = arbeidsordrenavn;
	}
	public String getProsjektnr() {
		return prosjektnr;
	}

	public void setProsjektnr(String prosjektnr) {
		this.prosjektnr = prosjektnr;
	}

	public String getProsjektnavn() {
		return prosjektnavn;
	}

	public void setProsjektnavn(String prosjektnavn) {
		this.prosjektnavn = prosjektnavn;
	}

	@Column(name = "ANSVAR_FRADATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAnsvarFradato() {
		return ansvarFradato;
	}

	public void setAnsvarFradato(Date ansvarFradato) {
		this.ansvarFradato = ansvarFradato;
	}

	@Column(name = "ANSVAR_TILDATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAnsvarTildato() {
		return ansvarTildato;
	}

	public void setAnsvarTildato(Date ansvarTildato) {
		this.ansvarTildato = ansvarTildato;
	}

	public String getAnsvar() {
		return ansvar;
	}

	public void setAnsvar(String ansvar) {
		this.ansvar = ansvar;
	}

	public String getSelskapskode() {
		return selskapskode;
	}

	public void setSelskapskode(String selskapskode) {
		this.selskapskode = selskapskode;
	}

	public String getTextForGUI() {
		return getArbeidsordrenr() + " - " + getArbeidsordrenavn();
	}
}
