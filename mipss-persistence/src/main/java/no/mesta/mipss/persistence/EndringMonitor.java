package no.mesta.mipss.persistence;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;


/**
 * JPA Entity Listener som setter tidsstempling på alle endringer 
 * i tabellposter som krever det rett før persistering.
 * Omfatter støtte for både ENDRE_* og OPPRETT_* feltene.
 *
 * @author jorge
 */
public class EndringMonitor<I extends IOwnableMipssEntity> {
    public EndringMonitor() {}
    
    /**
     * Setter tidsstempel for opprettelsen av en post
     * som inneholder OPPRETTET_AV og OPPRETTET_DATO felt.
     * @param ent
     */
    @PrePersist
    public void monitorOpprett(I ent) {
        OwnedMipssEntity owner = ent.getOwnedMipssEntity();
        if (owner.getOpprettetDato()==null) //kan ikke sette dato mer enn en gang fra EndringsMonitor
        	owner.setOpprettetDato(Clock.now());
    }
    
    /**
     * Setter tidsstempel for opprettelsen av en post
     * som inneholder ENDRET_AV og ENDRET_DATO felt.
     * @param ent
     */
    @PreUpdate
    public void monitorEndre(I ent) {
        OwnedMipssEntity owner = ent.getOwnedMipssEntity();
        owner.setEndretDato(Clock.now());
    }
}
