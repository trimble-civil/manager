package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@NamedQuery(name = JobbInfo.FIND_ALL, query = "SELECT j FROM JobbInfo j")
@Entity
public class JobbInfo {
	
	public static final String FIND_ALL = "JobbInfo.findAll"; 
	
	private Long jobbNummer;
	private String navn;
	private String frekvens;
	private String enhet;
	private Long rundetidSekundGrense;
	private Long kjoretidMinuttGrense;
	private Long sisteKjoringMinuttGrense;
	private Long varselAntallGrense;
	private Long alvorligAntallGrense;
	private String fritekst;
	
	@Id
	@Column(name = "JOBBNUMMER")
	public Long getJobbNummer() {
		return jobbNummer;
	}
	
	@Column(name = "NAVN", nullable = false)
	public String getNavn() {
		return navn;
	}

	@Column(name = "FREKVENS")
	public String getFrekvens() {
		return frekvens;
	}

	@Column(name = "HVA_SOM_TELLES")
	public String getEnhet() {
		return enhet;
	}

	@Column(name = "GRENSE_RUNDETID_SEKUNDER")
	public Long getRundetidSekundGrense() {
		return rundetidSekundGrense;
	}

	@Column(name = "GRENSE_KJORETID_MINUTTER")
	public Long getKjoretidMinuttGrense() {
		return kjoretidMinuttGrense;
	}

	@Column(name = "GRENSE_ANTALL_VARSEL")
	public Long getVarselAntallGrense() {
		return varselAntallGrense;
	}

	@Column(name = "GRENSE_ANTALL_ALVORLIG")
	public Long getAlvorligAntallGrense() {
		return alvorligAntallGrense;
	}
	
	@Column(name = "GRENSE_SIST_KJORING_MINUTTER")
	public Long getSisteKjoringMinuttGrense() {
		return sisteKjoringMinuttGrense;
	}

	@Column(name = "FRITEKST")
	public String getFritekst() {
		return fritekst;
	}
	
	public void setJobbNummer(Long jobbNummer) {
		this.jobbNummer = jobbNummer;
	}
	
	public void setNavn(String navn) {
		this.navn = navn;
	}
	
	public void setFrekvens(String frekvens) {
		this.frekvens = frekvens;
	}
	
	public void setEnhet(String enhet) {
		this.enhet = enhet;
	}
	
	public void setRundetidSekundGrense(Long rundetidSekundGrense) {
		this.rundetidSekundGrense = rundetidSekundGrense;
	}
	
	public void setKjoretidMinuttGrense(Long kjoretidMinuttGrense) {
		this.kjoretidMinuttGrense = kjoretidMinuttGrense;
	}
	
	public void setVarselAntallGrense(Long varselAntallGrense) {
		this.varselAntallGrense = varselAntallGrense;
	}
	
	public void setAlvorligAntallGrense(Long alvorligAntallGrense) {
		this.alvorligAntallGrense = alvorligAntallGrense;
	}
	
	public void setSisteKjoringMinuttGrense(Long sisteKjoringMinuttGrense) {
		this.sisteKjoringMinuttGrense = sisteKjoringMinuttGrense;
	}
	
	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}
	
}