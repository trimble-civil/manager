package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Sensortype.FIND_ALL, query = "select o from Sensortype o order by o.navn")
})
public class Sensortype extends MipssEntityBean<Sensortype> implements Serializable, IRenderableMipssEntity {
    public static final String FIND_ALL = "Sensortype.findAll";
    
    private String beskrivelse;
    private String navn;
    private List<KjoretoyUtstyrIo> kjoretoyUtstyrIoList = new ArrayList<KjoretoyUtstyrIo>();

    public Sensortype() {
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @OneToMany(mappedBy = "sensortype")
    public List<KjoretoyUtstyrIo> getKjoretoyUtstyrIoList() {
        return kjoretoyUtstyrIoList;
    }

    public void setKjoretoyUtstyrIoList(List<KjoretoyUtstyrIo> kjoretoyUtstyrIoList) {
        this.kjoretoyUtstyrIoList = kjoretoyUtstyrIoList;
    }

    public KjoretoyUtstyrIo addKjoretoyUtstyrIo(KjoretoyUtstyrIo kjoretoyUtstyrIo) {
        getKjoretoyUtstyrIoList().add(kjoretoyUtstyrIo);
        kjoretoyUtstyrIo.setSensortype(this);
        return kjoretoyUtstyrIo;
    }

    public KjoretoyUtstyrIo removeKjoretoyUtstyrIo(KjoretoyUtstyrIo kjoretoyUtstyrIo) {
        getKjoretoyUtstyrIoList().remove(kjoretoyUtstyrIo);
        kjoretoyUtstyrIo.setSensortype(null);
        return kjoretoyUtstyrIo;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Sensortype)) {
            return false;
        }
        Sensortype other = (Sensortype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getNavn()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Sensortype other = (Sensortype) o;
            return new CompareToBuilder().
                append(getNavn(), other.getNavn()).
                toComparison();
    }
}
