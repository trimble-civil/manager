package no.mesta.mipss.persistence.driftslogg;

public interface Warning {

    // TODO: move these to a property file?
    String WARNING_MANUALLY_CREATED = "Manuelt opprettet";
    String WARNING_NO_PAYMENT = "Intet oppgjør";
    String WARNING_PRODUCTION_OUTSIDE_RODE = "Brøyting utenfor brøyterode";
    String WARNING_PRODUCTION_OUTSIDE_CONTRACT = "Produksjon utenfor kontrakt";
    String WARNING_ARTICLE_CHANGED = "Artikkel endret";

    default boolean isIgnored() {
        return false;
    }

    boolean isArticleChanged();

    boolean isProductionOutsideRode();

    boolean isProductionOutsideContract();

    boolean hasNoPayment();

    boolean isManuallyCreated();

    default boolean containsWarnings() {
        return (hasNoPayment() || isManuallyCreated() || isArticleChanged() || isProductionOutsideContract() || isProductionOutsideRode());
    }

    default String getWarningString() {
        if (isIgnored()) {
            return null;
        }

        if (!(hasNoPayment() || isManuallyCreated() || isArticleChanged() || isProductionOutsideContract() || isProductionOutsideRode())){
            return "Ingen systemgenererte varsler";
        }

        StringBuilder sb = new StringBuilder();
        String separator = "";

        if (isProductionOutsideRode()) {
            sb.append(WARNING_PRODUCTION_OUTSIDE_RODE);
            separator = ", ";
        }

        if (isProductionOutsideContract()) {
            sb.append(separator).append(WARNING_PRODUCTION_OUTSIDE_CONTRACT);
            separator = ", ";
        }

        if (hasNoPayment()) {
            sb.append(separator).append(WARNING_NO_PAYMENT);
            separator = ", ";
        }

        if(isArticleChanged()) {
            sb.append(separator).append(WARNING_ARTICLE_CHANGED);
            separator = ", ";
        }

        if (isManuallyCreated()) {
            sb.append(separator).append(WARNING_MANUALLY_CREATED);
        }

        return sb.toString();
    }

}