package no.mesta.mipss.persistence.mipss1;


//@Entity
//@Table(name="KUNDEKONTRAKT")
//
//@NamedQuery(name = M1Kundekontrakt.QUERY_FIND_ONE, query = "select o from M1Kundekontrakt o where o.kundekontraktId=:kundekontraktId")

public class M1Kundekontrakt {
	public final static String QUERY_FIND_ONE = "M1Kundekontrakt.findOne";
	
//	@Column(name="KUNDEKONTRAKT_ID")
//	@Id
//	private Long kundekontraktId;
//	@Column(name="KUNDE_ID", nullable = false)
//	private Long kundeId;
//	@Column(name="KUNDEADRESSE_ID", nullable = false)
//	private Long kundeadresseId;
//	@Column(name="KONTRAKTTYPE_ID")
//	private Long kontrakttypeId;
//	@Column(name="KONTRAKTNUMMER")
//	private String kontraktnummer;
//	@Column(name="KONTRAKTNAVN")
//	private String kontraktnavn;
//	@Column(name="BESKRIVELSE")
//	private String beskrivelse;
//	@Column(name="DATO_GYLDIG_FRA")
//	@Temporal(TIMESTAMP)
//	private Date datoGyldigFra;
//	@Column(name="DATO_GYLDIG_TIL")
//	@Temporal(TIMESTAMP)
//	private Date datoGyldigTil;
//	
//	
//	
//	public Long getKundekontraktId() {
//		return kundekontraktId;
//	}
//	public void setKundekontraktId(Long kundekontraktId) {
//		this.kundekontraktId = kundekontraktId;
//	}
//	public Long getKundeId() {
//		return kundeId;
//	}
//	public void setKundeId(Long kundeId) {
//		this.kundeId = kundeId;
//	}
//	public Long getKundeadresseId() {
//		return kundeadresseId;
//	}
//	public void setKundeadresseId(Long kundeadresseId) {
//		this.kundeadresseId = kundeadresseId;
//	}
//	public Long getKontrakttypeId() {
//		return kontrakttypeId;
//	}
//	public void setKontrakttypeId(Long kontrakttypeId) {
//		this.kontrakttypeId = kontrakttypeId;
//	}
//	public String getKontraktnummer() {
//		return kontraktnummer;
//	}
//	public void setKontraktnummer(String kontraktnummer) {
//		this.kontraktnummer = kontraktnummer;
//	}
//	public String getKontraktnavn() {
//		return kontraktnavn;
//	}
//	public void setKontraktnavn(String kontraktnavn) {
//		this.kontraktnavn = kontraktnavn;
//	}
//	public String getBeskrivelse() {
//		return beskrivelse;
//	}
//	public void setBeskrivelse(String beskrivelse) {
//		this.beskrivelse = beskrivelse;
//	}
//	public Date getDatoGyldigFra() {
//		return datoGyldigFra;
//	}
//	public void setDatoGyldigFra(Date datoGyldigFra) {
//		this.datoGyldigFra = datoGyldigFra;
//	}
//	public Date getDatoGyldigTil() {
//		return datoGyldigTil;
//	}
//	public void setDatoGyldigTil(Date datoGyldigTil) {
//		this.datoGyldigTil = datoGyldigTil;
//	}
//	
//	/**
//     * @see java.lang.Object#toString()
//     * @see org.apache.commons.lang.builder.ToStringBuilder
//     * 
//     * @return
//     */
//    @Override
//    public String toString() {
//        return new ToStringBuilder(this).
//            append("kundekontraktId", kundekontraktId).
//            append("kontraktnummer", kontraktnummer).
//            append("kontraktnavn", kontraktnavn).
//            append("beskrivelse", beskrivelse).
//            append("datoGyldigFra", datoGyldigFra).
//            append("datoGyldigTil", datoGyldigTil).
//            toString();
//    }
//	
}
