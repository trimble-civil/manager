package no.mesta.mipss.persistence.applikasjon;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OptimisticLockableEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.VersionColumn;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Inneholder system og modulkonfigurasjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity(name="Konfigparam")
@Table(name="KONFIGPARAM")
@IdClass(KonfigparamPK.class)
@NamedQueries({
	@NamedQuery(name = Konfigparam.QUERY_GET_ALL_APPLICATIONS, query = "SELECT DISTINCT k.applikasjon FROM Konfigparam k"),
    @NamedQuery(name = Konfigparam.QUERY_FIND_ALL, query = "SELECT k FROM Konfigparam k"),
    @NamedQuery(name = Konfigparam.QUERY_GET_FOR_APPLICATION, query = "SELECT k FROM Konfigparam k WHERE k.applikasjon = :app"),
    @NamedQuery(name = Konfigparam.QUERY_GET_ONE, query = "SELECT k FROM Konfigparam k WHERE k.applikasjon = :app AND k.navn = :navn"),
    @NamedQuery(name = Konfigparam.DELETE_FOR_APPLICATION, query = "DELETE FROM Konfigparam k WHERE k.applikasjon = :app"),
    @NamedQuery(name = Konfigparam.DELETE, query = "DELETE FROM Konfigparam k WHERE k.applikasjon = :app AND k.navn = :navn")
})
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class Konfigparam extends MipssEntityBean<Konfigparam> implements Serializable, IOwnableMipssEntity, IRenderableMipssEntity, OptimisticLockableEntity, PropertyChangeSource {

    private static final Logger log = LoggerFactory.getLogger(Konfigparam.class);

	public static final String QUERY_GET_ALL_APPLICATIONS = "Konfigparam.getAllApplications";
    public static final String QUERY_FIND_ALL = "Konfigparam.findAll";
    public static final String QUERY_GET_FOR_APPLICATION = "Konfigparam.getForApp";
    public static final String QUERY_GET_ONE = "Konfigparam.getOne";
    public static final String DELETE_FOR_APPLICATION = "Konfigparam.deleteForApp";
    public static final String DELETE = "Konfigparam.delete";
    
    private String applikasjon;
    private String navn;
    private String verdi;
    private String beskrivelse;
    
    private OwnedMipssEntity ownedMipssEntity = new OwnedMipssEntity();
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    private VersionColumn version; 
      
	public Konfigparam() {
        log("Konfigparam() constructed");
    }

    @Id
    @Column(nullable = false)
    public String getApplikasjon() {
        return applikasjon;
    }
    
    public void setApplikasjon(String applikasjon) {
        log.trace("setApplikasjon({})", applikasjon);
        String old = this.applikasjon;
        this.applikasjon = applikasjon;
        props.firePropertyChange("applikasjon", old, applikasjon);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }
    
    public void setNavn(String navn) {
        log.trace("setNavn({})", navn);
        String old = this.navn;
        this.navn = navn;
        props.firePropertyChange("navn", old, navn);
    }

    @Column(nullable = false)
    public String getVerdi() {
        return verdi;
    }
    
    public void setVerdi(String verdi) {
        log.trace("setVerdi({})", verdi);
        String old = this.verdi;
        this.verdi = verdi;
        props.firePropertyChange("verdi", old, verdi);
    }

    @Column(nullable = false)
    public String getBeskrivelse() {
        return beskrivelse;
    }
    
    public void setBeskrivelse(String beskrivelse) {
        log.trace("setBeskrivelse({})", beskrivelse);
        String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        props.firePropertyChange("beskrivelse", old, beskrivelse);
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        return ownedMipssEntity;
    }

    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        log.trace("setOwnedMipssEntity({})", ownedMipssEntity);
        OwnedMipssEntity old = this.ownedMipssEntity;
        this.ownedMipssEntity = ownedMipssEntity;
        props.firePropertyChange("ownedMipssEntity", old, ownedMipssEntity);
    }
    
    @Embedded
    public VersionColumn getVersion() {
		return version;
	}
    
    public void setVersion(VersionColumn version) {
		this.version = version;
	}
    
    public String getTextForGUI() {
		return navn;
	}
    
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }
    public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }

	/** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(applikasjon).append(navn).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Konfigparam)) {return false;}
        
        Konfigparam other = (Konfigparam) o;
        
        return new EqualsBuilder().
            append(applikasjon, other.getApplikasjon()).
            append(navn, other.getNavn()).
            isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("applikasjon", getApplikasjon()).
            append("navn", getNavn()).
            append("verdi", getVerdi()).
            append("beskrivelse", getBeskrivelse()).
            append("ownedMipssEntity", getOwnedMipssEntity()).
            toString();        
    }
}
