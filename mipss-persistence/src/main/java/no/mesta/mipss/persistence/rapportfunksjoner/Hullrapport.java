package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
/**
 * Klasse som holder p� dataene til hull og overlapprapporten.
 * @author harkul 
 */
public class Hullrapport implements Serializable, IRenderableMipssEntity{
	private String kontraktNummer;
	private String kontraktNavn;
	private Double kmOverlapp;
	private Double kmHull;
	private Double kmUtenfor;
	
	public static final String[] getPropertyArray(){
		return new String[]{"kontraktNummer", "kontraktNavn", "kmOverlapp", "kmHull", "kmUtenfor"};
	}
	public String getTextForGUI() {
		return null;
	}

	public String getKontraktNummer() {
		return kontraktNummer;
	}

	public void setKontraktNummer(String kontraktNummer) {
		this.kontraktNummer = kontraktNummer;
	}

	public String getKontraktNavn() {
		return kontraktNavn;
	}

	public void setKontraktNavn(String kontraktNavn) {
		this.kontraktNavn = kontraktNavn;
	}

	public Double getKmOverlapp() {
		return kmOverlapp;
	}

	public void setKmOverlapp(Double kmOverlapp) {
		this.kmOverlapp = kmOverlapp;
	}

	public Double getKmHull() {
		return kmHull;
	}

	public void setKmHull(Double kmHull) {
		this.kmHull = kmHull;
	}
	public void setKmUtenfor(Double kmUtenfor) {
		this.kmUtenfor = kmUtenfor;
	}
	public Double getKmUtenfor() {
		return kmUtenfor;
	}
	
}
