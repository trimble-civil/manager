package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * Klasse for view mot hendelse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "HENDELSE_TRAFIKKTILTAK")
@IdClass(TrafikktiltakSokPK.class)
public class TrafikktiltakSok extends MipssEntityBean<TrafikktiltakSok> implements Serializable{
	private static final String ID_NAME_HENDELSEGUID = "hendelseGuid";
	private static final String ID_NAME_TYPE = "typeId";
	private static final String[] ID_NAMES = new String[] {ID_NAME_TYPE, ID_NAME_HENDELSEGUID};
    private String beskrivelse;
    private HendelseSokView hendelse;
    private String hendelseGuid;
    private Trafikktiltak type;
    private Long typeId;
    
    /**
     * Konstruktør
     * 
     */
    public TrafikktiltakSok() {
    }

    /**
     * Beskrivelse
     * 
     * @return
     */
    public String getBeskrivelse() {
        return beskrivelse;
    }

    /**
     * Hendelse
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "HENDELSE_GUID", referencedColumnName = "GUID", nullable = false, insertable = true, updatable = true)
    public HendelseSokView getHendelse() {
        return hendelse;
    }

    /**
     * Hendelse
     * 
     * @return
     */
    @Id
    @Column(name = "HENDELSE_GUID", nullable = false, insertable = false, updatable = false)
    public String getHendelseGuid() {
        return hendelseGuid;
    }

    /** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

    /**
     * Type
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "TILTAK_ID", referencedColumnName = "ID")
    public Trafikktiltak getType() {
        return type;
    }

    /**
     * Type
     * 
     * @return
     */
    @Id
    @Column(name = "TILTAK_ID", nullable = false, insertable = false, updatable = false)
    public Long getTypeId() {
        return typeId;
    }

    /**
     * Beskrivelse
     * 
     * @param beskrivelse
     */
    public void setBeskrivelse(String beskrivelse) {
        this.beskrivelse = beskrivelse;
    }

    /**
     * Hendelse
     * 
     * @param hendelse
     */
    public void setHendelse(HendelseSokView hendelse) {
        this.hendelse = hendelse;
        
        if(hendelse != null) {
            setHendelseGuid(hendelse.getGuid());
        } else {
            setHendelseGuid(null);
        }
    }

    /**
     * Hendelse
     * 
     * @param hendelsesGuid
     */
    public void setHendelseGuid(String hendelsesGuid) {
        this.hendelseGuid = hendelsesGuid;
    }

    /**
     * Type
     * 
     * @param type
     */
    public void setType(Trafikktiltak type) {
        this.type = type;
        
        if(type != null) {
            setTypeId(type.getId());
        } else {
            setTypeId(null);
        }
    }

    /**
     * Type
     * 
     * @param typeId
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

}
