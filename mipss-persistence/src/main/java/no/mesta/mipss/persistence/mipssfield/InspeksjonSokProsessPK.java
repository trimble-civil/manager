package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * PK klasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class InspeksjonSokProsessPK implements Serializable{
    private String inspeksjonGuid;
    private Long prosessId;
    
    public InspeksjonSokProsessPK() {
    }

    public void setInspeksjonGuid(String inspeksjonGuid) {
        this.inspeksjonGuid = inspeksjonGuid;
    }

    public String getInspeksjonGuid() {
        return inspeksjonGuid;
    }

    public void setProsessId(Long prosessId) {
        this.prosessId = prosessId;
    }

    public Long getProsessId() {
        return prosessId;
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof InspeksjonSokProsessPK)) {return false;}
        
        InspeksjonSokProsessPK other = (InspeksjonSokProsessPK) o;
        
        return new EqualsBuilder().append(inspeksjonGuid, other.getInspeksjonGuid()).append(prosessId, other.getProsessId()).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(inspeksjonGuid).append(prosessId).toHashCode();
    }
}
