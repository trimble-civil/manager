package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Prodreflinkseksjon
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Prodreflinkseksjon.QUERY_FIND_ALL, query = "select o from Prodreflinkseksjon o")
@IdClass(ProdreflinkseksjonPK.class)
public class Prodreflinkseksjon implements Serializable {
    public static final String QUERY_FIND_ALL = "Prodreflinkseksjon.findAll";
    private double fra;
    private int reflinkIdent;
    private Long strekningId;
    private double til;
    private Prodstrekning prodstrekning;

    public Prodreflinkseksjon() {
    }

    @Id
    @Column(nullable = false)
    public double getFra() {
        return fra;
    }

    public void setFra(double fra) {
        this.fra = fra;
    }

    @Id
    @Column(name="REFLINK_IDENT", nullable = false)
    public int getReflinkIdent() {
        return reflinkIdent;
    }

    public void setReflinkIdent(int reflinkIdent) {
        this.reflinkIdent = reflinkIdent;
    }

    @Id
    @Column(name="STREKNING_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getStrekningId() {
        return strekningId;
    }

    public void setStrekningId(Long strekningId) {
        this.strekningId = strekningId;
    }

    @Column(nullable = false)
    public double getTil() {
        return til;
    }

    public void setTil(double til) {
        this.til = til;
    }

    @ManyToOne
    @JoinColumn(name = "STREKNING_ID", referencedColumnName = "ID")
    public Prodstrekning getProdstrekning() {
        return prodstrekning;
    }

    public void setProdstrekning(Prodstrekning prodstrekning) {
        this.prodstrekning = prodstrekning;
        
        if(prodstrekning != null) {
            setStrekningId(prodstrekning.getId());
        } else {
            setStrekningId(null);
        }
    }
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("strekningId", strekningId).
            append("reflinkIdent", reflinkIdent).
            append("fra", fra).
            append("til", til).
            toString();
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Prodreflinkseksjon)) {return false;}
        
        Prodreflinkseksjon other = (Prodreflinkseksjon) o;
        return new EqualsBuilder().append(strekningId, other.getStrekningId()).append(reflinkIdent, other.getReflinkIdent()).append(fra, other.getFra()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(6959,6961).append(strekningId).append(reflinkIdent).append(fra).toHashCode();
    }
    
}
