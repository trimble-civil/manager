package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestillingmaster;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries( {
		@NamedQuery(name = Kjoretoyordre.FIND_ALL, query = "select o from Kjoretoyordre o"),
		@NamedQuery(name = Kjoretoyordre.FIND_BY_KJORETOY, query = "select o from Kjoretoyordre o where o.kjoretoy.id = :id"),
		@NamedQuery(name = Kjoretoyordre.FIND_BY_KONTRAKT, query = "select DISTINCT o FROM Kjoretoyordre o WHERE o.driftkontrakt.id = :id") })
@EntityListeners( { no.mesta.mipss.persistence.EndringMonitor.class })
@SequenceGenerator(name = "kjoretoyordreIdSeq", sequenceName = "KJORETOYORDRE_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Kjoretoyordre extends MipssEntityBean<Kjoretoyordre> implements
		Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "Kjoretoyordre.findAll";
	public static final String FIND_BY_KJORETOY = "Kjoretoyordre.findByKjoretoy";
	public static final String FIND_BY_KONTRAKT = "Kjoretoyordre.findByKontrakt";

	private Dfuindivid dfuindivid;
	private String fritekst;
	private Long id;
	private String selskapskode;
	private String prosjektNr;
	private String signatur;
	private List<Kjoretoyordre_Status> kjoretoyordre_StatusList = new ArrayList<Kjoretoyordre_Status>();
	private Kjoretoyordretype kjoretoyordretype;
	private List<Kjoretoyordrelogg> kjoretoyordreloggList = new ArrayList<Kjoretoyordrelogg>();
	private Kjoretoy kjoretoy;
	private String prosjektnavn;
	private String ansvar;
	private Boolean verkstedAdminFlagg;

	private Driftkontrakt driftkontrakt;
	private Installasjon installasjon;
	private String kontaktnavn;
	private Long kontakttelefon;
	private Boolean utenUtstyr;
	private Boolean maaHaBryterboks;

	private OwnedMipssEntity ownedMipssEntity;
	private List<Dfubestillingmaster> dfubestillingmasterList = new ArrayList<Dfubestillingmaster>();
	
	private List<KjoretoyordreUtstyr> kjoretoyordreUtstyrList = new ArrayList<KjoretoyordreUtstyr>();

	public Kjoretoyordre() {
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoyordreIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSelskapskode() {
		return selskapskode;
	}

	public void setSelskapskode(String selskapskode) {
		this.selskapskode = selskapskode;
	}

	@Column(name = "PROSJEKT_NR")
	public String getProsjektNr() {
		return prosjektNr;
	}

	public void setProsjektNr(String prosjektNr) {
		Object oldValue = this.prosjektNr;
		this.prosjektNr = prosjektNr;
		getProps().firePropertyChange("prosjektNr", oldValue, prosjektNr);
	}

	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}

	@OneToMany(mappedBy = "kjoretoyordre", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<Kjoretoyordre_Status> getKjoretoyordre_StatusList() {
		return kjoretoyordre_StatusList;
	}

	public void setKjoretoyordre_StatusList(
			List<Kjoretoyordre_Status> kjoretoyordre_StatusList) {
		this.kjoretoyordre_StatusList = kjoretoyordre_StatusList;
	}

	@Transient
	public Kjoretoyordrestatus getSisteKjoretoyordrestatus() {
		Date date = null;
		Kjoretoyordrestatus status = null;
		for (Kjoretoyordre_Status kjoretoyordre_Status : kjoretoyordre_StatusList) {
			Date opprettetDato = kjoretoyordre_Status.getOwnedMipssEntity().getOpprettetDato();
			if (date == null || date.before(opprettetDato)) {
				date = opprettetDato;
				status = kjoretoyordre_Status.getKjoretoyordrestatus();
			}
		}
		return status;
	}

	@Transient
	public Date getSisteKjoretoyordrestatusEndring() {
		Date date = null;
		for (Kjoretoyordre_Status kjoretoyordre_Status : kjoretoyordre_StatusList) {
			Date opprettetDato = kjoretoyordre_Status.getOwnedMipssEntity()
					.getOpprettetDato();
			if (date == null || date.before(opprettetDato)) {
				date = opprettetDato;
			}
		}
		return date;
	}
	
	@Transient
	public String getKjoretoyEksterntNavn() {
		if (kjoretoy != null) {
			return kjoretoy.getEksterntNavn();
		} 
		return "";
	}

	@Transient
	public String getKjoretoyMaskinnummer() {
		if (kjoretoy != null) {
			return kjoretoy.getMaskinnummerString();
		} 
		return "";
	}

	@Transient
	public String getKjoretoyRegnr() {
		if (kjoretoy != null) {
			return kjoretoy.getRegnr();
		}
		return "";
	}
	@Transient
	public String getKjoretoyLeverandorNr(){
		if (kjoretoy != null){
			Leverandor leverandor = kjoretoy.getLeverandor();
			if (leverandor!=null){
				return leverandor.getNr();
			}
		}
		return "";
	}
	@Transient
	public String getDfuSerienummer(){
		if (dfuindivid!=null){
			return dfuindivid.getSerienummer();
		}
		return null;
	}
	@ManyToOne
	@JoinColumn(name = "TYPE_NAVN", referencedColumnName = "NAVN")
	public Kjoretoyordretype getKjoretoyordretype() {
		return kjoretoyordretype;
	}

	public void setKjoretoyordretype(Kjoretoyordretype kjoretoyordretype) {
		Object oldValue = this.kjoretoyordretype;
		this.kjoretoyordretype = kjoretoyordretype;
		getProps().firePropertyChange("kjoretoyordretype", oldValue,
				kjoretoyordretype);
	}

	@OneToMany(mappedBy = "kjoretoyordre", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<Kjoretoyordrelogg> getKjoretoyordreloggList() {
		return kjoretoyordreloggList;
	}

	public void setKjoretoyordreloggList(
			List<Kjoretoyordrelogg> kjoretoyordreloggList) {
		this.kjoretoyordreloggList = kjoretoyordreloggList;
	}

	public Kjoretoyordrelogg addKjoretoyordrelogg(
			Kjoretoyordrelogg kjoretoyordrelogg) {
		getKjoretoyordreloggList().add(kjoretoyordrelogg);
		kjoretoyordrelogg.setKjoretoyordre(this);
		return kjoretoyordrelogg;
	}

	public Kjoretoyordrelogg removeKjoretoyordrelogg(
			Kjoretoyordrelogg kjoretoyordrelogg) {
		getKjoretoyordreloggList().remove(kjoretoyordrelogg);
		kjoretoyordrelogg.setKjoretoyordre(null);
		return kjoretoyordrelogg;
	}

	@ManyToOne
	@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}

	public void setKjoretoy(Kjoretoy kjoretoy) {
		this.kjoretoy = kjoretoy;
	}
	@Column(name="UTEN_UTSTYR_FLAGG", nullable = false)
	public Boolean getUtenUtstyr() {
		return utenUtstyr;
	}

	public void setUtenUtstyr(Boolean utenUtstyr) {
		this.utenUtstyr = utenUtstyr;
	}
	
	@Column(name="MAA_HA_BRYTERBOKS_FLAGG", nullable = false)
	public Boolean getMaaHaBryterboks() {
		return maaHaBryterboks;
	}

	public void setMaaHaBryterboks(Boolean maaHaBryterboks) {
		this.maaHaBryterboks = maaHaBryterboks;
	}

	public String getTextForGUI() {
		return String.valueOf(getId());
	}

	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		if (ownedMipssEntity == null) {
			ownedMipssEntity = new OwnedMipssEntity();
		}
		return ownedMipssEntity;
	}

	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		this.ownedMipssEntity = ownedMipssEntity;
	}

	@Transient
	public String getProsjektnavn() {
		return prosjektnavn;
	}

	public void setProsjektnavn(String prosjektnavn) {
		this.prosjektnavn = prosjektnavn;
	}

	@Transient
	public String getAnsvar() {
		return ansvar;
	}

	public void setAnsvar(String ansvar) {
		this.ansvar = ansvar;
	}

	@Column(name = "VERKSTED_ADMIN_FLAGG")
	public Boolean getVerkstedAdminFlagg() {
		return verkstedAdminFlagg;
	}

	public void setVerkstedAdminFlagg(Boolean verkstedAdminFlagg) {
		this.verkstedAdminFlagg = verkstedAdminFlagg;
	}

	@OneToMany(mappedBy = "kjoretoyordre", cascade = CascadeType.ALL)
	public List<Dfubestillingmaster> getDfubestillingmasterList() {
		return dfubestillingmasterList;
	}

	public void setDfubestillingmasterList(
			List<Dfubestillingmaster> dfubestillingmasterList) {
		this.dfubestillingmasterList = dfubestillingmasterList;
	}

	@ManyToOne
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		Object oldValue = this.driftkontrakt;
		this.driftkontrakt = driftkontrakt;
		getProps().firePropertyChange("driftkontrakt", oldValue, driftkontrakt);
	}
	
	@ManyToOne
	@JoinColumn(name ="SENDT_DFU_ID", referencedColumnName = "ID")
	public Dfuindivid getDfuindivid(){
		return dfuindivid;
	}
	
	public void setDfuindivid(Dfuindivid dfuindivid){
		Dfuindivid old = this.dfuindivid;
		this.dfuindivid = dfuindivid;
		getProps().firePropertyChange("dfuindivid", old, dfuindivid);
		
	}

	@ManyToOne
	@JoinColumn(name = "INSTALLASJON_ID", referencedColumnName = "ID")
	public Installasjon getInstallasjon() {
		return installasjon;
	}

	public void setInstallasjon(Installasjon installasjon) {
		Object oldValue = this.installasjon;
		this.installasjon = installasjon;
		getProps().firePropertyChange("installasjon", oldValue, installasjon);
	}

	public String getKontaktnavn() {
		return kontaktnavn;
	}

	public void setKontaktnavn(String kontaktnavn) {
		Object oldValue = this.kontaktnavn;
		this.kontaktnavn = kontaktnavn;
		getProps().firePropertyChange("kontaktnavn", oldValue, kontaktnavn);
	}

	public Long getKontakttelefon() {
		return kontakttelefon;
	}

	public void setKontakttelefon(Long kontakttelefon) {
		Object oldValue = this.kontakttelefon;
		this.kontakttelefon = kontakttelefon;
		getProps().firePropertyChange("kontakttelefon", oldValue,
				kontakttelefon);
	}

	/**
	 * Genererer en lesbar tekst med hovedlementene i entitybønnen.
	 * 
	 * @return en tekstlinje.
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("Id", getId()).append(
				"prosjekt nr", getProsjektNr()).append("signatur",
				getSignatur()).toString();
	}

	/**
	 * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
	 * 
	 * @param o
	 * @return true hvis nøklene er like.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof Kjoretoyordre)) {
			return false;
		}
		Kjoretoyordre other = (Kjoretoyordre) o;
		return new EqualsBuilder().append(getId(), other.getId()).isEquals();
	}

	/**
	 * Genererer en hashkode basert kun på Id.
	 * 
	 * @return hashkoden.
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(37, 79).append(getId()).append(
				getProsjektNr()).append(getSignatur()).toHashCode();
	}

	/**
	 * Sorteringskriterier
	 */
	public int compareTo(Object o) {
		Kjoretoyordre other = (Kjoretoyordre) o;
		return new CompareToBuilder().append(id, other.getId()).toComparison();
	}

	@Override
	public String[] getIdNames() {
		return new String[] { "id" };
	}

	@Transient
	public Dfubestillingmaster getDfubestillingmasterForBestilling() {
		return getDfubestillingmasterList().get(0);
	}

	@OneToMany(mappedBy = "kjoretoyordre", cascade = CascadeType.ALL)
	public List<KjoretoyordreUtstyr> getKjoretoyordreUtstyrList() {
		return kjoretoyordreUtstyrList;
	}

	public void setKjoretoyordreUtstyrList(
			List<KjoretoyordreUtstyr> kjoretoyordreUtstyrList) {
		this.kjoretoyordreUtstyrList = kjoretoyordreUtstyrList;
	}
}
