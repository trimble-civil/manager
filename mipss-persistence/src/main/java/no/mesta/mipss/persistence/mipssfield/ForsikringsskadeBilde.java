package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@Table(name = "SKADE_BILDE_XV")
@IdClass(SkadeBildePK.class)
public class ForsikringsskadeBilde extends MipssEntityBean<ForsikringsskadeBilde> implements Serializable{

	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "BILDE_GUID", referencedColumnName = "GUID")
	private Bilde bilde;
	@Id
	@Column(name = "BILDE_GUID", nullable = false, insertable = false, updatable = false)
	private String bildeGuid;
	@ManyToOne
	@JoinColumn(name = "SKADE_GUID", referencedColumnName = "GUID")
	private Forsikringsskade forsikringsskade;
	@Id
	@Column(name = "SKADE_GUID", nullable = false, insertable = false, updatable = false)
	private String forsikringsskadeGuid;
	
	
	public Bilde getBilde() {
		return bilde;
	}
	public Forsikringsskade getForsikringsskade(){
		return forsikringsskade;
	}
	
	public String getBildeGuid(){
		return bildeGuid;
	}
	public String getForsikringsskadeGuid(){
		return forsikringsskadeGuid;
	}
	
	public void setBilde(Bilde bilde) {
		Bilde old = this.bilde;
		this.bilde = bilde;

		if (bilde != null) {
			setBildeGuid(bilde.getGuid());
		} else {
			setBildeGuid(null);
		}

		firePropertyChange("bilde", old, bilde);
	}
	
	/**
	 * Id
	 * 
	 * @param bildeGuid
	 */
	public void setBildeGuid(String bildeGuid) {
		String old = this.bildeGuid;
		this.bildeGuid = bildeGuid;
		firePropertyChange("bildeGuid", old, bildeGuid);
	}
	
	public void setForsikringsskade(Forsikringsskade forsikringsskade) {
		Forsikringsskade old = this.forsikringsskade;
		this.forsikringsskade = forsikringsskade;

		if (forsikringsskade != null) {
			setForsikringsskadeGuid(forsikringsskade.getGuid());
		} else {
			setForsikringsskadeGuid(null);
		}
		firePropertyChange("forsikringsskade", old, forsikringsskade);
	}
	
	public void setForsikringsskadeGuid(String forsikringsskadeGuid) {
		String old = this.forsikringsskadeGuid;
		this.forsikringsskadeGuid = forsikringsskadeGuid;
		firePropertyChange("forsikringsskadeGuid", old, forsikringsskadeGuid);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("bildeGuid", bildeGuid).append("forsikringsskadeGuid", forsikringsskadeGuid).toString();
	}

}
