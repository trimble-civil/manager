package no.mesta.mipss.persistence.kontrakt.kontakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Kontraktkontakttype.FIND_ALL, query = "select o from Kontraktkontakttype o order by o.navn")
public class Kontraktkontakttype extends MipssEntityBean<Kontraktkontakttype> implements IRenderableMipssEntity, Serializable {
	public static final String FIND_ALL = "Kontraktkontakttype.findAll";
	private String navn;
	private String fritekst;
	private Boolean obligatoriskFlagg;
    private Boolean kunEnKontaktFlagg;
	
	public Kontraktkontakttype() {
	}
	
	@Id
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}
	
	@Column(name = "OBLIGATORISK_FLAGG")
	public Boolean getObligatoriskFlagg() {
		return obligatoriskFlagg;
	}

	public void setObligatoriskFlagg(Boolean obligatoriskFlagg) {
		this.obligatoriskFlagg = obligatoriskFlagg;
	}

	@Column(name = "KUN_EN_KONTAKT_FLAGG")
	public Boolean getKunEnKontaktFlagg() {
		return kunEnKontaktFlagg;
	}

	public void setKunEnKontaktFlagg(Boolean kunEnKontaktFlagg) {
		this.kunEnKontaktFlagg = kunEnKontaktFlagg;
	}

	public String getTextForGUI() {
		return getNavn();
	}
	
    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kontraktkontakttype)) {
            return false;
        }
        Kontraktkontakttype other = (Kontraktkontakttype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert på Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,13).
            append(getNavn()).
            toHashCode();
    }
}
