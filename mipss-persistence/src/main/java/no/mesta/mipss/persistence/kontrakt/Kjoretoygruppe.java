package no.mesta.mipss.persistence.kontrakt;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity(name=Kjoretoygruppe.BEAN_NAME)
@SequenceGenerator(name = Kjoretoygruppe.KJORETOYGRUPPE_ID_SEQ, sequenceName = "KJORETOYGRUPPE_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries({
	@NamedQuery(
        name = Kjoretoygruppe.FIND_BY_KONTRAKT, 
        query = "select o from Kjoretoygruppe o where o.kontraktId = :id")
})
public class Kjoretoygruppe extends MipssEntityBean<Kjoretoygruppe>{
	public static final String BEAN_NAME = "Kjoretoygruppe";

	public static final String KJORETOYGRUPPE_ID_SEQ="kjoretoygruppeIdSeq";
	public static final String FIND_BY_KONTRAKT = "Kjoretoygruppe.findByKontrakt";

	private Long id;
	private String navn;
	private String beskrivelse;
	
	private String opprettetAv;
	private Date opprettetDato;
	
	private String endretAv;
	private Date endretDato;
	
	private Driftkontrakt driftkontrakt;
	private Long kontraktId;
	
	private List<KontraktKjoretoyGruppering> kjoretoygruppe;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Kjoretoygruppe.KJORETOYGRUPPE_ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }
	
	public void setId(Long id){
		this.id = id;
	}
	
	@Column(name="KONTRAKT_ID", nullable = true, insertable = false, updatable = false)
    public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		Long old = this.kontraktId;
		this.kontraktId = kontraktId;
		getProps().firePropertyChange("kontraktId", old, kontraktId);
	}

	@ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
    public Driftkontrakt getDriftkontrakt() {
        return driftkontrakt;
    }

    public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
        this.driftkontrakt = driftkontrakt;
        
        if(driftkontrakt != null) {
        	setKontraktId(driftkontrakt.getId());
        } else {
        	setKontraktId(null);
        }
    }
    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
    	String old = this.opprettetAv;
        this.opprettetAv = opprettetAv;
        getProps().firePropertyChange("opprettetAv", old, opprettetAv);
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="OPPRETTET_DATO", nullable = false)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
    	Date old = this.opprettetDato;
        this.opprettetDato = opprettetDato;
        getProps().firePropertyChange("opprettetDato", old, opprettetDato);
    }
    
    @Column(name="ENDRET_AV", nullable = false)
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
    	String old = this.endretAv;
        this.endretAv = endretAv;
        getProps().firePropertyChange("endretAv", old, endretAv);
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="ENDRET_DATO", nullable = false)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
    	Date old = this.endretDato;
        this.endretDato = endretDato;
        getProps().firePropertyChange("endretDato", old, endretDato);
    }
    @Column(name="BESKRIVELSE")
    public String getBeskrivelse(){
    	return beskrivelse;
    }
    
    public void setBeskrivelse(String beskrivelse){
    	String old = this.beskrivelse;
    	this.beskrivelse = beskrivelse;
    	getProps().firePropertyChange("beskrivelse", old, beskrivelse);
    }
    
    @Column(name="NAVN", nullable=false)
    public String getNavn(){
    	return navn;
    }
    public void setNavn(String navn){
    	String old = this.navn;
    	this.navn = navn;
    	getProps().firePropertyChange("navn", old, navn);
    }
    
    @OneToMany(mappedBy = "kjoretoygruppe", cascade = {CascadeType.ALL})
    public List<KontraktKjoretoyGruppering> getKjoretoygruppe(){
    	return this.kjoretoygruppe;
    }
    
    public void setKjoretoygruppe(List<KontraktKjoretoyGruppering> kjoretoygruppe){
    	this.kjoretoygruppe = kjoretoygruppe;
    }
    @Override
    public String getTextForGUI(){
    	return navn;
    }
}
