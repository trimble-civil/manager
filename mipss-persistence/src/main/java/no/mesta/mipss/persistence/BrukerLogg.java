package no.mesta.mipss.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@SequenceGenerator(name = BrukerLogg.ID_SEQ, sequenceName = "BRUKERLOGG_ID_SEQ", initialValue = 1, allocationSize = 1)
@Entity
public class BrukerLogg extends MipssEntityBean<BrukerLogg>{
	public static final String ID_SEQ = "brukerloggIdSeq";
	private Long id;
	
	private String type;
	private String signatur;
	private Date dato;
	private Long varighet;
	private String beskrivelse;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = BrukerLogg.ID_SEQ)
	public Long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	
	@Column(name="BRUKERLOGGTYPE_NAVN")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSignatur() {
		return signatur;
	}
	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="opprettet_dato")
	public Date getDato() {
		return dato;
	}
	public void setDato(Date dato) {
		this.dato = dato;
	}
	public Long getVarighet() {
		return varighet;
	}
	public void setVarighet(Long varighet) {
		this.varighet = varighet;
	}
	public String getBeskrivelse(){
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse){
		this.beskrivelse = beskrivelse;
	}
	@Override
	public String toString() {
		return "BrukerLogg [id=" + id + ", type=" + type + ", signatur="
				+ signatur + ", dato=" + dato + ", varighet=" + varighet
				+ ", beskrivelse=" + beskrivelse + "]";
	}
	
	
}
