package no.mesta.mipss.persistence.kvernetf2;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Entitetsklasse for tiltak
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SequenceGenerator(name = "tiltakIdSeq", sequenceName = "TILTAK_ID_SEQ", initialValue = 1, allocationSize = 1)
@Entity
public class Tiltak {
	private Long id;
	private Prodstrekning prodstrekning;
	private Long kjoretoyId;
	private Prodtype prodtype;
	private Date fraDato;
	private Date tilDato;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tiltakIdSeq")
    @Column(nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
    @JoinColumn(name = "STREKNING_ID", referencedColumnName = "ID")
	public Prodstrekning getProdstrekning() {
		return prodstrekning;
	}
	public void setProdstrekning(Prodstrekning prodstrekning) {
		this.prodstrekning = prodstrekning;
	}
	
	@Column(name="KJORETOY_ID", nullable = false, insertable = false, updatable = false)
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	@ManyToOne
	@JoinColumn(name="PRODTYPE_ID", referencedColumnName = "ID")
	public Prodtype getProdtype() {
		return prodtype;
	}
	public void setProdtype(Prodtype prodtype) {
		this.prodtype = prodtype;
	}
	
	@Column(name="FRA_DATO_TID")
    @Temporal(TemporalType.TIMESTAMP)
	public Date getFraDato() {
		return fraDato;
	}
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	@Column(name="TIL_DATO_TID")
    @Temporal(TemporalType.TIMESTAMP)
	public Date getTilDato() {
		return tilDato;
	}
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	 /** {@inheritDoc} */
    @Override
	public int hashCode(){
    	return new HashCodeBuilder(2429,2171).append(id).toHashCode();
	}
	 /** {@inheritDoc} */
    @Override
	public boolean equals(Object o){
    	if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Tiltak)) {return false;}
        
        Tiltak other = (Tiltak) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
	}
	/** {@inheritDoc} */
    @Override
    public String toString() {
    	return new ToStringBuilder(this)
    		.append("id", id)
    		.append("kjoretoyId", kjoretoyId)
    		.append("fraDato", fraDato)
    		.append("tilDato", tilDato)
    		.toString();
    }
	
}
