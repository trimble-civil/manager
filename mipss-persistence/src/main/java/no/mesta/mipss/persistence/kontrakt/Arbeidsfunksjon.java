package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Arbeidsfunksjon.FIND_ALL, query = "select o from Arbeidsfunksjon o order by o.navn")
@EntityListeners( { no.mesta.mipss.persistence.EndringMonitor.class })
public class Arbeidsfunksjon implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity,
		Comparable<Arbeidsfunksjon> {
	public static final String FIND_ALL = "Arbeidsfunksjon.findAll";
	private String navn;
	private OwnedMipssEntity ownedMipssEntity;

	public Arbeidsfunksjon() {
	}

	/** {@inheritDoc} */
	public int compareTo(Arbeidsfunksjon o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(getNavn(), o.getNavn()).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof Arbeidsfunksjon)) {
			return false;
		}
		Arbeidsfunksjon other = (Arbeidsfunksjon) o;
		return new EqualsBuilder().append(getNavn(), other.getNavn()).isEquals();
	}

	@Id
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		if (ownedMipssEntity == null) {
			ownedMipssEntity = new OwnedMipssEntity();
		}
		return ownedMipssEntity;
	}

	@Transient
	public String getTextForGUI() {
		return getNavn();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 91).append(getNavn()).toHashCode();
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		this.ownedMipssEntity = ownedMipssEntity;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("navn", getNavn()).toString();
	}
}
