package no.mesta.mipss.persistence.areal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Arealtype
 * @author <a href="mailto:larse@mesan.no">Lars A. Eidsheim</a>
 *
 */
@SuppressWarnings({ "serial", "unchecked" })
@NamedQuery(name = "Arealtype.findAll", query = "select o from Arealtype o")
@Entity
public class Arealtype extends MipssEntityBean implements Serializable, IRenderableMipssEntity {
	
	public static final String FIND_ALL = "Arealtype.findAll";
	private String navn;
	private String fritekst;
	private Boolean valgbarFlagg;
	private String guiFarge;
	private String guiTykkelse;
	
	@Id
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	
	public String getFritekst() {
		return fritekst;
	}
	
	public void setFritekst(String fritekst) {
		String old = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", old, fritekst);
	}
	
	@Column(name = "VALGBAR_FLAGG")
	public Boolean getValgbarFlagg() {
		if(valgbarFlagg == null) {
			valgbarFlagg = false;			
		}
		return valgbarFlagg;
	}
	
	public void setValgbarFlagg(Boolean valgbarFlagg) {
		Boolean old = this.valgbarFlagg;
		this.valgbarFlagg = valgbarFlagg;
		firePropertyChange("valgbarFlagg", old, valgbarFlagg);
	}
	
	@Column(name="GUI_FARGE")
	public String getGuiFarge() {
		return guiFarge;
	}
	
	public void setGuiFarge(String guiFarge) {
		String old = this.guiFarge;
		this.guiFarge = guiFarge;
		firePropertyChange("guiFarge", old, guiFarge);
	}
	
	@Column(name="GUI_TYKKELSE")
	public String getGuiTykkelse() {
		return guiTykkelse;
	}
	
	public void setGuiTykkelse(String guiTykkelse) {
		String old = this.guiTykkelse;
		this.guiTykkelse = guiTykkelse;
		firePropertyChange("guiTykkelse", old, guiTykkelse);
	}
	
    /**
     * Sammenlikner Id-n�kkelen med o's Id-n�kkel.
     * @param o
     * @return true hvis n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Arealtype)) {
            return false;
        }
        Arealtype other = (Arealtype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
    }
	
    /**
     * Genererer en hashkode basert p� Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,13).
            append(getNavn()).
            toHashCode();
    }
    
    public String getTextForGUI() {
		return navn;
	}
}