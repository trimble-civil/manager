package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * PK klasse for veiref
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PunktveirefPK implements Serializable {
    private String stedfestGuid;
    private Date fraDato;

    /**
     * Konstruktør
     * 
     */
    public PunktveirefPK() {
    }

    /**
     * Konstruktør
     * 
     * @param fraDato
     * @param stedfestGuid
     */
    public PunktveirefPK(Date fraDato, String stedfestGuid) {
        this.fraDato = fraDato;
        this.stedfestGuid = stedfestGuid;
    }

    /**
     * Stedfest guid
     * 
     * @param stedfestGuid
     */
    public void setStedfestGuid(String stedfestGuid) {
        this.stedfestGuid = stedfestGuid;
    }

    /**
     * Stedfest guid
     * 
     * @return
     */
    public String getStedfestGuid() {
        return stedfestGuid;
    }

    /**
     * Fradato
     * 
     * @param fraDato
     */
    public void setFraDato(Date fraDato) {
        this.fraDato = fraDato;
    }

    /**
     * Fradato
     * 
     * @return
     */
    public Date getFraDato() {
        return fraDato;
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("stedfestGuid", stedfestGuid).
            append("fraDato", fraDato).
            toString();
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof PunktveirefPK)) {return false;}
        
        PunktveirefPK other = (PunktveirefPK) o;
        return new EqualsBuilder().append(stedfestGuid, other.getStedfestGuid()).
            append(fraDato, other.getFraDato()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2051,2049).append(stedfestGuid).append(fraDato).toHashCode();
    }
}
