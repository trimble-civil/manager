package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>
 * ProdstrekningSok
 * </p>
 * 
 * <p>
 * Denne er duplikat av {@code Prodstrekning} for felter som {@code
 * MipssFieldBean} søker på. Dette fordi viewet {@code InspeksjonSokView} må
 * kunne knytte seg opp mot den.
 * <p>
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @see no.mesta.mipss.persistence.kvernetf1.Prodstrekning
 * @see no.mesta.mipss.persistence.mipssfield.InspeksjonSokView
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "PRODSTREKNING")
public class ProdstrekningSok implements Serializable {
	private Long dfuId;
	private Double fraKm;
	private Date fraTidspunkt;
	private int fylkesnummer;
	private int hp;
	private Long id;
	private String inspeksjonGuid;
	private InspeksjonSokView inspeksjonSok;
	private int kommunenummer;
	private Double tilKm;
	private Date tilTidspunkt;
	private String veikategori;
	private int veinummer;
	private String veistatus;

	public ProdstrekningSok() {
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof ProdstrekningSok)) {
			return false;
		}

		ProdstrekningSok other = (ProdstrekningSok) o;
		return new EqualsBuilder().append(id, other.getId()).isEquals();
	}

	@Column(name = "DFU_ID", insertable = false, updatable = false)
	public Long getDfuId() {
		return dfuId;
	}

	/**
	 * Fra km
	 * 
	 * @return
	 */
	@Column(name = "FRA_KM", nullable = false)
	public Double getFraKm() {
		return fraKm;
	}

	/**
	 * Starttidspunkt
	 * 
	 * @return
	 */
	@Column(name="FRA_TIDSPUNKT")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}

	/**
	 * Fylke
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getFylkesnummer() {
		return fylkesnummer;
	}

	/**
	 * Hovedparcell
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getHp() {
		return hp;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	/**
	 * Inspeksjon
	 * 
	 * @return
	 */
	@Column(name = "INSPEKSJON_GUID", insertable = false, updatable = false)
	public String getInspeksjonGuid() {
		return inspeksjonGuid;
	}

	/**
	 * Inspeksjon
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID")
	public InspeksjonSokView getInspeksjonSok() {
		return inspeksjonSok;
	}

	/**
	 * Kommune
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getKommunenummer() {
		return kommunenummer;
	}

	/**
	 * Til km
	 * 
	 * @return
	 */
	@Column(name = "TIL_KM", nullable = false)
	public Double getTilKm() {
		return tilKm;
	}

	/**
	 * Sluttidspunkt
	 * 
	 * @return
	 */
	@Column(name="TIL_TIDSPUNKT")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}

	/**
	 * Vei kat
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public String getVeikategori() {
		return veikategori;
	}

	/**
	 * Vei nr
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public int getVeinummer() {
		return veinummer;
	}

	/**
	 * Vei status
	 * 
	 * @return
	 */
	@Column(nullable = false)
	public String getVeistatus() {
		return veistatus;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).toHashCode();
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}

	/**
	 * Fra km
	 * 
	 * @param fraKm
	 */
	public void setFraKm(Double fraKm) {
		this.fraKm = fraKm;
	}

	/**
	 * Starttidspunkt
	 * 
	 * @param fraTidspunkt
	 */
	public void setFraTidspunkt(Date fraTidspunkt) {
		this.fraTidspunkt = fraTidspunkt;
	}

	/**
	 * Fylke
	 * 
	 * @param fylkesnummer
	 */
	public void setFylkesnummer(int fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}

	/**
	 * Hovedparcell
	 * 
	 * @param hp
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}

	/**
	 * Id
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Inspeksjon
	 * 
	 * @param inspeksjonGuid
	 */
	public void setInspeksjonGuid(String inspeksjonGuid) {
		this.inspeksjonGuid = inspeksjonGuid;
	}

	/**
	 * Inspeksjon
	 * 
	 * @param inspeksjonSok
	 */
	public void setInspeksjonSok(InspeksjonSokView inspeksjonSok) {
		this.inspeksjonSok = inspeksjonSok;
	}

	/**
	 * Kommune
	 * 
	 * @param kommunenummer
	 */
	public void setKommunenummer(int kommunenummer) {
		this.kommunenummer = kommunenummer;
	}

	/**
	 * TIl km
	 * 
	 * @param tilKm
	 */
	public void setTilKm(Double tilKm) {
		this.tilKm = tilKm;
	}

	/**
	 * Sluttidspunkt
	 * 
	 * @param tilTidspunkt
	 */
	public void setTilTidspunkt(Date tilTidspunkt) {
		this.tilTidspunkt = tilTidspunkt;
	}

	/**
	 * Vei kat
	 * 
	 * @param veikategori
	 */
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}

	/**
	 * Vei nr
	 * 
	 * @param veinummer
	 */
	public void setVeinummer(int veinummer) {
		this.veinummer = veinummer;
	}

	/**
	 * Vei status
	 * 
	 * @param veistatus
	 */
	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("inspeksjonGuid", inspeksjonGuid).append(
				"fylkesnummer", fylkesnummer).append("kommunenummer", kommunenummer).append("veikategori", veikategori)
				.append("veistatus", veistatus).append("veinummer", veinummer).append("hp", hp).append("fraKm", fraKm)
				.append("tilKm", tilKm).toString();
	}
}
