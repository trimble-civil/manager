package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

@SuppressWarnings("serial")
@SequenceGenerator(name = "dfuvarekatalogIdSeq", sequenceName = "DFUVAREKATALOG_ID_SEQ", allocationSize = 1, initialValue = 1)
@NamedQueries( {
		@NamedQuery(name = Dfuvarekatalog.FIND_ALL, query = "select o from Dfuvarekatalog o where o.valgbar=true"),
		@NamedQuery(name = Dfuvarekatalog.FIND_BY_LEVERANDOR, query = "select o from Dfuvarekatalog o where o.leverandorNavn = :leverandorNavn and o.valgbar=true") })
@Entity
public class Dfuvarekatalog extends MipssEntityBean<Dfuvarekatalog> implements
		Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Dfuvarekatalog.findall";
	/** Parametre: leverandorNavn */
	public static final String FIND_BY_LEVERANDOR = "Dfuvarekatalog.findForLeverandor";
	private Long id;
	private String leverandorNavn;
	private String varenummer;
	private String beskrivelse;
	private String fritekst;
	private Double pris;
	private Long guiRekkefolge;
	private OwnedMipssEntity ownedMipssEntity;
	private Boolean valgbar;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dfuvarekatalogIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	@Column(name = "LEVERANDOR_NAVN")
	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		String old = this.leverandorNavn;
		this.leverandorNavn = leverandorNavn;
		firePropertyChange("leverandorNavn", old, leverandorNavn);
	}

	public String getVarenummer() {
		return varenummer;
	}

	public void setVarenummer(String varenummer) {
		String old = this.varenummer;
		this.varenummer = varenummer;
		firePropertyChange("varenummer", old, varenummer);
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		String old = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", old, fritekst);
	}

	public Double getPris() {
		return pris;
	}

	public void setPris(Double pris) {
		Double old = this.pris;
		this.pris = pris;
		firePropertyChange("pris", old, pris);
	}
	@Column(name="GUI_REKKEFOLGE")
	public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}
	
	public void setGuiRekkefolge(Long guiRekkefolge){
		Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}

	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		if (ownedMipssEntity == null) {
			ownedMipssEntity = new OwnedMipssEntity();
		}
		return ownedMipssEntity;
	}

	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		this.ownedMipssEntity = ownedMipssEntity;
	}

	public String getTextForGUI() {
		return beskrivelse;
	}
	
	/**
	 * Kan fjernes etter at rapport som bruker feltet er rettet
	 * @return
	 */
	@Transient
	public String getNavn() {
		return beskrivelse;
	}

	public void setValgbar(Boolean valgbar) {
		Boolean old = this.valgbar;
		this.valgbar = valgbar;
		firePropertyChange("valgbar", old, valgbar);
	}
	@Column(name="VALGBAR_FLAGG")
	public Boolean getValgbar() {
		return valgbar;
	}
}