package no.mesta.mipss.persistence.veinett;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name="kontraktveinett_import")
@SequenceGenerator(name = KontraktveinettImport.ID_SEQ, sequenceName = "KONTRAKTVEINETT_IMPORT_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries( { 
	@NamedQuery(name = KontraktveinettImport.QUERY_FIND_BY_DRIFTKONTRAKT, query = "SELECT p FROM KontraktveinettImport p where p.kontraktId=:kontraktId order by p.sistOppdatertDato desc")
})

public class KontraktveinettImport implements Serializable{
	public static final String ID_SEQ = "kontraktveinettimportIdSeq";
	public static final String QUERY_FIND_BY_DRIFTKONTRAKT = "KontraktveinettImport.fundByDriftkontrakt";
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = KontraktveinettImport.ID_SEQ)
    @Column(nullable = false)
	private Long id;
	@Column(name="KONTRAKT_ID")
	private Long kontraktId;
	@Column(name="VEINETT_ID")
	private Long veinettId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SIST_OPPDATERT_DATO")
	private Date sistOppdatertDato;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPPRETTET_DATO")
	private Date opprettetDato;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="kontraktveinettImport")
	private Set<KontraktveiImport> kontraktveiimport;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the kontraktId
	 */
	public Long getKontraktId() {
		return kontraktId;
	}
	/**
	 * @param kontraktId the kontraktId to set
	 */
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	/**
	 * @return the veinettId
	 */
	public Long getVeinettId() {
		return veinettId;
	}
	/**
	 * @param veinettId the veinettId to set
	 */
	public void setVeinettId(Long veinettId) {
		this.veinettId = veinettId;
	}
	/**
	 * @return the sistOppdatertDato
	 */
	public Date getSistOppdatertDato() {
		return sistOppdatertDato;
	}
	/**
	 * @param sistOppdatertDato the sistOppdatertDato to set
	 */
	public void setSistOppdatertDato(Date sistOppdatertDato) {
		this.sistOppdatertDato = sistOppdatertDato;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	public void setKontraktveiimport(Set<KontraktveiImport> kontraktveiimport) {
		this.kontraktveiimport = kontraktveiimport;
	}
	public Set<KontraktveiImport> getKontraktveiimport() {
		return kontraktveiimport;
	}
	
}
