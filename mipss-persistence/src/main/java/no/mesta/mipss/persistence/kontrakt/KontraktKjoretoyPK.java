package no.mesta.mipss.persistence.kontrakt;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class KontraktKjoretoyPK {
	private Long kontraktId;
	private Long kjoretoyId;
	
	public KontraktKjoretoyPK() {
	}
	
	public KontraktKjoretoyPK(Long kontraktId, Long kjoretoyId) {
		this.kontraktId = kontraktId;
		this.kjoretoyId = kjoretoyId;
	}
	
	public Long getKontraktId() {
		return kontraktId;
	}
	
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kontraktId).append(kjoretoyId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktKjoretoyPK)) {return false;}
        
        KontraktKjoretoyPK other = (KontraktKjoretoyPK) o;
        
        return new EqualsBuilder().append(kontraktId, other.kontraktId).append(kjoretoyId, other.kjoretoyId).isEquals();
    }
}
