package no.mesta.mipss.persistence.rapportfunksjoner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VeirappObject implements Rapp1Entity{
	protected Long veinummer;
	protected Date fraDato;
	protected Date tilDato;
	protected double km;
	protected long sekund;

	protected List<Veirapp> data;
	protected Map<Long, R12Prodtype> prodtypeMap;
	protected Map<Long, Materiale> materialeMap;

	private static final SimpleDateFormat df =new SimpleDateFormat("dd.MM.yyyy");
	
	public VeirappObject(List<Veirapp> data){
		this.data = data;
		initData();
	}
	
	private void initData() {
		veinummer = data.get(0).getVeinummer();
		for (Veirapp r:data){
			Date dato = r.getDogn();
			if (fraDato==null||dato.before(fraDato)){
				fraDato = dato;
			}
			if (tilDato==null||dato.after(tilDato)){
				tilDato = dato;
			}
			
			
			km += r.getTotalKjortKm()!=null?r.getTotalKjortKm():0;
			sekund += r.getTotalKjortSekund();
			
			handleProdtyper(r.getProdtyper());
			handleMateriale(r.getMaterialer());
		}
	}
	/**
	 * Slår sammen totaler for prodtypene for denne
	 * @param ptList
	 */
	protected void handleProdtyper(List<R12Prodtype> ptList){
		if (prodtypeMap==null){
			prodtypeMap = new HashMap<Long, R12Prodtype>();
		}
		if (ptList!=null&&ptList.size()>0){
			for (R12Prodtype pt:ptList){
				pt.setStroFlagg(Boolean.FALSE);//Veirapporten har ikke mengde pr strømetode..
				R12Prodtype p = prodtypeMap.get(pt.getId());
				if (p==null){
					prodtypeMap.put(pt.getId(), pt.clone());
				}else{
					p.addKjortKm(pt.getKjortKm());
					p.addKjortSekund(pt.getKjortSekund());
					p.addMengdeM2(pt.getMengdeM2());
				}
			}
		}
	}
	
	/**
	 * Slår sammen totaler for prodtypene for denne
	 * @param ptList
	 */
	protected void handleMateriale(List<Materiale> ptList){
		if (materialeMap==null){
			materialeMap = new HashMap<Long, Materiale>();
		}
		if (ptList!=null&&ptList.size()>0){
			for (Materiale mt:ptList){
				Materiale m = materialeMap.get(mt.getId());
				if (m==null){
					materialeMap.put(mt.getId(), mt.clone());
				}else{
					m.addTonn(mt.getTonn());
				}
			}
		}
	}
	
	/**
	 * Legger til alle prodtyper i dennes liste. De prodtypene denne ikke har utført
	 * vil legge seg til med kun header-informasjon. Listen med alle sorterte prodtyper vil bli oppdatert med de
	 * data denne evt har produsert. 
	 * @param alleSortert
	 */
	public void organizeProdtyper(List<R12Prodtype> alleSortert){
		List<R12Prodtype> pList = new ArrayList<R12Prodtype>();
		for (R12Prodtype a:alleSortert){
			R12Prodtype r12Prodtype = prodtypeMap.get(a.getId());
			if (r12Prodtype==null){
				r12Prodtype = createEmpty(a);
			}else{
				a.addKjortKm(r12Prodtype.getKjortKm());
				a.addKjortSekund(r12Prodtype.getKjortSekund());
				a.addMengdeM2(r12Prodtype.getMengdeM2());
			}
			
			pList.add(r12Prodtype);
		}
	}
	/**
	 * Legger til alle materialer i dennes liste. De materialene denne ikke har utført
	 * vil legge seg til med kun header-informasjon. Listen med alle sorterte materialene vil bli oppdatert med de
	 * data denne evt har produsert. 
	 * @param alleSortert
	 */
	public void organizeMateriale(List<Materiale> alleSortert){
		List<Materiale> pList = new ArrayList<Materiale>();
		for (Materiale a:alleSortert){
			Materiale materiale = materialeMap.get(a.getId());
			if (materiale==null){
				materiale = createEmptyMateriale(a);
			}else{
				a.addTonn(materiale.getTonn());
			}
			pList.add(materiale);
		}
	}
	/**
	 * Lager en placeholder prodtype, med kun header-informasjon og uten data
	 * @param pt
	 * @return
	 */
	private R12Prodtype createEmpty(R12Prodtype pt){
		R12Prodtype p = new R12Prodtype();
		p.setHeader(pt.getHeader());
		p.setId(pt.getId());
		p.setSeq(pt.getSeq());
		return p;
	}
	
	/**
	 * Lager en placeholder prodtype, med kun header-informasjon og uten data
	 * @param pt
	 * @return
	 */
	private Materiale createEmptyMateriale(Materiale pt){
		Materiale p = new Materiale();
		p.setHeader(pt.getHeader());
		p.setId(pt.getId());
		p.setSeq(pt.getSeq());
		return p;
	}
	public List<R12Prodtype> getProdtyper() {
		List<R12Prodtype> ptList = new ArrayList<R12Prodtype>();
		Comparator<R12Prodtype> comparator = new Comparator<R12Prodtype>(){
			public int compare(R12Prodtype o1, R12Prodtype o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}
		};
		Set<Long> keySet = prodtypeMap.keySet();
		for (Long l:keySet){
			ptList.add(prodtypeMap.get(l));
		}
		Collections.sort(ptList, comparator);
		return ptList;
	}

	public List<R12Stroprodukt> getStroprodukter() {
		return null;
	}

	public List<Materiale> getMaterialer() {
		List<Materiale> ptList = new ArrayList<Materiale>();
		Comparator<Materiale> comparator = new Comparator<Materiale>(){
			public int compare(Materiale o1, Materiale o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}
		};
		Set<Long> keySet = materialeMap.keySet();
		for (Long l:keySet){
			ptList.add(materialeMap.get(l));
		}
		Collections.sort(ptList, comparator);
		return ptList;
	}
	public Long getVeinummer() {
		return veinummer;
	}

	public double getKm() {
		return km;
	}

	public long getSekund() {
		return sekund;
	}
	public double getTid(){
		return (double)sekund/(double)86400;
	}
	public String getPeriode(){
		StringBuilder sb = new StringBuilder();
		sb.append(df.format(fraDato));
		sb.append("-");
		sb.append(df.format(tilDato));
		return sb.toString();
	}

	public Boolean getPaaKontrakt() {
		if (data!=null&&data.get(0)!=null)
			return data.get(0).getPaaKontraktFlagg().intValue()==1;
		return Boolean.FALSE;
	}
}
