package no.mesta.mipss.persistence.driftslogg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.mipssfield.Avvik;
@NamedQueries({
    @NamedQuery(name = Periode.FIND_PERIODE_BY_DATE_LEVKONTRAKT, query = "select o from Periode o where o.levkontraktIdent=:levkontraktIdent and o.fraDato=:fraDato and o.tilDato=:tilDato"),
})
@SequenceGenerator(name = Periode.ID_SEQ, sequenceName = "PERIODE_ID_SEQ", initialValue = 1, allocationSize = 1)
@SuppressWarnings("serial")
@Entity
@Table(name="PERIODE")
public class Periode extends MipssEntityBean<Periode> implements IRenderableMipssEntity{
	public static final String FIND_PERIODE_BY_DATE_LEVKONTRAKT = "Periode.findPeriodeByDateLevkontrakt";

	public static final String ID_SEQ = "periodeIdSeq";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Periode.ID_SEQ)
	private Long id;
	@Column(name="LEVKONTRAKT_IDENT", nullable = true, insertable = false, updatable = false)
	private String levkontraktIdent;
	
	@ManyToOne
	@JoinColumn(name = "LEVKONTRAKT_IDENT", referencedColumnName = "LEVKONTRAKT_IDENT")
	private Levkontrakt levkontrakt;
	
	
	@Column(name="FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fraDato;
	@Column(name="TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tilDato;
	
	@OneToMany(mappedBy="periode", cascade = { CascadeType.ALL }, orphanRemoval=true, fetch = FetchType.EAGER)
	private List<PeriodeStatus> statuser = new ArrayList<>();

	@OneToMany(mappedBy="period", fetch = FetchType.EAGER)
	private List<Trip> trips;

	@Transient
	private PeriodeStatus status;

	public Periode() {
		trips = new ArrayList<>();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the levkontraktIdent
	 */
	public String getLevkontraktIdent() {
		return levkontraktIdent;
	}
	
	public Levkontrakt getLevkontrakt(){
		return levkontrakt;
	}

	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}

	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}

	/**
	 * @return the aktivteter
	 */
	public List<Aktivitet> getAktiviteter() {
		List<Aktivitet> activities = new ArrayList<>();
		for(Trip trip : trips) {
			activities.addAll(trip.getActivities());
		}
		return activities;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	/**
	 * @param levkontraktIdent the levkontraktIdent to set
	 */
	public void setLevkontraktIdent(String levkontraktIdent) {
		String old = this.levkontraktIdent;
		this.levkontraktIdent = levkontraktIdent;
		firePropertyChange("levkontraktIdent", old, levkontraktIdent);
	}
	
	public void setLevkontrakt(Levkontrakt levkontrakt){
		Levkontrakt old = this.levkontrakt;
		this.levkontrakt = levkontrakt;
		firePropertyChange("levkontrakt", old, levkontrakt);
		if (levkontrakt==null)
			setLevkontraktIdent(null);
		else
			setLevkontraktIdent(levkontrakt.getLevkontraktIdent());
	}
	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		Date old = this.fraDato;
		this.fraDato = fraDato;
		firePropertyChange("fraDato", old, fraDato);
	}

	/**
	 * @param tilDato the tilDato to set
	 */
	public void setTilDato(Date tilDato) {
		Date old = this.tilDato;
		this.tilDato = tilDato;
		firePropertyChange("tilDato", old, tilDato);
	}

	public PeriodeStatus getStatus(){
		if (getStatuser() == null || getStatuser().size() == 0) {
			return null;
		}

		List<PeriodeStatus> stats = new ArrayList<PeriodeStatus>(getStatuser());
		Collections.sort(stats);
		return stats.get(0);
	}
	
	public void setStatuser(List<PeriodeStatus> statuser) {
		this.statuser = statuser;
	}

	public List<PeriodeStatus> getStatuser() {
		return statuser;
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}
}
