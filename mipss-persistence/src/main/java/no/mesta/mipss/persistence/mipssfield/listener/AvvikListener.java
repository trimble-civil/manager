/**
 * 
 */
package no.mesta.mipss.persistence.mipssfield.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

/**
 * Lytter på lagring av punktreg.
 * 
 * Hvis opprettetDato = null, så settes den.
 * 
 * Hvis opprettetAv = null, så settes den til Ikke satt
 * 
 * Hvis PunktregStatus.status = PunktregstatusType.LUKKET_STATUS, så
 * settes lukketDato
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class AvvikListener<E extends Avvik> {
	private static final Logger logger = LoggerFactory.getLogger(AvvikListener.class);

	/**
	 * Kontrollerer lagringen og oppdatering av Punktreg
	 * 
	 * @param ent
	 */
	@PrePersist
	@PreUpdate
	public void monitorFunn(E ent) {
		if(logger.isDebugEnabled()) {
			logger.debug("monitorOpprett(" + ent + ")");
		}
		if(ent.getOpprettetDato() == null) {
			ent.setOpprettetDato(Clock.now());
		}
		
		if(ent.getOpprettetAv() == null) {
			ent.setOpprettetAv("Ikke satt");
		}
		
		AvvikStatus status = ent.getStatus();
		if(logger.isDebugEnabled()) {
			logger.debug("Status for " + ent + ": " + status);
		}
		if(status != null && StringUtils.equalsIgnoreCase(status.getAvvikstatusType().getStatus(), AvvikstatusType.LUKKET_STATUS)) {
			if(ent.getLukketDato() == null) {
				ent.setLukketDato(status.getOpprettetDato());
			}
		}
	}
}
