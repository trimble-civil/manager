package no.mesta.mipss.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Version;

/**
 * Standardized version column implementation used for optimistic locking of entities. The
 * entity table in the database must contain a version column in order for this construct to
 * work.
 * 
 * @author Christian Wiik (Mesan)
 */
@SuppressWarnings("serial")
@Embeddable
public class VersionColumn implements Serializable {
	private Integer version;

	@Version
	@Column(name = "VERSJON")
	public Integer getVersion() {
		return version;
	}
	
	public void setVersion(Integer version) {
		this.version = version;
	}

}
