package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Materiale implements Serializable, Cloneable {

	private Long id;
	private String header;
	private Long seq;
	private Double tonn;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public Double getTonn() {
		return tonn;
	}
	public void setTonn(Double tonn) {
		this.tonn = tonn;
	}
	public void addTonn(Double tonn){
		if (this.tonn==null){
			this.tonn = tonn;
		}else{
			if (tonn!=null){
				this.tonn+=tonn;
			}
		}
	}
	@Override
	public Materiale clone(){
		Materiale mat = new Materiale();
		mat.setId(id);
		mat.setSeq(seq);
		mat.setHeader(header);
		mat.setTonn(tonn);
		return mat;
	}
}
