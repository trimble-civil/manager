package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

public class Passering implements Serializable{
	
	private Long kjoretoyId;
	private String kjoretoyNavn;
	private Long leverandorNr;
	private String leverandorNavn;
	private Boolean ansvarligKontraktFlagg;
	private String retning;
	private Date tidspunkt;
	private Long prodtypeId;
	private String prodtypeNavn;
	private Long stroproduktId;
	private String stroproduktNavn;	
		
	public static String[] getPropertiesArray() {
		return new String[] { "kjoretoyId", "kjoretoyNavn", "leverandorNr", "leverandorNavn", "ansvarligKontraktFlagg", "retning", "tidspunkt", 
							  "prodtypeId", "prodtypeNavn", "stroproduktId", "stroproduktNavn"};
	}

	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public String getKjoretoyNavn() {
		return kjoretoyNavn;
	}

	public void setKjoretoyNavn(String kjoretoyNavn) {
		this.kjoretoyNavn = kjoretoyNavn;
	}

	public Long getLeverandorNr() {
		return leverandorNr;
	}

	public void setLeverandorNr(Long leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		this.leverandorNavn = leverandorNavn;
	}

	public Boolean getAnsvarligKontraktFlagg() {
		return ansvarligKontraktFlagg;
	}

	public void setAnsvarligKontraktFlagg(Boolean ansvarligKontraktFlagg) {
		this.ansvarligKontraktFlagg = ansvarligKontraktFlagg;
	}
	
	public String ansvarligKontraktFlaggString(){
		if (getAnsvarligKontraktFlagg()) {
			return "X";
		} else{
			return "";
		}
	}

	public String getRetning() {
		return retning;
	}

	public void setRetning(String retning) {
		this.retning = retning;
	}

	public Date getTidspunkt() {
		return tidspunkt;
	}

	public void setTidspunkt(Date tidspunkt) {
		this.tidspunkt = tidspunkt;
	}

	public Long getProdtypeId() {
		return prodtypeId;
	}

	public void setProdtypeId(Long prodtypeId) {
		this.prodtypeId = prodtypeId;
	}

	public String getProdtypeNavn() {
		return prodtypeNavn;
	}

	public void setProdtypeNavn(String prodtypeNavn) {
		this.prodtypeNavn = prodtypeNavn;
	}

	public Long getStroproduktId() {
		return stroproduktId;
	}

	public void setStroproduktId(Long stroproduktId) {
		this.stroproduktId = stroproduktId;
	}

	public String getStroproduktNavn() {
		return stroproduktNavn;
	}

	public void setStroproduktNavn(String stroproduktNavn) {
		this.stroproduktNavn = stroproduktNavn;
	}
}
