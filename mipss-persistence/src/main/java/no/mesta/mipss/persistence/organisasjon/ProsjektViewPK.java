package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;

public class ProsjektViewPK implements Serializable{
	private String prosjektnr;
	private String selskapskode;
	
	public ProsjektViewPK(){
		
	}
	
	public ProsjektViewPK(String prosjektnr, String selskapskode){
		this.prosjektnr = prosjektnr;
		this.selskapskode = selskapskode;
	}
	public String getProsjektnr() {
		return prosjektnr;
	}
	public String getSelskapskode() {
		return selskapskode;
	}
	public void setProsjektnr(String prosjektnr) {
		this.prosjektnr = prosjektnr;
	}
	public void setSelskapskode(String selskapskode) {
		this.selskapskode = selskapskode;
	}
	
	
}
