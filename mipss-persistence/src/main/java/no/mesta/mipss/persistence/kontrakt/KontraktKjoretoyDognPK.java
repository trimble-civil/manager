package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
public class KontraktKjoretoyDognPK implements Serializable {
	private Long kontraktId;
	private Long kjoretoyId;
	private Date dogn;
	
	public KontraktKjoretoyDognPK() {
	}
	
	public KontraktKjoretoyDognPK(Long kontraktId, Long kjoretoyId, Date dogn) {
		super();
		this.kontraktId = kontraktId;
		this.kjoretoyId = kjoretoyId;
		this.dogn = dogn;
	}

	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public Date getDogn() {
		return dogn;
	}

	public void setDogn(Date dogn) {
		this.dogn = dogn;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kontraktId).append(kjoretoyId).append(dogn).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktKjoretoyDognPK)) {return false;}
        
        KontraktKjoretoyDognPK other = (KontraktKjoretoyDognPK) o;
        
        return new EqualsBuilder().append(kontraktId, other.kontraktId).append(kjoretoyId, other.kjoretoyId).append(dogn, other.dogn).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("kontraktId", kontraktId).
            append("kjoretoyId", kjoretoyId).
            append("dogn", dogn).
            toString();
    }
}
