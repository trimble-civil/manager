package no.mesta.mipss.persistence.kontrakt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.MipssEntityBean;
@SuppressWarnings("serial")

@Entity(name=KontraktKjoretoyGruppering.BEAN_NAME)
@NamedQueries({
	@NamedQuery(
        name = KontraktKjoretoyGruppering.FIND_BY_KONTRAKT, 
        query = "select o from KontraktKjoretoyGruppering o where o.kontraktId = :id"),
    @NamedQuery(
            name = KontraktKjoretoyGruppering.FIND_BY_KONTRAKT_KJORETOY, 
            query = "select o from KontraktKjoretoyGruppering o where o.kontraktId = :id and o.kjoretoyId=:kjoretoyId"),
    @NamedQuery(
    	name = KontraktKjoretoyGruppering.FIND_BY_GRUPPE, 
        query = "select o from KontraktKjoretoyGruppering o where o.kjoretoygruppeId = :id"),
    @NamedQuery(
        name = KontraktKjoretoyGruppering.FIND_KJORETOYNAVN_I_GRUPPE, 
        query = "select o.kjoretoy.kjoretoy.eksterntNavn from KontraktKjoretoyGruppering o where o.kjoretoygruppeId=:gruppeId")
})
@Table(name="KONTRAKT_KJT_GRUPPERING")
@IdClass(KontraktKjoretoyGrupperingPK.class)
public class KontraktKjoretoyGruppering extends MipssEntityBean<KontraktKjoretoyGruppering>{
	public static final String BEAN_NAME = "KontraktKjoretoyGruppering";
	public static final String FIND_BY_KONTRAKT = "KontraktKjoretoyGruppering.findByKontrakt";
	public static final String FIND_BY_KONTRAKT_KJORETOY = "KontraktKjoretoyGruppering.findByKontraktKjoretoy";
	public static final String FIND_BY_GRUPPE = "KontraktKjoretoyGruppering.findByGruppe";
	public static final String FIND_KJORETOYNAVN_I_GRUPPE = "KontraktKjoretoyGruppering.findKjoretoynavnIGruppe";
	
	private Kjoretoygruppe kjoretoygruppe;
	private KontraktKjoretoy kjoretoy;
	private String opprettetAv;
	private Date opprettetDato;
	
	private Long kjoretoygruppeId;
	private Long kontraktId;
	private Long kjoretoyId;
	
	
	
	@Id
	@Column(name = "GRUPPE_ID", nullable = false, insertable = false, updatable = false)
	public Long getKjoretoygruppeId(){
		return kjoretoygruppeId;
	}
	public void setKjoretoygruppeId(Long kjoretoygruppeId){
		this.kjoretoygruppeId = kjoretoygruppeId;
	}
	@Id
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getKontraktId(){
		return kontraktId;
	}
	public void setKontraktId(Long kontraktId){
		this.kontraktId = kontraktId;
	}
	@Id
	@Column(name = "KJORETOY_ID", nullable = false, insertable = false, updatable = false)
	public Long getKjoretoyId(){
		return kjoretoyId;
	}
	public void setKjoretoyId(Long kjoretoyId){
		this.kjoretoyId = kjoretoyId;
	}
	
	@ManyToOne
	@JoinColumn(name = "GRUPPE_ID", referencedColumnName = "ID", nullable = false)
	public Kjoretoygruppe getKjoretoygruppe(){
		return kjoretoygruppe;
	}
	
	public void setKjoretoygruppe(Kjoretoygruppe kjoretoygruppe){
		Kjoretoygruppe old = this.kjoretoygruppe;
		this.kjoretoygruppe = kjoretoygruppe;
		getProps().firePropertyChange("kjoretoygruppe", old, kjoretoygruppe);
		if (kjoretoygruppe!=null){
			setKjoretoygruppeId(kjoretoygruppe.getId());
		}else{
			setKjoretoygruppeId(null);
		}
	}
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "KJORETOY_ID", nullable = false),
		@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "KONTRAKT_ID", nullable = false)
	})
	public KontraktKjoretoy getKjoretoy(){
		return kjoretoy;
	}
	
	
	public void setKjoretoy(KontraktKjoretoy kjoretoy){
		KontraktKjoretoy old = this.kjoretoy;
		this.kjoretoy = kjoretoy;
		getProps().firePropertyChange("kjoretoy", old, kjoretoy);
		if (kjoretoy!=null){
			setKjoretoyId(kjoretoy.getKjoretoyId());
			setKontraktId(kjoretoy.getKontraktId());
		}else{
			setKjoretoyId(null);
			setKontraktId(null);
		}
	}
	
	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv(){
		return opprettetAv;
	}
	
	public void setOpprettetAv(String opprettetAv){
		String old = this.opprettetAv;
		this.opprettetAv=opprettetAv;
		getProps().firePropertyChange("opprettetAv", old, opprettetAv);
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OPPRETTET_DATO")
	public Date getOpprettetDato(){
		return this.opprettetDato;
	}
	
	
	public void setOpprettetDato(Date opprettetDato){
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		getProps().firePropertyChange("opprettetDato", old, opprettetDato);
	}
	
	
	
	@Transient
	public String getKjoretoynavn(){
		if (kjoretoy!=null&&kjoretoy.getKjoretoy()!=null)
			return kjoretoy.getKjoretoy().getEksterntNavn();
		return null;
	}
	@Transient
	public String getType(){
		if (kjoretoy!=null&&kjoretoy.getKjoretoy()!=null){
			if (kjoretoy.getKjoretoy().getKjoretoytype()!=null)
				return kjoretoy.getKjoretoy().getKjoretoytype().getNavn();
		}
		return null;
	}
	@Transient
	public String getMerke(){
		if (kjoretoy!=null&&kjoretoy.getKjoretoy()!=null){
			if (kjoretoy.getKjoretoy().getKjoretoymodell()!=null)
				return kjoretoy.getKjoretoy().getKjoretoymodell().getMerke();
		}
		return null;
	}
	@Transient
	public String getLeverandor(){
		if (kjoretoy!=null&&kjoretoy.getKjoretoy()!=null){
			if (kjoretoy.getKjoretoy().getLeverandor()!=null)
				return kjoretoy.getKjoretoy().getLeverandor().getNavn();
		}
		return null;
	}
	@Transient
	public String getRegnr(){
		if (kjoretoy!=null&&kjoretoy.getKjoretoy()!=null){
			return kjoretoy.getKjoretoy().getRegnr();
		}
		return null;
	}
}
