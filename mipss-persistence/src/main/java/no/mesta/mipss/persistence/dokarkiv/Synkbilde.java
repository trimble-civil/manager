package no.mesta.mipss.persistence.dokarkiv;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
public class Synkbilde extends MipssEntityBean<Synkbilde> implements Serializable{
	
	private Integer bredde;
	@Column(nullable = false, length = 255)
	private String filnavn;
	@Id
	@Column(nullable = false)
	private String guid;
	private Integer hoyde;
	@Lob
	@Column(name="BILDE_LOB")
	private byte[] bildeLob;
	@Lob
	@Column(name = "SMAABILDE_LOB")
	private byte[] smaabildeLob;
	@Column(name = "BESKRIVELSE", length = 255)
	private String beskrivelse;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	@Column (name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name="ENDRET_AV")
	private String endretAv;
	@Column(name="ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;
	/**
	 * @return the bredde
	 */
	public Integer getBredde() {
		return bredde;
	}
	/**
	 * @param bredde the bredde to set
	 */
	public void setBredde(Integer bredde) {
		this.bredde = bredde;
	}
	/**
	 * @return the filnavn
	 */
	public String getFilnavn() {
		return filnavn;
	}
	/**
	 * @param filnavn the filnavn to set
	 */
	public void setFilnavn(String filnavn) {
		this.filnavn = filnavn;
	}
	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}
	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}
	/**
	 * @return the hoyde
	 */
	public Integer getHoyde() {
		return hoyde;
	}
	/**
	 * @param hoyde the hoyde to set
	 */
	public void setHoyde(Integer hoyde) {
		this.hoyde = hoyde;
	}
	/**
	 * @return the bildeLob
	 */
	public byte[] getBildeLob() {
		return bildeLob;
	}
	/**
	 * @param bildeLob the bildeLob to set
	 */
	public void setBildeLob(byte[] bildeLob) {
		this.bildeLob = bildeLob;
	}
	/**
	 * @return the smaabildeLob
	 */
	public byte[] getSmaabildeLob() {
		return smaabildeLob;
	}
	/**
	 * @param smaabildeLob the smaabildeLob to set
	 */
	public void setSmaabildeLob(byte[] smaabildeLob) {
		this.smaabildeLob = smaabildeLob;
	}
	/**
	 * @return the beskrivelse
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}
	/**
	 * @param beskrivelse the beskrivelse to set
	 */
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}
	public Date getEndretDato() {
		return endretDato;
	}
	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}
	public String getEndretAv() {
		return endretAv;
	}
	
	
}
