package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Dokument;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "utstyrgruppeIdSeq", sequenceName = "UTSTYRGRUPPE_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = Utstyrgruppe.FIND_ALL, query = "select o from Utstyrgruppe o order by o.guiRekkefolge"),
    @NamedQuery(name = Utstyrgruppe.FIND_VALGBAR, query = "select o from Utstyrgruppe o where o.valgbarFlagg=true order by o.guiRekkefolge")
})

@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)

public class Utstyrgruppe extends MipssEntityBean<Utstyrgruppe> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity, Comparable<Utstyrgruppe> {
    public static final String FIND_ALL = "Utstyrgruppe.findAll";
    public static final String FIND_VALGBAR = "Utstyrgruppe.findValgbar";
    private Long serieportBitNr;
    private String fritekst;
    private Long guiRekkefolge;
    private Long id;
    private Boolean kanInstrFlagg;
    private Boolean kreverSubgrFlagg;
    private Boolean serieportFlagg;
    private String navn;
    private Boolean valgbarFlagg;
    private List<Utstyrsubgruppe> utstyrsubgruppeList = new ArrayList<Utstyrsubgruppe>();
    private Prodtype prodtype;
    private List<KjoretoyUtstyr> kjoretoyUtstyrList = new ArrayList<KjoretoyUtstyr>();
        
    // felt lagt til for hånd
    private OwnedMipssEntity ownedMipssEntity;
    private List<Dokument> dokumentList = new ArrayList<Dokument>();
    private List<Bilde> bildeList = new ArrayList<Bilde>();

    public Utstyrgruppe() {
    }

    @Column(name="SERIEPORT_BIT_NR")
    public Long getSerieportBitNr() {
        return serieportBitNr;
    }

    public void setSerieportBitNr(Long serieportBitNr) {
    	Long old = this.serieportBitNr;
        this.serieportBitNr = serieportBitNr;
        firePropertyChange("serieportBitNr", old, serieportBitNr);
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Column(name="GUI_REKKEFOLGE", nullable = false)
    public Long getGuiRekkefolge() {
        return guiRekkefolge;
    }

    public void setGuiRekkefolge(Long guiRekkefolge) {
    	Long old = this.guiRekkefolge;
        this.guiRekkefolge = guiRekkefolge;
        firePropertyChange("guiRekkefolge", old, guiRekkefolge);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utstyrgruppeIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(name="KAN_INSTR_FLAGG", nullable = false)
    public Boolean getKanInstrFlagg() {
        return kanInstrFlagg;
    }

    public void setKanInstrFlagg(Boolean kanInstrFlagg) {
    	Boolean old = this.kanInstrFlagg;
        this.kanInstrFlagg = kanInstrFlagg;
        firePropertyChange("kanInstrFlagg", old, kanInstrFlagg);
    }

    @Column(name="KREVER_SUBGR_FLAGG", nullable = false)
    public Boolean getKreverSubgrFlagg() {
        return kreverSubgrFlagg;
    }

    public void setKreverSubgrFlagg(Boolean kreverSubgrFlagg) {
    	Boolean old = this.kreverSubgrFlagg;
        this.kreverSubgrFlagg = kreverSubgrFlagg;
        firePropertyChange("kreverSubgrFlagg", old, kreverSubgrFlagg);
    }
    
    @Column(name="SERIEPORT_FLAGG", nullable = false)
    public Boolean getSerieportFlagg() {
        return serieportFlagg;
    }

    public void setSerieportFlagg(Boolean serieportFlagg) {
    	Boolean old = this.serieportFlagg;
        this.serieportFlagg = serieportFlagg;
        firePropertyChange("serieportFlagg", old, serieportFlagg);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
    	this.valgbarFlagg = valgbarFlagg;
    	firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }

    @OneToMany(mappedBy = "utstyrgruppe", fetch = FetchType.EAGER)
    @OrderBy("guiRekkefolge")
    public List<Utstyrsubgruppe> getUtstyrsubgruppeList() {
        return utstyrsubgruppeList;
    }

    public void setUtstyrsubgruppeList(List<Utstyrsubgruppe> utstyrsubgruppeList) {
        this.utstyrsubgruppeList = utstyrsubgruppeList;
    }

    public Utstyrsubgruppe addUtstyrsubgruppe(Utstyrsubgruppe utstyrsubgruppe) {
        getUtstyrsubgruppeList().add(utstyrsubgruppe);
        utstyrsubgruppe.setUtstyrgruppe(this);
        return utstyrsubgruppe;
    }

    public Utstyrsubgruppe removeUtstyrsubgruppe(Utstyrsubgruppe utstyrsubgruppe) {
        getUtstyrsubgruppeList().remove(utstyrsubgruppe);
        utstyrsubgruppe.setUtstyrgruppe(null);
        return utstyrsubgruppe;
    }

    @ManyToOne
    @JoinColumn(name = "PRODTYPE_ID", referencedColumnName = "ID")
    public Prodtype getProdtype() {
        return prodtype;
    }

    public void setProdtype(Prodtype prodtype) {
    	Prodtype old = this.prodtype;
        this.prodtype = prodtype;
        firePropertyChange("prodtype", old, prodtype);
    }

    @OneToMany(mappedBy = "utstyrgruppe")
    public List<KjoretoyUtstyr> getKjoretoyUtstyrList() {
        return kjoretoyUtstyrList;
    }

    public void setKjoretoyUtstyrList(List<KjoretoyUtstyr> kjoretoyUtstyrList) {
        this.kjoretoyUtstyrList = kjoretoyUtstyrList;
    }

    public KjoretoyUtstyr addKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
        getKjoretoyUtstyrList().add(kjoretoyUtstyr);
        kjoretoyUtstyr.setUtstyrgruppe(this);
        return kjoretoyUtstyr;
    }

    public KjoretoyUtstyr removeKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
        getKjoretoyUtstyrList().remove(kjoretoyUtstyr);
        kjoretoyUtstyr.setUtstyrgruppe(null);
        return kjoretoyUtstyr;
    }

    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("navn", getNavn()).
            append("bit_nr", getSerieportBitNr()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Utstyrgruppe)) {
            return false;
        }
        Utstyrgruppe other = (Utstyrgruppe) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Utstyrgruppe o) {
        Utstyrgruppe other = (Utstyrgruppe) o;
        int comp = new CompareToBuilder().
            append(getGuiRekkefolge(), other.getGuiRekkefolge()).
            toComparison();
        
        if(comp == 0) {
        	comp = new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
        }
        
        return comp;
    }

    public List<Dokument> getDokumentList() {
        return dokumentList;
    }

    /**
     * setter dokumentlisten i mange-til-mange forholdet.
     * @param dokumentList
     */
    public void setDokumentList(List<Dokument> dokumentList) {
        this.dokumentList = dokumentList;
    } 
    
    public List<Bilde> getBildeList() {
        return bildeList;
    }

    /**
     * setter bildelisten i fra et mange-til-mange forhold.
     * @param bildeList
     */
    public void setBildeList(List<Bilde> bildeList) {
        this.bildeList = bildeList;
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
}
