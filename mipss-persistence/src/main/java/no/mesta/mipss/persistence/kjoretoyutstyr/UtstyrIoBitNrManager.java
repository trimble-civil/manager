package no.mesta.mipss.persistence.kjoretoyutstyr;

import javax.persistence.PrePersist;

import javax.persistence.PreUpdate;

import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;

/**
 * Kontrollerer at {@code KjoretoyUtstyrIo} sin BitNr settes korrekt ifølge datamodellens regler.
 * @author jorge
 */
public class UtstyrIoBitNrManager {
    public UtstyrIoBitNrManager() {
    }
    
    /**
     * JPA entity listener som henter BitNt først fra IoListe. Men hvis denne verdien er null, hentes den 
     * fra kjoretyyUtstyr sin Utstyrgruppe. Hvis enten gruppen eller gruppens SerieportBitNr er null,  
     * kommer vi i en ulovlig tilstand ifølge modellen.
     * 
     * Overskriver eksisterende innhold i BitNr.
     * 
     * @param ent
     * @throws IllegalStateException hvis de ovenfor oppførte reglene ikke kan følges.
     */
    @PrePersist
    @PreUpdate
    public void manageBitNr(KjoretoyUtstyrIo ent) {
        // prøv å hente bitnr fra iolisten først
        Ioliste io = ent.getIoliste();
        if (io == null) {
            throw new IllegalStateException("KjoretoyUtstyrIo mangler obligatorisk Ioliste");
        }
        if (io.getBitNr() != null) {
            ent.setBitNr(io.getBitNr());
            return;
        }
        
        // ellers hente den fra Utstyrgruppen, hvis den eksisterer.
        KjoretoyUtstyr ku = ent.getKjoretoyUtstyr();
        if(ku == null) {
            throw new IllegalStateException("KjoretoyUtstyrIo mangler obligatorisk KjoretoyUtstyr");
        }
        Utstyrgruppe gruppe = ku.getUtstyrgruppe();
        if(gruppe != null && gruppe.getSerieportBitNr() != null) {
            ent.setBitNr(gruppe.getSerieportBitNr());
            return;
        }
        
        // vi er kommet i en ulovlig tilstand
        throw new IllegalStateException("KjoretoyUtstyrIo finner hverken BitNr i Ioliste eller SerieportBitNr i Utstyrgruppen");
    }
}
