package no.mesta.mipss.persistence.driftslogg;

public class GritAmount {

    private Long measureId;
    private Double gritAmount;

    public static String[] getPropertiesArray() {
        return new String[] {
                "measureId",
                "gritAmount"
        };
    }

    public Long getMeasureId() {
        return measureId;
    }

    public void setMeasureId(Long measureId) {
        this.measureId = measureId;
    }

    public Double getGritAmount() {
        return gritAmount;
    }

    public void setGritAmount(Double gritAmount) {
        this.gritAmount = gritAmount;
    }

}