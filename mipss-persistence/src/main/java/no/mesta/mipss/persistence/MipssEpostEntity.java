package no.mesta.mipss.persistence;

/**
 * Interface for entiteter som inneholder epost informasjon
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public interface MipssEpostEntity extends IRenderableMipssEntity{
	String getNavn();
	String getEpostAdresse();
	String getKopiEpostAdresse();
	Long getSendeEpostFlagg();
	Boolean getSendeEpost();
	void setSendeEpost(Boolean sendeEpost);
	String getType();
	Boolean getEkstraDagsrapport();
	void setEkstraDagsrapport(Boolean ekstraDagsrapport);
	OwnedMipssEntity getOwnedMipssEntity();
	Integer getNumber();
}
