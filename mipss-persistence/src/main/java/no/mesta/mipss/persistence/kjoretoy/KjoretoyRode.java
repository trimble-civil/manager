package no.mesta.mipss.persistence.kjoretoy;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetilknytningstype;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
        @NamedQuery(name = KjoretoyRode.FIND_ALL, query = "select o from KjoretoyRode o"),
        @NamedQuery(name = KjoretoyRode.FIND_BY_KJORETOY, query = "select o from KjoretoyRode o where o.kjoretoyId = :id"),
        @NamedQuery(
                name = KjoretoyRode.FIND_BY_KONTRAKT_AND_RODETILKNYTNINGSTYPE,
                query = "select o from KjoretoyRode o where o.kontraktId = :kontraktId and o.rodetilknytningstypeId = :rodetilknytningstypeId"
        ),
        @NamedQuery(
                name = KjoretoyRode.FIND_BY_KONTRAKT_AND_KJORETOY_AND_RODETILKNYTNINGSTYPE,
                query = "select o from KjoretoyRode o where o.kontraktId = :kontraktId and o.kjoretoyId = :kjoretoyId and " +
                        "o.rodetilknytningstypeId = :rodetilknytningstypeId"
        )
})
@Table(name = "KJORETOY_RODE")
@IdClass(KjoretoyRodePK.class)
public class KjoretoyRode extends MipssEntityBean<KjoretoyRode> {
    public static final String FIND_ALL = "KjoretoyRode.findAll";
    public static final String FIND_BY_KJORETOY = "KjoretoyRode.findByKjoretoy";
    public static final String FIND_BY_KONTRAKT_AND_RODETILKNYTNINGSTYPE = "KjoretoyRode.findByKontraktAndRodetilknytningstype";
    public static final String FIND_BY_KONTRAKT_AND_KJORETOY_AND_RODETILKNYTNINGSTYPE = "KjoretoyRode.findByKontraktAndKjoretoyAndRodetilknytningstype";

    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID", nullable = false)
    private Kjoretoy kjoretoy;

    @Id
    @Column(name = "KJORETOY_ID", nullable = false, insertable = false, updatable = false)
    private Long kjoretoyId;
    @Id
    @Column(name = "RODE_ID", nullable = false, insertable = false, updatable = false)
    private Long rodeId;
    @Column(name = "RODETILKNYTNINGSTYPE_ID", nullable = false, insertable = false, updatable = false)
    private Long rodetilknytningstypeId;
    @Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
    private Long kontraktId;
    @Id
    @Column(name = "FRA_TIDSPUNKT", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fraTidspunkt;
    @Column(name = "TIL_TIDSPUNKT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tilTidspunkt;
    @Column(name = "OPPRETTET_AV")
    private String opprettetAv;
    @Column(name = "OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date opprettetDato;
    @Column(name = "ENDRET_AV")
    private String endretAv;
    @Column(name = "ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endretDato;

    @ManyToOne
    @JoinColumn(name = "RODE_ID", referencedColumnName = "ID", nullable = false)
    private Rode rode;
    @ManyToOne
    @JoinColumn(name = "RODETILKNYTNINGSTYPE_ID", referencedColumnName = "ID", nullable = false)
    private Rodetilknytningstype rodetilknytningstype;
    @ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
    private Driftkontrakt kontrakt;

    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
        //this.kjoretoy = kjoretoy;
        Kjoretoy old = this.kjoretoy;
        this.kjoretoy = kjoretoy;

        if(kjoretoy != null) {
            setKjoretoyId(kjoretoy.getId());
        }
        else {
            setKjoretoyId(null);
        }

        firePropertyChange("kjoretoy",old,kjoretoy);

    }

    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        Long old = this.kjoretoyId;
        this.kjoretoyId = kjoretoyId;

        firePropertyChange("kjoretoyId", old, kjoretoyId);
    }

    public Long getRodeId() {
        return rodeId;
    }

    public void setRodeId(Long rodeId) {
        Long old = this.rodeId;
        this.rodeId = rodeId;

        firePropertyChange("rodeId", old, rodeId);
    }

    public Long getRodetilknytningstypeId() {
        return rodetilknytningstypeId;
    }

    public void setRodetilknytningstypeId(Long rodetilknytningstypeId) {
        Long old = this.rodetilknytningstypeId;
        this.rodetilknytningstypeId = rodetilknytningstypeId;

        firePropertyChange("rodetilknytningstypeId", old, rodetilknytningstypeId);
    }
    public Long getKontraktId() {
        return kontraktId;
    }

    public void setKontraktId(Long kontraktId) {
        Long old = this.kontraktId;
        this.kontraktId = kontraktId;

        firePropertyChange("kontraktId", old, kontraktId);
    }

    public Date getFraTidspunkt() {
        return fraTidspunkt;
    }

    public void setFraTidspunkt(Date fraTidspunkt) {
        Date old = this.fraTidspunkt;
        this.fraTidspunkt = fraTidspunkt;

        firePropertyChange("fraTidspunkt", old, fraTidspunkt);
    }

    public Date getTilTidspunkt() {
        return tilTidspunkt;
    }

    public void setTilTidspunkt(Date tilTidspunkt) {
        Date old = this.tilTidspunkt;
        this.tilTidspunkt = tilTidspunkt;

        firePropertyChange("tilTidspunkt", old, tilTidspunkt);
    }

    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    public Rode getRode() {
        return rode;
    }

    public void setRode(Rode rode) {
        Rode old = this.rode;
        this.rode = rode;

        if(rode != null) {
            setRodeId(rode.getId());
        } else {
            setRodeId(null);
        }

        firePropertyChange("rode", old, rode);
    }

    public Rodetilknytningstype getRodetilknytningstype() {
        return rodetilknytningstype;
    }

    public void setRodetilknytningstype(Rodetilknytningstype rodetilknytningstype) {
        Rodetilknytningstype old = this.rodetilknytningstype;
        this.rodetilknytningstype = rodetilknytningstype;

        if(rodetilknytningstype != null) {
            setRodetilknytningstypeId(rodetilknytningstype.getId());
        } else {
            setRodetilknytningstypeId(null);
        }

        firePropertyChange("rodetilknytningstype", old, rodetilknytningstype);
    }

    public Driftkontrakt getKontrakt() {
        return kontrakt;
    }

    public void setKontrakt(Driftkontrakt kontrakt) {
        Driftkontrakt old = this.kontrakt;
        this.kontrakt = kontrakt;

        if(kontrakt != null) {
            setKontraktId(kontrakt.getId());
        } else {
           setKontraktId(null);
        }

        firePropertyChange("kontrakt", old, kontrakt);
    }

}