package no.mesta.mipss.persistence.mipssfield;

public class HendelseTrafikktiltakPK {

	private Long trafikktiltakId;
	private String hendelseGuid;
	
	public HendelseTrafikktiltakPK(){
	}
	public HendelseTrafikktiltakPK(Long trafikktiltakId, String hendelseGuid){
		this.trafikktiltakId = trafikktiltakId;
		this.hendelseGuid = hendelseGuid;
		
	}

	public Long getTrafikktiltakId() {
		return trafikktiltakId;
	}

	public String getHendelseGuid() {
		return hendelseGuid;
	}


}
