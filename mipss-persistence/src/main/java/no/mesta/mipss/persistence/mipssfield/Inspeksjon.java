package no.mesta.mipss.persistence.mipssfield;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.inndata.Prodpunkt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.vo.InspisertProsess;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Inspeksjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name="INSPEKSJON_XV")
@NamedQueries( { @NamedQuery(name = Inspeksjon.QUERY_FIND_ALL, query = "SELECT i FROM Inspeksjon i") })
@SequenceGenerator(name = Inspeksjon.ID_SEQ, sequenceName = "INSPEKSJON_REFNUMMER_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class Inspeksjon extends MipssEntityBean<Inspeksjon> implements Serializable, IOwnableMipssEntity,
		IRenderableMipssEntity, Comparable<Inspeksjon>, PropertyChangeSource, MipssFieldDetailItem {

	private static final Logger log = LoggerFactory.getLogger(Inspeksjon.class);
	
	private static final String BESKRIVELSE = "beskrivelse";
	private static final String EMPTY_STRING = "";
	private static final String ETTERSLEP = "etterslep";
	private static final String ETTERSLEPSREG_FLAGG = "etterslepsregFlagg";
	private static final String FRA_TIDSPUNKT = "fraTidspunkt";
	private static final String HENDELSER = "hendelser";
	private static final String ID_NAME = "guid";
	private static final String[] ID_NAMES = new String[] { ID_NAME };
	public static final String ID_SEQ = "inspeksjonRefnummerSeq";
	private static final String INSPISERTE_PROSESSER = "inspiserteProsesser";
	private static final String INSPISERTE_PROSESSER_GUI_TXT = "inspiserteProsesserGUITxt";
	private static final String KONTRAKT = "kontrakt";
	private static final String KONTRAKT_ID = "kontraktId";
	private static final String OWNED_MIPSS_ENTITY = "ownedMipssEntity";
	private static final String PDA_DFU_IDENT = "pdaDfuIdent";
	private static final String PRODUKSJONSPUNKTER = "produksjonspunkter";
	private static final String PROSESSER = "prosesser";
	private static final String PUNKTREGISTRERINGER = "punktregistreringer";
	public static final String QUERY_FIND_ALL = "Inspeksjon.findAll";
	private static final String REFNUMMER = "refnummer";
	private static final String SLETTET_AV = "slettetAv";
	private static final String SLETTET_DATO = "slettetDato";
	private static final String STREKNINGER = "strekninger";
	private static final String TEMPERATUR = "temperatur";
	private static final String TEMPERATUR_ID = "temperaturId";
	private static final String TIL_TIDSPUNKT = "tilTidspunkt";
	private static final String TXT_FLERE = "(flere)";
	private static final String VAER = "vaer";
	private static final String VAER_ID = "vaerId";
	private static final String VIND = "vind";
	private static final String VIND_ID = "vindId";
	
	
	private String beskrivelse;
	private Long etterslepsregFlagg;
	private Date fraTidspunkt;
	private String guid;
	private List<InspisertProsess> inspiserteProsesser;
	private Driftkontrakt kontrakt;
	private Long kontraktId;
	private OwnedMipssEntity ownedMipssEntity = new OwnedMipssEntity();
	private Long pdaDfuIdent;
	private List<Prodpunkt> produksjonspunkter = new ArrayList<Prodpunkt>();
	private List<Prosess> prosesser = new ArrayList<Prosess>();
	private Long refnummer;
	private String slettetAv;
	private Date slettetDato;
	private List<Prodstrekning> strekninger = new ArrayList<Prodstrekning>();
	private Temperatur temperatur;
	private Long temperaturId;
	private Date tilTidspunkt;
	private Vaer vaer;
	private Long vaerId;
	private Vind vind;
	private Long vindId;
	private List<Sak> sakList;

	/**
	 * Konstruktør
	 * 
	 */
	public Inspeksjon() {
		log("Inspeksjon() constructed");
	}
	
	@OneToMany(mappedBy = "inspeksjon", fetch = FetchType.LAZY)
	public List<Sak> getSakList(){
		return this.sakList;
	}
	public void setSakList(List<Sak> sakList){
		List<Sak> old = this.sakList; 
		this.sakList = sakList;
		firePropertyChange("sakList", old, sakList);
	}
	
	/**
	 * Bygger en liste over alle prosesser som er inspisert på denne
	 * inspeksjonen
	 * 
	 */
	private void buildInspiserteProsesser() {
		inspiserteProsesser = new ArrayList<InspisertProsess>();
		List<Prosess> adHocsProsess = new ArrayList<Prosess>();
		for (Prosess p : prosesser) {
			InspisertProsess ip = new InspisertProsess(this, p);
			ip.setAntallFunn(getProsessFunn(p).size());
			inspiserteProsesser.add(ip);
		}

		//TODO utkommentert pga modellendringer
//		for (Avvik punkt : punktregistreringer) {
//			Prosess p = punkt.getProsess();
//			if (!adHocsProsess.contains(p) && !prosesser.contains(p)) {
//				InspisertProsess ip = new InspisertProsess(this, p);
//				ip.setAntallFunn(getProsessFunn(p).size());
//				inspiserteProsesser.add(ip);
//				adHocsProsess.add(p);
//			}
//		}
	}
	
	/** {@inheritDoc} */
	public int compareTo(Inspeksjon o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(fraTidspunkt, o.fraTidspunkt).append(tilTidspunkt, o.tilTidspunkt).append(
				getOwnedMipssEntity().getOpprettetAv(), o.getOwnedMipssEntity().getOpprettetAv()).toComparison();
	}

	/**
	 * Returnerer antall funn som er knyttet til denne inspeksjonen
	 * 
	 * @return
	 */
	@Transient
	public Integer getAntallFunn() {
		//TODO utkommentert pga modellendringer
//		if (punktregistreringer == null) {
//			return Integer.valueOf(0);
//		} else {
//			return Integer.valueOf(punktregistreringer.size());
//		}
		return 0;
	}

	@Transient
	public Integer getAntallHendelser() {
		//TODO utkommentert pga modellendringer
//		if (hendelser == null) {
//			return Integer.valueOf(0);
//		} else {
//			return Integer.valueOf(hendelser.size());
//		}
		return 0;
	}

	/**
	 * Beskrivelse
	 * 
	 * @return
	 */
	@Column(name = "BESKRIVELSE", length = 1000)
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Verktøymetode for å avgjøre Long i db om til bool
	 * 
	 * @return
	 */
	@Transient
	public Boolean getEtterslep() {
		return (etterslepsregFlagg != null && etterslepsregFlagg.equals(1L) ? Boolean.valueOf(true) : Boolean
				.valueOf(false));
	}

	/**
	 * Etterslep
	 * 
	 * @return
	 */
	@Column(name = "ETTERSLEPSREG_FLAGG", nullable = false)
	public Long getEtterslepsregFlagg() {
		return etterslepsregFlagg;
	}

	/**
	 * Når inspeksjonen starter
	 * 
	 * @return
	 */
	@Column(name = "FRA_TIDSPUNKT")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	@Id
	@Column(nullable = false)
	public String getGuid() {
		return guid;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	/**
	 * Henter listen over alle prosesser som er inspisert
	 * 
	 * @return
	 */
	@Transient
	public List<InspisertProsess> getInspiserteProsesser() {
		if (inspiserteProsesser == null) {
			buildInspiserteProsesser();
		}

		return inspiserteProsesser;
	}

	/**
	 * Lager en GUI text for hvilke prosesser som er i inspeksjonen
	 * 
	 * @return
	 */
	@Transient
	public String getInspiserteProsesserGUITxt() {
		
		if (prosesser == null || prosesser.size() == 0) {
			return null;
		} else {
			StringBuffer prosesserTxt = new StringBuffer();
			for (int i = 0; i < prosesser.size() && i < 3; i++) {
				Prosess p = prosesser.get(i);
				prosesserTxt.append(p.getTextForGUI());
				if (i < prosesser.size() - 1) {
					prosesserTxt.append(", ");
				}
			}

			if (prosesser.size() > 3) {
				prosesserTxt.append(TXT_FLERE);
			}

			return prosesserTxt.toString();
		}
	}

	/**
	 * Kontrakten
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
	public Driftkontrakt getKontrakt() {
		return kontrakt;
	}

	/**
	 * Kontrakten
	 * 
	 * @return
	 */
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getKontraktId() {
		return kontraktId;
	}

	/** {@inheritDoc} */
	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		return ownedMipssEntity;
	}

	@Column(name = "PDA_DFU_IDENT")
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}

	/**
	 * Lager gui tekst av produserte strekninger
	 * 
	 * @return
	 */
	@Transient
	public String getProdHpGUIText() {
		List<VeiInfo> hpStrekninger = getProdHpStrekninger();
		return VeiInfoUtil.getProdHpGUIText(hpStrekninger);
	}

	/**
	 * Gir ut alle HP som det er produksjon på
	 * 
	 * @return
	 */
	@Transient
	public List<VeiInfo> getProdHpStrekninger() {
		return HPFilterUtil.filterProdstrekningOnHp(strekninger);
	}

	/**
	 * Produksjon
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "inspeksjon", fetch = FetchType.LAZY)
	public List<Prodpunkt> getProduksjonspunkter() {
		return produksjonspunkter;
	}

	/**
	 * Tilknyttede prosesser
	 * 
	 * @return
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "INSPEKSJON_PROSESS_XV", joinColumns = { @JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID") }, inverseJoinColumns = { @JoinColumn(name = "PROSESS_ID", referencedColumnName = "ID") })
	public List<Prosess> getProsesser() {
		return prosesser;
	}
	
	/**
	 * Returnerer funn for en gitt prosess
	 * 
	 * @param p
	 * @return
	 */
	public List<Avvik> getProsessFunn(Prosess p) {
		List<Avvik> funn = new ArrayList<Avvik>();
		//TODO utkommentert pga modellendringer
//		for (Avvik f : getPunktregistreringer()) {
//			if (f.getProsess().equals(p)) {
//				funn.add(f);
//			}
//		}

		return funn;
	}


	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Inspeksjon.ID_SEQ)
	@Column(name=REFNUMMER)
	public Long getRefnummer(){
		return refnummer;
	}

	/**
	 * Datoen funnet er opprettet
	 * 
	 * @return
	 */
	@Transient
	public Date getRegistreringsDato() {
		if (ownedMipssEntity != null) {
			return ownedMipssEntity.getOpprettetDato();
		} else {
			return null;
		}
	}

	@Column(name = "SLETTET_AV")
	public String getSlettetAv() {
		return slettetAv;
	}

	@Column(name = "SLETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSlettetDato() {
		return slettetDato;
	}

	/**
	 * Produksjon
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "inspeksjon", fetch = FetchType.LAZY)
	public List<Prodstrekning> getStrekninger() {
		return strekninger;
	}

	/**
	 * Temperatur intervall
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "TEMPERATUR_ID", referencedColumnName = "ID")
	public Temperatur getTemperatur() {
		return temperatur;
	}

	/**
	 * Temperatur intervall
	 * 
	 * @return
	 */
	@Column(name = "TEMPERATUR_ID", nullable = false, insertable = false, updatable = false)
	public Long getTemperaturId() {
		return temperaturId;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		String fra = fraTidspunkt != null ? MipssDateFormatter.formatDate(fraTidspunkt,
				MipssDateFormatter.SHORT_DATE_TIME_FORMAT) : EMPTY_STRING;
		String til = tilTidspunkt != null ? MipssDateFormatter.formatDate(tilTidspunkt,
				MipssDateFormatter.SHORT_DATE_TIME_FORMAT) : EMPTY_STRING;
		String av = getOwnedMipssEntity().getOpprettetAv() != null ? getOwnedMipssEntity().getOpprettetAv()
				: EMPTY_STRING;

		return fra + " - " + til + " av " + av;
	}
	@Transient
	public void setOpprettetAv(String opprettetAv){
		getOwnedMipssEntity().setOpprettetAv(opprettetAv);
	}
	/**
	 * Tidspunkt
	 * 
	 * @return
	 */
	@Column(name = "TIL_TIDSPUNKT")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}

	/**
	 * Vær
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "VAER_ID", referencedColumnName = "ID")
	public Vaer getVaer() {
		return vaer;
	}

	/**
	 * Vær
	 * 
	 * @return
	 */
	@Column(name = "VAER_ID", nullable = false, insertable = false, updatable = false)
	public Long getVaerId() {
		return vaerId;
	}

	/**
	 * Vind
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "VIND_ID", referencedColumnName = "ID")
	public Vind getVind() {
		return vind;
	}

	/**
	 * Vind
	 * 
	 * @return
	 */
	@Column(name = "VIND_ID", nullable = false, insertable = false, updatable = false)
	public Long getVindId() {
		return vindId;
	}


	/**
	 * Beskrivelse
	 * 
	 * @param beskrivelse
	 */
	public void setBeskrivelse(String beskrivelse) {
		log.trace("setBeskrivelse", beskrivelse);
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;

		try {
			fireVetoableChange(BESKRIVELSE, old, beskrivelse);
		} catch (PropertyVetoException e) {
			this.beskrivelse = old;
		}

		firePropertyChange(BESKRIVELSE, old, beskrivelse);
	}

	public void setEtterslep(Boolean flag) {
		Boolean old = getEtterslep();
		if (Boolean.TRUE.equals(flag)) {
			setEtterslepsregFlagg(1L);
		} else {
			setEtterslepsregFlagg(0L);
		}

		firePropertyChange(ETTERSLEP, old, getEtterslep());
	}

	/**
	 * Etterslep
	 * 
	 * @param etterslepsregFlagg
	 */
	public void setEtterslepsregFlagg(Long etterslepsregFlagg) {
		log.trace("setEtterslepsregFlagg({})", etterslepsregFlagg);
		Long old = this.etterslepsregFlagg;
		this.etterslepsregFlagg = etterslepsregFlagg;

		try {
			fireVetoableChange(ETTERSLEPSREG_FLAGG, old, etterslepsregFlagg);
		} catch (PropertyVetoException e) {
			this.etterslepsregFlagg = old;
		}

		firePropertyChange(ETTERSLEPSREG_FLAGG, old, this.etterslepsregFlagg);
	}

	/**
	 * Når inspeksjonen starter
	 * 
	 * @param fraTidspunkt
	 */
	public void setFraTidspunkt(Date fraTidspunkt) {
		log.trace("setFraTidspunkt({})", fraTidspunkt);
		Date old = this.fraTidspunkt;
		this.fraTidspunkt = fraTidspunkt;
		firePropertyChange(FRA_TIDSPUNKT, old, fraTidspunkt);
	}

	/**
	 * Id
	 * 
	 * @param guid
	 */
	public void setGuid(String guid) {
		log.trace("setGuid({})", guid);
		String old = this.guid;
		this.guid = guid;
		firePropertyChange(ID_NAME, old, guid);
	}

	/**
	 * Pre persist tar ut listen og oppdatterer prosesser
	 * 
	 */
	public void setInspiserteProsesser(List<InspisertProsess> inspiserteProsesser) {
		List<InspisertProsess> old = getInspiserteProsesser();
		String oldTxt = getInspiserteProsesserGUITxt();
		this.inspiserteProsesser = inspiserteProsesser;
		firePropertyChange(INSPISERTE_PROSESSER, old, inspiserteProsesser);
		firePropertyChange(INSPISERTE_PROSESSER_GUI_TXT, oldTxt, getInspiserteProsesserGUITxt());
	}

	/**
	 * Kontrakten
	 * 
	 * @param kontrakt
	 */
	public void setKontrakt(Driftkontrakt kontrakt) {
		log.trace("setKontrakt({})", kontrakt);
		Driftkontrakt old = this.kontrakt;
		this.kontrakt = kontrakt;

		if (kontrakt != null) {
			setKontraktId(kontrakt.getId());
		} else {
			setKontraktId(null);
		}
		firePropertyChange(KONTRAKT, old, kontrakt);
	}

	/**
	 * Kontrakten
	 * 
	 * @param kontraktId
	 */
	public void setKontraktId(Long kontraktId) {
		log.trace("setKontraktId({})", kontraktId);
		Long old = this.kontraktId;
		this.kontraktId = kontraktId;
		firePropertyChange(KONTRAKT_ID, old, kontraktId);
	}

	/**
	 * @see #getOwnedMipssEntity()
	 * @param ownedMipssEntity
	 */
	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		log.trace("setOwnedMipssEntity({})", ownedMipssEntity);
		OwnedMipssEntity old = this.ownedMipssEntity;
		this.ownedMipssEntity = ownedMipssEntity;
		firePropertyChange(OWNED_MIPSS_ENTITY, old, ownedMipssEntity);
	}

	public void setPdaDfuIdent(Long pdaDfuIdent) {
		Long old = this.pdaDfuIdent;
		this.pdaDfuIdent = pdaDfuIdent;
		firePropertyChange(PDA_DFU_IDENT, old, pdaDfuIdent);
	}

	/**
	 * Produksjon
	 * 
	 * @param produksjonspunkter
	 */
	public void setProduksjonspunkter(List<Prodpunkt> produksjonspunkter) {
		log.trace("setProduksjonspunkter({})", produksjonspunkter);
		List<Prodpunkt> old = this.produksjonspunkter;
		this.produksjonspunkter = produksjonspunkter;
		inspiserteProsesser = null;
		firePropertyChange(PRODUKSJONSPUNKTER, old, produksjonspunkter);
	}

	/**
	 * Tilknyttede prosesser
	 * 
	 * @param prosesser
	 */
	public void setProsesser(List<Prosess> prosesser) {
		log.trace("setProsesser({})", prosesser);
		List<Prosess> old = this.prosesser;
		this.prosesser = prosesser;
		inspiserteProsesser = null;
		firePropertyChange(PROSESSER, old, prosesser);
	}

	public void setRefnummer(Long refnummer){
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}

	public void setSlettetAv(String slettetAv) {
		String old = this.slettetAv;
		this.slettetAv = slettetAv;
		firePropertyChange(SLETTET_AV, old, slettetAv);
	}

	public void setSlettetDato(Date slettetDato) {
		Date old = this.slettetDato;
		this.slettetDato = slettetDato;
		firePropertyChange(SLETTET_DATO, old, slettetDato);
	}

	/**
	 * Produksjon
	 * 
	 * @param strekninger
	 */
	public void setStrekninger(List<Prodstrekning> strekninger) {
		log.trace("setStrekninger({})", strekninger);
		List<Prodstrekning> old = this.strekninger;
		this.strekninger = strekninger;
		firePropertyChange(STREKNINGER, old, strekninger);
	}

	/**
	 * Temperatur intervall
	 * 
	 * @param temperatur
	 */
	public void setTemperatur(Temperatur temperatur) {
		log.trace("setTemperatur({})", temperatur);
		Temperatur old = this.temperatur;
		this.temperatur = temperatur;

		if (temperatur != null) {
			setTemperaturId(temperatur.getId());
		} else {
			setTemperaturId(null);
		}
		firePropertyChange(TEMPERATUR, old, temperatur);
	}

	/**
	 * Temperatur intervall
	 * 
	 * @param temperaturId
	 */
	public void setTemperaturId(Long temperaturId) {
		log.trace("setTemperaturId({})", temperaturId);
		Long old = this.temperaturId;
		this.temperaturId = temperaturId;
		firePropertyChange(TEMPERATUR_ID, old, temperaturId);
	}

	/**
	 * Tidspunkt
	 * 
	 * @param tilTidspunkt
	 */
	public void setTilTidspunkt(Date tilTidspunkt) {
		log.trace("setTilTidspunkt({})", tilTidspunkt);
		Date old = this.tilTidspunkt;
		this.tilTidspunkt = tilTidspunkt;
		firePropertyChange(TIL_TIDSPUNKT, old, tilTidspunkt);
	}

	/**
	 * Vær
	 * 
	 * @param vaer
	 */
	public void setVaer(Vaer vaer) {
		log.trace("setVaer({})", vaer);
		Vaer old = this.vaer;
		this.vaer = vaer;

		if (vaer != null) {
			setVaerId(vaer.getId());
		} else {
			setVaerId(null);
		}
		firePropertyChange(VAER, old, vaer);
	}

	/**
	 * Vær
	 * 
	 * @param vaerId
	 */
	public void setVaerId(Long vaerId) {
		log.trace("setVaerId({})", vaerId);
		Long old = this.vaerId;
		this.vaerId = vaerId;
		firePropertyChange(VAER_ID, old, vaerId);
	}

	/**
	 * Vind
	 * 
	 * @param vind
	 */
	public void setVind(Vind vind) {
		log.trace("setVind({})", vind);
		Vind old = this.vind;
		this.vind = vind;

		if (vind != null) {
			setVindId(vind.getId());
		} else {
			setVindId(null);
		}
		firePropertyChange(VIND, old, vind);
	}

	/**
	 * Vind
	 * 
	 * @param vindId
	 */
	public void setVindId(Long vindId) {
		log.trace("setVindId({})", vindId);
		Long old = this.vindId;
		this.vindId = vindId;
		firePropertyChange(VIND_ID, old, vindId);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(REFNUMMER, refnummer).append(ID_NAME, guid).append(KONTRAKT_ID, kontraktId).append(BESKRIVELSE,
				beskrivelse).append(FRA_TIDSPUNKT, fraTidspunkt).append(TIL_TIDSPUNKT, tilTidspunkt).append(
				ETTERSLEPSREG_FLAGG, etterslepsregFlagg).append(
				VAER_ID, vaerId).append(VIND_ID, vindId).append(TEMPERATUR_ID, temperaturId).toString();
	}
}
