package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@IdClass(BrukermenyPK.class)
@NamedQueries({
    @NamedQuery(name = Brukermeny.QUERY_BRUKERMENY_GET_FOR_USER, query = "SELECT bm FROM Brukermeny bm where LOWER(bm.signatur) = :signatur"),
    @NamedQuery(name = Brukermeny.QUERY_BRUKERMENY_DELETE_FOR_USER, query = "DELETE FROM Brukermeny bm where LOWER(bm.signatur) = :signatur")
    })
public class Brukermeny implements Serializable {
    public static final String QUERY_BRUKERMENY_GET_FOR_USER = "Brukermeny.getForUser";
    public static final String QUERY_BRUKERMENY_DELETE_FOR_USER = "Brukermeny.deleteForUser";
    
    private Long menypunktId;
    private String signatur;
    private Bruker bruker;
    private Menypunkt menypunkt;

    public Brukermeny() {
    }

    @Id
    @Column(name="MENYPUNKT_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getMenypunktId() {
        return menypunktId;
    }

    public void setMenypunktId(Long menypunktId) {
        this.menypunktId = menypunktId;
    }

    @Id
    @Column(nullable = false, insertable = false, updatable = false)
    public String getSignatur() {
        return signatur;
    }

    public void setSignatur(String signatur) {
        this.signatur = signatur;
    }

    @ManyToOne
    @JoinColumn(name = "SIGNATUR", referencedColumnName = "SIGNATUR")
    public Bruker getBruker() {
        return bruker;
    }

    public void setBruker(Bruker bruker) {
        this.bruker = bruker;
        
        if(bruker!=null) {
            setSignatur(bruker.getSignatur());
        } else {
            setSignatur(null);
        }
    }

    @ManyToOne
    @JoinColumn(name = "MENYPUNKT_ID", referencedColumnName = "ID")
    public Menypunkt getMenypunkt() {
        return menypunkt;
    }

    public void setMenypunkt(Menypunkt menypunkt) {
        this.menypunkt = menypunkt;
        
        if(menypunkt != null) {
            setMenypunktId(menypunkt.getId());
        } else {
            setMenypunktId(null);
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Brukermeny)) {return false;}
        if(o == this) {return true;}
        
        Brukermeny other = (Brukermeny) o;
        
        return new EqualsBuilder().
            append(signatur, other.getSignatur()).
            append(menypunktId, other.getMenypunktId()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(201,301).
            append(signatur).
            append(menypunktId).
            toHashCode();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("signatur", signatur).
            append("menypunktId", menypunktId).
            toString();
    }
}
