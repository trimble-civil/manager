package no.mesta.mipss.persistence;

import java.io.Serializable;

/**
 * Implementeres av entiteter som man ønsker skal vises i et GUI.
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
public interface IRenderableMipssEntity extends Serializable {

    /**
     * Metoden forventes å sette sammen en tekst som skal leses i et GUI.
     * 
     * @return
     */
    String getTextForGUI();

}