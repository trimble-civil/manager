package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.eclipse.persistence.indirection.IndirectList;


/**
 * 
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "kjoretoymodellIdSeq", sequenceName = "KJORETOYMODELL_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
	@NamedQuery(name = Kjoretoymodell.FIND_ALL, query = "select o from Kjoretoymodell o"),
	@NamedQuery(name = Kjoretoymodell.FIND_VALID, query = "select m from Kjoretoymodell m where m.valgbarFlagg = 1 order by m.merke")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

public class Kjoretoymodell extends MipssEntityBean<Kjoretoymodell> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "Kjoretoymodell.findAll";
	public static final String FIND_VALID = "Kjoretoymodell.findValid";
	
    private String fritekst;
    private Long id;
    private String merke;
    private String modell;
    private Long aar;
    private Boolean valgbarFlagg;
    private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
    
    private OwnedMipssEntity ownedMipssEntity;
    
    public Kjoretoymodell() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoymodellIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(nullable = false)
    public String getMerke() {
        return merke;
    }

    public void setMerke(String merke) {
    	String old = this.merke;
        this.merke = merke;
        firePropertyChange("merke", old, merke);
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
    	String old = this.modell;
        this.modell = modell;
        firePropertyChange("modell", old, modell);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
    	if(valgbarFlagg == null) {
    		valgbarFlagg = false;
    	}
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }

    @OneToMany(mappedBy = "kjoretoymodell")
    public List<Kjoretoy> getKjoretoyList() {
        return kjoretoyList;
    }

    public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
    	List<Kjoretoy> old = this.kjoretoyList;
        this.kjoretoyList = kjoretoyList;
        
        if(!(kjoretoyList instanceof IndirectList)) {
        	firePropertyChange("kjoretoyList", old, kjoretoyList);
        }
    }

    public Kjoretoy addKjoretoy(Kjoretoy kjoretoy) {
        getKjoretoyList().add(kjoretoy);
        kjoretoy.setKjoretoymodell(this);
        return kjoretoy;
    }

    public Kjoretoy removeKjoretoy(Kjoretoy kjoretoy) {
        getKjoretoyList().remove(kjoretoy);
        kjoretoy.setKjoretoymodell(null);
        return kjoretoy;
    }

    public void setAar(Long aar) {
    	Long old = this.aar;
        this.aar = aar;
        firePropertyChange("aar", old, aar);
    }

    @Column(name="AAR", nullable = true)
    public Long getAar() {
        return aar;
    }
    
    // de neste metodene er h�nsdkrevet
     /**
      * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         return new ToStringBuilder(this).
             append("id", getId()).
             append("merke", getMerke()).
             append("modell", getModell()).
             append("år", getAar()).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante n�klene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof Kjoretoymodell)) {
             return false;
         }
         Kjoretoymodell other = (Kjoretoymodell) o;
         return new EqualsBuilder().
             append(getId(), other.getId()).
             isEquals();
     }

     /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
         return new HashCodeBuilder(23,91).
             append(getId()).
             toHashCode();
     }

     @Transient
    public String getTextForGUI() {
        StringBuffer b = new StringBuffer();
        
        if (merke != null && merke.length() > 0) {
            b.append(merke);
        }
        if (modell != null && modell.length() > 0) {
            b.append(" - ").
            append(modell);
        }
        return b.toString();
    }

    public int compareTo(Object o) {
        Kjoretoymodell other = (Kjoretoymodell) o;
        return new CompareToBuilder().
            append(getId(), other.getId()).
            toComparison();
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    /**
     * @return en ny getKjoretoymodellNavn instans basert p� denne modellen.
     */
    @Transient
    public KjoretoymodellNavn getKjoretoymodellNavn() {
    	return new KjoretoymodellNavn(this);
    }
    
    /**
     * @return en ny KjoretoymodellAar basert p� denne modellen.
     */
    @Transient
    public KjoretoymodellAar getKjoretoymodellAar() {
    	return new KjoretoymodellAar(this);
    }
}
