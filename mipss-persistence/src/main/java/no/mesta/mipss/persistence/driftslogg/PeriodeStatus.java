package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;


@SuppressWarnings("serial")
@Entity
@IdClass(PeriodeStatusPK.class)
@Table(name="PERIODE_STATUS")
public class PeriodeStatus extends MipssEntityBean<PeriodeStatus> implements Comparable<PeriodeStatus>{
	public enum StatusType{
		OPEN, // Period is open and new trips may be added to it
		CLOSED, // Period is closed and new trips may not be added to it, however, the user are able to open this period again
		SENT_IO // Period is permanently closed and it cannot be opened again
	}
	
	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPPRETTET_DATO", nullable=false)
	private Date opprettetDato;
	
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	
	@Id
	@Column(name="PERIODE_ID", insertable=false, updatable=false, nullable=true)
	private Long periodeId;
	@Column(name="STATUS_ID", insertable=false, updatable=false, nullable=true)
	private Long statusId;
	
	@ManyToOne
	@JoinColumn(name = "PERIODE_ID", referencedColumnName = "ID")
	private Periode periode;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID", referencedColumnName = "ID")
	private PeriodestatusType status;

	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}

	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	/**
	 * @return the periodeId
	 */
	public Long getPeriodeId() {
		return periodeId;
	}

	/**
	 * @param periodeId the periodeId to set
	 */
	public void setPeriodeId(Long periodeId) {
		this.periodeId = periodeId;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the status
	 */
	public PeriodestatusType getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(PeriodestatusType status) {
		this.status = status;
		if (status==null){
			statusId = null;
		}else{
			statusId = status.getId();
		}
	}

	public void setPeriode(Periode periode) {
		this.periode = periode;
		if (periode==null)
			periodeId = null;
		else{
			periodeId = periode.getId();
		}
	}

	public Periode getPeriode() {
		return periode;
	}
	
	public int compareTo(PeriodeStatus o) {
		return new CompareToBuilder().append(o.getOpprettetDato(), getOpprettetDato()).toComparison();
	}

	public String getStatusNavn() {
		return status.getStatus();
	}
	
	public StatusType getStatusType(){
		switch (statusId.intValue()){
		case 5:return StatusType.OPEN;
		case 6:return StatusType.CLOSED;
		case 7:return StatusType.SENT_IO;
		default :return null;
		}
	}
	
}
