package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Friksjonsdetalj.QUERY_FIND_ALL, query = "select o from Friksjonsdetalj o"),
	@NamedQuery(name = Friksjonsdetalj.QUERY_FIND_BY_KONTRAKT_AND_TIME, query = 
		"select o from Friksjonsdetalj o where " +
		"o.dfuId in(:dfuIds) " +
		"and o.gmtTidspunkt between :fraGmtTidspunkt and :tilGmtTidspunkt")
})

@SequenceGenerator(name = Friksjonsdetalj.ID_SEQ, sequenceName = "friksjonsdetalj_id_seq", initialValue = 1, allocationSize = 1)

public class Friksjonsdetalj extends MipssEntityBean<Friksjonsdetalj> implements Serializable, IRenderableMipssEntity{
	
	private static final String[] ID_FIELDS = new String[]{"id"};
	
	public static final String ID_SEQ = "friksjonsdetaljIdSeq";
	public static final String QUERY_FIND_ALL ="Friksjonsdetalj.findAll";
	public static final String QUERY_FIND_BY_KONTRAKT_AND_TIME = "Friksjonsdetalj.findByKontraktAndTime"; 
    private String beskrivelse;
    private Long dfuId;
    private String endretAv;
    private Date endretDato;
    private Fore fore;
    private Long foreId;
    private Double friksjon;
    private String fullfortTiltak;
    private Long fylkesnummer;
    private Date gmtTidspunkt;
    private Long hp;
    private Long id;
    private Double km;
    private Long kommunenummer;
    private String maalertype;
    private boolean manuell;
    private Nedbor nedbor;
    private Long nedborId;
    private String opprettetAv;
    private Date opprettetDato;
    private String regnr;
    private String retning;
    private String startTiltak;
    private Double temperatur;
    private String utfortAv;
    private Date utfortDatoTid;
    private String veikategori;
    
    private Long veinummer;
    private String veistatus;

    private Date datoFra;
    private Date datoTil;
    
    public Friksjonsdetalj clone(){
    	Friksjonsdetalj n = new Friksjonsdetalj();
    	n.setBeskrivelse(beskrivelse);
    	n.setDfuId(dfuId);
    	n.setEndretAv(endretAv);
    	n.setEndretDato(endretDato);
    	n.setFore(fore);
    	n.setForeId(foreId);
    	n.setFriksjon(friksjon);
    	n.setFullfortTiltak(fullfortTiltak);
    	n.setFylkesnummer(fylkesnummer);
    	n.setGmtTidspunkt(gmtTidspunkt);
    	n.setHp(hp);
    	n.setId(id);
    	n.setKm(km);
    	n.setKommunenummer(kommunenummer);
    	n.setMaalertype(maalertype);
    	n.setManuell(manuell);
    	n.setNedbor(nedbor);
    	n.setNedborId(nedborId);
    	n.setOpprettetAv(opprettetAv);
    	n.setOpprettetDato(opprettetDato);
    	n.setRegnr(regnr);
    	n.setRetning(retning);
    	n.setStartTiltak(startTiltak);
    	n.setTemperatur(temperatur);
    	n.setUtfortAv(utfortAv);
    	n.setUtfortDatoTid(utfortDatoTid);
    	n.setVeikategori(veikategori);
    	n.setVeinummer(veinummer);
    	n.setVeistatus(veistatus);
    	n.setDatoFra(datoFra);
    	n.setDatoTil(datoTil);
    	return n;
    	
    }
    public Friksjonsdetalj() {
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Friksjonsdetalj)) {return false;}
        
        Friksjonsdetalj other = (Friksjonsdetalj) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    @Transient
    public String getDato(){
    	if (getGmtTidspunkt()!=null){
	    	String date = MipssDateFormatter.formatDate(MipssDateFormatter.convertFromGMTtoCET(getGmtTidspunkt()), MipssDateFormatter.DATE_FORMAT);
	    	return date;	
    	}return null;
    }

    @Column(name="DFU_ID")
    public Long getDfuId() {
        return dfuId;
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    @ManyToOne
    @JoinColumn(name = "FORE_ID", referencedColumnName = "ID")
    public Fore getFore() {
        return fore;
    }

    @Transient
    public Long getForeNr(){
    	if (fore!=null)
    		return fore.getNummer();
    	return null;
    }
    
    public Double getFriksjon() {
        return friksjon;
    }


    @Column(name="FULLFORT_TILTAK")
    public String getFullfortTiltak() {
        return fullfortTiltak;
    }

    @Column(nullable = false)
    public Long getFylkesnummer() {
        return fylkesnummer;
    }

    @Column(name="GMT_TIDSPUNKT")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getGmtTidspunkt() {
        return gmtTidspunkt;
    }

    @Column(nullable = false)
    public Long getHp() {
        return hp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Friksjonsdetalj.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    @Override
	public String[] getIdNames() {
		return ID_FIELDS;
	}

    @Transient
    public String getKl(){
    	if (getGmtTidspunkt()!=null){
	    	String kl = MipssDateFormatter.formatDate(MipssDateFormatter.convertFromGMTtoCET(getGmtTidspunkt()), MipssDateFormatter.SHORT_TIME_FORMAT);
	    	return kl;	
    	}
    	return null;
    }

    @Column(nullable = false)
    public Double getKm() {
        return km;
    }
    
    @Column(nullable = false)
    public Long getKommunenummer() {
        return kommunenummer;
    }
    public String getMaalertype() {
        return maalertype;
    }

    @ManyToOne
    @JoinColumn(name = "NEDBOR_ID", referencedColumnName = "ID")
    public Nedbor getNedbor() {
        return nedbor;
    }

    @Transient
    public Long getNedborNr(){
    	if (nedbor!=null)
    		return nedbor.getNummer();
    	return null;
    }

    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public String getRegnr() {
        return regnr;
    }

    public String getRetning() {
        return retning;
    }

    @Column(name="START_TILTAK")
    public String getStartTiltak() {
        return startTiltak;
    }

    public Double getTemperatur() {
        return temperatur;
    }

    public String getTextForGUI() {
		return null;
	}

    @Column(name="UTFORT_AV")
    public String getUtfortAv() {
        return utfortAv;
    }


    @Column(name="UTFORT_DATO_TID")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUtfortDatoTid() {
        return utfortDatoTid;
    }

    @Transient
    public String getVegnrhp(){
        return getVeikategoriStatus()+getVeinummer()+"/"+getHp();
    }

    @Column(nullable = false)
    public String getVeikategori() {
        return veikategori;
    }

    @Transient
    public String getVeikategoriStatus(){
    	String vs = "";
    	if (getVeikategori()!=null){
    		vs+=getVeikategori();
    	}
    	if (getVeistatus()!=null)
    		vs+=getVeistatus();
    	return vs;
    }

    @Column(nullable = false)
    public Long getVeinummer() {
        return veinummer;
    }

    @Column(nullable = false)
    public String getVeistatus() {
        return veistatus;
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(939,981).append(id).toHashCode();
    }

    @Transient
    public boolean isManuell(){
    	return manuell;
    }
    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
        System.out.println("beskrivelse..");
    }

    public void setDato(String dato){
    	Date old = getGmtTidspunkt();
    	String oldDato = getDato();
    	String oldKl = getKl();
    	boolean empty = getGmtTidspunkt()==null;
    	
		Calendar c = Calendar.getInstance();
		if (!empty)
			c.setTime(MipssDateFormatter.convertFromGMTtoCET(getGmtTidspunkt()));
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy");
		try {
			Date d = f.parse(dato);
			Calendar dc = Calendar.getInstance();
			dc.setTime(d);
			c.set(Calendar.YEAR, dc.get(Calendar.YEAR));
			c.set(Calendar.MONTH, dc.get(Calendar.MONTH));
			c.set(Calendar.DAY_OF_MONTH, dc.get(Calendar.DAY_OF_MONTH));
			if (empty){
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
			}
			setGmtTidspunkt(MipssDateFormatter.convertFromCETtoGMT(c.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
    	}
		
    	firePropertyChange("dato", oldDato, getDato());
    	firePropertyChange("kl", oldKl, getKl());
    	firePropertyChange("gmtTidspunkt", old, getGmtTidspunkt());
    	
    }
    public void setDfuId(Long dfuId) {
    	Long old = this.dfuId;
        this.dfuId = dfuId;
        firePropertyChange("dfuId", old, dfuId);
    }

    public void setEndretAv(String endretAv) {
        String old = this.endretAv;
    	this.endretAv = endretAv;
    	firePropertyChange("endretAv", old, endretAv);
    }

    public void setEndretDato(Date endretDato) {
        Date old = this.endretDato;
    	this.endretDato = endretDato;
    	firePropertyChange("endretDato", old, endretDato);
    }

    public void setFore(Fore fore) {
    	Fore old = this.fore;
        this.fore = fore;
        if(fore != null) {
        	setForeId(fore.getId());
        } else {
        	setForeId(null);
        }
        firePropertyChange("fore", old, fore);
    }

    public void setFriksjon(Double friksjon) {
        Double old = this.friksjon;
    	this.friksjon = friksjon;
    	firePropertyChange("friksjon", old, friksjon);
    }

    public void setFullfortTiltak(String fullfortTiltak) {
        String old = this.fullfortTiltak;
    	this.fullfortTiltak = fullfortTiltak;
    	firePropertyChange("fullfortTiltak", old, fullfortTiltak);
    }

    public void setFylkesnummer(Long fylkesnummer) {
    	Long old = this.fylkesnummer; 
        this.fylkesnummer = fylkesnummer;
        firePropertyChange("fylkesnummer", old, fylkesnummer);
    }

    public void setGmtTidspunkt(Date gmtTidspunkt) {
    	Date old = this.gmtTidspunkt;
        this.gmtTidspunkt = gmtTidspunkt;
        firePropertyChange("gmtTidspunkt", old, gmtTidspunkt);
    }

    public void setHp(Long hp) {
    	Long old = this.hp;
        this.hp = hp;
        firePropertyChange("hp", old, hp);
    }

    public void setId(Long id) {
    	Long old = id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    public void setKl(String cetKl){
    	String oldDato = getDato();
    	String oldKl = getKl();
    	Date old = getGmtTidspunkt();
    	
    	if (cetKl==null)
    		cetKl = "00:00";
    	SimpleDateFormat f = new SimpleDateFormat("HH:mm");
    	try {
			Date kl = f.parse(cetKl);
    		
			Calendar c = Calendar.getInstance();
    		if (getGmtTidspunkt()!=null)
    			c.setTime(MipssDateFormatter.convertFromGMTtoCET(getGmtTidspunkt()));
    		else
    			c.setTime(Clock.now());
    		
    		Calendar klCal = Calendar.getInstance();
    		klCal.setTime(kl);
    		c.set(Calendar.MINUTE, klCal.get(Calendar.MINUTE));
    		c.set(Calendar.HOUR_OF_DAY, klCal.get(Calendar.HOUR_OF_DAY));
    		setGmtTidspunkt(MipssDateFormatter.convertFromCETtoGMT(c.getTime()));
	    	
		} catch (ParseException e) {
			e.printStackTrace();
		}
		firePropertyChange("dato", oldDato, getDato());
    	firePropertyChange("kl", oldKl, getKl());
    	firePropertyChange("gmtTidspunkt", old, getGmtTidspunkt());
    	
    }

    public void setKm(Double km) {
    	Double old = this.km;
        this.km = km;
        firePropertyChange("km", old, km);
    }

    public void setKommunenummer(Long kommunenummer) {
        Long old = this.kommunenummer;
    	this.kommunenummer = kommunenummer;
    	firePropertyChange("kommunenummer", old, kommunenummer);
    }

    public void setMaalertype(String maalertype) {
    	String old = this.maalertype;
        this.maalertype = maalertype;
        firePropertyChange("maalertype", old, maalertype);
    }

    public void setManuell(boolean manuell){
    	this.manuell = manuell;
    }

    public void setNedbor(Nedbor nedbor) {
    	Nedbor old = this.nedbor;
        this.nedbor = nedbor;
        if(nedbor != null) {
        	setNedborId(nedbor.getId());
        } else {
        	setNedborId(null);
        }
        firePropertyChange("nedbor", old, nedbor);
    }

    public void setOpprettetAv(String opprettetAv) {
    	String old = this.opprettetAv;
        this.opprettetAv = opprettetAv;
        firePropertyChange("opprettetAv", old, opprettetAv);
    }

    public void setOpprettetDato(Date opprettetDato) {
    	Date old = this.opprettetDato;
        this.opprettetDato = opprettetDato;
        firePropertyChange("opprettetDato", old, opprettetDato);
    }

    public void setRegnr(String regnr) {
    	String old = this.regnr;
        this.regnr = regnr;
        firePropertyChange("regnr", old, regnr);
    }
    
    public void setRetning(String retning) {
    	String old = this.retning;
        this.retning = retning;
        firePropertyChange("retning", old, retning);
    }
    public void setStartTiltak(String startTiltak) {
    	String old = this.startTiltak;
        this.startTiltak = startTiltak;
        firePropertyChange("startTiltak", old, startTiltak);
    }
    
    public void setTemperatur(Double temperatur) {
    	Double old = this.temperatur;
        this.temperatur = temperatur;
        firePropertyChange("temperatur", old, temperatur);
    }
    public void setUtfortAv(String utfortAv) {
    	String old = this.utfortAv;
        this.utfortAv = utfortAv;
        firePropertyChange("utfortAv", old, utfortAv);
    }
    public void setUtfortDatoTid(Date utfortDatoTid) {
    	Date old = this.utfortDatoTid;
        this.utfortDatoTid = utfortDatoTid;
        firePropertyChange("utfortDatoTid", old, utfortDatoTid);
    }
    public void setVeikategori(String veikategori) {
    	String old = this.veikategori;
        this.veikategori = veikategori;
        firePropertyChange("veikategori", old, veikategori);
    }
    
    
    public void setVeikategoriStatus(String veikatStatus){
    	if (veikatStatus!=null&&!veikatStatus.equals("")){
    		veikatStatus = veikatStatus.toUpperCase();
	    	setVeikategori(veikatStatus.charAt(0)+"");
	    	if (veikatStatus.length()>1){
	    		setVeistatus(veikatStatus.charAt(1)+"");
	    	}else
	    		setVeistatus(null);
    	}else{
    		setVeikategori(null);
    		setVeistatus(null);
    	}
    	
    }

    public void setVeinummer(Long veinummer) {
    	Long old = this.veinummer;
        this.veinummer = veinummer;
        firePropertyChange("veinummer", old, veinummer);
    }
    
    public void setVeistatus(String veistatus) {
    	String old = this.veistatus;
        this.veistatus = veistatus;
        firePropertyChange("veistatus", old, veistatus);
    }

	/**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("gmtTidspunkt", gmtTidspunkt).
            append("dfuId", dfuId).
            append("fore", fore).
            append("nedbor", nedbor).
            append("maalertype", maalertype).
            append("friksjon", friksjon).
            append("utfortAv", utfortAv).
            append("utfortDatoTid", utfortDatoTid).
            append("regnr", regnr).
            append("fylkesnummer", fylkesnummer).
            append("kommunenummer", kommunenummer).
            append("veikategori", veikategori).
            append("veistatus", veistatus).
            append("veinummer", veinummer).
            append("hp", hp).
            append("km", km).
            append("retning", retning).
            append("temperatur", temperatur).
            append("startTiltak", startTiltak).
            append("fullfortTiltak", fullfortTiltak).
            append("beskrivelse", beskrivelse).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).            
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            toString();
    }

    @Column(name = "FORE_ID", nullable = false, insertable = false, updatable = false)
	public Long getForeId() {
		return foreId;
	}

	public void setForeId(Long foreId) {
		this.foreId = foreId;
	}

	@Column(name = "NEDBOR_ID", nullable = false, insertable = false, updatable = false)
	public Long getNedborId() {
		return nedborId;
	}

	public void setNedborId(Long nedborId) {
		this.nedborId = nedborId;
	}
	@Transient
	public Date getDatoFra() {
		return datoFra;
	}

	public void setDatoFra(Date datoFra) {
		this.datoFra = datoFra;
	}
	@Transient
	public Date getDatoTil() {
		return datoTil;
	}

	public void setDatoTil(Date datoTil) {
		this.datoTil = datoTil;
	}
}
