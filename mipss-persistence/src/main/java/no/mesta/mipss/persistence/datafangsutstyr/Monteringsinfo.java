package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@Table(name="MONTERINGSINFO")
public class Monteringsinfo extends MipssEntityBean<Installasjon> implements Serializable, IRenderableMipssEntity {

	private Long id;
	private String fritekst;
	private String plasseringsnotat;
	private String spenningTenning;
	private String spenningPluss;
	private Date opprettetDatoMontering;
	private String opprettetAvMontering;
	private Date signeringsdato;
	private String installasjonsted;
	private String montornavn;
	private String spenningJord;
	
	private Installasjon installasjon;
	
	@Id
	@Column(name="INSTALLASJON_ID", nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(table="MONTERINGSINFO")
	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		Object oldValue = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", oldValue, fritekst);
	}

	@Column(table="MONTERINGSINFO")
	public String getPlasseringsnotat() {
		return plasseringsnotat;
	}

	public void setPlasseringsnotat(String plasseringsnotat) {
		Object oldValue = this.plasseringsnotat;
		this.plasseringsnotat = plasseringsnotat;
		firePropertyChange("plasseringsnotat", oldValue, plasseringsnotat);
	}

	@Column(name="SPENNING_TENNING", table="MONTERINGSINFO")
	public String getSpenningTenning() {
		return spenningTenning;
	}

	public void setSpenningTenning(String spenningTenning) {
		Object oldValue = this.spenningTenning;
		this.spenningTenning = spenningTenning;
		firePropertyChange("spenningTenning", oldValue, spenningTenning);
	}

	@Column(name="SPENNING_PLUSS", table="MONTERINGSINFO")
	public String getSpenningPluss() {
		return spenningPluss;
	}

	public void setSpenningPluss(String spenningPluss) {
		Object oldValue = this.spenningPluss;
		this.spenningPluss = spenningPluss;
		firePropertyChange("spenningPluss", oldValue, spenningPluss);
	}

	@Column(name="SPENNING_JORD", table="MONTERINGSINFO")
	public String getSpenningJord() {
		return spenningJord;
	}

	public void setSpenningJord(String spenningJord) {
		Object oldValue = this.spenningJord;
		this.spenningJord = spenningJord;
		firePropertyChange("spenningJord", oldValue, spenningJord);
	}

	@Column(table="MONTERINGSINFO")
	public String getMontornavn() {
		return montornavn;
	}

	public void setMontornavn(String montornavn) {
		Object oldValue = this.montornavn;
		this.montornavn = montornavn;
		firePropertyChange("montornavn", oldValue, montornavn);
	}

	@Column(table="MONTERINGSINFO")
	public String getInstallasjonsted() {
		return installasjonsted;
	}

	public void setInstallasjonsted(String installasjonsted) {
		Object oldValue = this.installasjonsted;
		this.installasjonsted = installasjonsted;
		firePropertyChange("installasjonsted", oldValue, installasjonsted);
	}

	@Column(table="MONTERINGSINFO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSigneringsdato() {
		return signeringsdato;
	}

	public void setSigneringsdato(Date signeringsdato) {
		Object oldValue = this.signeringsdato;
		this.signeringsdato = signeringsdato;
		firePropertyChange("signeringsdato", oldValue, signeringsdato);
	}

	@Column(name="OPPRETTET_AV", table="MONTERINGSINFO")
	public String getOpprettetAvMontering() {
		return opprettetAvMontering;
	}

	public void setOpprettetAvMontering(String opprettetAvMontering) {
		this.opprettetAvMontering = opprettetAvMontering;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPPRETTET_DATO", table="MONTERINGSINFO")
	public Date getOpprettetDatoMontering() {
		return opprettetDatoMontering;
	}

	public void setOpprettetDatoMontering(Date opprettetDatoMontering) {
		this.opprettetDatoMontering = opprettetDatoMontering;
	}

	public void setInstallasjon(Installasjon installasjon) {
		this.installasjon = installasjon;
	}
	@OneToOne(mappedBy="monteringsinfo")
	public Installasjon getInstallasjon() {
		return installasjon;
	}
}
