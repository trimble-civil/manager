package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;

public class LeverandorVinterdrift implements Serializable {

	private String navn;
	private int nummer;

	public static String[] getPropertiesArray() {
		return new String[] {
				"navn",
				"nummer"
		};
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

}