package no.mesta.mipss.persistence.kontrakt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = SlettFeltstatistikk.ID_SEQ, sequenceName = "SLETT_FELTSTATISTIKK_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQuery(name = SlettFeltstatistikk.QUERY_FIND_ALL_ACTIVE, 
    query = "select o from SlettFeltstatistikk o where o.utfortDato is null")
    
@Table(name = "SLETT_FELTSTATISTIKK")
public class SlettFeltstatistikk extends MipssEntityBean<SlettFeltstatistikk>{
	public static final String QUERY_FIND_ALL_ACTIVE = "SlettFeltstatistikk.findAllActive";
	public static final String ID_SEQ = "slettFeltstatistikkIdSeq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SlettFeltstatistikk.ID_SEQ)
	private Long id;
	@Column(name="KONTRAKT_ID", nullable=false)
	private Long kontraktId;
	@Column(name="UTFORT_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date utfortDato;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the kontraktId
	 */
	public Long getKontraktId() {
		return kontraktId;
	}
	/**
	 * @param kontraktId the kontraktId to set
	 */
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	/**
	 * @return the utfortDato
	 */
	public Date getUtfortDato() {
		return utfortDato;
	}
	/**
	 * @param utfortDato the utfortDato to set
	 */
	public void setUtfortDato(Date utfortDato) {
		this.utfortDato = utfortDato;
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	
	
}
