package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = DfuKontrakt.QUERY_FIND_ALL, query = "select o from DfuKontrakt o order by o.kontraktId"),
	@NamedQuery(name =DfuKontrakt.QUERY_FIND_BY_KONTRAKT, query="select o from DfuKontrakt o where o.kontraktId=:id")
})

@Table(name = "DFU_KONTRAKT")
@IdClass(DfuKontraktPK.class)
public class DfuKontrakt extends MipssEntityBean<DfuKontrakt> implements IRenderableMipssEntity, Serializable {
	public static final String QUERY_FIND_BY_KONTRAKT = "DfuKontrakt.findByKontrakt";
	public static final String QUERY_FIND_ALL = "DfuKontrakt.findAll";
	
	private static final String[] ID_NAMES = {"dfuId", "kontraktId"};
	
    private Long dfuId;
    private Long kontraktId;
    private Dfuindivid dfuindivid;
    private Driftkontrakt driftkontrakt;
    private String navn;
    private Date fraDato;
    private Date tilDato;

    public DfuKontrakt() {
    }

    @Id
    @Column(name="DFU_ID", nullable = false, insertable = false, updatable = false)
    public Long getDfuId() {
        return dfuId;
    }

    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    @Id
    @Column(name="KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
    public Long getKontraktId() {
        return kontraktId;
    }

    public void setKontraktId(Long kontraktId) {
        this.kontraktId = kontraktId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DFU_ID", referencedColumnName = "ID")
    public Dfuindivid getDfuindivid() {
        return dfuindivid;
    }

    public void setDfuindivid(Dfuindivid dfuindivid) {
        this.dfuindivid = dfuindivid;
        
        if(dfuindivid != null) {
        	setDfuId(dfuindivid.getId());
        } else {
        	setDfuId(null);
        }
    }
    
    public void setNavn(String navn){
    	this.navn = navn;
    }
    
    public String getNavn(){
    	return navn;
    }
    
    @Column(name = "FRA_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getFraDato() {
		return fraDato;
	}

	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	@Column(name = "TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTilDato() {
		return tilDato;
	}

	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
    public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
		
        if(driftkontrakt != null) {
        	setKontraktId(driftkontrakt.getId());
        } else {
        	setKontraktId(null);
        }
	}

	@Transient
	public String getModelNavn() {
		return dfuindivid != null ? dfuindivid.getDfumodell() != null ? dfuindivid.getDfumodell().getNavn() : null : null;
	}
	
	@Transient
	public String getModelLeverandor() {
		return dfuindivid != null ? dfuindivid.getDfumodell() != null ? dfuindivid.getDfumodell().getLeverandorNavn() : null : null;
	}
	
	
	@Transient
	public String getSisteDfuStatus() {
		if(dfuindivid == null || dfuindivid.getDfu_StatusList() == null) {
			return null;
		}
		
		Date sisteDate = null;
		String sisteStatus = null;
		for(Dfu_Status dfuStatus : dfuindivid.getDfu_StatusList()) {
			if(sisteDate == null || dfuindivid.getOpprettetDato().after(sisteDate)) {
				sisteDate = dfuindivid.getOpprettetDato();
				sisteStatus = dfuStatus.getDfustatus().getNavn();
			}
		}
		
		return sisteStatus;
	}
	
	// de neste metodene er håndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entitybønnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("dfu id", getDfuId()).
            append("kontrakt id", getKontraktId()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof DfuKontrakt)) {
            return false;
        }
        DfuKontrakt other = (DfuKontrakt) o;
        return new EqualsBuilder().
            append(getDfuId(), other.getDfuId()).
            append(getKontraktId(), other.getKontraktId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getDfuId()).
            append(getKontraktId()).
            toHashCode();
    }

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public String getTextForGUI() {
		return toString();
	}
}
