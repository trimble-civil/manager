package no.mesta.mipss.persistence.config;

public class HintValues {
    public static final String TRUE = org.eclipse.persistence.config.HintValues.TRUE;
    public static final String FALSE = org.eclipse.persistence.config.HintValues.FALSE;
    public static final String MAP = org.eclipse.persistence.config.ResultType.Map;
}
