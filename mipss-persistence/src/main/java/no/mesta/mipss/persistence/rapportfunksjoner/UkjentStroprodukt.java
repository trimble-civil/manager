package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

public class UkjentStroprodukt implements Serializable{
	
	private Long kontraktId;
	private String kontrakt;
	private String kjoretoyId;
	private String regnr;
	private String maskinnr;
	private int dfuId;
	private String serienr;
	private String sprederType;
	private String konsolltekstTort;
	private String konsolltekstLosning;
	private String innTort;
	private String innLosning;
	private String mestaTort;
	private String mestaLosning;
	private String melding;
	private Date fraDatoTid;
	private Date tilDatoTid;
	private int antallTiltak;
	private Double sumTimer;
	private Double tonnTortTotalt;
	private Double tonnTortFraLosning;
	private Double m3Losning;
	private Double tonnLosning;
	
	
	public static String[] getPropertiesArray() {
		return new String[] {"kontraktId", "kontrakt", "kjoretoyId", "regnr", "maskinnr", "dfuId", "serienr", "sprederType", 
							"konsolltekstTort", "konsolltekstLosning", "innTort", "innLosning", "mestaTort",
							"mestaLosning", "melding", "fraDatoTid", "tilDatoTid", "antallTiltak", "sumTimer",
							"tonnTortTotalt", "tonnTortFraLosning", "m3Losning", "tonnLosning"};
	}


	/**
	 * @return the kontraktId
	 */
	public Long getKontraktId() {
		return kontraktId;
	}

	/**
	 * @param kontraktId the kontraktId to set
	 */
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	/**
	 * @return the kontrakt
	 */
	public String getKontrakt() {
		return kontrakt;
	}


	/**
	 * @param kontrakt the kontrakt to set
	 */
	public void setKontrakt(String kontrakt) {
		this.kontrakt = kontrakt;
	}


	/**
	 * @return the kjoretoyId
	 */
	public String getKjoretoyId() {
		return kjoretoyId;
	}


	/**
	 * @param kjoretoyId the kjoretoyId to set
	 */
	public void setKjoretoyId(String kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}


	/**
	 * @return the regnr
	 */
	public String getRegnr() {
		return regnr;
	}


	/**
	 * @param regnr the regnr to set
	 */
	public void setRegnr(String regnr) {
		this.regnr = regnr;
	}


	/**
	 * @return the maskinnr
	 */
	public String getMaskinnr() {
		return maskinnr;
	}


	/**
	 * @param maskinnr the maskinnr to set
	 */
	public void setMaskinnr(String maskinnr) {
		this.maskinnr = maskinnr;
	}


	/**
	 * @return the dfuId
	 */
	public int getDfuId() {
		return dfuId;
	}


	/**
	 * @param dfuId the dfuId to set
	 */
	public void setDfuId(int dfuId) {
		this.dfuId = dfuId;
	}


	/**
	 * @return the serienr
	 */
	public String getSerienr() {
		return serienr;
	}


	/**
	 * @param serienr the serienr to set
	 */
	public void setSerienr(String serienr) {
		this.serienr = serienr;
	}


	/**
	 * @return the sprederType
	 */
	public String getSprederType() {
		return sprederType;
	}


	/**
	 * @param sprederType the sprederType to set
	 */
	public void setSprederType(String sprederType) {
		this.sprederType = sprederType;
	}


	/**
	 * @return the konsolltekstTort
	 */
	public String getKonsolltekstTort() {
		return konsolltekstTort;
	}


	/**
	 * @param konsolltekstTort the konsolltekstTort to set
	 */
	public void setKonsolltekstTort(String konsolltekstTort) {
		this.konsolltekstTort = konsolltekstTort;
	}


	/**
	 * @return the konsolltekstLosning
	 */
	public String getKonsolltekstLosning() {
		return konsolltekstLosning;
	}


	/**
	 * @param konsolltekstLosning the konsolltekstLosning to set
	 */
	public void setKonsolltekstLosning(String konsolltekstLosning) {
		this.konsolltekstLosning = konsolltekstLosning;
	}


	/**
	 * @return the innTort
	 */
	public String getInnTort() {
		return innTort;
	}


	/**
	 * @param innTort the innTort to set
	 */
	public void setInnTort(String innTort) {
		this.innTort = innTort;
	}


	/**
	 * @return the innLosning
	 */
	public String getInnLosning() {
		return innLosning;
	}


	/**
	 * @param innLosning the innLosning to set
	 */
	public void setInnLosning(String innLosning) {
		this.innLosning = innLosning;
	}


	/**
	 * @return the mestaTort
	 */
	public String getMestaTort() {
		return mestaTort;
	}


	/**
	 * @param mestaTort the mestaTort to set
	 */
	public void setMestaTort(String mestaTort) {
		this.mestaTort = mestaTort;
	}


	/**
	 * @return the mestaLosning
	 */
	public String getMestaLosning() {
		return mestaLosning;
	}


	/**
	 * @param mestaLosning the mestaLosning to set
	 */
	public void setMestaLosning(String mestaLosning) {
		this.mestaLosning = mestaLosning;
	}


	/**
	 * @return the melding
	 */
	public String getMelding() {
		return melding;
	}


	/**
	 * @param melding the melding to set
	 */
	public void setMelding(String melding) {
		this.melding = melding;
	}


	/**
	 * @return the fraDatoTid
	 */
	public Date getFraDatoTid() {
		return fraDatoTid;
	}


	/**
	 * @param fraDatoTid the fraDatoTid to set
	 */
	public void setFraDatoTid(Date fraDatoTid) {
		this.fraDatoTid = fraDatoTid;
	}


	/**
	 * @return the tilDatoTid
	 */
	public Date getTilDatoTid() {
		return tilDatoTid;
	}


	/**
	 * @param tilDatoTid the tilDatoTid to set
	 */
	public void setTilDatoTid(Date tilDatoTid) {
		this.tilDatoTid = tilDatoTid;
	}


	/**
	 * @return the antallTiltak
	 */
	public int getAntallTiltak() {
		return antallTiltak;
	}


	/**
	 * @param antallTiltak the antallTiltak to set
	 */
	public void setAntallTiltak(int antallTiltak) {
		this.antallTiltak = antallTiltak;
	}


	/**
	 * @return the sumTimer
	 */
	public Double getSumTimer() {
		return sumTimer;
	}


	/**
	 * @param sumTimer the sumTimer to set
	 */
	public void setSumTimer(Double sumTimer) {
		this.sumTimer = sumTimer;
	}


	/**
	 * @return the tonnTortTotalt
	 */
	public Double getTonnTortTotalt() {
		return tonnTortTotalt;
	}


	/**
	 * @param tonnTortTotalt the tonnTortTotalt to set
	 */
	public void setTonnTortTotalt(Double tonnTortTotalt) {
		this.tonnTortTotalt = tonnTortTotalt;
	}


	/**
	 * @return the tonnTortFraLosning
	 */
	public Double getTonnTortFraLosning() {
		return tonnTortFraLosning;
	}


	/**
	 * @param tonnTortFraLosning the tonnTortFraLosning to set
	 */
	public void setTonnTortFraLosning(Double tonnTortFraLosning) {
		this.tonnTortFraLosning = tonnTortFraLosning;
	}


	/**
	 * @return the m3Losning
	 */
	public Double getM3Losning() {
		return m3Losning;
	}


	/**
	 * @param m3Losning the m3Losning to set
	 */
	public void setM3Losning(Double m3Losning) {
		this.m3Losning = m3Losning;
	}


	/**
	 * @return the tonnLosning
	 */
	public Double getTonnLosning() {
		return tonnLosning;
	}


	/**
	 * @param tonnLosning the tonnLosning to set
	 */
	public void setTonnLosning(Double tonnLosning) {
		this.tonnLosning = tonnLosning;
	}	
}
