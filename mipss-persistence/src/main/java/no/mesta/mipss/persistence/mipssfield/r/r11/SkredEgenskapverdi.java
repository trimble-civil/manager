package no.mesta.mipss.persistence.mipssfield.r.r11;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;

/**
 * SkredEgenskapverdi
 *
 * Inneholder en @see Egenskapverdi for et gitt @see skred
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

@SuppressWarnings("serial")
@Entity(name = SkredEgenskapverdi.BEAN_NAME)
@IdClass(SkredEgenskapverdiPK.class)
@Table(name="SKRED_EGENSKAPVERDI_XV")

@NamedQueries( { @NamedQuery(name = SkredEgenskapverdi.QUERY_FIND_BY_SKRED_GUID, query = "SELECT h FROM SkredEgenskapverdi h where h.skredGuid=:skredGuid") })

public class SkredEgenskapverdi extends MipssEntityBean<SkredEgenskapverdi>{

	public static final String BEAN_NAME = "SkredEgenskapverdi";
	public static final String QUERY_FIND_BY_SKRED_GUID = "SkredEgenskapverdi.findBySkredGuid";
	@ManyToOne
    @JoinColumn(name = "VERDI_ID", referencedColumnName = "ID")
	private Egenskapverdi egenskapverdi;
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@ManyToOne
    @JoinColumn(name = "SKRED_GUID", referencedColumnName = "GUID")
	private Skred skred;
	@Id
    @Column(name="SKRED_GUID", nullable = false, insertable = false, updatable = false)
	private String skredGuid;
	@Id
    @Column(name="VERDI_ID", nullable = false, insertable = false, updatable = false)
	private Long verdiId;

	public Egenskapverdi getEgenskapverdi() {
		return this.egenskapverdi;
	}

	public Date getOpprettetDato() {
		return this.opprettetDato;
	}
	public Skred getSkred() {
		return this.skred;
	}
	public String getSkredGuid() {
		return this.skredGuid;
	}
	public Long getVerdiId() {
		return this.verdiId;
	}

	public void setEgenskapverdi(final Egenskapverdi egenskapverdi) {
		this.egenskapverdi = egenskapverdi;

        if(egenskapverdi != null) {
        	setVerdiId(egenskapverdi.getId());
        } else {
        	setVerdiId(null);
        }
	}

	public void setOpprettetDato(final Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	public void setSkred(final Skred skred) {
		this.skred = skred;
		if (skred!=null){
			setSkredGuid(skred.getGuid());
		}else
			setSkredGuid(null);
	}

	public void setSkredGuid(final String skredGuid) {
		this.skredGuid = skredGuid;
	}
	public void setVerdiId(final Long verdiId) {
		this.verdiId = verdiId;
	}



}
