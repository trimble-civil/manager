package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * PK klasse for punktreg Ressurser
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PunktregressursPK implements Serializable {
	private String punktregGuid;
	private Long ressurstypeId;

	/**
	 * Konstruktør
	 * 
	 */
	public PunktregressursPK() {
	}

	/**
	 * Konstruktør
	 * 
	 * @param punktregGuid
	 * @param ressurstypeId
	 */
	public PunktregressursPK(String punktregGuid, Long ressurstypeId) {
		this.punktregGuid = punktregGuid;
		this.ressurstypeId = ressurstypeId;
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 * @see org.apache.commons.lang.builder.EqualsBuilder
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof PunktregressursPK)) {
			return false;
		}

		PunktregressursPK other = (PunktregressursPK) o;

		return new EqualsBuilder().append(punktregGuid, other.getPunktregGuid()).append(ressurstypeId,
				other.getEstimateUnitId()).isEquals();
	}

	/**
	 * Ressurstype
	 * 
	 * @return
	 */
	public Long getEstimateUnitId() {
		return ressurstypeId;
	}

	/**
	 * Punktreg
	 * 
	 * @return
	 */
	public String getPunktregGuid() {
		return punktregGuid;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 * @see org.apache.commons.lang.builder.HashCodeBuilder
	 * 
	 * @return
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(2069, 2031).append(punktregGuid).append(ressurstypeId).toHashCode();
	}

	/**
	 * Ressurstype
	 * 
	 * @param ressurstypeId
	 */
	public void setEstimateUnitId(Long ressurstypeId) {
		this.ressurstypeId = ressurstypeId;
	}

	/**
	 * Punktreg
	 * 
	 * @param punktregGuid
	 */
	public void setPunktregGuid(String punktregGuid) {
		this.punktregGuid = punktregGuid;
	}

	/**
	 * @see java.lang.Object#toString()
	 * @see org.apache.commons.lang.builder.ToStringBuilder
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("punktregGuid", punktregGuid).append("ressurstypeId", ressurstypeId)
				.toString();
	}
}
