package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class LevProdtypeRodePK implements Serializable {
	private String leverandorNr;
	private Long rodeId;
	private Long prodtypeId;
	private Timestamp fraDato;
	
	public LevProdtypeRodePK() {
	}
	
	public LevProdtypeRodePK(String leverandorNr, Long rodeId, Long prodtypeId, Timestamp fraDato) {
		this.leverandorNr = leverandorNr;
		this.rodeId = rodeId;
		this.prodtypeId = prodtypeId;
		this.fraDato = fraDato;
	}

	public String getLeverandorNr() {
		return leverandorNr;
	}

	public void setLeverandorNr(String leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	public Long getRodeId() {
		return rodeId;
	}

	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}

	public Long getProdtypeId() {
		return prodtypeId;
	}

	public void setProdtypeId(Long prodtypeId) {
		this.prodtypeId = prodtypeId;
	}

	public Timestamp getFraDato() {
		return fraDato;
	}

	public void setFraDato(Timestamp fraDato) {
		this.fraDato = fraDato;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(leverandorNr).append(rodeId).append(prodtypeId).append(fraDato).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof LevProdtypeRodePK)) {return false;}
        
        LevProdtypeRodePK other = (LevProdtypeRodePK) o;
        
        return new EqualsBuilder().append(leverandorNr, other.leverandorNr).append(rodeId, other.rodeId).append(prodtypeId, other.prodtypeId).append(fraDato, other.fraDato).isEquals();
    }
}
