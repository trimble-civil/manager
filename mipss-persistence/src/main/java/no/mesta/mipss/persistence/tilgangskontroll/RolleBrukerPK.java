package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
 @SuppressWarnings("serial")
public class RolleBrukerPK implements Serializable {
     private Long rolleId;
     private String signatur;

     public RolleBrukerPK() {
     }

     public RolleBrukerPK(Long rolleId, String signatur) {
         this.rolleId = rolleId;
         this.signatur = signatur;
     }

     public void setRolleId(Long rolleId) {
         this.rolleId = rolleId;
     }

     public Long getRolleId() {
         return rolleId;
     }

     public void setSignatur(String signatur) {
         this.signatur = signatur;
     }

     public String getSignatur() {
         return signatur;
     }
    
    @Override
    public boolean equals(Object o) {
        if(o instanceof RolleBrukerPK == false) {
            return false;
        }
        
        if(this == o) {
            return true;
        }
        
        RolleBrukerPK otherPK = (RolleBrukerPK) o;

        return new EqualsBuilder().
            append(rolleId, otherPK.getRolleId()).
            append(signatur, otherPK.getSignatur()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(1,3).
            append(rolleId).
            append(signatur).
            toHashCode();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("rolleId", rolleId).
            append("signatur", signatur).
            toString();
    }
}
