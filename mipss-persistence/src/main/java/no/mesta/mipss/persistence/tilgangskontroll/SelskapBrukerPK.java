package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SelskapBrukerPK implements Serializable {
    private String selskapskode;
    private String signatur;

    public SelskapBrukerPK() {
    }

    public SelskapBrukerPK(String selskapskode, String signatur) {
        this.selskapskode = selskapskode;
        this.signatur = signatur;
    }

    public boolean equals(Object other) {
        if (other instanceof SelskapBrukerPK) {
            final SelskapBrukerPK otherSelskapBrukerPK = (SelskapBrukerPK) other;
            final boolean areEqual = 
                (otherSelskapBrukerPK.selskapskode.equals(selskapskode) && otherSelskapBrukerPK.signatur.equals(signatur));
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void setSelskapskode(String selskapskode) {
        this.selskapskode = selskapskode;
    }

    public String getSelskapskode() {
        return selskapskode;
    }

    public void setSignatur(String signatur) {
        this.signatur = signatur;
    }

    public String getSignatur() {
        return signatur;
    }
}
