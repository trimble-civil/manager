package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@Table(name = "LEV_PRODTYPE_RODE")
@IdClass(LevProdtypeRodePK.class)
@NamedQueries({
	@NamedQuery(name = LevProdtypeRode.QUERY_FIND_FOR_LEV_AND_KONTRAKT, query = "SELECT o FROM LevProdtypeRode o LEFT JOIN FETCH o.rode where o.leverandorNr = :leverandorNr and o.rode.kontraktId = :kontraktId"),
	@NamedQuery(name = LevProdtypeRode.QUERY_FIND_FOR_KONTRAKT, query = "SELECT o FROM LevProdtypeRode o LEFT JOIN FETCH o.rode where o.rode.kontraktId = :id"),
    @NamedQuery(name = LevProdtypeRode.QUERY_FIND_BY_RODE, query = "SELECT o FROM LevProdtypeRode o where o.rode.id= :id")
})
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class LevProdtypeRode extends MipssEntityBean<LevProdtypeRode> implements Serializable, IRenderableMipssEntity,IOwnableMipssEntity {
	private static final String[] ID_NAMES = new String[] {"leverandorNr", "prosessId", "rodeId", "fraDato"};
	
	public static final String QUERY_FIND_FOR_LEV_AND_KONTRAKT = "LevProdtypeRode.findForLeverandorAndKontrakt";
	public static final String QUERY_FIND_FOR_KONTRAKT = "LevProdtypeRode.findForKontrakt";
	public static final String QUERY_FIND_BY_RODE= "LevProdtypeRode.findByRode";
	
	private String leverandorNr;
	private Long rodeId;
	private Long prodtypeId;
	private Timestamp fraDato;
	private Timestamp tilDato;

	private Leverandor leverandor;
	private Rode rode;
	private Prodtype prodtype;

	private OwnedMipssEntity ownedMipssEntity;

	public LevProdtypeRode() {
	}
	
	@Id
	@Column(name = "LEVERANDOR_NR", nullable = false, insertable = false, updatable = false)
	public String getLeverandorNr() {
		return leverandorNr;
	}

	public void setLeverandorNr(String leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	@Id
	@Column(name = "RODE_ID", nullable = false, insertable = false, updatable = false)
	public Long getRodeId() {
		return rodeId;
	}
	
	
	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}
	
	@Id
	@Column(name="PRODTYPE_ID", nullable = false, insertable = false, updatable = false)
	public Long getProdtypeId(){
		return prodtypeId; 
	}
	
	public void setProdtypeId(Long prodtypeId){
		this.prodtypeId = prodtypeId;
	}
	
	
	
	@Id
	@Column(name = "FRA_DATO", nullable = false)
	public Timestamp getFraDato() {
		return fraDato;
	}

	public void setFraDato(Timestamp fraDato) {
		this.fraDato = fraDato;
	}

	@Column(name = "TIL_DATO")
	public Timestamp getTilDato() {
		return tilDato;
	}

	public void setTilDato(Timestamp tilDato) {
		this.tilDato = tilDato;
	}

	@ManyToOne
	@JoinColumn(name = "LEVERANDOR_NR", referencedColumnName = "NR", nullable = false)
	public Leverandor getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(Leverandor leverandor) {
		this.leverandor = leverandor;
		
		if(leverandor != null) {
			setLeverandorNr(leverandor.getNr());
		} else {
			setLeverandorNr(null);
		}
	}

	@ManyToOne(cascade={CascadeType.REFRESH})
	@JoinColumn(name = "RODE_ID", referencedColumnName = "ID", nullable = false)
	public Rode getRode() {
		return rode;
	}

	public void setRode(Rode rode) {
		this.rode = rode;
		
		if(rode != null) {
			setRodeId(rode.getId());
		} else {
			setRodeId(null);
		}
	}

	
	@ManyToOne
	@JoinColumn(name="PRODTYPE_ID", referencedColumnName="ID", nullable = false )
	public Prodtype getProdtype(){
		return prodtype;
	}
	
	public void setProdtype(Prodtype prodtype){
		this.prodtype = prodtype;
		if (prodtype!=null){
			setProdtypeId(prodtype.getId());
		}else{
			setProdtypeId(null);
		}
	}
	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public String getTextForGUI() {
		return null;
	}
	
	@Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
	public int hashCode(){
		return new HashCodeBuilder().append(getLeverandorNr()).append(getProdtypeId()).append(getRodeId()).append(getFraDato()).toHashCode();
	}
	public boolean equals(Object o){
		if (o==null)
			return false;
		if (o==this)
			return true;
		if (! (o instanceof LevProdtypeRode))
			return false;
		
		LevProdtypeRode other = (LevProdtypeRode)o;
		return new EqualsBuilder()
			.append(getLeverandorNr(), other.getLeverandorNr())
			.append(getProdtypeId(), other.getProdtypeId())
			.append(getRodeId(), other.getRodeId())
			.append(getFraDato(), other.getFraDato())
			.isEquals();
			
		
	}
	
	public String toString(){
		return new ToStringBuilder(this)
			.append("leverandorNr", getLeverandorNr())
			.append("prodtypeId", getProdtypeId())
			.append("rodeId", getRodeId())
			.append("fraDato", getFraDato())
			.toString();
	}
}
