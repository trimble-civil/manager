package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Tilstand for et punktreg
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Avviktilstand.QUERY_FIND_ALL, query = "SELECT p FROM Avviktilstand p")
    })
public class Avviktilstand extends MipssEntityBean<Avviktilstand> implements Serializable, IRenderableMipssEntity, Comparable<Avviktilstand> {
    public static final String QUERY_FIND_ALL = "Avviktilstand.findAll";
    @Id
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private String navn;
    @Column(name = "VALGBAR_FLAGG")
    private Boolean valgbar;

	@Column(name = "GUI_REKKEFOLGE")
    private Long guiRekkefolge;

    /**
     * Konstruktør
     * 
     */
    public Avviktilstand() {
    }


    public Boolean getValgbar() {
		return valgbar;
	}

	public void setValgbar(Boolean valgbar) {
		Boolean old = this.valgbar;
		this.valgbar = valgbar;
		firePropertyChange("valgbar", old, valgbar);
	}

	public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}

	public void setGuiRekkefolge(Long guiRekkefolge) {
		Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}
    /**
     * Id
     * 
     * @return
     */
    
    public Long getId() {
        return id;
    }

    /**
     * Id
     * 
     * @param id
     */
    public void setId(final Long id) {
    	final Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    /**
     * Navn
     * 
     * @return
     */
    
    public String getNavn() {
        return navn;
    }

    /**
     * Navn
     * 
     * @param navn
     */
    public void setNavn(final String navn) {
    	final String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            toString();
    }

    @Override
    public boolean equals(final Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Avviktilstand)) {return false;}
        
        final Avviktilstand other = (Avviktilstand) o;
        
        return new EqualsBuilder().
            append(id, other.getId()).
            isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2063,2037).append(id).toHashCode();
    }

    @Override
    public String getTextForGUI() {
        return getNavn();
    }

    @Override
	public int compareTo(final Avviktilstand o) {
		if(o == null) {
			return 1;
		}
		
		return new CompareToBuilder().append(navn, o.navn).toComparison();
	}
}
