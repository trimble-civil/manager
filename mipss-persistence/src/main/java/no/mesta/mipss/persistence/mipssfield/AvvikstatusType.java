package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Grunndata, status typer for funn.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( { @NamedQuery(name = AvvikstatusType.QUERY_FIND_ALL, query = "SELECT p FROM AvvikstatusType p") })
@Table(name = "AVVIKSTATUS")
@SequenceGenerator(name = AvvikstatusType.ID_SEQ, sequenceName = "AVVIKSTATUS_ID_SEQ", initialValue = 1, allocationSize = 1)
//TODO gjør om til en ENUM-type
public class AvvikstatusType extends MipssEntityBean<AvvikstatusType> implements Serializable, Comparable<AvvikstatusType>, IRenderableMipssEntity {
	public static final String LUKKET_STATUS = "Lukket";
	public static final String GJENAPNET_STATUS = "Apnet";
	public static final String NY_STATUS = "Ny";
	public static final String QUERY_FIND_ALL = "AvvikstatusType.findAll";
	public static final String ID_SEQ = "avvikstatusTypeIdSeq";
	private Long id;
	private Long guiRekkefolge;
	private String status;
	private String navn;
	private Boolean valgbar;
	private Boolean g2;
	private Boolean g3;

	/**
	 * Konstruktør
	 * 
	 */
	public AvvikstatusType() {
	}
	@Id
	
	public Long getId(){
		return id;
	}
	/**
	 * <p>
	 * AvvikstatusType benytter {@linkplain #getGuiRekkefolge} for å sortere
	 * </p>
	 * 
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * @param o
	 * @return
	 */
	public int compareTo(AvvikstatusType o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(guiRekkefolge, o.guiRekkefolge).toComparison();
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 * @see org.apache.commons.lang.builder.EqualsBuilder
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof AvvikstatusType)) {
			return false;
		}

		AvvikstatusType other = (AvvikstatusType) o;

		return new EqualsBuilder().append(status, other.getStatus()).isEquals();
	}

	/**
	 * Gui rekkefølge
	 * 
	 * @return
	 */
	@Column(name = "GUI_REKKEFOLGE", nullable = false)
	public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Id
	 * 
	 * @return
	 */
	public String getNavn() {
		return navn;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		return (navn == null ? "" : navn);
	}

	/**
	 * @see java.lang.Object#hashCode()
	 * @see org.apache.commons.lang.builder.HashCodeBuilder
	 * 
	 * @return
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(2071, 2029).append(status).toHashCode();
	}

	public void setId(Long id){
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	/**
	 * Gui rekkefølge
	 * 
	 * @param guiRekkefolge
	 */
	public void setGuiRekkefolge(Long guiRekkefolge) {
		Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}

	/**
	 * Id
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		String old = this.status;
		this.status = status;
		firePropertyChange("status", old, status);
	}
	
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}


	public void setValgbar(Boolean valgbar) {
		Boolean old = this.valgbar;
		this.valgbar = valgbar;
		firePropertyChange("valgbar", old, valgbar);
	}

	@Column(name="VALGBAR_FLAGG")
	public Boolean getValgbar() {
		return valgbar;
	}
	
	@Column(name="G2_FLAGG")
	public Boolean getG2() {
		return g2;
	}

	public void setG2(Boolean g2) {
		Boolean old = this.g2;
		this.g2 = g2;
		firePropertyChange("g2", old, g2);
	}

	@Column(name="G3_FLAGG")
	public Boolean getG3() {
		return g3;
	}
	
	public void setG3(Boolean g3) {
		Boolean old = g3;
		this.g3 = g3;
		firePropertyChange("g3", old, g3);
	}

	/**
	 * @see java.lang.Object#toString()
	 * @see org.apache.commons.lang.builder.ToStringBuilder
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("status", status)
		.append("navn", navn)
				.append("guiRekkefolge", guiRekkefolge)
				.append("valgbar", valgbar)
				.append("g2", g2)
				.append("g3", g3)
				.toString();
	}
}
