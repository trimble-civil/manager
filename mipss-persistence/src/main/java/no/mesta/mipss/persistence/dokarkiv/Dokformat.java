package no.mesta.mipss.persistence.dokarkiv;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Dokformat.FIND_ALL, query = "select o from Dokformat o"),
	@NamedQuery(name = Dokformat.FIND_BY_EXT, query = "select o from Dokformat o where o.ext = :ext")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

public class Dokformat extends MipssEntityBean<Dokformat> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity, Comparable<Dokformat> {
	public static final String FIND_ALL = "Dokformat.findAll";
	public static final String FIND_BY_EXT = "Dokformat.findName";
    private String ext = "";
    private String navn = "";
    private String standardProgram = "";
    private List<Dokument> dokumentList;
    private Ikon ikon;
    
    private OwnedMipssEntity ownedMipssEntity;

    public Dokformat() {
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
    	String old = this.ext;
        this.ext = ext;
        firePropertyChange("ext", old, ext);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="STANDARD_PROGRAM", nullable = false)
    public String getStandardProgram() {
        return standardProgram;
    }

    public void setStandardProgram(String standardProgram) {
    	String old = this.standardProgram;
        this.standardProgram = standardProgram;
        firePropertyChange("standardProgram", old, standardProgram);
    }

    @OneToMany(mappedBy = "dokformat")
    public List<Dokument> getDokumentList() {
        return dokumentList;
    }

    public void setDokumentList(List<Dokument> dokumentList) {
        this.dokumentList = dokumentList;
    }

    public Dokument addDokument(Dokument dokument) {
        getDokumentList().add(dokument);
        dokument.setDokformat(this);
        return dokument;
    }

    public Dokument removeDokument(Dokument dokument) {
        getDokumentList().remove(dokument);
        dokument.setDokformat(null);
        return dokument;
    }

    @ManyToOne
    @JoinColumn(name = "IKON_ID", referencedColumnName = "ID")
    public Ikon getIkon() {
        return ikon;
    }

    public void setIkon(Ikon ikon) {
    	Ikon old = this.ikon;
        this.ikon = ikon;
        firePropertyChange("ikon", old, ikon);
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }

    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            append("ext", getExt()).
            append("std program", getStandardProgram()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Dokformat)) {
            return false;
        }
        Dokformat other = (Dokformat) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            append(getExt(), other.getExt()).
            append(getStandardProgram(), other.getStandardProgram()).
            isEquals();
    }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,91).
            append(getNavn()).
            append(getExt()).
            append(getStandardProgram()).
            toHashCode();
    }

   @Transient
   public String getTextForGUI() {
       return getNavn();
   }

   public int compareTo(Dokformat o) {
       return new CompareToBuilder().
           append(getNavn(), o.getNavn()).
           toComparison();
   }

}
