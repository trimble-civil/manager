package no.mesta.mipss.persistence.mipssfield;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import no.mesta.mipss.persistence.kontrakt.Kontraktlogg;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Prosess
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({@NamedQuery(name = Prosess.QUERY_FIND_ALL, query = "SELECT p FROM Prosess p")})
public class Prosess
        extends MipssEntityBean<Prosess> implements Serializable, IRenderableMipssEntity, Comparable<Prosess> {
    public static final String QUERY_FIND_ALL = "Prosess.findAll";

    @OneToMany(mappedBy = "prosess")
    private List<ObjektType> objektTypeList = new ArrayList<ObjektType>();
    @OneToMany(mappedBy = "prosess")
    private List<ObjektAvvikType> objektAvvikTypeList = new ArrayList<ObjektAvvikType>();
    @OneToMany(mappedBy = "prosess")
    private List<ObjektAvvikKategori> objektAvvikKategoriList = new ArrayList<ObjektAvvikKategori>();
    @OneToMany(mappedBy = "prosess")
    private List<ObjektStedTypeAngivelse> objektStedTypeAngivelseList = new ArrayList<ObjektStedTypeAngivelse>();

    private transient PropertyChangeSupport props = new PropertyChangeSupport(this);

    @Column(name = "BIT_NUMMER")
    private Long bitNummer;

    @ManyToOne
    @JoinColumn(name = "EIERPROSESS_ID", referencedColumnName = "ID")
    private Prosess eierprosess;

    @Column(name = "G2_FLAGG")
    private Boolean g2;

    @Column(name = "G3_FLAGG")
    private Boolean g3;

    @Column(name = "GYLDIG_FOR_ARBEID_FLAGG", nullable = false)
    private Boolean gyldigForArbeid;

    @Column(name = "GYLDIG_FOR_INSPEKSJON_FLAGG", nullable = false)
    private Boolean gyldigForInspeksjon;

    @Id
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String navn;
    @Transient
    private List<Prosess> prosesser = new ArrayList<Prosess>();
    @ManyToOne
    @JoinColumn(name = "PROSESSETT_ID", referencedColumnName = "ID")
    private Prosessett prosessett;
    @Column(name = "PROSESSETT_ID", insertable = false, updatable = false)
    private Long prosessettId;
    @Column(name = "PROSESS_KODE", nullable = false)
    private String prosessKode;

    @Transient
    private String prosesString;

    @Column(name = "elrapp_id")
    private Long elrappId;

    public Prosess() {
    }

    public List<ObjektType> getObjektType() {
        return objektTypeList;
    }

    public void setObjektTypeList(List<ObjektType> objektTypeList) {
        Object oldValue = this.objektTypeList;
        this.objektTypeList = objektTypeList;
        props.firePropertyChange("objektTypeList", oldValue, objektTypeList);
    }

    public Long getElrappId() {
        return elrappId;
    }

    public List<ObjektAvvikType> getObjektAvvikType() {
        return objektAvvikTypeList;
    }

    public void setObjektAvvikTypeList(List<ObjektAvvikType> objektTypeAvvikList) {
        Object oldValue = this.objektAvvikTypeList;
        this.objektAvvikTypeList = objektAvvikTypeList;
        props.firePropertyChange("objektAvvikTypeList", oldValue, objektAvvikTypeList);
    }

    public List<ObjektAvvikKategori> getObjektAvvikKategori() {
        return objektAvvikKategoriList;
    }

    public void setObjektAvvikKategoriList(List<ObjektAvvikKategori> objektAvvikKategoriList) {
        Object oldValue = this.objektAvvikKategoriList;
        this.objektAvvikKategoriList = objektAvvikKategoriList;
        props.firePropertyChange("objektAvvikKategoriList", oldValue, objektAvvikTypeList);
    }

    public List<ObjektStedTypeAngivelse> getObjektStedTypeAngivelse() {
        return objektStedTypeAngivelseList;
    }

    public void setObjektStedTypeAngivelseList(List<ObjektStedTypeAngivelse> objektStedTypeAngivelseList) {
        Object oldValue = this.objektStedTypeAngivelseList;
        this.objektStedTypeAngivelseList = objektStedTypeAngivelseList;
        props.firePropertyChange("objektStedTypeAngivelseList", oldValue, objektAvvikTypeList);
    }

    @Override
    public Object clone() {
        final Prosess p = new Prosess();
        p.setId(this.id);
        p.setBitNummer(this.bitNummer);
        if (this.eierprosess != null)
            p.setEierprosess((Prosess) this.eierprosess.clone());
        p.setNavn(this.navn);
        p.setProsessKode(this.prosessKode);
        p.setProsessett(this.prosessett);
        p.setProsessettId(this.prosessettId);
        p.setProsesser(new ArrayList<Prosess>());
        return p;
    }

    /**
     * {@inheritDoc}
     */
    public int compareTo(final Prosess other) {
        if (other == null) {
            return 1;
        }

        return new CompareToBuilder().append(getTextForGUI(), other.getTextForGUI()).toComparison();
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }

        if (o == this) {
            return true;
        }
        if (!(o instanceof Prosess)) {
            return false;
        }
        final Prosess other = (Prosess) o;
        return new EqualsBuilder().append(this.id, other.id).isEquals();
    }

    /**
     * Bit nr
     *
     * @return
     */
    public Long getBitNummer() {
        return this.bitNummer;
    }

    public Prosess getEierprosess() {
        return this.eierprosess;
    }

    public Boolean getG2() {
        return this.g2;
    }

    public Boolean getG3() {
        return this.g3;
    }

    public Boolean getGyldigForArbeid() {
        return this.gyldigForArbeid;
    }

    public Boolean getGyldigForInspeksjon() {
        return this.gyldigForInspeksjon;
    }

    /**
     * Id
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Navn
     *
     * @return
     */
    public String getNavn() {
        return this.navn;
    }

    public List<Prosess> getProsesser() {
        return this.prosesser;
    }

    public Prosessett getProsessett() {
        return this.prosessett;
    }

    public Long getProsessettId() {
        return this.prosessettId;
    }

    /**
     * Kode
     *
     * @return
     */
    public String getProsessKode() {
        return this.prosessKode;
    }

    public String getProsessString() {
        StringBuilder sb = new StringBuilder();
        if (eierprosess != null) {
            sb.append(eierprosess.getProsessKode());
            sb.append(" @ ");
        }
        sb.append(getProsessKode());
        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public String getTextForGUI() {
        String formattedProsessKode = getProsessKode();

        if (formattedProsessKode != null) {
            if (!formattedProsessKode.contains(".")) {
                formattedProsessKode = formattedProsessKode + "  ";
            }

            if (formattedProsessKode.length() < 4) {
                formattedProsessKode = " " + formattedProsessKode;
            }
        } else {
            formattedProsessKode = "";
        }

        return formattedProsessKode + " " + getNavn();
    }

    /**
     * Bit nr
     *
     * @param bitNummer
     */
    public void setBitNummer(final Long bitNummer) {
        final Long old = this.bitNummer;
        this.bitNummer = bitNummer;
        firePropertyChange("bitNummer", old, bitNummer);
    }

    public void setEierprosess(final Prosess eierprosess) {
        final Prosess old = this.eierprosess;
        this.eierprosess = eierprosess;
        firePropertyChange("eierprosess", old, eierprosess);
    }

    public void setG2(final Boolean g2) {
        final Boolean old = this.g2;
        this.g2 = g2;
        firePropertyChange("g2", old, g2);
    }

    public void setG3(final Boolean g3) {
        final Boolean old = g3;
        this.g3 = g3;
        firePropertyChange("g3", old, g3);
    }

    public void setGyldigForArbeid(final Boolean gyldigForArbeid) {
        final Boolean old = this.gyldigForArbeid;
        this.gyldigForArbeid = gyldigForArbeid;
        firePropertyChange("gyldigForArbeid", old, gyldigForArbeid);
    }

    public void setGyldigForInspeksjon(final Boolean gyldigForInspeksjon) {
        final Boolean old = this.gyldigForInspeksjon;
        this.gyldigForInspeksjon = gyldigForInspeksjon;
        firePropertyChange("gyldigForInspeksjon", old, gyldigForInspeksjon);
    }

    /**
     * Id
     *
     * @param id
     */
    public void setId(final Long id) {
        final Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    /**
     * Navn
     *
     * @param navn
     */
    public void setNavn(final String navn) {
        final String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    public void setProsesser(final List<Prosess> prosesser) {
        final List<Prosess> old = this.prosesser;
        this.prosesser = prosesser;
        firePropertyChange("prosesser", old, prosesser);
    }

    public void setProsessett(final Prosessett prosessett) {
        final Object old = this.prosessett;
        this.prosessett = prosessett;

        firePropertyChange("prosessett", old, prosessett);

        if (prosessett != null) {
            setProsessettId(prosessett.getId());
        } else {
            setProsessettId(null);
        }
    }

    public void setProsessettId(final Long prosessettId) {
        final Long old = this.prosessettId;
        this.prosessettId = prosessettId;
        firePropertyChange("prosessettId", old, prosessettId);
    }

    /**
     * Kode
     *
     * @param prosessKode
     */
    public void setProsessKode(final String prosessKode) {
        final String old = this.prosessKode;
        this.prosessKode = prosessKode;
        firePropertyChange("prosessKode", old, prosessKode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", this.id)
                .append("prosessKode", this.prosessKode)
                .append("navn", this.navn)
                .append("bitNummer", this.bitNummer)
                .toString();
    }
}
