package no.mesta.mipss.persistence;

import javax.persistence.PrePersist;

/**
 * 
 * @author jorge
 *
 */
public class OpprettingMonitor<I extends ICreatableMippsEntity> {

	public OpprettingMonitor() {}
	
    /**
     * Setter signaturen og tidsstempel for opprettelsen av en post
     * som inneholder OPPRETTET_AV og OPPRETTET_DATO felt.
     * @param ent
     */
    @PrePersist
    public void monitorOpprett(I ent) {
        MipssEntityCreator creator = ent.getMipssEntityCreator();
        if(creator == null) {
        	creator = new MipssEntityCreator();
        }
        
        creator.setOpprettetAv(creator.getUserSignatur());
        creator.setOpprettetDato(Clock.now());
    }
}
