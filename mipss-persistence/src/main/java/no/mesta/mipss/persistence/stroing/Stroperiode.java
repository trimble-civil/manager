package no.mesta.mipss.persistence.stroing;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IkkeoverlappendePeriode;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Stroperiode.FIND_ALL, query = "select o from Stroperiode o"),
	@NamedQuery(name = Stroperiode.FIND_BY_KJORETOY, query = "select o from Stroperiode o where o.kjoretoyId = :id")
})
@IdClass(StroperiodePK.class)
public class Stroperiode extends MipssEntityBean<Stroperiode> implements IkkeoverlappendePeriode {
	public static final String FIND_ALL = "Stroperiode.findAll";
	public static final String FIND_BY_KJORETOY = "Stroperiode.findByKjoretoy";
	
	private Long kjoretoyId;
	private Date fraDato;
	private Long produktId;
	private Date tilDato;
	private Long gramPrKvm;
	private Long strobreddeCm;
	private String opprettetAv;
	private Date opprettetDato;
	private String endretAv;
	private Date endretDato;
	
	private Kjoretoy kjoretoy;
	private Stroprodukt stroprodukt;

	@Id
	@Column(name = "KJORETOY_ID", nullable = false, insertable = false, updatable = false)
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		Long old = this.kjoretoyId;
		this.kjoretoyId = kjoretoyId;
		
		firePropertyChange("kjoretoyId", old, kjoretoyId);
	}

	@Id
	@Column(name = "FRA_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFraDato() {
		return fraDato;
	}

	public void setFraDato(Date fraDato) {
		Date old = this.fraDato;
		this.fraDato = fraDato;
		
		firePropertyChange("fraDato", old, fraDato);
	}

	@Column(name = "PRODUKT_ID", nullable = false, insertable = false, updatable = false)
	public Long getProduktId() {
		return produktId;
	}

	public void setProduktId(Long produktId) {
		Long old = this.produktId;
		this.produktId = produktId;
		
		firePropertyChange("produktId", old, produktId);
	}

	@Column(name = "TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTilDato() {
		return tilDato;
	}

	public void setTilDato(Date tilDato) {
		Date old = this.tilDato;
		this.tilDato = tilDato;
		
		firePropertyChange("tilDato", old, tilDato);
	}

	@Column(name = "GRAM_PR_KVM", nullable = false)
	public Long getGramPrKvm() {
		return gramPrKvm;
	}

	public void setGramPrKvm(Long gramPrKvm) {
		Long old = this.gramPrKvm;
		this.gramPrKvm = gramPrKvm;
		
		firePropertyChange("gramPrKvm", old, gramPrKvm);
	}

	@Column(name = "STROBREDDE_CM", nullable = false)
	public Long getStrobreddeCm() {
		return strobreddeCm;
	}

	public void setStrobreddeCm(Long strobreddeCm) {
		Long old = this.strobreddeCm;
		this.strobreddeCm = strobreddeCm;
		
		firePropertyChange("strobreddeCm", old, strobreddeCm);
	}

	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	@ManyToOne
	@JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID", nullable = false)
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}

	public void setKjoretoy(Kjoretoy kjoretoy) {
		Kjoretoy old = this.kjoretoy;
		this.kjoretoy = kjoretoy;
		
		if(kjoretoy != null) {
			setKjoretoyId(kjoretoy.getId());
		} else {
			setKjoretoyId(null);
		}
		
		firePropertyChange("kjoretoy", old, kjoretoy);
	}

	@ManyToOne
	@JoinColumn(name = "PRODUKT_ID", referencedColumnName = "ID", nullable = false)
	public Stroprodukt getStroprodukt() {
		return stroprodukt;
	}

	public void setStroprodukt(Stroprodukt stroprodukt) {
		Stroprodukt old = this.stroprodukt;
		this.stroprodukt = stroprodukt;
		
		if(stroprodukt != null) {
			setProduktId(stroprodukt.getId());
		} else {
			setProduktId(null);
		}
		
		firePropertyChange("stroprodukt", old, stroprodukt);
	}
}
