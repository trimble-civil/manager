package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

public class TripReturnCausePK {

    private Long tripId;
    private Date created;
    private String signature;

    public TripReturnCausePK() { }

    public TripReturnCausePK(Long tripId, Date created, String signature) {
        this.tripId = tripId;
        this.created = created;
        this.signature = signature;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

}