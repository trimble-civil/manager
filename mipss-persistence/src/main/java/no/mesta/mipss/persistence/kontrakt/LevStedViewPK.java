package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

@SuppressWarnings("serial")
public class LevStedViewPK implements Serializable{

	private Long levId;
	private Long stedId;

	public LevStedViewPK(){
		
	}
	public LevStedViewPK(Long levId, Long stedId){
		this.levId = levId;
		this.stedId = stedId;
		
	}
	public Long getLevId() {
		return levId;
	}
	public Long getStedId() {
		return stedId;
	}
	public void setLevId(Long levId) {
		this.levId = levId;
	}
	public void setStedId(Long stedId) {
		this.stedId = stedId;
	}
}
