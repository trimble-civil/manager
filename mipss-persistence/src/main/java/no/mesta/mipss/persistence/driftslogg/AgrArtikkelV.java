package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

import javax.persistence.*;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@NamedQueries({
		@NamedQuery(name = AgrArtikkelV.FIND_BY_LEV_KONTRAKT, query =
				"select o from AgrArtikkelV o where o.leverandorNr=:leverandorNr and o.levkontraktIdent=:levkontraktIdent")
})
@SuppressWarnings("serial")
@IdClass(AgrArtikkelVPK.class)
@Entity
@Table(name="AGR_ARTIKKEL_V")
public class AgrArtikkelV extends MipssEntityBean<AgrArtikkelV> implements IRenderableMipssEntity {
	public static final String FIND_BY_LEV_KONTRAKT = "AgrArtikkelV.findByLevKontrakt";
	@Id
	@Column(name="LEVKONTRAKT_IDENT")
	private String levkontraktIdent;
	
	@Column(name="LEVKONTRAKT_BESKRIVELSE")
	private String levkontraktBeskrivelse;
	
	@Column(name="LEVERANDOR_NR")
	private String leverandorNr;
	
	@Column(name="GRUPPE_NAVN")
	private String gruppeNavn;
	
	@Id
	@Column(name="ARTIKKEL_IDENT")
	private String artikkelIdent;
	
	@Column(name="ARTIKKEL_NAVN")
	private String artikkelNavn;
	
	@Column(name="ENHET")
	private String enhet;
	
	@Column(name="PRIS")
	private Double pris;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FRA_DATO")
	private Date fraDato;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIL_DATO")
	private Date tilDato;

	@Column(name="ARTIKKEL_STATUS")
	private String artikkelStatus;

	public AgrArtikkelV() {
	}

	public AgrArtikkelV(String artikkelIdent, String artikkelNavn, String gruppeNavn) {
		this.gruppeNavn = gruppeNavn;
		this.artikkelIdent = artikkelIdent;
		this.artikkelNavn = artikkelNavn;
	}

	public AgrArtikkelV(AgrArtikkelV old) {
		this.levkontraktIdent = old.levkontraktIdent;
		this.levkontraktBeskrivelse = old.levkontraktBeskrivelse;
		this.leverandorNr = old.leverandorNr;
		this.gruppeNavn = old.gruppeNavn;
		this.artikkelIdent = old.artikkelIdent;
		this.artikkelNavn = old.artikkelNavn;
		this.enhet = old.enhet;
		this.pris = old.pris;
		this.fraDato = old.fraDato;
		this.tilDato = old.tilDato;
		this.artikkelStatus = old.artikkelStatus;
	}

	/**
	 * @return the levkontraktIdent
	 */
	public String getLevkontraktIdent() {
		return levkontraktIdent;
	}

	/**
	 * @param levkontraktIdent the levkontraktIdent to set
	 */
	public void setLevkontraktIdent(String levkontraktIdent) {
		this.levkontraktIdent = levkontraktIdent;
	}

	/**
	 * @return the gruppeNavn
	 */
	public String getGruppeNavn() {
		return gruppeNavn;
	}

	/**
	 * @param gruppeNavn the gruppeNavn to set
	 */
	public void setGruppeNavn(String gruppeNavn) {
		this.gruppeNavn = gruppeNavn;
	}

	/**
	 * @return the artikkelIdent
	 */
	public String getArtikkelIdent() {
		return artikkelIdent;
	}

	/**
	 * @param artikkelIdent the artikkelIdent to set
	 */
	public void setArtikkelIdent(String artikkelIdent) {
		this.artikkelIdent = artikkelIdent;
	}

	/**
	 * @return the artikkelNavn
	 */
	public String getArtikkelNavn() {
		return artikkelNavn;
	}

	/**
	 * @param artikkelNavn the artikkelNavn to set
	 */
	public void setArtikkelNavn(String artikkelNavn) {
		this.artikkelNavn = artikkelNavn;
	}

	/**
	 * @return the enhet
	 */
	public String getEnhet() {
		return enhet;
	}

	/**
	 * @param enhet the enhet to set
	 */
	public void setEnhet(String enhet) {
		this.enhet = enhet;
	}

	/**
	 * @return the pris
	 */
	public Double getPris() {
		return pris;
	}

	/**
	 * @param pris the pris to set
	 */
	public void setPris(Double pris) {
		this.pris = pris;
	}

	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}

	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}

	/**
	 * @param tilDato the tilDato to set
	 */
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}

	/**
	 * @return the artikkelStatus
	 */
	public String getArtikkelStatus() {
		return artikkelStatus;
	}

	/**
	 * @param artikkelStatus the artikkelStatus to set
	 */
	public void setArtikkelStatus(String artikkelStatus) {
		this.artikkelStatus = artikkelStatus;
	}

	public void setLevkontraktBeskrivelse(String levkontraktBeskrivelse) {
		this.levkontraktBeskrivelse = levkontraktBeskrivelse;
	}

	public String getLevkontraktBeskrivelse() {
		return levkontraktBeskrivelse;
	}

	public void setLeverandorNr(String leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	public String getLeverandorNr() {
		return leverandorNr;
	}

	@Override
	public String getTextForGUI() {
		return artikkelNavn;
	}

	@Override
	public String toString() {
		return artikkelNavn;
	}

}