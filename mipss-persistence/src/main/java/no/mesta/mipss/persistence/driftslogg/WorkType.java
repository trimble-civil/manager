package no.mesta.mipss.persistence.driftslogg;

import javax.persistence.*;
import java.io.Serializable;

@SequenceGenerator(name = WorkType.ID_SEQ, sequenceName = "WORK_TYPE_ID_SEQ", initialValue = 1, allocationSize = 1)

@Entity
@Table(name="WORK_TYPE")
public class WorkType implements Serializable {

    public static final String ID_SEQ = "workTypeIdSeq";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = WorkType.ID_SEQ)
    private Long id;

    @Column(name = "NAME")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}