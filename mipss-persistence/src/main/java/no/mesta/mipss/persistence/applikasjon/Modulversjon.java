package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Holder den konkrete implementasjonen (jarfil) til en modul
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = "MODULVERSJON_SEQ", sequenceName = "MODULVERSJON_ID_SEQ", allocationSize=1)
@NamedQueries({
    @NamedQuery(name = "Modulversjon.findAll", query = "SELECT o FROM Modulversjon o")
})
public class Modulversjon implements Serializable {
    public static final String QUERY_FIND_ALL = "Modulversjon.findAll";
    
    private String endretAv;
    private Date endretDato;
    private String filnavn;
    private String gjenstandNavn;
    private String gruppeNavn;
    private Long id;
    private byte[] jarfilLob;
    private String opprettetAv;
    private Date opprettetDato;
    private String versjon;
    private List<AppMod> appModList = new ArrayList<AppMod>();
    private Modul modul;
    private Long modulId;
    private Integer size;

    /**
     * Konstrukt�r
     * 
     */
    public Modulversjon() {
    }

    /**
     * Hvem endret
     * 
     * @return
     */
    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    /**
     * Hvem endret
     * 
     * @param endretAv
     */
    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    /**
     * N�r ble den endret
     * 
     * @return
     */
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    /**
     * N�r ble den endret
     * 
     * @param endretDato
     */
    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    /**
     * Filnanvnet
     * 
     * @return
     */
    @Column(nullable = false)
    public String getFilnavn() {
        return filnavn;
    }
    
    /**
     * Filnavnet
     * 
     * @param filnavn
     */
    public void setFilnavn(String filnavn) {
        this.filnavn = filnavn;
    }

    /**
     * Maven artifactId
     * 
     * @return
     */
    @Column(name="GJENSTANDNAVN", nullable = false)
    public String getGjenstandNavn() {
        return gjenstandNavn;
    }

    /**
     * Maven artifactId
     * 
     * @param gjenstandNavn
     */
    public void setGjenstandNavn(String gjenstandNavn) {
        this.gjenstandNavn = gjenstandNavn;
    }

    /**
     * Maven groupId
     * 
     * @return
     */
    @Column(name="GRUPPENAVN", nullable = false)
    public String getGruppeNavn() {
        return gruppeNavn;
    }

    /**
     * Maven groupId
     * 
     * @param gruppeNavn
     */
    public void setGruppeNavn(String gruppeNavn) {
        this.gruppeNavn = gruppeNavn;
    }

    /**
     * Id
     * 
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MODULVERSJON_SEQ")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Filinnhold
     * 
     * @return
     */
    @Column(name="JARFIL_LOB", nullable = false)
    public byte[] getJarfilLob() {
        return jarfilLob;
    }

    /**
     * Filinnhold
     * 
     * @param jarfilLob
     */
    public void setJarfilLob(byte[] jarfilLob) {
        this.jarfilLob = jarfilLob;
    }

    /**
     * Opprettet av
     * 
     * @return
     */
    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    /**
     * Opprettet av
     * 
     * @param opprettetAv
     */
    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    /**
     * N�r den ble laget
     * 
     * @return
     */
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * N�r den ble laget
     * 
     * @param opprettetDato
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Maven version
     * 
     * @return
     */
    @Column(nullable = false)
    public String getVersjon() {
        return versjon;
    }

    /**
     * Maven version
     * 
     * @param versjon
     */
    public void setVersjon(String versjon) {
        this.versjon = versjon;
    }

    /**
     * Liste over applikasjoner som denne inng�r i
     * 
     * @return
     */
    @OneToMany(mappedBy = "modulversjon")
    public List<AppMod> getAppModList() {
        return appModList;
    }

    /**
     * Liste over applikasjoner som denne inng�r i
     * 
     * @param appModList
     */
    public void setAppModList(List<AppMod> appModList) {
        this.appModList = appModList;
    }

    /**
     * Legger til en knytting
     * 
     * @param appMod
     * @return
     */
    public AppMod addAppMod(AppMod appMod) {
        getAppModList().add(appMod);
        appMod.setModulversjon(this);
        return appMod;
    }
    
    /**
     * Fjerner en knytting
     * 
     * @param appMod
     * @return
     */
    public AppMod removeAppMod(AppMod appMod) {
        getAppModList().remove(appMod);
        appMod.setModulversjon(null);
        return appMod;
    }

    /**
     * Den logiske modulen denne er en implementasjon av
     * 
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "MODUL_ID", referencedColumnName = "ID")
    public Modul getModul() {
        return modul;
    }

    /**
     * Den logiske modulen denne er en implementasjon av
     * 
     * @param modul
     */
    public void setModul(Modul modul) {
        this.modul = modul;
        
        if(modul != null) {
            setModulId(modul.getId());
        } else {
            setModulId(null);
        }
    }

    /**
     * Den logiske modulen denne er en implementasjon av
     * 
     * @param modulId
     */
    public void setModulId(Long modulId) {
        this.modulId = modulId;
    }

    /**
     * Den logiske modulen denne er en implementasjon av
     * 
     * @return
     */
    @Column(name="MODUL_ID", nullable = false, insertable = false, 
         updatable = false)
    public Long getModulId() {
        return modulId;
    }
    
    /**
     * St�rrelsen p� jarfilen
     * 
     * @param size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * St�rrelsen p� jarfilen
     * 
     * @return
     */
    @Transient
    public Integer getSize() {
        if(size == null) {
            if(jarfilLob != null) {
                size = jarfilLob.length;
            } else {
                size = 0;
            }
        }
        
        return size;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof Modulversjon)) { return false;}
        if(o == this) { return true;}
        
        Modulversjon other = (Modulversjon) o;
        
        return new EqualsBuilder().
            append(gruppeNavn, other.getGruppeNavn()).
            append(gjenstandNavn, other.getGjenstandNavn()).
            append(versjon, other.getVersjon()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(13,987).
            append(gruppeNavn).
            append(gjenstandNavn).
            append(versjon).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        Long modulId = modul == null ? null : modul.getId();
        return new ToStringBuilder(this).
            append("id", id).
            append("modulId", modulId).
            append("groupId", gruppeNavn).
            append("artifactID", gjenstandNavn).
            append("versjon", versjon).
            append("filnavn", filnavn).
            toString();
    }
}
