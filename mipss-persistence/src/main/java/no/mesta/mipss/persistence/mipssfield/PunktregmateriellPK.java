package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * PK klasse for punktreg materiell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PunktregmateriellPK implements Serializable {
	public Long materielltypeId;
	public String punktregGuid;

	/**
	 * Konstruktør
	 * 
	 */
	public PunktregmateriellPK() {
	}

	/**
	 * Konstruktør
	 * 
	 * @param materielltypeId
	 * @param punktregGuid
	 */
	public PunktregmateriellPK(Long materielltypeId, String punktregGuid) {
		this.materielltypeId = materielltypeId;
		this.punktregGuid = punktregGuid;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof PunktregmateriellPK)) {
			return false;
		}

		PunktregmateriellPK other = (PunktregmateriellPK) o;

		return new EqualsBuilder().append(punktregGuid, other.getPunktregGuid()).append(materielltypeId,
				other.getEstimateUnitId()).isEquals();
	}

	public Long getEstimateUnitId() {
		return materielltypeId;
	}

	public String getPunktregGuid() {
		return punktregGuid;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(punktregGuid).append(materielltypeId).toHashCode();
	}

	public void setEstimateUnitId(Long materielltypeId) {
		this.materielltypeId = materielltypeId;
	}

	public void setPunktregGuid(String punktregGuid) {
		this.punktregGuid = punktregGuid;
	}
}
