package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "dfuloggIdSeq", sequenceName = "DFULOGG_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQuery(name = "Dfulogg.findAll", query = "select o from Dfulogg o")
public class Dfulogg implements Serializable, IRenderableMipssEntity {
    private String fritekst;
    private Long id;
    private String opprettetAv;
    private Date opprettetDato;
    private Dfuindivid dfuindivid;

    public Dfulogg() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dfuloggIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @ManyToOne
    @JoinColumn(name = "INDIVID_ID", referencedColumnName = "ID")
    public Dfuindivid getDfuindivid() {
        return dfuindivid;
    }

    public void setDfuindivid(Dfuindivid dfuindivid) {
        this.dfuindivid = dfuindivid;
    }
    
    // de neste metodene er håndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("dfu id", getDfuindivid()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Dfulogg)) {
            return false;
        }
        Dfulogg other = (Dfulogg) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getFritekst().substring(16);
    }

    public int compareTo(Object o) {
        Dfulogg other = (Dfulogg) o;
            return new CompareToBuilder().
                append(getOpprettetDato(), other.getOpprettetDato()).
                toComparison();
    }
    
    @Transient
    public String getStringForLoggPanel() {
		StringBuffer sb = new StringBuffer();
    	
    	sb.append(getOpprettetDato());
		sb.append("\n");
		sb.append(getOpprettetAv());
		sb.append("\n");
		sb.append(getFritekst());
		
		return sb.toString();
    }
}
