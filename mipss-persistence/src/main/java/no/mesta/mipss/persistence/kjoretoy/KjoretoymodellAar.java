/**
 * 
 */
package no.mesta.mipss.persistence.kjoretoy;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Implemeterer �ret for et Kjoretoymodell.
 * En del av strukturen modellnavn - �r - Kjoretoymodell.
 * 
 * @author jorge
 * 
 */
@SuppressWarnings("serial")
public class KjoretoymodellAar implements IRenderableMipssEntity, Comparable<KjoretoymodellAar> {
	private Long aar;
	private Kjoretoymodell kjoretoymodell;

	public KjoretoymodellAar(Kjoretoymodell modell) {
		if (modell.getAar() == null) {
			aar = Long.valueOf(0);
		} else {
			aar = modell.getAar();
		}
		kjoretoymodell = modell;
	}

	public String getTextForGUI() {
		if (aar.equals(Long.valueOf(0))) {
			return "Udef";
		} else {
			return String.valueOf(aar);
		}
	}

	/**
	 * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
	 * 
	 * @return en tekstlinje.
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("aar", aar).toString();
	}

	/**
	 * Sammenlikner med o.
	 * 
	 * @param o
	 * @return true hvis relevante n�klene er like.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof KjoretoymodellAar)) {
			return false;
		}
		KjoretoymodellAar other = (KjoretoymodellAar) o;
		return new EqualsBuilder().append(getAar(), other.getAar()).append(
				getKjoretoymodell().getId(), other.getKjoretoymodell().getId())
				.isEquals();
	}

	/**
	 * Genererer identifikator hashkode.
	 * 
	 * @return koden.
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(23, 91).append(getKjoretoymodell())
				.toHashCode();
	}

	public int compareTo(KjoretoymodellAar other) {
		return new CompareToBuilder().append(getAar(), other.getAar()).toComparison();
	}

	/**
	 * @return aar
	 */
	public Long getAar() {
		return aar;
	}

	/**
	 * @param aar
	 *            the aar to set
	 */
	public void setAar(Long aar) {
		this.aar = aar;
	}

	/**
	 * @return kjoretoymodell
	 */
	public Kjoretoymodell getKjoretoymodell() {
		return kjoretoymodell;
	}

	/**
	 * @param kjoretoymodell
	 *            the kjoretoymodell to set
	 */
	public void setKjoretoymodell(Kjoretoymodell kjoretoymodell) {
		this.kjoretoymodell = kjoretoymodell;
	}
}
