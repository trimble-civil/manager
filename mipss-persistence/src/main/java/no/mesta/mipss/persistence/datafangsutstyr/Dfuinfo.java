package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
@Entity
public class Dfuinfo extends MipssEntityBean<Dfuinfo> implements Serializable {
	private static final String[] ID_FIELDS = {"dfuId"};
	private Long dfuId;
	
	private Date sisteLivstegnDato;
	private Date sistePosGmtTidspunkt;
	private Dfuindivid dfuindivid;
	
	@Id
	@Column(nullable = false, name="DFU_ID", insertable = false, updatable = false)
	public Long getDfuId() {
		return dfuId;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="SISTE_LIVSTEGN_DATO")
	public Date getSisteLivstegnDato(){
		return sisteLivstegnDato;
	}
	
	@Transient
	public String getSisteLivstegnDatoString(){
		return MipssDateFormatter.formatDate(sisteLivstegnDato, true);
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SISTE_POS_GMT_TIDSPUNKT")
	public Date getSistePosGmtTidspunkt(){
		return sistePosGmtTidspunkt;
	}
	
	@OneToOne
	@JoinColumn(name = "DFU_ID", referencedColumnName = "ID")
	public Dfuindivid getDfuindivid(){
		return dfuindivid;
	}
	
	public void setDfuindivid(Dfuindivid dfuindivid){
		this.dfuindivid = dfuindivid;
		
		if(dfuindivid != null) {
			dfuId = dfuindivid.getId();
		} else {
			dfuId = null;
		}
	}
	
	
	public void setDfuId(Long dfuId){
		this.dfuId = dfuId;
	}

	public void setSisteLivstegnDato(Date sisteLivstegnDato) {
		this.sisteLivstegnDato = sisteLivstegnDato;
	}

	public void setSistePosGmtTidspunkt(Date sistePosGmtTidspunkt) {
		this.sistePosGmtTidspunkt = sistePosGmtTidspunkt;
	}


	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(1, 911).append(getDfuId()).toHashCode();
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof Dfuinfo)) {
			return false;
		}
		Dfuinfo other = (Dfuinfo) o;
		return new EqualsBuilder().append(getDfuId(), other.getDfuId()).isEquals();
	}
	
	@Override
	public String[] getIdNames() {
		return ID_FIELDS;
	}
}
