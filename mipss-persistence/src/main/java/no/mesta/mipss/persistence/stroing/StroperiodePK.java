package no.mesta.mipss.persistence.stroing;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class StroperiodePK implements Serializable {
	private Long kjoretoyId;
	private Date fraDato;
	
	public StroperiodePK() {
	}
	
	public StroperiodePK(Long kjoretoyId, Date fraDato) {
		this.kjoretoyId = kjoretoyId;
		this.fraDato = fraDato;
	}

	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	
	public Date getFraDato() {
		return fraDato;
	}
	
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kjoretoyId).append(fraDato).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof StroperiodePK)) {return false;}
        
        StroperiodePK other = (StroperiodePK) o;
        
        return new EqualsBuilder().append(kjoretoyId, other.kjoretoyId).append(fraDato, other.fraDato).isEquals();
    }
}
