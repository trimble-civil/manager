package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;


import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
    
@Table(name = "KJORETOY_BILDE")
@IdClass(KjoretoyBildePK.class)

@NamedQueries({
	@NamedQuery(name = "KjoretoyBilde.findAll", query = "select o from KjoretoyBilde o"),
    @NamedQuery(name = KjoretoyBilde.QUERY_BILDER_FOR_KJORETOY, query = "select kb from KjoretoyBilde kb where kb.kjoretoyId = :kjoretoyId")
})

public class KjoretoyBilde implements Serializable, IRenderableMipssEntity {
    public static final String QUERY_BILDER_FOR_KJORETOY = "KjoretoyBilde.getForKjoretoy";
    
    private String bildeGuid;
    private Long kjoretoyId;
    private Kjoretoy kjoretoy;

    public KjoretoyBilde() {
    }

    @Id
    @Column(name="BILDE_GUID", nullable = false)
    public String getBildeGuid() {
        return bildeGuid;
    }

    public void setBildeGuid(String bildeGuid) {
        this.bildeGuid = bildeGuid;
    }

    @Id
    @Column(name="KJORETOY_ID", nullable = false, insertable = false, 
        updatable = false)
    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
        this.kjoretoy = kjoretoy;
    }
    
    // de neste metodene er håndkodet
     /**
      * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         return new ToStringBuilder(this).
             append("kjoretoyId", getKjoretoyId()).
             append("bildeGuid", getBildeGuid()).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante nøklene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof KjoretoyBilde)) {
             return false;
         }
         KjoretoyBilde other = (KjoretoyBilde) o;
         return new EqualsBuilder().
             append(getKjoretoyId(), other.getKjoretoyId()).
             append(getBildeGuid(), other.getBildeGuid()).
             isEquals();
     }

     @Override
     public int hashCode() {
         return new HashCodeBuilder(7,91).
             append(getKjoretoyId()).
             append(getBildeGuid()).
             toHashCode();
     }

    @Transient
    public String getTextForGUI() {
        //TODO: bedre listbar tekst kommer fra bildeobjekttet. Kun hvis nødvendig
        return getKjoretoyId() + " - " + getBildeGuid();
    }

    public int compareTo(Object o) {
        KjoretoyBilde other = (KjoretoyBilde) o;
        return new CompareToBuilder().
            append(getBildeGuid(), other.getBildeGuid()).
            append(getKjoretoyId(), other.getKjoretoyId()).
            toComparison();
    }
}
