package no.mesta.mipss.persistence.datafangsutstyrbestilling;

public interface MipssDfuvare {
	Dfuvarekatalog getDfuvarekatalog();
	Long getAntall();
	void setAntall(Long antall);
}
