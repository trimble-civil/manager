package no.mesta.mipss.persistence.mipssfield;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@NamedQueries ({
	@NamedQuery(name = Pdabruker.FIND_ALL, query = "select o from PdabrukerKontrakt o"),
	@NamedQuery(name = Pdabruker.QUERY_FIND_BY_NAVN, query = "SELECT o FROM Pdabruker o where LOWER(o.navn) like :navn")
})
public class Pdabruker extends MipssEntityBean<Pdabruker> {
	public static final String FIND_ALL = "pdabruker.findAll";
	public static final String QUERY_FIND_BY_NAVN = "pdabruker.findByNavn";
	
	@Id
	@Column(nullable = false)
	private String signatur;
	
	private String navn;
	private Long tlfnummer;
	@Column(name="EPOST_ADRESSE")
	private String epostAdresse;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	

	/**
	 * @return the signatur
	 */
	public String getSignatur() {
		return signatur;
	}
	/**
	 * @param signatur the signatur to set
	 */
	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}
	/**
	 * @return the navn
	 */
	public String getNavn() {
		return navn;
	}
	/**
	 * @param navn the navn to set
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}
	/**
	 * @return the tlfnummer
	 */
	public Long getTlfnummer() {
		return tlfnummer;
	}
	/**
	 * @param tlfnummer the tlfnummer to set
	 */
	public void setTlfnummer(Long tlfnummer) {
		this.tlfnummer = tlfnummer;
	}
	/**
	 * @return the epostAdresse
	 */
	public String getEpostAdresse() {
		return epostAdresse;
	}
	/**
	 * @param epostAdresse the epostAdresse to set
	 */
	public void setEpostAdresse(String epostAdresse) {
		this.epostAdresse = epostAdresse;
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
}
