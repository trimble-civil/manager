package no.mesta.mipss.persistence.inndata;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Prodpunktdetalj
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Prodpunktdetalj.QUERY_FIND_ALL, query = "select o from Prodpunktdetalj o")
@IdClass(ProdpunktdetaljPK.class)
public class Prodpunktdetalj implements Serializable {

	public static final String QUERY_FIND_ALL = "Produpunktdetalj.findAll";

	private String detaljtype;
	private Long dfuId;
	private Date gmtTidspunkt;
	private String innstreng;
	private Prodpunkt prodpunkt;

	public Prodpunktdetalj() {
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 * @see org.apache.commons.lang.builder.EqualsBuilder
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Prodpunktdetalj)) {
			return false;
		}

		Prodpunktdetalj other = (Prodpunktdetalj) o;
		return new EqualsBuilder().append(dfuId, other.getDfuId()).append(gmtTidspunkt, other.getGmtTidspunkt())
				.isEquals();
	}

	@Column(nullable = false)
	public String getDetaljtype() {
		return detaljtype;
	}

	@Id
	@Column(name = "DFU_ID", nullable = false, insertable = false, updatable = false)
	public Long getDfuId() {
		return dfuId;
	}

	@Id
	@Column(name = "GMT_TIDSPUNKT", nullable = false, insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getGmtTidspunkt() {
		return gmtTidspunkt;
	}

	@Column(nullable = false)
	public String getInnstreng() {
		return innstreng;
	}

	@OneToOne
	@JoinColumns( { @JoinColumn(name = "DFU_ID", referencedColumnName = "DFU_ID"),
			@JoinColumn(name = "GMT_TIDSPUNKT", referencedColumnName = "GMT_TIDSPUNKT") })
	public Prodpunkt getProdpunkt() {
		return prodpunkt;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 * @see org.apache.commons.lang.builder.HashCodeBuilder
	 * 
	 * @return
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(791, 801).append(dfuId).append(gmtTidspunkt).toHashCode();
	}

	public void setDetaljtype(String detaljtype) {
		this.detaljtype = detaljtype;
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}

	public void setGmtTidspunkt(Date gmtTidspunkt) {
		this.gmtTidspunkt = gmtTidspunkt;
	}

	public void setInnstreng(String innstreng) {
		this.innstreng = innstreng;
	}

	public void setProdpunkt(Prodpunkt prodpunkt) {
		this.prodpunkt = prodpunkt;
	}

	/**
	 * @see java.lang.Object#toString()
	 * @see org.apache.commons.lang.builder.ToStringBuilder
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("dfuId", dfuId).append("gmtTidspunkt", gmtTidspunkt).append(
				"detaljtype", detaljtype).append("innstreng", innstreng).toString();
	}
}
