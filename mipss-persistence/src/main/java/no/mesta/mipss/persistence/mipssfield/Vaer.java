package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Typetabell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Vaer.QUERY_FIND_ALL, query = "SELECT v FROM Vaer v")
    })
public class Vaer implements Serializable, IRenderableMipssEntity, Comparable<Vaer> {
    public static final String QUERY_FIND_ALL = "Vaer.findAll";
    
    private Long id;
    private String navn;

    /**
     * Konstrukt�r
     * 
     */
    public Vaer() {
    }

    /** {@inheritDoc} */
    public int compareTo(Vaer o) {
        if(o == null) {return 1;}
        
        return new CompareToBuilder().append(navn, o.navn).toComparison();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Vaer)) {return false;}
        
        Vaer other = (Vaer) o;
        
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

    /**
     * Id
     * 
     * @return
     */
    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Navn
     * 
     * @return
     */
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    /** {@inheritDoc} */
    public String getTextForGUI() {
        return (navn == null ? "" : navn);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(4005, 5095).append(id).toHashCode();
    }
    
    /**
     * Id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Navn
     * 
     * @param navn
     */
    public void setNavn(String navn) {
        this.navn = navn;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            toString();
    }
}
