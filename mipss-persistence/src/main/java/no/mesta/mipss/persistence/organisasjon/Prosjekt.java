package no.mesta.mipss.persistence.organisasjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/*@Entity
@NamedQueries ({
	@NamedQuery(name = Prosjekt.FIND_ALL, query = "select o from Prosjekt o order by o.navn")
})

@SequenceGenerator(name = "prosjektIdSeq", sequenceName = "PROSJEKT_ID_SEQ", initialValue = 1, allocationSize = 1)

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})*/

@SuppressWarnings("serial")
public class Prosjekt implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "Prosjekt.findAll";

    private Long id;
    private String navn;
    private List<KontraktProsjekt> prosjektKontraktList = new ArrayList<KontraktProsjekt>();
    private Ansvarssted ansvarssted;
    private OwnedMipssEntity ownedMipssEntity;
    private List<Driftkontrakt> driftkontraktList = new ArrayList<Driftkontrakt>();
    
    public Prosjekt() {
    }

    /*@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prosjektIdSeq")
    @Column(nullable = false)*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    /*@OneToMany(mappedBy = "prosjekt")*/
    public List<KontraktProsjekt> getProsjektKontraktList() {
        return prosjektKontraktList;
    }

    public void setProsjektKontraktList(List<KontraktProsjekt> prosjektKontraktList) {
        this.prosjektKontraktList = prosjektKontraktList;
    }

    /*public ProsjektKontrakt addProsjektKontrakt(ProsjektKontrakt prosjektKontrakt) {
        getProsjektKontraktList().add(prosjektKontrakt);
        prosjektKontrakt.setProsjekt(this);
        return prosjektKontrakt;
    }

    public ProsjektKontrakt removeProsjektKontrakt(ProsjektKontrakt prosjektKontrakt) {
        getProsjektKontraktList().remove(prosjektKontrakt);
        prosjektKontrakt.setProsjekt(null);
        return prosjektKontrakt;
    }*/

    /*@ManyToOne
    @JoinColumn(name = "ANSVARSSTED_ID", referencedColumnName = "ID")*/
    public Ansvarssted getAnsvarssted() {
        return ansvarssted;
    }

    public void setAnsvarssted(Ansvarssted ansvarssted) {
        this.ansvarssted = ansvarssted;
    }
    
    //@Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }

	/**
	 * @return driftkontraktList
	 
    @ManyToMany
    @JoinTable(name = "PROSJEKT_KONTRAKT",
    		joinColumns = {@JoinColumn(name = "PROSJEKT_ID", referencedColumnName = "ID")},
    		inverseJoinColumns = {@JoinColumn(name = "kontrakt_id", referencedColumnName = "ID")})*/
	public List<Driftkontrakt> getDriftkontraktList() {
		return driftkontraktList;
	}

	/**
	 * @param driftkontraktList the driftkontraktList to set
	 */
	public void setDriftkontraktList(List<Driftkontrakt> driftkontraktList) {
		this.driftkontraktList = driftkontraktList;
	}
    
	/**
     * Genererer en lesbar tekst med hovedlementene i entitybønnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("Id", getId()).
            append("navn", getNavn()).
            toString();
    }

    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Prosjekt)) {
            return false;
        }
        Prosjekt other = (Prosjekt) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun på Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getId()).
            append(getNavn()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	Prosjekt other = (Prosjekt) o;
		return new CompareToBuilder().
			append(getNavn(), other.getNavn()).
			toComparison();
    }

    /** {@inheritDoc} */
	public String getTextForGUI() {
		return getNavn();
	}

}
