package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * JPA entitet generert i JDeveloper.
 * Grunndata for ordrestatus.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Kjoretoyordrestatus.FIND_ALL, query = "select o from Kjoretoyordrestatus o order by o.guiRekkefolge"),
	@NamedQuery(name = Kjoretoyordrestatus.FIND_ALL_VALID, query = "select o from Kjoretoyordrestatus o where o.valgbarFlagg = 1 order by o.guiRekkefolge")
})

public class Kjoretoyordrestatus extends MipssEntityBean<Kjoretoyordrestatus> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kjoretoyordrestatus.findAll";
	public static final String FIND_ALL_VALID = "Kjoretoyordrestatus.findAllValid";
    private String ident;
    private String navn;
	private String beskrivelse;
    private Boolean valgbarFlagg;
    private Boolean kreverLoggFlagg;
    private Long guiRekkefolge;
    private List<Kjoretoyordre_Status> kjoretoyordre_StatusList;

    public Kjoretoyordrestatus() {
    }

    @Id
    @Column(nullable = false)
    public String getIdent() {
		return ident;
	}

	public void setIdent(String ident) {
		this.ident = ident;
	}

	public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Column(name="KREVER_LOGG_FLAGG", nullable = false)
    public Boolean getKreverLoggFlagg() {
    	if(kreverLoggFlagg == null) {
    		kreverLoggFlagg = false;
    	}
        return kreverLoggFlagg;
    }

    public void setKreverLoggFlagg(Boolean kreverLoggFlagg) {
    	Boolean old = this.kreverLoggFlagg;
        this.kreverLoggFlagg = kreverLoggFlagg;
        firePropertyChange("kreverLoggFlagg", old, kreverLoggFlagg);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
    	if(valgbarFlagg == null) {
    		valgbarFlagg = false;
    	}
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }

    @Column(name="GUI_REKKEFOLGE", nullable = false)
    public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}

	public void setGuiRekkefolge(Long guiRekkefolge) {
		this.guiRekkefolge = guiRekkefolge;
	}

	@OneToMany(mappedBy = "kjoretoyordrestatus")
    public List<Kjoretoyordre_Status> getKjoretoyordre_StatusList() {
        return kjoretoyordre_StatusList;
    }

    public void setKjoretoyordre_StatusList(List<Kjoretoyordre_Status> kjoretoyordre_StatusList) {
        this.kjoretoyordre_StatusList = kjoretoyordre_StatusList;
    }

    public Kjoretoyordre_Status addKjoretoyordre_Status(Kjoretoyordre_Status kjoretoyordre_Status) {
        getKjoretoyordre_StatusList().add(kjoretoyordre_Status);
        kjoretoyordre_Status.setKjoretoyordrestatus(this);
        return kjoretoyordre_Status;
    }

    public Kjoretoyordre_Status removeKjoretoyordre_Status(Kjoretoyordre_Status kjoretoyordre_Status) {
        getKjoretoyordre_StatusList().remove(kjoretoyordre_Status);
        kjoretoyordre_Status.setKjoretoyordrestatus(null);
        return kjoretoyordre_Status;
    }

    /** {@inheritDoc} */
    @Transient
	public String getTextForGUI() {
		return getNavn();
	}
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
        	append("ident", getIdent()).    
        	append("navn", getNavn()).
            append("valgbar", getValgbarFlagg()).
            append("krever logg", getKreverLoggFlagg()).
            toString();
    }

    /**
     * Sammenlikner Id-n�kkelen med o's Id-n�kkel.
     * @param o
     * @return true hvis n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kjoretoyordrestatus)) {
            return false;
        }
        Kjoretoyordrestatus other = (Kjoretoyordrestatus) o;
        return new EqualsBuilder().
            append(getIdent(), other.getIdent()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert kun p� Id.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getIdent()).
            toHashCode();
    }

	/**
	* Sorteringskriterier
	*/
    public int compareTo(Object o) {
    	Kjoretoyordrestatus other = (Kjoretoyordrestatus) o;
		return new CompareToBuilder().
			append(getIdent(), other.getIdent()).
			toComparison();
    }
}
