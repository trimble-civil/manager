package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

public class Vinterdrift implements Serializable {

	private int kjoretoyId;
	private int rodeId;
	private String rodenavn;
	private String vinterdriftklasse;
	private int fylke;
	private String vegtype;
	private Date fraDatoRodestrekning;
	private Date tilDatoRodestrekning;
	private Double km;
	private Double kmProdstrekning;
	private int prodtypeId;
	private int utstyrId;
	private int stroproduktId;
	private Boolean stroFlagg;
	private Boolean broyteFlagg;
	private Long tiltakId;

	private String leverandor;
	private Double broytingKm;
	private Double sideplogKm;
	private Double tungHovlingKm;
	private Double tungHovlingTimer;
	private Double stroingTimer;
	private Double torrsaltTonn;
	private Double slurryTonn;
	private Double saltlosningKubikk;
	private Double befuktetSaltTonn;
	private Double sandSaltTonn;
	private Double renSandTonn;
	private Double fastsandTonn;

	public static String[] getPropertiesArray() {
		return new String[] {
				"kjoretoyId",
				"rodeId",
				"rodenavn",
				"vinterdriftklasse",
				"fylke",
				"vegtype",
				"fraDatoRodestrekning",
				"tilDatoRodestrekning",
				"km",
				"kmProdstrekning",
				"prodtypeId",
				"utstyrId",
				"stroproduktId",
				"stroFlagg",
				"broyteFlagg",
				"tiltakId"
		};
	}

	public int getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(int kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public int getRodeId() {
		return rodeId;
	}

	public void setRodeId(int rodeId) {
		this.rodeId = rodeId;
	}

	public String getRodenavn() {
		return rodenavn;
	}

	public void setRodenavn(String rodenavn) {
		this.rodenavn = rodenavn;
	}

	public String getVinterdriftklasse() {
		return vinterdriftklasse;
	}

	public void setVinterdriftklasse(String vinterdriftklasse) {
		this.vinterdriftklasse = vinterdriftklasse;
	}

	public int getFylke() {
		return fylke;
	}

	public void setFylke(int fylke) {
		this.fylke = fylke;
	}

	public String getVegtype() {
		return vegtype;
	}

	public void setVegtype(String vegtype) {
		this.vegtype = vegtype;
	}

	public Date getFraDatoRodestrekning() {
		return fraDatoRodestrekning;
	}

	public void setFraDatoRodestrekning(Date fraDatoRodestrekning) {
		this.fraDatoRodestrekning = fraDatoRodestrekning;
	}

	public Date getTilDatoRodestrekning() {
		return tilDatoRodestrekning;
	}

	public void setTilDatoRodestrekning(Date tilDatoRodestrekning) {
		this.tilDatoRodestrekning = tilDatoRodestrekning;
	}

	public Double getKm() {
		return km;
	}

	public void setKm(Double km) {
		this.km = km;
	}

	public Double getKmProdstrekning() {
		return kmProdstrekning;
	}

	public void setKmProdstrekning(Double kmProdstrekning) {
		this.kmProdstrekning = kmProdstrekning;
	}

	public int getProdtypeId() {
		return prodtypeId;
	}

	public void setProdtypeId(int prodtypeId) {
		this.prodtypeId = prodtypeId;
	}

	public int getUtstyrId() {
		return utstyrId;
	}

	public void setUtstyrId(int utstyrId) {
		this.utstyrId = utstyrId;
	}

	public int getStroproduktId() {
		return stroproduktId;
	}

	public void setStroproduktId(int stroproduktId) {
		this.stroproduktId = stroproduktId;
	}

	public Boolean getStroFlagg() {
		return stroFlagg;
	}

	public void setStroFlagg(Boolean stroFlagg) {
		this.stroFlagg = stroFlagg;
	}

	public Boolean getBroyteFlagg() {
		return broyteFlagg;
	}

	public Long getTiltakId() {
		return tiltakId;
	}

	public void setTiltakId(Long tiltakId) {
		this.tiltakId = tiltakId;
	}

	public String getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}

	public void setBroyteFlagg(Boolean broyteFlagg) {
		this.broyteFlagg = broyteFlagg;
	}

	public Double getBroytingKm() {
		return broytingKm;
	}

	public void addBroytingKm(Double broytingKm) {
		if (this.broytingKm != null) {
			this.broytingKm += broytingKm;
		} else {
			this.broytingKm = broytingKm;
		}
	}

	public Double getSideplogKm() {
		return sideplogKm;
	}

	public void addSideplogKm(Double sideplogKm) {
		if (this.sideplogKm != null) {
			this.sideplogKm += sideplogKm;
		} else {
			this.sideplogKm = sideplogKm;
		}
	}

	public Double getTungHovlingKm() {
		return tungHovlingKm;
	}

	public void addTungHovlingKm(Double tungHovlingKm) {
		if (this.tungHovlingKm != null) {
			this.tungHovlingKm += tungHovlingKm;
		} else {
			this.tungHovlingKm = tungHovlingKm;
		}
	}

	public Double getTungHovlingTimer() {
		return tungHovlingTimer;
	}

	public void addTungHovlingTimer(Double tungHovlingTimer) {
		if (this.tungHovlingTimer != null) {
			this.tungHovlingTimer += tungHovlingTimer;
		} else {
			this.tungHovlingTimer = tungHovlingTimer;
		}
	}

	public Double getStroingTimer() {
		return stroingTimer;
	}

	public void addStroingTimer(Double stroingTimer) {
		if (this.stroingTimer != null) {
			this.stroingTimer += stroingTimer;
		} else {
			this.stroingTimer = stroingTimer;
		}
	}

	public Double getTorrsaltTonn() {
		return torrsaltTonn;
	}

	public void addTorrsaltTonn(Double torrsaltTonn) {
		if (this.torrsaltTonn != null) {
			this.torrsaltTonn += torrsaltTonn;
		} else {
			this.torrsaltTonn = torrsaltTonn;
		}
	}

	public Double getSlurryTonn() {
		return slurryTonn;
	}

	public void addSlurryTonn(Double slurryTonn) {
		if (this.slurryTonn != null) {
			this.slurryTonn += slurryTonn;
		} else {
			this.slurryTonn = slurryTonn;
		}
	}

	public Double getSaltlosningKubikk() {
		return saltlosningKubikk;
	}

	public void addSaltlosningKubikk(Double saltlosningKubikk) {
		if (this.saltlosningKubikk != null) {
			this.saltlosningKubikk += saltlosningKubikk;
		} else {
			this.saltlosningKubikk = saltlosningKubikk;
		}
	}

	public Double getBefuktetSaltTonn() {
		return befuktetSaltTonn;
	}

	public void addBefuktetSaltTonn(Double befuktetSaltTonn) {
		if (this.befuktetSaltTonn != null) {
			this.befuktetSaltTonn += befuktetSaltTonn;
		} else {
			this.befuktetSaltTonn = befuktetSaltTonn;
		}
	}

	public Double getSandSaltTonn() {
		return sandSaltTonn;
	}

	public void addSandSaltTonn(Double sandSaltTonn) {
		if (this.sandSaltTonn != null) {
			this.sandSaltTonn += sandSaltTonn;
		} else {
			this.sandSaltTonn = sandSaltTonn;
		}
	}

	public Double getRenSandTonn() {
		return renSandTonn;
	}

	public void addRenSandTonn(Double renSandTonn) {
		if (this.renSandTonn != null) {
			this.renSandTonn += renSandTonn;
		} else {
			this.renSandTonn = renSandTonn;
		}
	}

	public Double getFastsandTonn() {
		return fastsandTonn;
	}

	public void addFastsandTonn(Double fastsandTonn) {
		if (this.fastsandTonn != null) {
			this.fastsandTonn += fastsandTonn;
		} else {
			this.fastsandTonn = fastsandTonn;
		}
	}

    public boolean hasData() {
        if (
                gyldigVerdi(broytingKm) ||
				gyldigVerdi(sideplogKm) ||
                gyldigVerdi(tungHovlingKm) ||
                gyldigVerdi(tungHovlingTimer) ||
                gyldigVerdi(stroingTimer) ||
                gyldigVerdi(torrsaltTonn) ||
                gyldigVerdi(slurryTonn) ||
                gyldigVerdi(saltlosningKubikk) ||
                gyldigVerdi(befuktetSaltTonn) ||
                gyldigVerdi(sandSaltTonn) ||
                gyldigVerdi(renSandTonn) ||
                gyldigVerdi(fastsandTonn)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean gyldigVerdi(Double verdi) {
        return verdi != null && verdi > 0;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Vinterdrift that = (Vinterdrift) o;

		if (rodeId != that.rodeId) return false;
		if (fylke != that.fylke) return false;
		if (vinterdriftklasse != null ? !vinterdriftklasse.equals(that.vinterdriftklasse) : that.vinterdriftklasse != null)
			return false;
		if (vegtype != null ? !vegtype.equals(that.vegtype) : that.vegtype != null) return false;
		return leverandor != null ? leverandor.equals(that.leverandor) : that.leverandor == null;
	}

	@Override
	public int hashCode() {
		int result = rodeId;
		result = 31 * result + (vinterdriftklasse != null ? vinterdriftklasse.hashCode() : 0);
		result = 31 * result + fylke;
		result = 31 * result + (vegtype != null ? vegtype.hashCode() : 0);
		result = 31 * result + (leverandor != null ? leverandor.hashCode() : 0);
		return result;
	}

}