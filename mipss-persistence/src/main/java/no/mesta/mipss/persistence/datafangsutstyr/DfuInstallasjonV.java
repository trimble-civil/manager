package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "DfuInstallasjonV.findAll", 
    query = "select o from DfuInstallasjonV o")
@Table(name = "DFU_INSTALLASJON_V")
public class DfuInstallasjonV implements Serializable, IRenderableMipssEntity  {
    private Date aktivFraDato;
    private Date aktivTilDato;
    private Long id;
    private String kategori;
    private Long kjoretoyId;
    private String modell;
    private String serienummer;
    private String simkortnummer;
    private Date sisteSignalDato;
    private String status;
    
    // disse felt er lagt til for hånd
    private List<String> statusList = new ArrayList<String>();

    public DfuInstallasjonV() {
    }

    public DfuInstallasjonV(Installasjon installasjon) {
    	aktivFraDato = installasjon.getAktivFraDato();
    	aktivTilDato = installasjon.getAktivTilDato();
    	id = installasjon.getId();
    	if(installasjon.getDfumodell() != null && installasjon.getDfumodell().getDfukategori() != null) {
    		kategori = installasjon.getDfumodell().getDfukategori().getNavn();
    	}
    	if(installasjon.getKjoretoy() != null) {
    		kjoretoyId = installasjon.getKjoretoy().getId();
    	}
    	modell = installasjon.getModellNavn();
    	if(installasjon.getDfuindivid() != null) {
    		serienummer = installasjon.getDfuindivid().getSerienummer();
    		if(installasjon.getDfuindivid().getDfuinfo() != null) {
    			sisteSignalDato = installasjon.getDfuindivid().getDfuinfo().getSisteLivstegnDato();
    		}
    	}
    }
    
    @Column(name="AKTIV_FRA_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getAktivFraDato() {
        return aktivFraDato;
    }

    public void setAktivFraDato(Date aktivFraDato) {
        this.aktivFraDato = aktivFraDato;
    }

    @Column(name="AKTIV_TIL_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getAktivTilDato() {
        return aktivTilDato;
    }

    public void setAktivTilDato(Date aktivTilDato) {
        this.aktivTilDato = aktivTilDato;
    }

    @Id
    @Column(name="ID", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    @Column(name="KJORETOY_ID")
    public Long getKjoretoyId() {
        return kjoretoyId;
    }

    public void setKjoretoyId(Long kjoreotyId) {
        this.kjoretoyId = kjoreotyId;
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    public String getSerienummer() {
        return serienummer;
    }

    public void setSerienummer(String serienummer) {
        this.serienummer = serienummer;
    }

    public String getSimkortnummer() {
        return simkortnummer;
    }

    public void setSimkortnummer(String simkortnummer) {
        this.simkortnummer = simkortnummer;
    }

    @Column(name="SISTE_SIGNAL_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSisteSignalDato() {
        return sisteSignalDato;
    }

    public void setSisteSignalDato(Date sisteSignalDato) {
        this.sisteSignalDato = sisteSignalDato;
    }

    
    public String getStatus() {
        return status;
    }

    @Transient
    public String getTextForGUI() {
        return String.valueOf(getId());
    }

    public int compareTo(Object o) {
        DfuInstallasjonV other = (DfuInstallasjonV) o;
        return new CompareToBuilder().
            append(getId(), other.getId()).
            toComparison();
    }
    
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof DfuInstallasjonV)) {
            return false;
        }
        DfuInstallasjonV other = (DfuInstallasjonV) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            append(getKjoretoyId(), other.getKjoretoyId()).
            append(getModell(), other.getModell()).
            append(getSerienummer(), other.getSerienummer()).
            isEquals();
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("IntallasjonId", getId()).
            append("KjøretøyId", getKjoretoyId()).
            append("Kategori", getKategori()).
            append("modell", getModell()).
            append("serienummer", getSerienummer()).
            toString();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(31,11).
            append(getId()).
            toHashCode();
    }
    
    @Transient
    public List<String> getStatusList() {
        if (statusList.isEmpty()) {
            addStatus(getSqlViewStatus());
        }
        return statusList;
    }
    
    public void addStatus(String status) {
        statusList.add(status);
    }
    
    @Transient
    public String getSqlViewStatus() {
        return status;
    }
    
    /**
     * Returnerer det siste statusnavnet. Dette garanteres av at SQL view'et er sortert etter
     * statusdato.
     * @return
     */
    @Transient
    public String getCurrentStatus() {
        return getStatusList().get(0);
    }
}
