package no.mesta.mipss.persistence.kvernetf1;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Nøkkelklasse for prodreflink seksjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class ProdreflinkseksjonPK implements Serializable {
    private double fra;
    private int reflinkIdent;
    private Long strekningId;

    public ProdreflinkseksjonPK() {
    }

    public ProdreflinkseksjonPK(double fra, int reflinkIdent, Long strekningId) {
    	this.fra = fra;
        this.reflinkIdent = reflinkIdent;
        this.strekningId = strekningId;
    }

    public void setFra(double fra) {
        this.fra = fra;
    }

    public double getFra() {
        return fra;
    }

    public void setReflinkIdent(int reflinkIdent) {
        this.reflinkIdent = reflinkIdent;
    }

    public int getReflinkIdent() {
        return reflinkIdent;
    }

    public void setStrekningId(Long strekningId) {
        this.strekningId = strekningId;
    }

    public Long getStrekningId() {
        return strekningId;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(fra).append(reflinkIdent).append(strekningId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof ProdreflinkseksjonPK)) {return false;}
        
        ProdreflinkseksjonPK other = (ProdreflinkseksjonPK) o;
        
        return new EqualsBuilder().append(fra, other.fra).append(reflinkIdent, other.reflinkIdent).append(strekningId, other.strekningId).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("fra", fra).
            append("reflinkIdent", reflinkIdent).
            append("strekningId", strekningId).
            toString();
    }
}