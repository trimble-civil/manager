package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "Dfustatus.findAll", query = "select o from Dfustatus o")
public class Dfustatus extends MipssEntityBean<Dfustatus> implements Serializable, IRenderableMipssEntity {
    public static final String FIND_ALL = "Dfustatus.findAll";
	private String beskrivelse;
    private String navn;
    private Boolean valgbarFlagg;
    private List<Dfu_Status> dfu_StatusList = new ArrayList<Dfu_Status>();

    public Dfustatus() {
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
    	if(valgbarFlagg == null) {
			valgbarFlagg = false;
		}
		return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }

    @OneToMany(mappedBy = "dfustatus")
    public List<Dfu_Status> getDfu_StatusList() {
        return dfu_StatusList;
    }

    public void setDfu_StatusList(List<Dfu_Status> dfu_StatusList) {
        this.dfu_StatusList = dfu_StatusList;
    }

    public Dfu_Status addDfu_Status(Dfu_Status dfu_Status) {
        getDfu_StatusList().add(dfu_Status);
        dfu_Status.setDfustatus(this);
        return dfu_Status;
    }

    public Dfu_Status removeDfu_Status(Dfu_Status dfu_Status) {
        getDfu_StatusList().remove(dfu_Status);
        dfu_Status.setDfustatus(null);
        return dfu_Status;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            append("valgbart", getValgbarFlagg()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Dfustatus)) {
            return false;
        }
        Dfustatus other = (Dfustatus) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getNavn()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Dfustatus other = (Dfustatus) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
