package no.mesta.mipss.persistence.mipssfield;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@Table(name = "FAVORITT_PROSESS")
@SequenceGenerator(name = FavorittProsess.FAVORITT_PROSESS_ID_SEQ, sequenceName = "FAVORITT_PROSESS_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries ({
	@NamedQuery(name = FavorittProsess.FIND_ALL, query = "select o from FavorittProsess o"),
	@NamedQuery(name = FavorittProsess.FIND_BY_FAVORITTLISTE, query = "SELECT o FROM FavorittProsess o where o.listeId = :id")
})
public class FavorittProsess extends MipssEntityBean<FavorittProsess> {
	public static final String FAVORITT_PROSESS_ID_SEQ = "favorittProsessIdSeq";
	public static final String FIND_ALL = "FavorittProsess.findAll";
	public static final String FIND_BY_FAVORITTLISTE = "FavorittProsess.findByFavorittliste";
	
	private Long id;
	private Long listeId;
	private Long prosessId;
	private Favorittliste favorittliste;
	private Prosess prosess;
	public FavorittProsess() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = FavorittProsess.FAVORITT_PROSESS_ID_SEQ)
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "LISTE_ID", nullable = false, insertable= false, updatable = false)
	public Long getListeId() {
		return listeId;
	}

	public void setListeId(Long listeId) {
		Object old = this.listeId;
		this.listeId = listeId;
		getProps().firePropertyChange("listeId", old, listeId);
	}

	@Column(name = "PROSESS_ID", nullable = false, insertable= false, updatable = false)
	public Long getProsessId() {
		return prosessId;
	}

	public void setProsessId(Long prosessId) {
		Object old = this.prosessId;
		this.prosessId = prosessId;
		getProps().firePropertyChange("prosessId", old, prosessId);
	}

	@ManyToOne
    @JoinColumn(name = "LISTE_ID", referencedColumnName = "ID")
	public Favorittliste getFavorittliste() {
		return favorittliste;
	}

	public void setFavorittliste(Favorittliste favorittliste) {
		Object old = this.favorittliste;
		this.favorittliste = favorittliste;
		getProps().firePropertyChange("favorittliste", old, favorittliste);
		
		if(favorittliste != null) {
			setListeId(favorittliste.getId());
		} else {
			setListeId(null);
		}
	}

	@ManyToOne
    @JoinColumn(name = "PROSESS_ID", referencedColumnName = "ID")
	public Prosess getProsess() {
		return prosess;
	}

	public void setProsess(Prosess prosess) {
		Object old = this.prosess;
		this.prosess = prosess;
		getProps().firePropertyChange("prosess", old, prosess);
		
		if(prosess != null) {
			setProsessId(prosess.getId());
		} else {
			setProsessId(null);
		}
	}
}
