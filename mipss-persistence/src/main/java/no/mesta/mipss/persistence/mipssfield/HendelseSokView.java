package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.valueobjects.ElrappStatus;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Søkerad for hendelser
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( { @NamedQuery(name = HendelseSokView.QUERY_FIND_ALL, query = "SELECT h FROM HendelseSokView h") })
@Table(name = "HENDELSE_SOK_V")
public class HendelseSokView extends MipssEntityBean<HendelseSokView> implements Serializable,
		Comparable<HendelseSokView>, IRenderableMipssEntity, MipssFieldDetailItem {

	private static final Logger log = LoggerFactory.getLogger(HendelseSokView.class);

	private static final String FIELD_NAME_AARSAKID = "aarsakId";
	private static final String FIELD_NAME_AARSAKNAVN = "aarsakNavn";
	private static final String FIELD_NAME_ANTALLBILDER = "antallBilder";
	private static final String FIELD_NAME_ANTALLFUNN = "antallFunn";
	private static final String FIELD_NAME_BESKRIVELSE = "beskrivelse";
	private static final String FIELD_NAME_DATO = "dato";
	private static final String FIELD_NAME_FYLKESNUMMER = "fylkesnummer";
	private static final String FIELD_NAME_HARINSPEKSJON = "harInspeksjon";
	private static final String FIELD_NAME_FRA_HP = "fraHp";
	private static final String FIELD_NAME_TIL_HP = "tilHp";
	private static final String FIELD_NAME_KJOREFELT = "kjorefelt";
	private static final String FIELD_NAME_KOMMUNE = "kommune";
	private static final String FIELD_NAME_KOMMUNENUMMER = "kommunenummer";
	private static final String FIELD_NAME_KONTRAKTID = "kontraktId";
	private static final String FIELD_NAME_FRA_METER = "fraMeter";
	private static final String FIELD_NAME_TIL_METER = "tilMeter";
	private static final String FIELD_NAME_OPPRETTETAV = "opprettetAv";
	private static final String FIELD_NAME_OPPRETTETDATO = "opprettetDato";
	private static final String FIELD_NAME_PDADFUIDENT = "pdaDfuIdent";
	private static final String FIELD_NAME_PDADFUNAVN = "pdaDfuNavn";
	private static final String FIELD_NAME_R5 = "r5";
	private static final String FIELD_NAME_R11 = "r11";
	private static final String FIELD_NAME_SKADESTED = "skadested";
	private static final String FIELD_NAME_SLETTETAV = "slettetAv";
	private static final String FIELD_NAME_SLETTETDATO = "slettetDato";
	private static final String FIELD_NAME_TILTAKTEKSTER = "tiltakTekster";
	private static final String FIELD_NAME_TYPEID = "typeId";
	private static final String FIELD_NAME_TYPENAVN = "typeNavn";
	private static final String FIELD_NAME_VEIKATEGORI = "veikategori";
	private static final String FIELD_NAME_VEINUMMER = "veinummer";
	private static final String FIELD_NAME_VEISTATUS = "veistatus";
	private static final String FIELD_NAME_FRA_X = "fraX";
	private static final String FIELD_NAME_FRA_Y = "fraY";
	private static final String FIELD_NAME_TIL_X = "tilX";
	private static final String FIELD_NAME_TIL_Y = "tilY";
	private static final String ID_NAME = "guid";
	private static final String[] ID_NAMES = new String[] { ID_NAME };
	public static final String QUERY_FIND_ALL = "HendelseSokView.findAll";
	private static final String REFNUMMER = "refnummer";
	
	public static HendelseSokView newInstance(Hendelse hendelse) {
		if(hendelse != null) {
			HendelseSokView view = new HendelseSokView();
			
			view.setGuid(hendelse.getGuid());
			view.setAarsakId(hendelse.getAarsakId());
			view.setAarsakNavn(hendelse.getAarsak() != null ? hendelse.getAarsak().getNavn() : null);
			view.setAntallBilder(hendelse.getAntallBilder());
			view.setDato(hendelse.getDato());
			view.setFylkesnummer(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getFylkesnummer() : 0);
			view.setFraHp(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getHp() : 0);
			view.setKjorefelt(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getKjorefelt() : "");
			//view.setKommune(kommune); // For langt ut i objektgrafen
			view.setKommunenummer(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getKommunenummer() : 0);
			view.setFraMeter(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getMeter() : 0);
			view.setOpprettetAv(hendelse.getOwnedMipssEntity().getOpprettetAv());
			view.setOpprettetDato(hendelse.getOwnedMipssEntity().getOpprettetDato());
			//view.setPdaDfuNavn(pdaDfuNavn); // For langt ut i objektgrafen
			view.setR5(hendelse.getSak().getForsikringsskade()!=null ? 1 : 0);
			view.setR11(hendelse.getSak().getSkred()!=null?1:0);
			view.setSlettetAv(hendelse.getSlettetAv());
			view.setSlettetDato(hendelse.getSlettetDato());
			//view.setTiltak(tiltak); // for komplisert
			//view.setTiltakTekster(tiltakTekster); // for komplisert
			view.setVeikategori(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getVeikategori() : null);
			view.setVeinummer(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getVeinummer() : null);
			view.setVeistatus(hendelse.getSak().getVeiref() != null ? hendelse.getSak().getVeiref().getVeistatus() : null);
			final Double x;
			if(hendelse.getSak().getStedfesting() != null) {
				x = hendelse.getSak().getStedfesting().get(0).getSnappetX() != null ? hendelse.getSak().getStedfesting().get(0).getSnappetX() : hendelse.getSak().getStedfesting().get(0).getX();
			} else {
				x = null;
			}
			view.setFraX(x);
			final Double y;
			if(hendelse.getSak().getStedfesting() != null) {
				y = hendelse.getSak().getStedfesting().get(0).getSnappetY() != null ? hendelse.getSak().getStedfesting().get(0).getSnappetY() : hendelse.getSak().getStedfesting().get(0).getY();
			} else {
				y = null;
			}
			view.setFraY(y);
			
			return view;
		} else {
			return null;
		}
	}
	private Long aarsakId;
	private String aarsakNavn;
	private Integer antallBilder;
	private Integer antallFunn;
	private String beskrivelse;
	private Date dato;
	private int fylkesnummer;
	private String guid;
	private Integer harInspeksjon;
	private int fraHp;
	private Integer tilHp;
	private String kjorefelt;
	private String kommune;
	private int kommunenummer;
	private Long kontraktId;
	private int fraMeter;
	private Integer tilMeter;
	private String opprettetAv;
	private Date opprettetDato;
	private Long pdaDfuIdent;
	private String pdaDfuNavn;
	private Integer r5;
	private Integer r11;
	private Long refnummer;
	private String skadested;
	private String slettetAv;
	private Date slettetDato;
	private List<TrafikktiltakSok> tiltak = new ArrayList<TrafikktiltakSok>();
	private String tiltakTekster;
	private String veikategori;
	private int veinummer;
	private String veistatus;
	private Double fraX;
	private Double fraY;
	private Double tilX;
	private Double tilY;
	private Date rapportertDato;
	private Long elrappVersjon;
	private Long elrappDokumentIdent;
	private Date endretDato;
	
	public HendelseSokView() {
		log("HendelseSokView() created");
	}
	
	/** {@inheritDoc} */
	public int compareTo(HendelseSokView o) {
		return new CompareToBuilder().append(dato, o.dato).append(opprettetAv, o.opprettetAv).toComparison();
	}

	@Column(name = "AARSAK_ID")
	public Long getAarsakId() {
		return aarsakId;
	}

	@Column(name = "AARSAK_NAVN")
	public String getAarsakNavn() {
		return aarsakNavn;
	}

	@Column(name = "ANTALL_BILDER")
	public Integer getAntallBilder() {
		return antallBilder;
	}

	@Column(name = "ANTALL_FUNN")
	public Integer getAntallFunn() {
		return antallFunn;
	}

	@Column(name = "BESKRIVELSE", length = 1000)
	public String getBeskrivelse() {
		return beskrivelse;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDato() {
		return dato;
	}

	@Transient
	public Boolean getFunnetUnderInspeksjon() {
		return getHarInspeksjon() == 1L;
	}

	public int getFylkesnummer() {
		return fylkesnummer;
	}

	@Id
	public String getGuid() {
		return guid;
	}

	public Integer getHarInspeksjon() {
		return harInspeksjon;
	}
	@Column(name="FRA_HP")
	public int getFraHp() {
		return fraHp;
	}
	@Column(name="TIL_HP")
	public Integer getTilHp() {
		return tilHp;
	}

	@Transient
	public String getHpGuiText() {
		VeiInfo veiInfo = VeiInfoUtil.buildVeiInfo(this);
		String append="";
		if ((getTilHp().intValue()!=getFraHp())&&(getTilMeter().intValue()!=getFraMeter())){
			append += " - hp"+getTilHp()+" m"+getTilMeter();
		}
		return veiInfo.toString()+append;
	}

	/** {@inheritDoc} */
	@Override
	@Transient
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public String getKjorefelt() {
		return kjorefelt;
	}

	public String getKommune() {
		return kommune;
	}

	public int getKommunenummer() {
		return kommunenummer;
	}

	@Column(name = "KONTRAKT_ID")
	public Long getKontraktId() {
		return kontraktId;
	}
	@Column(name="FRA_METER")
	public int getFraMeter() {
		return fraMeter;
	}
	@Column(name="TIL_METER")
	public Integer getTilMeter() {
		return tilMeter;
	}

	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@Column(name = "PDA_DFU_IDENT")
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}

	@Column(name = "PDA_DFU_NAVN")
	public String getPdaDfuNavn() {
		return pdaDfuNavn;
	}

	public Integer getR5() {
		return r5;
	}

	public Integer getR11() {
		return r11;
	}
	@Transient
	public Boolean getR5Sak() {
		return getR5() == 1L;
	}

	@Transient
	public Boolean getR11Sak(){
		return getR11() == 1L;
	}
	
	/**
	 * Gir typen R-skjema som en string
	 * @return
	 */
	@Transient
	public String getRString(){
		if (getR11()==1){
			return "R11";
		}
		if (getR5()==1){
			return "R5";
		}
		return "R2";
		
	}
	
	@Transient
	public ElrappStatus getElrappStatus(){
		if (getRapportertDato()==null){
			return new ElrappStatus(ElrappStatus.Status.IKKE_SENDT); 
		}
		if (getEndretDato()!=null){
			if (getRapportertDato().before(getEndretDato())){
				return new ElrappStatus(ElrappStatus.Status.ENDRET);
			}
		}
		return new ElrappStatus(ElrappStatus.Status.SENDT);
		
	}
	/**
	 * Dato hendelsen ble rapportert til Elrapp
	 * @return
	 */
	@Column(name = "RAPPORTERT_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRapportertDato(){
		return rapportertDato;
	}
	public void setRapportertDato(Date rapportertDato){
		this.rapportertDato = rapportertDato;
		
	}
	@Column(name = "ELRAPP_VERSJON")
	public Long getElrappVersjon(){
		return elrappVersjon;
	}
	@Column(name = "ELRAPP_DOKUMENT_IDENT")
	public Long getElrappDokumentIdent(){
		return elrappDokumentIdent;
	}
	
	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato(){
		return endretDato;
	}
	public void setEndretDato(Date endretDato){
		this.endretDato = endretDato;
		
	}
	public void setElrappVersjon(Long elrappVersjon){
		this.elrappVersjon = elrappVersjon;
		
	}
	public void setElrappDokumentIdent(Long elrappDokumentIdent){
		this.elrappDokumentIdent = elrappDokumentIdent;
		
	}
	@Column(name=REFNUMMER)
	public Long getRefnummer(){
		return refnummer;
	}

	public String getSkadested() {
		return skadested;
	}

	@Column(name = "SLETTET_AV")
	public String getSlettetAv() {
		return slettetAv;
	}

	@Column(name = "SLETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSlettetDato() {
		return slettetDato;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		String datoText = dato != null ? MipssDateFormatter.formatDate(dato, MipssDateFormatter.SHORT_DATE_TIME_FORMAT)
				: "";
		String av = opprettetAv != null ? opprettetAv : "";
		return datoText + " av " + av;
	}

	@OneToMany(mappedBy = "hendelse", fetch = FetchType.LAZY)
	public List<TrafikktiltakSok> getTiltak() {
		return tiltak;
	}

	@Column(name = "TILTAK_TEKSTER")
	public String getTiltakTekster() {
		return tiltakTekster;
	}

	@Column(nullable = false, length = 1)
	public String getVeikategori() {
		return veikategori;
	}

	@Column(nullable = false)
	public int getVeinummer() {
		return veinummer;
	}

	@Column(nullable = false, length = 1)
	public String getVeistatus() {
		return veistatus;
	}
	@Column(name="FRA_X")
	public Double getFraX() {
		return fraX;
	}
	@Column(name="TIL_X")
	public Double getTilX() {
		return tilX;
	}
	@Column(name="FRA_Y")
	public Double getFraY() {
		return fraY;
	}
	@Column(name="TIL_Y")
	public Double getTilY() {
		return tilY;
	}

	public void setAarsakId(Long aarsakId) {
		log.trace("setAarsakId({})", aarsakId);
		Long old = this.aarsakId;
		this.aarsakId = aarsakId;
		firePropertyChange(FIELD_NAME_AARSAKID, old, aarsakId);
	}

	public void setAarsakNavn(String aarsakNavn) {
		log.trace("setAarsakNavn({})", aarsakNavn);
		String old = this.aarsakNavn;
		this.aarsakNavn = aarsakNavn;
		firePropertyChange(FIELD_NAME_AARSAKNAVN, old, aarsakNavn);
	}

	public void setAntallBilder(Integer antallBilder) {
		log.trace("setAntallBilder({})", antallBilder);
		Integer old = this.antallBilder;
		this.antallBilder = antallBilder;
		firePropertyChange(FIELD_NAME_ANTALLBILDER, old, antallBilder);
	}

	public void setAntallFunn(Integer antallFunn) {
		log.trace("setAntallFunn({})", antallFunn);
		Integer old = this.antallFunn;
		this.antallFunn = antallFunn;
		firePropertyChange(FIELD_NAME_ANTALLFUNN, old, antallFunn);
	}
	
	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange(FIELD_NAME_BESKRIVELSE, old, beskrivelse);
	}

	public void setDato(Date dato) {
		log.trace("setDato({})", dato);
		Date old = this.dato;
		this.dato = dato;
		firePropertyChange(FIELD_NAME_DATO, old, dato);
	}

	public void setFylkesnummer(int fylkesnummer) {
		log.trace("setFylkesnummer({})", fylkesnummer);
		int old = this.fylkesnummer;
		this.fylkesnummer = fylkesnummer;
		firePropertyChange(FIELD_NAME_FYLKESNUMMER, old, fylkesnummer);
	}

	public void setGuid(String guid) {
		log.trace("setGuid({})", guid);
		String old = this.guid;
		this.guid = guid;
		firePropertyChange(ID_NAME, old, guid);
	}

	public void setHarInspeksjon(Integer harInspeksjon) {
		log.trace("setHarInspeksjon({})", harInspeksjon);
		Integer old = this.harInspeksjon;
		this.harInspeksjon = harInspeksjon;
		firePropertyChange(FIELD_NAME_HARINSPEKSJON, old, harInspeksjon);
	}

	public void setFraHp(int fraHp) {
		log.trace("setFraHp({})", fraHp);
		int old = this.fraHp;
		this.fraHp = fraHp;
		firePropertyChange(FIELD_NAME_FRA_HP, old, fraHp);
	}
	
	public void setTilHp(Integer tilHp) {
		log.trace("setTilHp({})", tilHp);
		Integer old = this.tilHp;
		this.tilHp = tilHp;
		firePropertyChange(FIELD_NAME_TIL_HP, old, tilHp);
	}

	public void setKjorefelt(String kjorefelt) {
		log.trace("setKjorefelt({})", kjorefelt);
		String old = this.kjorefelt;
		this.kjorefelt = kjorefelt;
		firePropertyChange(FIELD_NAME_KJOREFELT, old, kjorefelt);
	}

	public void setKommune(String kommune) {
		log.trace("setKommune({})", kommune);
		String old = this.kommune;
		this.kommune = kommune;
		firePropertyChange(FIELD_NAME_KOMMUNE, old, kommune);
	}

	public void setKommunenummer(int kommunenummer) {
		log.trace("setKommunenummer({})", kommunenummer);
		int old = this.kommunenummer;
		this.kommunenummer = kommunenummer;
		firePropertyChange(FIELD_NAME_KOMMUNENUMMER, old, kommunenummer);
	}

	public void setKontraktId(Long kontraktId) {
		log.trace("setKontraktId({})", kontraktId);
		Long old = this.kontraktId;
		this.kontraktId = kontraktId;
		firePropertyChange(FIELD_NAME_KONTRAKTID, old, kontraktId);
	}

	public void setFraMeter(int fraMeter) {
		log.trace("setFraMeter({})", fraMeter);
		int old = this.fraMeter;
		this.fraMeter = fraMeter;
		firePropertyChange(FIELD_NAME_FRA_METER, old, fraMeter);
	}
	public void setTilMeter(Integer tilMeter) {
		log.trace("setTilMeter({})", tilMeter);
		Integer old = this.tilMeter;
		this.tilMeter = tilMeter;
		firePropertyChange(FIELD_NAME_TIL_METER, old, tilMeter);
	}
	public void setOpprettetAv(String opprettetAv) {
		log.trace("setOpprettetAv({})", opprettetAv);
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange(FIELD_NAME_OPPRETTETAV, old, opprettetAv);
	}

	public void setOpprettetDato(Date opprettetDato) {
		log.trace("setOpprettetDato({})", opprettetDato);
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange(FIELD_NAME_OPPRETTETDATO, old, opprettetDato);
	}

	public void setPdaDfuIdent(Long pdaDfuIdent) {
		Long old = this.pdaDfuIdent;
		this.pdaDfuIdent = pdaDfuIdent;
		firePropertyChange(FIELD_NAME_PDADFUIDENT, old, pdaDfuIdent);
	}

	public void setPdaDfuNavn(String pdaDfuNavn) {
		String old = this.pdaDfuNavn;
		this.pdaDfuNavn = pdaDfuNavn;
		firePropertyChange(FIELD_NAME_PDADFUNAVN, old, pdaDfuNavn);
	}

	public void setR5(Integer r5) {
		log.trace("setR5({})", r5);
		Integer old = this.r5;
		this.r5 = r5;
		firePropertyChange(FIELD_NAME_R5, old, r5);
	}
	public void setR11(Integer r11){
		Integer old = this.r11;
		this.r11 = r11;
		firePropertyChange(FIELD_NAME_R11, old, r11);
	}

	public void setRefnummer(Long refnummer){
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}

	public void setSkadested(String skadested) {
		log.trace("setSkadested({})", skadested);
		String old = this.skadested;
		this.skadested = skadested;
		firePropertyChange(FIELD_NAME_SKADESTED, old, skadested);
	}

	public void setSlettetAv(String slettetAv) {
		log.trace("setSlettetAv({})", slettetDato);
		String oldValue = this.slettetAv;
		this.slettetAv = slettetAv;
		firePropertyChange(FIELD_NAME_SLETTETAV, oldValue, slettetAv);
	}

	public void setSlettetDato(Date slettetDato) {
		log.trace("setSlettetDato({})", slettetDato);
		Date oldValue = this.slettetDato;
		this.slettetDato = slettetDato;
		firePropertyChange(FIELD_NAME_SLETTETDATO, oldValue, slettetDato);
	}

	public void setTiltak(List<TrafikktiltakSok> tiltak) {
		// ingen listen på denne, for vi ønsker ikke trigge eager loading
		this.tiltak = tiltak;
	}

	public void setTiltakTekster(String tiltakTekster) {
		log.trace("setTiltakTekster({})", tiltakTekster);
		String old = this.tiltakTekster;
		this.tiltakTekster = tiltakTekster;
		firePropertyChange(FIELD_NAME_TILTAKTEKSTER, old, tiltakTekster);
	}


	public void setVeikategori(String veikategori) {
		log.trace("setVeikategori({})", veikategori);
		String old = this.veikategori;
		this.veikategori = veikategori;
		firePropertyChange(FIELD_NAME_VEIKATEGORI, old, veikategori);
	}

	public void setVeinummer(int veinummer) {
		log.trace("setVeinummer({})", veinummer);
		int old = this.veinummer;
		this.veinummer = veinummer;
		firePropertyChange(FIELD_NAME_VEINUMMER, old, veinummer);
	}

	public void setVeistatus(String veistatus) {
		log.trace("setVeistatus({})", veistatus);
		String old = this.veistatus;
		this.veistatus = veistatus;
		firePropertyChange(FIELD_NAME_VEISTATUS, old, veistatus);
	}

	public void setFraX(Double fraX) {
		log.trace("setFraX({})", fraX);
		Double old = this.fraX;
		this.fraX = fraX;
		firePropertyChange(FIELD_NAME_FRA_X, old, fraX);
	}
	public void setTilX(Double tilX) {
		log.trace("setTilX({})", tilX);
		Double old = this.tilX;
		this.tilX = tilX;
		firePropertyChange(FIELD_NAME_TIL_X, old, tilX);
	}
	public void setFraY(Double fraY) {
		log.trace("setFray({})", fraY);
		Double old = this.fraY;
		this.fraY = fraY;
		firePropertyChange(FIELD_NAME_FRA_Y, old, fraY);
	}
	
	public void setTilY(Double tilY) {
		log.trace("setTily({})", tilY);
		Double old = this.tilY;
		this.tilY = tilY;
		firePropertyChange(FIELD_NAME_TIL_Y, old, tilY);
	}
	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append(REFNUMMER, refnummer)
			.append(ID_NAME, guid)
			.append(FIELD_NAME_AARSAKID, aarsakId)
			.append(FIELD_NAME_AARSAKNAVN, aarsakNavn)
			.append(FIELD_NAME_ANTALLBILDER, antallBilder)
			.append(FIELD_NAME_ANTALLFUNN, antallFunn)
			.append(FIELD_NAME_DATO, dato)
			.append(FIELD_NAME_FYLKESNUMMER, fylkesnummer)
			.append(FIELD_NAME_HARINSPEKSJON, harInspeksjon)
			.append(FIELD_NAME_FRA_HP, fraHp)
			.append(FIELD_NAME_TIL_HP, tilHp)
			.append(FIELD_NAME_KJOREFELT, kjorefelt)
			.append(FIELD_NAME_KOMMUNE, kommune)
			.append(FIELD_NAME_KOMMUNENUMMER, kommunenummer)
			.append(FIELD_NAME_KONTRAKTID, kontraktId)
			.append(FIELD_NAME_FRA_METER, fraMeter)
			.append(FIELD_NAME_TIL_METER, tilMeter)
			.append(FIELD_NAME_OPPRETTETAV, opprettetAv)
			.append(FIELD_NAME_OPPRETTETDATO, opprettetDato)
			.append(FIELD_NAME_PDADFUIDENT, pdaDfuIdent)
			.append(FIELD_NAME_PDADFUNAVN, pdaDfuNavn)
			.append(FIELD_NAME_R5, r5)
			.append(FIELD_NAME_R11, r11)
			.append(FIELD_NAME_SKADESTED, skadested)
			.append(FIELD_NAME_SLETTETAV, slettetAv)
			.append(FIELD_NAME_SLETTETDATO, slettetDato)
			.append(FIELD_NAME_TILTAKTEKSTER, tiltakTekster)
			.append(FIELD_NAME_VEIKATEGORI, veikategori)
			.append(FIELD_NAME_VEINUMMER, veinummer)
			.append(FIELD_NAME_VEISTATUS, veistatus)
			.append(FIELD_NAME_FRA_X, fraX)
			.append(FIELD_NAME_FRA_Y, fraY)
			.append(FIELD_NAME_TIL_X, tilX)
			.append(FIELD_NAME_TIL_Y, tilY)
			.toString();
	}
}
