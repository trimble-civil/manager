package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.swing.ImageIcon;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Ikon;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Menygruppe.FIND_ALL, query = "select o from Menygruppe o")
public class Menygruppe extends MipssEntityBean<Menygruppe> implements Serializable, IRenderableMipssEntity {
	
	public static final String FIND_ALL = "Menygruppe.findAll";
	private String endretAv;
	private Date endretDato;
	private Long id;
	private Ikon ikon;
	private List<Menypunkt> menypunktList;
	private String navn;
	private String opprettetAv;
	private Date opprettetDato;

	public Menygruppe() {
	}

	public Menypunkt addMenypunkt(Menypunkt menypunkt) {
		getMenypunktList().add(menypunkt);
		menypunkt.setMenygruppe(this);
		return menypunkt;
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(value=TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	@Id
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	@ManyToOne
	@JoinColumn(name = "IKON_ID", referencedColumnName = "ID")
	public Ikon getIkon() {
		return ikon;
	}

	/**
	 * Gjør om BLOB fra DB til {@code ImageIcon}
	 * 
	 * @return
	 */
	@Transient
	public ImageIcon getImageIcon() {
		if (ikon != null && ikon.getIkonLob() != null) {
			return new ImageIcon(ikon.getIkonLob());
		} else {
			return null;
		}
	}

	@OneToMany(mappedBy = "menygruppe")
	public List<Menypunkt> getMenypunktList() {
		return menypunktList;
	}

	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(value=TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public Menypunkt removeMenypunkt(Menypunkt menypunkt) {
		getMenypunktList().remove(menypunkt);
		menypunkt.setMenygruppe(null);
		return menypunkt;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	public void setIkon(Ikon ikon) {
		Ikon old = this.ikon;
		this.ikon = ikon;
		firePropertyChange("ikon", old, ikon);
	}

	public void setMenypunktList(List<Menypunkt> menypunktList) {
		this.menypunktList = menypunktList;
	}

	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public String toString() {
		return getNavn();
	}

	public String getTextForGUI() {
		return navn;
	}
}