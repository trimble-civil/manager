package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * N�kkelklasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class KjoretoyBildePK implements Serializable {
    private String bildeGuid;
    private Long kjoretoyId;

    public KjoretoyBildePK() {
    }

    public KjoretoyBildePK(String bildeGuid, Long kjoretoyId) {
        this.bildeGuid = bildeGuid;
        this.kjoretoyId = kjoretoyId;
    }

    public void setBildeGuid(String bildeGuid) {
        this.bildeGuid = bildeGuid;
    }

    public String getBildeGuid() {
        return bildeGuid;
    }

    public void setKjoretoyId(Long kjoretoyId) {
        this.kjoretoyId = kjoretoyId;
    }

    public Long getKjoretoyId() {
        return kjoretoyId;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(bildeGuid).append(kjoretoyId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KjoretoyBildePK)) {return false;}
        
        KjoretoyBildePK other = (KjoretoyBildePK) o;
        
        return new EqualsBuilder().append(bildeGuid, other.bildeGuid).append(kjoretoyId, other.kjoretoyId).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("bildeGuid", bildeGuid).
            append("kjoretoyId", kjoretoyId).
            toString();
    }
}
