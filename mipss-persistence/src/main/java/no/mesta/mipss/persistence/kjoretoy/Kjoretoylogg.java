package no.mesta.mipss.persistence.kjoretoy;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Representerer et kj�ret�ylogg i databasen. Generert i JDeveloper.
 * @author jorge
 *
 */
@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "kjoretoyloggIdSeq", sequenceName = "KJORETOYLOGG_ID_SEQ", initialValue = 3, allocationSize = 1)

@NamedQueries({
	@NamedQuery(name = Kjoretoylogg.FIND_ALL, query = "select o from Kjoretoylogg o order by o.id desc"),
	@NamedQuery(name = Kjoretoylogg.FIND_BY_KJORETOY, query = "select o from Kjoretoylogg o where o.kjoretoy.id = :id")
})
@IdClass(KjoretoyloggPK.class)
public class Kjoretoylogg implements Serializable, IRenderableMipssEntity, Comparable<Kjoretoylogg> {
	public static final String FIND_ALL = "Kjoretoylogg.findAll";
	public static final String FIND_BY_KJORETOY = "Kjoretoylogg.findByKjoretoy";
    private Long id;
    private String tekst;
    private Kjoretoy kjoretoy;
    
    private String opprettetAv;
    private Date opprettetDato;
    
    public Kjoretoylogg() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoyloggIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
        this.kjoretoy = kjoretoy;
    }
    
    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Id
    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
    	if(opprettetDato == null) {
    		opprettetDato = Clock.now();
    	}
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }
    
    // de neste metodene er h�ndkodet
     /**
      * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
      * @return en tekstlinje.
      */
     @Override
     public String toString() {
         int size = Math.min(10, getTekst().length() - 1);
         return new ToStringBuilder(this).
             append("id", getId()).
             append("fritekst", getTekst().substring(0, size)).
             toString();
     }

     /**
      * Sammenlikner med o.
      * @param o
      * @return true hvis relevante n�klene er like. 
      */
     @Override
     public boolean equals(Object o) {
         if(this == o) {
             return true;
         }
         if(null == o) {
             return false;
         }
         if(!(o instanceof Kjoretoylogg)) {
             return false;
         }
         Kjoretoylogg other = (Kjoretoylogg) o;
         return new EqualsBuilder().
             append(getId(), other.getId()).
             isEquals();
     }

     /**
      * Genererer identifikator hashkode.
      * @return koden.
      */
     @Override
     public int hashCode() {
         return new HashCodeBuilder(23,73).
             append(getId()).
             toHashCode();
     }

    @Transient
    public String getTextForGUI() {
    	if(getOpprettetDato() == null) {
    		return getOpprettetDato() + " av " + getOpprettetAv();
    	}
    	else {
    		return String.valueOf(getId());
    	}
    }

    public int compareTo(Kjoretoylogg o) {
        CompareToBuilder comparer = new CompareToBuilder();
        
        Date otherDate = null;
        Date myDate = getOpprettetDato(); 
        if(o.getOpprettetDato() != null) {
        	otherDate = o.getOpprettetDato();
        }
        
        // sammenlikn etter dato hvis begge har den
        if(myDate != null && otherDate != null) {
        	comparer.append(myDate, otherDate);
        }
        // bruk nøkkelen hvis begge  har den
        else if(id != null && o.getId() != null) {
        	comparer.append(getId(), o.getId());
        }
        // denne er den nyeste og er ennå ikke serialisert(mangler dato)
        else if(myDate == null) {
        	comparer.append(Clock.now(), otherDate);
        }
        // den andre er den nyeste
        else if(otherDate == null) {
        	comparer.append(myDate, Clock.now());
        }
        return comparer.toComparison();
    }
}
