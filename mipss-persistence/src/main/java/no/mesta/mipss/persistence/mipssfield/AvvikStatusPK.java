package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * PK klasse for punktreg status
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class AvvikStatusPK implements Serializable {
    private String avvikGuid;
    private Date opprettetDato;

    /**
     * Konstrukt�r
     * 
     */
    public AvvikStatusPK() {
    }

    /**
     * Konstrukt�r
     * 
     * @param avvikGuid
     * @param status
     */
    public AvvikStatusPK(String avvikGuid, Date opprettetDato) {
        this.avvikGuid = avvikGuid;
        this.opprettetDato = opprettetDato;
    }

    /**
     * Punktreg
     * 
     * @param avvikGuid
     */
    public void setAvvikGuid(String avvikGuid) {
        this.avvikGuid = avvikGuid;
    }

    /**
     * Punktreg
     * 
     * @return
     */
    public String getAvvikGuid() {
        return avvikGuid;
    }

    /**
     * Status
     * 
     * @param status
     */
    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    /**
     * Status
     * 
     * @return
     */
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("avvikGuid", avvikGuid).
            append("opprettetDato", opprettetDato).
            toString();
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof AvvikStatusPK)) {return false;}
        
        AvvikStatusPK other = (AvvikStatusPK) o;
        
        return new EqualsBuilder().
            append(opprettetDato, other.getOpprettetDato()).
            append(avvikGuid, other.getAvvikGuid()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2075,2025).append(opprettetDato).append(avvikGuid).toHashCode();
    }
}
