package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Tilgangsnivaa.FIND_ALL, query = "select o from Tilgangsnivaa o")
public class Tilgangsnivaa extends MipssEntityBean<Tilgangsnivaa> implements Serializable, IRenderableMipssEntity, Comparable<Tilgangsnivaa> {
	public static final String FIND_ALL = "Tilgangsnivaa.findAll";
	
	public final static Tilgangsnivaa NO_ACCESS = new Tilgangsnivaa("INGEN");
	private final static List<Tilgangsnivaa> ORDER = new ArrayList<Tilgangsnivaa>();
	public final static Tilgangsnivaa READ = new Tilgangsnivaa("LESE");
	public final static Tilgangsnivaa WRITE = new Tilgangsnivaa("SKRIVE");
	private List<BrukerTilgang> brukerTilgangList;
	private String nivaa;
	private List<RolleTilgang> rolleTilgangList;
	static {
		ORDER.add(READ);
		ORDER.add(WRITE);
		ORDER.add(NO_ACCESS);
	}
	private String beskrivelse;

	/**
	 * Konstruktør
	 * 
	 */
	public Tilgangsnivaa() {
	}

	/**
	 * Konstruktør
	 * 
	 * @param nivaa
	 */
	public Tilgangsnivaa(String nivaa) {
		this.nivaa = nivaa;
	}

	public BrukerTilgang addBrukerTilgang(BrukerTilgang brukerTilgang) {
		getBrukerTilgangList().add(brukerTilgang);
		brukerTilgang.setTilgangsnivaa(this);
		return brukerTilgang;
	}

	public RolleTilgang addRolleTilgang(RolleTilgang rolleTilgang) {
		getRolleTilgangList().add(rolleTilgang);
		rolleTilgang.setTilgangsnivaa(this);
		return rolleTilgang;
	}

	/**
	 * 
	 * @param o
	 * @return
	 */
	public int compareTo(Tilgangsnivaa other) {
		if(other == null) {
			return 1;
		}
		
		int orderThis = ORDER.indexOf(this);
		int orderOther = ORDER.indexOf(other);
		
		return new CompareToBuilder().append(orderThis, orderOther).toComparison();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Tilgangsnivaa)) {
			return false;
		}

		Tilgangsnivaa other = (Tilgangsnivaa) o;

		return new EqualsBuilder().append(nivaa, other.getNivaa()).isEquals();
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	@OneToMany(mappedBy = "tilgangsnivaa")
	public List<BrukerTilgang> getBrukerTilgangList() {
		return brukerTilgangList;
	}

	@Id
	@Column(nullable = false)
	public String getNivaa() {
		return nivaa;
	}

	@OneToMany(mappedBy = "tilgangsnivaa")
	public List<RolleTilgang> getRolleTilgangList() {
		return rolleTilgangList;
	}

	@Transient
	public String getTextForGUI() {
		return getNivaa();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(13, 11).append(nivaa).toHashCode();
	}

	public BrukerTilgang removeBrukerTilgang(BrukerTilgang brukerTilgang) {
		getBrukerTilgangList().remove(brukerTilgang);
		brukerTilgang.setTilgangsnivaa(null);
		return brukerTilgang;
	}

	public RolleTilgang removeRolleTilgang(RolleTilgang rolleTilgang) {
		getRolleTilgangList().remove(rolleTilgang);
		rolleTilgang.setTilgangsnivaa(null);
		return rolleTilgang;
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

	public void setBrukerTilgangList(List<BrukerTilgang> brukerTilgangList) {
		this.brukerTilgangList = brukerTilgangList;
	}

	public void setNivaa(String nivaa) {
		String old = this.nivaa;
		this.nivaa = nivaa;
		firePropertyChange("nivaa", old, nivaa);
	}

	public void setRolleTilgangList(List<RolleTilgang> rolleTilgangList) {
		this.rolleTilgangList = rolleTilgangList;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("nivaa", nivaa).append("beskrivelse", beskrivelse).toString();
	}
}
