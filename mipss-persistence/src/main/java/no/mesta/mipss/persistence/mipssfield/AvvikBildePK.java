package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Pk klasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class AvvikBildePK implements Serializable {
    public String bildeGuid;
    public String avvikGuid;

    /**
     * Konstrukt�r
     * 
     */
    public AvvikBildePK() {
    }

    /**
     * Konstrukt�r
     * 
     * @param bildeGuid
     * @param avvikGuid
     */
    public AvvikBildePK(String bildeGuid, String avvikGuid) {
        this.bildeGuid = bildeGuid;
        this.avvikGuid = avvikGuid;
    }

    /**
     * Bilde
     * 
     * @param bildeGuid
     */
    public void setBildeGuid(String bildeGuid) {
        this.bildeGuid = bildeGuid;
    }

    /**
     * Bilde
     * 
     * @return
     */
    public String getBildeGuid() {
        return bildeGuid;
    }

    /**
     * Avvik
     * 
     * @param avvikGuid
     */
    public void setAvvikGuid(String avvikGuid) {
        this.avvikGuid = avvikGuid;
    }

    /**
     * Avvik
     * 
     * @return
     */
    public String getAvvikGuid() {
        return avvikGuid;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("bildeGuid", bildeGuid).
            append("avvikGuid", avvikGuid).
            toString();
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(4009, 5091).append(bildeGuid).
            append(avvikGuid).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof AvvikBildePK)) {return false;}
        
        AvvikBildePK other = (AvvikBildePK) o;
        
        return new EqualsBuilder().append(bildeGuid, other.getBildeGuid()).
            append(avvikGuid, other.getAvvikGuid()).isEquals();
    }
}
