package no.mesta.mipss.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.inndata.Prodpunkt;

@SuppressWarnings("serial")
@Entity
@Table(name = "DIGIDOK")
@NamedQueries({
    @NamedQuery(name = Digidok.QUERY_FIND_ALL, query = "SELECT p FROM Digidok p")
    })
public class Digidok extends MipssEntityBean<Digidok> {
    public static final String QUERY_FIND_ALL = "Digidok.findAll";

	@Id
	@Column(nullable = false, name="BIT_NR")
	private Long bitNr;
	@Column(name="FRITEKST")
	private String fritekst;
	
	/**
	 * @return the bitNr
	 */
	public Long getBitNr() {
		return bitNr;
	}
	/**
	 * @param bitNr the bitNr to set
	 */
	public void setBitNr(Long bitNr) {
		Long old = this.bitNr;
		this.bitNr = bitNr;
		getProps().firePropertyChange("bitNr", old, bitNr);
	}
	/**
	 * @return the fritekst
	 */
	public String getFritekst() {
		return fritekst;
	}
	/**
	 * @param fritekst the fritekst to set
	 */
	public void setFritekst(String fritekst) {
		String old = this.fritekst;
		this.fritekst = fritekst;
		getProps().firePropertyChange("fritekst", old, fritekst);
	}
	
	
	

}
