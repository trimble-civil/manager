package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

public class KapasitetRapportObject implements Serializable{
	
	private String regionNavn;
	private String kontraktId;
	private String kontraktnummer;
	private String kontraktNavn;
	private Long kjoretoyId;
	private String kjoretoyNavn;
	private String kjoretoyType;
	private String kjoretoyMerke;
	private String ansvarligKontrakt;
	private String leverandor;
	private String spredertype;
	private String sensortypeStroing;
	private String sensortypeBroyting;
	private String aktivitet;
	private String enhetStroing;
	private Double totalTimer;
	private Double totalKm;
	private Double produksjonTimer;
	private Double produksjonKm;
	private Double tomkjoringTimer;
	private Double tomkjoringKm;
	private Double stroingTimer;
	private Double stroingKm;
	private Double stromengde;
	private Double stroingPrTime;
	private Double effektivStroingPrTime;
	private Double stroingPrKm;
	private Double broytingTimer;
	private Double broytingKm;
	private Double broytingPrTime;
	private Double effektivBroytingPrTime;
	private String utstyr;
	private String sensortypeAnnen;
	private String enhetAnnen;
	private Double annenMengde;
	private Double annenKapasitet;
	private Double annenTimer;
	
	public static final String[] getPropertiesArray(){
		return new String[]{"regionNavn",
							"kontraktId", 
							"kontraktnummer", 
							"kontraktNavn", 
							"kjoretoyId", 
							"kjoretoyNavn", 
							"kjoretoyType",
							"kjoretoyMerke", 
							"ansvarligKontrakt",
							"leverandor", 
							"spredertype",
							"sensortypeStroing",
							"sensortypeBroyting",
							"aktivitet",
							"enhetStroing",
							"totalTimer", 
							"totalKm", 
							"produksjonTimer", 
							"produksjonKm",
							"tomkjoringTimer", 
							"tomkjoringKm", 
							"stroingTimer", 
							"stroingKm",
							"stromengde",
							"stroingPrTime", 
							"effektivStroingPrTime",
							"stroingPrKm",
							"broytingTimer", 
							"broytingKm", 
							"broytingPrTime", 
							"effektivBroytingPrTime",
							"utstyr",
							"sensortypeAnnen",
							"enhetAnnen",
							"annenMengde",
							"annenKapasitet",
							"annenTimer"};
	}

	public String getRegionNavn() {
		return regionNavn;
	}

	public void setRegionNavn(String regionNavn) {
		this.regionNavn = regionNavn;
	}

	public String getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(String kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getKontraktnummer() {
		return kontraktnummer;
	}

	public void setKontraktnummer(String kontraktnummer) {
		this.kontraktnummer = kontraktnummer;
	}

	public String getKontraktNavn() {
		return kontraktNavn;
	}

	public void setKontraktNavn(String kontraktNavn) {
		this.kontraktNavn = kontraktNavn;
	}

	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public String getKjoretoyNavn() {
		return kjoretoyNavn;
	}

	public void setKjoretoyNavn(String kjoretoyNavn) {
		this.kjoretoyNavn = kjoretoyNavn;
	}

	public String getKjoretoyType() {
		return kjoretoyType;
	}

	public void setKjoretoyType(String kjoretoyType) {
		this.kjoretoyType = kjoretoyType;
	}

	public String getKjoretoyMerke() {
		return kjoretoyMerke;
	}

	public void setKjoretoyMerke(String kjoretoyMerke) {
		this.kjoretoyMerke = kjoretoyMerke;
	}

	public String getAnsvarligKontrakt() {
		return ansvarligKontrakt;
	}

	public void setAnsvarligKontrakt(String ansvarligKontrakt) {
		this.ansvarligKontrakt = ansvarligKontrakt;
	}

	public String getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}

	public String getSpredertype() {
		return spredertype;
	}

	public void setSpredertype(String spredertype) {
		this.spredertype = spredertype;
	}

	public String getSensortypeStroing() {
		return sensortypeStroing;
	}

	public void setSensortypeStroing(String sensortypeStroing) {
		this.sensortypeStroing = sensortypeStroing;
	}

	public String getSensortypeBroyting() {
		return sensortypeBroyting;
	}

	public void setSensortypeBroyting(String sensortypeBroyting) {
		this.sensortypeBroyting = sensortypeBroyting;
	}

	public String getAktivitet() {
		return aktivitet;
	}

	public void setAktivitet(String aktivitet) {
		this.aktivitet = aktivitet;
	}

	public String getEnhetStroing() {
		return enhetStroing;
	}

	public void setEnhetStroing(String enhetStroing) {
		this.enhetStroing = enhetStroing;
	}

	public Double getTotalTimer() {
		return totalTimer;
	}

	public void setTotalTimer(Double totalTimer) {
		this.totalTimer = totalTimer;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

	public Double getProduksjonTimer() {
		return produksjonTimer;
	}

	public void setProduksjonTimer(Double produksjonTimer) {
		this.produksjonTimer = produksjonTimer;
	}

	public Double getProduksjonKm() {
		return produksjonKm;
	}

	public void setProduksjonKm(Double produksjonKm) {
		this.produksjonKm = produksjonKm;
	}

	public Double getTomkjoringTimer() {
		return tomkjoringTimer;
	}

	public void setTomkjoringTimer(Double tomkjoringTimer) {
		this.tomkjoringTimer = tomkjoringTimer;
	}

	public Double getTomkjoringKm() {
		return tomkjoringKm;
	}

	public void setTomkjoringKm(Double tomkjoringKm) {
		this.tomkjoringKm = tomkjoringKm;
	}

	public Double getStroingTimer() {
		return stroingTimer;
	}

	public void setStroingTimer(Double stroingTimer) {
		this.stroingTimer = stroingTimer;
	}

	public Double getStroingKm() {
		return stroingKm;
	}

	public void setStroingKm(Double stroingKm) {
		this.stroingKm = stroingKm;
	}

	public Double getStromengde() {
		return stromengde;
	}

	public void setStromengde(Double stromengde) {
		this.stromengde = stromengde;
	}

	public Double getStroingPrTime() {
		return stroingPrTime;
	}

	public void setStroingPrTime(Double stroingPrTime) {
		this.stroingPrTime = stroingPrTime;
	}

	public Double getEffektivStroingPrTime() {
		return effektivStroingPrTime;
	}

	public void setEffektivStroingPrTime(Double effektivStroingPrTime) {
		this.effektivStroingPrTime = effektivStroingPrTime;
	}

	public Double getStroingPrKm() {
		return stroingPrKm;
	}

	public void setStroingPrKm(Double stroingPrKm) {
		this.stroingPrKm = stroingPrKm;
	}

	public Double getBroytingKm() {
		return broytingKm;
	}

	public void setBroytingKm(Double broytingKm) {
		this.broytingKm = broytingKm;
	}

	public Double getBroytingTimer() {
		return broytingTimer;
	}

	public void setBroytingTimer(Double broytingTimer) {
		this.broytingTimer = broytingTimer;
	}

	public Double getBroytingPrTime() {
		return broytingPrTime;
	}

	public void setBroytingPrTime(Double broytingPrTime) {
		this.broytingPrTime = broytingPrTime;
	}

	public Double getEffektivBroytingPrTime() {
		return effektivBroytingPrTime;
	}

	public void setEffektivBroytingPrTime(Double effektivBroytingPrTime) {
		this.effektivBroytingPrTime = effektivBroytingPrTime;
	}

	public String getUtstyr() {
		return utstyr;
	}

	public void setUtstyr(String utstyr) {
		this.utstyr = utstyr;
	}

	public String getSensortypeAnnen() {
		return sensortypeAnnen;
	}

	public void setSensortypeAnnen(String sensortypeAnnen) {
		this.sensortypeAnnen = sensortypeAnnen;
	}

	public String getEnhetAnnen() {
		return enhetAnnen;
	}

	public void setEnhetAnnen(String enhetAnnen) {
		this.enhetAnnen = enhetAnnen;
	}

	public Double getAnnenMengde() {
		return annenMengde;
	}

	public void setAnnenMengde(Double annenMengde) {
		this.annenMengde = annenMengde;
	}

	public Double getAnnenKapasitet() {
		return annenKapasitet;
	}

	public void setAnnenKapasitet(Double annenKapasitet) {
		this.annenKapasitet = annenKapasitet;
	}

	public Double getAnnenTimer() {
		return annenTimer;
	}

	public void setAnnenTimer(Double annenTimer) {
		this.annenTimer = annenTimer;
	}

}
