package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Prosess for et inspeksjonsøk
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "INSPEKSJON_PROSESS")
@IdClass(InspeksjonSokProsessPK.class)
public class InspeksjonSokProsess implements Serializable, IRenderableMipssEntity {
    private String inspeksjonGuid;
    private InspeksjonSokView inspeksjonSok;
    private Long prosessId;
    
    public InspeksjonSokProsess() {
    }

    public void setInspeksjonGuid(String inspeksjonGuid) {
        this.inspeksjonGuid = inspeksjonGuid;
    }

    @Id
    @Column(name = "INSPEKSJON_GUID", nullable = false, insertable = false, updatable = false)
    public String getInspeksjonGuid() {
        return inspeksjonGuid;
    }

    public void setProsessId(Long prosessId) {
        this.prosessId = prosessId;
    }
    
    public void setInspeksjonSok(InspeksjonSokView inspeksjonSok) {
        this.inspeksjonSok = inspeksjonSok;
        if(inspeksjonSok != null) {
            setInspeksjonGuid(inspeksjonSok.getGuid());
        } else {
            setInspeksjonGuid(null);
        }
    }
    
    @ManyToOne
    @JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID")
    public InspeksjonSokView getInspeksjonSok() {
        return inspeksjonSok;
    }

    @Id
    @Column(name = "PROSESS_ID", nullable = false, insertable = true, updatable = true)
    public Long getProsessId() {
        return prosessId;
    }
        
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof InspeksjonSokProsess)) {return false;}
        
        InspeksjonSokProsess other = (InspeksjonSokProsess) o;
        
        return new EqualsBuilder().append(inspeksjonGuid, other.getInspeksjonGuid()).append(prosessId, other.getProsessId()).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(inspeksjonGuid).append(prosessId).toHashCode();
    }

    /** {@inheritDoc} */
    public String getTextForGUI() {
        return "";
    }
}
