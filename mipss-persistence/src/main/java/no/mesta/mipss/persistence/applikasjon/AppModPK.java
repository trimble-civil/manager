package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * PublicKey for AppMod
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class AppModPK implements Serializable {
    public Long appId;
    public Long modulversjonId;

    /**
     * Konstruktør
     */
    public AppModPK() {
    }

    /**
     * Konstruktør
     * 
     * @param appId
     * @param modulversjonId
     */
    public AppModPK(Long appId, Long modulversjonId) {
        this.appId = appId;
        this.modulversjonId = modulversjonId;
    }

    /**
     * Applikasjonens id
     * 
     * @param appId
     */
    public void setAppId(Long appId) {
        this.appId = appId;
    }

    /**
     * Applikasjonens id
     * 
     * @return
     */
    public Long getAppId() {
        return appId;
    }

    /**
     * Modulversjonens id
     * 
     * @param modulversjonId
     */
    public void setModulversjonId(Long modulversjonId) {
        this.modulversjonId = modulversjonId;
    }

    /**
     * Modulversjonens id
     * 
     * @return
     */
    public Long getModulversjonId() {
        return modulversjonId;
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(!(o instanceof AppModPK)) { return false;}
        if(o == this) { return true;}
    
        AppModPK other = (AppModPK) o;
        return new EqualsBuilder().
            append(appId, other.getAppId()).
            append(modulversjonId, other.getModulversjonId()).
            isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(25,975).
            append(appId).
            append(modulversjonId).
            toHashCode();
    }
    
    /**
     * @see java.lang.Object#toString()
     * @return
     */
    @Override
    public String toString() {
        return getClass().getName() + "[" + appId + "," + modulversjonId + "]";
    }
}
