package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Akkumulert implements Serializable{
	private Long id;
	private Long driftkontraktId;
	private Date dogn;
	private String vei;
	private Long prodtypeId;
	private Long stroproduktId;
	private Double kjortKm;
	private Long kjortSekund;
	private Long materialeId;
	private Double stromengde;
	
	public static String[] getPropertiesArray(){
		return new String[]{"id", "driftkontraktId", "dogn", "vei", "prodtypeId", "stroproduktId", "kjortKm", "kjortSekund", "materialeId", "stromengde"};
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the driftkontraktId
	 */
	public Long getDriftkontraktId() {
		return driftkontraktId;
	}
	/**
	 * @param driftkontraktId the driftkontraktId to set
	 */
	public void setDriftkontraktId(Long driftkontraktId) {
		this.driftkontraktId = driftkontraktId;
	}
	/**
	 * @return the dogn
	 */
	public Date getDogn() {
		return dogn;
	}
	/**
	 * @param dogn the dogn to set
	 */
	public void setDogn(Date dogn) {
		this.dogn = dogn;
	}
	/**
	 * @return the vei
	 */
	public String getVei() {
		return vei;
	}
	/**
	 * @param vei the vei to set
	 */
	public void setVei(String vei) {
		this.vei = vei;
	}
	/**
	 * @return the prodtypeId
	 */
	public Long getProdtypeId() {
		return prodtypeId;
	}
	/**
	 * @param prodtypeId the prodtypeId to set
	 */
	public void setProdtypeId(Long prodtypeId) {
		this.prodtypeId = prodtypeId;
	}
	/**
	 * @return the stroproduktId
	 */
	public Long getStroproduktId() {
		return stroproduktId;
	}
	/**
	 * @param stroproduktId the stroproduktId to set
	 */
	public void setStroproduktId(Long stroproduktId) {
		this.stroproduktId = stroproduktId;
	}
	/**
	 * @return the kjortKm
	 */
	public Double getKjortKm() {
		return kjortKm;
	}
	/**
	 * @param kjortKm the kjortKm to set
	 */
	public void setKjortKm(Double kjortKm) {
		this.kjortKm = kjortKm;
	}
	/**
	 * @return the kjortSekund
	 */
	public Long getKjortSekund() {
		return kjortSekund;
	}
	/**
	 * @param kjortSekund the kjortSekund to set
	 */
	public void setKjortSekund(Long kjortSekund) {
		this.kjortSekund = kjortSekund;
	}
	/**
	 * @return the materialeId
	 */
	public Long getMaterialeId() {
		return materialeId;
	}
	/**
	 * @param materialeId the materialeId to set
	 */
	public void setMaterialeId(Long materialeId) {
		this.materialeId = materialeId;
	}
	/**
	 * @return the stromengde
	 */
	public Double getStromengde() {
		return stromengde;
	}
	/**
	 * @param stromengde the stromengde to set
	 */
	public void setStromengde(Double stromengde) {
		this.stromengde = stromengde;
	}
	
	
}
