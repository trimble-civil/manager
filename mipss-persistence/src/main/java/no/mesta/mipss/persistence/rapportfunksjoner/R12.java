package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class R12 implements Serializable, Rapp1Entity{
	private String rodenavn;
	private Long rodeId;
	private String veikategori;
	private Long veinummer;
	private Date fraDatoTid;
	private Date tilDatoTid;
	private Double totalKjortKm;
	private Long totalKjortSekund;
	private List<R12Prodtype> prodtyper;
	private List<R12Stroprodukt> stroprodukter;
	
	public static final String[] getPropertiesArray(){
		return new String[]{"rodenavn", "rodeId", "veikategori", "veinummer", "fraDatoTid", "tilDatoTid", "totalKjortKm", "totalKjortSekund", "prodtyper", "stroprodukter"};
	}
	public String getRodenavn() {
		return rodenavn;
	}

	public void setRodenavn(String rodenavn) {
		this.rodenavn = rodenavn;
	}

	public Long getRodeId() {
		return rodeId;
	}

	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}

	public String getVeikategori() {
		return veikategori;
	}

	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}

	public Long getVeinummer() {
		return veinummer;
	}

	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}

	public Date getFraDatoTid() {
		return fraDatoTid;
	}

	public void setFraDatoTid(Date fraDatoTid) {
		this.fraDatoTid = fraDatoTid;
	}

	public Date getTilDatoTid() {
		return tilDatoTid;
	}

	public void setTilDatoTid(Date tilDatoTid) {
		this.tilDatoTid = tilDatoTid;
	}

	public Double getTotalKjortKm() {
		return totalKjortKm;
	}

	public void setTotalKjortKm(Double totalKjortKm) {
		this.totalKjortKm = totalKjortKm;
	}

	public Long getTotalKjortSekund() {
		return totalKjortSekund;
	}

	public void setTotalKjortSekund(Long totalKjortSekund) {
		this.totalKjortSekund = totalKjortSekund;
	}

	public List<R12Prodtype> getProdtyper(){
		return prodtyper;
	}
	
	public void setProdtyper(List<R12Prodtype> prodtyper){
		this.prodtyper = prodtyper;
	}

	public void setStroprodukter(List<R12Stroprodukt> stroprodukter) {
		this.stroprodukter = stroprodukter;
	}

	public List<R12Stroprodukt> getStroprodukter() {
		return stroprodukter;
	}

	public List<Materiale> getMaterialer() {
		return null;
	}
	public Boolean getPaaKontrakt() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}

