package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = KontraktKjoretoyDogn.QUERY_FIND_ALL, query = "select o from KontraktKjoretoyDogn o"),
	@NamedQuery(name = KontraktKjoretoyDogn.QUERY_FIND_BY_KONTRAKT_AND_PERIODE, query = "select o from KontraktKjoretoyDogn o where o.kontraktId = :kontraktId and o.dogn >= :fraDogn and o.dogn <= :tilDogn")
})
@Table(name = "KONTRAKT_KJORETOY_DOGN")

/*
@SqlResultSetMappings( { 
	@SqlResultSetMapping(name = "kontraktkjoretoyDognResult", entities = { 
			@EntityResult(entityClass = KontraktKjoretoyDogn.class, fields = {
				@FieldResult(name = "kontraktId", column = "kontrakt_id"),
				@FieldResult(name = "kjoretoyId", column = "kjoretoy_id"),
				@FieldResult(name = "tomkjoringKm", column = "tomkjoring"),
				@FieldResult(name = "totalKm", column = "total")
			}) 
		})
	})
		*/
@IdClass(KontraktKjoretoyDognPK.class)
public class KontraktKjoretoyDogn extends MipssEntityBean<KontraktKjoretoyDogn> implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "KontraktKjoretoyDogn.findAll";
	public static final String QUERY_FIND_BY_KONTRAKT_AND_PERIODE = "KontraktKjoretoyDogn.findByKontraktAndPeriode";
	private static final String[] idFields = {"kontraktId", "kjoretoyId", "dogn"};
	private Long kontraktId;
	private Long kjoretoyId;
	private Date dogn;
	private Double tomkjoringKm;
	private Double totalKm;
	
	public KontraktKjoretoyDogn() {
	}
	
	@Id
	@Column(name = "KONTRAKT_ID")
	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	@Id
	@Column(name = "KJORETOY_ID")
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDogn() {
		return dogn;
	}

	public void setDogn(Date dogn) {
		this.dogn = dogn;
	}

	@Column(name = "TOMKJORING_KM")
	public Double getTomkjoringKm() {
		return tomkjoringKm;
	}

	public void setTomkjoringKm(Double tomkjoringKm) {
		this.tomkjoringKm = tomkjoringKm;
	}

	@Column(name = "TOTAL_KM")
	public Double getTotalKm() {
		return totalKm;
	}

	public void setTotalKm(Double totalKm) {
		this.totalKm = totalKm;
	}

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kontraktId).append(kjoretoyId).append(dogn).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktKjoretoyDogn)) {return false;}
        
        KontraktKjoretoyDogn other = (KontraktKjoretoyDogn) o;
        
        return new EqualsBuilder().append(kontraktId, other.kontraktId).append(kjoretoyId, other.kjoretoyId).append(dogn, other.dogn).isEquals();
    }
	
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("kontraktId", kontraktId).
            append("kjoretoyId", kjoretoyId).
            append("dogn", dogn).
            toString();
    }
    
	@Override
	public String[] getIdNames() {
		return idFields;
	}

	public String getTextForGUI() {
		return toString();
	}
}
