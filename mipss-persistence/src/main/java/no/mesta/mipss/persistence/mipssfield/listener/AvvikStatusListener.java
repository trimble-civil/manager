/**
 * 
 */
package no.mesta.mipss.persistence.mipssfield.listener;

import javax.persistence.PrePersist;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

/**
 * Lytter på lagring av AvvikStatus, setter opprettetDato og
 * lukketDato på Avvik hvis Status == Lukket
 * 
 * @see no.mesta.mipss.persistence.mipssfield.AvvikStatus
 * @see no.mesta.mipss.persistence.mipssfield.AvvikstatusType
 * @see no.mesta.mipss.persistence.mipssfield.Avvik
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class AvvikStatusListener<E extends AvvikStatus> {
	private static final Logger logger = LoggerFactory.getLogger(AvvikStatusListener.class);
	
	/**
	 * Setter opprettetDato hvis null, og sjekker hva statusen er.
	 * 
	 * @param ent
	 */
	@PrePersist
	public void monitorOpprett(E ent) {
		logger.debug("monitorOpprett(" + ent + ") start");
		
		if(ent.getOpprettetDato() == null) {
			ent.setOpprettetDato(Clock.now());
		}
		
		if(ent.getOpprettetAv() == null) {
			ent.setOpprettetAv("Ikke satt");
		}
		
		if(StringUtils.equals(ent.getAvvikstatusType().getStatus(), AvvikstatusType.LUKKET_STATUS)) {
			ent.getAvvik().setLukketDato(ent.getOpprettetDato());
		}
	}

}
