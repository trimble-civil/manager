package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.stroing.Stroperiode;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "installasjonIdSeq", sequenceName = "INSTALLASJON_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries({
	@NamedQuery(name = "Installasjon.findAll", query = "select o from Installasjon o"),
	@NamedQuery(name = Installasjon.QUERY_FIND_BY_KJORETOY, query = "select o from Installasjon o where o.kjoretoy.id = :id"),
	@NamedQuery(name = Installasjon.QUERY_FIND, query = "select i from Installasjon i where i.aktivTilDato>=:fraDato and i.aktivFraDato<=:tilDato and i.kjoretoy.id=:kjoretoyId " +
														"or i.aktivTilDato is null and i.aktivFraDato <=:tilDato and i.kjoretoy.id=:kjoretoyId")
})
@Table(name="INSTALLASJON")
//@SecondaryTable(name="MONTERINGSINFO", pkJoinColumns=@PrimaryKeyJoinColumn(name="INSTALLASJON_ID"))
public class Installasjon extends MipssEntityBean<Installasjon> implements Serializable, IRenderableMipssEntity {
    public static final String QUERY_FIND = "Installasjon.find";
    public static final String QUERY_FIND_BY_KJORETOY = "Installasjon.findByKjoretoy";
    
    private static final String[] ID_NAMES = {"id"};
    
	private Date aktivFraDato;
    private Date aktivTilDato;
    private Long dfuId;
    private String endretAv;
    private Date endretDato;
    private Long id;
    private String ignorerMaske;
    private String modellNavn;
    private String opprettetAv;
    private Date opprettetDato;
    private Kjoretoy kjoretoy;
    private Dfumodell dfumodell;
    private Dfuindivid dfuindivid;
    private Monteringsinfo monteringsinfo;
    
    public Installasjon() {
    }

    @Column(name="AKTIV_FRA_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getAktivFraDato() {
        return aktivFraDato;
    }

    public void setAktivFraDato(Date aktivFraDato) {
        Object oldValue = this.aktivFraDato;
        this.aktivFraDato = aktivFraDato;
        getProps().firePropertyChange("aktivFraDato", oldValue, this.aktivFraDato);
    }

    @Column(name="AKTIV_TIL_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getAktivTilDato() {
        return aktivTilDato;
    }

    public void setAktivTilDato(Date aktivTilDato) {
        Object oldValue = this.aktivTilDato;
        this.aktivTilDato = aktivTilDato;
        getProps().firePropertyChange("aktivTilDato", oldValue, this.aktivTilDato);
    }

    @Column(name="DFU_ID", nullable=false, insertable=false, updatable=false)
    public Long getDfuId() {
        return dfuId;
    }

    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    @Transient
    public String getEndretDatoString() {
        return MipssDateFormatter.formatDate(endretDato, false);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "installasjonIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="IGNORER_MASKE")
    public String getIgnorerMaske() {
        return ignorerMaske;
    }

    public void setIgnorerMaske(String ignorerMaske) {
        Object oldValue = this.ignorerMaske;
        this.ignorerMaske = ignorerMaske;
        getProps().firePropertyChange("ignorerMaske", oldValue, this.ignorerMaske);
    }

    @Column(name="MODELL_NAVN", nullable=false, insertable=false, updatable=false)
    public String getModellNavn() {
        return modellNavn;
    }

    public void setModellNavn(String modellNavn) {
        this.modellNavn = modellNavn;
    }

    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @Transient
    public String getOpprettetDatoString() {
        return MipssDateFormatter.formatDate(opprettetDato, false);
    }
    
    @ManyToOne
    @JoinColumn(name = "KJORETOY_ID", referencedColumnName = "ID")
    public Kjoretoy getKjoretoy() {
        return kjoretoy;
    }

    public void setKjoretoy(Kjoretoy kjoretoy) {
        this.kjoretoy = kjoretoy;
    }
    
    public void setDfumodell(Dfumodell dfumodell) {
        Object oldValue = this.dfumodell;
    	this.dfumodell = dfumodell;
        
        getProps().firePropertyChange("dfumodell", oldValue, this.dfumodell);
    }

    /**
     * Hvis installasjonen referer til et individ, skal modellen hentes fra
     * individet.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "MODELL_NAVN", referencedColumnName = "NAVN")
    public Dfumodell getDfumodell() {
        if (dfumodell == null && getDfuindivid() != null) {
            return getDfuindivid().getDfumodell();
        }
        return dfumodell;
    }

    public void setDfuindivid(Dfuindivid dfuindivid) {
    	Object oldModell = this.dfumodell;
    	Object oldValue = this.dfuindivid;
    	this.dfuindivid = dfuindivid;
    	
    	if(this.dfuindivid != null) {
    		setDfumodell(this.dfuindivid.getDfumodell());
    	}
        
        getProps().firePropertyChange("dfuindivid", oldValue, this.dfuindivid);
        
        //Fyrer dfumodell siden denne kan være endret
        getProps().firePropertyChange("dfumodell", oldModell, getDfumodell());
    }

    @ManyToOne
    @JoinColumn(name = "DFU_ID", referencedColumnName = "ID")
    public Dfuindivid getDfuindivid() {
        return dfuindivid;
    }
    
    // De neste metodene er h�ndkodet.
    /**
     * Finner ut om installasjonen er av et individ.
     * Dette objektet definerer en installasjon av et identifisert individ
     * eller av en generisk modell.
     * @return true hvis et individ er installert.
     */
    public boolean isIndividInstallasjon() {
    	Long dfuId = getDfuId();
        return (dfuId != null && dfuId > 0);
    }
    
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("Id", getId()).
            append("dfuId", getDfuId()).
            append("modellNavn", getModellNavn()).
            append("kjoretoyId", getKjoretoy()).
            append("aktivFraDato", getAktivFraDato()).
            append("aktivTilDato", getAktivTilDato()).
            toString();
    }

    /**
     * Sammenlikner med o og tar hensyn til at denne klassen kan v�re enten
     * en individuell- eller en modellinstallasjon.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Installasjon)) {
            return false;
        }
        Installasjon other = (Installasjon) o;
        if(!(isIndividInstallasjon() == other.isIndividInstallasjon())) {
            return false;
        }
        boolean special = false;
        if (isIndividInstallasjon()) {
            special = new EqualsBuilder().append(getDfuId(), other.getDfuId()).isEquals();
        }
        else {
            special = new EqualsBuilder().append(getModellNavn(), other.getModellNavn()).isEquals();
        }
        boolean common = new EqualsBuilder().
            append(getId(), other.getId()).
            append(getKjoretoy(), other.getKjoretoy()).
            isEquals();
        return common && special;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(31,13).
            append(getId()).
            append(getKjoretoy()).
            append(getDfuId()).
            append(getModellNavn()).
            toHashCode();
    }

    public String getTextForGUI() {
        return getModellNavn();
    }

    public int compareTo(Object o) {
        Installasjon other = (Installasjon) o;
        return new CompareToBuilder().
            append(getModellNavn(), other.getModellNavn()).
            toComparison();
    }

	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public void setMonteringsinfo(Monteringsinfo monteringsinfo) {
		this.monteringsinfo = monteringsinfo;
	}

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name = "ID", referencedColumnName = "INSTALLASJON_ID", insertable=false, updatable=false)
	public Monteringsinfo getMonteringsinfo() {
		return monteringsinfo;
	}

	
}
