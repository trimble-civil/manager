package no.mesta.mipss.persistence.rapportfunksjoner;

import java.util.List;

public class R12Object extends Rapp1Object{
	private final List<R12> data;
	
	public R12Object(List<R12> data){
		this.data = data;
		initData();
	}
	private void initData() {
		for (R12 r:data){
			
			km += r.getTotalKjortKm()!=null?r.getTotalKjortKm():0;
			sekund += r.getTotalKjortSekund();
			handleProdtyper(r.getProdtyper());
			handleStroprodukter(r.getStroprodukter());
		}
	}
	
	public List<R12> getR12Data(){
		return data;
	}
	@Override
	public String getRodenavn(){
		return data.get(0).getRodenavn();
	}
}
