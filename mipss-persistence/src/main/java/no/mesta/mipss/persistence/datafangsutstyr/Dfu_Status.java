package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;


import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "dfuStatusIdSeq", sequenceName = "DFU_STATUS_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQuery(name = "Dfu_Status.findAll", query = "select o from Dfu_Status o")
public class Dfu_Status implements Serializable, IRenderableMipssEntity {
    private Long id;
    private Long levbestId;
    private String opprettetAv;
    private Date opprettetDato;
    private Dfuindivid dfuindivid;
    private Dfustatus dfustatus;

    public Dfu_Status() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dfuStatusIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="LEVBEST_ID")
    public Long getLevbestId() {
        return levbestId;
    }

    public void setLevbestId(Long levbestId) {
        this.levbestId = levbestId;
    }

    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }


    @ManyToOne
    @JoinColumn(name = "DFU_ID", referencedColumnName = "ID")
    public Dfuindivid getDfuindivid() {
        return dfuindivid;
    }

    public void setDfuindivid(Dfuindivid dfuindivid) {
        this.dfuindivid = dfuindivid;
    }

    @ManyToOne
    @JoinColumn(name = "STATUS_NAVN", referencedColumnName = "NAVN")
    public Dfustatus getDfustatus() {
        return dfustatus;
    }

    public void setDfustatus(Dfustatus dfustatus) {
        this.dfustatus = dfustatus;
    }
    
    // de neste metodene er håndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entitybønnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("besttillings Id", getLevbestId()).
            append("opprettet av", getOpprettetAv()).
            append("opprettet dato", getOpprettetDato()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Dfu_Status)) {
            return false;
        }
        Dfu_Status other = (Dfu_Status) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            append(getOpprettetAv(), other.getOpprettetAv()).
            append(getOpprettetDato(), other.getOpprettetDato()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            append(getOpprettetAv()).
            append(getOpprettetDato()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return String.valueOf(getId());
    }

    public int compareTo(Object o) {
        Dfu_Status other = (Dfu_Status) o;
        CompareToBuilder c = new CompareToBuilder();
        if (dfustatus != null && other.getDfustatus() != null) {
            c.append(dfustatus.getNavn(), other.getDfustatus().getNavn());
        }
        else {
            c.append(getOpprettetDato(), other.getOpprettetDato());
        }
        return c.toComparison();
    }
    
    @Transient
    public String getStatusNavn() {
        if (dfustatus == null) {
            return "";
        }
        return dfustatus.getNavn();
    }
}
