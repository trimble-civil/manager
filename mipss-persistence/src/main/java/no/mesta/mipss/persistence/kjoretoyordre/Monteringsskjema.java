package no.mesta.mipss.persistence.kjoretoyordre;

import java.awt.Image;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.MipssDfuvare;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyKontaktperson;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

/**
 * 
 * DTO klasse som holder på alle dataene som trengs for å generere bestillings- og monteringsskjemaet.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class Monteringsskjema {
	
	private Kjoretoyordretype kjoretoyordretype;
	private KontraktKontakttype kontraktKontakttype;
	private String prosjektNr;
	private String ansvarNr;
	private Leverandor leverandor;
	private List<KjoretoyKontaktperson> kontaktPersonerList;
	private Kjoretoy kjoretoy;
	private String monteringsOpplysninger;
	private Dfumodell dfumodell;
	private Image logo;
	private Driftkontrakt driftkontrakt;
	
	private Boolean sendesEkspress;
	private String navn;
	private String addresse;
	private String postnr;
	private String poststed;
	private String forsendelseMerknad;
	private List<? extends MipssDfuvare> mipssDfuvareList;
	
	private String installasjonKommentar;
	private String plasseringsnotat;
	private String spenningTenning;
	private String spenningPluss;
	private String spenningJord;
	private String montornavn;
	private String installasjonsted;
	private Date signeringsdato;
	
	private String dfuSerienummer;
	private String dfuTelefonnummer;
	private String ordreId;
	private Installasjon installasjon;
	private List<KjoretoyordreUtstyr> kjoretoyordreUtstyrList;
	
	private Boolean maaHaBryterboks;
	/**
	 * Logoen skal være IconResources.MESTA_HOVEDLOGO.getImage(), denne ligger som parameter til klassen
	 * for å unngå kobling mot klientside prosjekter. (som: studio-resources)
	 * @param logo
	 */
	public Monteringsskjema(Image logo){
		this.logo = logo;
	}
	public Kjoretoyordretype getKjoretoyordretype() {
		return kjoretoyordretype;
	}
	public KontraktKontakttype getKontraktKontakttype() {
		return kontraktKontakttype;
	}
	public String getProsjektNr() {
		return prosjektNr;
	}
	public String getAnsvarNr() {
		return ansvarNr;
	}
	public Leverandor getLeverandor() {
		return leverandor;
	}
	public List<KjoretoyKontaktperson> getKontaktPersonerList() {
		return kontaktPersonerList;
	}
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}
	public String getMonteringsOpplysninger() {
		return monteringsOpplysninger;
	}
	public Dfumodell getDfumodell() {
		return dfumodell;
	}
	public void setKjoretoyordretype(Kjoretoyordretype kjoretoyordretype) {
		this.kjoretoyordretype = kjoretoyordretype;
	}
	public void setKontraktKontakttype(KontraktKontakttype kontraktKontakttype) {
		this.kontraktKontakttype = kontraktKontakttype;
	}
	public void setProsjektNr(String prosjektNr) {
		this.prosjektNr = prosjektNr;
	}
	public void setAnsvarNr(String ansvarNr) {
		this.ansvarNr = ansvarNr;
	}
	public void setLeverandor(Leverandor leverandor) {
		this.leverandor = leverandor;
	}
	public void setKontaktPersonerList(
			List<KjoretoyKontaktperson> kontaktPersonerList) {
		this.kontaktPersonerList = kontaktPersonerList;
	}
	public void setKjoretoy(Kjoretoy kjoretoy) {
		this.kjoretoy = kjoretoy;
	}
	public void setMonteringsOpplysninger(String monteringsOpplysninger) {
		this.monteringsOpplysninger = monteringsOpplysninger;
	}
	public void setDfumodell(Dfumodell dfumodell) {
		this.dfumodell = dfumodell;
	}
	
	public Image getLogo(){
		return logo;
	}
	
	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
	}
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}
	public Boolean getSendesEkspress() {
		return sendesEkspress;
	}
	public void setSendesEkspress(Boolean sendesEkspress) {
		this.sendesEkspress = sendesEkspress;
	}
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
	public String getAddresse() {
		return addresse;
	}
	public void setAddresse(String addresse) {
		this.addresse = addresse;
	}
	public String getPostnr() {
		return postnr;
	}
	public void setPostnr(String postnr) {
		this.postnr = postnr;
	}
	public String getPoststed() {
		return poststed;
	}
	public void setPoststed(String poststed) {
		this.poststed = poststed;
	}
	public String getForsendelseMerknad() {
		return forsendelseMerknad;
	}
	public void setForsendelseMerknad(String forsendelseMerknad) {
		this.forsendelseMerknad = forsendelseMerknad;
	}
	public List<? extends MipssDfuvare> getMipssDfuvareList() {
		return mipssDfuvareList;
	}
	public void setMipssDfuvareList(List<? extends MipssDfuvare> mipssDfuvareList) {
		this.mipssDfuvareList = mipssDfuvareList;
	}
	public Monteringsskjema getThisForReport(){
		return this;
	}
	
	public Double getSumVarePris() {
		Double sum = 0d;
		
		if(mipssDfuvareList != null) {
			for(MipssDfuvare mipssDfuvare : mipssDfuvareList) {
				sum += mipssDfuvare.getAntall() * mipssDfuvare.getDfuvarekatalog().getPris();
			}
		}
		
		return sum;
	}
	public String getInstallasjonKommentar() {
		return installasjonKommentar;
	}
	public void setInstallasjonKommentar(String installasjonKommentar) {
		this.installasjonKommentar = installasjonKommentar;
	}
	public String getPlasseringsnotat() {
		return plasseringsnotat;
	}
	public void setPlasseringsnotat(String plasseringsnotat) {
		this.plasseringsnotat = plasseringsnotat;
	}
	public String getSpenningTenning() {
		return spenningTenning;
	}
	public void setSpenningTenning(String spenningTenning) {
		this.spenningTenning = spenningTenning;
	}
	public String getSpenningPluss() {
		return spenningPluss;
	}
	public void setSpenningPluss(String spenningPluss) {
		this.spenningPluss = spenningPluss;
	}
	public String getSpenningJord() {
		return spenningJord;
	}
	public void setSpenningJord(String spenningJord) {
		this.spenningJord = spenningJord;
	}
	public String getMontornavn() {
		return montornavn;
	}
	public void setMontornavn(String montornavn) {
		this.montornavn = montornavn;
	}
	public String getInstallasjonsted() {
		return installasjonsted;
	}
	public void setInstallasjonsted(String installasjonsted) {
		this.installasjonsted = installasjonsted;
	}
	public Date getSigneringsdato() {
		return signeringsdato;
	}
	public void setSigneringsdato(Date signeringsdato) {
		this.signeringsdato = signeringsdato;
	}
	public String getDfuSerienummer() {
		return dfuSerienummer;
	}
	public void setDfuSerienummer(String dfuSerienummer) {
		this.dfuSerienummer = dfuSerienummer;
	}
	public String getDfuTelefonnummer() {
		return dfuTelefonnummer;
	}
	public void setDfuTelefonnummer(String dfuTelefonnummer) {
		this.dfuTelefonnummer = dfuTelefonnummer;
	}
	public Installasjon getInstallasjon() {
		return installasjon;
	}
	public void setInstallasjon(Installasjon installasjon) {
		this.installasjon = installasjon;
	}
	public List<KjoretoyordreUtstyr> getKjoretoyordreUtstyrList() {
		return kjoretoyordreUtstyrList;
	}
	public void setKjoretoyordreUtstyrList(
			List<KjoretoyordreUtstyr> kjoretoyordreUtstyrList) {
		this.kjoretoyordreUtstyrList = kjoretoyordreUtstyrList;
	}
	public void setOrdreId(String ordreId) {
		this.ordreId = ordreId;
	}
	public String getOrdreId() {
		return ordreId;
	}
	public void setMaaHaBryterboks(Boolean maaHaBryterboks) {
		this.maaHaBryterboks = maaHaBryterboks;
	}
	public Boolean getMaaHaBryterboks() {
		return maaHaBryterboks;
	}
}