package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssFetch;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Rolle for brukere
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = "SEQ", sequenceName = "ROLLE_ID_SEQ", allocationSize = 1)
@NamedQueries( { @NamedQuery(name = Rolle.QUERY_ROLLE_FIND_ALL, query = "SELECT r FROM Rolle r") })
public class Rolle extends MipssEntityBean<Rolle> implements Serializable, IRenderableMipssEntity, Comparable<Rolle>,
		PropertyChangeSource {
	private static final String FIELD_BESKRIVELSE = "beskrivelse";
	private static final String FIELD_ENDRETAV = "endretAv";
	private static final String FIELD_ENDRETDATO = "endretDato";
	private static final String FIELD_ID = "id";
	private static final String FIELD_NAVN = "navn";
	private static final String FIELD_OPPRETTETAV = "opprettetAv";
	private static final String FIELD_OPPRETTETDATO = "opprettetDato";
	private static final String FIELD_ROLLEBRUKERLIST = "rolleBrukerList";
	private static final String FIELD_ROLLEMENYLIST = "rollemenyList";
	private static final String FIELD_ROLLETILGANGLIST = "rolleTilgangList";
	private static final String[] ID_NAMES = { FIELD_ID };
	public static final String QUERY_ROLLE_FIND_ALL = "Rolle.findAll";
	private String beskrivelse;
	private String endretAv;
	private Date endretDato;
	private Long id;
	private String navn;
	private String opprettetAv;
	private Date opprettetDato;
	private List<RolleBruker> rolleBrukerList = new ArrayList<RolleBruker>();
	private List<RolleTilgang> rolleTilgangList = new ArrayList<RolleTilgang>();

	public Rolle() {
	}

	public RolleBruker addRolleBruker(RolleBruker rolleBruker) {
		List<RolleBruker> old = new ArrayList<RolleBruker>(this.rolleBrukerList);
		getRolleBrukerList().add(rolleBruker);
		rolleBruker.setRolle(this);

		getProps().firePropertyChange(FIELD_ROLLEBRUKERLIST, old, rolleBrukerList);
		return rolleBruker;
	}

	public RolleTilgang addRolleTilgang(RolleTilgang rolleTilgang) {
		List<RolleTilgang> old = new ArrayList<RolleTilgang>(this.rolleTilgangList);
		getRolleTilgangList().add(rolleTilgang);
		rolleTilgang.setRolle(this);

		getProps().firePropertyChange(FIELD_ROLLETILGANGLIST, old, rolleTilgangList);
		return rolleTilgang;
	}

	/** {@inheritDoc} */
	public int compareTo(Rolle other) {
		if (other == null) {
			return 1;
		}

		return new CompareToBuilder().append(navn, other.getNavn()).toComparison();
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@OneToMany(mappedBy = "rolle", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MipssFetch()
	public List<RolleBruker> getRolleBrukerList() {
		return rolleBrukerList;
	}

	@MipssFetch()
	@OneToMany(mappedBy = "rolle", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	public List<RolleTilgang> getRolleTilgangList() {
		return rolleTilgangList;
	}

	/** {@inheritDoc} */
	@Transient
	public String getTextForGUI() {
		return getNavn();
	}

	public RolleBruker removeRolleBruker(RolleBruker rolleBruker) {
		List<RolleBruker> old = new ArrayList<RolleBruker>(this.rolleBrukerList);
		getRolleBrukerList().remove(rolleBruker);

		getProps().firePropertyChange(FIELD_ROLLEBRUKERLIST, old, rolleBrukerList);
		return rolleBruker;
	}

	public RolleTilgang removeRolleTilgang(RolleTilgang rolleTilgang) {
		List<RolleTilgang> old = new ArrayList<RolleTilgang>(this.rolleTilgangList);
		getRolleTilgangList().remove(rolleTilgang);

		getProps().firePropertyChange(FIELD_ROLLETILGANGLIST, old, rolleTilgangList);
		return rolleTilgang;
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		getProps().firePropertyChange(FIELD_BESKRIVELSE, old, beskrivelse);
	}

	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		getProps().firePropertyChange(FIELD_ENDRETAV, old, endretAv);
	}

	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		getProps().firePropertyChange(FIELD_ENDRETDATO, old, endretDato);
	}

	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		getProps().firePropertyChange(FIELD_ID, old, id);
	}

	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		getProps().firePropertyChange(FIELD_NAVN, old, navn);
	}

	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		getProps().firePropertyChange(FIELD_OPPRETTETAV, old, opprettetAv);
	}

	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		getProps().firePropertyChange(FIELD_OPPRETTETDATO, old, opprettetDato);
	}

	public void setRolleBrukerList(List<RolleBruker> rolleBrukerList) {
		List<RolleBruker> old = this.rolleBrukerList;
		this.rolleBrukerList = rolleBrukerList;
//		getProps().firePropertyChange(FIELD_ROLLEBRUKERLIST, old, rolleBrukerList);
	}

	public void setRolleTilgangList(List<RolleTilgang> rolleTilgangList) {
		List<RolleTilgang> old = this.rolleTilgangList;
		this.rolleTilgangList = rolleTilgangList;
//		getProps().firePropertyChange(FIELD_ROLLETILGANGLIST, old, rolleTilgangList);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(FIELD_ID, getId()).append(FIELD_NAVN, getNavn()).append(
				FIELD_BESKRIVELSE, getBeskrivelse()).toString();
	}
}
