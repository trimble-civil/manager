package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class KontraktLeverandorPK implements Serializable {
	private Long kontraktId;
	private String leverandorNr;
	private Date fraDato;
	
	public KontraktLeverandorPK() {
	}
	
	public KontraktLeverandorPK(Long kontraktId, String leverandorNr, Date fraDato) {
		this.kontraktId = kontraktId;
		this.leverandorNr = leverandorNr;
		this.fraDato = fraDato;
	}
	
	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getLeverandorNr() {
		return leverandorNr;
	}

	public void setLeverandorNr(String leverandorNr) {
		this.leverandorNr = leverandorNr;
	}

	public Date getFraDato() {
		return fraDato;
	}

	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kontraktId).append(leverandorNr).append(fraDato).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KontraktLeverandorPK)) {return false;}
        
        KontraktLeverandorPK other = (KontraktLeverandorPK) o;
        
        return new EqualsBuilder().append(kontraktId, other.kontraktId).append(leverandorNr, other.leverandorNr).append(fraDato, other.fraDato).isEquals();
    }
}
