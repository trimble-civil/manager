package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * N�kkel klasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class Kjoretoyordre_StatusPK implements Serializable {
    private Long ordreId;
    private String statusIdent;

    public Kjoretoyordre_StatusPK() {
    }

    public Kjoretoyordre_StatusPK(Long ordreId, String statusIdent) {
        this.ordreId = ordreId;
        this.statusIdent = statusIdent;
    }

    public void setOrdreId(Long ordreId) {
        this.ordreId = ordreId;
    }

    public Long getOrdreId() {
        return ordreId;
    }

    public void setStatusIdent(String statusIdent) {
        this.statusIdent = statusIdent;
    }

    public String getStatusIdent() {
        return statusIdent;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ordreId).append(statusIdent).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Kjoretoyordre_StatusPK)) {return false;}
        
        Kjoretoyordre_StatusPK other = (Kjoretoyordre_StatusPK) o;
        
        return new EqualsBuilder().append(ordreId, other.ordreId).append(statusIdent, other.statusIdent).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("ordreId", ordreId).
            append("statusIdent", statusIdent).
            toString();
    }
}
