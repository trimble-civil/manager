package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Kontrakttype.FIND_ALL, query = "select o from Kontrakttype o order by o.navn")

public class Kontrakttype extends MipssEntityBean<Kontrakttype> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kontrakttype.findAll";
	private String navn;
	private String beskrivelse;
	
	@Id
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}
	
    // de neste metodene er håndkodet
    /**
     * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
     * @param o
     * @return true hvis nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Kontrakttype)) {
            return false;
        }
        Kontrakttype other = (Kontrakttype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert på Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,13).
            append(getNavn()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getNavn() + " - " + getBeskrivelse();
    }

    public int compareTo(Object o) {
        Kontrakttype other = (Kontrakttype) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
    
}
