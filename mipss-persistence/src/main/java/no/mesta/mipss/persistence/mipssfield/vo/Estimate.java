package no.mesta.mipss.persistence.mipssfield.vo;

import java.io.Serializable;

/**
 * Klasse for bruk i rapporter for anslag
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class Estimate implements Serializable {
	private Double estimate;
	private Object key;
	private String kommentar;
	private String name;

	/**
	 * Hjelpe konstruktør for testing
	 * 
	 * @param e
	 * @param n
	 */
	public Estimate(Double e, String n) {
		key = new Object();
		estimate = e;
		name = n;
	}


	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		return key.equals(o);
	}

	/**
	 * Estimatet
	 * 
	 * @return
	 */
	public Double getEstimate() {
		return estimate;
	}

	/**
	 * Kommentar for estimatet
	 * 
	 * @return
	 */
	public String getKommentar() {
		return kommentar;
	}

	/**
	 * Navn for typen
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return key.hashCode();
	}

	/**
	 * Kommentar for estimatet
	 * 
	 * @param kommentar
	 */
	public void setKommentar(String kommentar) {
		this.kommentar = kommentar;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return name + " " + estimate;
	}
}
