package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
@Entity
@Table(name = "dummy_eksisterer_ikkeXXH21")
@SqlResultSetMappings({
	@SqlResultSetMapping(name=NokkeltallFelt.FIELD_RESULT_MAPPING, entities = {
		@EntityResult(entityClass = NokkeltallFelt.class, fields = {
			@FieldResult(name = "kontraktId", column = "KONTRAKT_ID"),
			@FieldResult(name = "kontraktnavn", column = "KONTRAKTNAVN"),
			@FieldResult(name = "kontraktveinettKm", column = "KONTRAKTVEINETT_KM"),
			@FieldResult(name = "regionId", column = "REGION_ID"),
			@FieldResult(name = "regionnavn", column = "REGIONNAVN"),
			@FieldResult(name = "fraDato", column = "FRA_DATO"),
			@FieldResult(name = "tilDato", column = "TIL_DATO"),
			@FieldResult(name = "antInspOpprettetAlle", column = "ANT_INSP_OPPRETTET_ALLE"),
			@FieldResult(name = "antInspOpprettet", column = "ANT_INSP_OPPRETTET"),
			@FieldResult(name = "antR2OpprettetAlle", column = "ANT_R2_OPPRETTET_ALLE"),
			@FieldResult(name = "antR2RapportertAlle", column = "ANT_R2_RAPPORTERT_ALLE"),
			@FieldResult(name = "antR2Opprettet", column = "ANT_R2_OPPRETTET"),
			@FieldResult(name = "antR2Rapportert", column = "ANT_R2_RAPPORTERT"),
			@FieldResult(name = "antR5OpprettetAlle", column = "ANT_R5_OPPRETTET_ALLE"),
			@FieldResult(name = "antR5RapportertAlle", column = "ANT_R5_RAPPORTERT_ALLE"),
			@FieldResult(name = "antR5Opprettet", column = "ANT_R5_OPPRETTET"),
			@FieldResult(name = "antR5Rapportert", column = "ANT_R5_RAPPORTERT"),
			@FieldResult(name = "antR11OpprettetAlle", column = "ANT_R11_OPPRETTET_ALLE"),
			@FieldResult(name = "antR11RapportertAlle", column = "ANT_R11_RAPPORTERT_ALLE"),
			@FieldResult(name = "antR11Opprettet", column = "ANT_R11_OPPRETTET"),
			@FieldResult(name = "antR11Rapportert", column = "ANT_R11_RAPPORTERT"),
			@FieldResult(name = "antTilleggsjobberAlle", column = "ANT_TILLEGGSJOBBER_ALLE"),
			@FieldResult(name = "antTilleggsjobber", column= "ANT_TILLEGGSJOBBER"),
			@FieldResult(name = "antAvvikOpprettetAlle", column= "ANT_AVVIK_OPPRETTET_ALLE"),
			@FieldResult(name = "antAvvikLukketAlle", column="ANT_AVVIK_LUKKET_ALLE"),
			@FieldResult(name = "antAvvikOpprettet", column="ANT_AVVIK_OPPRETTET"),
			@FieldResult(name = "antAvvikLukket", column="ANT_AVVIK_LUKKET"),
			@FieldResult(name = "antManglerAlle", column="ANT_MANGLER_ALLE"), //*
			@FieldResult(name = "antManglerLukketAlle", column="ANT_MANGLER_LUKKET_ALLE"), //*
			@FieldResult(name = "antMangler", column="ANT_MANGLER"),
			@FieldResult(name = "antManglerAapne", column="ANT_MANGLER_AAPNE"), //*
			@FieldResult(name = "antManglerLukket", column="ANT_MANGLER_LUKKET"),
			@FieldResult(name = "antDauBestAlle", column="ANT_DAU_BEST_ALLE"),
			@FieldResult(name = "antDauBest", column="ANT_DAU_BEST"),
			@FieldResult(name = "antRetardasjonAlle", column="ANT_RETARDASJON_ALLE"),
			@FieldResult(name = "kmKontFriskjonTotalt", column="KM_KONT_FRIKSJON_TOTALT"),
			@FieldResult(name = "antRetardasjon", column="ANT_RETARDASJON"),
			@FieldResult(name = "kmKontFriksjon", column="KM_KONT_FRIKSJON")
		})
	})
})
public class NokkeltallFelt implements Serializable, IRenderableMipssEntity{
	public static final String FIELD_RESULT_MAPPING = "NokkeltallFeltResult";
	
	@Id @Column(name="KONTRAKT_ID")
	private Long kontraktId;
	private String kontraktnavn;
	@Column(name="KONTRAKTVEINETT_KM")
	private Double kontraktveinettKm;
	@Column(name="REGION_ID")
	private Long regionId;
	private String regionnavn;
	@Column(name="FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fraDato;
	@Column(name="TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tilDato;
	@Column(name="ANT_INSP_OPPRETTET_ALLE")
	private Long antInspOpprettetAlle;
	@Column(name="ANT_INSP_OPPRETTET")
	private Long antInspOpprettet;
	
	@Column(name="ANT_R2_OPPRETTET_ALLE")
	private Long antR2OpprettetAlle;
	@Column(name="ANT_R2_RAPPORTERT_ALLE")
	private Long antR2RapportertAlle;
	@Column(name="ANT_R2_OPPRETTET")
	private Long antR2Opprettet;
	@Column(name="ANT_R2_RAPPORTERT")
	private Long antR2Rapportert;
	
	@Column(name="ANT_R5_OPPRETTET_ALLE")
	private Long antR5OpprettetAlle;
	@Column(name="ANT_R5_RAPPORTERT_ALLE")
	private Long antR5RapportertAlle;
	@Column(name="ANT_R5_OPPRETTET")
	private Long antR5Opprettet;
	@Column(name="ANT_R5_RAPPORTERT")
	private Long antR5Rapportert;
	
	@Column(name="ANT_R11_OPPRETTET_ALLE")
	private Long antR11OpprettetAlle;
	@Column(name="ANT_R11_RAPPORTERT_ALLE")
	private Long antR11RapportertAlle;
	@Column(name="ANT_R11_OPPRETTET")
	private Long antR11Opprettet;
	@Column(name="ANT_R11_RAPPORTERT")
	private Long antR11Rapportert;
	
	@Column(name="ANT_TILLEGGSJOBBER_ALLE")
	private Long antTilleggsjobberAlle;
	@Column(name="ANT_TILLEGGSJOBBER")
	private Long antTilleggsjobber;
	
	@Column(name="ANT_AVVIK_OPPRETTET_ALLE")
	private Long antAvvikOpprettetAlle;
	@Column(name="ANT_AVVIK_LUKKET_ALLE")
	private Long antAvvikLukketAlle;
	@Column(name="ANT_AVVIK_OPPRETTET")
	private Long antAvvikOpprettet;
	@Column(name="ANT_AVVIK_LUKKET")
	private Long antAvvikLukket;
	
	@Column(name="ANT_MANGLER_ALLE")
	private Long antManglerAlle;
	@Column(name="ANT_MANGLER_LUKKET_ALLE")
	private Long antManglerLukketAlle;
	@Column(name="ANT_MANGLER")
	private Long antMangler;
	@Column(name="ANT_MANGLER_AAPNE")
	private Long antManglerAapne;
	@Column(name="ANT_MANGLER_LUKKET")
	private Long antManglerLukket;
	
	@Column(name="ANT_DAU_BEST_ALLE")
	private Long antDauBestAlle;
	@Column(name="ANT_DAU_BEST")
	private Long antDauBest;
	
	@Column(name="ANT_RETARDASJON_ALLE")
	private Long antRetardasjonAlle;
	@Column(name="ANT_RETARDASJON")
	private Long antRetardasjon;
	
	@Column(name="KM_KONT_FRIKSJON_TOTALT")
	private Double kmKontFriskjonTotalt;
	@Column(name="KM_KONT_FRIKSJON")
	private Double kmKontFriksjon;
	
	@Transient
	private Double prosentInspisertEv;
	@Transient
	private Double prosentInspisertRv;
	@Transient
	private Double prosentInspisertFv;
	@Transient
	private Double prosentInspisertKv;
	
	@Transient 
	private Double inspKmEv;
	@Transient 
	private Double inspKmFv;
	@Transient 
	private Double inspKmRv;
	@Transient
	private Double veinettLengdeEv;
	@Transient
	private Double veinettLengdeRv;
	@Transient
	private Double veinettLengdeFv;
	@Transient
	private Double veinettLengdeKv;
	@Transient
	private Double inspKmKv;
	
	@Transient
	private Long antKjoretoy;
	@Transient 
	private Long antKjoretoyUtenLivstegn;
	
	public static final String[] getPropertiesArray(){
		return new String[]{  "kontraktId","kontraktnavn","kontraktveinettKm","regionId","regionnavn","fraDato","tilDato","antInspOpprettetAlle",
				  "antInspOpprettet","antR2OpprettetAlle","antR2RapportertAlle","antR2Opprettet","antR2Rapportert","antR5OpprettetAlle",
				  "antR5RapportertAlle","antR5Opprettet","antR5Rapportert","antR11OpprettetAlle","antR11RapportertAlle","antR11Opprettet",
				  "antR11Rapportert","antTilleggsjobberAlle","antTilleggsjobber","antAvvikOpprettetAlle","antAvvikLukketAlle","antAvvikOpprettet",
				  "antAvvikLukket","antManglerAlle","antManglerLukketAlle","antMangler","antManglerAapne","antManglerLukket",
				  "antDauBestAlle","antDauBest","antRetardasjonAlle","kmKontFriskjonTotalt","antRetardasjon","kmKontFriksjon"};
	}
	
	/**
	 * Slår sammen nøkkeltall for to resultater. 
	 * 
	 * Tar ikke med prosent inspisert EV, RV og FV eller veinettlengde for disse.
	 * @param n
	 */
	public void add(NokkeltallFelt n){
		kontraktId = n.kontraktId;
		kontraktnavn = n.kontraktnavn;
		regionId = n.regionId;
		regionnavn = n.regionnavn;
		
		antAvvikLukket=add(antAvvikLukket, n.antAvvikLukket);
		antAvvikLukketAlle=add(antAvvikLukketAlle, n.antAvvikLukketAlle);
		antAvvikOpprettet=add(antAvvikOpprettet, n.antAvvikOpprettet);
		antAvvikOpprettetAlle=add(antAvvikOpprettetAlle, n.antAvvikOpprettetAlle);
		antDauBest=add(antDauBest, n.antDauBest);
		antDauBestAlle=add(antDauBestAlle, n.antDauBestAlle);
		antInspOpprettet=add(antInspOpprettet, n.antInspOpprettet);
		antInspOpprettetAlle=add(antInspOpprettetAlle, n.antInspOpprettetAlle);
		antMangler=add(antMangler, n.antMangler);
		antManglerAapne=add(antManglerAapne, n.antManglerAapne);
		antManglerAlle=add(antManglerAlle, n.antManglerAlle);
		antManglerLukket=add(antManglerLukket, n.antManglerLukket);
		antManglerLukketAlle=add(antManglerLukketAlle, n.antManglerLukketAlle);
		
		antR11Opprettet=add(antR11Opprettet, n.antR11Opprettet);
		antR11OpprettetAlle=add(antR11OpprettetAlle, n.antR11OpprettetAlle);
		antR11Rapportert=add(antR11Rapportert, n.antR11Rapportert);
		antR11RapportertAlle=add(antR11RapportertAlle, n.antR11RapportertAlle);
		
		antR2Opprettet=add(antR2Opprettet, n.antR2Opprettet);
		antR2OpprettetAlle=add(antR2OpprettetAlle, n.antR2OpprettetAlle);
		antR2Rapportert=add(antR2Rapportert, n.antR2Rapportert);
		antR2RapportertAlle=add(antR2RapportertAlle, n.antR2RapportertAlle);
		
		antR5Rapportert=add(antR5Rapportert, n.antR5Rapportert);
		antR5RapportertAlle=add(antR5RapportertAlle, n.antR5RapportertAlle);
		antR5Opprettet=add(antR5Opprettet, n.antR5Opprettet);
		antR5OpprettetAlle=add(antR5OpprettetAlle, n.antR5OpprettetAlle);
		
		antRetardasjon=add(antRetardasjon, n.antRetardasjon);
		antRetardasjonAlle=add(antRetardasjonAlle, n.antRetardasjonAlle);
		antTilleggsjobber=add(antTilleggsjobber, antTilleggsjobber);
		antTilleggsjobberAlle=add(antTilleggsjobberAlle, n.antTilleggsjobberAlle);
		kmKontFriksjon=add(kmKontFriksjon, n.kmKontFriksjon);
		kmKontFriskjonTotalt=add(kmKontFriskjonTotalt, n.kmKontFriskjonTotalt);
		
		fraDato=fraDato==null?n.fraDato:fraDato.after(n.fraDato)?n.fraDato:fraDato;
		tilDato=tilDato==null?n.tilDato:tilDato.before(n.tilDato)?n.tilDato:tilDato;
		
		if (kontraktveinettKm==null){//Kan ikke summere lengden på kontraktsveinettet for hver uke
			kontraktveinettKm = n.kontraktveinettKm;
		}
	}
	private Double add(Double v1, Double v2){
		if (v1==null&&v2==null){
			return null;
		}
		if (v2!=null){
			Double v = v1==null?v2:v1+v2;
			return v;	
		}
		return v1;
		
	}
	private Long add(Long l1, Long l2){
		if (l1==null&&l2==null){
			return null;
		}
		if (l2!=null){
			Long v = l1==null?l2:l1+l2;
			return v;
		}
		return l1;
	}
	/**
	 * @return the kontraktId
	 */
	public Long getKontraktId() {
		return kontraktId;
	}
	
	/**
	 * Returnerer kontraktens navn på formen: kontraktnummer - kontraktnavn
	 * @return
	 */
	public String getKontraktnavn(){
		return kontraktnavn;
	}

	/**
	 * Returnerer regionens id
	 * @return
	 */
	public Long getRegionId(){
		return regionId;
	}
	/**
	 * Returnerer regionens navn
	 * @return
	 */
	public String getRegionnavn(){
		return regionnavn;
	}
	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}

	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}

	/**
	 * Returnerer kontraktveinettets lengde i Km
	 * @return
	 */
	public Double getKontraktveinettKm(){
		return kontraktveinettKm;
	}

	/**
	 * @return the antInspOpprettetAlle
	 */
	public Long getAntInspOpprettetAlle() {
		return antInspOpprettetAlle;
	}

	/**
	 * @return the antInspOpprettet
	 */
	public Long getAntInspOpprettet() {
		return antInspOpprettet;
	}

	/**
	 * @return the antR2OpprettetAlle
	 */
	public Long getAntR2OpprettetAlle() {
		return antR2OpprettetAlle;
	}

	/**
	 * @return the antR2RapportertAlle
	 */
	public Long getAntR2RapportertAlle() {
		return antR2RapportertAlle;
	}

	/**
	 * @return the antR2Opprettet
	 */
	public Long getAntR2Opprettet() {
		return antR2Opprettet;
	}

	/**
	 * @return the antR2Rapportert
	 */
	public Long getAntR2Rapportert() {
		return antR2Rapportert;
	}

	/**
	 * @return the antR5OpprettetAlle
	 */
	public Long getAntR5OpprettetAlle() {
		return antR5OpprettetAlle;
	}

	/**
	 * @return the antR5RapportertAlle
	 */
	public Long getAntR5RapportertAlle() {
		return antR5RapportertAlle;
	}

	/**
	 * @return the antR5Opprettet
	 */
	public Long getAntR5Opprettet() {
		return antR5Opprettet;
	}

	/**
	 * @return the antR5Rapportert
	 */
	public Long getAntR5Rapportert() {
		return antR5Rapportert;
	}

	/**
	 * @return the antR11OpprettetAlle
	 */
	public Long getAntR11OpprettetAlle() {
		return antR11OpprettetAlle;
	}

	/**
	 * @return the antR11RapportertAlle
	 */
	public Long getAntR11RapportertAlle() {
		return antR11RapportertAlle;
	}

	/**
	 * @return the antR11Opprettet
	 */
	public Long getAntR11Opprettet() {
		return antR11Opprettet;
	}

	/**
	 * @return the antR11Rapportert
	 */
	public Long getAntR11Rapportert() {
		return antR11Rapportert;
	}

	/**
	 * @return the antTilleggsjobberAlle
	 */
	public Long getAntTilleggsjobberAlle() {
		return antTilleggsjobberAlle;
	}

	/**
	 * @return the antTilleggsjobber
	 */
	public Long getAntTilleggsjobber() {
		return antTilleggsjobber;
	}

	/**
	 * @return the antAvvikOpprettetAlle
	 */
	public Long getAntAvvikOpprettetAlle() {
		return antAvvikOpprettetAlle;
	}

	/**
	 * @return the antAvvikLukketAlle
	 */
	public Long getAntAvvikLukketAlle() {
		return antAvvikLukketAlle;
	}

	/**
	 * @return the antAvvikOpprettet
	 */
	public Long getAntAvvikOpprettet() {
		return antAvvikOpprettet;
	}

	/**
	 * @return the antAvvikLukket
	 */
	public Long getAntAvvikLukket() {
		return antAvvikLukket;
	}

	/**
	 * @return the antManglerAlle
	 */
	public Long getAntManglerAlle() {
		return antManglerAlle;
	}

	/**
	 * @return the antManglerLukketAlle
	 */
	public Long getAntManglerLukketAlle() {
		return antManglerLukketAlle;
	}

	/**
	 * @return the antMangler
	 */
	public Long getAntMangler() {
		return antMangler;
	}

	/**
	 * @return the antManglerAapne
	 */
	public Long getAntManglerAapne() {
		return antManglerAapne;
	}

	/**
	 * @return the antManglerLukket
	 */
	public Long getAntManglerLukket() {
		return antManglerLukket;
	}

	/**
	 * @return the antDauBestAlle
	 */
	public Long getAntDauBestAlle() {
		return antDauBestAlle;
	}

	/**
	 * @return the antDauBest
	 */
	public Long getAntDauBest() {
		return antDauBest;
	}

	/**
	 * @return the antRetardasjonAlle
	 */
	public Long getAntRetardasjonAlle() {
		return antRetardasjonAlle;
	}

	/**
	 * @return the antRetardasjon
	 */
	public Long getAntRetardasjon() {
		return antRetardasjon;
	}

	/**
	 * @return the kmKontFriskjonTotalt
	 */
	public Double getKmKontFriskjonTotalt() {
		return kmKontFriskjonTotalt;
	}

	/**
	 * @return the kmKontFriksjon
	 */
	public Double getKmKontFriksjon() {
		return kmKontFriksjon;
	}

	/**
	 * @param kontraktId the kontraktId to set
	 */
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	/**
	 * @param kontraktNavn the kontraktNavn to set
	 */
	public void setKontraktnavn(String kontraktnavn) {
		this.kontraktnavn = kontraktnavn;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	/**
	 * @param regionNavn the regionNavn to set
	 */
	public void setRegionnavn(String regionnavn) {
		this.regionnavn = regionnavn;
	}

	/**
	 * @param kontraktVeinettKm the kontraktVeinettKm to set
	 */
	public void setKontraktveinettKm(Double kontraktveinettKm) {
		this.kontraktveinettKm = kontraktveinettKm;
	}

	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	/**
	 * @param tilDato the tilDato to set
	 */
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}

	/**
	 * @param antInspOpprettetAlle the antInspOpprettetAlle to set
	 */
	public void setAntInspOpprettetAlle(Long antInspOpprettetAlle) {
		this.antInspOpprettetAlle = antInspOpprettetAlle;
	}

	/**
	 * @param antInspOpprettet the antInspOpprettet to set
	 */
	public void setAntInspOpprettet(Long antInspOpprettet) {
		this.antInspOpprettet = antInspOpprettet;
	}

	/**
	 * @param antR2OpprettetAlle the antR2OpprettetAlle to set
	 */
	public void setAntR2OpprettetAlle(Long antR2OpprettetAlle) {
		this.antR2OpprettetAlle = antR2OpprettetAlle;
	}

	/**
	 * @param antR2RapportertAlle the antR2RapportertAlle to set
	 */
	public void setAntR2RapportertAlle(Long antR2RapportertAlle) {
		this.antR2RapportertAlle = antR2RapportertAlle;
	}

	/**
	 * @param antR2Opprettet the antR2Opprettet to set
	 */
	public void setAntR2Opprettet(Long antR2Opprettet) {
		this.antR2Opprettet = antR2Opprettet;
	}

	/**
	 * @param antR2Rapportert the antR2Rapportert to set
	 */
	public void setAntR2Rapportert(Long antR2Rapportert) {
		this.antR2Rapportert = antR2Rapportert;
	}

	/**
	 * @param antR5OpprettetAlle the antR5OpprettetAlle to set
	 */
	public void setAntR5OpprettetAlle(Long antR5OpprettetAlle) {
		this.antR5OpprettetAlle = antR5OpprettetAlle;
	}

	/**
	 * @param antR5RapportertAlle the antR5RapportertAlle to set
	 */
	public void setAntR5RapportertAlle(Long antR5RapportertAlle) {
		this.antR5RapportertAlle = antR5RapportertAlle;
	}

	/**
	 * @param antR5Opprettet the antR5Opprettet to set
	 */
	public void setAntR5Opprettet(Long antR5Opprettet) {
		this.antR5Opprettet = antR5Opprettet;
	}

	/**
	 * @param antR5Rapportert the antR5Rapportert to set
	 */
	public void setAntR5Rapportert(Long antR5Rapportert) {
		this.antR5Rapportert = antR5Rapportert;
	}

	/**
	 * @param antR11OpprettetAlle the antR11OpprettetAlle to set
	 */
	public void setAntR11OpprettetAlle(Long antR11OpprettetAlle) {
		this.antR11OpprettetAlle = antR11OpprettetAlle;
	}

	/**
	 * @param antR11RapportertAlle the antR11RapportertAlle to set
	 */
	public void setAntR11RapportertAlle(Long antR11RapportertAlle) {
		this.antR11RapportertAlle = antR11RapportertAlle;
	}

	/**
	 * @param antR11Opprettet the antR11Opprettet to set
	 */
	public void setAntR11Opprettet(Long antR11Opprettet) {
		this.antR11Opprettet = antR11Opprettet;
	}

	/**
	 * @param antR11Rapportert the antR11Rapportert to set
	 */
	public void setAntR11Rapportert(Long antR11Rapportert) {
		this.antR11Rapportert = antR11Rapportert;
	}

	/**
	 * @param antTilleggsjobberAlle the antTilleggsjobberAlle to set
	 */
	public void setAntTilleggsjobberAlle(Long antTilleggsjobberAlle) {
		this.antTilleggsjobberAlle = antTilleggsjobberAlle;
	}

	/**
	 * @param antTilleggsjobber the antTilleggsjobber to set
	 */
	public void setAntTilleggsjobber(Long antTilleggsjobber) {
		this.antTilleggsjobber = antTilleggsjobber;
	}

	/**
	 * @param antAvvikOpprettetAlle the antAvvikOpprettetAlle to set
	 */
	public void setAntAvvikOpprettetAlle(Long antAvvikOpprettetAlle) {
		this.antAvvikOpprettetAlle = antAvvikOpprettetAlle;
	}

	/**
	 * @param antAvvikLukketAlle the antAvvikLukketAlle to set
	 */
	public void setAntAvvikLukketAlle(Long antAvvikLukketAlle) {
		this.antAvvikLukketAlle = antAvvikLukketAlle;
	}

	/**
	 * @param antAvvikOpprettet the antAvvikOpprettet to set
	 */
	public void setAntAvvikOpprettet(Long antAvvikOpprettet) {
		this.antAvvikOpprettet = antAvvikOpprettet;
	}

	/**
	 * @param antAvvikLukket the antAvvikLukket to set
	 */
	public void setAntAvvikLukket(Long antAvvikLukket) {
		this.antAvvikLukket = antAvvikLukket;
	}

	/**
	 * @param antManglerAlle the antManglerAlle to set
	 */
	public void setAntManglerAlle(Long antManglerAlle) {
		this.antManglerAlle = antManglerAlle;
	}

	/**
	 * @param antManglerLukketAlle the antManglerLukketAlle to set
	 */
	public void setAntManglerLukketAlle(Long antManglerLukketAlle) {
		this.antManglerLukketAlle = antManglerLukketAlle;
	}

	/**
	 * @param antMangler the antMangler to set
	 */
	public void setAntMangler(Long antMangler) {
		this.antMangler = antMangler;
	}

	/**
	 * @param antManglerAapne the antManglerAapne to set
	 */
	public void setAntManglerAapne(Long antManglerAapne) {
		this.antManglerAapne = antManglerAapne;
	}

	/**
	 * @param antManglerLukket the antManglerLukket to set
	 */
	public void setAntManglerLukket(Long antManglerLukket) {
		this.antManglerLukket = antManglerLukket;
	}

	/**
	 * @param antDauBestAlle the antDauBestAlle to set
	 */
	public void setAntDauBestAlle(Long antDauBestAlle) {
		this.antDauBestAlle = antDauBestAlle;
	}

	/**
	 * @param antDauBest the antDauBest to set
	 */
	public void setAntDauBest(Long antDauBest) {
		this.antDauBest = antDauBest;
	}

	/**
	 * @param antRetardasjonAlle the antRetardasjonAlle to set
	 */
	public void setAntRetardasjonAlle(Long antRetardasjonAlle) {
		this.antRetardasjonAlle = antRetardasjonAlle;
	}

	/**
	 * @param antRetardasjon the antRetardasjon to set
	 */
	public void setAntRetardasjon(Long antRetardasjon) {
		this.antRetardasjon = antRetardasjon;
	}

	/**
	 * @param kmKontFriskjonTotalt the kmKontFriskjonTotalt to set
	 */
	public void setKmKontFriskjonTotalt(Double kmKontFriskjonTotalt) {
		this.kmKontFriskjonTotalt = kmKontFriskjonTotalt;
	}

	/**
	 * @param kmKontFriksjon the kmKontFriksjon to set
	 */
	public void setKmKontFriksjon(Double kmKontFriksjon) {
		this.kmKontFriksjon = kmKontFriksjon;
	}

	public String getTextForGUI() {
		return null;
	}

	/**
	 * @return the prosentInspisertEv
	 */
	public Double getProsentInspisertEv() {
		if (veinettLengdeEv!=null&&inspKmEv!=null){
			return (inspKmEv/veinettLengdeEv)*100;
		}
		return null;
	}

	/**
	 * @return the prosentInspisertRv
	 */
	public Double getProsentInspisertRv() {
		if (veinettLengdeRv!=null&&inspKmRv!=null){
			return (inspKmRv/veinettLengdeRv)*100;
		}
		return null;
	}

	/**
	 * @return the prosentInspisertFv
	 */
	public Double getProsentInspisertFv() {
		if (veinettLengdeFv!=null&&inspKmFv!=null){
			return (inspKmFv/veinettLengdeFv)*100;
		}
		return null;
	}
	public Double getProsentInspisertKv() {
		if (veinettLengdeKv!=null&&inspKmKv!=null){
			return (inspKmKv/veinettLengdeKv)*100;
		}
		return null;
	}
	public Double getDekningsgradInspeksjon(){
		Double kmVeinett = veinettLengdeEv;
		kmVeinett = add(kmVeinett, veinettLengdeRv);
		kmVeinett = add(kmVeinett, veinettLengdeFv);
		kmVeinett = add(kmVeinett, veinettLengdeKv);
		
		Double kmInsp = inspKmEv;
		kmInsp = add(kmInsp, inspKmRv);
		kmInsp = add(kmInsp, inspKmFv);
		kmInsp = add(kmInsp, inspKmKv);
		
		if (kmVeinett!=null&&kmInsp!=null){	
			return (kmInsp/kmVeinett)*100;
		}
		return null;
	}
	
	/**
	 * @return the veinettLengdeEv
	 */
	public Double getVeinettLengdeEv() {
		return veinettLengdeEv;
	}

	/**
	 * @return the veinettLengdeRv
	 */
	public Double getVeinettLengdeRv() {
		return veinettLengdeRv;
	}

	/**
	 * @return the veinettLengdeFv
	 */
	public Double getVeinettLengdeFv() {
		return veinettLengdeFv;
	}

	

	/**
	 * @param veinettLengdeEv the veinettLengdeEv to set
	 */
	public void setVeinettLengdeEv(Double veinettLengdeEv) {
		this.veinettLengdeEv = veinettLengdeEv;
	}

	/**
	 * @param veinettLengdeRv the veinettLengdeRv to set
	 */
	public void setVeinettLengdeRv(Double veinettLengdeRv) {
		this.veinettLengdeRv = veinettLengdeRv;
	}

	/**
	 * @param veinettLengdeFv the veinettLengdeFv to set
	 */
	public void setVeinettLengdeFv(Double veinettLengdeFv) {
		this.veinettLengdeFv = veinettLengdeFv;
	}

	/**
	 * @return the inspKmEv
	 */
	public Double getInspKmEv() {
		return inspKmEv;
	}

	/**
	 * @param inspKmEv the inspKmEv to set
	 */
	public void setInspKmEv(Double inspKmEv) {
		this.inspKmEv = inspKmEv;
	}

	/**
	 * @return the inspKmFv
	 */
	public Double getInspKmFv() {
		return inspKmFv;
	}

	/**
	 * @param inspKmFv the inspKmFv to set
	 */
	public void setInspKmFv(Double inspKmFv) {
		this.inspKmFv = inspKmFv;
	}

	/**
	 * @return the inspKmRv
	 */
	public Double getInspKmRv() {
		return inspKmRv;
	}

	/**
	 * @param inspKmRv the inspKmRv to set
	 */
	public void setInspKmRv(Double inspKmRv) {
		this.inspKmRv = inspKmRv;
	}

	public void setVeinettLengdeKv(Double veinettLengdeKv) {
		this.veinettLengdeKv = veinettLengdeKv;
	}
	public Double getVeinettLengdeKv(){
		return veinettLengdeKv;
	}

	public void setInspKmKv(Double inspKmKv) {
		this.inspKmKv = inspKmKv;
	}
	public Double getInspKmKv(){
		return inspKmKv;
	}

	public void setAntKjoretoy(Long antKjoretoy) {
		this.antKjoretoy = antKjoretoy;
	}

	public Long getAntKjoretoy() {
		return antKjoretoy;
	}

	public void setAntKjoretoyUtenLivstegn(Long antKjoretoyUtenLivstegn) {
		this.antKjoretoyUtenLivstegn = antKjoretoyUtenLivstegn;
	}

	public Long getAntKjoretoyUtenLivstegn() {
		return antKjoretoyUtenLivstegn;
	}
}
