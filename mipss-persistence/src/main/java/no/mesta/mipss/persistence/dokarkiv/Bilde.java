package no.mesta.mipss.persistence.dokarkiv;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Beskriver et bilde i databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( {
		@NamedQuery(name = Bilde.QUERY_FIND_ALL, query = "SELECT b FROM Bilde b"),
		@NamedQuery(name = Bilde.QUERY_FIND_ALL_WITH_LOB, query = "SELECT b FROM Bilde b"),
		@NamedQuery(name = Bilde.GET_WITH_LOB, query = "SELECT b FROM Bilde b WHERE b.guid = :guid"),
		@NamedQuery(name = Bilde.GET_FOR_KJORETOY, query = "select b from Bilde b where b.guid in (select k.bildeGuid from KjoretoyBilde k where k.kjoretoyId = :id)") })
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class Bilde extends MipssEntityBean<Bilde> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity,
		Comparable<Bilde> {
	private static final String FIELD_BESKRIVELSE = "beskrivelse";
	private static final String FIELD_BILDELOB = "bildeLob";
	private static final String FIELD_BREDDE = "bredde";
	private static final String FIELD_FILNAVN = "filnavn";
	private static final String FIELD_HOYDE = "hoyde";
	private static final String FIELD_SMAABILDELOB = "smaaBildeLob";
	private static final String FIELD_SMAAIMAGE = "smaaImage";
	private static final String FIELD_TEXTFORGUI = "textForGUI";
	public static final String GET_FOR_KJORETOY = "Bilde.getForKjoretoy";
	public static final String GET_WITH_LOB = "Bilde.getWithLob";
	private static final String ID_GUID = "guid";
	private static final String[] NAMES = { ID_GUID };
	public static final String QUERY_FIND_ALL = "Bilde.findAll";
	public static final String QUERY_FIND_ALL_WITH_LOB = "Bilde.findAllWithLob";
	private transient BufferedImage bilde;
	private byte[] bildeLob;
	private Integer bredde;
	private String filnavn;
	private String guid;
	private Integer hoyde;
	private List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
	private OwnedMipssEntity ownedMipssEntity = new OwnedMipssEntity();
	private transient BufferedImage smaaBilde;
	private byte[] smaabildeLob;
	private String tekst;
	private int bildeSize;
	@Transient
	public int getBildeSize() {
		return bildeSize;
	}

	public void setBildeSize(int bildeSize) {
		this.bildeSize = bildeSize;
	}

	private int smaabildeSize;

	/**
	 * Konstruktør
	 */
	public Bilde() {
	}

	/** {@inheritDoc} */
	public int compareTo(Bilde o) {
		if (o == null) {
			return 1;
		}

		Date thisD = getOwnedMipssEntity().getOpprettetDato();
		Date otherD = o.getOwnedMipssEntity().getOpprettetDato();

		return new CompareToBuilder().append(thisD, otherD).append(filnavn, o.filnavn).toComparison();
	}

	/**
	 * Beskrivelse av bildet
	 * 
	 * @return
	 */
	@Column(name = "BESKRIVELSE", length = 255)
	public String getBeskrivelse() {
		return tekst;
	}

	/**
	 * Binære bildedata
	 * 
	 * @return
	 */
	@Lob
	@Column(name="BILDE_LOB")
	public byte[] getBildeLob() {
		return bildeLob;
	}

	/**
	 * Bildets bredde
	 * 
	 * @return
	 */
	public Integer getBredde() {
		return bredde;
	}

	/**
	 * Delegat metode
	 * 
	 * @return
	 */
	@Transient
	public String getEndrer() {
		return getOwnedMipssEntity().getEndretAv();
	}

	/**
	 * Delegat metode
	 * 
	 * @return
	 */
	@Transient
	public Date getEndringsDato() {
		return getOwnedMipssEntity().getEndretDato();
	}

	/**
	 * Bildets filnavn
	 * 
	 * @return
	 */
	@Column(nullable = false, length = 255)
	public String getFilnavn() {
		return filnavn;
	}

	/**
	 * Global unik id for bildet
	 * 
	 * @return
	 */
	@Id
	@Column(nullable = false)
	public String getGuid() {
		return guid;
	}

	/**
	 * Bildets h�yde
	 * 
	 * @return
	 */
	public Integer getHoyde() {
		return hoyde;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return NAMES;
	}

	/**
	 * Lager bilde av blob, null hvis der ikke er blob
	 * 
	 * @return
	 */
	@Transient
	public BufferedImage getImage() {
		if (bilde != null) {
			return bilde;
		} else {
			if (bildeLob != null) {
//				bilde = ImageUtil.bytesToImage(getBildeLob());
				try {
					bilde = ImageIO.read(new ByteArrayInputStream(getBildeLob()));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int fHeight = 1500;
	            int fWidth = 1500;
	            
	            //Work out the resized width/height
	            if (bilde.getHeight() > fHeight || bilde.getWidth() > fWidth) {
	                int wid = fWidth;
	                float sum = (float)bilde.getWidth() / (float)bilde.getHeight();
	                fWidth = Math.round(fHeight * sum);

	                if (fWidth > wid) {
	                    //rezise again for the width this time
	                    fHeight = Math.round(wid/sum);
	                    fWidth = wid;
	                }
	            }
				bilde = ImageUtil.getScaledInstance(bilde, fWidth, fHeight, RenderingHints.VALUE_INTERPOLATION_BILINEAR, false);
//				byte[] scaledInstance = ImageUtil.getScaledInstance(new ByteArrayInputStream(getBildeLob()), fWidth, fHeight, 0.6f);
//				bilde = ImageUtil.bytesToImage(scaledInstance);
				return bilde;
			} else {
				return null;
			}
		}
	}

	@ManyToMany(mappedBy = "bildeList", fetch = FetchType.LAZY)
	public List<Kjoretoy> getKjoretoyList() {
		return kjoretoyList;
	}

	/**
	 * Delegat metode
	 * 
	 * @return
	 */
	@Transient
	public Date getNyDato() {
		return getOwnedMipssEntity().getOpprettetDato();
	}

	/**
	 * Delegat metode
	 * 
	 * @return
	 */
	@Transient
	public String getOppretter() {
		return getOwnedMipssEntity().getOpprettetAv();
	}

	/** {@inheritDoc} */
	@Embedded
	public OwnedMipssEntity getOwnedMipssEntity() {
		if (ownedMipssEntity == null) {
			ownedMipssEntity = new OwnedMipssEntity();
		}
		return ownedMipssEntity;
	}

	/**
	 * Binær blob for en liten representasjon av bildet
	 * 
	 * @return
	 */
	@Column(name = "SMAABILDE_LOB")
	@Lob
	@Basic(fetch = FetchType.EAGER)
	public byte[] getSmaabildeLob() {
		return smaabildeLob;
	}

	/**
	 * Lager bilde av små bilde blob, null hvis der ikke er blob
	 * 
	 * @return
	 */
	@Transient
	public BufferedImage getSmaaImage() {
		if (smaaBilde != null) {
			return smaaBilde;
		} else {
			if (smaabildeLob != null) {
				smaaBilde = ImageUtil.bytesToImage(smaabildeLob);
				return smaaBilde;
			} else {
				return null;
			}
		}
	}

	/** {@inheritDoc} */
	@Transient
	public String getTextForGUI() {
		if (tekst != null && tekst.length() > 0) {
			return tekst;
		}
		return filnavn;
	}

	/**
	 * Beskrivelse av bildet
	 * 
	 * @param tekst
	 */
	public void setBeskrivelse(String tekst) {
		String old = this.tekst;
		String oldGuiText = getTextForGUI();
		this.tekst = tekst;
		
		try {
			fireVetoableChange(FIELD_BESKRIVELSE, old, tekst);
		} catch (PropertyVetoException e) {
			this.tekst = old;
		}
		
		firePropertyChange(FIELD_BESKRIVELSE, old, tekst);
		firePropertyChange(FIELD_TEXTFORGUI, oldGuiText, getTextForGUI());
	}

	/**
	 * Binære bildedata
	 * 
	 * @param bildeLob
	 */
	public void setBildeLob(byte[] bildeLob) {
		if (bildeLob!=null)
			setBildeSize(bildeLob.length);
		else
			setBildeSize(0);
		byte[] old = this.bildeLob;
		this.bildeLob = bildeLob;
		bilde = null;
		firePropertyChange(FIELD_BILDELOB, old, bildeLob);
	}

	/**
	 * Bildets bredde
	 * 
	 * @param bredde
	 */
	public void setBredde(Integer bredde) {
		Integer old = this.bredde;
		this.bredde = bredde;
		firePropertyChange(FIELD_BREDDE, old, bredde);
	}

	/**
	 * Delegat metode
	 * 
	 * @param s
	 */
	public void setEndrer(String s) {
		getOwnedMipssEntity().setEndretAv(s);
	}

	/**
	 * Delegat metode
	 * 
	 * @param d
	 */
	public void setEndringsDato(Date d) {
		getOwnedMipssEntity().setEndretDato(d);
	}

	/**
	 * Bildets filnavn
	 * 
	 * @param filnavn
	 */
	public void setFilnavn(String filnavn) {
		String old = this.filnavn;
		String oldGuiText = getTextForGUI();
		this.filnavn = filnavn;
		firePropertyChange(FIELD_FILNAVN, old, filnavn);
		firePropertyChange(FIELD_TEXTFORGUI, oldGuiText, getTextForGUI());
	}

	/**
	 * Global unik id for bildet
	 * 
	 * @param id
	 */
	public void setGuid(String id) {
		String old = this.guid;
		this.guid = id;
		firePropertyChange(ID_GUID, old, id);
	}

	/**
	 * Bildets høyde
	 * 
	 * @param hoyde
	 */
	public void setHoyde(Integer hoyde) {
		Integer old = this.hoyde;
		this.hoyde = hoyde;
		firePropertyChange(FIELD_HOYDE, old, hoyde);
	}

	public void setKjoretoyList(List<Kjoretoy> kjoretoyList) {
		this.kjoretoyList = kjoretoyList;
	}

	/**
	 * Delegat metode
	 * 
	 * @param d
	 */
	public void setNyDato(Date d) {
		getOwnedMipssEntity().setOpprettetDato(d);
	}

	/**
	 * Delegat metode
	 * 
	 * @param s
	 */
	public void setOppretter(String s) {
		getOwnedMipssEntity().setOpprettetAv(s);
	}

	/**
	 * @see #getOwnedMipssEntity()
	 * @param ownedMipssEntity
	 */
	public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
		this.ownedMipssEntity = ownedMipssEntity;
	}

	/**
	 * Binær blob for en liten representasjon av bildet
	 * 
	 * @param smaabildeLob
	 */
	public void setSmaabildeLob(byte[] smaabildeLob) {
		if (smaabildeLob!=null)
			setSmaabildeSize(smaabildeLob.length);
		else
			setSmaabildeSize(0);
		BufferedImage oldImage = smaaBilde;
		smaaBilde=null;
		byte[] old = this.smaabildeLob;
		this.smaabildeLob = smaabildeLob;
		firePropertyChange(FIELD_SMAABILDELOB, old, smaabildeLob);
		firePropertyChange(FIELD_SMAAIMAGE, oldImage, getSmaaImage());
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(ID_GUID, guid).append("filnavn", filnavn).append("bredde", bredde)
				.append("hoyde", hoyde).append("beskrivelse", tekst).append("bildeLob",
						bildeLob == null ? "null" : "not null").append("smaabildeLob",
						smaabildeLob == null ? "null" : "not null").toString();
	}

	public void setSmaabildeSize(int smaabildeSize) {
		this.smaabildeSize = smaabildeSize;
	}
	@Transient
	public int getSmaabildeSize() {
		return smaabildeSize;
	}
}
