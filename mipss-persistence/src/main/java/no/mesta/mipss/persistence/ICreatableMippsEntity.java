package no.mesta.mipss.persistence;

public interface ICreatableMippsEntity {

	/**
	 * Henter det @Embedded objektet som inneholder felt for {@code opprettetAv}
	 * og {@code opprettetDato}.
     * @return {@code MipssEntityCreator} instanse.
	 * @return
	 */
	public MipssEntityCreator getMipssEntityCreator();
}
