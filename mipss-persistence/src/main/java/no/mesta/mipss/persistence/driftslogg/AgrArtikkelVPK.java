package no.mesta.mipss.persistence.driftslogg;

import java.io.Serializable;

public class AgrArtikkelVPK implements Serializable{
	private String levkontraktIdent;
	private String artikkelIdent;
	
	public AgrArtikkelVPK(){
		
	}
	public AgrArtikkelVPK(String levkontraktIdent, String artikkelIdent){
		this.setLevkontraktIdent(levkontraktIdent);
		this.setArtikkelIdent(artikkelIdent);
	}
	public void setArtikkelIdent(String artikkelIdent) {
		this.artikkelIdent = artikkelIdent;
	}
	public String getArtikkelIdent() {
		return artikkelIdent;
	}
	public void setLevkontraktIdent(String levkontraktIdent) {
		this.levkontraktIdent = levkontraktIdent;
	}
	public String getLevkontraktIdent() {
		return levkontraktIdent;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikkelIdent == null) ? 0 : artikkelIdent.hashCode());
		result = prime * result + ((levkontraktIdent == null) ? 0 : levkontraktIdent.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgrArtikkelVPK other = (AgrArtikkelVPK) obj;
		if (artikkelIdent == null) {
			if (other.artikkelIdent != null)
				return false;
		} else if (!artikkelIdent.equals(other.artikkelIdent))
			return false;
		if (levkontraktIdent == null) {
			if (other.levkontraktIdent != null)
				return false;
		} else if (!levkontraktIdent.equals(other.levkontraktIdent))
			return false;
		return true;
	}
	
	
	
}
