package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssFetch;
import no.mesta.mipss.persistence.applikasjon.Brukermeny;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Uttrykker en bruker slik den er definert i databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@mesta.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( { @NamedQuery(name = Bruker.QUERY_BRUKER_FIND_ALL, query = "SELECT b FROM Bruker b") })
public class Bruker extends MipssEntityBean<Bruker> implements Serializable, IRenderableMipssEntity,
		Comparable<Bruker>, PropertyChangeSource {
	private static final String FIELD_BRUKERMENY = "brukermenyList";
	private static final String FIELD_BRUKERTILGANGLIST = "brukerTilgangList";
	private static final String FIELD_ENDRETAV = "endretAv";
	private static final String FIELD_ENDRETDATO = "endretDato";
	private static final String FIELD_ID = "signatur";
	private static final String FIELD_NAVN = "navn";
	private static final String FIELD_OPPRETTETAV = "opprettetAv";
	private static final String FIELD_OPPRETTETDATO = "opprettetDato";
	private static final String FIELD_PILOTTILGANG = "pilottilgangFlagg";
	private static final String FIELD_ROLLEBRUKERLIST = "rolleBrukerList";
	private static final String FIELD_SELSKAPBRUKERLIST = "selskapBrukerList";
	private static final String FIELD_STABFUNKSJONER = "stabfunksjoner";
	private static final String FIELD_PASSORDHASHED = "passordHashed";
	private static final String FIELD_EPOST = "epost";
	private static final String FIELD_BYTTPASSORD = "byttPassord";
	private static final String[] ID_NAMES = { FIELD_ID };
	public static final String QUERY_BRUKER_FIND_ALL = "Bruker.findAll";
	private List<Brukermeny> brukermenyList = new ArrayList<Brukermeny>();
	private List<BrukerTilgang> brukerTilgangList = new ArrayList<BrukerTilgang>();
	private String endretAv;
	private Date endretDato;
	private String navn;
	private String opprettetAv;
	private Date opprettetDato;
	private Long pilottilgangFlagg;
	private String elrappIdent;
	private List<RolleBruker> rolleBrukerList = new ArrayList<RolleBruker>();
	private List<SelskapBruker> selskapBrukerList = new ArrayList<SelskapBruker>();
	private String signatur;
	private List<KontraktStab> stabfunksjoner = new ArrayList<KontraktStab>();
	
	//Ny innlogging
	private String epost;
	private String passordHashed;
	private Boolean byttPassord;

	public Bruker() {
	}

	public Brukermeny addBrukermeny(Brukermeny brukermeny) {
		List<Brukermeny> old = new ArrayList<Brukermeny>(this.brukermenyList);
		getBrukermenyList().add(brukermeny);
		brukermeny.setBruker(this);

		getProps().firePropertyChange(FIELD_BRUKERMENY, old, brukermenyList);
		return brukermeny;
	}

	public BrukerTilgang addBrukerTilgang(BrukerTilgang brukerTilgang) {
		getBrukerTilgangList().add(brukerTilgang);
		brukerTilgang.setBruker(this);
		return brukerTilgang;
	}

	public RolleBruker addRolleBruker(RolleBruker rolleBruker) {
		List<RolleBruker> old = new ArrayList<RolleBruker>(this.rolleBrukerList);
		getRolleBrukerList().add(rolleBruker);
		rolleBruker.setBruker(this);

		getProps().firePropertyChange(FIELD_ROLLEBRUKERLIST, old, rolleBrukerList);
		return rolleBruker;
	}

	public SelskapBruker addSelskapBruker(SelskapBruker selskapBruker) {
		List<SelskapBruker> old = new ArrayList<SelskapBruker>(this.selskapBrukerList);
		getSelskapBrukerList().add(selskapBruker);
		selskapBruker.setBruker(this);

		getProps().firePropertyChange(FIELD_SELSKAPBRUKERLIST, old, selskapBrukerList);
		return selskapBruker;
	}

	public KontraktStab addStabfunksjon(KontraktStab stab) {
		List<KontraktStab> old = new ArrayList<KontraktStab>(this.stabfunksjoner);
		getStabfunksjoner().add(stab);
		stab.setBruker(this);
		getProps().firePropertyChange(FIELD_STABFUNKSJONER, old, stabfunksjoner);
		return stab;
	}

	/** {@inheritDoc} */
	public int compareTo(Bruker other) {
		if (other == null) {
			return 1;
		}

		return new CompareToBuilder().append(navn, other.getNavn()).append(signatur, other.signatur).append(
				opprettetDato, other.opprettetDato).toComparison();
	}

	@OneToMany(mappedBy = "bruker", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MipssFetch()
	public List<Brukermeny> getBrukermenyList() {
		return brukermenyList;
	}

	@OneToMany(mappedBy = "bruker", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MipssFetch()
	public List<BrukerTilgang> getBrukerTilgangList() {
		return brukerTilgangList;
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	/** {@inheritDoc} */
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}

	public String getNavn() {
		return navn;
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@Column(name = "PILOTTILGANG_FLAGG", nullable = false)
	public Long getPilottilgangFlagg() {
		return pilottilgangFlagg;
	}

	@OneToMany(mappedBy = "bruker", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MipssFetch
	public List<RolleBruker> getRolleBrukerList() {
		return rolleBrukerList;
	}

	@OneToMany(mappedBy = "bruker", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MipssFetch
	public List<SelskapBruker> getSelskapBrukerList() {
		return selskapBrukerList;
	}

	@Id
	@Column(nullable = false)
	public String getSignatur() {
		return signatur;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "bruker")
	@MipssFetch
	public List<KontraktStab> getStabfunksjoner() {
		return stabfunksjoner;
	}

	@Column(name="ELRAPP_IDENT")
	public String getElrappIdent(){
		return elrappIdent;
	}
	public void setElrappIdent(String elrappIdent){
		this.elrappIdent = elrappIdent;
	}
	
	/** NY INNLOGGING **/

	@Column(name="EPOST")
	public String getEpost(){
		return epost;
	}
	public void setEpost(String epost){
		String oldValue = this.epost;
		this.epost = epost;
		getProps().firePropertyChange(FIELD_EPOST, oldValue, epost);
	}
	@Column(name="PASSORD_HASHED")
	public String getPassordHashed(){
		return passordHashed;
	}
	public void setPassordHashed(String passordHashed){
		String oldValue = this.passordHashed;
		this.passordHashed = passordHashed;
		getProps().firePropertyChange(FIELD_PASSORDHASHED, oldValue, passordHashed);
	}
	@Column(name="BYTT_PASSORD_FLAGG")
	public Boolean getByttPassord(){
		return byttPassord;
	}
	public void setByttPassord(Boolean byttPassord){
		Boolean oldValue = this.byttPassord;
		this.byttPassord = byttPassord;
		getProps().firePropertyChange(FIELD_BYTTPASSORD, oldValue, byttPassord);
	}
	
	/** NYE INNLOGGING SLUTT**/
	
	/** {@inheritDoc} */
	@Transient
	public String getTextForGUI() {
		return getNavn();
	}

	/**
	 * Sjekker om brukeren har lesetilgang
	 * 
	 * @param modul
	 * @return
	 */
	public boolean hasModuleReadAccess(Modul modul) {
		if (isBanishedFromModule(modul)){
			return false;
		}
		return hasUserLevel(modul, Tilgangsnivaa.READ);
	}

	/**
	 * Sjekker om brukeren har skrivetilgang
	 * 
	 * @param modul
	 * @return
	 */
	public boolean hasModuleWriteAccess(Modul modul) {
		if (isBanishedFromModule(modul)){
			return false;
		}
		if (hasUserLevel(modul, Tilgangsnivaa.READ)){
			return false;
		}
		return hasUserLevel(modul, Tilgangsnivaa.WRITE);
		
	}

	public boolean hasUserLevel(Modul modul, Tilgangsnivaa nivaa) {
		// sjekk om nivaa tilgang er satt i mladen
		// sjekk om nivaa tilgang er satt i rollen
		Collections.sort(brukerTilgangList);
		for (BrukerTilgang bt : brukerTilgangList) {
			if (bt.getModul().equals(modul)) {
				if (bt.getTilgangsnivaa().equals(nivaa)) {
					return true;
				}
				break;
			}
		}

		for (RolleBruker rb : rolleBrukerList) {
			List<RolleTilgang> rolleTilganger = rb.getRolle().getRolleTilgangList();
			Collections.sort(rolleTilganger);
			for (RolleTilgang rt : rolleTilganger) {
				if (rt.getModul().equals(modul)) {
					if (rt.getTilgangsnivaa().equals(nivaa)) {
						return true;
					}
					break;
				}
			}
		}

		return false;
	}

	/**
	 * Sjekker om ingen tilgang er satt for en gitt modul for denne brukeren
	 * 
	 * @param modul
	 * @return
	 */
	@Transient
	public boolean isBanishedFromModule(Modul modul) {
		return hasUserLevel(modul, Tilgangsnivaa.NO_ACCESS);
	}

	public Brukermeny removeBrukermeny(Brukermeny brukermeny) {
		List<Brukermeny> old = new ArrayList<Brukermeny>(this.brukermenyList);
		getBrukermenyList().remove(brukermeny);

		getProps().firePropertyChange(FIELD_BRUKERMENY, old, brukermenyList);
		return brukermeny;
	}

	public BrukerTilgang removeBrukerTilgang(BrukerTilgang brukerTilgang) {
		List<BrukerTilgang> old = new ArrayList<BrukerTilgang>(this.brukerTilgangList);
		getBrukerTilgangList().remove(brukerTilgang);

		getProps().firePropertyChange(FIELD_BRUKERTILGANGLIST, old, brukerTilgangList);
		return brukerTilgang;
	}

	public RolleBruker removeRolleBruker(RolleBruker rolleBruker) {
		List<RolleBruker> old = new ArrayList<RolleBruker>(this.rolleBrukerList);
		getRolleBrukerList().remove(rolleBruker);

		getProps().firePropertyChange(FIELD_ROLLEBRUKERLIST, old, rolleBrukerList);
		return rolleBruker;
	}

	public SelskapBruker removeSelskapBruker(SelskapBruker selskapBruker) {
		List<SelskapBruker> old = new ArrayList<SelskapBruker>(this.selskapBrukerList);
		getSelskapBrukerList().remove(selskapBruker);

		getProps().firePropertyChange(FIELD_SELSKAPBRUKERLIST, old, selskapBrukerList);
		return selskapBruker;
	}

	public KontraktStab removeStabFunksjon(KontraktStab stab) {
		List<KontraktStab> old = new ArrayList<KontraktStab>(this.stabfunksjoner);
		getStabfunksjoner().remove(stab);

		getProps().firePropertyChange(FIELD_STABFUNKSJONER, old, stabfunksjoner);
		return stab;
	}

	public void setBrukermenyList(List<Brukermeny> brukermenyList) {
		List<Brukermeny> old = this.brukermenyList;
		this.brukermenyList = brukermenyList;
//		getProps().firePropertyChange(FIELD_BRUKERMENY, old, brukermenyList);
	}

	public void setBrukerTilgangList(List<BrukerTilgang> brukerTilgangList) {
		List<BrukerTilgang> old = this.brukerTilgangList;
		this.brukerTilgangList = brukerTilgangList;
//		getProps().firePropertyChange(FIELD_BRUKERTILGANGLIST, old, brukerTilgangList);
	}

	public void setEndretAv(String endretAv) {
		String oldValue = this.endretAv;
		this.endretAv = endretAv;
		getProps().firePropertyChange(FIELD_ENDRETAV, oldValue, endretAv);
	}

	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		getProps().firePropertyChange(FIELD_ENDRETDATO, old, endretDato);
	}

	public void setNavn(String navn) {
		String oldValue = this.navn;
		this.navn = navn;
		getProps().firePropertyChange(FIELD_NAVN, oldValue, navn);
	}

	public void setOpprettetAv(String opprettetAv) {
		String oldValue = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		getProps().firePropertyChange(FIELD_OPPRETTETAV, oldValue, opprettetAv);
	}

	public void setOpprettetDato(Date opprettetDato) {
		Date oldValue = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		getProps().firePropertyChange(FIELD_OPPRETTETDATO, oldValue, opprettetDato);
	}

	public void setPilottilgangFlagg(Long pilottilgangFlagg) {
		Long old = this.pilottilgangFlagg;
		this.pilottilgangFlagg = pilottilgangFlagg;
		getProps().firePropertyChange(FIELD_PILOTTILGANG, old, pilottilgangFlagg);
	}

	public void setRolleBrukerList(List<RolleBruker> rolleBrukerList) {
		List<RolleBruker> old = this.rolleBrukerList;
		this.rolleBrukerList = rolleBrukerList;
//		getProps().firePropertyChange(FIELD_ROLLEBRUKERLIST, old, rolleBrukerList);
	}

	public void setSelskapBrukerList(List<SelskapBruker> selskapBrukerList) {
		List<SelskapBruker> old = this.selskapBrukerList;
		this.selskapBrukerList = selskapBrukerList;
//		getProps().firePropertyChange(FIELD_SELSKAPBRUKERLIST, old, selskapBrukerList);
	}

	public void setSignatur(String signatur) {
		String oldValue = this.signatur;
		this.signatur = signatur;
		getProps().firePropertyChange(FIELD_ID, oldValue, signatur);
	}

	public void setStabfunksjoner(List<KontraktStab> stabfunksjoner) {
		List<KontraktStab> old = this.stabfunksjoner;
		this.stabfunksjoner = stabfunksjoner;
//		getProps().firePropertyChange(FIELD_STABFUNKSJONER, old, stabfunksjoner);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(FIELD_ID, getSignatur()).append(FIELD_NAVN, getNavn()).toString();
	}
}
