package no.mesta.mipss.persistence.veinett;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = Veinettveireferanse.ID_SEQ, sequenceName = "VEINETTVEIREFERANSE_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
@NamedQuery(name = Veinettveireferanse.QUERY_FIND_ALL, 
    query = "select o from Veinettveireferanse o")
public class Veinettveireferanse implements Serializable, IOwnableMipssEntity, IRenderableMipssEntity {
    public static final String QUERY_FIND_ALL = "Veinettveireferanse.findAll";
    public static final String ID_SEQ = "veinettveireferanseIdSeq";
    
    private Double fraKm;
    private Long fylkesnummer;
    private Long hp;
    private Long id;
    private Long kommunenummer;
    private Double tilKm;
    private String veikategori;
    private Long veinummer;
    private String veistatus;
    private Long rodeId;
    private Veinettreflinkseksjon veinettreflinkseksjon;

    // felt lagt til for hånd
    private OwnedMipssEntity ownedMipssEntity;
    
    public Veinettveireferanse() {
    }

    /**
     * Kopierer en annen veireferanse
     * 
     * @param v
     */
    public Veinettveireferanse(Veinettveireferanse v){
    	setFylkesnummer(v.getFylkesnummer());
    	setKommunenummer(v.getKommunenummer());
    	setHp(v.getHp());
    	setVeinummer(v.getVeinummer());
    	setVeistatus(v.getVeistatus());
    	setVeikategori(v.getVeikategori());
    	setFraKm(v.getFraKm());
    	setTilKm(v.getTilKm());
    }
    
    @Column(name="RODE_ID", nullable = true)
    public Long getRodeId(){
    	return rodeId;
    }
    
    public void setRodeId(Long rodeId){
    	this.rodeId = rodeId;
    }

    @Column(name="FRA_KM")
    public Double getFraKm() {
        return fraKm;
    }

    public void setFraKm(Double fraKm) {
        this.fraKm = fraKm;
    }

    public Long getFylkesnummer() {
        return fylkesnummer;
    }

    public void setFylkesnummer(Long fylkesnummer) {
        this.fylkesnummer = fylkesnummer;
    }

    public Long getHp() {
        return hp;
    }

    public void setHp(Long hp) {
        this.hp = hp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Veinettveireferanse.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKommunenummer() {
        return kommunenummer;
    }

    public void setKommunenummer(Long kommunenummer) {
        this.kommunenummer = kommunenummer;
    }

    @Column(name="TIL_KM")
    public Double getTilKm() {
        return tilKm;
    }

    public void setTilKm(Double tilKm) {
        this.tilKm = tilKm;
    }

    public String getVeikategori() {
        return veikategori;
    }

    public void setVeikategori(String veikategori) {
        this.veikategori = veikategori!=null?veikategori.toUpperCase():veikategori;
    }

    public Long getVeinummer() {
        return veinummer;
    }

    public void setVeinummer(Long veinummer) {
        this.veinummer = veinummer;
    }

    public String getVeistatus() {
        return veistatus;
    }

    public void setVeistatus(String veistatus) {
        this.veistatus = veistatus!=null?veistatus.toUpperCase():veistatus;
    }

    @ManyToOne
    @JoinColumn(name = "REFLINKSEKSJON_ID", referencedColumnName = "ID")
    public Veinettreflinkseksjon getVeinettreflinkseksjon() {
        return veinettreflinkseksjon;
    }

    public void setVeinettreflinkseksjon(Veinettreflinkseksjon veinettreflinkseksjon) {
        this.veinettreflinkseksjon = veinettreflinkseksjon;
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("fylkesnummer", fylkesnummer).
            append("kommunenummer", kommunenummer).
            append("veikategori", veikategori).
            append("veistatus", veistatus).
            append("veinummer", veinummer).
            append("hp", hp).
            append("fraMeter", fraKm).
            append("tilMeter", tilKm).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Veinettveireferanse)) {return false;}
        
        Veinettveireferanse other = (Veinettveireferanse) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(751,761).append(id).toHashCode();
    }



	public String getTextForGUI() {
		return getFylkesnummer()+"/"+getKommunenummer()+" "+getHp()+":"+getFraKm()+"-"+getTilKm();
	}
}
