package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Typetabell
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Temperatur.QUERY_FIND_ALL, query = "SELECT t FROM Temperatur t")
    })
public class Temperatur implements Serializable, IRenderableMipssEntity, Comparable<Temperatur> {
    public static final String QUERY_FIND_ALL = "Temperatur.findAll";
    
    private Long id;
    private String navn;

    /**
     * Konstruktør
     * 
     */
    public Temperatur() {
    }

    /** {@inheritDoc} */
    public int compareTo(Temperatur o) {
        if(o == null) {return 1;}
              
        return new CompareToBuilder().append(navn, o.navn).toComparison();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Temperatur)) {return false;}
        
        Temperatur other = (Temperatur) o;
        
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }

    /**
     * Id
     * 
     * @return
     */
    @Id
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Navn
     * 
     * @return
     */
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    /** {@inheritDoc} */
    public String getTextForGUI() {
        return (navn == null ? "" : navn);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(4001, 5099).append(id).toHashCode();
    }
    
    /**
     * Id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Navn
     * 
     * @param navn
     */
    public void setNavn(String navn) {
        this.navn = navn;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            toString();
    }
}
