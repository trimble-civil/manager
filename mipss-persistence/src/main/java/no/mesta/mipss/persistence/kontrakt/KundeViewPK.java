package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

@SuppressWarnings("serial")
public class KundeViewPK implements Serializable{

	private Long kundeId;
	private Long adresseId;

	public KundeViewPK(){
		
	}
	public KundeViewPK(Long kundeId, Long adresseId){
		this.kundeId = kundeId;
		this.adresseId = adresseId;
		
	}
	public void setKundeId(Long kundeId) {
		this.kundeId = kundeId;
	}
	public Long getKundeId() {
		return kundeId;
	}
	public void setAdresseId(Long adresseId) {
		this.adresseId = adresseId;
	}
	public Long getAdresseId() {
		return adresseId;
	}
}
