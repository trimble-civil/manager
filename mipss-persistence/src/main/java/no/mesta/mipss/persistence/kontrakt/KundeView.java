package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(
        name = KundeView.QUERY_FIND_ALL,
        query = "SELECT o FROM KundeView o")
    })
@Table(name = "KUNDE_V")
@IdClass(KundeViewPK.class)
public class KundeView implements Serializable, IRenderableMipssEntity {
	public static final String QUERY_FIND_ALL = "KundeView.findAll";
	
	private Long kundeId;
	private String kundeNavn;
	private String kundeNr;
	private Long termId;
	private Long adresseId;
	private String adresse1;
	private String adresse2;
	private String adresse3;
	private String postnr;
	private String poststed;
	private String status;
	private String kundeType;
	
	public KundeView() {
	}
	
	@Id
	@Column(name = "KUNDE_ID")
	public Long getKundeId() {
		return kundeId;
	}

	public void setKundeId(Long kundeId) {
		this.kundeId = kundeId;
	}

	@Column(name = "KUNDE_NAVN")
	public String getKundeNavn() {
		return kundeNavn;
	}

	public void setKundeNavn(String kundeNavn) {
		this.kundeNavn = kundeNavn;
	}

	@Column(name = "KUNDE_NR")
	public String getKundeNr() {
		return kundeNr;
	}

	public void setKundeNr(String kundeNr) {
		this.kundeNr = kundeNr;
	}

	@Column(name = "TERM_ID")
	public Long getTermId() {
		return termId;
	}

	public void setTermId(Long termId) {
		this.termId = termId;
	}

	@Id
	@Column(name = "ADRESSE_ID")
	public Long getAdresseId() {
		return adresseId;
	}

	public void setAdresseId(Long adresseId) {
		this.adresseId = adresseId;
	}

	public String getAdresse1() {
		return adresse1;
	}

	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	public String getAdresse2() {
		return adresse2;
	}

	public void setAdresse2(String adresse2) {
		this.adresse2 = adresse2;
	}

	public String getAdresse3() {
		return adresse3;
	}

	public void setAdresse3(String adresse3) {
		this.adresse3 = adresse3;
	}

	public String getPostnr() {
		return postnr;
	}

	public void setPostnr(String postnr) {
		this.postnr = postnr;
	}

	public String getPoststed() {
		return poststed;
	}

	public void setPoststed(String poststed) {
		this.poststed = poststed;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "KUNDE_TYPE")
	public String getKundeType() {
		return kundeType;
	}

	public void setKundeType(String kundeType) {
		this.kundeType = kundeType;
	}

	public String getTextForGUI() {
		return getKundeNr() + " - " + getKundeNavn();
	}
}
