package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "kjoretoyUtstyrIoIdSeq", sequenceName = "KJORETOY_UTSTYR_IO_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = KjoretoyUtstyrIo.FIND_ALL, 
    		query = "select o from KjoretoyUtstyrIo o"),
    @NamedQuery(name = KjoretoyUtstyrIo.LIST_FOR_KJORETOYUTSTYR, 
    		query = "select io from KjoretoyUtstyrIo io where io.kjoretoyUtstyr.id = :id",
    		hints={@QueryHint(name=QueryHints.REFRESH, value=HintValues.TRUE)})
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class, 
	no.mesta.mipss.persistence.kjoretoyutstyr.UtstyrIoBitNrManager.class/*,
	PeriodeIoMonitorKjoretoyUtstyrIO.class*/})

@Table(name = "KJORETOY_UTSTYR_IO")

public class KjoretoyUtstyrIo implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
	public static final String FIND_ALL = "KjoretoyUtstyrIo.findAll";
	public static final String LIST_FOR_KJORETOYUTSTYR = "KjoretoyUtstyrIo.ListForKjoretoyUtstyr";
	
    private Date fraDato;
    private Long id;
    private Date tilDato;
    private Sensortype sensortype;
    private KjoretoyUtstyr kjoretoyUtstyr;
    
    // felt lagt til for h�nd
    private OwnedMipssEntity ownedMipssEntity;
    private Ioliste ioliste;
    private Long bitNr;
    private PropertyChangeSupport props;
    private transient Logger logger;
    private String sensortypeNavn;
    
    private final UUID uuid;
    
    public KjoretoyUtstyrIo() {
    	props = new PropertyChangeSupport(this);
    	uuid = UUID.randomUUID();
    }

    @Column(name="FRA_DATO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getFraDato() {
        return fraDato;
    }

    public void setFraDato(Date fraDato) {
    	Date old = this.fraDato;
        this.fraDato = fraDato;
        logSetter("fraDato", old, fraDato);
        props.firePropertyChange("fraDato", old, fraDato);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kjoretoyUtstyrIoIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="TIL_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTilDato() {
        return tilDato;
    }

    public void setTilDato(Date tilDato) {
    	Date old = this.tilDato;
        this.tilDato = tilDato;
        logSetter("tilDato", old, tilDato);
        props.firePropertyChange("tilDato", old, tilDato);
    }

    @ManyToOne
    @JoinColumn(name = "SENSORTYPE_NAVN", referencedColumnName = "NAVN")
    public Sensortype getSensortype() {
        return sensortype;
    }

    /**
     * setter Sensortype og kopierer dens navn til sensortypeNavn.
     * @param sensortype
     */
    public void setSensortype(Sensortype sensortype) {
    	Sensortype old = this.sensortype;
        this.sensortype = sensortype;
        
        sensortypeNavn = sensortype != null ? sensortype.getNavn() : null;
        
        logSetter("sensortype", old, sensortype);
        props.firePropertyChange("sensortype", old, sensortype);
    }

    @ManyToOne
    @JoinColumn(name = "KJORETOY_UTSTYR_ID", referencedColumnName = "ID")
    public KjoretoyUtstyr getKjoretoyUtstyr() {
        return kjoretoyUtstyr;
    }

    /**
     * Setter b�de dette feltet og kjoretoyUtstyrId hvis hvis kallparameterert
     * ikke er null.
     * @param kjoretoyUtstyr
     */
    public void setKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
    	Object oldGruppe = getUtstyrgruppe();
    	Object oldSubgruppe = getUtstyrsubgruppe();
    	Object oldModell = getUtstyrmodell();
    	KjoretoyUtstyr old = this.kjoretoyUtstyr;
        this.kjoretoyUtstyr = kjoretoyUtstyr;
        logSetter("kjoretoyUtstyr", old, kjoretoyUtstyr);
        props.firePropertyChange("kjoretoyUtstyr", old, kjoretoyUtstyr);
        props.firePropertyChange("utstyrgruppe", oldGruppe, getUtstyrgruppe());
        props.firePropertyChange("utstyrsubgruppe", oldSubgruppe, getUtstyrsubgruppe());
        props.firePropertyChange("utstyrmodell", oldModell, getUtstyrmodell());
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("kjoreoty id", getKjoretoyUtstyr().getId()).
            append("sensortypeNavn", getSensortypeNavn()).
            append("fraDato", getFraDato()).
            append("tilDato", getTilDato()).
            append("bitNr", getBitNr()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof KjoretoyUtstyrIo)) {
            return false;
        }
        KjoretoyUtstyrIo other = (KjoretoyUtstyrIo) o;
        
        if(getId() != null && other.getId() != null) {
        	return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
        } else if(getId() == null && other.getId() == null) {
        	return new EqualsBuilder().
            append(uuid, other.uuid).
            isEquals();
        } else {
        	return false;
        }
        
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return String.valueOf(getId());
    }

    public int compareTo(Object o) {
        KjoretoyUtstyrIo other = (KjoretoyUtstyrIo) o;
            return new CompareToBuilder().
                append(getFraDato(), other.getFraDato()).
                toComparison();
    }

    public void setIoliste(Ioliste ioliste) {
    	Ioliste old = this.ioliste;
        this.ioliste = ioliste;
        if(ioliste != null && ioliste.getBitNr() != null) {
        	setBitNr(ioliste.getBitNr());
        } else if(ioliste == null) {
        	setBitNr(null);
        }
        logSetter("ioliste", old, ioliste);
        props.firePropertyChange("ioliste", old, ioliste);
    }

    @ManyToOne
    @JoinColumn(name = "IOLISTE_ID", referencedColumnName = "ID")
    public Ioliste getIoliste() {
        return ioliste;
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }

    @Column(name="BIT_NR")
    public Long getBitNr() {
        return bitNr;
    }

    /**
     * setter BitNr som er en intern verdi. Utl�ser ikke eventer.
     * @param bitNr
     */
    public void setBitNr(Long bitNr) {
        this.bitNr = bitNr;
    }
    
    /**
     * Legger til den leverte {@code PropertyChangeListener} for alle felt.
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Legger til den leverte {@code PropertyChangeListener} for feltet nevnt i {@code propName}
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Fjerner den leverte {@code PropertyChangeListener}
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Fjerner den leverte {@code PropertyChangeListener} fra feltet i {@code propName}
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
    
    /**
     * loggemetode for settere.
     * @param setter
     * @param oldO
     * @param newO
     */
    private void logSetter(String setter, Object oldO, Object newO) {
    	if(logger == null) {
    		logger = LoggerFactory.getLogger(KjoretoyUtstyrIo.class);
    	}
    	logger.debug("Setter " + setter + " fra: " + oldO + " til: " + newO);
    }

	/**
	 * @return the sensortypeNavn
	 */
    @Transient
	public String getSensortypeNavn() {
		return sensortypeNavn;
	}

	/**
	 * Hjelpefelt: genererer ikke events.
	 * @param sensortypeNavn the sensortypeNavn to set
	 */
	public void setSensortypeNavn(String sensortypeNavn) {
		this.sensortypeNavn = sensortypeNavn;
	}
	
	@Transient
	public String getDfuModellNavn() {
		return ioliste != null ? ioliste.getDfumodell() != null ? ioliste.getDfumodell().getNavn() : null : null;
	}
	
	@Transient
	public String getIolisteNavn() {
		return ioliste != null ? ioliste.getNavn1() : "";
	}
	
	@Transient
	public Utstyrgruppe getUtstyrgruppe() {
		return kjoretoyUtstyr != null ? kjoretoyUtstyr.getUtstyrgruppe() : null;
	}
	
	@Transient
	public Utstyrsubgruppe getUtstyrsubgruppe() {
		return kjoretoyUtstyr != null ? kjoretoyUtstyr.getUtstyrsubgruppe() : null;
	}
	
	@Transient
	public Utstyrmodell getUtstyrmodell() {
		return kjoretoyUtstyr != null ? kjoretoyUtstyr.getUtstyrmodell() : null;
	}
}
