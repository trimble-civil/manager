package no.mesta.mipss.persistence.mipss1;

import java.io.Serializable;

import java.util.Date;

@SuppressWarnings("serial")
public class StrekningTid implements Serializable{

    private String enhetId;
    private Date tidStart;
    private Date tidStopp;
    public StrekningTid() {
    }

    public void setEnhetId(String enhetId) {
        this.enhetId = enhetId;
    }

    public String getEnhetId() {
        return enhetId;
    }

    public void setTidStart(Date tidStart) {
        this.tidStart = tidStart;
    }

    public Date getTidStart() {
        return tidStart;
    }

    public void setTidStopp(Date tidStopp) {
        this.tidStopp = tidStopp;
    }

    public Date getTidStopp() {
        return tidStopp;
    }
}
