package no.mesta.mipss.persistence.kjoretoyordre;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
public class KjoretoyordreUtstyrPK implements Serializable{
	private Long ordreId;
	private Long utstyrId;

	public KjoretoyordreUtstyrPK() {
	}
	
	public KjoretoyordreUtstyrPK(Long ordreId, Long utstyrId) {
		this.ordreId = ordreId;
		this.utstyrId = utstyrId;
	}

	public Long getOrdreId() {
		return ordreId;
	}

	public void setOrdreId(Long ordreId) {
		this.ordreId = ordreId;
	}

	public Long getUtstyrId() {
		return utstyrId;
	}

	public void setUtstyrId(Long utstyrId) {
		this.utstyrId = utstyrId;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ordreId).append(utstyrId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof KjoretoyordreUtstyrPK)) {return false;}
        
        KjoretoyordreUtstyrPK other = (KjoretoyordreUtstyrPK) o;
        
        return new EqualsBuilder().append(ordreId, other.ordreId).append(utstyrId, other.utstyrId).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("ordreId", ordreId).
            append("utstyrId", utstyrId).
            toString();
    }
}
