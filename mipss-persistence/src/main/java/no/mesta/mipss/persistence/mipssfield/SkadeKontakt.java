package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;
import java.util.ResourceBundle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Skadekontakt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings({ "serial" })
@Entity
@NamedQueries({
    @NamedQuery(name = SkadeKontakt.QUERY_FIND_ALL, query = "SELECT s FROM SkadeKontakt s"),
    @NamedQuery(name = SkadeKontakt.QUERY_FIND_BY_FORSIKRINGSSKADE, query = "SELECT s FROM SkadeKontakt s where s.skade.guid=:guid")
    })
@Table(name="SKADE_KONTAKT_XV")
public class SkadeKontakt extends MipssEntityBean<SkadeKontakt> implements Serializable,IRenderableMipssEntity {
	public static final String QUERY_FIND_BY_FORSIKRINGSSKADE = "SkadeKontakt.findByForsikringsskade";
	public static final String QUERY_FIND_ALL = "SkadeKontakt.findAll";
	
	public static final String BERGINSSELSKAP = "Bergingsselskap";
    public static final String EIER_AV_KJORETOY = "Skadevolder - Eier";
    public static final String FORER_AV_KJORETOY = ResourceBundle.getBundle("persistanceTxts").getString("forer");
    public static final String FORSIKRINGSELSKAP = "Skadevolder - Forsikringsselsk";
    public static final String LENSMANN = "Lensmann";
	public static final String POLITI = "Politi";
	public static final String VITNE = "Vitne";
	@Column(name = "ADRESSE")
    private String adresse;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dato;
    @Column(name="EPOST_ADRESSE")
    private String epostAdresse;
    @Id
    @Column(nullable = false)
    private String guid;
    @Transient
    private String[] idNames = new String[]{"guid"};
    @Column(name="KONTAKT_NAVN")
    private String kontaktNavn;
    @Column(name="KONTAKT_TLFNUMMER")
    private Long kontaktTlfnummer;
    @Column(name = "KONTAKT_ID", nullable = false, insertable = false, updatable = false)
    private Long kontaktId;
    @Column(name = "NAVN")
    private String navn;
    @Column(name = "POSTNUMMER")
    private Long postnummer;
    @Column(name = "POSTSTED")
    private String poststed;
    @Column(name = "RESULTAT")
    private String resultat;
    @ManyToOne
    @JoinColumn(name = "SKADE_GUID", referencedColumnName = "GUID")
    private Forsikringsskade skade;
    @Column(name = "TLFNUMMER")
	private Long tlfnummer;
    @ManyToOne
    @JoinColumn(name = "KONTAKT_ID", referencedColumnName = "ID")
	private Skadekontakttype type;
    
    /**
     * Konstruktør
     * 
     */
    public SkadeKontakt() {
    }

    public boolean isBlank(){
    	return StringUtils.isBlank(adresse)&&
    		   StringUtils.isBlank(epostAdresse)&&
    		   StringUtils.isBlank(kontaktNavn)&&
    		   kontaktTlfnummer==null&&
    		   StringUtils.isBlank(navn)&&
    		   postnummer==null&&
    		   StringUtils.isBlank(poststed)&&
    		   StringUtils.isBlank(resultat)&&
    		   tlfnummer==null;
    }
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof SkadeKontakt)) {return false;}
        
        SkadeKontakt other = (SkadeKontakt) o;
        
        return new EqualsBuilder().append(guid, other.getGuid()).isEquals();
    }
    /**
     * Adresse
     * 
     * @return
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Dato
     * 
     * @return
     */
    public Date getDato() {
        return dato;
    }

    /**
     * E-post
     * 
     * @return
     */
    public String getEpostAdresse() {
        return epostAdresse;
    }

    public String getGuid(){
    	return guid;
    }

    @Override
	public String[] getIdNames() {
		return idNames;
	}

    
    public String getKontaktNavn() {
        return kontaktNavn;
    }
   
   	/**
     * Tlf nummer
     * 
     * @return
     */
    public Long getKontaktTlfnummer() {
    	return kontaktTlfnummer;
    }
    /**
     * @return
     */
    public Long getKontaktId() {
        return kontaktId;
    }

    /**
     * Navn
     * 
     * @return
     */
    public String getNavn() {
        return navn;
    }

    /**
     * Postnummer
     * 
     * @return
     */
    public Long getPostnummer() {
        return postnummer;
    }

    /**
     * Poststed
     * 
     * @return
     */
    public String getPoststed() {
        return poststed;
    }

    /**
     * Resultat
     * 
     * @return
     */
    public String getResultat() {
        return resultat;
    }

    /**
     * Skade
     * 
     * @return
     */
    public Forsikringsskade getSkade() {
        return skade;        
    }

    public String getTextForGUI() {
		return "TODO";
	}

    /**
     * Tlf nummer
     * 
     * @return
     */
    public Long getTlfnummer() {
        return tlfnummer;
    }

    /**
     * Type
     * 
     * @return
     */
    public Skadekontakttype getType() {
        return type;
    }

    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2035, 2065).append(guid).toHashCode();
    }

    /**
     * Adresse
     * 
     * @param adresse
     */
    public void setAdresse(String adresse) {
    	String old = this.adresse;
        this.adresse = adresse;
        getProps().firePropertyChange("adresse", old, adresse);
    }

    /**
     * Dato
     * 
     * @param dato
     */
    public void setDato(Date dato) {
    	Date old = this.dato;
        this.dato = dato;
        getProps().firePropertyChange("dato", old, dato);
    }

    /**
     * E-post
     * 
     * @param epostAdresse
     */
    public void setEpostAdresse(String epostAdresse) {
    	String old = this.epostAdresse;
        this.epostAdresse = epostAdresse;
        getProps().firePropertyChange("epostAdresse", old, epostAdresse);
    }
    public void setGuid(String guid){
    	String old = this.guid;
   		this.guid = guid;
   		getProps().firePropertyChange("guid", old, guid);
   	}

    public void setKontaktNavn(String kontaktNavn) {
    	String old = this.kontaktNavn;
        this.kontaktNavn=kontaktNavn;
        getProps().firePropertyChange("kontaktNavn", old, kontaktNavn);
    }

    /**
     * Tlf nummer
     * 
     * @param tlfnummer2
     */
    public void setKontaktTlfnummer(Long kontaktTlfnummer) {
    	Long old = this.kontaktTlfnummer;
        this.kontaktTlfnummer = kontaktTlfnummer;
        getProps().firePropertyChange("kontaktTlfnummer", old, kontaktTlfnummer);
    }

    /**
     * @param kontakttypeNavn
     */
    public void setKontaktId(Long kontaktId) {
        Long old = this.kontaktId;
    	this.kontaktId = kontaktId;
        getProps().firePropertyChange("kontaktId", old, kontaktId);
    }

    /**
     * Navn
     * 
     * @param navn
     */
    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        getProps().firePropertyChange("navn", old, navn);
    }

    /**
     * Postnummer
     * 
     * @param postnummer
     */
    public void setPostnummer(Long postnummer) {
    	Long old = this.postnummer;
        this.postnummer = postnummer;
        getProps().firePropertyChange("postnummer", old, postnummer);
    }

    /**
     * Poststed
     * 
     * @param poststed
     */
    public void setPoststed(String poststed) {
    	String old = this.poststed;
        this.poststed = poststed;
        getProps().firePropertyChange("poststed", old, poststed);
    }
    
    /**
     * Resultat
     * 
     * @param resultat
     */
    public void setResultat(String resultat) {
    	String old = this.resultat;
        this.resultat = resultat;
        getProps().firePropertyChange("resultat", old, resultat);
    }

    /**
     * Skade
     * 
     * @param skade
     */
    public void setSkade(Forsikringsskade skade) {
    	Forsikringsskade old = this.skade;
        this.skade = skade;
        getProps().firePropertyChange("skade", old, skade);
    }
    
    /**
     * Tlf nummer
     * 
     * @param tlfnummer1
     */
    public void setTlfnummer(Long tlfnummer) {
    	Long old = this.tlfnummer;
        this.tlfnummer = tlfnummer;
        getProps().firePropertyChange("tlfnummer", old, tlfnummer);
    }

	/**
     * Type
     * 
     * @param type
     */
    public void setType(Skadekontakttype type) {
    	Skadekontakttype old = this.type;
        this.type = type;
        
        if(type != null) {
        	setKontaktId(type.getId());
        } else {
            setKontaktId(null);
        }
        getProps().firePropertyChange("type", old, type);
    }

	/**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
        	append("guid", guid).
            append("kontaktId", kontaktId).
            append("dato", dato).
            append("navn", navn).
            append("adresse", adresse).
            append("postnummer", postnummer).
            append("poststed", poststed).
            append("tlfnummer", tlfnummer).
            append("kontaktTlfnummer", kontaktTlfnummer).
            append("epostAdresse", epostAdresse).
            append("resultat", resultat).
            toString();
    }
}
