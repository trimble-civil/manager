package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class R12Prodtype implements Serializable, Cloneable{

	private Long id;
	private String header;
	private Long seq;
	private Boolean stroFlagg;
	private Double kjortKm;
	private Long kjortSekund;
	
	private Double kmt;
	private Double prosent;
	
	//Klipping
	private Double mengdeM2;
	
	private List<R12Prodtype> prodtyper;
	
	public static String[] getPropertiesArray() {
		return new String[] { "id", "header", "seq", "stroFlagg", "kjortKm", "kjortSekund", "mengdeM2"};
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public Boolean getStroFlagg() {
		return stroFlagg;
	}
	public void setStroFlagg(Boolean stroFlagg) {
		this.stroFlagg = stroFlagg;
	}
	public Double getKjortKm() {
		return kjortKm;
	}
	public void setKjortKm(Double kjortKm) {
		this.kjortKm = kjortKm;
	}
	public void addKjortKm(Double kjortKm){
		if (this.kjortKm==null){
			this.kjortKm = kjortKm;
		}else{
			if (kjortKm!=null){
				this.kjortKm+=kjortKm;
			}
		}
	}
	public Long getKjortSekund() {
		return kjortSekund;
	}
	public void setKjortSekund(Long kjortSekund) {
		this.kjortSekund = kjortSekund;
	}
	public void addKjortSekund(Long kjortSekund){
		if (this.kjortSekund==null){
			this.kjortSekund=kjortSekund;
		}else{
			if (kjortSekund!=null){
				this.kjortSekund+=kjortSekund;
			}
		}
	}
	public void addProdtype(R12Prodtype pt){
		if (prodtyper==null)
			prodtyper=new ArrayList<R12Prodtype>();
		prodtyper.add(pt);
	}
	/**
	 * Legger inn en ny prodtype, dersom denne eksisterer fra før vil
	 * verdiene slås sammen med eksisterende prodtype
	 * @param pt
	 */
	public void mergeProdtype(R12Prodtype pt){
		if (prodtyper==null){
			prodtyper=new ArrayList<R12Prodtype>();
		}
		boolean merged = false;
		for (R12Prodtype existing:prodtyper){
			if (existing.getId().equals(pt.getId())){
				existing.addKjortKm(pt.getKjortKm());
				existing.addKjortSekund(pt.getKjortSekund());
				existing.addMengdeM2(pt.getMengdeM2());
				merged=true;
			}
		}
		if (!merged){
			prodtyper.add(pt);
		}
	}
	public List<R12Prodtype> getProdtyper(){
		return prodtyper;
	}
	public Double getTid(){
		if (kjortSekund==null){
			return null;
		}
		return (double)kjortSekund/(double)86400;
	}
	public void setKmt(Double kmt) {
		this.kmt = kmt;
	}

	public Double getKmt() {
		return kmt;
	}

	public boolean equals(Object o){
		return id.equals(((R12Prodtype)o).getId());
	}
	
	@Override
	public R12Prodtype clone(){
		R12Prodtype pt = new R12Prodtype();
		pt.setId(id);
		pt.setSeq(seq);
		pt.setHeader(header);
		pt.setKjortKm(kjortKm);
		pt.setKjortSekund(kjortSekund);
		pt.setStroFlagg(stroFlagg);
		pt.setMengdeM2(mengdeM2);
		if (getProdtyper()!=null){
			for (R12Prodtype p:getProdtyper()){
				pt.addProdtype(p.clone());
			}
		}
		return pt;
	}

	public void setProsent(Double prosent) {
		this.prosent = prosent;
	}

	public Double getProsent() {
		return prosent;
	}
	
	//Klipping
	public void addMengdeM2(Double mengdeM2){
		if (this.mengdeM2==null){
			this.mengdeM2 = mengdeM2;
		}else{
			if (mengdeM2!=null){
				this.mengdeM2 += mengdeM2;
			}
		}
	}
	public Double getMengdeM2() {
		return mengdeM2;
	}
	public void setMengdeM2(Double mengdeM2) {
		this.mengdeM2 = mengdeM2;
	}
}
