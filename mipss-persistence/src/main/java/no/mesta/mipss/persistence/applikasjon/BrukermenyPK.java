package no.mesta.mipss.persistence.applikasjon;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BrukermenyPK implements Serializable {
    private Long menypunktId;
    private String signatur;

    public BrukermenyPK() {
    }

    public BrukermenyPK(Long menypunktId, String signatur) {
        this.menypunktId = menypunktId;
        this.signatur = signatur;
    }

    public boolean equals(Object other) {
        if (other instanceof BrukermenyPK) {
            final BrukermenyPK otherBrukermenyPK = (BrukermenyPK) other;
            final boolean areEqual = 
                (otherBrukermenyPK.menypunktId.equals(menypunktId) && otherBrukermenyPK.signatur.equals(signatur));
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void setMenypunktId(Long menypunktId) {
        this.menypunktId = menypunktId;
    }

    public Long getMenypunktId() {
        return menypunktId;
    }

    public void setSignatur(String signatur) {
        this.signatur = signatur;
    }

    public String getSignatur() {
        return signatur;
    }
}
