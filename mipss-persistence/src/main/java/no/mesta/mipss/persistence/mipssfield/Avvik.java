package no.mesta.mipss.persistence.mipssfield;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.jdesktop.observablecollections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.*;

/**
 * Funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name="AVVIK_XV")
@NamedQueries( { 
	@NamedQuery(name = Avvik.QUERY_FIND_ALL, query = "SELECT p FROM Avvik p"),
	@NamedQuery(name = Avvik.QUERY_FIND_ENTITY_INFO_BY_SAK, 
				query = "SELECT new no.mesta.mipss.persistence.mipssfield.FeltEntityInfo(a.guid, a.refnummer, a.slettetDato) FROM Avvik a where a.sakGuid=:sakGuid"),
	@NamedQuery(name = Avvik.QUERY_HENT_MED_REFNUMMER, query = "SELECT a FROM Avvik a where a.refnummer=:refnummer"),
	})

	@NamedNativeQueries( { 		
		@NamedNativeQuery(name = Avvik.QUERY_GET_BILDER_UTENRELASJONER, query = 
				"SELECT " + "b.GUID, b.FILNAVN, b.BILDE_LOB,  b.BESKRIVELSE,  b.HOYDE,  b.SMAABILDE_LOB,  b.BREDDE, "
				+ "b.OPPRETTET_DATO,  b.ENDRET_AV,  b.OPPRETTET_AV,  b.ENDRET_DATO  FROM "
				+ "AVVIK_BILDE_XV sb  JOIN Bilde b ON sb.BILDE_GUID = b.GUID  WHERE  sb.AVVIK_GUID = ?1 " 
				+ "and not exists (select * from skade_bilde_xv where bilde_guid=b.guid) "
				+ "and not exists (select * from hendelse_bilde_xv where bilde_guid=b.guid) "
				+ "and not exists (select * from skred_bilde_xv where bilde_guid=b.guid) "
				+ " ", resultSetMapping = "BildeResults")})
@SequenceGenerator(name = Avvik.ID_SEQ, sequenceName = "AVVIK_REFNUMMER_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.mipssfield.listener.AvvikListener.class)
public class Avvik extends MipssEntityBean<Avvik> implements Serializable, IRenderableMipssEntity,
		Comparable<Avvik>, MipssFieldDetailItem, ProcessRelated {

	private final static Logger log = LoggerFactory.getLogger(Avvik.class);

	private static final String BLANK = "";
	private static final String BY = resolvePropertyText("by");
	private static final String FIELD_NAME_BESKRIVELSE = "beskrivelse";
	private static final String FIELD_NAME_BILDE = "bilde";
	private static final String FIELD_NAME_BILDER = "bilder";
	private static final String FIELD_NAME_ETTERSLEPFLAGG = "etterslepFlagg";
	private static final String FIELD_NAME_LUKKETDATO = "lukketDato";
	private static final String FIELD_NAME_OPPRETTETAV = "opprettetAv";
	private static final String FIELD_NAME_OPPRETTETDATO = "opprettetDato";
	private static final String FIELD_NAME_ENDRETAV = "endretAv";
	private static final String FIELD_NAME_ENDRETDATO = "endretDato";
	private static final String FIELD_NAME_PROSESS = "prosess";
	private static final String FIELD_NAME_PROSESSID = "prosessId";
	private static final String FIELD_NAME_SLETTETAV = "slettetAv";
	private static final String FIELD_NAME_SLETTETDATO = "slettetDato";
	private static final String FIELD_NAME_SORTERTE_BILDER = "sortertebilder";
	private static final String FIELD_NAME_STATUS = "status";
	private static final String FIELD_NAME_STATUS_BILDE = "statusBilde";
	private static final String FIELD_NAME_STATUSER = "statuser";
	private static final String FIELD_NAME_STATUSHISTORY = "statusHistory";
	private static final String FIELD_NAME_STEDFESTING = "stedfesting";
	private static final String FIELD_NAME_TILSTAND = "tilstand";
	private static final String FIELD_NAME_TILSTANDID = "tilstandId";
	private static final String FIELD_NAME_TILTAKSDATO = "tiltaksDato";
	private static final String FIELD_NAME_VEIREF = "veiref";
	private static final String ID_NAME = "guid";
	private static final String[] ID_NAMES = new String[] { ID_NAME };
	public static final String ID_SEQ = "avvikRefnummerSeq";
	public static final String QUERY_FIND_ALL = "Avvik.findAll";
	public static final String QUERY_FIND_ENTITY_INFO_BY_SAK = "Avvik.findGuidBySak";
	private static final String SPACE = " ";
	public static final String QUERY_HENT_MED_REFNUMMER = "avvik.hentMedRefnummer";
	public static final String QUERY_GET_BILDER_UTENRELASJONER = "avvik.getBilderUtenrelasjoner";

	@Column(name = "BESKRIVELSE", length = 1000)
	private String beskrivelse;
	@Column(name = "ETTERSLEP_FLAGG", nullable = false)
	private Boolean etterslep;
	@Column(name = "ENDRET_AV")
	private String endretAv;
	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;
	@Id
	@Column(nullable = false)
	private String guid;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LUKKET_DATO")
	private Date lukketDato;
	@Column(name = "OPPRETTET_AV")
	private String opprettetAv;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OPPRETTET_DATO")
	private Date opprettetDato;
	@ManyToOne
	@JoinColumn(name = "PROSESS_ID", referencedColumnName = "ID")
	private Prosess prosess;
	@Column(name = "PROSESS_ID", nullable = false, insertable = false, updatable = false)
	private Long prosessId;
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Avvik.ID_SEQ)
	private Long refnummer;
	
	@Column(name = "SLETTET_AV")
	private String slettetAv;
	@Column(name = "SLETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date slettetDato;
	@Transient
	private transient List<AvvikBilde> sortertebilder;
	
	@OneToMany(mappedBy = "avvik", cascade = { CascadeType.ALL }, orphanRemoval=true)
	private List<AvvikStatus> statuser = new ArrayList<AvvikStatus>();
	
	@OneToOne(optional = false, mappedBy = "avvik", cascade = { CascadeType.ALL })
	private Punktstedfest stedfesting;
	@ManyToOne
	@JoinColumn(name = "TILSTAND_ID", referencedColumnName = "ID")
	private Avviktilstand tilstand;
	@Column(name = "TILSTAND_ID", nullable = true, insertable = false, updatable = false)
	private Long tilstandId;
	@ManyToOne
	@JoinColumn(name = "AARSAK_ID", referencedColumnName = "ID")
	private Avvikaarsak aarsak;
	@Column(name = "AARSAK_ID", nullable = true, insertable = false, updatable = false)
	private Long aarsakId;
	@Column(name = "TILTAKS_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tiltaksDato;
	@OneToMany(mappedBy = "avvik", cascade = CascadeType.ALL, orphanRemoval=true)
	private List<AvvikBilde> usortertebilder = new ArrayList<AvvikBilde>();
	@Transient
	private Date sisteStatusDatoRapport;
	@Transient
	private String sisteStatusRapport;
	@Transient
	private String prosessRapport;
	@Column(name="ESTIMAT")
	private String estimat;
	
	@ManyToOne
	@JoinColumn(name = "SAK_GUID", referencedColumnName = "GUID", nullable=false)
	private Sak sak;
	@Column(name = "SAK_GUID", nullable = true, insertable = false, updatable = false)
	private String sakGuid;
	
	
	@Transient
	private boolean mangel;
	
	@Transient
	private String prosessString;
	private String merknad;

    @ManyToOne
    @JoinColumn(name = "ARBEIDTYPE_ID", referencedColumnName = "ID")
	private ArbeidType arbeidType;

    @ManyToOne
    @JoinColumn(name = "OBJEKTTYPE_ID", referencedColumnName = "ID")
	private ObjektType objektType;

    @ManyToOne
    @JoinColumn(name = "OBJEKTAVVIKTYPE_ID", referencedColumnName = "ID")
	private ObjektAvvikType objektAvvikType;

    @ManyToOne
    @JoinColumn(name = "OBJEKTAVVIKKATEGORI_ID", referencedColumnName = "ID")
	private ObjektAvvikKategori objektAvvikKategori;

    @ManyToOne
    @JoinColumn(name = "OBJEKTSTEDTYPEANGIVELSE_ID", referencedColumnName = "ID")
	private ObjektStedTypeAngivelse objektStedTypeAngivelse;

	public ArbeidType getArbeidType() {
		return arbeidType;
	}

	public void setArbeidType(ArbeidType arbeidType) {
        ArbeidType old = this.arbeidType;
		this.arbeidType = arbeidType;
        firePropertyChange("arbeidType", old, arbeidType);
	}



	public ObjektType getObjektType() {
		return objektType;
	}

	public void setObjektType(ObjektType objektType) {
		ObjektType old = this.objektType;
		this.objektType = objektType;
		firePropertyChange("objektType", old, objektType);
	}

	public ObjektAvvikType getObjektAvvikType() {
		return objektAvvikType;
	}

	public void setObjektAvvikType(ObjektAvvikType objektAvvikType) {
		ObjektAvvikType old = this.objektAvvikType;
		this.objektAvvikType = objektAvvikType;
		firePropertyChange("objektAvvikType", old, objektAvvikType);
	}

	public ObjektAvvikKategori getObjektAvvikKategori() {
		return objektAvvikKategori;
	}

	public void setObjektAvvikKategori(ObjektAvvikKategori objektAvvikKategori) {
		ObjektAvvikKategori old = this.objektAvvikKategori;
		this.objektAvvikKategori = objektAvvikKategori;
		firePropertyChange("objektAvvikKategori", old, objektAvvikKategori);
	}

	public ObjektStedTypeAngivelse getObjektStedTypeAngivelse() {
		return objektStedTypeAngivelse;
	}

	public void setObjektStedTypeAngivelse(ObjektStedTypeAngivelse objektStedTypeAngivelse) {
		ObjektStedTypeAngivelse old = this.objektStedTypeAngivelse;
		this.objektStedTypeAngivelse = objektStedTypeAngivelse;
		firePropertyChange("objektStedTypeAngivelse", old, objektStedTypeAngivelse);
	}

	public Sak getSak() {
		return sak;
	}

	public void setMangel(boolean mangel){
		this.mangel = mangel;
	}
	/**
	 * Forteller om avviket er en mangel eller ikke
	 * 
	 * Ny definisjon: 
	 * Om avviket er en mangel eller ikke defineres av databasefunksjonen : feltstudio_pk.mangel_sjekk
	 * 
	 * Gammel definisjon:
	 * Avviket er en mangel dersom tiltaksdato er satt og datoen er:
	 * <p>1. F�r enn dagens dato<br>
	 * 2. Avviket er lukket etter tiltaksdato
	 * </p>
	 * @return
	 */
	public boolean isMangel(){
		return mangel;
	}
	public String getSakGuid() {
		return sakGuid;
	}

	public String getEndretAv(){
		return this.endretAv;
	}
	public Date getEndretDato(){
		return this.endretDato;
	}
	public void setSak(Sak sak) {
		Sak old = this.sak;
		this.sak = sak;
		if (sak!=null){
			setSakGuid(sak.getGuid());
		}else{
			setSakGuid(null);
		}
		firePropertyChange("sak", old, sak);
			
	}

	public void setSakGuid(String sakGuid) {
		String old = this.sakGuid;
		this.sakGuid = sakGuid;
		firePropertyChange("sakGuid", old, sakGuid);
	}

	/**
	 * Konstruktør
	 * 
	 */
	public Avvik() {
		log("Punktreg() constructed");
	}

	public void addBilde(AvvikBilde pb) {
		Bilde oldBilde = getBilde();
		List<AvvikBilde> old = new ArrayList<AvvikBilde>(usortertebilder);
		List<AvvikBilde> oldSorterte = new ArrayList<AvvikBilde>(getSorterteBilder());
		AvvikBilde statusBilde = getStatusBilde();

		usortertebilder.add(pb);

		firePropertyChange(FIELD_NAME_BILDE, oldBilde, getBilde());
		firePropertyChange(FIELD_NAME_BILDER, old, usortertebilder);
		firePropertyChange(FIELD_NAME_SORTERTE_BILDER, oldSorterte, getSorterteBilder());
		firePropertyChange(FIELD_NAME_STATUS_BILDE, statusBilde, getStatusBilde());
	}

	/**
	 * Legger til en status
	 * 
	 * @param status
	 */
	public void addStatus(AvvikStatus status) {
		log.trace("addStatus", status);
		AvvikBilde statusBilde = getStatusBilde();
		List<AvvikStatus> oldStatuser = new ArrayList<AvvikStatus>(this.statuser);
		AvvikStatus oldStatus = getStatus();
		Bilde bilde = getBilde();
		List<AvvikStatus> oldHistory = new ArrayList<AvvikStatus>(getStatusHistory());

		if (status == null) {
			throw new IllegalArgumentException("status cannot be null");
		}
		if (statuser == null) {
			throw new IllegalStateException("statuser List is null");
		}

		status.setAvvik(this);
		statuser.add(status);

		firePropertyChange(FIELD_NAME_STATUSER, oldStatuser, statuser);
		firePropertyChange(FIELD_NAME_STATUS, oldStatus, getStatus());
		firePropertyChange(FIELD_NAME_STATUSHISTORY, oldHistory, getStatusHistory());
		firePropertyChange(FIELD_NAME_BILDE, bilde, getBilde());
		firePropertyChange(FIELD_NAME_STATUS_BILDE, statusBilde, getStatusBilde());
	}

	@Override
	public int compareTo(Avvik o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(opprettetDato, o.opprettetDato).append(opprettetAv, o.opprettetAv)
				.toComparison();
	}

	/**
	 * Gir ut antallet bilder
	 * 
	 * @return
	 */
	@Transient
	public int getAntallBilder() {
		if (usortertebilder == null) {
			return 0;
		} else {
			return usortertebilder.size();
		}
	}

	/**
	 * Beskrivelse
	 * 
	 * @return
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Henter fortrinns ut bildet som har samme status som funnet. Hvis ikke så
	 * tar den bildet med neste status osv.
	 * 
	 * @return
	 */
	@Transient
	public Bilde getBilde() {
		if (getBilder() == null || getBilder().size() == 0) {
			return null;
		}

		List<AvvikBilde> statusBilder = getSorterteBilder();

		if (statusBilder == null || statusBilder.size() == 0) {
			return null;
		}

		AvvikStatus s = getStatus();
		if (s == null) {
			return statusBilder.get(0).getBilde();
		}

		for (AvvikBilde pb : statusBilder) {
			if (s.getAvvikstatusType() != null && s.getAvvikstatusType().equals(pb.getStatusType())) {
				// Avbryt og returner bildet
				return pb.getBilde();
			}
		}

		// Returner bilde med høyeste status
		return statusBilder.get(0).getBilde();
	}

	/**
	 * Gir ut et bilde for en gitt status
	 * 
	 * @param status
	 * @return
	 */
	public Bilde getBilde(String status) {
		if (getBilder() == null || getBilder().size() == 0) {
			return null;
		}

		List<AvvikBilde> sortertebilder = getBilder();

		for (AvvikBilde pb : sortertebilder) {
			if (StringUtils.equals(status, pb.getStatusType().getStatus())) {
				return pb.getBilde();
			}
		}

		return null;
	}

	/**
	 * Bilder, bilder er alltid usortert
	 * 
	 * @return
	 */
	public List<AvvikBilde> getBilder() {
		return usortertebilder;
	}


	/**
	 * Etterslep flagg
	 * 
	 * @return
	 */
	public Boolean getEtterslepFlagg() {
		return etterslep;
	}

	/**
	 * Id
	 * 
	 * @return
	 */
	public String getGuid() {
		return guid;
	}

	@Transient
	@Override
	public String[] getIdNames() {
		return ID_NAMES;
	}
	
	@Transient
	@Override
	public int hashCode(){
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(getGuid());
		return builder.toHashCode();
	}

	/**
	 * Når ble funnet lukket
	 * 
	 * @return
	 */
	public Date getLukketDato() {
		return lukketDato;
	}

	/**
	 * Hvem opprettet funnet
	 * 
	 * @return
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}

	/**
	 * Når ble funnet opprettet
	 * 
	 * @return
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	/**
	 * Lager gui tekst av produserte strekninger
	 * 
	 * @return
	 */
	@Transient
	public String getProdHpGUIText() {
		return VeiInfoUtil.buildVeiInfo(getVeiref()).toString();
	}

	/**
	 * Gir ut alle HP som det er produksjon på
	 * 
	 * @return
	 */
	@Transient
	public List<VeiInfo> getProdHpStrekninger() {
		return HPFilterUtil.filterPunktveirefOnHp(Collections.singletonList(getStedfesting().getVeiref()));
	}
	
	/**
	 * Hovedprosessen dette funnet er knyttet til
	 * 
	 * @return
	 */
	public Prosess getProsess() {
		return prosess;
	}

	@Transient
	public String getProsessString(){
		if (getProsess() != null && getProsess().getProsessString() != null) {
			return getProsess().getProsessString();
		}
		return "";
	}
	
	@Transient
	public Prosess getRotprosessForPdfRapport(){
		Prosess p = prosess;
		if (p!=null){
			while(p.getEierprosess()!=null)
				p = p.getEierprosess();
		}
		return p;
	}
	/**
	 * Hovedprosessen dette funnet er knyttet til
	 * 
	 * @return
	 */
	public Long getProsessId() {
		return prosessId;
	}


	public Long getRefnummer(){
		return refnummer;
	}


	@Transient
	public Date getSisteStatusDato() {
		if (getStatus() != null) {
			return getStatus().getOpprettetDato();
		} else {
			return null;
		}
	}
	
	@Transient
	public Date getSisteStatusDatoRapport(){
		return sisteStatusDatoRapport;
	}
	public void setSisteStatusDatoRapport(Date sisteStatusDatoRapport){
		this.sisteStatusDatoRapport = sisteStatusDatoRapport;
	}
	@Transient
	public String getSisteStatusRapport(){
		return sisteStatusRapport;
	}
	public void setSisteStatusRapport(String sisteStatusRapport){
		this.sisteStatusRapport = sisteStatusRapport;
	}
	@Transient
	public String getProsessRapport(){
		return prosessRapport;
	}
	public void setProsessRapport(String prosessRapport){
		this.prosessRapport = prosessRapport;
	}


	@Transient
	public AvvikstatusType getSisteStatusType() {
		if (getStatus() != null) {
			return getStatus().getAvvikstatusType();
		} else {
			return null;
		}
	}

	public String getSlettetAv() {
		return slettetAv;
	}

	public Date getSlettetDato() {
		return slettetDato;
	}

	/**
	 * Bilder, de er alltid sorterte
	 * 
	 * @return
	 */
	@Transient
	public List<AvvikBilde> getSorterteBilder() {
		if (sortertebilder == null) {
			sortertebilder = new ArrayList<AvvikBilde>(usortertebilder);
			Collections.sort(sortertebilder);
		}

		return sortertebilder;
	}

	/**
	 * Returnerer funnets siste status
	 * 
	 * @return
	 */
	@Transient
	public AvvikStatus getStatus() {
		List<AvvikStatus> stats = getStatusHistory();

		return (stats == null || stats.size() == 0 ? null : stats.get(stats.size() - 1));
	}

	/**
	 * Finner når funnet hadde en gitt status
	 * 
	 * @param statusText
	 * @return
	 */
	@Transient
	public AvvikStatus getStatus(final String statusText) {
		List<AvvikStatus> stats = getStatusHistory();

		if (stats == null) {
			return null;
		}

		AvvikStatus status = null;
		int i = stats.size() - 1;
		boolean found = false;
		do {
			AvvikStatus s = stats.get(i);
			if (StringUtils.equals(statusText, s.getAvvikstatusType().getStatus())) {
				status = s;
				found = true;
			}
			i--;
		} while (i > -1 && !found);

		return status;
	}

	/**
	 * Gir ut bildet som har samme status som funnet
	 * 
	 * @return
	 */
	@Transient
	public AvvikBilde getStatusBilde() {
		AvvikStatus status = getStatus();

		if (status != null) {
			for (AvvikBilde pb : getSorterteBilder()) {
				if (status.equals(pb.getStatusType())) {
					return pb;
				}
			}
		} else {
			return null;
		}

		return null;
	}

	/**
	 * Statuser
	 * 
	 * @return
	 */
	public List<AvvikStatus> getStatuser() {
		return statuser;
	}

	/**
	 * Gir en sortert liste over historikken til funnet
	 * 
	 * @return
	 */
	@Transient
	public List<AvvikStatus> getStatusHistory() {
		if (getStatuser() == null || getStatuser().size() == 0) {
			return Collections.emptyList();
		}

		List<AvvikStatus> stats = new ArrayList<AvvikStatus>(getStatuser());
		Collections.sort(stats);

		log.trace("getStatusHistory({})", stats);
		return stats;
	}

	/**
	 * Gir de typene som dette funnet har i sine statuser
	 * 
	 * @return
	 */
	@Transient
	public Set<AvvikstatusType> getStatusTyper() {
		Set<AvvikstatusType> set = new TreeSet<AvvikstatusType>();

		for (AvvikStatus ps : getStatusHistory()) {
			set.add(ps.getAvvikstatusType());
		}

		return set;
	}

	/**
	 * Stedfesting av hendelsen
	 * 
	 * @return
	 */
	public Punktstedfest getStedfesting() {
		return stedfesting;
	}

	@Override
	public String getTextForGUI() {
		String dato = opprettetDato != null ? MipssDateFormatter.formatDate(opprettetDato,
				MipssDateFormatter.SHORT_DATE_TIME_FORMAT) : BLANK;
		String av = opprettetAv != null ? opprettetAv : BLANK;
		return dato + SPACE + BY + SPACE + av;
	}

	@Transient
	public Avvik getThisForReport() {
		return this;
	}

	/**
	 * Tilstand
	 * 
	 * @return
	 */
	public Avviktilstand getTilstand() {
		return tilstand;
	}


	/**
	 * Tilstand
	 * 
	 * @return
	 */
	public Long getAarsakId() {
		return aarsakId;
	}

	/**
	 * Tilstand
	 * 
	 * @return
	 */
	public Avvikaarsak getAarsak() {
		return aarsak;
	}

	/**
	 * Tilstand
	 * 
	 * @return
	 */
	public Long getTilstandId() {
		return tilstandId;
	}
	/**
	 * Dator
	 * 
	 * @return
	 */
	public Date getTiltaksDato() {
		return tiltaksDato;
	}

	/**
	 * Sikkerhet flagg
	 * 
	 * @return
	 */
	public Boolean getTrafikksikkerhetFlagg() {
        return objektAvvikType != null && "Trafikkfarlig".equals(objektAvvikType.getElrappId());
	}

	/**
	 * Returnerer hvor funnet befinner seg på vei
	 * 
	 * @return
	 */
	@Transient
	public Punktveiref getVeiref() {
		if (getStedfesting() != null) {
			return getStedfesting().getVeiref();
		} else {
			return null;
		}
	}

	/**
	 * Returnerer om funnet er lukket
	 * 
	 * @return
	 */
	@Transient
	public boolean isLukket() {
		String s = (getStatus() == null ? null : getStatus().getAvvikstatusType().getStatus());

		return StringUtils.equals(AvvikstatusType.LUKKET_STATUS, s);
	}

	public void listElementPropertyChanged(ObservableList list, int index) {
		// Ignorer
	}

	public void removeBilde(AvvikBilde pb) {
		Bilde oldBilde = getBilde();
		List<AvvikBilde> old = new ArrayList<AvvikBilde>(usortertebilder);
		List<AvvikBilde> oldSorterte = new ArrayList<AvvikBilde>(getSorterteBilder());
		AvvikBilde statusBilde = getStatusBilde();

		usortertebilder.remove(pb);

		firePropertyChange(FIELD_NAME_BILDE, oldBilde, getBilde());
		firePropertyChange(FIELD_NAME_BILDER, old, usortertebilder);
		firePropertyChange(FIELD_NAME_SORTERTE_BILDER, oldSorterte, getSorterteBilder());
		firePropertyChange(FIELD_NAME_STATUS_BILDE, statusBilde, getStatusBilde());
	}

	/**
	 * Fjerner en status
	 * 
	 * @param status
	 */
	public void removeStatus(AvvikStatus status) {
		log.trace("removeStatus({})", status);
		if (status == null) {
			throw new IllegalArgumentException("status cannot be null");
		}

		statuser.remove(status);
		firePropertyChange(FIELD_NAME_STATUSER, null, statuser);
		firePropertyChange(FIELD_NAME_STATUS, null, getStatus());
		firePropertyChange(FIELD_NAME_STATUSHISTORY, null, getStatusHistory());
		firePropertyChange(FIELD_NAME_BILDE, null, getBilde());
	}

	/**
	 * Beskrivelse
	 * 
	 * @param beskrivelse
	 */
	public void setBeskrivelse(String beskrivelse) {
		log.trace("setBeskrivelse({})", beskrivelse);
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;

		try {
			fireVetoableChange(FIELD_NAME_BESKRIVELSE, old, beskrivelse);
		} catch (PropertyVetoException e) {
			this.beskrivelse = old;
		}

		firePropertyChange(FIELD_NAME_BESKRIVELSE, old, beskrivelse);
	}

	/**
	 * Bilder, 
	 * 
	 * @param bilder
	 */
	public void setBilder(List<AvvikBilde> bilder) {
		log.trace("setBilder({})", bilder);
		
		List<AvvikBilde> old = this.usortertebilder;
		this.usortertebilder = bilder;
		firePropertyChange(FIELD_NAME_BILDER, old, bilder);
	}

	/**
	 * Etterslep
	 * 
	 * @param etterslepFlagg
	 */
	public void setEtterslepFlagg(Boolean etterslepFlagg) {
		log.trace("setEtterslepFlagg({})", etterslepFlagg);
		Boolean old = this.etterslep;
		this.etterslep = etterslepFlagg;

		firePropertyChange(FIELD_NAME_ETTERSLEPFLAGG, old, etterslepFlagg);
	}


	/**
	 * Id
	 * 
	 * @param guid
	 */
	public void setGuid(String guid) {
		log.trace("setGuid({})", guid);
		String old = this.guid;
		this.guid = guid;

		firePropertyChange(ID_NAME, old, guid);
	}

	/**
	 * Lukker funnet
	 * 
	 * @param lukketDato
	 */
	public void setLukketDato(Date lukketDato) {
		log.trace("setLukketDato({})", lukketDato);
		Date old = this.lukketDato;
		this.lukketDato = lukketDato;
		firePropertyChange(FIELD_NAME_LUKKETDATO, old, lukketDato);
	}

	/**
	 * Hvem opprettet avviket
	 * 
	 * @param opprettetAv
	 */
	public void setOpprettetAv(String opprettetAv) {
		log.trace("setOpprettetAv({})", opprettetDato);
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange(FIELD_NAME_OPPRETTETAV, old, opprettetAv);
	}

	/**
	 * Når ble avviket opprettet
	 * 
	 * @param opprettetDato
	 */
	public void setOpprettetDato(Date opprettetDato) {
		log.trace("setOpprettetDato({})", opprettetDato);
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange(FIELD_NAME_OPPRETTETDATO, old, opprettetDato);
	}
	/**
	 * Hvem endret avviket
	 * 
	 * @param endretAv
	 */
	public void setEndretAv(String endretAv) {
		log.trace("setEndretAv({})", endretAv);
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange(FIELD_NAME_ENDRETAV, old, endretAv);
	}

	/**
	 * Når ble avviket endret
	 * 
	 * @param endretDato
	 */
	public void setEndretDato(Date endretDato) {
		log.trace("setEndretDato({})", endretDato);
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange(FIELD_NAME_ENDRETDATO, old, endretDato);
	}

	/**
	 * Hovedprosessen dette funnet er knyttet til
	 * 
	 * @param prosess
	 */
	public void setProsess(Prosess prosess) {
		log.trace("setProsess({})", prosess);
		Prosess old = this.prosess;
		this.prosess = prosess;
		if (prosess!=null)
			setProsessId(prosess.getId());
		else
			setProsessId(null);
		firePropertyChange(FIELD_NAME_PROSESS, old, prosess);
		if (getProsessString() != null) {
			firePropertyChange("prosessString", null, getProsessString());
		}		
	}

	/**
	 * Hovedprosessen dette funnet er knyttet til
	 * 
	 * @param prosessId
	 */
	public void setProsessId(Long prosessId) {
		log.trace("setProsessId({})", prosessId);
		Long old = this.prosessId;
		this.prosessId = prosessId;
		firePropertyChange(FIELD_NAME_PROSESSID, old, prosessId);
	}

	public void setRefnummer(Long refnummer){
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}

	public void setSlettetAv(String slettetAv) {
		String oldValue = this.slettetAv;
		this.slettetAv = slettetAv;
		firePropertyChange(FIELD_NAME_SLETTETAV, oldValue, slettetAv);
	}

	public void setSlettetDato(Date slettetDato) {
		Date oldValue = this.slettetDato;
		this.slettetDato = slettetDato;
		firePropertyChange(FIELD_NAME_SLETTETDATO, oldValue, slettetDato);
	}

	/**
	 * Statuser
	 * 
	 * @param statuser
	 */
	public void setStatuser(List<AvvikStatus> statuser) {
		log.trace("setStatuser({})", statuser);
		List<AvvikStatus> old = this.statuser;
		this.statuser = statuser;
		firePropertyChange(FIELD_NAME_STATUSER, old, statuser);
		firePropertyChange(FIELD_NAME_STATUS, null, getStatus());
		firePropertyChange(FIELD_NAME_STATUSHISTORY, null, getStatusHistory());
	}

	/**
	 * Stedfesting av hendelsen
	 * 
	 * @param stedfesting
	 */
	public void setStedfesting(Punktstedfest stedfesting) {
		log.trace("setStedfesting({})", stedfesting);
		this.stedfesting = stedfesting;
		if (stedfesting != null) {
			stedfesting.setAvvik(this);
		}
		firePropertyChange(FIELD_NAME_STEDFESTING, null, stedfesting);
		firePropertyChange(FIELD_NAME_VEIREF, null, (stedfesting == null ? null
				: stedfesting.getVeiref()));
	}

	public void setStedfestingForRapport(Punktstedfest stedfesting){
		this.stedfesting=stedfesting;
	}
	/**
	 * Tilstand
	 * 
	 * @param tilstand
	 */
	public void setTilstand(Avviktilstand tilstand) {
		log.trace("setTilstand({})", tilstand);
		Avviktilstand old = this.tilstand;
		this.tilstand = tilstand;

		firePropertyChange(FIELD_NAME_TILSTAND, old, tilstand);

		if (tilstand != null) {
			setTilstandId(tilstand.getId());
		} else {
			setTilstandId(null);
		}
	}

	/**
	 * Tilstand
	 * 
	 * @param tilstandId
	 */
	public void setTilstandId(Long tilstandId) {
		log.trace("setTilstandId({})", tilstandId);
		Long old = this.tilstandId;
		this.tilstandId = tilstandId;
		firePropertyChange(FIELD_NAME_TILSTANDID, old, tilstandId);
	}

	/**
	 * Tilstand
	 * 
	 * @param aarsak
	 */
	public void setAarsak(Avvikaarsak aarsak) {
		log.trace("setAarsak({})", aarsak);
		Avvikaarsak old = this.aarsak;
		this.aarsak = aarsak;

		firePropertyChange("aarsak", old, aarsak);

		if (aarsak != null) {
			setAarsakId(aarsak.getId());
		} else {
			setAarsakId(null);
		}
	}

	/**
	 * Tilstand
	 * 
	 * @param aarsakId
	 */
	public void setAarsakId(Long aarsakId) {
		log.trace("setAarsakId({})", aarsakId);
		Long old = this.aarsakId;
		this.aarsakId = aarsakId;
		firePropertyChange("aarsakId", old, aarsakId);
	}
	/**
	 * Dato
	 * 
	 * @param tiltaksDato
	 */
	public void setTiltaksDato(Date tiltaksDato) {
		log.trace("setTiltaksDato({})", tiltaksDato);
		Date old = this.tiltaksDato;
		this.tiltaksDato = tiltaksDato;
		firePropertyChange(FIELD_NAME_TILTAKSDATO, old, tiltaksDato);
	}
	
	public void setVeiref(Punktveiref vei) {
		if(stedfesting != null) {
			Punktveiref old = getVeiref();
			stedfesting.addPunktveiref(vei);
			firePropertyChange(FIELD_NAME_VEIREF, old, getVeiref());
		}
	}
	public Boolean getTilleggsarbeid() {
        return !(arbeidType != null && "Funksjonsansvar".equals(arbeidType.getElrappId()));
	}

	public void setEstimat(String estimat) {
		String old = this.estimat;
		this.estimat = estimat;
		firePropertyChange("estimat", old, estimat);
	}
	
	public String getEstimat() {
		return estimat;
	}

    public String getMerknad() {
        return merknad;
    }

    public void setMerknad(String merknad) {
		String old = this.merknad;
        this.merknad = merknad;
		firePropertyChange("merknad", old, merknad);
    }

    /**
	 * @see java.lang.Object#toString()
	 * @see org.apache.commons.lang.builder.ToStringBuilder
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(ID_NAME, guid)
				.append(FIELD_NAME_TILSTANDID, tilstandId)
				.append(FIELD_NAME_BESKRIVELSE, beskrivelse)
				.append(FIELD_NAME_ETTERSLEPFLAGG, etterslep)
				.append(FIELD_NAME_TILTAKSDATO, tiltaksDato)
				.append("refnummer", refnummer)
				.toString();
	}

	/**
	 * Setter beskrivelsen
	 * 
	 * @param s
	 * @return
	 */
	public Avvik withBeskrivelse(String s) {
		setBeskrivelse(s);
		return this;
	}

	/**
	 * Setter om det er etterslep eller ikke
	 * 
	 * @param f
	 * @return
	 */
	public Avvik withEtterslep(boolean f) {
		setEtterslepFlagg(f);
		return this;
	}

	/**
	 * Setter id
	 * 
	 * @param g
	 * @return
	 */
	public Avvik withGuid(String g) {
		setGuid(g);
		return this;
	}

	/**
	 * Setter hvem som opprettet funnet
	 * 
	 * @param a
	 * @return
	 */
	public Avvik withOpprettetAv(String a) {
		setOpprettetAv(a);
		return this;
	}

	/**
	 * Setter n�r funnet ble gjort
	 * 
	 * @param d
	 * @return
	 */
	public Avvik withOpprettetDato(Date d) {
		setOpprettetDato(d);
		return this;
	}

	/**
	 * Setter stedfesting
	 * 
	 * @param s
	 * @return
	 */
	public Avvik withPunktstedfest(Punktstedfest s) {
		setStedfesting(s);
		return this;
	}

	/**
	 * Setter tilstanden
	 * 
	 * @param t
	 * @return
	 */
	public Avvik withTilstand(Avviktilstand t) {
		setTilstand(t);
		return this;
	}
}
