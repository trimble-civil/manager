package no.mesta.mipss.persistence.kontrakt;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.areal.Areal;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.mipssfield.Favorittliste;
import no.mesta.mipss.persistence.mipssfield.KontraktProsess;
import no.mesta.mipss.persistence.mipssfield.PdabrukerKontrakt;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.persistence.veinett.Veinett;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Generert i JDeveloper. Tilpasset MIPPS prosjektkrav.
 * 
 * @author jorge
 * 
 */
@SuppressWarnings("serial")
@Entity(name = Driftkontrakt.BEAN_NAME)
@SequenceGenerator(name = "driftkontraktIdSeq", sequenceName = "DRIFTKONTRAKT_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries( {
		@NamedQuery(name = Driftkontrakt.QUERY_FIND_ALL, query = "select o from Driftkontrakt o order by o.kontraktnummer"),
		@NamedQuery(name = Driftkontrakt.QUERY_FIND_VALID, query = "select o from Driftkontrakt o where o.gyldigTilDato is null or o.gyldigTilDato <= CURRENT_DATE order by o.kontraktnavn"),
		@NamedQuery(name = Driftkontrakt.QUERY_FIND_GYLDIGE_ELRAPP, query = "select o from Driftkontrakt o where o.typeNavn='Funksjonskontrakt' and o.kundeNr=1001 and o.gyldigTilDato > CURRENT_DATE order by o.kontraktnummer"),
		@NamedQuery(name = Driftkontrakt.QUERY_FIND_ALL_BY_NAME, query = "select o from Driftkontrakt o order by o.kontraktnavn"),
		@NamedQuery(name = Driftkontrakt.QUERY_FIND_FOR_ADMIN, query = "select distinct o from Driftkontrakt o " +
																		"LEFT JOIN FETCH o.kontraktloggList " +
																		"LEFT JOIN FETCH o.kontraktStabList " +
																		"LEFT JOIN FETCH o.kontraktLeverandorList " +
																		"LEFT JOIN FETCH o.kontraktKontaktypeList " +
																		"LEFT JOIN FETCH o.dfuKontraktList " +
																		"LEFT JOIN FETCH o.favorittlisteList " +
																		"where o.id=:id",
																	hints={@QueryHint(name=QueryHints.REFRESH, value=HintValues.TRUE)})
})
@Table(name = "DRIFTKONTRAKT")
public class Driftkontrakt implements Serializable, IRenderableMipssEntity, Comparable<Driftkontrakt> {
	public static final String BEAN_NAME = "Driftkontrakt";
	public static final String QUERY_FIND_ALL = "Driftkontrakt.findAll";
	public static final String QUERY_FIND_VALID = "Driftkontrakt.findValid";
	public static final String QUERY_FIND_ALL_BY_NAME = "Driftkontrakt.findAllByName";
	public static final String QUERY_FIND_FOR_ADMIN = "Driftkontrakt.findForAdmin";
	public static final String QUERY_FIND_GYLDIGE_ELRAPP = "Driftkontrakt.findGyldigeElrapp";
	

	private static final String BYGGELEDER_NAVN = "Byggeleder";
	private static final String DRIFTSLEDER_NAVN = "Driftsleder";

	private String ansvarssted;
	private Long arealId;
	private String beskrivelse;
	private String endretAv;
	private Date endretDato;
	private Long epostRodetypeId;
	private Date gyldigFraDato;
	private Date gyldigTilDato;
	private String epostEkstraOvertekstHtml;
	private String epostEkstraUndertekstHtml;
	private Long id;
	private String kontraktnavn;
	private String kontraktnummer;
	private String opprettetAv;
	private Date opprettetDato;
	private String typeNavn;
	private Long veinettId;
	private String kundeNr;
	private Long distriktId;
	private Kontrakttype kontrakttype;
	private Kunderegion kunderegion;
	private Long prosessettId;
	private Driftdistrikt driftdistrikt;
	private List<Kontraktlogg> kontraktloggList = new ArrayList<Kontraktlogg>();

	// lagt til for hånd
	private List<KontraktProsjekt> prosjektKontraktList = new ArrayList<KontraktProsjekt>();
	private List<KontraktStab> kontraktStabList = new ArrayList<KontraktStab>();
	private List<PdabrukerKontrakt> pdabrukerList = new ArrayList<PdabrukerKontrakt>();
	private List<KontraktLeverandor> kontraktLeverandorList = new ArrayList<KontraktLeverandor>();
	private List<KontraktKontakttype> kontraktKontaktypeList = new ArrayList<KontraktKontakttype>();
	private List<DfuKontrakt> dfuKontraktList = new ArrayList<DfuKontrakt>();

	private List<Favorittliste> favorittlisteList = new ArrayList<Favorittliste>();
	private List<KontraktProsess> kontraktProsessList = new ArrayList<KontraktProsess>();
	
	private Veinett veinett;
	private Areal areal;
	private Rodetype rodetype;

	private String elrappKontraktIdent;
	private Date elrappStartDato;
	private Date elrappSluttDato;
	
	private String kontraktpassord;
	private String kontraktpassordHashed;
	private transient PropertyChangeSupport props = new PropertyChangeSupport(this);

	public Driftkontrakt() {
	}
	@Column(name="ELRAPP_KONTRAKT_IDENT")
	public String getElrappKontraktIdent(){
		return elrappKontraktIdent;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ELRAPP_START_DATO")
	public Date getElrappStartDato(){
		return elrappStartDato;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ELRAPP_SLUTT_DATO")
	public Date getElrappSluttDato(){
		return elrappSluttDato;
	}
	
	public void setElrappKontraktIdent(String elrappKontraktIdent){
		this.elrappKontraktIdent = elrappKontraktIdent;
	}
	public void setElrappStartDato(Date elrappStartDato){
		this.elrappStartDato = elrappStartDato;
	}
	public void setElrappSluttDato(Date elrappSluttDato){
		this.elrappSluttDato = elrappSluttDato;
	}
	
	public String getAnsvarssted() {
		return ansvarssted;
	}

	public void setAnsvarssted(String ansvarssted) {
		Object oldValue = this.ansvarssted;
		this.ansvarssted = ansvarssted;
		getProps().firePropertyChange("ansvarssted", oldValue, ansvarssted);
	}

	@Column(name = "AREAL_ID", nullable = true, insertable = false, updatable = false)
	public Long getArealId() {
		return arealId;
	}

	public void setArealId(Long arealId) {
		this.arealId = arealId;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		Object oldValue = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		getProps().firePropertyChange("beskrivelse", oldValue, beskrivelse);
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}
	@Column(name = "PROSESSETT_ID")
	public Long getProsessettId(){
		return prosessettId;
	}
	public void setProsessettId(Long prosessettId){
		this.prosessettId = prosessettId;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	@Column(name = "EPOST_RODETYPE_ID", nullable = true, insertable = false, updatable = false)
	public Long getEpostRodetypeId() {
		return epostRodetypeId;
	}

	public void setEpostRodetypeId(Long epostRodetypeId) {
		this.epostRodetypeId = epostRodetypeId;
	}

	@Column(name = "GYLDIG_FRA_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getGyldigFraDato() {
		return gyldigFraDato;
	}

	public void setGyldigFraDato(Date gyldigFraDato) {
		Object oldValue = this.gyldigFraDato;
		this.gyldigFraDato = gyldigFraDato;
		getProps().firePropertyChange("gyldigFraDato", oldValue, gyldigFraDato);
	}

	@Column(name = "GYLDIG_TIL_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getGyldigTilDato() {
		return gyldigTilDato;
	}

	public void setGyldigTilDato(Date gyldigTilDato) {
		Object oldValue = this.gyldigTilDato;
		this.gyldigTilDato = gyldigTilDato;
		getProps().firePropertyChange("gyldigTilDato", oldValue, gyldigTilDato);
	}

	@Column(name = "EPOST_EKSTRA_OVERTEKST_HTML")
	public String getEpostEkstraOvertekstHtml() {
		return epostEkstraOvertekstHtml;
	}

	public void setEpostEkstraOvertekstHtml(String epostEkstraOvertekstHtml) {
		Object oldValue = this.epostEkstraOvertekstHtml;
		this.epostEkstraOvertekstHtml = epostEkstraOvertekstHtml;
		getProps().firePropertyChange("epostEkstraOvertekstHtml", oldValue,
				epostEkstraOvertekstHtml);
	}

	@Column(name = "EPOST_EKSTRA_UNDERTEKST_HTML")
	public String getEpostEkstraUndertekstHtml() {
		return epostEkstraUndertekstHtml;
	}

	public void setEpostEkstraUndertekstHtml(String epostEkstraUndertekstHtml) {
		Object oldValue = this.epostEkstraUndertekstHtml;
		this.epostEkstraUndertekstHtml = epostEkstraUndertekstHtml;
		getProps().firePropertyChange("epostEkstraUndertekstHtml", oldValue,
				epostEkstraUndertekstHtml);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "driftkontraktIdSeq")
	@Column(nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKontraktnavn() {
		return kontraktnavn;
	}

	public void setKontraktnavn(String kontraktnavn) {
		Object oldValue = this.kontraktnavn;
		this.kontraktnavn = kontraktnavn;
		getProps().firePropertyChange("kontraktnavn", oldValue, kontraktnavn);
	}

	public String getKontraktnummer() {
		return kontraktnummer;
	}

	public void setKontraktnummer(String kontraktnummer) {
		Object oldValue = this.kontraktnummer;
		this.kontraktnummer = kontraktnummer;
		getProps().firePropertyChange("kontraktnummer", oldValue,
				kontraktnummer);
	}

	@Column(name = "OPPRETTET_AV", nullable = false)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@Column(name = "TYPE_NAVN", nullable = true, insertable = false, updatable = false)
	public String getTypeNavn() {
		return typeNavn;
	}

	public void setTypeNavn(String typeNavn) {
		this.typeNavn = typeNavn;
	}

	@Column(name = "VEINETT_ID", nullable = true, insertable = false, updatable = false)
	public Long getVeinettId() {
		return veinettId;
	}

	public void setVeinettId(Long veinettId) {
		this.veinettId = veinettId;
	}

	@Column(name = "KUNDE_NR")
	public String getKundeNr() {
		return kundeNr;
	}

	public void setKundeNr(String kundeNr) {
		Object oldValue = this.kundeNr;
		this.kundeNr = kundeNr;
		getProps().firePropertyChange("kundeNr", oldValue, kundeNr);
	}

	@Column(name = "DISTRIKT_ID", nullable = true, insertable = false, updatable = false)
	public Long getDistriktId() {
		return distriktId;
	}

	public void setDistriktId(Long distriktId) {
		this.distriktId = distriktId;
	}

	@ManyToOne
	@JoinColumn(name = "DISTRIKT_ID", referencedColumnName = "ID", nullable = true)
	public Driftdistrikt getDriftdistrikt() {
		return driftdistrikt;
	}

	public void setDriftdistrikt(Driftdistrikt driftdistrikt) {
		Driftdistrikt old = this.driftdistrikt;
		this.driftdistrikt = driftdistrikt;

		getProps().firePropertyChange("driftdistrikt", old, driftdistrikt);

		if (driftdistrikt != null) {
			setDistriktId(driftdistrikt.getId());
		} else {
			setDistriktId(null);
		}
	}

	@OneToMany(mappedBy = "driftkontrakt")
	public List<Kontraktlogg> getKontraktloggList() {
		return kontraktloggList;
	}

	public void setKontraktloggList(List<Kontraktlogg> kontraktloggList) {
		Object oldValue = this.kontraktloggList;
		this.kontraktloggList = kontraktloggList;
		props
				.firePropertyChange("kontraktloggList", oldValue,
						kontraktloggList);
	}

	public Kontraktlogg addKontraktlogg(Kontraktlogg kontraktlogg) {
		getKontraktloggList().add(kontraktlogg);
		kontraktlogg.setDriftkontrakt(this);
		return kontraktlogg;
	}

	public Kontraktlogg removeKontraktlogg(Kontraktlogg kontraktlogg) {
		getKontraktloggList().remove(kontraktlogg);
		kontraktlogg.setDriftkontrakt(null);
		return kontraktlogg;
	}

	@Transient
	public KontraktKontakttype getByggeleder() {
		for (KontraktKontakttype k : getKontraktKontaktypeList()) {
			if (k.getKontakttypeNavn().equals(BYGGELEDER_NAVN)) {
				return k;
			}
		}
		return null;
	}

	@Transient
	public KontraktKontakttype getDriftsleder() {
		for (KontraktKontakttype k : getKontraktKontaktypeList()) {
			if (k.getKontakttypeNavn().equals(DRIFTSLEDER_NAVN)) {
				return k;
			}
		}
		return null;
	}
	
	// de neste metodene er håndkodet
	/**
	 * Sammenlikner Id-nøkkelen med o's Id-nøkkel.
	 * 
	 * @param o
	 * @return true hvis nøklene er like.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof Driftkontrakt)) {
			return false;
		}
		Driftkontrakt other = (Driftkontrakt) o;
		return new EqualsBuilder().append(getId(), other.getId()).isEquals();
	}

	/**
	 * Genererer en hashkode basert på Id og kontraktsnummer.
	 * 
	 * @return hashkoden.
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(37, 79).append(getId()).append(
				getKontraktnummer()).toHashCode();
	}

	/** {@inheritDoc} */
	@Transient
	public String getTextForGUI() {
		return StringUtils.leftPad(getKontraktnummer(), 4, "0") + " - "
				+ getKontraktnavn();
	}

	/**
	 * Henter alle prosjekter hvor denne kontrakten h�rer til.
	 * 
	 * @return prosjektList
	 */
	@OneToMany(mappedBy = "driftkontrakt", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<KontraktProsjekt> getProsjektKontraktList() {
		return prosjektKontraktList;
	}

	/**
	 * @param prosjektList
	 *            the prosjektList to set
	 */
	public void setProsjektKontraktList(List<KontraktProsjekt> prosjektList) {
		this.prosjektKontraktList = prosjektList;
	}

	/**
	 * Kontrakttype
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "TYPE_NAVN", referencedColumnName = "NAVN", nullable = true)
	public Kontrakttype getKontrakttype() {
		return kontrakttype;
	}

	
	/**
	 * Kontrakttype
	 * 
	 * @param kontrakttype
	 */
	public void setKontrakttype(Kontrakttype kontrakttype) {
		Kontrakttype old = this.kontrakttype;
		this.kontrakttype = kontrakttype;

		getProps().firePropertyChange("kontrakttype", old, kontrakttype);

		if (kontrakttype != null) {
			setTypeNavn(kontrakttype.getNavn());
		} else {
			setTypeNavn(null);
		}
	}
	@ManyToOne
	@JoinColumn(name = "KUNDEREGION_NAVN", referencedColumnName = "NAVN", nullable = true)
	public Kunderegion getKunderegion() {
		return kunderegion;
	}
	
	public void setKunderegion(Kunderegion kunderegion) {
		Kunderegion old = this.kunderegion;
		this.kunderegion = kunderegion;

		getProps().firePropertyChange("kunderegion", old, kunderegion);
	}
	/**
	 * Veinett
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "VEINETT_ID", referencedColumnName = "ID")
	public Veinett getVeinett() {
		return veinett;
	}

	/**
	 * Veinett
	 * 
	 * @param veinett
	 */
	public void setVeinett(Veinett veinett) {
		Veinett old = this.veinett;
		this.veinett = veinett;

		getProps().firePropertyChange("veinett", old, veinett);

		if (veinett != null) {
			setVeinettId(veinett.getId());
		} else {
			setVeinettId(null);
		}
	}

	/**
	 * Areal
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "AREAL_ID", referencedColumnName = "ID")
	public Areal getAreal() {
		return areal;
	}

	/**
	 * Areal
	 * 
	 * @param areal
	 */
	public void setAreal(Areal areal) {
		Areal old = this.areal;
		this.areal = areal;

		getProps().firePropertyChange("areal", old, areal);

		if (areal != null) {
			setArealId(areal.getId());
		} else {
			setArealId(null);
		}
	}

	@ManyToOne
	@JoinColumn(name = "EPOST_RODETYPE_ID", referencedColumnName = "ID")
	public Rodetype getRodetype() {
		return rodetype;
	}

	public void setRodetype(Rodetype rodetype) {
		Rodetype old = this.rodetype;
		this.rodetype = rodetype;

		getProps().firePropertyChange("rodetype", old, rodetype);

		if (rodetype != null) {
			setEpostRodetypeId(rodetype.getId());
		} else {
			setEpostRodetypeId(null);
		}
	}

	@OneToMany(mappedBy = "driftkontrakt", cascade = CascadeType.ALL)
	public List<KontraktStab> getKontraktStabList() {
		return kontraktStabList;
	}

	public void setKontraktStabList(List<KontraktStab> kontraktStabList) {
		this.kontraktStabList = kontraktStabList;
	}

	@OneToMany(mappedBy = "driftkontrakt", cascade = CascadeType.ALL)
	public List<PdabrukerKontrakt> getPdabrukerList() {
		return pdabrukerList;
	}

	public void setPdabrukerList(List<PdabrukerKontrakt> pdabrukerList) {
		this.pdabrukerList = pdabrukerList;
	}
	@OneToMany(mappedBy = "driftkontrakt", cascade = {CascadeType.ALL})
	public List<KontraktLeverandor> getKontraktLeverandorList() {
		return kontraktLeverandorList;
	}

	public void setKontraktLeverandorList(
			List<KontraktLeverandor> kontraktLeverandorList) {
		this.kontraktLeverandorList = kontraktLeverandorList;
	}

	@OneToMany(mappedBy = "driftkontrakt", cascade = CascadeType.ALL)
	public List<KontraktKontakttype> getKontraktKontaktypeList() {
		return kontraktKontaktypeList;
	}

	public void setKontraktKontaktypeList(
			List<KontraktKontakttype> kontraktKontaktypeList) {
		this.kontraktKontaktypeList = kontraktKontaktypeList;
	}

	@OneToMany(mappedBy = "driftkontrakt", cascade = CascadeType.ALL)
	public List<DfuKontrakt> getDfuKontraktList() {
		return dfuKontraktList;
	}

	public void setDfuKontraktList(List<DfuKontrakt> dfuKontraktList) {
		this.dfuKontraktList = dfuKontraktList;
	}

	@OneToMany(mappedBy = "driftkontrakt", cascade = CascadeType.ALL)
	public List<Favorittliste> getFavorittlisteList() {
		return favorittlisteList;
	}

	public void setFavorittlisteList(List<Favorittliste> favorittlisteList) {
		this.favorittlisteList = favorittlisteList;
	}
	
	@OneToMany(mappedBy = "driftkontrakt", cascade = CascadeType.ALL)
	public List<KontraktProsess> getKontraktProsessList() {
		return kontraktProsessList;
	}

	public void setKontraktProsessList(List<KontraktProsess> kontraktProsessList) {
		this.kontraktProsessList = kontraktProsessList;
	}
	
	/**
	 * @return the kontraktpassord
	 */
	public String getKontraktpassord() {
		return kontraktpassord;
	}
	/**
	 * @param kontraktpassord the kontraktpassord to set
	 */
	public void setKontraktpassord(String kontraktpassord) {
		String old = this.kontraktpassord;
		this.kontraktpassord = kontraktpassord;
		getProps().firePropertyChange("kontraktpassord", old, kontraktpassord);
	}
	/**
	 * @return the kontraktpassordHashed
	 */
	@Column(name="KONTRAKTPASSORD_HASHED")
	public String getKontraktpassordHashed() {
		return kontraktpassordHashed;
	}
	/**
	 * @param kontraktpassordHashed the kontraktpassordHashed to set
	 */
	public void setKontraktpassordHashed(String kontraktpassordHashed) {
		String old = this.kontraktpassordHashed;
		this.kontraktpassordHashed = kontraktpassordHashed;
		getProps().firePropertyChange("kontraktpassordHashed", old, kontraktpassordHashed);
	}
	
	@Transient
	public KontraktProsjekt getStandardKontraktProsjekt() {
		for(KontraktProsjekt kontraktProsjekt : getProsjektKontraktList()) {
			if(kontraktProsjekt.getStandardFlaggBoolean()) {
				return kontraktProsjekt;
			}
		}
		return null;
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	/** [@inheritDoc} */
	public int compareTo(Driftkontrakt other) {
		return new CompareToBuilder().append(getTextForGUI(),
				other.getTextForGUI()).toComparison();
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName,
			PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName,
			PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}
}
