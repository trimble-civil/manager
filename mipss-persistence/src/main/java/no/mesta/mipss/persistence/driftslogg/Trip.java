package no.mesta.mipss.persistence.driftslogg;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import javax.persistence.*;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@NamedQueries({
        @NamedQuery(name = Trip.FIND_BY_TRIP_ID, query = "SELECT o FROM Trip o WHERE o.id=:tripId"),
        @NamedQuery(name= Trip.FIND_BY_VEHICLES_DATE, query = "SELECT o from Trip o where o.vehicle.id in :vehicles and o.startTime < :endTime and o.endTime > :startTime")
})
@SequenceGenerator(name = Trip.ID_SEQ, sequenceName = "TRIP_ID_SEQ", initialValue = 1, allocationSize = 1)

@SuppressWarnings("serial")
@Entity
@Table(name="TRIP")
public class Trip extends MipssEntityBean<MipssEntityBean> implements IRenderableMipssEntity, Warning {

    public static final String FIND_BY_TRIP_ID = "Trip.findByTripId";
    public static final String FIND_BY_VEHICLES_DATE = "Trip.findByVehicleDate";
    public static final String ID_SEQ = "tripIdSeq";

    public enum Platform {

        WEBAPP("finalized"),
        WEB("manual"),
        UNKNOWN("unknown");

        private String identifier;

        Platform(String identifier) {
            this.identifier = identifier;
        }

        public String getIdentifier() {
            return identifier;
        }

        public static Platform getPlatformFromString(String identifier) {
            for (Platform p : Platform.values()) {
                if (p.getIdentifier().equalsIgnoreCase(identifier)) {
                    return p;
                }
            }
            return UNKNOWN;
        }

    }

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Trip.ID_SEQ)
    private Long id;

    @OneToOne(orphanRemoval = true, cascade = ALL, fetch = EAGER)
    @JoinColumn(name = "PERSON_ID")
    private Person person;

    @Column(name = "VENDOR_ID")
    private String vendorId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_TIME")
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_TIME")
    private Date endTime;

    @Column(name = "DISTANCE")
    private Double distance;

    @Column(name = "WEATHER_TYPE_NAME")
    private String weatherTypeName;

    @Column(name = "WEATHER_TEMP_NAME")
    private String weatherTempName;

    @Column(name = "TRIP_COMMENT")
    private String tripComment;

    @Column(name = "USED_GRIT")
    private boolean usedGrit;

    @Column(name = "GRIT_SALT")
    private Double gritSalt;

    @Column(name = "GRIT_SALT_UNIT")
    private String gritSaltUnit;

    @Column(name = "GRIT_SAND")
    private Double gritSand;

    @Column(name = "GRIT_SAND_UNIT")
    private String gritSandUnit;

    @Column(name = "GRIT_SOLUTION")
    private Double gritSolution;

    @Column(name = "GRIT_SOLUTION_UNIT")
    private String gritSolutionUnit;

    @Transient
    private Double amount;

    @Transient
    private Double hoursTotal;

    @Transient
    private Double plowingTotalKm; // both plowing and regular planing

    @Transient
    private Double planingTotalKm;

    @Transient
    private Double heavyPlaningTotalKm;

    @Transient
    private Double sidePloughTotalKm;

    @Transient
    private Double dryGrittingTotalKg;

    @Transient
    private Double wetGrittingTotalLiter;

    @Transient
    private boolean isArticleChanged;

    @Transient
    private boolean isProductionOutsideRode;

    @Transient
    private boolean isProductionOutsideContract;

    @Transient
    private boolean hasNoPayment;

    @Transient
    private boolean manuallyCreated;

    @Column(name = "TRIPPROCESSSTATUS")
    private String processStatusId;

    @Transient
    private boolean ignored;

    @OneToMany(orphanRemoval = true, cascade = ALL, fetch = EAGER)
    @JoinColumn(name = "TRIP_ID")
    private List<Aktivitet> activities;

    @ManyToMany(cascade = CascadeType.ALL, fetch = EAGER)
    @JoinTable(
            name = "TRIP_WORK",
            joinColumns = @JoinColumn(
                    name = "TRIP_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(
                    name = "WORK_TYPE_ID", referencedColumnName = "ID"))
    private List<WorkType> work;

    @OneToOne(orphanRemoval = true, cascade = ALL, fetch = EAGER)
    @JoinColumn(name = "LEVKONTRAKT_IDENT")
    private Levkontrakt contractorContract;

    @ManyToOne
    @JoinColumn(name = "VEHICLE_ID")
    private Kjoretoy vehicle;

    @ManyToOne(cascade = ALL, fetch = EAGER)
    @JoinColumn(name = "PERIODE_ID")
    private Periode period;

    @Column(name = "STATUS")
    private String tripStatus;

    @ManyToOne(cascade = ALL, fetch = EAGER)
    @JoinColumn(name = "WORK_GRIT_PRODUCT_ID")
    private TripGritProduct workGritProduct;

    @Column(name = "WORK_GRIT_DOSAGE")
    private Double workGritDosage;

    public Trip() {
        activities = new ArrayList<>();
    }

    @OneToMany(orphanRemoval = true, cascade = ALL, fetch = EAGER)
    @JoinColumn(name = "TRIP_ID")
    private List<TripReturnCause> returnCauses;

    public Long getId() {
        return id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public Long getVehicleId() {
        return vehicle.getId();
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public String getWeatherTypeName() {
        return weatherTypeName;
    }

    public String getWeatherTempName() {
        return weatherTempName;
    }

    public String getTripComment() {
        return tripComment;
    }

    @Override
    public String getTextForGUI() {
        return "TODO: implement me";
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setStartTime(Date d) {
        this.startTime = d;
    }

    public void setEndTime(Date d) {
        this.endTime = d;
    }

    public Levkontrakt getContractorContract() {
        return contractorContract;
    }

    public Kjoretoy getVehicle() {
        return vehicle;
    }

    public Double getAmount() {
        return amount;
    }

    public void addAmount(Double amount) {
        this.amount = add(this.amount, amount);
    }

    public Double getHoursTotal() {
        return hoursTotal;
    }

    public void addHours(Double hours) {
        this.hoursTotal = add(this.hoursTotal, hours);
    }

    public Double getPlowingTotalKm() {
        return plowingTotalKm;
    }

    public void addPlowingKm(Double km) {
        this.plowingTotalKm = add(this.plowingTotalKm, km);
    }

    public Double getPlaningTotalKm() {
        return planingTotalKm;
    }

    public void addPlaningKm(Double km) {
        this.planingTotalKm = add(this.planingTotalKm, km);
    }

    public Double getHeavyPlaningTotalKm() {
        return heavyPlaningTotalKm;
    }

    public void addHeavyPlaningKm(Double km) {
        this.heavyPlaningTotalKm = add(this.heavyPlaningTotalKm, km);
    }

    public Double getSidePloughTotalKm() {
        return sidePloughTotalKm;
    }

    public void addSidePloughKm(Double km) {
        this.sidePloughTotalKm = add(this.sidePloughTotalKm, km);
    }

    public Double getDryGrittingTotalKg() {
        return dryGrittingTotalKg;
    }

    public void addDryGrittingKg(Double kg) {
        this.dryGrittingTotalKg = add(this.dryGrittingTotalKg, kg);
    }

    public Double getWetGrittingTotalLiter() {
        return wetGrittingTotalLiter;
    }

    public void addWetGrittingLiter(Double liter) {
        this.wetGrittingTotalLiter = add(this.wetGrittingTotalLiter, liter);
    }

    @Override
    public boolean isArticleChanged() {
        return isArticleChanged;
    }

    public void setArticleChanged(boolean isArticleChanged) {
        this.isArticleChanged = isArticleChanged;
    }

    @Override
    public boolean isProductionOutsideRode() {
        return isProductionOutsideRode;
    }

    public void setProductionOutsideRode(boolean isProductionOutsideRode) {
        this.isProductionOutsideRode = isProductionOutsideRode;
    }

    @Override
    public boolean isProductionOutsideContract() {
        return isProductionOutsideContract;
    }

    public void setProductionOutsideContract(boolean isProductionOutsideContract) {
        this.isProductionOutsideContract = isProductionOutsideContract;
    }

    @Override
    public boolean hasNoPayment() {
        return hasNoPayment;
    }

    public void setHasNoPayment(boolean hasNoPayment) {
        this.hasNoPayment = hasNoPayment;
    }

    @Override
    public boolean isManuallyCreated() {
        return manuallyCreated;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    @Override
    public boolean isIgnored() {
        return ignored;
    }

    public void setManuallyCreated(boolean manuallyCreated) {
        this.manuallyCreated = manuallyCreated;
    }

    public TripProcessStatus getProcessStatus() {
        return TripProcessStatus.fromId(processStatusId);
    }

    public void setProcessStatusId(String processStatusId) {
        this.processStatusId = processStatusId;
    }

    public List<Aktivitet> getActivities() {
        return activities;
    }

    private Double add(Double field, Double amount) {
        if (field != null && amount != null) {
            return field + amount;
        } else if (field == null && amount != null ) {
            return amount;
        } else if (field != null){
            return field;
        } else {
            return null;
        }
    }

    public void setHoursTotal(Double hoursTotal) {
        this.hoursTotal = hoursTotal;
    }

    public Periode getPeriod() {
        return period;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public Double getDuration() {
        if (getStartTime() != null && getEndTime() != null) {
            return (double) ChronoUnit.MINUTES.between(getStartTime().toInstant(), getEndTime().toInstant()) / 60;
        }
        return null;
    }

    public Platform getPlatform() {
        return Platform.getPlatformFromString(getTripStatus());
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public List<WorkType> getWork() {
        return work;
    }

    public TripGritProduct getWorkGritProduct() {
        return workGritProduct;
    }

    public Double getWorkGritDosage() {
        return workGritDosage;
    }

    public boolean hasUsedGrit() {
        return usedGrit;
    }

    public Double getGritSalt() {
        return gritSalt;
    }

    public String getGritSaltUnit() {
        return gritSaltUnit;
    }

    public Double getGritSand() {
        return gritSand;
    }

    public String getGritSandUnit() {
        return gritSandUnit;
    }

    public Double getGritSolution() {
        return gritSolution;
    }

    public String getGritSolutionUnit() {
        return gritSolutionUnit;
    }

    public Person getPerson() {
        return person;
    }

    public void setPeriod(Periode period) {
        this.period = period;
    }

    public void setActivities(List<Aktivitet> activities) {
        this.activities = activities;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<TripReturnCause> getReturnCauses() {
        return returnCauses;
    }

    public void setVehicle(Kjoretoy vehicle) {
        this.vehicle = vehicle;
    }

}