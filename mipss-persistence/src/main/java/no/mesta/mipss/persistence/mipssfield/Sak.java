package no.mesta.mipss.persistence.mipssfield;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.AdditionalCriteria;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;

@SuppressWarnings("serial")
@Entity
@Table(name="SAK_XV")
@NamedQueries( { 
	@NamedQuery(name = Sak.QUERY_HENT_MED_REFNUMMER, query = "SELECT s FROM Sak s where s.refnummer=:refnummer")})
@NamedNativeQueries({
	@NamedNativeQuery(name=Sak.QUERY_OPPDATER_INTERNNOTAT, query = "UPDATE sak_xv s set s.internnotat=? where s.guid =?")})
	
@SequenceGenerator(name = Sak.REFNUMMER_SEQ, sequenceName = "SAK_REFNUMMER_SEQ", initialValue = 1, allocationSize = 1)
public class Sak extends MipssEntityBean<Sak> implements IRenderableMipssEntity, MipssFieldDetailItem{
	
	public static final String REFNUMMER_SEQ = "sakRefnummerSeq";
	public static final String QUERY_HENT_MED_REFNUMMER = "sak.hentMedRefnummer";
	public static final String QUERY_OPPDATER_INTERNNOTAT = "sak.oppdaterInternnotat";
	@Column(name="ALLTID_PAA_PDA_FLAGG")
	private Boolean alltidPaaPda;
	@OneToMany(mappedBy = "sak", orphanRemoval=true)
	@OrderBy("opprettetDato")
	private List<Avvik> avvik;
	@Column(name="ENDRET_AV")
	private String endretAv;
    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endretDato;
	@OneToOne(mappedBy = "sak", cascade=CascadeType.REFRESH, orphanRemoval=true)
	private Forsikringsskade forsikringsskade;
	@Id
	@Column(nullable = false)
	private String guid;
	@OneToOne(mappedBy = "sak", cascade=CascadeType.REFRESH, orphanRemoval=true)
	
	private Hendelse hendelse;
	@ManyToOne
    @JoinColumn(name = "INSPEKSJON_GUID", referencedColumnName = "GUID")
    private Inspeksjon inspeksjon;
	private String kommune;
	@ManyToOne
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
	private Driftkontrakt kontrakt;
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	private Long kontraktId;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name="PDA_DFU_IDENT")
	private Long pdaDfuIdent;
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Sak.REFNUMMER_SEQ)
	@Column(name="refnummer")
	private Long refnummer;
	
	private String skadested;
	@OneToOne(mappedBy = "sak",cascade=CascadeType.REFRESH, orphanRemoval=true)
	private Skred skred;
	@Column(name="SLETTET_AV")
    private String slettetAv;
	@Column(name="SLETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date slettetDato;

	@OneToMany(mappedBy = "sak", cascade = { CascadeType.ALL }, orphanRemoval=true)
	private List<Punktstedfest> stedfesting;
	
	private String internnotat;
	//en uheldig bieffekt av å sette annotasjoner på attributtnivå..
	@SuppressWarnings("unused")
	@Transient
	private Punktveiref veiref;
	@SuppressWarnings("unused")
	@Transient
	private String veirefString;
	@SuppressWarnings("unused")
	@Transient
	private String internnotatFormatted;
	
	public Punktveiref getVeiref(){
		List<Punktstedfest> sl = getStedfesting();
		if (sl!=null&&sl.size()>0)
			return sl.get(0).getVeiref();
		return null;
	}
	public String getVeirefString(){
		Punktveiref veiref = getVeiref();
		if (veiref!=null){
			return veiref.getFylkesnummer()+"/"+veiref.getKommunenummer()+" "+veiref.getVeikategori()+veiref.getVeistatus()+veiref.getVeinummer()+" hp"+veiref.getHp()+" m"+veiref.getMeter();
		}
		return null;
	}
	public List<Punktstedfest> getStedfesting() {
		return stedfesting;
	}
	public void setStedfesting(List<Punktstedfest> stedfesting) {
		this.stedfesting = stedfesting;
	}
	
	public Boolean getAlltidPaaPda() {
		return alltidPaaPda;
	}
	public List<Avvik> getAvvik() {
		return avvik;
	}
	public String getEndretAv() {
		return endretAv;
	}
	public Date getEndretDato() {
		return endretDato;
	}
	public Forsikringsskade getForsikringsskade() {
		return forsikringsskade;
	}
	public String getGuid() {
		return guid;
	}
	public Hendelse getHendelse() {
		return hendelse;
	}
	public Inspeksjon getInspeksjon() {
		return inspeksjon;
	}
	public String getKommune() {
		return kommune;
	}
	public Driftkontrakt getKontrakt() {
		return kontrakt;
	}
	public Long getKontraktId() {
		return kontraktId;
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}
	public Long getRefnummer() {
		return refnummer;
	}
	public String getSkadested() {
		return skadested;
	}
	
	public Skred getSkred() {
		return skred;
	}
	public String getSlettetAv() {
		return slettetAv;
	}
	public Date getSlettetDato() {
		return slettetDato;
	}
	
	public void setAlltidPaaPda(Boolean alltidPaaPda) {
		Boolean old = this.alltidPaaPda;
		this.alltidPaaPda = alltidPaaPda;
		firePropertyChange("alltidPaaPda", old, alltidPaaPda);
	}
	public void setAvvik(List<Avvik> avvik) {
		this.avvik = avvik;
	}
	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}
	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}
	public void setForsikringsskade(Forsikringsskade forsikringsskade) {
		this.forsikringsskade = forsikringsskade;
	}
	public void setGuid(String guid) {
		String old = this.guid;
		this.guid = guid;
		firePropertyChange("guid", old, guid);
	}
	public void setHendelse(Hendelse hendelse) {
		this.hendelse = hendelse;
	}
	public void setInspeksjon(Inspeksjon inspeksjon) {
		Inspeksjon old = this.inspeksjon;
		this.inspeksjon = inspeksjon;
		firePropertyChange("inspeksjon", old, inspeksjon);
	}
	public void setKommune(String kommune) {
		String old = this.kommune;
		this.kommune = kommune;
		firePropertyChange("kommune", old, kommune);
	}
	public void setKontrakt(Driftkontrakt kontrakt) {
		Driftkontrakt old = this.kontrakt;
		this.kontrakt = kontrakt;
		if (kontrakt!=null){
			setKontraktId(kontrakt.getId());
		}else{
			setKontraktId(null);
		}
			
		firePropertyChange("kontrakt", old, kontrakt);
	}
	public void setKontraktId(Long kontraktId) {
		Long old = this.kontraktId;
		this.kontraktId = kontraktId;
		firePropertyChange("kontraktId", old, kontraktId);
	}
	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}
	public void setPdaDfuIdent(Long pdaDfuIdent) {
		Long old = this.pdaDfuIdent;
		this.pdaDfuIdent = pdaDfuIdent;
		firePropertyChange("pdaDfuIdent", old, pdaDfuIdent);
	}
	public void setRefnummer(Long refnummer) {
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}
	public void setSkadested(String skadested) {
		String old = this.skadested;
		this.skadested = skadested;
		firePropertyChange("skadested", old, skadested);
	}
	public void setSkred(Skred skred) {
		this.skred = skred;
	}
	public void setSlettetAv(String slettetAv) {
		String old = this.slettetAv;
		this.slettetAv = slettetAv;
		firePropertyChange("slettetAv", old, slettetAv);
	}
	public void setSlettetDato(Date slettetDato) {
		Date old = this.slettetDato;
		this.slettetDato = slettetDato;
		firePropertyChange("slettetDato", old, slettetDato);
	}
	public void setInternnotat(String internnotat) {
		String old = this.internnotat;
		String oldF = getInternnotatFormatted();
		this.internnotat = internnotat;
		firePropertyChange("internnotat", old, internnotat);
		firePropertyChange("internnotatFormatted", oldF, getInternnotatFormatted());
	}
	public String getInternnotat() {
		return internnotat;
	}
	public String getInternnotatFormatted(){
		if (internnotat==null)
			return "";
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append(internnotat.replace("\n", "<br>"));
		sb.append("</html>");
		return sb.toString();
	}
    

	
}
