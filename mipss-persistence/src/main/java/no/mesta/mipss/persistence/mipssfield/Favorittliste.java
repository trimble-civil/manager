package no.mesta.mipss.persistence.mipssfield;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

@SuppressWarnings("serial")
@Entity
@SequenceGenerator(name = Favorittliste.FAVORITTLISTE_ID_SEQ, sequenceName = "FAVORITTLISTE_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries ({
	@NamedQuery(name = Favorittliste.FIND_ALL, query = "select o from Favorittliste o"),
	@NamedQuery(name = Favorittliste.FIND_BY_KONTRAKT, query = "SELECT o FROM Favorittliste o where o.kontraktId = :id")
})
public class Favorittliste extends MipssEntityBean<Favorittliste> {
	public static final String FAVORITTLISTE_ID_SEQ = "favorittlisteIdSeq";
	public static final String FIND_ALL = "Favorittliste.findAll";
	public static final String FIND_BY_KONTRAKT = "Favorittliste.findByKontrakt";
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = FAVORITTLISTE_ID_SEQ)
	@Column(nullable = false)

	private Long id;
	@Column(name = "KONTRAKT_ID", nullable = false, insertable = false, updatable = false)
	private Long kontraktId;
	@Column(nullable = false)
	private String navn;
	private String beskrivelse;
	@Column(name = "OPPRETTET_AV")
	private String opprettetAv;
	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name = "ENDRET_AV")
	private String endretAv;
	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;
	@ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
	private Driftkontrakt driftkontrakt;
	@OneToMany(mappedBy = "favorittliste", cascade = CascadeType.ALL)
	private List<FavorittProsess> favorittProsessList = new ArrayList<FavorittProsess>();
	@Transient
	private Boolean lagreFavorittProsesser = Boolean.FALSE;
	
	public Favorittliste() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		Object old = this.kontraktId;
		this.kontraktId = kontraktId;
		getProps().firePropertyChange("kontraktId", old, kontraktId);
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		Object old = this.navn;
		this.navn = navn;
		getProps().firePropertyChange("navn", old, navn);
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		Object old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		getProps().firePropertyChange("beskrivelse", old, beskrivelse);
	}

	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		Object old = this.driftkontrakt;
		this.driftkontrakt = driftkontrakt;
		
		getProps().firePropertyChange("driftkontrakt", old, driftkontrakt);
		
		if(this.driftkontrakt != null) {
			setKontraktId(driftkontrakt.getId());
		} else {
			setKontraktId(null);
		}
	}

	public List<FavorittProsess> getFavorittProsessList() {
		return favorittProsessList;
	}

	public void setFavorittProsessList(List<FavorittProsess> favorittProsessList) {
		this.favorittProsessList = favorittProsessList;
	}

	/**
	 * Om denne er satt true vil nye favorittprosesser lagres og de som er tatt bort slettes
	 * 
	 * @return
	 */
	public Boolean getLagreFavorittProsesser() {
		return lagreFavorittProsesser;
	}

	public void setLagreFavorittProsesser(Boolean lagreFavorittProsesser) {
		this.lagreFavorittProsesser = lagreFavorittProsesser;
	}
}
