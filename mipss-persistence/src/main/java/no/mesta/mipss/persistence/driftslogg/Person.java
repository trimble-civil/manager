package no.mesta.mipss.persistence.driftslogg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@SequenceGenerator(name = Person.ID_SEQ, sequenceName = "PERSON_ID_SEQ", initialValue = 1, allocationSize = 1)

@SuppressWarnings("serial")
@Entity
@Table(name = "PERSON")
public class Person implements Serializable {

    public static final String ID_SEQ = "personIdSeq";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Trip.ID_SEQ)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
    private Date created;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ENABLED")
    private boolean enabled;

    @Column(name = "EXPIRED")
    private boolean expired;

    @Column(name = "FIRSTNAME")
    private String firstName;

    @Column(name = "LASTNAME")
    private String lastName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LASTUPDATEDATE")
    private Date lastUpdated;

    @Column(name = "LEVERANDOR_NAVN")
    private String contractorName;

    @Column(name = "LEVERANDOR_NR")
    private String contractorNr;

    @Column(name = "VALGBARFLAGG")
    private boolean selectable;

    @Column(name = "LOCKED")
    private boolean locked;

    @Column(name = "PASSWORD")
    private String passwordHash;

    @Column(name = "ROLE")
    private String ROLE;

    @Column(name = "DRIFTKONTRAKT_NR")
    private String contractorContractNr;

    @Column(name = "ADRESSE")
    private String address;

    @Column(name = "E_POST_SENDT_FLAGG")
    private boolean emailSent;

    @Column(name = "ORGNR")
    private String organizationNumber;

    public Long getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

    public String getEmail() {
        return email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isExpired() {
        return expired;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public String getContractorName() {
        return contractorName;
    }

    public String getContractorNr() {
        return contractorNr;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public boolean isLocked() {
        return locked;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getROLE() {
        return ROLE;
    }

    public String getContractorContractNr() {
        return contractorContractNr;
    }

    public String getAddress() {
        return address;
    }

    public boolean isEmailSent() {
        return emailSent;
    }

    public String getOrganizationNumber() {
        return organizationNumber;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

}