package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Rodetype
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Rodetype.QUERY_FIND_ALL, query = "select o from Rodetype o order by o.id"),
	@NamedQuery(name = Rodetype.QUERY_FIND_COMMON, query = "select o from Rodetype o where o.driftkontrakt is null and o.valgbarFlagg=true order by o.id"),
	@NamedQuery(name = Rodetype.QUERY_FIND_BY_KONTRAKTID, query = "select o from Rodetype o where o.driftkontrakt.id=:kontraktId and o.valgbarFlagg=true order by o.id")
})
@SequenceGenerator(name = Rodetype.ID_SEQ, sequenceName = "RODETYPE_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
public class Rodetype extends MipssEntityBean<Rodetype> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
    public static final String QUERY_FIND_ALL = "Rodetype.findAll";
    public static final String QUERY_FIND_COMMON = "Rodetype.findCommon";
    public static final String QUERY_FIND_BY_KONTRAKTID = "Rodetype.findByKontraktId";
    public static final String ID_SEQ = "rodetypeIdSeq";
    private Boolean genererDauFlagg;
    private Long id;
    private String navn;
    private Boolean valgbarFlagg;
    private List<Rode> rodeList;
    
    private Driftkontrakt driftkontrakt;
    private String beskrivelse;
    private Boolean bareEnRodeFlagg;
    private Boolean genererProduksjonFlagg;
    private Boolean kanHaOverlappFlagg;
    private Date slettetDato;
    private OwnedMipssEntity ownedMipssEntity;
    public Rodetype() {
    }
    
    @ManyToOne
    @JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID")
    public Driftkontrakt getDriftkontrakt() {
        return driftkontrakt;
    }

    public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
    	Driftkontrakt old = this.driftkontrakt;
        this.driftkontrakt = driftkontrakt;
        firePropertyChange("driftkontrakt", old, driftkontrakt);
    }
    
    @Column(name="SLETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSlettetDato() {
        return slettetDato;
    }

    public void setSlettetDato(Date slettetDato) {
    	Date old = this.slettetDato;
        this.slettetDato = slettetDato;
        firePropertyChange("slettetDato", old, slettetDato);
    }
    
    @Column(name="BARE_EN_RODE_FLAGG", nullable = false)
    public Boolean getBareEnRodeFlagg() {
        return bareEnRodeFlagg;
    }

    public void setBareEnRodeFlagg(Boolean bareEnRodeFlagg) {
    	Boolean old = this.bareEnRodeFlagg;
        this.bareEnRodeFlagg = bareEnRodeFlagg;
        firePropertyChange("bareEnRodeFlagg", old, bareEnRodeFlagg);
    }
    
    @Column(name="GENERER_PRODUKSJON_FLAGG", nullable = false)
    public Boolean getGenererProduksjonFlagg() {
        return genererProduksjonFlagg;
    }

    public void setGenererProduksjonFlagg(Boolean genererProduksjonFlagg) {
    	Boolean old = this.genererProduksjonFlagg;
        this.genererProduksjonFlagg = genererProduksjonFlagg;
        firePropertyChange("genererProduksjonFlagg", old, genererProduksjonFlagg);
    }
    
    @Column(name="KAN_HA_OVERLAPP_FLAGG", nullable = false)
    public Boolean getKanHaOverlappFlagg() {
        return kanHaOverlappFlagg;
    }

    public void setKanHaOverlappFlagg(Boolean kanHaOverlappFlagg) {
    	Boolean old = this.kanHaOverlappFlagg;
        this.kanHaOverlappFlagg = kanHaOverlappFlagg;
        firePropertyChange("kanHaOverlappFlagg", old, kanHaOverlappFlagg);
    }
    
    @Column(name="GENERER_DAU_FLAGG", nullable = false)
    public Boolean getGenererDauFlagg() {
        return genererDauFlagg;
    }

    public void setGenererDauFlagg(Boolean genererDauFlagg) {
    	Boolean old = this.genererDauFlagg;
        this.genererDauFlagg = genererDauFlagg;
        firePropertyChange("genererDauFlagg", old, genererDauFlagg);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Rodetype.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }
    
    public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

    @OneToMany(mappedBy = "rodetype")
    public List<Rode> getRodeList() {
        return rodeList;
    }

    public void setRodeList(List<Rode> rodeList) {
        this.rodeList = rodeList;
    }

    public Rode addRode(Rode rode) {
        getRodeList().add(rode);
        rode.setRodetype(this);
        return rode;
    }

    public Rode removeRode(Rode rode) {
        getRodeList().remove(rode);
        rode.setRodetype(null);
        return rode;
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            append("valgbarFlagg", valgbarFlagg).
            append("bareEnRodeFlagg", bareEnRodeFlagg).
            append("genererDauFlagg", genererDauFlagg).
            append("genererProduksjonFlagg", genererProduksjonFlagg).
            append("kanHaOverlappFlagg", kanHaOverlappFlagg).
            append("beskrivelse", beskrivelse).
            append("slettetDato", slettetDato).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Rodetype)) {return false;}
        
        Rodetype other = (Rodetype) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(1251,1621).append(id).toHashCode();
    }
    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
}
