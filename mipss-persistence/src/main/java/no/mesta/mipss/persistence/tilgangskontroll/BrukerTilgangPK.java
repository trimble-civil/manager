package no.mesta.mipss.persistence.tilgangskontroll;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BrukerTilgangPK implements Serializable {
    private Long modulId;
    private String signatur;

    public BrukerTilgangPK() {
    }

    public BrukerTilgangPK(Long modulId, String signatur) {
        this.modulId = modulId;
        this.signatur = signatur;
    }

    public boolean equals(Object other) {
        if (other instanceof BrukerTilgangPK) {
            final BrukerTilgangPK otherBrukerTilgangPK = (BrukerTilgangPK) other;
            final boolean areEqual = 
                (otherBrukerTilgangPK.modulId.equals(modulId) && otherBrukerTilgangPK.signatur.equals(signatur));
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void setModulId(Long modulId) {
        this.modulId = modulId;
    }

    public Long getModulId() {
        return modulId;
    }

    public void setSignatur(String signatur) {
        this.signatur = signatur;
    }

    public String getSignatur() {
        return signatur;
    }
}
