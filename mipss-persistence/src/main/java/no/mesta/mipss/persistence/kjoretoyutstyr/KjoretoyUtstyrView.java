/**
 * 
 */
package no.mesta.mipss.persistence.kjoretoyutstyr;

import java.io.Serializable;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Forenklet versjon av {@code KjoretoyUtstyr} som flater ut hele
 * strukturen med grupper, subgrupper, osv og som velger den siste perioden, samt
 * portnummeret som passer til den.
 * 
 * Denne klassen er ikke en del av JPA entitetene, men en erstattning for et DB-view.
 *  
 * @author jorge
 *
 */
@SuppressWarnings("serial")
public class KjoretoyUtstyrView implements Serializable, IRenderableMipssEntity {
	private Long id;
	private Long kjoretoyId;
	private String utstyrgruppeNavn;
	private String utstyrsubgruppeNavn;
	private String utstyrmodellNavn;
	private String individSerienummer;
	private Long aktivPort;
	private String fritekst;
	
	/**
	 * Oppretter en utflatet versjon av den leverte instansen.
	 * @param base
	 */
	public KjoretoyUtstyrView(KjoretoyUtstyr base) {
		// disse n�klene kan ikke v�re null
		id = base.getId();
		kjoretoyId = base.getKjoretoyId();
		
		if(base.getUtstyrgruppeForGui() == null) {
			utstyrgruppeNavn = "";
		}
		else {
			utstyrgruppeNavn = base.getUtstyrgruppeForGui().getNavn();
		}
		
		if(base.getUtstyrsubgruppeForGui() == null) {
			utstyrsubgruppeNavn = "";
		}
		else {
			utstyrsubgruppeNavn = base.getUtstyrsubgruppeForGui().getNavn();
		}
		
		if(base.getUtstyrmodellForGui() == null) {
			utstyrmodellNavn = "";
		}
		else {
			utstyrmodellNavn = base.getUtstyrmodellForGui().getNavn();
		}

		aktivPort = getAktivPort(base);
		
		fritekst = base.getFritekst();
	}
	
	/**
	 * Velger en {@code KjoretoyUtstyrIo} fra listen holdt av det leverte {@code KjoretoyUtstyr}
	 * ut fra om den aktive perioden i {@code KjoretoyUtstyrIo} starter innenfor den aktive
	 * peioden i det leverte {@code KjoretoyUtstyr}.
	 * 
	 * Henter s� {@code bitNr} feltet i den valgte {@code KjoretoyUtstyrIo}.
	 * @param base
	 * @param periode
	 * @return bitnummeret eller null hvis ingen relevant {@code KjoretoyUtstyrIo}.
	 */
	private Long getAktivPort(KjoretoyUtstyr base) {
		long now = Clock.now().getTime();
		
		// ingen periode -> ingen port
		if(base.getKjoretoyUtstyrIoList() == null) {
			return null;
		}
		
		// listen er sortert med nyeste først og det finnes ingen overlappinger for en port
		for(KjoretoyUtstyrIo io : base.getKjoretoyUtstyrIoList()) {
			long ioFra = io.getFraDato().getTime();
			
			if(ioFra <= now) {
				if(io.getTilDato() == null || io.getTilDato().getTime() >= now) {
					return io.getBitNr();
				}
			}
			
			//long ioTil = Long.MAX_VALUE;
			//if(io.getTilDato() == null) {
			//	ioTil = io.getTilDato().getTime();
			//}
//			if(ioFra > fra) {
//				return io.getBitNr();
//			}
		}
		
		return null;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the kjoretoyId
	 */
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	/**
	 * @param kjoretoyId the kjoretoyId to set
	 */
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	/**
	 * @return the utstyrgruppeNavn
	 */
	public String getUtstyrgruppeNavn() {
		return utstyrgruppeNavn;
	}

	/**
	 * @param utstyrgruppeNavn the utstyrgruppeNavn to set
	 */
	public void setUtstyrgruppeNavn(String utstyrgruppeNavn) {
		this.utstyrgruppeNavn = utstyrgruppeNavn;
	}

	/**
	 * @return the utstyrsubgruppeNavn
	 */
	public String getUtstyrsubgruppeNavn() {
		return utstyrsubgruppeNavn;
	}

	/**
	 * @param utstyrsubgruppeNavn the utstyrsubgruppeNavn to set
	 */
	public void setUtstyrsubgruppeNavn(String utstyrsubgruppeNavn) {
		this.utstyrsubgruppeNavn = utstyrsubgruppeNavn;
	}

	/**
	 * @return the utstyrmodellNavn
	 */
	public String getUtstyrmodellNavn() {
		return utstyrmodellNavn;
	}

	/**
	 * @param utstyrmodellNavn the utstyrmodellNavn to set
	 */
	public void setUtstyrmodellNavn(String utstyrmodellNavn) {
		this.utstyrmodellNavn = utstyrmodellNavn;
	}

	/**
	 * @return the individSerienummer
	 */
	public String getIndividSerienummer() {
		return individSerienummer;
	}

	/**
	 * @param individSerienummer the individSerienummer to set
	 */
	public void setIndividSerienummer(String individSerienummer) {
		this.individSerienummer = individSerienummer;
	}

	/**
	 * @return the aktivPort
	 */
	public Long getAktivPort() {
		return aktivPort;
	}

	/**
	 * @param aktivPort the aktivPort to set
	 */
	public void setAktivPort(Long aktivPort) {
		this.aktivPort = aktivPort;
	}

	/**
	 * @return the fritekst
	 */
	public String getFritekst() {
		return fritekst;
	}

	/**
	 * @param fritekst the fritekst to set
	 */
	public void setFritekst(String fritekst) {
		this.fritekst = fritekst;
	}

	public String getTextForGUI() {
		return String.valueOf(getId());
	}
	
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("kjoretoy id", getKjoretoyId()).
            append("gruppe", getUtstyrgruppeNavn()).
            append("subgruppe", getUtstyrsubgruppeNavn()).
            append("modell", getUtstyrmodellNavn()).
            append("individ", getIndividSerienummer()).
            append("port", getAktivPort()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof KjoretoyUtstyrView)) {
            return false;
        }
        KjoretoyUtstyrView other = (KjoretoyUtstyrView) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,97).
            append(getId()).toHashCode();
    }

    public int compareTo(Object o) {
        KjoretoyUtstyrView other = (KjoretoyUtstyrView) o;
            return new CompareToBuilder().
                append(getId(), other.getId()).
                toComparison();
    }
}
