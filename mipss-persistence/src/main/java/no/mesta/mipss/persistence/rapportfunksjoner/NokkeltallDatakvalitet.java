package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;

public class NokkeltallDatakvalitet implements Serializable{
	private String kontraktnavn;
	
	/** Antall kjøretøy UTEN siste livstegn siste 3 døgn */
	private int antKjoretoyUtenSisteLivstegn3d;
	/** Antall kjøretøy UTEN siste livstegn siste 14 døgn */
	private int antKjoretoyUtenSisteLivstegn14d;
	/** Antall kjøretøy uten siste GPS siste 3 døgn */
	private int antKjoretoyUtenSisteGps3d;
	/** Antall kjøretøy uten siste GPS siste 14 døgn */
	private int antKjoretoyUtenSisteGps14d;
	/** Antall kjøretøy med manuell spreder uten gyldig strøperiode  */
	private int antKjoretoyManuellSprederUtenGyldigStroperiode;
	/** Ukjent strømetode (antall kjøretøy) */
	private int antKjoretoyUkjentStrometode;
	/** Ukjent produksjon (antall kjøretøy)  */
	private int antKjoretoyUkjentProduksjon;
	/** Ikke produksjonssatte bestillinger: antall. (mer enn 14 dager gamle fra sysdate)  */
	private int antIkkeProduksjonsatteBestillinger;
	/** Antall produksjonssatte kjøretøy uten utstyr på kontrakten. sysdate  */
	private int antProdsatteKjoretoyUtenUtstyr;
	/** Total antall kjøretøy med feil */
	private int antTotalKjoretoyMFeil;
	
	public void incAntKjoretoyUtenSisteLivstegn3d(){
		antKjoretoyUtenSisteLivstegn3d++;
	}
	public void incAntKjoretoyUtenSisteLivstegn14d(){
		antKjoretoyUtenSisteLivstegn14d++;
	}
	public void incAntKjoretoyUtenSisteGps3d(){
		antKjoretoyUtenSisteGps3d++;
	}
	public void incAntKjoretoyUtenSisteGps14d(){
		antKjoretoyUtenSisteGps14d++;
	}
	public void incAntKjoretoyManuellSprederUtenGyldigStroperiode(){
		antKjoretoyManuellSprederUtenGyldigStroperiode++;
	}
	public void incAntKjoretoyUkjentStrometode(){
		antKjoretoyUkjentStrometode++;
	}
	public void incAntKjoretoyUkjentProduksjon(){
		antKjoretoyUkjentProduksjon++;
	}
	public void incAntIkkeProduksjonsatteBestillinger(){
		antIkkeProduksjonsatteBestillinger++;
	}
	public void incAntProdsatteKjoretoyUtenUtstyr(){
		antProdsatteKjoretoyUtenUtstyr++;
	}
	public void incAntTotalKjoretoyMFeil(){
		antTotalKjoretoyMFeil++;
	}
	/**
	 * @return the antKjoretoyUtenSisteLivstegn3d
	 */
	public int getAntKjoretoyUtenSisteLivstegn3d() {
		return antKjoretoyUtenSisteLivstegn3d;
	}
	/**
	 * @param antKjoretoyUtenSisteLivstegn3d the antKjoretoyUtenSisteLivstegn3d to set
	 */
	public void setAntKjoretoyUtenSisteLivstegn3d(int antKjoretoyUtenSisteLivstegn3d) {
		this.antKjoretoyUtenSisteLivstegn3d = antKjoretoyUtenSisteLivstegn3d;
	}
	/**
	 * @return the antKjoretoyUtenSisteLivstegn14d
	 */
	public int getAntKjoretoyUtenSisteLivstegn14d() {
		return antKjoretoyUtenSisteLivstegn14d;
	}
	/**
	 * @param antKjoretoyUtenSisteLivstegn14d the antKjoretoyUtenSisteLivstegn14d to set
	 */
	public void setAntKjoretoyUtenSisteLivstegn14d(
			int antKjoretoyUtenSisteLivstegn14d) {
		this.antKjoretoyUtenSisteLivstegn14d = antKjoretoyUtenSisteLivstegn14d;
	}
	/**
	 * @return the antKjoretoyUtenSisteGps3d
	 */
	public int getAntKjoretoyUtenSisteGps3d() {
		return antKjoretoyUtenSisteGps3d;
	}
	/**
	 * @param antKjoretoyUtenSisteGps3d the antKjoretoyUtenSisteGps3d to set
	 */
	public void setAntKjoretoyUtenSisteGps3d(int antKjoretoyUtenSisteGps3d) {
		this.antKjoretoyUtenSisteGps3d = antKjoretoyUtenSisteGps3d;
	}
	/**
	 * @return the antKjoretoyUtenSisteGps14d
	 */
	public int getAntKjoretoyUtenSisteGps14d() {
		return antKjoretoyUtenSisteGps14d;
	}
	/**
	 * @param antKjoretoyUtenSisteGps14d the antKjoretoyUtenSisteGps14d to set
	 */
	public void setAntKjoretoyUtenSisteGps14d(int antKjoretoyUtenSisteGps14d) {
		this.antKjoretoyUtenSisteGps14d = antKjoretoyUtenSisteGps14d;
	}
	/**
	 * @return the antKjoretoyManuellSprederUtenGyldigStroperiode
	 */
	public int getAntKjoretoyManuellSprederUtenGyldigStroperiode() {
		return antKjoretoyManuellSprederUtenGyldigStroperiode;
	}
	/**
	 * @param antKjoretoyManuellSprederUtenGyldigStroperiode the antKjoretoyManuellSprederUtenGyldigStroperiode to set
	 */
	public void setAntKjoretoyManuellSprederUtenGyldigStroperiode(
			int antKjoretoyManuellSprederUtenGyldigStroperiode) {
		this.antKjoretoyManuellSprederUtenGyldigStroperiode = antKjoretoyManuellSprederUtenGyldigStroperiode;
	}
	/**
	 * @return the antKjoretoyUkjentStrometode
	 */
	public int getAntKjoretoyUkjentStrometode() {
		return antKjoretoyUkjentStrometode;
	}
	/**
	 * @param antKjoretoyUkjentStrometode the antKjoretoyUkjentStrometode to set
	 */
	public void setAntKjoretoyUkjentStrometode(int antKjoretoyUkjentStrometode) {
		this.antKjoretoyUkjentStrometode = antKjoretoyUkjentStrometode;
	}
	/**
	 * @return the antKjoretoyUkjentProduksjon
	 */
	public int getAntKjoretoyUkjentProduksjon() {
		return antKjoretoyUkjentProduksjon;
	}
	/**
	 * @param antKjoretoyUkjentProduksjon the antKjoretoyUkjentProduksjon to set
	 */
	public void setAntKjoretoyUkjentProduksjon(int antKjoretoyUkjentProduksjon) {
		this.antKjoretoyUkjentProduksjon = antKjoretoyUkjentProduksjon;
	}
	/**
	 * @return the antIkkeProduksjonsatteBestillinger
	 */
	public int getAntIkkeProduksjonsatteBestillinger() {
		return antIkkeProduksjonsatteBestillinger;
	}
	/**
	 * @param antIkkeProduksjonsatteBestillinger the antIkkeProduksjonsatteBestillinger to set
	 */
	public void setAntIkkeProduksjonsatteBestillinger(
			int antIkkeProduksjonsatteBestillinger) {
		this.antIkkeProduksjonsatteBestillinger = antIkkeProduksjonsatteBestillinger;
	}
	/**
	 * @return the antProdsatteKjoretoyUtenUtstyr
	 */
	public int getAntProdsatteKjoretoyUtenUtstyr() {
		return antProdsatteKjoretoyUtenUtstyr;
	}
	/**
	 * @param antProdsatteKjoretoyUtenUtstyr the antProdsatteKjoretoyUtenUtstyr to set
	 */
	public void setAntProdsatteKjoretoyUtenUtstyr(int antProdsatteKjoretoyUtenUtstyr) {
		this.antProdsatteKjoretoyUtenUtstyr = antProdsatteKjoretoyUtenUtstyr;
	}
	/**
	 * @return the antTotalKjoretoyMFeil
	 */
	public int getAntTotalKjoretoyMFeil() {
		return antTotalKjoretoyMFeil;
	}
	/**
	 * @param antTotalKjoretoyMFeil the antTotalKjoretoyMFeil to set
	 */
	public void setAntTotalKjoretoyMFeil(int antTotalKjoretoyMFeil) {
		this.antTotalKjoretoyMFeil = antTotalKjoretoyMFeil;
	}
	public void setKontraktnavn(String kontraktnavn) {
		this.kontraktnavn = kontraktnavn;
	}
	public String getKontraktnavn() {
		return kontraktnavn;
	}
	
	
}
