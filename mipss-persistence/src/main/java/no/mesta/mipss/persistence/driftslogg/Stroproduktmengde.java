package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

import javax.persistence.*;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.stroing.Stroprodukt;

@SuppressWarnings("serial")
@Entity
@Table(name="STROPRODUKTMENGDE")
@NamedQueries({
	@NamedQuery(name = Stroproduktmengde.QUERY_GET_ONE, query = "SELECT s FROM Stroproduktmengde s where s.id=:id")
})
@SequenceGenerator(name = Stroproduktmengde.ID_SEQ, sequenceName = "STROPRODUKTMENGDE_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Stroproduktmengde extends MipssEntityBean<Stroproduktmengde> implements IRenderableMipssEntity{

	public static final String QUERY_GET_ONE = "Stroproduktmengde.getStroproduktById";
	public static final String ID_SEQ = "stroproduktmengdeIdSeq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Stroproduktmengde.ID_SEQ)
	private Long id;
	@Column(name="AKTIVITET_ID", nullable = true, insertable = false, updatable = false)
	private Long aktivitetId;
	@ManyToOne
	@JoinColumn(name = "AKTIVITET_ID", referencedColumnName = "ID")
	private Aktivitet aktivitet;
	@ManyToOne
	@JoinColumn(name = "STROPRODUKT_ID", referencedColumnName = "ID")
	private Stroprodukt stroprodukt;
	
	@Column(name="STROPRODUKT_ID", nullable=true, insertable=false, updatable = false)
	private Long stroproduktId;
	
	@Column(name="ENHET_VAATT")
	private String enhetVaatt;
	@Column(name="MELDT_MENGDE_TORT")
	private Double meldtMengdeTort;
	@Column(name="MELDT_MENGDE_VAATT")
	private Double meldtMengdeVaatt;
	@Column(name="OMFORENT_MENGDE_TORT")
	private Double omforentMengdeTort;
	@Column(name="OMFORENT_MENGDE_VAATT")
	private Double omforentMengdeVaatt;
	private String kommentar;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	@Column(name="ENDRET_AV")
	private String endretAv;
	
	@Column(name="ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;
	@Column(name="INTERN_KOMMENTAR")
	private String internKommentar;
	
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @return the aktivitetId
	 */
	public Long getAktivitetId() {
		return aktivitetId;
	}
	/**
	 * @return the stroproduktId
	 */
	public Long getStroproduktId() {
		return stroproduktId;
	}
	/**
	 * @return the enhetVaatt
	 */
	public String getEnhetVaatt() {
		return enhetVaatt;
	}
	/**
	 * @return the meldtMengdeTort
	 */
	public Double getMeldtMengdeTort() {
		return meldtMengdeTort;
	}
	/**
	 * @return the meldtMengdeVaatt
	 */
	public Double getMeldtMengdeVaatt() {
		return meldtMengdeVaatt;
	}
	/**
	 * @return the omforentMengdeTort
	 */
	public Double getOmforentMengdeTort() {
		return omforentMengdeTort;
	}
	/**
	 * @return the omforentMengdeVaatt
	 */
	public Double getOmforentMengdeVaatt() {
		return omforentMengdeVaatt;
	}
	/**
	 * @return the kommentar
	 */
	public String getKommentar() {
		return kommentar;
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @return the endretAv
	 */
	public String getEndretAv() {
		return endretAv;
	}
	/**
	 * @return the endretDato
	 */
	public Date getEndretDato() {
		return endretDato;
	}
	/**
	 * @return the internKommentar
	 */
	public String getInternKommentar() {
		return internKommentar;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	/**
	 * @param aktivitetId the aktivitetId to set
	 */
	public void setAktivitetId(Long aktivitetId) {
		Long old = this.aktivitetId;
		this.aktivitetId = aktivitetId;
		firePropertyChange("aktivitetId", old, aktivitetId);
	}
	/**
	 * @param stroproduktId the stroproduktId to set
	 */
	public void setStroproduktId(Long stroproduktId) {
		Long old = this.stroproduktId;
		this.stroproduktId = stroproduktId;
		firePropertyChange("stroproduktId", old, stroproduktId);
	}
	/**
	 * Setter enhetVaatt. Ved å sende inn "..." blir enhet vaatt satt til null.
	 * Det er gjort på denne måten på grunn av muligheten for å kunne endre strøprodukt/enhetstype.
	 * @param enhetVaatt the enhetVaatt to set
	 */
	public void setEnhetVaatt(String enhetVaatt) {
		String old = this.enhetVaatt;
		
		if(enhetVaatt.equals("...")){
			this.enhetVaatt = null;
			firePropertyChange("enhetVaatt", old, null);
		}
		else {
			this.enhetVaatt = enhetVaatt;
			firePropertyChange("enhetVaatt", old, enhetVaatt);
		}	
	}
	/**
	 * @param meldtMengdeTort the meldtMengdeTort to set
	 */
	public void setMeldtMengdeTort(Double meldtMengdeTort) {
		Double old = this.meldtMengdeTort;
		this.meldtMengdeTort = meldtMengdeTort;
		firePropertyChange("meldtMengdeTort", old, meldtMengdeTort);
	}
	/**
	 * @param meldtMengdeVaatt the meldtMengdeVaatt to set
	 */
	public void setMeldtMengdeVaatt(Double meldtMengdeVaatt) {
		Double old = this.meldtMengdeVaatt;
		this.meldtMengdeVaatt = meldtMengdeVaatt;
		firePropertyChange("meldtMengdeVaatt", old, meldtMengdeVaatt);
	}
	/**
	 * @param omforentMengdeTort the omforentMengdeTort to set
	 */
	public void setOmforentMengdeTort(Double omforentMengdeTort) {
		Double old = this.omforentMengdeTort;
		this.omforentMengdeTort = omforentMengdeTort;
		firePropertyChange("omforentMengdeTort", old, omforentMengdeTort);
	}
	/**
	 * @param omforentMengdeVaatt the omforentMengdeVaatt to set
	 */
	public void setOmforentMengdeVaatt(Double omforentMengdeVaatt) {
		Double old = this.omforentMengdeVaatt;
		this.omforentMengdeVaatt = omforentMengdeVaatt;
		firePropertyChange("omforentMengdeVaatt", old, omforentMengdeVaatt);
	}
	/**
	 * @param kommentar the kommentar to set
	 */
	public void setKommentar(String kommentar) {
		String old = this.kommentar;
		this.kommentar = kommentar;
		firePropertyChange("kommentar", old, kommentar);
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}
	/**
	 * @param endretAv the endretAv to set
	 */
	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}
	/**
	 * @param endretDato the endretDato to set
	 */
	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}
	/**
	 * @param internKommentar the internKommentar to set
	 */
	public void setInternKommentar(String internKommentar) {
		String old = this.internKommentar;
		this.internKommentar = internKommentar;
		firePropertyChange("internKommentar", old, internKommentar);
	}
	public void setAktivitet(Aktivitet aktivitet) {
		Aktivitet old = this.aktivitet;
		this.aktivitet = aktivitet;
		firePropertyChange("aktivitet", old, aktivitet);
		
		if (aktivitet!=null){
			setAktivitetId(aktivitet.getId());
		}else{
			setAktivitetId(null);
		}
	}
	public Aktivitet getAktivitet() {
		return aktivitet;
	}
	public void setStroprodukt(Stroprodukt stroprodukt) {
		Stroprodukt old = this.stroprodukt;
		this.stroprodukt = stroprodukt;
		firePropertyChange("stroprodukt", old, stroprodukt);
		
		if (stroprodukt!=null){
			setStroproduktId(stroprodukt.getId());
		}else{
			setStroproduktId(null);
		}
	}
	public Stroprodukt getStroprodukt() {
		return stroprodukt;
	}
}
