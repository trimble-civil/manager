package no.mesta.mipss.persistence.mipssfield;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.vo.HPFilterKey;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;

/**
 * Filterer strekninger til unike HP
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class HPFilterUtil {

    /**
     * Vasker en liste med prodstrekninger til unike HP'er
     * 
     * @param strekninger
     * @return
     */
    public static List<VeiInfo> filterProdstrekningSokOnHp(Collection<ProdstrekningSok> strekninger) {
        List<VeiInfo> veier = new ArrayList<VeiInfo>();
        for(ProdstrekningSok p:strekninger) {
            veier.add(VeiInfoUtil.buildVeiInfo(p));
        }
        
        return filterOnHp(veier);
    }

    /**
     * Vasker en liste med prodstrekninger til unike HP'er
     * 
     * @param strekninger
     * @return
     */
    public static List<VeiInfo> filterProdstrekningOnHp(Collection<Prodstrekning> strekninger) {
        List<VeiInfo> veier = new ArrayList<VeiInfo>();
        for(Prodstrekning p:strekninger) {
            veier.add(VeiInfoUtil.buildVeiInfo(p));
        }
        
        return filterOnHp(veier);
    }

    /**
     * Vasker en liste med prodstrekninger til unike HP'er
     * 
     * @param veirefs
     * @return
     */
    public static List<VeiInfo> filterPunktveirefOnHp(Collection<Punktveiref> veirefs) {
        List<VeiInfo> veier = new ArrayList<VeiInfo>();
        for(Punktveiref p:veirefs) {
            veier.add(VeiInfoUtil.buildVeiInfo(p));
        }
        
        return filterOnHp(veier);
    }
    
    /**
     * Filtrerer veier gitt HP
     * 
     * @param veier
     * @return
     */
    public static List<VeiInfo> filterOnHp(List<VeiInfo> veier) {
        Map<HPFilterKey, VeiInfo> vask = new HashMap<HPFilterKey, VeiInfo>();
        List<VeiInfo> vasket = new ArrayList<VeiInfo>();
        for (VeiInfo v:veier) {
            HPFilterKey key = new HPFilterKey(v);
            if(!vask.containsKey(key)) {
                vask.put(key, v);
                vasket.add(v);
            }
        }
        
        return vasket;
    }
}
