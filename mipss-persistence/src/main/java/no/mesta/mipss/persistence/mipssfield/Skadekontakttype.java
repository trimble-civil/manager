package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Type klasse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
    @NamedQuery(name = Skadekontakttype.QUERY_FIND_ALL, query = "SELECT s FROM Skadekontakttype s")
    })
@SequenceGenerator(name = Skadekontakttype.ID_SEQ, sequenceName = "SKADEKONTAKT_ID_SEQ", initialValue = 1, allocationSize = 1)
@Table(name="SKADEKONTAKT")
public class Skadekontakttype extends MipssEntityBean<Skadekontakttype> implements Serializable, IRenderableMipssEntity {	
    public static final String ID_SEQ = "skadekontaktIdSeq";
    public static final String QUERY_FIND_ALL = "Skadekontakttype.findAll";
    
    @Column(name="GUI_REKKEFOLGE")
    private Long guiRekkefolge;
    
    @Id
    @Column(nullable=false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Skadekontakttype.ID_SEQ)
    private Long id;
    
    @Column(name="NAVN")
    private String navn;
    
    @Column(name="TYPE")
    private String type;
    
    @Column(name="VALGBAR_FLAGG")
    private Boolean valgbar;
    
    /**
     * Konstrukt�r
     * 
     */
    public Skadekontakttype() {
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Skadekontakttype)) {return false;}
        
        Skadekontakttype other = (Skadekontakttype) o;
        return new EqualsBuilder().append(navn, other.getNavn()).isEquals();
    }

    public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}
     

     public Long getId() {
		return id;
	}
     
    public String getNavn() {
        return navn;
    }
    
    public String getTextForGUI() {
		return navn;
	}

    public String getType() {
    	 return type;
     }
    
    public Boolean getValgbar() {
		return valgbar;
	}

	/**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(2037,2063).append(navn).toHashCode();
    }

	public void setGuiRekkefolge(Long guiRekkefolge) {
		Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}

	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	/**
     * Id
     * 
     * @param navn
     */
    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

	public void setType(String type) {
    	 String old = this.type;
    	 this.type = type;
    	 firePropertyChange("type", old, type);
     }

	public void setValgbar(Boolean valgbar) {
		Boolean old = this.valgbar;
		this.valgbar = valgbar;
		firePropertyChange("valgbar", old, valgbar);
	}

	/**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", navn).
            append("type", type).
            toString();
    }
}
