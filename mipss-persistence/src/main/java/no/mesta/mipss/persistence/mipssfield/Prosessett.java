package no.mesta.mipss.persistence.mipssfield;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@NamedQueries( { @NamedQuery(name = Prosessett.QUERY_FIND_ALL, query = "SELECT p FROM Prosessett p") })
public class Prosessett extends MipssEntityBean<Prosessett> implements IRenderableMipssEntity {
	
	
	public static final String QUERY_FIND_ALL = "Prosesssett.findAll";
	
	@Id
	@Column(nullable = false)
	private Long id;
	private String navn;
	private String beskrivelse;

//	@Column(name = "FRA_AAR", nullable = false)
//	private Long fraAar;
	
	@Column(name = "OPPRETTET_AV")
	private String opprettetAv;
	
	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	
	@Column(name="ENDRET_AV")
	private String endretAv;
	
	@Column(name="ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endretDato;
	
	@OneToMany(mappedBy = "prosessett", cascade = CascadeType.ALL)
	private List<Prosess> prosessList = new ArrayList<Prosess>();
	
	public Prosessett(){
		
	}
	@Override
	public Object clone(){
		Prosessett p = new Prosessett();
		p.setId(id);
		p.setNavn(navn);
		p.setBeskrivelse(beskrivelse);
//		p.setFraAar(fraAar);
		p.setOpprettetAv(opprettetAv);
		p.setOpprettetDato(opprettetDato);
		p.setEndretAv(endretAv);
		p.setEndretDato(endretDato);
		return p;
	}
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange("beskrivelse", old, beskrivelse);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public void setOpprettetAv(String opprettetAv) {
		String old = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange("opprettetAv", old, opprettetAv);
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	public void setOpprettetDato(Date opprettetDato) {
		Date old = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange("opprettetDato", old, opprettetDato);
	}
	public String getEndretAv() {
		return endretAv;
	}
	public void setEndretAv(String endretAv) {
		String old = this.endretAv;
		this.endretAv = endretAv;
		firePropertyChange("endretAv", old, endretAv);
	}
	public Date getEndretDato() {
		return endretDato;
	}
	public void setEndretDato(Date endretDato) {
		Date old = this.endretDato;
		this.endretDato = endretDato;
		firePropertyChange("endretDato", old, endretDato);
	}
	
	public List<Prosess> getProsessList() {
		return prosessList;
	}

	public void setProsessList(List<Prosess> prosessList) {
		this.prosessList = prosessList;
	}
	
	/** {@inheritDoc} */
	@Transient
	public String getTextForGUI() {
		return getNavn();
	}
}
