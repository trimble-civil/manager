package no.mesta.mipss.persistence.jobbinfo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.driftslogg.Periode;

import org.apache.commons.lang.builder.HashCodeBuilder;
@NamedQueries({
    @NamedQuery(name = Feilmelding.SOK_UBEHANDLET, query = "select o from Feilmelding o where o.behandletFlagg=0"),
    @NamedQuery(name = Feilmelding.SOK_BEHANDLET, query = "select o from Feilmelding o where o.behandletFlagg=1"),
})

@SuppressWarnings("serial")
@Entity
@Table(name="FEILMELDING")
public class Feilmelding extends MipssEntityBean<Feilmelding> implements IRenderableMipssEntity{
	public static final String SOK_UBEHANDLET = "Feilmelding.sokUbehandlet";
	public static final String SOK_BEHANDLET = "Feilmelding.sokBehandlet";

	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="TYPE_ID", nullable = false, insertable = false, updatable = false)
	private Long typeId;
	
	@ManyToOne
	@JoinColumn(name = "TYPE_ID", referencedColumnName = "ID")
	private Feilmeldingstype feilmeldingstype;
	
	@Column(name="MELDING")
	private String melding;
	
	@Column(name="BEHANDLET_FLAGG")
	private Boolean behandletFlagg;
	
	@Column(name="BEHANDLET_AV")
	private String behandletAv;
	
	@Column(name="KOMMENTAR")
	private String kommentar;
	
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	
	@Column(name="BEHANDLET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date behandletDato;


	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}

	public String getMelding() {
		return melding;
	}



	public void setMelding(String melding) {
		this.melding = melding;
	}



	public Boolean getBehandletFlagg() {
		return behandletFlagg;
	}



	public void setBehandletFlagg(Boolean behandletFlagg) {
		this.behandletFlagg = behandletFlagg;
	}



	public String getBehandletAv() {
		return behandletAv;
	}



	public void setBehandletAv(String behandletAv) {
		this.behandletAv = behandletAv;
	}



	public Date getOpprettetDato() {
		return opprettetDato;
	}



	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	
	public Date getBehandletDato() {
		return behandletDato;
	}



	public void setBehandletDato(Date behandletDato) {
		this.behandletDato = behandletDato;
	}

	public int hashCode(){
		return new HashCodeBuilder().append(getId()).toHashCode();
	}



	public void setKommentar(String kommentar) {
		this.kommentar = kommentar;
	}



	public String getKommentar() {
		return kommentar;
	}

	public Long getTypeId() {
		return typeId;
	}



	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}



	public Feilmeldingstype getFeilmeldingstype() {
		return feilmeldingstype;
	}



	public void setFeilmeldingstype(Feilmeldingstype feilmeldingstype) {
		this.feilmeldingstype = feilmeldingstype;
	}

}
