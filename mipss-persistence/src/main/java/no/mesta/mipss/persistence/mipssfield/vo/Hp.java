package no.mesta.mipss.persistence.mipssfield.vo;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

/**
 * Skriver pent ut en HP
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class Hp extends HPFilterKey implements Serializable {

	public Hp(VeiInfo v) {
		super(v);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return (StringUtils.isBlank(kategori) ? "" : kategori) + (StringUtils.isBlank(status) ? "" : status)
				+ (nummer == 0 ? "" : String.valueOf(nummer)) + " " + (hp == 0 ? "" : "hp" + String.valueOf(hp));
	}
}
