package no.mesta.mipss.persistence.rapportfunksjoner;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class Veirapp implements Serializable, Rapp1Entity{
	
	private String veikategori;
	private Long veinummer;
	private Long kjoretoyId;
	private String kjoretoyNavn;
	private Date dogn;
	private Double totalKjortKm;
	private Long totalKjortSekund;
	private Long paaKontraktFlagg;
	private List<R12Prodtype> prodtyper;
	private List<Materiale> materialer;
	
	public String getVeikategori() {
		return veikategori;
	}
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}
	public Long getVeinummer() {
		return veinummer;
	}
	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	public String getKjoretoyNavn(){
		return kjoretoyNavn;
	}
	public void setKjoretoyNavn(String kjoretoyNavn){
		this.kjoretoyNavn = kjoretoyNavn;
	}
	public Date getDogn() {
		return dogn;
	}
	public void setDogn(Date dogn) {
		this.dogn = dogn;
	}
	public Double getTotalKjortKm() {
		return totalKjortKm;
	}
	public void setTotalKjortKm(Double totalKjortKm) {
		this.totalKjortKm = totalKjortKm;
	}
	public Long getTotalKjortSekund() {
		return totalKjortSekund;
	}
	public void setTotalKjortSekund(Long totalKjortSekund) {
		this.totalKjortSekund = totalKjortSekund;
	}
	public List<R12Prodtype> getProdtyper() {
		return prodtyper;
	}
	public void setProdtyper(List<R12Prodtype> prodtyper) {
		this.prodtyper = prodtyper;
	}
	public List<Materiale> getMaterialer() {
		return materialer;
	}
	public void setMaterialer(List<Materiale> materialer) {
		this.materialer = materialer;
	}
	public List<R12Stroprodukt> getStroprodukter() {
		return null;
	}
	public double getTid(){
		return (double)totalKjortSekund/(double)86400;
	}
	public Boolean getPaaKontrakt() {
		if (paaKontraktFlagg!=null){
			return paaKontraktFlagg.intValue()==1;
		}
		return Boolean.FALSE;
	}
	public void setPaaKontraktFlagg(Long paaKontraktFlagg) {
		this.paaKontraktFlagg = paaKontraktFlagg;
	}
	public Long getPaaKontraktFlagg() {
		return paaKontraktFlagg;
	}
	
}
