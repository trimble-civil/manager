package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "Dfukategori.findAll", 
    query = "select o from Dfukategori o")
public class Dfukategori extends MipssEntityBean<Dfukategori> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Dfukategori.findAll";

    private String fritekst;
    private String navn;
    private List<Dfumodell> dfumodellList = new ArrayList<Dfumodell>();

    public Dfukategori() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @OneToMany(mappedBy = "dfukategori")
    public List<Dfumodell> getDfumodellList() {
        return dfumodellList;
    }

    public void setDfumodellList(List<Dfumodell> dfumodellList) {
        this.dfumodellList = dfumodellList;
    }

    public Dfumodell addDfumodell(Dfumodell dfumodell) {
        getDfumodellList().add(dfumodell);
        dfumodell.setDfukategori(this);
        return dfumodell;
    }

    public Dfumodell removeDfumodell(Dfumodell dfumodell) {
        getDfumodellList().remove(dfumodell);
        dfumodell.setDfukategori(null);
        return dfumodell;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Dfukategori)) {
            return false;
        }
        Dfukategori other = (Dfukategori) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append((getNavn())).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Dfukategori other = (Dfukategori) o;
        return new CompareToBuilder().
            append(getNavn(), other.getNavn()).
            toComparison();
    }
}
