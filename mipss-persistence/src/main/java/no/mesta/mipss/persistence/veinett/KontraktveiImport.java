package no.mesta.mipss.persistence.veinett;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
@SuppressWarnings("serial")
@Entity
@Table(name="kontraktvei_import")
@SequenceGenerator(name = KontraktveiImport.ID_SEQ, sequenceName = "KONTRAKTVEI_IMPORT_ID_SEQ", initialValue = 1, allocationSize = 1)

public class KontraktveiImport implements Serializable{
	public static final String ID_SEQ = "kontraktveiimportIdSeq";
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = KontraktveiImport.ID_SEQ)
    @Column(nullable = false)
	private Long id;
	@ManyToOne
    @JoinColumn(name="KONTRAKTVEINETT_ID", nullable=false)
	private KontraktveinettImport kontraktveinettImport;
	
	private Long fylke;
	private Long kommune;
	private String vegkategori;
	private String vegstatus;
	private Long vegnummer;
	private Long hp;
	@Column(name="FRAMETER")
	private Long fraMeter;
	@Column(name="TILMETER")
	private Long tilMeter;
	private Long sectype2;
	private Long veglengde;
	@Column(name="FRASTED")
	private String fraSted;
	@Column(name="TILSTED")
	private String tilSted;
	@Column(name="AADT_TOTAL")
	private Long AadtTotal;
	private String vegfunksjon;
	@Column(name="TYPE_STATEGI")
	private String typeStrategi;
	@Transient
	private String driftsKlasse;
	
	public String toString(){
		return new ToStringBuilder(this)
		.append("fylke", fylke)
		.append("kommune", kommune)
		.append("vegkategori", vegkategori)
		.append("vegstatus", vegstatus)
		.append("vegnummer", vegnummer)
		.append("hp", hp)
		.append("fraMeter", fraMeter)
		.append("tilMeter", tilMeter)
		.append("sectype2", sectype2)
		.append("veglengde", veglengde)
		.append("fraSted", fraSted)
		.append("tilSted", tilSted)
		.append("AadtTotal", AadtTotal)
		.append("vegfunksjon", vegfunksjon)
		.append("typeStrategi", typeStrategi)
		.append("driftsKlasse", driftsKlasse).toString();
	}
	/**
	 * @return the fylke
	 */
	public Long getFylke() {
		return fylke;
	}
	/**
	 * @param fylke the fylke to set
	 */
	public void setFylke(Long fylke) {
		this.fylke = fylke;
	}
	/**
	 * @return the kommune
	 */
	public Long getKommune() {
		return kommune;
	}
	/**
	 * @param kommune the kommune to set
	 */
	public void setKommune(Long kommune) {
		this.kommune = kommune;
	}
	/**
	 * @return the vegkategori
	 */
	public String getVegkategori() {
		return vegkategori;
	}
	/**
	 * @param vegkategori the vegkategori to set
	 */
	public void setVegkategori(String vegkategori) {
		this.vegkategori = vegkategori;
	}
	/**
	 * @return the vegstatus
	 */
	public String getVegstatus() {
		return vegstatus;
	}
	/**
	 * @param vegstatus the vegstatus to set
	 */
	public void setVegstatus(String vegstatus) {
		this.vegstatus = vegstatus;
	}
	/**
	 * @return the vegnummer
	 */
	public Long getVegnummer() {
		return vegnummer;
	}
	/**
	 * @param vegnummer the vegnummer to set
	 */
	public void setVegnummer(Long vegnummer) {
		this.vegnummer = vegnummer;
	}
	/**
	 * @return the hp
	 */
	public Long getHp() {
		return hp;
	}
	/**
	 * @param hp the hp to set
	 */
	public void setHp(Long hp) {
		this.hp = hp;
	}
	/**
	 * @return the fraMeter
	 */
	public Long getFraMeter() {
		return fraMeter;
	}
	/**
	 * @param fraMeter the fraMeter to set
	 */
	public void setFraMeter(Long fraMeter) {
		this.fraMeter = fraMeter;
	}
	/**
	 * @return the tilMeter
	 */
	public Long getTilMeter() {
		return tilMeter;
	}
	/**
	 * @param tilMeter the tilMeter to set
	 */
	public void setTilMeter(Long tilMeter) {
		this.tilMeter = tilMeter;
	}
	/**
	 * @return the sectype2
	 */
	public Long getSectype2() {
		return sectype2;
	}
	/**
	 * @param sectype2 the sectype2 to set
	 */
	public void setSectype2(Long sectype2) {
		this.sectype2 = sectype2;
	}
	/**
	 * @return the veglengde
	 */
	public Long getVeglengde() {
		return veglengde;
	}
	/**
	 * @param veglengde the veglengde to set
	 */
	public void setVeglengde(Long veglengde) {
		this.veglengde = veglengde;
	}
	/**
	 * @return the fraSted
	 */
	public String getFraSted() {
		return fraSted;
	}
	/**
	 * @param fraSted the fraSted to set
	 */
	public void setFraSted(String fraSted) {
		this.fraSted = fraSted;
	}
	/**
	 * @return the tilSted
	 */
	public String getTilSted() {
		return tilSted;
	}
	/**
	 * @param tilSted the tilSted to set
	 */
	public void setTilSted(String tilSted) {
		this.tilSted = tilSted;
	}
	/**
	 * @return the aadtTotal
	 */
	public Long getAadtTotal() {
		return AadtTotal;
	}
	/**
	 * @param aadtTotal the aadtTotal to set
	 */
	public void setAadtTotal(Long aadtTotal) {
		AadtTotal = aadtTotal;
	}
	/**
	 * @return the vegfunksjon
	 */
	public String getVegfunksjon() {
		return vegfunksjon;
	}
	/**
	 * @param vegfunksjon the vegfunksjon to set
	 */
	public void setVegfunksjon(String vegfunksjon) {
		this.vegfunksjon = vegfunksjon;
	}
	/**
	 * @return the typeStrategi
	 */
	public String getTypeStrategi() {
		return typeStrategi;
	}
	/**
	 * @param typeStrategi the typeStrategi to set
	 */
	public void setTypeStrategi(String typeStrategi) {
		this.typeStrategi = typeStrategi;
	}
	public void setDriftsKlasse(String driftsKlasse) {
		this.driftsKlasse = driftsKlasse;
	}
	public String getDriftsKlasse() {
		return driftsKlasse;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setKontraktveinettImport(KontraktveinettImport kontraktveinettImport) {
		this.kontraktveinettImport = kontraktveinettImport;
	}
	public KontraktveinettImport getKontraktveinettImport() {
		return kontraktveinettImport;
	}
	
	
}
