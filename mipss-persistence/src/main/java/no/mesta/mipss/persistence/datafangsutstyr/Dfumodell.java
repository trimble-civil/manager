package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries( {
		@NamedQuery(name = Dfumodell.FIND_ALL, query = "select o from Dfumodell o"),
		@NamedQuery(name = Dfumodell.FIND_BY_LEVERANDOR, query = "select o from Dfumodell o where o.leverandorNavn = :leverandorNavn") })
public class Dfumodell extends MipssEntityBean<Dfumodell> implements
		Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Dfumodell.findAll";
	/** Parametre: leverandorNavn */
	public static final String FIND_BY_LEVERANDOR = "Dfumodell.findByLeverandor";
	private String endretAv;
	private Date endretDato;
	private String fritekst;
	private String leverandorNavn;
	private String navn;
	private String opprettetAv;
	private Date opprettetDato;
	private Boolean valgbarFlagg;
	private List<Ioliste> iolisteList = new ArrayList<Ioliste>();
	private Dfukategori dfukategori;
	private List<Dfuindivid> dfuindividList = new ArrayList<Dfuindivid>();
	private List<Installasjon> installasjonList = new ArrayList<Installasjon>();

	public Dfumodell() {
	}

	@Column(name = "ENDRET_AV")
	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	@Column(name = "ENDRET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndretDato() {
		return endretDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	public String getFritekst() {
		return fritekst;
	}

	public void setFritekst(String fritekst) {
		String old = this.fritekst;
		this.fritekst = fritekst;
		firePropertyChange("fritekst", old, fritekst);
	}

	@Column(name = "LEVERANDOR_NAVN", nullable = false)
	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		String old = this.leverandorNavn;
		this.leverandorNavn = leverandorNavn;
		firePropertyChange("leverandorNavn", old, leverandorNavn);
	}

	@Id
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}

	@Column(name = "OPPRETTET_AV")
	public String getOpprettetAv() {
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	@Column(name = "OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	@Column(name = "VALGBAR_FLAGG", nullable = false)
	public Boolean getValgbarFlagg() {
		if (valgbarFlagg == null) {
			valgbarFlagg = false;
		}
		return valgbarFlagg;
	}

	public void setValgbarFlagg(Boolean valgbarFlagg) {
		Boolean old = this.valgbarFlagg;
		this.valgbarFlagg = valgbarFlagg;
		firePropertyChange("valgbarFlagg", old, valgbarFlagg);
	}

	@OneToMany(mappedBy = "dfumodell", fetch = FetchType.EAGER)
	public List<Ioliste> getIolisteList() {
		return iolisteList;
	}

	public void setIolisteList(List<Ioliste> iolisteList) {
		this.iolisteList = iolisteList;
	}

	public Ioliste addIoliste(Ioliste ioliste) {
		getIolisteList().add(ioliste);
		ioliste.setDfumodell(this);
		return ioliste;
	}

	public Ioliste removeIoliste(Ioliste ioliste) {
		getIolisteList().remove(ioliste);
		ioliste.setDfumodell(null);
		return ioliste;
	}

	@ManyToOne
	@JoinColumn(name = "KATEGORI_NAVN", referencedColumnName = "NAVN")
	public Dfukategori getDfukategori() {
		return dfukategori;
	}

	public void setDfukategori(Dfukategori dfukategori) {
		Dfukategori old = this.dfukategori;
		this.dfukategori = dfukategori;
		firePropertyChange("dfukategori", old, dfukategori);
	}

	@OneToMany(mappedBy = "dfumodell")
	public List<Dfuindivid> getDfuindividList() {
		return dfuindividList;
	}

	public void setDfuindividList(List<Dfuindivid> dfuindividList) {
		this.dfuindividList = dfuindividList;
	}

	public Dfuindivid addDfuindivid(Dfuindivid dfuindivid) {
		getDfuindividList().add(dfuindivid);
		dfuindivid.setDfumodell(this);
		return dfuindivid;
	}

	public Dfuindivid removeDfuindivid(Dfuindivid dfuindivid) {
		getDfuindividList().remove(dfuindivid);
		dfuindivid.setDfumodell(null);
		return dfuindivid;
	}

	public void setInstallasjonList(List<Installasjon> installasjonList) {
		this.installasjonList = installasjonList;
	}

	@OneToMany(mappedBy = "dfumodell")
	public List<Installasjon> getInstallasjonList() {
		return installasjonList;
	}

	public Installasjon addInstallasjon(Installasjon installasjon) {
		getInstallasjonList().add(installasjon);
		installasjon.setDfumodell(this);
		return installasjon;
	}

	public Installasjon removeInstallasjon(Installasjon installasjon) {
		getInstallasjonList().remove(installasjon);
		installasjon.setDfumodell(null);
		return installasjon;
	}

	// de neste metodene er h�ndkodet
	/**
	 * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
	 * 
	 * @return en tekstlinje.
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("navn", getNavn()).append(
				"leverandornavn", getLeverandorNavn()).toString();
	}

	/**
	 * Sammenlikner med o.
	 * 
	 * @param o
	 * @return true hvis relevante n�klene er like.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof Dfumodell)) {
			return false;
		}
		Dfumodell other = (Dfumodell) o;
		return new EqualsBuilder().append(getNavn(), other.getNavn())
				.isEquals();
	}

	/**
	 * Genererer identifikator hashkode.
	 * 
	 * @return koden.
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(7, 91).append(getNavn()).toHashCode();
	}

	public String getTextForGUI() {
		return getNavn();
	}

	public int compareTo(Object o) {
		Dfumodell other = (Dfumodell) o;
		return new CompareToBuilder().append(getNavn(), other.getNavn())
				.toComparison();
	}
	
	@Transient
	public List<Ioliste> getIolisteForMonteringsskjema() {
		List<Ioliste> retList = new ArrayList<Ioliste>();
		for(Ioliste ioliste : iolisteList) {
			if(ioliste.getVisesIMonteringsskjemaFlagg() != 0) {
				retList.add(ioliste);
			}
		}
		
		Collections.sort(retList, new Comparator<Ioliste>() {
			public int compare(Ioliste o1, Ioliste o2) {
				return o1.getGuiRekkefolge().compareTo(o2.getGuiRekkefolge());
			}
		});
		
		return retList;
	}
}
