package no.mesta.mipss.persistence.driftslogg;

import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class Mipsstall implements IRenderableMipssEntity{
	
	private Date fraDato;
	private Date tilDato;
	private String kjoretoyNavn;
	private String rodeNavn;
	private String tiltak;
	private Double timer;
	private Double km;
	private Double tonnTorrstoff;
	
	
	public String getPeriode(){
		//TODO...
		return "";
	}
	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}

	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}

	/**
	 * @param tilDato the tilDato to set
	 */
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}

	/**
	 * @return the kjoretoyNavn
	 */
	public String getKjoretoyNavn() {
		return kjoretoyNavn;
	}

	/**
	 * @param kjoretoyNavn the kjoretoyNavn to set
	 */
	public void setKjoretoyNavn(String kjoretoyNavn) {
		this.kjoretoyNavn = kjoretoyNavn;
	}

	/**
	 * @return the rodeNavn
	 */
	public String getRodeNavn() {
		return rodeNavn;
	}

	/**
	 * @param rodeNavn the rodeNavn to set
	 */
	public void setRodeNavn(String rodeNavn) {
		this.rodeNavn = rodeNavn;
	}

	/**
	 * @return the tiltak
	 */
	public String getTiltak() {
		return tiltak;
	}

	/**
	 * @param tiltak the tiltak to set
	 */
	public void setTiltak(String tiltak) {
		this.tiltak = tiltak;
	}

	/**
	 * @return the timer
	 */
	public Double getTimer() {
		return timer;
	}

	/**
	 * @param timer the timer to set
	 */
	public void setTimer(Double timer) {
		this.timer = timer;
	}

	/**
	 * @return the km
	 */
	public Double getKm() {
		return km;
	}

	/**
	 * @param km the km to set
	 */
	public void setKm(Double km) {
		this.km = km;
	}

	/**
	 * @return the tonnTorrstoff
	 */
	public Double getTonnTorrstoff() {
		return tonnTorrstoff;
	}

	/**
	 * @param tonnTorrstoff the tonnTorrstoff to set
	 */
	public void setTonnTorrstoff(Double tonnTorrstoff) {
		this.tonnTorrstoff = tonnTorrstoff;
	}

	public String getTextForGUI() {
		return null;
	}

}
