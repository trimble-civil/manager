package no.mesta.mipss.persistence.config;

public class QueryHints {
	public static final String REFRESH = org.eclipse.persistence.config.QueryHints.REFRESH;//"eclipselink.refresh";
	public static final String READ_ONLY =  org.eclipse.persistence.config.QueryHints.READ_ONLY;
	public static final String FETCH_GROUP = org.eclipse.persistence.config.QueryHints.FETCH_GROUP;
	public static final String RESULT_TYPE = org.eclipse.persistence.config.QueryHints.RESULT_TYPE; 
}
