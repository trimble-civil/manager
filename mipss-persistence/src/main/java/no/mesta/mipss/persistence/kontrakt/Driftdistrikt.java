package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@SequenceGenerator(name = "driftdistriktIdSeq", sequenceName = "DRIFTDISTRIKT_ID_SEQ", initialValue = 1, allocationSize = 1)

@Entity
@NamedQuery(name = Driftdistrikt.FIND_ALL, 
    query = "select o from Driftdistrikt o")
public class Driftdistrikt extends MipssEntityBean<Driftdistrikt> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Driftdistrikt.findAll";
    private Long arealId;
    private String beskrivelse;
    private String endretAv;
    private Date endretDato;
    private Long id;
    private String navn;
    private String opprettetAv;
    private Date opprettetDato;
    private Driftregion driftregion;
    private List<Driftkontrakt> driftkontraktList = new ArrayList<Driftkontrakt>();

    public Driftdistrikt() {
    }

    @Column(name="AREAL_ID")
    public Long getArealId() {
        return arealId;
    }

    public void setArealId(Long arealId) {
    	Long old = this.arealId;
        this.arealId = arealId;
        firePropertyChange("arealId", old, arealId);
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
    	String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;
        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "driftdistriktIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }
    

    public void setId(Long id) {
    	Long old = this.id;
        this.id = id;
        firePropertyChange("id", old, id);
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="OPPRETTET_AV", nullable = false)
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO", nullable = false)
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }


    @ManyToOne
    @JoinColumn(name = "REGION_ID", referencedColumnName = "ID")
    public Driftregion getDriftregion() {
        return driftregion;
    }

    public void setDriftregion(Driftregion driftregion) {
    	Driftregion old = this.driftregion;
        this.driftregion = driftregion;
        firePropertyChange("driftregion", old, driftregion);
    }

    @OneToMany(mappedBy = "driftdistrikt")
    public List<Driftkontrakt> getDriftkontraktList() {
        return driftkontraktList;
    }

    public void setDriftkontraktList(List<Driftkontrakt> driftkontraktList) {
        this.driftkontraktList = driftkontraktList;
    }

    public Driftkontrakt addDriftkontrakt(Driftkontrakt driftkontrakt) {
        getDriftkontraktList().add(driftkontrakt);
        driftkontrakt.setDriftdistrikt(this);
        return driftkontrakt;
    }

    public Driftkontrakt removeDriftkontrakt(Driftkontrakt driftkontrakt) {
        getDriftkontraktList().remove(driftkontrakt);
        driftkontrakt.setDriftdistrikt(null);
        return driftkontrakt;
    }
    
    // de neste metodene er h�ndkodet
    /**
     * Sammenlikner Id-n�kkelen med o's Id-n�kkel.
     * @param o
     * @return true hvis n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Driftdistrikt)) {
            return false;
        }
        Driftdistrikt other = (Driftdistrikt) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
    }

    /**
     * Genererer en hashkode basert p� Id og kontraktsnummer.
     * @return hashkoden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(37,79).
            append(getId()).
            toHashCode();
    }

    @Transient
    public String getTextForGUI() {
        return getNavn();
    }

    public int compareTo(Object o) {
        Driftdistrikt other = (Driftdistrikt) o;
                return new CompareToBuilder().
                    append(getNavn(), other.getNavn()).
                    toComparison();
    }
    
}
