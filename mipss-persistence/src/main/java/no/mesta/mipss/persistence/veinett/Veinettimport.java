package no.mesta.mipss.persistence.veinett;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;

@Entity
@Table(name="VEINETT_IMPORT")
@SequenceGenerator(name = Veinettimport.ID_SEQ, sequenceName = "VEINETT_IMPORT_ID_SEQ", initialValue = 1, allocationSize = 1)
@NamedQueries({
	@NamedQuery(name = Veinettimport.QUERY_FIND_ALL, 
			query = "select v from Veinettimport v order by v.veinettId"),
	@NamedQuery(name = Veinettimport.QUERY_FIND_ALL_VEINETT, 
			query = "select distinct v.veinettId from Veinettimport v order by v.veinettId"),
	@NamedQuery(name = Veinettimport.QUERY_FIND_BY_VEINETT, 
			query = "select v from Veinettimport v where v.veinettId =:veinettId order by v.id", 
			hints={@QueryHint(name=QueryHints.REFRESH, value=HintValues.TRUE)})
})
public class Veinettimport {
	public static final String QUERY_FIND_ALL = "Veinettimport.findAll";
	public static final String QUERY_FIND_ALL_VEINETT = "Veinettimport.findAllVeinett";
	public static final String QUERY_FIND_BY_VEINETT = "Veinettimport.findByVeinett";
	
	
	public static final String ID_SEQ = "veinettimportIdSeq";
	private Long id;
	private Long veinettId;
	private Integer fylkesnummer;
	private Integer kommunenummer;
	private char veikategori;
	private char veistatus;
	private Integer veinummer;
	private Integer fraHp;
	private Integer fraMeter;
	private Integer tilHp;
	private Integer tilMeter;
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Veinettimport.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="VEINETT_ID", nullable = true)
    public Long getVeinettId(){
    	return veinettId;
    }
    
    public void setVeinettId(Long veinettId){
    	this.veinettId = veinettId;
    }
    
    public Integer getFylkesnummer() {
        return fylkesnummer;
    }

    public void setFylkesnummer(Integer fylkesnummer) {
        this.fylkesnummer = fylkesnummer;
    }
    
    public Integer getKommunenummer() {
        return kommunenummer;
    }

    public void setKommunenummer(Integer kommunenummer) {
        this.kommunenummer = kommunenummer;
    }
    
    public char getVeikategori() {
        return veikategori;
    }

    public void setVeikategori(char veikategori) {
        this.veikategori = veikategori;
    }
    
    public char getVeistatus() {
        return veistatus;
    }

    public void setVeistatus(char veistatus) {
        this.veistatus = veistatus;
    }
    
    public Integer getVeinummer() {
        return veinummer;
    }

    public void setVeinummer(Integer veinummer) {
        this.veinummer = veinummer;
    }
    @Column(name="FRA_HP", nullable = true)
    public Integer getFraHp() {
        return fraHp;
    }

    public void setFraHp(Integer fraHp) {
        this.fraHp = fraHp;
    }
    
    @Column(name="FRA_METER", nullable = true)
    public Integer getFraMeter() {
        return fraMeter;
    }

    public void setFraMeter(Integer fraMeter) {
        this.fraMeter = fraMeter;
    }
    
    @Column(name="TIL_HP", nullable = true)
    public Integer getTilHp() {
        return tilHp;
    }

    public void setTilHp(Integer tilHp) {
        this.tilHp = tilHp;
    }
    
    @Column(name="TIL_METER", nullable = true)
    public Integer getTilMeter() {
        return tilMeter;
    }

    public void setTilMeter(Integer tilMeter) {
        this.tilMeter = tilMeter;
    }
	
}
