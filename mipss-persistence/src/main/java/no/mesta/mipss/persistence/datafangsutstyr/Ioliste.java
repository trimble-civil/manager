package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = "iolisteIdSeq", sequenceName = "IOLISTE_ID_SEQ", initialValue = 1, allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = Ioliste.FIND_ALL, query = "select o from Ioliste o"),
    @NamedQuery(name = Ioliste.FIND_FOR_KJORETOY, query = "select o from Ioliste o " +
    														"where o.dfumodell.navn = " +
    														"(select i.modellNavn from Installasjon i " +
    															"where i.kjoretoy.id = :id " +
    															"and (" +
    																"(i.aktivTilDato>=:now " +
    																"and i.aktivFraDato<=:now) " +
    																"or " +
    																"(i.aktivTilDato is null " +
    																"and i.aktivFraDato <=:now)" +
    															"))")
})

public class Ioliste implements Serializable, IRenderableMipssEntity {
    public static final String FIND_ALL = "Ioliste.findAll";
    public static final String FIND_FOR_KJORETOY = "Ioliste.findForKjoretoy";
    
    private Long bitNr;
    private String endretAv;
    private Date endretDato;
    private String fritekst;
    private Long id;
    private String navn1;
    private String navn2;
    private String opprettetAv;
    private Date opprettetDato;
    private Dfumodell dfumodell;
    private Ioteknologi ioteknologi;
    
    private Long visesIMonteringsskjemaFlagg;
    private Long guiRekkefolge;

    public Ioliste() {
    }

    @Column(name="BIT_NR")
    public Long getBitNr() {
        return bitNr;
    }

    public void setBitNr(Long bitNr) {
        this.bitNr = bitNr;
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "iolisteIdSeq")
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(nullable = false)
    public String getNavn1() {
        return navn1;
    }

    public void setNavn1(String navn1) {
        this.navn1 = navn1;
    }

    public String getNavn2() {
        return navn2;
    }

    public void setNavn2(String navn2) {
        this.navn2 = navn2;
    }

    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }


    @ManyToOne
    @JoinColumn(name = "MODELL_NAVN", referencedColumnName = "NAVN")
    public Dfumodell getDfumodell() {
        return dfumodell;
    }

    public void setDfumodell(Dfumodell dfumodell) {
        this.dfumodell = dfumodell;
    }

    @ManyToOne
    @JoinColumn(name = "TEKNOLOGI_NAVN", referencedColumnName = "NAVN")
    public Ioteknologi getIoteknologi() {
        return ioteknologi;
    }

    public void setIoteknologi(Ioteknologi ioteknologi) {
        this.ioteknologi = ioteknologi;
    }
    
    @Column(name="VISES_I_MONTERINGSSKJEMA_FLAGG", nullable=false)
    public Long getVisesIMonteringsskjemaFlagg() {
		return visesIMonteringsskjemaFlagg;
	}

	public void setVisesIMonteringsskjemaFlagg(Long visesIMonteringsskjemaFlagg) {
		this.visesIMonteringsskjemaFlagg = visesIMonteringsskjemaFlagg;
	}

	@Column(name="GUI_REKKEFOLGE", nullable=false)
	public Long getGuiRekkefolge() {
		return guiRekkefolge;
	}

	public void setGuiRekkefolge(Long guiRekkefolge) {
		this.guiRekkefolge = guiRekkefolge;
	}

	// de neste metodene er håndkodet
    /**
     * Genererer en lesbar tekst med hovedlementene i entitybønnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", getId()).
            append("bitnummer", getBitNr()).
            append("navn1", getNavn1()).
            append("navn2", getNavn2()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante nøklene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Ioliste)) {
            return false;
        }
        Ioliste other = (Ioliste) o;
        return new EqualsBuilder().
            append(getId(), other.getId()).
            isEquals();
     }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,91).
            append(getId()).
            toHashCode();
    }
        
    public String getTextForGUI() {
        return getNavn2();
    }

    public int compareTo(Object o) {
        Ioliste other = (Ioliste) o;
            return new CompareToBuilder().
                append(getId(), other.getId()).
                toComparison();
    }
    
    @Transient
    public String getIoteknologiNavn() {
        return ioteknologi.getNavn();
    }
}
