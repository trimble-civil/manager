package no.mesta.mipss.persistence.mipssfield.r.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import no.mesta.mipss.persistence.MipssEntityBean;

/**
 * Egenskap
 *
 * Definerer en egenskap for et skred
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

@SuppressWarnings("serial")
@Entity(name = Egenskap.BEAN_NAME)
@Table(name="EGENSKAP")
@NamedQueries({
	@NamedQuery(name = Egenskap.QUERY_FIND_ALL, query = "SELECT e FROM Egenskap e"),
	@NamedQuery(name = Egenskap.QUERY_FIND_BY_TYPE, query = "SELECT e FROM Egenskap e where e.egenskaptype.id=:egenskaptypeId")
})

public class Egenskap extends MipssEntityBean<Egenskap>{
	public static final String BEAN_NAME = "Egenskap";
	public static final String QUERY_FIND_ALL = "Egenskap.findAll";
	public static final String QUERY_FIND_BY_TYPE = "Egenskap.findByType";

	@Column(name="BARE_EN_VERDI_FLAGG")
	private Boolean bareEnVerdi;
	@ManyToOne(optional = true)
	@JoinColumn(name = "TYPE_ID", referencedColumnName = "ID")
	private Egenskaptype egenskaptype;
	private String fortekst;
	@Id
	@Column(nullable = false)
	private Long id;
	private String navn;

	public Boolean getBareEnVerdi() {
		return this.bareEnVerdi;
	}
	public Egenskaptype getEgenskaptype() {
		return this.egenskaptype;
	}
	public String getFortekst() {
		return this.fortekst;
	}
	public Long getId() {
		return this.id;
	}
	public String getNavn() {
		return this.navn;
	}

	@Override
	public String getTextForGUI(){
		return this.id+":"+this.navn;
	}
	public void setBareEnVerdi(final Boolean bareEnVerdi) {
		final Boolean old = this.bareEnVerdi;
		this.bareEnVerdi = bareEnVerdi;
		firePropertyChange("bareEnVerdi", old, bareEnVerdi);
	}
	public void setEgenskaptype(final Egenskaptype egenskaptype) {
		final Egenskaptype old = this.egenskaptype;
		this.egenskaptype = egenskaptype;
		firePropertyChange("egenskaptype", old, egenskaptype);
	}
	public void setFortekst(final String fortekst) {
		final String old = this.fortekst;
		this.fortekst = fortekst;
		firePropertyChange("fortekst", old, fortekst);
	}
	public void setId(final Long id) {
		final Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}
	public void setNavn(final String navn) {
		final String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
}
