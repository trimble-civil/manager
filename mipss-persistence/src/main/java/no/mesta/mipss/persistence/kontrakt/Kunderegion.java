package no.mesta.mipss.persistence.kontrakt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = Kunderegion.FIND_ALL, query = "select o from Kunderegion o order by o.navn")
public class Kunderegion extends MipssEntityBean<Kunderegion> implements Serializable, IRenderableMipssEntity {
	public static final String FIND_ALL = "Kunderegion.findAll";
	private String navn;
	
	@Id
	@Column(nullable = false)
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}
	@Transient
    public String getTextForGUI() {
        return getNavn();
    }
	
}
