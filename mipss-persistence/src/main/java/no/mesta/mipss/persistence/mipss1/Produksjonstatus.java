package no.mesta.mipss.persistence.mipss1;

import java.io.Serializable;

import java.util.Date;

@SuppressWarnings("serial")
public class Produksjonstatus implements Serializable{
    private String enhetId;
    private String produksjonstatus;
    private String timestamp;
    private String retning;
    private String latitude;
    private String longitude;
    private Date tidspunkt;
    private String kilometrering;
    
    
    public Produksjonstatus() {
    }


    public void setProduksjonstatus(String produksjonstatus) {
        this.produksjonstatus = produksjonstatus;
    }

    public String getProduksjonstatus() {
        return produksjonstatus;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setEnhetId(String enhetId) {
        this.enhetId = enhetId;
    }

    public String getEnhetId() {
        return enhetId;
    }

    public void setRetning(String retning) {
        this.retning = retning;
    }

    public String getRetning() {
        return retning;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setTidspunkt(Date tidspunkt) {
        this.tidspunkt = tidspunkt;
    }

    public Date getTidspunkt() {
        return tidspunkt;
    }

    public void setKilometrering(String kilometrering) {
        this.kilometrering = kilometrering;
    }

    public String getKilometrering() {
        return kilometrering;
    }
}
