package no.mesta.mipss.persistence.datafangsutstyr;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * N�kkelklasse for DFUKontrakt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class DfuKontraktPK implements Serializable {
    private Long dfuId;
    private Long kontraktId;

    /**
     * Konstruktør
     * 
     */
    public DfuKontraktPK() {
    }

    /**
     * Konstruktør
     * 
     * @param dfuId
     * @param kontraktId
     */
    public DfuKontraktPK(Long dfuId, Long kontraktId) {
        this.dfuId = dfuId;
        this.kontraktId = kontraktId;
    }

    /**
     * DFU id
     * 
     * @param dfuId
     */
    public void setDfuId(Long dfuId) {
        this.dfuId = dfuId;
    }

    /**
     * DFU id
     * 
     * @return
     */
    public Long getDfuId() {
        return dfuId;
    }

    /**
     * Kontrakt id
     * 
     * @param kontraktId
     */
    public void setKontraktId(Long kontraktId) {
        this.kontraktId = kontraktId;
    }

    /**
     * Kontrakt id
     * 
     * @return
     */
    public Long getKontraktId() {
        return kontraktId;
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dfuId).append(kontraktId).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof DfuKontraktPK)) {return false;}
        
        DfuKontraktPK other = (DfuKontraktPK) o;
        
        return new EqualsBuilder().append(dfuId, other.dfuId).append(kontraktId, other.kontraktId).isEquals();
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("dfuId", dfuId).
            append("kontraktId", kontraktId).
            toString();
    }
}
