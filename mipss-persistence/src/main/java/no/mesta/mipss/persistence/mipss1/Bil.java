package no.mesta.mipss.persistence.mipss1;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
/*@Embeddable
@SqlResultSetMapping(
    name = "bilResult",
    entities = {@EntityResult(entityClass=Bil.class,
                              fields={@FieldResult(name="enhetId", column="ENHET_ID")}
                )}
)*/
@SuppressWarnings("serial")
public class Bil  implements Serializable, IRenderableMipssEntity, Comparable<Bil>{
    private String enhetId;
    
    public Bil() {
    }

    public void setEnhetId(String enhetId) {
        this.enhetId = enhetId;
    }

    public String getEnhetId() {
        return enhetId;
    }
    
    public String toString(){
        return enhetId;
    }
    
    public String getTextForGUI() {
        return enhetId;
    }

    public int compareTo(Bil o) {
        return 0;
    }
}
