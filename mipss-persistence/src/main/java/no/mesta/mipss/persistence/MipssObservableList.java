package no.mesta.mipss.persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import no.mesta.mipss.common.MipssObservableListListener;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.VetoableChangeSource;

import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;

/**
 * Liste som kan observeres og serialiseres
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class MipssObservableList<E extends Serializable> implements List<E>, ObservableList<E>, Serializable, PropertyChangeListener, VetoableChangeListener {
	private List<E> list;
	private transient List<ObservableListListener> listeners;
	private boolean register;
	private ArrayList<VetoableChangeListener> vetoListeners;
	
	
	/**
	 * Lager listene
	 * 
	 */
	public MipssObservableList() {
		list = new ArrayList<E>();
	}

	public MipssObservableList(List<E> list) {
		this.list = new ArrayList<E>(list);
	}

	public MipssObservableList(List<E> list, boolean register){
		this.register = register;
		this.list = new ArrayList<E>(list);
		if (register){
			for (E e:list){
				addListenerToEntity(e);
			}
		}
	}
	
	/** {@inheritDoc} */
	public boolean add(E e) {
		if (list.add(e)) {
			int index = list.indexOf(e);
			for (ObservableListListener l : getListeneres()) {
				l.listElementsAdded(this, index, 1);
			}
			if (register)
				addListenerToEntity(e);
			return true;
		} else {
			return false;
		}
	}

	private void addListenerToEntity(E e){
		if (e instanceof PropertyChangeSource){
			((PropertyChangeSource)e).addPropertyChangeListener(this);
		}
		if (e instanceof VetoableChangeSource){
			((VetoableChangeSource)e).addVetoableChangeListener(this);
		}
	}
	
	private void removeListenerToEntity(E e){
		if (e instanceof PropertyChangeSource){
			((PropertyChangeSource)e).removePropertyChangeListener(this);
		}
		if (e instanceof VetoableChangeSource){
			((VetoableChangeSource)e).removeVetoableChangeListener(this);
		}
	}
	/** {@inheritDoc} */
	public void add(int index, E element) {
		list.add(index, element);
		
		for (ObservableListListener l : getListeneres()) {
			l.listElementsAdded(this, index, 1);
		}
		if (register)
			addListenerToEntity(element);
	}

	/** {@inheritDoc} */
	public boolean addAll(Collection<? extends E> c) {
		return addAll(size(), c);
	}

	/** {@inheritDoc} */
	public boolean addAll(int index, Collection<? extends E> c) {
		if (list.addAll(index, c)) {
			for (ObservableListListener l : getListeneres()) {
				l.listElementsAdded(this, index, c.size());
			}
			for (E e:c){
				if (register)
					addListenerToEntity(e);
			}
			return true;
		} else {
			return false;
		}
	}
	
	/** {@inheritDoc} */
	public void addObservableListListener(ObservableListListener observableListListener) {
		getListeneres().add(observableListListener);
	}

	/** {@inheritDoc} */
	public void addVetoableListListener(VetoableChangeListener vetoListListener) {
		getVetoListeneres().add(vetoListListener);
	}
	
	/** {@inheritDoc} */
	public void clear() {
		for (E e:list){
			if (register)
				removeListenerToEntity(e);
		}
		
		List<E> copy = new ArrayList<E>(list);
		list.clear();
		if (!copy.isEmpty()) {
			for (ObservableListListener l : getListeneres()) {
				l.listElementsRemoved(this, 0, copy);
			}
		}
	}

	/** {@inheritDoc} */
	public Object clone() {
			MipssObservableList<E> v = new MipssObservableList<E>();
			v.list = new ArrayList<E>(list);
			v.listeners = new ArrayList<ObservableListListener>(getListeneres());
			return v;
	}

	/** {@inheritDoc} */
	public boolean contains(Object o) {
		return list.contains(o);
	}

	/** {@inheritDoc} */
	public boolean containsAll(Collection<?> c) {
		return list.containsAll(c);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o.getClass().equals(getClass()))) {
			return false;
		}

		MipssObservableList other = (MipssObservableList) o;

		return list.equals(other.getList());
	}

	/** {@inheritDoc} */
	public E get(int index) {
		if (list.size()>index&&index!=-1)
			return list.get(index);
		else 
			return null;
	}

	/**
	 * Benyttes til å gjøre .equals(Object)
	 * 
	 * @return
	 */
	private List<E> getList() {
		return list;
	}

	/**
	 * Returnerer listen av lyttere
	 * 
	 * @return
	 */
	public List<ObservableListListener> getListeneres() {
		if (listeners == null) {
			listeners = new ArrayList<ObservableListListener>();
		}

		return listeners;
	}
	
	/**
	 * Returnerer listen av lyttere
	 * 
	 * @return
	 */
	public List<VetoableChangeListener> getVetoListeneres() {
		if (vetoListeners == null) {
			vetoListeners = new ArrayList<VetoableChangeListener>();
		}
		return vetoListeners;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return list.hashCode();
	}

	/** {@inheritDoc} */
	public int indexOf(Object o) {
		return list.indexOf(o);
	}

	/** {@inheritDoc} */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/** {@inheritDoc} */
	public Iterator<E> iterator() {
		return list.iterator();
	}

	/** {@inheritDoc} */
	public int lastIndexOf(Object o) {
		return list.lastIndexOf(o);
	}

	/** {@inheritDoc} */
	public ListIterator<E> listIterator() {
		return list.listIterator();
	}

	/** {@inheritDoc} */
	public ListIterator<E> listIterator(int index) {
		return list.listIterator(index);
	}

	/** {@inheritDoc} */
	public E remove(int index) {
		E oldValue = list.remove(index);
		if (register)
			removeListenerToEntity(oldValue);
		
		for (ObservableListListener l : getListeneres()) {
			l.listElementsRemoved(this, index, Collections.singletonList(oldValue));
		}
		return oldValue;
	}
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public boolean remove(Object o) {
		int index = list.indexOf(o);
		
		
		if (list.remove(o)) {
			for (ObservableListListener l : getListeneres()) {
				l.listElementsRemoved(this, index, Collections.singletonList(o));
			}
			if (register){
				removeListenerToEntity((E)o);
			}
			return true;
		} else {
			return false;
		}
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public boolean removeAll(Collection<?> c) {
		int index = list.indexOf(c.toArray()[0]);
		
		
		if (list.removeAll(c)) {
			for (ObservableListListener l : getListeneres()) {
				l.listElementsRemoved(this, index, new ArrayList(c));
			}
			if (register){
				for (Object o:c){
					removeListenerToEntity((E)o);
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/** {@inheritDoc} */
	public void removeObservableListListener(ObservableListListener observableListListener) {
		getListeneres().remove(observableListListener);
	}
	/** {@inheritDoc} */
	public void removeVetoableChangeListener(VetoableChangeListener veotableListener) {
		getVetoListeneres().remove(veotableListener);
	}

	/** {@inheritDoc} */
	public boolean retainAll(Collection<?> c) {
		boolean modified = false;
		int index = -1;
		List<E> removedElements = new ArrayList<E>();
		Iterator<E> iterator = iterator();
		while (iterator.hasNext()) {
			E e = iterator.next();
			if (c.contains(e)) {
				if (index == -1) {
					index = list.indexOf(e);
				}
				removedElements.add(e);
				list.remove(e);
				modified = true;
			}
		}
		if (register){
			for (E el:removedElements){
				removeListenerToEntity(el);
			}
		}

		if (modified) {
			for (ObservableListListener l : getListeneres()) {
				l.listElementsRemoved(this, index, removedElements);
			}
		}

		return modified;
	}

	/** {@inheritDoc} */
	public E set(int index, E element) {
		if (register){
			removeListenerToEntity(list.get(index));
			addListenerToEntity(element);
		}
		E oldValue = list.set(index, element);
		

		for (ObservableListListener l : getListeneres()) {
			l.listElementReplaced(this, index, oldValue);
		}

		return oldValue;
	}

	/** {@inheritDoc} */
	public int size() {
		return list.size();
	}

	/** {@inheritDoc} */
	public List<E> subList(int fromIndex, int toIndex) {
		return list.subList(fromIndex, toIndex);
	}

	/** {@inheritDoc} */
	public boolean supportsElementPropertyChanged() {
		return register;
	}

	/** {@inheritDoc} */
	public Object[] toArray() {
		return list.toArray();
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public Object[] toArray(Object[] a) {
		return list.toArray(a);
	}
	
	/** {@inheritDoc} */
	@Override
	public String toString() {
		if(list == null) {
			return "list == null";
		} else {
			return list.toString();
		}
	}

	@SuppressWarnings("unchecked")
	public void propertyChange(PropertyChangeEvent evt) {
		int index = list.indexOf(evt.getSource());
		for (ObservableListListener l : getListeneres()) {
			if (l instanceof MipssObservableListListener){
				((MipssObservableListListener) l).listElementPropertyChanged(this, index, evt);
			}else{
				l.listElementPropertyChanged(this, index);
			}
		}
	}

	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		for (VetoableChangeListener l:getVetoListeneres()){
			l.vetoableChange(evt);
		}
	}
}
