package no.mesta.mipss.persistence.mipssfield.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Brukes til å sortere prodstrekninger
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class HPFilterKey implements Serializable {
	protected int hp;
	protected String kategori;
	protected int nummer;
	protected String status;

	public HPFilterKey(String kategori, String status, int nummer, int hp) {
		this.kategori = kategori;
		this.status = status;
		this.nummer = nummer;
		this.hp = hp;
	}

	public HPFilterKey(VeiInfo p) {
		this(p.getVeikategori(), p.getVeistatus(), p.getVeinummer(), p.getHp());
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof HPFilterKey)) {
			return false;
		}

		HPFilterKey other = (HPFilterKey) o;
		return new EqualsBuilder().append(kategori, other.kategori).append(status, other.status).append(nummer,
				other.nummer).append(hp, other.hp).isEquals();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(kategori).append(status).append(nummer).append(hp).toHashCode();
	}
}
