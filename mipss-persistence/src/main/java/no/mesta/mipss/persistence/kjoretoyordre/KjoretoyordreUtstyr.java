package no.mesta.mipss.persistence.kjoretoyordre;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;

@SuppressWarnings("serial")
@Entity
@Table(name="KJORETOYORDRE_UTSTYR")
@IdClass(KjoretoyordreUtstyrPK.class)
public class KjoretoyordreUtstyr extends MipssEntityBean<KjoretoyordreUtstyr> {
	@Id
	@Column(name="ORDRE_ID", nullable=false, updatable=false, insertable=false)
	private Long ordreId;
	
	@Id
	@Column(name="UTSTYR_ID", nullable=false, updatable=false, insertable=false)
	private Long utstyrId;
	
	@Column(name="UTSTYR_IO_ID", updatable=false, insertable=false)
	private Long utstyrIoId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPPRETTET_DATO")
	private Date opprettetDato;
	
	@ManyToOne
	@JoinColumn(name = "ORDRE_ID", referencedColumnName = "ID")
	private Kjoretoyordre kjoretoyordre;
	
	@ManyToOne
	@JoinColumn(name = "UTSTYR_ID", referencedColumnName = "ID")
	private KjoretoyUtstyr kjoretoyUtstyr;
	
	@ManyToOne
	@JoinColumn(name = "UTSTYR_IO_ID", referencedColumnName = "ID")
	private KjoretoyUtstyrIo kjoretoyUtstyrIo;
	
	

	public KjoretoyordreUtstyr() {
	}
	
	public Long getOrdreId() {
		return ordreId;
	}

	public void setOrdreId(Long ordreId) {
		this.ordreId = ordreId;
	}

	public Long getUtstyrId() {
		return utstyrId;
	}

	public void setUtstyrId(Long utstyrId) {
		this.utstyrId = utstyrId;
	}

	public Long getUtstyrIoId() {
		return utstyrIoId;
	}

	public void setUtstyrIoId(Long utstyrIoId) {
		this.utstyrIoId = utstyrIoId;
	}

	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public Kjoretoyordre getKjoretoyordre() {
		return kjoretoyordre;
	}

	public void setKjoretoyordre(Kjoretoyordre kjoretoyordre) {
		Object oldValue = this.kjoretoyordre;
        this.kjoretoyordre = kjoretoyordre;
        
        if(kjoretoyordre != null) {
        	setOrdreId(kjoretoyordre.getId());
        } else {
        	setOrdreId(null);
        }
        
        getProps().firePropertyChange("kjoretoyordre", oldValue, this.kjoretoyordre);
	}

	public KjoretoyUtstyr getKjoretoyUtstyr() {
		return kjoretoyUtstyr;
	}
	public KjoretoyordreUtstyr getThisForReport(){
    	return this;
    }
	public void setKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
		Object oldValue = this.kjoretoyUtstyr;
		this.kjoretoyUtstyr = kjoretoyUtstyr;
		
		if(kjoretoyUtstyr != null) {
        	setUtstyrId(kjoretoyUtstyr.getId());
        } else {
        	setUtstyrId(null);
        }
		
        getProps().firePropertyChange("kjoretoyUtstyr", oldValue, this.kjoretoyUtstyr);
	}

	public KjoretoyUtstyrIo getKjoretoyUtstyrIo() {
		return kjoretoyUtstyrIo;
	}

	public void setKjoretoyUtstyrIo(KjoretoyUtstyrIo kjoretoyUtstyrIo) {
		Object oldValue = this.kjoretoyUtstyrIo;
        this.kjoretoyUtstyrIo = kjoretoyUtstyrIo;
        
        if(kjoretoyUtstyrIo != null) {
        	setUtstyrIoId(kjoretoyUtstyrIo.getId());
        } else {
        	setUtstyrIoId(null);
        }
        
        getProps().firePropertyChange("kjoretoyUtstyrIo", oldValue, this.kjoretoyUtstyrIo);
	}

	
}
