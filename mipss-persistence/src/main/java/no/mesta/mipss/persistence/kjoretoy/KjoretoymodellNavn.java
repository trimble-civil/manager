/**
 * 
 */
package no.mesta.mipss.persistence.kjoretoy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

/**
 * Implementerer et navn som tilsvarer merke og modell i en Kjoretoymodell.
 * 
 * Brukes til � opprette en hiererkisk struktur hvor en instans av denne klassen har 
 * en eller fler instalser av klassen KjoretoymodellAar som i sin tur peker til sine 
 * Kjoretoymodell JPA entiteter.
 * 
 * @author jorge
 *
 */
@SuppressWarnings("serial")
public class KjoretoymodellNavn implements IRenderableMipssEntity {
	private String navn;
	private List<KjoretoymodellAar> aarList;
	
	/**
	 * Oppretter en ny KjoretoymodellNavn instans basert p� den leverte modellen.
	 * Instansen innehodler en �rliste med dens �r som f�rste objekt.
	 * @param modell
	 */
	public KjoretoymodellNavn(Kjoretoymodell modell) {
		aarList = new ArrayList<KjoretoymodellAar>();
		navn = modell.getTextForGUI();
		add(modell);
	}
	
	/**
	 * Legger til en ny modell til sin �rsliste.
	 * Det testes ikke om merke og modell er de samme.
	 * @param modell
	 */
	public final void add(Kjoretoymodell modell) {
		KjoretoymodellAar aar = new KjoretoymodellAar(modell);
		for(KjoretoymodellAar existing : aarList) {
			if(existing.equals(aar)) {
				return;
			}
		}
		aarList.add(aar);
	}

	/** {@inheritDoc }*/
	public String getTextForGUI() {
		return navn;
	}
	
	/**
	 * 
	 * @return TRUE hvis det kun finnes ett �r fro dette navnet 
	 */
	public boolean hasOneYearOnly() {
		return (aarList.size() == 1);
	}
	
	/**
	 * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
	 * 
	 * @return en tekstlinje.
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).
			append("navn", navn).
			append("�r", aarList).
			toString();
	}

	/**
	 * Sammenlikner med o.
	 * 
	 * @param o
	 * @return true hvis relevante n�klene er like.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (null == o) {
			return false;
		}
		if (!(o instanceof KjoretoymodellNavn)) {
			return false;
		}
		KjoretoymodellNavn other = (KjoretoymodellNavn) o;
		return new EqualsBuilder().
					append(getNavn(), other.getNavn()).
					isEquals();
	}

	/**
	 * Genererer identifikator hashkode.
	 * 
	 * @return koden.
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(23, 91).
				append(getNavn()).
				append(getAarList()).
				toHashCode();
	}

	public int compareTo(Object o) {
		KjoretoymodellNavn other = (KjoretoymodellNavn) o;
		return new CompareToBuilder().
				append(getNavn(), other.getNavn()).
				toComparison();
	}

	/**
	 * @return navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * @param navn the navn to set
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * @return aarList
	 */
	public List<KjoretoymodellAar> getAarList() {
		Collections.sort(aarList);
		return aarList;
	}

	/**
	 * @param aarList the aarList to set
	 */
	public void setAarList(List<KjoretoymodellAar> aarList) {
		this.aarList = aarList;
	}
}
