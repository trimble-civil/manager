package no.mesta.mipss.persistence.mipssfield;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
@SuppressWarnings("serial")
@Entity
@Table(name="PDABRUKER_KONTRAKT")
@IdClass(PdabrukerKontraktPK.class)

@NamedQueries ({
	@NamedQuery(name = PdabrukerKontrakt.FIND_ALL, query = "select o from PdabrukerKontrakt o"),
	@NamedQuery(name = PdabrukerKontrakt.QUERY_FIND_FOR_KONTRAKT, query = "SELECT o FROM PdabrukerKontrakt o where o.kontraktId = :id")
})

public class PdabrukerKontrakt extends MipssEntityBean<PdabrukerKontrakt>{
	public static final String FIND_ALL = "PdabrukerKontrakt.findAll";
	public static final String QUERY_FIND_FOR_KONTRAKT = "PdabrukerKontrakt.findAllForKontrakt";

	@Id
	@Column(nullable = false, insertable = false, updatable = false)
	private String signatur;
	@Id
	@Column(name="KONTRAKT_ID", insertable = false, updatable = false)
	private Long kontraktId;
	
	@ManyToOne
	@JoinColumn(name = "KONTRAKT_ID", referencedColumnName = "ID", nullable = false)
	private Driftkontrakt driftkontrakt;
	
	@ManyToOne (cascade={CascadeType.PERSIST})
	@JoinColumn(name = "SIGNATUR", referencedColumnName = "SIGNATUR", nullable = false)
	private Pdabruker pdabruker;
	
	@Column(name="INAKTIV_FLAGG")
	private Boolean inaktivFlagg;
	@Column(name="OPPRETTET_AV")
	private String opprettetAv;
	@Column(name="OPPRETTET_DATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date opprettetDato;
	/**
	 * @return the signatur
	 */
	public String getSignatur() {
		return signatur;
	}
	/**
	 * @param signatur the signatur to set
	 */
	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}
	/**
	 * @return the kontraktId
	 */
	public Long getKontraktId() {
		return kontraktId;
	}
	/**
	 * @param kontraktId the kontraktId to set
	 */
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	/**
	 * @return the driftkontrakt
	 */
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}
	/**
	 * @param driftkontrakt the driftkontrakt to set
	 */
	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
		if (driftkontrakt!=null){
			setKontraktId(driftkontrakt.getId());
		}else
			setKontraktId(null);
	}
	/**
	 * @return the inaktivFlagg
	 */
	public Boolean getInaktivFlagg() {
		return inaktivFlagg;
	}
	/**
	 * @param inaktivFlagg the inaktivFlagg to set
	 */
	public void setInaktivFlagg(Boolean inaktivFlagg) {
		this.inaktivFlagg = inaktivFlagg;
	}
	/**
	 * @return the opprettetAv
	 */
	public String getOpprettetAv() {
		return opprettetAv;
	}
	/**
	 * @param opprettetAv the opprettetAv to set
	 */
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	/**
	 * @return the opprettetDato
	 */
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	/**
	 * @param opprettetDato the opprettetDato to set
	 */
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	public void setPdabruker(Pdabruker pdabruker) {
		this.pdabruker = pdabruker;
		if (pdabruker!=null){
			setSignatur(pdabruker.getSignatur());
		}else
			setSignatur(null);
	}
	public Pdabruker getPdabruker() {
		return pdabruker;
	}
	
	
}
