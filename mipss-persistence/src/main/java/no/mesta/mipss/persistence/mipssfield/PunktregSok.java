package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Denne entiteten brukes mot en Oracle tabellfunksjon
 * (feltstudio_pk.punktreg_sok()). Denne kan kun kalles som native sql, og
 * dermed benytter vi oss av SqlResultSetMapping.
 * 
 * @author <a href="mailto:arnljot.arntsen@aventir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "dummy_eksisterer_ikke")
@SqlResultSetMappings( { @SqlResultSetMapping(name = "PunktregSokResult", entities = { @EntityResult(entityClass = PunktregSok.class, fields = {
		@FieldResult(name = PunktregSok.FIELD_REFNUMMER, column = PunktregSok.COLUMN_REFNUMMER),
		@FieldResult(name = PunktregSok.FIELD_PUNKTREGGUID, column = PunktregSok.COLUMN_PUNKTREG_GUID),
		@FieldResult(name = PunktregSok.FIELD_AARSAKID, column = PunktregSok.COLUMN_AARSAK_ID),
		@FieldResult(name = PunktregSok.FIELD_AARSAKNAVN, column = PunktregSok.COLUMN_AARSAK_NAVN),
		@FieldResult(name = PunktregSok.FIELD_ANTALLBILDER, column = PunktregSok.COLUMN_ANTALL_BILDER),
		@FieldResult(name = PunktregSok.FIELD_AVVIKUTENFORKONTRAKT, column = PunktregSok.COLUMN_AVVIK_UTENFOR_KONTRAKT_FLAGG),
		@FieldResult(name = PunktregSok.FIELD_BESKRIVESLSE, column = PunktregSok.COLUMN_BESKRIVELSE),
		@FieldResult(name = PunktregSok.FIELD_ETTERSLEPFLAFF, column = PunktregSok.COLUMN_ETTERSLEP_FLAGG),
		@FieldResult(name = PunktregSok.FIELD_FORSIKRINGSSKADE, column = PunktregSok.COLUMN_FORSIKRINGSSKADE_FLAGG),
		@FieldResult(name = PunktregSok.FIELD_FYLKESNUMMER, column = PunktregSok.COLUMN_FYLKESNUMMER),
		@FieldResult(name = PunktregSok.FIELD_HENDELSEGUID, column = PunktregSok.COLUMN_HENDELSE_GUID),
		@FieldResult(name = PunktregSok.FIELD_HP, column = PunktregSok.COLUMN_HP),
		@FieldResult(name = PunktregSok.FIELD_INSPEKSJONGUID, column = PunktregSok.COLUMN_INSPEKSJON_GUID),
		@FieldResult(name = PunktregSok.FIELD_KJOREFELT, column = PunktregSok.COLUMN_KJOREFELT),
		@FieldResult(name = PunktregSok.FIELD_KOMMUNENUMMER, column = PunktregSok.COLUMN_KOMMUNENUMMER),
		@FieldResult(name = PunktregSok.FIELD_KONTRAKTID, column = PunktregSok.COLUMN_KONTRAKT_ID),
		@FieldResult(name = PunktregSok.FIELD_LUKKETDATO, column = PunktregSok.COLUMN_LUKKET_DATO),
		@FieldResult(name = PunktregSok.FIELD_METER, column = PunktregSok.COLUMN_METER),
		@FieldResult(name = PunktregSok.FIELD_OPPRETTETAV, column = PunktregSok.COLUMN_OPPRETTET_AV),
		@FieldResult(name = PunktregSok.FIELD_OPPRETTETDATO, column = PunktregSok.COLUMN_OPPRETTET_DATO),
		@FieldResult(name = PunktregSok.FIELD_PDADFUIDENT, column = PunktregSok.COLUMN_PDA_DFU_IDENT),
		@FieldResult(name = PunktregSok.FIELD_PDADFUNAVN, column = PunktregSok.COLUMN_PDA_DFU_NAVN),
		@FieldResult(name = PunktregSok.FIELD_PROSESSID, column = PunktregSok.COLUMN_PROSESS_ID),
		@FieldResult(name = PunktregSok.FIELD_PROSESSKODE, column = PunktregSok.COLUMN_PROSESS_KODE),
		@FieldResult(name = PunktregSok.FIELD_PROSESSNAVN, column = PunktregSok.COLUMN_PROSESS_NAVN),
		@FieldResult(name = PunktregSok.FIELD_SISTESTATUS, column = PunktregSok.COLUMN_SISTE_STATUS),
		@FieldResult(name = PunktregSok.FIELD_SISTESTATUSDATO, column = PunktregSok.COLUMN_SISTE_STATUS_DATO),
		@FieldResult(name = PunktregSok.FIELD_SISTESTATUSNAVN, column = PunktregSok.COLUMN_SISTE_STATUS_NAVN),
		@FieldResult(name = PunktregSok.FIELD_SLETTETAV, column = PunktregSok.COLUMN_SLETTET_AV),
		@FieldResult(name = PunktregSok.FIELD_SLETTETDATO, column = PunktregSok.COLUMN_SLETTET_DATO),
		@FieldResult(name = PunktregSok.FIELD_TILSTANDTYPEID, column = PunktregSok.COLUMN_TILSTANDTYPE_ID),
		@FieldResult(name = PunktregSok.FIELD_TILSTANDTYPENAVN, column = PunktregSok.COLUMN_TILSTANDTYPE_NAVN),
		@FieldResult(name = PunktregSok.FIELD_TILTAKSDATO, column = PunktregSok.COLUMN_TILTAKS_DATO),
		@FieldResult(name = PunktregSok.FIELD_TRAFIKKSIKKERHET, column = PunktregSok.COLUMN_TRAFIKKSIKKERHET_FLAGG),
		@FieldResult(name = PunktregSok.FIELD_UNDERPROSESSID, column = PunktregSok.COLUMN_UNDERPROSESS_ID),
		@FieldResult(name = PunktregSok.FIELD_UNDERPROSESSKODE, column = PunktregSok.COLUMN_UNDERPROSESS_KODE),
		@FieldResult(name = PunktregSok.FIELD_UNDERPROSESSNAVN, column = PunktregSok.COLUMN_UNDERPROSESS_NAVN),
		@FieldResult(name = PunktregSok.FIELD_VEIKATEGORI, column = PunktregSok.COLUMN_VEIKATEGORI),
		@FieldResult(name = PunktregSok.FIELD_VEINUMMER, column = PunktregSok.COLUMN_VEINUMMER),
		@FieldResult(name = PunktregSok.FIELD_VEISTATUS, column = PunktregSok.COLUMN_VEISTATUS),
		@FieldResult(name = PunktregSok.FIELD_X, column = PunktregSok.COLUMN_X),
		@FieldResult(name = PunktregSok.FIELD_Y, column = PunktregSok.COLUMN_Y) }) }) })
public class PunktregSok extends MipssEntityBean<PunktregSok> implements IRenderableMipssEntity, Serializable,
		Comparable<PunktregSok>, MipssFieldDetailItem {
	private static final String BLANK = "";
	public static final String COLUMN_AARSAK_ID = "AARSAK_ID";
	public static final String COLUMN_AARSAK_NAVN = "AARSAK_NAVN";
	public static final String COLUMN_ANTALL_BILDER = "ANTALL_BILDER";
	public static final String COLUMN_AVVIK_UTENFOR_KONTRAKT_FLAGG = "AVVIK_UTENFOR_KONTRAKT_FLAGG";
	public static final String COLUMN_BESKRIVELSE = "BESKRIVELSE";
	public static final String COLUMN_ESTIMAT = "ESTIMAT_STRENG";
	public static final String COLUMN_ETTERSLEP_FLAGG = "ETTERSLEP_FLAGG";
	public static final String COLUMN_FORSIKRINGSSKADE_FLAGG = "FORSIKRINGSSKADE_FLAGG";
	public static final String COLUMN_FYLKESNUMMER = "FYLKESNUMMER";
	public static final String COLUMN_HENDELSE_GUID = "HENDELSE_GUID";
	public static final String COLUMN_HISTORIKK = "HISTORIKK_STRENG";
	public static final String COLUMN_HP = "HP";
	public static final String COLUMN_INSPEKSJON_GUID = "INSPEKSJON_GUID";
	public static final String COLUMN_KJOREFELT = "KJOREFELT";
	public static final String COLUMN_KOMMUNENUMMER = "KOMMUNENUMMER";
	public static final String COLUMN_KONTRAKT_ID = "KONTRAKT_ID";
	public static final String COLUMN_LUKKET_DATO = "LUKKET_DATO";
	public static final String COLUMN_METER = "METER";
	public static final String COLUMN_OPPRETTET_AV = "OPPRETTET_AV";
	public static final String COLUMN_OPPRETTET_DATO = "OPPRETTET_DATO";
	public static final String COLUMN_PDA_DFU_IDENT = "PDA_DFU_IDENT";
	public static final String COLUMN_PDA_DFU_NAVN = "PDA_DFU_NAVN";
	public static final String COLUMN_PROSESS_ID = "PROSESS_ID";
	public static final String COLUMN_PROSESS_KODE = "PROSESS_KODE";
	public static final String COLUMN_PROSESS_NAVN = "PROSESS_NAVN";
	public static final String COLUMN_PUNKTREG_GUID = "PUNKTREG_GUID";
	public static final String COLUMN_REFNUMMER = "REFNUMMER";
	public static final String COLUMN_SISTE_STATUS = "SISTE_STATUS";
	public static final String COLUMN_SISTE_STATUS_DATO = "SISTE_STATUS_DATO";
	public static final String COLUMN_SISTE_STATUS_NAVN = "SISTE_STATUS_NAVN";
	public static final String COLUMN_SLETTET_AV = "SLETTET_AV";
	public static final String COLUMN_SLETTET_DATO = "SLETTET_DATO";
	public static final String COLUMN_TILSTANDTYPE_ID = "TILSTANDTYPE_ID";
	public static final String COLUMN_TILSTANDTYPE_NAVN = "TILSTANDTYPE_NAVN";
	public static final String COLUMN_TILTAKS_DATO = "TILTAKS_DATO";
	public static final String COLUMN_TRAFIKKSIKKERHET_FLAGG = "TRAFIKKSIKKERHET_FLAGG";
	public static final String COLUMN_UNDERPROSESS_ID = "UNDERPROSESS_ID";
	public static final String COLUMN_UNDERPROSESS_KODE = "UNDERPROSESS_KODE";
	public static final String COLUMN_UNDERPROSESS_NAVN = "UNDERPROSESS_NAVN";
	public static final String COLUMN_VEIKATEGORI = "VEIKATEGORI";
	public static final String COLUMN_VEINUMMER = "VEINUMMER";
	public static final String COLUMN_VEISTATUS = "VEISTATUS";
	public static final String COLUMN_X = "X";
	public static final String COLUMN_Y = "Y";
	
	public static final String FIELD_AARSAKID = "aarsakId";
	public static final String FIELD_AARSAKNAVN = "aarsakNavn";
	public static final String FIELD_ANTALLBILDER = "antallBilder";
	public static final String FIELD_AVVIKUTENFORKONTRAKT = "avvikUtenforKontraktFlagg";
	public static final String FIELD_BESKRIVESLSE = "beskrivelse";
	public static final String FIELD_ESTIMAT = "estimat";
	public static final String FIELD_ETTERSLEPFLAFF = "etterslepFlagg";
	public static final String FIELD_FORSIKRINGSSKADE = "forsikringsskadeFlagg";
	public static final String FIELD_FYLKESNUMMER = "fylkesnummer";
	public static final String FIELD_HARHENDELSE = "harHendelse";
	public static final String FIELD_HARINSPEKSJON = "harInspeksjon";
	public static final String FIELD_HENDELSEGUID = "hendelseGuid";
	public static final String FIELD_HISTORIKK = "historikk";
	public static final String FIELD_HP = "hp";
	public static final String FIELD_INSPEKSJONGUID = "inspeksjonGuid";
	public static final String FIELD_KJOREFELT = "kjorefelt";
	public static final String FIELD_KOMMUNENUMMER = "kommunenummer";
	public static final String FIELD_KONTRAKTID = "kontraktId";
	public static final String FIELD_LUKKETDATO = "lukketDato";
	public static final String FIELD_METER = "meter";
	public static final String FIELD_OPPRETTETAV = "opprettetAv";
	public static final String FIELD_OPPRETTETDATO = "opprettetDato";
	public static final String FIELD_PDADFUIDENT = "pdaDfuIdent";
	public static final String FIELD_PDADFUNAVN = "pdaDfuNavn";
	public static final String FIELD_PROSESSID = "prosessId";
	public static final String FIELD_PROSESSKODE = "prosessKode";
	public static final String FIELD_PROSESSNAVN = "prosessNavn";
	public static final String FIELD_UNDERPROSESSID = "underprosessId";
	public static final String FIELD_UNDERPROSESSKODE = "underprosessKode";
	public static final String FIELD_UNDERPROSESSNAVN = "underprosessNavn";
	public static final String FIELD_PROSESSTXT = "prosessTxt";
	public static final String FIELD_PUNKTREGGUID = "punktregGuid";
	public static final String FIELD_REFNUMMER = "refnummer";
	public static final String FIELD_SISTESTATUS = "sisteStatus";
	public static final String FIELD_SISTESTATUSDATO = "sisteStatusDato";
	public static final String FIELD_SISTESTATUSNAVN = "sisteStatusNavn";
	public static final String FIELD_SLETTETAV = "slettetAv";
	public static final String FIELD_SLETTETDATO = "slettetDato";
	public static final String FIELD_TILSTANDTYPEID = "tilstandtypeId";
	public static final String FIELD_TILSTANDTYPENAVN = "tilstandtypeNavn";
	public static final String FIELD_TILTAKSDATO = "tiltaksDato";
	public static final String FIELD_TRAFIKKSIKKERHET = "trafikksikkerhetFlagg";
	public static final String FIELD_VEI = "vei";
	public static final String FIELD_VEIKATEGORI = "veikategori";
	public static final String FIELD_VEINUMMER = "veinummer";
	public static final String FIELD_VEISTATUS = "veistatus";
	public static final String FIELD_X = "x";
	public static final String FIELD_Y = "y";
	
	private static final String[] ID_NAMES = { FIELD_PUNKTREGGUID };
	public String getGuid(){
		return getPunktregGuid();
	}
	public static PunktregSok newInstance(Avvik punkt) {
		if (punkt != null) {
			PunktregSok view = new PunktregSok();
			view.setPunktregGuid(punkt.getGuid());
			view.setOpprettetAv(punkt.getOpprettetAv());
			view.setOpprettetDato(punkt.getOpprettetDato());
			
			view.setAntallBilder(punkt.getAntallBilder());
			
			view.setBeskrivelse(punkt.getBeskrivelse());
			view.setEtterslepFlagg(punkt.getEtterslepFlagg()?1L:0L);
			final Long forsikringsskade = 0L;
			view.setForsikringsskadeFlagg(forsikringsskade);
			if (punkt.getVeiref()!=null){
				view.setFylkesnummer(punkt.getVeiref().getFylkesnummer());
				view.setHp(punkt.getVeiref().getHp());
				view.setKjorefelt(punkt.getVeiref().getKjorefelt());
				view.setKommunenummer(punkt.getVeiref().getKommunenummer());
				view.setMeter(punkt.getVeiref().getMeter());
				view.setVeikategori(punkt.getVeiref().getVeikategori());
				view.setVeinummer(punkt.getVeiref().getVeinummer());
				view.setVeistatus(punkt.getVeiref().getVeistatus());
			}
			
			view.setLukketDato(punkt.getLukketDato());
			// view.setPdaDfuNavn(pdaDfuNavn); // For langt ut i objektgrafen
			view.setProsessId(punkt.getProsessId());
			view.setUnderprosessId(punkt.getProsessId());
			if (punkt.getProsess()!=null){
				view.setProsessKode(punkt.getProsess().getProsessKode());
				view.setProsessNavn(punkt.getProsess().getNavn());
			}
			
			if (punkt.getStatus()!=null){
				view.setSisteStatus(punkt.getStatus().getAvvikstatusType().getStatus());
				view.setSisteStatusDato(punkt.getStatus().getOpprettetDato());
				view.setSisteStatusNavn(punkt.getStatus().getAvvikstatusType().getStatus());
			}
			view.setSlettetAv(punkt.getSlettetAv());
			view.setSlettetDato(punkt.getSlettetDato());
			view.setTilstandtypeId(punkt.getTilstandId());
			view.setTilstandtypeNavn(punkt.getTilstand() != null ? punkt.getTilstand().getNavn() : null);
			view.setTiltaksDato(punkt.getTiltaksDato());
			view.setTrafikksikkerhetFlagg(punkt.getTrafikksikkerhetFlagg()?1L:0L);			
			
			final Double x;
			if (punkt.getStedfesting() != null) {
				x = punkt.getStedfesting().getSnappetX() != null ? punkt.getStedfesting().getSnappetX() : punkt
						.getStedfesting().getX();
			} else {
				x = null;
			}
			view.setX(x);
			final Double y;
			if (punkt.getStedfesting() != null) {
				y = punkt.getStedfesting().getSnappetY() != null ? punkt.getStedfesting().getSnappetY() : punkt
						.getStedfesting().getY();
			} else {
				y = null;
			}
			view.setY(y);

			return view;
		} else {
			return null;
		}
	}
	private Long aarsakId;
	private String aarsakNavn;
	private Integer antallBilder;
	private Long avvikUtenforKontraktFlagg;
	private String beskrivelse;
	private String estimat;
	private Long etterslepFlagg;
	private Long forsikringsskadeFlagg;
	private int fylkesnummer;
	private String hendelseGuid;
	private String historikk;
	private int hp;
	private String inspeksjonGuid;
	private String kjorefelt;
	private int kommunenummer;
	private Long kontraktId;
	private Date lukketDato;
	private int meter;
	private String opprettetAv;
	private Date opprettetDato;
	private Long pdaDfuIdent;
	private String pdaDfuNavn;
	private Long prosessId;
	private String prosessKode;
	private String prosessNavn;
	private String punktregGuid;
	private Long refnummer;
	private String sisteStatus;
	private Date sisteStatusDato;
	private String sisteStatusNavn;
	private String slettetAv;
	private Date slettetDato;
	private Long tilstandtypeId;
	private String tilstandtypeNavn;
	private Date tiltaksDato;
	private Long trafikksikkerhetFlagg;
	private Long underprosessId;
	private String underprosessKode;
	private String underprosessNavn;

	private String veikategori;
	private int veinummer;
	private String veistatus;
	
	private Double x;
	
	private Double y;

	public PunktregSok() {
		log("PunktregSok() opprettet");
	}
	
	/** {@inheritDoc} */
	public int compareTo(PunktregSok o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(opprettetDato, o.opprettetDato).append(opprettetAv, o.opprettetAv)
				.toComparison();
	}

	@Column(name = COLUMN_AARSAK_ID)
	public Long getAarsakId() {
		return aarsakId;
	}

	@Column(name = COLUMN_AARSAK_NAVN)
	public String getAarsakNavn() {
		return aarsakNavn;
	}

	@Column(name = COLUMN_ANTALL_BILDER)
	public Integer getAntallBilder() {
		return antallBilder;
	}

	@Column(name = COLUMN_AVVIK_UTENFOR_KONTRAKT_FLAGG)
	public Long getAvvikUtenforKontraktFlagg() {
		return avvikUtenforKontraktFlagg;
	}

	@Column(name = COLUMN_BESKRIVELSE, length = 1000)
	public String getBeskrivelse() {
		return beskrivelse;
	}

	@Column(name = COLUMN_ESTIMAT)
	public String getEstimat(){
		return estimat;
	}

	@Transient
	public Boolean getEtterslep() {
		return etterslepFlagg != null && etterslepFlagg.equals(1L) ? Boolean.TRUE : Boolean.FALSE;
	}

	@Column(name = COLUMN_ETTERSLEP_FLAGG)
	public Long getEtterslepFlagg() {
		return etterslepFlagg;
	}

	@Column(name = COLUMN_FORSIKRINGSSKADE_FLAGG)
	public Long getForsikringsskadeFlagg() {
		return forsikringsskadeFlagg;
	}

	@Column(name = COLUMN_FYLKESNUMMER)
	public int getFylkesnummer() {
		return fylkesnummer;
	}

	@Transient
	public Boolean getHarHendelse() {
		return hendelseGuid != null;
	}

	@Transient
	public Boolean getHarInspeksjon() {
		return inspeksjonGuid != null;
	}

	@Column(name = COLUMN_HENDELSE_GUID)
	public String getHendelseGuid() {
		return hendelseGuid;
	}

	@Column(name = COLUMN_HISTORIKK)
	public String getHistorikk(){
		return historikk;
	}

	@Column(name = COLUMN_HP)
	public int getHp() {
		return hp;
	}

	/** {@inheritDoc} */
	@Override
	@Transient
	public String[] getIdNames() {
		return ID_NAMES;
	}

	@Column(name = COLUMN_INSPEKSJON_GUID)
	public String getInspeksjonGuid() {
		return inspeksjonGuid;
	}

	@Column(name = COLUMN_KJOREFELT)
	public String getKjorefelt() {
		return kjorefelt;
	}

	@Column(name = COLUMN_KOMMUNENUMMER)
	public int getKommunenummer() {
		return kommunenummer;
	}

	@Column(name = COLUMN_KONTRAKT_ID)
	public Long getKontraktId() {
		return kontraktId;
	}

	@Transient
	public Boolean getLukket() {
		return lukketDato == null ? Boolean.FALSE : Boolean.TRUE;
	}

	@Column(name = COLUMN_LUKKET_DATO)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLukketDato() {
		return lukketDato;
	}

	@Column(name = COLUMN_METER)
	public int getMeter() {
		return meter;
	}

	@Column(name = COLUMN_OPPRETTET_AV)
	public String getOpprettetAv() {
		return opprettetAv;
	}

	@Column(name = COLUMN_OPPRETTET_DATO)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOpprettetDato() {
		return opprettetDato;
	}

	@Column(name = COLUMN_PDA_DFU_IDENT)
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}

	@Column(name = COLUMN_PDA_DFU_NAVN)
	public String getPdaDfuNavn() {
		return pdaDfuNavn;
	}

	@Column(name = COLUMN_PROSESS_ID)
	public Long getProsessId() {
		return prosessId;
	}

	@Column(name = COLUMN_PROSESS_KODE)
	public String getProsessKode() {
		return prosessKode;
	}

	@Column(name = COLUMN_PROSESS_NAVN)
	public String getProsessNavn() {
		return prosessNavn;
	}
	
	@Column(name = COLUMN_UNDERPROSESS_ID)
	public Long getUnderprosessId() {
		return underprosessId;
	}

	@Column(name = COLUMN_UNDERPROSESS_KODE)
	public String getUnderprosessKode() {
		return prosessKode;
	}

	@Column(name = COLUMN_UNDERPROSESS_NAVN)
	public String getUnderprosessNavn() {
		return prosessNavn;
	}

	@Transient
	public String getProsessTxt() {
		return (underprosessId!=null)?underprosessKode+" "+underprosessNavn+" ("+prosessKode + " " + prosessNavn+")":prosessKode+" "+prosessNavn;
	}

	@Id
	@Column(name = COLUMN_PUNKTREG_GUID)
	public String getPunktregGuid() {
		return punktregGuid;
	}

	@Column(name = COLUMN_REFNUMMER)
	public Long getRefnummer(){
		return refnummer;
	}

	@Column(name = COLUMN_SISTE_STATUS)
	public String getSisteStatus() {
		return sisteStatus;
	}

	@Column(name = COLUMN_SISTE_STATUS_DATO)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSisteStatusDato() {
		return sisteStatusDato;
	}

	@Column(name = COLUMN_SISTE_STATUS_NAVN)
	public String getSisteStatusNavn() {
		return sisteStatusNavn;
	}

	@Column(name = COLUMN_SLETTET_AV)
	public String getSlettetAv() {
		return slettetAv;
	}

	@Column(name = COLUMN_SLETTET_DATO)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSlettetDato() {
		return slettetDato;
	}

	/** {@inheritDoc} */
	public String getTextForGUI() {
		return MipssDateFormatter.formatDate(opprettetDato, MipssDateFormatter.SHORT_DATE_TIME_FORMAT) + " av "
				+ opprettetAv;
	}

	@Transient
	public PunktregSok getThisForReport() {
		return this;
	}

	@Column(name = COLUMN_TILSTANDTYPE_ID)
	public Long getTilstandtypeId() {
		return tilstandtypeId;
	}

	@Column(name = COLUMN_TILSTANDTYPE_NAVN)
	public String getTilstandtypeNavn() {
		return tilstandtypeNavn;
	}

	@Column(name = COLUMN_TILTAKS_DATO)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTiltaksDato() {
		return tiltaksDato;
	}

	@Column(name = COLUMN_TRAFIKKSIKKERHET_FLAGG)
	public Long getTrafikksikkerhetFlagg() {
		return trafikksikkerhetFlagg;
	}

	@Transient
	public String getVei() {
		return (!StringUtils.isBlank(veikategori) ? veikategori : BLANK)
				+ (!StringUtils.isBlank(veistatus) ? veistatus : BLANK);
	}

	@Transient
	public String getVeiGUITxt() {
		return VeiInfoUtil.buildVeiInfo(this).toString();
	}

	@Column(name = COLUMN_VEIKATEGORI)
	public String getVeikategori() {
		return veikategori;
	}

	@Column(name = COLUMN_VEINUMMER)
	public int getVeinummer() {
		return veinummer;
	}
	@Column(name = COLUMN_VEISTATUS)
	public String getVeistatus() {
		return veistatus;
	}
	@Column(name = COLUMN_X)
	public Double getX() {
		return x;
	}
	@Column(name = COLUMN_Y)
	public Double getY() {
		return y;
	}

	public void setAarsakId(Long aarsakId) {
		Long oldValue = this.aarsakId;
		this.aarsakId = aarsakId;
		firePropertyChange(FIELD_AARSAKID, oldValue, aarsakId);
	}

	public void setAarsakNavn(String aarsakNavn) {
		String oldValue = this.aarsakNavn;
		this.aarsakNavn = aarsakNavn;
		firePropertyChange(FIELD_AARSAKNAVN, oldValue, aarsakNavn);
	}

	public void setAntallBilder(Integer antallBilder) {
		Integer oldValue = this.antallBilder;
		this.antallBilder = antallBilder;
		firePropertyChange(FIELD_ANTALLBILDER, oldValue, antallBilder);
	}

	public void setAvvikUtenforKontraktFlagg(Long avvikUtenforKontraktFlagg) {
		Long oldValue = this.avvikUtenforKontraktFlagg;
		this.avvikUtenforKontraktFlagg = avvikUtenforKontraktFlagg;
		firePropertyChange(FIELD_AVVIKUTENFORKONTRAKT, oldValue, avvikUtenforKontraktFlagg);
	}

	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		firePropertyChange(FIELD_BESKRIVESLSE, old, beskrivelse);
	}

	public void setEstimat(String estimat){
		String old = this.estimat;
		this.estimat = estimat;
		firePropertyChange(FIELD_ESTIMAT, old, estimat);
	}

	public void setEtterslepFlagg(Long etterslepFlagg) {
		Long oldValue = this.etterslepFlagg;
		this.etterslepFlagg = etterslepFlagg;
		firePropertyChange(FIELD_ETTERSLEPFLAFF, oldValue, etterslepFlagg);
	}

	public void setForsikringsskadeFlagg(Long forsikringsskadeFlagg) {
		Long oldValue = this.forsikringsskadeFlagg;
		this.forsikringsskadeFlagg = forsikringsskadeFlagg;
		firePropertyChange(FIELD_FORSIKRINGSSKADE, oldValue, forsikringsskadeFlagg);
	}

	public void setFylkesnummer(int fylkesnummer) {
		int oldValue = this.fylkesnummer;
		this.fylkesnummer = fylkesnummer;
		firePropertyChange(FIELD_FYLKESNUMMER, oldValue, fylkesnummer);
	}

	public void setHendelseGuid(String hendelseGuid) {
		String oldValue = this.hendelseGuid;
		this.hendelseGuid = hendelseGuid;
		firePropertyChange(FIELD_HENDELSEGUID, oldValue, hendelseGuid);
		firePropertyChange(FIELD_HARHENDELSE, oldValue != null, hendelseGuid != null);
	}

	public void setHistorikk(String historikk){
		String old = this.historikk;
		this.historikk = historikk;
		firePropertyChange(FIELD_HISTORIKK, old, historikk);
	}

	public void setHp(int hp) {
		int oldValue = this.hp;
		this.hp = hp;
		firePropertyChange(FIELD_HP, oldValue, hp);
	}

	public void setInspeksjonGuid(String inspeksjonGuid) {
		String oldValue = this.inspeksjonGuid;
		this.inspeksjonGuid = inspeksjonGuid;
		firePropertyChange(FIELD_INSPEKSJONGUID, oldValue, inspeksjonGuid);
		firePropertyChange(FIELD_HARINSPEKSJON, oldValue != null, inspeksjonGuid != null);
	}

	public void setKjorefelt(String kjorefelt) {
		String oldValue = this.kjorefelt;
		this.kjorefelt = kjorefelt;
		firePropertyChange(FIELD_KJOREFELT, oldValue, kjorefelt);
	}

	public void setKommunenummer(int kommunenummer) {
		int oldValue = this.kommunenummer;
		this.kommunenummer = kommunenummer;
		firePropertyChange(FIELD_KOMMUNENUMMER, oldValue, kommunenummer);
	}

	public void setKontraktId(Long kontraktId) {
		Long oldValue = this.kontraktId;
		this.kontraktId = kontraktId;
		firePropertyChange(FIELD_KONTRAKTID, oldValue, kontraktId);
	}

	public void setLukketDato(Date lukketDato) {
		Date oldValue = this.lukketDato;
		this.lukketDato = lukketDato;
		firePropertyChange(FIELD_LUKKETDATO, oldValue, lukketDato);
	}

	public void setMeter(int meter) {
		int oldValue = this.meter;
		this.meter = meter;
		firePropertyChange(FIELD_METER, oldValue, meter);
	}

	public void setOpprettetAv(String opprettetAv) {
		String oldValue = this.opprettetAv;
		this.opprettetAv = opprettetAv;
		firePropertyChange(FIELD_OPPRETTETAV, oldValue, opprettetAv);
	}

	public void setOpprettetDato(Date opprettetDato) {
		Date oldValue = this.opprettetDato;
		this.opprettetDato = opprettetDato;
		firePropertyChange(FIELD_OPPRETTETDATO, oldValue, opprettetDato);
	}

	public void setPdaDfuIdent(Long pdaDfuIdent) {
		Long oldValue = this.pdaDfuIdent;
		this.pdaDfuIdent = pdaDfuIdent;
		firePropertyChange(FIELD_PDADFUIDENT, oldValue, pdaDfuIdent);
	}

	public void setPdaDfuNavn(String pdaDfuNavn) {
		String oldValue = this.pdaDfuNavn;
		this.pdaDfuNavn = pdaDfuNavn;
		firePropertyChange(FIELD_PDADFUNAVN, oldValue, pdaDfuNavn);
	}

	public void setProsessId(Long prosessId) {
		Long oldValue = this.prosessId;
		this.prosessId = prosessId;
		firePropertyChange(FIELD_PROSESSID, oldValue, prosessId);
	}

	public void setProsessKode(String prosessKode) {
		String old = this.prosessKode;
		this.prosessKode = prosessKode;
		firePropertyChange(FIELD_PROSESSKODE, old, prosessKode);
		firePropertyChange(FIELD_PROSESSTXT, old + " " + prosessNavn, prosessKode + " " + prosessNavn);
	}

	public void setProsessNavn(String prosessNavn) {
		String old = this.prosessNavn;
		this.prosessNavn = prosessNavn;
		firePropertyChange(FIELD_PROSESSNAVN, old, prosessNavn);
		firePropertyChange(FIELD_PROSESSTXT, prosessKode + " " + old, prosessKode + " " + prosessNavn);
	}
	
	public void setUnderprosessId(Long underprosessId) {
		Long oldValue = this.underprosessId;
		this.underprosessId = underprosessId;
		firePropertyChange(FIELD_UNDERPROSESSID, oldValue, underprosessId);
	}

	public void setUnderprosessKode(String underprosessKode) {
		String old = this.underprosessKode;
		this.underprosessKode = underprosessKode;
		firePropertyChange(FIELD_UNDERPROSESSKODE, old, underprosessKode);
		firePropertyChange(FIELD_PROSESSTXT, old + " " + underprosessNavn, underprosessKode + " " + underprosessNavn);
	}

	public void setUnderprosessNavn(String underprosessNavn) {
		String old = this.underprosessNavn;
		this.underprosessNavn = underprosessNavn;
		firePropertyChange(FIELD_UNDERPROSESSNAVN, old, underprosessNavn);
		firePropertyChange(FIELD_PROSESSTXT, underprosessKode + " " + old, underprosessKode + " " + underprosessNavn);
	}

	public void setPunktregGuid(String punktregGuid) {
		String oldValue = this.punktregGuid;
		this.punktregGuid = punktregGuid;
		firePropertyChange(FIELD_PUNKTREGGUID, oldValue, punktregGuid);
	}

	public void setRefnummer(Long refnummer){
		Long old = this.refnummer;
		this.refnummer = refnummer;
		firePropertyChange("refnummer", old, refnummer);
	}

	public void setSisteStatus(String sisteStatus) {
		String oldValue = this.sisteStatus;
		this.sisteStatus = sisteStatus;
		firePropertyChange(FIELD_SISTESTATUS, oldValue, sisteStatus);
	}

	public void setSisteStatusDato(Date sisteStatusDato) {
		Date oldValue = this.sisteStatusDato;
		this.sisteStatusDato = sisteStatusDato;
		firePropertyChange(FIELD_SISTESTATUSDATO, oldValue, sisteStatusDato);
	}

	public void setSisteStatusNavn(String sisteStatusNavn) {
		String oldValue = this.sisteStatusNavn;
		this.sisteStatusNavn = sisteStatusNavn;
		firePropertyChange(FIELD_SISTESTATUSNAVN, oldValue, sisteStatusNavn);
	}

	public void setSlettetAv(String slettetAv) {
		String oldValue = this.slettetAv;
		this.slettetAv = slettetAv;
		firePropertyChange(FIELD_SLETTETAV, oldValue, slettetAv);
	}

	public void setSlettetDato(Date slettetDato) {
		Date oldValue = this.slettetDato;
		this.slettetDato = slettetDato;
		firePropertyChange(FIELD_SLETTETDATO, oldValue, slettetDato);
	}

	public void setTilstandtypeId(Long tilstandtypeId) {
		Long oldValue = this.tilstandtypeId;
		this.tilstandtypeId = tilstandtypeId;
		firePropertyChange(FIELD_TILSTANDTYPEID, oldValue, tilstandtypeId);
	}

	public void setTilstandtypeNavn(String tilstandtypeNavn) {
		String oldValue = this.tilstandtypeNavn;
		this.tilstandtypeNavn = tilstandtypeNavn;
		firePropertyChange(FIELD_TILSTANDTYPENAVN, oldValue, tilstandtypeNavn);
	}

	public void setTiltaksDato(Date tiltaksDato) {
		Date oldValue = this.tiltaksDato;
		this.tiltaksDato = tiltaksDato;
		firePropertyChange(FIELD_TILTAKSDATO, oldValue, tiltaksDato);
	}

	public void setTrafikksikkerhetFlagg(Long trafikksikkerhetFlagg) {
		Long oldValue = this.trafikksikkerhetFlagg;
		this.trafikksikkerhetFlagg = trafikksikkerhetFlagg;
		firePropertyChange(FIELD_TRAFIKKSIKKERHET, oldValue, trafikksikkerhetFlagg);
	}


	public void setVeikategori(String veikategori) {
		String oldValue = this.veikategori;
		this.veikategori = veikategori;
		firePropertyChange(FIELD_VEIKATEGORI, oldValue, veikategori);
		firePropertyChange(FIELD_VEI, oldValue + veistatus, veikategori + veistatus);
	}

	public void setVeinummer(int veinummer) {
		int oldValue = this.veinummer;
		this.veinummer = veinummer;
		firePropertyChange(FIELD_VEINUMMER, oldValue, veinummer);
	}
	
	public void setVeistatus(String veistatus) {
		String oldValue = this.veistatus;
		this.veistatus = veistatus;
		firePropertyChange(FIELD_VEISTATUS, oldValue, veistatus);
		firePropertyChange(FIELD_VEI, veikategori + oldValue, veikategori + veistatus);
	}
	
	public void setX(Double x) {
		Double oldValue = this.x;
		this.x = x;
		firePropertyChange(FIELD_X, oldValue, x);
	}
	

	public void setY(Double y) {
		Double oldValue = this.y;
		this.y = y;
		firePropertyChange(FIELD_Y, oldValue, y);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append(FIELD_REFNUMMER, refnummer).append(FIELD_PUNKTREGGUID, punktregGuid).append(FIELD_AARSAKID, aarsakId)
				.append(FIELD_AARSAKNAVN, aarsakNavn).append(FIELD_ANTALLBILDER, antallBilder).append(
						FIELD_AVVIKUTENFORKONTRAKT, avvikUtenforKontraktFlagg).append(FIELD_BESKRIVESLSE, beskrivelse)
				.append(FIELD_ETTERSLEPFLAFF, etterslepFlagg).append(FIELD_FORSIKRINGSSKADE, forsikringsskadeFlagg)
				.append(FIELD_FYLKESNUMMER, fylkesnummer).append(FIELD_HARHENDELSE, getHarHendelse()).append(
						FIELD_HARINSPEKSJON, getHarInspeksjon()).append(FIELD_HENDELSEGUID, hendelseGuid).append(
						FIELD_HP, hp).append(FIELD_INSPEKSJONGUID, inspeksjonGuid).append(FIELD_KJOREFELT, kjorefelt)
				.append(FIELD_KOMMUNENUMMER, kommunenummer).append(FIELD_KONTRAKTID, kontraktId).append(
						FIELD_LUKKETDATO, lukketDato).append(FIELD_METER, meter).append(FIELD_OPPRETTETAV, opprettetAv)
				.append(FIELD_OPPRETTETDATO, opprettetDato).append(FIELD_PDADFUIDENT, pdaDfuIdent).append(
						FIELD_PDADFUNAVN, pdaDfuNavn)
						.append(FIELD_PROSESSID, prosessId).append(FIELD_PROSESSKODE, prosessKode).append(FIELD_PROSESSNAVN, prosessNavn)
						.append(FIELD_UNDERPROSESSID, underprosessId).append(FIELD_UNDERPROSESSKODE, underprosessKode).append(FIELD_UNDERPROSESSNAVN, underprosessNavn)
						.append(FIELD_PROSESSTXT, getProsessTxt())
				.append(FIELD_SISTESTATUS, sisteStatus).append(FIELD_SISTESTATUSDATO, sisteStatusDato).append(
						FIELD_SISTESTATUSNAVN, sisteStatusNavn).append(FIELD_SLETTETAV, slettetAv).append(
						FIELD_SLETTETDATO, slettetDato).append(FIELD_TILSTANDTYPEID, tilstandtypeId).append(
						FIELD_TILSTANDTYPENAVN, tilstandtypeNavn).append(FIELD_TILTAKSDATO, tiltaksDato).append(
						FIELD_TRAFIKKSIKKERHET, trafikksikkerhetFlagg)
				.append(FIELD_VEIKATEGORI, veikategori).append(FIELD_VEINUMMER, veinummer).append(FIELD_VEISTATUS,
						veistatus).append(FIELD_X, x).append(FIELD_Y, y).toString();
	}
}
