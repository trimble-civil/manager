package no.mesta.mipss.persistence.mipssfield;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * �rsaktypen for en hendelse
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries( {
        @NamedQuery(name = Hendelseaarsak.QUERY_FIND_ALL, query = "SELECT h FROM Hendelseaarsak h"),
        @NamedQuery(name = Hendelseaarsak.QUERY_FIND_VALID, query = "SELECT h FROM Hendelseaarsak h where h.valgbar = true") })
@SequenceGenerator(name = Hendelseaarsak.ID_SEQ, sequenceName = "HENDELSEAARSAK_ID_SEQ", initialValue = 1, allocationSize = 1)
public class Hendelseaarsak extends MipssEntityBean<Hendelseaarsak> implements Serializable, Comparable<Hendelseaarsak>, IRenderableMipssEntity {
    public static final String ID_SEQ = "hendelseaarsakIdSeq";
	public static final String QUERY_FIND_ALL = "Hendelseaarsak.findAll";
    public static final String QUERY_FIND_VALID = "Hendelseaarsak.findValid";
	@Column(name = "GUI_REKKEFOLGE")
	private Long guiRekkefolge;
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Hendelseaarsak.ID_SEQ)
	private Long id;
	@Column(nullable = false)
	private String navn;
	@Column(name = "VALGBAR_FLAGG")
	private Boolean valgbar;
	@Column(name = "ELRAPP_ID")
	private String elrappId;

	/**
	 * Konstruktør
	 *
	 */
	public Hendelseaarsak() {
	}
	/** {@inheritDoc} */
	public int compareTo(final Hendelseaarsak o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(this.navn, o.navn).toComparison();
	}
	/** {@inheritDoc} */
	@Override
	public boolean equals(final Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof Hendelseaarsak)) {
			return false;
		}

		final Hendelseaarsak other = (Hendelseaarsak) o;

		return new EqualsBuilder().append(this.id, other.getId()).isEquals();
	}
	public Long getGuiRekkefolge() {
		return this.guiRekkefolge;
	}
	/**
	 * Id
	 *
	 * @return
	 */
	public Long getId() {
		return this.id;
	}
	/**
	 * Navnet til �rsaktypen
	 *
	 * @return
	 */
	public String getNavn() {
		return this.navn;
	}

	/** {@inheritDoc} */
	@Override
	public String getTextForGUI() {
		return this.navn;
	}

	public Boolean getValgbar() {
		return this.valgbar;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).toHashCode();
	}

	public void setGuiRekkefolge(final Long guiRekkefolge) {
		final Long old = this.guiRekkefolge;
		this.guiRekkefolge = guiRekkefolge;
		firePropertyChange("guiRekkefolge", old, guiRekkefolge);
	}

	/**
	 * Id
	 *
	 * @param id
	 */
	public void setId(final Long id) {
		final Long old = this.id;
		this.id = id;
		firePropertyChange("id", old, id);
	}

	/**
	 * Navnet til �rsaktypen
	 *
	 * @param navn
	 */
	public void setNavn(final String navn) {
		final String old = this.navn;
		this.navn = navn;
		firePropertyChange("navn", old, navn);
	}

	public void setValgbar(final Boolean valgbar) {
		final Boolean old = this.valgbar;
		this.valgbar = valgbar;
		firePropertyChange("valgbar", old, valgbar);
	}

	public String getElrappId() {
		return elrappId;
	}

	public void setElrappId(String elrappId) {
		this.elrappId = elrappId;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", this.id).append("navn", this.navn).toString();
	}
}
