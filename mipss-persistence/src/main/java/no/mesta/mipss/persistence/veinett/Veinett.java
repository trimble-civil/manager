package no.mesta.mipss.persistence.veinett;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Veinett
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity

@SequenceGenerator(name = Veinett.ID_SEQ, sequenceName = "VEINETT_ID_SEQ", initialValue = 1, allocationSize = 1)
@EntityListeners(no.mesta.mipss.persistence.EndringMonitor.class)
@NamedQueries({
	@NamedQuery(name = Veinett.QUERY_FIND_ALL, query = "select o from Veinett o"),
	@NamedQuery(name = Veinett.QUERY_FIND_BY_TYPE, query = "select o from Veinett o where o.veinetttype.navn=:type"),
	@NamedQuery(name = Veinett.QUERY_FIND_WITH_REFLINKSEKSJON, query ="select distinct o from Veinett o LEFT JOIN FETCH o.veinettreflinkseksjonList where o.id=:id")
})
public class Veinett implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity {
   
	public static final String QUERY_FIND_ALL = "Veinett.findAll";
	public static final String QUERY_FIND_BY_TYPE= "Veinett.findByType";
    public static final String QUERY_FIND_WITH_REFLINKSEKSJON = "Veinett.findWithReflinkseksjon";
    
    public static final String ID_SEQ = "veinettIdSeq";
    
    private String fritekst;
    private String guiFarge;
    private int guiTykkelse;
    private Long id;
    private String navn;
    private Veinetttype veinetttype;
    private List<Veinettreflinkseksjon> veinettreflinkseksjonList = new ArrayList<Veinettreflinkseksjon>();
    
    private OwnedMipssEntity ownedMipssEntity;
    public Veinett() {
    }


    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
        this.fritekst = fritekst;
    }

    @Column(name="GUI_FARGE")
    public String getGuiFarge() {
        return guiFarge;
    }

    public void setGuiFarge(String guiFarge) {
        this.guiFarge = guiFarge;
    }

    @Column(name="GUI_TYKKELSE")
    public int getGuiTykkelse() {
        return guiTykkelse;
    }

    public void setGuiTykkelse(int guiTykkelse) {
        this.guiTykkelse = guiTykkelse;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Veinett.ID_SEQ)
    @Column(nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }


    @ManyToOne
    @JoinColumn(name = "TYPE_NAVN", referencedColumnName = "NAVN")
    public Veinetttype getVeinetttype() {
        return veinetttype;
    }

    public void setVeinetttype(Veinetttype veinetttype) {
        this.veinetttype = veinetttype;
    }

    @OneToMany(mappedBy = "veinett", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Veinettreflinkseksjon> getVeinettreflinkseksjonList() {
        return veinettreflinkseksjonList;
    }

    public void setVeinettreflinkseksjonList(List<Veinettreflinkseksjon> veinettreflinkseksjonList) {
        this.veinettreflinkseksjonList = veinettreflinkseksjonList;
    }

    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }
    
    public String getTextForGUI() {
		return navn;
	}
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("id", id).
            append("navn", navn).
            append("fritekst", fritekst).
            append("guiFarge", guiFarge).
            append("guiTykkelse", guiTykkelse).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Veinett)) {return false;}
        
        Veinett other = (Veinett) o;
        return new EqualsBuilder().append(id, other.getId()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(731,741).append(id).toHashCode();
    }
}
