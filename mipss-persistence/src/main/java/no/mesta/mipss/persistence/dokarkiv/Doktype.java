package no.mesta.mipss.persistence.dokarkiv;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import no.mesta.mipss.persistence.IOwnableMipssEntity;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.OwnedMipssEntity;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Doktype.FIND_ALL, query = "select o from Doktype o"),
	@NamedQuery(name = Doktype.FIND_NAME, query = "select o from Doktype o where o.navn = :navn")
})

@EntityListeners({no.mesta.mipss.persistence.EndringMonitor.class})

public class Doktype extends MipssEntityBean<Doktype> implements Serializable, IRenderableMipssEntity, IOwnableMipssEntity, Comparable<Doktype> {
	public static final String FIND_ALL = "Doktype.findAll";
	public static final String FIND_NAME = "Doktype.findName";
    private String fritekst = "";
    private String navn = "";
    private List<Dokument> dokumentList;
    private Long begrensetTilgangFlagg;

    private OwnedMipssEntity ownedMipssEntity;
    
    public Doktype() {
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @OneToMany(mappedBy = "doktype")
    public List<Dokument> getDokumentList() {
        return dokumentList;
    }

    public void setDokumentList(List<Dokument> dokumentList) {
        this.dokumentList = dokumentList;
    }

    public Dokument addDokument(Dokument dokument) {
        getDokumentList().add(dokument);
        dokument.setDoktype(this);
        return dokument;
    }

    public Dokument removeDokument(Dokument dokument) {
        getDokumentList().remove(dokument);
        dokument.setDoktype(null);
        return dokument;
    }
    
    @Embedded
    public OwnedMipssEntity getOwnedMipssEntity() {
        if (ownedMipssEntity == null) {
            ownedMipssEntity = new OwnedMipssEntity();
        }
        return ownedMipssEntity;
    }
    
    public void setOwnedMipssEntity(OwnedMipssEntity ownedMipssEntity) {
        this.ownedMipssEntity = ownedMipssEntity;
    }

    @Column(name = "BEGRENSET_TILGANG_FLAGG", nullable = false)
    public Long getBegrensetTilgangFlagg() {
		return begrensetTilgangFlagg;
	}

	public void setBegrensetTilgangFlagg(Long begrensetTilgangFlagg) {
		this.begrensetTilgangFlagg = begrensetTilgangFlagg;
	}

	/**
     * Genererer en lesbar tekst med hovedlementene i entityb�nnen.
     * @return en tekstlinje.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", getNavn()).
            append("fritekst", getFritekst()).
            toString();
    }

    /**
     * Sammenlikner med o.
     * @param o
     * @return true hvis relevante n�klene er like. 
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(null == o) {
            return false;
        }
        if(!(o instanceof Doktype)) {
            return false;
        }
        Doktype other = (Doktype) o;
        return new EqualsBuilder().
            append(getNavn(), other.getNavn()).
            isEquals();
    }

    /**
     * Genererer identifikator hashkode.
     * @return koden.
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,91).
            append(getNavn()).
            append(getFritekst()).
            toHashCode();
    }

   @Transient
   public String getTextForGUI() {
       return getNavn();
   }

   public int compareTo(Doktype o) {
       return new CompareToBuilder().
           append(getNavn(), o.getNavn()).
           toComparison();
   }
}
