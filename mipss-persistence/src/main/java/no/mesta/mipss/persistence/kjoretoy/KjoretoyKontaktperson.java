package no.mesta.mipss.persistence.kjoretoy;

public interface KjoretoyKontaktperson {

	String getKontakttypeNavn();
	String getKontaktnavn();
	String getAdressePoststed();
	String getTelefonnummer();
}
