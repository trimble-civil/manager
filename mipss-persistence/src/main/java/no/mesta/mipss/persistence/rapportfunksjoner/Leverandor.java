package no.mesta.mipss.persistence.rapportfunksjoner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Leverandor {
	/**
	 * Oppretter en ny leverandør for rapporten. Benytter data i listen til å slå sammen totaler
	 * for leverandøren. 
	 * 
	 * @param data Skal inneholde rapportdata for kun èn leverandør
	 */
	public Leverandor(){
//		this.data = data;
//		Rapp1 rapp1 = data.get(0);
//		navn = rapp1.getLeverandorNavn();
//		initData();
	}

	
	
	
	
	
	
	/****************************
	 * TESTDATA
	 * @return
	 * @throws ParseException
	 */
	
	public static List<Rapp1> getTestData() {
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		List<Rapp1> rl = new ArrayList<Rapp1>();
		try{
			Rapp1 tl1 = getTestLeverandor(9041, "Veimester rode 4", 2142, "51000377", 1, 36531, "MESTA DRIFT AS", 0, f.parse("01.01.2011 08:47:30"), f.parse("01.01.2011 09:27:16"), 14.542, 1036);
			Rapp1 tl2 = getTestLeverandor(9041, "Veimester rode 4", 2142, "51000377", 1, 36531, "MESTA DRIFT AS", 0, f.parse("02.01.2011 07:47:43"), f.parse("02.01.2011 07:48:58"), 1.222, 74);
			Rapp1 tl3 = getTestLeverandor(10078, "Veimester rode 2", 2142, "51000377", 1, 36531, "MESTA DRIFT AS", 0, f.parse("01.01.2011 09:30:10"), f.parse("01.01.2011 09:33:28"), 4.334, 197);
			Rapp1 tl4 = getTestLeverandor(10078, "Veimester rode 2", 2142, "51000377", 1, 36531, "MESTA DRIFT AS", 0, f.parse("02.01.2011 06:57:36"), f.parse("02.01.2011 07:59:31"), 14.078, 632);
			Rapp1 tl5 = getTestLeverandor(10078, "Veimester rode 2", 2566, "51401270", 0, 36531, "MESTA DRIFT AS", 0, f.parse("02.01.2011 22:19:48"), f.parse("02.01.2011 22:32:14"), 2.058, 106);
			
			Rapp1 tl6 = getTestLeverandor(10078, "Veimester rode 2", 2834, "KT74505", 0, 5390, "ISACHSEN ENTREPRENØR AS", 1, f.parse("01.01.2011 10:56:53"), f.parse("01.01.2011 11:02:23"), 1.228, 86460);
			Rapp1 tl7 = getTestLeverandor(10078, "Veimester rode 2", 2834, "KT74505", 0, 5390, "ISACHSEN ENTREPRENØR AS", 1, f.parse("02.01.2011 06:57:09"), f.parse("02.01.2011 07:02:23"), 1.236, 108);
			
			R12Prodtype t11 = getTestProdtype(5, "Strøing", 90, true, 5d, 300);
			R12Prodtype t12 = getTestProdtype(98, "Tomkjøring", 10, false, 10d, 720);
			R12Prodtype t21 = getTestProdtype(98, "Tomkjøring", 10, false, null, 60);
			R12Prodtype t31 = getTestProdtype(98, "Tomkjøring", 10, false, 5d, 180);
			R12Prodtype t41 = getTestProdtype(98, "Tomkjøring", 10, false, 15d, 630);
			R12Prodtype t51 = getTestProdtype(98, "Tomkjøring", 10, false, 5d, 120);
			
			R12Prodtype t61 = getTestProdtype(5, "Strøing", 90, true, 5d, 120);
			R12Prodtype t62 = getTestProdtype(99, "Ukjent", 5, false, 5d, 120);
			R12Prodtype t71 = getTestProdtype(5, "Strøing", 90, true, 5d, 90);
			R12Prodtype t72 = getTestProdtype(99, "Ukjent", 5, false, 5d, 90);
			
			R12Stroprodukt s11 = getTestStroprodukt(99, "Ukjent", 199, 3.772, 316, Double.valueOf(0.5), null);
			
			R12Stroprodukt s61 = getTestStroprodukt(1, "Tørt salt (NaCl)", 105, 1.228, 124,  Double.valueOf(0.05), null);
			R12Stroprodukt s62 = getTestStroprodukt(6, "Cola", 105, 1.228, 124,  Double.valueOf(0.05), null);
			R12Stroprodukt s71 = getTestStroprodukt(1, "Tørt salt (NaCl)", 105, 1.236, 108,  null, Double.valueOf(100));
		
			List<R12Prodtype> t1 = new ArrayList<R12Prodtype>();
			t1.add(t11);
			t1.add(t12);
			List<R12Prodtype> t2 = new ArrayList<R12Prodtype>();
			t2.add(t21);
			List<R12Prodtype> t3 = new ArrayList<R12Prodtype>();
			t3.add(t31);
			List<R12Prodtype> t4 = new ArrayList<R12Prodtype>();
			t4.add(t41);
			List<R12Prodtype> t5 = new ArrayList<R12Prodtype>();
			t5.add(t51);
			List<R12Prodtype> t6 = new ArrayList<R12Prodtype>();
			t6.add(t61);
			t6.add(t62);
			List<R12Prodtype> t7 = new ArrayList<R12Prodtype>();
			t7.add(t71);
			t7.add(t72);
			
			List<R12Stroprodukt> s1 = new ArrayList<R12Stroprodukt>();
			s1.add(s11);
			List<R12Stroprodukt> s6 = new ArrayList<R12Stroprodukt>();
			s6.add(s61);
			s6.add(s62);
			List<R12Stroprodukt> s7 = new ArrayList<R12Stroprodukt>();
			s7.add(s71);
			
			tl1.setProdtyper(t1);
			tl2.setProdtyper(t2);
			tl3.setProdtyper(t3);
			tl4.setProdtyper(t4);
			tl5.setProdtyper(t5);
			tl6.setProdtyper(t6);
			tl7.setProdtyper(t7);
			
			tl1.setStroprodukter(s1);
			tl6.setStroprodukter(s6);
			tl7.setStroprodukter(s7);
			
			rl.add(tl1);
			rl.add(tl2);
			rl.add(tl3);
			rl.add(tl4);
			rl.add(tl5);
			rl.add(tl6);
			rl.add(tl7);
		} catch (Throwable t){
			t.printStackTrace();
		}
		return rl;
	}
	
	public static Rapp1 getTestLeverandor(long rodeId, String rodenavn, long kjoretoyId, String kjoretoyNavn, long kontraktFlagg, long leverandorNr, String leverandorNavn, long samprod, Date fra, Date til, double totKm, long totSekund){
		Rapp1 l = new Rapp1();
		l.setRodeId(rodeId);
		l.setRodeNavn(rodenavn);
		l.setKjoretoyId(kjoretoyId);
		l.setKjoretoyNavn(kjoretoyNavn);
		l.setPaKontraktFlagg(kontraktFlagg);
		l.setLeverandorNavn(leverandorNavn);
		l.setLeverandorNr(leverandorNr);
		l.setSamproduksjonFlagg(samprod);
		l.setFraDato(fra);
		l.setTilDato(til);
		l.setKjortKm(totKm);
		l.setKjortSekund(totSekund);
		l.setRodelengde(50d);
		return l;
	}
	public static R12Prodtype getTestProdtype(long id, String header, long seq, boolean stroFlagg, Double kjortKm, long kjortSekund){
		R12Prodtype r = new R12Prodtype();
		r.setId(id);
		r.setSeq(seq);
		r.setHeader(header);
		r.setStroFlagg(stroFlagg);
		r.setKjortKm(kjortKm);
		r.setKjortSekund(kjortSekund);
		return r;
	}
	public static R12Stroprodukt getTestStroprodukt(long id, String header, long seq, double kjortKm, long kjortSekund, Double mengdeTonn, Double mengdeM3){
		R12Stroprodukt s = new R12Stroprodukt();
		s.setId(id);
		s.setHeader(header);
		s.setSeq(seq);
		s.setKjortKm(kjortKm);
		s.setKjortSekund(kjortSekund);
		s.setMengdeTonn(mengdeTonn);
		s.setMengdeM3(mengdeM3);
		return s;
	}
	
}
