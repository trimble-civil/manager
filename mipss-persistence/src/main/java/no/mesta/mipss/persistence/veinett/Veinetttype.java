package no.mesta.mipss.persistence.veinett;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Veinetttype - spesifiserer typen til et veinett samt parametere som angir hvordan et veinett
 * skal se ut i GUI
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = Veinetttype.QUERY_FIND_ALL, query = "select o from Veinetttype o"),
    @NamedQuery(name = Veinetttype.QUERY_FIND, query = "select o from Veinetttype o where o.navn=:navn")
})

public class Veinetttype extends MipssEntityBean<Veinetttype> implements Serializable, IRenderableMipssEntity {
	
    public static final String QUERY_FIND_ALL = "Veinetttype.findAll";
    public static final String QUERY_FIND = "Veinetttype.find";

    private String endretAv;
    private Date endretDato;
    private String fritekst;
    private String guiFarge;
    private Integer guiTykkelse;
    private String navn;
    private String opprettetAv;
    private Date opprettetDato;
    private Boolean valgbarFlagg;

    public Veinetttype() {
    }

    @Column(name="ENDRET_AV")
    public String getEndretAv() {
        return endretAv;
    }

    public void setEndretAv(String endretAv) {
        this.endretAv = endretAv;
    }

    @Column(name="ENDRET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndretDato() {
        return endretDato;
    }

    public void setEndretDato(Date endretDato) {
        this.endretDato = endretDato;
    }

    public String getFritekst() {
        return fritekst;
    }

    public void setFritekst(String fritekst) {
    	String old = this.fritekst;
        this.fritekst = fritekst;
        firePropertyChange("fritekst", old, fritekst);
    }

    @Column(name="GUI_FARGE")
    public String getGuiFarge() {
        return guiFarge;
    }

    public void setGuiFarge(String guiFarge) {
    	String old = this.guiFarge;
        this.guiFarge = guiFarge;
        firePropertyChange("guiFarge", old, guiFarge);
    }

    @Column(name="GUI_TYKKELSE")
    public Integer getGuiTykkelse() {
        return guiTykkelse;
    }

    public void setGuiTykkelse(Integer guiTykkelse) {
    	Integer old = this.guiTykkelse;
        this.guiTykkelse = guiTykkelse;
        firePropertyChange("guiTykkelse", old, guiTykkelse);
    }

    @Id
    @Column(nullable = false)
    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
    	String old = this.navn;
        this.navn = navn;
        firePropertyChange("navn", old, navn);
    }

    @Column(name="OPPRETTET_AV")
    public String getOpprettetAv() {
        return opprettetAv;
    }

    public void setOpprettetAv(String opprettetAv) {
        this.opprettetAv = opprettetAv;
    }

    @Column(name="OPPRETTET_DATO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getOpprettetDato() {
        return opprettetDato;
    }

    public void setOpprettetDato(Date opprettetDato) {
        this.opprettetDato = opprettetDato;
    }

    @Column(name="VALGBAR_FLAGG", nullable = false)
    public Boolean getValgbarFlagg() {
        return valgbarFlagg;
    }

    public void setValgbarFlagg(Boolean valgbarFlagg) {
    	Boolean old = this.valgbarFlagg;
        this.valgbarFlagg = valgbarFlagg;
        firePropertyChange("valgbarFlagg", old, valgbarFlagg);
    }
    
    /**
     * @see java.lang.Object#toString()
     * @see org.apache.commons.lang.builder.ToStringBuilder
     * 
     * @return
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
            append("navn", navn).
            append("fritekst", fritekst).
            append("valgbarFlagg", valgbarFlagg).
            append("guiFarge", guiFarge).
            append("guiTykkelse", guiTykkelse).
            append("opprettetAv", opprettetAv).
            append("opprettetDato", opprettetDato).
            append("endretAv", endretAv).
            append("endretDato", endretDato).
            toString();
    }
    
    /**
     * @see java.lang.Object#equals(Object)
     * @see org.apache.commons.lang.builder.EqualsBuilder
     * 
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof Veinetttype)) {return false;}
        
        Veinetttype other = (Veinetttype) o;
        return new EqualsBuilder().append(navn, other.getNavn()).isEquals();
    }
    
    /**
     * @see java.lang.Object#hashCode()
     * @see org.apache.commons.lang.builder.HashCodeBuilder
     * 
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(721,731).append(navn).toHashCode();
    }

	public String getTextForGUI() {
		return navn;
	}
}