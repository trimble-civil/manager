package no.mesta.mipss.persistence.datafangsutstyrbestilling;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class DfuDfuvarePK implements Serializable {
	private Long dfuvareId;
	private String modellNavn;
	
	public DfuDfuvarePK(Long dfuvareId, String modellNavn) {
		this.dfuvareId = dfuvareId;
		this.modellNavn = modellNavn;
	}
	
	public DfuDfuvarePK() {
	}

	public Long getDfuvareId() {
		return dfuvareId;
	}

	public void setDfuvareId(Long dfuvareId) {
		this.dfuvareId = dfuvareId;
	}

	public String getModellNavn() {
		return modellNavn;
	}

	public void setModellNavn(String modellNavn) {
		this.modellNavn = modellNavn;
	}
	
    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dfuvareId).append(modellNavn).toHashCode();
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if(o == null) {return false;}
        if(o == this) {return true;}
        if(!(o instanceof DfuDfuvarePK)) {return false;}
        
        DfuDfuvarePK other = (DfuDfuvarePK) o;
        
        return new EqualsBuilder().append(dfuvareId, other.dfuvareId).append(modellNavn, other.modellNavn).isEquals();
    }
}
