package no.mesta.mipss.valueobjects;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class ElrappStatus implements Serializable, Comparable<ElrappStatus>, IRenderableMipssEntity{
	private final Status status;
	private Integer order;
	public enum Status{
		SENDT,
		IKKE_SENDT,
		ENDRET
	}
	
	public ElrappStatus(Status status){
		this.status = status;
		switch(status){
			case SENDT:order=1;break;
			case ENDRET:order=2;break;
			case IKKE_SENDT:order=3;break;
		}
	}
	
	public Status getStatus(){
		return status;
	}

	public int compareTo(ElrappStatus o) {
		ElrappStatus s = (ElrappStatus)o;
		return s.order.compareTo(order);
	}

	public String getTextForGUI() {
		return null;
	}
	public static ElrappStatus getElrappStatus(Date rapportertDato, Date endretDato){
		if (rapportertDato==null)
			return new ElrappStatus(ElrappStatus.Status.IKKE_SENDT);
		if (endretDato!=null){
			if (rapportertDato.before(endretDato)){
				return new ElrappStatus(ElrappStatus.Status.ENDRET);
			}
		}
		return new ElrappStatus(ElrappStatus.Status.SENDT);
	}
	
	public String toString(){
		return status.toString();
	}
	
	
	
}
