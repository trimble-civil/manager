package no.mesta.mipss.valueobjects;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class Epostadresse implements Serializable, IRenderableMipssEntity {
	private String adresse;

	public Epostadresse(String adresse) {
		this.adresse = adresse;
	}

	public String getAdresse() {
		return adresse;
	}

	public String getTextForGUI() {
		return adresse;
	}
	
	public String toString() {
		return adresse;
	}
}
