package no.mesta.mipss.valueobjects;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class ElrappTypeR implements Serializable, IRenderableMipssEntity{
	public enum Type{
		R2, 
		R5, 
		R11
	}
	private Type type;
	public ElrappTypeR(Type type){
		this.type = type;
	}
	public Type getType(){
		return type;
	}
	public String getTextForGUI() {
		return null;
	}
}
