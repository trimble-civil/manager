package no.mesta.mipss.db.exception;

/**
 * Exception for DAO klasser
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class DAOException extends Exception {
    
    /**
     * Konstruktør
     */
    public DAOException() {
    }

    /**
     * Konstruktør
     * 
     * @param message
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * Konstruktør
     * 
     * @param cause
     */
    public DAOException(Throwable cause) {
        super(cause);
    }

    /**
     * Konstruktør
     * 
     * @param message
     * @param cause
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
