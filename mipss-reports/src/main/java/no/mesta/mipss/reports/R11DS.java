package no.mesta.mipss.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Datasource for R11 rapporten
 * 
 * @author harkul
 *
 */

public class R11DS extends MipssFeltDS<Skred>{
	private static final Logger logger = LoggerFactory.getLogger(R11DS.class);
	private List<R11RapportObjekt> entities;
	private int pageSize = 5;
	private boolean inited;
	private int currentEntity;
	private int currentPage;
	
	public R11DS(List<String> guids) {
		super(Skred.class, guids);
	}

	public R11DS(Class<Skred> clazz, List<String> guids, MipssFelt fieldBean) {
		super(clazz, guids, fieldBean);
	}
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		System.out.println(field.getName());
		logger.debug("getFieldValue(" + field.getName() + ") start");
		Object value = entity.getFieldValue(field.getName());
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}
	/**
	 * Laster sidevis med data fra databasen
	 * 
	 */
	@Override
	protected void loadPageData() {
		entities = null;
		
		List<String> pageOfIds = new ArrayList<String>();
		int currentPageStart = pageSize * currentPage;
		int currentPageEnd = currentPageStart + pageSize;
		for(int c = currentPageStart; c < getGuids().size() && c < currentPageEnd; c++) {
			pageOfIds.add(getGuids().get(c));
		}
		
		if(!pageOfIds.isEmpty()) {
			entities = new ArrayList<R11RapportObjekt>(); 
			for (Skred p:fieldBean.hentObjekter(Skred.class, pageOfIds)){
				entities.add(new R11RapportObjekt(p));
				
			}
		}
		
		if(entities == null) {
			entities = new ArrayList<R11RapportObjekt>();
		}
	}

	@Override
	public boolean next() throws JRException {
		logger.debug("next() start");
		entity = null;
		
		if(!inited) {
			loadPageData();
			inited = true;
		}
		
		
		if(currentEntity == getPageSize() && ((currentPage * pageSize) + currentEntity < getGuids().size())) {
			currentPage++;
			currentEntity = 0;
			loadPageData();
		}
		
		if(currentEntity < entities.size()) {
			entity = entities.get(currentEntity);
			logger.debug("Current entity is: " + entity);
			currentEntity++;
		}
		
		boolean hasNext = (entity != null) && ((currentPage * pageSize) + currentEntity <= getGuids().size());
		logger.debug("next() done with " + hasNext);
		return hasNext;
	}
}
