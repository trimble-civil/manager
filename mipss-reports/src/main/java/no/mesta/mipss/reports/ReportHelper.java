package no.mesta.mipss.reports;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRImageRenderer;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSwapFile;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapsgen.MapItem;
import no.mesta.mipss.mapsgen.MapsGen;
import no.mesta.mipss.mapsgen.PointImage;
import no.mesta.mipss.mapsgen.PointItem;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.resources.images.IconResources;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hjelpeklasse for å kunen vise rapporter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportHelper {
	public static final int EXPORT_PDF = 0;
	public static final int EXPORT_XLS = 1;

	private static final Logger logger = LoggerFactory.getLogger(ReportHelper.class);
	/**
	 * Lager en PDF rapport av en jasper template
	 * 
	 * @param jasperReport
	 * @param data
	 * @return
	 */
	public static File buildReport(JasperReport jasperReport, JRDataSource data, int outformat,
			Map<String, Object> parameters) {
		File outFile = null;

		if (parameters == null) {
			parameters = new HashMap<String, Object>();
		}

		try {
			String tmpDir = System.getProperty("java.io.tmpdir");
			JRSwapFile swapFile = new JRSwapFile(tmpDir, 1024, 1024);
			JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer(64, swapFile);

			parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			parameters.put("REPORT_RENDER_HELPER", new ReportRenderHelper());

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, data);
			JRAbstractExporter exporter = null;
			if (outformat == EXPORT_PDF) {
				exporter = new JRPdfExporter();
				outFile = File.createTempFile("temp_report", ".pdf");
			} else if (outformat == EXPORT_XLS) {
				exporter = new JExcelApiExporter();
				outFile = File.createTempFile("temp_report", ".xls");
			} else {
				throw new IllegalArgumentException("Unknown exporter");
			}

			FileOutputStream fos = new FileOutputStream(outFile);
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
			exporter.exportReport();

			IOUtils.closeQuietly(fos);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		} 
		return outFile;
	}

	/**
	 * Lager en PDF rapport av en jasper template
	 * 
	 * @param jasperTemplateFileName
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static File buildReport(String jasperTemplateFileName, Collection data) {
		return buildReport(findReport(jasperTemplateFileName), new JRBeanCollectionDataSource(data), EXPORT_PDF, null);
	}

	/**
	 * Lager en rapport av en jasper template
	 * 
	 * @param jasperTemplateFileName
	 * @param data
	 * @param outformat
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static File buildReport(String jasperTemplateFileName, Collection data, int outformat) {
		return buildReport(findReport(jasperTemplateFileName), new JRBeanCollectionDataSource(data), outformat, null);
	}

	public static File buildReport(String jasperTemplateFileName, JRDataSource data) {
		return buildReport(findReport(jasperTemplateFileName), data, EXPORT_PDF, null);
	}

	/**
	 * Lager en rapport av en jasper template
	 * 
	 * @param jasperTemplateFileName
	 * @param data
	 * @param outformat
	 * @param parameters
	 * @return
	 */
	public static File buildReport(String jasperTemplateFileName, JRDataSource data, int outformat,
			Map<String, Object> parameters) {
		return buildReport(findReport(jasperTemplateFileName), data, outformat, parameters);
	}

	/**
	 * Laster en report
	 * 
	 * @param reportFile
	 * @return
	 */
	public static JasperReport findReport(String reportFile) {
		InputStream stream = findTemplate(reportFile);
		JasperReport report = null;
		try {
			report = (JasperReport) JRLoader.loadObject(stream);
		} catch (JRException e) {
			logger.warn("findReport: ",e);
		}
		return report;
	}

	/**
	 * Leter fram på classpath en stream
	 * 
	 * @param template
	 * @return
	 */
	public static InputStream findTemplate(String template) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		InputStream stream = cl.getResourceAsStream(template);

		if (stream == null) {
			stream = ReportHelper.class.getResourceAsStream(template);
		}

		if (stream == null) {
			stream = JRLoader.getResourceInputStream(template);
		}

		if (stream == null) {
			throw new IllegalArgumentException("Template not found! " + template);
		}

		return stream;
	}

	public static MapImage generateMap(Avvik funn, int width, int height, double scale) {
		if(logger.isTraceEnabled()) {
			logger.trace(funn + " " + width + " " + height + " " + scale);
		}
		return generateMap(PunktregSok.newInstance(funn), width, height, scale);
	}

	public static MapImage generateMap(PunktregSok funn, int width, int height, double scale) {
		if(logger.isTraceEnabled()) {
			logger.trace(funn + " " + width + " " + height + " " + scale);
		}
		if (funn.getX() == null || funn.getY() == null) {
			logger.trace("no location");
			return null;
		}

		PointItem item = new PointItem();
		item.setPointImageIndex(0);
		item.setPos(new Point());
		item.getPos().setLocation(funn.getX(), funn.getY());
		logger.trace("location set to: " + item.getPos());

		PointImage image = new PointImage();
		image.setHotspot(new Point(22, 40));
		image.setImageData(ImageUtil.imageToBytes(ImageUtil.convertImageToBufferedImage(IconResources.MAP_PIN_LEFT
				.getImage())));
		
		try {
			MapsGen mapsGen = BeanUtil.lookup(MapsGen.BEAN_NAME, MapsGen.class);
			byte[] mapImagebytes = mapsGen.generateMap(Collections.singletonList((MapItem) item),
					Collections.singletonList(image), new Dimension(width, height), scale);
						
			logger.trace("returns map image");
			return new MapImage(mapImagebytes);
		} catch (Throwable t) {
			logger.error("Throwable in generateMap", t);
			return null;
		}
	}
	
	private static class MapImage extends JRImageRenderer {
		
		public MapImage(byte[] bytes) {
			super(bytes);
		}
	}
}
