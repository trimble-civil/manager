package no.mesta.mipss.reports;

import java.awt.image.BufferedImage;
import java.util.Date;

import net.sf.jasperreports.engine.JRImageRenderer;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

/**
 * Lager et bilde som kan benyttes i jasper rapport
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class ReportImage extends JRImageRenderer implements Comparable<ReportImage> {
	private String beskrivelse;
	private Date dato;
	private String filename;
	private AvvikstatusType funnStatus;
	private String status;

	/**
	 * Konstruktør som sender byte[] til superklassen og setter filnavn
	 * 
	 * @param bilde
	 */
	public ReportImage(Bilde bilde) {
		super((byte[])null);
//		this(bilde.getBildeLob(), bilde.getFilnavn(), bilde.getBeskrivelse(), null, bilde.getOwnedMipssEntity()
//				.getOpprettetDato());
	}
	
	/**
	 * Konstruktør
	 * 
	 * @param bilde
	 * @param status
	 */
	public ReportImage(Bilde bilde, AvvikstatusType status) {
		super((byte[])null);
//		super(bilde.getBildeLob());
		this.dato = bilde.getOwnedMipssEntity().getOpprettetDato();
		this.beskrivelse = bilde.getBeskrivelse();
		this.filename = bilde.getFilnavn();
		this.status = status.getStatus();
		this.funnStatus = status;
	}
	
	/**
	 * Konstruktør
	 * 
	 * @param image
	 * @param filename
	 * @param beskrivelse
	 * @param status
	 */
	public ReportImage(BufferedImage image, String filename, String beskrivelse, String status) {
		this(ReportImageUtil.imageToBytes(image), filename, beskrivelse, status);
	}

	/**
	 * Konstruktør
	 * 
	 * @param data
	 * @param filename
	 * @param beskrivelse
	 * @param status
	 */
	public ReportImage(byte[] data, String filename, String beskrivelse, String status) {
		this(data, filename, beskrivelse, status, null);
	}

	public ReportImage(byte[] data, String filename, String beskrivelse, String status, Date dato) {
		super(data);
		this.filename = filename;
		this.beskrivelse = beskrivelse;
		this.status = status;
		this.dato = dato;
	}

	/**
	 * Konstruktør for funn bilder
	 * 
	 * @param bilde
	 */
	public ReportImage(AvvikBilde bilde) {
		this(bilde.getBilde(), bilde.getStatusType());
	}

	/**
	 * Konstruktør for funn bilder
	 * 
	 * @param bilde
	 */
	public ReportImage(BufferedImage image, AvvikBilde bilde) {
		super(ReportImageUtil.imageToBytes(image));
		this.beskrivelse = bilde.getBilde().getBeskrivelse();
		this.dato = bilde.getBilde().getOwnedMipssEntity().getOpprettetDato();
		this.filename = bilde.getBilde().getFilnavn();
		this.funnStatus = bilde.getStatusType();
		this.status = bilde.getStatusType().getNavn();
	}

	/**
	 * Sammenligner datoer
	 * 
	 * @param otherDato
	 * @return
	 */
	private int compareDate(Date otherDato) {
		if(dato == null && otherDato != null) {
			return -1;
		} else if(dato != null && otherDato == null) {
			return 1;
		} else if(dato == null && otherDato == null) {
			return 0;
		} else {
			return dato.compareTo(otherDato);
		}
	}
	
	/**
	 * Sammenligner status for funn
	 * 
	 * @param otherStatus
	 * @return
	 */
	private int compareStatus(AvvikstatusType otherStatus) {
		if(funnStatus == null && otherStatus != null) {
			return -1;
		} else if(funnStatus != null && otherStatus == null) {
			return 1;
		} else if(funnStatus == null && otherStatus == null) {
			return 0;
		} else {
			return funnStatus.compareTo(otherStatus);
		}		
	}

	/** {@inheritDoc} */
	public int compareTo(ReportImage o) {
		
		Date otherD = o.getDato();
		AvvikstatusType otherS = o.getFunnStatus();
		
		int datoC = compareDate(otherD);
		int statusC = compareStatus(otherS);
		
		if(datoC == 0) {
			if(statusC == 0) {
				String otherStatus = o.getStatus();
				
				if(status == null && otherStatus == null) {
					return 0;
				} else if (status != null && otherStatus != null) {
					return status.compareTo(otherStatus);
				} else if (status != null && otherStatus == null) {
					return 1;
				} else {
					return -1;
				}
				
			} else {
				return statusC;
			}
		} else {
			return datoC;
		}
	}

	/**
	 * Beskrivelsen av bildet
	 * 
	 * @return
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Dato for når bildet er tatt
	 * 
	 * @return
	 */
	public Date getDato() {
		return dato;
	}

	/**
	 * Navnet til bildet
	 * 
	 * @return
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Funn bildes status
	 * 
	 * @return
	 */
	public AvvikstatusType getFunnStatus() {
		return funnStatus;
	}

	/**
	 * Status for bildet
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Beskrivelsen av bildet
	 * 
	 * @param beskrivelse
	 */
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	/**
	 * Dato for når bildet er tatt
	 * 
	 * @param dato
	 */
	public void setDato(Date dato) {
		this.dato = dato;
	}

	/**
	 * Navnet til bildet
	 * 
	 * @param filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Funn bildes status
	 * 
	 * @param funnStatus
	 */
	public void setFunnStatus(AvvikstatusType funnStatus) {
		this.funnStatus = funnStatus;
	}

	/**
	 * Status for bildet
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
