package no.mesta.mipss.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.persistence.mipssfield.Avvik;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PunktregDS  extends MipssFeltDS<Avvik> {
	private static final Logger logger = LoggerFactory.getLogger(PunktregDS.class);
	private int currentEntity;
	private int currentPage;
	private List<PunktregRapportObjekt> entities;
	private int pageSize = 5;
	private boolean inited;
	
	public PunktregDS(List<String> guids) {
		super(Avvik.class, guids);
	}

	public PunktregDS(Class<Avvik> clazz, List<String> guids, MipssFelt fieldBean) {
		super(clazz, guids, fieldBean);
	}
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		logger.debug("getFieldValue(" + field.getName() + ") start");
		Object value = entity.getFieldValue(field.getName());
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}
	/**
	 * Laster sidevis med data fra databasen
	 * 
	 */
	@Override
	protected void loadPageData() {
		entities = null;
		
		List<String> pageOfIds = new ArrayList<String>();
		int currentPageStart = pageSize * currentPage;
		int currentPageEnd = currentPageStart + pageSize;
		for(int c = currentPageStart; c < getGuids().size() && c < currentPageEnd; c++) {
			pageOfIds.add(getGuids().get(c));
		}
		
		if(!pageOfIds.isEmpty()) {
			entities = new ArrayList<PunktregRapportObjekt>(); 
			for (Avvik p:fieldBean.hentObjekter(Avvik.class, pageOfIds)){
				entities.add(new PunktregRapportObjekt(p));
			}
		}
		
		if(entities == null) {
			entities = new ArrayList<PunktregRapportObjekt>();
		}
	}

	@Override
	public boolean next() throws JRException {
		logger.debug("next() start");
		entity = null;
		
		if(!inited) {
			loadPageData();
			inited = true;
		}
		
		
		if(currentEntity == getPageSize() && ((currentPage * pageSize) + currentEntity < getGuids().size())) {
			currentPage++;
			currentEntity = 0;
			loadPageData();
		}
		
		if(currentEntity < entities.size()) {
			entity = entities.get(currentEntity);
			logger.debug("Current entity is: " + entity);
			currentEntity++;
		}
		
		boolean hasNext = (entity != null) && ((currentPage * pageSize) + currentEntity <= getGuids().size());
		logger.debug("next() done with " + hasNext);
		return hasNext;
	}
}
