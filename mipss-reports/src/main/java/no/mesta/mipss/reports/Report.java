package no.mesta.mipss.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;

/**
 * Tilgjengelige rapporter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class Report implements Serializable, Comparable<Report> {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("reports");
	public static final Report DISCOVERY_DETAIL_PRINT = new Report(BUNDLE.getGuiString("funn"), "Funn.jasper", ReportParameter.INCLUDE_IMAGES_PUNKTREG);
	public static final Report R2_FORM = new Report(BUNDLE.getGuiString("r2"), "R2.jasper", ReportParameter.INCLUDE_IMAGES);
	public static final Report HP_REPORT = new Report(BUNDLE.getGuiString("hpReport"), "InspeksjonHPReport.jasper");
	public static final Report INSPECTION_OVERVIEW = new Report(BUNDLE.getGuiString("inspectionOverview"), "InspeksjonOversikt.jasper");
	public static final Report INSPECTION_REPORT = new Report(BUNDLE.getGuiString("inspectionReport"), "inspeksjonsrapport_main.jasper");
	
	public static final Report DISCOVERY_OVERVIEW = new Report(BUNDLE.getGuiString("discoveryOverview"), "FunnOversikt.jasper");
	
	public static final Report R5_FORM = new Report(BUNDLE.getGuiString("r5"), "R5.jasper", ReportParameter.INCLUDE_IMAGES );
	public static final Report R11_FORM = new Report(BUNDLE.getGuiString("r11"), "R11.jasper", ReportParameter.INCLUDE_IMAGES );
	public static final Report KJORETOYORDRE = new Report(BUNDLE.getGuiString("kjoretoyordre"), "kjoretoyordre.jasper");
	public static final Report BESTILLINGSSKJEMA = new Report(BUNDLE.getGuiString("bestilling"), "best_hovedskjema.jasper");
	
	private final String jasperFile;
	private final String name;
	private final List<ReportParameter> parameters;

	public Report(String name, String jasperFile, ReportParameter... parameters) {
		this.name = name;
		this.jasperFile = jasperFile;

		this.parameters = new ArrayList<ReportParameter>();

		for (ReportParameter param : parameters) {
			this.parameters.add(param);
		}
	}

	/** {@inheritDoc} */
	public int compareTo(Report o) {
		if (o == null) {
			return 1;
		}

		return new CompareToBuilder().append(name, o.name).toComparison();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof Report)) {
			return false;
		}

		return StringUtils.equals(name, ((Report) o).name);
	}

	/**
	 * Standard spørsmål for denne rapporten
	 * 
	 * @return
	 */
	public List<ReportParameter> getDefaultParameters() {
		return parameters;
	}

	/**
	 * Jasperfilen til rapporten
	 * 
	 * @return
	 */
	public String getJasperFileName() {
		return jasperFile;
	}

	/**
	 * Navnet til rapporten
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return name + ": " + jasperFile;
	}
}
