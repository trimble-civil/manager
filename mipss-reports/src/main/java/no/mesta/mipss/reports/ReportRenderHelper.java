package no.mesta.mipss.reports;

/**
 * Kan brukes av jasper templates i printWhenExpressions
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportRenderHelper {
	private static final String EMPTY_STRING = "";
	private final Boolean[] isPrinted = {Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE };
	
	public Boolean renderGroup(int group) {
		isPrinted[group] = Boolean.TRUE;
		return isPrinted[group];
	}
	
	public String getGroupFieldOnePrinted() {
		isPrinted[0] = Boolean.TRUE;
		return EMPTY_STRING;
	}

	public String getGroupFieldTwoPrinted() {
		isPrinted[1] = Boolean.TRUE;
		return EMPTY_STRING;
	}

	public String getGroupFieldThreePrinted() {
		isPrinted[2] = Boolean.TRUE;
		return EMPTY_STRING;
	}

	public String getGroupFieldFourPrinted() {
		isPrinted[3] = Boolean.TRUE;
		return EMPTY_STRING;
	}
	
	public boolean isGroupFieldOnePrinted() {
		return isPrinted[0];
	}

	public boolean isGroupFieldTwoPrinted() {
		return isPrinted[1];
	}

	public boolean isGroupFieldThreePrinted() {
		return isPrinted[2];
	}

	public boolean isGroupFieldFourPrinted() {
		return isPrinted[3];
	}
	
	public Boolean isGroupFieldPrinted(int group) {
		return isPrinted[group];
	}
}
