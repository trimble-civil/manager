package no.mesta.mipss.reports;

import java.io.Serializable;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

import org.apache.commons.lang.StringUtils;

/**
 * Parametere for rapporter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportParameter implements Serializable {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("reports");
	public static final ReportParameter INCLUDE_DISCOVERY_SUB_REPORT = new ReportParameter("INCLUDE_DISCOVERY_SUB_REPORT", Boolean.class, Boolean.FALSE);
	public static final ReportParameter INCLUDE_FULLPAGE_MAP = new ReportParameter("INCLUDE_FULLPAGE_MAP",Boolean.class, Boolean.FALSE);
	public static final ReportParameter INCLUDE_IMAGES = new ReportParameter("INCLUDE_IMAGES", Boolean.class,Boolean.TRUE);
	public static final ReportParameter INCLUDE_IMAGES_PUNKTREG = new ReportParameter("INCLUDE_IMAGES_PUNKTREG", Boolean.class,Boolean.FALSE);
	
	private final Object defaultValue;
	private final String name;
	private final Class<?> type;

	public ReportParameter(String name, Class<?> type, Object defaultValue) {
		this.name = name;
		this.type = type;
		this.defaultValue = defaultValue;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof ReportParameter)) {
			return false;
		}

		return StringUtils.equals(name, ((ReportParameter) o).name);
	}

	/**
	 * Standard verdien for dette parameteret
	 * 
	 * @return
	 */
	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Navnet til parameteret
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	public String getTitle() {
		return BUNDLE.getGuiString(name);
	}

	/**
	 * Typen til parameter verdien
	 * 
	 * @return
	 */
	public Class<?> getType() {
		return type;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return name + ": " + type;
	}
}
