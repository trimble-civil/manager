package no.mesta.mipss.reports;

import java.util.Collections;
import java.util.List;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

@SuppressWarnings("serial")
public class PunktregRapportObjekt extends Avvik{

	public PunktregRapportObjekt(Avvik p){
		for (String s: p.getWritableFieldNames()){
			if (!s.equals("estimater"))
				setFieldValue(s, p.getFieldValue(s));
		}
	}
	/**
	 * Gir ut bilde for statusen funnet har nå
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getRapportNaaBilde() {
		AvvikStatus status = getStatus();
		Bilde bilde = getBilde(status.getAvvikstatusType().getStatus());

		if (bilde != null) {
			return new ReportImage(bilde.getBildeLob(), bilde.getFilnavn(), bilde.getBeskrivelse(), status.getAvvikstatusType().getStatus());
		} else {
			return null;
		}
	}
	/**
	 * Gir ut det første bildet for funnet
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getRapportNyBilde() {
		Bilde bilde = getBilde(AvvikstatusType.NY_STATUS);

		if (bilde != null) {
			return new ReportImage(bilde.getBildeLob(), bilde.getFilnavn(), bilde.getBeskrivelse(),
					AvvikstatusType.NY_STATUS);
		} else {
			return null;
		}
	}
	/**
	 * Benyttes av jasper reports templates
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getReportImage() {
		AvvikBilde bilde = getStatusBilde();

		if (bilde != null) {
			return new ReportImage(ReportImageUtil.ensureLandscapeMode(bilde.getBilde().getImage()), bilde);
		} else {
			return null;
		}
	}
	/**
	 * Gir bilde som skal være nederst til venstre i funn rapport
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getReportImageBL() {
		List<AvvikBilde> punktregBilder = getSorterteBilder();

		if (punktregBilder != null && punktregBilder.size() > 2) {
			int index = getSorterteBilder().size() > 3 ? punktregBilder.size() - 2 : 2;
			AvvikBilde bilde = punktregBilder.get(index);
			
			if (bilde.getBilde()!=null){
				if (bilde.getBilde().getSmaaImage()!=null){
					return new ReportImage(bilde.getBilde().getSmaaImage(), bilde);
//					return new ReportImage(ReportImageUtil.ensureLandscapeMode(bilde.getBilde().getSmaaImage()), bilde);
				}
			}
		}
		return null;
	}
	/**
	 * Gir bilde som skal være nederst til høyre i funn rapport
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getReportImageBR() {
		List<AvvikBilde> punktregBilder = getSorterteBilder();

		if (punktregBilder != null && punktregBilder.size() > 3) {
			int index = punktregBilder.size() > 4 ? punktregBilder.size() - 1 : 3;
			AvvikBilde bilde = punktregBilder.get(index);
			if (bilde.getBilde()!=null){
				if (bilde.getBilde().getSmaaImage()!=null){
					return new ReportImage(bilde.getBilde().getSmaaImage(), bilde);
//					return new ReportImage(ReportImageUtil.ensureLandscapeMode(bilde.getBilde().getSmaaImage()), bilde);
				}
			}
		}
		return null;
	}
	/**
	 * Henter ut bildene i portrett modus
	 * 
	 * @return
	 */
	@Transient
	public List<ReportImage> getReportImages() {
		List<ReportImage> images = ReportImageUtil.createDiscoveryImageLandscapes(getSorterteBilder());
		Collections.sort(images);
		return images;
	}

	/**
	 * Gir bilde som skal være øverst til venstre i funn rapport
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getReportImageTL() {
		List<AvvikBilde> punktregBilder = getSorterteBilder();

		if (punktregBilder != null && punktregBilder.size() > 0) {
			AvvikBilde bilde = punktregBilder.get(0);
			if (bilde.getBilde()!=null){
				if (bilde.getBilde().getSmaaImage()!=null){
					return new ReportImage(bilde.getBilde().getSmaaImage(), bilde);
//					return new ReportImage(ReportImageUtil.ensureLandscapeMode(bilde.getBilde().getSmaaImage()), bilde);
				}
			}
		} 
		return null;
		
	}

	/**
	 * Gir bilde som skal være øverst til høyre i funn rapport
	 * 
	 * @return
	 */
	@Transient
	public ReportImage getReportImageTR() {
		List<AvvikBilde> punktregBilder = getSorterteBilder();

		if (punktregBilder != null && punktregBilder.size() > 1) {
			AvvikBilde bilde = punktregBilder.get(1);
			if (bilde.getBilde().getSmaaImage()!=null){
				return new ReportImage(bilde.getBilde().getSmaaImage(), bilde);
//				return new ReportImage(ReportImageUtil.ensureLandscapeMode(bilde.getBilde().getSmaaImage()), bilde);
			}	
		}
		return null;
	}
}
