package no.mesta.mipss.reports;

import java.util.List;

import javax.persistence.Transient;

import no.mesta.mipss.persistence.mipssfield.Hendelse;

@SuppressWarnings("serial")
public class HendelseRapportObjekt extends Hendelse{

	public HendelseRapportObjekt(Hendelse h){
		for (String s: h.getWritableFieldNames()){
			setFieldValue(s, h.getFieldValue(s));
		}
	}
	@Transient
	public List<ReportImage> getReportImages() {
		return ReportImageUtil.createImagePortraits(getBilder());
	}

}
