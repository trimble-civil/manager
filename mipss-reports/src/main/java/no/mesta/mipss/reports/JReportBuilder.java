package no.mesta.mipss.reports;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import net.sf.jasperreports.engine.JRDataSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.resources.buttons.ButtonToolkit;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXBusyLabel;

/**
 * GUI for å starte byggingen av en rapport
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JReportBuilder extends JDialog implements WindowListener {
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("reports");
	private static final Logger logger = LoggerFactory.getLogger(JReportBuilder.class);
	private boolean abort;
	private ReportBuilderTask buildReportTask;
	private final JRDataSource dataSource;
	private boolean errors;
	private final int exportType;
	private File file;
	private final List<ReportParameter> parameters;
	private final Map<ReportParameter, ReportParameterValue> parameterValues;
	private final JFrame parent;
	private final Report report;

	public JReportBuilder(Report report, List<ReportParameter> parameters, JRDataSource dataSource, int exportType, JFrame parent) {
		super(parent, report.getName(), true);
		this.dataSource = dataSource;
		this.report = report;
		this.parameters = parameters;
		this.exportType = exportType;
		this.parent = parent;
		parameterValues = new HashMap<ReportParameter, ReportParameterValue>();
	
		BoxLayout layout = new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS);
		setLayout(layout);
		setIconImage(IconResources.ADOBE_16.getImage());

		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, ButtonToolkit.createEmblem(IconResources.QUESTION_ICON, false), BoxUtil
				.createHorizontalStrut(20), createQuestions()));
		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, Box.createHorizontalGlue(), createButtons()));
		add(BoxUtil.createVerticalStrut(5));

		
		pack();
		setLocationRelativeTo(parent);
		setResizable(false);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(this);
		setVisible(true);
	}
	public JReportBuilder(Report report, List<ReportParameter> parameters, JRDataSource dataSource, int exportType, JFrame parent, boolean buildReport) {
		super(parent, report.getName(), true);
		this.dataSource = dataSource;
		this.report = report;
		this.parameters = parameters;
		this.exportType = exportType;
		this.parent = parent;

		parameterValues = new HashMap<ReportParameter, ReportParameterValue>();

		BoxLayout layout = new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS);
		setLayout(layout);
		setIconImage(IconResources.ADOBE_16.getImage());

		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, ButtonToolkit.createEmblem(IconResources.QUESTION_ICON, false), BoxUtil
				.createHorizontalStrut(20), createQuestions()));
		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, Box.createHorizontalGlue(), createButtons()));
		add(BoxUtil.createVerticalStrut(5));

		
		pack();
		setLocationRelativeTo(parent);
		setResizable(false);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(this);
		
		if (buildReport)
			buildReport();
		setVisible(true);
	}
	/**
	 * Bygger rapporten og endrer GUI
	 * 
	 */
	private void buildReport() {
		buildReportTask = new ReportBuilderTask();
		buildReportTask.execute();

		getContentPane().removeAll();

		JPanel labelPanel = new JPanel(new FlowLayout());
		labelPanel.add(new JLabel(BUNDLE.getGuiString("buildingReports")));

		JPanel spinnerPanel = new JPanel(new FlowLayout());
		JXBusyLabel busyLabel = new JXBusyLabel();
		busyLabel.setBusy(true);
		spinnerPanel.add(busyLabel);

		JButton cancel = new JButton(IconResources.STOP_PROCESS_ICON);
		cancel.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			public void actionPerformed(ActionEvent e) {
				if (buildReportTask != null) {
					buildReportTask.cancel();
				}
				abort = true;
				dispose();
			}
		});

		add(BoxUtil.createVerticalStrut(5));
		add(labelPanel);
		add(BoxUtil.createVerticalStrut(5));
		add(spinnerPanel);
		add(BoxUtil.createVerticalStrut(5));
		add(BoxUtil.createHorizontalBox(5, Box.createHorizontalGlue(), cancel));
		add(BoxUtil.createVerticalStrut(5));
		pack();
		setLocationRelativeTo(parent);
	}
	
	/**
	 * Lager avbryt og ok knapp
	 * 
	 * @return
	 */
	private Component createButtons() {
		JButton ok = new JButton(BUNDLE.getGuiString("ok"));
		ok.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			public void actionPerformed(ActionEvent e) {
				buildReport();
			}

		});
		JButton cancel = new JButton(BUNDLE.getGuiString("cancel"));
		cancel.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			public void actionPerformed(ActionEvent e) {
				abort = true;
				dispose();
			}

		});

		return BoxUtil.createHorizontalBox(0, cancel, BoxUtil.createHorizontalStrut(5), ok);
	}

	/**
	 * Lager listen med spørsmål til brukeren for rapporten
	 * 
	 * @return
	 */
	private Component createQuestions() {
		Box questions = Box.createVerticalBox();

		if (parameters != null && !parameters.isEmpty()) {
			for (ReportParameter param : parameters) {
				ReportParameterValue paramValue = new ReportParameterValue();

				parameterValues.put(param, paramValue);

				Component guiControl = null;

				if (param.getType().equals(Boolean.class)) {
					guiControl = new JCheckBox(param.getTitle());
					((JCheckBox) guiControl).setSelected(param.getDefaultValue() == null ? Boolean.FALSE
							: (Boolean) param.getDefaultValue());
					paramValue.setValue(param.getDefaultValue() == null ? Boolean.FALSE : param.getDefaultValue());
					Binding<JCheckBox, Boolean, ReportParameterValue, Object> binding = BindingHelper.createbinding(
							(JCheckBox) guiControl, "selected", paramValue, "value", BindingHelper.READ_WRITE);
					binding.bind();
					questions.add(guiControl);
					questions.add(BoxUtil.createVerticalStrut(5));
					
				} else {
					paramValue.setValue(param.getDefaultValue());
					questions.add(new JLabel(BUNDLE.getGuiString("okToContinue")));
				}
			}
		} else {
			questions.add(new JLabel(BUNDLE.getGuiString("okToContinue")));
		}

		return questions;
	}

	/**
	 * Rapporten som ble laget
	 * 
	 * @return
	 */
	public File getGeneratedReportFile() {
		return file;
	}

	/**
	 * Returnerer parameterene som kan sendes til jasper
	 * 
	 * @return
	 */
	public Map<String, Object> getParameters() {
		Map<String, Object> params = new HashMap<String, Object>();
		if (parameters != null) {
			for (ReportParameter param : parameters) {
				ReportParameterValue paramValue = parameterValues.get(param);
				logger.warn("Setting " + param.getName() + " to " + paramValue.getValue());
				params.put(param.getName(), paramValue.getValue());
			}
		} else {
			params = new HashMap<String, Object>();
		}

		return params;
	}

	public boolean hasErrors() {
		return errors;
	}

	private void setErrors(boolean flag) {
		errors = flag;
	}

	/** {@inheritDoc} */
	public void windowActivated(WindowEvent e) {
	}

	/** {@inheritDoc} */
	public void windowClosed(WindowEvent e) {
		removeWindowListener(this);
		if (abort) {
			file = null;
			if (buildReportTask != null) {
				buildReportTask.cancel();
			}
		}

		buildReportTask = null;
	}

	/** {@inheritDoc} */
	public void windowClosing(WindowEvent e) {
		abort = true;
		dispose();
	}

	/** {@inheritDoc} */
	public void windowDeactivated(WindowEvent e) {
	}

	/** {@inheritDoc} */
	public void windowDeiconified(WindowEvent e) {
	}

	/** {@inheritDoc} */
	public void windowIconified(WindowEvent e) {
	}

	/** {@inheritDoc} */
	public void windowOpened(WindowEvent e) {
	}

	/**
	 * Starter JASPER
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class ReportBuilderTask extends SwingWorker<Void, Void> {
		private boolean cancelled;
		private File reportFile;

		public void cancel() {
			cancelled = true;
			cancel(true);
		}

		/* {@inheritDoc} */
		@Override
		protected Void doInBackground() throws Exception {
			try {
				reportFile = ReportHelper.buildReport(report.getJasperFileName(), dataSource, exportType,
						getParameters());
			} catch (Throwable e) {
				e.printStackTrace();
				if (!isCancelled()) {
					setErrors(true);
					logger.error("Report generation failed", e);
					String msg = BUNDLE.getGuiString("feilVedRapportGenerering");
					String title = BUNDLE.getGuiString("feilVedRapportGenerering.title");
					JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.ERROR_MESSAGE);
				}
			}

			return null;
		}
		
		/* {@inheritDoc} */
		@Override
		protected void done() {
			if (!cancelled) {
				file = reportFile;
				abort = false;
				dispose();
			} else {
				reportFile = null;
			}
		}
	}
}
