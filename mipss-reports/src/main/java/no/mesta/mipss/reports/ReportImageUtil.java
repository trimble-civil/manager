package no.mesta.mipss.reports;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.PixelGrabber;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;

import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hjelpeklasse for håndtering av bilder
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportImageUtil {
    private static final Logger logger = LoggerFactory.getLogger(ReportImageUtil.class);
    public static final int IMAGE_JPEG = 0;
	public static final int IMAGE_PNG = 1;

    
    /**
     * Lager et buffered image av et array av bytes
     * 
     * @param bytes
     * @return
     */
    public static BufferedImage bytesToImage(byte[] bytes) {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        BufferedImage image = null;
        try {
            image = ImageIO.read(bais);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
        	IOUtils.closeQuietly(bais);
        }
        
        return image;
    }
    
    public static BufferedImage bytesToImageJPEG(byte[] bytes) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        Iterator<ImageReader> readers = ImageIO.getImageReaders(bais);
        if (!readers.hasNext()){
        	throw new IllegalStateException("fant ikke bildeleser.");
        }
        ImageInputStream stream = ImageIO.createImageInputStream(bais);
        ImageReader reader = (ImageReader)readers.next();
        ImageReadParam param = reader.getDefaultReadParam();
        reader.setInput(stream, true, true);
        BufferedImage bi;
        try {
            bi = reader.read(0, param);
        } finally {
            reader.dispose();
            stream.close();
        }
        return bi;        
    }
    
    /**
     * Konverterer et image til et bufferedImage
     * @param img
     * @return
     */
    public static BufferedImage convertImageToBufferedImage(Image img) {
		if((img instanceof BufferedImage)) {
			return (BufferedImage)img;
		}
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bi.createGraphics();
		g2d.drawImage(img, 0, 0, null);
		g2d.dispose();
		return bi;
	}
    public static BufferedImage createBufferedImage(Image image, int type, int w, int h){
		if (type == ReportImageUtil.IMAGE_PNG && hasAlpha(image)) {
			type = BufferedImage.TYPE_INT_ARGB;
		}
		else {
			type = BufferedImage.TYPE_INT_RGB;
		}

		BufferedImage bi = new BufferedImage(w, h, type);

		Graphics g = bi.createGraphics();
		g.drawImage(image, 0, 0, w, h, null);
		g.dispose();
		
		return bi;
	}



    /**
     * Lager et buffered image av et MIPSS bilde
     * 
     * @param b
     * @return
     */
    public static BufferedImage createBufferedImage(Bilde b) {
        return b.getImage();
    }
    
    /**
     * Lager funn bilder til rapporter, og sørger for at de er stående
     * 
     * @param bilder
     * @return
     */
    public static List<ReportImage> createDiscoveryImageLandscapes(Collection<AvvikBilde> bilder) {
        List<ReportImage> result = new ArrayList<ReportImage>();
        for(AvvikBilde b:bilder) {
            BufferedImage img = ensureLandscapeMode(b.getBilde().getImage());
            result.add(new ReportImage(img, b));
        }
        
        return result;
    }

    /**
     * Lager bilder til rapporter, og sørger for at de er stående
     * 
     * @param bilder
     * @return
     */
    public static List<ReportImage> createImagePortraits(Collection<Bilde> bilder) {
        List<ReportImage> result = new ArrayList<ReportImage>();
        for(Bilde b:bilder) {
            BufferedImage img = createBufferedImage(b);
            img = ensurePortraitMode(img);
            result.add(new ReportImage(img, b.getFilnavn(), b.getBeskrivelse(), null));
        }
        
        return result;
    }
    
    /**
     * Sørger for at et bilde er "liggende" i landskaps modus
     * 
     * @param src
     * @return
     */
    public static BufferedImage ensureLandscapeMode(BufferedImage src) {
        BufferedImage dst = null;
        if(src.getHeight() > src.getWidth()) {
            dst = rotate90(src);
        } else {
            dst = src;
        }
        
        return dst;
    }

    /**
     * Sørger for at et bilde er "stående" i portrett modus
     * 
     * @param src
     * @return
     */
    public static BufferedImage ensurePortraitMode(BufferedImage src) {
        BufferedImage dst = null;
        if(src.getWidth() > src.getHeight()) {
            dst = rotate90(src);
        } else {
            dst = src;
        }
        
        return dst;
    }
    
    /**
     * Omgjør bilde data til en bytestrøm
     * 
     * @param image
     * @return bytestrøm for png
     */
    public static byte[] imageToBytes(Image image) {
//    	if(image instanceof ToolkitImage) {
//    		return imageToBytes((ToolkitImage) image);
//    	}
    	if (image instanceof RenderedImage){
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        try {
	            ImageIO.write((RenderedImage)image, "png", baos);
	        } catch (IOException e) {
	            throw new IllegalStateException(e.toString());
	        } finally {
	        	IOUtils.closeQuietly(baos);
	        }
	        
	        return baos.toByteArray();
    	}
    	else{
    		ByteArrayOutputStream bas = new ByteArrayOutputStream();
    		BufferedImage scaledBuff = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    		Graphics2D bufImageGraphics = scaledBuff.createGraphics();
    		bufImageGraphics.drawImage(image, 0, 0, null);
    		try {
    			ImageIO.write(scaledBuff, "png", bas);
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			IOUtils.closeQuietly(bas);
    		}
    		return bas.toByteArray();
    	}
    }

    /**
     * Gjør om et ToolkitImage til en byte array
     * @param image
     * @return
     */
//    public static byte[] imageToBytes(Image image){
//		ByteArrayOutputStream bas = new ByteArrayOutputStream();
//		BufferedImage scaledBuff = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
//		Graphics2D bufImageGraphics = scaledBuff.createGraphics();
//		bufImageGraphics.drawImage(image, 0, 0, null);
//		try {
//			ImageIO.write(scaledBuff, "png", bas);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			IOUtils.closeQuietly(bas);
//		}
//		
//    	return bas.toByteArray();
//    }
    
    /**
     * Lager et bilde objekt fra en fil i classpath
     * 
     * @param imageFile
     * @return
     */
    public static Bilde load(String imageFile) {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        InputStream stream = cl.getResourceAsStream(imageFile);
        
        if(stream == null) {
            // Slik at applikasjonen også virker utenfor webstart
            stream = ImageIcon.class.getResourceAsStream(imageFile);
        }
        
        if(stream == null) {
            logger.warn("Image not found " + imageFile);
            return null;
        } else {
            // Alle Exceptions må slukes, for vi kan ikke crashe Jasper
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                IOUtils.copy(stream, baos);
            } catch (IOException e) {
                logger.warn("Could not copy streams", e);
            } finally {
            	IOUtils.closeQuietly(stream);
            	IOUtils.closeQuietly(baos);
            }
            
            byte[] bytes = baos.toByteArray();
            
            Bilde b = new Bilde();
            b.setFilnavn(imageFile);
            b.setBildeLob(bytes);
            
            return b;
        }
    }
    
    public static ImageIcon padIcon(int left, int right, int top, int bottom, ImageIcon image){
    	
    	BufferedImage destImage = new BufferedImage(image.getIconWidth()+left+right, image.getIconHeight()+top+bottom, BufferedImage.TYPE_INT_ARGB);
    	Graphics2D g2 = destImage.createGraphics();
    	g2.drawImage(image.getImage(), left, top, null);
    	
    	ImageIcon destIcon = new ImageIcon (destImage);
    	return destIcon;
    }
    
    public static boolean hasAlpha(Image image)	{
		try {
			PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
			pg.grabPixels();
			
			return pg.getColorModel().hasAlpha();
		}
		catch (InterruptedException e) {
			return false;
		}
	}
    /**
     * Roterer et bilde 90 grader
     * 
     * @param src
     * @return
     */
    public static BufferedImage rotate90(BufferedImage src) {
        AffineTransform rotate = new AffineTransform();//.getRotateInstance(Math.toRadians(90d));
        rotate.rotate(Math.toRadians(90d), src.getHeight() /2, src.getHeight() /2);
        AffineTransformOp rotateOp = new AffineTransformOp(rotate, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        
        return rotateOp.filter(src, null);
    }
    
    public static Image scaleImage(ImageIcon image, int w, int h){
		
		if (image!=null){
			Image scaled = image.getImage().getScaledInstance(w, h, Image.SCALE_SMOOTH);
			return scaled;
		}
		return null;
	}
    public static Image scaleImageFast(ImageIcon image, int w, int h){
		
		if (image!=null){
			Image scaled = image.getImage().getScaledInstance(w, h, Image.SCALE_FAST);
			return scaled;
		}
		return null;
	}
    /**
     * Mulig denne skal ta i mot et ImageIcon og returnere et Image..
     *  
     * @param imageStr bildet..
     * @param w bredde
     * @param h høyde
     * @param compression fra 0-1 høyere gir mindre kompresjon og bedre kvalitet. 
     * @return
     */
    public static byte[] getScaledInstance(InputStream imageStr, int w, int h, float compression){
		try {
			BufferedImage read = ImageIO.read(imageStr);
			if (read!=null){ 
				int ww = read.getHeight()>read.getWidth()?Math.min(w, h):Math.max(w, h);
				int hh = read.getHeight()>read.getWidth()?Math.max(w, h):Math.min(w, h);
				w = ww;
				h = hh;
				Image scaled = read.getScaledInstance(w, h, Image.SCALE_SMOOTH);
				ByteArrayOutputStream bas = new ByteArrayOutputStream();
				BufferedImage scaledBuff = new BufferedImage(w, h, read.getType());
				Graphics2D bufImageGraphics = scaledBuff.createGraphics();
				bufImageGraphics.drawImage(scaled, 0, 0, null);
				
				Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpeg");
				if (!writers.hasNext())
					throw new IllegalStateException("fant ikke bildeskriver.");
				ImageWriter writer = writers.next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(bas);
				writer.setOutput(ios);
				ImageWriteParam param = writer.getDefaultWriteParam();
				if (compression >= 0) {
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(compression);
				}
				
				writer.write(null, new IIOImage(scaledBuff, null, null), param);
				ios.close();
				writer.dispose();
				
				byte[] data = bas.toByteArray();
				return data;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
    public static ByteArrayOutputStream compressImage(BufferedImage image, float compression){
    	try {
			ByteArrayOutputStream bas = new ByteArrayOutputStream();
			compressImage(image, compression, bas);
			return bas;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
    }
    
    public static void compressImage(BufferedImage image, float quality, OutputStream out) throws IOException {
        Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpeg");
        if (!writers.hasNext())
            throw new IllegalStateException("fant ikke bildeskriver.");
        ImageWriter writer = writers.next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);
        ImageWriteParam param = writer.getDefaultWriteParam();
        if (quality >= 0) {
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);
        }
        writer.write(null, new IIOImage(image, null, null), param);
        ios.close();
        writer.dispose();
    }
    
    public static BufferedImage compressImage(byte[] bytes, float compression){
    	try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));
			ByteArrayOutputStream out = new ByteArrayOutputStream(50000);
            compressImage(image, compression, out);
            return ImageIO.read(new ByteArrayInputStream(out.toByteArray()));
		} catch (IOException e) {
			e.printStackTrace();
		}

    	return null;
    }
    @SuppressWarnings("unchecked")
	public static void saveCompressedImage(BufferedImage image, File file) throws IOException	{
		ImageWriter writer = null;
		Iterator iter = ImageIO.getImageWritersByFormatName("JPEG");
		writer = (ImageWriter)iter.next();
		
		ImageOutputStream ios = ImageIO.createImageOutputStream(file);
		writer.setOutput(ios);
		
		ImageWriteParam iwparam =writer.getDefaultWriteParam();
		
		iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwparam.setCompressionQuality(0.7F);
		
		writer.write(null, new IIOImage(image, null, null), iwparam);
		
		ios.flush();
		writer.dispose();
		ios.close();
	}
    
//    public static void saveImage(BufferedImage image, File file, float compression){    	
//    	try {
//			Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("png");
//			if (!writers.hasNext())
//				throw new IllegalStateException("fant ikke bildeskriver.");
//			ImageWriter writer = writers.next();
//			ImageOutputStream ios = ImageIO.createImageOutputStream(file);
//			writer.setOutput(ios);
//			ImageWriteParam param = writer.getDefaultWriteParam();
//			if (compression >= 0) {
//				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//				param.setCompressionQuality(compression);
//			}
//			writer.write(null, new IIOImage(image, null, null), param);
//			ios.close();
//			writer.dispose();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    }
    /**
     * Konverterer et ImageIcon til et gråskala bilde
     * @param image
     * @return
     */
    public static ImageIcon toGrayScale(ImageIcon image){
    	BufferedImage srcImage =convertImageToBufferedImage(image.getImage());
    	ColorSpace grayColorSpace= ColorSpace.getInstance (ColorSpace.CS_GRAY);
    	ColorConvertOp grayOp =new ColorConvertOp (grayColorSpace, null);
    	BufferedImage destImage = grayOp.filter (srcImage, null);
    	ImageIcon destIcon = new ImageIcon (destImage);
    	return destIcon;
    }
}
