package no.mesta.mipss.reports;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

import net.sf.jasperreports.engine.JRAbstractRenderer;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.util.JRTypeSniffer;
import no.mesta.mipss.common.ImageUtil;

@SuppressWarnings("serial")
public class MipssImageRenderer extends JRAbstractRenderer{

	private final ImageIcon image;
	private byte imageType = IMAGE_TYPE_UNKNOWN;
	
	public MipssImageRenderer(ImageIcon image){
		this.image = image;
		if (image!=null){
			imageType = JRTypeSniffer.getImageType(ImageUtil.imageToBytes(image.getImage()));
		}
	}
	
	
	public Dimension2D getDimension() throws JRException {
		Dimension dimension = new Dimension(image.getIconWidth(), image.getIconHeight());
		System.out.println("getDimension:"+dimension);
		return new Dimension(221, 52);
	}

	public byte[] getImageData() throws JRException {
		return ImageUtil.imageToBytes(image.getImage());
	}

	public byte getImageType() {
		return imageType;
	}

	public byte getType() {
		return TYPE_IMAGE;
	}

	public void render(Graphics2D grx, Rectangle2D rectangle) throws JRException {
		int w = (int)rectangle.getWidth();
		int h = (int)rectangle.getHeight();
		Image scaleImage = ImageUtil.scaleImage(image, w, h);
		
		grx.drawImage(scaleImage, 
					(int)rectangle.getX(), 
					(int)rectangle.getY(), 
					(int)rectangle.getWidth(), 
					(int)rectangle.getHeight(), 
					null
		);
	}

}
