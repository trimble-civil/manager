package no.mesta.mipss.reports;

import java.util.Collections;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Property;

/**
 * Laster bilder for en gitt hendelse
 * 
 * @author ararnt
 */
public class HendelseBildeDS implements JRDataSource {
	private final String hguid;
	private boolean inited;
	private List<Bilde> bilder;
	private MipssFelt bean;
	private Bilde bilde;
	private int itemIndex = -1;
	private static final Logger logger = LoggerFactory.getLogger(HendelseBildeDS.class);
	
	public HendelseBildeDS(String hguid) {
		this.hguid = hguid;
	}
	
	public HendelseBildeDS(String hguid, MipssFelt bean) {
		this.hguid = hguid;
		this.bean = bean;
	}


	/** {@inheritDoc} */
	public Object getFieldValue(JRField field) throws JRException {
		logger.debug("getFieldValue(" + field.getName() + ") start");
		
		Property<Bilde, ?> property = BeanProperty.create(field.getName());
		Object value = property.getValue(bilde);
		
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}

	/** {@inheritDoc} */
	public boolean next() throws JRException {
		boolean hasNext = false;
		bilde = null;
		
		if(!inited) {
			loadData();
		}
		
		if(itemIndex != -1 && !bilder.isEmpty()) {
			if(itemIndex <= bilder.size() -1 ) {
				bilde = bilder.get(itemIndex);
				itemIndex++;
			}
		}
		
		hasNext = bilde != null && itemIndex <= (bilder.size());
		
		return hasNext;
	}

	private void loadData() {
		bilder = bean.hentHendelseBilder(hguid);
		logger.debug("loadData() loaded:"+(bilder!=null?bilder.size():0)+" bilder");
		Collections.sort(bilder);
		inited = true;
		
		if(bilder != null) {
			itemIndex = 0;
		}
	}

	public void setBean(MipssFelt bean) {
		this.bean = bean;
	}
}
