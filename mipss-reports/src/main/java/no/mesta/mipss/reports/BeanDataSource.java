package no.mesta.mipss.reports;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Property;

public class BeanDataSource<E> implements JRDataSource{

	private final E entity;
	private int count = -1;
	public BeanDataSource(E entity){
		this.entity = entity;
	}
	
	public Object getFieldValue(JRField field) throws JRException {
		Property<E, ?> property = BeanProperty.create(field.getName());
		System.out.println("name:"+field.getName());
		Object value = property.getValue(entity);
		System.out.println(field.getName()+"\t =  \t"+value);
		return value;
	}
	
	public boolean next() throws JRException {
		count++;
		return count<1;
	}
}
