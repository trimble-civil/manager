package no.mesta.mipss.reports;

import java.util.Collections;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Property;

/**
 * Laster bilder for et gitt funn
 * 
 * @author ararnt
 */
public class PunktregBildeDS implements JRDataSource {
	private final String pguid;
	private boolean inited;
	private List<AvvikBilde> bilder;
	private MipssField bean;
	private AvvikBilde punktregBilde;
	private int itemIndex = -1;
	private static final Logger logger = LoggerFactory.getLogger(PunktregBildeDS.class);
	
	public PunktregBildeDS(String pguid) {
		this.pguid = pguid;
	}
	
	public PunktregBildeDS(String pguid, MipssField bean) {
		this.pguid = pguid;
		this.bean = bean;
	}


	/** {@inheritDoc} */
	public Object getFieldValue(JRField field) throws JRException {
		logger.debug("getFieldValue(" + field.getName() + ") start");
		
		Property<AvvikBilde, ?> property = BeanProperty.create(field.getName());
		Object value = property.getValue(punktregBilde);
		
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}

	/** {@inheritDoc} */
	public boolean next() throws JRException {
		boolean hasNext = false;
		punktregBilde = null;
		
		if(!inited) {
			loadData();
		}
		
		if(itemIndex != -1 && !bilder.isEmpty()) {
			if(itemIndex <= bilder.size() -1 ) {
				punktregBilde = bilder.get(itemIndex);
				itemIndex++;
			}
		}
		
		hasNext = punktregBilde != null && itemIndex <= (bilder.size());
		logger.debug("next() done with " + hasNext );
		return hasNext;
	}

	private void loadData() {
		bilder = bean.hentPunktregBildeInfo(pguid);
		Collections.sort(bilder);
		inited = true;
		
		if(bilder != null) {
			itemIndex = 0;
		}
		
		logger.debug("loadData() done with " + (bilder != null ? String.valueOf(bilder.size()) : "null"));
	}

	public void setBean(MipssField bean) {
		this.bean = bean;
	}
}
