package no.mesta.mipss.reports;

import net.sf.jasperreports.engine.JRRenderable;
import no.mesta.mipss.resources.images.IconResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bilder som kan brukes i jasper reports og tilsvarende
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportImages {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ReportImages.class);
	private static final ReportImages resource = new ReportImages();
	public static final JRRenderable MESTA_SORT_HVIT_LOGO = resource.load2("images/hovedlogo_sort.png");
	public static final JRRenderable MESTA_HOVEDLOGO = resource.load2("images/hovedlogo_farge.jpg");
	public static final JRRenderable SVV_HOVEDLOGO = resource.load2("images/svv_logo.png");
	
	public JRRenderable load2(String imageFile){
		return new MipssImageRenderer(IconResources.load(imageFile));
	}
		
	/**
	 * Logo
	 * 
	 */
	public JRRenderable getMestaSortHvitLogo() {
		return MESTA_SORT_HVIT_LOGO;
	}
	public JRRenderable getMestaFargeLogo() {
		return MESTA_SORT_HVIT_LOGO;
	}
	public JRRenderable getSvvLogo() {
		return MESTA_SORT_HVIT_LOGO;
	}
}
