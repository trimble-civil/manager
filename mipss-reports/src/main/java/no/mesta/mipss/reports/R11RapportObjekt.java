package no.mesta.mipss.reports;

import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.r.r11.SkredEgenskapverdi;
/**
 * Rapportobjekt for R11 rapporten. 
 * 
 * Arver fra Skred for å unngå endringer i rapportgenereringsrammeverket.
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class R11RapportObjekt extends Skred{
	private static final PropertyResourceBundleUtil BUNDLE = PropertyResourceBundleUtil.getPropertyBundle("reports");

	private final Skred skred;
	
	public R11RapportObjekt(Skred skred){
		this.skred = skred;
	}
	
	public Date getRegistrertDato(){
		return skred.getOpprettetDato();
	}
	public String getRegistrertAv(){
		return skred.getOpprettetAv();
	}
	public String getFylke(){
		return skred.getSak().getVeiref().getFylkesnummer()+"";
	}
	public String getVei(){
		Punktveiref v= skred.getSak().getVeiref();
		StringBuilder sb = new StringBuilder(v.getVeikategori());
		sb.append(v.getVeistatus());
		sb.append(v.getVeinummer());
		return sb.toString();
	}
	public String getFraHPm(){
		Punktveiref v= skred.getSak().getStedfesting().get(0).getVeiref();
		StringBuilder sb = new StringBuilder();
		sb.append(v.getHp());
		sb.append(" / ");
		sb.append(v.getMeter());
		return sb.toString();
	}
	public String getTilHPm(){
		if (skred.getSak().getStedfesting().size()>1){
			Punktveiref v= skred.getSak().getStedfesting().get(1).getVeiref();
			StringBuilder sb = new StringBuilder();
			sb.append(v.getHp());
			sb.append(" / ");
			sb.append(v.getMeter());
			return sb.toString();
		}return null;
	}
	public String getStedsnavn(){
		return skred.getSak().getSkadested();
	}
	public Date getSkreddato(){
		return skred.getSkredDato();
	}
	
	public boolean isHoyreFelt(){
		return true;
	}
	public boolean isVenstreFelt(){
		return true;
	}
	public String getKoordinatFra(){
		Punktstedfest ps = skred.getSak().getStedfesting().get(0);
		StringBuilder ne = new StringBuilder("N:");
		ne.append(Math.round(ps.getY()));
		ne.append(" ");
		ne.append(BUNDLE.getGuiString("east"));
		ne.append(":");
		ne.append(Math.round(ps.getX()));
		return ne.toString();
	}
	public String getKoordinatTil(){
		if (skred.getSak().getStedfesting().size()>1){
			Punktstedfest ps = skred.getSak().getStedfesting().get(1);
			StringBuilder ne = new StringBuilder("N:");
			ne.append(Math.round(ps.getY()));
			ne.append(" ");
			ne.append(BUNDLE.getGuiString("east"));
			ne.append(":");
			ne.append(Math.round(ps.getX()));
		return ne.toString();
		}
		return null;
	}
	public Integer getAnslagSkredTotal(){
		return skred.getAnslagTotalSkredstorrelse();
	}
	public Double getTemperatur(){
		return skred.getTemperatur();
	}
	public Boolean getStengtPgaSkredfare(){
		return skred.getStengtPgaSkredfare();
	}
	public Boolean getGjelderGS(){
		return skred.getGjelderGS();
	}
	public String getKommentar(){
		return skred.getKommentar();
	}
	public Date getStengtDato(){
		return skred.getStengtDato();
	}
	public Date getAapnetDato(){
		return skred.getAapnetDato();
	}
	
	public Integer getAntallBilder(){
		if (skred.getAntallBilder()==null)
			return 0;
		return skred.getAntallBilder();
	}
	public List<Bilde> getBilder(){
		return skred.getBilder();
	}
	public String getGuid(){
		return skred.getGuid();
	}
	/**
	 * Sjekker om en Egenskapverdi med angitt id er lagt til i skredets egenskapliste
	 * @param id
	 * @return
	 */
	public boolean getEgenskapverdi(int id){
		List<SkredEgenskapverdi> skredegenskapList = skred.getSkredEgenskapList();
		for (SkredEgenskapverdi e:skredegenskapList){
			if (e.getEgenskapverdi().getId().intValue()==id){
				return true;
			}
		}
		return false;
	}
	/**
	 * Henter verdien til en Egenskap med gitt id
	 * @param id
	 * @return
	 */
	public String getVerdiForEgenskap(int id){
		List<SkredEgenskapverdi> skredegenskapList = skred.getSkredEgenskapList();
		for (SkredEgenskapverdi e:skredegenskapList){
			if (e.getEgenskapverdi().getEgenskap().getId().intValue()==id){
				return e.getEgenskapverdi().getVerdi();
			}
		}
		return null;
	}
	public Boolean getIngenNedbor(){
		List<SkredEgenskapverdi> skredegenskapList = skred.getSkredEgenskapList();
		for (SkredEgenskapverdi e:skredegenskapList){
			if (e.getEgenskapverdi().getEgenskap().getId().intValue()==9){
				return false;
			}
			if (e.getEgenskapverdi().getEgenskap().getId().intValue()==10){
				return false;
			}
		}
		return true;
	}
	/** DATA FRA VÆRSTASJONER, valgfrie felter i rapporten */
	public Double getRegnVS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getRegnMmPrDag():null;
	}
	public Double getVatsnoVS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getSluddMmPrDag():null;
	}
	public Double getSnoVS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getSnoMmPrDag():null;
	}
	public String getVindretningVS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getVindretning():null;
	}
	public Double getVindstyrkeVS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getVindstyrkeMPrSek():null;
	}
	public Double getTemperaturVS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getTemperatur():null;
	}
	public Double getNedbor3VS(){
		return skred.getVaerFraVaerstasjon()!=null?skred.getVaerFraVaerstasjon().getMmNedborSiste3Dogn():null;
	}
	public String toString(){
		return "Skred rapportobjekt...."+getRefnummer();
	}
	
}
