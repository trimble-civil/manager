package no.mesta.mipss.reports;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.common.PropertyChangeSource;

/**
 * Holder verdier før de sendes til Jasper
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportParameterValue implements Serializable, PropertyChangeSource {

	private static final Logger log = LoggerFactory.getLogger(ReportParameterValue.class);

	private transient PropertyChangeSupport props;
	private transient Logger logger;
	private Object value;
	
	/** {@inheritDoc} */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof ReportParameterValue)) {
			return false;
		}

		return new EqualsBuilder().append(value, ((ReportParameterValue) o).value).isEquals();
	}

	private PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}

	public Object getValue() {
		return value;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(value).toHashCode();
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}

	public void setValue(Object value) {
		Object old = this.value;
		this.value = value;
		getProps().firePropertyChange("value", old, value);
		log.debug("setValue(" + old + "," + value +")");
	}
}
