package no.mesta.mipss.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.persistence.MipssEntityBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Datakilde for jasper rapporter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <E>
 */
public class MipssFeltDS<E extends MipssEntityBean<E>> implements JRDataSource {
	private static final Logger logger = LoggerFactory.getLogger(MipssFieldDS.class);
	private int currentEntity;
	private int currentPage;
	private List<E> entities;
	protected E entity;
	protected MipssFelt fieldBean;
	private final List<String> guids;
	private int pageSize = 5;
	private Class<E> clazz;
	private boolean inited;
	
	public MipssFeltDS(Class<E> clazz, List<String> guids) {
		this.clazz = clazz;
		this.guids = guids;
	}

	public MipssFeltDS(Class<E> clazz, List<String> guids, MipssFelt fieldBean) {
		this.clazz = clazz;
		this.guids = guids;
		this.fieldBean = fieldBean;
	}

	/**
	 * Teller i siden til elementet som behandles
	 * 
	 * @return
	 */
	public int getCurrentEntity() {
		return currentEntity;
	}

	/**
	 * Siden i datasettet som behandles
	 * 
	 * @return
	 */
	public int getCurrentPage() {
		return currentPage;
	}
	
	/**
	 * Data access
	 * 
	 * @return
	 */
	public MipssFelt getFieldBean() {
		return fieldBean;
	}

	/** {@inheritDoc} */
	public Object getFieldValue(JRField field) throws JRException {
		logger.debug("getFieldValue(" + field.getName() + ") start");
		Object value = entity.getFieldValue(field.getName());
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}

	/**
	 * Antall sider i datakilden
	 * 
	 * @return
	 */
	public int getNumberOfPages() {
		return guids.size() / pageSize;
	}
	
	/**
	 * Antall elementer i hver side
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * Angir om den er på siste side
	 * 
	 * @return
	 */
	public boolean isLastPage() {
		return currentPage < getNumberOfPages()- 1;
	}
	
	/**
	 * Laster sidevis med data fra databasen
	 * 
	 */
	protected void loadPageData() {
		entities = null;
		
		List<String> pageOfIds = new ArrayList<String>();
		int currentPageStart = pageSize * currentPage;
		int currentPageEnd = currentPageStart + pageSize;
		for(int c = currentPageStart; c < guids.size() && c < currentPageEnd; c++) {
			pageOfIds.add(guids.get(c));
		}
		
		if(!pageOfIds.isEmpty()) {
			entities = fieldBean.hentObjekter(clazz, pageOfIds);
		}
		
		
		if(entities == null) {
			entities = new ArrayList<E>();
		}
	}

	
	/** {@inheritDoc} */
	public boolean next() throws JRException {
		logger.debug("next() start");
		entity = null;
		
		if(!inited) {
			loadPageData();
			inited = true;
		}
		
		
		if(currentEntity == getPageSize() && ((currentPage * pageSize) + currentEntity < guids.size())) {
			currentPage++;
			currentEntity = 0;
			loadPageData();
		}
		
		if(currentEntity < entities.size()) {
			entity = entities.get(currentEntity);
			logger.debug("Current entity is: " + entity);
			currentEntity++;
		}
		
		boolean hasNext = (entity != null) && ((currentPage * pageSize) + currentEntity <= guids.size());
		logger.debug("next() done with " + hasNext);
		return hasNext;
	}
	public void setEntity(E entity){
		this.entity = entity;
	}
	/**
	 * Data access
	 * 
	 * @param fieldBean
	 */
	public void setFieldBean(MipssFelt fieldBean) {
		this.fieldBean = fieldBean;
	}
	
	protected List<String> getGuids(){
		return guids;
	}
}
