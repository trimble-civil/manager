package no.mesta.mipss.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.persistence.mipssfield.Hendelse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HendelseDS extends MipssFeltDS<Hendelse> {
	private static final Logger logger = LoggerFactory.getLogger(MipssFieldDS.class);
	private int currentEntity;
	private int currentPage;
	private List<HendelseRapportObjekt> entities;
	private Hendelse entity;
	private int pageSize = 5;
	private boolean inited;
	
	public HendelseDS(List<String> guids) {
		super(Hendelse.class, guids);
	}
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		logger.debug("getFieldValue(" + field.getName() + ") start");
		Object value = entity.getFieldValue(field.getName());
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}
	/**
	 * Laster sidevis med data fra databasen
	 * 
	 */
	@Override
	protected void loadPageData() {
		entities = null;
		
		List<String> pageOfIds = new ArrayList<String>();
		int currentPageStart = pageSize * currentPage;
		int currentPageEnd = currentPageStart + pageSize;
		for(int c = currentPageStart; c < getGuids().size() && c < currentPageEnd; c++) {
			pageOfIds.add(getGuids().get(c));
		}
		
		if(!pageOfIds.isEmpty()) {
			entities = new ArrayList<HendelseRapportObjekt>();
			for (String guid:pageOfIds){
				Hendelse h = fieldBean.hentHendelseRapport(guid);
				entities.add(new HendelseRapportObjekt(h));
			}
		}
		
		if(entities == null) {
			entities = new ArrayList<HendelseRapportObjekt>();
		}
	}

	@Override
	public boolean next() throws JRException {
		logger.debug("next() start");
		entity = null;
		
		if(!inited) {
			loadPageData();
			inited = true;
		}
		
		
		if(currentEntity == getPageSize() && ((currentPage * pageSize) + currentEntity < getGuids().size())) {
			currentPage++;
			currentEntity = 0;
			loadPageData();
		}
		
		if(currentEntity < entities.size()) {
			entity = entities.get(currentEntity);
			logger.debug("Current entity is: " + entity);
			currentEntity++;
		}
		
		boolean hasNext = (entity != null) && ((currentPage * pageSize) + currentEntity <= getGuids().size());
		logger.debug("next() done with " + hasNext);
		return hasNext;
	}
	@Override
	public void setEntity(Hendelse entity){
		this.entity = entity;
	}
}
