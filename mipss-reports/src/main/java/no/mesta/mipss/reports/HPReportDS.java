package no.mesta.mipss.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import no.mesta.mipss.core.PropertyUtil;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.vo.HPReport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Property;

/**
 * Datakilde for jasper rapporter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 * @param <E>
 */
public class HPReportDS implements JRDataSource {
	private static final Logger logger = LoggerFactory.getLogger(HPReportDS.class);
	private int currentEntity;
	private int currentPage;
	private List<HPReport> entities;
	private HPReport entity;
	private MipssField fieldBean;
	private final List<String> guids;
	private int pageSize = 5;
	private boolean inited;
	
	public HPReportDS(List<String> guids) {
		this.guids = guids;
	}

	/**
	 * Teller i siden til elementet som behandles
	 * 
	 * @return
	 */
	public int getCurrentEntity() {
		return currentEntity;
	}

	/**
	 * Siden i datasettet som behandles
	 * 
	 * @return
	 */
	public int getCurrentPage() {
		return currentPage;
	}
	
	/**
	 * Data access
	 * 
	 * @return
	 */
	public MipssField getFieldBean() {
		return fieldBean;
	}

	/** {@inheritDoc} */
	public Object getFieldValue(JRField field) throws JRException {
		logger.debug("getFieldValue(" + field.getName() + ") start");
		Property<HPReport, ?> property = PropertyUtil.createProperty(field.getName());
		Object value = property.getValue(entity);
		logger.debug("getFieldValue(" + field.getName() + ") done with " + value);
		return value;
	}

	/**
	 * Antall sider i datakilden
	 * 
	 * @return
	 */
	public int getNumberOfPages() {
		return guids.size() / pageSize;
	}
	
	/**
	 * Antall elementer i hver side
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * Angir om den er på siste side
	 * 
	 * @return
	 */
	public boolean isLastPage() {
		return currentPage < getNumberOfPages()- 1;
	}
	
	/**
	 * Laster sidevis med data fra databasen
	 * 
	 */
	private void loadPageData() {
		entities = null;
		
		List<String> pageOfIds = new ArrayList<String>();
		int currentPageStart = pageSize * currentPage;
		int currentPageEnd = currentPageStart + pageSize;
		for(int c = currentPageStart; c < guids.size() && c < currentPageEnd; c++) {
			pageOfIds.add(guids.get(c));
		}
		
		if(!pageOfIds.isEmpty()) {
			entities = fieldBean.hentHPReport(pageOfIds);
		}
		
		if(entities == null) {
			entities = new ArrayList<HPReport>();
		}
	}

	/** {@inheritDoc} */
	public boolean next() throws JRException {
		logger.debug("next() start");
		entity = null;
		
		if(!inited) {
			loadPageData();
			inited = true;
		}
		int entitiesSize = entities.size();
		
		
		if(currentEntity == entitiesSize) {
			currentPage++;
			currentEntity = 0;
			loadPageData();
		}
		
		if(currentEntity < entities.size()) {
			entity = entities.get(currentEntity);
			logger.debug("Current entity is: " + entity);
			currentEntity++;
		}
		
		boolean hasNext = entity != null && currentPage <= getNumberOfPages();
		logger.debug("next() done with " + currentPage + ", " + hasNext);
		return hasNext;
	}

	/**
	 * Data access
	 * 
	 * @param fieldBean
	 */
	public void setFieldBean(MipssField fieldBean) {
		this.fieldBean = fieldBean;
	}
}
