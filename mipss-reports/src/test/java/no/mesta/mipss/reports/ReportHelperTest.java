package no.mesta.mipss.reports;

import junit.framework.TestCase;

import net.sf.jasperreports.engine.JasperReport;

/**
 * Tester report helperen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ReportHelperTest extends TestCase{
    public ReportHelperTest() {
    }
    
    public void testFindReport() {
        try {
            JasperReport report = ReportHelper.findReport("Funn.jasper");
            assertNotNull("The report is empty", report);
        } catch (Throwable e) {
            assertNotNull("It threw exception", e);
        }
    }
}
