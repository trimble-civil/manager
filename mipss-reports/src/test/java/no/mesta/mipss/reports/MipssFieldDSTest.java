package no.mesta.mipss.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRPropertiesHolder;
import net.sf.jasperreports.engine.JRPropertiesMap;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.mipssfield.Avvik;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

/**
 * Tester DS for felt
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssFieldDSTest extends MockObjectTestCase {
	private static final Logger logger = LoggerFactory.getLogger(MipssFieldDSTest.class);
	private final List<String> guids = new ArrayList<String>();
	private final List<String> page1 = new ArrayList<String>();
	private final List<String> page2 = new ArrayList<String>();
	private final List<Avvik> result1 = new ArrayList<Avvik>();
	private final List<Avvik> result2 = new ArrayList<Avvik>();

	public Avvik createFunn(String guid) {
		Avvik funn = new Avvik();

		funn.setGuid(guid);

		return funn;
	}

	private void fillResult(List<Avvik> result, int start, int stop) {
		for (int c = start; c < stop; c++) {
			result.add(createFunn("guid" + c));
		}
	}

	/** {@inheritDoc} */
	public void setUp() {
		logger.debug("setUp() start");
		for (int c = 0; c < 8; c++) {
			guids.add("guid" + String.valueOf(c));
		}

		for (int c = 0; c < 5; c++) {
			page1.add("guid" + String.valueOf(c));
		}

		for (int c = 5; c < 8; c++) {
			page2.add("guid" + String.valueOf(c));
		}

		fillResult(result1, 0, 5);
		fillResult(result2, 5, 8);
		
		logger.debug("setUp() guids: " + guids);
		logger.debug("setUp() page1: " + page1);
		logger.debug("setUp() page2: " + page2);
		logger.debug("setUp() result1: " + result1);
		logger.debug("setUp() result2: " + result2);
	}

	public void testFunnDS() {
		Mock mockedFieldBean = mock(MipssField.class);

		MipssFieldDS<Avvik> funnDS = new MipssFieldDS<Avvik>(Avvik.class, guids);
		funnDS.setFieldBean((MipssField) mockedFieldBean.proxy());

		mockedFieldBean.expects(once()).method("hentObjekter").with(eq(Avvik.class), eq(page1)).will(
				returnValue(result1));
		mockedFieldBean.expects(once()).method("hentObjekter").with(eq(Avvik.class), eq(page2)).will(
				returnValue(result2));

		try {
			int c = 0;
			while (funnDS.next()) {
				String receivedGuid = (String) funnDS.getFieldValue(new Field("guid"));
				
				assertEquals(guids.get(c), receivedGuid);
				c++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("caught exception", e);
			fail("caught exception");
		}
	}

	class Field implements JRField, Cloneable {
		private final String name;

		public Field(String name) {
			this.name = name;
		}

		/** {@inheritDoc} */
		@Override
		public Object clone() {
			return new Field(name);
		}

		/** {@inheritDoc} */
		public String getDescription() {
			// Not needed: Auto-generated method stub
			return null;
		}

		/** {@inheritDoc} */
		public String getName() {
			return name;
		}

		/** {@inheritDoc} */
		public JRPropertiesHolder getParentProperties() {
			// Not needed: Auto-generated method stub
			return null;
		}

		/** {@inheritDoc} */
		public JRPropertiesMap getPropertiesMap() {
			// Not needed: Auto-generated method stub
			return null;
		}

		/** {@inheritDoc} */
		public Class<?> getValueClass() {
			// Not needed: Auto-generated method stub
			return null;
		}

		/** {@inheritDoc} */
		public String getValueClassName() {
			// Not needed: Auto-generated method stub
			return null;
		}

		/** {@inheritDoc} */
		public boolean hasProperties() {
			// Not needed: Auto-generated method stub
			return false;
		}

		/** {@inheritDoc} */
		public void setDescription(String description) {
			// Not needed: Auto-generated method stub
		}
	}
}
