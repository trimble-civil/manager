@echo off
echo ROBOT FRAMEWORK STEPS INTO ACTION

set CP=%1
set LEVEL=0
set ERRORS=0
shift

:loop
	if %LEVEL%==2 call :set_test %1
	if %LEVEL%==1 call :set_target %1
	if %LEVEL%==0 call :set_cp %1
	if "%1" == "-d" call :done_cp
	:next
	shift
	if ""%1"" == """" goto test
	goto loop

:test
echo READY TO EXECUTE TEST {%TEST%} TARGET IS {%TARGET%}
set CLASSPATH=%CP%

call jybot -d %TARGET% %TEST%
set ERRORS=%errorlevel%

:: Slutt p� script
goto :mainEnd

:: Funksjoner kan legges til herunder
:set_cp
	set CP=%CP%;%1
	goto :EOF

:set_target
	echo targeting %1
	set TARGET=%1
	set LEVEL=2
	goto :EOF

:done_cp
	set LEVEL=1
	goto :EOF

:set_test
	set TEST=%1
	goto :EOF
	
:: Slutt p� funksjoner
goto :EOF

:mainEnd
:: Beyond here lies monsters
echo RESULT: %ERRORS%
echo 0 	All critical tests passed.
echo 1-249 	Returned number of critical tests failed.
echo 250 	250 or more critical failures.
echo 251 	Help or version information printed.
echo 252 	Invalid test data or command line options.
echo 253 	Test execution stopped by user.
echo 255 	Unexpected internal error.
exit %ERRORS%