package no.mesta.mipss.common.property;

/**
 * Interface for accessing property files
 * 
 * @author Christian Wiik (Mesan)
 */
public interface PropertyAccessor {
		
	/**
	 * Get the current value of the specified property as a <tt>String</tt>.
	 * 
	 * @param propertyName - object containing the name of the property.
	 * @return the value of the property as a <tt>String</tt>.
	 */
	public String getString(PropertyName propertyName);
	
	/**
	 * Get the current value of the specified property as a <tt>String</tt>.
	 * 
	 * @param propertyName - String containing the name of the property.
	 * @return the value of the property as a <tt>String</tt>.
	 */
	public String getString(String propertyName);
	
	/**
	 * Get the current value of the specified property as an <tt>int</tt>.
	 * 
	 * @param propertyName - object containing the name of the property.
	 * @return the value of the property as an <tt>int</tt>.
	 */
	public int getInt(PropertyName propertyName);
	
	/**
	 * Get the current value of the specified property as an <tt>int</tt>.
	 * 
	 * @param propertyName - String containing the name of the property.
	 * @return the value of the property as an <tt>int</tt>.
	 */
	public int getInt(String propertyName);
	
	/**
	 * Get the multiple values of the specified property as a <tt>String[]</tt> array.
	 * 
	 * @param propertyName - String containing the name of the property.
	 * @return the multiple values of the property as a <tt>String[]</tt> array.
	 */
	public String[] getStringArray(String propertyName);

}