package no.mesta.mipss.common;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.PixelGrabber;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;


/**
 * Hjelpeklasse for håndtering av bilder. Klassen er en derivat av ImageUtil som ligger i mipss-persistence, og er replikert hit
 * for å unngå potensielle problemer med refactorering i forbindelse med Beansbinding. Klassen inneholder ikke referanser til andre 
 * Mipss-domeneklasser.  
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class ImageUtil {
    public static final int IMAGE_JPEG = 0;
	public static final int IMAGE_PNG = 1;

    
    /**
     * Lager et buffered image av et array av bytes
     * 
     * @param bytes
     * @return
     */
    public static BufferedImage bytesToImage(byte[] bytes) {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        BufferedImage image = null;
        try {
            image = ImageIO.read(bais);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
        	IOUtils.closeQuietly(bais);
        }
        
        return image;
    }
    
    public static BufferedImage bytesToImageJPEG(byte[] bytes) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        Iterator<ImageReader> readers = ImageIO.getImageReaders(bais);
        if (!readers.hasNext()){
        	throw new IllegalStateException("fant ikke bildeleser.");
        }
        ImageInputStream stream = ImageIO.createImageInputStream(bais);
        ImageReader reader = (ImageReader)readers.next();
        ImageReadParam param = reader.getDefaultReadParam();
        reader.setInput(stream, true, true);
        BufferedImage bi;
        try {
            bi = reader.read(0, param);
        } finally {
            reader.dispose();
            stream.close();
        }
        return bi;        
    }
    
    /**
     * Konverterer et image til et bufferedImage
     * @param img
     * @return
     */
    public static BufferedImage convertImageToBufferedImage(Image img) {
		if((img instanceof BufferedImage)) {
			return (BufferedImage)img;
		}
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bi.createGraphics();
		g2d.drawImage(img, 0, 0, null);
		g2d.dispose();
		return bi;
	}
    public static BufferedImage createBufferedImage(Image image, int type, int w, int h){
		if (type == ImageUtil.IMAGE_PNG && hasAlpha(image)) {
			type = BufferedImage.TYPE_INT_ARGB;
		}
		else {
			type = BufferedImage.TYPE_INT_RGB;
		}

		BufferedImage bi = new BufferedImage(w, h, type);

		Graphics g = bi.createGraphics();
		g.drawImage(image, 0, 0, w, h, null);
		g.dispose();
		
		return bi;
	}

    /**
     * Sørger for at et bilde er "liggende" i landskaps modus
     * 
     * @param src
     * @return
     */
    public static BufferedImage ensureLandscapeMode(BufferedImage src) {
        BufferedImage dst = null;
        if(src.getHeight() > src.getWidth()) {
            dst = rotate90(src);
        } else {
            dst = src;
        }
        
        return dst;
    }

    /**
     * Sørger for at et bilde er "stående" i portrett modus
     * 
     * @param src
     * @return
     */
    public static BufferedImage ensurePortraitMode(BufferedImage src) {
        BufferedImage dst = null;
        if(src.getWidth() > src.getHeight()) {
            dst = rotate90(src);
        } else {
            dst = src;
        }
        
        return dst;
    }
    
    /**
     * Omgjør bilde data til en bytestrøm
     * 
     * @param image
     * @return bytestrøm for png
     */
    public static byte[] imageToBytes(Image image) {
//    	if(image instanceof ToolkitImage) {
//    		return imageToBytes((ToolkitImage) image);
//    	}
    	if (image instanceof RenderedImage){
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        try {
	            ImageIO.write((RenderedImage)image, "png", baos);
	        } catch (IOException e) {
	            throw new IllegalStateException(e.toString());
	        } finally {
	        	IOUtils.closeQuietly(baos);
	        }
	        
	        return baos.toByteArray();
    	}
    	else{
    		ByteArrayOutputStream bas = new ByteArrayOutputStream();
    		BufferedImage scaledBuff = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    		Graphics2D bufImageGraphics = scaledBuff.createGraphics();
    		bufImageGraphics.drawImage(image, 0, 0, null);
    		try {
    			ImageIO.write(scaledBuff, "png", bas);
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			IOUtils.closeQuietly(bas);
    		}
    		return bas.toByteArray();
    	}
    }

    /**
     * Gjør om et ToolkitImage til en byte array
     * @param image
     * @return
     */
//    public static byte[] imageToBytes(Image image){
//		ByteArrayOutputStream bas = new ByteArrayOutputStream();
//		BufferedImage scaledBuff = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
//		Graphics2D bufImageGraphics = scaledBuff.createGraphics();
//		bufImageGraphics.drawImage(image, 0, 0, null);
//		try {
//			ImageIO.write(scaledBuff, "png", bas);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			IOUtils.closeQuietly(bas);
//		}
//		
//    	return bas.toByteArray();
//    }
    
    public static ImageIcon padIcon(int left, int right, int top, int bottom, ImageIcon image){
    	
    	BufferedImage destImage = new BufferedImage(image.getIconWidth()+left+right, image.getIconHeight()+top+bottom, BufferedImage.TYPE_INT_ARGB);
    	Graphics2D g2 = destImage.createGraphics();
    	g2.drawImage(image.getImage(), left, top, null);
    	
    	ImageIcon destIcon = new ImageIcon (destImage);
    	return destIcon;
    }
    
    public static boolean hasAlpha(Image image)	{
		try {
			PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
			pg.grabPixels();
			
			return pg.getColorModel().hasAlpha();
		}
		catch (InterruptedException e) {
			return false;
		}
	}
    /**
     * Roterer et bilde 90 grader
     * 
     * @param src
     * @return
     */
    public static BufferedImage rotate90(BufferedImage src) {
        AffineTransform rotate = new AffineTransform();//.getRotateInstance(Math.toRadians(90d));
        rotate.rotate(Math.toRadians(90d), src.getHeight() /2, src.getHeight() /2);
        AffineTransformOp rotateOp = new AffineTransformOp(rotate, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        
        return rotateOp.filter(src, null);
    }
    
    public static Image scaleImage(ImageIcon image, int w, int h){
		
		if (image!=null){
			Image scaled = image.getImage().getScaledInstance(w, h, Image.SCALE_SMOOTH);
			new ImageIcon(scaled);
			return scaled;
		}
		return null;
	}
    
    /**
     * Convenience method that returns a scaled instance of the
     * provided {@code BufferedImage}.
     *
     * @param img the original image to be scaled
     * @param targetWidth the desired width of the scaled instance,
     *    in pixels
     * @param targetHeight the desired height of the scaled instance,
     *    in pixels
     * @param hint one of the rendering hints that corresponds to
     *    {@code RenderingHints.KEY_INTERPOLATION} (e.g.
     *    {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC})
     * @param higherQuality if true, this method will use a multi-step
     *    scaling technique that provides higher quality than the usual
     *    one-step technique (only useful in downscaling cases, where
     *    {@code targetWidth} or {@code targetHeight} is
     *    smaller than the original dimensions, and generally only when
     *    the {@code BILINEAR} hint is specified)
     * @return a scaled version of the original {@code BufferedImage}
     */
    public static BufferedImage getScaledInstance(BufferedImage img,
                                           int targetWidth,
                                           int targetHeight,
                                           Object hint,
                                           boolean higherQuality)
    {
        int type = (img.getTransparency() == Transparency.OPAQUE) ?
            BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage)img;
        int w, h;
        if (higherQuality) {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
            h = img.getHeight();
        } else {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() call
            w = targetWidth;
            h = targetHeight;
        }
        
        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }
    
    /**
     * Mulig denne skal ta i mot et ImageIcon og returnere et Image..
     *  
     * @param imageStr bildet..
     * @param w bredde
     * @param h høyde
     * @param compression fra 0-1 høyere gir mindre kompresjon og bedre kvalitet. 
     * @return
     */
    public static byte[] getScaledInstance(InputStream imageStr, int w, int h, float compression){
		try {
			BufferedImage read = ImageIO.read(imageStr);
			if (read!=null){ 
				int ww = read.getHeight()>read.getWidth()?Math.min(w, h):Math.max(w, h);
				int hh = read.getHeight()>read.getWidth()?Math.max(w, h):Math.min(w, h);
				w = ww;
				h = hh;
				Image scaled = read.getScaledInstance(w, h, Image.SCALE_SMOOTH);
				ByteArrayOutputStream bas = new ByteArrayOutputStream();
				BufferedImage scaledBuff = new BufferedImage(w, h, read.getType());
				Graphics2D bufImageGraphics = scaledBuff.createGraphics();
				bufImageGraphics.drawImage(scaled, 0, 0, null);
				
				Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpeg");
				if (!writers.hasNext())
					throw new IllegalStateException("fant ikke bildeskriver.");
				ImageWriter writer = writers.next();
				ImageOutputStream ios = ImageIO.createImageOutputStream(bas);
				writer.setOutput(ios);
				ImageWriteParam param = writer.getDefaultWriteParam();
				if (compression >= 0) {
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(compression);
				}
				
				writer.write(null, new IIOImage(scaledBuff, null, null), param);
				ios.close();
				writer.dispose();
				
				byte[] data = bas.toByteArray();
				return data;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
    public static ByteArrayOutputStream compressImage(BufferedImage image, float compression){
    	try {
			ByteArrayOutputStream bas = new ByteArrayOutputStream();
			compressImage(image, compression, bas);
			return bas;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
    }
    
    public static BufferedImage timestampImage(BufferedImage image, Date dato){
    	BufferedImage timestamped = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
    	Graphics gr = timestamped.getGraphics();
    	String datoStr = MipssDateFormatter.formatDate(dato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	Color background = new Color(0f,0f,0f,0.9f);
    	Color fontColor = Color.white;
    	Font dateFont = new Font("Arial", Font.PLAIN, 12);
    	gr.drawImage(image, 0,0, null);
    	gr.setFont(dateFont);
    	
    	gr.translate(10, 10);
    	Rectangle2D rect = gr.getFontMetrics(dateFont).getStringBounds(datoStr, gr);
    	gr.setColor(background);
    	
    	gr.fillRect((int)(rect.getX()), (int)(rect.getY()), (int)(rect.getWidth()), (int)(rect.getHeight()));
    	gr.setColor(fontColor);
    	gr.drawString(datoStr, 0,0);
    	return timestamped;
    	
    	
    }
    public static void compressImage(BufferedImage image, float quality, OutputStream out) throws IOException {
        Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpeg");
        if (!writers.hasNext())
            throw new IllegalStateException("fant ikke bildeskriver.");
        ImageWriter writer = writers.next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);
        ImageWriteParam param = writer.getDefaultWriteParam();
        if (quality >= 0) {
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);
        }
        writer.write(null, new IIOImage(image, null, null), param);
        ios.close();
        writer.dispose();
    }
    
    public static BufferedImage compressImage(byte[] bytes, float compression){
    	try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));
			ByteArrayOutputStream out = new ByteArrayOutputStream(50000);
            compressImage(image, compression, out);
            return ImageIO.read(new ByteArrayInputStream(out.toByteArray()));
		} catch (IOException e) {
			e.printStackTrace();
		}

    	return null;
    }
    

    public static BufferedImage overlay(BufferedImage background, ImageIcon overlayImage, int x, int y, int w, int h, float opacity){
    	BufferedImage image = new BufferedImage(background.getWidth(), background.getTileHeight(), BufferedImage.TYPE_INT_ARGB);
    	Graphics2D gr = (Graphics2D)image.getGraphics();
    	Image scaleImage = scaleImage(overlayImage, w, h);
    	gr.drawImage(background, 0, 0, background.getWidth(), background.getHeight(), null);    	
    	Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity);
        gr.setComposite(c);
    	gr.drawImage(scaleImage, x, y, w, h, null);
    	return image;
    }
    
    @SuppressWarnings("unchecked")
	public static void saveCompressedImage(BufferedImage image, File file) throws IOException	{
		ImageWriter writer = null;
		Iterator iter = ImageIO.getImageWritersByFormatName("JPEG");
		writer = (ImageWriter)iter.next();
		
		ImageOutputStream ios = ImageIO.createImageOutputStream(file);
		writer.setOutput(ios);
		
		ImageWriteParam iwparam =writer.getDefaultWriteParam();
		
		iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwparam.setCompressionQuality(0.7F);
		
		writer.write(null, new IIOImage(image, null, null), iwparam);
		
		ios.flush();
		writer.dispose();
		ios.close();
	}
    
//    public static void saveImage(BufferedImage image, File file, float compression){    	
//    	try {
//			Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("png");
//			if (!writers.hasNext())
//				throw new IllegalStateException("fant ikke bildeskriver.");
//			ImageWriter writer = writers.next();
//			ImageOutputStream ios = ImageIO.createImageOutputStream(file);
//			writer.setOutput(ios);
//			ImageWriteParam param = writer.getDefaultWriteParam();
//			if (compression >= 0) {
//				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//				param.setCompressionQuality(compression);
//			}
//			writer.write(null, new IIOImage(image, null, null), param);
//			ios.close();
//			writer.dispose();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    }
    /**
     * Konverterer et ImageIcon til et gråskala bilde
     * @param image
     * @return
     */
    public static ImageIcon toGrayScale(ImageIcon image){
    	BufferedImage srcImage =convertImageToBufferedImage(image.getImage());
    	ColorSpace grayColorSpace= ColorSpace.getInstance (ColorSpace.CS_GRAY);
    	ColorConvertOp grayOp =new ColorConvertOp (grayColorSpace, null);
    	BufferedImage destImage = grayOp.filter (srcImage, null);
    	ImageIcon destIcon = new ImageIcon (destImage);
    	return destIcon;
    }
    
    public static byte[] readFromDisk(File imageFile) {
    	try {
			return IOUtils.toByteArray(new FileInputStream(imageFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
    }
}
