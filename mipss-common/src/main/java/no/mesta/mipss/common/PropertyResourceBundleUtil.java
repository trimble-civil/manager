package no.mesta.mipss.common;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Forenkler tilgangen til PropertyResourceBundle
 * </p>
 * 
 * <p>
 * Der finnes noen delegasjons metoder for .properties:
 * </p>
 * 
 * <ul>
 * <li><b>${nøkkel}:</b> Leser fra en annen verdi i mipss.properties</li>
 * <li><b>&{nøkkel}:</b> Leser fra VM argumenter</li>
 * </ul>
 * 
 * <p>
 * Denne støtter også arrays i property filer, det løses ved å slå opp
 * "nøkkel[]", så leses alle "nøkkel[n]" fra property filen.</p
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PropertyResourceBundleUtil extends ResourceBundle {
	private static final Logger logger = LoggerFactory.getLogger(PropertyResourceBundleUtil.class);
	private Locale locale;

	/**
	 * Konstruerer med en resourceBundle
	 * 
	 * @param resourceBundle
	 */
	private PropertyResourceBundleUtil(ResourceBundle resourceBundle, Locale locale) {
		setParent(resourceBundle);
		setLocale(locale);
	}

	/**
	 * Mipss safe metode for å hente ut en property string
	 * 
	 * @param key
	 * @throws IllegalArgumentException
	 *             hvis key er null
	 * @throws ClassCastException
	 *             hvis det er noe galt med property bundle
	 * @return
	 */
	public String getGuiString(String key) {
		String s = null;

		try {
			s = getPropertyString(key);
		} catch (NullPointerException ne) {
			throw new IllegalArgumentException(ne);
		} catch (MissingResourceException me) {
			s = "TODO: " + key;
		} catch (ClassCastException ce) {
			throw new IllegalStateException(ce);
		}

		return s;
	}

	/**
	 * Formaterer en guiString med objektene inn
	 * 
	 * @param key
	 * @param locale
	 * @param values
	 */
	public String getGuiString(String key, Locale locale, Object[] values) {
		String message = getGuiString(key);
		String result = null;
		try {
			MessageFormat mf = new MessageFormat(message, locale);
			result = mf.format(values);
		} catch (IllegalArgumentException iea) {
			result = "TODO: " + key;
		}

		return result;
	}

	/**
	 * Formaterer en norsk guiString med objektene inn
	 * 
	 * @param key
	 * @param values
	 */
	public String getGuiString(String key, Object[] values) {
		return getGuiString(key, getLocale(), values);
	}
	
	public String getGuiString(String key, String... values){
		return getGuiString(key, getLocale(), values);
	}

	/** {@inheritDoc} */
	@Override
	public Enumeration<String> getKeys() {
		return parent.getKeys();
	}

	/**
	 * @return the locale
	 */
	@Override
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Henter verdien fra property
	 * 
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		return getGuiString(key);
	}

	/**
	 * Henter en string og fyller inn verdiene
	 * 
	 * @param key
	 * @param locale
	 * @param values
	 * @return
	 */
	public String getProperty(String key, Locale locale, Object[] values) {
		return getGuiString(key, locale, values);
	}

	/**
	 * Henter en string, og fletter inn verdiene
	 * 
	 * @param key
	 * @param values
	 * @return
	 */
	public String getProperty(String key, Object[] values) {
		return getGuiString(key, values);
	}

	private String getPropertyString(String property) {
		logger.trace("getPropertyString(" + property + ") start");
		String value = parent.getString(property);
		if (StringUtils.startsWith(value, "&{")) {
			String key = value.substring(2, value.length() - 1);
			value = System.getProperty(key);
		}

		if (StringUtils.startsWith(value, "${")) {
			value = parent.getString(value.substring(2, value.length() - 1));
		}

		logger.trace(property + " is " + value);
		return value;
	}

	/** {@inheritDoc} */
	@Override
	public Object handleGetObject(String key) {
		logger.trace("handleGetObject(" + key + ") start");
		if (key == null) {
			throw new NullPointerException("Key is null");
		}

		if (StringUtils.endsWith(key, "[]")) {
			List<String> values = new ArrayList<String>();
			MissingResourceException e = null;
			int index = 0;
			do {
				try {
					String arrayKey = key.substring(0, key.length() - 2) + "[" + index + "]";
					values.add(getPropertyString(arrayKey));
					index++;
				} catch (MissingResourceException ex) {
					e = ex;
				}
			} while (e == null);

			if (values.size() > 0) {
				String[] strings = new String[values.size()];
				strings = values.toArray(strings);

				return strings;
			} else {
				throw new MissingResourceException("Can't find resource for key " + key, parent.getClass().getName(),
						key);
			}
		} else {
			return getPropertyString(key);
		}
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Returnerer en bundle med norsk locale
	 * 
	 * @param baseName
	 * @return
	 */
	public static PropertyResourceBundleUtil getPropertyBundle(String baseName) {
		return getPropertyBundle(baseName, new Locale("no", "NO"));
	}

	/**
	 * Henter tak i en resourceBundle
	 * 
	 * @param baseName
	 * @param locale
	 * @throws IllegalArgumentException
	 *             hvis baseName eller locale er null
	 * @throws IllegalStateException
	 *             hvis getBundle ender opp i MissingResourceException
	 * @return
	 */
	public static PropertyResourceBundleUtil getPropertyBundle(String baseName, Locale locale) {
		ResourceBundle resourceBundle = null;

		try {
			resourceBundle = PropertyResourceBundle.getBundle(baseName, locale, PropertyResourceBundleUtil.class
					.getClassLoader());
		} catch (NullPointerException e) {
			throw new IllegalArgumentException(e);
		} catch (MissingResourceException e) {
			try {
				resourceBundle = PropertyResourceBundle.getBundle(baseName, locale, Thread.currentThread()
						.getContextClassLoader());
			} catch (Throwable t) {
				throw new IllegalStateException(t);
			}
		}

		return new PropertyResourceBundleUtil(resourceBundle, locale);
	}
}
