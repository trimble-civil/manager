package no.mesta.mipss.common.property;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;

/**
 * A simple property accessor providing access to properties in
 * files (e.g <em>plugin-admin.properties</em>)
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link PropertyAccessorFactory}
 */
public class DefaultPropertyAccessor implements PropertyAccessor {
	
	private PropertyResourceBundle bundle;
	
	/**
	 * Parameterized constructor. Invoked by <tt>PropertyAccessorFactory</tt>.
	 * 
	 * @param propertyFileName - the name of the .properties file to provide access to.
	 * @return DefaultPropertyAccessor - the default property accessor (instance of this class).
	 */
	public DefaultPropertyAccessor(String propertyFileName) {
		initBundle(propertyFileName);
	}	
		
	/**
	 * Get the current value of the specified property as a <tt>String</tt>.
	 * 
	 * @param propertyName - object containing the name of the property
	 * @return the value of the property as a <tt>String</tt>
	 */
	public String getString(PropertyName propertyName) {
		String propertyValue = bundle.getString(propertyName.getPropertyName());
		return propertyValue;
	}
	/**
	 * Get the current value of the specified property as a <tt>String</tt>.
	 * 
	 * @param propertyName - object containing the name of the property
	 * @return the value of the property as a <tt>String</tt>
	 */
	public String getString(PropertyName propertyName, Object... values) {
		String propertyValue = bundle.getString(propertyName.getPropertyName());
		
		return propertyValue;
	}
	/**
	 * Get the current value of the specified property as an <tt>int</tt>.
	 * 
	 * @param propertyName - object containing the name of the property
	 * @return the value of the property as an <tt>int</tt>
	 */
	public int getInt(PropertyName propertyName) {
		int propertyValue = Integer.parseInt(bundle.getString(propertyName.getPropertyName()));
		return propertyValue;
	}
	
	/**
	 * Get the current value of the specified property as a <tt>String</tt>.
	 * 
	 * @param propertyName - String containing the name of the property
	 * @return the value of the property as a <tt>String</tt>
	 */
	public String getString(String propertyName) {
		String propertyValue = bundle.getString(propertyName);
		return propertyValue;
	}
		
	/**
	 * Get the current value of the specified property as an <tt>int</tt>.
	 * 
	 * @param propertyName - String containing the name of the property
	 * @return the value of the property as an <tt>int</tt>
	 */
	public int getInt(String propertyName) {
		int propertyValue = Integer.parseInt(bundle.getString(propertyName));
		return propertyValue;
	}
	
	/**
	 * <p>Get the multiple values of the specified property as a <tt>String[]</tt> array.</p>
	 * 
	 * <p>Note: This implementation regards multiple values as <em>comma-separated values</em> associated with <strong>one</strong> key. Only
	 * one occurrence of the property key is supposed to exist. In other words, there are not actually multiple occurrences of a property key.</p>
	 * 
	 * @param propertyName - String containing the name of the property.
	 * @return the multiple values of the property as a <tt>String[]</tt> array.
	 */ 
	public String[] getStringArray(String propertyName) {
		
		// Retrieve comma-separated column-names String based on the enum
		StringTokenizer tokenizer = new StringTokenizer(bundle.getString(propertyName), ",");

		// Tokenize and organize column names into a String array
		List<String> columnList = new ArrayList<String>(tokenizer.countTokens());
		
		while(tokenizer.hasMoreTokens()) {
			columnList.add(tokenizer.nextToken().trim());
		}
		
		String[] columns = new String[columnList.size()];
		columns = columnList.toArray(columns);
		
		return columns;
		
	}

	// Initialize the resource bundle
	private void initBundle(String propertyFileName) {
		if(bundle == null) {
			bundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(propertyFileName);
		}		
	}

}