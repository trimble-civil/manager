package no.mesta.mipss.common;

import java.beans.VetoableChangeListener;

public interface VetoableChangeSource {
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#addVetoableChangeListener(VetoableChangeListener)
	 * @param l
	 */
	public void addVetoableChangeListener(VetoableChangeListener listener);
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#addVetoableChangeListener(String,
	 *      VetoableChangeListener)
	 * @param l
	 */
	public void addVetoableChangeListener(String propertyName, VetoableChangeListener listener);
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#removeVetoableChangeListener(VetoableChangeListener)
	 * @param l
	 */
	public void removeVetoableChangeListener(VetoableChangeListener listener);
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.VetoableChangeSupport#removeVetoableChangeListener(String,
	 *      VetoableChangeListener)
	 * @param l
	 */
	public void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener);
}
