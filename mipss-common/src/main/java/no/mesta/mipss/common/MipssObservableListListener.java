package no.mesta.mipss.common;

import java.beans.PropertyChangeEvent;
import java.beans.VetoableChangeListener;

import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;

public interface MipssObservableListListener<T> extends ObservableListListener, VetoableChangeListener{

	void listElementPropertyChanged(ObservableList<T> list, int index, PropertyChangeEvent evt);
}
