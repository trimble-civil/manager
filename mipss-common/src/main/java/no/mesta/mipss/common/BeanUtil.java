package no.mesta.mipss.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * Hjelpeklasse for aa bruke Beans
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class BeanUtil {
    private static final Logger log = LoggerFactory.getLogger(BeanUtil.class);

    private static HashMap<String, Object> beanCache= new HashMap<String, Object>();
    private static BeanLookup beanLookup = findBeanLookup();

    /**
     * Henter en spesifik bean
     * 
     * @param beanName
     * @param beanInterface
     * @return
     */
    @SuppressWarnings("unchecked")
	public static<T> T lookup(String beanName, Class<T> beanInterface) {
        log.debug(String.format("Lookup(%s, %s)", beanName, beanInterface.getClass().toString()));
    	T bean = (T)beanCache.get(beanName);
        if (bean==null){
            bean = beanLookup.lookup(beanName, beanInterface);
            log.debug("Looked up bean");
            beanCache.put(beanName, bean);
        } else {
            log.debug("Found bean in cache");
        }
        
        return bean;
    }

    public static void setBeanLookup(BeanLookup beanLookup) {
        BeanUtil.beanLookup = beanLookup;
    }

    private static BeanLookup findBeanLookup() {
        Properties properties = new Properties();
        String url = "";
        try {
            properties.load(BeanUtil.class.getResourceAsStream("/jndi.properties"));
            url = properties.getProperty("java.naming.provider.url");
            if(url != null && url.startsWith("http-remoting")) {
                log.info(String.format("Using WildflyBeanLookup url=%s", url));
                return new WildflyBeanLookup();
            }
        } catch (IOException e) {
            log.error("Unable to load jndi.properties", e);
        }
        log.info(String.format("Using RemoteBeanLookup, url=%s", url));
        return new RemoteBeanLookup();
    }
}
