package no.mesta.mipss.common.property;


/**
 * Interface for named properties
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link Properties}
 */
public interface PropertyName {
	
	/**
	 * Get the name of this property.
	 * 
	 * @return the name of this property
	 */
	public String getPropertyName();

}