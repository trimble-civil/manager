package no.mesta.mipss.common.property;

/**
 * Simple factory for obtaining <tt>PropertyAccessor</tt> instances. Conceals the target implementation
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link DefaultPropertyAccessor}
 */
public class PropertyAccessorFactory {
	
	/**
	 * Get a <tt>PropertyAccessor</tt> instance.
	 * 
	 * @param propertyFileName - the name of the .properties file to provide access to.
	 * @return PropertyAccessor - a property accessor.
	 */
	public static PropertyAccessor getPropertyAccessor(String propertyFileName) {
		return new DefaultPropertyAccessor(propertyFileName);
	}
	
	/**
	 * 
	 * Get a <tt>PropertyAccessor</tt> instance.
	 * 
	 * @param propertyFileName - object containing the name of the .properties file to provide access to.
	 * @return PropertyAccessor - a property accessor.
	 */
	public static PropertyAccessor getPropertyAccessor(PropertyName propertyFileName) {
		return new DefaultPropertyAccessor(propertyFileName.getPropertyName());
	}

}