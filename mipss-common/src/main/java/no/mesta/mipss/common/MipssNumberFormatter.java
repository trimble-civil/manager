package no.mesta.mipss.common;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class MipssNumberFormatter {

	private static final String PHONE_FORMAT = "##,##,##,##";
	
	public static String formatPhoneNumber(Long tlfNumber) {
		if(tlfNumber == null) {
			return "";
		}
		
		DecimalFormatSymbols phoneNumberSymbols = new DecimalFormatSymbols(new Locale("no", "NO"));  
		phoneNumberSymbols.setGroupingSeparator(' ');
		NumberFormat nf = new DecimalFormat(PHONE_FORMAT, phoneNumberSymbols);
		
		return nf.format(tlfNumber);
	}

	public static String formatNumber(Number number) {
		return getNumberFormat(2, 0, 1).format(number);
	}

	public static String formatNumberForcedDecimal(Number number) {
		return getNumberFormat(2, 2, 1).format(number);
	}

	private static NumberFormat getNumberFormat(int maxFractionDigits, int minFractionDigits, int minIntegerDigits) {
		NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());

		numberFormat.setMaximumFractionDigits(maxFractionDigits);
		numberFormat.setMinimumFractionDigits(minFractionDigits);
		numberFormat.setMinimumIntegerDigits(minIntegerDigits);

		return numberFormat;
	}

}