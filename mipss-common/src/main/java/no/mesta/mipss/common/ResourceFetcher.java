package no.mesta.mipss.common;

import java.io.InputStream;
import java.net.URL;

/**
 * Henter resusser som Mipss trenger
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ResourceFetcher {

	public static InputStream getResourceAsStream(String resource) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		InputStream stream = cl.getResourceAsStream(resource);
		
		if(stream == null) {
			stream = ResourceFetcher.class.getResourceAsStream(resource);
		}
		
		return stream;
	}
	
	public static URL getResourceAsURL(String resource) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(resource);
		
		if(url == null) {
			url = ResourceFetcher.class.getResource(resource);
		}
		
		return url;
	}
}
