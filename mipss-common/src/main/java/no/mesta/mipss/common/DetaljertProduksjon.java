package no.mesta.mipss.common;

import java.io.Serializable;

/**
 * Klasse som holder verdiene etter spørring etter all produksjon gruppert på produksjonstype for en gitt kontrakt over
 * en gitt periode. Benyttes i ProduksjonPanel i Kontraktdashboard.
 *
 * @author TomSka
 * @since 13.04.2016
 */
public class DetaljertProduksjon implements Serializable {

    private String prodtype;
    private String enhet;
    private double mengde_prod;
    private double timer_totalt;
    private double kapasitet;
    private double km_prod;
    private double km_prod_time;

    public static final String[] getPropertiesArray(){
        return new String[] {
                "prodtype",
                "enhet",
                "mengde_prod",
                "timer_totalt",
                "kapasitet",
                "km_prod",
                "km_prod_time"
        };
    }

    public String getProdtype() {
        return prodtype;
    }

    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }

    public String getEnhet() {
        return enhet;
    }

    public void setEnhet(String enhet) {
        this.enhet = enhet;
    }

    public double getMengde_prod() {
        return mengde_prod;
    }

    public void setMengde_prod(double mengde_prod) {
        this.mengde_prod = mengde_prod;
    }

    public double getTimer_totalt() {
        return timer_totalt;
    }

    public void setTimer_totalt(double timer_totalt) {
        this.timer_totalt = timer_totalt;
    }

    public double getKapasitet() {
        return kapasitet;
    }

    public void setKapasitet(double kapasitet) {
        this.kapasitet = kapasitet;
    }

    public double getKm_prod() {
        return km_prod;
    }

    public void setKm_prod(double km_prod) {
        this.km_prod = km_prod;
    }

    public double getKm_prod_time() {
        return km_prod_time;
    }

    public void setKm_prod_time(double km_prod_time) {
        this.km_prod_time = km_prod_time;
    }

}