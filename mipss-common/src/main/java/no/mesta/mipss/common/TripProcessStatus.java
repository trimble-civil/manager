package no.mesta.mipss.common;

public enum TripProcessStatus {

    ONGOING                      ( "1", "Pågående"                          ,  1),
    UNPROCESSED                  ("2a", "Ubehandlet UE"                     ,  2),
    RETURNED_TO_SUBCONTRACTOR    ("2b", "Tilbakesendt økt"                  ,  2),
    PROCESSED_BY_SUBCONTRACTOR   ( "3", "Behandlet UE"                      ,  3),
    SUBMITTED_BY_SUBCONTRACTOR   ( "4", "Innsendt av UE"                    ,  4),
    UNDER_PROCESS_BY_CONTRACTOR  ( "5", "Under behandling av Ent (Manager)" ,  5),
    PROCESSED_BY_CONTRACTOR      ( "6", "Behandlet Entreprenør (Manager)"   ,  6),
    SENT_TO_IO                   ( "7", "Sendt IO"                          ,  7),
    PAID                         ("8a", "Utbetalt"                          ,  8),
    REJECTED                     ("8b", "Avvist"                            ,  8),
    UNKNOWN                      ("99", "Ukjent"                            , 99);

    private String id;
    private String text;
    private int level;

    TripProcessStatus(String id, String text, int level) {
        this.id = id;
        this.text = text;
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public int getLevel() {
        return level;
    }

    public static TripProcessStatus fromText(String text) {
        for (TripProcessStatus ts :  TripProcessStatus.values()) {
            if (ts.getText().equals(text)) {
                return ts;
            }
        }
        return TripProcessStatus.UNKNOWN;
    }

    public static TripProcessStatus fromId(String id) {
        for (TripProcessStatus ts : TripProcessStatus.values()) {
            if (ts.getId().equals(id)) {
                return ts;
            }
        }
        return TripProcessStatus.UNKNOWN;
    }

}