package no.mesta.mipss.common;

public interface BeanLookup {
    public <T> T lookup(String beanName, Class<T> beanInterface);
}
