package no.mesta.mipss.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

public class WildflyBeanLookup implements BeanLookup {

    private static final Logger log = LoggerFactory.getLogger(WildflyBeanLookup.class);

    private static Context context;

    private static final String EAR_FILE = "mipss-server-ear-1.0-SNAPSHOT";

    @Override
    public <T> T lookup(String beanName, Class<T> beanInterface) {
        try {
            String jndiName = String.format("%s/%s/%s!%s", EAR_FILE, findJar(beanName), beanName, beanInterface.getName());
            log.debug("Looking up bean '{}'", jndiName);
            return (T) PortableRemoteObject.narrow(getContext().lookup(
                    jndiName), beanInterface);
        } catch (NamingException e) {
            throw new IllegalArgumentException("Bean not found " + beanName, e);
        }
    }

    private  Context getContext() {
        if(context == null) {
            try {
                context = new InitialContext();
            } catch (NamingException e) {
                throw new IllegalStateException("Misconfiguration?", e);
            }
        }

        return context;
    }

    private String findJar(String beanName) {
        return getProject(beanName)+"-1.0-SNAPSHOT";
    }

    private String getProject(String beanName) {
        switch(beanName) {
            case "AccessControlBean":
            case "BrukerLoggBean":
            case "BrukerLogg":
                return "mipss-accesscontrol";
            case "BrukerpreferanserBean":
               return "mipss-brukerpreferanser";
            case "DfuBean":
            case "DfuService":
                return "mipss-dfu";
            case "KjoretoyGruppeService":
            case "MaintenanceContractBean":
                return "mipss-driftkontrakt";
            case "DriftsloggBean":
            case "DriftsloggService":
                return "mipss-driftsloggservice";
            case "ElrappService":
            case "ElrappServiceBean":
                return "mipss-elrappservice";
            case "MipssEntityServiceBean":
            case "GrunndataServiceBean":
                return "mipss-entityservice";
            case "EpostService":
                return "mipss-epostservice";
            case "AndroidUpdateBean":
            case "AvvikBean":
            case "InspeksjonBean":
            case "MipssFeltBean":
            case "RskjemaServiceBean":
            case "SakServiceBean":
            case "InspeksjonsrapportBean":
                return "mipss-felt";
            case "MipssField":
            case "MipssFieldBean":
                return "mipss-field";
            case "FriksjonsdetaljBean":
                return "mipss-friksjonsdetalj";
            case "MipssKjoretoyBean":
                return "mipss-kjoretoy";
            case "Klimadata":
            case "KlimadataBean":
                return "mipss-klimadata";
            case "DokumentBean":
            case "KonfigParamBean":
                return "mipss-konfigparam";
            case "KontraktdashboardBean":
                return "mipss-kontraktdashboard";
            case "MapsGen":
            case "MapsGenBean":
                return "mipss-mapsgen";
            case "ApplikasjonLoader":
                return "mipss-menunode";
            case "MipssMapBean":
                return "mipss-mipssmap";
            case "ProduksjonService":
                return "mipss-produksjonservice";
            case "AdminrapportService":
            case "Prodrapport":
            case "ProdrapportBean":
                return "mipss-produksjonsrapporter";
            case "Rodegenerator":
            case "RodegeneratorBean":
                return "mipss-rodegenerator";
            case "SamWebserviceFacade":
                return "mipss-samwebservice";
            case "JobbinfoServiceBean":
            case "MipssSystemStatusServiceBean":
                return "mipss-systemstatus";
            case "MipssServerUtils":
                return "mipss-utils";
            case "ConvertBean":
            case "VeinettImportBean":
            case "VeinettServiceBean":
                return "mipss-veinettservice";
            case "NvdbWebserviceBean":
            case "Produksjon":
                return "mipss-webservice";
            case "MipssCharts":
                return "mipss-chart";
            case "MipssServerUtilsBean":
                return "mipss-utils";
            default:
                throw new IllegalArgumentException("Unable to lookup jar file for "+beanName);
        }
    }
}
