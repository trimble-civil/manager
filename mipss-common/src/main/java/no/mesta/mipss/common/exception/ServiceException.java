package no.mesta.mipss.common.exception;

/**
 * <p>Generell exception til bruk i tjenestelag.</p>
 * 
 * <p>Merk at feilmelding i denne klassens konstruktører er ment for logg og <strong>ikke</strong> bruker.</p>
 * 
 * @author Christian Wiik (Mesan)
 */
public class ServiceException extends RuntimeException {
	
	private static final long serialVersionUID = 1856862247618223014L;
	
	/**
	 * Konstruerer en exception med feilmelding og årsak.
	 * 
	 * @param message - feilmeldingen til denne feilen.
	 * @param cause - årsaken bak denne feilen.
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * Konstruerer en exception med feilmelding.
	 * 
	 * @param message - feilmeldingen til denne feilen.
	 */
	public ServiceException(String message) {
		super(message);
	}

}