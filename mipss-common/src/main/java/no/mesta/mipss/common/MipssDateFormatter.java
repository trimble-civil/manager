package no.mesta.mipss.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Felles formatteringsklasse for datoer med utilities metoder.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssDateFormatter {

	public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String SHORT_TIME_FORMAT = "HH:mm";
    public static final String SHORT_DATE_TIME_FORMAT = "dd.MM.yy HH:mm";
    public static final String SHORT_DATE_FORMAT = "dd.MM.yy";
    public static final String LONG_DATE_TIME_FORMAT = "dd.MM.yy HH:mm:ss";
    public static final String LONG_YEARDATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
    public static final String SHORT_DATE_TIME_NO_YEAR_FORMAT = "dd.MM HH:mm";
	public static final String ORACLE_LONG_DATE_TIME_FORMAT = "dd.mm.yyyy hh24:mi:ss";
	public static final String SHORT_DAY_MONTH_TIME_FORMAT = "dd. MMM HH:mm";
	public static final String LONG_TIME_YEARDATE_FORMAT = "HH:mm dd.MM.yyyy";
    
    private MipssDateFormatter() {}
    
    /**
     * Formatterer dato/tidsangivelser.
     * 
     * @param date
     * @return formatert string eller tom string hvis date var null.
     */
    public static String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat(MipssDateFormatter.DATE_TIME_FORMAT, new Locale("no", "NO"));
        return formatter.format(date);
    }
    /**
     * Parser en dato, default format er dd.MM.yyyy
     * @param date
     * @return
     */
    public static Date parseDate(String date){
    	return parseDate(date, DATE_FORMAT);
    }
    public static Date parseDate(String date, String format){
    	SimpleDateFormat formatter = new SimpleDateFormat(format, new Locale("no", "NO"));
    	try {
			return formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Illegal date format");
    }

    /**
     * Formatterer dato
     * 
     * @param date
     * @param includeTime
     * @return
     */
    public static String formatDate(Date date, boolean includeTime) {
        if (includeTime) {
            return formatDate(date);
        }

        if (date == null) {
            return "";
        }
        
        SimpleDateFormat formatter = new SimpleDateFormat(MipssDateFormatter.DATE_FORMAT, new Locale("no", "NO"));
        return formatter.format(date);
    }
    
    public static String formatDate(Date date, String format) {
        if (date == null) {
            return "";
        }

        SimpleDateFormat formatter = new SimpleDateFormat(format, new Locale("no", "NO"));
        return formatter.format(date);
    
    
    }
    
    public static Date convertFromGMTtoCET(Date gmtDate){
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone("CET"));
    	c.setTime(gmtDate);
    	int d = c.get(Calendar.DAY_OF_MONTH);
    	int m = c.get(Calendar.MONTH);
    	int y = c.get(Calendar.YEAR);
    	int hh = c.get(Calendar.HOUR_OF_DAY);
    	int mm = c.get(Calendar.MINUTE);
    	int ss = c.get(Calendar.SECOND);
    	int ms = c.get(Calendar.MILLISECOND);
    	Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    	cal.set(Calendar.DAY_OF_MONTH, d);
    	cal.set(Calendar.MONTH, m);
    	cal.set(Calendar.YEAR, y);
    	cal.set(Calendar.HOUR_OF_DAY, hh);
    	cal.set(Calendar.MINUTE, mm);
    	cal.set(Calendar.SECOND, ss);
    	cal.set(Calendar.MILLISECOND, ms);
		
    	return cal.getTime();
		
	}
	/**
	 * Konverter en dato fra CET til GMT
	 * @param cetDate
	 * @return
	 */
	public static Date convertFromCETtoGMT(Date cetDate){
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    	c.setTime(cetDate);
    	int d = c.get(Calendar.DAY_OF_MONTH);
    	int m = c.get(Calendar.MONTH);
    	int y = c.get(Calendar.YEAR);
    	int hh = c.get(Calendar.HOUR_OF_DAY);
    	int mm = c.get(Calendar.MINUTE);
    	int ss = c.get(Calendar.SECOND);
    	int ms = c.get(Calendar.MILLISECOND);
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, d);
    	cal.set(Calendar.MONTH, m);
    	cal.set(Calendar.YEAR, y);
    	cal.set(Calendar.HOUR_OF_DAY, hh);
    	cal.set(Calendar.MINUTE, mm);
    	cal.set(Calendar.SECOND, ss);
    	cal.set(Calendar.MILLISECOND, ms);
    	
    	return cal.getTime();
		
	}
}
