package no.mesta.mipss.common;

import java.io.Serializable;

/**
 * Klasse som holder verdieene etter spørringer etter strøproduktmengder for en gitt kontrakt over en gitt periode.
 * Benyttes fra Dashboard, strøprodukt-seksjonen.
 * @author OLEHEL
 *
 */
public class StroMetodeMedMengde implements Serializable{
	private String stroprodukt_navn; 
	private Double tonn_fast;
	private Double m3_losning;
	
	 
	public static final String[] getPropertiesArray(){
		return new String[]{"stroprodukt_navn", "tonn_fast", "m3_losning"};
	}
	
	public String getStroprodukt_navn() {
		return stroprodukt_navn;
	}
	
	public void setStroprodukt_navn(String stroprodukt_navn) {
		this.stroprodukt_navn = stroprodukt_navn;
	}

	public Double getTonn_fast() {
		return tonn_fast;
	}
	public void setTonn_fast(Double tonn_fast) {
		this.tonn_fast = tonn_fast;
	}
	public Double getM3_losning() {
		return m3_losning;
	}
	public void setM3_losning(Double m3_losning) {
		this.m3_losning = m3_losning;
	}

}
