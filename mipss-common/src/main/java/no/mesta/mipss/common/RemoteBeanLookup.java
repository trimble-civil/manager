package no.mesta.mipss.common;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

public class RemoteBeanLookup implements BeanLookup {

    private static Context context;

    @Override
    public <T> T lookup(String beanName, Class<T> beanInterface) {
        try {
            return (T) PortableRemoteObject.narrow(getContext().lookup("app/" + beanName), beanInterface);
        } catch (NamingException e) {
            throw new IllegalArgumentException("Bean not found " + beanName, e);
        }
    }

    /**
     * Skaffer contexten til applikasjonen
     *
     * @return
     */
    private  Context getContext() {
        if(context == null) {
            try {
                context = new InitialContext();
            } catch (NamingException e) {
                throw new IllegalStateException("Misconfiguration?", e);
            }
        }

        return context;
    }
}
