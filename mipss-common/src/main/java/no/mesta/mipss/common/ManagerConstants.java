package no.mesta.mipss.common;

public class ManagerConstants {

    public static final double WEIGHT_VOLUME_FACTOR = 1.18;

    public enum RodeConnectionType {
        PLOWING_RODE(1, "Brøyterode");

        private int id;
        private String name;

        RodeConnectionType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public enum Stroprod {
        TORRSALT(1, "T\u00F8rrsalt"),
        SAND_MED_SALT(2, "Sand med salt"),
        REN_SAND(3, "Ren sand"),
        SLURRY(4, "Slurry"),
        SALTLOSNING(5, "Saltl\u00F8sning"),
        BEFUKTET_SALT(6, "Befuktet salt"),
        FAST_SAND(7, "Fast sand"),
        MGCL_LOSNING(8, "MgCl-l\u00F8sning"),
        CACL_LOSNING(10, "CaCl-l\u00F8sning"),
        MGCL_TORT(11, "MgCl-t\u00F8rt"),
        CACL_TORT(12, "CaCl-t\u00F8rt"),
        SAND_M_SALTLOSNING(13, "Sand m. saltl\u00F8sning"),
        BEFUKTET_CACL(14, "Befuktet Cacl"),
        UKJENT(99, "Ukjent");

        private long id;
        private String navn;

        Stroprod(long id, String navn) {
            this.id = id;
            this.navn = navn;
        }

        public long id() {
            return id;
        }

        public String navn() {
            return navn;
        }

        public static Stroprod idTilEnum(int prodtypeId) {
            for (Stroprod type : Stroprod.values()) {
                if (type.id == prodtypeId) {
                    return type;
                }
            }

            return UKJENT;
        }

        public static Stroprod navnTilEnum(String navn) {
            if (navn != null) {
                for (Stroprod type : Stroprod.values()) {
                    if (type.navn.equalsIgnoreCase(navn)) {
                        return type;
                    }
                }
            }

            return UKJENT;
        }
    }

    public enum ProdType {
        BROYTING_HOVLING_INTENSJON(0, "Br\u00F8yt/H\u00F8vl intensjon"),
        SIDEPLOG(1, "Sideplog"),
        KRAFTUTTAK(3, "Kraftuttak"),
        SKILTVASK(4, "Skiltvask"),
        STROING(5, "Str\u00F8ing"),
        BROYTING(6, "Br\u00F8yting"),
        HOVLING(7, "H\u00F8vling"),
        FEIING(8, "Feiing"),
        KLIPPING(9, "Klipping"),
        MANUELLE_TILTAK(10, "Manuelle tiltak"),
        TIPP(12, "Tipp"),
        STIKKSETTING(13, "Stikksetting"),
        SNORYDDING(14, "Sn\u00F8rydding"),
        RENHOLD(15, "Renhold"),
        SLAMSUGING(16, "Slamsuging"),
        KANTUTLEGGING(17, "Kantutlegging"),
        FRIKSJONSMALING(18, "Friksjonsm\u00E5ling"),
        ASFALTARBEID(19, "Asfaltarbeid"),
        KRANARBEID(20, "Kranarbeid"),
        SWEEPER(21, "Sweeper"),
        KOLONNEKJORING(22, "Kolonnekj\u00F8ring"),
        GRUSSTRENGSPREDING(23, "Spreding av grusstreng etter h\u00F8vling"),
        TUNG_HOVLING(24, "H\u00F8vling med tung veih\u00F8vel (midtskj\u00E6r)"),
        TOMKJORING(98, "Kj\u00F8ring uten produksjon"),
        UKJENT(99, "Ukjent");

        private long id;
        private String navn;

        ProdType(long id, String navn) {
            this.id = id;
            this.navn = navn;
        }

        public long getId() {
            return id;
        }

        public String getNavn() {
            return navn;
        }

        public static ProdType idTilEnum(int prodtypeId) {
            for (ProdType type : ProdType.values()) {
                if (type.id == prodtypeId) {
                    return type;
                }
            }

            return UKJENT;
        }

        public static ProdType navnTilEnum(String navn) {
            if (navn != null) {
                for (ProdType type : ProdType.values()) {
                    if (type.navn.equalsIgnoreCase(navn)) {
                        return type;
                    }
                }
            }

            return UKJENT;
        }
    }

}