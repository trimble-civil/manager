package no.mesta.mipss.resources.images;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Tegner et rødt kryss i en sort ramme
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class MissingIcon extends ImageIcon implements Icon {
	private Image bufferedImage = null;
	private int height = 32;
	private BasicStroke stroke = new BasicStroke(4);
	private int width = 32;

	/**
	 * Konstruktør
	 * 
	 */
	public MissingIcon() {
		super();
	}

	/**
	 * Konstruktør
	 * 
	 * @param width
	 * @param height
	 */
	public MissingIcon(int width, int height) {
		this();
		this.width = width;
		this.height = height;
	}

	/** {@inheritDoc} */
	public int getIconHeight() {
		return height;
	}

	/** {@inheritDoc} */
	public int getIconWidth() {
		return width;
	}

	/** {@inheritDoc} */
	@Override
	public Image getImage() {
		if (bufferedImage == null) {
			bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_INDEXED);
			Graphics g = bufferedImage.getGraphics();
			paintIcon(null, g, 0, 0);
		}

		return bufferedImage;
	}

	/** {@inheritDoc} */
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Graphics2D g2d = (Graphics2D) g.create();

		g2d.setColor(Color.WHITE);
		g2d.fillRect(x + 1, y + 1, width - 2, height - 2);

		g2d.setColor(Color.BLACK);
		g2d.drawRect(x + 1, y + 1, width - 2, height - 2);

		g2d.setColor(Color.RED);

		g2d.setStroke(stroke);
		g2d.drawLine(x + 10, y + 10, x + width - 10, y + height - 10);
		g2d.drawLine(x + 10, y + height - 10, x + width - 10, y + 10);

		g2d.dispose();
	}
}
