package no.mesta.mipss.resources.images;

import java.awt.*;
import java.net.URL;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse for gjenbrukbare ikoner I GUI
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class IconResources {
	
	
	private static final Logger logger = LoggerFactory.getLogger(IconResources.class);
//	private static final IconResources resource = new IconResources();
	public static final ImageIcon AGRESSO_16 = load("images/agresso.png");
	public static final ImageIcon APPROVE_48 = load("images/approve.png");
	public static final ImageIcon LIST_16 = load("images/list.png");
	public static final ImageIcon MIPSS_FELT_LOGO = load("images/mipssfelt.png");
	public static final ImageIcon DETALJER_ICON_16 = load("images/details.png");
	public static final ImageIcon SAK_ICON_16 = load("images/sak.png");
	public static final ImageIcon SAKSBEHANDLING_ICON_16 = load("images/saksbehandling.png");
	public static final ImageIcon SHAKING_HAND_64= load("images/shaking_hand.png");
	public static final ImageIcon SHRED_48= load("images/shred_48.jpg");
	public static final ImageIcon ACCESS_LEVEL = load("images/access-level.png");
	public static final ImageIcon ABORT_ICON = load("images/close.png");
	public static final ImageIcon ADOBE_16 = load("images/adobe16.png");
	public static final ImageIcon ACTUAL_PIXELS_ICON = load("images/actualPixelsIcon.png");
	public static final ImageIcon ADD_ALL_ICON = load("images/add-all.png");
	public static final ImageIcon LOCK2_ICON_16 = load("images/lock2-16.png");
	public static final ImageIcon UNLOCK2_ICON_16 = load("images/unlock2-16.png");
	public static final ImageIcon LOCK_ICON_16 = load("images/icon_lock16.png");
	public static final ImageIcon ADD_ITEM_ICON = load("images/list-add.png");
	public static final ImageIcon DOCUMENT_OPEN = load("images/DocumentOpen16.gif");
	public static final ImageIcon ADD_ONE_ICON = load("images/add-one.png");
	public static final ImageIcon ATTACHMENT = load("images/attachment.png");
	public static final ImageIcon BIL_SMALL_ICON = load("images/arbeid_small.png");
	public static final ImageIcon BIL_SMALL_GRUPPE_ICON = load("images/arbeid_small_gruppe.png");
	public static final ImageIcon BIL = load("images/arbeid.png");
	public static final ImageIcon BOOKMARK_EDIT = load("images/bookmark_edit.png");
	public static final ImageIcon BUBBLE_SM = load("images/bubble_sm.png");
	public static final ImageIcon BUTTON_CANCEL_ICON = load("images/cancel.png");
	public static final ImageIcon BUTTON_UP = load("images/buttonUp.png");
	public static final ImageIcon BUTTON_DOWN = load("images/buttonDown.png");
	public static final ImageIcon CAMERA_ICON = load("images/icon_kamera.png");
	public static final ImageIcon CANCEL_ICON = load("images/close.png");
	public static final ImageIcon CLOSE_TAB_ICON = load("images/closeTab.png");
	public static final ImageIcon COPY_ICON = load("images/copy.png");
	public static final ImageIcon COPY_ICON_BIG = load("images/editcopy.png");
	public static final ImageIcon DELETE_ICON = load("images/button_delete.png");
	public static final ImageIcon DELETE_LAYERS = load("images/deletelayers.png");
	public static final ImageIcon DETALJER_SMALL_ICON = load("images/edit-find.png");
	public static final ImageIcon ACTIVATE_16 = load("images/activate.png");
	public static final ImageIcon DISPOSE_ICON = load("images/edit-delete.png");
	public static final ImageIcon DOCUMENT_NEW = load("images/document-new.png");
	public static final ImageIcon EDIT_ICON = load("images/edit.png");
	public static final ImageIcon ELRAPP_ICON_16 = load("images/elrapp.png");
	public static final ImageIcon EPOST_SMALL_ICON = load("images/internet-mail.png");
	public static final ImageIcon ERROR_ICON = load("images/dialog-error.png");
	public static final ImageIcon ERROR_ICON_SMALL = load("images/dialog-error-small.png");
	public static final ImageIcon EXCEL_ICON16 = load("images/excelicon.png");
	public static final ImageIcon WORD_ICON16 = load("images/wordicon.png");
	public static final ImageIcon POWERPOINT_ICON16 = load("images/powerpointicon.png");
	public static final ImageIcon FITWINDOW_ICON = load("images/fitToWindow.png");
	public static final ImageIcon FORTRESS_ICON = load("images/fortress.png");
	public static final ImageIcon FORTRESS_SELECTED_ICON = load("images/fortress_selected.png");
	public static final ImageIcon FRA_HP_START_ICON = load("images/fraHpStart.png");
	public static final ImageIcon FUNN_ICON = load("images/funn.png");
	public static final ImageIcon FUNN_SMALL_ICON_16 = load("images/funn_small.png");
	public static final ImageIcon GISLEFOSS_ICON = load("images/gisle.png");
	public static final ImageIcon GO_FIRST = load("images/go-first.png");
	public static final ImageIcon GO_LAST = load("images/go-last.png");
	public static final ImageIcon GO_NEXT_ICON = load("images/go-next.png");
	public static final ImageIcon GO_PREVIOUS_ICON = load("images/go-previous.png");
	public static final ImageIcon GRAF_ICON_SMALL = load("images/graf_small.png");
	public static final ImageIcon GRAF_TAB_16 = load("images/graf_tab.png");
	public static final ImageIcon HELE_HP_ICON = load("images/helHp.png");
	public static final ImageIcon HELP_ICON = load("images/help.png");
	public static final ImageIcon HELP_16_ICON = load("images/help_16.png");
	public static final ImageIcon HENDELSE_ICON = load("images/hendelse.png");
	public static final ImageIcon HENDELSE_SMALL_ICON = load("images/hendelse_small.png");
	public static final ImageIcon HISTORY_ICON = load("images/history.png");
	public static final ImageIcon ICON_FARGE = load("images/farge_icon.png");
	public static final ImageIcon ICON_NORGE = load("images/norge_icon.png");
	public static final ImageIcon INFO_ICON = load("images/dialog-info.png");
	public static final ImageIcon INFO_16_DASHBOARD = load("images/dashboard_info_16.png");
	public static final ImageIcon INNSTILLINGER_DASHBOARD = load("images/innstillinger_dashboard.png");	
	public static final ImageIcon INSPEKSJON_ICON = load("images/inspeksjon.png");
	public static final ImageIcon INSPEKSJON_SMALL_ICON = load("images/inspeksjon_small.png");
	public static final ImageIcon LOGO = load("images/logo.png");
	public static final ImageIcon MAIL_SEND = load("images/mail_send.png");
	public static final ImageIcon MAP_NORGE = load("images/MAP_NORGE.png");
	public static final ImageIcon MAP_NORGE_DARK = load("images/MAP_NORGE_DARK.png");
	public static final ImageIcon MAP_PIN = load("images/map_pin.gif");
	public static final ImageIcon MAP_PIN_CENTER = load("images/pin_lightblue.png");
	public static final ImageIcon MAP_PIN_CURSOR = load("images/pin_lightblue_sm.png");
	public static final ImageIcon MAP_PIN_LEFT = load("images/map_pin_lefty.png");
	public static final ImageIcon MAP_PIN_LEFT_SM = load("images/map_pin_lefty_sm.png");
	public static final ImageIcon MAP_PIN_RIGHTY = load("images/map_pin_righty.png");
	public static final ImageIcon MAP_PIN_RIGHTY_RED = load("images/map_pin_righty_red.png");
	public static final ImageIcon MAP_PIN_RIGHTY_YELLOW = load("images/map_pin_righty_yellow.png");
	public static final ImageIcon MAP_PIN_RIGHTY_SM = load("images/map_pin_righty_sm.png");
	public static final ImageIcon MAP_PIN_AVVIK = load("images/map_pin_avvik.png");
	public static final ImageIcon MAP_PIN_HENDELSE = load("images/map_pin_hendelse.png");
	public static final ImageIcon MAP_PIN_FORSIKRINGSSKADE = load("images/map_pin_forsikringsskade.png");
	public static final ImageIcon MAP_PIN_SKRED = load("images/map_pin_skred.png");
	
	public static final ImageIcon MIPSS_FIELD_ICON = load("images/pda.png");
	public static final ImageIcon MIPSS_ICON = load("images/mipss-ico.png");
	public static final ImageIcon MIPSS_LOGG = load("images/mipss_logg.png");
	public static final ImageIcon MISSING_ICON = new MissingIcon();
	public static final ImageIcon MODULE_ICON = load("images/module.png");
	public static final ImageIcon NEW_ICON = load("images/new.png");
	public static final ImageIcon NO_ICON = load("images/cancel.png");
	public static final ImageIcon NORGE_ICON = load("images/norge.png");
	public static final ImageIcon OK_ICON = load("images/ok.png");
	public static final ImageIcon OK2_ICON = load("images/ok2.png");
	public static final ImageIcon OK3_ICON = load("images/ok3.png");
	public static final ImageIcon OK_48 = load("images/ok_48.png");
	public static final ImageIcon OK_INFO_48 = load("images/ok_info_48.png");
	public static final ImageIcon OPPFRISK_DASHBOARD = load("images/oppfrisk_dashboard.png");
	public static final ImageIcon PDA_ICON = load("images/pda-small.png");
	public static final ImageIcon PEN_CURSOR_ICON = load("images/pen_cursor.png");
	public static final ImageIcon PEN_ICON = load("images/pen.png");
	public static final ImageIcon PLABACK_ICON = load("images/tool_playback.png");
	public static final ImageIcon PLAYER_PAUSE = load("images/player_pause.png");
	public static final ImageIcon PLAYER_PLAY = load("images/player_play.png");
	public static final ImageIcon PLAYER_PLAY_S = load("images/player_play24.png");
	public static final ImageIcon PLAYER_STOP = load("images/player_stop.png");
	public static final ImageIcon PLUGIN_CACHED = load("images/plugin-cached.png");
	public static final ImageIcon PLUGIN_NOT_STARTED = load("images/plugin-not-started.png");
	public static final ImageIcon PLUGIN_STARTING = load("images/plugin-starting.png");
	public static final ImageIcon PLUGIN_STOPPING = load("images/plugin-stopping.png");
	public static final ImageIcon POINT_SMALL = load("images/point.png");
	public static final ImageIcon PROSESS_16 = load("images/process.png");
	public static final ImageIcon VIAPUNKT_ICON = load("images/viapunkt_icon.png");
	public static final ImageIcon PREFERENCES_SMALL_ICON = load("images/preferences-system.png");
	public static final ImageIcon PRINT_ICON = load("images/document-print.png");
	public static final ImageIcon QUESTION_ICON = load("images/dialog-question.png");
	public static final ImageIcon RADAR_16 = load("images/radar_16.png");
	public static final ImageIcon RAPPORT_DAME = load("images/rapportdame.png");
	public static final ImageIcon REFRESH_VIEW_ICON = load("images/view-refresh.png");
	public static final ImageIcon REFRESH_16_DASHBOARD = load("images/refresh_16_dashboard.png");
	public static final ImageIcon REMOVE_ALL_ICON = load("images/remove-all.png");
	public static final ImageIcon REMOVE_ITEM_ICON = load("images/list-remove.png");
	public static final ImageIcon REMOVE_ONE_ICON = load("images/remove-one.png");
	public static final ImageIcon RESET_ICON = load("images/reset.png");
	public static final ImageIcon RULER_ICON = load("images/ruler_icon.png");
	public static final ImageIcon ROLES_ICON = load("images/system-roles.png");
	public static final ImageIcon ROTATECCW_ICON = load("images/rotate_ccw.png");
	public static final ImageIcon ROTATECW_ICON = load("images/rotate_cw.png");
	public static final ImageIcon SAVE_ICON = load("images/save.png");
	public static final ImageIcon SAVE2_ICON = load("images/save2.png");
	public static final ImageIcon SAVE_ICON_BIG = load("images/media-floppy.png");
	public static final ImageIcon SEARCH_ICON = load("images/system-search.png");
	public static final ImageIcon SECURE_ICON = load("images/emblem-secure.png");
	public static final ImageIcon SELECT_ALL = load("images/selectrows.png");
	public static final ImageIcon SERIOUS_OK_ICON = load("images/seriousOk.png");
	public static final ImageIcon SHORTCUT_ICON = load("images/shortcut.png");
	public static final ImageIcon SPREADSHEET_SMALL_ICON = load("images/x-office-spreadsheet.png");
	public static final ImageIcon SPREDER_ICON_64 = load("images/sprederikon_64.png");
	public static final ImageIcon SPREDER_ICON_32 = load("images/sprederikon_32.png");
	public static final ImageIcon SPREDER_ICON_24 = load("images/sprederikon_24.png");
	public static final ImageIcon SPREDER_ICON_16 = load("images/sprederikon_16.png");
	public static final ImageIcon STOP_PROCESS_ICON = load("images/process-stop.png");
	public static final ImageIcon TABLE_ICON = load("images/table.png");
	public static final ImageIcon LINEAR_ICON = load("images/linear_icon.png");
	public static final ImageIcon TABLES_ICON = load("images/tables_icon.png");
	public static final ImageIcon TABLE3_ICON = load("images/table3.png");
	public static final ImageIcon TABLE3_PCK_ICON = load("images/table4_package.png");
	public static final ImageIcon TABLE2_ICON = load("images/table2.png");
	public static final ImageIcon TABLE_RESTORE = load("images/tabell_restore.png");
	public static final ImageIcon TEMPERATURE_ICON = load("images/temperature30.png");
	public static final ImageIcon THREADS_ICON = load("images/threads.png");
	public static final ImageIcon TIL_HP_SLUTT_ICON = load("images/tilHpSlutt.png");
	public static final ImageIcon TOOL_PAN_ICON = load("images/tool_pan.png");
	public static final ImageIcon TOOL_ZOOM_ICON = load("images/viewmag+.png");
	public static final ImageIcon TRASH_ICON = load("images/user-trash.png");
	public static final ImageIcon UNTRASH_ICON = load("images/untrash.png");
	public static final ImageIcon VAERSTASJON_ICON = load("images/vaerstasjon_icon.png");
	public static final ImageIcon VEI_ICON = load("images/vei.png");
	public static final ImageIcon VEI_BIG_ICON = load("images/vei_big.png");
	public static final ImageIcon WARNING_ICON = load("images/dialog-warning.png");
	public static final ImageIcon DASHBOARD_WARNING_ICON = load("images/dashboard_warning.png");
	public static final ImageIcon WARNING16= load("images/Tango-dialog-warning.png");
	public static final ImageIcon WARNING_128= load("images/warning_128.png");
	public static final ImageIcon WARNING_72= load("images/warning_72.png");
	public static final ImageIcon WARNING_48= load("images/warning_48.png");
	public static final ImageIcon YELLOW_HELMET_ICON = load("images/yellow-helmet.png");
	public static final ImageIcon YES_ICON = load("images/ok2.png");
	public static final ImageIcon YR_ICON = load("images/yr.png");
	public static final ImageIcon ZOOM_IN_ICON = load("images/zoomIn.png");
	public static final ImageIcon ZOOM_OUT_ICON = load("images/zoomOut.png");
	public static final ImageIcon HAND_CLOSED_ICON = load("images/hand_closed.png");
	public static final ImageIcon HAND_OPEN_ICON = load("images/hand_open.png");
	public static final ImageIcon QMARK_ICON = load("images/qmark.png");
	public static final ImageIcon WIZARD_ICON = load("images/wizard.png");
	public static final ImageIcon TOMATO = load("images/tomato.png");
	public static final ImageIcon START_SANNTID = load("images/start_sanntid.png");
	public static final ImageIcon STOPP_SANNTID = load("images/stopp_sanntid.png");
	public static final ImageIcon SELECT_LEVS = load("images/insertcell.png");
	public static final ImageIcon SELECT_OWNERS = load("images/insertcell2.png");
	public static final ImageIcon SPREDER_ICON = load("images/spreder2.png");
	public static final ImageIcon SPEEDOMETER = load("images/speedometer.png");
	public static final ImageIcon SPEEDOMETER_NEEDLE = load("images/speedometer_needle.png");
	public static final ImageIcon CARBON_PATTERN = load("images/carbonpattern.png");
	public static final ImageIcon CARBON_PATTERN2 = load("images/carbonpattern2.png");
	public static final ImageIcon RETURN = load("images/return.png");
	public static final ImageIcon PREVIOUS = load("images/previous.png");
	public static final ImageIcon NEXT = load("images/next.png");
	public static final ImageIcon ADD_ROW = load("images/add_row.png");
	public static final ImageIcon MAP_MARKER = load("images/map_marker.png");
	public static final ImageIcon REPORTS = load("images/reports.png");
	public static final ImageIcon HISTORY2 = load("images/history2.png");
	public static final ImageIcon PERIOD_STATUS_SUBMITTED_16 = load("images/submitted_16.png");
	public static final ImageIcon PERIOD_STATUS_PROCESSED_16 = load("images/processed_16.png");
	public static final ImageIcon PERIOD_STATUS_PAID_16 = load("images/paid_16.png");
	public static final ImageIcon PERIOD_STATUS_REGISTERED_16 = load("images/registered_16.png");

	public static final ImageIcon GREEN_LIGHT = load("images/green_light.png");
	public static final ImageIcon RED_LIGHT= load("images/red_light.png");
	public static final ImageIcon YELLOW_LIGHT= load("images/yellow_light.png");
	
	public static final ImageIcon GREEN_LIGHT_16 = load("images/green_light_sm.png");
	public static final ImageIcon RED_LIGHT_16= load("images/red_light_sm.png");
	public static final ImageIcon YELLOW_LIGHT_16= load("images/yellow_light_sm.png");
	
	public static final ImageIcon MESTA_HOVEDLOGO = load("images/hovedlogo_farge.jpg");
	public static final ImageIcon MESTA_LOGO_SHADOW = load("images/mesta_logo_shadow.png");
	
	public static final ImageIcon SVV_HOVEDLOGO = load("images/svv_logo.png");
	public static final ImageIcon INFO_16 = load("images/info.png");
	public static final ImageIcon LASTEBIL_ICON = load("images/lastebil_icon.png");
	public static final ImageIcon WEBCAM_ICON_32 = load("images/web_camera.png");
	public static final ImageIcon TIME_16= load("images/full-time_16.png");
	
	public static final ImageIcon SORT_ASC= load("images/sort_asc.png");
	public static final ImageIcon SORT_DESC= load("images/sort_desc.png");
	public static final ImageIcon EMPTY_FOLDER_256 = load("images/empty_folder_256.png");
	public static final ImageIcon IMPORT_EXPORT_64 = load("images/import-export-icon.png");
	
	public static final ImageIcon HOMER_DOH = load("images/homerdoh.png");
	public static final ImageIcon HOMER_AHH = load("images/homer_ahh.png");
	public static final ImageIcon HOMER_THISSUCKS = load("images/homer_thissucks.png");
	public static final ImageIcon HOMER_HAPPY = load("images/homer_happy.png");

	/**
	 * Laster et ikon fra jarfilen
	 * 
	 * @param iconFile
	 * @return
	 */
	public static ImageIcon load(String iconFile) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(iconFile);

		logger.trace("URL for icon(" + iconFile + ") is: " + url);
		if (url == null) {
			// Slik at applikasjonen også virker utenfor webstart
			url = ImageIcon.class.getResource(iconFile);
			logger.trace("URL for icon(" + iconFile + "), second attempt, is: " + url);
		}

		if (url == null) {
			return MISSING_ICON;
		} else {
			return new ImageIcon(url);
		}
	}
}
