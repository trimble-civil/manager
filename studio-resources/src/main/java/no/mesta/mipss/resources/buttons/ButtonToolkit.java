package no.mesta.mipss.resources.buttons;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import no.mesta.mipss.resources.images.IconResources;

/**
 * Klasse for å lage standard knapper
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ButtonToolkit {

	public static JButton createAbortButton() {
		return createButton(IconResources.ABORT_ICON, "Avbryt");
	}

	public static JButton createButton(ImageIcon icon, String text) {
		JButton button = new JButton(text, icon);

		return button;
	}

	public static JButton createCancelButton() {
		return createButton(IconResources.CANCEL_ICON, "Avbryt");
	}

	public static JButton createEmblem(ImageIcon icon, boolean border) {
		JButton emblem = new JButton(icon);
		emblem.setMinimumSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
		emblem.setMaximumSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
		emblem.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
		emblem.setFocusable(false);
		emblem.setEnabled(false);
		emblem.setDisabledIcon(icon);

		if (!border) {
			emblem.setBorder(null);
		}
		return emblem;
	}

	/**
	 * En knapp med tooltip="Gå tilbake", og et ikon
	 * 
	 * @return
	 */
	public static JButton createGoBackButton() {
		JButton button = new JButton(IconResources.GO_PREVIOUS_ICON);
		button.setToolTipText("Gå tilbake");

		return button;
	}

	public static JButton createGoFirstButton() {
		JButton button = new JButton(IconResources.GO_FIRST);
		button.setToolTipText("Gå til første");

		return button;
	}

	public static JButton createGoForwardButton() {
		JButton button = new JButton(IconResources.GO_NEXT_ICON);
		button.setToolTipText("Gå fremover");

		return button;
	}

	public static JButton createGoLastButton() {
		JButton button = new JButton(IconResources.GO_LAST);
		button.setToolTipText("Gå til siste");

		return button;
	}

	public static JButton createNoButton() {
		return createButton(IconResources.NO_ICON, "Nei");
	}

	public static JButton createOkButton() {
		return createButton(IconResources.OK2_ICON, "OK");
	}

	public static JButton createRefreshButton() {
		JButton button = new JButton(IconResources.REFRESH_VIEW_ICON);
		button.setToolTipText("Last på nytt");

		return button;
	}

	public static JButton createSaveButton() {
		return createButton(null, "Lagre");
	}

	public static JButton createSecureEmblem() {
		JButton button = new JButton(IconResources.SECURE_ICON);
		button.setMinimumSize(new Dimension(20, 20));
		button.setMaximumSize(new Dimension(20, 20));
		button.setPreferredSize(new Dimension(20, 20));
		button.setToolTipText("Sikker tilkobling");
		button.setFocusable(false);
		button.setEnabled(false);
		button.setDisabledIcon(IconResources.SECURE_ICON);
		return button;
	}

	public static JButton createSeriousOkButton() {
		return createButton(IconResources.SERIOUS_OK_ICON, "OK");
	}

	public static JButton createStopButton() {
		JButton button = new JButton(IconResources.STOP_PROCESS_ICON);
		button.setToolTipText("Stopp");

		return button;
	}

	public static JButton createYesButton() {
		return createButton(IconResources.YES_ICON, "Ja");
	}
}
