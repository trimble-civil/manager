package no.mesta.mipss.sam;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

public class FileUtils {
	/**
	 * Create a new temporary directory. Use something like
	 * {@link #recursiveDelete(File)} to clean this directory up since it isn't
	 * deleted automatically
	 * @return  the new directory
	 * @throws IOException if there is an error creating the temporary directory
	 */
	public static File createTempDir() throws IOException{
	    final File sysTempDir = getTempDir();
	    File newTempDir;
	    final int maxAttempts = 9;
	    int attemptCount = 0;
	    do{
	        attemptCount++;
	        if(attemptCount > maxAttempts){
	            throw new IOException("The highly improbable has occurred! Failed to " +"create a unique temporary directory after " +maxAttempts + " attempts.");
	        }
	        String dirName = UUID.randomUUID().toString();
	        newTempDir = new File(sysTempDir, dirName);
	    } while(newTempDir.exists());

	    if(newTempDir.mkdirs()){
	        return newTempDir;
	    }
	    else{
	        throw new IOException("Failed to create temp dir named " +newTempDir.getAbsolutePath());
	    }
	}

	public static void appendTempExt(File dir){
		for (String filename : dir.list()){
			File orig = new File(dir, filename);
			filename=filename.substring(0, filename.indexOf('.'))+((new Random().nextInt() & 0xffff)+1)+filename.substring(filename.indexOf('.'));
			File dest = new File(dir, filename);
			orig.renameTo(dest);
		}
		
//		new Random().nextInt() & 0xffff
	}
	
	public static String getRandomFilename(String filename){
		return filename.substring(0, filename.indexOf('.'))+((new Random().nextInt() & 0xffff)+1)+filename.substring(filename.indexOf('.'));
	}
	public static File createTempFile(String name) throws IOException{
		return new File(FileUtils.createTempDir(), name);
	}
	/**
	 * Recursively delete file or directory
	 * @param fileOrDir
	 *          the file or dir to delete
	 * @return
	 *          true iff all files are successfully deleted
	 */
	public static boolean recursiveDelete(File fileOrDir){
	    if(fileOrDir.isDirectory()){
	        for(File innerFile: fileOrDir.listFiles()){
	            if(!recursiveDelete(innerFile)){
	                return false;
	            }
	        }
	    }

	    return fileOrDir.delete();
	}
	
	public static File getTempDir() {
		String tempdir = System.getProperty("java.io.tmpdir");
    	return new File(tempdir);
	}
}
