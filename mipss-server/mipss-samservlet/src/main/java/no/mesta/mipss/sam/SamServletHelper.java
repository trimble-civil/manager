package no.mesta.mipss.sam;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import no.mesta.mipss.samwebservice.SamWebserviceFacade;

import org.datacontract.schemas._2004._07.smart.ArrayOfContent;
import org.datacontract.schemas._2004._07.smart.ArrayOfPoint;
import org.datacontract.schemas._2004._07.smart.Content;
import org.datacontract.schemas._2004._07.smart.FileImport;
import org.datacontract.schemas._2004._07.smart.Point;
import org.datacontract.schemas._2004._07.smart.PointImport;
import org.datacontract.schemas._2004._07.smart.Sender;

public class SamServletHelper {

	private final SamWebserviceFacade samService;
	
	public static final String PARAM_KONTRAKT = "kontrakt_id";
	public static final String PARAM_RODETYPE = "rodetype_id";
	public static final String PARAM_FRA_DATO = "dato_fra";
	public static final String PARAM_TIL_DATO = "dato_til";
	public static final String PARAM_RETURNER_FIL= "fil";
	public static final String PARAM_XML= "xml";
	public static final String DATE_PATTERN = "dd.MM.yyyy HH:mm:ss";


	

	public SamServletHelper(SamWebserviceFacade samService){
		this.samService = samService;
		
	}
	public String createXml(List<Content> content){
		Calendar c = Calendar.getInstance();
		XMLGregorianCalendar xmlCal = dateToCalendar(c.getTime());
		
		Sender sender = new Sender();
		sender.setCompanyField("Mesta Drift AS");
		sender.setTimestampField(xmlCal);
		
		FileImport fi = new FileImport();
		fi.setSenderField(sender);
		ArrayOfContent ao = new ArrayOfContent();
		ao.getContent().addAll(content);
		fi.setContentField(ao);
		return samService.toXML(fi);
	}
	
	public String createXmlPunkt(List<Point> points){
		Calendar c = Calendar.getInstance();
		XMLGregorianCalendar xmlCal = dateToCalendar(c.getTime());
		
		Sender sender = new Sender();
		sender.setCompanyField("Mesta Drift AS");
		sender.setTimestampField(xmlCal);
		
		PointImport pi = new PointImport();
		ArrayOfPoint ap = new ArrayOfPoint();
		ap.getPoint().addAll(points);
		
		pi.setPointField(ap);
		pi.setSenderField(sender);
		
		
		return samService.toXML(pi);
	}
	
	public XMLGregorianCalendar dateToCalendar(Date date){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		XMLGregorianCalendar xmlCal=null;
		try {
			xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xmlCal;
	}
}
