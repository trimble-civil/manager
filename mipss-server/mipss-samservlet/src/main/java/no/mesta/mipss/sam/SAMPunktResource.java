package no.mesta.mipss.sam;

import java.awt.geom.Point2D.Double;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.mesta.mipss.persistence.mipssfield.SamPunktSok;
import no.mesta.mipss.samwebservice.SamWebserviceFacade;
import no.mesta.mipss.service.produksjon.ProduksjonService;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.datacontract.schemas._2004._07.smart.Coordinate;
import org.datacontract.schemas._2004._07.smart.MessageType;
import org.datacontract.schemas._2004._07.smart.Point;
import org.datacontract.schemas._2004._07.smart.Priority;
/**
 * Servlet som sender punktdata til SAM Oslo kommune<br><br>
 * parametere:
 * <p>
 * <code>kontrakt_id:</code> Kontrakten det skal hentes data fra<br>
 * <code>dato_fra:</code> første tidspunkt det skal hentes data for på formatet dd.MM.yyyy HH:mm:ss<br>
 * <code>dato_til:</code> siste tidspunkt det skal hentes data for på formatet dd.MM.yyyy HH:mm:ss<br>
 * <code>xml:</code> returnerer xml til browseren, hvis denne er fraværende <b>vil data sendes til Oslo kommune</b><br>
 * </p>
 * <p>
 * eksempel på url:<br>
 * <p>
 * <b>http://webappst.vegprod.no/samservlet/punkt/?<code>kontrakt_id</code>=261&<code>dato_fra</code>=18.02.2011%2000:00:00&<code>dato_til</code>=20.02.2011%2000:00:00&<code>xml</code>=true</b>
 * </p>
 * <p>
 * for utvikler:<br>
 * <b>http://localhost:3128/samservlet/punkt/?<code>kontrakt_id</code>=261&<code>dato_fra</code>=18.02.2011%2000:00:00&<code>dato_til</code>=20.02.2011%2000:00:00&<code>xml</code>=true</b>
 * </p>
 * </p>
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class SAMPunktResource extends HttpServlet{

	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
	@EJB
	private SamWebserviceFacade samService;

	
	private StringBuilder errMsg = new StringBuilder();
	private String kontraktId;
	private String lagXml;
	private Date fraDato;
	private Date tilDato;

	private SamServletHelper helper;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException{
//		try {
//			InitialContext ctx = new InitialContext();
//			samService= (SamWebserviceFacade) ctx.lookup("ejb/"+SamWebserviceFacade.BEAN_NAME);
//		} catch (NamingException e1) {
//			e1.printStackTrace();
//		}
		helper = new SamServletHelper(samService);
		if (!validateRequest(req, resp)){
			PrintWriter w= resp.getWriter();
			w.write("<html>Could not validate request<br>"+errMsg.toString());
			w.flush();
			w.close();
		}else{
			try{
				handleRequest(req,resp);
			} catch (Exception e){
				PrintWriter pw = resp.getWriter();
				for (String s:ExceptionUtils.getRootCauseStackTrace(e)){
					pw.write(s+"\n");
				}
				pw.write("\n\n");
				pw.write(ExceptionUtils.getFullStackTrace(e));
				
				pw.close();
				e.printStackTrace();
				throw new ServletException(e);
			}
		}
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		initHttpParams(req);
		try{
			if (lagXml!=null){
				resp.setContentType("text/xml");
				PrintWriter w = resp.getWriter();
				w.write(helper.createXmlPunkt(getPunktregistreringer()));
				w.close();
			}
			else{
				String webserviceResult = sendPunktTilSam();
				if (webserviceResult.toLowerCase().equals("ok")){
					resp.setStatus(HttpServletResponse.SC_OK);
				}else{
					resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, webserviceResult);
					throw new ServletException("Msg from SAM:"+webserviceResult);
				}
			}
		} catch (Exception e){
			throw new ServletException(e);
		}
	}
	private String sendPunktTilSam(){
		return samService.sendPoint(getPunktregistreringer());
	}
	@SuppressWarnings("unchecked")
	private List<Point> getPunktregistreringer(){
		StringBuilder sb = new StringBuilder("select guid, utm_x, utm_y, ");
		sb.append("hovedprosess_kode, prosess_kode, prosess_navn, trafikkfare_flagg, ");
		sb.append("beskrivelse, lukket_dato, leverandor, sam_type ");
		sb.append("from table(oslo_sam_pk.sam_punkt(#kontraktId, #fraTidspunkt, #tilTidspunkt))");
		
		Query q = em.createNativeQuery(sb.toString(), SamPunktSok.RESULTSET_MAPPING_NAME);
		q.setParameter("kontraktId", kontraktId);
		q.setParameter("fraTidspunkt", fraDato);
		q.setParameter("tilTidspunkt", tilDato);
		
		List<SamPunktSok> punktregistreringer = (List<SamPunktSok>)q.getResultList();
		List<Point> samPunkter = new ArrayList<Point>();
		for (SamPunktSok s:punktregistreringer){
			samPunkter.add(createPoint(s));
		}
		return samPunkter;
	}
	
	private Point createPoint(SamPunktSok s){
		Point p = new Point();
		p.setCoordinateField(createCoordinate(s));
		p.setMessageTypeField(createMessageType(s));
		if (s.getTrafikkfare()!=null&&s.getTrafikkfare()){
			p.setPriorityField(Priority.HOY);
		}else{
			p.setPriorityField(Priority.NORMAL);
		}
		p.setPriorityFieldSpecified(true);
		p.setRemarksField(s.getBeskrivelse());
		if (s.getProsesskode()!=null)
			p.setSubTypeField(s.getProsessnavn());
		p.setTimestampField(helper.dateToCalendar(s.getLukketDato()));
		p.setVehicleIdField(s.getLeverandor());
		return p;
	}
	
	/**
	 * Konverterer koordinatene til punktreg fra utm33 til utm32 
	 * @param s
	 * @return
	 */
	private Coordinate createCoordinate(SamPunktSok s){
		Double utm32 =null;
		try {
			utm32 = CoordinateUtils.convertToUTM32(s.getUtmX(), s.getUtmY());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		Coordinate c = new Coordinate();
		c.setLongitudeField(utm32.x);
		c.setLatitudeField(utm32.y);
		
		return c;
	}
	
	private MessageType createMessageType(SamPunktSok s){
		return MessageType.fromValue(s.getSamType());
		
	}
	private void initHttpParams(HttpServletRequest req) throws ServletException {
		String datoFra = req.getParameter(SamServletHelper.PARAM_FRA_DATO);
		String datoTil = req.getParameter(SamServletHelper.PARAM_TIL_DATO);
		kontraktId = req.getParameter(SamServletHelper.PARAM_KONTRAKT);
		lagXml = req.getParameter(SamServletHelper.PARAM_XML);
		parseDates(datoFra, datoTil);
	}
	
	private void parseDates(String datoFra, String datoTil) throws ServletException {
		SimpleDateFormat f = new SimpleDateFormat(SamServletHelper.DATE_PATTERN);
		try {
			fraDato = f.parse(datoFra);
			tilDato = f.parse(datoTil);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	private boolean validateRequest(HttpServletRequest req, HttpServletResponse resp) {
		boolean validated=true;

		try{
			String datoFra = req.getParameter(SamServletHelper.PARAM_FRA_DATO);
			String datoTil = req.getParameter(SamServletHelper.PARAM_TIL_DATO);
			String kontraktId = req.getParameter(SamServletHelper.PARAM_KONTRAKT);
			
			datoFra = StringUtils.replace(datoFra, "%20", " ");
			datoTil = StringUtils.replace(datoTil, "%20", " ");
			SimpleDateFormat f = new SimpleDateFormat(SamServletHelper.DATE_PATTERN);
			f.parse(datoFra);
			f.parse(datoTil);
			
			if (!StringUtils.isNumeric(StringUtils.trimToNull(kontraktId))){
				validated = false;
				errMsg.append("kontrakt_id er ikke et heltall<br>");
			}
		} catch (Exception e){
			e.printStackTrace();
			validated=false;
			errMsg.append("feil:"+e.getMessage()+"<br>");
		}
		return validated;
	}
}