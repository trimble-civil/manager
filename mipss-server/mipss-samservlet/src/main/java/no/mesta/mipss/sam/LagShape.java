package no.mesta.mipss.sam;

import java.awt.Point;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.service.produksjon.SamShapeVO;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FeatureStore;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

public class LagShape {

	private final String skift;
	private final String omraade;

	public LagShape(String skift, String omraade){
		this.skift = skift;
		this.omraade = omraade;
		
	}

	public File lagShape(List<SamShapeVO> rs, String prodtype, Date date) throws Exception{
	    FeatureCollection<SimpleFeatureType, SimpleFeature> collection = FeatureCollections.newCollection();
	    GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    
	    SimpleFeatureType type = createFeatureType(); 
	    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(type);
	    
	    System.out.println("Lager shape av:"+rs.size()+" reflinker");
	    
	    for (SamShapeVO r:rs){
	    	
		    
		    CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");	
			CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32632");
			MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
		    List<LineString> lines = new ArrayList<LineString>();
			for (ReflinkStrekning strekning:r.getStrekningList()){
		    	Point[] vectors = strekning.getVectors();
		    	List<Coordinate> cl = new ArrayList<Coordinate>();
		    	for (Point p:vectors){
		    		Coordinate targetCoord = JTS.transform(new Coordinate(p.getX(), p.getY()), new Coordinate(), transform);
					cl.add(targetCoord);
		    	}
		    	Coordinate[] ca = cl.toArray(new Coordinate[]{});
		    	LineString line = geometryFactory.createLineString(ca);
		    	lines.add(line);
		    }
			
			MultiLineString ms = geometryFactory.createMultiLineString(lines.toArray(new LineString[]{}));
			
			featureBuilder.add(ms);
			featureBuilder.add(omraade);
			featureBuilder.add(skift);
			featureBuilder.add(prodtype);
			
			featureBuilder.add(date);
			
			String tilKl = MipssDateFormatter.formatDate(r.getTilTidspunkt(), MipssDateFormatter.TIME_FORMAT);
			String fraTilKl = MipssDateFormatter.formatDate(r.getFraTidspunkt(), MipssDateFormatter.TIME_FORMAT)+"-"+tilKl;
			featureBuilder.add(fraTilKl);
			if (r.getMengde()!=null&&r.getMengde()!=0){
				featureBuilder.add(r.getMateriale());
				featureBuilder.add(r.getMengde());
			}
			
			SimpleFeature feature = featureBuilder.buildFeature(null);
	        collection.add(feature);
	    }
	    
	    File newFile = FileUtils.createTempFile("smart.shp");
	    DataStoreFactorySpi dataStoreFactory = new ShapefileDataStoreFactory();
		
	    Map<String, Serializable> params = new HashMap<String, Serializable>();
	    params.put("url", newFile.toURI().toURL());
	    params.put("create spatial index", Boolean.TRUE);
	
	    ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
	    newDataStore.createSchema(type);
	    CoordinateReferenceSystem crs = CRS.decode("EPSG:32632");
	    newDataStore.forceSchemaCRS(crs);

	    /*
	     * Write the features to the shapefile
	     */
	    Transaction transaction = new DefaultTransaction("create");
	
	    String typeName = newDataStore.getTypeNames()[0];
	    FeatureStore<SimpleFeatureType, SimpleFeature> featureStore =(FeatureStore<SimpleFeatureType, SimpleFeature>) newDataStore.getFeatureSource(typeName);
	
	    featureStore.setTransaction(transaction);
	    try {
	        featureStore.addFeatures(collection);
	        transaction.commit();
	
	    } catch (Exception problem) {
	        problem.printStackTrace();
	        transaction.rollback();
	
	    } finally {
	        transaction.close();
	    }
	    return newFile;
	}
	
	private SimpleFeatureType createFeatureType() {
		
        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
		try {
			CoordinateReferenceSystem crs= CRS.decode("EPSG:32632");
			builder.setCRS(crs);
		} catch (NoSuchAuthorityCodeException e) {
			e.printStackTrace();
		} catch (FactoryException e) {
			e.printStackTrace();
		}

        // add attributes in order
        builder.add("Location", MultiLineString.class);
        builder.length(15).add("Kontrakt", String.class);
        builder.length(15).add("Skift", String.class);
        builder.length(15).add("Tiltak", String.class);
        builder.length(15).add("Dato", Date.class);
        builder.length(30).add("Tidsrom", String.class);
        builder.length(15).add("Materiale", String.class);
        builder.length(15).add("Mengde", Double.class);
        
        // build the type
        final SimpleFeatureType location = builder.buildFeatureType();
        
        return location;
    }	
}
