package no.mesta.mipss.sam;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.samwebservice.SamWebserviceFacade;
import no.mesta.mipss.service.produksjon.ProdReflinkStrekning;
import no.mesta.mipss.service.produksjon.ProduksjonQueryParams;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.service.produksjon.SamShapeVO;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.datacontract.schemas._2004._07.smart.Content;
import org.datacontract.schemas._2004._07.smart.FileType;

/**
 * Servlet som sender data til SAM Oslo kommune<br><br>
 * parametere:
 * <p>
 * <b>kontrakt_id:</b> Kontrakten det skal hentes data fra<br>
 * <b>rodetype_id:</b> Rodetypen som benyttes for å hente ut produksjon<br> 
 * <b>dato_fra:</b> f�rste tidspunkt det skal hentes data for p� formatet dd.MM.yyyy HH:mm:ss<br>
 * <b>dato_til:</b> siste tidspunkt det skal hentes data for p� formatet dd.MM.yyyy HH:mm:ss<br>
 * <b>fil:</b> om det skal lages en fil som sendes i response. ikke til stede hvis det skal sendes til webservice<br>
 * <b>xml:</b> returnerer xml til browseren<br>
 * <b>omraade:</b> hvilket omr�de for Oslo kommune som skal vises i shape (Nord, Vest, Sentrum, �st )<br>
 * <b>skift:</b> hvilket skift dataene gjelder for, vises i shape (Natt, Dag, Kveld)<br>
 * </p>
 * <p>
 * eksempel på url:</br>
 * <b>http://10.60.71.61:8080/samservlet/?kontrakt_id=261&rodetype_id=18&dato_fra=01.11.2011%2006:00:00&dato_til=01.11.2011%2012:00:00&fil=true&omraade=nord&skift=Natt(23:00-07:00)</b>
 * <b>http://localhost:3128/samservlet/?kontrakt_id=261&rodetype_id=18&dato_fra=23.08.2010%2000:00:00&dato_til=23.08.2010%2006:30:00&fil=true&omraade=nord&skift=Natt(23:00-07:00)</b>
 * 
 * </p>
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class SAMResource extends HttpServlet{
	@EJB
	private ProduksjonService service;
	@EJB
	private SamWebserviceFacade samService;

	private static final String PARAM_SHAPE_OMRAADE="omraade"; //Nord, Vest, Sentrum, �st 
	private static final String PARAM_SHAPE_SKIFT="skift"; //Natt (23:00-07:00), Dag (07:00-15:00), Kveld (15:00-23:00)

	
	
	private StringBuilder errMsg=new StringBuilder();
	
	private PropertyResourceBundleUtil bundle = PropertyResourceBundleUtil.getPropertyBundle("samservletText");
	private String omraade;
	private String skift;
	private String kontraktId;
	private String rodetypeId;
	private String lagFil;
	private String lagXml;
	private Date fraDato;
	private Date tilDato;
	
	private SamServletHelper helper;
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException{
		
//		try {
//			InitialContext ctx = new InitialContext();
//			service= (ProduksjonService) ctx.lookup("ejb/"+ProduksjonService.BEAN_NAME);
//			samService= (SamWebserviceFacade) ctx.lookup("ejb/"+SamWebserviceFacade.BEAN_NAME);
//		} catch (NamingException e1) {
//			e1.printStackTrace();
//		}
		
		helper = new SamServletHelper(samService);
		if (!validateRequest(req, resp)){
			PrintWriter w= resp.getWriter();
			w.write("<html>Could not validate request<br>"+errMsg.toString());
			w.flush();
			w.close();
		}else{
			try{
				handleRequest(req, resp);
			} catch (Exception e){
				PrintWriter pw = resp.getWriter();
				for (String s:ExceptionUtils.getRootCauseStackTrace(e)){
					pw.write(s+"\n");
				}
				pw.write("\n\n");
				pw.write(ExceptionUtils.getFullStackTrace(e));
				
				pw.close();
				e.printStackTrace();
				throw new ServletException(e);
			} 
		}
    }
	/**
	 * H�ndterer requesten. Kaller p� de tjenestene som er n�dvendige for � lage og sende shape-filene
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		initHttpParams(req);
		ProduksjonQueryParams params = createQueryParams();
		List<ProdReflinkStrekning> strekninger = service.getProduksjonStrekningerSamShape(params);
		
		Map<String, List<SamShapeVO>> prodMap = getProdMap(strekninger);
		
		try{
			if (lagFil!=null){
				byte[] zipFil = lagFiler(prodMap, fraDato, tilDato);
				ServletOutputStream out = resp.getOutputStream();
				resp.setContentType("application/zip"); 
	            resp.setHeader("Content-Disposition","inline; filename=shape.zip;");
	            out.write(zipFil); 
	            out.flush();
			}
			
			else if (lagXml!=null){
				List<Content> shapeFiler = lagShapeFiler(prodMap, fraDato, tilDato);
				resp.setContentType("text/xml");
				PrintWriter w = resp.getWriter();
				w.write(helper.createXml(shapeFiler));
			}
			else{
				List<Content> shapeFiler = lagShapeFiler(prodMap, fraDato, tilDato);
				String webserviceResult = sendShapeFiler(shapeFiler);
				if (webserviceResult.toLowerCase().equals("ok")){
					resp.setStatus(HttpServletResponse.SC_OK);
				}else{
					resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, webserviceResult);
					throw new ServletException("Msg from SAM:"+webserviceResult);
					
				}
				
			}
		} catch (Exception e){
			throw new ServletException(e);
		}
	}
	
//	public void test() throws ServletException{
//		
//		String datoFra = "04.02.2009 00:00:00";
//		String datoTil = "04.02.2009 02:00:00";
//		parseDates(datoFra, datoTil);
//		kontraktId="261";
//		rodetypeId="18";
//		omraade="Nord";
//		skift="Natt";
//		
//		ProduksjonQueryParams params = createQueryParams();
//		List<ProdReflinkStrekning> strekninger = service.getProduksjonStrekningerSamShape(params);
//		System.out.println(strekninger);
//		Map<String, List<SamShapeVO>> prodMap = getProdMap(strekninger);
//		System.out.println(prodMap);
////		List<Content> shapeFiler = lagShapeFiler(prodMap, fraDato, tilDato);
////		System.out.println(createXml(shapeFiler));
//	}
//	
//	public static void main(String[] args) throws Exception{
//		new SAMResource().test();	
//	}
	   
	private ProduksjonQueryParams createQueryParams() {
		ProduksjonQueryParams params = new ProduksjonQueryParams();
		params.setKontraktId(Long.valueOf(kontraktId));
		params.setRodetypeId(Long.valueOf(rodetypeId));
		params.setFraDato(fraDato);
		params.setTilDato(tilDato);
		return params;
	}
	private void initHttpParams(HttpServletRequest req) throws ServletException {
		String datoFra = req.getParameter(SamServletHelper.PARAM_FRA_DATO);
		String datoTil = req.getParameter(SamServletHelper.PARAM_TIL_DATO);
		kontraktId = req.getParameter(SamServletHelper.PARAM_KONTRAKT);
		rodetypeId = req.getParameter(SamServletHelper.PARAM_RODETYPE);
		lagFil = req.getParameter(SamServletHelper.PARAM_RETURNER_FIL);
		lagXml = req.getParameter(SamServletHelper.PARAM_XML);
		omraade = req.getParameter(PARAM_SHAPE_OMRAADE);
		skift = req.getParameter(PARAM_SHAPE_SKIFT);
		parseDates(datoFra, datoTil);
	}
	private void parseDates(String datoFra, String datoTil) throws ServletException {
		SimpleDateFormat f = new SimpleDateFormat(SamServletHelper.DATE_PATTERN);
		try {
			fraDato = f.parse(datoFra);
			tilDato = f.parse(datoTil);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}
	/**
	 * Lag et map med produksjon, der key er produksjonstype og value er strekningene..
	 * @param strekninger
	 * @return
	 */
	private Map<String, List<SamShapeVO>> getProdMap(List<ProdReflinkStrekning> strekninger) {
		Map<String, List<SamShapeVO>> prodMap = new HashMap<String, List<SamShapeVO>>();
		for (ProdReflinkStrekning ps:strekninger){
			String prodtype = ps.getProdstrekning().getProdtype();
			List<SamShapeVO> list = prodMap.get(prodtype);
			if (list==null){
				list = new ArrayList<SamShapeVO>();
				prodMap.put(prodtype, list);
			}
			if (ps.getStrekningList()!=null){
				SamShapeVO vo = new SamShapeVO();
				vo.setFraTidspunkt(ps.getProdstrekning().getFraTidspunkt());
				vo.setTilTidspunkt(ps.getProdstrekning().getTilTidspunkt());
				vo.setMateriale(ps.getProdstrekning().getMateriale());
				vo.setMengde(ps.getProdstrekning().getMengde());
				vo.setProdtype(prodtype);
				vo.setStrekningList(ps.getStrekningList());
				list.add(vo);
			}
		}
		return prodMap;
	}

	
	/**
	 * Sender shape-filer til webservice
	 * @param shapeFiler
	 */
	private String sendShapeFiler(List<Content> shapeFiler){
		return samService.send(shapeFiler);
	}
	
	/**
	 * Lager og pakker shapefiler i en .zip fil
	 * @param prodMap
	 * @param fraDato
	 * @param tilDato
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private byte[] lagFiler(Map<String, List<SamShapeVO>> prodMap, Date fraDato, Date tilDato) throws ServletException, IOException {
		LagShape s = new LagShape(skift, omraade);		
		File tempZipDir=null;
		try {
			tempZipDir = FileUtils.createTempDir();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (String key:prodMap.keySet()){
			List<SamShapeVO> prod = prodMap.get(key);
			try {
				String filename = getNameFromProdtype(key);
				File shapeDir = s.lagShape(prod, filename, tilDato);
				shapeDir = new File(shapeDir.getPath().substring(0, shapeDir.getPath().lastIndexOf(File.separator)));
				File zipFile = File.createTempFile(filename, ".zip", tempZipDir);
				pakkShapeFiler(shapeDir, zipFile, null);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		String[] files = tempZipDir.list();
		byte[] shapeFil= pakkShapeFiler(tempZipDir, File.createTempFile("shape", ".zip", tempZipDir), files);
		return shapeFil;
	}
	/**
	 * Lager shapefiler av alle produksjonstypene
	 * @param prodMap
	 * @throws ServletException
	 */
	private List<Content> lagShapeFiler(Map<String, List<SamShapeVO>> prodMap, Date fraDato, Date tilDato) throws ServletException {
		LagShape s = new LagShape(skift, omraade);
		List<Content> shapeFiler = new ArrayList<Content>();
		File tempZipDir=null;
		try {
			tempZipDir = FileUtils.createTempDir();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (String key:prodMap.keySet()){
			List<SamShapeVO> prod = prodMap.get(key);
			try {
				String filename = getNameFromProdtype(key);
				File shapeDir = s.lagShape(prod, filename, tilDato);
				shapeDir = new File(shapeDir.getPath().substring(0, shapeDir.getPath().lastIndexOf(File.separator)));
				File zipFile = File.createTempFile(filename, ".zip", tempZipDir);
				byte[] shapeFil = pakkShapeFiler(shapeDir, zipFile, null);
				Content c = new Content();
				c.setProductionTypeField(key);
				c.setFileContentField(shapeFil);
				c.setFileNameField(filename+".zip");
				c.setFileTypeField(FileType.ZIP);
				c.setDateFromField(helper.dateToCalendar(fraDato));
				c.setDateToField(helper.dateToCalendar(tilDato));
				shapeFiler.add(c);
//				FileUtils.recursiveDelete(shapeDir);
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
//		FileUtils.recursiveDelete(tempZipDir);
		return shapeFiler;
	}
    /**
     * Konverterer norske bokstaver til ae, o, a, AE, O, A
     * 
     * @param pt
     * @return
     */
	private String getNameFromProdtype(String pt){
		String ae = bundle.getGuiString("ae");
		String o = bundle.getGuiString("o");
		String a = bundle.getGuiString("a");
		
		String AE = bundle.getGuiString("AE");
		String O = bundle.getGuiString("O");
		String A = bundle.getGuiString("A");
		
		return pt.replace(ae, "ae")
			   .replace(o, "o")
			   .replace(a, "a")
			   .replace(AE, "AE")
			   .replace(O, "O")
			   .replace(A, "A");
		
	}
	
	/**
	 * Pakker sammen alle filene til shape inn i en zip fil og returnerer dette som en byte[]
	 * 
	 * @param src 
	 * @param prodtype
	 * @return
	 * @throws IOException
	 */
	private byte[] pakkShapeFiler(File src, File zipFile, String[] files) throws IOException{
		
		if (files==null)
			files = src.list();		
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
		BufferedInputStream in = null;
	    byte[] data    = new byte[1000];
	    for (int i=0; i<files.length; i++){
	    	String filename = src.getPath() + "/" + files[i]; 
	    	File file = new File(filename);
	    	FileInputStream fi = new FileInputStream(file);
	    	in = new BufferedInputStream(fi, 1000);
	    	ZipEntry z = new ZipEntry(files[i]);
	    	out.putNextEntry(z);
	    	int count;
	    	while((count = in.read(data,0,1000)) != -1){
	    		out.write(data, 0, count);
	    	}
	    	in.close();
	    }
	    out.flush();
	    out.close();
	    FileInputStream outZip = new FileInputStream(zipFile);
	    byte[] fileContent = new byte[(int)zipFile.length()];
	    outZip.read(fileContent);
	    outZip.close();
		return fileContent;
	}
	
	/**
	 * Sjekker at alle de p�krevde parameterne har verdier og er i riktig format. 
	 * 
	 * @param req
	 * @param resp 
	 * @return true hvis alt er i orden
	 * @throws IOException 
	 */
	private boolean validateRequest(HttpServletRequest req, HttpServletResponse resp){
		boolean validated=true;

		try{
			String datoFra = req.getParameter(SamServletHelper.PARAM_FRA_DATO);
			String datoTil = req.getParameter(SamServletHelper.PARAM_TIL_DATO);
			String kontraktId = req.getParameter(SamServletHelper.PARAM_KONTRAKT);
			String rodetypeId = req.getParameter(SamServletHelper.PARAM_RODETYPE);
			
			if (datoFra==null)
				errMsg.append("Dato fra kan ikke v�re null<br>");
			if (datoTil==null)
				errMsg.append("Dato til kan ikke v�re null<br>");
			datoFra = StringUtils.replace(datoFra, "%20", " ");
			datoTil = StringUtils.replace(datoTil, "%20", " ");
			SimpleDateFormat f = new SimpleDateFormat(SamServletHelper.DATE_PATTERN);
			f.parse(datoFra);
			f.parse(datoTil);
			
			if (!StringUtils.isNumeric(StringUtils.trimToNull(kontraktId))){
				validated = false;
				errMsg.append("kontrakt_id er ikke et heltall<br>");
			}
			if (!StringUtils.isNumeric(StringUtils.trimToNull(rodetypeId))){
				validated = false;
				errMsg.append("rodetype_id er ikke et heltall<br>");
			}
		} catch (Exception e){
			e.printStackTrace();
			validated=false;
			errMsg.append("feil:"+e.getMessage()+"<br>");
		}
		return validated;
	}
	
}
