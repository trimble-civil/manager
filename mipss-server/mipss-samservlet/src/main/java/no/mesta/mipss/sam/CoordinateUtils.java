package no.mesta.mipss.sam;

import java.awt.geom.Point2D;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;

public class CoordinateUtils {

	public static Point2D.Double convertToUTM32(double x, double y) throws Exception{
		CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");	
		CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32632");
		MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
		Coordinate c = JTS.transform(new Coordinate(x, y), null, transform);
		return new Point2D.Double(c.x, c.y);
	}
}
