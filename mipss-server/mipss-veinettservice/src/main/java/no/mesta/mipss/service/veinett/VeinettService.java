package no.mesta.mipss.service.veinett;

import java.awt.Point;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

@Remote
public interface VeinettService {
	public static final String BEAN_NAME = "VeinettServiceBean";
	
	/**
     * Henter kontraktsveinettet til gitt kontrakt 
     * @param driftkontraktId id til kontrakten
     * @return
     */
    KontraktveinettVO getKontraktveinett(Long driftkontraktId, boolean veireferanser);
    
    /**
     * Henter ut veinettet til roden med initialisert reflinkseksjonList
     * @param rodeId
     * @return
     */
    Veinett getVeinettForRode(Rode rode);
    /**
     * Henter ut en veireferanse for en gitt posisjon (oppgitt i UTM koordinater)
     * @param point
     * @return
     */
    Veinettveireferanse getVeireferanseForPosisjon(Point point);
    /**
     * Henter ut en reflinkseksjon for en gitt posisjon (oppgitt i UTM koordinater)
     * @param point
     * @return
     */
    Veinettreflinkseksjon getVeinettreflinkseksjonForPosisjon(Point point);
    
    /**
     * Snapper en posisjon til vei
     * @param point
     * @return
     */
    Point snapPosisjonTilVei(Point point);
    
    /**
     * Henter veireferansene til en gitt kontrakt, 
     * 
     * format: Fylkesnummer, kommunenummer, veitype (veikat+veistatus), veinummer, hp
     * eks : 18, 0, EV, 6, 35
     * 
     * @param driftkontraktId
     * @return
     */
    List<VeirefVO> getVeirefListForDriftkontrakt(Long driftkontraktId);
    
    /**
     * Henter veinettreflinkseksjoner for veireferansene
     * @param veirefs
     * @return
     */
    List<Veinettreflinkseksjon> getVeinettreflinkseksjonFromVeireferanser(List<Veinettveireferanse> veirefs);
}
