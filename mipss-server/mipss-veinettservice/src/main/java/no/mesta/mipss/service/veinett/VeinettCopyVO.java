package no.mesta.mipss.service.veinett;

import java.io.Serializable;

@SuppressWarnings("serial")
public class VeinettCopyVO implements Serializable{
	private Long srcVeinettId;
	private Long destVeinettId;
	private Long rodeTilId;
	
	public Long getSrcVeinettId() {
		return srcVeinettId;
	}
	public void setSrcVeinettId(Long srcVeinettId) {
		this.srcVeinettId = srcVeinettId;
	}
	public Long getDestVeinettId() {
		return destVeinettId;
	}
	public void setDestVeinettId(Long destVeinettId) {
		this.destVeinettId = destVeinettId;
	}
	public void setRodeTilId(Long rodeTilId) {
		this.rodeTilId = rodeTilId;
	}
	public Long getRodeTilId() {
		return rodeTilId;
	}
	
}
