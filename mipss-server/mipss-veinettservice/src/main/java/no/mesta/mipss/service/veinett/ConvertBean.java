package no.mesta.mipss.service.veinett;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.json.JSONObject;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kvernetf1.Prodreflinkseksjon;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.veinett.KontraktveiImport;
import no.mesta.mipss.persistence.veinett.KontraktveinettImport;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettimport;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.webservice.NvdbTranslator;
import no.mesta.mipss.webservice.NvdbWebserviceLocal;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

@Stateless (name =Convert.BEAN_NAME, mappedName="ejb/"+Convert.BEAN_NAME)
public class ConvertBean implements Convert{
	private Logger log = LoggerFactory.getLogger(ConvertBean.class);
	
	@PersistenceContext(unitName="mipssPersistence")
	private EntityManager mipss2;
	@EJB 
	private NvdbWebserviceLocal nvdb;
	
	@SuppressWarnings("unchecked")
	//TODO flyttet til veinettimportbean
	public List<Long> getAllVeinettimport(){
		return mipss2.createNamedQuery(Veinettimport.QUERY_FIND_ALL_VEINETT).getResultList();
	}
	
	public void prepareVeinettImport(){
		//call magnes cleanup prosedyre...
	}
	
	public void saveVeinett(Veinett veinett){
		mipss2.persist(veinett);
		log.debug("veinett "+veinett.getId()+" saved");
	}
	
	@SuppressWarnings("unchecked")
	public List<Veinett> getAlleRodersVeinett(){
		return mipss2.createNamedQuery(Veinett.QUERY_FIND_BY_TYPE)
					.setParameter("type", "Rode")
					.getResultList();
	}
	
	public Veinett copyAndTransform(VeinettCopyVO v, String brukerSign){
		Veinett src = mipss2.find(Veinett.class, v.getSrcVeinettId());
		Veinett dst = mipss2.find(Veinett.class, v.getDestVeinettId());
		List<Veinettreflinkseksjon> nyList = new ArrayList<Veinettreflinkseksjon>();
		for (Veinettreflinkseksjon vs :src.getVeinettreflinkseksjonList()){
			List<Veinettreflinkseksjon> vlist = new ArrayList<Veinettreflinkseksjon>();
			vlist.add(vs);
			List<Veinettveireferanse> rr = nvdb.getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(vlist);
			if (rr!=null&&rr.size()>0){
				Veinettreflinkseksjon ny = new Veinettreflinkseksjon();
				ny.setFra(vs.getFra());
				ny.setTil(vs.getTil());
				ny.setReflinkIdent(vs.getReflinkIdent());
				ny.setVeinettveireferanseList(rr);
				ny.setVeinett(dst);
				ny.getOwnedMipssEntity().setOpprettetAv(brukerSign);
				ny.getOwnedMipssEntity().setOpprettetDato(Clock.now());
				nyList.add(ny);
				for (Veinettveireferanse vr:rr){
					vr.setVeinettreflinkseksjon(ny);
					vr.getOwnedMipssEntity().setOpprettetAv(brukerSign);
					vr.getOwnedMipssEntity().setOpprettetDato(Clock.now());
				}
			}
		}
		dst.setVeinettreflinkseksjonList(nyList);
		dst.getOwnedMipssEntity().setEndretAv(brukerSign);
		dst.getOwnedMipssEntity().setEndretDato(Clock.now());
		saveVeinett(dst);
		return dst;
	}
	
	public List<VeinettCopyVO> getAlleVeinettSomSkalTransformeres(){
		Query q = mipss2.createNativeQuery("select gml_veinett_id, ny_veinett_id, ny_rode_id from temp_nytt_veinett");
		
		return new NativeQueryWrapper<VeinettCopyVO>(q, VeinettCopyVO.class, 
				"srcVeinettId", "destVeinettId", "rodeTilId")
				.getWrappedList();
	}
	

	public Long getAntallStrekninger(Long fraStrekningId, Long tilStrekningId){
		StringBuilder q = new StringBuilder();
		q.append("select count(*) from prodstrekning where id between ?1 and ?2");
		BigDecimal count = (BigDecimal)mipss2.createNativeQuery(q.toString()).setParameter(1, fraStrekningId).setParameter(2, tilStrekningId).getSingleResult();
		if (count!=null)
			return count.longValue();
		return null;
		
	}
	public List<BigDecimal> hentAlleProdstrekningerSomSkalGjenopprettes(Long fraStrekningId, Long tilStrekningId){
		StringBuilder query = new StringBuilder();
		query.append("select id from prodstrekning where id between ?1 and ?2");
		List<BigDecimal> resultList = (List<BigDecimal>)mipss2.createNativeQuery(query.toString()).setParameter(1, fraStrekningId).setParameter(2, tilStrekningId).getResultList();
		return resultList;
	}
	
	public void gjenopprettProdreflinkForStrekning(Long[] strekningId){
		for (Long sid:strekningId){
			if (sid==null)
				continue;
			String selQryStr = "select fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, hp, fra_km, til_km from prodstrekning where id = ?";
			String delQryStr = "delete from prodreflinkseksjon where strekning_id=?";
			String insQryStr = "insert into prodreflinkseksjon(strekning_id, reflink_ident, fra, til) values(?,?,?,?)";
			
			Query delQry = mipss2.createNativeQuery(delQryStr).setParameter(1, sid);
			int deletedRows = delQry.executeUpdate();
			System.out.println("ConvertBean removed:"+deletedRows+" rows");
			
			Query selQry = mipss2.createNativeQuery(selQryStr).setParameter(1, sid);
			NativeQueryWrapper<Prodstrekning>  wrapper = new NativeQueryWrapper<Prodstrekning>(selQry, Prodstrekning.class, "fylkesnummer", "kommunenummer", "veikategori", "veistatus", "veinummer", "hp", "fraKm", "tilKm");
			Prodstrekning ps = wrapper.getWrappedSingleResult();
	
			Veinettveireferanse v = new Veinettveireferanse();
			v.setFylkesnummer(ps.getFylkesnummer());
			v.setKommunenummer(ps.getKommunenummer());
			v.setVeikategori(ps.getVeikategori());
			v.setVeistatus(ps.getVeistatus());
			v.setVeinummer(ps.getVeinummer());
			v.setHp(ps.getHp());
			v.setFraKm(ps.getFraKm());
			v.setTilKm(ps.getTilKm());
			
			List<Veinettreflinkseksjon> reflinkSectionsWithinRoadref = nvdb.getReflinkSectionsWithinRoadref(v);
			
			List<Prodreflinkseksjon> prodreflinkseksjon = NvdbTranslator.translateFromVeinettreflinkseksjonToProdreflinkseksjon(reflinkSectionsWithinRoadref);
			for (Prodreflinkseksjon pr:prodreflinkseksjon){
				int inserted = mipss2.createNativeQuery(insQryStr)
				.setParameter(1, sid)
				.setParameter(2, pr.getReflinkIdent())
				.setParameter(3, pr.getFra())
				.setParameter(4, pr.getTil()).executeUpdate();
				System.out.println("ConvertBean: inserted:"+inserted+" rows");
			}
			System.out.println("ferdig med strekning:"+sid);
		}
	}
	
	public List<KontraktveiImport> importFromElrapp(String kontraktNummer, String oppstartAar){
		DefaultHttpClient client = new DefaultHttpClient();
		try {
			kontraktNummer = URLEncoder.encode(kontraktNummer, "UTF-8");
			oppstartAar = URLEncoder.encode(oppstartAar, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = "https://elrapp.nois.no/Vegnett/Vegnett.svc/AktivtVegnett?Kontraktnr="+kontraktNummer+"&Oppstart="+oppstartAar;
		HttpGet method = new HttpGet(url);
		try{
			HttpResponse response = client.execute((HttpUriRequest) method);
			InputStream rstream = response.getEntity().getContent();
			String jsonString = convertStreamToString(rstream);
			jsonString = jsonString.substring(1, jsonString.length()-1);
			jsonString = jsonString.replaceAll("\\\\\\\\r\\\\\\\\n", "");
			jsonString = jsonString.replaceAll("\\\\", "");
			jsonString = jsonString.replace("{\"VegnettCsv\":\"", "");
			jsonString = jsonString.trim();
			jsonString = jsonString.substring(0, jsonString.length()-2);

			String vegnettCsv = jsonString;
			List<KontraktveiImport> values = readValuesFromCsv(vegnettCsv);
			return values;
		} catch (Throwable t){
			t.printStackTrace();
			throw new RuntimeException(t);
		}
	}
	
	private List<KontraktveiImport> readValuesFromCsv(String vegnettCsv){
		String[] values = vegnettCsv.split(";");
		List<KontraktveiImport> imports = new ArrayList<KontraktveiImport>();
		int lengdeHeader = finnLengdeHeader(values);
		String[] object = new String[lengdeHeader];
		String[] headers = new String[lengdeHeader];
		for (int i=0;i<lengdeHeader;i++){
			headers[i]=values[i];
		}
		int count = 0;
		for (int i=lengdeHeader;i<values.length;i++){
			if (i%lengdeHeader==0&&i!=lengdeHeader){
				imports.add(createKontraktveinettImport(object, headers));
				object = new String[lengdeHeader];
				count =0;
			}
			object[count] = values[i];
			count++;
			
		}
		imports.add(createKontraktveinettImport(object, headers));
		return imports;
	}
	private int finnLengdeHeader(String[] values){
		//går ut i fra at første Long markerer slutten på header
		int c = 0;
		for (String s:values){
			try{
				Long.parseLong(s);
				return c++;
			}catch(NumberFormatException e){
				c++;
			}
		}
		return 0;
	}
	//Fylke, Kommune, Vegkategori, Vegstatus, Vegnr, Hp, Frameter, Tilmeter, SecType2, 
	//Veglengde, Frasted, Tilsted, ÅDT, total, Vegfunksjon, Type strategi, Driftsklasse
	private KontraktveiImport createKontraktveinettImport(String[] object, String[] headers){
		KontraktveiImport imp = new KontraktveiImport();
		List<String> hList = Arrays.asList(headers);
		int index = 0;
		if (hList.contains("Fylke"))
			imp.setFylke(toLong(object[index++]));
		if (hList.contains("Kommune"))
			imp.setKommune(toLong(object[index++]));
		if (hList.contains("Vegkategori"))
			imp.setVegkategori(object[index++]);
		if (hList.contains("Vegstatus"))
			imp.setVegstatus(object[index++]);
		if (hList.contains("Vegnr"))
			imp.setVegnummer(toLong(object[index++]));
		if (hList.contains("Hp"))
			imp.setHp(toLong(object[index++]));
		if (hList.contains("Frameter"))
			imp.setFraMeter(toLong(object[index++]));
		if (hList.contains("Tilmeter"))
			imp.setTilMeter(toLong(object[index++]));
		if (hList.contains("SecType2"))
			imp.setSectype2(toLong(object[index++]));
		if (hList.contains("Veglengde"))
			imp.setVeglengde(toLong(object[index++]));
		if (hList.contains("Frasted"))
			imp.setFraSted(object[index++]);
		if (hList.contains("Tilsted"))
			imp.setTilSted(object[index++]);
		if (hList.contains("ÅDT, total"))
			imp.setAadtTotal(toLong(object[index++]));
		if (hList.contains("Vegfunksjon"))
			imp.setVegfunksjon(object[index++]);
		if (hList.contains("Type strategi"))
			imp.setTypeStrategi(object[index++]);
		if (hList.contains("Driftsklasse"))
			imp.setDriftsKlasse(object[index++]);
		return imp;
	}
	
	private Long toLong(String str){
		if (!StringUtils.isBlank(str))
			return Long.valueOf(str);
		return null;
	}
	
	public static String convertStreamToString(java.io.InputStream is) {
	    Scanner s = new Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.apache.http.wire").setLevel(java.util.logging.Level.FINEST);
		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST);

		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug");

		ConvertBean c = new ConvertBean();
		c.importFromElrapp("D0103", "2011");
	}
	
	public List<Driftkontrakt> hentGyldigeKontrakter(){
		return mipss2.createNamedQuery(Driftkontrakt.QUERY_FIND_GYLDIGE_ELRAPP).getResultList();
	}
	public Date getSistOppdatertVegnett(String kontraktNummer, String oppstartAar){
		
		DefaultHttpClient client = new DefaultHttpClient();
		try {
			kontraktNummer = URLEncoder.encode(kontraktNummer, "UTF-8");
			oppstartAar = URLEncoder.encode(oppstartAar, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = "https://elrapp.nois.no/Vegnett/Vegnett.svc/SistOppdatertVegnett?Kontraktnr="+kontraktNummer+"&Oppstart="+oppstartAar;
		
		HttpGet method = new HttpGet(url);
		Date date = null;
		try{
			HttpResponse response = client.execute((HttpUriRequest) method);
			InputStream rstream = response.getEntity().getContent();
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        Document doc = docBuilder.parse(rstream);
	        String dateString = doc.getFirstChild().getTextContent();
	        SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
	        date = f.parse(dateString);
		} catch (Throwable t){
			t.printStackTrace();
		}
		return date;
	}
	
	public KontraktveinettImport getKontraktveinettImportForDriftkontrakt(Long driftkontraktId){
		List<KontraktveinettImport> resultList = mipss2.createNamedQuery(KontraktveinettImport.QUERY_FIND_BY_DRIFTKONTRAKT).setParameter("kontraktId", driftkontraktId).getResultList();
		if (resultList.size()>0)
			return resultList.get(0);
		return null;
	}
	@Override
	public void persistImport(Driftkontrakt kontrakt, List<KontraktveiImport> imports, Date sistOppdatert, String brukerSign) {
		KontraktveinettImport ki = new KontraktveinettImport();
		ki.setKontraktId(kontrakt.getId());
		ki.setOpprettetAv(brukerSign);
		ki.setOpprettetDato(Clock.now());
		ki.setSistOppdatertDato(sistOppdatert);
		mipss2.persist(ki);
		for (KontraktveiImport kis:imports){
			kis.setKontraktveinettImport(ki);
			mipss2.persist(kis);
		}
	}

}
