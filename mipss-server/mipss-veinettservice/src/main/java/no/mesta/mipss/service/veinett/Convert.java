package no.mesta.mipss.service.veinett;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.KontraktveiImport;
import no.mesta.mipss.persistence.veinett.KontraktveinettImport;
import no.mesta.mipss.persistence.veinett.Veinett;

@Remote
public interface Convert {
	public static final String BEAN_NAME="ConvertBean";
	
	/**
	 * Lagrer et veinett i databasen
	 * @param veinett
	 */
	void saveVeinett(Veinett veinett);
	/**
	 * Henter alle veinett med veinetttype Rode
	 * @return
	 */
	List<Veinett> getAlleRodersVeinett();
	List<VeinettCopyVO> getAlleVeinettSomSkalTransformeres();
	Veinett copyAndTransform(VeinettCopyVO v, String brukerSign);
	List<BigDecimal> hentAlleProdstrekningerSomSkalGjenopprettes(Long fraStrekningId, Long tilStrekningId);
	void gjenopprettProdreflinkForStrekning(Long[] strekningId);
	Long getAntallStrekninger(Long fraStrekningId, Long tilStrekningId);
	 
	List<Driftkontrakt> hentGyldigeKontrakter();
	Date getSistOppdatertVegnett(String kontraktNummer, String oppstartAar);
	List<KontraktveiImport> importFromElrapp(String kontraktNummer, String oppstartAar);
	void persistImport(Driftkontrakt kontrakt, List<KontraktveiImport> imports, Date sistOppdatert, String brukerSign);
	KontraktveinettImport getKontraktveinettImportForDriftkontrakt(Long driftkontraktId);
}
