package no.mesta.mipss.service.veinett;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
public class VeirefVO implements Serializable, IRenderableMipssEntity{
	
	private Long fylkesnummer;
	private Long kommunenummer;
	private String veitype; //veikategori + veistatus
	private Long veinummer;
	private Long hp;
	private Long minMeter;
	
	
	public Long getFylkesnummer() {
		return fylkesnummer;
	}
	public Long getKommunenummer() {
		return kommunenummer;
	}
	public String getVeitype() {
		return veitype;
	}
	public Long getVeinummer() {
		return veinummer;
	}
	public Long getHp() {
		return hp;
	}
	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}
	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}
	public void setVeitype(String veitype) {
		this.veitype = veitype;
	}
	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}
	public void setHp(Long hp) {
		this.hp = hp;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof VeirefVO))
			return false;
		if (o==this)
			return true;
		VeirefVO other = (VeirefVO) o;
		
		return new EqualsBuilder()
			.append(fylkesnummer, other.getFylkesnummer())
			.append(kommunenummer, other.getKommunenummer())
			.append(veitype, other.getVeitype())
			.append(veinummer, other.getVeinummer())
			.isEquals();
	}
	@Override
	public int hashCode(){
		return new HashCodeBuilder()
		.append(fylkesnummer)
		.append(kommunenummer)
		.append(veitype)
		.append(veinummer)
		.toHashCode();
	}
	/**
	 * Returnerer fylkesnummer, veitype og veinummer som en streng.
	 */
	@Override
	public String toString(){
		return fylkesnummer+veitype+veinummer;
		/*return new ToStringBuilder(null)
		.append(fylkesnummer)
		.append(veitype)
		.append(veinummer).toString();
		*/
	}
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setMinMeter(Long minMeter) {
		this.minMeter = minMeter;
	}
	public Long getMinMeter() {
		return minMeter;
	}
}
