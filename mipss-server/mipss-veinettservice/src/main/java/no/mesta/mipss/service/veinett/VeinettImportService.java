package no.mesta.mipss.service.veinett;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface VeinettImportService {
	public static final String BEAN_NAME="VeinettImportBean";
	
	/**
	 * Henter alle veinett_id som skal importeres
	 * @return
	 */
	List<Long> getAllVeinettimport();
	
	/**
	 * Importerer veinettet fra veinettimport og transformerer det til veireferanser og reflinkseksjoner, 
	 * deretter knyttes dette mot Veinett.
	 * @param veinett
	 */
	void importFromImportTable(Long veinett, String signatur);
}
