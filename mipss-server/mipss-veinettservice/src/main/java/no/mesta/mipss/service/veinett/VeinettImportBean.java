package no.mesta.mipss.service.veinett;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettimport;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.NvdbWebserviceLocal;
import no.mesta.mipss.webservice.SerializableReflinkSection;
import no.mesta.mipss.webservice.SerializableRoadRef;
import no.mesta.mipss.webservice.service.Veireferanse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless (name =VeinettImportService.BEAN_NAME, mappedName="ejb/"+VeinettImportService.BEAN_NAME)
public class VeinettImportBean implements VeinettImportService{
	private Logger log = LoggerFactory.getLogger(VeinettImportBean.class);

    @PersistenceContext(unitName="mipssPersistence")
	private EntityManager em;
	@EJB 
	private NvdbWebserviceLocal nvdb;
	
	
	@SuppressWarnings("unchecked")
	public List<Long> getAllVeinettimport(){
		return em.createNamedQuery(Veinettimport.QUERY_FIND_ALL_VEINETT).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void importFromImportTable(Long veinettimport, String signatur){
		final Query namedQuery = em.createNamedQuery(Veinettimport.QUERY_FIND_BY_VEINETT);
		namedQuery.setParameter("veinettId", veinettimport);
		namedQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
		List<Veinettimport> veinettList = namedQuery.getResultList();
		for (Veinettimport v:veinettList){
			v.getFraHp();
		}

		List<Veireferanse> veirefList = new ArrayList<Veireferanse>();
		log.debug("found :"+veinettList.size()+" veinettimports");
		for (Veinettimport v:veinettList){
			em.refresh(v);
			veirefList.add(VeinettUtil.getVeireferanseFromVeinettimport(v));
		}
		createVeinett(veirefList, veinettimport, signatur);
	}
	
	private void createVeinett(List<Veireferanse> v, Long vi, String signatur){
		Veinett veinett = em.find(Veinett.class, vi);
		log.debug("Creating veinett:"+vi);
		
		List<Veinettreflinkseksjon> veinettreflinkseksjon = getVeinettreflinkseksjonFromVeireferanseList(SerializableRoadRef.createSerializableRoadRef(v), signatur);
		log.debug("Got :"+veinettreflinkseksjon.size()+" veinettreflinkseksjon from converter");
		veinett.setVeinettreflinkseksjonList(veinettreflinkseksjon);
		
		for (Veinettreflinkseksjon vr:veinettreflinkseksjon){
			log.debug("before:"+vr.getVeinett());
			vr.setVeinett(veinett);
			log.debug("after:"+vr.getVeinett());
		}
		veinett.getOwnedMipssEntity().setEndretAv(signatur);
		veinett.getOwnedMipssEntity().setEndretDato(Clock.now());
		em.persist(veinett);
		log.debug("Veinett saved :");
		VeinettUtil.p(veinett);
		log.debug("DONE");
	}
	
	public List<Veinettreflinkseksjon> getVeinettreflinkseksjonFromVeireferanseList(List<SerializableRoadRef> roadRefs, String signatur){
		log.debug("getVeinettreflinkseksjonFromVeirefereanseList()"+roadRefs.size());
    	ArrayList<Veinettreflinkseksjon> veinettreflinkseksjonList = new ArrayList<Veinettreflinkseksjon>();

    	for (SerializableRoadRef roadRef : roadRefs){
    		List<SerializableReflinkSection> reflinkSeksjonList = nvdb.getReflinkSectionsWithinRoadref(roadRef);
    		log.debug(" F="+roadRef.getCounty()+" K="+roadRef.getMunicipality()+" Kat="+(char)(roadRef.getCategory())+" StO="+(char)(roadRef.getStatusOver())+" St="+(char)(roadRef.getStatus())+" NR="+roadRef.getNumber()+" FraHP="+roadRef.getFromParcel()+" FraKM="+roadRef.getFromMeter()+" TilHp="+roadRef.getToParcel()+" TilKm="+roadRef.getToMeter());
    		log.debug("\tantall reflinkseksjoner="+reflinkSeksjonList.size());
    		Veinettreflinkseksjon veinettreflinkseksjon = null;
			for (SerializableReflinkSection ref:reflinkSeksjonList){
				List<SerializableReflinkSection> refList = new ArrayList<>();
				refList.add(ref);
				List<Veinettveireferanse> results = nvdb.getRoadrefsWithinReflinkSecations(refList);
				if (results!=null){
    				veinettreflinkseksjon = VeinettUtil.getVeinettreflinkseksjon(ref, signatur);
    				for (Veinettveireferanse v:results){
    					v.setVeinettreflinkseksjon(veinettreflinkseksjon);
    					v.getOwnedMipssEntity().setOpprettetAv(signatur);
    			    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    					veinettreflinkseksjon.getVeinettveireferanseList().add(v);
    				}
    				log.debug("antall veireferanser:"+results.size());
    				veinettreflinkseksjonList.add(veinettreflinkseksjon);
				}else{
					log.error("Results from webservice is null, this is wrong and should not happen! reflinkseksjon: id="+ref.getId()+" fra="+ref.getFrom()+" til="+ref.getTo());
				}
			}
    	}
    	return veinettreflinkseksjonList;
    }

    /**
     * For use in testing
     */
    public void setEm(EntityManager em) {
        this.em = em;
    }

    /**
     * For use in testing
     */
    public void setNvdb(NvdbWebserviceLocal nvdb) {
        this.nvdb = nvdb;
    }
	
}
