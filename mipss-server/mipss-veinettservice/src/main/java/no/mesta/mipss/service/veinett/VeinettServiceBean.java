package no.mesta.mipss.service.veinett;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.areal.Areal;
import no.mesta.mipss.persistence.areal.Arealtype;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.webservice.NvdbWebserviceLocal;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service for å hente ut veinettrelaterte objekter som roder, kontraktveinett etc.. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@Stateless(name=VeinettService.BEAN_NAME, mappedName="ejb/"+VeinettService.BEAN_NAME)
public class VeinettServiceBean implements VeinettService{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
	
	@EJB
    private NvdbWebserviceLocal service;
	
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
    public KontraktveinettVO getKontraktveinett(Long driftkontraktId, boolean veireferanser){
		
    	log.debug("Henter kontrakt for id:"+driftkontraktId);
    	Driftkontrakt d = em.find(Driftkontrakt.class, driftkontraktId);
    	em.refresh(d);
    	
    	long start = System.currentTimeMillis();
    	List<Veinettreflinkseksjon> reflinkSeksjoner = em.createNamedQuery(veireferanser?Veinettreflinkseksjon.QUERY_FIND_EAGER:Veinettreflinkseksjon.QUERY_FIND)
    	.setParameter("veinettId", d.getVeinettId())
    	.getResultList();
    	
    	log.debug("Hentet "+reflinkSeksjoner.size()+" på "+(System.currentTimeMillis()-start)+" ms");
    	start = System.currentTimeMillis();
    	List<ReflinkStrekning> geometry = service.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(reflinkSeksjoner);
    	log.debug("Konverterte "+geometry.size()+" reflinkseksjoner til geometry p� "+(System.currentTimeMillis()-start)+" ms");
    	
    	if (veireferanser){
    		for (ReflinkStrekning r:geometry){
	    		for (Veinettreflinkseksjon rs:reflinkSeksjoner){
	    			if (rs.getReflinkIdent().intValue()==r.getReflinkId()&&
	    					rs.getFra().doubleValue()==r.getFra()&&
	    					rs.getTil().doubleValue()==r.getTil()){
	    				r.setVeirefList(rs.getVeinettveireferanseList());
	    				break;
	    			}
	    		}
    		}
    	}
    	if (d.getAreal()==null && geometry!=null && geometry.size()>0){
    		Areal extent = getExtent(geometry);
    		if (extent!=null){
	    		d.setAreal(extent);
	    		extent.setNavn(d.getTextForGUI());
	    		Arealtype type = em.find(Arealtype.class, "DriftkontraktAreal");
	    		extent.setArealtype(type);
	    		em.persist(d);
	    		em.persist(extent);
	    		em.flush();
    		}
    	}
    	
    	KontraktveinettVO kontraktveinett = new KontraktveinettVO(); 
    	kontraktveinett.setVeinettIndices(geometry);
    	kontraktveinett.setKontrakt(d);
    	return kontraktveinett;
    }
	
	/** {@inheritDoc} */
	public List<VeirefVO> getVeirefListForDriftkontrakt(Long driftkontraktId){
		StringBuilder sb = new StringBuilder();
		sb.append("select distinct fylkesnummer, kommunenummer, veikategori||veistatus veitype, veinummer, hp, min(vr.fra_km)*1000 min_meter ");
		sb.append("from veinettveireferanse vr ");
		sb.append("join veinettreflinkseksjon rs on rs.id= vr.reflinkseksjon_id ");
		sb.append("join veinett v on v.id = rs.veinett_id ");
		sb.append("join driftkontrakt d on d.veinett_id=v.id and d.id=? ");
		sb.append("group by fylkesnummer, kommunenummer, veikategori||veistatus, veinummer, hp ");
		sb.append("order by fylkesnummer, veikategori||veistatus, veinummer, hp");
		
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, driftkontraktId);
		
		return new NativeQueryWrapper<VeirefVO>(q, VeirefVO.class, 
				"fylkesnummer", "kommunenummer", "veitype", "veinummer", "hp", "minMeter")
				.getWrappedList();
	}
	
	/**
	 * Lager et areal av en liste med ReflinkStrekning (geometrien til et veinett)
	 * @param geometry
	 * @return
	 */
	private Areal getExtent(List<ReflinkStrekning> geometry){
		Double llx = null;
		Double lly = null;
		Double urx = null;
		Double ury = null;
		
		int c =0; 
		for (ReflinkStrekning rs:geometry){
			for (Point p:rs.getVectors()){
				if (c==0){
					llx = p.getX();
					lly = p.getY();
					urx = p.getX();
					ury = p.getY();
				}
				if (p.getX()<llx)
					llx = p.getX();
				if (p.getY()<lly)
					lly = p.getY();
				
				if (p.getX()>urx)
					urx = p.getX();
				if (p.getY()>ury){
					ury = p.getY();
				}
			}
			c++;
		}
		if (llx==null||lly==null||urx==null||ury==null)
			return null;
		
		Areal areal = new Areal();
		areal.setX1(llx);
		areal.setY1(ury);
		
		Point p1 = new Point();
		p1.setLocation(llx, lly);
		Point p2 = new Point();
		p2.setLocation(urx, lly);
		areal.setDeltaX(calculateDistance(p1, p2));//getWidth(urx, llx));
		p1.setLocation(llx, lly);
		p2.setLocation(llx, ury);
		areal.setDeltaY(calculateDistance(p1, p2));//getHeight(ury, lly));
		return areal;
		
	}
	
	/**
	 * Returenerer bredden til en bbox
	 * @param urx
	 * @param llx
	 * @return
	 */
	private double getWidth(double urx, double llx){   
    	double w = 0;
    	if (urx<0 && llx<0){
    		double x1 = Math.abs(urx);
        	double x2 = Math.abs(llx);
        	w = x2-x1;
    	}else if (llx < 0){
    		double x2 = Math.abs(llx);
    		w = urx+x2;
    	}else{
    		w = urx-llx;
    	}

    	return w;
    }
	
	public double calculateDistance(Point p1, Point p2){
    	double x1 = Math.pow(p1.getX()-p2.getX(), 2);
		double y1 = Math.pow(p1.getY()-p2.getY(), 2);		
		double l = Math.sqrt(x1+y1);
		return l;
    }
	
	/**
	 * Returnerer høyden for en bbox
	 * @param ury
	 * @param lly
	 * @return
	 */
	private double getHeight(double ury, double lly){
		return ury-lly;
	}
	
    
    /** {@inheritDoc} */
	public Veinett getVeinettForRode(Rode rode){
		if (rode.getVeinett()==null)
			return null;
		Query query = em.createNamedQuery(Veinett.QUERY_FIND_WITH_REFLINKSEKSJON).setParameter("id", rode.getVeinett().getId());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		Veinett veinett = (Veinett) query.getSingleResult();
		veinett.getVeinettreflinkseksjonList().size();
		return veinett;
	}
	
	/** {@inheritDoc} */
	public Veinettveireferanse getVeireferanseForPosisjon(Point point) {
		Veinettreflinkseksjon reflink = getVeinettreflinkseksjonForPosisjon(point);
		
		Veinettveireferanse veiref = null;
		if (reflink!=null){
			List<Veinettreflinkseksjon> reflist = new ArrayList<Veinettreflinkseksjon>();
			reflist.add(reflink);
			List<Veinettveireferanse> veirefs = service.getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(reflist);
			if (veirefs!=null&&veirefs.size()>0){
				veiref = veirefs.get(0);
				veiref.setVeinettreflinkseksjon(reflink);
			}else{
				log.debug("fant ingen roadrefs innenfor veinettreflinkseksjonene:"+reflist);
			}
		}else{
			log.debug("Fant ingen veinettreflinkseksjon for posisjon:"+point);
		}
		return veiref;
	}
	//TODO bedre feilhåndtering
	public Point snapPosisjonTilVei(Point point){
		try{
		Veinettreflinkseksjon r = service.getReflinkSectionFromGeometry(point);
		if (r!=null&&r.getFra()==0&&r.getTil()==0&&r.getReflinkIdent()==0){
			return null;
		}
//		Veinettreflinkseksjon r = getVeinettreflinkseksjonForPosisjon(point);
		if (r!=null){
			List<Veinettreflinkseksjon> rl = new ArrayList<Veinettreflinkseksjon>();
			rl.add(r);
			List<ReflinkStrekning> geo = service.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(rl);
			if (geo.size()>0){
				Point[] v = geo.get(0).getVectors();
				if (v!=null&&v.length>0){
					return v[0];
				}
			}
		}
		} catch (Throwable t){
			t.printStackTrace();
			throw new RuntimeException("Feil ved kall mot NvdbWebService");
		}
		return null;
	}
	
	/** {@inheritDoc} */
	public Veinettreflinkseksjon getVeinettreflinkseksjonForPosisjon(Point point){
		try{
			return service.getReflinkSectionFromGeometry(point);
		} catch (Throwable t){
			t.printStackTrace();
		}
		throw new RuntimeException("Feil ved kall mot NvdbWebService");
		/*
		Point point1 = new Point();
		point1.setLocation(point.getX()-10, point.getY()-10);
		Point point2 = new Point();
		point2.setLocation(point.getX()+10, point.getY()+10);
		Point point3 = new Point();
		point3.setLocation(point.getX()-10, point.getY()+10);
		Point point4 = new Point();
		point4.setLocation(point.getX()+10, point.getY()-10);
		
		List<Point> points = new ArrayList<Point>();
    	points.add(point1);
    	points.add(point2);
    	points.add(point3);
    	points.add(point4);
    	List<Veinettreflinkseksjon> reflinks = service.getReflinkSectionsFromGeometry(points);
    	Veinettreflinkseksjon rset=null;
		if (reflinks.size()>0){
			rset = reflinks.get(0);
		}
		return rset;*/
	}
	
	public List<Veinettreflinkseksjon> getVeinettreflinkseksjonFromVeireferanser(List<Veinettveireferanse> veirefs){
		List<Veinettreflinkseksjon> list = new ArrayList<Veinettreflinkseksjon>();
		for (Veinettveireferanse vr:veirefs){
			list.addAll(service.getReflinkSectionsWithinRoadref(vr));
		}
		return list;
	}
    
	
}
