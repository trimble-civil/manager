package no.mesta.mipss.service.veinett;

import java.io.Serializable;
import java.util.List;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.webservice.ReflinkStrekning;

/**
 * Value klasse som holder på en driftkontrakt og en liste med ReflinkStrekning som utgjør
 * et kontraktveinett
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class KontraktveinettVO implements Serializable{
	private List<ReflinkStrekning> veinettIndices;
	private Driftkontrakt kontrakt;
	
	public KontraktveinettVO(){
		
	}

	/**
	 * @return the veinettIndices
	 */
	public List<ReflinkStrekning> getVeinettIndices() {
		return veinettIndices;
	}

	/**
	 * @param veinettIndices the veinettIndices to set
	 */
	public void setVeinettIndices(List<ReflinkStrekning> veinettIndices) {
		this.veinettIndices = veinettIndices;
	}

	/**
	 * @return the kontrakt
	 */
	public Driftkontrakt getKontrakt() {
		return kontrakt;
	}

	/**
	 * @param kontrakt the kontrakt to set
	 */
	public void setKontrakt(Driftkontrakt kontrakt) {
		this.kontrakt = kontrakt;
	}
}
