package no.mesta.mipss.service.veinett;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.webservice.SerializableReflinkSection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettimport;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.service.Veireferanse;

public class VeinettUtil {
	private static Logger log = LoggerFactory.getLogger(VeinettUtil.class);
	
    /**
     * Gjør om et ReflinkSeksjon objekt fra webservice til et Veinettreflinkseksjon objekt
     * til databasen.
     * @param reflinkSeksjon
     * @return
     */
    public static Veinettreflinkseksjon getVeinettreflinkseksjon(SerializableReflinkSection reflinkSeksjon, String signatur){
    	Veinettreflinkseksjon v = new Veinettreflinkseksjon();
    	v.setFra(reflinkSeksjon.getFrom());
    	v.setTil(reflinkSeksjon.getTo());
    	v.setReflinkIdent(new Long(reflinkSeksjon.getId()));
    	v.getOwnedMipssEntity().setOpprettetAv(signatur);
    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	return v;
    }
    
    
    /**
     * Konverterer et Veireferanse object fra webservice til et Veinettveireferanse objekt
     * til databasen 
     * @param veireferanse
     * @return
     */
    public static Veinettveireferanse getVeinettveireferanse(Veireferanse veireferanse, String signatur){
    	if (veireferanse==null)
    		throw new IllegalArgumentException("Veireferanse kan ikke v�re null!");
    	
    	Veinettveireferanse v= new Veinettveireferanse();
    	v.setFraKm((((double)veireferanse.getFraMeter())/1000));
    	v.setFylkesnummer(new Long(veireferanse.getFylkenummer()));
    	v.setHp(new Long(veireferanse.getFrahp())); //TODO her er det noe muffens.. finnes bare en HP i databasen WS returnerer fra og til HP
    	v.setKommunenummer(new Long(veireferanse.getKommunenummer()));
    	v.setTilKm((((double)veireferanse.getTilMeter())/1000));
    	v.setVeikategori((char)veireferanse.getVegkategori()+"");
    	v.setVeinummer(new Long(veireferanse.getVegnummer()));
    	v.setVeistatus((char)veireferanse.getVegstatus()+"");
    	v.getOwnedMipssEntity().setOpprettetAv(signatur);
    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	return v;
    }
    
	/**
	 * Konverterer et veinettimport-objekt til veireferanse for bruk mot webservice
	 */
	public static Veireferanse getVeireferanseFromVeinettimport(Veinettimport veinett){
		Veireferanse veireferanse = new Veireferanse();
		veireferanse.setFrahp(veinett.getFraHp().intValue());
		veireferanse.setTilhp(veinett.getTilHp().intValue());
		veireferanse.setFraMeter(veinett.getFraMeter());
		veireferanse.setTilMeter(veinett.getTilMeter());
		veireferanse.setFylkenummer(veinett.getFylkesnummer().intValue());
		if (veinett.getKommunenummer()!=null)
			veireferanse.setKommunenummer(veinett.getKommunenummer().intValue());
		veireferanse.setVegkategori(veinett.getVeikategori());
		veireferanse.setVegnummer(veinett.getVeinummer().intValue());
		veireferanse.setVegstatus(veinett.getVeistatus());

        char vso;
        if(veinett.getVeistatus()=='W' || veinett.getVeistatus()=='S') {
            vso = 'V';
        } else {
            vso = veinett.getVeistatus();
        }
		veireferanse.setVegstatusoverordnet(vso);
		return veireferanse;
	}
	
	/**
	 * Konverterer en liste med veinettimport-objekter til en liste med veireferanser for bruk mot webservice
	 * @param veinettList
	 * @return
	 */
	public static List<Veireferanse> getVeireferanseListFromVeinettimport(List<Veinettimport> veinettList){
		List<Veireferanse> veireferanseList = new ArrayList<Veireferanse>();
		for (Veinettimport vi:veinettList){
			veireferanseList.add(getVeireferanseFromVeinettimport(vi));
		}
		return veireferanseList;
	}
	
	public static void p(Veinett veinett){
		List<Veinettreflinkseksjon> v = veinett.getVeinettreflinkseksjonList();
		for (Veinettreflinkseksjon r:v){
			log.debug("\t"+r.getFra()+"  "+r.getTil()+"  "+r.getReflinkIdent());
			List<Veinettveireferanse> veinettveireferanseList = r.getVeinettveireferanseList();
			for (Veinettveireferanse vei:veinettveireferanseList){
		 		log.debug(" F="+vei.getFylkesnummer()+" K="+vei.getKommunenummer()+" Kat="+vei.getVeikategori()+" St="+vei.getVeistatus()+" NR="+vei.getVeinummer()+" FraHP="+vei.getHp()+" FraKM="+vei.getFraKm()+" TilHp="+vei.getHp()+" TilKm="+vei.getTilKm());
		 		log.debug("Reflinkseksjonid="+vei.getVeinettreflinkseksjon().getId());
			}
		}
	}
}
