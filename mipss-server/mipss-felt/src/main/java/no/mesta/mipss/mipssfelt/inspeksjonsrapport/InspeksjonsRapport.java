package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class InspeksjonsRapport implements Serializable{
	private String guid;
	private Long refnummer;
	
	private String beskrivelse;
	private String prosessKontrollert;
	private String opprettetAv;
	private Date fraTidspunkt;
	private Date tilTidspunkt;
	
	private List<StrekningAvvik> strekningAvvik;
	private List<Avvik> avvik;
	
	public static String[] getPropertiesArray(){
		return new String[]{"guid", "refnummer", "opprettetAv", "fraTidspunkt", "tilTidspunkt", 
							"beskrivelse", "prosessKontrollert", "strekningAvvik", "avvik"};
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Long getRefnummer() {
		return refnummer;
	}
	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}
	public List<StrekningAvvik> getStrekningAvvik() {
		return strekningAvvik;
	}
	public void setStrekningAvvik(List<StrekningAvvik> strekningAvvik) {
		this.strekningAvvik = strekningAvvik;
	}
	public List<Avvik> getAvvik() {
		return avvik;
	}
	public void setAvvik(List<Avvik> avvik) {
		this.avvik = avvik;
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	public String getProsessKontrollert() {
		return prosessKontrollert;
	}
	public void setProsessKontrollert(String prosessKontrollert) {
		this.prosessKontrollert = prosessKontrollert;
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}
	public void setFraTidspunkt(Date fraTidspunkt) {
		this.fraTidspunkt = fraTidspunkt;
	}
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}
	public void setTilTidspunkt(Date tilTidspunkt) {
		this.tilTidspunkt = tilTidspunkt;
	}
	
	
}
