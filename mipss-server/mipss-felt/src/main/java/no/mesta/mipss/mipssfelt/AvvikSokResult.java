package no.mesta.mipss.mipssfelt;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class AvvikSokResult implements Serializable, IRenderableMipssEntity {
	public static String[] getPropertiesArray() {
		return new String[] { "guid", "sakGuid", "refnummer", "opprettetAv", "opprettetDato", "slettetAv", "slettetDato", "lukketDato",
				"sisteStatus", "statusType", "sisteStatusDato", "tiltaksDato", "prosessId", "prosessKode",
				"prosessNavn", "aarsakId", "aarsakNavn", "tilleggsarbeid",
				"etterslep", "trafikksikkerhet", "fylkesnummer", "kommunenummer", "veikategori",
				"veistatus", "veinummer", "hp", "meter", "kjorefelt",
				"beskrivelse", "antallBilder", "x", "y", "historikkStreng", "mangel", "pdaDfuNavn", "pdaDfuId"};
	}
	@Deprecated
	public static String[] getPropertiesArrayMangler() {
		return new String[] { "guid", "sakGuid", "refnummer", "opprettetAv", "opprettetDato", "slettetAv", "slettetDato",
				/*"sisteStatus", */"tiltaksDato", "prosessId", /*"prosessKode",
				"prosessNavn",*/ "aarsakId", /*"aarsakNavn",*/ "tilleggsarbeid",
				"etterslep", "fylkesnummer", "kommunenummer", "veikategori",
				"veistatus", "veinummer", "hp", "meter", "kjorefelt",
				"beskrivelse", "antallBilder", "mangel"};
	}
	private String slettetAv;
	private Date slettetDato;
	private String guid;
	private String sakGuid;
	private Long refnummer;
	private String opprettetAv;
	private Date opprettetDato;
	private Date lukketDato;
	private String sisteStatus;
	private String statusType;
	private Date sisteStatusDato;
	private Date tiltaksDato;// frist

	private Long prosessId;
	private String prosessKode;
	private String prosessNavn;

	private Long aarsakId;
	private String aarsakNavn;
	private Long antallBilder;

	private Boolean tilleggsarbeid;
	private Boolean etterslep;
	private Boolean trafikksikkerhet;

	private Long fylkesnummer;
	private Long kommunenummer;
	private String veikategori;
	private String veistatus;
	private Long veinummer;
	private Long hp;
	private Long meter;
	private String kjorefelt;

	private String beskrivelse;
	
	private Double snappetX;
	private Double snappetY;
	
	private String historikkStreng;
	
	private Boolean mangel;
	
	private String pdaDfuNavn;
	private String pdaDfuId;
	
	public Double getSnappetX() {
		return snappetX;
	}
	public void setSnappetX(Double snappetX) {
		this.snappetX = snappetX;
	}
	public Double getSnappetY() {
		return snappetY;
	}
	public void setSnappetY(Double snappetY) {
		this.snappetY = snappetY;
	}
	private Double x;
	private Double y;

	public Long getRefnummer() {
		return refnummer;
	}

	public String getOpprettetAv() {
		return opprettetAv;
	}

	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public String getSisteStatus() {
		return sisteStatus;
	}

	public Date getTiltaksDato() {
		return tiltaksDato;
	}

	public Long getProsessId() {
		return prosessId;
	}

	public String getProsessKode() {
		return prosessKode;
	}

	public String getProsessNavn() {
		return prosessNavn;
	}

	public Long getAarsakId() {
		return aarsakId;
	}

	public String getAarsakNavn() {
		return aarsakNavn;
	}

	public Boolean getTilleggsarbeid() {
		return tilleggsarbeid;
	}

	public Boolean getEtterslep() {
		return etterslep;
	}

	public Long getFylkesnummer() {
		return fylkesnummer;
	}

	public Long getKommunenummer() {
		return kommunenummer;
	}

	public String getVeikategori() {
		return veikategori;
	}

	public String getVeistatus() {
		return veistatus;
	}
	
	public String getVeitype(){
		StringBuilder sb = new StringBuilder();
		if (veikategori!=null){
			sb.append(veikategori);
		}
		if (veistatus!=null){
			sb.append(veistatus);
		}
		return sb.toString();
	}

	public Long getVeinummer() {
		return veinummer;
	}

	public Long getHp() {
		return hp;
	}

	public Long getMeter() {
		return meter;
	}

	public String getKjorefelt() {
		return kjorefelt;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public String getVeiGUITxt(){
		StringBuilder sb = new StringBuilder();
		sb.append(getFylkesnummer());
		sb.append("/");
		sb.append(getKommunenummer());
		sb.append(" ");
		sb.append(getVeikategori());
		sb.append(getVeistatus());
		sb.append(getVeinummer());
		sb.append(" HP:");
		sb.append(getHp());
		sb.append(" m:");
		sb.append(getMeter());
		return sb.toString();
	}
	
	public AvvikSokResult getThisForReport(){
		return this;
	}
	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setSisteStatus(String sisteStatus) {
		this.sisteStatus = sisteStatus;
	}

	public void setTiltaksDato(Date tiltaksDato) {
		this.tiltaksDato = tiltaksDato;
	}

	public void setProsessId(Long prosessId) {
		this.prosessId = prosessId;
	}

	public void setProsessKode(String prosessKode) {
		this.prosessKode = prosessKode;
	}

	public void setProsessNavn(String prosessNavn) {
		this.prosessNavn = prosessNavn;
	}

	public void setAarsakId(Long aarsakId) {
		this.aarsakId = aarsakId;
	}

	public void setAarsakNavn(String aarsakNavn) {
		this.aarsakNavn = aarsakNavn;
	}

	public void setTilleggsarbeid(Boolean tilleggsarbeid) {
		this.tilleggsarbeid = tilleggsarbeid;
	}

	public void setEtterslep(Boolean etterslep) {
		this.etterslep = etterslep;
	}

	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}

	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}

	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}

	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}

	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}

	public void setHp(Long hp) {
		this.hp = hp;
	}

	public void setMeter(Long meter) {
		this.meter = meter;
	}

	public void setKjorefelt(String kjorefelt) {
		this.kjorefelt = kjorefelt;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public String getTextForGUI() {
		return null;
	}

	public void setAntallBilder(Long antallBilder) {
		this.antallBilder = antallBilder;
	}

	public Long getAntallBilder() {
		return antallBilder;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return guid;
	}

	public void setSakGuid(String sakGuid) {
		this.sakGuid = sakGuid;
	}

	public String getSakGuid() {
		return sakGuid;
	}

	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}

	public Date getSlettetDato() {
		return slettetDato;
	}

	public void setSlettetAv(String slettetAv) {
		this.slettetAv = slettetAv;
	}

	public String getSlettetAv() {
		return slettetAv;
	}
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
	public String getStatusType() {
		return statusType;
	}
	public void setSisteStatusDato(Date sisteStatusDato) {
		this.sisteStatusDato = sisteStatusDato;
	}
	public Date getSisteStatusDato() {
		return sisteStatusDato;
	}
	public void setLukketDato(Date lukketDato) {
		this.lukketDato = lukketDato;
	}
	public Date getLukketDato() {
		return lukketDato;
	}
	
	public Double getX() {
		return x;
	}
	public void setX(Double x) {
		this.x = x;
	}
	public Double getY() {
		return y;
	}
	public void setY(Double y) {
		this.y = y;
	}
	public boolean equals(Object o){
		if (!(o instanceof AvvikSokResult))
			return false;
		AvvikSokResult other = (AvvikSokResult)o;
		return new EqualsBuilder().append(other.guid, guid).isEquals();
	}
	public void setHistorikkStreng(String historikkStreng) {
		this.historikkStreng = historikkStreng;
	}
	public String getHistorikkStreng() {
		return historikkStreng;
	}

	public Boolean getMangel(){
		return mangel;
	}
	public void setMangel(Boolean mangel){
		this.mangel = mangel;
	}
	public void setPdaDfuNavn(String pdaDfuNavn) {
		this.pdaDfuNavn = pdaDfuNavn;
	}
	public String getPdaDfuNavn() {
		return pdaDfuNavn;
	}
	public void setTrafikksikkerhet(Boolean trafikksikkerhet) {
		this.trafikksikkerhet = trafikksikkerhet;
	}
	public Boolean getTrafikksikkerhet() {
		return trafikksikkerhet;
	}
	public void setPdaDfuId(String pdaDfuId) {
		this.pdaDfuId = pdaDfuId;
	}
	public String getPdaDfuId() {
		return pdaDfuId;
	}
}
