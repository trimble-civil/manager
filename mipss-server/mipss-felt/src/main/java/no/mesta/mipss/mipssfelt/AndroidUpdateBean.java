package no.mesta.mipss.mipssfelt;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Dokument;

@Stateless(name = AndroidUpdateService.BEAN_NAME, mappedName="ejb/"+AndroidUpdateService.BEAN_NAME)
public class AndroidUpdateBean implements AndroidUpdateService{
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;

	@Override
	public Dokument getLatest() {
		Dokument dok = (Dokument)em.createNamedQuery(Dokument.FIND_BY_DOKTYPE).setParameter("doktypenavn", "Androidklient").getSingleResult();
		return dok;
	}
	
	@Override
	public Dokument getLatestManual() {
		Dokument dok = (Dokument)em.createNamedQuery(Dokument.FIND_BY_DOKTYPE).setParameter("doktypenavn", "AndroidBrukermanual").getSingleResult();
		return dok;
	}

	
	public void update(byte[] lob, String brukerSign){
		Dokument dok = (Dokument)em.createNamedQuery(Dokument.FIND_BY_DOKTYPE).setParameter("doktypenavn", "Androidklient").getSingleResult();
		dok.setLob(lob);
		dok.getOwnedMipssEntity().setEndretAv(brukerSign);
		dok.getOwnedMipssEntity().setEndretDato(Clock.now());
		em.merge(dok);
	}
	
	public void updateBrukermanual(byte[] lob, String brukerSign){
		Dokument dok = (Dokument)em.createNamedQuery(Dokument.FIND_BY_DOKTYPE).setParameter("doktypenavn", "AndroidBrukermanual").getSingleResult();
		dok.setLob(lob);
		dok.getOwnedMipssEntity().setEndretAv(brukerSign);
		dok.getOwnedMipssEntity().setEndretDato(Clock.now());
		em.merge(dok);
	}

	
}
