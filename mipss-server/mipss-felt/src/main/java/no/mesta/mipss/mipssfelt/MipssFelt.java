package no.mesta.mipss.mipssfelt;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.valueobjects.ElrappTypeR;

/**
 * Tjeneste for mipss-felt-studio for entiteter som ikke er direkte knyttet mot mipss-felt entitetene
 * @author harkul
 *
 */
@Remote
public interface MipssFelt {

	public static final String BEAN_NAME = "MipssFeltBean";
	
	/**
	 * Henter alle bildene som er tilknyttet en sak.
	 * Henter fra avvik, hendelse, skred og forsikringsskade
	 * @param sakGuid
	 * @return
	 */
	List<Bilde> getBilderFromSak(String sakGuid);
	/**
	 * Henter alle brukerne som har registrert noe i Sak eller Inspeksjon
	 * Settes pdaDfuId begrenses listen også til pda
	 * @return
	 */
	List<String> getFeltBrukere(Long kontraktId, Long[] pdaDfuId);
	
	Hendelse hentHendelse(String guid);
	Hendelse hentHendelseRapport(String guid);
	List<Bilde> hentHendelseBilder(String guid);
	Forsikringsskade hentForsikringsskade(String guid);
	Skred hentSkred(String guid);
	
	Long getDriftkontraktIdForRskjema(Long refnummer, ElrappTypeR.Type typeR);
	Long getDriftkontraktIdForInspeksjon(Long refnummer);
	Long getDriftkontraktIdForAvvik(Long refnummer);
	Long getDriftkontraktIdForSak(Long refnummer);
	
	NokkeltallFelt hentMangelStatistikk(Long driftkontraktId);
	/**
	 * Henter guid til avvik som tilhører saken
	 * @param sakGuid
	 * @return
	 */
	List<FeltEntityInfo> hentEntityInfoAvvik(String sakGuid);
	/**
	 * Henter guid til hendelser som tilhører saken
	 * @param sakGuid
	 * @return
	 */
	List<FeltEntityInfo> hentEntityInfoHendelse(String sakGuid) ;
	/**
	 * Henter guid til skred som tilhører saken
	 * @param sakGuid
	 * @return
	 */
	List<FeltEntityInfo> hentEntityInfoSkred(String sakGuid) ;
	/**
	 * Henter bilder tilhørende en gitt forsikringsskade
	 * @param guid
	 * @return
	 */
	List<Bilde> hentForsikringsskadeBilder(String guid);
	
	/**
	 * Lagrer en sak i databasen, Det er ikke satt opp cascading på relasjonene så kun Sak sine attributter vil bli lagret. 
	 * @param sak
	 * @return
	 */
	Sak persistSak(Sak sak);

	<E extends MipssEntityBean<E>> List<E> hentObjekter(Class<E> clazz, List<String> guids);

	List<FeltEntityInfo> hentEntityInfoForsikringsskade(String sakGuid);
	
	/**
	 * Lagrer bildene til synkbildetabellen
	 * @param bilder
	 */
	void persistSynkbilde(List<String> bildeGuids, String brukerSign);
}
