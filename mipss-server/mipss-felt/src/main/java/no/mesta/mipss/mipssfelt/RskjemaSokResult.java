package no.mesta.mipss.mipssfelt;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.valueobjects.ElrappStatus;
import no.mesta.mipss.valueobjects.ElrappTypeR;

@SuppressWarnings("serial")
public class RskjemaSokResult implements Serializable, IRenderableMipssEntity{

	private String guid;
	private Long refnummer;
	private String typeR;
	private Date opprettetDato;
	private String opprettetAv;
	private Long fylkesnummer;
	private Long kommunenummer;
	private String veikategori;
	private String veistatus;
	private Long veinummer;
	private Long hp;
	private Long meter;
	private String skadested;
	private String beskrivelse;
	private Long elrappId;
	private Long elrappVersjon;
	private Date rapportertDato;
	private Date endretDato;
	private Date slettetDato;
	private String skjemaEmne;
	private Long sakRefnummer;
	private String sakGuid;
	private String pdaDfuNavn;
	
	private Double snappetX;
	private Double snappetY;
	private Double x;
	private Double y;
	
	public static String[] getPropertiesArray() {
		return new String[] { "guid", "refnummer", "typeR", "opprettetDato",
				"fylkesnummer", "kommunenummer", "veikategori",
				"veistatus", "veinummer", "hp", "meter",
				"skadested", "beskrivelse", "elrappId",
				"elrappVersjon", "rapportertDato", "skjemaEmne", "endretDato", "slettetDato", "sakRefnummer", "sakGuid" };
	}

	public Long getRefnummer() {
		return refnummer;
	}

	public String getTypeR() {
		return typeR;
	}

	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public Long getFylkesnummer() {
		return fylkesnummer;
	}

	public Long getKommunenummer() {
		return kommunenummer;
	}

	public String getVeikategori() {
		return veikategori;
	}

	public String getVeistatus() {
		return veistatus;
	}

	public String getVeitype(){
		return getVeikategori()!=null?getVeikategori()+getVeistatus():null;
	}

	public Long getVeinummer() {
		return veinummer;
	}

	
	public Long getHp() {
		return hp;
	}

	public Long getMeter() {
		return meter;
	}

	public String getSkadested() {
		return skadested;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public Long getElrappId() {
		return elrappId;
	}

	public Long getElrappVersjon() {
		return elrappVersjon;
	}

	public Date getRapportertDato() {
		return rapportertDato;
	}

	public String getSkjemaEmne() {
		return skjemaEmne;
	}

	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}

	public void setTypeR(String typeR) {
		this.typeR = typeR;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}

	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}

	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}

	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}

	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}

	public void setHp(Long hp) {
		this.hp = hp;
	}

	public void setMeter(Long meter) {
		this.meter = meter;
	}

	public void setSkadested(String skadested) {
		this.skadested = skadested;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public void setElrappId(Long elrappId) {
		this.elrappId = elrappId;
	}

	public void setElrappVersjon(Long elrappVersjon) {
		this.elrappVersjon = elrappVersjon;
	}

	public void setRapportertDato(Date rapportertDato) {
		this.rapportertDato = rapportertDato;
	}

	public void setSkjemaEmne(String skjemaEmne) {
		this.skjemaEmne = skjemaEmne;
	}

	public String getTextForGUI() {
		return null;
	}

	public ElrappStatus getElrappStatus() {
//		if (elrappStatus !=null)
//			return elrappStatus;
		if (getRapportertDato()==null){
			return new ElrappStatus(ElrappStatus.Status.IKKE_SENDT); 
		}
		if (getEndretDato()!=null){
			if (getRapportertDato().before(getEndretDato())){
				return new ElrappStatus(ElrappStatus.Status.ENDRET);
			}
		}
		return new ElrappStatus(ElrappStatus.Status.SENDT);
//		return elrappStatus;
	}

	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}

	public Date getSlettetDato() {
		return slettetDato;
	}

	public void setEndretDato(Date endretDato) {
		this.endretDato = endretDato;
	}

	public Date getEndretDato() {
		return endretDato;
	}

	public void setSakGuid(String sakGuid) {
		this.sakGuid = sakGuid;
	}

	public String getSakGuid() {
		return sakGuid;
	}

	public void setSakRefnummer(Long sakRefnummer) {
		this.sakRefnummer = sakRefnummer;
	}

	public Long getSakRefnummer() {
		return sakRefnummer;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return guid;
	}
	
	public ElrappTypeR.Type getElrappTypeR(){
		if ("R2".equals(getTypeR())){
			return ElrappTypeR.Type.R2;
		}
		if ("R5".equals(getTypeR())){
			return ElrappTypeR.Type.R5;
		}
		if ("R11".equals(getTypeR())){
			return ElrappTypeR.Type.R11;
		}
		return null;
	}

	public Double getSnappetX() {
		return snappetX;
	}

	public void setSnappetX(Double snappetX) {
		this.snappetX = snappetX;
	}

	public Double getSnappetY() {
		return snappetY;
	}

	public void setSnappetY(Double snappetY) {
		this.snappetY = snappetY;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public void setPdaDfuNavn(String pdaDfuNavn) {
		this.pdaDfuNavn = pdaDfuNavn;
	}

	public String getPdaDfuNavn() {
		return pdaDfuNavn;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public String getOpprettetAv() {
		return opprettetAv;
	}

}
