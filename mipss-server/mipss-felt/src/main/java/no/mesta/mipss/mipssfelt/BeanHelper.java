package no.mesta.mipss.mipssfelt;

import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.persistence.EagerFetcher;
import no.mesta.mipss.persistence.MipssEntityBean;

public class BeanHelper {
	public static void fetchAndRefresh(EntityManager em, MipssEntityBean<?> entity, String... fields){
		EagerFetcher.fetch(entity, fields);
		em.refresh(entity);
	}
	
	/**
	 * Sletter objekter som ikke lenger skal være med i relasjoner
	 * 
	 * @param old
	 * @param current
	 */
	public static void deleteOldEntities(EntityManager em, List<?> old, List<?> current) {
		for (Object o : old) {
			if (!current.contains(o)) {
				em.remove(o);
			}
		}
	}
	
}
