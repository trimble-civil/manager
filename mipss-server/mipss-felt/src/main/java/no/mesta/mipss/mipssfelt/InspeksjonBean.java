package no.mesta.mipss.mipssfelt;

import java.sql.Array;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.StructHelper;
import no.mesta.mipss.query.WhereAndBuilder;

@Stateless(name = InspeksjonService.BEAN_NAME, mappedName="ejb/"+InspeksjonService.BEAN_NAME)
public class InspeksjonBean implements InspeksjonService{
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	
	/** {@inheritDoc} */
	public List<InspeksjonSokResult> sokInspeksjoner(InspeksjonQueryParams params) throws Exception{
        StringBuilder query = getInspeksjonSokQuery();

        WhereAndBuilder builder = new WhereAndBuilder();
        if (params.isKunEtterslep())
        	builder.addWhereAnd("etterslep_flagg=1");
        if (params.isKunSlettede()){
        	builder.addWhereAnd("slettet_dato is not null");
        }else{
        	builder.addWhereAnd("slettet_dato is null");
        }
        
        query.append(builder.toString());
        query.append(" order by opprettet_dato desc ");
        
        StructHelper structHelper = new StructHelper();
        
        Query q = em.createNativeQuery(query.toString())
		.setParameter("kontraktId", params.getValgtKontrakt().getId())
		.setParameter("veiliste", getVeiArray(structHelper, params.getVeiListe(), params.isKunEtterslep()))
		.setParameter("pdaliste", getPdaArray(structHelper, params.getPdaDfuIdentList()))
		.setParameter("brukerliste", getBrukerArray(structHelper, params.getBrukerList()))
		.setParameter("fradato", params.getDateFra())
		.setParameter("tildato", params.getDateTil())
        .setParameter("prosessliste",  getProsessArray(structHelper, params.getProsesser()));

		structHelper.goodnight();
		
        NativeQueryWrapper<InspeksjonSokResult> wrapper = new NativeQueryWrapper<InspeksjonSokResult>(q, InspeksjonSokResult.class, InspeksjonSokResult.getPropertiesArray());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");
        
		return wrapper.getWrappedList();
	}
	
	/** {@inheritDoc} */
	public Inspeksjon hentInspeksjon(String guid){
		Inspeksjon inspeksjon = em.find(Inspeksjon.class, guid);
		BeanHelper.fetchAndRefresh(em, inspeksjon, "prosesser", "strekninger", "inspiserteProsesser");
		//eager fetching..
		for (Sak s:inspeksjon.getSakList()){
			for (Avvik a:s.getAvvik()){
				a.getStatus();
			}
			s.getVeiref();
//			BeanHelper.fetchAndRefresh(em, s, "avvik");
		}
		
		return inspeksjon;
	}
	
	/** {@inheritDoc} */
	public Inspeksjon hentInspeksjon(Long refnummer){
		try{
			String guid = (String)em.createNativeQuery("select guid from inspeksjon_xv where refnummer=?").setParameter(1, refnummer).getSingleResult();
			return hentInspeksjon(guid);
		} catch (NoResultException e){
			return null;
		}
	}
	
	/** {@inheritDoc} */
	public Inspeksjon deleteInspeksjon(Inspeksjon inspeksjon, String brukerSign) {
		inspeksjon.setSlettetAv(brukerSign);
		inspeksjon.setSlettetDato(Clock.now());
		inspeksjon = em.merge(inspeksjon);
		return inspeksjon;
	}
	
	/** {@inheritDoc} */
	public void deleteInspeksjon(List<InspeksjonSokResult> inspeksjonList, String brukerSign){
		for (InspeksjonSokResult i:inspeksjonList){
			Inspeksjon in = em.find(Inspeksjon.class, i.getGuid());
			in.setSlettetAv(brukerSign);
			in.setSlettetDato(Clock.now());
			em.merge(in);
		}
	}
	
	/** {@inheritDoc} */
	public Inspeksjon persistInspeksjon(Inspeksjon in) {
		Inspeksjon i = em.find(Inspeksjon.class, in.getGuid());
		i.setBeskrivelse(in.getBeskrivelse());
		i.setEtterslepsregFlagg(in.getEtterslepsregFlagg());
		return em.merge(i);
	}
	
	/**
	 * Lager en java.sql.Array av en liste med Veinettveireferanser
	 * @param sh
	 * @param veiList
	 * @param inkluderKmIVeiListe
	 * @return
	 * @throws Exception
	 */
	private Array getVeiArray(StructHelper sh, List<Veinettveireferanse> veiList, boolean inkluderKmIVeiListe) throws Exception{
		return sh.createVeireferanseArray(veiList, inkluderKmIVeiListe);
	}
	
	/**
	 * Lager en java.sql.Array av en array med brukere
	 * @param sh
	 * @param brukerArray
	 * @return
	 * @throws Exception
	 */
	private Array getBrukerArray(StructHelper sh, String[] brukerArray) throws Exception{
		List<String> brukerList = brukerArray==null?null:Arrays.asList(brukerArray);
        return sh.createBrukerArray(brukerList);
	}
	
	/**
	 * Lager en java.sql.Array av en array med pdaDfuId
	 * @param sh
	 * @param pda
	 * @return
	 * @throws Exception
	 */
	private Array getPdaArray(StructHelper sh, Long[] pda) throws Exception{
		List<Long> pdaList = pda==null?null:Arrays.asList(pda);
		return sh.createPdaArray(pdaList);
	}
	
	/**
	 * Lager en java.sql.Array av en array med prosessid
	 * @param sh
	 * @param prosesser
	 * @return
	 * @throws Exception
	 */
	private Array getProsessArray(StructHelper sh, Long[] prosesser) throws Exception{
        List<Long> prosessList = prosesser==null?null:Arrays.asList(prosesser);
		return sh.createProsessArray(prosessList);
	}
	/**
	 * Spørring for å hente ut inspeksjoner fra databasen
	 * @return
	 */
	private StringBuilder getInspeksjonSokQuery(){
        StringBuilder query = new StringBuilder();
        query.append("select refnummer, guid, opprettet_av, opprettet_dato, fra_tidspunkt, ");
        query.append("til_tidspunkt, etterslep_flagg, antall_saker, hp_insp_text, prosess_insp_text, vaer_id, ");
        query.append("vaer_navn, vind_id, vind_navn, temperatur_id, temperatur_navn, ev_insp, rv_insp, fv_insp, ");
        query.append("kv_insp, pda_dfu_ident, pda_dfu_navn, beskrivelse, slettet_av, slettet_dato ");
        query.append("from table(feltstudio_pk.inspeksjon_sok(#kontraktId, #fradato, #tildato, #veiliste, #pdaliste, #brukerliste, #prosessliste)) ");
        return query;
	}
}
