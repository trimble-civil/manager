package no.mesta.mipss.mipssfelt;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

/**
 * Tjeneste for inspeksjoner og tilhørende entiteter
 * @author harkul
 *
 */
@Remote
public interface InspeksjonService {
	public static final String BEAN_NAME = "InspeksjonBean";
	
	/**
	 * Søker etter inspeksjoner med gitte parametere
	 * @param params
	 * @return
	 */
	List<InspeksjonSokResult> sokInspeksjoner(InspeksjonQueryParams params) throws Exception;
	
	/**
	 * Henter en inspeksjon med angitt guid, og eager-fetcher prosesser og strekninger.
	 * @param guid
	 * @return
	 */
	Inspeksjon hentInspeksjon(String guid);
	Inspeksjon hentInspeksjon(Long refnummer);
	/**
	 * Sletter en inspeksjon i databasen( setter slettet dato og brukersign)
	 * @param inspeksjon
	 * @param brukerSign
	 * @return
	 */
	Inspeksjon deleteInspeksjon(Inspeksjon inspeksjon, String brukerSign);
	void deleteInspeksjon(List<InspeksjonSokResult> inspeksjonList, String brukerSign);
	
	Inspeksjon persistInspeksjon(Inspeksjon in);
}
