package no.mesta.mipss.mipssfelt;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;

/**
 * Søkeresultatobjekt for søking etter inspeksjoner. 
 * 
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class InspeksjonSokResult implements Serializable, IRenderableMipssEntity, MipssFieldDetailItem{
	public static String[] getPropertiesArray(){
		return new String[]{"refnummer", "guid", "opprettetAv", "opprettetDato", "fraTidspunkt", "tilTidspunkt", 
							"etterslep", "antallSaker", "hpInspText", "prosessInspText", "vaerId", "vaerNavn", "vindId", "vindNavn",
							"temperaturId", "temperaturNavn", "kmEvInspisert", "kmRvInspisert", "kmFvInspisert", "kmKvInspisert",
							"pdaDfuIdent", "pdaDfuNavn", "beskrivelse", "slettetAv", "slettetDato"};
	}
	private Long antallSaker;
	private String beskrivelse;
	private Boolean etterslep;
	private Date fraTidspunkt;
	private String guid;
	private String hpInspText;
	private Double kmEvInspisert;
	private Double kmFvInspisert;
	private Double kmKvInspisert;
	private Double kmRvInspisert;
	private String opprettetAv;
	private Date opprettetDato;
	private Long pdaDfuIdent;
	private String pdaDfuNavn;
	private String prosessInspText;
	private Long refnummer;
	private String slettetAv;
	private Date slettetDato;
	private Long temperaturId;
	private String temperaturNavn;
	private Date tilTidspunkt;
	private Long vaerId;
	private String vaerNavn;
	private Long vindId;
	
	private String vindNavn;

	public Long getAntallSaker() {
		return antallSaker;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}
	
	public Boolean getEtterslep() {
		return etterslep;
	}
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}
	public String getGuid() {
		return guid;
	}
	public String getHpInspText() {
		return hpInspText;
	}
	public Double getKmEvInspisert() {
		return kmEvInspisert;
	}
	public Double getKmFvInspisert() {
		return kmFvInspisert;
	}
	public Double getKmKvInspisert() {
		return kmKvInspisert;
	}
	public Double getKmRvInspisert() {
		return kmRvInspisert;
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}
	public String getPdaDfuNavn() {
		return pdaDfuNavn;
	}
	public String getProsessInspText() {
		return prosessInspText;
	}
	public Long getRefnummer() {
		return refnummer;
	}
	public String getSlettetAv() {
		return slettetAv;
	}
	public Date getSlettetDato() {
		return slettetDato;
	}
	public Long getTemperaturId() {
		return temperaturId;
	}
	public String getTemperaturNavn() {
		return temperaturNavn;
	}
	public String getTextForGUI() {
		return null;
	}
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}
	public Long getVaerId() {
		return vaerId;
	}
	public String getVaerNavn() {
		return vaerNavn;
	}
	public Long getVindId() {
		return vindId;
	}
	public String getVindNavn() {
		return vindNavn;
	}
	public void setAntallSaker(Long antallSaker) {
		this.antallSaker = antallSaker;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	public void setEtterslep(Boolean etterslep) {
		this.etterslep = etterslep;
	}
	public void setFraTidspunkt(Date fraTidspunkt) {
		this.fraTidspunkt = fraTidspunkt;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public void setHpInspText(String hpInspText) {
		this.hpInspText = hpInspText;
	}
	public void setKmEvInspisert(Double kmEvInspisert) {
		this.kmEvInspisert = kmEvInspisert;
	}
	public void setKmFvInspisert(Double kmFvInspisert) {
		this.kmFvInspisert = kmFvInspisert;
	}
	public void setKmKvInspisert(Double kmKvInspisert) {
		this.kmKvInspisert = kmKvInspisert;
	}
	public void setKmRvInspisert(Double kmRvInspisert) {
		this.kmRvInspisert = kmRvInspisert;
	}
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	public void setPdaDfuIdent(Long pdaDfuIdent) {
		this.pdaDfuIdent = pdaDfuIdent;
	}
	public void setPdaDfuNavn(String pdaDfuNavn) {
		this.pdaDfuNavn = pdaDfuNavn;
	}
	public void setProsessInspText(String prosessInspText) {
		this.prosessInspText = prosessInspText;
	}
	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}
	public void setSlettetAv(String slettetAv) {
		this.slettetAv = slettetAv;
	}
	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}
	public void setTemperaturId(Long temperaturId) {
		this.temperaturId = temperaturId;
	}
	public void setTemperaturNavn(String temperaturNavn) {
		this.temperaturNavn = temperaturNavn;
	}
	public void setTilTidspunkt(Date tilTidspunkt) {
		this.tilTidspunkt = tilTidspunkt;
	}
	public void setVaerId(Long vaerId) {
		this.vaerId = vaerId;
	}

	public void setVaerNavn(String vaerNavn) {
		this.vaerNavn = vaerNavn;
	}

	public void setVindId(Long vindId) {
		this.vindId = vindId;
	}

	public void setVindNavn(String vindNavn) {
		this.vindNavn = vindNavn;
	}
	
	
	
}
