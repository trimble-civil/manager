package no.mesta.mipss.mipssfelt;

import java.util.List;

import no.mesta.mipss.common.PropertyChangeSource;

@SuppressWarnings("serial")
public class AvvikQueryParams extends QueryParams implements PropertyChangeSource, Cloneable{
	public enum RapportType{
		OPPRETTET_I_PERIODEN,
		APNE_PR_TILDATO_EL_LUKKET_I_PERIODEN,
		STATUSENDRING_I_PERIODEN
	}
	private boolean etterslep;
	private boolean tilleggsarbeid;
	private boolean trafikksikkerhet;
	private boolean fristdatoOverskredet;
//	private boolean innenforPeriode;
//	private boolean statusendring;
	private boolean mangler;
	private boolean kommendeMangler;
	
	/** for mangler */
	private boolean aapentAvvik;
	private boolean nyeMangler;
	private boolean alleMangler;
	
	private List<String> avvikstatus;
	private List<Long> avvikstatusId;
	private List<Long> avvikaarsak;
	private List<String> avvikaarsakStr;
	
	private Long[] tilstandId;
	
	private RapportType rapportType;
	
	public void setRapportType(RapportType rapportType){
		this.rapportType = rapportType;
	}
	public RapportType getRapportType(){
		return rapportType;
	}
	public boolean isEtterslep() {
		return etterslep;
	}
	public boolean isTilleggsarbeid() {
		return tilleggsarbeid;
	}
	public boolean isTrafikksikkerhet() {
		return trafikksikkerhet;
	}
	public boolean isFristdatoOverskredet() {
		return fristdatoOverskredet;
	}
	public void setEtterslep(boolean etterslep) {
		boolean old = this.etterslep;
		this.etterslep = etterslep;
		getProps().firePropertyChange("etterslep", old, etterslep);
		
	}
	public void setTilleggsarbeid(boolean tilleggsarbeid) {
		boolean old = this.tilleggsarbeid;
		this.tilleggsarbeid = tilleggsarbeid;
		getProps().firePropertyChange("tilleggsarbeid", old, tilleggsarbeid);
	}
	public void setTrafikksikkerhet(boolean trafikksikkerhet) {
		boolean old = this.trafikksikkerhet;
		this.trafikksikkerhet = trafikksikkerhet;
		getProps().firePropertyChange("trafikksikkerhet", old, trafikksikkerhet);
	}
	public void setFristdatoOverskredet(boolean fristdatoOverskredet) {
		boolean old = this.fristdatoOverskredet;
		this.fristdatoOverskredet = fristdatoOverskredet;
		getProps().firePropertyChange("fristdatoOverskredet", old, fristdatoOverskredet);
	}
	public void setAvvikstatus(List<String> avvikstatus) {
		List<String> old = this.avvikstatus;
		this.avvikstatus = avvikstatus;
		getProps().firePropertyChange("avvikstatus", old, avvikstatus);
	}
	public List<String> getAvvikstatus() {
		return avvikstatus;
	}
	public void setTilstandId(Long[] tilstandId) {
		Long[] old = this.tilstandId;
		this.tilstandId = tilstandId;
		getProps().firePropertyChange("tilstandId", old, tilstandId);
	}
	public Long[] getTilstandId() {
		return tilstandId;
	}
	public void setAvvikaarsak(List<Long> avvikaarsak) {
		List<Long> old = this.avvikaarsak;
		this.avvikaarsak = avvikaarsak;
		getProps().firePropertyChange("avvikaarsak", old, avvikaarsak);
	}
	public List<Long> getAvvikaarsak() {
		return avvikaarsak;
	}
	
	public void setAvvikaarsakStr(List<String> avvikaarsakStr) {
		List<String> old = this.avvikaarsakStr;
		this.avvikaarsakStr = avvikaarsakStr;
		getProps().firePropertyChange("avvikaarsakStr", old, avvikaarsakStr);
	}
	public List<String> getAvvikaarsakStr() {
		return avvikaarsakStr;
	}
	public void setAvvikstatusId(List<Long> avvikstatusId) {
		this.avvikstatusId = avvikstatusId;
	}
	public List<Long> getAvvikstatusId() {
		return avvikstatusId;
	}
//	public void setInnenforPeriode(boolean innenforPeriode) {
//		boolean old = this.innenforPeriode;
//		this.innenforPeriode = innenforPeriode;
//		getProps().firePropertyChange("innenforPeriode", old, innenforPeriode);
//	}
//	public boolean isInnenforPeriode() {
//		return innenforPeriode;
//	}
//	
//	public void setStatusendring(boolean statusendring) {
//		boolean old = this.statusendring;
//		this.statusendring = statusendring;
//		getProps().firePropertyChange("statusendring", old, statusendring);
//	}
//	public boolean isStatusendring() {
//		return statusendring;
//	}
	
	public void setMangler(boolean mangler){
		boolean old = this.mangler;
		this.mangler = mangler;
		getProps().firePropertyChange("mangler", old, mangler);
	}
	public boolean isMangler(){
		return mangler;
	}
	@Override
	/**
	 * Gir en shallow clone av AvvikQueryParams
	 */
	public AvvikQueryParams clone(){
		AvvikQueryParams p = new AvvikQueryParams();
		p.setAvvikaarsak(getAvvikaarsak());
		p.setAvvikaarsakStr(getAvvikaarsakStr());
		p.setAvvikstatus(getAvvikstatus());
		p.setAvvikstatusId(getAvvikstatusId());
		p.setBrukerList(getBrukerList());
		p.setDateFra(getDateFra());
		p.setDateTil(getDateTil());
		p.setEtterslep(isEtterslep());
		p.setFristdatoOverskredet(isFristdatoOverskredet());
		p.setInkluderKmIVeiListe(isInkluderKmIVeiListe());
//		p.setInnenforPeriode(isInnenforPeriode());
//		p.setStatusendring(isStatusendring());
		p.setKunSlettede(isKunSlettede());
		p.setMangler(isMangler());
		p.setPdaDfuIdentList(getPdaDfuIdentList());
		p.setProsesser(getProsesser());
		p.setProsesskoder(getProsesskoder());
		p.setTilleggsarbeid(isTilleggsarbeid());
		p.setTilstandId(getTilstandId());
		p.setTrafikksikkerhet(isTrafikksikkerhet());
		p.setValgtKontrakt(getValgtKontrakt());
		p.setVeiListe(getVeiListe());
		return p;
	}
	public void setAapentAvvik(boolean aapentAvvik) {
		this.aapentAvvik = aapentAvvik;
	}
	public boolean isAapentAvvik() {
		return aapentAvvik;
	}
	public void setNyeMangler(boolean nyeMangler) {
		this.nyeMangler = nyeMangler;
	}
	public boolean isNyeMangler() {
		return nyeMangler;
	}
	public void setKommendeMangler(boolean kommendeMangler) {
		this.kommendeMangler = kommendeMangler;
		setDateEnabled(!kommendeMangler);
	}
	public boolean isKommendeMangler() {
		return kommendeMangler;
	}
	public void setAlleMangler(boolean alleMangler) {
		this.alleMangler = alleMangler;
		setDateEnabled(!alleMangler);
	}
	public boolean isAlleMangler() {
		return alleMangler;
	}
	
}
