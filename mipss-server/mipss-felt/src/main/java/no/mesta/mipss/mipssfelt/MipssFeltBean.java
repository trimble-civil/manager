package no.mesta.mipss.mipssfelt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Synkbilde;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.valueobjects.ElrappTypeR;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(name = MipssFelt.BEAN_NAME, mappedName="ejb/"+MipssFelt.BEAN_NAME)
@SuppressWarnings("unchecked")
public class MipssFeltBean implements MipssFelt{
    private static final Logger log = LoggerFactory.getLogger(MipssFeltBean.class);

	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	@EJB 
	private AvvikServiceLocal avvikService;
	
	public List<Bilde> getBilderFromSak(String sakGuid){
		StringBuilder sb = new StringBuilder();
		sb.append("select ab.bilde_guid from avvik_bilde_xv ab join avvik_xv a on ab.avvik_guid=a.guid where a.sak_guid = ?1 ");
		sb.append("union ");
		sb.append("select ab.bilde_guid from hendelse_bilde_xv ab join hendelse_xv a on ab.hendelse_guid=a.guid where a.sak_guid = ?1 ");
		sb.append("union ");
		sb.append("select ab.bilde_guid from skade_bilde_xv ab join forsikringsskade_xv a on ab.skade_guid=a.guid where a.sak_guid = ?1 ");
		sb.append("union ");
		sb.append("select ab.bilde_guid from skred_bilde_xv ab join skred_xv a on ab.skred_guid=a.guid where a.sak_guid = ?1 ");
		
		Query query = em.createNativeQuery(sb.toString());
		query.setParameter(1, sakGuid);
		List<Bilde> bilder = new ArrayList<Bilde>();
		try{
			List<String> resultList = query.getResultList();
			for (String guid :resultList){
				Bilde bilde= em.find(Bilde.class, guid);
				bilder.add(bilde);
			}
		} catch (NoResultException e){
			return null;
		}
		return bilder;
		
	}
	public List<String> getFeltBrukere(Long kontraktId, Long[] pdaDfuId) {

		StringBuilder sb = new StringBuilder();
 		sb.append("SELECT opprettet_av FROM ");
 		sb.append("(SELECT distinct(opprettet_av) FROM sak where kontrakt_id = ?1");
 		if (pdaDfuId!=null&&pdaDfuId.length!=0){
 			sb.append(" and pda_dfu_ident in ("+StringUtils.join(pdaDfuId, ", ")+")");
 		}
 		sb.append(") ");
 		sb.append("UNION ");
 		sb.append("(SELECT distinct(opprettet_av) FROM inspeksjon WHERE kontrakt_id = ?1");
 		if (pdaDfuId!=null&&pdaDfuId.length!=0){
 			sb.append(" and pda_dfu_ident in ("+StringUtils.join(pdaDfuId, ", ")+")");
 		}
 		sb.append(") ");

 		Query q = em.createNativeQuery(sb.toString());
 		q.setParameter(1, kontraktId);
		List<String> results = q.getResultList();
		return results;
	}
	
	public NokkeltallFelt hentMangelStatistikk(Long driftkontraktId){
		StringBuilder sb = new StringBuilder();
		sb.append("select KONTRAKT_ID, ANT_MANGLER_ALLE, ANT_MANGLER_LUKKET_ALLE, ANT_MANGLER_AAPNE from ");
		sb.append("table(statistikk_pk.felt_stat_f(?, sysdate, sysdate))");

        log.debug("Fetching missing statistics for contract {}", driftkontraktId);
		Query q = em.createNativeQuery(sb.toString(), NokkeltallFelt.FIELD_RESULT_MAPPING).setParameter(1, driftkontraktId);
		NokkeltallFelt nokkeltall;
		try {
			nokkeltall = (NokkeltallFelt) q.getSingleResult();
		} catch(NoResultException e) {
			log.error("Unable to fetch statistics for contract {}", driftkontraktId);
			nokkeltall = new NokkeltallFelt();
		}
		return nokkeltall;
	}
	
	public Hendelse hentHendelse(String guid){
		Hendelse hendelse = em.find(Hendelse.class, guid);
		BeanHelper.fetchAndRefresh(em, hendelse, "bilder", "tiltak");
		return hendelse;
	}
	public Hendelse hentHendelseRapport(String guid){
		Hendelse hendelse = em.find(Hendelse.class, guid);
		BeanHelper.fetchAndRefresh(em, hendelse, "bilder", "tiltak");
		hendelse.getSak().getVeiref();
		hendelse.getSak().getKontrakt().getByggeleder();
		return hendelse;
	}
	/** {@inheritDoc} */
	public Hendelse hentHendelse(Long refnummer){
		try{
		Hendelse hendelse = (Hendelse)em.createNamedQuery(Hendelse.QUERY_HENT_MED_REFNUMMER).setParameter("refnummer", refnummer).getSingleResult();
		BeanHelper.fetchAndRefresh(em, hendelse, "bilder", "tiltak");
		return hendelse;
		} catch (NoResultException e){
			return null;
		}
	}
	public List<Bilde> hentHendelseBilder(String guid) {
		Query bildeQuery = em.createNamedQuery(Hendelse.QUERY_GET_BILDER);
		bildeQuery.setParameter(1, guid);
		bildeQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return bildeQuery.getResultList();
	}
	public Forsikringsskade hentForsikringsskade(String guid){
		Forsikringsskade fs = em.find(Forsikringsskade.class, guid);
		BeanHelper.fetchAndRefresh(em, fs, "bilder", "skadeKontakter");
		return fs;
	}
	public Skred hentSkred(String guid){
		Skred skred = em.find(Skred.class, guid);
		BeanHelper.fetchAndRefresh(em, skred, "bilder", "skredEgenskapList");
		return skred;
	}
	
	
	public List<Bilde> hentForsikringsskadeBilder(String guid) {
		Query bildeQuery = em.createNamedQuery(Forsikringsskade.QUERY_GET_BILDER);
		bildeQuery.setParameter(1, guid);
		bildeQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return bildeQuery.getResultList();
	}
	
	public List<FeltEntityInfo> hentEntityInfoAvvik(String sakGuid) {
		Query q = em.createNamedQuery(Avvik.QUERY_FIND_ENTITY_INFO_BY_SAK).setParameter("sakGuid", sakGuid);
		return q.getResultList();
	}
	public List<FeltEntityInfo> hentEntityInfoHendelse(String sakGuid) {
		Query q = em.createNamedQuery(Hendelse.QUERY_FIND_ENTITY_INFO_BY_SAK).setParameter("sakGuid", sakGuid);
		return q.getResultList();
	}
	public List<FeltEntityInfo> hentEntityInfoForsikringsskade(String sakGuid){
		Query q = em.createNamedQuery(Forsikringsskade.QUERY_FIND_ENTITY_INFO_BY_SAK).setParameter("sakGuid", sakGuid);
		return q.getResultList();
	}
	public List<FeltEntityInfo> hentEntityInfoSkred(String sakGuid){
		Query q = em.createNamedQuery(Skred.QUERY_FIND_ENTITY_INFO_BY_SAK).setParameter("sakGuid", sakGuid);
		return q.getResultList();
	}
	
	public Long getDriftkontraktIdForRskjema(Long refnummer, ElrappTypeR.Type typeR){
		String elrappEntity = "";
		switch (typeR){
			case R2: elrappEntity = "Hendelse"; break;
			case R5: elrappEntity = "Forsikringsskade"; break;
			case R11: elrappEntity = "Skred"; break;
		}
		Long dkId = null;
		try{
			dkId = (Long)em.createQuery("SELECT s.sak.kontraktId from "+elrappEntity+" s where s.refnummer=:refnummer")
							.setParameter("refnummer", refnummer)
							.getSingleResult();
		} catch (NoResultException e){
		}
		return dkId;
		
	}
	
	public Long getDriftkontraktIdForInspeksjon(Long refnummer) {
		Long dkId = null;
		try{
			dkId = (Long)em.createQuery("SELECT h.kontraktId from Inspeksjon h where h.refnummer=:refnummer")
							.setParameter("refnummer", refnummer)
							.getSingleResult();
		} catch (NoResultException e){
		}
		return dkId;
	}
	public Long getDriftkontraktIdForAvvik(Long refnummer) {
		Long dkId = null;
		try{
			dkId = (Long)em.createQuery("SELECT h.sak.kontraktId from Avvik h where h.refnummer=:refnummer")
							.setParameter("refnummer", refnummer)
							.getSingleResult();
		} catch (NoResultException e){
		}
		return dkId;
	}
	
	public Long getDriftkontraktIdForSak(Long refnummer) {
		Long dkId = null;
		try{
			dkId = (Long)em.createQuery("SELECT h.kontraktId from Sak h where h.refnummer=:refnummer")
							.setParameter("refnummer", refnummer)
							.getSingleResult();
		} catch (NoResultException e){
		}
		return dkId;
	}
	
	
	public Sak persistSak(Sak sak){
		Sak merged = em.merge(sak);
		return merged;
	}
	/** {@inheritDoc} */
	public <E extends MipssEntityBean<E>> List<E> hentObjekter(Class<E> clazz, List<String> guids) {

		if (guids == null || guids.size() == 0) {
			return new ArrayList<E>();
		}

		int size = guids.size();
		int reminder = size % 1000;
		int pages = (size - reminder) / 1000;
		List<E> result = new ArrayList<E>();
		Object[] ordererdResults = new Object[guids.size()];
		
		for (int page = 0; page <= pages; page++) {
			int fromIndex = page * 1000;
			int pageLength = 1000;
			int toIndex = fromIndex + pageLength;
			if (toIndex > guids.size()) {
				toIndex = guids.size();
			}
			List<String> ids = guids.subList(fromIndex, toIndex);
			SelectQueryBuilder select = new SelectQueryBuilder();
			select.addSelect("o");
			select.addFrom(clazz.getSimpleName() + " o");
			select.addWhere("o.guid " + createSqlIn(ids), AND_OPERATOR);

			String selectString = select.toString();
			Query query = em.createQuery(selectString);

			List<E> entities = query.getResultList();
			result.addAll(entities);
		}
		if (clazz.equals(Forsikringsskade.class)){
			for (Object f:result){
				MipssEntityBean<Forsikringsskade> me=(MipssEntityBean<Forsikringsskade>)f;
				Forsikringsskade fs = (Forsikringsskade)me;
				//eager fetching..
				fs.getSak().getKontrakt().getByggeleder();
				fs.getSak().getVeiref();
				fs.getSkadeKontakter().size();
				fs.getBilder().size();
				int indexOf = guids.indexOf(fs.getGuid());
				ordererdResults[indexOf] = fs;
//				orderedList.add(indexOf, (E)fs);
			}
		}
		if (clazz.equals(Skred.class)){
			for (Object f:result){
				MipssEntityBean<Skred> me=(MipssEntityBean<Skred>)f;
				Skred fs = (Skred)me;
				//eager fetching..
				fs.getSak().getStedfesting().size();
				fs.getSak().getVeiref();
				fs.getBilder().size();
				fs.getSkredEgenskapList().size();
				int indexOf = guids.indexOf(fs.getGuid());
				ordererdResults[indexOf] = fs;
			}
		}
		if (clazz.equals(Avvik.class)){
			for (Object a:result){
				MipssEntityBean<Avvik> me = (MipssEntityBean<Avvik>)a;
				Avvik av = (Avvik)me;
				av.setMangel(avvikService.mangelSjekk(av.getSak().getKontrakt(), av));
				av.getStatuser().size();
				av.getBilder().size();
				av.getVeiref();
				int indexOf = guids.indexOf(av.getGuid());
				ordererdResults[indexOf] = av;
			}
		}
		List<?> list = (List<?>)Arrays.asList(ordererdResults);
		return (List<E>)list;
	}
	
	/** {@inheritDoc} */
	public void persistSynkbilde(List<String> bildeGuids, String brukerSign){
		for (String guid:bildeGuids){
			Bilde b = em.find(Bilde.class, guid);
			Synkbilde s = em.find(Synkbilde.class, guid);
			if (s==null){
				s = new Synkbilde();
				s.setGuid(b.getGuid());
				s.setOpprettetAv(brukerSign);
				s.setOpprettetDato(Clock.now());
			}else{
				s.setEndretAv(brukerSign);
				s.setEndretDato(Clock.now());
			}
			s.setSmaabildeLob(b.getSmaabildeLob());
			s.setBredde(b.getBredde());
			s.setHoyde(b.getHoyde());
			s.setBeskrivelse(b.getBeskrivelse());
			s.setFilnavn(b.getFilnavn());
			em.merge(s);
		}
	}
	
	private String createSqlIn(List<?> guider) {
		String sql = " in (" + createGroup(guider, false) + RIGHT_P;
		return sql;
	}
	private static final String RIGHT_P = ")";
	private static final String AND_OPERATOR = " AND ";

	private String createGroup(List<?> ider, boolean ignoreString) {
		String sql = "";
		int i = 0;
		for (Object id : ider) {
			if (id instanceof String && !ignoreString) {
				sql += "'" + id + "'";
			} else {
				sql += id;
			}
			if (i < ider.size() - 1) {
				sql += ",";
			}
			i++;
		}
		return sql;
	}
	
}
