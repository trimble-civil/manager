package no.mesta.mipss.mipssfelt;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.valueobjects.ElrappStatus;

@SuppressWarnings("serial")
public class SakSokResult implements Serializable, IRenderableMipssEntity{
	
	public static String[] getPropertiesArray() {
		return new String[] { "guid", "refnummer", "opprettetDato", "opprettetAv", "sisteStatusDato",
				"sisteStatus", "tiltaksDato", "antallAvvik", "r2RapportertDato", "r2EndretDato", 
				"r5RapportertDato", "r5EndretDato", "r11RapportertDato", "r11EndretDato", "r2", "r5", "r11", "tilleggsarbeid", "etterslep",
				"fylkesnummer", "kommunenummer", "veikategori",
				"veistatus", "veinummer", "hp", "meter", "skadested",
				"prosesserText", "pdaDfuIdent", "slettetDato", "slettetAv", "internnotat" };
	}
	private Long antallAvvik;
	
	private Boolean etterslep;
	
	private Long fylkesnummer;
	private String guid;
	private Long hp;
	private Long kommunenummer;
	private Long meter;
	private Date opprettetDato;
	private String opprettetAv;
	
	private String prosesserText;
	
	private Boolean r11;
	private Date r11EndretDato;
	private Date r11RapportertDato;
	
	private Boolean r2;
	private Date r2EndretDato;
	private Date r2RapportertDato;
	
	private Boolean r5;
	private Date r5EndretDato;
	private Date r5RapportertDato;
	
	private Long refnummer;
	
	private String sisteStatus;
	private Date sisteStatusDato;
	private Date slettetDato;
	private String slettetAv;

	private String skadested;
	private Boolean tilleggsarbeid;
	private Date tiltaksDato;

	private String veikategori;
	private Long veinummer;
	private String veistatus;
	
	private Long pdaDfuIdent;
	
	private String internnotat;
	
	public Long getAntallAvvik() {
		return antallAvvik;
	}
	
	
	public Boolean getEtterslep() {
		return etterslep;
	}
	public Long getFylkesnummer() {
		return fylkesnummer;
	}

	public String getGuid() {
		return guid;
	}

	public Long getHp() {
		return hp;
	}

	public Long getKommunenummer() {
		return kommunenummer;
	}

	public Long getMeter() {
		return meter;
	}

	public Date getOpprettetDato() {
		return opprettetDato;
	}

	public String getProsesserText() {
		return prosesserText;
	}

	public Date getR11EndretDato() {
		return r11EndretDato;
	}

	public Date getR11RapportertDato() {
		return r11RapportertDato;
	}

	public ElrappStatus getR11Status() {
		if (!getR11()){
			return null;
		}
		return ElrappStatus.getElrappStatus(getR11RapportertDato(), getR11EndretDato());
	}

	public Date getR2EndretDato() {
		return r2EndretDato;
	}

	public Date getR2RapportertDato() {
		return r2RapportertDato;
	}

	public ElrappStatus getR2Status() {
		if (!getR2()){
//			System.out.println("not r2");
			return null;
		}
//		System.out.println("R2");
		return ElrappStatus.getElrappStatus(getR2RapportertDato(), getR2EndretDato());
	}
	public Date getR5EndretDato() {
		return r5EndretDato;
	}
	public Date getR5RapportertDato() {
		return r5RapportertDato;
	}
	
	public ElrappStatus getR5Status() {
		if (!getR5()){
			return null;
		}
		return ElrappStatus.getElrappStatus(getR5RapportertDato(), getR5EndretDato());
	}

	public Long getRefnummer() {
		return refnummer;
	}

	public String getSisteStatus() {
		return sisteStatus;
	}

	public Date getSisteStatusDato() {
		return sisteStatusDato;
	}

	public String getSkadested() {
		return skadested;
	}

	public String getTextForGUI() {
		return null;
	}

	public Boolean getTilleggsarbeid() {
		return tilleggsarbeid;
	}

	public Date getTiltaksDato() {
		return tiltaksDato;
	}

	public String getVeikategori() {
		return veikategori;
	}

	public Long getVeinummer() {
		return veinummer;
	}
	
	public String getVeistatus() {
		return veistatus;
	}

	public String getVeitype(){
		return getVeikategori()!=null?getVeikategori()+getVeistatus():null;
	}

	public Boolean getR11() {
		return r11;
	}

	public Boolean getR2() {
		return r2;
	}

	public Boolean getR5() {
		return r5;
	}
	public Boolean isSlettbar(){
		return (getSlettetDato()==null&&!getR11()&&!getR2()&&!getR5()&&getAntallAvvik()==0);
	}

	public void setAntallAvvik(Long antallAvvik) {
		this.antallAvvik = antallAvvik;
	}

	public void setEtterslep(Boolean etterslep) {
		this.etterslep = etterslep;
	}
	
	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public void setHp(Long hp) {
		this.hp = hp;
	}

	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}

	public void setMeter(Long meter) {
		this.meter = meter;
	}

	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}

	public void setProsesserText(String prosesserText) {
		this.prosesserText = prosesserText;
	}

	public void setR11(Boolean r11) {
		this.r11 = r11;
	}

	public void setR11EndretDato(Date r11EndretDato) {
		this.r11EndretDato = r11EndretDato;
	}

	public void setR11RapportertDato(Date r11RapportertDato) {
		this.r11RapportertDato = r11RapportertDato;
	}

	public void setR2(Boolean r2) {
		this.r2 = r2;
	}

	public void setR2EndretDato(Date r2EndretDato) {
		this.r2EndretDato = r2EndretDato;
	}

	public void setR2RapportertDato(Date r2RapportertDato) {
		this.r2RapportertDato = r2RapportertDato;
	}

	public void setR5(Boolean r5) {
		this.r5 = r5;
	}

	public void setR5EndretDato(Date r5EndretDato) {
		this.r5EndretDato = r5EndretDato;
	}

	public void setR5RapportertDato(Date r5RapportertDato) {
		this.r5RapportertDato = r5RapportertDato;
	}

	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}

	public void setSisteStatus(String sisteStatus) {
		this.sisteStatus = sisteStatus;
	}

	public void setSisteStatusDato(Date sisteStatusDato) {
		this.sisteStatusDato = sisteStatusDato;
	}

	public void setSkadested(String skadested) {
		this.skadested = skadested;
	}

	public void setTilleggsarbeid(Boolean tilleggsarbeid) {
		this.tilleggsarbeid = tilleggsarbeid;
	}

	public void setTiltaksDato(Date tiltaksDato) {
		this.tiltaksDato = tiltaksDato;
	}

	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}

	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}

	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public void setPdaDfuIdent(Long pdaDfuIdent) {
		this.pdaDfuIdent = pdaDfuIdent;
	}
	public Long getPdaDfuIdent() {
		return pdaDfuIdent;
	}
	public void setSlettetAv(String slettetAv) {
		this.slettetAv = slettetAv;
	}
	public String getSlettetAv() {
		return slettetAv;
	}
	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}
	public Date getSlettetDato() {
		return slettetDato;
	}


	public void setInternnotat(String internnotat) {
		this.internnotat = internnotat;
	}


	public String getInternnotat() {
		return internnotat;
	}

}
