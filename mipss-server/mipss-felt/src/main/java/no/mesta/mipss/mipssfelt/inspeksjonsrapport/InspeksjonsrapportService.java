package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Remote;
import javax.naming.NamingException;

@Remote
public interface InspeksjonsrapportService {

	public static final String BEAN_NAME = "InspeksjonsrapportBean";

	List<InspeksjonsRapport> hentRapportdata(InspeksjonsrapportParams params) throws SQLException;
}
