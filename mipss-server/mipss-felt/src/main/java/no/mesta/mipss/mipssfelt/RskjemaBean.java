package no.mesta.mipss.mipssfelt;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.StructHelper;
import no.mesta.mipss.query.WhereAndBuilder;

import org.apache.commons.lang.StringUtils;

@Stateless(name = RskjemaService.BEAN_NAME, mappedName="ejb/"+RskjemaService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class RskjemaBean implements RskjemaService {
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;

	public List<RskjemaSokResult> sokRskjema(RskjemaQueryParams params)throws Exception {
		StructHelper structHelper = new StructHelper();

        Array veiArray = structHelper.createVeireferanseArray(params.getVeiListe(), params.isInkluderKmIVeiListe());
        List<String> brukerList = params.getBrukerList()==null?null:Arrays.asList(params.getBrukerList());
        Array brukerArray = structHelper.createBrukerArray(brukerList);
        List<Long> pdaList = params.getPdaDfuIdentList()==null?null:Arrays.asList(params.getPdaDfuIdentList());
        Array pdaArray = structHelper.createPdaArray(pdaList);
        StringBuilder query = new StringBuilder();

		structHelper.goodnight();
        
        query.append("select guid, refnummer, type_r_skjema, opprettet_dato, fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, ");
        query.append("hp, meter, skadested, beskrivelse, elrapp_id, elrapp_versjon, rapportert_dato, skjema_emne, endret_dato, slettet_dato, ");
        query.append("sak_refnummer, sak_guid from ");
        query.append("table(feltstudio_pk.rskjema_sok(#kontraktId, #fradato, #tildato, #veiliste, #pdaliste, #brukerliste)) ");
        
        WhereAndBuilder builder = new WhereAndBuilder();
        if (params.isKunSlettede()){
        	builder.addWhereAnd("slettet_dato is not null");
        }else{
        	builder.addWhereAnd("slettet_dato is null");
        }
        if (params.getTypeR()!=null){
        	builder.addWhereAnd("type_r_skjema = #typeR");
        }
        if (params.getElrappStatus()!=null){
        	switch (params.getElrappStatus()){
        	case ENDRET: builder.addWhereAnd("endret_dato > rapportert_dato");break;
        	case IKKE_SENDT: builder.addWhereAnd("rapportert_dato is null");break;
        	case SENDT: builder.addWhereAnd("rapportert_dato is not null and (rapportert_dato >= endret_dato or endret_dato is null)");break;
        	}
        }
        query.append(builder.toString());
        query.append(" order by opprettet_dato desc  ");
        Query q = em.createNativeQuery(query.toString())
		.setParameter("kontraktId", params.getValgtKontrakt().getId())
		.setParameter("veiliste", veiArray)
		.setParameter("pdaliste", pdaArray)
		.setParameter("brukerliste", brukerArray)
		.setParameter("fradato", params.getDateFra())
		.setParameter("tildato", params.getDateTil());
        if (params.getTypeR()!=null){
	        switch (params.getTypeR()){
	    	case R2: q.setParameter("typeR", "R2");break;
	    	case R5: q.setParameter("typeR", "R5");break;
	    	case R11:q.setParameter("typeR", "R11"); break;
	    	}
        }
        
        NativeQueryWrapper<RskjemaSokResult> wrapper = new NativeQueryWrapper<RskjemaSokResult>(q, RskjemaSokResult.class, RskjemaSokResult.getPropertiesArray());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");
        
		List<RskjemaSokResult> result = wrapper.getWrappedList();
		return result;
	}

	public List<RskjemaSokResult> sokHendelse(Date datoFra, Date datoTil, Driftkontrakt kontrakt, Long[] aarsakId){
		StringBuilder q = new StringBuilder();
		
		q.append("select h.guid, ps.snappet_x, ps.snappet_y, ps.x, ps.y "); 
		q.append("from hendelse_xv h ");
		q.append("join sak_xv s on s.guid = h.sak_guid ");
		q.append("join punktstedfest_xv ps on ps.sak_guid = s.guid "); 
		q.append("where h.opprettet_dato between ?1 and ?2 ");
		q.append("and s.kontrakt_id = ?3 ");
		if (aarsakId!=null&&aarsakId.length!=0){
			String aids = StringUtils.join(aarsakId, ',');
			q.append("and aarsak_id in ("+aids+") ");
		}
		Query qry = em.createNativeQuery(q.toString())
		.setParameter(1, datoFra)
		.setParameter(2, datoTil)
		.setParameter(3, kontrakt.getId());
        NativeQueryWrapper<RskjemaSokResult> wrapper = new NativeQueryWrapper<RskjemaSokResult>(qry, RskjemaSokResult.class, "guid", "snappetX", "snappetY", "x", "y");
        return wrapper.getWrappedList();
	}
	public List<RskjemaSokResult> sokForsikringsskade(Date datoFra, Date datoTil, Driftkontrakt kontrakt){
		StringBuilder q = new StringBuilder();
		
		q.append("select f.guid, ps.snappet_x, ps.snappet_y, ps.x, ps.y "); 
		q.append("from forsikringsskade_xv f ");
		q.append("join sak_xv s on s.guid = f.sak_guid ");
		q.append("join punktstedfest_xv ps on ps.sak_guid = s.guid "); 
		q.append("where f.opprettet_dato between ?1 and ?2 ");
		q.append("and s.kontrakt_id = ?3 ");
		Query qry = em.createNativeQuery(q.toString())
		.setParameter(1, datoFra)
		.setParameter(2, datoTil)
		.setParameter(3, kontrakt.getId());
        NativeQueryWrapper<RskjemaSokResult> wrapper = new NativeQueryWrapper<RskjemaSokResult>(qry, RskjemaSokResult.class, "guid", "snappetX", "snappetY", "x", "y");
        return wrapper.getWrappedList();
	}
	public List<RskjemaSokResult> sokSkred(Date datoFra, Date datoTil, Driftkontrakt kontrakt){
		StringBuilder q = new StringBuilder();
		q.append("select sk.guid, ps.snappet_x, ps.snappet_y, ps.x, ps.y "); 
		q.append("from skred_xv sk ");
		q.append("join sak_xv s on s.guid = sk.sak_guid ");
		q.append("join punktstedfest_xv ps on ps.sak_guid = s.guid "); 
		q.append("where sk.opprettet_dato between ?1 and ?2 ");
		q.append("and s.kontrakt_id = ?3 ");
		Query qry = em.createNativeQuery(q.toString())
		.setParameter(1, datoFra)
		.setParameter(2, datoTil)
		.setParameter(3, kontrakt.getId());
        NativeQueryWrapper<RskjemaSokResult> wrapper = new NativeQueryWrapper<RskjemaSokResult>(qry, RskjemaSokResult.class, "guid", "snappetX", "snappetY", "x", "y");
        return wrapper.getWrappedList();
	}
	public Hendelse hentHendelse(String guid) {
		Hendelse hendelse = em.find(Hendelse.class, guid);
		BeanHelper.fetchAndRefresh(em, hendelse, "bilder", "tiltak");
		BeanHelper.fetchAndRefresh(em, hendelse.getSak(), "stedfesting");
		hendelse.setNewEntity(false);
		return hendelse;
	}
	
	public Hendelse hentHendelse(Long refnummer){
		try{
			Hendelse hendelse = (Hendelse)em.createNamedQuery(Hendelse.QUERY_HENT_MED_REFNUMMER).setParameter("refnummer", refnummer).getSingleResult();
			BeanHelper.fetchAndRefresh(em, hendelse, "bilder", "tiltak");
			BeanHelper.fetchAndRefresh(em, hendelse.getSak(), "stedfesting");
			hendelse.setNewEntity(false);
		return hendelse;
		} catch (NoResultException e){
			return null;
		}
	}
	public Hendelse hentHendelseElrapp(Long elrappId, Long driftkontraktId){
		try{
			Hendelse hendelse = (Hendelse)em.createNamedQuery(Hendelse.QUERY_HENT_MED_ELRAPPID)
				.setParameter("elrappId", elrappId)
				.setParameter("driftkontraktId", driftkontraktId).getSingleResult();
			BeanHelper.fetchAndRefresh(em, hendelse, "bilder", "tiltak");
			BeanHelper.fetchAndRefresh(em, hendelse.getSak(), "stedfesting");
			hendelse.setNewEntity(false);
		return hendelse;
		} catch (NoResultException e){
			return null;
		}
	}
	public Hendelse persistHendelse(Hendelse hendelse, String brukerSign){
		Query q = em.createNamedQuery(Hendelse.QUERY_GET_BILDER_UTENRELASJONER).setParameter(1, hendelse.getGuid());
		List<Bilde> oldBilder = q.getResultList();
		BeanHelper.deleteOldEntities(em, oldBilder, hendelse.getBilder());
		
		if (hendelse.getOwnedMipssEntity()==null||hendelse.getOwnedMipssEntity().getOpprettetAv()!=null){
			hendelse.getOwnedMipssEntity().setEndretAv(brukerSign);
			hendelse.getOwnedMipssEntity().setEndretDato(Clock.now());
		}else{
			hendelse.getOwnedMipssEntity().setOpprettetAv(brukerSign);
			hendelse.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		}
		
		hendelse = em.merge(hendelse);
		return hendelse;
	}
	public Forsikringsskade hentForsikringsskade(String guid){
		Forsikringsskade fs = em.find(Forsikringsskade.class, guid);
		if (fs==null)
			return null;
		BeanHelper.fetchAndRefresh(em, fs, "skadeKontakter", "bilder");
		BeanHelper.fetchAndRefresh(em, fs.getSak(), "stedfesting");
		fs.getSak().getKontrakt().getByggeleder();
		fs.setNewEntity(false);
		return fs;
	}
	public Forsikringsskade hentForsikringsskade(Long refnummer){
		try{
			Forsikringsskade fs = (Forsikringsskade)em.createNamedQuery(Forsikringsskade.QUERY_HENT_MED_REFNUMMER).setParameter("refnummer", refnummer).getSingleResult();
			BeanHelper.fetchAndRefresh(em, fs, "skadeKontakter", "bilder");
			BeanHelper.fetchAndRefresh(em, fs.getSak(), "stedfesting");
			fs.getSak().getKontrakt().getByggeleder();
			fs.setNewEntity(false);
		return fs;
		} catch (NoResultException e){
			return null;
		}
	}
	public Forsikringsskade hentForsikringsskadeElrapp(Long elrappId, Long driftkontraktId){
		try{
			Forsikringsskade fs = (Forsikringsskade)em.createNamedQuery(Forsikringsskade.QUERY_HENT_MED_ELRAPP_ID)
				.setParameter("elrappId", elrappId)
				.setParameter("driftkontraktId", driftkontraktId).getSingleResult();
			BeanHelper.fetchAndRefresh(em, fs, "skadeKontakter", "bilder");
			BeanHelper.fetchAndRefresh(em, fs.getSak(), "stedfesting");
			fs.getSak().getKontrakt().getByggeleder();
			fs.setNewEntity(false);
		return fs;
		} catch (NoResultException e){
			return null;
		}
	}
	public Forsikringsskade persistForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign){
		/** TODO gjøres client side, kan evt fjernes.*/
		List<SkadeKontakt> sk = forsikringsskade.getSkadeKontakter();
		List<SkadeKontakt> toRemove = new ArrayList<SkadeKontakt>();
		if (sk!=null){
			for (SkadeKontakt s:sk){
				if (isEmptySkadeKontakt(s)){
					toRemove.add(s);
				}
			}
		}
		sk.removeAll(toRemove);
		if (forsikringsskade.getOpprettetAv()!=null){
			forsikringsskade.setEndretAv(brukerSign);
			forsikringsskade.setEndretDato(Clock.now());
		}else{
			forsikringsskade.setOpprettetAv(brukerSign);
			forsikringsskade.setOpprettetDato(Clock.now());
		}
		forsikringsskade = em.merge(forsikringsskade);
		return forsikringsskade;
	}
	//TODO REMOVE
	private boolean isEmptySkadeKontakt(SkadeKontakt sk){
		return StringUtils.isBlank(sk.getAdresse())
			&& StringUtils.isBlank(sk.getEpostAdresse())
			&& StringUtils.isBlank(sk.getNavn())
			&& StringUtils.isBlank(sk.getPoststed())
			&& StringUtils.isBlank(sk.getKontaktNavn())
			&& StringUtils.isBlank(sk.getResultat())
			&& sk.getDato()==null
			&& sk.getPostnummer()==null
			&& sk.getTlfnummer()==null
			&& sk.getKontaktTlfnummer()==null
			&& (sk.getPostnummer()==null||sk.getPostnummer().intValue()==0);
	}
	public Skred hentSkred(String guid){
		Skred s = em.find(Skred.class, guid);
		BeanHelper.fetchAndRefresh(em, s, "bilder", "skredEgenskapList");
		BeanHelper.fetchAndRefresh(em, s.getSak(), "stedfesting");
		s.setNewEntity(false);
		return s;
	}
	public Skred hentSkred(Long refnummer){
		try{
			Skred s = (Skred)em.createNamedQuery(Skred.QUERY_HENT_MED_REFNUMMER).setParameter("refnummer", refnummer).getSingleResult();
			BeanHelper.fetchAndRefresh(em, s, "bilder", "skredEgenskapList");
			BeanHelper.fetchAndRefresh(em, s.getSak(), "stedfesting");
			s.setNewEntity(false);
		return s;
		} catch (NoResultException e){
			return null;
		}
	}
	public Skred hentSkredElrapp(Long elrappId, Long driftkontraktId){
		try{
			Skred s = (Skred)em.createNamedQuery(Skred.QUERY_HENT_MED_ELRAPP_ID)
				.setParameter("elrappId", elrappId)
				.setParameter("driftkontraktId", driftkontraktId).getSingleResult();
			BeanHelper.fetchAndRefresh(em, s, "bilder", "skredEgenskapList");
			BeanHelper.fetchAndRefresh(em, s.getSak(), "stedfesting");
			s.setNewEntity(false);
		return s;
		} catch (NoResultException e){
			return null;
		}
	}
	public Skred persistSkred(Skred skred, String brukerSign){
		Query q = em.createNamedQuery(Skred.QUERY_GET_BILDER_UTENRELASJONER).setParameter(1, skred.getGuid());
		List<Bilde> oldBilder = q.getResultList();
		BeanHelper.deleteOldEntities(em, oldBilder, skred.getBilder());
		
		if (skred.getOpprettetAv()!=null){
			skred.setEndretAv(brukerSign);
			skred.setEndretDato(Clock.now());
		}else{
			skred.setOpprettetAv(brukerSign);
			skred.setOpprettetDato(Clock.now());
		}
		for (Punktstedfest ps:skred.getSak().getStedfesting()){
			em.merge(ps);
		}
		skred = em.merge(skred);
		
		
		return skred;
	}
	
	/** {@inheritDoc} */
	public List<Egenskap> getAlleEgenskaper() {
		return em.createNamedQuery(Egenskap.QUERY_FIND_BY_TYPE)
			.setParameter("egenskaptypeId", Long.valueOf(1))
			.getResultList();
	}
	
	/** {@inheritDoc} */
	public List<Egenskapverdi> getEgenskapverdier(Egenskap egenskap) {
		return em.createNamedQuery(Egenskapverdi.QUERY_FIND_BY_EGENSKAP)
			.setParameter("id", egenskap.getId())
			.setHint(QueryHints.REFRESH, HintValues.TRUE)
			.getResultList();
	}
	/** {@inheritDoc} */
	public List<Hendelseaarsak> hentAlleHendelseaarsaker(){
		return em.createNamedQuery(Hendelseaarsak.QUERY_FIND_VALID).getResultList();
	}
	/** {@inheritDoc} */
	public List<Trafikktiltak> hentAlleTrafikktiltak(){
		return em.createNamedQuery(Trafikktiltak.QUERY_FIND_ALL).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektType> hentAlleObjektTyper(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektType o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektAvvikType> hentAlleObjektAvvikTyper(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektAvvikType o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektAvvikKategori> hentAlleObjektAvvikKategorier(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektAvvikKategori o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektStedTypeAngivelse> hentAlleObjektStedTypeAngivelser(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektStedTypeAngivelse o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ArbeidType> hentAlleArbeidTyper(){
		return em.createNamedQuery(ArbeidType.QUERY_FIND_ALL).getResultList();
	}

	public Hendelse deleteHendelse(Hendelse hendelse, String brukerSign) {
		hendelse.setSlettetAv(brukerSign);
		hendelse.setSlettetDato(Clock.now());
		hendelse = em.merge(hendelse);
		return hendelse;
	}
	
	public Hendelse restoreHendelse(Hendelse hendelse, String brukerSign) {
		hendelse.setSlettetAv(null);
		hendelse.setSlettetDato(null);
		hendelse.getOwnedMipssEntity().setEndretAv(brukerSign);
		hendelse.getOwnedMipssEntity().setEndretDato(Clock.now());
		hendelse = em.merge(hendelse);
		return hendelse;
	}


	public Forsikringsskade deleteForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign) {
		List<SkadeKontakt> toRemove = new ArrayList<SkadeKontakt>();
		for (SkadeKontakt sk:forsikringsskade.getSkadeKontakter()){
			if (isKontaktEmpty(sk))
				toRemove.add(sk);
		}
		forsikringsskade.getSkadeKontakter().removeAll(toRemove);
		
		forsikringsskade.setSlettetAv(brukerSign);
		forsikringsskade.setSlettetDato(Clock.now());
		forsikringsskade = em.merge(forsikringsskade);
		
		return forsikringsskade;
	}
	
	private boolean isKontaktEmpty(SkadeKontakt sk){
		return StringUtils.isBlank(sk.getGuid());
	}
	
	public Forsikringsskade restoreForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign) {
		List<SkadeKontakt> toRemove = new ArrayList<SkadeKontakt>();
		for (SkadeKontakt sk:forsikringsskade.getSkadeKontakter()){
			if (isKontaktEmpty(sk))
				toRemove.add(sk);
		}
		forsikringsskade.getSkadeKontakter().removeAll(toRemove);
		
		forsikringsskade.setSlettetAv(null);
		forsikringsskade.setSlettetDato(null);
		forsikringsskade.setEndretAv(brukerSign);
		forsikringsskade.setEndretDato(Clock.now());
		forsikringsskade = em.merge(forsikringsskade);
		return forsikringsskade;
	}

	public Skred deleteSkred(Skred skred, String brukerSign) {
		skred.setSlettetAv(brukerSign);
		skred.setSlettetDato(Clock.now());
		skred = em.merge(skred);
		return skred;	
	}
	
	public Skred restoreSkred(Skred skred, String brukerSign) {
		skred.setSlettetAv(null);
		skred.setSlettetDato(null);
		skred.setEndretAv(brukerSign);
		skred.setEndretDato(Clock.now());
		skred = em.merge(skred);
		return skred;	
	}
	
	public void deleteRskjema(List<RskjemaSokResult> selectedEntities,	String brukerSign){
		for (RskjemaSokResult rs:selectedEntities){
			if (rs.getTypeR().equals("R2")){
				Hendelse h = em.find(Hendelse.class, rs.getGuid());
				h.setSlettetDato(Clock.now());
				h.setSlettetAv(brukerSign);
				em.merge(h);
			}
			if (rs.getTypeR().equals("R5")){
				Forsikringsskade f = em.find(Forsikringsskade.class, rs.getGuid());
				f.setSlettetDato(Clock.now());
				f.setSlettetAv(brukerSign);
				em.merge(f);
							}
			if (rs.getTypeR().equals("R11")){
				Skred s = em.find(Skred.class, rs.getGuid());
				s.setSlettetDato(Clock.now());
				s.setSlettetAv(brukerSign);
				em.merge(s);
			}
		}
	}
	
}
