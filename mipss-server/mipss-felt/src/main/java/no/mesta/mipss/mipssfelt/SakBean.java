package no.mesta.mipss.mipssfelt;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.mipssfelt.SakQueryParams.TYPE;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.StructHelper;
import no.mesta.mipss.query.WhereAndBuilder;

@Stateless(name = SakService.BEAN_NAME, mappedName="ejb/"+SakService.BEAN_NAME)
public class SakBean implements SakService{
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	@EJB
	private AvvikServiceLocal avvikService;

	public List<SakSokResult> sokSaker(SakQueryParams params) throws Exception {
		StructHelper structHelper = new StructHelper();

        Array veiArray = structHelper.createVeireferanseArray(params.getVeiListe(), params.isInkluderKmIVeiListe());
        List<String> brukerList = params.getBrukerList()==null?null:Arrays.asList(params.getBrukerList());
        Array brukerArray = structHelper.createBrukerArray(brukerList);
        List<Long> pdaList = params.getPdaDfuIdentList()==null?null:Arrays.asList(params.getPdaDfuIdentList());
        Array pdaArray = structHelper.createPdaArray(pdaList);
        StringBuilder query = new StringBuilder();

		structHelper.goodnight();
        
        query.append("select guid, refnummer, opprettet_dato, opprettet_av, siste_status_dato, siste_status, tiltaks_dato, ");
        query.append("antall_avvik, r2_rapportert_dato, r2_endret_dato, r5_rapportert_dato, r5_endret_dato, r11_rapportert_dato, r11_endret_dato, ");
        query.append("r2_flagg, r5_flagg, r11_flagg, tilleggsarbeid_flagg, etterslep_flagg, ");
        query.append("fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, ");
        query.append("hp, meter, skadested, prosesser_text, pda_dfu_ident, slettet_dato, slettet_av, internnotat from ");
        query.append("table(feltstudio_pk.sak_sok(#kontraktId, #fradato, #tildato, #veiliste, #pdaliste, #brukerliste)) ");
        
        WhereAndBuilder builder = new WhereAndBuilder();
        switch (params.getType()){
        case ALLE:
        	break;
        case FERDIG_BEHANDLET: 
        	builder.addWhereAnd("feltapp_pk.aktiv_sak_flagg(guid)=0");      	
        	break;
        case TIL_BEHANDLING:
        	builder.addWhereAnd("feltapp_pk.aktiv_sak_flagg(guid)=1");
        	break;
        }
        if (params.isIkkeEtterslep()){
        	builder.addWhereAnd("etterslep_flagg = 0");
        }
        if (params.isIkkeTilleggsarbeid()){
        	builder.addWhereAnd("tilleggsarbeid_flagg = 0");
        }
        
        if (params.isKunSlettede()){
        	builder.addWhereAnd("slettet_dato is not null");
        }else{
        	builder.addWhereAnd("slettet_dato is null");
        }
        query.append(builder.toString());
        query.append(" order by opprettet_dato desc ");
        Query q = em.createNativeQuery(query.toString())
		.setParameter("kontraktId", params.getValgtKontrakt().getId())
		.setParameter("veiliste", veiArray)
		.setParameter("pdaliste", pdaArray)
		.setParameter("brukerliste", brukerArray)
		.setParameter("fradato", params.getDateFra())
		.setParameter("tildato", params.getDateTil());
		
        NativeQueryWrapper<SakSokResult> wrapper = new NativeQueryWrapper<SakSokResult>(q, SakSokResult.class, SakSokResult.getPropertiesArray());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");
        
        List<SakSokResult> finalList = new ArrayList<SakSokResult>();
        if (params.getType() == TYPE.FERDIG_BEHANDLET) {
        	for (SakSokResult sakSokResult : wrapper.getWrappedList()) {			
        		if (	(sakSokResult.getR2() && sakSokResult.getR2RapportertDato() != null) ||
        				(sakSokResult.getR5() && sakSokResult.getR5RapportertDato() != null) ||
        				(sakSokResult.getR11() && sakSokResult.getR11RapportertDato() != null)) {
        			finalList.add(sakSokResult);
    			} 
        		
        		if (!sakSokResult.getR2() && !sakSokResult.getR5() && !sakSokResult.getR11()) {
        			finalList.add(sakSokResult);
				}        		
			}
        	return finalList;
		} else{
			return wrapper.getWrappedList();
		}
	}
	/** {@inheritDoc} */ 
	public Sak hentSak(String guid){
		Sak sak = em.find(Sak.class, guid);
		BeanHelper.fetchAndRefresh(em, sak, "stedfesting");
		if (sak.getAvvik()!=null)
			for (Avvik a:sak.getAvvik()){
				a.setNewEntity(false);
				a.getBilder().size();
				a.getStatuser().size();
				a.setMangel(avvikService.mangelSjekk(a.getSak().getKontrakt(), a));
			}
		
		sak.setNewEntity(false);
		return sak;
	}
	/** {@inheritDoc} */
	public Sak hentSak(Long refnummer){
		try{
		Sak sak = (Sak)em.createNamedQuery(Sak.QUERY_HENT_MED_REFNUMMER).setParameter("refnummer", refnummer).getSingleResult();
		BeanHelper.fetchAndRefresh(em, sak, "stedfesting");
		if (sak.getAvvik()!=null)
			for (Avvik a:sak.getAvvik()){
				a.setNewEntity(false);
				a.getBilder().size();
				a.getStatuser().size();
				a.setMangel(avvikService.mangelSjekk(a.getSak().getKontrakt(), a));
			}
		sak.setNewEntity(false);
		return sak;
		} catch (NoResultException e){
			return null;
		}
	}
	public Sak persistSak(Sak sak, String brukerSign) {
		if (sak.getOpprettetAv()==null){
			sak.setOpprettetAv(brukerSign);
			sak.setOpprettetDato(Clock.now());
		}else{
			sak.setEndretAv(brukerSign);
			sak.setEndretDato(Clock.now());
		}
		sak =em.merge(sak);
		
		return sak;
	}
	
	public void persistInternnotat(String guid, String internnotat){
		em.createNamedQuery(Sak.QUERY_OPPDATER_INTERNNOTAT)
		.setParameter(1, internnotat)
		.setParameter(2, guid)
		.executeUpdate();
	}
	
	public Sak deleteSak(Sak sak, String brukerSign){
		sak.setSlettetAv(brukerSign);
		sak.setSlettetDato(Clock.now());
		sak = em.merge(sak);
		return sak;
	}
	
	public void deleteSaker(List<SakSokResult> saker, String brukerSign){
		for (SakSokResult s:saker){
			Sak sak = em.find(Sak.class, s.getGuid());
			sak.setSlettetAv(brukerSign);
			sak.setSlettetDato(Clock.now());
			em.merge(sak);
		}
	}

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public void setAvvikService(AvvikServiceLocal avvikService) {
        this.avvikService = avvikService;
    }
}
