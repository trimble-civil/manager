package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Avvik implements Serializable{
	
//	private String prosessKode; //prosessavvik
//	private String status; //status
//	private Date statusDato;//statusdato
//	private Date tiltaksDato;//tiltaksfristdato
//	private String beskrivelse;//kommentarer
//	private String opprettetAv;//utfortav
//	private Long refnummer;//funnid
//	private Long meter;//km...
	
	private String guid;
	private Long refnummer;
	private String opprettetAv;
	private Date opprettetDato;
	private String beskrivelse;
	private Date tiltaksDato;
	private Date sisteStatusDato;
	private String sisteStatus;
	private String prosessKode;
	private Long fylkesnummer;
	private Long kommunenummer;
	private String veikategori;
	private String veistatus;
	private Long veinummer;
	private Long hp;
	private Long meter;

	public static String[] getPropertiesArray(){
		return new String[] {"guid", "refnummer", "opprettetAv", "opprettetDato", "beskrivelse",
					  "tiltaksDato", "sisteStatusDato", "sisteStatus", "prosessKode",
					  "fylkesnummer", "kommunenummer", "veikategori", "veistatus",
					  "veinummer", "hp", "meter"};
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Long getRefnummer() {
		return refnummer;
	}
	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}
	public String getOpprettetAv() {
		return opprettetAv;
	}
	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}
	public Date getOpprettetDato() {
		return opprettetDato;
	}
	public void setOpprettetDato(Date opprettetDato) {
		this.opprettetDato = opprettetDato;
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	public Date getTiltaksDato() {
		return tiltaksDato;
	}
	public void setTiltaksDato(Date tiltaksDato) {
		this.tiltaksDato = tiltaksDato;
	}
	public Date getSisteStatusDato() {
		return sisteStatusDato;
	}
	public void setSisteStatusDato(Date sisteStatusDato) {
		this.sisteStatusDato = sisteStatusDato;
	}
	public String getSisteStatus() {
		return sisteStatus;
	}
	public void setSisteStatus(String sisteStatus) {
		this.sisteStatus = sisteStatus;
	}
	public String getProsessKode() {
		return prosessKode;
	}
	public void setProsessKode(String prosessKode) {
		this.prosessKode = prosessKode;
	}
	public Long getFylkesnummer() {
		return fylkesnummer;
	}
	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}
	public Long getKommunenummer() {
		return kommunenummer;
	}
	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}
	public String getVeikategori() {
		return veikategori;
	}
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}
	public String getVeistatus() {
		return veistatus;
	}
	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}
	public Long getVeinummer() {
		return veinummer;
	}
	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}
	public Long getHp() {
		return hp;
	}
	public void setHp(Long hp) {
		this.hp = hp;
	}
	public Long getMeter() {
		return meter;
	}
	public void setMeter(Long meter) {
		this.meter = meter;
	}
	public Double getKm(){
		return ((double)meter)/1000d;
	}
	
	
}
