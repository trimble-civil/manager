package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.util.Date;
import java.util.List;

import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

public class InspeksjonsrapportParams extends QueryParams{
	private List<Veinettveireferanse> veireferanser;
	private Long driftkontraktId;
	private Date fraTidspunkt;
	private Date tilTidspunkt;
	/**
	 * Om km skal v�re med i utvalget for hvilke inspeksjoner soms kal v�re med. FALSE = kun ned til HP. 
	 */
	private Boolean inkluderKm;
	
	public List<Veinettveireferanse> getVeireferanser() {
		return veireferanser;
	}
	public void setVeireferanser(List<Veinettveireferanse> veireferanser) {
		this.veireferanser = veireferanser;
	}
	public Long getDriftkontraktId() {
		return driftkontraktId;
	}
	public void setDriftkontraktId(Long driftkontraktId) {
		this.driftkontraktId = driftkontraktId;
	}
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}
	public void setFraTidspunkt(Date fraTidspunkt) {
		this.fraTidspunkt = fraTidspunkt;
	}
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}
	public void setTilTidspunkt(Date tilTidspunkt) {
		this.tilTidspunkt = tilTidspunkt;
	}
	public Boolean getInkluderKm() {
		return inkluderKm;
	}
	public void setInkluderKm(Boolean inkluderKm) {
		this.inkluderKm = inkluderKm;
	}
	
	
}
