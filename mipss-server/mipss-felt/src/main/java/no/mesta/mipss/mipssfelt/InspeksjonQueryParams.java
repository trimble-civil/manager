package no.mesta.mipss.mipssfelt;




@SuppressWarnings("serial")
public class InspeksjonQueryParams extends QueryParams{
	private boolean kunEtterslep;
	
	private boolean hentPunkter;
	private boolean hentStrekninger;
	private boolean kunSistePunkt;
	private boolean ingenProsesser;
	private boolean hentAvvik;
	private boolean hentHendelser;
	private boolean hentForsikringsskade;
	private boolean hentSkred;
	
	private boolean visMerkelapper;
	
	public InspeksjonQueryParams(){

	}
	public boolean isKunEtterslep() {
		return kunEtterslep;
	}
	public void setKunEtterslep(boolean kunEtterslep) {
		this.kunEtterslep = kunEtterslep;
	}
	public boolean isHentPunkter() {
		return hentPunkter;
	}
	public void setHentPunkter(boolean hentPunkter) {
		this.hentPunkter = hentPunkter;
	}
	public boolean isHentStrekninger() {
		return hentStrekninger;
	}
	public void setHentStrekninger(boolean hentStrekninger) {
		this.hentStrekninger = hentStrekninger;
	}
	public boolean isKunSistePunkt() {
		return kunSistePunkt;
	}
	public void setKunSistePunkt(boolean kunSistePunkt) {
		this.kunSistePunkt = kunSistePunkt;
	}
	public boolean isIngenProsesser() {
		return ingenProsesser;
	}
	public void setIngenProsesser(boolean ingenProsesser) {
		this.ingenProsesser = ingenProsesser;
	}
	public boolean isVisMerkelapper() {
		return visMerkelapper;
	}
	public void setVisMerkelapper(boolean visMerkelapper) {
		this.visMerkelapper = visMerkelapper;
	}
	public boolean isHentAvvik() {
		return hentAvvik;
	}
	public void setHentAvvik(boolean hentAvvik) {
		this.hentAvvik = hentAvvik;
	}
	/**
	 * Henter r-skjema.
	 * @return
	 */
	public boolean isHentHendelser() {
		return hentHendelser;
	}
	public void setHentHendelser(boolean hentHendelser) {
		this.hentHendelser = hentHendelser;
	}
	public boolean isHentForsikringsskade() {
		return hentForsikringsskade;
	}
	public void setHentForsikringsskade(boolean hentForsikringsskade) {
		this.hentForsikringsskade = hentForsikringsskade;
	}
	public boolean isHentSkred() {
		return hentSkred;
	}
	public void setHentSkred(boolean hentSkred) {
		this.hentSkred = hentSkred;
	}
}
