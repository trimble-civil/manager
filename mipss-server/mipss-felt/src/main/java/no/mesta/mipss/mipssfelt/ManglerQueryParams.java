package no.mesta.mipss.mipssfelt;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

@SuppressWarnings("serial")
public class ManglerQueryParams implements java.io.Serializable{
	private Driftkontrakt driftkontrakt;
	private Boolean alle;
	
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}
	public Boolean getAlle() {
		return alle;
	}
	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
	}
	public void setAlle(Boolean alle) {
		this.alle = alle;
	}
}
