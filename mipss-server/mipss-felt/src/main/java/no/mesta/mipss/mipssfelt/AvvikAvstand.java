package no.mesta.mipss.mipssfelt;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class AvvikAvstand implements Serializable, IRenderableMipssEntity{
	
	private String guid;
	private String sakGuid;
	private Date slettetDato;
	private String status;
	private String prosessKode;
	private String veireferanse;
	private Date registrertDato;
	private Long refnummer;
	private Double avstand;
	private Double x;
	private Double y;
	private String registrertAv;
	
	private String beskrivelse;
	
	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}
	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the prosessKode
	 */
	public String getProsessKode() {
		return prosessKode;
	}
	/**
	 * @param prosessKode the prosessKode to set
	 */
	public void setProsessKode(String prosessKode) {
		this.prosessKode = prosessKode;
	}
	/**
	 * @return the veireferanse
	 */
	public String getVeireferanse() {
		return veireferanse;
	}
	/**
	 * @param veireferanse the veireferanse to set
	 */
	public void setVeireferanse(String veireferanse) {
		this.veireferanse = veireferanse;
	}
	/**
	 * @return the registrertDato
	 */
	public Date getRegistrertDato() {
		return registrertDato;
	}
	/**
	 * @param registrertDato the registrertDato to set
	 */
	public void setRegistrertDato(Date registrertDato) {
		this.registrertDato = registrertDato;
	}
	/**
	 * @return the refnummer
	 */
	public Long getRefnummer() {
		return refnummer;
	}
	/**
	 * @param refnummer the refnummer to set
	 */
	public void setRefnummer(Long refnummer) {
		this.refnummer = refnummer;
	}
	/**
	 * @return the avstand
	 */
	public Double getAvstand() {
		return avstand;
	}
	/**
	 * @param avstand the avstand to set
	 */
	public void setAvstand(Double avstand) {
		this.avstand = avstand;
	}
	@Override
	public String getTextForGUI() {
		return null;
	}
	public void setRegistrertAv(String registrertAv) {
		this.registrertAv = registrertAv;
	}
	public String getRegistrertAv(){
		return registrertAv;
	}
	public void setX(Double x) {
		this.x = x;
	}
	public Double getX() {
		return x;
	}
	public void setY(Double y) {
		this.y = y;
	}
	public Double getY() {
		return y;
	}
	public void setSlettetDato(Date slettetDato) {
		this.slettetDato = slettetDato;
	}
	public Date getSlettetDato() {
		return slettetDato;
	}
	public void setSakGuid(String sakGuid) {
		this.sakGuid = sakGuid;
	}
	public String getSakGuid() {
		return sakGuid;
	}
	
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	

}
