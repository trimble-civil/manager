package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class StrekningAvvik implements Serializable{
	private Strekning strekning;
	private List<Avvik> avvikList;
	public static String[] getPropertiesArray(){
		return new String[]{"strekning", "avvikList"};
	}
	public Strekning getStrekning() {
		return strekning;
	}
	public void setStrekning(Strekning strekning) {
		this.strekning = strekning;
	}
	public List<Avvik> getAvvikList() {
		return avvikList;
	}
	public void setAvvikList(List<Avvik> avvikList) {
		this.avvikList = avvikList;
	}
	public Integer getAntallAvvik(){
		if (avvikList!=null)
			return avvikList.size();
		return 0;
	}
}
