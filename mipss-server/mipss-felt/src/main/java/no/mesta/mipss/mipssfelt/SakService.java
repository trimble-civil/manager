package no.mesta.mipss.mipssfelt;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.mipssfield.Sak;

@Remote
public interface SakService {

	public static final String BEAN_NAME = "SakServiceBean";
	/**
	 * Søker etter saker med gitte parametere
	 * @param params
	 * @return
	 */

	List<SakSokResult> sokSaker(SakQueryParams params) throws Exception;
	Sak hentSak(String guid);
	Sak hentSak(Long refnummer);
	
	/**
	 * Lagrer saken, dersom det finnes avvik og dette er en ny sak, returneres saken uten avvik.
	 * @param sak
	 * @param brukerSign
	 * @return
	 */
	Sak persistSak(Sak sak, String brukerSign);
	void persistInternnotat(String guid, String internnotat);
	
	/**
	 * Sletter en sak i systemet. 
	 * 
	 * setter slettet_dato og slettet_av feltene i databasen.
	 * @param sak
	 * @param brukerSign
	 * @return
	 */
	Sak deleteSak(Sak sak, String brukerSign);
	
	/**
	 * Sletter saker i systemet
	 *
	 * Setter slettet_dato og slettet_av 
	 * @param saker
	 * @param brukerSign
	 */
	void deleteSaker(List<SakSokResult> saker, String brukerSign);
}
