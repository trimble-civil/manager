package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.sql.Array;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NestedTableDescriptor;
import no.mesta.mipss.query.StructHelper;
import no.mesta.mipss.query.WhereAndBuilder;

@Stateless(name = InspeksjonsrapportService.BEAN_NAME, mappedName="ejb/"+InspeksjonsrapportService.BEAN_NAME)
public class InspeksjonsrapportBean implements InspeksjonsrapportService{

	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;

	public List<InspeksjonsRapport> hentRapportdata(InspeksjonsrapportParams params) throws SQLException {
        StructHelper structHelper = new StructHelper();

        Array array = structHelper.createVeireferanseArray(params.getVeireferanser(), params.getInkluderKm());

		structHelper.goodnight();

        StringBuilder query = new StringBuilder();
        query.append("select * from table(rapport_funksjoner.insp_rapp(#kontraktId, #veiliste, #fradato, #tildato)) ");
        
        WhereAndBuilder wab = new WhereAndBuilder();
        if (params.getBrukerList()!=null){
        	String brukere = StringUtils.join(params.getBrukerList(), "','");
    		wab.addWhereAnd("opprettet_av in ('"+brukere+"') ");
        }
        if (params.getPdaDfuIdentList()!=null){
        	
        }
        query.append(wab.toString());
        Query q = em.createNativeQuery(query.toString())
			.setParameter("kontraktId", params.getDriftkontraktId())
			.setParameter("veiliste", array)
			.setParameter("fradato", params.getFraTidspunkt())
			.setParameter("tildato", params.getTilTidspunkt());
        
        NativeQueryWrapper<InspeksjonsRapport> wrapper = new NativeQueryWrapper<InspeksjonsRapport>(q, InspeksjonsRapport.class, InspeksjonsRapport.getPropertiesArray());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");
        
        NestedTableDescriptor<?> descStr = new NestedTableDescriptor<StrekningAvvik>(StrekningAvvik.class, StrekningAvvik.getPropertiesArray());
        NestedTableDescriptor<?> descStrStr = new NestedTableDescriptor<Strekning>(Strekning.class, Strekning.getPropertiesArray());
        NestedTableDescriptor<?> descStrAvvik = new NestedTableDescriptor<Avvik>(Avvik.class, Avvik.getPropertiesArray());
        descStr.addDescriptor(descStrStr);
        descStr.addDescriptor(descStrAvvik);
        NestedTableDescriptor<?> descAvvik = new NestedTableDescriptor<Avvik>(Avvik.class, Avvik.getPropertiesArray());
        wrapper.addNestedTableDescriptor(descStr);
        wrapper.addNestedTableDescriptor(descAvvik);
        List<InspeksjonsRapport> wrappedList = wrapper.getWrappedList();
        return wrappedList;
	}
	
	/**
	 * Oppretter en object[] av veinettvereferansen der indexene i array matcher rekkef�lgen objektet er definert i databasen.
	 * @param vr
	 * @param inkluderKm
	 * @return
	 */
	private Object[] convertVeinettveireferanseToOjbect(Veinettveireferanse vr, boolean inkluderKm){
		Object[] ar = new Object[8];
		ar[0]=vr.getFylkesnummer();
		ar[1]=vr.getKommunenummer();
		ar[2]=vr.getVeikategori();
		ar[3]=vr.getVeistatus();
		ar[4]=vr.getVeinummer();
		ar[5]=vr.getHp();
		if (inkluderKm){
			ar[6]=vr.getFraKm();
			ar[7]=vr.getTilKm();
		}else{
			ar[6]=null;
			ar[7]=null;
		}
		return ar;
	}
}
