package no.mesta.mipss.mipssfelt;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.valueobjects.ElrappStatus;
import no.mesta.mipss.valueobjects.ElrappStatus.Status;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import no.mesta.mipss.valueobjects.ElrappTypeR.Type;


@SuppressWarnings("serial")
public class RskjemaQueryParams extends QueryParams implements PropertyChangeSource{
	
	private ElrappStatus.Status elrappStatus;
	private ElrappTypeR.Type typeR;
	
	
	public ElrappStatus.Status getElrappStatus() {
		return elrappStatus;
	}
	public ElrappTypeR.Type getTypeR() {
		return typeR;
	}
	public void setElrappStatus(ElrappStatus.Status elrappStatus) {
		Status old = this.elrappStatus;
		this.elrappStatus = elrappStatus;
		getProps().firePropertyChange("elrappStatus", old, elrappStatus);
	}
	public void setTypeR(ElrappTypeR.Type typeR) {
		Type old = this.typeR;
		this.typeR = typeR;
		getProps().firePropertyChange("typeR", old, typeR);
	}
}
