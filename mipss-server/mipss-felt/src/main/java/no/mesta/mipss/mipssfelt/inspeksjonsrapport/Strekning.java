package no.mesta.mipss.mipssfelt.inspeksjonsrapport;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class Strekning implements Serializable{
	private Long fylkesnummer;
	private Long kommunenummer;
	private String veikategori;
	private String veistatus;
	private Long veinummer;
	private Long hp;
	private Double fraKm;
	private Double tilKm;
	private Date fraTidspunkt;
	private Date tilTidspunkt;
	
	public static String[] getPropertiesArray(){
		return new String[]{"fylkesnummer", "kommunenummer", "veikategori", "veistatus", 
							"veinummer", "hp", "fraKm", "tilKm", "fraTidspunkt", "tilTidspunkt"};
	}
	public String getVeiident(){
		return veikategori+veistatus+veinummer;
	}
	public Long getFylkesnummer() {
		return fylkesnummer;
	}
	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}
	public Long getKommunenummer() {
		return kommunenummer;
	}
	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}

	public String getVeikategori() {
		return veikategori;
	}

	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}

	public String getVeistatus() {
		return veistatus;
	}

	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}

	public Long getVeinummer() {
		return veinummer;
	}

	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}

	public Long getHp() {
		return hp;
	}

	public void setHp(Long hp) {
		this.hp = hp;
	}

	public Double getFraKm() {
		return fraKm;
	}

	public void setFraKm(Double fraKm) {
		this.fraKm = fraKm;
	}

	public Double getTilKm() {
		return tilKm;
	}

	public void setTilKm(Double tilKm) {
		this.tilKm = tilKm;
	}
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}
	public void setFraTidspunkt(Date fraTidspunkt) {
		this.fraTidspunkt = fraTidspunkt;
	}
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}
	public void setTilTidspunkt(Date tilTidspunkt) {
		this.tilTidspunkt = tilTidspunkt;
	}
}
