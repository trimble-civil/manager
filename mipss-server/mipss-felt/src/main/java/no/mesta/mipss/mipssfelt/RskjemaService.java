package no.mesta.mipss.mipssfelt;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;

@Remote
public interface RskjemaService {
	public static final String BEAN_NAME = "RskjemaServiceBean";
	
	/**
	 * Søker etter r-skjema med gitte parametere
	 * @param params
	 * @return
	 */
	List<RskjemaSokResult> sokRskjema(RskjemaQueryParams params) throws Exception;

	List<RskjemaSokResult> sokHendelse(Date datoFra, Date datoTil, Driftkontrakt kontrakt, Long[] aarsakId);
	List<RskjemaSokResult> sokForsikringsskade(Date datoFra, Date datoTil, Driftkontrakt kontrakt);
	List<RskjemaSokResult> sokSkred(Date datoFra, Date datoTil, Driftkontrakt kontrakt);
	
	Hendelse hentHendelse(String guid);
	Hendelse hentHendelse(Long refnummer);
	Hendelse hentHendelseElrapp(Long elrappId, Long driftkontraktId);
	Hendelse persistHendelse(Hendelse hendelse, String brukerSign);
	Hendelse deleteHendelse(Hendelse hendelse, String brukerSign);
	Hendelse restoreHendelse(Hendelse hendelse, String brukerSign);
	
	Forsikringsskade hentForsikringsskade(String guid);
	Forsikringsskade hentForsikringsskade(Long refnummer);
	Forsikringsskade hentForsikringsskadeElrapp(Long elrappId, Long driftkontraktId);
	Forsikringsskade persistForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign);
	Forsikringsskade deleteForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign);
	Forsikringsskade restoreForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign);

	Skred hentSkred(String guid);
	Skred hentSkred(Long refnummer);
	Skred hentSkredElrapp(Long elrappId, Long driftkontraktId);
	Skred persistSkred(Skred skred, String brukerSign);
	Skred deleteSkred(Skred skred, String brukerSign);
	Skred restoreSkred(Skred skred, String brukerSign);
	
	/**
	 * Henter ut alle hendelsesårsakene
	 * @return
	 */
	List<Hendelseaarsak> hentAlleHendelseaarsaker();
	
	/**
	 * Henter ut alle trafikktiltak
	 * @return
	 */
	List<Trafikktiltak> hentAlleTrafikktiltak();

	List<ObjektType> hentAlleObjektTyper(Long prosessId, Long kontraktId);
	List<ObjektAvvikType> hentAlleObjektAvvikTyper(Long prosessId, Long kontraktId);
	List<ObjektAvvikKategori> hentAlleObjektAvvikKategorier(Long prosessId, Long kontraktId);
	List<ObjektStedTypeAngivelse> hentAlleObjektStedTypeAngivelser(Long prosessId, Long kontraktId);
	List<ArbeidType> hentAlleArbeidTyper();

	/**
	 * Henter ut alle egenskapene til et Skred
	 * @return
	 */
	List<Egenskap> getAlleEgenskaper();
	/**
	 * Henter ut alle mulige verdier tilknyttet en Egenskap
	 * @param egenskap
	 * @return
	 */
	List<Egenskapverdi> getEgenskapverdier(Egenskap egenskap);

	void deleteRskjema(List<RskjemaSokResult> selectedEntities,	String brukerSign);
	
	
}
