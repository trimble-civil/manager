package no.mesta.mipss.mipssfelt;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

@SuppressWarnings("serial")
public class QueryParams implements Serializable, PropertyChangeSource{
	private String[]  brukerList;
	private Date dateFra;
	private Date dateTil;
	private boolean inkluderKmIVeiListe;
	private boolean kunSlettede;
	private Long[] pdaDfuIdentList;
	private PropertyChangeSupport props;
	private Long[] prosesser;
	private String[] prosesskoder;
	private Driftkontrakt valgtKontrakt;
	private List<Veinettveireferanse> veiListe;
	private boolean dateEnabled=true;
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}
	
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}
	public String[]  getBrukerList() {
		return brukerList;
	}
	public Date getDateFra() {
		return dateFra;
	}
	public Date getDateTil() {
		return dateTil;
	}
	public Long[] getPdaDfuIdentList() {
		return pdaDfuIdentList;
	}
	protected PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}
	public Long[] getProsesser() {
		return prosesser;
	}
	public String[] getProsesskoder() {
		return prosesskoder;
	}
	
	public Driftkontrakt getValgtKontrakt() {
		return valgtKontrakt;
	}
	public List<Veinettveireferanse> getVeiListe() {
		return veiListe;
	}
	public boolean isInkluderKmIVeiListe() {
		return inkluderKmIVeiListe;
	}
	public boolean isKunSlettede() {
		return kunSlettede;
	}
	/**
	 * Slår sammen innholdet i denne klassen til angitt objekt.
	 * @param params
	 */
	public void merge(QueryParams params){
		params.setDateFra(dateFra);
		params.setDateTil(dateTil);
		params.setInkluderKmIVeiListe(inkluderKmIVeiListe);
		params.setPdaDfuIdentList(pdaDfuIdentList);
		params.setValgtKontrakt(valgtKontrakt);
		params.setVeiListe(veiListe);
		params.setProsesser(prosesser);
	}
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}
	
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}
	public void setBrukerList(String[] brukerList) {
		String[]  old = this.brukerList;
		this.brukerList = brukerList;
		getProps().firePropertyChange("brukerList", old, brukerList);
	}
	public void setDateFra(Date dateFra) {
		Date old = this.dateFra;
		this.dateFra = dateFra;
		getProps().firePropertyChange("dateFra", old, dateFra);
	}
	public void setDateTil(Date dateTil) {
		Date old = this.dateTil;
		this.dateTil = dateTil;
		getProps().firePropertyChange("dateTil", old, dateTil);
	}
	public void setInkluderKmIVeiListe(boolean inkluderKmIVeiListe) {
		this.inkluderKmIVeiListe = inkluderKmIVeiListe;
	}

	public void setKunSlettede(boolean kunSlettede) {
		boolean old = this.kunSlettede;
		this.kunSlettede = kunSlettede;
		getProps().firePropertyChange("kunSlettede", old, kunSlettede);
	}

	public void setPdaDfuIdentList(Long[] pdaDfuIdentList) {
		Long[] old = this.pdaDfuIdentList;
		this.pdaDfuIdentList = pdaDfuIdentList;
		getProps().firePropertyChange("pdaDfuIdentList", old, pdaDfuIdentList);
	}

	public void setProsesser(Long[] prosesser) {
		Long[] old = this.prosesser;
		this.prosesser = prosesser;
		getProps().firePropertyChange("prosesser", old, prosesser);
	}

	public void setProsesskoder(String[] prosesskoder) {
		String[] old = this.prosesskoder;
		this.prosesskoder = prosesskoder;
		getProps().firePropertyChange("prosesskoder", old, prosesskoder);
	}

	public void setValgtKontrakt(Driftkontrakt valgtKontrakt) {
		Driftkontrakt old = this.valgtKontrakt;
		this.valgtKontrakt = valgtKontrakt;
		getProps().firePropertyChange("valgtKontrakt", old, valgtKontrakt);
	}

	public void setVeiListe(List<Veinettveireferanse> veiListe) {
		List<Veinettveireferanse> old = this.veiListe;
		this.veiListe = veiListe;
		getProps().firePropertyChange("veiListe", old, veiListe);
	}

	public void setDateEnabled(boolean dateEnabled) {
		boolean old = this.dateEnabled;
		this.dateEnabled = dateEnabled;
		getProps().firePropertyChange("dateEnabled", old, dateEnabled);
	}

	public boolean isDateEnabled() {
		return dateEnabled;
	}
	
	
}
