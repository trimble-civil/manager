package no.mesta.mipss.mipssfelt;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.*;

/**
 * Tjeneste for avvik og tilhørende entiteter
 * 
 * @author harkul
 *
 */
@Remote
public interface AvvikService {
	public static final String BEAN_NAME = "AvvikBean";
	@Deprecated
	List<AvvikSokResult> sokMangler(ManglerQueryParams params);
	List<AvvikSokResult> sokMangler(AvvikQueryParams params) throws Exception;
	/**
	 * Søker etter avvik med gitte parametere
	 * @param params
	 * @return
	 */
	List<AvvikSokResult> sokAvvik(AvvikQueryParams params) throws Exception;
	List<AvvikSokResult> sokAvvikEntity(AvvikQueryParams params);
	/**
	 * Henter et avvik med angitt guid og refresher objektet fra databasen
	 * @param guid
	 * @return
	 */
	Avvik hentAvvik(String guid);
	
	/**
	 * Lagrer et avvik i databasen
	 * @param avvik
	 * @param brukerSign
	 * @return
	 */
	Avvik persistAvvik(Avvik avvik, String brukerSign);
	
	/**
	 * Sjekker om et avvik er en mangel på angitt dato
	 * @param dato
	 * @return
	 */
	Boolean mangelSjekk(Driftkontrakt kontrakt, Avvik avvik, Date dato);
	
	/**
	 * Sjekker om avviket er en mangel pr Clock.now()
	 * 
	 * @return
	 */
	Boolean mangelSjekk(Driftkontrakt kontrakt, Avvik avvik);
	
	/**
	 * Sjekker om avviket er en mangel pr Clock.now(). 
	 * 
	 * OBs: Må hente driftkontraktens id først og kan være noe treigere enn de andre mangelsjekk funksjonene
	 * @param avvikGuid
	 * @return
	 */
	Boolean mangelSjekk(String avvikGuid);
	/**
	 * Sletter et avvik i databasen( setter slettet dato og brukersign)
	 * @param avvik
	 * @param brukerSign
	 * @return
	 */
	Avvik deleteAvvik(Avvik avvik, String brukerSign);
	
	/**
	 * Sletter alle avvik i listen (setter slettet dato og brukersign);
	 * @param avvikList
	 * @param brukerSign
	 * @return returnerer de avvikene som ikke kunne slettes pga tilknyttet r-skjema.
	 */
	List<AvvikSokResult> deleteAvvik(List<AvvikSokResult> avvikList, String brukerSign);
	/**
	 * Gjenoppretter avviket i databasen( fjernet slettet_dato og slettet av)
	 * @param avvik
	 * @param brukerSign
	 * @return
	 */
	Avvik restoreAvvik(Avvik avvik, String brukerSign);
	/**
	 * Henter et avvik med angitt refnummer og refresher objektet fra databasen
	 * 
	 * @param refnummer
	 * @return null hvis det ikke finnes avvik med angitt refnummer
	 */
	Avvik hentAvvik(Long refnummer);
	
	/**
	 * Henter alle statustypene i databasen
	 * @return
	 */
	List<AvvikstatusType> hentAlleStatusTyper();
	
	/**
	 * Henter alle avvikaarsaker fra databasen
	 * @return
	 */
	List<Avvikaarsak> hentAlleAvvikaarsaker();
	List<Avviktilstand> hentAlleAvviktilstander();

	List<ObjektType> hentAlleObjektTyper(Long prosessId, Long kontraktId);
	List<ObjektAvvikType> hentAlleObjektAvvikTyper(Long prosessId, Long kontraktId);
	List<ObjektAvvikKategori> hentAlleObjektAvvikKategorier(Long prosessId, Long kontraktId);
	List<ObjektStedTypeAngivelse> hentAlleObjektStedTypeAngivelser(Long prosessId, Long kontraktId);
	List<ArbeidType> hentAlleArbeidTyper();

	void persistPunktstedfest(Punktstedfest stedfest);
	
	Avvik leggTilKommentar(String avvikGuid, String kommentar, String brukerSign);
	List<AvvikAvstand> sokAvvikINarheten(Avvik avvik, Long driftkontraktId);
	
}
