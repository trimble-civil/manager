package no.mesta.mipss.mipssfelt;

import java.util.ArrayList;
import java.util.List;

/**
 * Bygger en select
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SelectQueryBuilder {

	private List<String> asc = new ArrayList<String>();
	private List<String> from = new ArrayList<String>();
	private List<String> join = new ArrayList<String>();
	private List<String> operator = new ArrayList<String>();
	private List<String> orderBy = new ArrayList<String>();
	private List<String> select = new ArrayList<String>();
	private List<String> where = new ArrayList<String>();

	public void addFrom(String s) {
		from.add(s);
	}

	public void addJoin(String s) {
		join.add(s);
	}

	public void addOrderBy(String o, String a) {
		orderBy.add(o);
		asc.add(a);
	}

	public void addSelect(String s) {
		select.add(s);
	}

	public void addWhere(String w, String o) {
		where.add(w);
		operator.add(o);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		StringBuffer query = new StringBuffer("SELECT\n");

		int i = 0;
		for (String s : select) {
			i++;
			query.append("\t" + s);

			if (i == select.size()) {
				query.append("\n");
			} else if (select.size() > 1) {
				query.append(",\n");
			}
		}

		query.append("FROM\n");
		i = 0;
		for (String s : from) {
			i++;
			query.append("\t" + s);

			if (i == from.size()) {
				query.append("\n");
			} else if (from.size() > 1) {
				query.append(",\n");
			}
		}

		for (String s : join) {
			query.append("\t" + s + "\n");
		}

		if (where.size() > 0) {
			query.append("WHERE\n");
			i = 0;
			for (String s : where) {
				i++;
				if (i > 1) {
					query.append("\t" + operator.get(i - 1) + " ");
				}

				if (i == 1) {
					query.append("\t" + s);
				} else {
					query.append(s);
				}

				query.append("\n");
			}
		}

		if (orderBy.size() > 0) {
			query.append("ORDER BY\n");
			i = 0;
			for (String s : orderBy) {
				i++;
				query.append("\t" + s + " " + asc.get(i - 1));
				if (i < orderBy.size()) {
					query.append(",");
				}
				query.append("\n");
			}
		}

		return query.toString();
	}
}
