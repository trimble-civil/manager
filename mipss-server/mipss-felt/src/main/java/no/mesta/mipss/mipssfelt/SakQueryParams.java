package no.mesta.mipss.mipssfelt;

import no.mesta.mipss.common.PropertyChangeSource;

@SuppressWarnings("serial")
public class SakQueryParams extends QueryParams implements PropertyChangeSource{

	public enum TYPE{
		ALLE,
		FERDIG_BEHANDLET,
		TIL_BEHANDLING
	}
	
	private boolean ikkeEtterslep;
	private boolean ikkeTilleggsarbeid;
	
	private TYPE type = TYPE.ALLE;
	
	public TYPE getType(){
		return type;
	}
	public boolean isIkkeEtterslep() {
		return ikkeEtterslep;
	}
	public boolean isIkkeTilleggsarbeid() {
		return ikkeTilleggsarbeid;
	}
	public void setIkkeEtterslep(boolean ikkeEtterslep) {
		boolean old = this.ikkeEtterslep;
		this.ikkeEtterslep = ikkeEtterslep;
		getProps().firePropertyChange("ikkeEtterslep", old, ikkeEtterslep);
	}
	public void setIkkeTilleggsarbeid(boolean ikkeTilleggsarbeid) {
		boolean old = this.ikkeTilleggsarbeid;
		this.ikkeTilleggsarbeid = ikkeTilleggsarbeid;
		getProps().firePropertyChange("ikkeTilleggsarbeid", old, ikkeTilleggsarbeid);
	}
	public void setType(TYPE type){
		TYPE old = this.type;
		this.type = type;
		getProps().firePropertyChange("type", old, type);
	}
}
