package no.mesta.mipss.mipssfelt;

import java.awt.Point;
import java.sql.Array;
import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.mipssfelt.AvvikQueryParams.RapportType;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.StructHelper;
import no.mesta.mipss.query.WhereAndBuilder;

import org.apache.commons.lang.StringUtils;

@Stateless(name = AvvikService.BEAN_NAME, mappedName="ejb/"+AvvikService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class AvvikBean implements AvvikService, AvvikServiceLocal{
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	@EJB private KonfigParamLocal konfig;
	
	public List<AvvikSokResult> sokMangler(AvvikQueryParams params) throws Exception {
		StructHelper structHelper = new StructHelper();

        Array veiArray = structHelper.createVeireferanseArray(params.getVeiListe(), params.isInkluderKmIVeiListe());
        List<String> brukerList = params.getBrukerList()==null?null:Arrays.asList(params.getBrukerList());
        Array brukerArray = structHelper.createBrukerArray(brukerList);
        List<Long> pdaList = params.getPdaDfuIdentList()==null?null:Arrays.asList(params.getPdaDfuIdentList());
        Array pdaArray = structHelper.createPdaArray(pdaList);
        Array prosessarray = structHelper.createProsessArray(null);

		structHelper.goodnight();

        StringBuilder query = new StringBuilder();
        query.append("select avvik_guid, sak_guid, refnummer, opprettet_av, opprettet_dato, slettet_av, slettet_dato, lukket_dato, siste_status_navn, siste_status, siste_status_dato, tiltaks_dato, ");
        query.append("prosess_id, prosess_kode, prosess_navn, aarsak_id, aarsak_navn, tilleggsarbeid_flagg, ");
        query.append("etterslep_flagg, trafikksikkerhet_flagg, fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, ");
        query.append("hp, meter, kjorefelt, beskrivelse, antall_bilder, x, y, historikk_streng, mangel_flagg, pda_dfu_navn, pda_dfu_ident from ");
        
        query.append("table(feltstudio_pk.avvik_sok(#kontraktId, #fradato, #tildato, ");
        query.append("#avvikFlagg, #aktiveAvvikFlagg, #nyeManglerFlagg, #apneManglerFlagg, #alleManglerFlagg, #kommendeManglerFlagg, ");
        query.append("#veiliste, #pdaliste, #brukerliste, #prosessliste)) ");

        WhereAndBuilder builder = new WhereAndBuilder();
    	
    	query.append(builder.toString());
    	query.append(" order by tiltaks_dato desc ");
    	
        Query q = em.createNativeQuery(query.toString())
		.setParameter("kontraktId", params.getValgtKontrakt().getId())
		.setParameter("veiliste", veiArray)
		.setParameter("pdaliste", pdaArray)
		.setParameter("brukerliste", brukerArray)
		.setParameter("fradato", params.getDateFra())
		.setParameter("tildato", params.getDateTil())
        .setParameter("prosessliste", prosessarray)
        .setParameter("aktiveAvvikFlagg", 0)
        .setParameter("avvikFlagg", 0)
        .setParameter("nyeManglerFlagg", params.isNyeMangler()?1:0)
        .setParameter("apneManglerFlagg", params.isAapentAvvik()?1:0)
        .setParameter("alleManglerFlagg", params.isAlleMangler()?1:0)
        .setParameter("kommendeManglerFlagg", params.isKommendeMangler()?1:0);
		
        NativeQueryWrapper<AvvikSokResult> wrapper = new NativeQueryWrapper<AvvikSokResult>(q, AvvikSokResult.class, AvvikSokResult.getPropertiesArray());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");
        
		List<AvvikSokResult> result = wrapper.getWrappedList();
		return result;
	}
	
	public List<AvvikSokResult> sokAvvik(AvvikQueryParams params) throws Exception {
		StructHelper structHelper = new StructHelper();

        Array veiArray = structHelper.createVeireferanseArray(params.getVeiListe(), params.isInkluderKmIVeiListe());
        List<String> brukerList = params.getBrukerList()==null?null:Arrays.asList(params.getBrukerList());
        Array brukerArray = structHelper.createBrukerArray(brukerList);
        List<Long> pdaList = params.getPdaDfuIdentList()==null?null:Arrays.asList(params.getPdaDfuIdentList());
        Array pdaArray = structHelper.createPdaArray(pdaList);
        List<Long> prosessList = params.getProsesser()==null?null:Arrays.asList(params.getProsesser());
        Array prosessarray = structHelper.createProsessArray(prosessList);

		structHelper.goodnight();
        
        StringBuilder query = new StringBuilder();
        query.append("select avvik_guid, sak_guid, refnummer, opprettet_av, opprettet_dato, slettet_av, slettet_dato, lukket_dato, siste_status_navn, siste_status, siste_status_dato, tiltaks_dato, ");
        query.append("prosess_id, prosess_kode, prosess_navn, aarsak_id, aarsak_navn, tilleggsarbeid_flagg, ");
        query.append("etterslep_flagg, trafikksikkerhet_flagg, fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, ");
        query.append("hp, meter, kjorefelt, beskrivelse, antall_bilder, x, y, historikk_streng, mangel_flagg, pda_dfu_navn, pda_dfu_ident from ");
        query.append("table(feltstudio_pk.avvik_sok(#kontraktId, #fradato, #tildato, ");
        query.append("#avvikFlagg, #aktiveAvvikFlagg, #nyeManglerFlagg, #apneManglerFlagg, #alleManglerFlagg, #kommendeManglerFlagg, ");
        query.append("#veiliste, #pdaliste, #brukerliste, #prosessliste)) ");
        
        WhereAndBuilder builder = new WhereAndBuilder();
        if (params.isEtterslep())
        	builder.addWhereAnd("etterslep_flagg=1");
        
    	if (params.isFristdatoOverskredet()){
    		builder.addWhereAnd("(trunc(tiltaks_dato) < trunc(lukket_dato)) or (trunc(tiltaks_dato) < trunc(sysdate) and lukket_dato is null) ");
    	}
    	
    	if (params.isKunSlettede())
    		builder.addWhereAnd("slettet_dato is not null");
    	else
    		builder.addWhereAnd("slettet_dato is null");
    	
    	if (params.isTilleggsarbeid())
    		builder.addWhereAnd("tilleggsarbeid_flagg=1");
    	
    	if (params.isTrafikksikkerhet())
    		builder.addWhereAnd("trafikksikkerhet_flagg=1");
    	
    	if (params.getAvvikstatus()!=null){
    		String status = StringUtils.join(params.getAvvikstatus(), "','");
    		builder.addWhereAnd("siste_status in ('"+status+"') ");
    	}
    	if (params.getAvvikaarsak()!=null){
    		String aarsak = StringUtils.join(params.getAvvikaarsak(), ",");
    		builder.addWhereAnd("aarsak_id in ("+aarsak+") ");
    	}
    	query.append(builder.toString());
    	query.append(" order by opprettet_dato desc ");
    	
    	RapportType rapportType = params.getRapportType();
    	int sokeType = 1;
    	int aktiveAvvikFlagg = 0;
    	switch (rapportType){
	    	case OPPRETTET_I_PERIODEN: sokeType=1;break;
	    	case STATUSENDRING_I_PERIODEN:sokeType=2;break; 
	    	case APNE_PR_TILDATO_EL_LUKKET_I_PERIODEN: sokeType=0;aktiveAvvikFlagg=1;break;//TODO fjern aktiveavvikflagg og sett soketype=3 ved release etter avtale med magne
	    	
    	}
    	
        Query q = em.createNativeQuery(query.toString())
		.setParameter("kontraktId", params.getValgtKontrakt().getId())
		.setParameter("veiliste", veiArray)
		.setParameter("pdaliste", pdaArray)
		.setParameter("brukerliste", brukerArray)
		.setParameter("fradato", params.getDateFra())
		.setParameter("tildato", params.getDateTil())
        .setParameter("prosessliste", prosessarray)
        .setParameter("aktiveAvvikFlagg", aktiveAvvikFlagg)
        .setParameter("avvikFlagg", sokeType)
        .setParameter("nyeManglerFlagg", 0)
        .setParameter("apneManglerFlagg", 0)
        .setParameter("alleManglerFlagg", 0)
        .setParameter("kommendeManglerFlagg", 0);
		
        NativeQueryWrapper<AvvikSokResult> wrapper = new NativeQueryWrapper<AvvikSokResult>(q, AvvikSokResult.class, AvvikSokResult.getPropertiesArray());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");
        
		List<AvvikSokResult> result = wrapper.getWrappedList();
		return result;
	}
	
	public List<AvvikSokResult> sokAvvikEntity(AvvikQueryParams params){
		StringBuilder q = new StringBuilder();
		q.append("select distinct a.guid, ps.snappet_x, ps.snappet_y, ps.x, ps.y from avvik_xv a ");
		q.append("join sak_xv s on s.guid = a.sak_guid ");
		q.append("join punktstedfest_xv ps on ps.sak_guid = s.guid "); 
		if (params.getAvvikstatusId()!=null&&params.getAvvikstatusId().size()!=0){
			q.append("join avvik_status_xv ast on ast.avvik_guid = a.guid ");
		}
		
		q.append("where a.opprettet_dato between ?1 and ?2 ");
		q.append("and s.kontrakt_id = ?3 ");
		if (params.getTilstandId()!=null&&params.getTilstandId().length!=0){
			String tids = StringUtils.join(params.getTilstandId(), ',');
			q.append("and a.tilstand_id in ("+tids+") ");
		}		
		if (params.getProsesser()!=null&&params.getProsesser().length!=0){
			String pids = StringUtils.join(params.getProsesser(), ',');
			q.append("and a.prosess_id in ("+pids+") ");
		}
		if (params.getAvvikstatusId()==null&&params.getAvvikstatusId().size()!=0){
			String sids = StringUtils.join(params.getAvvikstatusId(), ',');
			q.append("and ast.status_id in ("+sids+") ");
		}
		
		
		Query query = em.createNativeQuery(q.toString())
			.setParameter(1, params.getDateFra())
			.setParameter(2, params.getDateTil())
			.setParameter(3, params.getValgtKontrakt().getId());
		
        NativeQueryWrapper<AvvikSokResult> wrapper = new NativeQueryWrapper<AvvikSokResult>(query, AvvikSokResult.class, "guid", "snappetX", "snappetY", "x", "y");
        return wrapper.getWrappedList();

//        
//		if (params.getProsesser()!=null&&params.getProsesser().length!=0){
//			query.setParameter("prosessId", Arrays.asList(params.getProsesser()));
//		}
//		
//		q.append("select a from Avvik a where a.opprettetDato between :datoFra and :datoTil and a.sak.kontrakt.id=:kontraktId ");
//		if (params.getTilstandId()!=null&&params.getTilstandId().length!=0){
//			q.append("and a.tilstandId in :tilstandId ");
//		}
//		if (params.getProsesser()!=null&&params.getProsesser().length!=0){
//			q.append("and a.prosessId in :prosessId ");
//		}
//		
//		 Query query= em.createQuery(q.toString())
//			.setParameter("datoFra", params.getDateFra())
//			.setParameter("datoTil", params.getDateTil())
//			.setParameter("kontraktId", params.getValgtKontrakt().getId());
//		if (params.getTilstandId()!=null&&params.getTilstandId().length!=0){
//			query.setParameter("tilstandId", Arrays.asList(params.getTilstandId()));
//		}
//		if (params.getProsesser()!=null&&params.getProsesser().length!=0){
//			query.setParameter("prosessId", Arrays.asList(params.getProsesser()));
//		}
//		List<Avvik> resultList = query.getResultList();
//		
//		//TODO optimaliser slik at spørringen inkluderer jointabellen avvikstatus
//		List<Avvik> al = new ArrayList<Avvik>();
//		for (Avvik a : resultList){
//			if (containsStatus(a, params.getAvvikstatus())){
//				a.getSak().getStedfesting().size();
//				a.getAntallBilder();
//				al.add(a);
//			}
//		}
//		return al;
	}
	
	public List<AvvikSokResult> sokMangler(ManglerQueryParams params){
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("av.guid avvik_guid, ");
		sb.append("av.sak_guid sak_guid, ");
		sb.append("av.refnummer, ");
		sb.append("av.opprettet_av, ");
		sb.append("av.opprettet_dato, ");
		sb.append("av.slettet_av, ");
		sb.append("av.slettet_dato, ");
		sb.append("av.tiltaks_dato, ");
		sb.append("av.prosess_id, ");
		sb.append("av.aarsak_id, ");
		sb.append("av.tilleggsarbeid_flagg, ");
		sb.append("av.etterslep_flagg, ");
		sb.append("pv.fylkesnummer, ");
		sb.append("pv.kommunenummer, ");
		sb.append("pv.veikategori, ");
		sb.append("pv.veistatus, ");
		sb.append("pv.veinummer, ");
		sb.append("pv.hp, ");
		sb.append("pv.meter, ");
		sb.append("pv.kjorefelt, ");
		sb.append("av.beskrivelse, ");
		sb.append("(select count ( * ) ");
		sb.append("                   from avvik_bilde_xv ab ");
		sb.append("where ab.avvik_guid = av.guid) ");
		sb.append("                   antall_bilder ");
		sb.append("from           sak_xv s ");
		sb.append("join      avvik_xv av ");
		sb.append("on av.sak_guid = s.guid ");
		sb.append("        left join dfu_kontrakt dk ");
		sb.append("on dk.kontrakt_id = s.kontrakt_id ");
		sb.append("and dk.dfu_id = s.pda_dfu_ident ");
		sb.append("left join punktstedfest_xv psf ");
		sb.append("on psf.avvik_guid = av.guid ");
		sb.append("left join punktveiref_xv pv ");
		sb.append("on pv.stedfest_guid = psf.guid ");
		sb.append("left join avviktilstand t ");
		sb.append("on av.tilstand_id = t.id ");
		sb.append("left join avvikaarsak avaa ");
		sb.append("on avaa.id = av.aarsak_id ");
		sb.append("left join hendelse_xv h ");
		sb.append("on h.sak_guid = s.guid ");
		sb.append("left join forsikringsskade_xv fs ");
		sb.append("on fs.sak_guid = s.guid ");
		sb.append("where nvl (pv.fra_dato, trunc (sysdate)) = (select nvl (max (fra_dato), trunc (sysdate)) ");
		sb.append("from punktveiref_xv ");
		sb.append("where stedfest_guid = psf.guid) ");
		sb.append("and avvik_guid in( ");
		if (params.getAlle()){
			sb.append(getQueryAlle());
		}else{
			sb.append(getQueryAktive());
		}
		sb.append(") ");
		sb.append(" order by av.opprettet_dato desc");
		
		Query q = em.createNativeQuery(sb.toString())
		.setParameter("kontraktId", params.getDriftkontrakt().getId());
		
        NativeQueryWrapper<AvvikSokResult> wrapper = new NativeQueryWrapper<AvvikSokResult>(q, AvvikSokResult.class, AvvikSokResult.getPropertiesArrayMangler());
        wrapper.setDateMask("yyyy-MM-dd HH:mm:ss");

		return wrapper.getWrappedList();
		
	}
	private String getQueryAktive(){
		StringBuilder sb = new StringBuilder();
		sb.append("select avvik.guid ");
		sb.append("from      sak ");
		sb.append("join avvik ");
		sb.append("on avvik.sak_guid = sak.guid ");
		sb.append("where sak.kontrakt_id = #kontraktId ");
		sb.append("and avvik.tiltaks_dato < sysdate ");
		sb.append("and lukket_dato is null ");
		return sb.toString();
	}
	private String getQueryAlle(){
		StringBuilder sb = new StringBuilder();
		sb.append("select avvik.guid ");
		sb.append("from      sak ");
		sb.append("join avvik ");
		sb.append("on avvik.sak_guid = sak.guid ");
		sb.append("where sak.kontrakt_id = #kontraktId ");
		sb.append("and avvik.tiltaks_dato < sysdate ");
		sb.append("and nvl(lukket_dato, sysdate + 1) > avvik.tiltaks_dato ");
		return sb.toString();
	}
	private boolean containsStatus(Avvik avvik, List<String> status){
		if (status==null||status.isEmpty())
			return true;
		
		AvvikStatus as = avvik.getStatus();
		if (as!=null){
			AvvikstatusType type = as.getAvvikstatusType();
			if (type!=null){
				if (status.contains(type.getStatus())){
					return true;
				}
			}
		}
		return false;
	}
	
	/** {@inheritDoc} */
	public Avvik hentAvvik(String guid){
		Avvik avvik = em.find(Avvik.class, guid);
		BeanHelper.fetchAndRefresh(em, avvik, "bilder", "statuser");
		avvik.setNewEntity(false);
		avvik.setMangel(mangelSjekk(avvik.getSak().getKontrakt(), avvik));
		return avvik;
	}
	
	/** {@inheritDoc} */
	public Avvik hentAvvik(Long refnummer){
		try{
			Avvik avvik = (Avvik)em.createNamedQuery(Avvik.QUERY_HENT_MED_REFNUMMER).setParameter("refnummer", refnummer).getSingleResult();
			BeanHelper.fetchAndRefresh(em, avvik, "bilder", "statuser");
			avvik.setNewEntity(false);
			avvik.setMangel(mangelSjekk(avvik.getSak().getKontrakt(), avvik));
		return avvik;
		} catch (NoResultException e){
			return null;
		}
	}
	/** {@inheritDoc} */
	public List<AvvikstatusType> hentAlleStatusTyper() {
		Query query = em.createNamedQuery(AvvikstatusType.QUERY_FIND_ALL);
		return query.getResultList();
	}
	/** {@inheritDoc} */
	public List<Avvikaarsak> hentAlleAvvikaarsaker(){
		return em.createNamedQuery(Avvikaarsak.QUERY_FIND_VALID).getResultList();
	}
	/** {@inheritDoc} */
	public List<Avviktilstand> hentAlleAvviktilstander(){
		return em.createNamedQuery(Avviktilstand.QUERY_FIND_ALL).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektType> hentAlleObjektTyper(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektType o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektAvvikType> hentAlleObjektAvvikTyper(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektAvvikType o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektAvvikKategori> hentAlleObjektAvvikKategorier(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektAvvikKategori o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ObjektStedTypeAngivelse> hentAlleObjektStedTypeAngivelser(Long prosessId, Long kontraktId){
		return em.createQuery("SELECT o from ObjektStedTypeAngivelse o where o.valgbarFlagg = 1 and o.prosess.id = " + prosessId+" and o.kontrakt.id="+kontraktId).getResultList();
	}
	/** {@inheritDoc} */
	public List<ArbeidType> hentAlleArbeidTyper(){
		return em.createNamedQuery(ArbeidType.QUERY_FIND_ALL).getResultList();
	}

	public void persistPunktstedfest(Punktstedfest stedfest){
		em.merge(stedfest);
	}
	/** {@inheritDoc} */
	public Avvik persistAvvik(Avvik avvik, String brukerSign) {
		
		//slett bilder som evt. har blitt fjernet, tar også med bilde-entiteten dersom bildet ikke er tilknyttet skred, hendelse, eller skade.
		Query q = em.createNamedQuery(Avvik.QUERY_GET_BILDER_UTENRELASJONER).setParameter(1, avvik.getGuid());
		List<Bilde> oldBilder = q.getResultList();
		List<AvvikBilde> bilder = avvik.getBilder();
		List<Bilde> current = new ArrayList<Bilde>();
		for (AvvikBilde ab:bilder){
			current.add(ab.getBilde());
		}

		BeanHelper.deleteOldEntities(em, oldBilder, current);
		
		
		if (avvik.getStedfesting()!=null){
			Punktveiref veiref = avvik.getStedfesting().getVeiref();
			if (veiref.getFraDato()==null){
				//NUKE ME
				avvik.getStedfesting().removePunktveiref(veiref);
			}
		}
		
		if (avvik.isNewEntity()){
			avvik.setOpprettetAv(brukerSign);
			avvik.setOpprettetDato(Clock.now());
			em.persist(avvik);
		}else{
			avvik.setEndretAv(brukerSign);
			avvik.setEndretDato(Clock.now());
			
			AvvikStatus status = avvik.getStatus();
			AvvikstatusType type = status.getAvvikstatusType();
			if (type.getStatus().equals(AvvikstatusType.LUKKET_STATUS)&&avvik.getLukketDato()==null){
				//legg på status=gjenåpnet hvis status=lukket og lukket_dato er null
				AvvikstatusType gjenapnet = (AvvikstatusType)em.createQuery("SELECT a FROM AvvikstatusType a where a.status=:gjenapnet").setParameter("gjenapnet", AvvikstatusType.GJENAPNET_STATUS).getSingleResult();
				avvik.addStatus(createAvvikStatus(avvik, gjenapnet, brukerSign, Clock.now()));
			}
			else if (avvik.getLukketDato()!=null&&!(type.getStatus().equals(AvvikstatusType.LUKKET_STATUS))){
				//legg på status=lukket hvis lukketdato eksisterer og status ikke er=lukket
				AvvikstatusType lukket = (AvvikstatusType)em.createQuery("SELECT a FROM AvvikstatusType a where a.status=:lukket").setParameter("lukket", AvvikstatusType.LUKKET_STATUS).getSingleResult();
				Date lukketDato = Clock.now();
				avvik.addStatus(createAvvikStatus(avvik, lukket, brukerSign, lukketDato));
				avvik.setLukketDato(lukketDato);
			}
			
			avvik = em.merge(avvik);
		}
		return avvik;
	}
	
	private AvvikStatus createAvvikStatus(Avvik avvik, AvvikstatusType type, String brukerSign, Date dato){
		AvvikStatus as = new AvvikStatus();
		as.setAvvik(avvik);
		as.setAvvikstatusType(type);
		as.setOpprettetAv(brukerSign);
		as.setOpprettetDato(dato);
		return as;
	}
	/** {@inheritDoc} */
	public Boolean mangelSjekk(Driftkontrakt kontrakt, Avvik avvik, Date dato){
        if(avvik.getTiltaksDato() == null || avvik.getSlettetDato() != null) {
            return false;
        }

        Date limit = getNextDay(avvik.getTiltaksDato());

        if(limit.after(new Date())) {
            return false;
        }

        if(avvik.getLukketDato() != null && avvik.getLukketDato().before(avvik.getTiltaksDato())) {
            return false;
        }

        if((avvik.getEtterslepFlagg() || avvik.getTilleggsarbeid()) && !avvik.getTrafikksikkerhetFlagg()) {
            return false;
        }

        if(!betweenDates(avvik.getOpprettetDato(), kontrakt.getGyldigFraDato(), kontrakt.getGyldigTilDato())) {
            return false;
        }
        return true;
	}

    private boolean betweenDates(Date date, Date fromDate, Date toDate) {
        return date.after(fromDate) && date.before(toDate);
    }

    private Date getNextDay(Date date) {
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }

    /** {@inheritDoc} */
	public Boolean mangelSjekk(Driftkontrakt kontrakt, Avvik avvik){
		return mangelSjekk(kontrakt, avvik, Clock.now());
	}
	/** {@inheritDoc} */
	public Boolean mangelSjekk(String avvikGuid){
		Avvik avvik = hentAvvik(avvikGuid);
		return mangelSjekk(avvik.getSak().getKontrakt(), avvik);
	}
	/** {@inheritDoc} */
	public Avvik deleteAvvik(Avvik avvik, String brukerSign) {
		avvik.setSlettetAv(brukerSign);
		avvik.setSlettetDato(Clock.now());
		avvik = em.merge(avvik);
		return avvik;
	}
	public List<AvvikSokResult> deleteAvvik(List<AvvikSokResult> avvikList, String brukerSign){
		List<AvvikSokResult> notDeleted = new ArrayList<AvvikSokResult>();
		for (AvvikSokResult s:avvikList){
			Avvik avvik = em.find(Avvik.class, s.getGuid());
			if (isAvvikSlettbar(avvik)){
				avvik.setSlettetAv(brukerSign);
				avvik.setSlettetDato(Clock.now());
				em.merge(avvik);
			}else{
				notDeleted.add(s);
			}
		}
		
		return notDeleted;
	}
	private boolean isAvvikSlettbar(Avvik avvik){
		Long statusId = avvik.getStatus().getAvvikstatusType().getId();
		if  (statusId.equals(Long.valueOf(15))){//lukket avvik
			return false;
		}
		Sak sak = avvik.getSak();
		if (antallAvvikSomIkkeErSlettet(sak)>1)
			return true;
		
		if ((sak.getHendelse()!=null && sak.getHendelse().getSlettetDato()==null) ||
			(sak.getForsikringsskade()!=null && sak.getForsikringsskade().getSlettetDato()==null))
			return false;
		
		return true;
	}
	private int antallAvvikSomIkkeErSlettet(Sak sak){
		int c = 0;
		for (Avvik av:sak.getAvvik()){
			if (av.getSlettetDato()==null)
				c++;
		}
		return c;
	}
	/** {@inheritDoc} */
	public Avvik restoreAvvik(Avvik avvik, String brukerSign) {
		avvik.setSlettetAv(null);
		avvik.setSlettetDato(null);
		avvik.setEndretAv(brukerSign);
		avvik.setEndretDato(Clock.now());
		avvik = em.merge(avvik);
		return avvik;
	}

	@Override
	public Avvik leggTilKommentar(String avvikGuid, String kommentar, String brukerSign) {
		String dato = MipssDateFormatter.formatDate(Clock.now());
		StringBuilder sb = new StringBuilder();
		sb.append(dato);
		sb.append(" ");
		sb.append(brukerSign);
		sb.append(": ");
		sb.append(kommentar);
		
		Avvik avvik = em.find(Avvik.class, avvikGuid);
		if (avvik==null){
			Avvik a = new Avvik();
			a.setBeskrivelse(sb.toString());
			return a;
		}
		String beskrivelse = avvik.getBeskrivelse();
		
		StringBuilder sb2 = new StringBuilder();
		if (beskrivelse!=null){
			sb2.append(beskrivelse);
			sb2.append("\n");
		}
		sb2.append(sb.toString());
		
		avvik.setBeskrivelse(sb2.toString());
		em.merge(avvik);
		return avvik;
	}
	
	public List<AvvikAvstand> sokAvvikINarheten(Avvik avvik, Long driftkontraktId){
		
		Double snappetX = avvik.getStedfesting().getSnappetX();
		Double snappetY = avvik.getStedfesting().getSnappetY();
		
		Double xx = avvik.getStedfesting().getX();
		Double yy = avvik.getStedfesting().getY();
		
		Double x = snappetX==null?xx:snappetX;
		Double y = snappetX==null?yy:snappetY;
		
		Double maxAvstand = Double.valueOf(konfig.hentEnForApp("studio.mipss.field", "avstandAvvikINarheten").getVerdi());
		Double x1 = x-maxAvstand;
		Double x2 = x+maxAvstand;
		Double y1 = y-maxAvstand;
		Double y2 = y+maxAvstand;
		
		StringBuilder sb = new StringBuilder();
		sb.append("select av.guid, av.sak_guid, av.slettet_dato, ");
		sb.append("nvl(psf.snappet_x, psf.x) as vx,  ");
		sb.append("nvl(psf.snappet_y, psf.y) as vy, ");
		sb.append("av.beskrivelse ");
		sb.append("from avvik_xv av ");
		sb.append("left join punktstedfest_xv psf on psf.avvik_guid = av.guid ");
		sb.append("left join sak_xv s on s.guid = av.sak_guid ");
		sb.append("where  ");
		sb.append("nvl(psf.snappet_x, psf.x) >= ?1  ");
		sb.append("and nvl(psf.snappet_x, psf.x) <= ?2 ");
		sb.append("and nvl(psf.snappet_y, psf.y) >= ?3 ");
		sb.append("and nvl(psf.snappet_y, psf.y) <= ?4 ");
		sb.append("and s.kontrakt_id = ?5 ");
		sb.append("and av.guid <> ?6");

		Query q = em.createNativeQuery(sb.toString())
			.setParameter(1, x1)
			.setParameter(2, x2)
			.setParameter(3, y1)
			.setParameter(4, y2)
			.setParameter(5, driftkontraktId)
			.setParameter(6, avvik.getGuid());
		
		Point orig = new Point();
		orig.setLocation(x, y);
		List<AvvikAvstand> avvikList = new NativeQueryWrapper<AvvikAvstand>(q, AvvikAvstand.class, "guid", "sakGuid", "slettetDato", "x", "y", "beskrivelse").getWrappedList();
		for (AvvikAvstand a:avvikList){
			Avvik foundAvvik = em.find(Avvik.class, a.getGuid());
			Point avvikLocation = new Point();
			Double ax = foundAvvik.getStedfesting().getSnappetX();
			Double ay = foundAvvik.getStedfesting().getSnappetY();
			if (ax==null||ay==null){
				ax = foundAvvik.getStedfesting().getX();
				ay = foundAvvik.getStedfesting().getY();
			}
			
			avvikLocation.setLocation(ax, ay);
			
			double avstand = orig.distance(avvikLocation);
			a.setAvstand(avstand);
			
			if (foundAvvik.getProsess()!=null){
				a.setProsessKode(foundAvvik.getProsess().getProsessKode());
			}
			a.setRefnummer(foundAvvik.getRefnummer());
			a.setRegistrertDato(foundAvvik.getOpprettetDato());
			a.setRegistrertAv(foundAvvik.getOpprettetAv());
			if (foundAvvik.getStatus()!=null){
				a.setStatus(foundAvvik.getStatus().getTextForGUI());
			}
			if (foundAvvik.getVeiref()!=null){
				a.setVeireferanse(foundAvvik.getProdHpGUIText());
			}
		}
		
		
		
		return avvikList;
	}

    public void setEm(EntityManager em) {
        this.em = em;
    }
}
