package no.mesta.mipss.mipssfelt;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.dokarkiv.Dokument;

@Remote
/**
 * Tjeneste for å hente siste versjon av android-klienten fra databasen.
 */
public interface AndroidUpdateService {
	public static final String BEAN_NAME = "AndroidUpdateServiceBean";
	
	/**
	 * Henter siste versjon av android-klienten
	 * @return
	 */
	Dokument getLatest();
	/**
	 * Henter siste versjon av brukermanualen
	 * @return
	 */
	Dokument getLatestManual();
	
	/**
	 * Oppdaterer android-klient-lob i databasen
	 * @param lob
	 * @param brukerSign
	 */
	void update(byte[] lob, String brukerSign);
	
	/**
	 * Oppdaterer brukermanual for android-klient i databasen
	 * @param lob
	 * @param brukerSign
	 */
	void updateBrukermanual(byte[] lob, String brukerSign);
}
