package no.mesta.mipss.mipssfelt;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Avvik;

import javax.ejb.Remote;
import java.util.Date;

@Remote
public interface AvvikServiceLocal {
	/**
	 * Sjekker om et avvik er en mangel på angitt dato
	 * @param dato
	 * @return
	 */
	Boolean mangelSjekk(Driftkontrakt kontrakt, Avvik avvik, Date dato);
	
	/**
	 * Sjekker om avviket er en mangel pr Clock.now()
	 * 
	 * @return
	 */
	Boolean mangelSjekk(Driftkontrakt kontrakt, Avvik avvik);
}
