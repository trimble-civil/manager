package no.mesta.mipss.yrrs;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.mesta.mipss.mipssfelt.AndroidUpdateService;
import no.mesta.mipss.persistence.dokarkiv.Dokument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class UpdateResource extends HttpServlet{
	@EJB
	private AndroidUpdateService bean;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException{
		if (!validateRequest(req)){
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		try{
			handleRequest(req, resp);
		} catch (Exception e){
			e.printStackTrace();
			throw new ServletException(e);
		} 
		
    }
	
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		
		if (req.getParameterMap().containsKey("apk")){
			sendApk(resp);	
		}
		else if (req.getParameterMap().containsKey("brukermanual")){
			sendBrukermanual(resp);
		}else{
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	private void sendApk(HttpServletResponse resp) throws IOException {
		Dokument apk = bean.getLatest();
		byte[] latest = apk.getLob();
		String navn = apk.getNavn()+"."+apk.getDokformat().getNavn().toLowerCase();
		resp.setContentType("application/vnd.android.package-archive"); 
		resp.setHeader("Content-Disposition", "attachment; filename="+navn);
		resp.setContentLength(latest.length);
		ServletOutputStream os = resp.getOutputStream();
		os.write(latest);
		os.close();
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
	private void sendBrukermanual(HttpServletResponse resp) throws IOException {
		Dokument manual = bean.getLatestManual();
		byte[] latest = manual.getLob();
		String navn = manual.getNavn()+"."+manual.getDokformat().getNavn().toLowerCase();
		resp.setContentType("application/pdf"); 
		resp.setHeader("Content-Disposition", "attachment; filename="+navn);
		resp.setContentLength(latest.length);
		ServletOutputStream os = resp.getOutputStream();
		os.write(latest);
		os.close();
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	private boolean validateRequest(HttpServletRequest req){
		boolean validated=true;
		
		return validated;
	}
}
