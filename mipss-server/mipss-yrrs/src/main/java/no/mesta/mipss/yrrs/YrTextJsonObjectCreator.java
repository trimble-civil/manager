package no.mesta.mipss.yrrs;

import java.util.Calendar;
import java.util.List;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.klimadata.yr.WeatherService;
import no.mesta.mipss.klimadata.yr.WeatherText;
import no.mesta.mipss.klimadata.yr.export.TextForecast;
import no.mesta.mipss.klimadata.yr.export.Weather;
import no.mesta.mipss.konfigparam.KonfigParam;

import javax.ejb.EJB;
import javax.ejb.Stateless;

public class YrTextJsonObjectCreator {
	private final Weather weather;
	private PropertyResourceBundleUtil bundle = PropertyResourceBundleUtil.getPropertyBundle("yrtexts");
	private WeatherService service;
	private String[] days = {bundle.getGuiString("sondag"), "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", bundle.getGuiString("lordag")};
	private final String lat;
	private final String lon;

	public YrTextJsonObjectCreator(KonfigParam konfig, Weather weather, double lat, double lon){
		service = new WeatherService(konfig);
		this.weather = weather;
		this.lat = String.valueOf(lat);
		this.lon = String.valueOf(lon);
	}
	
	public void run(){
		List<WeatherText> w = service.getTextForecast(lat, lon);
		for (WeatherText wt:w){
			TextForecast t = new TextForecast();
			Calendar c = Calendar.getInstance();
			c.setTime(wt.getDateFrom());
			int from = c.get(Calendar.DAY_OF_WEEK);
			c.setTime(wt.getDateTo());
			int to = c.get(Calendar.DAY_OF_WEEK);
			
			//yr behandler kl 00:00 som datoen f�r i UTC-tid i dato-til feltet
			if (c.get(Calendar.HOUR_OF_DAY)<10){
				to--;
			}
			from--;
			to--;
			if (to==-1)//s�ndag-1==-1 endre til l�rdag(6)
				to=6;
			
			t.setTitle(getDayOfWeekText(from, to));
			t.setContent(wt.getLocation()+". "+wt.getForecast());
			weather.getTextForecasts().add(t);
		}

	}
	private String getDayOfWeekText(int day1, int day2){
		if (day1==day2){
			return days[day1];
		}
		if (Math.abs(day2-day1)==1||(day2==6&&day1==0)){
			return days[day1]+" og "+days[day2];
		}else
			return days[day1]+" til "+days[day2];
	}
}
