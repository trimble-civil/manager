package no.mesta.mipss.yrrs;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

@SuppressWarnings("serial")
public class TMSResource extends HttpServlet{
	private static String TILE_URL_TEMPLATE = "http://10.60.71.57:8080/geoserver/gwc/service/tms/1.0.0/MestaGWC@EPSG:32633@png/%d/%d/%d.png";
	
	private Integer z;
	private Integer x;
	private Integer y;
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		decomposeURI(req);
		String url = String.format(getTileUrlTemplate(), z, x, y);
		
		byte[] tile = getTile(url);
		resp.setContentType("image/png"); 
		ServletOutputStream os = resp.getOutputStream();
		os.write(tile);
		os.close();
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	protected String getTileUrlTemplate(){
		return TILE_URL_TEMPLATE;
	}
	private byte[] getTile(String url) throws ClientProtocolException, IOException{
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet method = new HttpGet(url);
		HttpResponse response = client.execute((HttpUriRequest) method);
		return IOUtils.toByteArray(response.getEntity().getContent());
		
	}
	private void decomposeURI(HttpServletRequest request) {
		String path = request.getRequestURI();
		String y = path.substring(path.lastIndexOf('/')+1, path.lastIndexOf(".png"));
		path = path.substring(0, path.lastIndexOf('/'));
		String x = path.substring(path.lastIndexOf('/')+1);
		path = path.substring(0, path.lastIndexOf('/'));
		String z = path.substring(path.lastIndexOf('/')+1);
		path = path.substring(0, path.lastIndexOf('/'));
		this.z = Integer.valueOf(z);
		this.x = Integer.valueOf(x);
		this.y = Integer.valueOf(y);
	}
	
	public static void main(String[] args) {
		int zoomLevel = 0;
		int y = 0;
		int x = 0;
		int ty = (int)(Math.pow(2, zoomLevel) - y - 1);
		System.out.println(ty);
	}
}
