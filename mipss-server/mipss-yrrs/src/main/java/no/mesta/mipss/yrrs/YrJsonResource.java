package no.mesta.mipss.yrrs;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import no.mesta.mipss.klimadata.LatLongUtils;
import no.mesta.mipss.klimadata.wsklima.Klimadata;
import no.mesta.mipss.klimadata.yr.export.Forecast;
import no.mesta.mipss.klimadata.yr.export.HourForecast;
import no.mesta.mipss.klimadata.yr.export.Weather;
import no.mesta.mipss.konfigparam.KonfigParam;

/**
 * Servlet som genererer en JSON med værdata fra yr. 
 * Inneholder 6-timers varsel, metrologens tekstvarsel samt en BASE64 kodet graf
 * <br><br>
 * Parametere:
 * <p>
 * <b>width:</b> bredden bå grafen som skal genereres<br>
 * <b>height:</b> høyden på grafen som skal genereres<br> 
 * <b>lat:</b> koordinat som sendes til yr<br>
 * <b>lon:</b> koordinat som sendes til yr<br>
 * <b>span:</b> antall timer fremover i tid grafen skal vise. Kan vise opptil 48 timer fremover avhengig av dekningen til dataene fra yr.no.<br> 
 * </p>
 * <p>
 * Det er opp til klienten å beregne bredden på grafen. Dersom grafen viser fra 0-18 timer passer det med <code>width=300</code>.
 * </p>
 * <p>
 * eksempel på url:</br><b>
 * 	http://localhost:8080/yrrs/json?lat=59.7051&lon=10.5124&width=600&height=285&span=48</b>
 * </p>
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class YrJsonResource extends HttpServlet{
	@EJB
	private Klimadata k;
	@EJB
	private KonfigParam konfig;
	private double lat;
	private double lon;
	private int width;
	private int height;
	private int span;
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException{
		if (!validateRequest(req)){
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		try{
			handleRequest(req, resp);
		} catch (Exception e){
			e.printStackTrace();
			throw new ServletException(e);
		} 
    }
	
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		initParams(req);
		Weather weather = new Weather();
		YrTextJsonObjectCreator text = new YrTextJsonObjectCreator(konfig, weather, lat, lon);
		YrDataJsonObjectCreator data = new YrDataJsonObjectCreator(konfig, weather, lat, lon, span, width, height);
		text.run();
		data.run();
		
		resp.setContentType("application/json"); 
		ServletOutputStream os = resp.getOutputStream();
		
		PrintWriter pw = new PrintWriter(os);
		JSONObject jsonObj = JSONObject.fromObject(weather);
		String json = jsonObj.toString(2);
		pw.write(json);
		pw.close();
	}
	
	/**
	 * Henter ut verdiene som trengs fra request
	 * @param req
	 */
	private void initParams(HttpServletRequest req){
		String latStr = req.getParameter("lat");
		String lonStr = req.getParameter("lon");
		String spanStr = req.getParameter("span");
		String wStr = req.getParameter("width");
		String hStr = req.getParameter("height");
		lat = Double.valueOf(latStr);
		lon = Double.valueOf(lonStr);
		if (spanStr==null){
			span = Integer.valueOf(12);
		}else{
			span = Integer.valueOf(spanStr);
		}
		
		if (wStr==null||hStr==null){
			width = 500;
			height = 200;
		}else{
			width = Integer.valueOf(wStr);
			height = Integer.valueOf(hStr);
		}
	}
	/**
	 * Sjekker at påkrevde felter er til stede i request.
	 * @param req
	 * @return
	 */
	private boolean validateRequest(HttpServletRequest req){
		boolean validated=false;
		try{
			Double.parseDouble(req.getParameter("lat"));
			Double.parseDouble(req.getParameter("lon"));
			validated = true;
		} catch (NumberFormatException e){
			e.printStackTrace();
		} catch (Throwable t){
			t.printStackTrace();
		}
		return validated;
	}
}
