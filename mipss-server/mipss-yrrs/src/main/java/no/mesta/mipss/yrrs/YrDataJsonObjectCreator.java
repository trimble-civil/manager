package no.mesta.mipss.yrrs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.imageio.ImageIO;

import no.mesta.mipss.klimadata.yr.TimeSpan;
import no.mesta.mipss.klimadata.yr.WeatherChart;
import no.mesta.mipss.klimadata.yr.WeatherData;
import no.mesta.mipss.klimadata.yr.WeatherForecast;
import no.mesta.mipss.klimadata.yr.WeatherService;
import no.mesta.mipss.klimadata.yr.export.Forecast;
import no.mesta.mipss.klimadata.yr.export.HourForecast;
import no.mesta.mipss.klimadata.yr.export.Weather;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.persistence.Clock;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

public class YrDataJsonObjectCreator {
	private List<WeatherForecast> list;
	private final Weather weather;
	private final WeatherService service;
	private final String lat;
	private final String lon;
	private final int span;
	private final int width;
	private final int height;

	public YrDataJsonObjectCreator(KonfigParam konfig, Weather weather, double lat, double lon, int span, int width, int height){
		service = new WeatherService(konfig);
		this.weather = weather;
		this.lat = String.valueOf(lat);
		this.lon = String.valueOf(lon);
		this.span = span;
		this.width = width;
		this.height = height;
	}
	
	public void run(){
		long start = System.currentTimeMillis();
		list = service.getWeatherDataNewVersion(lat, lon);

		getWeatherForJSON();
		getChart();
	}
	
	private void getChart(){
		
		Date start = list.get(0).getFirstDate();
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		c.add(Calendar.HOUR_OF_DAY, span);
		
		Date end = c.getTime();
		List<WeatherData> cropped = new ArrayList<WeatherData>();
		
		for (WeatherForecast d:list){
			Date date = d.getFirstDate();
			if (date.before(end))
				cropped.add(d.getWeatherDataHour());
		}
		WeatherChart ch = new WeatherChart(null, cropped);
		ch.setSize(width, height);
		ch.setGridColor(new Color(238, 238,238));
		ch.setAlternateColor(true);
		ch.setDaySeparatorColor(Color.darkGray);
		ch.setMargins(0);
		BufferedImage image = new BufferedImage(width,height, BufferedImage.TYPE_INT_ARGB);
		image.getGraphics().setColor(Color.white);
		image.getGraphics().fillRect(0, 0, width, height);
		ch.paint(image.getGraphics());
		String imgString = Base64.encodeBase64String(imageToBytes(image));
		weather.setMeteogram(imgString);
	}
	
	private void getWeatherForJSON(){
		int[] hours = new int[]{0,6,12,18};
		Calendar c = new GregorianCalendar();
		c.setTime(Clock.now());
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		
		int now = c.get(Calendar.HOUR_OF_DAY);
		
		int startIndex = 0;
		for (int i=0;i<hours.length;i++){
			if (now<hours[i]){
				startIndex = i;
				break;
			}
		}
		weather.setFrom(c.getTime());
		Date last = null;
		for (int i=0;i<3;i++){
			Forecast forecast = new Forecast();
			forecast.setDate(c.getTime());
			last = c.getTime();
			for (;startIndex<hours.length;startIndex++){
				c.set(Calendar.HOUR_OF_DAY, hours[startIndex]);
				
				List<WeatherForecast> weatherDataForDay = getWeatherDataForDay(c.getTime(), list);
				
				WeatherForecast weatherDataForHour = getWeatherDataForHour(c.getTime(), weatherDataForDay);
				WeatherData hour = weatherDataForHour.getWeatherDataOfSpan(TimeSpan.HOUR);
				WeatherData hour6 = weatherDataForHour.getWeatherDataOfSpan(TimeSpan.HOUR6);
				if (hour==null||hour6==null)
					continue;
				Date dateFrom = hour6.getDateFrom();
				Date dateTo = hour6.getDateTo();
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateFrom);
				int hourFrom = cal.get(Calendar.HOUR_OF_DAY);
				cal.setTime(dateTo);
				int hourTo = cal.get(Calendar.HOUR_OF_DAY);
				
				
				HourForecast hf = new HourForecast();
				hf.setFromHour(hourFrom);
				hf.setToHour(hourTo);
				hf.setHumidity(hour.getHumidity());
				hf.setPrecipitation(hour6.getPrecipitation());
				hf.setTemperature(hour.getDegree());
				hf.setDewPoint(hour.getDewPoint());
				hf.setBeaufort(hour.getBeaufort());
				hf.setWeatherCode(hour6.getWeatherId());
				hf.setWindSpeed(hour.getWindSpeed());
				hf.setWindDirection(hour.getWindDirection());
				forecast.getHourForecast().add(hf);
			}
			startIndex =0;
			weather.getForecasts().add(forecast);
			c.add(Calendar.DAY_OF_MONTH, 1);
		}
		weather.setTo(last);
	}
	private byte[] imageToBytes(Image image) {
    	if (image instanceof RenderedImage){
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        try {
	            ImageIO.write((RenderedImage)image, "png", baos);
	        } catch (IOException e) {
	            throw new IllegalStateException(e.toString());
	        } finally {
	        	IOUtils.closeQuietly(baos);
	        }
	        
	        return baos.toByteArray();
    	}
    	else{
    		ByteArrayOutputStream bas = new ByteArrayOutputStream();
    		BufferedImage scaledBuff = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    		Graphics2D bufImageGraphics = scaledBuff.createGraphics();
    		bufImageGraphics.drawImage(image, 0, 0, null);
    		try {
    			ImageIO.write(scaledBuff, "png", bas);
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			IOUtils.closeQuietly(bas);
    		}
    		return bas.toByteArray();
    	}
    }
	private WeatherForecast getWeatherDataForHour(Date hour, List<WeatherForecast> list){
		for (WeatherForecast wl:list){
			if (wl.getFirstDate().equals(hour))
				return wl;
		}
		return null;
	}
	
	private List<WeatherForecast> getWeatherDataForDay(Date day, List<WeatherForecast> list){
		List<WeatherForecast> wd = new ArrayList<WeatherForecast>();
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		int d = c.get(Calendar.DAY_OF_MONTH);
		
		for (WeatherForecast wl:list){
			c.setTime(wl.getFirstDate());
			int dd = c.get(Calendar.DAY_OF_MONTH);
			if (dd==d){
				wd.add(wl);
			}
		}
		return wd;
	}
}
