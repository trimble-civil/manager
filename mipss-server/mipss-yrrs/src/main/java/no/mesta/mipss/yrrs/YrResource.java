package no.mesta.mipss.yrrs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.mesta.mipss.klimadata.wsklima.Klimadata;
import no.mesta.mipss.klimadata.yr.WeatherChart;
import no.mesta.mipss.klimadata.yr.WeatherData;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet som genererer en graf med data fra yr.no basert på gitte parametere i requestobjektet
 * <p>
 * <b>width:</b> bredden bå bildet som skal genereres<br>
 * <b>height:</b> høyden på bildet som skal genereres<br> 
 * <b>lat:</b> koordinat som sendes til yr<br>
 * <b>lon:</b> koordinat som sendes til yr<br>
 * <b>span:</b> antall timer fremover i tid grafen skal vise. Kan vise opptil 48 timer fremover avhengig av dekningen til dataene fra yr.no.<br> 
 * </p>
 * <p>
 * Det er opp til klienten å beregne bredden på grafen. Dersom grafen viser fra 0-18 timer passer det med <code>width=300</code>.
 * </p>
 * <p>
 * eksempel på url:</br><b>
 * 	http://webappst.vegprod.no/yrrs/?lat=60&lon=10&width=300&height=285&span=12</b>
 * </p>
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class YrResource extends HttpServlet{
	@EJB
	private Klimadata k;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException{
		if (!validateRequest(req)){
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		try{
			handleRequest(req, resp);
		} catch (Exception e){
			e.printStackTrace();
			throw new ServletException(e);
		} 
		
    }
	
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		String latStr = req.getParameter("lat");
		String lonStr = req.getParameter("lon");
		String spanStr = req.getParameter("span");
		String wStr = req.getParameter("width");
		String hStr = req.getParameter("height");
		
		double lat = Double.valueOf(latStr);
		double lon = Double.valueOf(lonStr);
		int span = -1;
		if (spanStr==null){
			span = Integer.valueOf(12);
		}else{
			span = Integer.valueOf(spanStr);
		}
		
		int width  = -1;
		int height = -1;
		if (wStr==null||hStr==null){
			width = 500;
			height = 200;
		}else{
			width = Integer.valueOf(wStr);
			height = Integer.valueOf(hStr);
		}
		
		byte[] chart = generateChart(lat, lon, span, width, height);
		resp.setContentType("image/png"); 
		ServletOutputStream os = resp.getOutputStream();
		
		os.write(chart);
		os.close();
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
	private byte[] generateChart(double lat, double lon, int span, int width, int height) {
		List<WeatherData> weatherDataFromYr=null;
		try{
			weatherDataFromYr = k.getWeatherDataFromYrLatLong(lat, lon);
		} catch (Exception e){
			throw new RuntimeException(e);
		}
		Date start = weatherDataFromYr.get(0).getDateFrom();
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		c.add(Calendar.HOUR_OF_DAY, span);
		
		Date end = c.getTime();
		List<WeatherData> cropped = new ArrayList<WeatherData>();
		for (WeatherData d:weatherDataFromYr){
			Date date = d.getDateFrom();
			if (date.before(end))
				cropped.add(d);
		}
		WeatherChart ch = new WeatherChart(null, cropped);
		ch.setSize(width, height);
		ch.setGridColor(Color.gray);
		ch.setDaySeparatorColor(Color.darkGray);
		ch.setMargins(0);
		BufferedImage image = new BufferedImage(width,height, BufferedImage.TYPE_INT_ARGB);
		image.getGraphics().setColor(Color.white);
		image.getGraphics().fillRect(0, 0, width, height);
		ch.paint(image.getGraphics());
		
		return imageToBytes(image);
	}

    public static byte[] imageToBytes(Image image) {
    	if (image instanceof RenderedImage){
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        try {
	            ImageIO.write((RenderedImage)image, "png", baos);
	        } catch (IOException e) {
	            throw new IllegalStateException(e.toString());
	        } finally {
	        	IOUtils.closeQuietly(baos);
	        }
	        
	        return baos.toByteArray();
    	}
    	else{
    		ByteArrayOutputStream bas = new ByteArrayOutputStream();
    		BufferedImage scaledBuff = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    		Graphics2D bufImageGraphics = scaledBuff.createGraphics();
    		bufImageGraphics.drawImage(image, 0, 0, null);
    		try {
    			ImageIO.write(scaledBuff, "png", bas);
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			IOUtils.closeQuietly(bas);
    		}
    		return bas.toByteArray();
    	}
    }
	private boolean validateRequest(HttpServletRequest req){
		boolean validated=false;
		try{
			Double.parseDouble(req.getParameter("lat"));
			Double.parseDouble(req.getParameter("lon"));
			validated = true;
		} catch (NumberFormatException e){
			log.debug("Exception during validate request", e);
			e.printStackTrace();
		} catch (Throwable t){
			log.debug("Throwable during validate request", t);
			t.printStackTrace();
		}
		return validated;
	}
}
