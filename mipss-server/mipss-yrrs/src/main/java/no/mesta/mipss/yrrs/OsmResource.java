package no.mesta.mipss.yrrs;

public class OsmResource extends TMSResource{
	private static String TILE_URL_TEMPLATE = "http://c.tile.openstreetmap.org/%d/%d/%d.png";
	protected String getTileUrlTemplate(){
		return TILE_URL_TEMPLATE;
	}
}
