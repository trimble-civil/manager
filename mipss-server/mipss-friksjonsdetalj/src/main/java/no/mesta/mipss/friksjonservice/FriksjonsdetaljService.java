package no.mesta.mipss.friksjonservice;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kvernetf1.Fore;
import no.mesta.mipss.persistence.kvernetf1.Nedbor;

@Remote
public interface FriksjonsdetaljService {

	public static final String BEAN_NAME = "FriksjonsdetaljBean";
	
	/**
	 * Henter ut alle føre entitetene
	 * @return
	 */
	public List<Fore> getForeList();
	/**
	 * Henter ut alle nedbor entitetene
	 * @return
	 */
	public List<Nedbor> getNedborList();
}
