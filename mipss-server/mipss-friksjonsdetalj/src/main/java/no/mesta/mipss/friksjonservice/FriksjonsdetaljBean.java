package no.mesta.mipss.friksjonservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.kvernetf1.Fore;
import no.mesta.mipss.persistence.kvernetf1.Nedbor;

@Stateless(name = FriksjonsdetaljService.BEAN_NAME, mappedName="ejb/"+FriksjonsdetaljService.BEAN_NAME)
public class FriksjonsdetaljBean implements FriksjonsdetaljService{
	
	@PersistenceContext(unitName = "mipssPersistence")
	EntityManager em;
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Fore> getForeList() {
		Query q = em.createNamedQuery(Fore.QUERY_FIND_ALL);
		return q.getResultList();
	}
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Nedbor> getNedborList() {
		Query q = em.createNamedQuery(Nedbor.QUERY_FIND_ALL);
		return q.getResultList();
	}

}
