package no.mesta.mipss.rodeservice;

public enum RoderapportType {
	OVERLAPPENDE_RODER,
	HULL_RODER,
	RODER_UTENFOR_KONTRAKT
}
