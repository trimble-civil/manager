package no.mesta.mipss.rodeservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

public class OverlappHelper {

	public List<Veinettveireferanse> sjekkOverlappVeinett(List<Veinettveireferanse> rodeVeiref, Veinett v2){
		Map<String, List<RodeVei>> veinett = new HashMap<String, List<RodeVei>>();
		for (Veinettveireferanse v:rodeVeiref){
			RodeVei r = new RodeVei(v);
			List<RodeVei> list = veinett.get(r.getKey());
			if (list==null){
				list = new ArrayList<RodeVei>();
				veinett.put(r.getKey(), list);
			}
			list.add(r);
		}

		List<Veinettveireferanse> overlappList = new ArrayList<Veinettveireferanse>();
		for (Veinettreflinkseksjon vr:v2.getVeinettreflinkseksjonList()){
			for (Veinettveireferanse v:vr.getVeinettveireferanseList()){
				String key = v.getFylkesnummer()+v.getKommunenummer()+v.getVeikategori()+v.getVeistatus()+v.getVeinummer()+"hp"+v.getHp();
				List<RodeVei> list = veinett.get(key);
				if (list==null)
					continue;
				List<Veinettveireferanse> overlapp = compare(list, v);
				if (overlapp!=null){
					for (Veinettveireferanse ref : overlapp) {
						overlappList.add(ref);
					}
				}
			}
		}

		return overlappList;
	}

	public List<Veinettveireferanse> sjekkOverlapp(List<Veinettveireferanse> rodeVeiref1, List<Veinettveireferanse> rodeVeiref2){
		List<Veinettveireferanse> overlappList = new ArrayList<Veinettveireferanse>();
		Map<String, List<RodeVei>> veinett = new HashMap<String, List<RodeVei>>();
		for (Veinettveireferanse v:rodeVeiref1){
			RodeVei r = new RodeVei(v);
			List<RodeVei> list = veinett.get(r.getKey());
			if (list==null){
				list = new ArrayList<RodeVei>();
				veinett.put(r.getKey(), list);
			}
			list.add(r);
		}

		for (Veinettveireferanse v:rodeVeiref2){
			String key = v.getFylkesnummer()+v.getKommunenummer()+v.getVeikategori()+v.getVeistatus()+v.getVeinummer()+"hp"+v.getHp();		
			List<RodeVei> list = veinett.get(key);
			if (list==null)
				continue;
			List<Veinettveireferanse> overlapp = compare(list, v);
			if (overlapp!=null){
				for (Veinettveireferanse ref : overlapp) {
					overlappList.add(ref);
				}
			}
		}

		return overlappList;
	}

	/**
	 * Traverserer gjennom og sjekker om en (sb)rode overlapper en (R12)rode.
	 * Siden det kan være forskjellig antall Veinettveireferanser mellom en (sb)rode og en (R12)rode
	 * opprettes det en liste som fylles og returneres med alle overlappende veinettveireferanser.
	 * Se MIPSS-1331
	 * @param rode
	 * @param v
	 * @return
	 */
	private List<Veinettveireferanse> compare(List<RodeVei> rode, Veinettveireferanse v){
		List<Veinettveireferanse> veinettveireferanser = new ArrayList<Veinettveireferanse>();
		for (RodeVei rv:rode){
			if (rv.isOverlappende(v.getFraKm(), v.getTilKm())){					
				veinettveireferanser.add(rv.getOverlapp(v.getFraKm(), v.getTilKm()));			
			}
		}

		if (veinettveireferanser.isEmpty()) {
			return null;
		}else{
			return veinettveireferanser;
		}
	}

	class RodeVei{
		private int fylkesnummer;
		private int kommunenummer;
		private String veitype;
		private int nummer;
		private int hp;
		private double kmFra;
		private double kmTil;

		private String key;
		private final Veinettveireferanse veiref;

		public RodeVei(Veinettveireferanse veiref){
			this.veiref = veiref;
			this.veitype = veiref.getVeikategori()+veiref.getVeistatus();
			this.nummer = veiref.getVeinummer().intValue();
			this.hp = veiref.getHp().intValue();
			this.kmFra = veiref.getFraKm().doubleValue();
			this.kmTil = veiref.getTilKm().doubleValue();
			this.fylkesnummer = veiref.getFylkesnummer().intValue();
			this.kommunenummer = veiref.getKommunenummer().intValue();
			if (kmFra>kmTil){
				double temp = this.kmFra;
				this.kmFra=this.kmTil;
				this.kmTil = temp;
			}
			this.key = fylkesnummer+kommunenummer+veitype+nummer+"hp"+hp;
		}

		public int hashCode(){
			return key.hashCode();
		}

		public boolean equals(Object o){
			RodeVei other = (RodeVei)o;
			return other.getKey().equals(key);
		}
		public String getKey(){
			return key;
		}

		public Veinettveireferanse getOverlapp(double kmFra, double kmTil){
			Veinettveireferanse v = new Veinettveireferanse();
			v.setFylkesnummer(veiref.getFylkesnummer());
			v.setKommunenummer(veiref.getKommunenummer());
			v.setVeikategori(veiref.getVeikategori());
			v.setVeistatus(veiref.getVeistatus());
			v.setVeinummer(veiref.getVeinummer());
			v.setHp(veiref.getHp());
			//TODO overlappkm..
			if (kmFra>kmTil){
				double temp = kmFra;
				kmFra = kmTil;
				kmTil = kmFra;
			}
			if (this.kmFra>this.kmTil){
				double temp = this.kmFra;
				this.kmFra = this.kmTil;
				this.kmTil = temp;
			}
			if (this.kmTil>kmFra){
				v.setFraKm(this.kmFra>kmFra?this.kmFra:kmFra);
				v.setTilKm(this.kmTil<kmTil?this.kmTil:kmTil);

			}

			return v;
		}
		public boolean isOverlappende(double kmFra2, double kmTil2){
			if (kmFra2>kmTil2){
				double temp = kmFra2;
				kmFra2=kmTil2;
				kmTil2=temp;
			}
			return kmFra2 >= kmFra && kmFra2 < kmTil ||
			kmTil2 > kmFra && kmTil2 < kmTil ||
			kmFra > kmFra2 && kmFra < kmTil2 ||
			kmTil > kmFra2 && kmTil < kmTil2;
		}
	}
}
