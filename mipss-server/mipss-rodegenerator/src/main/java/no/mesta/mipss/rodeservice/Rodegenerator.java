package no.mesta.mipss.rodeservice;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;


@Remote
public interface Rodegenerator {
    public static final String BEAN_NAME = "RodegeneratorBean";
    /**
     * Henter alle rodetypene
     * @return
     */
    List<Rodetype> getRodetypeList();
    
    /**
     * Henter en liste med roder som tilhører en kontrakt og med spesifisert rodetype
     * @param kontraktId
     * @param rodetypeId
     * @return
     */
    List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId);
    /**
     * Henter en liste med roder som tilhører en kontrakt og med spesifisert rodetype, kan også ta med lengden til roden
     * @param driftkontraktId
     * @param rodetypeId
     * @param rodelengde
     * @return
     */
    List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId, Boolean rodelengde, Boolean kunAktive);
	List<Rode> getRodeForKontraktFull(Long driftkontraktId, Long rodetypeId, Boolean rodelengde, Boolean kunAktive);

    Double getDekningsgradRodetypeKontraktveinett(Long driftkontraktId, Long rodetypeId, Boolean kunAktive);

    /**
     * henter rodetypene for en gitt kontrakt.
     * @param driftkontraktId
     * @return
     */
    List<Rodetype> getRodetypeForKontrakt(Long driftkontraktId);
    
    /**
     * Henter ut lengden i KM for gitt veinett
     * @param veinettId
     * @return
     */
    Double getLengdeForVeinett(Long veinettId);
    
    List<Veinettveireferanse> getVeireferanseForRode(Rode rode);
    List<Veinettveireferanse> getVeinettForDriftkontrakt(Long driftkontraktId);
    /**
     * Bruker nativequery for å hent veinettveireferanser raskt..
     * @param veinettId
     * @return
     */
    List<Veinettveireferanse> getVeinettForDriftkontraktRaskt(Long veinettId);
    /**
	 * Henter veinett for aktive strø-brøyteroder 
	 * @param driftkontraktId
	 * @return
	 */
    List<Veinettveireferanse> getVeinettForStroBroyt(Long driftkontraktId);
    Double getRodeLengde(Rode rode);
    Rodetype persistRodetypeForKontrakt(Rodetype type, Driftkontrakt kontrakt);
    Rode persistRode(Rode rode);
    Rode copyRode(Rode src, Rode dest);
    Rode mergeRode(Rode rode);
    Rodetype mergeRodetype(Rodetype type);
    void removeRode(Rode rode);
    void removeRodetype(Rodetype rodetype);
    Rode persistStrekninger(List<Veinettreflinkseksjon> strekninger, Rode rode);
    Rode persistVeirefStrekninger(List<Veinettveireferanse> strekninger, Rode rode);
    Map<Rode, List<Veinettveireferanse>> sjekkForOverlapp(List<Veinettreflinkseksjon> nyeRodestrekninger, Rode rode, Long rodetypeId);
    Map<Rode, List<Veinettveireferanse>> sjekkForOverlapp(Rode src, Rode dest);
    List<List<String>> getRapportdata(RoderapportType type, Long kontraktId) throws SQLException;
}
