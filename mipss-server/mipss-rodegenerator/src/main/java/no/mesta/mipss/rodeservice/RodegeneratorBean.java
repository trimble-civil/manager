package no.mesta.mipss.rodeservice;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinetttype;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.webservice.NvdbWebserviceLocal;

import org.eclipse.persistence.sessions.DatabaseRecord;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Bønne for å hente data fra databasen til rodegeneratoren, samt transformering 
 * av geometri for veinett.
 * 
 * Bruker en .NET webservice for transformering
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@Stateless(name=Rodegenerator.BEAN_NAME, mappedName="ejb/"+Rodegenerator.BEAN_NAME)
@SuppressWarnings("unchecked")

public class RodegeneratorBean implements Rodegenerator{
    private static final Logger log = LoggerFactory.getLogger(RodegeneratorBean.class);
    
    @PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
    
    @EJB
    private NvdbWebserviceLocal ws;
    
    public RodegeneratorBean() {
    }

    /** {@inheritDoc} */
	public List<Rodetype> getRodetypeList() {
        return em.createNamedQuery(Rodetype.QUERY_FIND_ALL).getResultList();
    }
    
    /** {@inheritDoc} */
	public List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId){
    	return em.createNamedQuery(Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE)
    		.setParameter("kontraktId", driftkontraktId)
    		.setParameter("rodetypeId", rodetypeId)
    		.getResultList();
    }
	
	public Double getDekningsgradRodetypeKontraktveinett(Long driftkontraktId, Long rodetypeId, Boolean kunAktive){
		Driftkontrakt d = em.find(Driftkontrakt.class, driftkontraktId);
		Double dlength = getVeinettLengde(d.getVeinettId());
		List<Rode> rodeForKontrakt = getRodeForKontrakt(driftkontraktId, rodetypeId, Boolean.TRUE, kunAktive);
		
		Double rlength = 0.0;
		for (Rode r :rodeForKontrakt){
			if (r.getRodeLengde()!=null){
				rlength +=r.getRodeLengde();
			}
		}
		if (dlength==null){
			return null;
		}
		if (rlength==null){
			return null;
		}
		
		return (rlength/dlength)*100;
	}
	
	public List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId, Boolean rodelengde, Boolean kunAktive){
		List<Rode> rodeList =  null;
		if (kunAktive){
			rodeList = em.createNamedQuery(Rode.QUERY_FIND_AKTIVE_BY_KONTRAKT_AND_TYPE)
			.setParameter("kontraktId", driftkontraktId)
			.setParameter("rodetypeId", rodetypeId)
			.setParameter("now", Clock.now())
			.getResultList();
		}else{
			rodeList = em.createNamedQuery(Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE)
			.setParameter("kontraktId", driftkontraktId)
			.setParameter("rodetypeId", rodetypeId)
			.getResultList();
		}
		
		if (rodeList.isEmpty())
			return rodeList;
		
		String inList = getVeinettIdList(rodeList);
		if (!inList.equals("")){
			log.debug("inlist:"+inList);
//			List<VeinettlengdeView> list = em.createQuery("SELECT l from VeinettlengdeView l where l.veinettId in("+inList+") order by l.veinettId").getResultList();
			for (int i=0;i<rodeList.size();i++){
				Rode r = rodeList.get(i);
                Double length = getRodeLengde(r);
                r.setRodeLengde(length);
				log.debug("rodeveinettid={}, length={}",r.getVeinett().getId(), length);
			}
		}
		return rodeList;
	}
	public List<Rode> getRodeForKontraktFull(Long driftkontraktId, Long rodetypeId, Boolean rodelengde, Boolean kunAktive){
		List<Rode> rodeList =  null;
		if (kunAktive){
			rodeList = em.createNamedQuery(Rode.QUERY_FIND_AKTIVE_BY_KONTRAKT_AND_TYPE)
			.setParameter("kontraktId", driftkontraktId)
			.setParameter("rodetypeId", rodetypeId)
			.setParameter("now", Clock.now())
			.getResultList();
		}else{
			rodeList = em.createNamedQuery(Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE)
			.setParameter("kontraktId", driftkontraktId)
			.setParameter("rodetypeId", rodetypeId)
			.getResultList();
		}
		
		if (rodeList.isEmpty())
			return rodeList;
		
		String inList = getVeinettIdList(rodeList);
		if (!inList.equals("")){
			log.debug("inlist:"+inList);
//			List<VeinettlengdeView> list = em.createQuery("SELECT l from VeinettlengdeView l where l.veinettId in("+inList+") order by l.veinettId").getResultList();
			for (int i=0;i<rodeList.size();i++){
				Rode r = rodeList.get(i);
				r.setRodeLengde(getRodeLengde(r));
				List<Veinettreflinkseksjon> veinettreflinkseksjonList = r.getVeinett().getVeinettreflinkseksjonList();
				for (Veinettreflinkseksjon vr:veinettreflinkseksjonList){
					vr.getVeinettveireferanseList().size();
				}
				
				log.debug("rodeveinettid="+r.getVeinett().getId());
			}
		}
		return rodeList;
	}
	public Rodetype persistRodetypeForKontrakt(Rodetype type, Driftkontrakt kontrakt){
		type.setDriftkontrakt(kontrakt);
		em.persist(type);
		em.flush();
		return type;
	}
	
	public Rodetype mergeRodetype(Rodetype type){
		type = em.merge(type);
		em.persist(type);
		em.flush();
		return type;
	}
	
	public Rode persistRode(Rode rode){
		if (rode.getId()==null){
			if (rode.getVeinett()==null)
				rode.setVeinett(getNyttVeinett(rode));
		}else{
			rode = em.merge(rode);
		}
		rode = em.merge(rode);
		em.flush();
		rode.setRodeLengde(getRodeLengde(rode));
		return rode;
	}
	public Map<Rode, List<Veinettveireferanse>> sjekkForOverlapp(List<Veinettreflinkseksjon> nyeRodestrekninger, Rode rode, Long rodetypeId){
		if (rode.getRodetype().getKanHaOverlappFlagg()){//rodetypen tillater overlapp returner som om ingen strekninger overlapper.
			return new HashMap<Rode, List<Veinettveireferanse>>();
		}
		Veinett rodeveinett = rode.getVeinett();
		List<Veinettreflinkseksjon> list = rodeveinett.getVeinettreflinkseksjonList();
		List<Veinettreflinkseksjon> newList = new ArrayList<Veinettreflinkseksjon>();
		newList.addAll(list);
		if (nyeRodestrekninger!=null)
			newList.addAll(nyeRodestrekninger);
		newList = mergeStrekninger(newList);
		List<Veinettveireferanse> rodeVeiref = new ArrayList<Veinettveireferanse>();
		for (Veinettreflinkseksjon v:newList){
			List<Veinettreflinkseksjon> ref = new ArrayList<Veinettreflinkseksjon>();
			ref.add(v);
			rodeVeiref.addAll(ws.getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(ref));
		}
		
		List<Rode> rodeForKontrakt = getRodeForKontrakt(rode.getKontraktId(), rodetypeId);
		OverlappHelper overlappHelper = new OverlappHelper();
		Map<Rode, List<Veinettveireferanse>> overlappMap = new HashMap<Rode, List<Veinettveireferanse>>();
		long from1 = rode.getGyldigFraDato().getTime();
		long to1 = rode.getGyldigTilDato()!=null?rode.getGyldigTilDato().getTime():Long.MAX_VALUE;
		
		for (Rode r:rodeForKontrakt){
			if (r.equals(rode))
				continue;
			long from2 = r.getGyldigFraDato().getTime();
			long to2 = r.getGyldigTilDato()!=null?r.getGyldigTilDato().getTime():Long.MAX_VALUE;
			
			if (!(from1 <= to2 && from2 <= to1)){//ikke-overlappende periode
				continue;
			}
			Veinett veinett = r.getVeinett();
			List<Veinettveireferanse> overlapp = overlappHelper.sjekkOverlappVeinett(rodeVeiref, veinett);
			if (overlapp!=null&&overlapp.size()!=0){
				overlappMap.put(r, overlapp);
			}
		}
		return overlappMap;
	}
	
	public Map<Rode, List<Veinettveireferanse>> sjekkForOverlapp(Rode src, Rode dest){
		Rode srcOrig = em.find(Rode.class, src.getId());
		Veinett veinett = srcOrig.getVeinett();
		if (veinett!=null){
			List<Veinettreflinkseksjon> veinettreflinkseksjonList = veinett.getVeinettreflinkseksjonList();
			return sjekkForOverlapp(veinettreflinkseksjonList, dest, dest.getRodetype().getId());
		}
		return null;
	}
	
	public Rode copyRode(Rode src, Rode dest){
		Veinett v = getNyttVeinett(dest);
		if (dest.getVeinett()!=null){
			v.setGuiFarge(dest.getVeinett().getGuiFarge());
		}
		em.persist(dest);
		src = em.merge(src);
		List<Veinettreflinkseksjon> veiref = new ArrayList<Veinettreflinkseksjon>();
		for (Veinettreflinkseksjon vr:src.getVeinett().getVeinettreflinkseksjonList()){	
			Veinettreflinkseksjon reflinkseksjon = new Veinettreflinkseksjon(vr);
			reflinkseksjon.getOwnedMipssEntity().setOpprettetAv(dest.getOwnedMipssEntity().getOpprettetAv());
			List<Veinettveireferanse> veireferanser = new ArrayList<Veinettveireferanse>();
			for (Veinettveireferanse vv: vr.getVeinettveireferanseList()){
				Veinettveireferanse veireferanse = new Veinettveireferanse(vv);
				veireferanse.setVeinettreflinkseksjon(reflinkseksjon);
				veireferanse.getOwnedMipssEntity().setOpprettetAv(dest.getOwnedMipssEntity().getOpprettetAv());
				veireferanse.setRodeId(dest.getId());
				veireferanser.add(veireferanse);
				
			}
			
			reflinkseksjon.setVeinettveireferanseList(veireferanser);
			reflinkseksjon.setVeinett(v);
			veiref.add(reflinkseksjon);
		}
		v.setVeinettreflinkseksjonList(veiref);

		dest.setVeinett(v);
		em.persist(dest);
		em.flush();
		dest.setRodeLengde(getRodeLengde(dest));
		return dest;
	}
	public Rode mergeRode(Rode rode){
		rode = em.merge(rode);
		em.persist(rode);
		em.flush();
//		rode.setRodeLengde(getRodeLengde(rode));
		return rode;
	}
	
	private Veinett getNyttVeinett(Rode rode){
		String navn = rode.getNavn();
    	Veinetttype type = new Veinetttype();
    	type.setNavn("Rode");
    	
    	Veinett v = new Veinett();
    	v.getOwnedMipssEntity().setOpprettetAv(rode.getOwnedMipssEntity().getOpprettetAv());
    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	v.setNavn(navn);
    	v.setGuiFarge("0xffffff");
    	v.setGuiTykkelse(2);
    	v.setVeinetttype(type);
    	return v;
    }
	
	public List<Veinettveireferanse> getVeireferanseForRode(Rode rode){
		List<Veinettveireferanse> veilist = em.createQuery("Select v from Veinettveireferanse v where v.rodeId=:rodeId").setParameter("rodeId", rode.getId()).getResultList();
		return veilist;
	}
	
	public List<Veinettveireferanse> getVeinettForDriftkontrakt(Long driftkontraktId){
		Driftkontrakt k = em.find(Driftkontrakt.class, driftkontraktId);
		List<Veinettveireferanse> veiList = em.createQuery("Select v from Veinettveireferanse v where v.veinettreflinkseksjon.id in (select vr.id from Veinettreflinkseksjon vr where vr.veinett.id=:veinettId)").setParameter("veinettId", k.getVeinett().getId()).getResultList();
		return veiList;
	}
	
	public List<Veinettveireferanse> getVeinettForDriftkontraktRaskt(Long veinettId){
		StringBuilder q = new StringBuilder();
		q.append("select v.id, v.fylkesnummer, v.kommunenummer, v.veistatus, v.veikategori, v.veinummer, hp, v.fra_km, v.til_km ");
		q.append("from Veinettveireferanse v ");
		q.append("join veinettreflinkseksjon vr on vr.id = v.reflinkseksjon_id ");
		q.append("and vr.veinett_id=? ");
		
		Query qry = em.createNativeQuery(q.toString()).setParameter(1, veinettId);
        NativeQueryWrapper<Veinettveireferanse> wrapper = new NativeQueryWrapper<Veinettveireferanse>(qry, Veinettveireferanse.class, new String[]{"id", "fylkesnummer", "kommunenummer", "veistatus", "veikategori", "veinummer", "hp", "fraKm", "tilKm"});
        return wrapper.getWrappedList();
	}
	
	/**
	 * Henter veinett for aktive strø-brøyteroder 
	 * @param driftkontraktId
	 * @return
	 */
	public List<Veinettveireferanse> getVeinettForStroBroyt(Long driftkontraktId){
		StringBuilder q = new StringBuilder();
		q.append("select v.id, v.fylkesnummer, v.kommunenummer, v.veistatus, v.veikategori, v.veinummer, hp, v.fra_km, v.til_km ");
		q.append("from Veinettveireferanse v ");
		q.append("join veinettreflinkseksjon vr on vr.id = v.reflinkseksjon_id ");
		q.append("and vr.veinett_id in(select veinett_id from rode where kontrakt_id=? and type_id = (select verdi from konfigparam where navn = 'stro_broyte_rodetype_id') and sysdate between gyldig_fra_dato and gyldig_til_dato)");
		
		Query qry = em.createNativeQuery(q.toString()).setParameter(1, driftkontraktId);
        NativeQueryWrapper<Veinettveireferanse> wrapper = new NativeQueryWrapper<Veinettveireferanse>(qry, Veinettveireferanse.class, new String[]{"id", "fylkesnummer", "kommunenummer", "veistatus", "veikategori", "veinummer", "hp", "fraKm", "tilKm"});
        return wrapper.getWrappedList();
	}
	
	public void removeRode(Rode rode){
		Rode merged = em.merge(rode);
		if (merged.getVeinett()!=null){
			Query deleteVeiref = em.createNativeQuery("delete from veinettveireferanse where rode_id=?").setParameter(1, merged.getId());
			deleteVeiref.executeUpdate();
			em.remove(merged.getVeinett());
			em.flush();
		}
		em.remove(merged);
		em.flush();
	}
	
	public Rode persistStrekninger(List<Veinettreflinkseksjon> strekninger, Rode rode){
		int updated = em.createNativeQuery("delete from veinettveireferanse v where v.rode_id=?").setParameter(1, rode.getId()).executeUpdate();
		log.debug("Nuked::"+updated+" veireffer..");
		
		rode = em.merge(rode);
		Veinett veinett = rode.getVeinett();
		em.refresh(veinett);
		List<Veinettreflinkseksjon> list = veinett.getVeinettreflinkseksjonList();
		
		
		
		List<Veinettreflinkseksjon> newList = new ArrayList<Veinettreflinkseksjon>();
		newList.addAll(list);
		newList.addAll(strekninger);
		
		log.debug("Antall strekninger før merge:"+newList.size()+" added:"+strekninger.size());
		newList = mergeStrekninger(newList);
		log.debug("Antall strekninger etter merge:"+newList.size());
		log.debug("lagrer strekninger..");
		for (Veinettreflinkseksjon v:newList){
			v.setVeinett(veinett);
			log.debug("henter veireferanser for reflinkeseksjon");
			List<Veinettreflinkseksjon> ref = new ArrayList<Veinettreflinkseksjon>();
			ref.add(v);
			List<Veinettveireferanse> vr = ws.getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(ref);
			for (Veinettveireferanse vref :vr){
				vref.setVeinettreflinkseksjon(v);
				vref.setRodeId(rode.getId());
				vref.getOwnedMipssEntity().setOpprettetAv(rode.getOwnedMipssEntity().getEndretAv());
				
			}
			v.setVeinettveireferanseList(vr);
			v.getOwnedMipssEntity().setOpprettetAv(rode.getOwnedMipssEntity().getEndretAv());	
		}
		
		veinett.setVeinettreflinkseksjonList(newList);
		em.persist(rode);
//		em.flush();
		return rode;
	}
	
	public Double getLengdeForVeinett(Long veinettId){
		return getVeinettLengde(veinettId);
	}
	public Rode persistVeirefStrekninger(List<Veinettveireferanse> strekninger, Rode rode){
		rode = em.merge(rode);
		Veinett veinett = rode.getVeinett();
		em.refresh(veinett);
		int updated = em.createQuery("Delete from Veinettveireferanse v where v.rodeId=:rodeId").setParameter("rodeId", rode.getId()).executeUpdate();
		updated = em.createQuery("Delete from Veinettreflinkseksjon v where v.veinett.id=:veinettId").setParameter("veinettId", veinett.getId()).executeUpdate();
		
		List<Veinettreflinkseksjon> list = new ArrayList<Veinettreflinkseksjon>();
		for (Veinettveireferanse vr:strekninger){
//			List<Veinettreflinkseksjon> rf = ws.getReflinkSectionsWithinRoadref(vr);
//			for (Veinettreflinkseksjon r:rf){
//				log.debug("Veinettrefsec:"+r);
//			}
			list.addAll(ws.getReflinkSectionsWithinRoadref(vr));
		}
		
		rode = persistStrekninger(list, rode);
		em.refresh(rode);
		return rode;
	}
	/**
	 * Slår sammen strekninger slik at det ikke finnes overlapp.
	 * @param list
	 * @return
	 */
	private List<Veinettreflinkseksjon> mergeStrekninger(List<Veinettreflinkseksjon> list){
    	sort2(list);
    	List<Veinettreflinkseksjon> merged = new ArrayList<Veinettreflinkseksjon>();
    	Veinettreflinkseksjon forrige = null;
    	for (Veinettreflinkseksjon v:list){
    		if (merged.isEmpty()){
    			merged.add(v);
    			forrige = v;
    		}else{
				//en annen reflink
				if (v.getReflinkIdent().longValue()!=forrige.getReflinkIdent().longValue()){
					merged.add(v);
					forrige = v;
				}else{
					double fra = v.getFra();
					double til = v.getTil();
					//overlapp
					if (fra<=forrige.getTil()&&til>forrige.getTil()){
						forrige.setTil(til);
					}else if (fra>forrige.getTil()){
						merged.add(v);
						forrige = v;
					}
				}
    		}
    	}
    	return merged;
    }
	/**
	 * Sorterer listen etter 1. reflinkId 2. fra 3. til
	 * @param list
	 */
    private void sort2(List<Veinettreflinkseksjon> list){
    	
    	Collections.sort(list, new Comparator<Veinettreflinkseksjon>(){
			public int compare(Veinettreflinkseksjon o1, Veinettreflinkseksjon o2) {
				long vn1 = o1.getReflinkIdent();
				long vn2 = o2.getReflinkIdent();
				
				double fra1 = o1.getFra();
				double fra2 = o2.getFra();
				
				double til1 = o1.getTil();
				double til2 = o2.getTil();
				
				int result = (vn1==vn2)? 0 : ((vn1<vn2)?-1:1);
				if (result==0){
					result = (fra1==fra2)?0: ((fra1<fra2)?-1:1);
					if (result==0){
						result = (til1==til2)?0:((til1<til2)?-1:1);
					}
				}
				return result;
			}
    	});
    }
	public void removeRodetype(Rodetype rodetype){
		if (rodetype.getDriftkontrakt()!=null){
			Rodetype merged = em.merge(rodetype);
			em.remove(merged);
			em.flush();
		}
	}

	private String getVeinettIdList(List<Rode> rodeList){
		String s = "";
		for (int i=0;i<rodeList.size();i++){
			if (rodeList.get(i).getVeinett()!=null){
				if (i<rodeList.size()-1){
					s+=rodeList.get(i).getVeinett().getId()+", ";
				}else{
					s+=rodeList.get(i).getVeinett().getId();
				}
			}
		}
		if (s.length()>1)
			if (s.charAt(s.length()-2)==',')
				s = s.substring(0, s.length()-2);
		return s;
	}
	
	private Double getVeinettLengde(Long veinettId){
		String q = "SELECT sum(v.lengdeKm) from VeinettlengdeView v where v.veinettId=:veinettId";
		
		Number length = (Number)em.createQuery(q).setParameter("veinettId", veinettId).getSingleResult();
		if (length!=null){
			return length.doubleValue();
		}
		return null;
	}
	public Double getRodeLengde(Rode rode){
		if (rode == null || rode.getVeinett() == null)
			return null;

		try {
			//grunnet ConcurrentModificationException ved nøstede transaksjoner gjøres dette med native. 
			BigDecimal res=(BigDecimal)em.createNativeQuery("select lengde_km from veinettlengde_v where veinett_id=?").setParameter(1, rode.getVeinett().getId()).getSingleResult();
			return res.doubleValue();
		} catch (NoResultException e){
            log.error("Unable to find rode length for rode {}", rode.getId());
		} catch (NonUniqueResultException e){
			log.error("Found more than one row when fetching rode length for rode {}", rode.getId(), e);
		} catch (NullPointerException e) {
			log.error("Nullpointer received in getRodeLengde for rode {}", rode.getId());
		}

		return null;
	}
	/** {@inheritDoc} */
    public List<Rodetype> getRodetypeForKontrakt(Long driftkontraktId){
    	List<Rodetype> rodetypeList = em.createNamedQuery(Rodetype.QUERY_FIND_COMMON).getResultList();
    	List<Rodetype> typeList = em.createNamedQuery(Rodetype.QUERY_FIND_BY_KONTRAKTID).setParameter("kontraktId", driftkontraktId).getResultList();
    	if (typeList!=null){
    		for (Rodetype type:typeList){
    			if (!rodetypeList.contains(type))
    				rodetypeList.add(type);
    		}
    	}
    	return rodetypeList;
    }
    
    private double getLength(Veinettveireferanse vr){
		double fra = vr.getFraKm();
		double til = vr.getTilKm();
		if (fra>til){
			return fra-til;
		}else
			return til-fra;
	}
    
    
    /**
     * TODO pakk dette sammen med JPA klasser.Flyttet i hui og hast fra klientsiden..
     * @param type
     * @param kontraktId
     * @return
     * @throws SQLException
     */
    public List<List<String>> getRapportdata(RoderapportType type, Long kontraktId) throws SQLException{
    	
    	
    	String statement = getStatement(type);
    	
    	Query query = em.createNativeQuery(statement);
    	query.setHint(QueryHints.RESULT_TYPE, HintValues.MAP);
    	query.setParameter(1, kontraktId);
    	List<DatabaseRecord> resultList = query.getResultList();
    	return consumeResultSet(resultList);
    	
//    	Context ctx;
//    	Connection connection = null;
//		try {
//			ctx = new InitialContext();
//			DataSource ds = (DataSource) ctx.lookup("java:/mipssDS");
//			connection = ds.getConnection();
//		} catch (NamingException e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//		if (connection instanceof WrappedConnectionJDK6) {
//			WrappedConnectionJDK6 wrapped = (WrappedConnectionJDK6) connection;
//			connection = wrapped.getUnderlyingConnection();
//		}
////		PreparedStatement ps = getStatement(type, connection);
////		ps.setLong(1, kontraktId);
//    	
////    	return consumeResultSet(ps.getResultSet());
//		return null;
    }
    private List<List<String>> consumeResultSet(List<DatabaseRecord> rs) throws SQLException{
		
		List<List<String>> data = new ArrayList<List<String>>();
		List<String> header = new ArrayList<String>();
		
		for (int i=0;i<rs.size();i++){
			DatabaseRecord rec = rs.get(i);
			if (i==0){
				Vector fields = rec.getFields();
				for (int j=0;j<fields.size();j++){
					header.add(String.valueOf(fields.get(j)));
				}
				data.add(header);
			}
			List<String> row = new ArrayList<String>();
			Vector values = rec.getValues();
			for (int j=0;j<values.size();j++){
				row.add(String.valueOf(values.get(j)));
			}
			data.add(row);
		}
		return data;
	}
    private String getStatement(RoderapportType type){
		switch(type){
			case OVERLAPPENDE_RODER: return "select * from table(valider_pk.overlapp_stro_broyte) where kontrakt_id=?";
			case HULL_RODER: return "select * from table(valider_pk.hull_stro_broyte) where kontrakt_id=?";
			case RODER_UTENFOR_KONTRAKT: return "select * from table(valider_pk.stro_broyte_utenfor_kontrakt) where kontrakt_id=?"; 
		}
		return null;
	}
}
