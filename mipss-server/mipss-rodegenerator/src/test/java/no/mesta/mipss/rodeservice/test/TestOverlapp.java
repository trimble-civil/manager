package no.mesta.mipss.rodeservice.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.rodeservice.OverlappHelper;

public class TestOverlapp extends TestCase {
	private List<Veinettveireferanse> v1=new ArrayList<Veinettveireferanse>();
	private Veinett v2;
	
	protected void setUp() throws Exception {
		super.setUp();
		v2 = new Veinett();
		v1.addAll(createVR());
		List<Veinettreflinkseksjon> vr2 = new ArrayList<Veinettreflinkseksjon>();
		vr2.add(createVR2());
		v2.setVeinettreflinkseksjonList(vr2);
		
	}
	private List<Veinettveireferanse> createVR(){
		List<Veinettveireferanse> vl = new ArrayList<Veinettveireferanse>();
		Veinettveireferanse v1 = createV(Long.valueOf(1), Long.valueOf(0),"R", "V", 23, 1, 0, 1000);
		Veinettveireferanse v2 = createV(Long.valueOf(1), Long.valueOf(0),"R", "V", 23, 2, 0, 1000);
		Veinettveireferanse v3 = createV(Long.valueOf(1), Long.valueOf(0),"R", "V", 23, 3, 610, 651);
		vl.add(v1);
		vl.add(v2);
		vl.add(v3);
		return vl;
	}
	private Veinettreflinkseksjon createVR2(){
		Veinettreflinkseksjon vr = new Veinettreflinkseksjon();
		List<Veinettveireferanse> vl = new ArrayList<Veinettveireferanse>();
		Veinettveireferanse v1 = createV(Long.valueOf(1), Long.valueOf(0),"R", "V", 23, 3, 650, 1175);
		Veinettveireferanse v2 = createV(Long.valueOf(1), Long.valueOf(0),"R", "V", 23, 4, 0, 1000);
		Veinettveireferanse v3 = createV(Long.valueOf(1), Long.valueOf(0),"R", "V", 23, 5, 0, 100);
		v1.setVeinettreflinkseksjon(vr);
		v2.setVeinettreflinkseksjon(vr);
		v3.setVeinettreflinkseksjon(vr);
		vl.add(v1);
		vl.add(v2);
		vl.add(v3);
		vr.setVeinettveireferanseList(vl);
		return vr;
	}
	private Veinettveireferanse createV(Long fylkesnummer, Long kommunenummer, String kategori, String status, int nummer, int hp, double kmfra, double kmtil){
		Veinettveireferanse v = new Veinettveireferanse();
		v.setFylkesnummer(fylkesnummer);
		v.setKommunenummer(kommunenummer);
		v.setVeikategori(kategori);
		v.setVeistatus(status);
		v.setVeinummer(Long.valueOf(nummer));
		v.setHp(Long.valueOf(hp));
		v.setFraKm(kmfra);
		v.setTilKm(kmtil);
		return v;
	}
	
	public void testSjekkOverlappVeinett() {
		OverlappHelper helper = new OverlappHelper();
		List<Veinettveireferanse> overlapp = helper.sjekkOverlappVeinett(v1, v2);
		assertNotNull(overlapp);
		assertEquals(1, overlapp.size());
		Veinettveireferanse vr = overlapp.get(0);
		assertEquals("R", vr.getVeikategori());
		assertEquals("V", vr.getVeistatus());
		assertEquals(Long.valueOf(23), vr.getVeinummer());
		assertEquals(Long.valueOf(3), vr.getHp());
		assertEquals(Double.valueOf(650), vr.getFraKm());
		assertEquals(Double.valueOf(651), vr.getTilKm());
	}
	
	//TODO test veinett med samme veireferanse men ikke samme fylke/kommunenummer

}
