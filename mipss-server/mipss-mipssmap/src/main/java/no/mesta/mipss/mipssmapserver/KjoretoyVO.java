package no.mesta.mipss.mipssmapserver;

import java.io.Serializable;

import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;

/**
 * Vo klasse for å overføre pda eller kjoretoy
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 * 
 */
@SuppressWarnings("serial")
public class KjoretoyVO implements Serializable {
	private KjoretoyDTO kjoretoy;
	private Dfuindivid pda;
	private String pdaNavn;
	public KjoretoyVO(Dfuindivid pda) {
		this.pda = pda;
	}

	public KjoretoyVO(KjoretoyDTO kjoretoy) {
		this.kjoretoy = kjoretoy;
	}

	/**
	 * @return the kjoretoy
	 */
	public KjoretoyDTO getKjoretoy() {
		return kjoretoy;
	}

	/**
	 * @return the pda
	 */
	public Dfuindivid getPda() {
		return pda;
	}

	/**
	 * @param kjoretoy
	 *            the kjoretoy to set
	 */
	public void setKjoretoy(KjoretoyDTO kjoretoy) {
		this.kjoretoy = kjoretoy;
	}

	/**
	 * @param pda
	 *            the pda to set
	 */
	public void setPda(Dfuindivid pda) {
		this.pda = pda;
	}

	public void setPdaNavn(String pdaNavn) {
		this.pdaNavn = pdaNavn;
	}

	public String getPdaNavn() {
		return pdaNavn;
	}
}
