package no.mesta.mipss.mipssmapserver;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;


@Remote
public interface MipssMap {
    public static final String BEAN_NAME = "MipssMapBean";
    
    /**
     * Henter kjoretoy og pda som er registrert for en kontrakt
     * @param kontraktId
     * @return
     */
    List<KjoretoyVO> getKjoretoyPdaForKontrakt(Long kontraktId);
    /**
     * Henter kjoretoy og pda som har gjort produksjon p� angitt kontrakt innen for tidsrommet
     * @param fraDato
     * @param tilDato
     * @param kontraktId
     * @return
     */
    List<KjoretoyVO> getKjoretoyPdaForTidsperiode(Date fraDato, Date tilDato, Long kontraktId);
    
    /**
     * Henter en liste med alle kjoretoy og pda 
     * @return
     */
    List<KjoretoyVO> getAllKjoretoyPda();
    
    /**
     * Henter alle pda som er knyttet til kontrakten
     * @param kontraktId kontraktens id. 
     * @return
     */
    List<Dfuindivid> getPdaForKontrakt(Long kontraktId);
    /**
     * Henter dfuindivid for et kj�ret�y p� en gitt dato
     * Denne henter kun ut f�rste dfuindivid som ligger i installasjon hvis det finnes flere installasjoner.
     */
    Dfuindivid getDfuForKjoretoy(Long kjoretoyId, Date dato);
    /**
     * Henter en liste med alle driftkontrakter
     * @return 
     */
    List<Driftkontrakt> getDriftkontraktList();
    
    /**
     * Henter Dfuindivider som er knyttet mot kontrakten
     * @param kontraktId
     * @return
     */
//    List<Dfuindivid> getDfuindividForKontrakt(Long kontraktId);
    
    /**
     * Henter et kjøretøyet med angitt dfu på angitt tidspunkt
     * @param dfuId
     * @param time
     * @return
     */
    Kjoretoy getKjoretoyFromDfuAndTime(Long dfuId, Date time);
    /**
     * Henter kj�ret�y som er knyttet til kontrakten
     * @param id
     * @return
     */
    List<Kjoretoy> getKjoretoyForKontrakt(Long id);
    /**
     * Henter rodene som tilh�rer en kontrakt
     * @param driftkontraktId id til kontrakten
     * @param rodetypeId hvilken type roder som skal returneres
     * @return
     */
    List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId);
    /**
     * Henter rodene som tilh�rer en kontrakt, tar kun med roder med 
     * gyldighetsperiode som en innenfor dagens dato
     * @param driftkontraktId id til kontrakten
     * @param rodetypeId hvilken type roder som skal returneres
     * @return
     */
    List<Rode> getAktiveRodeForKontrakt(Long driftkontraktId, Long rodetypeId);
    /**
     * Henter ut rodetypene som er definert for kontrakten
     * @param driftkontraktId id til kontrakten
     * @return
     */
    List<Rodetype> getRodetypeForKontrakt(Long driftkontraktId);

    void setFarger();
    /**
     * Beregner lengden til en rode
     * @param rodeId
     * @return
     */
    Double getRodeLengde(Long rodeId);
    
    /** 
     * Beregner lengden til et veinett
     * @param veinettId
     * @return
     */
    Double getVeinettLengde(Long veinettId);
}