package no.mesta.mipss.mipssmapserver;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class KjoretoyDTO implements Serializable{
	private Long id;
	private String navn;
	private String guiTekst;
	private String merke;
	private String type;
	private String leverandorNavn;
	private String regnr;
	
	public KjoretoyDTO(){
		
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the navn
	 */
	public String getNavn() {
		return navn;
	}
	/**
	 * @param navn the navn to set
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}
	/**
	 * @return the guiTekst
	 */
	public String getGuiTekst() {
		return guiTekst;
	}
	/**
	 * @param guiTekst the guiTekst to set
	 */
	public void setGuiTekst(String guiTekst) {
		this.guiTekst = guiTekst;
	}
	/**
	 * @return the merke
	 */
	public String getMerke() {
		return merke;
	}
	/**
	 * @param merke the merke to set
	 */
	public void setMerke(String merke) {
		this.merke = merke;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the leverandorNavn
	 */
	public String getLeverandorNavn() {
		return leverandorNavn;
	}
	/**
	 * @param leverandorNavn the leverandorNavn to set
	 */
	public void setLeverandorNavn(String leverandorNavn) {
		this.leverandorNavn = leverandorNavn;
	}
	
	public void setRegnr(String regnr) {
		this.regnr = regnr;
	}
	public String getRegnr() {
		return regnr;
	}
	
	public int hashCode(){
		return new HashCodeBuilder().append(getId()).append(getNavn()).toHashCode();
	}
	public boolean equals(Object other){
		if (other==this)return true;
		if (!(other instanceof KjoretoyDTO))
			return false;
		KjoretoyDTO o = (KjoretoyDTO)other;
		return new EqualsBuilder().append(o.getId(), getId()).append(o.getNavn(), getNavn()).isEquals();
	}
	
}
