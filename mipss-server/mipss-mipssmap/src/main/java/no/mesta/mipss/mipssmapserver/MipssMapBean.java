package no.mesta.mipss.mipssmapserver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.rodeservice.Rodegenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bønne for å hente data fra databasen til kartklienten
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@Stateless(name=MipssMap.BEAN_NAME, mappedName="ejb/"+MipssMap.BEAN_NAME)
@SuppressWarnings("unchecked")
public class MipssMapBean implements MipssMap {
    private static final Logger log = LoggerFactory.getLogger(MipssMapBean.class);

    @PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
    
    @EJB
    Rodegenerator rodegenerator;
    
    public MipssMapBean() {
    }
    
    /** {@inheritDoc} */
    public List<KjoretoyVO> getAllKjoretoyPda(){
    	List<KjoretoyVO> kpList = new ArrayList<KjoretoyVO>();
    	Query q = em.createNativeQuery("select k.id, k.eksternt_navn, k.regnr, k.guitekst, m.merke, k.type_navn, l.navn "+
				 "from kjoretoy k "+
				 "left outer join leverandor l on k.leverandor_nr=l.nr "+
				 "left join kjoretoymodell m on k.modell_id=m.id "+
				 "where k.slettet_flagg<>1 ");
		List<KjoretoyDTO> kjoretoyList = new NativeQueryWrapper<KjoretoyDTO>(q, KjoretoyDTO.class, new Class[]{Long.class, String.class, String.class, String.class, String.class, String.class, String.class}, "id", "navn", "regnr", "guiTekst", "merke", "type", "leverandorNavn").getWrappedList();
		for (KjoretoyDTO k:kjoretoyList){
			kpList.add(new KjoretoyVO(k));
		}
		
    	List<DfuKontrakt> pdaList = em.createNamedQuery(DfuKontrakt.QUERY_FIND_ALL).getResultList();
    	for (DfuKontrakt k:pdaList){
    		kpList.add(new KjoretoyVO(k.getDfuindivid()));
    	}
    	return kpList;
    }
    
    public Kjoretoy getKjoretoyFromDfuAndTime(Long dfuId, Date time){
    	List<Installasjon> resultList = em.createQuery("select i from Installasjon i where i.aktivFraDato<=:fraDato and i.aktivTilDato>=:tilDato and i.dfuindivid.id=:dfuId " +
		"or i.aktivFraDato<=:fraDato and i.aktivTilDato is null and i.dfuindivid.id=:dfuId").setParameter("fraDato", time).setParameter("tilDato", time).setParameter("dfuId", dfuId).getResultList();
    	Kjoretoy k = null;
    	if (resultList.size()>0){
    		k = resultList.get(0).getKjoretoy();
    	}
    	
    	return k;
    }
    
    /** {@inheritDoc} */
    public Dfuindivid getDfuForKjoretoy(Long kjoretoyId, Date dato){
    	Kjoretoy k = em.find(Kjoretoy.class, kjoretoyId);
    	if (k.getInstallasjonList()!=null&&k.getInstallasjonList().size()>0){
    		if (k.getInstallasjonList().get(0)!=null)
    			return (Dfuindivid)k.getInstallasjonList().get(0).getDfuindivid();
    	}
    	return null;
    }
    
    /** {@inheritDoc} */ //Uferdig..
//    public List<Dfuindivid> getDfuindividForKontrakt(Long kontraktId){
//    	List<Dfuindivid> dfuList = em.createNamedQuery(Dfuindivid.QUERY_FIND_ALL).getResultList();
//    	log.debug("Fant "+dfuList.size()+" biler for kontrakt:"+kontraktId);
//    	return dfuList;
//    }
    
    /** {@inheritDoc} */
    public List<Driftkontrakt> getDriftkontraktList() {
        return em.createNamedQuery(Driftkontrakt.QUERY_FIND_ALL).getResultList();
    }

    /** {@inheritDoc} */
    public List<Kjoretoy> getKjoretoyForKontrakt(Long id){
    	List<Kjoretoy> kjoretoyList = new ArrayList<Kjoretoy>();
    	return kjoretoyList;
    }
    
    /** {@inheritDoc} */
    public List<KjoretoyVO> getKjoretoyPdaForKontrakt(Long kontraktId){
    	List<KjoretoyVO> kpList = new ArrayList<KjoretoyVO>();
        log.debug("Fetching vehicle pda for contract {}", kontraktId);
    	StringBuilder sb = new StringBuilder();
    	sb.append("select k.id, k.eksternt_navn, k.regnr, k.guitekst, m.merke, k.type_navn, l.navn ");
    	sb.append("from kontrakt_kjoretoy kt ");
    	sb.append("left join kjoretoy k on kt.kjoretoy_id = k.id ");
    	sb.append("left join leverandor l on k.leverandor_nr = l.nr ");
    	sb.append("left join kjoretoymodell m on k.modell_id=m.id ");
    	sb.append("where k.slettet_flagg<>1 ");
    	sb.append("and kt.kontrakt_id = ?1");
    	//TODO legge til kriterie fra-til dato
    	Query q = em.createNativeQuery(sb.toString());
    	q.setParameter(1, kontraktId);
    	
    	List<KjoretoyDTO> kjoretoyList = new NativeQueryWrapper<KjoretoyDTO>(q, KjoretoyDTO.class, 
    			new Class[]{
    				Long.class, String.class, String.class, 
    				String.class, String.class, String.class, 
    				String.class}, 
    				"id", "navn", "regnr", 
    				"guiTekst", "merke", "type", 
    				"leverandorNavn").getWrappedList();
    	
    	for (KjoretoyDTO k:kjoretoyList){
    		kpList.add(new KjoretoyVO(k));
    	}
    	
    	
    	List<DfuKontrakt> pdaList = em.createNamedQuery(DfuKontrakt.QUERY_FIND_BY_KONTRAKT)
    		.setParameter("id", kontraktId)
    		.getResultList();
    	
    	for (DfuKontrakt k:pdaList){
    		
    		
    		
    		if (isActiveNow(k.getFraDato(), k.getTilDato())){
	    		KjoretoyVO kv = new KjoretoyVO(k.getDfuindivid());
	    		kv.setPdaNavn(k.getNavn());
	    		kpList.add(kv);
    		}
    	}
    	log.debug("Hentet "+kjoretoyList.size()+ " kjoretoy");
    	log.debug("Hentet "+pdaList.size()+ " pda");
    	log.debug("Totalt :"+kpList.size()+" enheter");
    	return kpList;
    }
    private boolean isActiveNow(Date fra, Date til){
    	if (fra==null||fra.before(Clock.now())){
			if (til==null||til.after(Clock.now())){
				return true;
			}
		}
    	return false;
    }
    /** {@inheritDoc} */
    public List<KjoretoyVO> getKjoretoyPdaForTidsperiode(Date fraDato, Date tilDato, Long kontraktId){
    	//TODO: implement...
    	return null;
    }
    
    /**
	 * Returnerer lengden på veireferansen
	 * 
	 * @param vr
	 * @return
	 */
	private double getLength(Veinettveireferanse vr){
		double fra = vr.getFraKm();
		double til = vr.getTilKm();
		if (fra>til){
			return fra-til;
		}else
			return til-fra;
	}
    
    /** {@inheritDoc} */
    public List<Dfuindivid> getPdaForKontrakt(Long kontraktId){
    	log.debug("getPdaForKontrakt(" + kontraktId + ") start");
    	List<Dfuindivid> dfuer = em.createNamedQuery(Dfuindivid.FIND_BY_KONTRAKT).setParameter("kontraktId", kontraktId).getResultList();
    	
    	//Initialiser kontraktlisten
    	// Alternativer: Bruk JOIN FETCH i spørring og java.util.Set, eller fjern duplikater her.
    	// Dette er enklere, om kanskje ikke mer effektivt
    	List<Dfuindivid> list = new ArrayList<Dfuindivid>();
    	for(Dfuindivid dfu:dfuer) {
    		List<DfuKontrakt> dfuList = dfu.getDfuKontraktList();
    		DfuKontrakt dfuKontrakt = null;
    		for (DfuKontrakt d:dfuList){
    			if (d.getKontraktId().equals(kontraktId)){
    				dfuKontrakt = d;
    				break;
    			}
    		}
    		//sjekk om dfukontrakt sin gyldighetsperiode er gyldig. 
    		if (dfuKontrakt!=null){
    			if (isActiveNow(dfuKontrakt.getFraDato(), dfuKontrakt.getTilDato())){
					list.add(dfu);
				}
    		}
    	}
    	
    	log.debug("getPdaForKontrakt(" + kontraktId + ") done with: " + (dfuer == null ? "null" : String.valueOf(dfuer.size())));
    	return list;
    }

    /** {@inheritDoc} */
	public List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId){
    	return em.createNamedQuery(Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE)
    		.setParameter("kontraktId", driftkontraktId)
    		.setParameter("rodetypeId", rodetypeId)
    		.getResultList();
    }
	
    /** {@inheritDoc} */
	public List<Rode> getAktiveRodeForKontrakt(Long driftkontraktId, Long rodetypeId){
    	return em.createNamedQuery(Rode.QUERY_FIND_AKTIVE_BY_KONTRAKT_AND_TYPE)
    		.setParameter("kontraktId", driftkontraktId)
    		.setParameter("rodetypeId", rodetypeId)
    		.setParameter("now", Clock.now())
    		.getResultList();
    }
	
	public List<Rode> getRodeForKontrakt(Long driftkontraktId, Long rodetypeId, boolean rodelengde){
		
		return null;
	}
	
	public Double getRodeLengde(Long rodeId){
		List<Veinettveireferanse> resultList = em.createQuery("SELECT v from Veinettveireferanse v where v.veinettreflinkseksjon.veinett.id = (select r.veinett.id from Rode r where r.id=:id)").setParameter("id", rodeId).getResultList();
		double lengde = 0;
		for (Veinettveireferanse vr:resultList){
			lengde+=getLength(vr);
		}
		return lengde;
	}
	
	/** {@inheritDoc} */
    public List<Rodetype> getRodetypeForKontrakt(Long driftkontraktId){
    	return rodegenerator.getRodetypeForKontrakt(driftkontraktId);
    }
	
	public Double getVeinettLengde(Long veinettId){
		List<Veinettveireferanse> resultList = em.createQuery("SELECT v FROM Veinettveireferanse v where v.veinettreflinkseksjon.veinett.id =:id").setParameter("id", veinettId).getResultList();
		double lengde = 0;
		for (Veinettveireferanse vr:resultList){
			lengde+=getLength(vr);
		}
		return lengde;
	}
	
	public void setFarger(){
		String[] farger = new String[]{"0x0000ff","0x00ff00", "0xff0000", "0x00ffff","0x006c36", "0xff8000", "0x8000ff","0x808040","0xff80ff", "0x400080"};
		List<Rode> rodeList = em.createQuery("Select r from Rode r").getResultList();
		int i =0;
		for (Rode r:rodeList){
			r.getVeinett().setGuiFarge(farger[i]);
			
			em.persist(r);
			System.out.println("saved rode:"+r.getNavn()+" med farge:"+farger[i]);
			i++;
			if (i==10)
				i=0;
		}
	}
}