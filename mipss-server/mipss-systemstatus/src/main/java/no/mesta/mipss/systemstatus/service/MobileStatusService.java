package no.mesta.mipss.systemstatus.service;

import no.mesta.mipss.systemstatus.dto.SystemStatus;

/**
 * System status service for the mobile server. 
 * 
 * @author Christian Wiik (Mesan)
 */
public class MobileStatusService extends AbstractSystemStatusService {

	/** {@inheritDoc} */
	public SystemStatus getSystemStatus() {
		return new SystemStatusServiceStub().getMobileServerStatus();
	}

}