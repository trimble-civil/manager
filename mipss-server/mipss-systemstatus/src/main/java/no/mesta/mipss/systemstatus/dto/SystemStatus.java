package no.mesta.mipss.systemstatus.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for transfer objects holding system status data. 
 * 
 * @author Christian Wiik (Mesan)
 */
public interface SystemStatus extends Serializable {
	
	/**
	 * Returns a list containing status items for the monitored component.
	 * 
	 * @return <tt>list</tt> - containing status items.
	 */
	List<StatusItem> getStatusItems();
	
}