package no.mesta.mipss.systemstatus.dto;

import java.io.Serializable;

/**
 * Interface for transfer objects holding a status parameter for a particular component.  
 * 
 * @author Christian Wiik (Mesan)
 */
public interface StatusItem extends Serializable {
	
	/**
	 * Returns the item.
	 * 
	 * @return the item.
	 */
	String getItem();
	
	/**
	 * @return the status
	 */
	String getStatus();
	
	/**
	 * Returns the status detail.
	 * 
	 * @return the status detail.
	 */
	StatusDetail getDetail();
	
	/**
	 * Returns the status flag.
	 * 
	 * @return the status item flag.
	 */
	StatusFlag getFlag();

}