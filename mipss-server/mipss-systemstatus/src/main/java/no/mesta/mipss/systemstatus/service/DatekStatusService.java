package no.mesta.mipss.systemstatus.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.systemstatus.dto.AbstractSystemStatus;
import no.mesta.mipss.systemstatus.dto.SimpleSystemStatus;
import no.mesta.mipss.systemstatus.dto.StatusItem;
import no.mesta.mipss.systemstatus.dto.SystemStatus;
import no.mesta.mipss.systemstatus.enumeration.StatusFlagEnum;

/**
 * System status service for the Datek server. 
 * 
 * @author Christian Wiik (Mesan)
 */
public class DatekStatusService extends AbstractSystemStatusService {

	/** {@inheritDoc} */
	public SystemStatus getSystemStatus() {
		StatusItem availabilityStatus = getAvailabilityStatus();
		
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(availabilityStatus);
		
		return status;
	}
	
	/**
	 * <p>Returns a status item representing system availability.</p>
	 * 
	 * <p>The conclusion is drawn upon the ability to open a <tt>HTTPURLConnection</tt> to the datek server URL.</p>
	 */
	private StatusItem getAvailabilityStatus() {
		StatusItem item;
		Konfigparam param = konfigParamService.hentEnForApp("mipss.admin", "datek.server.url");
		
		try {
			
			URL mapServerUrl = new URL(param.getVerdi());
			HttpURLConnection connection = (HttpURLConnection)mapServerUrl.openConnection();
			
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Tilkobling til '" + param.getVerdi() + "' OK", StatusFlagEnum.OK);
			
		} catch (MalformedURLException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + "' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		} catch (IOException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + "' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		}
		
		return item;
	}

}