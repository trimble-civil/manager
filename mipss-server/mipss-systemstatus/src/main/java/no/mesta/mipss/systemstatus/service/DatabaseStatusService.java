package no.mesta.mipss.systemstatus.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.persistence.datafangsutstyrbestilling.JobbInfo;
import no.mesta.mipss.systemstatus.dto.SimpleStatusItem;
import no.mesta.mipss.systemstatus.dto.SimpleSystemStatus;
import no.mesta.mipss.systemstatus.dto.SystemStatus;
import no.mesta.mipss.systemstatus.enumeration.StatusFlagEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * System status service for the MIPSS database. 
 * 
 * @author Christian Wiik (Mesan)
 */
public class DatabaseStatusService extends AbstractSystemStatusService {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private final EntityManager em;
	
	public DatabaseStatusService(EntityManager em) {
		this.em = em;
	}
	
	/** {@inheritDoc} */
	public SystemStatus getSystemStatus() {
		log.info("\nHey, I getSystemStatus()");
		
		Query q = em.createNamedQuery(JobbInfo.FIND_ALL);
		List<JobbInfo> jobbInfoList = q.getResultList();
		
		log.info("\nFikk laget query og hentet " + jobbInfoList.size() + " JobbInfo elementer fra DB");
		
		
		SimpleSystemStatus s = new SimpleSystemStatus();
		SimpleStatusItem si = (SimpleStatusItem)statusItemBuilder.buildStatusItem("JAU", "HAU: ", "SJAU", StatusFlagEnum.OK);
		
		s.addStatusItem(si);
		
		return s;
	}
	
}
