package no.mesta.mipss.systemstatus.dto;

/**
 * Basic status detail implementation.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SimpleStatusDetail implements StatusDetail {
	private String description;

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

}
