/**
 * 
 */
package no.mesta.mipss.jobbinfo;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.jobbinfo.Feilmelding;

/**
 * @author olehel
 *
 */
@SuppressWarnings("unchecked")
@Stateless(name = JobbinfoService.BEAN_NAME, mappedName="ejb/"+JobbinfoService.BEAN_NAME)
public class JobbinfoServiceBean implements JobbinfoService {
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;	


	/**
	 * Henter ut liste av alle ubehandlede meldinger
	 */
	@Override
	public List<Feilmelding> getUbehandledeMeldinger() {
		return em.createNamedQuery(Feilmelding.SOK_UBEHANDLET).setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
	}

	/**
	 * Henter ut liste av alle behandlede meldinger
	 */
	
	@Override
	public List<Feilmelding> getBehandledeMeldinger() {
		return em.createNamedQuery(Feilmelding.SOK_BEHANDLET).setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
	}

	@Override
	public Feilmelding updateFeilmelding(Feilmelding feilmelding) {
		feilmelding=em.merge(feilmelding);
		return feilmelding;
		
	}

}
