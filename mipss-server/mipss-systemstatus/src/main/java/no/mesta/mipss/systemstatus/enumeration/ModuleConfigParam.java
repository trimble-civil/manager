package no.mesta.mipss.systemstatus.enumeration;

/**
 * Enumerated values needed throughout the module.
 * 
 * @author Christian Wiik (Mesan)
 */
public enum ModuleConfigParam {
	
	KART_BASE_URL("studio.mipss.mapplugin", "mapserverBaseURL");
	
	private String appName;
	private String paramName;
	
	private ModuleConfigParam(String appName, String paramName) {
		this.appName = appName;
		this.paramName = paramName;
	}

	public String getParamName() {
		return paramName;
	}
	
	public String getAppName() {
		return appName;
	}

}