package no.mesta.mipss.systemstatus.service;

import no.mesta.mipss.systemstatus.dto.SystemStatus;

/**
 * System status service for the M2M server. 
 * 
 * @author Christian Wiik (Mesan)
 */
public class M2MStatusService extends AbstractSystemStatusService {

	/** {@inheritDoc} */
	public SystemStatus getSystemStatus() {
		return new SystemStatusServiceStub().getM2MSServerStatus();
	}

}