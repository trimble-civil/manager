package no.mesta.mipss.systemstatus.enumeration;

import java.awt.Color;

import no.mesta.mipss.systemstatus.dto.StatusFlag;

/**
 * Enumerated values representing status flags.
 * 
 * @author Christian Wiik (Mesan)
 */
public enum StatusFlagEnum implements StatusFlag {
	
	OK("OK", Color.GREEN),
	ATTENTION("Hmm", Color.YELLOW),
	FAILED("Feilet", Color.RED);
	
	private String description;
	private Color color;
	
	private StatusFlagEnum(String description, Color color) {
		this.description = description;
		this.color = color;
	}

	public String getDescription() {
		return description;
	}

	public Color getColor() {
		return color;
	}

}