package no.mesta.mipss.systemstatus.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic system status implementation.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SimpleSystemStatus extends AbstractSystemStatus {
	private List<StatusItem> statusItems;

	public SimpleSystemStatus() {
		statusItems = new ArrayList<StatusItem>();
	}

	/** {@inheritDoc} */
	public void addStatusItem(StatusItem statusItem) {
		statusItems.add(statusItem);
	}

	/** {@inheritDoc} */
	public void addStatusItems(List<StatusItem> statusItems) {
		this.statusItems.addAll(statusItems);
	}

	/** {@inheritDoc} */
	public List<StatusItem> getStatusItems() {
		return statusItems;
	}

}