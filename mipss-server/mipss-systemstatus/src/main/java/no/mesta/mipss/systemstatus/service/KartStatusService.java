package no.mesta.mipss.systemstatus.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.systemstatus.dto.AbstractSystemStatus;
import no.mesta.mipss.systemstatus.dto.SimpleSystemStatus;
import no.mesta.mipss.systemstatus.dto.StatusItem;
import no.mesta.mipss.systemstatus.dto.SystemStatus;
import no.mesta.mipss.systemstatus.enumeration.StatusFlagEnum;

/**
 * System status service for the map server. 
 * 
 * @author Christian Wiik (Mesan)
 */
public class KartStatusService extends AbstractSystemStatusService {

	/** {@inheritDoc} */
	public SystemStatus getSystemStatus() {
		
		StatusItem availabilityStatus = getAvailabilityStatus();
		StatusItem tileCacheStatus = getTileCacheStatus();
		StatusItem wmsStatus = getWmsStatus();
		
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(availabilityStatus);
		status.addStatusItem(tileCacheStatus);
		status.addStatusItem(wmsStatus);
		
		return status;
	}
	
	/**
	 * <p>Returns a status item representing system availability.</p>
	 * 
	 * <p>The conclusion is drawn upon the ability to open a <tt>HTTPURLConnection</tt> to the map server URL.</p>
	 */
	private StatusItem getAvailabilityStatus() {
		
		StatusItem item;
		Konfigparam param = konfigParamService.hentEnForApp("studio.mipss.mapplugin", "mapserverURL");
		
		try {
			
			URL mapServerUrl = new URL(param.getVerdi());
			HttpURLConnection connection = (HttpURLConnection)mapServerUrl.openConnection();
			
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Tilkobling til '" + param.getVerdi() + "' OK", StatusFlagEnum.OK);
			
		} catch (MalformedURLException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + "' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		} catch (IOException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + "' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		}
		
		return item;
	}

	/**
	 * <p>Returns a status item representing tile cache availability .</p>
	 * 
	 * <p>The conclusion is drawn upon the ability to open a <tt>HTTPURLConnection</tt> to the tile cache URL.</p>
	 */
	private StatusItem getTileCacheStatus() {
		
		StatusItem item;
		
		Konfigparam param = konfigParamService.hentEnForApp("studio.mipss.mapplugin", "mapserverBaseURL");
		
		try {

			URL mapServerUrl = new URL(param.getVerdi());
			HttpURLConnection connection = (HttpURLConnection)mapServerUrl.openConnection();
			Integer responseCode = connection.getResponseCode();
			
			if(responseCode == null) {
				item = statusItemBuilder.buildStatusItem("Tile Cache", "Nei", "Tilkobling til '" + param.getVerdi() + " ' mislyktes.", StatusFlagEnum.FAILED);
			}
			else if(responseCode == HttpURLConnection.HTTP_OK)  {
				item = statusItemBuilder.buildStatusItem("Tile Cache", "Ja", "Tilkobling til '" + param.getVerdi() + " ' OK (HTTP " + HttpURLConnection.HTTP_OK + ")", StatusFlagEnum.OK);
			}
			else {
				item = statusItemBuilder.buildStatusItem("Tile Cache", "Tja", "Tilkobling til '" + param.getVerdi() + " ' lykkes, men med en uventet respons kode (HTTP " + HttpURLConnection.HTTP_OK + ")", StatusFlagEnum.OK);
			}
			
		} catch (MalformedURLException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + " ' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		} catch (IOException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + " ' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		}
		
		return item;
	}
	
	/**
	 * <p>Returns a status item representing wms server availability .</p>
	 * 
	 * <p>The conclusion is drawn upon the ability to open a <tt>HTTPURLConnection</tt> to the wms server URL.</p>
	 */
	private StatusItem getWmsStatus() {
		
		StatusItem item;
		
		Konfigparam param = konfigParamService.hentEnForApp("studio.mipss.mapplugin", "mapserverWmsURL");
		
		try {

			URL mapServerUrl = new URL(param.getVerdi());
			HttpURLConnection connection = (HttpURLConnection)mapServerUrl.openConnection();
			Integer responseCode = connection.getResponseCode();
			
			if(responseCode == null) {
				item = statusItemBuilder.buildStatusItem("WMS Geoserver", "Nei", "Tilkobling til '" + param.getVerdi() + " ' mislyktes.", StatusFlagEnum.FAILED);
			}
			else if(responseCode == HttpURLConnection.HTTP_OK)  {
				item = statusItemBuilder.buildStatusItem("WMS Geoserver", "Ja", "Tilkobling til '" + param.getVerdi() + " ' OK (HTTP " + HttpURLConnection.HTTP_OK + ")", StatusFlagEnum.OK);
			}
			else {
				item = statusItemBuilder.buildStatusItem("WMS Geoserver", "Tja", "Tilkobling til '" + param.getVerdi() + " ' lykkes, men med en uventet respons kode (HTTP " + HttpURLConnection.HTTP_OK + ")", StatusFlagEnum.OK);
			}
			
		} catch (MalformedURLException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + " ' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		} catch (IOException e) {
			item = statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Tilkobling til '" + param.getVerdi() + " ' mislyktes. Årsak: Feil av type '" + e.getClass().getSimpleName() + "' oppstod med meldingen " + e.getMessage(), StatusFlagEnum.FAILED);
		}
		
		return item;
	}

}