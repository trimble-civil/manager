package no.mesta.mipss.systemstatus.service;

import no.mesta.mipss.systemstatus.MipssSystemStatusService;
import no.mesta.mipss.systemstatus.builder.StatusItemBuilder;
import no.mesta.mipss.systemstatus.dto.AbstractSystemStatus;
import no.mesta.mipss.systemstatus.dto.SimpleSystemStatus;
import no.mesta.mipss.systemstatus.dto.SystemStatus;
import no.mesta.mipss.systemstatus.enumeration.StatusFlagEnum;

/**
 * Service delegate POJO for the <tt>MipssMonitorService</tt> EJB.  
 * 
 * @author Christian Wiik (Mesan)
 */
public class SystemStatusServiceStub implements MipssSystemStatusService {

	private StatusItemBuilder statusItemBuilder;
	
	public SystemStatusServiceStub() {
		statusItemBuilder = new StatusItemBuilder();
	}
	
	public SystemStatus getDatabaseStatus() {
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Tilkobling OK", StatusFlagEnum.OK));
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tiltakspakke", "Ok", "OK", StatusFlagEnum.OK));
		status.addStatusItem(statusItemBuilder.buildStatusItem("DAU rapport", "Ok", "OK", StatusFlagEnum.OK));
		status.addStatusItem(statusItemBuilder.buildStatusItem("Utsendelse e-post", "Sen", "OK", StatusFlagEnum.ATTENTION));
		return status;
	}
	
	public SystemStatus getDatekServerStatus() {
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Kanarifuglen lever og alt er vel.", StatusFlagEnum.OK)); 
		return status;
	}
	
	public SystemStatus getKartServerStatus() {
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Kanarifuglen lever og alt er vel.", StatusFlagEnum.OK)); 
		return status;
	}
	
	public SystemStatus getKvernemotorStatus() {
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Kanarifuglen lever og alt er vel.", StatusFlagEnum.OK)); 
		return status;

	}
	
	public SystemStatus getM2MSServerStatus() {
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tilgjengelig", "Ja", "Kanarifuglen lever og alt er vel.", StatusFlagEnum.OK)); 
		return status;

	}
	
	public SystemStatus getMobileServerStatus() {
		AbstractSystemStatus status = new SimpleSystemStatus();
		status.addStatusItem(statusItemBuilder.buildStatusItem("Tilgjengelig", "Nei", "Kanarifuglen er død.", StatusFlagEnum.FAILED)); 
		return status;
	}

}