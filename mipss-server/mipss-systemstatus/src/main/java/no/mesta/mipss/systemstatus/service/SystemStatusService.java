package no.mesta.mipss.systemstatus.service;

import no.mesta.mipss.systemstatus.dto.SystemStatus;

/**
 * Interface for system status service implementations.
 * 
 * @author Christian Wiik (Mesan)
 */
public interface SystemStatusService {
	
	SystemStatus getSystemStatus();

}