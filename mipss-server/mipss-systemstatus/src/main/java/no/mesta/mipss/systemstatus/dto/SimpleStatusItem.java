package no.mesta.mipss.systemstatus.dto;


/**
 * Basic status item implementation.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SimpleStatusItem implements StatusItem {
	private String item;
	private String status;
	private StatusDetail detail;
	private StatusFlag flag;
	
	/** {@inheritDoc} */
	public String getItem() {
		return item;
	}
	
	/** {@inheritDoc} */
	public String getStatus() {
		return status;
	}
	
	/** {@inheritDoc} */
	public StatusDetail getDetail() {
		return detail;
	}

	/** {@inheritDoc} */
	public StatusFlag getFlag() {
		return flag;
	}
	
	public void setItem(String item) {
		this.item = item;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public void setDetail(StatusDetail detail) {
		this.detail = detail;
	}
	
	public void setFlag(StatusFlag flag) {
		this.flag = flag;
	}

}