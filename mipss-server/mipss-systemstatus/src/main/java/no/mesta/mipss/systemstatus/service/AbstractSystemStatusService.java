package no.mesta.mipss.systemstatus.service;

import javax.ejb.EJB;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.systemstatus.builder.StatusItemBuilder;
import no.mesta.mipss.util.ProxyUtil;

/**
 * Abstract base for system status services. 
 * 
 * @author Christian Wiik (Mesan)
 */
public abstract class AbstractSystemStatusService implements SystemStatusService {
	
	@EJB
	protected KonfigParam konfigParamService;
	protected StatusItemBuilder statusItemBuilder;
	
	/* Instansiate common services */
	public AbstractSystemStatusService() {
		konfigParamService = BeanUtil.lookup(KonfigParam.BEAN_NAME, KonfigParam.class);
		statusItemBuilder = new StatusItemBuilder();
	}
}