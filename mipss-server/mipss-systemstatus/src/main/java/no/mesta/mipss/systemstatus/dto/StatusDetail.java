package no.mesta.mipss.systemstatus.dto;

import java.io.Serializable;

/**
 * Interface for status detail implementations.
 * 
 * @author Christian Wiik (Mesan)
 */
public interface StatusDetail extends Serializable {
	
	String getDescription(); 

}
