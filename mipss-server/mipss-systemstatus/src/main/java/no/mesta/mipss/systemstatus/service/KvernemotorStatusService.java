package no.mesta.mipss.systemstatus.service;

import no.mesta.mipss.systemstatus.dto.SystemStatus;

/**
 * System status service for kvernemotor. 
 * 
 * @author Christian Wiik (Mesan)
 */
public class KvernemotorStatusService extends AbstractSystemStatusService {

	/** {@inheritDoc} */
	public SystemStatus getSystemStatus() {
		return new SystemStatusServiceStub().getKvernemotorStatus();
	}

}