package no.mesta.mipss.systemstatus.dto;

import java.awt.Color;

/**
 * Interface for status flag implementations.
 * 
 * @author Christian Wiik (Mesan)
 */
public interface StatusFlag {
	
	/**
	 * Gets the flag description.
	 * 
	 * @return the flag description.
	 */
	String getDescription();
	
	/**
	 * Gets the flag color.
	 * 
	 * @return the flag color.
	 */
	Color getColor();
	
}