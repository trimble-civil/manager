package no.mesta.mipss.systemstatus.builder;

import no.mesta.mipss.systemstatus.dto.SimpleStatusDetail;
import no.mesta.mipss.systemstatus.dto.SimpleStatusItem;
import no.mesta.mipss.systemstatus.dto.StatusItem;
import no.mesta.mipss.systemstatus.enumeration.StatusFlagEnum;

/**
 * Utility for building status items instances.
 * 
 * @author Christian Wiik (Mesan)
 */
public class StatusItemBuilder {
	
	public StatusItem buildStatusItem(String item, String status, String detail, StatusFlagEnum flag) {
		SimpleStatusDetail statusDetail = new SimpleStatusDetail();
		statusDetail.setDescription(detail);
		
		SimpleStatusItem statusItem = new SimpleStatusItem();
		statusItem.setItem(item);
		statusItem.setStatus(status);
		statusItem.setFlag(flag);
		statusItem.setDetail(statusDetail);
		
		return statusItem;
	}

}
