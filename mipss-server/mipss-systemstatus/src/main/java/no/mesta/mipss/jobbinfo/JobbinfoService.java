package no.mesta.mipss.jobbinfo;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.jobbinfo.Feilmelding;
/**
 * Henter ut feilmeldinger slik at brukeren skal kunne sette behandlet flagg i klienten.
 * Kan hente ut lister av både behandlede og ubehandlede meldinger.
 * @author olehel
 *
 */
@Remote
public interface JobbinfoService {
	public static final String BEAN_NAME = "JobbinfoServiceBean";
	
	List<Feilmelding> getUbehandledeMeldinger();
	List<Feilmelding> getBehandledeMeldinger();
	Feilmelding updateFeilmelding(Feilmelding feilmelding);
	
	
}
