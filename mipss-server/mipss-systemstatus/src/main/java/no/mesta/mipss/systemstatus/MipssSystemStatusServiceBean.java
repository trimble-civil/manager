package no.mesta.mipss.systemstatus;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.systemstatus.dto.SystemStatus;
import no.mesta.mipss.systemstatus.service.AbstractSystemStatusService;
import no.mesta.mipss.systemstatus.service.DatabaseStatusService;
import no.mesta.mipss.systemstatus.service.DatekStatusService;
import no.mesta.mipss.systemstatus.service.KartStatusService;
import no.mesta.mipss.systemstatus.service.KvernemotorStatusService;
import no.mesta.mipss.systemstatus.service.M2MStatusService;
import no.mesta.mipss.systemstatus.service.MobileStatusService;

/**
 * Tilbyr operasjoner for uthenting av overvåkningsinformasjon fra
 * forskjellige komponenter som MIPSS-løsningen er avhengig av.
 * 
 * @author Christian Wiik (Mesan)
 */
@Stateless(name = MipssSystemStatusService.BEAN_NAME, mappedName="ejb/"+MipssSystemStatusService.BEAN_NAME)
public class MipssSystemStatusServiceBean implements MipssSystemStatusService {
	
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;	
	
	private AbstractSystemStatusService databaseStatusService;
	private AbstractSystemStatusService datekStatusService;
	private AbstractSystemStatusService kartStatusService;
	private AbstractSystemStatusService kvernemotorStatusService;
	private AbstractSystemStatusService m2mStatusService;
	private AbstractSystemStatusService mobileStatusService;
	
	public MipssSystemStatusServiceBean() {
		databaseStatusService = new DatabaseStatusService(em);
		datekStatusService = new DatekStatusService();
		kartStatusService = new KartStatusService(); 
		kvernemotorStatusService = new KvernemotorStatusService();
		m2mStatusService = new M2MStatusService();
		mobileStatusService = new MobileStatusService();
	}
	
	public SystemStatus getDatabaseStatus() {
		return databaseStatusService.getSystemStatus();
	}

	public SystemStatus getDatekServerStatus() {
		return datekStatusService.getSystemStatus();
	}

	public SystemStatus getKartServerStatus() {
		return kartStatusService.getSystemStatus();
	}

	public SystemStatus getKvernemotorStatus() {
		return kvernemotorStatusService.getSystemStatus();
	}

	public SystemStatus getM2MSServerStatus() {
		return m2mStatusService.getSystemStatus();
	}

	public SystemStatus getMobileServerStatus() {
		return mobileStatusService.getSystemStatus();
	}

}