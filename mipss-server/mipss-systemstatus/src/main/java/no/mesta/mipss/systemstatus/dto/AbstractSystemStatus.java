package no.mesta.mipss.systemstatus.dto;

import java.util.List;

/**
 * Abstract base for system status implementations.
 * 
 * @author Christian Wiik (Mesan)
 */
public abstract class AbstractSystemStatus implements SystemStatus {
	/**
	 * Adds a status item to the list of status items.
	 */
	public abstract void addStatusItem(StatusItem statusItem);
	
	/**
	 * Adds a collection of status item to the list of status items.
	 */
	public abstract void addStatusItems(List<StatusItem> statusItems);

}