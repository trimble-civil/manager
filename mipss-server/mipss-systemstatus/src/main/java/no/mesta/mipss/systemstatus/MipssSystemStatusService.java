package no.mesta.mipss.systemstatus;

import javax.ejb.Remote;

import no.mesta.mipss.systemstatus.dto.SystemStatus;


/**
 * Tilbyr operasjoner for uthenting av overvåkningsinformasjon fra
 * forskjellige komponenter som MIPSS-løsningen er avhengig av.
 * 
 * @author Christian Wiik (Mesan)
 */
@Remote
public interface MipssSystemStatusService {
	
	public static final String BEAN_NAME = "MipssSystemStatusServiceBean";
	
	SystemStatus getDatabaseStatus();
	SystemStatus getDatekServerStatus();
	SystemStatus getKartServerStatus();
	SystemStatus getKvernemotorStatus();
	SystemStatus getM2MSServerStatus();
	SystemStatus getMobileServerStatus();
	
}