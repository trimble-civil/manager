package no.mesta.mipss.elrapp.adapter;

import no.mesta.mipss.elrapp.ElrappException;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

public interface ElrappAdapter {

    /**
     * Sender et R-skjema til elrapp
     *
     * @param o entitieten som skal transformeres og sendes til elrapp
     * @return ElrappResponse - inneholder dokumentVersjon og dokumentId fra elrapp
     * @throws ElrappException hvis noe går galt i overføringen til elrapp
     */
    ElrappResponse sendRSchema(MipssEntityBean<?> o, Bruker bruker) throws ElrappException;
}
