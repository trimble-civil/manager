package no.mesta.mipss.elrapp;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class NullHostnameVerifier implements HostnameVerifier {
    public boolean verify(String host, SSLSession arg1) {
        return true;
    }
}
