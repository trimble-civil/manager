package no.mesta.mipss.elrapp;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ElrappResponse implements Serializable {

    private final Long elrappDokumentId;
    private final Long elrappVersjon;
    private final String code;
    private final String rtype;

    public ElrappResponse(Long elrappDokumentId, Long elrappVersjon, String code, String rtype) {
        this.elrappDokumentId = elrappDokumentId;
        this.elrappVersjon = elrappVersjon;
        this.code = code;
        this.rtype = rtype;
    }

    public Long getElrappDokumentId() {
        return elrappDokumentId;
    }

    public Long getElrappVersjon() {
        return elrappVersjon;
    }

    public String getCode() {
        return code;
    }

    public String getRtype() {
        return rtype;
    }

}
