package no.mesta.mipss.elrapp;

public class ElrappValidationException extends ElrappException {

    public ElrappValidationException() {
    }

    public ElrappValidationException(String message) {
        super(message);
    }

    public ElrappValidationException(Throwable cause) {
        super(cause);
    }

    public ElrappValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
