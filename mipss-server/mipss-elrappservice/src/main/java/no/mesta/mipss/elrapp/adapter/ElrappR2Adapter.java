package no.mesta.mipss.elrapp.adapter;

import generated.Submission;
import no.mesta.mipss.elrapp.CommonHelper;
import no.mesta.mipss.elrapp.ElrappException;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.elrapp.facade.ElrappFacade;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseTrafikktiltak;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import no.nois.elrapp.elrapp_common.DateTime;
import no.nois.elrapp.elrapp_form_r2_2.ObjectFactory;
import no.nois.elrapp.elrapp_form_r2_2.R22;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Adapter for R2 skjema to Elrapp
 *
 * @author harkul
 */
class ElrappR2Adapter implements ElrappAdapter {

    private static final Logger log = LoggerFactory.getLogger(ElrappR2Adapter.class);
    private final ElrappFacade elrapp;

    ElrappR2Adapter(ElrappFacade elrapp) {
        this.elrapp = elrapp;
    }

    @Override
    public ElrappResponse sendRSchema(MipssEntityBean<?> o, Bruker bruker) throws ElrappException {
        try {
            // Create form
            Hendelse h = (Hendelse) o;
            R22 form = createForm(h);
            Long versjon = h.getElrappVersjon();
            if (versjon != null) {
                versjon++;
                form.setVersion(BigInteger.valueOf(versjon));
            } else {
                form.setVersion(BigInteger.valueOf(0));
            }

            Submission sub = CommonHelper.createSubmission(CommonHelper.createLogin(h.getSak().getKontrakt(), bruker), form);
            return elrapp.sendRscheme(ElrappTypeR.Type.R2, sub);
        } catch (Exception ex) {
            log.error("Error occured sending schema of type R2", ex);
            throw new ElrappException("Could not send R2 schema! "+ex.getMessage());
        }
    }

    private static R22 createForm(Hendelse hendelse) {
        ObjectFactory factory = new ObjectFactory();
        R22 form = factory.createR22();
        form.setSubject(hendelse.getSkjemaEmne());
        Punktveiref veiref = hendelse.getSak().getVeiref();
        form.setRoadReference(CommonHelper.createRoadReference(veiref, hendelse.getSak()));
        form.setDocumentReference(hendelse.getElrappDokumentIdent());

        form.setDateTime(CommonHelper.createDateTime(hendelse.getOwnedMipssEntity().getOpprettetDato()));
        if (hendelse.getAarsak() != null)
            form.setCause(hendelse.getAarsak().getElrappId());

        if(hendelse.getArbeidType()!=null) {
            form.setRepair(hendelse.getArbeidType().getElrappId());
        }
        form.setPlanForRepair(hendelse.getUtbedringplan() != null ? hendelse.getUtbedringplan() : "");
        form.setPlannedReparedBy(hendelse.getPlanlagtUtbedret() != null ? getElrappDate(hendelse.getPlanlagtUtbedret()) : null);
        form.setIsRepaired(hendelse.isUtbedret());

        StringBuilder beskrivelse = new StringBuilder();
        beskrivelse.append(hendelse.getBeskrivelse());

        R22.Process process = factory.createR22Process();
        if(hendelse.getProsess() != null) {
            process.setProcessRec(hendelse.getProsess().getElrappId());
            if (hendelse.getObjektType() != null) {
                process.setObjectRec(hendelse.getObjektType().getElrappId());
            }
            if (hendelse.getObjektAvvikType() != null) {
                process.setDeviation(hendelse.getObjektAvvikType().getElrappId());
            }
            if (hendelse.getObjektAvvikKategori() != null && hendelse.getObjektAvvikKategori().getElrappId() != null) {
                process.setDeviationCategory(hendelse.getObjektAvvikKategori().getElrappId().intValue());
            }
            if (hendelse.getObjektStedTypeAngivelse() != null && hendelse.getObjektStedTypeAngivelse().getElrappId() != null) {
                process.setLocationOrType(hendelse.getObjektStedTypeAngivelse().getElrappId().intValue());
            }
        }
        form.setProcess(process);

        form.setComment(hendelse.getMerknad() != null ? hendelse.getMerknad() : "");

        for (Bilde bilde : hendelse.getBilder()) {
            if (bilde.getBeskrivelse() != null) {
                beskrivelse.append(bilde.getFilnavn() + ": " + bilde.getBeskrivelse() + ".");
            }
        }

        if (beskrivelse.toString().length() < 2000) {
            form.setDescriptionOfEventAndActions(beskrivelse.toString());
        } else {
            form.setDescriptionOfEventAndActions(beskrivelse.toString().substring(0, 1999));
        }

        form.setAttachments(CommonHelper.createAttachments(hendelse.getBilder()));
        return form;
    }

    private static DateTime getElrappDate(Date planlagtUtbedret) {
        DateTime date = new DateTime();
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(planlagtUtbedret);
        date.setYear(c.get(Calendar.YEAR));
        date.setMonth(c.get(Calendar.MONTH)+1);
        date.setDay(c.get(Calendar.DAY_OF_MONTH));
        date.setHour(c.get(Calendar.HOUR_OF_DAY));
        date.setMinute(c.get(Calendar.MINUTE));
        date.setSeconds(c.get(Calendar.SECOND));
        return date;
    }

    /**
     * Lager en streng med trafikktiltakene til hendelsen
     *
     * @param tiltak
     * @return
     */
    private static String createActionsTaken(List<HendelseTrafikktiltak> tiltak) {
        StringBuilder sb = new StringBuilder();
        for (HendelseTrafikktiltak tt : tiltak) {
            sb.append(tt.getTrafikktiltak().getNavn() + " " + tt.getBeskrivelse());
            sb.append("\n");
        }
        return sb.toString();
    }
}
