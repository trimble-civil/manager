package no.mesta.mipss.elrapp.adapter;

import generated.Submission;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.elrapp.CommonHelper;
import no.mesta.mipss.elrapp.ElrappException;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.elrapp.facade.ElrappFacade;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.nois.elrapp.elrapp_form_r11.Form;
import no.nois.elrapp.elrapp_form_r11.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.List;

class ElrappR11Adapter implements ElrappAdapter {

    private final Logger log = LoggerFactory.getLogger(ElrappR11Adapter.class);

    private final ElrappFacade elrapp;
    private final NvdbWebservice nvdb;

    public static final Long SKREDTYPE_ID = Long.valueOf(1);
    public static final Long LOSNEOMRADE_ID = Long.valueOf(2);
    public static final Long VOLUM_ID = Long.valueOf(3);
    public static final Long HOYDE_ID = Long.valueOf(4);
    public static final Long SKADE_ID = Long.valueOf(5);
    public static final Long STENGING_ID = Long.valueOf(6);
    public static final Long BLOKKERTVEI_ID = Long.valueOf(7);
    public static final Long VIND_ID = Long.valueOf(8);
    public static final Long SNO_ID = Long.valueOf(9);
    public static final Long REGN_ID = Long.valueOf(10);
    public static final Long FELT_ID = Long.valueOf(11);
    public static final Long STENGNINGSRETNING_ID = Long.valueOf(12);
    public static final Long STENGNINGSKRED_ID = Long.valueOf(13);

    ElrappR11Adapter(ElrappFacade elrapp, NvdbWebservice nvdb) {
        this.elrapp = elrapp;
        this.nvdb = nvdb;
    }

    @Override
    public ElrappResponse sendRSchema(MipssEntityBean<?> o, Bruker bruker) throws ElrappException {
        try {
            R11Helper helper = new R11Helper(nvdb);
            Skred skred = (Skred) o;

            Form form = createForm(skred, helper);

            Submission sub = CommonHelper.createSubmission(CommonHelper.createLogin(skred.getSak().getKontrakt(), bruker), form);
            return elrapp.sendRscheme(ElrappTypeR.Type.R11, sub);
        } catch (Exception ex) {
            log.error("Error occured sending schema of type R11", ex);
            throw new ElrappException("Could not send R11 schema! "+ ex.getMessage());
        }
    }

    private static Form createForm(Skred skred, R11Helper helper) throws ElrappException {
        ObjectFactory fact = new ObjectFactory();
        Form form = fact.createForm();
        form.setAttachments(CommonHelper.createAttachments(skred.getBilder()));
        form.setDocumentReference(skred.getElrappDokumentIdent());
        form.setSubject(skred.getSkjemaEmne());
        form.setWeatherConditionOnRoad(helper.createWeatherConditionOnRoad(skred));

        form.setSchemeType(helper.createSchemeType(skred, fact));

        Long versjon = skred.getElrappVersjon();
        if (versjon != null) {
            versjon++;
            form.setVersion(BigInteger.valueOf(versjon));
        } else {
            form.setVersion(BigInteger.valueOf(0));
        }

        return form;
    }

    public List<String> validate(MipssEntityBean<?> o) throws ElrappException {
        // TODO Auto-generated method stub
        return null;
    }


}
