package no.mesta.mipss.elrapp;

import generated.Submission;
import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.nois.elrapp.elrapp_common.Attachments;
import no.nois.elrapp.elrapp_common.Attachments.Attachment;
import no.nois.elrapp.elrapp_common.DateTime;
import no.nois.elrapp.elrapp_common.Login;
import no.nois.elrapp.elrapp_common.RoadReference;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.awt.image.BufferedImage;
import java.io.File;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CommonHelper {

    public static final Submission createSubmission(Login login, Object form) {
        Submission s = new Submission();
//		s.setForm((Form)form);
//		s.setLogin(login);
        s.getContent().add(login);
        s.getContent().add(form);

        return s;
    }


    /**
     * Lager vedlegg til r2 rapporten
     * koder bildene som base64
     *
     * @param bilder
     */
    public static final Attachments createAttachments(List<Bilde> bilder) {
        Attachments at = new Attachments();
        if (bilder == null || bilder.size() == 0) {
			at.getAttachment(); // Causes empty list be created (which service requires)
            return at;
        }

        for (Bilde b : bilder) {
            Attachment a = new Attachment();
            a.setFileName(b.getFilnavn());
            try {
                BufferedImage timestampImage = ImageUtil.timestampImage(ImageUtil.bytesToImage(b.getBildeLob()), b.getOwnedMipssEntity().getOpprettetDato());
                a.setBase64EncodedData(ImageUtil.compressImage(timestampImage, 0.7f).toByteArray());
            } catch (Exception e) {
                e.printStackTrace();
            }
            a.setMimeType("image/jpeg");
            at.getAttachment().add(a);
        }
        return at;
    }

    public static void main(String[] args) throws Exception {
        byte[] b = FileUtils.readFileToByteArray(new File("D:/b64test/bilde.base"));
        byte[] b6 = encode(b);
        FileUtils.writeByteArrayToFile(new File("D:/b64test/bilde.base1"), b6);
//		byte[] base = FileUtils.readFileToByteArray(new File("d:/b64test/bilde.base"));
//		byte[] decode = decode(base);
//		FileUtils.writeByteArrayToFile(new File("d:/b64test/bilde.base.jpg"), decode);


    }

    /**
     * Koder en byte[] til base64
     *
     * @param b
     * @return
     * @throws Exception
     */
    public static byte[] encode(byte[] b) throws Exception {
        byte[] encoded = Base64.encodeBase64(b);
        return encoded;
    }

    public static byte[] decode(byte[] str) {
        byte[] decoded = Base64.decodeBase64(str);
        return decoded;
    }

    /**
     * Lager et DateTime objekt med dato fra hendelsen
     *
     * @param dato
     * @return
     */
    public static final DateTime createDateTime(Date dato) {
        if (dato == null)
            return null;
        Calendar c = Calendar.getInstance();
        c.setTime(dato);

        DateTime dt = new DateTime();
        dt.setDay(c.get(Calendar.DAY_OF_MONTH));
        dt.setMonth(c.get(Calendar.MONTH) + 1);
        dt.setYear(c.get(Calendar.YEAR));
        dt.setHour(c.get(Calendar.HOUR_OF_DAY));
        dt.setMinute(c.get(Calendar.MINUTE));
        dt.setSeconds(c.get(Calendar.SECOND));
        return dt;
    }

    /**
     * Lager et RoadReference objekt fra punktveiref
     *
     * @param veiref
     * @return
     */
    public static final RoadReference createRoadReference(Punktveiref veiref, Sak sak) {
        RoadReference rr = new RoadReference();
        rr.setCounty(BigInteger.valueOf(veiref.getFylkesnummer()));
        rr.setMunicipality(BigInteger.valueOf(veiref.getKommunenummer()));
        rr.setLocationName(sak.getSkadested());
        rr.setRoadCategory(veiref.getVeikategori());
        rr.setRoadStatus(veiref.getVeistatus());
        rr.setRoadNumber(BigInteger.valueOf(veiref.getVeinummer()));
        rr.setHp(BigInteger.valueOf(veiref.getHp()));
        rr.setMeter(BigInteger.valueOf(veiref.getMeter()));
        return rr;
    }

    public static final Login createLogin(Driftkontrakt dk, Bruker bruker) {
        Login l = new Login();
        l.setUserID(bruker.getElrappIdent());
        l.setContractID(dk.getElrappKontraktIdent());
        Calendar c = Calendar.getInstance();
        c.setTime(dk.getElrappStartDato());
        int yearStart = c.get(Calendar.YEAR);
        c.setTime(dk.getElrappSluttDato());
        int yearEnd = c.get(Calendar.YEAR);

        try {
            XMLGregorianCalendar startYear = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(yearStart, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);
            XMLGregorianCalendar endYear = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(yearEnd, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);
            l.setContractStartYear(startYear);
            l.setContractEndYear(endYear);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return l;
    }
}
