package no.mesta.mipss.elrapp;

/**
 * Exception som kastes hvis noe går galt med innsendingen av data til elrapp
 *
 * @author harkul
 */
public class ElrappException extends Exception {

    public ElrappException() {
    }

    public ElrappException(String message) {
        super(message);
    }

    public ElrappException(Throwable cause) {
        super(cause);
    }

    public ElrappException(String message, Throwable cause) {
        super(message, cause);
    }

}
