package no.mesta.mipss.elrapp.adapter;

import no.mesta.mipss.elrapp.CommonHelper;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.r.r11.SkredEgenskapverdi;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;
import no.nois.elrapp.elrapp_common.Coordinate;
import no.nois.elrapp.elrapp_form_r11.*;

import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static no.mesta.mipss.elrapp.adapter.ElrappR11Adapter.*;

public class R11Helper {

    private final NvdbWebservice nvdb;

    public R11Helper(NvdbWebservice nvdb) {
        this.nvdb = nvdb;
    }

    /**
     * Oppretter beskrivelse av v�r p� vei
     *
     * @param s
     * @return
     */
    public Form.WeatherConditionOnRoad createWeatherConditionOnRoad(Skred s) {
        Form.WeatherConditionOnRoad w = new Form.WeatherConditionOnRoad();
        String sno = getSkredEgenskapverdi(s.getSkredEgenskapList(), SNO_ID);
        String regn = getSkredEgenskapverdi(s.getSkredEgenskapList(), REGN_ID);
        boolean nedborSatt = false;
        if (s.getIngenNedbor() == null || s.getIngenNedbor() == false) {
            if (sno == null && regn == null) {
                w.setDownFall("U");
                nedborSatt = true;
            }
        }
        if (s.getIngenNedbor() != null && s.getIngenNedbor() == true) {
            w.setDownFall("Y");
            nedborSatt = true;
        }
        if (!nedborSatt) {
            w.setRain(regn);
            w.setSnow(sno);
        }

        w.setTemperature_0020(s.getTemperatur());
        w.setWindDirection(getSkredEgenskapverdi(s.getSkredEgenskapList(), VIND_ID));
        return w;
    }

    public Form.SchemeType createSchemeType(Skred skred, ObjectFactory fact) {
        Form.SchemeType type = fact.createFormSchemeType();
        if (skred.getStengtPgaSkredfare() != null && skred.getStengtPgaSkredfare()) {
            type.setLandslideRisk(createLandslideRisk(skred, fact));
        } else {
            type.setLandslide(createLandslide(skred, fact));
        }
        return type;
    }

    private Form.SchemeType.LandslideRisk createLandslideRisk(Skred skred, ObjectFactory fact) {
        Form.SchemeType.LandslideRisk ls = fact.createFormSchemeTypeLandslideRisk();
        ls.setBlockedRoads(createBlockedRoad(skred, fact));
        return ls;
    }

    private Form.SchemeType.Landslide createLandslide(Skred skred, ObjectFactory fact) {
        Form.SchemeType.Landslide ls = fact.createFormSchemeTypeLandslide();
        ls.getContent().add(fact.createLocation(skred.getSak().getSkadested()));
        ls.getContent().add(fact.createDateTime(CommonHelper.createDateTime(skred.getSkredDato())));
        ls.getContent().add(createLandslideRange(skred, fact));
        ls.getContent().add(createLandslideDescription(skred, fact));
        if ((skred.getStengtPgaSkredfare() != null && skred.getStengtPgaSkredfare())) {
            ls.getContent().add(createBlockedRoad(skred, fact));
        }
        ls.getContent().add(fact.createLandslideClosing(convertSkredstenging(getSkredEgenskapverdiObj(skred.getSkredEgenskapList(), STENGNINGSKRED_ID))));
        return ls;
    }

    private String convertSkredstenging(Egenskapverdi skredstenging) {
        if (skredstenging == null)
            return null;
        //58=Y
        //59=N
        //60=S
        Long id = skredstenging.getId();
        if (Long.valueOf(58).equals(id)) {
            return "Y";
        }
        if (Long.valueOf(59).equals(id)) {
            return "N";
        }
        if (Long.valueOf(60).equals(id)) {
            return "S";
        }
        return null;
    }

    private LandslideDescription createLandslideDescription(Skred skred, ObjectFactory fact) {
        LandslideDescription desc = fact.createLandslideDescription();
        List<SkredEgenskapverdi> sl = skred.getSkredEgenskapList();
        desc.setType(getSkredEgenskapverdi(sl, SKREDTYPE_ID));
        desc.setArea(getSkredEgenskapverdi(sl, LOSNEOMRADE_ID));
        desc.setCubicMetersOnRoad(getSkredEgenskapverdi(sl, VOLUM_ID));
        desc.setTotalAvelangeAreaInSqMeters(skred.getAnslagTotalSkredstorrelse());
        desc.setHeight(getSkredEgenskapverdi(sl, HOYDE_ID));
        desc.setBlockedRoadLength(getSkredEgenskapverdi(sl, BLOKKERTVEI_ID));
        desc.setDamages(createDamages(skred, fact));
        return desc;
    }

    /**
     * Oppretter skader som er tilknyttet skredet
     *
     * @param s
     * @return
     */
    private LandslideDescription.Damages createDamages(Skred s, ObjectFactory fact) {
        LandslideDescription.Damages d = fact.createLandslideDescriptionDamages();
        List<Egenskapverdi> skredEgenskapList = getSkredEgenskapList(s.getSkredEgenskapList(), SKADE_ID);
        for (Egenskapverdi e : skredEgenskapList) {
            d.getDamage().add(e.getVerdi());
        }

        return d;
    }

    private LandslideRange createLandslideRange(Skred skred, ObjectFactory fact) {
        List<Punktstedfest> sl = skred.getSak().getStedfesting();
        Punktstedfest from = sl.get(0);
        Punktstedfest to = sl.get(1);
        LandslideRange range = fact.createLandslideRange();
        range.setFrom(createLandslideRangeFrom(from.getVeiref(), skred.getSak(), fact));
        range.setTo(createLandslideRangeTo(to.getVeiref(), skred.getSak(), fact));
        range.setLane(getSkredEgenskapverdi(skred.getSkredEgenskapList(), FELT_ID));
        return range;
    }

    private no.nois.elrapp.elrapp_form_r11.LandslideRange.From createLandslideRangeFrom(Punktveiref v, Sak s, ObjectFactory fact) {
        no.nois.elrapp.elrapp_form_r11.LandslideRange.From from = fact.createLandslideRangeFrom();
        from.setCoordinate(createCoordinate(v));
        from.setRoadReference(CommonHelper.createRoadReference(v, s));
        return from;
    }

    private no.nois.elrapp.elrapp_form_r11.LandslideRange.To createLandslideRangeTo(Punktveiref v, Sak s, ObjectFactory fact) {
        no.nois.elrapp.elrapp_form_r11.LandslideRange.To to = fact.createLandslideRangeTo();
        to.setCoordinate(createCoordinate(v));
        to.setRoadReference(CommonHelper.createRoadReference(v, s));
        return to;
    }

    /**
     * Oppretter blokkert vei og informasjon om stenging.
     *
     * @param s
     * @return
     */
    private BlockedRoads createBlockedRoad(Skred s, ObjectFactory fact) {
        BlockedRoads br = fact.createBlockedRoads();
        List<SkredEgenskapverdi> sl = s.getSkredEgenskapList();

        BlockedRoads.BlockedRoad b = fact.createBlockedRoadsBlockedRoad();
        b.setRange(createClosedRange(s, fact));
        b.setDateTimeClosedFrom(CommonHelper.createDateTime(s.getStengtDato()));
        b.setDateTimeClosedTo(CommonHelper.createDateTime(s.getAapnetDato()));
        b.setClosingDirection(getSkredEgenskapverdi(sl, STENGNINGSRETNING_ID));
        b.setClosingType(getSkredEgenskapverdi(sl, STENGING_ID));
        br.getBlockedRoad().add(b);
        return br;
    }

    /**
     * Oppretter strekningen skredet g�r over
     *
     * @param s
     * @return
     */
    private BlockedRoads.BlockedRoad.Range createClosedRange(Skred s, ObjectFactory fact) {
        BlockedRoads.BlockedRoad.Range r = fact.createBlockedRoadsBlockedRoadRange();
        List<Punktstedfest> sl = s.getSak().getStedfesting();
        Punktstedfest from = sl.get(0);
        Punktstedfest to = sl.get(1);
        r.setFrom(createFrom(from.getVeiref(), s.getSak(), fact));
//		r.setLane(getSkredEgenskapverdi(s.getSkredEgenskapList(), FELT_ID));

        r.setTo(createTo(to.getVeiref(), s.getSak(), fact));
        return r;
    }

    /**
     * Lager til-veiref
     *
     * @param v
     * @return
     */
    private BlockedRoads.BlockedRoad.Range.To createTo(Punktveiref v, Sak h, ObjectFactory fact) {
        BlockedRoads.BlockedRoad.Range.To t = fact.createBlockedRoadsBlockedRoadRangeTo();
        t.setCoordinate(createCoordinate(v));
        t.setRoadReference(CommonHelper.createRoadReference(v, h));
        return t;
    }

    /**
     * Lager fra-veiref
     *
     * @param v
     * @param fact
     * @return
     */
    private BlockedRoads.BlockedRoad.Range.From createFrom(Punktveiref v, Sak h, ObjectFactory fact) {
        BlockedRoads.BlockedRoad.Range.From f = fact.createBlockedRoadsBlockedRoadRangeFrom();
        f.setCoordinate(createCoordinate(v));
        f.setRoadReference(CommonHelper.createRoadReference(v, h));
        return f;
    }

    /**
     * Oppretter en koordinat
     *
     * @param pv
     * @return
     */
    private Coordinate createCoordinate(Punktveiref pv) {
        Point p = getCoordinateFromNvdb(pv);
        if (p != null) {
            Coordinate c = new Coordinate();
            c.setEast(BigDecimal.valueOf(p.getX()));
            c.setNorth(BigDecimal.valueOf(p.getY()));
            return c;
        }
        return null;
    }

    /**
     * Fors�ker � hente koordinat til punktveiref fra NVDB
     *
     * @param pv
     * @return
     */
    private Point getCoordinateFromNvdb(Punktveiref pv) {
        if (pv != null) {
            Veinettveireferanse c1 = convertFromPunktveiref(pv);

            List<Veinettreflinkseksjon> v1 = nvdb.getReflinkSectionsWithinRoadref(c1);
            List<ReflinkStrekning> geo1 = nvdb.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(v1);
            if (geo1.size() > 0) {
                ReflinkStrekning rs = geo1.get(0);
                Point p1 = rs.getVectors()[0];
                return p1;
            }
        }
        return null;
    }

    /**
     * Konverter et Punktveiref til Veinettveireferanse
     *
     * @param pv
     * @return
     */
    private Veinettveireferanse convertFromPunktveiref(Punktveiref pv) {
        Veinettveireferanse c1 = new Veinettveireferanse();
        c1.setFraKm(Double.valueOf((double) pv.getMeter() / 1000));
        c1.setTilKm(Double.valueOf((double) pv.getMeter() / 1000));
        c1.setFylkesnummer(Long.valueOf(pv.getFylkesnummer()));
        c1.setKommunenummer(Long.valueOf(pv.getKommunenummer()));
        c1.setHp(Long.valueOf(pv.getHp()));
        c1.setVeikategori(pv.getVeikategori());
        c1.setVeistatus(pv.getVeistatus());
        c1.setVeinummer(Long.valueOf(pv.getVeinummer()));
        return c1;
    }


    /**
     * Lager en string med kommentarene til bildene
     *
     * @param s
     * @return TODO fjerne denne?
     */
    private String getPictureComments(Skred s) {

        StringBuilder sb = new StringBuilder();
        sb.append(s.getKommentar());
        sb.append("   \n");
        for (Bilde b : s.getBilder()) {
            sb.append(b.getFilnavn());
            sb.append(":");
            sb.append(b.getBeskrivelse());
            sb.append("   \n");
        }
        return sb.toString();
    }

    /**
     * Henter verdien til Skredegenskapverdi med angitt id fra listen.
     *
     * @param all
     * @param id
     * @return
     */
    private String getSkredEgenskapverdi(List<SkredEgenskapverdi> all, Long id) {
        for (SkredEgenskapverdi v : all) {
            if (v.getEgenskapverdi().getEgenskap().getId().equals(id)) {
                return v.getEgenskapverdi().getVerdi();
            }
        }
        return null;
    }

    private Egenskapverdi getSkredEgenskapverdiObj(List<SkredEgenskapverdi> all, Long id) {
        for (SkredEgenskapverdi v : all) {
            if (v.getEgenskapverdi().getEgenskap().getId().equals(id)) {
                return v.getEgenskapverdi();
            }
        }
        return null;
    }

    private List<Egenskapverdi> getSkredEgenskapList(List<SkredEgenskapverdi> all, Long id) {
        List<Egenskapverdi> avTypen = new ArrayList<Egenskapverdi>();
        for (SkredEgenskapverdi v : all) {
            if (v.getEgenskapverdi().getEgenskap().getId().equals(id)) {
                avTypen.add(v.getEgenskapverdi());
            }
        }
        return avTypen;
    }
}
