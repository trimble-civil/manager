package no.mesta.mipss.elrapp.facade;

import generated.Submission;
import no.mesta.mipss.elrapp.*;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import no.nois.IElrappContractorService;
import no.nois.SendRSchemeResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractElrappFacade implements ElrappFacade {
    private static final Logger log = LoggerFactory.getLogger(AbstractElrappFacade.class);
    private static final String NIL_REMOVE = "xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";

    /**
     * Sender r-schema til elrapp
     *
     * @param submission
     * @return ElrappResponse - inneholder dokumentVersjon og dokumentId fra elrapp
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws ElrappException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public ElrappResponse sendRscheme(ElrappTypeR.Type type, Submission submission) throws NoSuchAlgorithmException, KeyManagementException, ElrappException, SAXException, ParserConfigurationException, IOException {
        SSLContext context = SSLContext.getInstance("SSLv3");
        TrustManager[] trustManagerArray = {new X509CertTrustManager()};
        context.init(null, trustManagerArray, null);

        HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(new NullHostnameVerifier());

        String xml = createXml(submission);
        log.debug("XML created for submission: {}", xml);
        return sendRScheme(type, xml);
    }

    public abstract Object getService();

    /**
     * Sender schema til elrapp. Og leter etter documentReferansen til r-skjemaet. Ved feil
     * returneres null foreløpig
     *
     * @param xml
     * @return elrappDocumentId
     */
    private ElrappResponse sendRScheme(ElrappTypeR.Type type, String xml) throws ElrappException {
        if (log.isDebugEnabled()) {
            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        }
        Object service = getService();
        List<Object> content;
        try {
            SendRSchemeResponse.SendRSchemeResult result = ((IElrappContractorService) service).sendRScheme(xml);
            content = new ArrayList<Object>(result.getContent());
        } catch (Exception t) {
            log.error("Unable to send schema to Elrapp", t);
            throw new ElrappException(t);
        }
        return ElrappResponseParser.parse(type, (Node) content.get(0));
    }

    /**
     * Lager xml ut av et objekt. Objektet må være JAX-annotert
     *
     * @param o
     * @return
     */
    private static String createXml(Object o) {
        StringWriter w = new StringWriter();
        try {
            JAXBContext ctx = JAXBContext.newInstance(o.getClass());
            Marshaller m = ctx.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(o, w);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        //TODO desverre en nasty hack vi må gjøre for å lage en xml som er kompatibel med validatoren til ELRAPP...
        String xml = StringUtils.remove(w.toString(), NIL_REMOVE);
        return xml;
    }
}
