package no.mesta.mipss.elrapp.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ElrappFacadeFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ElrappFacadeFactory.class);

    private static final String PROD = "PROD";
    private static final String TEST = "TEST";
    private static final String PROD_WCF = "PROD_WCF";
    private static final String TEST_WCF = "TEST_WCF";

    private ElrappFacadeFactory() {
    }

    public static ElrappFacade createElrappFacade(String mode){
        String url, username = "mesta_ws", password = "mesta_ws";
		if(PROD.equals(mode) || PROD_WCF.equals(mode)) {
                url = "https://elrapp.nois.no/contractorservice/ElrappContractorService.svc";
        } else if(TEST.equals(mode) || TEST_WCF.equals(mode)) {
                url = "https://elrapp-kurs.nois.no/contractorservice/ElrappContractorService.svc";
        } else {
            throw new IllegalArgumentException("Unsupported elrapp mode: " + mode);
        }
        return new ElrappFacadeWCF(url, username, password);
    }
}

	