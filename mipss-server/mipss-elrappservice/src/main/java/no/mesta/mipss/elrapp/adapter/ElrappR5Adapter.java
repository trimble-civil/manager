package no.mesta.mipss.elrapp.adapter;

import generated.Submission;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.elrapp.CommonHelper;
import no.mesta.mipss.elrapp.ElrappException;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.elrapp.facade.ElrappFacade;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.ForsikringsskadeBilde;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import no.nois.elrapp.elrapp_form_r5.Form;
import no.nois.elrapp.elrapp_form_r5.Form.TortFeasor;
import no.nois.elrapp.elrapp_form_r5.Form.TortFeasorIdentification;
import no.nois.elrapp.elrapp_form_r5.Form.TortFeasorIdentification.SalvageCompanies;
import no.nois.elrapp.elrapp_form_r5.Form.TortFeasorIdentification.SalvageCompanies.SalvageCompany;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for R5 skjema to Elrapp
 *
 * @author harkul
 */
class ElrappR5Adapter implements ElrappAdapter {

    private static final Logger log = LoggerFactory.getLogger(ElrappR5Adapter.class);
    private final ElrappFacade elrapp;

    ElrappR5Adapter(ElrappFacade elrapp) {
        this.elrapp = elrapp;
    }

    @Override
    public ElrappResponse sendRSchema(MipssEntityBean<?> o, Bruker bruker) throws ElrappException {
        try {
            Forsikringsskade fs = (Forsikringsskade) o;

            Form form = createForm(fs);

            Submission sub = CommonHelper.createSubmission(CommonHelper.createLogin(fs.getSak().getKontrakt(), bruker), form);
            return elrapp.sendRscheme(ElrappTypeR.Type.R5, sub);
        } catch (Exception e) {
            log.error("Error occured sending schema of type R11", e);
            throw new ElrappException("Could not send R5 schema! "+e.getMessage());
        }
    }

    /**
     * Send et R5 skjema til elrapp
     *
     *
     *@param fs  @return
     * @throws ElrappException
     */
    private static Form createForm(Forsikringsskade fs) throws Exception {
        Form form = new Form();
        form.setAssumeDamageDate(fs.getAntattTidsromForSkaden());

        StringBuilder kommentar = new StringBuilder();
        kommentar.append(fs.getFritekst());

        List<Bilde> bilder = new ArrayList<Bilde>();
        for (ForsikringsskadeBilde fsb : fs.getBilder()) {
            Bilde bilde = fsb.getBilde();
            bilder.add(bilde);
            if (bilde.getBeskrivelse() != null) {
                kommentar.append(bilde.getFilnavn() + ": " + bilde.getBeskrivelse() + ".");
            }
        }
        form.setAttachments(CommonHelper.createAttachments(bilder));
        if (fs.getEstimatKostnad() != null) {
            form.setCostNOK(BigInteger.valueOf(fs.getEstimatKostnad().longValue()));
        } else {
            form.setCostNOK(BigInteger.valueOf(0));
        }

        if (kommentar.toString().length() < 2000) {
            form.setDamageDescription(kommentar.toString());
        } else {
            form.setDamageDescription(kommentar.toString().substring(0, 1999));
        }


        form.setDelayWhy(fs.getUtsattRepKommentar());
        form.setDiscoveredDate(CommonHelper.createDateTime(fs.getEntrepenorDato()));
        form.setDiscoveredName(fs.getEntrepenorNavn());
        form.setDocumentReference(fs.getElrappDokumentIdent());
        form.setIncidentDate(CommonHelper.createDateTime(fs.getSkadeDato()));
        form.setMunicipalityName(fs.getSak().getKommune());
        //TODO
        form.setRepairDate(MipssDateFormatter.formatDate(fs.getReparertDato()));
        form.setReportedDamageDate(CommonHelper.createDateTime(fs.getMeldtDato()));
        form.setReportedName(fs.getMeldtAv());
        form.setRoadReference(CommonHelper.createRoadReference(fs.getSak().getVeiref(), fs.getSak()));
//		form.setSubject("Mesta refnummer:"+fs.getHendelse().getRefnummer());
        form.setSubject(fs.getSkjemaEmne());
        form.setTortFeasor(createTortFeasor(fs));
        form.setTortFeasorIdentification(createTortFeasorIdentification(fs));

        if (fs.getSkadevolderRegnr() != null && !fs.getSkadevolderRegnr().toLowerCase().equals("ukjent")) {
            form.setTortFeasorKnown("Y");
        } else {
            form.setTortFeasorKnown("N");
        }

        Long versjon = fs.getElrappVersjon();
        if (versjon != null) {
            versjon++;
            form.setVersion(BigInteger.valueOf(versjon));
        } else {
            form.setVersion(BigInteger.valueOf(0));
        }

        return form;
    }

    /**
     * Opprett en skadevolder
     *
     * @param fs
     * @return
     */
    private static TortFeasor createTortFeasor(Forsikringsskade fs) {
        TortFeasor tf = new TortFeasor();
        tf.setCarDriverName(fs.getForerNavn());
        if (fs.getForerTlf() != null)
            tf.setCarDriverPhone(fs.getForerTlf().toString());
        tf.setCarMake(fs.getSkadevolderBilmerke());
        tf.setCarOwnerAddress(fs.getEierAdresse());
        tf.setCarOwnerName(fs.getEierAvKjoretoy());
        if (fs.getEierTelefonNr() != null)
            tf.setCarOwnerPhone(fs.getEierTelefonNr().toString());
        tf.setCarRegistrationNumber(fs.getSkadevolderRegnr());
        tf.setInsuranceCompany(fs.getForsikringsselskap());
        if (fs.getBroyteskade() == null) {
            tf.setSnowClearanceReason("N");
        } else {
            tf.setSnowClearanceReason(fs.getBroyteskade() ? "Y" : "N");
        }
        tf.setTrailerRegistrationNumber(fs.getSkadevolderHenger());
        return tf;
    }

    private static boolean isPolitiblank(Forsikringsskade fs) {
        SkadeKontakt politiLensmann = fs.getPolitiLensmann();
        return StringUtils.isBlank(politiLensmann.getKontaktNavn()) && StringUtils.isBlank(politiLensmann.getResultat()) && politiLensmann.getDato() == null && StringUtils.isBlank(politiLensmann.getNavn());
    }

    /**
     * opprett politi og bergingsselskapkontakt
     *
     * @param fs
     * @return
     */
    private static TortFeasorIdentification createTortFeasorIdentification(Forsikringsskade fs) {

        TortFeasorIdentification tf = new TortFeasorIdentification();

        if (fs.getAnmeldt() == null) {
            tf.setPoliceFiled("N");
        } else {
            tf.setPoliceFiled(fs.getAnmeldt() ? "Y" : "N");
        }
        if (fs.getPolitiLensmann() != null) {
            if (!isPolitiblank(fs)) {
                tf.setPoliceContactPersonDate(CommonHelper.createDateTime(fs.getPolitiLensmann().getDato()));
                String navn = fs.getPolitiLensmann().getKontaktNavn();//foretrekker at kontakt_navn benyttes til politi
                if (navn == null) {
                    navn = fs.getPolitiLensmann().getNavn();//gamle mipss-felt lagrer i b�de kontakt_navn og navn
                }
                Long tlfNr = fs.getPolitiLensmann().getKontaktTlfnummer();//foretrekker at kontakt_tlfnummer benyttes til politi
                if (tlfNr == null) {
                    tlfNr = fs.getPolitiLensmann().getTlfnummer();//gamle mipss-felt lagrer i b�de kontakt_tlfnummer og tlfnummer
                }

                tf.setPoliceContactPersonName(navn + " " + (tlfNr != null ? String.valueOf(tlfNr) : ""));
                tf.setPoliceInvestigateResult(fs.getPolitiLensmann().getResultat());
            }
        }
        tf.setSalvageCompanies(createSalvageCompanies(fs));
        tf.setSalvageCompanyResult(createSalvageCompanyResults(fs));
        tf.setSupplementaryInformation(fs.getTilleggsopplysninger());
        SkadeKontakt vitne = getVitne(fs);

        if (vitne != null) {
            tf.setWitness("Y");
            tf.setWitnessDate(CommonHelper.createDateTime(vitne.getDato()));
            tf.setWitnessName(vitne.getNavn());
        } else {
            tf.setWitness("N");
        }
        return tf;
    }

    /**
     * Opprett en liste med bergingsselskap
     *
     * @param fs
     * @return
     */
    private static SalvageCompanies createSalvageCompanies(Forsikringsskade fs) {
        SalvageCompanies s = new SalvageCompanies();
        List<SkadeKontakt> bs = fs.getBergingsSelskaper();
        for (SkadeKontakt sk : bs) {
            SalvageCompany sc = createSalvageCompany(sk);
            if (sc != null && !isBergingsselskapBlank(sk))
                s.getSalvageCompany().add(sc);

        }
        return s;
    }

    private static boolean isBergingsselskapBlank(SkadeKontakt sk) {
        return StringUtils.isBlank(sk.getNavn());
    }

    /**
     * Hent første vitne fra forsikringsskade
     *
     * @param fs
     * @return
     */
    private static SkadeKontakt getVitne(Forsikringsskade fs) {
        if (fs.getVitner().size() > 0) {
            return fs.getVitner().get(0);
        }
        return null;
    }

    /**
     * Opprett et bergingsselskap
     *
     * @param sk
     * @return
     */
    private static SalvageCompany createSalvageCompany(SkadeKontakt sk) {
        SalvageCompany sc = null;
        String namePhone = null;
        if (sk.getKontaktNavn() != null)
            namePhone = sk.getKontaktNavn() + " ";
        if (sk.getKontaktTlfnummer() != null) {
            namePhone += sk.getKontaktTlfnummer().toString();
        }
        if (namePhone != null) {
            if (sc == null)
                sc = new SalvageCompany();
            sc.setContactPersonNamePhone(namePhone);
        }

        String sNamePhone = null;
        if (sk.getNavn() != null) {
            sNamePhone = sk.getNavn() + " ";
        }
        if (sk.getTlfnummer() != null)
            sNamePhone += sk.getTlfnummer().toString();
        if (sNamePhone != null) {
            if (sc == null)
                sc = new SalvageCompany();
            sc.setSalvageCompanyNamePhone(sNamePhone);
        }
        return sc;
    }

    /**
     * Hent resultat fra bergingsselskaper
     *
     * @param fs
     * @return
     */
    private static String createSalvageCompanyResults(Forsikringsskade fs) {
        StringBuilder sb = new StringBuilder();
        for (SkadeKontakt sk : fs.getBergingsSelskaper()) {
            if (StringUtils.isNotBlank(sk.getResultat()))
                sb.append(sk.getResultat());
        }
        return sb.toString();
    }
}
