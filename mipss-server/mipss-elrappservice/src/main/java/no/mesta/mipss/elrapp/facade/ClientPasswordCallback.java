package no.mesta.mipss.elrapp.facade;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

/**
 * Passord for webservice
 *
 * @author harkul
 */
public class ClientPasswordCallback implements CallbackHandler {

    private final static Logger log = LoggerFactory.getLogger(ClientPasswordCallback.class);
    private final String password;

    public ClientPasswordCallback(String password) {
        this.password = password;
    }

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for(Callback callback : callbacks) {
            try {
                ((WSPasswordCallback)callback).setPassword(password);
            } catch(Exception e) {
                log.error("Unable to set password in callback", e);
            }
        }
    }
}
