package no.mesta.mipss.elrapp;

import no.mesta.mipss.valueobjects.ElrappTypeR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.*;

public final class ElrappResponseParser {
    private static final Logger LOG = LoggerFactory.getLogger(ElrappResponseParser.class);

    private ElrappResponseParser() {
    }

    public static ElrappResponse parse(ElrappTypeR.Type type, Node root) throws ElrappException {
        Node code = searchForNode(root, "//SubmissionStatus/Code/text()").item(0);
        String codeText = code.getTextContent();
        LOG.debug("CODE: {}", codeText);

        if (codeText.equals("OK")) {
            Node docRef = searchForNode(root, "//DocumentReference/text()").item(0);
            Node docVer = searchForNode(root, "//DocumentVersion/text()").item(0);
            Long elrappDokumentId = Long.valueOf(docRef.getTextContent());
            Long elrappVersjon = Long.valueOf(docVer.getTextContent());
            LOG.debug("DOCREF: {}", docRef.getTextContent());
            LOG.debug("DOCVER: {}", docVer.getTextContent());
            ElrappResponse res = new ElrappResponse(elrappDokumentId, elrappVersjon, codeText, type.toString());
            return res;
        } else {
            Node desc = searchForNode(root, "//SubmissionStatus/Description/text()").item(0);
            Node stack = searchForNode(root, "//SubmissionStatus/StackTrace/text()").item(0);

            String stacktrace = stack != null ? stack.getTextContent() : null;
            String message = desc != null ? desc.getTextContent() : null;
            LOG.debug("DESCRIPTION: {}", desc);
            LOG.debug("STACKTRACE: {}",  stack);

            if (stacktrace != null) {
                throw new ElrappException(message, new ElrappException(stacktrace));
            } else {
                throw new ElrappException(message);
            }
        }
    }

    /**
     * Let etter en node
     *
     * @param root
     * @param xpExpr
     * @return
     */
    private static NodeList searchForNode(Node root, String xpExpr) {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        Object result = null;

        try {
            XPathExpression expr = xpath.compile(xpExpr);
            result = expr.evaluate(root, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return (NodeList) result;
    }
}
