package no.mesta.mipss.elrapp.facade;

import generated.Submission;
import no.mesta.mipss.elrapp.ElrappException;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Interface for sending av skjema til elrapp
 *
 * @author harkul
 */
public interface ElrappFacade {

    ElrappResponse sendRscheme(ElrappTypeR.Type type, Submission submission) throws NoSuchAlgorithmException, KeyManagementException, ElrappException, SAXException, ParserConfigurationException, IOException;
}
