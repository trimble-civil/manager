package no.mesta.mipss.elrapp;

import javax.security.auth.callback.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ElrappUsernamePasswordCallbackHandler implements CallbackHandler {

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

        for (int i = 0; i < callbacks.length; i++) {
            Callback callback = callbacks[i];
            if (callback instanceof NameCallback) {
                handleUsernameCallback((NameCallback) callback);
            } else if (callback instanceof PasswordCallback) {
                handlePasswordCallback((PasswordCallback) callback);
            } else {
                throw new UnsupportedCallbackException(callback, "Unknow callback for username or password");
            }
        }
    }

    private void handleUsernameCallback(NameCallback cb) throws IOException {
        //BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.err.println("***Please Enter Your User Name: ");
        System.err.flush();
        cb.setName((new BufferedReader(new InputStreamReader(System.in))).readLine());
    }

    private void handlePasswordCallback(PasswordCallback cb) throws IOException {
        //BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.err.println("***Please Enter Your Password: ");
        System.err.flush();
        cb.setPassword((new BufferedReader(new InputStreamReader(System.in))).readLine().toCharArray());
    }
}
