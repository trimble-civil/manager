package no.mesta.mipss.elrapp.facade;

import java.net.URL;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import no.nois.ElrappContractorService;
import no.nois.IElrappContractorService;

import org.apache.cxf.rt.security.SecurityConstants;

public final class ElrappFacadeWCF extends AbstractElrappFacade {

    private final String url;
    private final String username;
    private final String password;

    public ElrappFacadeWCF(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
	public Object getService() {
		ClassLoader cl = ElrappContractorService.class.getClassLoader();
		URL wsdlUrl = cl.getResource("ElrappContractorServiceTest.wsdl");
		ElrappContractorService s = new ElrappContractorService(wsdlUrl);

		IElrappContractorService ss = s.getBasicHttpBindingIElrappContractorService();
		Map<String, Object> requestContext = ((BindingProvider)ss).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
		requestContext.put(SecurityConstants.USERNAME, username);
		requestContext.put(SecurityConstants.CALLBACK_HANDLER, new ClientPasswordCallback(password));
		return ss;
	}

}