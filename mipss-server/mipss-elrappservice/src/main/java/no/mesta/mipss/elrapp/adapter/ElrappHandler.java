package no.mesta.mipss.elrapp.adapter;

import no.mesta.mipss.elrapp.ElrappException;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.elrapp.facade.ElrappFacade;
import no.mesta.mipss.elrapp.facade.ElrappFacadeFactory;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.webservice.NvdbWebservice;

/**
 * Enkel klasse som bestemmer hvilken elrappadapter som skal benyttes til å
 * sende skjema til Elrapp.
 *
 * @author harkul
 */
public final class ElrappHandler {

    private final Bruker bruker;
    private final String mode;

    /**
     * Hvis entity er av en klasse som ikke er støttet av elrappadapterene
     * vil det bli kastet en IllegalArgumentException.
     *
     * @param bruker
     * @param mode
     */
    public ElrappHandler(Bruker bruker, String mode) {
        this.bruker = bruker;
        this.mode = mode;
    }

    /**
     * Sender skjema til Elrapp
     *
     * @return ElrappResponse - inneholder dokumentVersjon og dokumentId fra elrapp
     * @throws ElrappException - hvis noe går galt i overføringen av skjema.
     */
    public ElrappResponse sendSchema(MipssEntityBean<?> entity, NvdbWebservice nvdb) throws ElrappException {
        ElrappAdapter adapter;

        ElrappFacade elrapp = ElrappFacadeFactory.createElrappFacade(this.mode);

        if (entity instanceof Skred) {
            adapter = new ElrappR11Adapter(elrapp, nvdb);
        } else if (entity instanceof Forsikringsskade) {
            adapter = new ElrappR5Adapter(elrapp);
        } else if (entity instanceof Hendelse) {
            adapter = new ElrappR2Adapter(elrapp);
        } else {
            throw new IllegalArgumentException("The MipssEntityBean [" + entity + "] is not a valid R schema object.");
        }

        return adapter.sendRSchema(entity, bruker);
    }

}
