package no.mesta.mipss.elrapp;

import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

import javax.ejb.Remote;

@Remote
public interface ElrappService {
    public static final String BEAN_NAME = "ElrappServiceBean";

    /**
     * Klienten må validere rskjemaobjektet før det sendes til elrapp.
     * <p/>
     * Sender et r-skjema til elrapp og lagrer elrappDocumentId på hendelsen / skred / forsikringsskade
     * hvis det ikke finnes fra før. Oppdaterer også elrappVersjon.
     * <p/>
     * Skred objekt sendes R11
     * Forsikringsskade sendes R5
     * Hendelse sendes R2
     *
     * @param hsv
     * @throws ElrappException
     */
    ElrappResponse sendRSchema(RskjemaSokResult rskjema, Bruker bruker) throws ElrappException;
}
