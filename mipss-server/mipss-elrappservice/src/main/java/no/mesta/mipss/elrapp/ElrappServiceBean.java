package no.mesta.mipss.elrapp;

import no.mesta.mipss.elrapp.adapter.ElrappHandler;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.webservice.NvdbWebservice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name = ElrappService.BEAN_NAME, mappedName = "ejb/" + ElrappService.BEAN_NAME)
public class ElrappServiceBean implements ElrappService {
    private static final Logger LOG = LoggerFactory.getLogger(ElrappServiceBean.class);

    @EJB
    private KonfigParam konfigparam;
    @EJB
    private RskjemaService rskjemaService;
    @EJB
    private NvdbWebservice nvdb;

    @PersistenceContext(unitName = "mipssPersistence")
    private EntityManager em;

    public ElrappResponse sendRSchema(RskjemaSokResult rskjema, Bruker bruker) throws ElrappException {

        ElrappResponse res = null;
        try {
            switch (rskjema.getElrappTypeR()) {
                case R2:
                    Hendelse h = rskjemaService.hentHendelse(rskjema.getGuid());
                    res = sendSchema(h, bruker);
                    updateVersionAndDates(h, res);
                    em.merge(h);
                    break;
                case R5:
                    Forsikringsskade f = rskjemaService.hentForsikringsskade(rskjema.getGuid());
                    res = sendSchema(f, bruker);
                    updateVersionAndDates(f, res);
                    em.merge(f);
                    break;
                case R11:
                    Skred s = rskjemaService.hentSkred(rskjema.getGuid());
                    res = sendSchema(s, bruker);
                    updateVersionAndDates(s, res);
                    em.merge(s);
                    break;
                default:
                    throw new ElrappException("Unsupported ");
            }
        } catch (Throwable t) {
            throw new ElrappException(t.getMessage(), t);
        }
        return res;
    }

    private void updateVersionAndDates(Hendelse h, ElrappResponse res) {
        if (h.getElrappDokumentIdent() == null) {
            h.setElrappDokumentIdent(res.getElrappDokumentId());
        }

        if (res.getElrappVersjon() != null) {
            h.setElrappVersjon(res.getElrappVersjon());
        }
        h.setRapportertDato(Clock.now());
    }

    private void updateVersionAndDates(Skred h, ElrappResponse res) {
        if (h.getElrappDokumentIdent() == null) {
            h.setElrappDokumentIdent(res.getElrappDokumentId());
        }

        if (res.getElrappVersjon() != null) {
            h.setElrappVersjon(res.getElrappVersjon());
        }
        h.setRapportertDato(Clock.now());
    }

    private void updateVersionAndDates(Forsikringsskade h, ElrappResponse res) {
        if (h.getElrappDokumentIdent() == null) {
            h.setElrappDokumentIdent(res.getElrappDokumentId());
        }

        if (res.getElrappVersjon() != null) {
            h.setElrappVersjon(res.getElrappVersjon());
        }
        h.setRapportertDato(Clock.now());
    }

    /**
     * Sender skjema til elrapp
     *
     * @param entity
     * @return
     * @throws ElrappException
     */
    private ElrappResponse sendSchema(MipssEntityBean<?> entity, Bruker bruker) throws ElrappException {
        Konfigparam param = konfigparam.hentEnForApp("mipss.elrapp", "elrappVersjon");
        String mode = param.getVerdi();
        LOG.debug("Creating Elrapp facade with config '{}': {}", param.getNavn(), mode);

        ElrappHandler h = new ElrappHandler(bruker, mode);
        return h.sendSchema(entity, nvdb);
    }
}
