package no.mesta.mipss.service.produksjon.friksjon;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class FriksjonQueryParams implements Serializable{

	private Long driftkontraktId;  
	private Date fraDatoCet;  
	private Date tilDatoCet;  
	private String veikategori;  
	private String veistatus;
	private Long veinummer;  
	private Long hp;
	private List<Long> dfuIdList;
	
	public Long getDriftkontraktId() {
		return driftkontraktId;
	}
	public void setDriftkontraktId(Long driftkontraktId) {
		this.driftkontraktId = driftkontraktId;
	}
	public Date getFraDatoCet() {
		return fraDatoCet;
	}
	public void setFraDatoCet(Date fraDatoCet) {
		this.fraDatoCet = fraDatoCet;
	}
	public Date getTilDatoCet() {
		return tilDatoCet;
	}
	public void setTilDatoCet(Date tilDatoCet) {
		this.tilDatoCet = tilDatoCet;
	}
	public String getVeikategori() {
		return veikategori;
	}
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}
	public String getVeistatus(){
		return veistatus;
	}
	public void setVeistatus(String veistatus){
		this.veistatus = veistatus;
	}
	public Long getVeinummer() {
		return veinummer;
	}
	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}
	public Long getHp() {
		return hp;
	}
	public void setHp(Long hp) {
		this.hp = hp;
	}
	public void setDfuIdList(List<Long> dfuIdList) {
		this.dfuIdList = dfuIdList;
	}
	public List<Long> getDfuIdList() {
		return dfuIdList;
	}
}
