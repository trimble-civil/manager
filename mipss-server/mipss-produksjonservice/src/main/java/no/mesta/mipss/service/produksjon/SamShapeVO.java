package no.mesta.mipss.service.produksjon;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.webservice.ReflinkStrekning;

public class SamShapeVO {

	private String materiale;
	private Double mengde;
	private String prodtype;
	private Date fraTidspunkt;
	private Date tilTidspunkt;
	private List<ReflinkStrekning> strekningList;
	
	public void setStrekningList(List<ReflinkStrekning> strekningList) {
		this.strekningList = strekningList;
	}
	public List<ReflinkStrekning> getStrekningList(){
		return strekningList;
	}
	public List<Point> getVectors() {
		List<Point> vectors = new ArrayList<Point>();
		for (ReflinkStrekning rs:getStrekningList()){
			vectors.addAll(Arrays.asList(rs.getVectors()));
		}
		return vectors;
	}
	public String getMateriale() {
		return materiale;
	}
	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}
	public Double getMengde() {
		return mengde;
	}
	public void setMengde(Double mengde) {
		this.mengde = mengde;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public Date getFraTidspunkt() {
		return fraTidspunkt;
	}
	public void setFraTidspunkt(Date fraTidspunkt) {
		this.fraTidspunkt = fraTidspunkt;
	}
	public Date getTilTidspunkt() {
		return tilTidspunkt;
	}
	public void setTilTidspunkt(Date tilTidspunkt) {
		this.tilTidspunkt = tilTidspunkt;
	}
}
