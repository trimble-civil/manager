package no.mesta.mipss.service.produksjon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.webservice.ReflinkStrekning;

@SuppressWarnings("serial")
public class ProdReflinkStrekning implements Serializable{

	private Prodstrekning prodstrekning;
	private List<ReflinkStrekning> strekningList;
	private List<ProdtypeVO> prodtyper;
	/**
	 * @return the prodstrekning
	 */
	public Prodstrekning getProdstrekning() {
		return prodstrekning;
	}
	/**
	 * @param prodstrekning the prodstrekning to set
	 */
	public void setProdstrekning(Prodstrekning prodstrekning) {
		this.prodstrekning = prodstrekning;
	}
	
	/**
	 * @return the strekningList
	 */
	public List<ReflinkStrekning> getStrekningList() {
		return strekningList;
	}
	
	public void addReflinkStrekning(ReflinkStrekning strekning){
		if (strekningList==null)
			strekningList = new ArrayList<ReflinkStrekning>();
		strekningList.add(strekning);
	}
	/**
	 * @param strekningList the strekningList to set
	 */
	public void setStrekningList(List<ReflinkStrekning> strekningList) {
		this.strekningList = strekningList;
	}
	/**
	 * @return the prodtyper
	 */
	public List<ProdtypeVO> getProdtypeList() {
		return prodtyper;
	}
	/**
	 * @param prodtyper the prodtyper to set
	 */
	public void setProdtypeList(List<ProdtypeVO> prodtyper) {
		this.prodtyper = prodtyper;
	}
	
}
