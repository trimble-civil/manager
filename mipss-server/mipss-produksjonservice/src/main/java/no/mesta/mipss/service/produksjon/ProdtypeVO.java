package no.mesta.mipss.service.produksjon;

import java.io.Serializable;

import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
/**
 * 
 * Vo klasse som holder på produksjonstyper og farge
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class ProdtypeVO implements Serializable{
	private Long prodtypeId;
	private Prodtype prodtype;
	private String utstyrsnavn;
	private Integer bitNr;
	
	/**
	 * @return the prodtypeId
	 */
	public Long getProdtypeId() {
		return prodtypeId;
	}
	/**
	 * @param prodtypeId the prodtypeId to set
	 */
	public void setProdtypeId(Long prodtypeId) {
		this.prodtypeId = prodtypeId;
	}
	/**
	 * @return the prodtype
	 */
	public Prodtype getProdtype() {
		return prodtype;
	}
	/**
	 * @param prodtype the prodtype to set
	 */
	public void setProdtype(Prodtype prodtype) {
		this.prodtype = prodtype;
	}
	/**
	 * @return the utstyrsnavn
	 */
	public String getUtstyrsnavn() {
		return utstyrsnavn;
	}
	/**
	 * @param utstyrsnavn the utstyrsnavn to set
	 */
	public void setUtstyrsnavn(String utstyrsnavn) {
		this.utstyrsnavn = utstyrsnavn;
	}
	/**
	 * @return the bitNr
	 */
	public Integer getBitNr() {
		return bitNr;
	}
	/**
	 * @param bitNr the bitNr to set
	 */
	public void setBitNr(Integer bitNr) {
		this.bitNr = bitNr;
	}
	
	
}
