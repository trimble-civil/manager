package no.mesta.mipss.service.produksjon.friksjon;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Klasse som baker sammen et datasett av friksjonsdata som danner grunnlaget for rapporteringen av friksjonsm�ling. 
 * 
 * Strekninger av friksjonsm�linger blir generert basert p� hvilken strekninglengde friksjonen skal spres utover.
 * @author harkul
 *
 */
public class FriksjonRapportHelper {
	enum VEID_TYPE{
		FRIKSJON,
		HASTIGHET
	}
//	public FriksjonHelper() throws ParseException{
//		List<FriksjonVO> l = new ArrayList<FriksjonVO>();
//		//meter stigende
//		SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
////		l.add(c(28, 60, 0.277,"0 ",  f.parse("08:52:14")));
////		l.add(c(76, 61, .301, "1 ",  f.parse("08:52:11")));
////		l.add(c(91,	62, .3,   "2 ",  f.parse("08:52:10")));
////		l.add(c(122,63, .301, "3 ",  f.parse("08:52:08")));
////		l.add(c(153,64, .305, "4 ",  f.parse("08:52:06")));
////		l.add(c(200,65, .292, "5 ",  f.parse("08:52:03")));
////		l.add(c(215,66, .212, "6 ",  f.parse("08:52:02")));
////		l.add(c(246,67, .235, "7 ",  f.parse("08:52:00")));
////		l.add(c(261,66, .209, "8 ",  f.parse("08:51:59")));
////		l.add(c(322,65, .213, "9 ",  f.parse("08:51:55")));
////		l.add(c(368,64, .233, "10", f.parse("08:51:53")));
////		l.add(c(383,63, .216, "11", f.parse("08:51:51")));
////		l.add(c(444,62, .217, "12", f.parse("08:51:47")));
////		l.add(c(459,61, .247, "13", f.parse("08:51:46")));
////		l.add(c(490,71, .25,  "14", f.parse("08:51:44")));
////		l.add(c(532,72, .25,  "15", f.parse("08:51:42")));
////		l.add(c(577,73, .25,  "16", f.parse("08:51:39")));
//
//		//meter synkende
//		l.add(c(577,73, .25,  "16", f.parse("08:51:39")));
//		l.add(c(532,72, .25,  "15", f.parse("08:51:42")));
//		l.add(c(490,71, .25,  "14", f.parse("08:51:44")));
//		l.add(c(459,61, .247, "13", f.parse("08:51:46")));
//		l.add(c(444,62, .217, "12", f.parse("08:51:47")));
//		l.add(c(383,63, .216, "11", f.parse("08:51:51")));
//		l.add(c(368,64, .233, "10", f.parse("08:51:53")));
//		l.add(c(322,65, .213, "9",  f.parse("08:51:55")));
//		l.add(c(261,66, .209, "8",  f.parse("08:51:59")));
//		l.add(c(246,67, .235, "7",  f.parse("08:52:00")));
//		l.add(c(215,66, .212, "6",  f.parse("08:52:02")));
//		l.add(c(200,65, .292, "5",  f.parse("08:52:03")));
//		l.add(c(153,64, .305, "4",  f.parse("08:52:06")));
//		l.add(c(122,63, .301, "3",  f.parse("08:52:08")));
//		l.add(c(91,	62, .3,   "2",  f.parse("08:52:10")));
//		l.add(c(76, 61, .301, "1",  f.parse("08:52:11")));
//		l.add(c(28, 60, 0.277,"0",  f.parse("08:52:14")));
//		
//		for (FriksjonVO ff:genererStrekninger(l, 20)){
//			System.out.println(ff.getMeter()+" "+ff.getAktivitet()+" "+ff.getFriksjon()+" "+ff.getHastighet()+" "+f.format(ff.getDate()));
////			System.out.println(ff.getMeter()+"\t"+ff.getHastighet()+" \t"+f.format(ff.getDate()));
//		}
//	}
	public static void main(String[] args) throws ParseException {
		new FriksjonRapportHelper();
	}
	
//	 
    public List<FriksjonVO> genererStrekninger(List<FriksjonVO> friksjonList, int strekningLengde){
    	if (friksjonList==null||friksjonList.size()==0)
    		return new ArrayList<FriksjonVO>();
    		
    	
    	//hent f�rste og siste meterverdi og tidspunkt
    	FriksjonVO first = friksjonList.get(0);
    	FriksjonVO last = friksjonList.get(friksjonList.size()-1);
    	int startMeter = first.getMeter().intValue();
    	int stoppMeter = first.getMeter().intValue();
    	
    	Date startTid = first.getDate();
    	Date stoppTid = first.getDate();
    	for (FriksjonVO v:friksjonList){
    		if (v.getMeter()<startMeter)
    			startMeter = v.getMeter().intValue();
    		if (v.getMeter()>stoppMeter){
    			stoppMeter = v.getMeter().intValue();
    		}
    		if (v.getDate().before(startTid))
    			startTid = v.getDate();
    		if (v.getDate().after(stoppTid))
    			stoppTid = v.getDate();
    	}
    	
    	
    	boolean stigendeMeter = first.getMeter()<last.getMeter();
    	
    	int avstand = Math.abs(stoppMeter-startMeter);
    	int meterStrekningAntall = avstand/strekningLengde;
    	List<FriksjonVO> strekningList =null;
    	if (stigendeMeter){
    		strekningList = byggStigendeMeter(friksjonList, strekningLengde, startMeter, meterStrekningAntall);
    	}else{
    		strekningList = byggSynkendeMeter(friksjonList, strekningLengde, stoppMeter, meterStrekningAntall);
    	}
    	
    	if (strekningList.size()<2){
    		return new ArrayList<FriksjonVO>();
    	}
    	//fordel tiden.. 
    	long tid = Math.abs(stoppTid.getTime()-startTid.getTime());
    	long tidPr = tid/strekningList.size();
    	long hittil = 0;
    	
    	for (FriksjonVO f:strekningList){
	    	f.setDate(new Date(startTid.getTime()+tidPr+hittil));
    		hittil+=tidPr;
    	}
    	
    	return strekningList;
    }

    private List<FriksjonVO> byggStigendeMeter(List<FriksjonVO> friksjonList, int strekningLengde, int startMeter, int meterStrekningAntall) {
    	int currentMeter = 0;
		List<FriksjonVO> strekningList = new ArrayList<FriksjonVO>();
		for (int i=0;i<meterStrekningAntall;i++){
			int fraMeter = startMeter+currentMeter;
			int tilMeter = startMeter+strekningLengde+currentMeter;
			currentMeter +=strekningLengde;
			
			List<FriksjonVO> withinMetersList = getFriksjonInnenforMeterStigende(fraMeter, tilMeter, friksjonList);
			if (withinMetersList.size()==1){
				FriksjonVO f = withinMetersList.get(0);
				strekningList.add(create(tilMeter, f.getFriksjon(), f.getHastighet(), f.getAktivitet(), f, f.getDate()));
			}else{
				double value = getVeidFriksjon(withinMetersList, fraMeter, tilMeter, VEID_TYPE.FRIKSJON);
				double hastighet = getVeidFriksjon(withinMetersList, fraMeter, tilMeter, VEID_TYPE.HASTIGHET);
				FriksjonVO f = create(tilMeter, value, hastighet, withinMetersList.get(0).getAktivitet(), withinMetersList.get(0), withinMetersList.get(0).getDate());
				strekningList.add(f);
			}
		}
		return strekningList;
    }
    
    private List<FriksjonVO> byggSynkendeMeter(List<FriksjonVO> friksjonList, int strekningLengde, int stoppMeter, int meterStrekningAntall) {
    	int currentMeter = 0;
		List<FriksjonVO> strekningList = new ArrayList<FriksjonVO>();
		for (int i=0;i<meterStrekningAntall;i++){
			int fraMeter = stoppMeter-currentMeter;
			int tilMeter = stoppMeter-strekningLengde-currentMeter;
			currentMeter +=strekningLengde;
			
//			for (FriksjonVO f:friksjonList){
//				System.out.println(f.getMeter());
//			}
			//TODO
			List<FriksjonVO> withinMetersList = getFriksjonInnenforMeterSynkende(fraMeter, tilMeter, friksjonList);
			if (withinMetersList.size()==1){
				FriksjonVO f = withinMetersList.get(0);
				strekningList.add(create(tilMeter, f.getFriksjon(), f.getHastighet(), f.getAktivitet(), f, f.getDate()));
			}else{
				
				double value = getVeidFriksjon(withinMetersList, fraMeter, tilMeter, VEID_TYPE.FRIKSJON);
				double hastighet = getVeidFriksjon(withinMetersList, fraMeter, tilMeter, VEID_TYPE.HASTIGHET);
				FriksjonVO f = create(tilMeter, value, hastighet, withinMetersList.get(0).getAktivitet(), withinMetersList.get(0), withinMetersList.get(0).getDate());
				strekningList.add(f);
			}
		}
		return strekningList;
    }

    private int getMeterInnenfor(List<FriksjonVO> fList, int index, int from, int to){
    	FriksjonVO f = fList.get(index);
    	int meter = f.getMeter().intValue();
    	if (index==0){
    		return Math.abs(meter-from);
    	}
    	if (index==fList.size()-1){
    		FriksjonVO prev = fList.get(index-1);
    		return Math.abs(to-prev.getMeter().intValue());
    	}
    	FriksjonVO prev = fList.get(index-1);
    	return Math.abs(meter-prev.getMeter().intValue());
    }
    /**
     * Henter friksjonsm�linger innenfor de angitte fra og til meterne. 
     * @param from
     * @param to
     * @param list
     * @return
     */
    private List<FriksjonVO> getFriksjonInnenforMeterStigende(int from, int to, List<FriksjonVO> list){
    	List<FriksjonVO> fList = new ArrayList<FriksjonVO>();
    	for (int i=0;i<list.size();i++){
    		FriksjonVO f = list.get(i);
    		if (f.getMeter().intValue()>from&&f.getMeter().intValue()<=to){
    			fList.add(f);
    		}
    		if (f.getMeter().intValue()>to){
    			fList.add(f);
    			//ingen flere som vil matche..
    			break;
    		}
    	}
    	return fList;
    }
    
    /**
     * Henter friksjonsm�linger innenfor de angitte fra og til meterne. 
     * @param from
     * @param to
     * @param list
     * @return
     */
    private List<FriksjonVO> getFriksjonInnenforMeterSynkende(int from, int to, List<FriksjonVO> list){
    	List<FriksjonVO> fList = new ArrayList<FriksjonVO>();
    	for (int i=0;i<list.size();i++){
    		FriksjonVO f = list.get(i);
    		if (f.getMeter().intValue()<from&&f.getMeter().intValue()>=to){
    			fList.add(f);
    		}
    		if (f.getMeter().intValue()<to){
    			fList.add(f);
    			//ingen flere som vil matche..
    			break;
    		}
    	}
    	return fList;
    }
    
    /**
     * Henter veid friksjon fra listen med friksjonsm�linger som er innenfor fraMeter og tilMeter
     * 
     * @param fList listen med m�linger det skal regnes veide gjennomsnitt av
     * @param fraMeter 
     * @param tilMeter
     * @param type 
     * @return
     */
    private double getVeidFriksjon(List<FriksjonVO> fList, int fraMeter, int tilMeter, VEID_TYPE type){
    	double[] verdier = new double[fList.size()];
    	double[] vekter = new double[fList.size()];
    	
    	double distance = Math.abs(fraMeter-tilMeter);
    	
    	int cindex = 0;
    	for (int i=0;i<fList.size();i++){
    		double avstandInnenfor = getMeterInnenfor(fList, i, fraMeter, tilMeter);
    		
    		double vekt = avstandInnenfor/distance*100;
    		double verdi = 0;
    		FriksjonVO c = fList.get(i);
    		switch(type){
    			case FRIKSJON: verdi = c.getFriksjon();break;
    			case HASTIGHET: verdi = c.getHastighet();break;
    		}
    		verdier[cindex]=verdi;
    		vekter[cindex]=vekt;
    		cindex++;
    	}
    	
    	double totVerdi = 0;
    	for (int i=0;i<verdier.length;i++){
    		totVerdi += verdier[i]*vekter[i];
    	}
    	double totVekt = 0;
    	for (int i=0;i<vekter.length;i++){
    		totVekt += vekter[i];
    	}
    	
    	//rund av til to desimaler
    	double r = Math.pow(10, 2);
    	double verdi = (totVerdi/totVekt)*r;
    	double tmp = Math.round(verdi);
    	return tmp/r;
    }
//    48 1 0.301 61.0 08:52:12
//    68 1 0.301 61.0 08:52:11
//    88 1 0.3 61.6 08:52:10
//    108 2 0.3 62.85 08:52:08
//    128 3 0.3 63.3 08:52:07
//    148 4 0.305 64.0 08:52:06
//    168 4 0.3 64.75 08:52:04
//    188 5 0.292 65.0 08:52:03
//    208 5 0.26 65.4 08:52:02
//    228 6 0.23 66.65 08:52:01
//    248 7 0.23 66.9 08:51:59
//    268 8 0.21 65.65 08:51:58
//    288 9 0.213 65.0 08:51:57
//    308 9 0.213 65.0 08:51:55
//    328 9 0.22 64.7 08:51:54
//    348 10 0.233 64.0 08:51:53
//    368 10 0.233 64.0 08:51:51
//    388 11 0.22 62.75 08:51:50
//    408 12 0.217 62.0 08:51:49
//    428 12 0.217 62.0 08:51:48
//    448 12 0.22 61.8 08:51:46
//    468 13 0.25 65.5 08:51:45
//    488 14 0.25 71.0 08:51:44
//    508 14 0.25 71.9 08:51:42
//    528 15 0.25 72.0 08:51:41
//    548 15 0.25 72.8 08:51:40
//    568 16 0.25 73.0 08:51:39
	    
    public FriksjonVO create(int meter, double friksjon, double hastighet, String aktivitet, FriksjonVO orig, Date tidspunkt){
    	FriksjonVO v = new FriksjonVO();
    	v.setMeter(Long.valueOf(meter));
    	v.setFriksjon(friksjon);
    	v.setAktivitet(aktivitet);
    	v.setHastighet(hastighet);
    	
    	v.setDate(tidspunkt);
    	v.setDfuId(orig.getDfuId());
    	v.setHp(orig.getHp());
    	v.setNmea(orig.getNmea());
    	v.setSnappetX(orig.getSnappetX());
    	v.setSnappetY(orig.getSnappetY());
    	v.setUsikker(orig.getUsikker());
    	v.setVeikategori(orig.getVeikategori());
    	v.setVeinummer(orig.getVeinummer());
    	v.setVeistatus(orig.getVeistatus());
    	v.setEksterntNavn(orig.getEksterntNavn());
    	
    	
    	
    	return v;
    }
    
    public FriksjonVO c(int meter, double hastighet, double friksjon, String aktivitet, Date date){
    	FriksjonVO v = new FriksjonVO();
    	v.setMeter(Long.valueOf(meter));
    	v.setFriksjon(friksjon);
    	v.setAktivitet(aktivitet);
    	v.setHastighet(hastighet);
    	v.setDate(date);
    	return v;
    }
    
    public boolean isInverted(List<FriksjonVO> friksjonList){
		if (friksjonList.size()>1){
			FriksjonVO first = friksjonList.get(0);
			FriksjonVO second = friksjonList.get(1);
			return first.getMeter()>second.getMeter();
		}
		return false;
	}
    
    
}

