package no.mesta.mipss.service.produksjon;

import java.io.Serializable;
import java.util.List;

import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
/**
 * Vo klasse for inspeksjoner, inneholder listen med ProdReflinkStrekning og inspeksjonsobjektet
 * 
 * @See ProdReflinkStrekning
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class InspeksjonVO implements Serializable{
	private List<ProdReflinkStrekning> strekninger;
	private Inspeksjon inspeksjon;
	
	public InspeksjonVO(){
		
	}

	/**
	 * @return the strekninger
	 */
	public List<ProdReflinkStrekning> getStrekninger() {
		return strekninger;
	}

	/**
	 * @param strekninger the strekninger to set
	 */
	public void setStrekninger(List<ProdReflinkStrekning> strekninger) {
		this.strekninger = strekninger;
	}

	/**
	 * @return the inspeksjon
	 */
	public Inspeksjon getInspeksjon() {
		return inspeksjon;
	}

	/**
	 * @param inspeksjon the inspeksjon to set
	 */
	public void setInspeksjon(Inspeksjon inspeksjon) {
		this.inspeksjon = inspeksjon;
	}
}
