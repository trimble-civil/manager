package no.mesta.mipss.service.produksjon;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
@SuppressWarnings("serial")
@Entity
@Table(name = "PunktregVO_dummy_table")
@SqlResultSetMapping(name = PunktregVO.MAPPING_NAME, entities = { 
	@EntityResult(entityClass = PunktregVO.class, fields = {
		@FieldResult(name = "guid", column = "GUID"),
		@FieldResult(name = "x", column = "X"),
		@FieldResult(name = "y", column = "Y"),
		@FieldResult(name = "snappetX", column = "SNAPPET_X"),
		@FieldResult(name = "snappetY", column = "SNAPPET_Y") 
	}) 
}) 

public class PunktregVO implements Serializable{
	public static final String MAPPING_NAME = "PunktregVOResult";
	public enum TYPE {
		FUNN, 
		HENDELSE,
		FORSIKRINGSSKADE,
		SKRED
	}
	private String guid;
	private Double snappetX;
	private Double snappetY;
	private Double x;
	private Double y;
	private TYPE type;
	
	public PunktregVO(){
		
	}
	
	public void setType(TYPE type){
		this.type = type;
	}
	@Transient
	public TYPE getType(){
		return type;
	}
	@Id
	@Column(name = "guid")
	public String getGuid() {
		return guid;
	}
	@Column(name="snappet_x")
	public Double getSnappetX() {
		return snappetX;
	}
	@Column(name="snappet_y")
	public Double getSnappetY() {
		return snappetY;
	}
	@Column(name="x")
	public Double getX() {
		return x;
	}
	@Column(name="y")
	public Double getY() {
		return y;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}
	public void setSnappetX(Double snappetX) {
		this.snappetX = snappetX;
	}
	public void setSnappetY(Double snappetY) {
		this.snappetY = snappetY;
	}
	public void setX(Double x) {
		this.x = x;
	}
	public void setY(Double y) {
		this.y = y;
	}
}
