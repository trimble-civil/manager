package no.mesta.mipss.service.produksjon;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.kjoretoy.KjoretoyDfuVO;
import no.mesta.mipss.kjoretoy.MipssKjoretoyLocal;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import no.mesta.mipss.mipssfelt.InspeksjonService;
import no.mesta.mipss.mipssfield.HendelseQueryFilter;
import no.mesta.mipss.mipssfield.MipssFieldLocal;
import no.mesta.mipss.mipssutils.MipssServerUtils;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kvernetf1.*;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonHelper;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;
import no.mesta.mipss.util.DateFormatter;
import no.mesta.mipss.webservice.*;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.Veireferanse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
/**
 * Tjeneste for � hente ut produksjon gjort av kj�ret�y eller pda
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kul�</a>
 *
 */
@Stateless(name=ProduksjonService.BEAN_NAME, mappedName="ejb/"+ProduksjonService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class ProduksjonServiceBean implements ProduksjonService{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
	
	@EJB
	private NvdbWebserviceLocal service;
	@EJB
	private KonfigParamLocal konfig;
	@EJB
	private MipssFieldLocal field;
	@EJB
	private InspeksjonService inspeksjonService;
	@EJB
	private MipssKjoretoyLocal kjoretoy;
	@EJB
	private MipssServerUtils serverUtils;
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("produksjonserviceText");
	
	/** {@inheritDoc} */
	public List<ProdpunktVO> getProdpunktFromDfuinfo(Long dfuId, Long kjoretoyId, Date tidsvinduStart, Date tidsvinduStopp, Date forrigePunktGmt, boolean tidsvindu){
		Date fraDate = DateFormatter.convertFromCETtoGMT(tidsvinduStart);
		Date tilDate = DateFormatter.convertFromCETtoGMT(tidsvinduStopp);
		String fra = MipssDateFormatter.formatDate(fraDate, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String til = MipssDateFormatter.formatDate(tilDate, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String forrige = forrigePunktGmt==null?"":MipssDateFormatter.formatDate(forrigePunktGmt, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		if (dfuId==null){
			if (kjoretoyId==null){
				throw new IllegalStateException("KjoretoyId og dfuId kan ikke være null");
			}
			int id = getDfuIdFromKjoretoy(kjoretoyId, fra, til);
			
			if (id==-1){
				log.debug("fant ikke dfu_id fra kjøretøy:"+kjoretoyId+" "+fra+" -  "+til);
				return null;
			}
			else{
				dfuId = new Long(id);
			}
		}
		String sisteDato ="";
		//forrigePunktGmt er null ved første kallet. 
		if (forrigePunktGmt!=null){
			//sjekk om det har kommet nye punkter i dfuinfo.
			Query sisteDfuinfo = em.createNativeQuery(NativeQueryHolder.getFinnSisteDfuInfoDate());
			sisteDfuinfo.setParameter(1, dfuId);
			
			sisteDato = ((String)(sisteDfuinfo.getResultList().get(0)));
			SimpleDateFormat format = new SimpleDateFormat(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
			try {
				if (forrigePunktGmt.equals(format.parse(sisteDato))){
					return null;
				}
			} catch (ParseException e) {
				log.error("Klarte ikke å parse datoen:"+sisteDato);
				return null;
			}
		}else
			log.debug("Henter punkter for dfu:"+dfuId+" kjid="+kjoretoyId);
		
		List<ProdpunktVO> prodpunktList = null;
		if (tidsvindu){//spør ut alle punktene som ikke er hentet innenfor tidsvinduet
			Query q = em.createNativeQuery(NativeQueryHolder.getProdpunktInfoQuery());
			q.setParameter(1, dfuId);
//			q.setParameter(2, fra);
			q.setParameter(2, forrige.equals("")?fra:forrige);
			log.debug("Henter punkter mellom"+(sisteDato.equals("")?fra:sisteDato)+" og "+til);
			q.setParameter(3, til);
			prodpunktList = new NativeQueryWrapper<ProdpunktVO>(q, ProdpunktVO.class, new Class[]{
				byte[].class, Double.class, byte[].class, 
				Date.class, Long.class, String.class, 
				String.class, Double.class, Double.class, Double.class},
				"digiByte", "retning", "ignorerMaskByte", 
				"gmtTidspunkt", "dfuId", "GPSYY", "GPSXX", "snappetX", 
				"snappetY", "hastighet").getWrappedList();
			log.debug("fant:"+prodpunktList.size()+" punkter");
		}else{ //henter siste punkt basert på dfuinfo
			Query q = em.createNativeQuery(NativeQueryHolder.getFinnSisteDfuInfo());
			q.setParameter(1, fra);
			q.setParameter(2, til);
			q.setParameter(3, dfuId);
			
			prodpunktList = new NativeQueryWrapper<ProdpunktVO>(q, ProdpunktVO.class, new Class[]{
					byte[].class, Double.class, byte[].class, 
					Date.class, Long.class, String.class, 
					String.class, Double.class},
					"digiByte", "retning", "ignorerMaskByte", 
					"gmtTidspunkt", "dfuId", "GPSXX", "GPSYY", "hastighet").getWrappedList();
		}
		return prodpunktList;
	}
	
	/** {@inheritDoc} */
    public List<Prodpunktinfo> getProdpunktInfo(Long dfuindividId, Date datoFra, Date datoTil){
        return em.createNamedQuery(Prodpunktinfo.QUERY_FIND_PRODPUNKTINFO)
            .setParameter("dfuId", dfuindividId)
            .setParameter("fraTidspunkt", datoFra)
            .setParameter("tilTidspunkt", datoTil)
            .getResultList();
    }
    /** {@inheritDoc} */
    public Prodpunktinfo getProdpunktInfo(Long dfuId, Date date){
    	String dateStr = MipssDateFormatter.formatDate(date, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	
    	Query q = em.createNativeQuery(NativeQueryHolder.getSingleProdpunktInfoQuery());
    	q.setParameter(1, dfuId);
    	q.setParameter(2, dateStr);
    	
    	Prodpunktinfo result = new NativeQueryWrapper<Prodpunktinfo>(q, Prodpunktinfo.class, new Class[]{
    		Long.class, Long.class, String.class, String.class, Long.class, Long.class, Long.class, Date.class, Long.class}, 
    		"fylkesnummer", "kommunenummer", "veikategori", "veistatus", "veinummer", "hp", "meter", "gmtTidspunkt", "dfuId").getWrappedSingleResult();
    	return result;
    }
    /** {@inheritDoc} */
    public List<Prodtype> getProdtypeList(){
    	return em.createNamedQuery(Prodtype.FIND_ALL).getResultList();
    }

    @Override
    public Prodtype getSingleProdType(Long prodTypeId) {
    	return em.find(Prodtype.class, prodTypeId);
	}
    
    /** {@inheritDoc} */
    public List<ProdReflinkStrekning> getProduksjonStrekninger(ProduksjonQueryParams params){
    	List<ProdReflinkStrekning> alleStrekninger = new ArrayList<ProdReflinkStrekning>();
    	List<Installasjon> installasjon = em.createNamedQuery(Installasjon.QUERY_FIND).setParameter("kjoretoyId", params.getKjoretoyId()).setParameter("fraDato", params.getFraDato()).setParameter("tilDato", params.getTilDato()).getResultList();
    	
    	Date fraDato = null;
    	Date tilDato = null;
    	for (Installasjon i:installasjon){
    		//for første installasjon skal parameteret fraTidspunkt være gjeldende
    		if (fraDato==null){
    			fraDato = params.getFraDato();
    		}else{
    			fraDato = i.getAktivFraDato();
    		}
    		//hvis tilTidspunktet er innenfor aktivDato skal tilTidspunktet benyttes
    		if (i.getAktivTilDato()==null || i.getAktivTilDato().after(params.getTilDato())){
    			tilDato = params.getTilDato();
    		}else
    			tilDato = i.getAktivTilDato();

    		
    	    Query q = em.createNativeQuery(NativeQueryHolder.getProduksjonStrekningerQuery());
    	    q.setParameter(1, i.getDfuId());
    	    q.setParameter(2, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
    	    q.setParameter(3, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
    	    
    	    NativeQueryWrapper<Prodstrekning> w = new NativeQueryWrapper<Prodstrekning>(q, Prodstrekning.class, new Class[]{
    	    	Long.class, Long.class, byte[].class, Date.class, Date.class, 
    	    	Double.class, Double.class, 
    	    	Long.class, Long.class, Long.class, String.class, Long.class, String.class, 
    	    	int.class, double.class, double.class  
    	    }, "id", "dfuId", "digiByte", "fraTidspunkt", "tilTidspunkt", 
    	       "fraKm", "tilKm", 
    	       "fylkesnummer", "hp", "kommunenummer", "veikategori", "veinummer", "veistatus", 
    	       "reflinkId", "reflinkFra", "reflinkTil");
    	    List<Prodstrekning> wrappedList = w.getWrappedList();
    	    List<Prodstrekning> prodList = optimizeProdstrekning(wrappedList);
    	    
    		List<ProdReflinkStrekning> strekningList = getProdReflinkStrekningFromProdstrekning(params.getProdtyper(), params.isAllProduksjon(), i, prodList);
    		alleStrekninger.addAll(strekningList);
    	}
    	return alleStrekninger;
    }
    
    /** {@inheritDoc} */
    public List<ProdReflinkStrekning> getProduksjonStrekningerKontrakt(ProduksjonQueryParams params){
    	Date fraDato = params.getFraDato();
    	Date tilDato = params.getTilDato();
		MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		
	    Query q = em.createNativeQuery(NativeQueryHolder.getProduksjonStrekningerKontraktQuery());
	    q.setParameter(1, params.getKontraktId());
	    q.setParameter(2, params.getRodetypeId());
	    q.setParameter(3, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
	    q.setParameter(4, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));	    
	    NativeQueryWrapper<Prodstrekning> w = new NativeQueryWrapper<Prodstrekning>(q, Prodstrekning.class, new Class[]{
	    	String.class, Long.class, Long.class, 
	    	byte[].class, Date.class, Date.class, 
	    	Double.class, Double.class, 
	    	Long.class, Long.class, Long.class, String.class, Long.class, String.class, 
	    	int.class, double.class, double.class  
	    }, "prodtype", "id", "dfuId", 
	    	"digiByte", "fraTidspunkt", "tilTidspunkt", 
	       "fraKm", "tilKm", 
	       "fylkesnummer", "hp", "kommunenummer", "veikategori", "veinummer", "veistatus", 
	       "reflinkId", "reflinkFra", "reflinkTil");
	    List<Prodstrekning> wrappedList = w.getWrappedList();
    	return finalizeProdstrekninger(wrappedList);
    }
    
    public List<ProdReflinkStrekning> getProduksjonStrekningerSamShape(ProduksjonQueryParams params){
    	Date fraDato = params.getFraDato();
    	Date tilDato = params.getTilDato();
		MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		
		String prodtypeIds = konfig.hentEnForApp("studio.samwebservice", "prodtype_id").getVerdi();

	    Query q = em.createNativeQuery(NativeQueryHolder.getProduksjonStrekningerKontraktMedMaterialeQuery(prodtypeIds));
	    q.setParameter(1, params.getKontraktId());
	    q.setParameter(2, params.getRodetypeId());
	    q.setParameter(3, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
	    q.setParameter(4, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));	    
	    NativeQueryWrapper<Prodstrekning> w = new NativeQueryWrapper<Prodstrekning>(q, Prodstrekning.class, new Class[]{
	    	String.class, Long.class, Long.class, 
	    	byte[].class, Date.class, Date.class, 
	    	Double.class, Double.class, 
	    	Long.class, Long.class, Long.class, String.class, Long.class, String.class, 
	    	int.class, double.class, double.class, String.class, Double.class
	    }, 
	    	"prodtype", "id", "dfuId", 
	    	"digiByte", "fraTidspunkt", "tilTidspunkt", 
	    	"fraKm", "tilKm", 
	    	"fylkesnummer", "hp", "kommunenummer", "veikategori", "veinummer", "veistatus", 
	    	"reflinkId", "reflinkFra", "reflinkTil", "materiale", "mengde");
	    
	    
	    List<Prodstrekning> wrappedList = w.getWrappedList();
    	return finalizeProdstrekninger(wrappedList);
    }
    /**
     * Pakker sammen strekningene til et format som inneholder informasjon om veireferanser, reflinkseksjoner og geometri
     * @param list
     * @return
     */
    private List<ProdReflinkStrekning> finalizeProdstrekninger(List<Prodstrekning> list){
    	List<ProdReflinkStrekning> alleStrekninger = new ArrayList<ProdReflinkStrekning>();
    	List<Prodstrekning> prodList = optimizeProdstrekning(list);
		List<ProdReflinkStrekning> strekningList = getProdReflinkStrekningFromProdstrekningHarProdtype(prodList);
		alleStrekninger.addAll(strekningList);
		return alleStrekninger;
    }
    /**
     * Lager en objektgraf ut av prodstrekningene. Prodstrekningene ligger i listen flere ganger med
     * forskjellige reflinkseksjoner. Reflinkseksjonene for prodstrekningene samles sammen og legges
     * på ett prodstrekning objekt. 
     * 
     * (Prodstrekning kan ha flere reflinkseksjoner)
     * 
     * @param list
     * @return
     */
    private List<Prodstrekning> optimizeProdstrekning(List<Prodstrekning> list){
    	List<Prodstrekning> pList = new ArrayList<Prodstrekning>();
    	Prodstrekning prev = null;
    	for (Prodstrekning p:list){
    		Prodreflinkseksjon prs = p.getReflinkseksjonTemp();
    		if (prs.getReflinkIdent()!=0)
    			p.getProdreflinkseksjonList().add(prs);
			
    		if(prev!=null){
    			//samme strekning som forrige
    			if(prev.getId().intValue()==p.getId().intValue()){
    				prs.setProdstrekning(prev);
    				if (prs.getReflinkIdent()!=0)
    					prev.getProdreflinkseksjonList().add(prs);
    			}else{ //ny strekning
    				pList.add(p);
    				prev = p;
    			}
    		}else{
    			pList.add(p);
    			prev = p;
    		}
    	}
    	return pList;
    }
    
    /**
     * Henter et kjøretøy med en gitt id
     */
    public Kjoretoy getKjoretoyWithId(Long kjoretoyId){
    	return em.find(Kjoretoy.class, kjoretoyId);
    }
    
    /**
     * Henter et dfuindivid med en gitt id
     */
    public Dfuindivid getDfuWithId(Long dfuId){
    	return em.find(Dfuindivid.class, dfuId);
    }
    
    
    private List<ReflinkSeksjon> getReflinkseksjonFromProdstrekning(Prodstrekning ps){
    	Veireferanse veiref= NvdbTranslator.translateFromProdstrekningToVeireferanse(ps);
    	List<ReflinkSeksjon> reflinkSections = SerializableReflinkSection.createWebserviceSections(service.getReflinkSectionsWithinRoadref(new SerializableRoadRef(veiref)));
    	return reflinkSections;
    }
    
    /** {@inheritDoc} */
    public List<ReflinkStrekning> getReflinkStrekningFromProdstrekning(List<Prodstrekning> list, boolean merge){
    	log.debug("getReflinkStrekningFromProdstrekning()");
    	List<ReflinkSeksjon> reflinks = new ArrayList<ReflinkSeksjon>();
    	for (Prodstrekning ps:list){
    		if (merge)
    			ps = em.merge(ps);
    		if (ps.getProdreflinkseksjonList().isEmpty()){
    			Veireferanse veiref= NvdbTranslator.translateFromProdstrekningToVeireferanse(ps);
				List<ReflinkSeksjon> wsreflinker = SerializableReflinkSection.createWebserviceSections(service.getReflinkSectionsWithinRoadref(new SerializableRoadRef(veiref)));
				reflinks.addAll(wsreflinker);
    		}else{
    			List<ReflinkSeksjon> wsreflinker = convertProdreflinkseksjonToReflinkSeksjon(ps.getProdreflinkseksjonList());
    			reflinks.addAll(wsreflinker);
    		}
    	}
    	log.debug("sender til webservice..");
    	List<ReflinkStrekning> rs = service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinks));
    	return rs;
    }
    
    public List<ReflinkStrekning> getReflinkStrekningFromProdreflinkseksjon(List<Prodreflinkseksjon> list){
    	return service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(convertProdreflinkseksjonToReflinkSeksjon(list)));
    }
    
    /**
     * Lager tegnbar geometri med data om strekningene fra en liste med Prodstrekning
     * 
     * @param prodtyper prodtyper som skal v�re med, de som ikke finnes i denne listen vil ikke v�re med 
     * i resultatet. Denne kan v�re null som f�rer til at alle prodtyper blir med i resultatet
     * @param allProdtyper alle prodtypene skal v�re med, men kun de som ligger i prodtyper listen skal f� en egen prodtype-farge. Alle andre prodtyper f�r fargen 0xb400d5
     * @param installasjon installasjonen som var aktiv p� tidspunktet for Prodstrekningen, kan v�re null for enheter uten installasjoner f.eks PDA
     * @param prodList listen med produserte strekninger
     * 
     * @return
     */
	private List<ProdReflinkStrekning> getProdReflinkStrekningFromProdstrekning(List<Prodtype> prodtyper, boolean allProduksjon, Installasjon installasjon, List<Prodstrekning> prodList) {
		boolean brukKvernemotorReflinker = konfig.hentEnForApp("mipss.produksjonservice", "brukKvernemotorReflinkSections").getVerdi().equals("1");
		log.trace("brukKvernemotorReflinker="+brukKvernemotorReflinker);
		long pstart =System.currentTimeMillis();
		Map<String, List<ProdtypeVO>> prodtypeMap = new HashMap<String, List<ProdtypeVO>>();
		List<ProdReflinkStrekning> strekningList = new ArrayList<ProdReflinkStrekning>();
		List<ReflinkSeksjon> reflinks = new ArrayList<ReflinkSeksjon>();
		for (Prodstrekning ps:prodList){
			if (brukKvernemotorReflinker){
				log.trace("Bruker reflinkseksjoner til å lage geometri");
				if (ps.getProdreflinkseksjonList().isEmpty()){
					log.debug("Prodstrekning inneholder ingen reflinkseksjoner, bruker prodstrekning..");
					brukKvernemotorReflinker=false;
				}else{
					List<ReflinkSeksjon> wsreflinker = convertProdreflinkseksjonToReflinkSeksjon(ps.getProdreflinkseksjonList());
					reflinks.addAll(wsreflinker);
				}
			}
			if (!brukKvernemotorReflinker){
				ProdReflinkStrekning s = new ProdReflinkStrekning();
				s.setProdstrekning(ps);
				log.debug("Bruker ikke reflinkseksjoner til å lage geometri");
				Veireferanse veiref= NvdbTranslator.translateFromProdstrekningToVeireferanse(ps);
				List<ReflinkSeksjon> reflinkSections = SerializableReflinkSection.createWebserviceSections(service.getReflinkSectionsWithinRoadref(new SerializableRoadRef(veiref)));
				s.setStrekningList(service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinkSections)));
				String digi = ps.getDigi();
				List<ProdtypeVO> prodtypeListForDigi = prodtypeMap.get(digi);
				if (prodtypeListForDigi==null){
					if (installasjon!=null){
						prodtypeListForDigi = getProdtypeForDigi(installasjon.getKjoretoy().getId(), digi, ps.getFraTidspunkt(), prodtyper, allProduksjon);
						prodtypeMap.put(digi, prodtypeListForDigi);
					}else{
						//for inspeksjoner....
						prodtypeListForDigi = getProdtypeForDigi(null, digi, null, null, false);
						prodtypeMap.put(digi, prodtypeListForDigi);
					}
				}
				if (prodtyper!=null&&!allProduksjon&&prodtypeListForDigi.isEmpty()){
				}else{
					if (prodtypeListForDigi!=null&&!prodtypeListForDigi.isEmpty()){
						s.setProdtypeList(prodtypeListForDigi);
						strekningList.add(s);
					}
				}
			}
		}
		
		List<ReflinkStrekning> rs = service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinks));
		List<ProdReflinkStrekning> merged = mergeReflinkStrekningAndReflinkseksjon(prodList, rs);
		for (ProdReflinkStrekning prs:merged){
			String digi = prs.getProdstrekning().getDigi();
			List<ProdtypeVO> prodtypeListForDigi = prodtypeMap.get(digi);
			if (prodtypeListForDigi==null){
				if (installasjon!=null){
					prodtypeListForDigi = getProdtypeForDigi(installasjon.getKjoretoy().getId(), digi, prs.getProdstrekning().getFraTidspunkt(), prodtyper, allProduksjon);
					prodtypeMap.put(digi, prodtypeListForDigi);
				}else{
					//for inspeksjoner....
					prodtypeListForDigi = getProdtypeForDigi(null, digi, null, null, false);
					prodtypeMap.put(digi, prodtypeListForDigi);
				}
			}
			if (prodtyper!=null&&!allProduksjon&&prodtypeListForDigi.isEmpty()){
			}else{
				if (prodtypeListForDigi!=null&&!prodtypeListForDigi.isEmpty()){
					prs.setProdtypeList(prodtypeListForDigi);
					strekningList.add(prs);
				}
			}
		}
		log.debug("Ferdig: konverterte "+prodList.size()+" prodstrekninger til "+strekningList.size()+" geometri objekter på"+(System.currentTimeMillis()-pstart));
		
		return strekningList;
	}

	private List<ProdReflinkStrekning> getProdReflinkStrekningFromProdstrekningHarProdtype(List<Prodstrekning> prodList) {
		boolean brukKvernemotorReflinker = konfig.hentEnForApp("mipss.produksjonservice", "brukKvernemotorReflinkSections").getVerdi().equals("1");
		long pstart =System.currentTimeMillis();
		List<ReflinkSeksjon> reflinks = new ArrayList<ReflinkSeksjon>();
		for (Prodstrekning ps:prodList){
			if (brukKvernemotorReflinker){
				log.trace("Bruker reflinkseksjoner til å lage geometri");
				if (ps.getProdreflinkseksjonList().isEmpty()){
					log.debug("Prodstrekning inneholder ingen reflinkseksjoner, bruker prodstrekning..");
					brukKvernemotorReflinker=false;
				}else{
					List<ReflinkSeksjon> wsreflinker = convertProdreflinkseksjonToReflinkSeksjon(ps.getProdreflinkseksjonList());
					reflinks.addAll(wsreflinker);
				}
			}
			if (!brukKvernemotorReflinker){
//				ProdReflinkStrekning s = new ProdReflinkStrekning();
//				s.setProdstrekning(ps);
				
//				log.debug("Bruker ikke reflinkseksjoner til å lage geometri");
				Veireferanse veiref= NvdbTranslator.translateFromProdstrekningToVeireferanse(ps);
				List<ReflinkSeksjon> reflinkSections = SerializableReflinkSection.createWebserviceSections(service.getReflinkSectionsWithinRoadref(new SerializableRoadRef(veiref)));
				reflinks.addAll(reflinkSections);
//				s.setStrekningList(service.getGeometryWithinReflinkSections(reflinkSections));
			}
		}
		
		List<ReflinkStrekning> rs = service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinks));
		List<ProdReflinkStrekning> merged = mergeReflinkStrekningAndReflinkseksjon(prodList, rs);
		
		log.debug("Ferdig: konverterte "+prodList.size()+" prodstrekninger til "+merged.size()+" geometri objekter på"+(System.currentTimeMillis()-pstart));
		
		return merged;
	}
	/** {@inheritDoc} */
    public List<ProdReflinkStrekning> getProduksjonStrekningerDfu(ProduksjonQueryParams params){//Long dfuId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon) {
    	long start = System.currentTimeMillis();
    	List<Prodstrekning> prodList = em.createNamedQuery(Prodstrekning.QUERY_FIND_LAZY)
		.setParameter("dfuId", params.getDfuId())
		.setParameter("fraTidspunkt", params.getFraDato())
		.setParameter("tilTidspunkt", params.getTilDato())
		.getResultList();
    	log.debug("Hentet:"+prodList.size()+" prodstrekninger fra db på:"+(System.currentTimeMillis()-start)+"ms");
    	return getProdReflinkStrekningFromProdstrekning(params.getProdtyper(), params.isAllProduksjon(), null, prodList);
    }
    
    /** {@inheritDoc} */
    public List<ProdpunktVO> getProduksjonPunktDfu(ProduksjonQueryParams params){//Long dfuId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon){
    	return getProdpunktListFromProdpunkt(params.getProdtyper(), params.isAllProduksjon(), params.getFraDato(), params.getTilDato(), null, params.getDfuId());
    }
    
    /** {@inheritDoc} */
    public List<ProdpunktVO> getProduksjonPunktKjoretoy(ProduksjonQueryParams params){//Long kjoretoyId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon){
    	List<Installasjon> installasjon = getInstallasjonFromKjoretoyAndTime(params.getKjoretoyId(), params.getFraDato(), params.getTilDato());//em.createNamedQuery(Installasjon.QUERY_FIND).setParameter("kjoretoyId", params.getKjoretoyId()).setParameter("fraDato", params.getFraDato()).setParameter("tilDato", params.getTilDato()).getResultList();
    	log.debug("fant:"+installasjon.size()+" installasjoner for kjoretoyId:"+params.getKjoretoyId()+" i perioden "+params.getFraDato()+" - "+params.getTilDato());
    	//cacher prodtyper for digitale inn bits for en installasjon
    	List<ProdpunktVO> punktList = new ArrayList<ProdpunktVO>();
    	
    	Date fraDato = null;
    	Date tilDato = null;
    	for (Installasjon i:installasjon){
    		
    		//for første installasjon skal parameteret fraTidspunkt være gjeldende
    		if (fraDato==null){
    			fraDato = params.getFraDato();
    		}else{
    			fraDato = i.getAktivFraDato();
    		}
    		//hvis tilTidspunktet er innenfor aktivDato skal tilTidspunktet benyttes
    		if (i.getAktivTilDato()==null || i.getAktivTilDato().after(params.getTilDato())){
    			tilDato = params.getTilDato();
    		}else
    			tilDato = i.getAktivTilDato();
    		log.debug("Henter prodpunkt for kjøretøy:"+i.getKjoretoy()+" "+fraDato+"-"+tilDato);
    		List<ProdpunktVO> prodpunktList = getProdpunktListFromProdpunkt(params.getProdtyper(), params.isAllProduksjon(), fraDato, tilDato, i, i.getDfuId());
    		punktList.addAll(prodpunktList);
    		log.debug("La til:"+prodpunktList.size()+" punkter for kjøretøy:"+params.getKjoretoyId());
    	}
    	log.debug("getProduksjonPunktKjoretoy() done..");
    	return punktList;
    }
    private List<Installasjon> getInstallasjonFromKjoretoyAndTime(Long kjoretoyId, Date fraDato, Date tilDato){
    	String q = NativeQueryHolder.getInstallasjonFromKjoretoyAndTid();
    	String fDato = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.DATE_TIME_FORMAT);
		String tDato = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.DATE_TIME_FORMAT);
		
    	Query query = em.createNativeQuery(q).setParameter(1, fDato)
    	.setParameter(2, tDato)
    	.setParameter(3, kjoretoyId);
    	
    	List<Installasjon> installasjonList = new NativeQueryWrapper<Installasjon>(query, Installasjon.class, 
    			new Class[]{Long.class, Date.class, Date.class}, 
    			new String[]{"dfuId", "aktivFraDato", "aktivTilDato"})
    			.getWrappedList();
    	if (installasjonList!=null){
	    	for (Installasjon i:installasjonList){
	    		Kjoretoy k = new Kjoretoy();
	    		k.setId(kjoretoyId);
	    		i.setKjoretoy(k);
	    	}
    	}
    	return installasjonList;
    	
    }
    public List<ProdpunktVO> getPdopPunkterKjoretoy(ProduksjonQueryParams params){
       	List<Installasjon> installasjon = em.createNamedQuery(Installasjon.QUERY_FIND).setParameter("kjoretoyId", params.getKjoretoyId()).setParameter("fraDato", params.getFraDato()).setParameter("tilDato", params.getTilDato()).getResultList();
       	List<ProdpunktVO> punktList = new ArrayList<ProdpunktVO>();
    	
    	Date fraDato = null;
    	Date tilDato = null;
    	for (Installasjon i:installasjon){
    		
    		//for første installasjon skal parameteret fraTidspunkt være gjeldende
    		if (fraDato==null){
    			fraDato = params.getFraDato();
    		}else{
    			fraDato = i.getAktivFraDato();
    		}
    		//hvis tilTidspunktet er innenfor aktivDato skal tilTidspunktet benyttes
    		if (i.getAktivTilDato()==null || i.getAktivTilDato().after(params.getTilDato())){
    			tilDato = params.getTilDato();
    		}else
    			tilDato = i.getAktivTilDato();
    		log.debug("Henter prodpunkt for kjøretøy:"+i.getKjoretoy());
    		
    		String fDato = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(fraDato), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    		String tDato = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(tilDato), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

    		
    		List<ProdpunktVO> prodpunktList = getPdopPunkter(i.getDfuId(), fDato, tDato);
    		punktList.addAll(prodpunktList);
    		log.debug("La til:"+prodpunktList.size()+" punkter for kjøretøy:"+params.getKjoretoyId());
    	}
    	log.debug("getProduksjonPunktKjoretoy() done..");
    	return punktList;

    }
    
    public List<ProdpunktVO> getPdopPunkterDFU(ProduksjonQueryParams params){
		String fDato = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(params.getFraDato()), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String tDato = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(params.getTilDato()), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		return getPdopPunkter(params.getDfuId(), fDato, tDato);
    }
    
    public List<ProdpunktVO> getPdopPunkter(Long dfuId, String fraDato, String tilDato){
		Query q = em.createNativeQuery(NativeQueryHolder.getPdopPunkterQuery());
		q.setParameter(1, dfuId);
		q.setParameter(2, fraDato);
		q.setParameter(3, tilDato);

		List<ProdpunktVO> prodpunktList = new NativeQueryWrapper<ProdpunktVO>(q, ProdpunktVO.class, new Class[]{
			byte[].class, String.class, String.class, Double.class, Double.class, Double.class, byte[].class, 
			Date.class, Long.class, Double.class, Double.class},
			"digiByte", "GPSXX", "GPSYY", "snappetX", "snappetY", "retning", "ignorerMaskByte", 
			"gmtTidspunkt", "dfuId", "pdop", "hastighet").getWrappedList();
		
		List<ProdpunktVO> list = new ArrayList<ProdpunktVO>();
		for (ProdpunktVO p:prodpunktList){
			if (p.getDigi().equals("0000")|| ( (p.getSnappetX()==null||p.getSnappetY()==null) && (p.getGpsX()==null||p.getGpsY()==null)) )
				continue;
			list.add(p);
			
		}
		return list;
    }
    
    /**
     * Samler sammen prodpunkter og produksjonstyper for en enhet. Dato spesifiseres i CET og konverteres i kallet til GMT (dataene ligger i db som GMT)
     * 
     * @param prodtyper listen med prodtyper som skal være med i utvalget, prodtyper som ikke ligger her vil ikke komme med.
     * @param allProduksjon true hvis man allikevel vil ha med all produksjon, denne produksjonen vil komme med en egen farge
     * @param fraDato første dato for utvalget (CET)
     * @param tilDato andre dato for utvalget (CET)
     * @param installasjon installasjonen i det aktuelle tidsrommet for dfu det skal hentes data for.  null for enheter som ikke har Installasjon f.eks PDA. 
     * @param dfuId id til enheten
     * 
     * @return
     */
	private List<ProdpunktVO> getProdpunktListFromProdpunkt(List<Prodtype> prodtyper, boolean allProduksjon, Date fraDato, Date tilDato, Installasjon installasjon, Long dfuId) {
		Map<String, List<ProdtypeVO>> prodtypeMap = new HashMap<String, List<ProdtypeVO>>();
		List<ProdpunktVO> punktList = new ArrayList<ProdpunktVO>();
		
		log.debug("Henter produksjon for dfu:"+dfuId+" - "+fraDato+" - "+tilDato);
		
		String fdato = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(fraDato), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String tdato = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(tilDato), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		
		Query q = em.createNativeQuery(NativeQueryHolder.getProdpunktInfoQuery());
		q.setParameter(1, dfuId);
		q.setParameter(2, fdato);
		q.setParameter(3, tdato);
		
		List<ProdpunktVO> prodpunktList = new NativeQueryWrapper<ProdpunktVO>(q, ProdpunktVO.class, new Class[]{
				byte[].class, Double.class, byte[].class, 
				Date.class, Long.class, String.class, 
				String.class, Double.class, Double.class, Double.class},
				"digiByte", "retning", "ignorerMaskByte", 
				"gmtTidspunkt", "dfuId", "GPSYY", "GPSXX", "snappetX", 
				"snappetY", "hastighet").getWrappedList();
		
		log.debug("fant:"+prodpunktList.size()+" punkter");
		for (ProdpunktVO p:prodpunktList){
			if (p.getDigi().equals("0000")|| ( (p.getSnappetX()==null||p.getSnappetY()==null) && (p.getGpsX()==null||p.getGpsY()==null)) )
				continue;
			String digi = p.getDigiAndMask();
			List<ProdtypeVO> prodtypeListForDigi = prodtypeMap.get(digi);
			log.trace("prodtypeList for digi:"+digi+" "+prodtypeListForDigi);
			
			//prodtype er ikke cachet opp, putt i cache. 
			if (prodtypeListForDigi==null){
				if (installasjon!=null){
					prodtypeListForDigi = getProdtypeForDigi(installasjon.getKjoretoy().getId(), digi, p.getGmtTidspunkt(), prodtyper, allProduksjon);
					prodtypeMap.put(digi, prodtypeListForDigi);
					log.debug("installasjon:"+installasjon);
				}else if (digi.equals("1001")){
					prodtypeListForDigi = getProdtypeForDigi(null, digi, p.getGmtTidspunkt(), prodtyper, allProduksjon);
					prodtypeMap.put(digi, prodtypeListForDigi);
				}
			}
			
			if (prodtyper!=null&&!allProduksjon&&(prodtypeListForDigi==null||prodtypeListForDigi.isEmpty())){
				log.debug("nothing to do...");
			}else{
				if (prodtypeListForDigi!=null&&!prodtypeListForDigi.isEmpty()){
					p.setProdtypeList(prodtypeListForDigi);
					punktList.add(p);
					log.trace("added point.."+p);
				}
			}
		}
		log.debug("returnerer:"+punktList.size()+" punkter");
		return punktList;
	}
	
	public ProdpunktVO getProdpunkt(Long dfuId, Date datoGmt){
		String gmtDato = MipssDateFormatter.formatDate(datoGmt, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String qry = NativeQueryHolder.getProdpunktInfoSingleQuery();
		
		Query q = em.createNativeQuery(qry);
		q.setParameter(1, dfuId);
		q.setParameter(2, gmtDato);
		ProdpunktVO prodpunkt = new NativeQueryWrapper<ProdpunktVO>(q, ProdpunktVO.class, new Class[]{
			byte[].class, Double.class, byte[].class, 
			Date.class, Long.class, String.class, 
			String.class, Double.class, Double.class, Double.class},
			"digiByte", "retning", "ignorerMaskByte", 
			"gmtTidspunkt", "dfuId", "GPSYY", "GPSXX", "snappetX", 
			"snappetY", "hastighet").getWrappedSingleResult();
		
		return prodpunkt;
	}
    /** {@inheritDoc} */
    public ProdpunktVO getSisteProduksjonPunktKjoretoy(ProduksjonQueryParams params){
    	ProdpunktVO p=null;
    	try{
    		Date fdato = DateFormatter.convertFromCETtoGMT(params.getFraDato());
    		Date tdato = DateFormatter.convertFromCETtoGMT(params.getTilDato());
    		
	    	String fra = MipssDateFormatter.formatDate(fdato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
	    	String til = MipssDateFormatter.formatDate(tdato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
	    	int dfuId = getDfuIdFromKjoretoy(params.getKjoretoyId(), fra, til);
	    	log.debug("Fant dfu_id="+dfuId+" til kjøretøy med id:"+params.getKjoretoyId());
	    	Query q = em.createNativeQuery(NativeQueryHolder.getSisteProdpunktQuery());
	    	
	    	
	    	q.setParameter(1, dfuId);
	    	q.setParameter(2, til);
	    	q.setParameter(3, fra);
	    	
	    	List<ProdpunktVO> wrappedList = new NativeQueryWrapper<ProdpunktVO>(q, ProdpunktVO.class, new Class[]{
	    		byte[].class, Double.class, byte[].class, Date.class, Long.class, String.class, String.class, Double.class, Double.class}, 
	    		"digiByte", "retning", "ignorerMaskByte", "gmtTidspunkt", "dfuId", "GPSXX", "GPSYY", "snappetX", "snappetY").getWrappedList();
	    	p = wrappedList.get(0);
	    	log.debug("Fant "+wrappedList.size()+" punkter");
	    	if (p.getGmtTidspunkt()==null)
	    		return null;
	    	p.setProdtypeList(getProdtypeForDigi(params.getKjoretoyId(), p.getDigiAndMask(), p.getGmtTidspunkt(), params.getProdtyper(), params.isAllProduksjon()));
	    	
    	} catch (IndexOutOfBoundsException  e){
    		log.debug(e.getMessage());
    	}
    	return p;
    }
    
    @SuppressWarnings("rawtypes")
	private int getDfuIdFromKjoretoy(Long kjoretoyId, String fra, String til){
    	Query q = em.createNativeQuery(NativeQueryHolder.getDfuIdFromKjoretoyQuery());
    	q.setParameter(1, kjoretoyId);
    	q.setParameter(2, fra);
    	q.setParameter(3, til);
    	List resultList = q.getResultList();
    	if (resultList.isEmpty()){
    		q = em.createNativeQuery(NativeQueryHolder.getDfuIdFromKjoretoyQuery2());
        	q.setParameter(1, kjoretoyId);
        	q.setParameter(2, fra);
        	q.setParameter(3, til);
        	resultList = q.getResultList();
        	if (resultList.isEmpty())
        		return -1;
    	}
    	int id = ((BigDecimal)(resultList.get(0))).intValue();
    	return id;
    }

    /** {@inheritDoc} */
    public ProdpunktVO getSisteProduksjonPunktDfu(ProduksjonQueryParams params){//Long dfuId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon){
    	ProdpunktVO p = null;
    	try{
    		Date fdato = DateFormatter.convertFromCETtoGMT(params.getFraDato());
    		Date tdato = DateFormatter.convertFromCETtoGMT(params.getTilDato());
    		
    		Prodpunktinfo prodpunkt = (Prodpunktinfo)em.createNamedQuery(Prodpunktinfo.QUERY_FIND_LAST)
    							.setParameter("dfuId", params.getDfuId())
    							.setParameter("tilTidspunkt", tdato)
    							.setParameter("fraTidspunkt", fdato)
    							.getSingleResult();
    		
	    	p = new ProdpunktVO();
	    	p.setDigi(prodpunkt.getProdpunkt().getDigi());
	    	p.setRetning(prodpunkt.getProdpunkt().getRetning());
	    	p.setIgnorerMask(prodpunkt.getProdpunkt().getIgnorerMaske());
	    	p.setGmtTidspunkt(prodpunkt.getGmtTidspunkt());
	    	p.setDfuId(prodpunkt.getDfuId());
	    	p.setSnappetX(prodpunkt.getSnappetX());
	    	p.setSnappetY(prodpunkt.getSnappetY());
    	}catch (NoResultException e){
    		
    	}
    	return p;
    }
    //TODO HER ER DET NOE GALT!!!
    @SuppressWarnings("rawtypes")
	public String getLeverandorNavnDfu(Long dfuId, Date fraDatoCet, Date tilDatoCet){
    	String datoCetFra = MipssDateFormatter.formatDate(fraDatoCet, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	String datoCetTil = MipssDateFormatter.formatDate(tilDatoCet, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	
    	Query q = em.createNativeQuery(NativeQueryHolder.getDfuIdFromKjoretoyQuery());
    	q.setParameter(1, dfuId);
    	q.setParameter(2, datoCetFra);
    	q.setParameter(3, datoCetTil);
    	
    	List resultList = q.getResultList();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			String leverandorNavn = (String)o;
    			return leverandorNavn;
    		}
    	}
    	return null;
    }
    
    
    
    /**
     * Henter prodtyper for digital inn bits for et kj�ret�y p� et gitt tidspunkt.
     * @param kjoretoyId
     * @param digi
     * @param tidspunkt
     * @return
     */
    private List<ProdtypeVO> getProdtypeForDigi(Long kjoretoyId, String digi, Date tidspunkt, List<Prodtype> prodtyper, boolean allProduksjon){
    	List<ProdtypeVO> prodtypeList = new ArrayList<ProdtypeVO>();
    	if (digi.equals("1001")){
    		log.debug("traff inspeksjon");
    		Prodtype pt = new Prodtype();
    		pt.setGuiFarge("0x0000ff");
    		pt.setNavn("Inspeksjon");
    		ProdtypeVO pr = new ProdtypeVO();
    		pr.setProdtype(pt);
    		pr.setUtstyrsnavn("PDA");
    		prodtypeList.add(pr);
    		return prodtypeList;
    	}
    	Integer bit = Integer.parseInt(digi, 16);
    	if ((bit&0x800)==0x800){//0x800 bit 11 satt kontinuerlig friksjonsm�ler. 
    		
    		Prodtype pt = new Prodtype();
    		pt.setNavn(resources.getGuiString("kont.friksjonsmaler"));
    		pt.setGuiFarge("0x2222ff");
    		ProdtypeVO pr = new ProdtypeVO();
    		pr.setProdtype(pt);
    		pr.setUtstyrsnavn(resources.getGuiString("friksjonsmaler"));
    		prodtypeList.add(pr);
    	}
    	
    	
    	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yy hh:mm:ss");
    	Query query = em.createNativeQuery(NativeQueryHolder.getProdtypeFromDigiQuery());
    	query.setParameter(1, kjoretoyId);
    	query.setParameter(2, digi);
    	query.setParameter(3, df.format(tidspunkt));
    	List<ProdtypeVO> prodlist = new NativeQueryWrapper<ProdtypeVO>(query, ProdtypeVO.class, new Class[]{Long.class, String.class, Integer.class}, "prodtypeId", "utstyrsnavn", "bitNr" ).getWrappedList();
    	if (prodlist.size()>0){
	    	log.debug("Fant :"+prodlist.size()+" prodtyper for digi:"+digi+" på tidspunkt:"+tidspunkt+":"+prodlist.get(0).getProdtypeId());
	    	Prodtype tomkjoring = getSingleProdType(98L);
	    	String fargetomkjoring = konfig.hentEnForApp("studio.mipss.mipssmap", "fargeTomkjoring").getVerdi();
			tomkjoring.setGuiFarge(fargetomkjoring);
			String fargeannen = konfig.hentEnForApp("studio.mipss.mipssmap", "fargeProduksjon").getVerdi();
			
	    	for (ProdtypeVO pr:prodlist){
	    		Prodtype prodtype = em.find(Prodtype.class, pr.getProdtypeId());
	    		
	    		if (prodtyper!=null){
	    			if (allProduksjon){
	    				boolean add = true;
	    				if (prodtype.getId().intValue()==98){
	    					add = false;
	    				}
	    				if (prodtyper.contains(tomkjoring))
	    					add = true;
	    				
	    				if (add){
			    			if (!prodtyper.contains(prodtype)){
			    				Prodtype pt = new Prodtype();
			    				pt.setGuiFarge(fargeannen);
			    				pt.setId(new Long(999));
			    				pt.setNavn(prodtype.getNavn());
			    				pt.setSesong(prodtype.getSesong());
			    				pr.setProdtype(pt);
			    			}else{
			    				pr.setProdtype(prodtype);
			    			}
			    			prodtypeList.add(pr);
	    				}
	    			}else{
	    				if (prodtyper.contains(prodtype)){
	    					pr.setProdtype(prodtype);
	    					prodtypeList.add(pr);
	    				}
	    			}
	    		}else{
		    		pr.setProdtype(prodtype);
		    		log.debug("Looked up prodtype:"+prodtype);
		    		prodtypeList.add(pr);
	    		}
	    	}
    	}
    	
    	return prodtypeList;
    }
    
    /**
     * Slår sammen listen med prodstrekninger og listen med ReflinkStrekninger. 
     * Veldig ineffektiv sånn den står nå..
     * @param prodList
     * @param pointList
     * @return
     */
    private List<ProdReflinkStrekning> mergeReflinkStrekningAndReflinkseksjon(List<Prodstrekning> prodList, List<ReflinkStrekning> pointList){
    	log.debug("mergeReflinkStrekningAndReflinkseksjon("+prodList.size()+", "+pointList.size());
    	
    	List<ProdReflinkStrekning> strekningList = new ArrayList<ProdReflinkStrekning>();
    	for (Prodstrekning p:prodList){
    		log.trace("prodstrekning..reflinkseksjoner:"+p.getProdreflinkseksjonList().size());
    		ProdReflinkStrekning prodReflinkStrekning = new ProdReflinkStrekning();
    		prodReflinkStrekning.setProdstrekning(p);
			List<Prodreflinkseksjon> convertedList = convertToAndFromOnProdreflinksjoner(p.getProdreflinkseksjonList());
    		for (Prodreflinkseksjon pr: convertedList){
    			boolean found = false;
	    		for (ReflinkStrekning strekning: pointList){
		    		if (pr.getReflinkIdent()==strekning.getReflinkId()&& 
		    			pr.getFra()==strekning.getFra() &&
		    			pr.getTil()==strekning.getTil()){
		    			prodReflinkStrekning.addReflinkStrekning(strekning);
		    			log.trace("Added strekning to reflinkstrekning..");
		    			found = true;
		    			break; //Går ut av løkken første gang man finner treff. Kan ligge mange like reflinkStrekninger i lista.
		    		}
	    		}
	    		if (!found){
	    			log.error("No match found for:"+pr.getReflinkIdent()+" "+pr.getFra()+"-"+pr.getTil());
	    		}
    		}
    		strekningList.add(prodReflinkStrekning);
    	}
    	return strekningList;	
    }
    
    /**
     * Filtrert listen med strekninger til kun å inneholde strekninger på veiene i veilisten
     * @param params
     * @param prodstrekninger
     * @return
     */
    private List<Prodreflinkseksjon> filterList(InspeksjonQueryParams params, List<Prodreflinkseksjon> prodstrekninger){
    	if (prodstrekninger==null||prodstrekninger.isEmpty())
    		return prodstrekninger;
    	List<Veinettveireferanse> gyldigListe = params.getVeiListe();
    	if (gyldigListe==null||gyldigListe.size()==0){
    		return prodstrekninger;
    	}
    	List<Prodreflinkseksjon> filtrert = new ArrayList<Prodreflinkseksjon>();
    	for (Prodreflinkseksjon pr:prodstrekninger){
    		if (containsVei(gyldigListe, pr.getProdstrekning())){
    			filtrert.add(pr);
    		}
    	}
    	return filtrert;
    }
    private List<Prodstrekning> filterListProdstrekning(InspeksjonQueryParams params, List<Prodstrekning> prodstrekninger){
    	if (prodstrekninger==null||prodstrekninger.isEmpty())
    		return prodstrekninger;
    	List<Veinettveireferanse> gyldigListe = params.getVeiListe();
    	if (gyldigListe==null||gyldigListe.size()==0){
    		return prodstrekninger;
    	}
    	List<Prodstrekning> filtrert = new ArrayList<Prodstrekning>();
    	for (Prodstrekning pr:prodstrekninger){
    		if (containsVei(gyldigListe, pr)){
    			filtrert.add(pr);
    		}
    	}
    	return filtrert;
    }
    /**
     * Sjekker om prodstrekning er representert i listen med veireferanser
     * @param list
     * @param p
     * @return
     */
    private boolean containsVei(List<Veinettveireferanse> list, Prodstrekning p){
    	for (Veinettveireferanse v:list){
    		boolean eq = new EqualsBuilder()
    		.append(p.getFylkesnummer(), v.getFylkesnummer())
    		.append(p.getKommunenummer(), v.getKommunenummer())
    		.append(p.getVeikategori(), v.getVeikategori())
    		.append(p.getVeistatus(),v.getVeistatus())
    		.append(p.getVeinummer(), v.getVeinummer())
    		.append(p.getHp(), v.getHp()).isEquals();
    		if (eq)
    			return true;
    	}
    	return false;
    }
    
    public List<ProdReflinkStrekning> hentInspeksjonStrekninger(String guid, InspeksjonQueryParams params){
    	log.debug("hentInspeksjonStrekninger ("+guid+")");
    	Query query = em.createNativeQuery(NativeQueryHolder.getInspeksjonStrekningerQuery());
    	query.setParameter(1, guid);
    	
    	List<Prodreflinkseksjon> wrappedList = new NativeQueryWrapper<Prodreflinkseksjon>(query, Prodreflinkseksjon.class, new Class[]{Long.class, int.class, double.class, double.class}, "strekningId", "reflinkIdent", "fra", "til" ).getWrappedList();
//    	wrappedList = filterList(params, wrappedList);//TODO denne vil feile ettersom det ikke er hentet noen prodstrekninger
    	
    	List<ReflinkStrekning> strekninger =null;
    	List<ProdReflinkStrekning> plist=null;
    	
    	String strekningIds = "";

    	if (wrappedList==null||wrappedList.isEmpty()){
        	//forhåpendligvis kan vi fjerne dette når strekningene er kværnet opp igjen...
    		boolean brukKvernemotorReflinker = konfig.hentEnForApp("mipss.produksjonservice", "brukKvernemotorReflinkSections").getVerdi().equals("1");
    		List<Prodstrekning> prodList= em.createQuery("SELECT p.strekninger from Inspeksjon p where p.guid=:guid").setParameter("guid", guid).getResultList();
        	prodList = filterListProdstrekning(params, prodList);
        	if (prodList==null||prodList.size()==0){
        		return null;
        	}
        	List<Long> psIds = new ArrayList<Long>();
        	for (Prodstrekning ps:prodList){
        		psIds.add(ps.getId());
        	}
        	List<Prodreflinkseksjon> prodreflinkseksjoner = em.createQuery("SELECT p.prodreflinkseksjonList from Prodstrekning p where p.id in ?1").setParameter(1, psIds).getResultList();
        	for (Prodstrekning p:prodList){
        		for (Prodreflinkseksjon pr:prodreflinkseksjoner){
        			if (pr.getStrekningId().equals(p.getId())){
        				p.addProdreflinkseksjon(pr);
        			}
        		}
        	}
        	
    		List<ProdReflinkStrekning> strUtenReflinks = new ArrayList<ProdReflinkStrekning>();
    		List<ReflinkSeksjon> reflinks = new ArrayList<ReflinkSeksjon>();
    		for (Prodstrekning p:prodList){
    			boolean overriddenBrukKvernemotorReflinker = false;
    			if (brukKvernemotorReflinker){
    				if (p.getProdreflinkseksjonList().size()==0){
    					overriddenBrukKvernemotorReflinker  = true;
    				}else{
    					List<ReflinkSeksjon> c = convertProdreflinkseksjonToReflinkSeksjon(p.getProdreflinkseksjonList());
	        			reflinks.addAll(c);	
    				}
    			}
    			if (!brukKvernemotorReflinker||overriddenBrukKvernemotorReflinker){
    				List<ReflinkSeksjon> refList = getReflinkseksjonFromProdstrekning(p);
    				ProdReflinkStrekning s = new ProdReflinkStrekning();
    				s.setStrekningList(service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(refList)));
    				s.setProdstrekning(p);
    				strUtenReflinks.add(s);
    			}
    		}
    		List<ReflinkStrekning> g = service.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinks));
    		
			List<ProdReflinkStrekning> str = mergeReflinkStrekningAndReflinkseksjon(prodList, g);
			str.addAll(strUtenReflinks);
			plist=str;
    	}else{
    		strekninger = getReflinkStrekningFromProdreflinkseksjon(wrappedList);
    		log.debug("henter strekninger.. + tid");
    		for (int i=0;i<wrappedList.size();i++){
    			strekningIds+=wrappedList.get(i).getStrekningId().intValue();
    			if (i<wrappedList.size()-1){
    				strekningIds+=",";
    			}
    		}
    		String q= "select id, to_char(fra_tidspunkt,'dd.MM.yyyy hh24:mi:ss'), to_char(til_tidspunkt,'dd.MM.yyyy hh24:mi:ss'), digi, fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, hp from prodstrekning where id in("+strekningIds+")";
    		query = em.createNativeQuery(q);
    		List<Prodstrekning> pl = new NativeQueryWrapper<Prodstrekning>(query, Prodstrekning.class,  new Class[]{Long.class, Date.class, Date.class, byte[].class, Long.class, Long.class, String.class, String.class, Long.class, Long.class}, "id", "fraTidspunkt", "tilTidspunkt", "digiByte", "fylkesnummer", "kommunenummer", "veikategori", "veistatus", "veinummer", "hp").getWrappedList();
    		pl = filterListProdstrekning(params, pl);
    		plist = new ArrayList<ProdReflinkStrekning>();
    		for (Prodstrekning p:pl){
    			ProdReflinkStrekning prodReflinkStrekning = new ProdReflinkStrekning();
        		prodReflinkStrekning.setProdstrekning(p);
        		
        		List<Prodreflinkseksjon> convertedList = convertToAndFromOnProdreflinksjoner(wrappedList);
        		
        		for (Prodreflinkseksjon pr:convertedList){
        			if (pr.getStrekningId().intValue()==p.getId().intValue()){
        				boolean found = false;
        	    		for (ReflinkStrekning strekning: strekninger){
        		    		if (pr.getReflinkIdent()==strekning.getReflinkId()&& 
        		    			pr.getFra()==strekning.getFra() &&
        		    			pr.getTil()==strekning.getTil()){
	        		    			prodReflinkStrekning.addReflinkStrekning(strekning);
	        		    			log.debug("Added strekning to reflinkstrekning..");
	        		    			found = true;
        		    		}
        	    		}
        	    		if (!found){
        	    			log.error("No match found for:"+pr.getReflinkIdent()+" "+pr.getFra()+"-"+pr.getTil());
        	    		}
        			}
        		}
        		plist.add(prodReflinkStrekning);
    		}
    		log.debug("done.. ");
    	}
    	return plist;
    }
    

    /**
     * Metode som snur fra og til verdiene i prodreflinksjoner, slik at tegning av disse strekningene kan gjøres korrekt.
     * Dette blir gjort isteden for å lagre de snudde verdiene på selve prodreflinkseksjonen for å unngå eventuelle problemer i basen. 
     * @param wrappedList
     * @return
     */
    private List<Prodreflinkseksjon> convertToAndFromOnProdreflinksjoner(List<Prodreflinkseksjon> wrappedList) {

    	List<Prodreflinkseksjon> convertedList = new ArrayList<Prodreflinkseksjon>();
    	
    	for (Prodreflinkseksjon originalPs : wrappedList) {
			
    		Prodreflinkseksjon ps = new Prodreflinkseksjon();
    		
    		ps.setProdstrekning(originalPs.getProdstrekning());
    		ps.setReflinkIdent(originalPs.getReflinkIdent());
    		ps.setStrekningId(originalPs.getStrekningId());
    		
    		if (originalPs.getTil() < originalPs.getFra()) {
				ps.setFra(originalPs.getTil());
				ps.setTil(originalPs.getFra());
			} else{
				ps.setFra(originalPs.getFra());
				ps.setTil(originalPs.getTil());
			}
    		
    		convertedList.add(ps);
		}
    	
    	return convertedList;
	}

	/** {@inheritDoc} */
    @SuppressWarnings("rawtypes")
	public List<ProdpunktVO> hentInspeksjonPunkter(String inspeksjonGuid, boolean kunSiste){
    	String q = kunSiste?NativeQueryHolder.getSisteInspeksjonPunktQuery(inspeksjonGuid):NativeQueryHolder.getInspeksjonPunktQuery(inspeksjonGuid);
    	Query query = em.createNativeQuery(q);
    	Class[] c = new Class[]{String.class, byte[].class, Double.class, byte[].class, Date.class, Long.class, Double.class, Double.class, String.class};
    	String[] f = new String[]{"inspeksjonId", "digiByte", "retning", "ignorerMaskByte", "gmtTidspunkt", "dfuId", "snappetX", "snappetY", "guid"};
    	List<ProdpunktVO> wrappedList = new NativeQueryWrapper<ProdpunktVO>(query, ProdpunktVO.class, c, f).getWrappedList();
    	return wrappedList;
    }
    /** {@inheritDoc} */
    @SuppressWarnings("rawtypes")
	public List<PunktregVO> hentAvvikFraInspeksjon(String inspeksjonGuid){
    	//TODO endre spørring til å ta med sak
    	log.debug("hentFunnFraInspeksjon("+inspeksjonGuid+")");
    	Query query = em.createNativeQuery(NativeQueryHolder.getAvvikFraInspeksjonQuery());
    	query.setParameter(1, inspeksjonGuid);
    	Class[] c = new Class[]{String.class, Double.class, Double.class, Double.class, Double.class};
    	String[] f  = new String[]{"guid", "x", "y", "snappetX", "snappetY"};
    	List<PunktregVO> list = new NativeQueryWrapper<PunktregVO>(query, PunktregVO.class, c, f).getWrappedList();
    	for (PunktregVO v:list){
    		v.setType(PunktregVO.TYPE.FUNN);
    	}
    	return list;
    }
    
    /** {@inheritDoc} */
    @SuppressWarnings("rawtypes")
	public List<PunktregVO> hentHendelseFraInspeksjon(String inspeksjonGuid){
    	//TODO endre spørring til å ta med sak
    	log.debug("hentHendelseFraInspeksjon("+inspeksjonGuid+")");
    	Query hendelser = em.createNativeQuery(NativeQueryHolder.getHendelseFraInspeksjonQuery());
    	hendelser.setParameter(1, inspeksjonGuid);
    	Class[] c = new Class[]{String.class, Double.class, Double.class, Double.class, Double.class};
    	String[] f  = new String[]{"guid", "x", "y", "snappetX", "snappetY"};
    	List<PunktregVO> list = new NativeQueryWrapper<PunktregVO>(hendelser, PunktregVO.class, c, f).getWrappedList();
    	for (PunktregVO v:list){
    		v.setType(PunktregVO.TYPE.HENDELSE);
    	}
    	return list;
    }
    /** {@inheritDoc} */
    @SuppressWarnings("rawtypes")
	public List<PunktregVO> hentSkredFraInspeksjon(String inspeksjonGuid){
    	Query skred = em.createNativeQuery(NativeQueryHolder.getSkredFraInspeksjonQuery());
    	skred.setParameter(1, inspeksjonGuid);
    	Class[] c = new Class[]{String.class, Double.class, Double.class, Double.class, Double.class};
    	String[] f  = new String[]{"guid", "x", "y", "snappetX", "snappetY"};
    	List<PunktregVO> list = new NativeQueryWrapper<PunktregVO>(skred, PunktregVO.class, c, f).getWrappedList();
    	for (PunktregVO v:list){
    		v.setType(PunktregVO.TYPE.SKRED);
    	}
    	return list;

    }
    /** {@inheritDoc} */
    @SuppressWarnings("rawtypes")
	public List<PunktregVO> hentForsikringsskadeFraInspeksjon(String inspeksjonGuid){
    	Query forsikringsskader = em.createNativeQuery(NativeQueryHolder.getForsikringsskadeFraInspeksjonQuery());
    	forsikringsskader.setParameter(1, inspeksjonGuid);
    	Class[] c = new Class[]{String.class, Double.class, Double.class, Double.class, Double.class};
    	String[] f  = new String[]{"guid", "x", "y", "snappetX", "snappetY"};
    	List<PunktregVO> list = new NativeQueryWrapper<PunktregVO>(forsikringsskader, PunktregVO.class, c, f).getWrappedList();
    	for (PunktregVO v:list){
    		v.setType(PunktregVO.TYPE.FORSIKRINGSSKADE);
    	}
    	return list;
    }

    	
    
    /** {@inheritDoc} */
    public List<Hendelse> hentHendelser(HendelseQueryFilter filter){
    	String guids = "";
    	for (HendelseSokView v:field.sokHendelser(filter)){
    		guids+="'"+v.getGuid()+"', ";
    	}
    	if (guids.equals(""))
    		return null;
    	String query = "SELECT h from Hendelse h where h.guid in("+guids.substring(0, guids.length()-2)+") order by h.ownedMipssEntity.opprettetDato";
    	log.debug("Executing query:"+query);
    	List<Hendelse> hendelser = em.createQuery(query).getResultList();
    	log.debug("Fant "+hendelser.size()+" hendelser");
    
    	return hendelser;
    }
    
    /** {@inheritDoc} */
    public List<Friksjonsdetalj> getFriksjonsdetalj(Long driftkontraktId, Date fraDatoCet, Date tilDatoCet){
    	Date fraDatoGmt = DateFormatter.convertFromCETtoGMT(fraDatoCet);
		Date tilDatoGmt = DateFormatter.convertFromCETtoGMT(tilDatoCet);
		
		Long stroBroytRodetypeId = Long.valueOf(konfig.hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
    	Query q = em.createNativeQuery(NativeQueryHolder.getFriksjonsdetaljQuery());
    	q.setParameter(1, driftkontraktId);
    	q.setParameter(2, stroBroytRodetypeId);
    	q.setParameter(3, fraDatoGmt);
    	q.setParameter(4, tilDatoGmt);
    	
    	List<Friksjonsdetalj> resultList = 
    		new NativeQueryWrapper<Friksjonsdetalj>(q, Friksjonsdetalj.class, 
    				new Class[]{String.class, Long.class, String.class, Date.class, Long.class,
    				Double.class, String.class, Long.class, Date.class,
    				Long.class, Long.class, Double.class, Long.class,
    				String.class, Long.class, String.class, Date.class, String.class,
    				String.class, String.class, Double.class, String.class,
    				Date.class, String.class, Long.class, String.class}, 
    				"beskrivelse", "dfuId", "endretAv", "endretDato", "foreId", 
    				"friksjon", "fullfortTiltak", "fylkesnummer", "gmtTidspunkt",
    				"hp", "id", "km", "kommunenummer",
    				"maalertype", "nedborId", "opprettetAv", "opprettetDato", "regnr",
    				"retning", "startTiltak", "temperatur", "utfortAv",
    				"utfortDatoTid", "veikategori", "veinummer", "veistatus")
    				.getWrappedList();
    	 
    	
    	Map<Long, Nedbor> nedborMap = new HashMap<Long, Nedbor>();
    	for(Nedbor nedbor : (List<Nedbor>)em.createNamedQuery(Nedbor.QUERY_FIND_ALL).getResultList()) {
    		nedborMap.put(nedbor.getId(), nedbor);
    	}
    	
    	Map<Long, Fore> foreMap = new HashMap<Long, Fore>();
    	for(Fore fore : (List<Fore>)em.createNamedQuery(Fore.QUERY_FIND_ALL).getResultList()) {
    		foreMap.put(fore.getId(), fore);
    	}
    	
    	for (Friksjonsdetalj f:resultList){
    		if (getProdpunkt(f.getDfuId(), f.getGmtTidspunkt())==null){
    			f.setManuell(true);
    		}
    		f.setRegnr(getRegnrFromDfu(f.getDfuId(), f.getGmtTidspunkt(), f.getGmtTidspunkt()));
    		
    		if(f.getNedborId() != null) {
    			f.setNedbor(nedborMap.get(f.getNedborId()));
    		}
    		
    		if(f.getForeId() != null) {
    			f.setFore(foreMap.get(f.getForeId()));
    		}
    	}
    	
    	
    	return resultList;
    }
    /** {@inheritDoc} */
    public Map<String, List<Prodstrekning>> getKontinuerligFriksjonsdetalj(FriksjonQueryParams params){
    	String vk = params.getVeikategori();
    	String vs = params.getVeistatus();
    	Long vn = params.getVeinummer();
    	Long hp = params.getHp();
    	if (params.getDfuIdList()==null||params.getDfuIdList().isEmpty()){
    		List<KjoretoyDfuVO> dfuMedFriksjon = kjoretoy.getDfuMedFriksjon(params.getDriftkontraktId(), params.getFraDatoCet(), params.getTilDatoCet());
    		List<Long> dfuIds = new ArrayList<Long>();
    		for (KjoretoyDfuVO d:dfuMedFriksjon){
    			dfuIds.add(d.getDfuId());
    		}
    		params.setDfuIdList(dfuIds);
    	}
    	Long stroBroytRodetypeId = Long.valueOf(konfig.hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
    	
    	String dfuIds = StringUtils.join(params.getDfuIdList(), ',');
    	Query q = em.createNativeQuery(NativeQueryHolder.getProduksjonStrekningerUtenReflinkQuery(vk, vs, vn, hp, dfuIds));
    	
    	
	    
    	Date fra = DateFormatter.round(params.getFraDatoCet(), true);
    	Date til = DateFormatter.round(params.getTilDatoCet(), false);
    	q.setParameter(1, dfuIds);
    	q.setParameter(2, params.getDriftkontraktId());
    	q.setParameter(3, stroBroytRodetypeId);
	    q.setParameter(4, MipssDateFormatter.formatDate(fra, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
	    q.setParameter(5, MipssDateFormatter.formatDate(til, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
	    q.setParameter(6, serverUtils.hentMaskeKontFriksjon());
	    int pn = 7;
    	if (vk!=null){
    		q.setParameter(pn, vk);
    		pn++;
    	}
    	if (vs!=null){
    		q.setParameter(pn, vs);
    		pn++;
    	}
    	if (vn!=null){
    		q.setParameter(pn, vn);
    		pn++;
    	}
    	if (hp!=null){
    		q.setParameter(pn, hp);
    		pn++;
    	}
    	
	    NativeQueryWrapper<Prodstrekning> w = new NativeQueryWrapper<Prodstrekning>(q, Prodstrekning.class, new Class[]{
	    	Long.class, Long.class, byte[].class, Date.class, Date.class, 
	    	Double.class, Double.class, 
	    	Long.class, Long.class, Long.class, String.class, Long.class, String.class, 
	    	int.class, double.class, double.class  
	    }, "id", "dfuId", "digiByte", "fraTidspunkt", "tilTidspunkt", 
	       "fraKm", "tilKm", 
	       "fylkesnummer", "hp", "kommunenummer", "veikategori", "veinummer", "veistatus");
	    
	    FriksjonHelper h = new FriksjonHelper();
	    Map<Long, List<Prodstrekning>> strPrDfu = h.lagStrekningPrDfuList(w.getWrappedList());
	    Map<String, List<Prodstrekning>> strPrKjoretoy = new HashMap<String, List<Prodstrekning>>();
	    //lager et map med eksternt navn og prodstrekninger
	    for (Long dfuId: strPrDfu.keySet()){
	    	String eksterntNavn = getEksterntnavnFromDfu(dfuId, fra, til);
	    	if (StringUtils.isBlank(eksterntNavn)){
	    		eksterntNavn = "dfuId:"+dfuId.toString();
	    	}
	    	List<Prodstrekning> ps = strPrDfu.get(dfuId);
	    	//sl�r sammen strekninger p� samme hp, pr kj�retoy
	    	ps = h.joinStrekninger(ps);
	    	try{
		    	ps = fjernStrekningerUtenFriksjonsmaling(ps);
		    }catch (Exception e){
		    	//nothing..
		    	e.printStackTrace();
		    }
	    	strPrKjoretoy.put(eksterntNavn, ps);
	    }
	    
	    return strPrKjoretoy;
    }
    
	private List<Prodstrekning> fjernStrekningerUtenFriksjonsmaling(List<Prodstrekning> ps){
    	List<Prodstrekning> cleaned = new ArrayList<Prodstrekning>();
    	for (Prodstrekning p:ps){
    		Date fraTidGMT = MipssDateFormatter.convertFromCETtoGMT(p.getFraTidspunkt());
        	Date tilTidGMT = MipssDateFormatter.convertFromCETtoGMT(p.getTilTidspunkt());
        	String fra = MipssDateFormatter.formatDate(fraTidGMT, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
        	String til = MipssDateFormatter.formatDate(tilTidGMT, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
	    	String query = NativeQueryHolder.getSjekkHastighetFriksjonQuery();
	    	Query q = em.createNativeQuery(query);
	    	q.setParameter(1, p.getDfuId());
	    	q.setParameter(2, fra);
	    	q.setParameter(3, til);
	    	q.setParameter(4, p.getVeinummer());
	    	q.setParameter(5, p.getHp());
	    	Object[] v = (Object[])q.getSingleResult();
	    	BigDecimal antall = (BigDecimal) v[0];
	    	BigDecimal hastighet = (BigDecimal)v[1];
	    	if (antall!=null && antall.intValue()>1 && hastighet!=null && hastighet.intValue()>0)
	    		cleaned.add(p);
    	}
    	
    	return cleaned;
    }

    
    
    
    
    /** {@inheritDoc} */
    public List<FriksjonVO> hentFriksjonsdataForProdstrekning(Prodstrekning ps){
    	Query q = em.createNativeQuery(NativeQueryHolder.getProdpunktdetaljForDfuAndTid());
    	Long dfu = ps.getDfuId();
    	Date fraTidGMT = MipssDateFormatter.convertFromCETtoGMT(ps.getFraTidspunkt());
    	Date tilTidGMT = MipssDateFormatter.convertFromCETtoGMT(ps.getTilTidspunkt());
    	String fra = MipssDateFormatter.formatDate(fraTidGMT, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	String til = MipssDateFormatter.formatDate(tilTidGMT, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	q.setParameter(1, dfu);
    	q.setParameter(2, fra);
    	q.setParameter(3, til);
    	q.setParameter(4, ps.getVeinummer());
    	q.setParameter(5, ps.getHp());
    	
    	NativeQueryWrapper<FriksjonVO> w = new NativeQueryWrapper<FriksjonVO>(q, FriksjonVO.class,
    			"dfuId", "date", "nmea", "veikategori", "veistatus", "veinummer", "hp", "meter", "snappetX", "snappetY", "hastighet", "retning");
    	List<FriksjonVO> list = w.getWrappedList();
    	Map<Long, String> dfuEksterntNavn= new HashMap<Long, String>();
    	
    	List<FriksjonVO> cleanList = new ArrayList<FriksjonVO>();
    	for (FriksjonVO f:list){
    		String eksterntNavn = dfuEksterntNavn.get(f.getDfuId());
    		if (eksterntNavn==null){
    			eksterntNavn = getEksterntnavnFromDfu(f.getDfuId(), f.getDate(), f.getDate());
    			dfuEksterntNavn.put(f.getDfuId(), eksterntNavn);
    		}
    		f.setEksterntNavn(eksterntNavn);
    		f.setDate(MipssDateFormatter.convertFromGMTtoCET(f.getDate()));
    		String nmea = f.getNmea();
    		f.setFriksjon(getFriksjonFromNMEA(nmea));
    		if (f.getFriksjon()!=null)
    			cleanList.add(f);
    	}
    	
    	return cleanList;
    }
    
    
    
    
  
    /**
     * Henter friksjonsinformasjon fra NMEA-strengen
     * @param nmea
     * @return
     */
    private Double getFriksjonFromNMEA(String nmea){
    	String[] split = nmea.split(",");
    	Double friksjon = null;
    	if (split.length>6){
    		String f = split[6];
    		try{
    			friksjon = Double.parseDouble(f);
    		}catch (NumberFormatException e){
    			friksjon = null;
    		}
    	}
    	return friksjon;
    	
    }
    @SuppressWarnings("rawtypes")
	public List<Integer> getFylkenummerFromDriftkontrakt(Long driftkontraktId){
    	StringBuilder sb = new StringBuilder();
    	sb.append("select distinct fylkesnummer ");
    	sb.append("from veinettveireferanse vr ");
    	sb.append("join veinettreflinkseksjon vs ");
    	sb.append("on vr.reflinkseksjon_id = vs.id ");
    	sb.append("join veinett v ");
    	sb.append("on vs.veinett_id = v.id ");
    	sb.append("where v.type_navn = 'Kontrakt' ");
    	sb.append("and v.id = (select veinett_id ");
    	sb.append("from driftkontrakt ");
    	sb.append("where id = ?1)");
    	
    	Query q = em.createNativeQuery(sb.toString());
    	q.setParameter(1, driftkontraktId);
    	List resultList = q.getResultList();
    	List<Integer> fylkenummerList = new ArrayList<Integer>();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			BigDecimal fylkenummer = (BigDecimal)o;
    			fylkenummerList.add(fylkenummer.intValue());
    		}
    	}
    	return fylkenummerList;
    }
    @SuppressWarnings("rawtypes")
	public List<Integer> getKommunenummerFromDriftkontrakt(Long driftkontraktId){
    	StringBuilder sb = new StringBuilder();
    	sb.append("select distinct kommunenummer ");
    	sb.append("from veinettveireferanse vr ");
    	sb.append("join veinettreflinkseksjon vs ");
    	sb.append("on vr.reflinkseksjon_id = vs.id ");
    	sb.append("join veinett v ");
    	sb.append("on vs.veinett_id = v.id ");
    	sb.append("where v.type_navn = 'Kontrakt' ");
    	sb.append("and v.id = (select veinett_id ");
    	sb.append("from driftkontrakt ");
    	sb.append("where id = ?1)");
    	
    	Query q = em.createNativeQuery(sb.toString());
    	q.setParameter(1, driftkontraktId);
    	List resultList = q.getResultList();
    	List<Integer> kommunenummerList = new ArrayList<Integer>();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			BigDecimal fylkenummer = (BigDecimal)o;
    			kommunenummerList .add(fylkenummer.intValue());
       		}
    	}
    	return kommunenummerList ;
    }
    /** {@inheritDoc} */
    public void updateFriksjonsdetaljer(List<Friksjonsdetalj> flist){
    	for (Friksjonsdetalj f:flist){
    		f = em.merge(f);
    	}
    }
    /** {@inheritDoc} */
    public void deleteFriksjonsdetaljer(List<Friksjonsdetalj> flist){
    	for (Friksjonsdetalj f:flist){
    		if (f.isManuell()) {
				Friksjonsdetalj merged = em.merge(f);
				em.remove(merged);
			}
    	}
    }
    
    @SuppressWarnings("rawtypes")
	private String getRegnrFromDfu(Long dfuId, Date fraDato, Date tilDato){
    	String fra = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	String til = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	String qry = NativeQueryHolder.getRegnrDfu();
    	Query q = em.createNativeQuery(qry);
    	
    	q.setParameter(1, dfuId);
    	q.setParameter(2, fra);
    	q.setParameter(3, til);
    	List resultList = q.getResultList();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			String regnr = (String)o;
    			return regnr;
    		}
    	}
    	return "";
    }
    
    @SuppressWarnings("rawtypes")
	public String getEksterntnavnFromDfu(Long dfuId, Date fraDato, Date tilDato){
    	String fra = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	String til = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
    	String qry = NativeQueryHolder.getEksterntNavnFromDfu();
    	Query q = em.createNativeQuery(qry);
    	
    	q.setParameter(1, dfuId);
    	q.setParameter(2, fra);
    	q.setParameter(3, til);
    	List resultList = q.getResultList();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			String regnr = (String)o;
    			return regnr;
    		}
    	}
    	return "";
    }
    
    public String getKosenavnForPda(Long pdaDfuId, Long kontraktId){
    	String qry = NativeQueryHolder.getKosenavnForPda();
    	Query q = em.createNativeQuery(qry);
    	q.setParameter(1, pdaDfuId);
    	q.setParameter(2, kontraktId);
    	String kosenavn = (String)q.getSingleResult();
    	return kosenavn;
    }

   
    /**
     * Konverterer Prodreflinkseksjon som benyttes mot databasen til Reflinkseksjon som benyttes av webservice
     * @param list
     * @return
     */
    private List<ReflinkSeksjon> convertProdreflinkseksjonToReflinkSeksjon(List<Prodreflinkseksjon> list){
        List<ReflinkSeksjon> reflinkseksjoner = new ArrayList<ReflinkSeksjon>();
        
        for (Prodreflinkseksjon p:list){
            ReflinkSeksjon ref = new ReflinkSeksjon();
//            double fraTemp = p.getFra();
//            double tilTemp = p.getTil();
            
            if (p.getTil()<p.getFra()){
            	ref.setFra(p.getTil());
                ref.setTil(p.getFra());
//                p.setTil(p.getFra()); //Kommentert ut for å unngå at objektet persisteres med snudde verdier
//                p.setFra(tilTemp);	// --------- || ----------
            }else{
	            ref.setFra(p.getFra());
	            ref.setTil(p.getTil());
            }
            ref.setReflinkId(p.getReflinkIdent());
            
            reflinkseksjoner.add(ref);
        }
        return reflinkseksjoner;
    }
    
    /**
     * Holder for nativequeries. Disse spørringene er lagt til som en del 
     * av ytelsesoptimaliseringen for uthenting av data fra store tabeller. 
     * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
     *
     */
    private static class NativeQueryHolder{
    	
    	public static String getSingleProdpunktInfoQuery(){
    		StringBuilder qq = new StringBuilder();
        	qq.append("select pi.fylkesnummer, pi.kommunenummer, pi.veikategori, pi.veistatus, pi.veinummer, pi.hp, pi.meter, to_char(pi.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), pi.dfu_id ");
        	qq.append("from prodpunktinfo pi where pi.dfu_id=?1 ");
        	qq.append("and CAST(pi.gmt_tidspunkt as date)=to_date(?2, 'dd.mm.yyyy hh24:mi:ss')");
//        	qq.append("and pi.gmt_tidspunkt=to_date(?2, 'dd.mm.yyyy hh24:mi:ss')");
        	return qq.toString();
    	}
    	
    	/**
    	 * Spørring som henter ut alle prodstrekningene til en dfu innenfor et tidsrom. Tar også med reflinkseksjoner
    	 * @return
    	 */
    	public static String getProduksjonStrekningerQuery(){
    		StringBuilder q = new StringBuilder();
			q.append("select p.id, p.dfu_id, p.digi, to_char(p.fra_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), to_char(p.til_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), "); 
			q.append("p.fra_km, p.til_km, ");
			q.append("p.fylkesnummer, p.hp, p.kommunenummer, p.veikategori, p.veinummer, p.veistatus, ");
			q.append("pr.reflink_ident, pr.fra, pr.til "); 
			q.append("from prodstrekning p left join "); 
			q.append("prodreflinkseksjon pr on pr.strekning_id=p.id "); 
			q.append("where p.dfu_Id=?1 "); 
			q.append("and p.fra_tidspunkt >= trunc(cast(to_date(?2, 'dd.mm.yyyy hh24:mi:ss') as date)) "); 
			q.append("and p.fra_tidspunkt <= trunc(cast(to_date(?3, 'dd.mm.yyyy hh24:mi:ss') as date))+1 - (1/86400) "); 
			q.append("and p.fra_tidspunkt <= to_date(?3, 'dd.mm.yyyy hh24:mi:ss') "); 
			q.append("and p.til_tidspunkt >= to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
			q.append("order by p.fra_tidspunkt ");
			return q.toString();
    	}
    	
    	/**
    	 * Sp�rring som henter ut alle prodstrekningene til en dfu innenfor et tidsrom, p� angitt kontrakt og rodetype og kun prodstrekninger
    	 * som har digi som angir at det er en friksjonsm�ling. 
    	 * 
    	 * valgfritt � begrense ned til hp (veikategori, veistatus, veinummer, hp)
    	 * 
    	 * Tar IKKE med reflinkseksjoner. 
    	 * <p>
    	 * <ul>
    	 * <li>?1: dfu_id</li>
    	 * <li>?2: kontrakt_id</li>
    	 * <li>?3: rodetype_id</li>
    	 * <li>?4: fra_tidspunkt</li>
    	 * <li>?5: til_tidspunkt</li>
    	 * <li>?6: digi_byte for prodtype
    	 * <li>?7: veikategori</li>
    	 * <li>?8: veistatus</li>
    	 * <li>?9: veinummer</li>
    	 * <li>?10: hp</li>
    	 * </ul>
    	 * 
    	 * : nummrene tildeles etter hvilket parameter i rekkef�lgen den har av de som ikke er null.
    	 * </p> 
    	 * @return
    	 */
    	public static String getProduksjonStrekningerUtenReflinkQuery(String veikategori, String veistatus, Long veinummer, Long hp, String dfuIds){
    		StringBuilder q = new StringBuilder();
			q.append("select p.id, p.dfu_id, p.digi, ");
			q.append("to_char(p.fra_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
			q.append("to_char(p.til_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), "); 
			q.append("p.fra_km, p.til_km, ");
			q.append("p.fylkesnummer, p.hp, p.kommunenummer, p.veikategori, p.veinummer, p.veistatus ");
			q.append("from prodstrekning p "); 
			q.append("where p.dfu_Id in("+dfuIds+") "); 
			q.append("and p.id in(select strekning_id from rodestrekning r where r.rode_id in ");
			q.append("    (select id from rode rr WHERE p.fra_tidspunkt BETWEEN rr.gyldig_fra_dato AND rr.gyldig_til_dato and rr.kontrakt_id=?2 and rr.type_id=?3)) ");
			q.append("and p.fra_tidspunkt >= trunc(to_date(?4, 'dd.mm.yyyy hh24:mi:ss')) "); 
			q.append("and p.fra_tidspunkt <= trunc(to_date(?5, 'dd.mm.yyyy hh24:mi:ss'))+1 - (1/86400) "); 
			q.append("and p.fra_tidspunkt >= to_date(?4, 'dd.mm.yyyy hh24:mi:ss') "); 
			q.append("and p.til_tidspunkt <= to_date(?5, 'dd.mm.yyyy hh24:mi:ss') ");
			q.append("and utl_raw.bit_and(p.digi, ?6)=?6 ");
			int pn  = 7;
			if (veikategori!=null){
				q.append("and p.veikategori=?"+pn+" ");
				pn++;
			}
			if (veistatus!=null){
				q.append("and p.veistatus=?"+pn+" ");
				pn++;
			}
			if (veinummer!=null){
				q.append("and p.veinummer=?"+pn+" ");
				pn++;
			}
			if (hp!=null){
				q.append("and p.hp=?"+pn+" ");
				pn++;
			}
//			q.append("and digi > '04FF' ");
			q.append("order by p.dfu_id, p.fra_tidspunkt ");
			return q.toString();
    	}
    	
    	/**
    	 * Spørring som henter strekninger basert på rode, rodetype og tidsrom
    	 * @return
    	 */
    	public static String getProduksjonStrekningerKontraktQuery(){
    		StringBuilder q = new StringBuilder();    		
    		q.append("select pt.navn, p.id, p.dfu_id, p.digi, to_char(p.fra_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
    		q.append("to_char(p.til_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), p.fra_km, p.til_km, ");
    		q.append("p.fylkesnummer, p.hp, p.kommunenummer, p.veikategori, p.veinummer, p.veistatus,  "); 
    		q.append("pr.reflink_ident, pr.fra, pr.til  ");
    		
    		q.append("from rodestrekning r   ");
    		q.append("join prodstrekning p on r.strekning_id=p.id  ");  
    		q.append("join tiltak tt on tt.strekning_id= r.strekning_id  ");
    		q.append("join prodtype pt on pt.id=tt.prodtype_id  ");
    		q.append("left join prodreflinkseksjon pr on pr.strekning_id=p.id  "); 
        
    		q.append("where r.rode_id in (select id from rode where kontrakt_id=?1 and type_id=?2)  ");  
    		q.append("and r.fra_dato_tid between to_date(?3, 'dd.mm.yyyy hh24:mi:ss') and to_date(?4, 'dd.mm.yyyy hh24:mi:ss')  "); 
    		q.append("order by pt.navn, p.dfu_id, p.fra_tidspunkt  ");
    		return q.toString();
    	}
    	public static String getProduksjonStrekningerKontraktMedMaterialeQuery(String prodtypeIds){
    		StringBuilder q = new StringBuilder();    		
    		q.append("select pt.navn, p.id, p.dfu_id, p.digi, ");
    		q.append("to_char(p.fra_tidspunkt, 'dd.mm.yyyy hh24:mi:ss') fra_tidspunkt, "); 
    		q.append("to_char(p.til_tidspunkt, 'dd.mm.yyyy hh24:mi:ss') til_tidspunkt, ");
    		q.append("p.fra_km, p.til_km, p.fylkesnummer, p.hp, ");
    		q.append("p.kommunenummer, p.veikategori, p.veinummer, ");
    		q.append("p.veistatus,  pr.reflink_ident, pr.fra, pr.til, ");
    		q.append("mm.rapporteringsenhet||' '||mm.navn materiale_enhet, sm.stromengde  ");
    		q.append("from rodestrekning r  ");
    		q.append("join prodstrekning p on r.strekning_id=p.id ");  
    		q.append("join tiltak tt on tt.strekning_id= r.strekning_id ");  
    		q.append("join prodtype pt on pt.id=tt.prodtype_id  ");
    		q.append("left join prodreflinkseksjon pr on pr.strekning_id=p.id ");  
    		q.append("left join stromengde sm on sm.tiltak_id=tt.id ");
    		q.append("left join materiale mm on mm.id=sm.materiale_id ");
    		q.append("where r.rode_id ");
    		q.append("in (select id from rode where kontrakt_id=?1 and type_id=?2) ");  
    		q.append("and r.fra_dato_tid between to_date(?3, 'dd.mm.yyyy hh24:mi:ss') and to_date(?4, 'dd.mm.yyyy hh24:mi:ss') ");  
    		q.append("and pt.id in(");
    		q.append(prodtypeIds);
    		q.append(") ");
    		q.append("order by pt.navn, p.dfu_id, p.fra_tidspunkt ");
    		return q.toString();
    	}
    	
    	/**
    	 * Spørring som henter ut alle prodpunktene og pdopverdier for en dfu innenfor et tidsrom
    	 * @return
    	 */
    	public static String getPdopPunkterQuery(){
    		StringBuilder qq = new StringBuilder();
        	qq.append("select p.digi, p.lengdegrad, p.breddegrad, pi.snappet_x, pi.snappet_y, p.retning, p.ignorer_maske, ");
        	qq.append("to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), p.dfu_id, p.pdop, p.hastighet ");
        	qq.append("from prodpunkt p left join prodpunktinfo pi on p.gmt_tidspunkt=pi.gmt_tidspunkt and p.dfu_id=pi.dfu_id ");
        	qq.append("where p.dfu_id=?1 and ");
        	qq.append("p.gmt_tidspunkt between ");
        	qq.append("to_date(?2, 'dd.mm.yyyy hh24:mi:ss') and ");
        	qq.append("to_date(?3, 'dd.mm.yyyy hh24:mi:ss') order by p.gmt_tidspunkt");
        	return qq.toString();
    	}
    	/**
    	 * Spørring som henter ut alle prodpunktene til en dfu innenfor et tidsrom
    	 * @return
    	 */
    	public static String getProdpunktInfoQuery(){
    		StringBuilder qq = new StringBuilder();
    		qq.append("select p.digi, p.retning, p.ignorer_maske, to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
    		qq.append("p.dfu_id, p.breddegrad, p.lengdegrad, pi.snappet_x, pi.snappet_y, p.hastighet ");
    		qq.append("from prodpunkt p left join prodpunktinfo pi on p.gmt_tidspunkt=pi.gmt_tidspunkt and p.dfu_id=pi.dfu_id ");
    		qq.append("where p.dfu_id=?1 and ");
    		qq.append("p.gmt_tidspunkt between ");
    		qq.append("to_date(?2, 'dd.mm.yyyy hh24:mi:ss') and ");
    		qq.append("to_date(?3, 'dd.mm.yyyy hh24:mi:ss') order by p.gmt_tidspunkt");
    		return qq.toString();
    	}
    	/**
    	 * Spørring som henter ut alle prodpunktene til en dfu innenfor et tidsrom
    	 * @return
    	 */
    	public static String getProdpunktInfoSingleQuery(){
    		StringBuilder qq = new StringBuilder();
    		qq.append("select p.digi, p.retning, p.ignorer_maske, to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
    		qq.append("p.dfu_id, p.breddegrad, p.lengdegrad, pi.snappet_x, pi.snappet_y, p.hastighet ");
    		qq.append("from prodpunkt p left join prodpunktinfo pi on p.gmt_tidspunkt=pi.gmt_tidspunkt and p.dfu_id=pi.dfu_id ");
    		qq.append("where p.dfu_id=?1 and ");
    		qq.append("p.gmt_tidspunkt = to_date(?2, 'dd.mm.yyyy hh24:mi:ss') ");
    		return qq.toString();
    	}
    	/**
    	 * Spørring som henter ut siste prodpunkt for en dfu innefor et tidsrom
    	 * @return
    	 */
    	public static String getSisteProdpunktQuery(){
    		StringBuilder sb = new StringBuilder();
	    	sb.append("select p.digi, p.retning, p.ignorer_maske, to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
	    	sb.append("p.dfu_id, p.lengdegrad, p.breddegrad, pi.snappet_x, pi.snappet_y "); 
	    	sb.append("from prodpunkt p left join prodpunktinfo pi on p.dfu_id=pi.dfu_id and p.gmt_tidspunkt=pi.gmt_tidspunkt ");
	    	sb.append("where p.dfu_id=?1 ");
	    	sb.append("and p.gmt_tidspunkt=( ");
	    	sb.append("SELECT max(pp.gmt_tidspunkt) from prodpunkt pp ");
	    	sb.append("where pp.dfu_id=?1 ");
	    	sb.append("AND pp.breddegrad is not null ");
	    	sb.append("AND pp.gmt_Tidspunkt<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') ");
	    	sb.append("AND pp.gmt_Tidspunkt>=to_date(?3, 'dd.mm.yyyy hh24:mi:ss')) ");
	    	return sb.toString();
    	}
    	/**
    	 * Spørring som henter en dfu_id for et kjøretøy innenfor et tidsrom
    	 * @return
    	 */
    	//TODO her er det noe GALT!!!
    	public static String getDfuIdFromKjoretoyQuery(){
    		StringBuilder sb = new StringBuilder();
        	sb.append("select dfu_id from Installasjon i "); 
        	sb.append("where i.aktiv_fra_dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
        	sb.append("and i.aktiv_Til_Dato>=to_date(?3, 'dd.mm.yyyy hh24:mi:ss') ");
        	sb.append("and i.kjoretoy_id=?1 "); 
        	sb.append("or ");
        	sb.append("i.aktiv_Fra_Dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') ");
        	sb.append("and i.aktiv_Til_Dato is null "); 
        	sb.append("and i.kjoretoy_id=?1 ");
        	return sb.toString();
    	}
    	public static String getDfuIdFromKjoretoyQuery2(){
    		StringBuilder sb = new StringBuilder();
        	sb.append("select dfu_id from Installasjon i "); 
        	sb.append("where i.aktiv_Fra_Dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') ");
        	sb.append("and i.aktiv_Til_Dato is null "); 
        	sb.append("and i.kjoretoy_id=?1 ");
        	return sb.toString();
    	}
    	/**
    	 * Spørring som kaller på en tabellfunksjon for å hente ut produksjonstypen for en digi på et gitt kjøretøy
    	 * @return
    	 */
    	public static String getProdtypeFromDigiQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select prodtype_id, utstyrnavn, bit_nr from table(feltstudio_pk.prod_liste_f(?1, ?2, to_date(?3, 'dd.MM.yy hh24:mi:ss')))");
    		return sb.toString();
    	}
    	
    	/**
    	 * Spørring som henter ut alle strekningene som er generert for en inspeksjon_guid
    	 * @return
    	 */
    	public static String getInspeksjonStrekningerQuery(){
        	StringBuilder sb = new StringBuilder();
        	sb.append("select strekning_id, reflink_ident, fra, til ");
        	sb.append("from prodreflinkseksjon where strekning_id in(select id from prodstrekning where inspeksjon_Guid=?1)");
        	return sb.toString();
    	}
    	/**
    	 * Spørring som henter ut siste punktet for en inspeksjon_guid 
    	 * @param inspeksjonGuid
    	 * @return
    	 */
    	public static String getSisteInspeksjonPunktQuery(String inspeksjonGuid){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select i.refnummer, digi, retning, ignorer_maske, to_char(pp.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
    		sb.append("p.dfu_id, p.snappet_x, p.snappet_y, pp.inspeksjon_guid ");
    		sb.append("from prodpunkt pp ");
    		sb.append("join prodpunktinfo p on p.dfu_id= pp.dfu_id and p.gmt_tidspunkt=pp.gmt_tidspunkt and ");
    		sb.append("pp.inspeksjon_guid='"+inspeksjonGuid+"' ");
    		sb.append("join inspeksjon_xv i on i.guid = pp.inspeksjon_guid ");
    		sb.append("and pp.gmt_tidspunkt= (select max(pp2.gmt_tidspunkt) from prodpunkt p2 ");
    		sb.append("						join prodpunktinfo pp2 on pp2.dfu_id=p2.dfu_id and ");
    		sb.append("						pp2.gmt_tidspunkt=p2.gmt_tidspunkt where p2.inspeksjon_guid='"+inspeksjonGuid+"') ");
    		return sb.toString();
    	}
    	/**
    	 * Spørring som henter ut alle inspeksjonpunktene for en inspeksjon_guid 
    	 * @param inspeksjonGuid
    	 * @return
    	 */
    	public static String getInspeksjonPunktQuery(String inspeksjonGuid){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select i.refnummer, digi, retning, ignorer_maske, to_char(pp.gmt_tidspunkt,'dd.mm.yyyy hh24:mi:ss'), p.dfu_id, ");
    		sb.append("p.snappet_x, p.snappet_y, pp.inspeksjon_guid ");
    		sb.append("from prodpunkt pp ");
    		sb.append("join prodpunktinfo p on p.dfu_id= pp.dfu_id and p.gmt_tidspunkt=pp.gmt_tidspunkt ");
    		sb.append("join inspeksjon_xv i on i.guid = pp.inspeksjon_guid ");
    		sb.append("and pp.inspeksjon_guid='"+inspeksjonGuid+"' order by pp.gmt_tidspunkt");
    		return sb.toString();
    	}
    	/**
    	 * Spørring som henter funn som er registrert på en inspeksjon
    	 * @param inspeksjonGuid
    	 * @return
    	 */
    	public static String getAvvikFraInspeksjonQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select a.guid, ps.x, ps.y, ps.snappet_x, ps.snappet_y from avvik_xv a ");
    		sb.append("join sak_xv s on s.inspeksjon_guid=? ");
    		sb.append("join punktstedfest_xv ps on a.guid=ps.avvik_guid ");
    		sb.append("where a.sak_guid = s.guid");
    		return sb.toString();
    	}
    	
    	/**
    	 * Spørring som henter hendelser som er registrert på en inspeksjon_guid
    	 * @param inspeksjonGuid
    	 * @return
    	 */
    	public static String getHendelseFraInspeksjonQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select h.guid, ps.x, ps.y, ps.snappet_x, ps.snappet_y from hendelse_xv h ");
    		sb.append("join sak_xv s on s.inspeksjon_guid=? ");
    		sb.append("join punktstedfest_xv ps on ps.sak_guid=s.guid ");
    		sb.append("where h.sak_guid = s.guid ");
    		return sb.toString();
    	}
    	/**
    	 * Spørring som henter forsikringsskade som er registrert på en inspeksjon_guid
    	 * @param inspeksjonGuid
    	 * @return
    	 */
    	public static String getForsikringsskadeFraInspeksjonQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select h.guid, ps.x, ps.y, ps.snappet_x, ps.snappet_y from forsikringsskade_xv h ");
    		sb.append("join sak_xv s on s.inspeksjon_guid=? ");
    		sb.append("join punktstedfest_xv ps on ps.sak_guid=s.guid ");
    		sb.append("where h.sak_guid = s.guid ");
    		return sb.toString();
    	}
    	/**
    	 * Spørring som henter forsikringsskade som er registrert på en inspeksjon_guid
    	 * @param inspeksjonGuid
    	 * @return
    	 */
    	public static String getSkredFraInspeksjonQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select h.guid, ps.x, ps.y, ps.snappet_x, ps.snappet_y from skred_xv h ");
    		sb.append("join sak_xv s on s.inspeksjon_guid=? ");
    		sb.append("join punktstedfest_xv ps on ps.sak_guid=s.guid ");
    		sb.append("where h.sak_guid = s.guid ");
    		return sb.toString();
    	}
    	/**
    	 * Spørring som henter ut prodpunkt basert på dfuinfo. Dfuinfo søkes opp innenfor et tidsrom og dfu_id
    	 * @return
    	 */
    	public static String getFinnSisteDfuInfo(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select p.digi, p.retning, p.ignorer_maske, to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), ");
    		sb.append("p.dfu_id, p.lengdegrad, p.breddegrad, p.hastighet ");
    		sb.append("from prodpunkt p "); 
    		sb.append("join dfuinfo d on d.dfu_id= p.dfu_id and p.gmt_tidspunkt= d.siste_pos_gmt_tidspunkt ");
    		sb.append("where d.siste_pos_gmt_tidspunkt between to_date(?1, 'dd.mm.yyyy hh24:mi:ss') and to_date(?2, 'dd.mm.yyyy hh24:mi:ss') ");
    		sb.append("and d.dfu_id=?3");
    		return sb.toString();
    	}
    	

    	public static String getFinnSisteDfuInfoDate(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select to_char(siste_pos_gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss') from dfuinfo where dfu_id=?1");
    		return sb.toString();
    	}
    	
    	/**
    	 * Henter regnr for en gitt dfu innenfor et tidsrom
    	 * 1 : dfuId
    	 * 2 : fraDato
    	 * 3 : tilDato
    	 * @return query
    	 */
    	public static String getRegnrDfu(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select i.regnr from installasjon_v i ");
    		sb.append("left join kjoretoy k on k.id = i.kjoretoy_id "); 
    		sb.append("left join leverandor l on k.leverandor_nr = l.nr ");
    		sb.append("where dfu_id = ?1 ");
    		sb.append("and i.aktiv_fra_dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
    		sb.append("and i.aktiv_Til_Dato>=to_date(?3, 'dd.mm.yyyy hh24:mi:ss') ");
    		sb.append("or ");
    		sb.append("dfu_id = ?1 ");
    		sb.append("and i.aktiv_fra_dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
    		sb.append("and i.aktiv_Til_Dato is null");
    		return sb.toString();
    	}
    	
    	/**
    	 * Henter regnr for en gitt dfu innenfor et tidsrom
    	 * 1 : dfuId
    	 * 2 : fraDato
    	 * 3 : tilDato
    	 * @return query
    	 */
    	public static String getEksterntNavnFromDfu(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select i.kortnavn from installasjon_v i ");
    		sb.append("left join kjoretoy k on k.id = i.kjoretoy_id "); 
    		sb.append("left join leverandor l on k.leverandor_nr = l.nr ");
    		sb.append("where dfu_id = ?1 ");
    		sb.append("and i.aktiv_fra_dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
    		sb.append("and i.aktiv_Til_Dato>=to_date(?3, 'dd.mm.yyyy hh24:mi:ss') ");
    		sb.append("or ");
    		sb.append("dfu_id = ?1 ");
    		sb.append("and i.aktiv_fra_dato<=to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
    		sb.append("and i.aktiv_Til_Dato is null");
    		return sb.toString();
    	}
    	
    	/**
    	 * Henter regnr for en gitt dfu innenfor et tidsrom
    	 * 1 : dfuId
    	 * 2 : fraDato
    	 * 3 : tilDato
    	 * @return query
    	 */
    	public static String getKosenavnForPda(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select dk.navn from dfu_kontrakt dk ");
    		sb.append("where dk.dfu_id=?1 ");
    		sb.append("and dk.kontrakt_id=?2 ");
    		return sb.toString();
    	}
    	
    	/**
    	 * Henter installasjoner for en gitt dfu i en tidsperiode
    	 * 
    	 * 1. fra_dato
    	 * 2. til_dato
    	 * 3. kjoretoyId
    	 * @return
    	 */
    	public static String getInstallasjonFromKjoretoyAndTid(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select dfu_id, to_char(aktiv_fra_dato, 'dd.mm.yyyy hh24:mi:ss'), to_char(aktiv_til_dato, 'dd.mm.yyyy hh24:mi:ss') ");
    		sb.append("from Installasjon i ");
    		sb.append("where i.aktiv_Til_Dato>=to_date(?1, 'dd.mm.yyyy hh24:mi') ");
    		sb.append("and i.aktiv_Fra_Dato<=to_date(?2, 'dd.mm.yyyy hh24:mi') ");
    		sb.append("and i.kjoretoy_id=?3 ");
    		sb.append("or i.aktiv_Til_Dato is null ");
    		sb.append("and i.aktiv_Fra_Dato <=to_date(?2, 'dd.mm.yyyy hh24:mi') ");
    		sb.append("and i.kjoretoy_id=?3");
    		return sb.toString();
    	}
    	/**
    	 * Henter prodpunktdetalj for en gitt dfu i en tidsperiode
    	 * ?1: dfu_id
    	 * ?2: fra_dato
    	 * ?3: til_dato
    	 * @return
    	 */
    	public static String getProdpunktdetaljForDfuAndTid(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select p.dfu_id, to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), p.innstreng, ");
    		sb.append("pi.veikategori, pi.veistatus, pi.veinummer, pi.hp, pi.meter, pi.snappet_x, pi.snappet_y, pp.hastighet, pp.retning ");
    		sb.append("from prodpunktdetalj p ");
    		sb.append("join prodpunktinfo pi on pi.dfu_id = p.dfu_id and pi.gmt_tidspunkt= p.gmt_tidspunkt ");
    		sb.append("join prodpunkt pp on pp.dfu_id=p.dfu_id and pp.gmt_tidspunkt = p.gmt_tidspunkt ");
    		sb.append("where p.dfu_id=?1 ");
	    	sb.append("and p.gmt_tidspunkt between to_date(?2, 'dd.mm.yyyy hh24:mi:ss') ");
	    	sb.append("and to_date(?3, 'dd.mm.yyyy hh24:mi:ss') ");
	    	sb.append("and pi.veinummer=?4 ");
	    	sb.append("and pi.hp=?5 ");
	    	sb.append("and pi.meter is not null ");
	    	sb.append("order by p.gmt_tidspunkt");
    		return sb.toString();
    	}
       	/**
    	 * <p>
    	 * Henter friksjonsdetaljer for gitt kontrakt innen for en tidsperiode.
    	 * 
    	 * Kun friksjonsm�liner som er gjennomf�rt p� kontraktens str�/br�yteroder blir med i resultatet. 
    	 *  
    	 * <ul>
    	 * <li>?1: driftkontrakt_id</li>
    	 * <li>?2: rodetype_id for str� br�yteroder </li>
    	 * <li>?3: fra_tidspunkt (GMT)</li>
    	 * <li>?4: til_tidspunkt (GMT)</li>
    	 * 
    	 * </ul>
    	 * @return
    	 */
    	public static String getFriksjonsdetaljQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append(" SELECT beskrivelse, dfu_id, endret_av, ");
    		sb.append("  to_char(endret_dato, 'DD.MM.YYYY HH24:MI:SS') endret_dato, ");
    		sb.append("  fore_id, friksjon, fullfort_tiltak, fylkesnummer, ");
    		sb.append("  to_char(gmt_tidspunkt, 'DD.MM.YYYY HH24:MI:SS') gmt_tidspunkt, ");
    		sb.append("  hp, id, km, kommunenummer, maalertype, nedbor_id, ");
    		sb.append("  opprettet_av, ");
    		sb.append("  to_char(opprettet_dato, 'DD.MM.YYYY HH24:MI:SS') opprettet_dato, ");
    		sb.append("  regnr, retning, start_tiltak, temperatur, utfort_av, ");
    		sb.append("  to_char(utfort_dato_tid, 'DD.MM.YYYY HH24:MI:SS') utfort_dato_tid, ");
    		sb.append("  veikategori, veinummer, veistatus ");
    		sb.append("   FROM friksjonsdetalj ");
    		sb.append("  WHERE (fylkesnummer, kommunenummer, veikategori, veistatus, veinummer, hp) IN ");
    		sb.append("  (SELECT fylkesnummer,kommunenummer, veikategori, veistatus, veinummer, hp ");
    		sb.append("     FROM veinettveireferanse ");
    		sb.append("    WHERE reflinkseksjon_id IN ");
    		sb.append("    (SELECT id ");
    		sb.append("       FROM veinettreflinkseksjon ");
    		sb.append("      WHERE veinett_id in ");
    		sb.append("      (select veinett_id from rode rr "); 
    		sb.append("    		  WHERE friksjonsdetalj.gmt_tidspunkt BETWEEN rr.gyldig_fra_dato AND rr.gyldig_til_dato "); 
    		sb.append("    		  and rr.kontrakt_id=?1 and rr.type_id=?2) ");
    		sb.append("    ) ");
    		sb.append("  AND friksjonsdetalj.km BETWEEN veinettveireferanse.fra_km AND veinettveireferanse.til_km ");
    		sb.append("  ) ");
    		sb.append(" AND gmt_tidspunkt BETWEEN ?3 AND ?4 ");
    		return sb.toString();
    	}
    	
    	/**
    	 * <p>
    	 * Henter makshastighet for en strekning med friksjonsm�ling
    	 * 
    	 * <ul>
    	 * <li>?1: dfu_id</li>
    	 * <li>?2: fra_tidspunkt (GMT)</li>
    	 * <li>?3: til_tidspunkt (GMT)</li>
    	 * <li>?4: veinummer </li>
    	 * <li>?5: hp </li>
    	 * 
    	 * 
    	 * </ul>
    	 * @return
    	 */
    	public static String getSjekkHastighetFriksjonQuery(){
    		StringBuilder sb = new StringBuilder();
    		sb.append("select count(*), max(hastighet) ");
    		sb.append("  from prodpunktdetalj p "); 
    		sb.append("  join prodpunktinfo pi on pi.dfu_id = p.dfu_id "); 
    		sb.append("    and pi.gmt_tidspunkt= p.gmt_tidspunkt  ");
    		sb.append("  join prodpunkt pp on pp.dfu_id=p.dfu_id  ");
    		sb.append("    and pp.gmt_tidspunkt = p.gmt_tidspunkt  ");
    		sb.append("  where p.dfu_id=?1  ");
    		sb.append("    and p.gmt_tidspunkt between to_date(?2, 'dd.mm.yyyy hh24:mi:ss') "); 
    		sb.append("                            and to_date(?3, 'dd.mm.yyyy hh24:mi:ss')  ");
    		sb.append("    and pi.veinummer=?4  ");
    		sb.append("    and pi.hp=?5  ");
    		sb.append("    and pi.meter is not null "); 
    		sb.append("  order by p.gmt_tidspunkt");
    		return sb.toString();
    	}
    }
}
