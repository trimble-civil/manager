package no.mesta.mipss.service.produksjon.friksjon;

import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;

@SuppressWarnings("serial")
public class FriksjonVO implements IRenderableMipssEntity{
	
	private Date date;
	private Double friksjon;
	private String usikker;
	private String aktivitet;
	private Long dfuId;
	private String nmea;
	private String veikategori;
	private String veistatus;
	private Long veinummer;
	private Long hp;
	private Long meter;
	private Double snappetX;
	private Double snappetY;
	private Double hastighet;
	private String eksterntNavn;
    private Double retning;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getFriksjon() {
		return friksjon;
	}
	public void setFriksjon(Double friksjon) {
		this.friksjon = friksjon;
	}
	public String getUsikker() {
		return usikker;
	}
	public void setUsikker(String usikker) {
		this.usikker = usikker;
	}
	public String getAktivitet() {
		return aktivitet;
	}
	public void setAktivitet(String aktivitet) {
		this.aktivitet = aktivitet;
	}
	
	public void setNmea(String nmea) {
		this.nmea = nmea;
	}
	public String getNmea() {
		return nmea;
	}
	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}
	public Long getDfuId() {
		return dfuId;
	}
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getVeikategori() {
		return veikategori;
	}
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}
	public String getVeistatus() {
		return veistatus;
	}
	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}
	public Long getVeinummer() {
		return veinummer;
	}
	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}
	public Long getHp() {
		return hp;
	}
	public void setHp(Long hp) {
		this.hp = hp;
	}
	public Long getMeter() {
		return meter;
	}
	public void setMeter(Long meter) {
		this.meter = meter;
	}
	public void setSnappetX(Double snappetX){
		this.snappetX = snappetX;
		
	}
	public Double getSnappetX() {
		return snappetX;
	}
	public void setSnappetY(Double snappetY){
		this.snappetY = snappetY;
		
	}
	public Double getSnappetY() {
		
		return snappetY;
	}
	public void setHastighet(Double hastighet) {
		this.hastighet = hastighet;
	}
	public Double getHastighet() {
		return hastighet;
	}
	public void setEksterntNavn(String eksterntNavn) {
		this.eksterntNavn = eksterntNavn;
	}
	public String getEksterntNavn() {
		return eksterntNavn;
	}
	public void setRetning(Double retning) {
		this.retning = retning;
	}
	public Double getRetning() {
		return retning;
	}
	
}
