package no.mesta.mipss.service.produksjon.friksjon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;

public class FriksjonHelper {

    public List<Prodstrekning> joinStrekninger(List<Prodstrekning> pl){
    	List<Prodstrekning> joined = new ArrayList<Prodstrekning>();
    	Prodstrekning prev = null;
    	for (Prodstrekning p:pl){
    		if (prev!=null){
    			//samme strekning
    			if (isEqualProdstrekning(prev, p)){
    				prev.setTilTidspunkt(p.getTilTidspunkt());
    				prev.setTilKm(p.getTilKm());
    			}else{
    				joined.add(prev);
    				prev = p;
    			}
    		}else{
    			prev = p;
    		}
    	}
    	if (prev!=null)
    		joined.add(prev);
    	return joined;
    }
    
    private boolean isEqualProdstrekning(Prodstrekning p1, Prodstrekning p2){
    	return new EqualsBuilder()
    		.append(p1.getVeinummer(), p2.getVeinummer())
    		.append(p1.getHp(), p2.getHp())
    		.isEquals() && p1.getTilTidspunkt().equals(p2.getFraTidspunkt()) &&isEqualKmRetning(p1, p2);
    }
    private boolean isEqualKmRetning(Prodstrekning p1, Prodstrekning p2){
    	boolean stigendeP1 = p1.getFraKm()<p1.getTilKm();
    	boolean stigendeP2 = p2.getFraKm()<p2.getTilKm();
    	return stigendeP1==stigendeP2;
    }

    public Map<Long, List<Prodstrekning>> lagStrekningPrDfuList(List<Prodstrekning> ps){
    	Map<Long, List<Prodstrekning>> strPrKjoretoy = new HashMap<Long, List<Prodstrekning>>();
	    for (Prodstrekning p:ps){
	    	List<Prodstrekning> psList = strPrKjoretoy.get(p.getDfuId());
	    	if (psList==null){
	    		psList = new ArrayList<Prodstrekning>();
	    		strPrKjoretoy.put(p.getDfuId(), psList);
	    	}
	    	psList.add(p);
	    }
	    return strPrKjoretoy;
    }
}
