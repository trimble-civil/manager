package no.mesta.mipss.service.produksjon;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import no.mesta.mipss.mipssfield.HendelseQueryFilter;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.persistence.kvernetf1.Prodpunktinfo;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;
import no.mesta.mipss.webservice.ReflinkStrekning;
/**
 * Remote interface for tjenesten for å hente ut produksjon gjort av kjøretøy eller pda
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@Remote
public interface ProduksjonService {
	public static String BEAN_NAME = "ProduksjonService";

	/**
	 * Henter et prodpunkt basert på data i dfuinfo innenfor et tidsrom og dfu_id. dfu_id kan være null hvis 
	 * kjoretoyId ikke er null, dfu_id vil da bli hentet fra installasjonen. 
	 * 
	 * returnerer null dersom det forrige punktet har samme dato som det siste punktet 
	 * eller datoen ikke kunne parses
	 * eller dersom kjøretøyet ikke har en installasjon
	 * 
	 * @param dfuId
	 * @param kjoretoyId
	 * @param tidsvinduStart
	 * @param tidsvinduStopp
	 * @return
	 */
	List<ProdpunktVO> getProdpunktFromDfuinfo(Long dfuId, Long kjoretoyId, Date tidsvinduStart, Date tidsvinduStopp, Date forrigePunktGmt, boolean tidsvindu);
	
    /**
     * Henter snappede data for en dfu
     * @param dfuindividId id til dfu enheten som har produsert punktene
     * @param datoFra f�rste dato i s�ket
     * @param datoTil sluttdato i s�ket
     * @return
     */
    List<Prodpunktinfo> getProdpunktInfo(Long dfuindividId, Date datoFra, Date datoTil);
    
    /**
     * Henter ut et prodpunktinfor med dfu_id og dato
     * @param dfuId
     * @param date
     * @return
     */
    Prodpunktinfo getProdpunktInfo(Long dfuId, Date date);
    /**
     * Henter en liste over produksjonstyper 
     * @return
     */
    List<Prodtype> getProdtypeList();
    
    /**
     * Henter produserte strekninger for et kjøretøy innenfor et gitt tidsrom
     * @param kjoretoyId id til kjoretoy
     * @param fraTidspunkt f�rste tidspunkt for s�king etter produksjon
     * @param tilTidspunkt andre tidspunkt for s�king etter produksjon
     * @param prodtyper de prodtypene som skal være med i utvalget, dersom denne er null vil alle prodtyper bli med
     * @param allProduksjon true hvis man vil ha med all produksjon og ikke bare de som ligger i prodtyper, produksjon som ikke ligger i prodtyper vil få en egen farge
     * @return
     */
    List<ProdReflinkStrekning> getProduksjonStrekninger(ProduksjonQueryParams params);//Long kjoretoyId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon);
    
    /**
     * Henter produserte strekninger for en gitt rodetype på en kontrakt. 
     * 
     * Strekgningene vil returneres med 1 pr. prodtype. Dvs hvis det har vært strøing og brøyting
     * på f.eks RV23HP1 m0-m1000 vil man få denne strekningen to ganger, en pr prodtype.
     *  
     * @param params aktuelle: fraDato, tilDato, kontraktId, rodetypeId
     * @return
     */
    List<ProdReflinkStrekning> getProduksjonStrekningerKontrakt(ProduksjonQueryParams params);

    /**
     * Henter produserte strekninger pakket sammen for SAM oslo..
     * @param params
     * @return
     */
    List<ProdReflinkStrekning> getProduksjonStrekningerSamShape(ProduksjonQueryParams params);
    
    
    /**
     * Henter en liste med prodpunkter og baker sammen med prodtyper innenfor et gitt tidspunkt. 
     * For å finne ut prodtypen må vi for hver installasjon innenfor tidsrommet hente ut en liste 
     * med prodtyper fra kartapp_pk.prod_liste_f.  Tidspunktene skal være oppgitt i CET, denne metoden 
     * transformerer til GMT for å hente ut punktene i @see{Prodpunkt}. 
     * @param kjoretoyId
     * @param fraTidspunkt
     * @param tilTidspunkt
     * @param prodtyper (denne kan være null hvis man ønsker alle prodtyper)
     * @param allProdtyper
     * @return
     */
    List<ProdpunktVO> getProduksjonPunktKjoretoy(ProduksjonQueryParams params);//Long kjoretoyId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon);
    
    /**
     * Henter siste prodpunkt og baker sammen med prodtype innenfor et gitt tidsrom. 
     * For å finne ut prodtypen må vi for hver installasjon innenfor tidsrommet hente ut en liste 
     * med prodtyper fra kartapp_pk.prod_liste_f. 
     * @param kjoretoyId
     * @param fraTidspunkt
     * @param tilTidspunkt
     * @param prodtyper (denne kan være null hvis man ønsker alle prodtyper)
     * @param allProdtyper
     * @return
     */
    ProdpunktVO getSisteProduksjonPunktKjoretoy(ProduksjonQueryParams params);//Long kjoretoyId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon);
    
    /**
     * Henter produserte strekninger for en dfu innenfor et gitt tidsrom
     * @param dfuId id til dfu
     * @param fraTidspunkt f�rste tidspunkt for s�king etter produksjon
     * @param tilTidspunkt andre tidspunkt for s�king etter produksjon
     * @param prodtyper de prodtypene som skal være med i utvalget, dersom denne er null vil alle prodtyper bli med
     * @param allProduksjon true hvis man vil ha med all produksjon og ikke bare de som ligger i prodtyper, produksjon som ikke ligger i prodtyper vil få en egen farge
     * @return
     */
    List<ProdReflinkStrekning> getProduksjonStrekningerDfu(ProduksjonQueryParams params);//Long dfuId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon);
    
    /**
     * Henter en liste med prodpunkter og baker sammen med prodtyper innenfor et gitt tidspunkt hvis det finnes prodtype. 
     * 
     * Tidspunktene skal være oppgitt i CET, denne metoden transformerer til GMT for å hente ut punktene i @see{Prodpunkt}. 
     * @param dfuId
     * @param fraTidspunkt
     * @param tilTidspunkt
     * @param prodtyper (denne kan være null hvis man ønsker alle prodtyper)
     * @param allProdtyper
     * @return
     */
    List<ProdpunktVO> getProduksjonPunktDfu(ProduksjonQueryParams params);//Long dfuId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon);
    
    /**
     * Henter et prodpunkt for angitt dfu og tidspunkt
     * @param dfuId
     * @param datoGmt
     * @return
     */
    ProdpunktVO getProdpunkt(Long dfuId, Date datoGmt);
    
    /**
     * Henter siste prodpunkt og baker sammen med prodtype innenfor et gitt tidsrom hvis det finnes prodtype.
     * 
     * @param kjoretoyId
     * @param fraTidspunkt
     * @param tilTidspunkt
     * @param prodtyper (denne kan være null hvis man ønsker alle prodtyper)
     * @param allProdtyper
     * @return
     */
    ProdpunktVO getSisteProduksjonPunktDfu(ProduksjonQueryParams params);//Long dfuId, Date fraTidspunkt, Date tilTidspunkt, List<Prodtype> prodtyper, boolean allProduksjon);
    /**
     * Henter ut punkter med pdopverdier for en gitt dfu
     * @param params
     * @return
     */
    List<ProdpunktVO> getPdopPunkterDFU(ProduksjonQueryParams params);
    /**
     * Henter ut punkter med pdopverdier for et kjøretøy
     * @param params
     * @return
     */
    List<ProdpunktVO> getPdopPunkterKjoretoy(ProduksjonQueryParams params);
    
    /**
     * Henter en liste med avvik som er registrert sammen med en inspeksjon
     * @param inspeksjonGuid
     * @return
     */
    List<PunktregVO> hentAvvikFraInspeksjon(String inspeksjonGuid);
    /**
     * Henter en liste med hendelser som er registrert sammen med en inspeksjon
     * @param inspeksjonGuid
     * @return
     */
    
    List<PunktregVO> hentHendelseFraInspeksjon(String inspeksjonGuid);
   
    /**
     * Henter en liste med skred som er registrert sammen med en inspeksjon
     * @param inspeksjonGuid
     * @return
     */
    List<PunktregVO> hentSkredFraInspeksjon(String inspeksjonGuid);
    /**
     * Henter en liste med forsikringsskader som er registrert sammen med en inspeksjon
     * @param inspeksjonGuid
     * @param funn true dersom det også skal hentes funn som er knyttet til hendelsene.
     * @return
     */
    List<PunktregVO> hentForsikringsskadeFraInspeksjon(String inspeksjonGuid);
    
    /**
     * Henter strekningene til en inspeksjon med guid
     * @param guid
     * @return
     */
    List<ProdReflinkStrekning> hentInspeksjonStrekninger(String guid, InspeksjonQueryParams params);
       
    /**
     * Henter prodpunktene til inspeksjonen med gitt guid
     * @param inspeksjonGuid
     * @param kunSiste
     * @return
     */
    List<ProdpunktVO> hentInspeksjonPunkter(String inspeksjonGuid, boolean kunSiste);
    
    /**
     * Henter hendelser basert på søkekriteriene i filteret. Bruker MipssField.sokHendelser for å 
     * finn aktuelle guid til hendelsene.
     * @param filter
     * @return
     */
    List<Hendelse> hentHendelser(HendelseQueryFilter filter);
    
    /**
     * Henter en kjøretøy entitet
     * @param kjoretoyId
     * @return
     */
    Kjoretoy getKjoretoyWithId(Long kjoretoyId);
    
    /**
     * Henter navnet til levarandøren til dfu innenfor angitt tidsrom
     * @param dfuId
     * @param fraDatoCet
     * @param tilDatoCet
     * @return
     */
    String getLeverandorNavnDfu(Long dfuId, Date fraDatoCet, Date tilDatoCet);
    /**
     * Henter en dfuindivid entitet
     * @param dfuId
     * @return
     */
    Dfuindivid getDfuWithId(Long dfuId);
    
    /**
     * Henter geometri for en liste med prodstrekninger. Dersom en prodstrekning ikke har reflinkseksjoner
     * vil veireferansen til prodstrekningen bli benyttet til å lage geometri
     * @param list
     * @return
     */
    List<ReflinkStrekning> getReflinkStrekningFromProdstrekning(List<Prodstrekning> list, boolean merge);
    
    /**
     * Henter ut friksjonsdetaljer
     * @param driftkontraktId
     * @param fraDato
     * @param tilDato
     * @return
     */
    List<Friksjonsdetalj> getFriksjonsdetalj(Long driftkontraktId, Date fraDatoCet, Date tilDatoCet);
    
    /**
     * Henter friksjonsdetaljer for kontinuerlige målinger
     * @param params
     * @return
     */
    Map<String,List<Prodstrekning>> getKontinuerligFriksjonsdetalj(FriksjonQueryParams params);
    
    /**
     * Henter friksjonsdata som tilhører angitt prodstrekning
     * 
     * @param ps
     * @return
     */
    List<FriksjonVO> hentFriksjonsdataForProdstrekning(Prodstrekning ps);
    /**
     * Henter alle fylkesnummere som ligger på kontraktens veinett
     * @param driftkontraktId
     * @return
     */
    List<Integer> getFylkenummerFromDriftkontrakt(Long driftkontraktId);
    /**
     * Henter alle kommunenummere som ligger på kontraktens veinett
     * @param driftkontraktId
     * @return
     */
    List<Integer> getKommunenummerFromDriftkontrakt(Long driftkontraktId);
    /**
     * Lagrer en liste med friksjonsdetaljer. 
     * for hver Friksjonsdetalj vil det forsøkes å finne en DFU_ID basert på regnr og gmtTidspunkt, dersom det ikke
     * eksisterer en dfu_id for friksjonsdetaljen vil det kastes en exception
     * @param flist
     */
    void updateFriksjonsdetaljer(List<Friksjonsdetalj> flist);
    
    /**
     * Henter eksternt navn for en dfu innenfor angitt tidsrom
     * @param dfuId
     * @param fraDato
     * @param tilDato
     * @return
     */
    String getEksterntnavnFromDfu(Long dfuId, Date fraDato, Date tilDato);
    
    /**
     * Henter "kosenavnet" til pda. 
     * @param pdaDfuId
     * @param kontraktId
     * @return
     */
    String getKosenavnForPda(Long pdaDfuId, Long kontraktId);
    /**
     * Sletter en liste med friksjonsdetaljer. 
     * @param flist
     */
    void deleteFriksjonsdetaljer(List<Friksjonsdetalj> flist);
    
    Prodtype getSingleProdType(Long prodTypeId);

}
