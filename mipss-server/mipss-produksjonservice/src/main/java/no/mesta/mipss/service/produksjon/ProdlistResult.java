package no.mesta.mipss.service.produksjon;

import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;

public class ProdlistResult {
	private Prodtype prodtype;
	private Long prodtypeId;
	private String utstyrsnavn;
	private Integer bitNr;
	/**
	 * @return the prodtype
	 */
	public Prodtype getProdtype() {
		return prodtype;
	}
	/**
	 * @param prodtype the prodtype to set
	 */
	public void setProdtype(Prodtype prodtype) {
		this.prodtype = prodtype;
	}
	/**
	 * @return the prodtypeId
	 */
	public Long getProdtypeId() {
		return prodtypeId;
	}
	/**
	 * @param prodtypeId the prodtypeId to set
	 */
	public void setProdtypeId(Long prodtypeId) {
		this.prodtypeId = prodtypeId;
	}
	/**
	 * @return the utstyrsnavn
	 */
	public String getUtstyrsnavn() {
		return utstyrsnavn;
	}
	/**
	 * @param utstyrsnavn the utstyrsnavn to set
	 */
	public void setUtstyrsnavn(String utstyrsnavn) {
		this.utstyrsnavn = utstyrsnavn;
	}
	/**
	 * @return the bitNr
	 */
	public Integer getBitNr() {
		return bitNr;
	}
	/**
	 * @param bitNr the bitNr to set
	 */
	public void setBitNr(Integer bitNr) {
		this.bitNr = bitNr;
	}
	
}
