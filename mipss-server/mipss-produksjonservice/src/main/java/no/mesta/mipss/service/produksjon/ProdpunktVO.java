package no.mesta.mipss.service.produksjon;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.util.DateFormatter;
/**
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kul�</a>
 *
 */
@SuppressWarnings("serial")
public class ProdpunktVO implements Serializable{

	private List<ProdtypeVO> prodtype;
	private String guid;
	private String inspeksjonId;
	private String digi;
	private String ignorerMask;
	private Date gmtTidspunkt;
	private Date cetTidspunkt;
	private Long dfuId;
	private Double snappetX;
	private Double snappetY;
	private Double gpsX;
	private Double gpsY;
	private Double pdop;
	private Double retning;
	private Double hastighet;
	private String gpsxx;
	private String gpsyy;
	
	private boolean calendarConverted;
	public ProdpunktVO(){
		
	}
	/**
	 * @return the prodtype
	 */
	public List<ProdtypeVO> getProdtypeList() {
		return prodtype;
	}
	public void setDigiByte(byte[] digi){
    	String s = "";
    	for (Byte b:digi){
    		s+=pad(Integer.toHexString(0xFF&b));
    	}
    	setDigi(s);
    }
	
	private String pad(String hx){
		int pads = 2-hx.length();
		String padded = "";
		for (int i=0;i<pads;i++){
			padded+="0";
		}
		padded+=hx;
		return padded;
		
	}
	public String getInspeksjonId() {
		return inspeksjonId;
	}
	public void setInspeksjonId(String inspeksjonId) {
		this.inspeksjonId = inspeksjonId;
	}
	/**
	 * @param prodtype the prodtype to set
	 */
	public void setProdtypeList(List<ProdtypeVO> prodtype) {
		this.prodtype = prodtype;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return guid;
	}

	public String getDigi() {
		return digi;
	}

	public void setDigi(String digi) {
		this.digi = digi;
	}
	public String getDigiAndMask(){
		if (getIgnorerMask()!=null){
    		String im = getIgnorerMask();
    		String di = getDigi();
    		
    		Integer digi = Integer.parseInt(di, 16);
    		Integer mask = Integer.parseInt(im, 16);
    		
    		int digiAndMask = digi&mask;
    		String dm = Integer.toHexString(digiAndMask);
    		while (dm.length()<4)
    			dm = 0+dm;
    		return dm;
    	}
    	return getDigi();
	}
	public String getIgnorerMask() {
		return ignorerMask;
	}

	public void setIgnorerMaskByte(byte[] ignorerMask){
		String s = "";
    	for (Byte b:ignorerMask){
    		s+=pad(Integer.toHexString(0xFF&b));
    	}
    	setIgnorerMask(s);
	}
	public void setIgnorerMask(String ignorerMask) {
		this.ignorerMask = ignorerMask;
	}

	public Date getGmtTidspunkt() {
		return gmtTidspunkt;
	}

	public void setGmtTidspunkt(Date gmtTidspunkt) {
		this.gmtTidspunkt = gmtTidspunkt;
		if (!calendarConverted){
			calendarConverted=true;
			setCetTidspunkt(DateFormatter.convertFromGMTtoCET(gmtTidspunkt));
		}
	}

	public Date getCetTidspunkt() {
		return cetTidspunkt;
	}

	public void setCetTidspunkt(Date cetTidspunkt) {
		this.cetTidspunkt = cetTidspunkt;
		if (!calendarConverted){
			calendarConverted = true;
			setGmtTidspunkt(DateFormatter.convertFromCETtoGMT(cetTidspunkt));
		}
	}

	public Long getDfuId() {
		return dfuId;
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}

	public Double getSnappetX() {
		return snappetX;
	}

	public void setSnappetX(Double snappetX) {
		this.snappetX = snappetX;
	}

	public Double getSnappetY() {
		return snappetY;
	}

	public void setSnappetY(Double snappetY) {
		this.snappetY = snappetY;
	}

	public Double getRetning() {
		return retning;
	}

	public void setRetning(Double retning) {
		this.retning = retning;
	}

	public void setHastighet(Double hastighet) {
		this.hastighet = hastighet;
	}

	public Double getHastighet() {
		return hastighet;
	}

	public void setPdop(Double pdop) {
		this.pdop = pdop;
	}

	public Double getPdop() {
		return pdop;
	}

	public void setGpsX(Double gpsX) {
		this.gpsX = gpsX;
	}

	public Double getGpsX() {
		return gpsX;
	}

	public void setGpsY(Double gpsY) {
		this.gpsY = gpsY;
	}

	public Double getGpsY() {
		return gpsY;
	}
	
	public void setGPSXX(String x){
		if (x.startsWith("N")){//noen samples i databasen inneholder NaN.. 
			return;
		}
		if (x.indexOf('W')!=-1){
			gpsxx=x;
		}else{
			setGpsX(Double.parseDouble(x.replace("E", "").replace(',','.')));
		}
	}
	public String getGPSXX(){
		return gpsxx;
	}
	public void setGPSYY(String y){
		if (y.startsWith("N")){ //noen samples i databasen inneholder NaN.. 
			return;
		}
		setGpsY(Double.parseDouble(y.replace("N", "").replace(',','.')));
	}
}
