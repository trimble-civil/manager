package no.mesta.mipss.service.produksjon;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
/**
 * Spørringsparametere for å hente ut produksjon
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class ProduksjonQueryParams implements Serializable{
	private Long kjoretoyId; 
	private Long dfuId; 
	private Long kontraktId;
	private Long rodetypeId;
	
	private Date fraDato; 
	private Date tilDato; 
	private List<Prodtype> prodtyper; 
	private boolean allProduksjon;
	private boolean kunSiste;
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	public Long getDfuId() {
		return dfuId;
	}
	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}
	public Date getFraDato() {
		return fraDato;
	}
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	public Date getTilDato() {
		return tilDato;
	}
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	public List<Prodtype> getProdtyper() {
		return prodtyper;
	}
	public void setProdtyper(List<Prodtype> prodtyper) {
		this.prodtyper = prodtyper;
	}
	public boolean isAllProduksjon() {
		return allProduksjon;
	}
	public void setAllProduksjon(boolean allProduksjon) {
		this.allProduksjon = allProduksjon;
	}
	public void setKunSiste(boolean kunSiste) {
		this.kunSiste = kunSiste;
	}
	public boolean isKunSiste(){
		return kunSiste;
	}
	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}
	public Long getKontraktId() {
		return kontraktId;
	}
	public void setRodetypeId(Long rodetypeId) {
		this.rodetypeId = rodetypeId;
	}
	public Long getRodetypeId() {
		return rodetypeId;
	}
}
