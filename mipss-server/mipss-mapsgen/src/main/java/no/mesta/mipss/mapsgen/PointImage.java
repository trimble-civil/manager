package no.mesta.mipss.mapsgen;

import java.awt.Point;
import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Bilde for et punkt i kart
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PointImage implements Serializable {
	private Point hotspot;
	private byte[] imageData;

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof PointImage)) {
			return false;
		}

		PointImage other = (PointImage) o;

		return new EqualsBuilder().append(imageData, other.imageData).append(hotspot, other.hotspot).isEquals();
	}

	public Point getHotspot() {
		return hotspot;
	}

	public byte[] getImageData() {
		return imageData;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(imageData).append(hotspot).toHashCode();
	}

	public void setHotspot(Point hotspot) {
		this.hotspot = hotspot;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}
}
