package no.mesta.mipss.mapsgen;

import java.awt.Dimension;
import java.util.List;

import javax.ejb.Remote;

/**
 * Lager buffered images av kart for strekninger og punkter, sender dette
 * tilbake til klienten som byte[]
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * @author <a href="mailto:haraldk@mesan.no>Harald A. Kulø</a>
 */
@Remote
public interface MapsGen {
	public static final String BEAN_NAME = "MapsGenBean";

	/**
	 * Lager kartbildet
	 * 
	 * @param mapItems
	 *            strekninger og punkter
	 * @param pointImages
	 *            evt bilder for punkter blandt mapItems
	 * @param size
	 *            kartstørelsen i pixler som man ønsker
	 * 
	 * @return byte[] som representerer BufferedImage
	 */
	public byte[] generateMap(List<MapItem> mapItems, List<PointImage> pointImages, Dimension size, double resolution) throws MapGenerationException;

	/**
	 * Lager et kart
	 * @param box bounds til kartet
	 * @param size størrelsen til kartet
	 * @param useAuthentication bestemmer hvilken tjeneste man ender opp med å bruke, noen tjenester bruker autentisering.
	 * @param tokenString Brukes i tjenester som bruker autentisering, token sendes som et parameter i URL'en til kartelementet.
	 * @return
	 */
	byte[] getMap(Bbox box, Dimension size, boolean useAuthentication, String tokenString);
}