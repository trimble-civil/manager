package no.mesta.mipss.mapsgen;

/**
 * Exception som kastes når det ikke er mulig å generere kartet
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class MapGenerationException extends Exception {

	public MapGenerationException() {
	}

	public MapGenerationException(String message) {
		super(message);
	}

	public MapGenerationException(Throwable cause) {
		super(cause);
	}

	public MapGenerationException(String message, Throwable cause) {
		super(message, cause);
	}

}
