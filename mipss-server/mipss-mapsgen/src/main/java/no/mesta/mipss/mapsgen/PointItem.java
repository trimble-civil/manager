package no.mesta.mipss.mapsgen;

import java.awt.Point;
import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
* Representerer et kartpunkt
*
* @author <a href="arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
*/
public class PointItem extends MapItem implements Comparable<MapItem>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pointImageIndex;
	private Point pos;
	
	/** {@inheritDoc} */
	@Override
	public int compareTo(MapItem o) {
		if(o == null) {
			return 1;
		}
		
		if(o instanceof PointItem) {
			return 0;
		} else {
			return 1;
		}
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof PointItem)) {
			return false;
		}
		
		PointItem other = (PointItem) o;
		
		return new EqualsBuilder().append(pos, other.pos).append(pointImageIndex, other.pointImageIndex).isEquals();
	}

	public int getPointImageIndex() {
		return pointImageIndex;
	}

	public Point getPos() {
		return pos;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(pos).append(pointImageIndex).toHashCode();
	}
	
	public void setPointImageIndex(int pointImageIndex) {
		this.pointImageIndex = pointImageIndex;
	}
	
	public void setPos(Point pos) {
		this.pos = pos;
	}

	@Override
	public Bbox getBbox() {
		Bbox box = new Bbox();
		box.setLlx(pos.getX());
		box.setUrx(pos.getX());
		box.setLly(pos.getY());
		box.setUry(pos.getY());
		return box;
	}
}