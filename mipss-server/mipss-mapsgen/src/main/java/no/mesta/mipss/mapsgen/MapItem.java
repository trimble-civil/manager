package no.mesta.mipss.mapsgen;

import java.io.Serializable;

/**
* Inneholder sortering for punkter og strekninger
*/
@SuppressWarnings("serial")
public abstract class MapItem implements Comparable<MapItem>, Serializable{

	/** {@inheritDoc} */
	public abstract int compareTo(MapItem o);
	
	public abstract Bbox getBbox();
}