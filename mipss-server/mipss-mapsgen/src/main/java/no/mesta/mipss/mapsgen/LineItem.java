package no.mesta.mipss.mapsgen;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Representerer en strekning i et kart
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class LineItem extends MapItem implements Serializable {
	private Color color;
	private Point[] coords;
	
	/** {@inheritDoc} */
	@Override
	public int compareTo(MapItem o) {
		if (o == null) {
			return 1;
		}

		if (o instanceof LineItem) {
			return 0;
		} else {
			return -1;
		}
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (o == this) {
			return true;
		}

		if (!(o instanceof LineItem)) {
			return false;
		}

		LineItem other = (LineItem) o;

		return new EqualsBuilder().append(color, other.color).append(coords, other.coords).isEquals();
	}

	public Color getColor() {
		return color;
	}

	public Point[] getCoords() {
		return coords;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(color).append(coords).toHashCode();
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setCoords(Point[] coords) {
		this.coords = coords;
	}

	@Override
	public Bbox getBbox() {
		Bbox b = new Bbox();
		for (Point p:coords){
			Bbox bb = new Bbox();
			bb.setLlx(p.getX());
			bb.setLly(p.getY());
			bb.setUrx(p.getX());
			bb.setUry(p.getY());
			b.addBounds(bb);
		}
		return b;
	}
}