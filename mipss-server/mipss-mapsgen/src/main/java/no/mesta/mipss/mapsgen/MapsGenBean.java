package no.mesta.mipss.mapsgen;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.KonfigparamPK;
import no.mesta.mipss.util.MapURLHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementasjon av MapsGen
 * 
 * @author <a href="mailto:haralk@mesan.no>Harald A. Kulø</a>
 */
@Stateless(name = MapsGen.BEAN_NAME, mappedName="ejb/"+MapsGen.BEAN_NAME)
public class MapsGenBean implements MapsGen {
	@PersistenceContext(unitName="mipssPersistence")
	private EntityManager em;
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
    private String username;
    private String password;
    private boolean useAuthentication;
    String token = "";
    
	@EJB
	private KonfigParamLocal konfigParamLocal;

	
	/** {@inheritDoc} */
	public byte[] generateMap(List<MapItem> mapItems, List<PointImage> pointImages, Dimension size, double resolution) throws MapGenerationException{
		String app = "mipss.mapsgen";
		String tokenString="";
		Bbox box = getBboxForItems(mapItems);
		int s = Math.max(size.height, size.width);
		box = getBboxForMap(box, resolution, s);
		//Sjekk om vi skal bruke autentisering
		useAuthentication = Boolean.parseBoolean(konfigParamLocal.hentEnForApp(app, "bruk_nordeca_autentisering").getVerdi());
		//Hvis autentisering, hent ut token:
		if(useAuthentication){
			username = konfigParamLocal.hentEnForApp(app, "brukernavn").getVerdi();
			password = konfigParamLocal.hentEnForApp(app, "passord").getVerdi();
			tokenString = MapURLHelper.hentTokenString(token, konfigParamLocal.hentEnForApp(app, "token_url").getVerdi(),username,password);
		}

		//Sender med token uansett, har man ikke autentisering, er den tom:
		String mapUrl = getMapUrl(s, s, box.toBbox(), useAuthentication, tokenString);
		
		Image image = getImage(mapUrl);
		
		if (image==null)
			throw new MapGenerationException("Klarte ikke å hente bildet fra wms tjeneren");
		paint(mapItems, image.getGraphics(), pointImages, box, s);
		//må skalere bildet hvis det ikke er kvadratisk
		if (size.height!=s||size.width!=s){
			image = ImageUtil.scaleImage(new ImageIcon(image), size.width, size.height);
		}
		return ImageUtil.imageToBytes(image);
	}
	
	public byte[] getMap(Bbox box, Dimension size, boolean useAuthentication, String tokenString){
		String mapUrl = getMapUrl(size.height, size.width, box.toBbox(), useAuthentication, tokenString);
		BufferedImage image = getImage(mapUrl);
		return ImageUtil.imageToBytes(image);
	}
	/**
	 * Rendrer alle MapItems på graphics objektet
	 * @param mapItems Items som skal tegnes på kartet
	 * @param g graphics objektet som skal ta vare på grafikken
	 * @param pointImages bilder til mapItems som benytter bilder
	 * @param box boundingboxen til kartutsnittet
	 * @param size størrelsen på kartet i pixler
	 */
	private void paint(List<MapItem> mapItems, Graphics g, List<PointImage> pointImages, Bbox box, int size){
		for (MapItem item:mapItems){
			if (item instanceof PointItem){
				paintPoint((PointItem)item, pointImages.get(((PointItem) item).getPointImageIndex()), (Graphics2D)g, box, size);
			}
			if (item instanceof LineItem){
				paintLines((LineItem)item, (Graphics2D)g, box, size);
			}
		}
	}
	
	/**
	 * Tegner PointItems på kartet
	 * @param point
	 * @param image
	 * @param g
	 * @param box
	 * @param size
	 */
	private void paintPoint(PointItem point, PointImage image, Graphics2D g, Bbox box, int size){
		Point px = geoToPixel(point.getPos(), box, size);
		Image img = ImageUtil.bytesToImage(image.getImageData());
		g.drawImage(img, px.x-image.getHotspot().x, px.y-image.getHotspot().y, null);
	}
	/**
	 * Tegner LineItems på kartet
	 * @param lines
	 * @param g
	 * @param box
	 * @param size
	 */
	private void paintLines(LineItem lines, Graphics2D g, Bbox box, int size){
		int count = 0;
		Point prev = null;
		g.setColor(lines.getColor());
		g.setStroke(new BasicStroke(3));
		for (Point p:lines.getCoords()){
    		if ((count!=0)){
    			Point p1 = geoToPixel(prev, box, size);
    			Point p2 = geoToPixel(p, box, size);
    			g.drawLine(p1.x, p1.y, p2.x, p2.y);
    			prev = p;
    		}
    		if (count==0)
              prev = p;
          count++;
    	}
	}
	
	/**
	 * Konverterer en koordinat til en pixel
	 * 
	 * @param geo punktet som skal konverteres
	 * @param box boundingboxen til kartet
	 * @param size størrelsen til kartet i pixler
	 * @return
	 */
	private Point geoToPixel(Point geo, Bbox box, int size){
		double width = box.getWidth();//box.getUrx() - box.getLlx();
        double res = width/size;
    	double x = (geo.getX() - box.getLlx()) / res;
    	double ury = size * res+box.getLly();
    	double y = (ury - geo.getY()) / res;
    	return new Point((int)x, (int)y);
	}
	
	/**
	 * Henter et bilde for url
	 * @param url
	 * @return
	 */
	private BufferedImage getImage(String url){
		BufferedImage image  = null;
		int tries = 3;
        while (image==null && tries > 0) {
			try{
				URI uri=null;
				try {
					uri = new URI(url);
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
				InputStream ins = uri.toURL().openStream();
		        image = ImageIO.read(ins);
		        return image;
			} catch (Exception e){
				e.printStackTrace();
				tries--;
				log.debug("Error getting image, retries left:"+tries);
			}
        }
        return image;
	}

	/**
	 * Returnerer boundingboxen for alle items som skal tegnes på kartet
	 * @param mapItems
	 * @return
	 */
	private Bbox getBboxForItems(List<MapItem> mapItems){
		Bbox box = new Bbox();
		for (MapItem item:mapItems){
			box.addBounds(item.getBbox());
		}
		return box;
	}
	
	/**
	 * Lager en boundingbox for kartet basert på bbox for kartitems, resolution og størrelsen på bildet
	 * Plasserer items i senter av boundingboxen 
	 * @param itemBbox boundingboxen til alle items i kartet
	 * @param resolution oppløsningen (m/pixel) til kartet
	 * @param size størrelsen til kartet i pixler
	 * @return
	 */
	private Bbox getBboxForMap(Bbox itemBbox, double resolution, int size){
		double s = size*resolution;
		Bbox box = new Bbox();
		double xdiff = (s-itemBbox.getWidth())/2;
		box.setLlx(itemBbox.getLlx()-(xdiff));
		box.setUrx(itemBbox.getUrx()+(xdiff));
		
		double ydiff = (s-itemBbox.getHeight())/2;
		box.setLly(itemBbox.getLly()-(ydiff));
		box.setUry(itemBbox.getUry()+(ydiff));
		return box;
	}
	
	/**
	 * Lager en url til kartet
	 * @param height høyden til kartet i pixler
	 * @param width bredden til kartet i pixler
	 * @param bbox boundingboxen til kartet
	 * @return
	 */
	private String getMapUrl(int height, int width, String bbox, boolean useAuthentication, String tokenString){
		String format = "image/png";
        String srs = "EPSG:32633";
        String tiled = "true";
        String service = "wms";
        String version = "1.1.1";
        String request = "GetMap";
        String exceptions = "application/vnd.ogc.se_inimage";
        
        KonfigparamPK pk = new KonfigparamPK("mipss.mapsgen", "layer");
        String layers = em.find(Konfigparam.class, pk).getVerdi();//"MestaWMS";//
        pk = new KonfigparamPK("studio.mipss.mapplugin", "mapserverTilesOrigin");
        String tilesOrigin = em.find(Konfigparam.class, pk).getVerdi();//"-640841.17766,6278226.21073";//
        pk = new KonfigparamPK("mipss.mapsgen", "wmsbaseurl");
        String baseUrl = em.find(Konfigparam.class, pk).getVerdi();//"http://mest-lxap003.vegprod.no:8080/geoserver/wms?";//
        
        
        String url = baseUrl+ 
        				"?"+
                        "height="+height+
                        "&width="+width+
                        "&layers="+layers+
                        "&styles="+
                        "&SRS="+srs+
                        "&format="+format+
                        "&tiled="+tiled+
                        "&tilesorigin="+tilesOrigin+
                        "&service="+service+
                        "&version="+version+
                        "&request="+request+
                        "&exceptions="+exceptions+
                        "&BBOX="+bbox+
                        "";

        if(useAuthentication){
        	url = url +"&tokenstring=" +  tokenString;
        }


        log.debug(url);
        return url;
	}
	 
	public static void main(String[] args) throws Exception {
		getTestData();
	}
	public static void getTestData() throws Exception{
		List<MapItem> items = new ArrayList<MapItem>();
		items.add(get1());
		items.add(get2());
		items.add(get3());
		items.add(get4());
		items.add(get5());
		items.add(get6());

		double scale = 0.4;
		MapsGenBean b = new MapsGenBean();
		byte[] generateMap = b.generateMap(items, new ArrayList<PointImage>(), new Dimension(900, 768), scale);
		BufferedImage img = ImageUtil.bytesToImage(generateMap);
		JFrame f = new JFrame();
		f.add(new JLabel(new ImageIcon(img)));
		f.pack();
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	private static LineItem get6() {
		LineItem li5 = new LineItem();
		li5.setColor(Color.BLUE);
		Point[] p5 = new Point[52];
		Point pp0 = new Point();
		pp0.setLocation(256209.0, 6649605.0);
		p5[0]=pp0;
		Point pp1 = new Point();
		pp1.setLocation(256207.0, 6649608.0);
		p5[1]=pp1;
		Point pp2 = new Point();
		pp2.setLocation(256203.0, 6649613.0);
		p5[2]=pp2;
		Point pp3 = new Point();
		pp3.setLocation(256199.0, 6649618.0);
		p5[3]=pp3;
		Point pp4 = new Point();
		pp4.setLocation(256197.0, 6649622.0);
		p5[4]=pp4;
		Point pp5 = new Point();
		pp5.setLocation(256194.0, 6649627.0);
		p5[5]=pp5;
		Point pp6 = new Point();
		pp6.setLocation(256193.0, 6649631.0);
		p5[6]=pp6;
		Point pp7 = new Point();
		pp7.setLocation(256191.0, 6649635.0);
		p5[7]=pp7;
		Point pp8 = new Point();
		pp8.setLocation(256189.0, 6649642.0);
		p5[8]=pp8;
		Point pp9 = new Point();
		pp9.setLocation(256187.0, 6649650.0);
		p5[9]=pp9;
		Point pp10 = new Point();
		pp10.setLocation(256185.0, 6649655.0);
		p5[10]=pp10;
		Point pp11 = new Point();
		pp11.setLocation(256184.0, 6649660.0);
		p5[11]=pp11;
		Point pp12 = new Point();
		pp12.setLocation(256183.0, 6649666.0);
		p5[12]=pp12;
		Point pp13 = new Point();
		pp13.setLocation(256182.0, 6649671.0);
		p5[13]=pp13;
		Point pp14 = new Point();
		pp14.setLocation(256181.0, 6649673.0);
		p5[14]=pp14;
		Point pp15 = new Point();
		pp15.setLocation(256181.0, 6649676.0);
		p5[15]=pp15;
		Point pp16 = new Point();
		pp16.setLocation(256180.0, 6649680.0);
		p5[16]=pp16;
		Point pp17 = new Point();
		pp17.setLocation(256179.0, 6649684.0);
		p5[17]=pp17;
		Point pp18 = new Point();
		pp18.setLocation(256179.0, 6649688.0);
		p5[18]=pp18;
		Point pp19 = new Point();
		pp19.setLocation(256179.0, 6649690.0);
		p5[19]=pp19;
		Point pp20 = new Point();
		pp20.setLocation(256179.0, 6649693.0);
		p5[20]=pp20;
		Point pp21 = new Point();
		pp21.setLocation(256179.0, 6649693.0);
		p5[21]=pp21;
		Point pp22 = new Point();
		pp22.setLocation(256179.0, 6649695.0);
		p5[22]=pp22;
		Point pp23 = new Point();
		pp23.setLocation(256179.0, 6649698.0);
		p5[23]=pp23;
		Point pp24 = new Point();
		pp24.setLocation(256180.0, 6649700.0);
		p5[24]=pp24;
		Point pp25 = new Point();
		pp25.setLocation(256182.0, 6649702.0);
		p5[25]=pp25;
		Point pp26 = new Point();
		pp26.setLocation(256183.0, 6649704.0);
		p5[26]=pp26;
		Point pp27 = new Point();
		pp27.setLocation(256185.0, 6649706.0);
		p5[27]=pp27;
		Point pp28 = new Point();
		pp28.setLocation(256187.0, 6649708.0);
		p5[28]=pp28;
		Point pp29 = new Point();
		pp29.setLocation(256189.0, 6649710.0);
		p5[29]=pp29;
		Point pp30 = new Point();
		pp30.setLocation(256191.0, 6649711.0);
		p5[30]=pp30;
		Point pp31 = new Point();
		pp31.setLocation(256194.0, 6649712.0);
		p5[31]=pp31;
		Point pp32 = new Point();
		pp32.setLocation(256197.0, 6649714.0);
		p5[32]=pp32;
		Point pp33 = new Point();
		pp33.setLocation(256200.0, 6649715.0);
		p5[33]=pp33;
		Point pp34 = new Point();
		pp34.setLocation(256202.0, 6649716.0);
		p5[34]=pp34;
		Point pp35 = new Point();
		pp35.setLocation(256204.0, 6649717.0);
		p5[35]=pp35;
		Point pp36 = new Point();
		pp36.setLocation(256214.0, 6649721.0);
		p5[36]=pp36;
		Point pp37 = new Point();
		pp37.setLocation(256222.0, 6649724.0);
		p5[37]=pp37;
		Point pp38 = new Point();
		pp38.setLocation(256229.0, 6649726.0);
		p5[38]=pp38;
		Point pp39 = new Point();
		pp39.setLocation(256236.0, 6649727.0);
		p5[39]=pp39;
		Point pp40 = new Point();
		pp40.setLocation(256244.0, 6649730.0);
		p5[40]=pp40;
		Point pp41 = new Point();
		pp41.setLocation(256251.0, 6649731.0);
		p5[41]=pp41;
		Point pp42 = new Point();
		pp42.setLocation(256260.0, 6649733.0);
		p5[42]=pp42;
		Point pp43 = new Point();
		pp43.setLocation(256268.0, 6649734.0);
		p5[43]=pp43;
		Point pp44 = new Point();
		pp44.setLocation(256275.0, 6649736.0);
		p5[44]=pp44;
		Point pp45 = new Point();
		pp45.setLocation(256280.0, 6649736.0);
		p5[45]=pp45;
		Point pp46 = new Point();
		pp46.setLocation(256286.0, 6649737.0);
		p5[46]=pp46;
		Point pp47 = new Point();
		pp47.setLocation(256286.0, 6649737.0);
		p5[47]=pp47;
		Point pp48 = new Point();
		pp48.setLocation(256293.0, 6649738.0);
		p5[48]=pp48;
		Point pp49 = new Point();
		pp49.setLocation(256297.0, 6649738.0);
		p5[49]=pp49;
		Point pp50 = new Point();
		pp50.setLocation(256305.0, 6649739.0);
		p5[50]=pp50;
		Point pp51 = new Point();
		pp51.setLocation(256307.0, 6649739.0);
		p5[51]=pp51;
		li5.setCoords(p5);
		return li5;
	}

	private static LineItem get5() {
		LineItem li4 = new LineItem();
		li4.setColor(Color.BLUE);
		Point[] p4 = new Point[51];
		Point pp0 = new Point();
		pp0.setLocation(256312.0, 6649733.0);
		p4[0]=pp0;
		Point pp1 = new Point();
		pp1.setLocation(256313.0, 6649732.0);
		p4[1]=pp1;
		Point pp2 = new Point();
		pp2.setLocation(256314.0, 6649732.0);
		p4[2]=pp2;
		Point pp3 = new Point();
		pp3.setLocation(256314.0, 6649732.0);
		p4[3]=pp3;
		Point pp4 = new Point();
		pp4.setLocation(256315.0, 6649732.0);
		p4[4]=pp4;
		Point pp5 = new Point();
		pp5.setLocation(256316.0, 6649732.0);
		p4[5]=pp5;
		Point pp6 = new Point();
		pp6.setLocation(256317.0, 6649732.0);
		p4[6]=pp6;
		Point pp7 = new Point();
		pp7.setLocation(256317.0, 6649732.0);
		p4[7]=pp7;
		Point pp8 = new Point();
		pp8.setLocation(256318.0, 6649732.0);
		p4[8]=pp8;
		Point pp9 = new Point();
		pp9.setLocation(256319.0, 6649733.0);
		p4[9]=pp9;
		Point pp10 = new Point();
		pp10.setLocation(256320.0, 6649733.0);
		p4[10]=pp10;
		Point pp11 = new Point();
		pp11.setLocation(256320.0, 6649733.0);
		p4[11]=pp11;
		Point pp12 = new Point();
		pp12.setLocation(256320.0, 6649734.0);
		p4[12]=pp12;
		Point pp13 = new Point();
		pp13.setLocation(256321.0, 6649734.0);
		p4[13]=pp13;
		Point pp14 = new Point();
		pp14.setLocation(256322.0, 6649735.0);
		p4[14]=pp14;
		Point pp15 = new Point();
		pp15.setLocation(256322.0, 6649736.0);
		p4[15]=pp15;
		Point pp16 = new Point();
		pp16.setLocation(256323.0, 6649737.0);
		p4[16]=pp16;
		Point pp17 = new Point();
		pp17.setLocation(256323.0, 6649738.0);
		p4[17]=pp17;
		Point pp18 = new Point();
		pp18.setLocation(256323.0, 6649739.0);
		p4[18]=pp18;
		Point pp19 = new Point();
		pp19.setLocation(256323.0, 6649740.0);
		p4[19]=pp19;
		Point pp20 = new Point();
		pp20.setLocation(256323.0, 6649740.0);
		p4[20]=pp20;
		Point pp21 = new Point();
		pp21.setLocation(256323.0, 6649741.0);
		p4[21]=pp21;
		Point pp22 = new Point();
		pp22.setLocation(256323.0, 6649742.0);
		p4[22]=pp22;
		Point pp23 = new Point();
		pp23.setLocation(256323.0, 6649743.0);
		p4[23]=pp23;
		Point pp24 = new Point();
		pp24.setLocation(256322.0, 6649744.0);
		p4[24]=pp24;
		Point pp25 = new Point();
		pp25.setLocation(256322.0, 6649745.0);
		p4[25]=pp25;
		Point pp26 = new Point();
		pp26.setLocation(256321.0, 6649745.0);
		p4[26]=pp26;
		Point pp27 = new Point();
		pp27.setLocation(256321.0, 6649746.0);
		p4[27]=pp27;
		Point pp28 = new Point();
		pp28.setLocation(256321.0, 6649746.0);
		p4[28]=pp28;
		Point pp29 = new Point();
		pp29.setLocation(256320.0, 6649747.0);
		p4[29]=pp29;
		Point pp30 = new Point();
		pp30.setLocation(256319.0, 6649747.0);
		p4[30]=pp30;
		Point pp31 = new Point();
		pp31.setLocation(256318.0, 6649747.0);
		p4[31]=pp31;
		Point pp32 = new Point();
		pp32.setLocation(256317.0, 6649748.0);
		p4[32]=pp32;
		Point pp33 = new Point();
		pp33.setLocation(256316.0, 6649748.0);
		p4[33]=pp33;
		Point pp34 = new Point();
		pp34.setLocation(256316.0, 6649748.0);
		p4[34]=pp34;
		Point pp35 = new Point();
		pp35.setLocation(256315.0, 6649748.0);
		p4[35]=pp35;
		Point pp36 = new Point();
		pp36.setLocation(256314.0, 6649748.0);
		p4[36]=pp36;
		Point pp37 = new Point();
		pp37.setLocation(256313.0, 6649748.0);
		p4[37]=pp37;
		Point pp38 = new Point();
		pp38.setLocation(256312.0, 6649747.0);
		p4[38]=pp38;
		Point pp39 = new Point();
		pp39.setLocation(256312.0, 6649747.0);
		p4[39]=pp39;
		Point pp40 = new Point();
		pp40.setLocation(256311.0, 6649747.0);
		p4[40]=pp40;
		Point pp41 = new Point();
		pp41.setLocation(256311.0, 6649746.0);
		p4[41]=pp41;
		Point pp42 = new Point();
		pp42.setLocation(256310.0, 6649746.0);
		p4[42]=pp42;
		Point pp43 = new Point();
		pp43.setLocation(256309.0, 6649745.0);
		p4[43]=pp43;
		Point pp44 = new Point();
		pp44.setLocation(256309.0, 6649744.0);
		p4[44]=pp44;
		Point pp45 = new Point();
		pp45.setLocation(256308.0, 6649743.0);
		p4[45]=pp45;
		Point pp46 = new Point();
		pp46.setLocation(256308.0, 6649743.0);
		p4[46]=pp46;
		Point pp47 = new Point();
		pp47.setLocation(256308.0, 6649742.0);
		p4[47]=pp47;
		Point pp48 = new Point();
		pp48.setLocation(256307.0, 6649741.0);
		p4[48]=pp48;
		Point pp49 = new Point();
		pp49.setLocation(256307.0, 6649740.0);
		p4[49]=pp49;
		Point pp50 = new Point();
		pp50.setLocation(256307.0, 6649739.0);
		p4[50]=pp50;
		li4.setCoords(p4);
		return li4;
	}

	private static LineItem get4() {
		LineItem li3 = new LineItem();
		li3.setColor(Color.BLUE);
		Point[] p3 = new Point[20];
		Point pp0 = new Point();
		pp0.setLocation(256312.0, 6649733.0);
		p3[0]=pp0;
		Point pp1 = new Point();
		pp1.setLocation(256313.0, 6649732.0);
		p3[1]=pp1;
		Point pp2 = new Point();
		pp2.setLocation(256314.0, 6649732.0);
		p3[2]=pp2;
		Point pp3 = new Point();
		pp3.setLocation(256314.0, 6649732.0);
		p3[3]=pp3;
		Point pp4 = new Point();
		pp4.setLocation(256315.0, 6649732.0);
		p3[4]=pp4;
		Point pp5 = new Point();
		pp5.setLocation(256316.0, 6649732.0);
		p3[5]=pp5;
		Point pp6 = new Point();
		pp6.setLocation(256317.0, 6649732.0);
		p3[6]=pp6;
		Point pp7 = new Point();
		pp7.setLocation(256317.0, 6649732.0);
		p3[7]=pp7;
		Point pp8 = new Point();
		pp8.setLocation(256318.0, 6649732.0);
		p3[8]=pp8;
		Point pp9 = new Point();
		pp9.setLocation(256319.0, 6649733.0);
		p3[9]=pp9;
		Point pp10 = new Point();
		pp10.setLocation(256320.0, 6649733.0);
		p3[10]=pp10;
		Point pp11 = new Point();
		pp11.setLocation(256320.0, 6649733.0);
		p3[11]=pp11;
		Point pp12 = new Point();
		pp12.setLocation(256320.0, 6649734.0);
		p3[12]=pp12;
		Point pp13 = new Point();
		pp13.setLocation(256321.0, 6649734.0);
		p3[13]=pp13;
		Point pp14 = new Point();
		pp14.setLocation(256322.0, 6649735.0);
		p3[14]=pp14;
		Point pp15 = new Point();
		pp15.setLocation(256322.0, 6649736.0);
		p3[15]=pp15;
		Point pp16 = new Point();
		pp16.setLocation(256323.0, 6649737.0);
		p3[16]=pp16;
		Point pp17 = new Point();
		pp17.setLocation(256323.0, 6649738.0);
		p3[17]=pp17;
		Point pp18 = new Point();
		pp18.setLocation(256323.0, 6649739.0);
		p3[18]=pp18;
		Point pp19 = new Point();
		pp19.setLocation(256323.0, 6649740.0);
		p3[19]=pp19;
		li3.setCoords(p3);
		return li3;
	}

	private static LineItem get3() {
		LineItem li2 = new LineItem();
		li2.setColor(Color.BLUE);
		Point[] p2 = new Point[3];
		Point pp0 = new Point();
		pp0.setLocation(256323.0, 6649740.0);
		p2[0]=pp0;
		Point pp1 = new Point();
		pp1.setLocation(256325.0, 6649740.0);
		p2[1]=pp1;
		Point pp2 = new Point();
		pp2.setLocation(256328.0, 6649740.0);
		p2[2]=pp2;
		li2.setCoords(p2);
		return li2;
	}

	private static LineItem get2() {
		LineItem li1 = new LineItem();
		li1.setColor(Color.BLUE);
		Point[] p1 = new Point[5];
		Point pp0 = new Point();
		pp0.setLocation(256328.0, 6649740.0);
		p1[0]=pp0;
		Point pp1 = new Point();
		pp1.setLocation(256330.0, 6649740.0);
		p1[1]=pp1;
		Point pp2 = new Point();
		pp2.setLocation(256335.0, 6649740.0);
		p1[2]=pp2;
		Point pp3 = new Point();
		pp3.setLocation(256342.0, 6649741.0);
		p1[3]=pp3;
		Point pp4 = new Point();
		pp4.setLocation(256345.0, 6649741.0);
		p1[4]=pp4;
		li1.setCoords(p1);
		return li1;
	}

	private static LineItem get1() {
		LineItem li0 = new LineItem();
		li0.setColor(Color.BLUE);
		Point[] p0 = new Point[4];
		Point pp0 = new Point();
		pp0.setLocation(256359.0, 6649742.0);
		p0[0]=pp0;
		Point pp1 = new Point();
		pp1.setLocation(256356.0, 6649742.0);
		p0[1]=pp1;
		Point pp2 = new Point();
		pp2.setLocation(256350.0, 6649742.0);
		p0[2]=pp2;
		Point pp3 = new Point();
		pp3.setLocation(256345.0, 6649741.0);
		p0[3]=pp3;
		li0.setCoords(p0);
		return li0;
	}
}
//public static void main(String[] args) {
//	List<MapItem> mapItems = new ArrayList<MapItem>();
//	PointItem i = new PointItem();
//	i.setPointImageIndex(0);
//	i.setPos(new Point2D.Double(246778, 6635197));
//	mapItems.add(i);
//	i = new PointItem();
//	i.setPointImageIndex(0);
//	i.setPos(new Point2D.Double(-12364,6730590));
//	mapItems.add(i);
//	i.setPos(new Point2D.Double(246777.21,6635192.312));
//	mapItems.add(i);
//	i = new PointItem();
//	i.setPointImageIndex(0);
//	i.setPos(new Point2D.Double(246872.40,6635295.312));
//	mapItems.add(i);
//	i = new PointItem();
//	i.setPointImageIndex(0);
//	i.setPos(new Point2D.Double(246794.00986, 6635242.180264));
//	mapItems.add(i);

//	
//	PointImage img = new PointImage();
//	img.setImageData(ImageUtil.imageToBytes(load("C:/code/mipss-studio/trunk/studio-resources/src/main/resources/images/map_pin_righty.png")));
//	img.setHotspot(new Point(0, 41));
//	List<PointImage> imgs = new ArrayList<PointImage>();
//	imgs.add(img);
//	
//	Point2D p1 = new Point2D.Double(246778, 6635303);//LL
//	Point2D p2 = new Point2D.Double(246859, 6635197);//UR
//	Point2D pp1= new Point2D.Double(246859, 6635303);//LR
//	Point2D pp2= new Point2D.Double(246778, 6635197);//UL
//	LineItem l1 = new LineItem();
//	l1.setColor(Color.blue);
//	l1.setCoords(new Point2D[]{p1, p2});
//	
//	LineItem l2 = new LineItem();
//	l2.setColor(Color.red);
//	l2.setCoords(new Point2D[]{pp1, pp2});
//	mapItems.add(l1);
//	mapItems.add(l2);
//	
//	Dimension size = new Dimension(1500,500);
//	byte[] generateMap = new MapsGenBean().generateMap(mapItems, imgs, size, 0.5);
//	BufferedImage ig = ImageUtil.bytesToImage(generateMap);
//	JFrame ff = new JFrame();
//	ff.add(new JLabel(new ImageIcon(ig)));
//	ff.pack();
//	ff.setVisible(true);
//}
//private static BufferedImage load(String iconFile) {
//	File f = new File(iconFile);
//	try {
//		return ImageIO.read(f);
//	} catch (IOException e) {
//		return null;
//	}
//}
//}


