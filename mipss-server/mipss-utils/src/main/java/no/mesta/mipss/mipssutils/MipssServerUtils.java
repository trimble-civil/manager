package no.mesta.mipss.mipssutils;

import java.util.List;

import javax.ejb.Remote;

/**
 * Henter objekter og GUID'er
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Remote
public interface MipssServerUtils {
	public static final String BEAN_NAME = "MipssServerUtilsBean";

	/**
	 * <p>Henter en guid</p>
	 * 
	 * <p><code>SELECT SYS_GUID() FROM dual;</code></p>
	 * 
	 * @return
	 */
	public String fetchGuid();
	
	/**
	 * Tømmer EM for data. Gjør først en flush() for å dumpe det som evt ikke er lagret i db, deretter en clear()
	 */
	void clearEM();
	
	/**
	 * Henter en liste med objekter fra databasen basert på GUID
	 * 
	 * @param <T>
	 * @param klasse
	 * @param nokkler
	 * @return
	 */
	public<T> List<T> hentObjekter(Class<T> klasse, List<String> nokkler);
	
	public String hentSekvens(String navn);
	
	/**
	 * Henter masken for kontinuerlig friksjonsdata (bit innstilling)
	 * @return
	 */
	Object hentMaskeKontFriksjon();
}