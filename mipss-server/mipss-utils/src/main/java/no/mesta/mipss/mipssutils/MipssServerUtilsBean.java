package no.mesta.mipss.mipssutils;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.factories.SessionManager;

/**
 * Henter objekter og GUID'er
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Stateless(name = MipssServerUtils.BEAN_NAME, mappedName="ejb/"+MipssServerUtils.BEAN_NAME)
public class MipssServerUtilsBean implements MipssServerUtils{
	private static final String BLANK = "";
	private static final String COMMA = ",";
	private static final String IN = "in";
	private static final String LEFT_P = "(";
	private static final Logger logger = LoggerFactory.getLogger(MipssServerUtilsBean.class);
	private static final String RIGHT_P = ")";
	private static final String SPACE = " ";
	private static final String SQUOTE = "'";
	private Logger log = LoggerFactory.getLogger(MipssServerUtilsBean.class);
	@PersistenceContext(unitName = "mipssPersistence")
	EntityManager em;
	
	public void clearEM(){
		em.flush();
		em.clear();
		if (JpaHelper.isEclipseLink(em)){
			log.debug("Jpa ServerSession will invalidate all entities");
			JpaHelper.getEntityManager(em).getServerSession().getIdentityMapAccessor().invalidateAll();
			log.debug("All entities are invalidated");
		}
	}
	protected String createGroup(List<?> ider, boolean ignoreString) {
		String sql = BLANK;
		int i = 0;
		for (Object id : ider) {
			if (id instanceof String && !ignoreString) {
				sql += SQUOTE + id + SQUOTE;
			} else {
				sql += id;
			}
			if (i < ider.size() - 1) {
				sql += COMMA;
			}
			i++;
		}
		return sql;
	}

	protected String createGroup(Object[] ider, boolean ignoreString) {
		String sql = BLANK;
		int i = 0;
		for (Object id : ider) {
			if (id instanceof String && !ignoreString) {
				sql += SQUOTE + id + SQUOTE;
			} else {
				sql += id;
			}
			if (i < ider.length - 1) {
				sql += COMMA;
			}
			i++;
		}

		return sql;
	}

	protected String createSqlIn(List<?> guider) {
		final String sql = SPACE + IN + SPACE + LEFT_P + createGroup(guider, false) + RIGHT_P;
		return sql;
	}

	protected String createSqlIn(Object[] ider) {
		final String sql = SPACE + IN + SPACE + LEFT_P + createGroup(ider, false) + RIGHT_P;
		return sql;
	}

	/** {@inheritDoc} */
	public String fetchGuid() {
		final String nativeQuery = "SELECT RAWTOHEX(SYS_GUID()) FROM dual";
		
		String guid = (String) em.createNativeQuery(nativeQuery).getSingleResult();

		/*String guid = null;
		if(r.size() == 1) {
			guid = (String) r.get(0);
		}*/
		return guid;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public <T> List<T> hentObjekter(Class<T> klasse, List<String> nokkler) {
		logger.debug("hentObjekter(" + klasse + ", " + nokkler + ") start");
		final String select = "SELECT o FROM " + klasse.getSimpleName() + " o WHERE o.guid " + createSqlIn(nokkler);
		
		final Query query = em.createQuery(select);

		logger.debug("Will execute query: " + select);
		List<T> entities = query.getResultList();
		logger.debug("hentObjekter(" + klasse + ", " + nokkler + ") done with "
				+ (entities == null ? "null" : String.valueOf(entities.size())));
		return entities;
	}

	public String hentSekvens(String navn) {
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append(navn);
		sb.append(".nextval from dual");
		final Query query = em.createNativeQuery(sb.toString());
		List<?> resultList = query.getResultList();
    	if (resultList.isEmpty())
    		throw new RuntimeException("Klarte ikke å hente ny id fra sekvens("+navn+")!");
    	String value = String.valueOf(resultList.get(0));
    	return value;
	}
	
	public Object hentMaskeKontFriksjon(){
		StringBuilder sb = new StringBuilder();
		sb.append("select admin_pk.hent_maske_kont_friksjon from dual");
		
		return em.createNativeQuery(sb.toString()).getSingleResult();
	}
}
