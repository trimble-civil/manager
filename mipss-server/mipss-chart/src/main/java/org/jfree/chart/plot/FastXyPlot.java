package org.jfree.chart.plot;
import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Map;

import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.chart.plot.XYPlot;

import org.jfree.chart.renderer.RendererUtilities;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;


/**
* Performs fast rendering of large datasets in nearly constant time.
*/
@SuppressWarnings("serial")
public class FastXyPlot extends XYPlot {

   private final HashSet<Integer> renderedPixels = new HashSet<Integer>();

   public FastXyPlot(XYDataset dataset, ValueAxis domainAxis, ValueAxis rangeAxis, XYItemRenderer renderer) {
      super(dataset, domainAxis, rangeAxis, renderer);
   }

   /**
    * Determines if the item is to be rendered in the area of the plot where one of the previous
    * items has already been rendered.
    */
   private boolean hasRendered(XYDataset dataset, ValueAxis xAxis, ValueAxis yAxis, RectangleEdge domainEdge,
                        RectangleEdge rangeEdge, Rectangle2D dataArea, int series, int item) {
      boolean hasRendered = true;

      int width = (int) dataArea.getWidth();

      double xValue = dataset.getXValue(series, item);
      double yValue = dataset.getYValue(series, item);
      int x = (int) xAxis.valueToJava2D(xValue, dataArea, domainEdge);
      int y = (int) yAxis.valueToJava2D(yValue, dataArea, rangeEdge);

      int itemKey = x + width * y;
      if (!renderedPixels.contains(itemKey)) {
         renderedPixels.add(itemKey);
         hasRendered = false;
      }

      return hasRendered;
   }


   @Override
   public boolean render(Graphics2D g2,
                    Rectangle2D dataArea,
                    int index,
                    PlotRenderingInfo info,
                    CrosshairState crosshairState) {


      boolean foundData = false;
      boolean disableOptimization = false;

      XYDataset dataset = getDataset(index);

      if (!DatasetUtilities.isEmptyOrNull(dataset)) {
         foundData = true;
         ValueAxis xAxis = getDomainAxisForDataset(index);
         ValueAxis yAxis = getRangeAxisForDataset(index);
         XYItemRenderer renderer = getRenderer(index);
         if (renderer == null) {
            renderer = getRenderer();
            if (renderer == null) { // no default renderer available
               return foundData;
            }
         }

         XYItemRendererState state = renderer.initialise(g2, dataArea, this, dataset, info);
         int passCount = renderer.getPassCount();

         if (renderer instanceof XYLineAndShapeRenderer) {
            disableOptimization = ((XYLineAndShapeRenderer) renderer).getDrawSeriesLineAsPath();   // in case of e.g. splines
         }

         RectangleEdge domainEdge = getDomainAxisEdge();
         RectangleEdge rangeEdge = getDomainAxisEdge();

         SeriesRenderingOrder seriesOrder = getSeriesRenderingOrder();
         if (seriesOrder == SeriesRenderingOrder.REVERSE) {
            //render series in reverse order
            for (int pass = 0; pass < passCount; pass++) {
               renderedPixels.clear();                     // need to clear every pass or else shapes won't be drawn correctly
               int seriesCount = dataset.getSeriesCount();
               for (int series = seriesCount - 1; series >= 0; series--) {
                  int firstItem = 0;
                  int lastItem = dataset.getItemCount(series) - 1;
                  if (lastItem == -1) {
                     continue;
                  }
                  if (state.getProcessVisibleItemsOnly()) {
                     int[] itemBounds = RendererUtilities.findLiveItems(dataset, series, xAxis.getLowerBound(), xAxis.getUpperBound());
                     firstItem = itemBounds[0];
                     lastItem = itemBounds[1];
                  }
                  for (int item = firstItem; item <= lastItem; item++) {
                     if (disableOptimization || !hasRendered(dataset, xAxis, yAxis, domainEdge, rangeEdge, dataArea, series, item)) {
                        renderer.drawItem(g2, state, dataArea, info, this, xAxis, yAxis, dataset, series, item, crosshairState, pass);
                     }
                  }
               }
            }
         } else {
            //render series in forward order
            for (int pass = 0; pass < passCount; pass++) {
               renderedPixels.clear();                     // need to clear every pass or else shapes won't be drawn correctly
               int seriesCount = dataset.getSeriesCount();
               for (int series = 0; series < seriesCount; series++) {
                  int firstItem = 0;
                  int lastItem = dataset.getItemCount(series) - 1;
                  if (state.getProcessVisibleItemsOnly()) {
                     int[] itemBounds = RendererUtilities.findLiveItems(dataset, series, xAxis.getLowerBound(), xAxis.getUpperBound());
                     firstItem = itemBounds[0];
                     lastItem = itemBounds[1];
                  }
                  for (int item = firstItem; item <= lastItem; item++) {
                     if (disableOptimization || !hasRendered(dataset, xAxis, yAxis, domainEdge, rangeEdge, dataArea, series, item)) {
                        renderer.drawItem(g2, state, dataArea, info, this, xAxis, yAxis, dataset, series, item, crosshairState, pass);
                     }
                  }
               }
            }
         }
      }
      return foundData;
   }
   @Override
   public void draw(Graphics2D g2, Rectangle2D area, Point2D anchor,
           PlotState parentState, PlotRenderingInfo info) {

//       // if the plot area is too small, just return...
//       boolean b1 = (area.getWidth() <= MINIMUM_WIDTH_TO_DRAW);
//       boolean b2 = (area.getHeight() <= MINIMUM_HEIGHT_TO_DRAW);
//       if (b1 || b2) {
//           return;
//       }

       // record the plot area...
       if (info != null) {
           info.setPlotArea(area);
       }

       // adjust the drawing area for the plot insets (if any)...
       RectangleInsets insets = getInsets();
       insets.trim(area);

       AxisSpace space = calculateAxisSpace(g2, area);
       Rectangle2D dataArea = space.shrink(area, null);
       
       getAxisOffset().trim(dataArea);
       createAndAddEntity((Rectangle2D) dataArea.clone(), info, null, null);
       if (info != null) {
           info.setDataArea(dataArea);
       }

       // draw the plot background and axes...
       drawBackground(g2, dataArea);
       Map axisStateMap = drawAxes(g2, area, dataArea, info);

       PlotOrientation orient = getOrientation();

       // the anchor point is typically the point where the mouse last
       // clicked - the crosshairs will be driven off this point...
       if (anchor != null && !dataArea.contains(anchor)) {
           anchor = null;
       }
       CrosshairState crosshairState = new CrosshairState();
       crosshairState.setCrosshairDistance(Double.POSITIVE_INFINITY);
       crosshairState.setAnchor(anchor);

       crosshairState.setAnchorX(Double.NaN);
       crosshairState.setAnchorY(Double.NaN);
       if (anchor != null) {
           ValueAxis domainAxis = getDomainAxis();
           if (domainAxis != null) {
               double x;
               if (orient == PlotOrientation.VERTICAL) {
                   x = domainAxis.java2DToValue(anchor.getX(), dataArea,
                           getDomainAxisEdge());
               }
               else {
                   x = domainAxis.java2DToValue(anchor.getY(), dataArea,
                           getDomainAxisEdge());
               }
               crosshairState.setAnchorX(x);
           }
           ValueAxis rangeAxis = getRangeAxis();
           if (rangeAxis != null) {
               double y;
               if (orient == PlotOrientation.VERTICAL) {
                   y = rangeAxis.java2DToValue(anchor.getY(), dataArea,
                           getRangeAxisEdge());
               }
               else {
                   y = rangeAxis.java2DToValue(anchor.getX(), dataArea,
                           getRangeAxisEdge());
               }
               crosshairState.setAnchorY(y);
           }
       }
       crosshairState.setCrosshairX(getDomainCrosshairValue());
       crosshairState.setCrosshairY(getRangeCrosshairValue());
       Shape originalClip = g2.getClip();
       Composite originalComposite = g2.getComposite();

       g2.clip(dataArea);
       g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
               getForegroundAlpha()));

       AxisState domainAxisState = (AxisState) axisStateMap.get(
               getDomainAxis());
       if (domainAxisState == null) {
           if (parentState != null) {
               domainAxisState = (AxisState) parentState.getSharedAxisStates()
                       .get(getDomainAxis());
           }
       }

       AxisState rangeAxisState = (AxisState) axisStateMap.get(getRangeAxis());
       if (rangeAxisState == null) {
           if (parentState != null) {
               rangeAxisState = (AxisState) parentState.getSharedAxisStates()
                       .get(getRangeAxis());
           }
       }
       if (domainAxisState != null) {
           drawDomainTickBands(g2, dataArea, domainAxisState.getTicks());
       }
       if (rangeAxisState != null) {
           drawRangeTickBands(g2, dataArea, rangeAxisState.getTicks());
       }
       if (domainAxisState != null) {
           drawDomainGridlines(g2, dataArea, domainAxisState.getTicks());
           drawZeroDomainBaseline(g2, dataArea);
       }
       if (rangeAxisState != null) {
           drawRangeGridlines(g2, dataArea, rangeAxisState.getTicks());
           drawZeroRangeBaseline(g2, dataArea);
       }

       // draw the markers that are associated with a specific renderer...
       for (int i = 0; i < getRendererCount(); i++) {
           drawDomainMarkers(g2, dataArea, i, Layer.BACKGROUND);
       }
       for (int i = 0; i < getRendererCount(); i++) {
           drawRangeMarkers(g2, dataArea, i, Layer.BACKGROUND);
       }

       // now draw annotations and render data items...
       boolean foundData = false;
       DatasetRenderingOrder order = getDatasetRenderingOrder();
       if (order == DatasetRenderingOrder.FORWARD) {

           // draw background annotations
           int rendererCount = getRendererCount();
           for (int i = 0; i < rendererCount; i++) {
               XYItemRenderer r = getRenderer(i);
               if (r != null) {
                   ValueAxis domainAxis = getDomainAxisForDataset(i);
                   ValueAxis rangeAxis = getRangeAxisForDataset(i);
                   r.drawAnnotations(g2, dataArea, domainAxis, rangeAxis,
                           Layer.BACKGROUND, info);
               }
           }

           // render data items...
           for (int i = 0; i < getDatasetCount(); i++) {
               foundData = render(g2, dataArea, i, info, crosshairState)
                   || foundData;
           }

           // draw foreground annotations
           for (int i = 0; i < rendererCount; i++) {
               XYItemRenderer r = getRenderer(i);
               if (r != null) {
                   ValueAxis domainAxis = getDomainAxisForDataset(i);
                   ValueAxis rangeAxis = getRangeAxisForDataset(i);
                   r.drawAnnotations(g2, dataArea, domainAxis, rangeAxis,
                           Layer.FOREGROUND, info);
               }
           }

       }
       else if (order == DatasetRenderingOrder.REVERSE) {

           // draw background annotations
           int rendererCount = getRendererCount();
           for (int i = rendererCount - 1; i >= 0; i--) {
               XYItemRenderer r = getRenderer(i);
               if (i >= getDatasetCount()) { // we need the dataset to make
                   continue;                 // a link to the axes
               }
               if (r != null) {
                   ValueAxis domainAxis = getDomainAxisForDataset(i);
                   ValueAxis rangeAxis = getRangeAxisForDataset(i);
                   r.drawAnnotations(g2, dataArea, domainAxis, rangeAxis,
                           Layer.BACKGROUND, info);
               }
           }

           for (int i = getDatasetCount() - 1; i >= 0; i--) {
               foundData = render(g2, dataArea, i, info, crosshairState)
                   || foundData;
           }

           // draw foreground annotations
           for (int i = rendererCount - 1; i >= 0; i--) {
               XYItemRenderer r = getRenderer(i);
               if (i >= getDatasetCount()) { // we need the dataset to make
                   continue;                 // a link to the axes
               }
               if (r != null) {
                   ValueAxis domainAxis = getDomainAxisForDataset(i);
                   ValueAxis rangeAxis = getRangeAxisForDataset(i);
                   r.drawAnnotations(g2, dataArea, domainAxis, rangeAxis,
                           Layer.FOREGROUND, info);
               }
           }

       }

       // draw domain crosshair if required...
       int xAxisIndex = crosshairState.getDomainAxisIndex();
       ValueAxis xAxis = getDomainAxis(xAxisIndex);
       RectangleEdge xAxisEdge = getDomainAxisEdge(xAxisIndex);
       
       if (!isDomainCrosshairLockedOnData() && anchor != null) {
           double xx;
           if (orient == PlotOrientation.VERTICAL) {
               xx = xAxis.java2DToValue(anchor.getX(), dataArea, xAxisEdge);
           }
           else {
               xx = xAxis.java2DToValue(anchor.getY(), dataArea, xAxisEdge);
           }
           crosshairState.setCrosshairX(xx);
       }
       setDomainCrosshairValue(crosshairState.getCrosshairX(), false);
       if (isDomainCrosshairVisible()) {
           double x = getDomainCrosshairValue();
           Paint paint = getDomainCrosshairPaint();
           Stroke stroke = getDomainCrosshairStroke();
           drawDomainCrosshair(g2, dataArea, orient, x, xAxis, stroke, paint);
       }

       // draw range crosshair if required...
       int yAxisIndex = crosshairState.getRangeAxisIndex();
       ValueAxis yAxis = getRangeAxis(yAxisIndex);
       RectangleEdge yAxisEdge = getRangeAxisEdge(yAxisIndex);
       
       if (!isRangeCrosshairLockedOnData() && anchor != null) {
           double yy;
           if (orient == PlotOrientation.VERTICAL) {
               yy = yAxis.java2DToValue(anchor.getY(), dataArea, yAxisEdge);
           } else {
               yy = yAxis.java2DToValue(anchor.getX(), dataArea, yAxisEdge);
           }
           crosshairState.setCrosshairY(yy);
       }
       setRangeCrosshairValue(crosshairState.getCrosshairY(), false);
       if (isRangeCrosshairVisible()) {
           double y = getRangeCrosshairValue();
           Paint paint = getRangeCrosshairPaint();
           Stroke stroke = getRangeCrosshairStroke();
           drawRangeCrosshair(g2, dataArea, orient, y, yAxis, stroke, paint);
       }

       if (!foundData) {
           drawNoDataMessage(g2, dataArea);
       }

       for (int i = 0; i < getRendererCount(); i++) {
           drawDomainMarkers(g2, dataArea, i, Layer.FOREGROUND);
       }
       for (int i = 0; i < getRendererCount(); i++) {
           drawRangeMarkers(g2, dataArea, i, Layer.FOREGROUND);
       }

       drawAnnotations(g2, dataArea, info);
       g2.setClip(originalClip);
       g2.setComposite(originalComposite);

       drawOutline(g2, dataArea);

   }
}

