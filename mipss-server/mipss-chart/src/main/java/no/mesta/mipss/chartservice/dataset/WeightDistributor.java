package no.mesta.mipss.chartservice.dataset;

import java.util.HashMap;

public class WeightDistributor {
    private HashMap<String, Integer> weights;
    private double totalWeightCount;
    public WeightDistributor() {
        weights = new HashMap<String, Integer>();
    }
    
	public void add(String subChartName, int w){
		weights.put(subChartName, w);
    }
    public int getWeight(String subChartName){
    	int w = weights.get(subChartName);
    	if (w==0)
    		w=20;
    	return w;
    }


    public void setTotalWeightCount(int totalWeightCount) {
        this.totalWeightCount += totalWeightCount;
    }
    public double getTotalWeightCount(){
        return totalWeightCount;
    }
    public int getSubplotCount() {
        return weights.size();
    }
}
