package no.mesta.mipss.chartservice.values;

import java.util.Comparator;

import no.mesta.mipss.chartservice.dataset.CategoryChartData;

@SuppressWarnings("unchecked")
public class CategoryChartDataComparator implements Comparator{
    
    public int compare(Object o1, Object o2) {
        int c1 = ((CategoryChartData)o1).getSortering();
        int c2 = ((CategoryChartData)o2).getSortering();
        
        if (c1 == c2) {
            return 0;
        } else if (c1 < c2) {
            return -1;
        } else {
            return 1;
        }
    }
    
}