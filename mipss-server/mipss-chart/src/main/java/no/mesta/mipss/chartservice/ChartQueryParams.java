package no.mesta.mipss.chartservice;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;

@SuppressWarnings("serial")
public class ChartQueryParams implements Serializable, Cloneable{
	private Long kjoretoyId;
	private Date fraDate;
	private Date tilDate;
	private Long driftkontraktId;
	private Long rodetypeId = Long.valueOf(15);//standard strø/brøyteroder
	
	private Boolean hastighet=true;
	private Boolean veinummer;
	private Boolean vei=true;
	private Boolean rode=true;
	private Boolean produksjon=true;
	private Boolean sprederdata;
	private Boolean hideId=false;
	
	private Boolean kunProduksjon = false;
	private Boolean kunKontraktKjoretoy = false;
	
	private Kjoretoygruppe kjoretoygruppe;
	
	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public Date getFraDate() {
		return fraDate;
	}

	public void setFraDate(Date fraDate) {
		this.fraDate = fraDate;
	}

	public Date getTilDate() {
		return tilDate;
	}

	public void setTilDate(Date tilDate) {
		this.tilDate = tilDate;
	}

	public void setHastighet(Boolean hastighet) {
		this.hastighet = hastighet;
	}
	
	public void setVeinummer(Boolean veinummer) {
		this.veinummer = veinummer;
	}

	public void setVei(Boolean vei) {
		this.vei = vei;
	}

	public void setRode(Boolean rode) {
		this.rode = rode;
	}

	public void setProduksjon(Boolean produksjon) {
		this.produksjon = produksjon;
	}

	public void setSprederdata(Boolean sprederdata) {
		this.sprederdata = sprederdata;
	}
	
	public void setKunProduksjon(Boolean kunProduksjon){
		this.kunProduksjon = kunProduksjon;
	}
	
	public Boolean getHastighet() {
		return hastighet;
	}

	public Boolean getVeinummer() {
		return this.veinummer;
	}

	public Boolean getVei() {
		return this.vei;
	}

	public Boolean getRode() {
		return this.rode;
	}

	public Boolean getProduksjon() {
		return this.produksjon;
	}

	public Boolean getSprederdata() {
		return this.sprederdata;
	}
	
	public Boolean getKunProduksjon(){
		return this.kunProduksjon;
	}

	public void setDriftkontraktId(Long driftkontraktId) {
		this.driftkontraktId = driftkontraktId;
	}

	public Long getDriftkontraktId() {
		return driftkontraktId;
	}
	public Boolean getHideId(){
		return hideId;
	}
	public void setHideId(Boolean hideId){
		this.hideId = hideId;
	}
	
	public Boolean getKunKontraktKjoretoy(){
		return this.kunKontraktKjoretoy;
	}
	public void setKunKontraktKjoretoy(Boolean kunKontraktKjoretoy){
		this.kunKontraktKjoretoy = kunKontraktKjoretoy;
	}
	@Override
	public ChartQueryParams clone(){
		ChartQueryParams p = new ChartQueryParams();
		p.setDriftkontraktId(getDriftkontraktId());
		p.setFraDate(getFraDate());
		p.setHastighet(getHastighet());
		p.setKjoretoyId(getKjoretoyId());
		p.setKunProduksjon(getKunProduksjon());
		p.setProduksjon(getProduksjon());
		p.setRode(getRode());
		p.setSprederdata(getSprederdata());
		p.setTilDate(getTilDate());
		p.setVei(getVei());
		p.setVeinummer(getVeinummer());
		p.setKunKontraktKjoretoy(getKunKontraktKjoretoy());
		p.setKjoretoygruppe(getKjoretoygruppe());
		return p;
	}

	public void setKjoretoygruppe(Kjoretoygruppe kjoretoygruppe) {
		this.kjoretoygruppe = kjoretoygruppe;
	}

	public Kjoretoygruppe getKjoretoygruppe() {
		return kjoretoygruppe;
	}

	public void setRodetypeId(Long rodetypeId) {
		this.rodetypeId = rodetypeId;
	}

	public Long getRodetypeId() {
		return rodetypeId;
	}
}
