package no.mesta.mipss.chartservice.dataset;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import no.mesta.mipss.chartservice.values.ChartSequence;
import no.mesta.mipss.chartservice.values.ChartSeries;
import no.mesta.mipss.chartservice.values.SprederData;
import no.mesta.mipss.common.PropertyResourceBundleUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

@SuppressWarnings("unchecked")
public class DatasetFactory {
	private static PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("chartText");
    private static Logger logger = LoggerFactory.getLogger("no.mesta.mipps.rapporter.charts.dataset.DatasetFactory");
    private static String tenning_pa = resources.getGuiString("hastighet.tenning.paa");
    private static String tenning_av = resources.getGuiString("hastighet.tenning.av");
    private DatasetFactory() {
    }
    

    /**
     * Konverter ChartSeries objektet til dataset som representerer hastighet over tid. 
     * @param serie
     * @return
     */
	public static XYSeriesCollection createHastighetDataset(ChartSeries serie, ArrayList ignitionSetting){
        
    	XYSeriesCollection dataset = new XYSeriesCollection();
        for (int j=0;j<serie.size();j++){
            ChartSequence seq = (ChartSequence)serie.get(j);
            XYSeries fst = new XYSeries("");
            
            for (int i=0; i <seq.size();i++){
                boolean previg=false;
                if (i!=0){
                    previg=((XYChartData)seq.get(i-1)).getIgnition();
                }
                XYChartData g = (XYChartData)seq.get(i);
                boolean ig = g.getIgnition();
                if (!ig==previg){
                    //Marker ignitionMark =   new ValueMarker(s.getFirstMillisecond());
                    Marker ignitionMark =   new ValueMarker(g.getDomain().getTime());
                    ignitionMark.setPaint(Color.red);
                    ignitionMark.setLabel(ig==true?tenning_pa:tenning_av);
                    ignitionMark.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
                    ignitionMark.setLabelTextAnchor(TextAnchor.TOP_LEFT);
                    ignitionSetting.add(ignitionMark);
                }
                /*if (g.isLogging()){
                    Marker logMark = new ValueMarker(g.getDomain().getTime());
                    logMark.setPaint(Color.green);
                    logMark.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
                    ignitionSetting.add(logMark);
                }*/
                fst.add(g.getDomain().getTime(), g.getRange());
            }
            
            //dataset.addSeries(data);
            dataset.addSeries(fst);
        }
        
        
        return dataset;
    }
    
    /**
     * Lag et dataset som viser produksjon
     * @param data
     * @param categories
     * @return
     */
    public static IntervalXYDataset createCategoryDataset(ChartSeries data, ArrayList categories){
        XYIntervalSeriesCollection  collection = new XYIntervalSeriesCollection();
        for (int j=0;j<data.size();j++){
            ChartSequence seq = (ChartSequence)data.get(j);
            for (int k=0;k<seq.size();k++){
                CategoryChartData prodData = (CategoryChartData) seq.get(k);
                int pos = 0;
                String pda = prodData.getCategory();
                if (pda!=null){
                    if (!categories.contains(pda)){
                        categories.add(pda);
                    }
                    pos = categories.indexOf(pda);
                    XYIntervalSeries serie = new XYIntervalSeries(prodData.getCategory());
                    int series = collection.getSeriesCount();
                    for (int i=0;i<series;i++){
                        if (collection.getSeries(i).getKey().equals(serie.getKey())){
                            serie = collection.getSeries(i);
                        }
                    }
                    Date fra = prodData.getStartDate();
                    Date til = prodData.getEndDate();
                    addItem(serie, fra.getTime(), til.getTime(), pos);
                    collection.addSeries(serie);
                }
            }
        }
        //logger.debug("dataset size:"+collection.getSeriesCount());
        return collection;
    }
    
  
    /**
     * Lag et dataset som viser strøbredde
     * @param serie
     * @return
     */
    public static XYSeriesCollection createBreddeDataset(ChartSeries serie){
    	XYSeriesCollection dataset = new XYSeriesCollection();
        Calendar c = Calendar.getInstance();
        for (int i =0;i<serie.size();i++){
            ChartSequence seq = (ChartSequence)serie.get(i);
            XYSeries data = new XYSeries("");
            for (int j=0;j<seq.size();j++){
                SprederData d = (SprederData)seq.get(j);
                c.setTime(d.getTidFra());
                data.add(c.getTimeInMillis(), d.getBredde());
                if (d.getTidTil()!=null)
                if (j+1==seq.size()){
                    c.setTime(d.getTidTil());
                    data.add(c.getTimeInMillis(), d.getBredde());
                }
            }
            dataset.addSeries(data);
        }
        return dataset;
    }
    public static XYSeriesCollection createDoseringDataset(ChartSeries serie){
    	XYSeriesCollection dataset = new XYSeriesCollection();
        Calendar c = Calendar.getInstance();
        for (int i =0;i<serie.size();i++){
            ChartSequence seq = (ChartSequence)serie.get(i);
            //TimeSeries data = new TimeSeries("", Second.class);
            XYSeries data = new XYSeries("");
            
            for (int j=0;j<seq.size();j++){
                SprederData d = (SprederData)seq.get(j);
                c.setTime(d.getTidFra());
                
                data.add(c.getTimeInMillis(), d.getDosering());
                if (d.getTidTil()!=null)
                if (j+1==seq.size()){
                    c.setTime(d.getTidTil());
                    data.add(c.getTimeInMillis(), d.getDosering());
                }
            }
            dataset.addSeries(data);
        }
        return dataset;
    }
    public static XYSeriesCollection createSymmetriDataset(ChartSeries serie, ArrayList categories){
    	XYSeriesCollection dataset = new XYSeriesCollection();
        Calendar c = Calendar.getInstance();
        //logger.debug("serie count:"+serie.getArrayList().size());
        for (int i =0;i<serie.size();i++){
            ChartSequence seq = (ChartSequence)serie.get(i);
            XYSeries data = new XYSeries("");
            //logger.debug("sequence count:"+seq.getArrayList().size());    
            for (int j=0;j<seq.size();j++){
                CategoryChartData d = (CategoryChartData)seq.get(j);
                int pos = 0;
                if (!categories.contains(d.getCategory())){
                    categories.add(d.getCategory());
                }
                pos = categories.indexOf(d.getCategory());
                c.setTime(d.getStartDate());
                
                data.add(c.getTimeInMillis(), pos);
                if (d.getEndDate()!=null)
                if (j+1==seq.size()){
                    c.setTime(d.getEndDate());    
                    data.add(c.getTimeInMillis(), pos);
                }
            }
            dataset.addSeries(data);
        }
        return dataset;
    }
    
   
    //FastXYDataset 
    public static XYSeriesCollection createDoseringLakeDataset(ChartSeries serie){
    	XYSeriesCollection dataset = new XYSeriesCollection();
        Calendar c = Calendar.getInstance();
        for (int i =0;i<serie.size();i++){
            ChartSequence seq = (ChartSequence)serie.get(i);
            //TimeSeries data = new TimeSeries("", Second.class);
            XYSeries data = new XYSeries("");
            for (int j=0;j<seq.size();j++){
                SprederData d = (SprederData)seq.get(j);
                //c.setTime(d.getDomain());
                c.setTime(d.getTidFra());
                /*Day day = new Day(c.getTime());
                Hour h = new Hour(c.get(Calendar.HOUR_OF_DAY), day);
                Minute m = new Minute(c.get(Calendar.MINUTE), h);
                Second s = new Second(c.get(Calendar.SECOND), m);
                data.add(s, d.getDoseringLake());
               */ 
                data.add(c.getTimeInMillis(), d.getDoseringLake());
                if (d.getTidTil()!=null)
                if (j+1==seq.size()){
                    c.setTime(d.getTidTil());
                    /*day = new Day(c.getTime());
                    h = new Hour(c.get(Calendar.HOUR_OF_DAY), day);
                    m = new Minute(c.get(Calendar.MINUTE), h);
                    s = new Second(c.get(Calendar.SECOND), m);
                    data.add(s, d.getDoseringLake());
                    */
                    data.add(c.getTimeInMillis(), d.getDoseringLake());
                }
            }
            dataset.addSeries(data);
        }
        
        return dataset;
    }
    
    public static IntervalXYDataset[] createKjoretoyDataset(ChartSeries data, ArrayList categories, HashMap produksjonsTyper, ArrayList[] domainMarkers, ArrayList labelColors){
        logger.debug("createKjoretoyDataset() start");
        long start = System.currentTimeMillis();
        XYIntervalSeriesCollection[]  datasetArray = new XYIntervalSeriesCollection[data.size()];
        
        for (int j=0;j<data.size();j++){
            XYIntervalSeriesCollection  collection = new XYIntervalSeriesCollection();
            ChartSequence seq = (ChartSequence)data.get(j);
            //logger.debug("\tsequence size:"+seq.getArrayList().size());
            for (int k=0;k<seq.size();k++){
                KjoretoyChartData prodData = (KjoretoyChartData) seq.get(k);
                String pda = prodData.getProduksjonType();
                String enhetId = prodData.getCategory();
                if (enhetId!=null){
                    if (!categories.contains(enhetId)){
                        categories.add(enhetId);
                        labelColors.add(""+prodData.isPaaKontrakt());
                    }
                }
                
//                if (pda!=null){
                    produksjonsTyper.put(pda, prodData.getFarge());
                    XYIntervalSeries serie = new XYIntervalSeries(prodData.getProduksjonType());
                    
                    int series = collection.getSeriesCount();
                    for (int i=0;i<series;i++){
                        if (collection.getSeries(i).getKey().equals(serie.getKey())){
                            serie = collection.getSeries(i);
                        }
                    }
                    Date fra = prodData.getStartDate();
                    Date til = prodData.getEndDate();
                    addItem(serie, fra.getTime(), til.getTime(), 0);      
                    collection.addSeries(serie);
  //              }
  
                /*boolean marker = prodData.isMarkering();
                if (!marker){
                    markStart = prodData.getStartDate();
                    IntervalMarker mark = new IntervalMarker(markStart.getTime(), prodData.getEndDate().getTime());// prodData.getStartDate().getTime());
                    mark.setAlpha(0.25f);
                    mark.setPaint(Color.blue);
                    if (domainMarkers[pos]==null){
                        ArrayList markerList = new ArrayList();
                        markerList.add(mark);
                        domainMarkers[pos]=markerList;
                    }else{
                        ArrayList markerList = domainMarkers[pos];//(ArrayList)domainMarkers.get(pos);
                        markerList.add(mark);
                    }
                }*/
                /*
                if (!prevMark && marker){   
                    markStart = prodData.getStartDate();
                }
                if(prevMark && !marker){
                    IntervalMarker mark = new IntervalMarker(markStart.getTime(), prevData.getEndDate().getTime());// prodData.getStartDate().getTime());
                    mark.setAlpha(0.25f);
                    mark.setPaint(Color.blue);
                    if (domainMarkers[pos]==null){
                        ArrayList markerList = new ArrayList();
                        markerList.add(mark);
                        domainMarkers[pos]=markerList;
                    }else{
                        ArrayList markerList = domainMarkers[pos];//(ArrayList)domainMarkers.get(pos);
                        markerList.add(mark);
                    }
                }
                if (k==seq.getArrayList().size()-1){
                    
                        
                }*/
                //prevMark = marker;
                //prevData = prodData;
            }
            datasetArray[j] = collection;
        }
        //logger.debug("dataset size:"+collection.getSeriesCount());
        logger.debug("createKjoretoyDataset() done.. runtime:"+(System.currentTimeMillis()-start));
        return datasetArray;
    }
    
    /**
     * Legg til en registrering i serien. Denne metoden definerer også bar-høyden på linjene i grafen
     * 
     * @param xyintervalseries
     * @param fra
     * @param til
     * @param i
     */
    private static void addItem(XYIntervalSeries xyintervalseries, double fra, double til, int i){
        double ymin = i-0.15;
        double ymax = i+0.15;
        xyintervalseries.add(fra, fra, til, i, ymin, ymax);
    }

    private static void addOrUpdateSeries(String kategori, XYMipssLoggChartData prodpunktData, HashMap<String, MipssLoggSeries> currentSerie){
    	if (currentSerie.get(kategori) != null) {
			currentSerie.get(kategori).setTilDato(prodpunktData.getTidspunkt());
		}
		else {
			currentSerie.put(kategori, new MipssLoggSeries(kategori, prodpunktData.getTidspunkt()));
		}
		
    }
    
    private static void endSeries(String kategori, XYMipssLoggChartData prodpunktData, 
    								HashMap<String, MipssLoggSeries> currentSerie, 
    								HashMap<String, List<MipssLoggSeries>> allSeries){
    	if (currentSerie.get(kategori) != null) {
			allSeries.get(kategori).add(currentSerie.get(kategori));
			currentSerie.put(kategori, null);
		}
    }
    
    private static void endAllOpenSeries(
			HashMap<String, MipssLoggSeries> currentSerie,
			HashMap<String, List<MipssLoggSeries>> allSeries,
			ArrayList<String> kategori) {
		
    	for (int i = 0; i < kategori.size(); i++) {
    		if(currentSerie.get(kategori.get(i))!= null){
    			allSeries.get(kategori.get(i)).add(currentSerie.get(kategori.get(i)));
    		}	
		}
	}
    
    private static void evaluateSeries(boolean state, String cat, XYMipssLoggChartData prodpunktData, HashMap<String, MipssLoggSeries> currentSerie, HashMap<String, List<MipssLoggSeries>> allSeries ){
    	if (state) {			
			addOrUpdateSeries(cat, prodpunktData, currentSerie);	
		} 
		else {				
			endSeries(cat, prodpunktData, currentSerie, allSeries);							
		}
    }
    
    /**
     * Opprett et datasett som inneholder prodpunktene
     * @param loggData
     * @param categories
     * @return IntervalXYDataset
     */
    public static IntervalXYDataset createMipssLoggDataset(List<XYMipssLoggChartData> loggData, ArrayList<String> categories) {

    	XYIntervalSeriesCollection  collection = new XYIntervalSeriesCollection();
    	
    	HashMap<String, List<MipssLoggSeries>> allSeries = new HashMap<String, List<MipssLoggSeries>>();
		HashMap<String, MipssLoggSeries> currentSerie = new HashMap<String, MipssLoggSeries>();

    	for (String cat:categories){
    		List<MipssLoggSeries> s = new ArrayList<MipssLoggSeries>();
    		allSeries.put(cat, s);
    	}
    	
    	collection.addSeries(new XYIntervalSeries("IGN"));
    	collection.addSeries(new XYIntervalSeries("OTHER"));

		for (int i = 0; i < loggData.size(); i++) {
			XYMipssLoggChartData prodpunktData = loggData.get(i);
						
			evaluateSeries(prodpunktData.isIgn(), "IGN", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isLog(), "LOG", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isDi2(), "DI2", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isDi3(), "DI3", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isDi4(), "DI4", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isAn2(), "AN2", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isAn3(), "AN3", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isAn4(), "AN4", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isSpreder(), "Spreder", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isEltrip(), "Eltrip", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isUkjentSprederdata(), "Ukjent NMEA", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isKontFriksjon(), "Kont.friksj.", prodpunktData, currentSerie, allSeries);
			
			evaluateSeries(prodpunktData.isInspeksjoner(), "Inspeksjoner", prodpunktData, currentSerie, allSeries);	
			
			
			//Avslutter alle åpne serier
			if(i == loggData.size() - 1)
				endAllOpenSeries(currentSerie, allSeries, categories);
		}

    	//Legger alle serier til
    	for (int i = 0; i < allSeries.size(); i++) {
    		
    		List<MipssLoggSeries> prodList = allSeries.get(categories.get(i));
    		for (int j = 0; j < prodList.size(); j++) {
    			
    			XYIntervalSeries serie = null;
    			MipssLoggSeries prodSerie = prodList.get(j);
    			int series = collection.getSeriesCount();
    			
//    			for (int k=0;k<series;k++){
//    				if (collection.getSeries(k).getKey().equals(prodSerie.getKey()))
//                    	serie = collection.getSeries(k);                  
//                }
    			
    			for (int k=0;k<series;k++){
    				if (prodSerie.getKey().equals("IGN")){
    					serie = collection.getSeries(0);  
    				} else {
    					serie = collection.getSeries(1);
    				}
                    	                
                }

    			addItem(serie, prodSerie.getFraDato().getTime(), prodSerie.getTilDato().getTime(), i);
    			collection.addSeries(serie);
    		}		            
    	}
    	return collection;
    }


    /**
     * 
     * @param loggData
     * @return En seriesCollection som inneholder alle CEN-punkter
     */
	public static XYSeriesCollection createCenDatset(List<XYMipssLoggChartData> loggData) {
		XYSeriesCollection  collection = new XYSeriesCollection();
		XYSeries serie = new XYSeries("CEN");
		for (int i = 0; i < loggData.size(); i++) {
			XYMipssLoggChartData prodpunktData = loggData.get(i);			
			
			if(prodpunktData.getCen() != null){
		        serie.add(prodpunktData.getTidspunkt().getTime(), 0);
		        
			}			
		}
		if (!serie.isEmpty()) 
			collection.addSeries(serie);
		return collection;
	}
	
	/**
	 * 
	 * @param loggData
	 * @return En seriesCollection som inneholder alle GPS- og Livstegin-punkter.
	 */
	public static XYSeriesCollection createGpsDatset(List<XYMipssLoggChartData> loggData) {
		XYSeriesCollection  collection = new XYSeriesCollection();
		XYSeries gpsSerie = new XYSeries("GPS");
		XYSeries livSerie = new XYSeries("LIV");
		for (int i = 0; i < loggData.size(); i++) {
			XYMipssLoggChartData prodpunktData = loggData.get(i);			
			
			if(prodpunktData.isLivstegn()){
				livSerie.add(prodpunktData.getTidspunkt().getTime(), 1);		        
			}else if(prodpunktData.getBreddegrad() != null && prodpunktData.getLengdegrad() != null){
				gpsSerie.add(prodpunktData.getTidspunkt().getTime(), 0);
			}			
		}
		if (!livSerie.isEmpty()) 
			collection.addSeries(livSerie);
		if (!gpsSerie.isEmpty()) 
			collection.addSeries(gpsSerie);
		return collection;
	}


	public static XYSeriesCollection createHastighetloggDataset(List<XYMipssLoggChartData> loggData, ArrayList ignitionSetting) {
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries hastighetSerie = new XYSeries("Km/t");
		boolean previg = false;
        for (int j=0; j < loggData.size(); j++){
        	XYMipssLoggChartData prodpunktData = loggData.get(j);	
        	
        	if (prodpunktData.isIgn()) {
        		Double hastighet = prodpunktData.getHastighet() != null ? prodpunktData.getHastighet() : 0.0;
        		hastighetSerie.add(prodpunktData.getTidspunkt().getTime(), hastighet);
			} else{
				hastighetSerie.add(prodpunktData.getTidspunkt().getTime(), prodpunktData.getHastighet());
			}
        	  	
        	if (j != 0) {
				previg = loggData.get(j-1).isIgn();
			}
        	boolean ign = prodpunktData.isIgn();
        	if (!ign == previg) {
        		Marker ignitionMark = new ValueMarker(prodpunktData.getTidspunkt().getTime());
                ignitionMark.setPaint(Color.red);
                ignitionMark.setLabel(ign==true?tenning_pa:tenning_av);
                ignitionMark.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
                ignitionMark.setLabelTextAnchor(TextAnchor.TOP_LEFT);
                ignitionSetting.add(ignitionMark);
			}
        }
        
        if (!hastighetSerie.isEmpty()) {
			dataset.addSeries(hastighetSerie);
		}
        
        return dataset;
	}
}
