package no.mesta.mipss.chartservice.dataset;

import java.util.Date;

public class CategoryChartData{

    private Date startDate;
    private Date endDate;
    private String category;
    private int sortering;
    
    public CategoryChartData() {
    }

    public CategoryChartData(Date startDate, Date endDate, String category, int sortering){
        this.startDate = startDate;
        this.endDate = endDate;
        this.category = category;
        this.sortering = sortering;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setSortering(Integer sortering) {
        this.sortering = sortering;
    }

    public int getSortering() {
        return sortering;
    }
}
