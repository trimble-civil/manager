package no.mesta.mipss.chartservice.dataset;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.util.DateFormatter;

/**
 * Klasse som inneholder informasjon om hvert prodpunkt.
 * Diverse boolean-verdier blir satt basert på digi-verdien fra tabellen prodpunkt.
 * @author larnym
 */

public class XYMipssLoggChartData implements Serializable{
	private static final long serialVersionUID = 3786423260777498200L;
	private Date tidspunkt;
    private Double hastighet;
    private byte [] digiByte;
    private String cen;
    private String breddegrad;
    private String lengdegrad;
    
    //Signaler
    private boolean gps;
    private boolean livstegn;
    private boolean ign;
    private boolean log;
    private boolean di2;
    private boolean di3;
    private boolean di4;
    private boolean an2;
    private boolean an3;
    private boolean an4;
    private boolean spreder;
    private boolean eltrip;
    private boolean ukjentSprederdata;
    private boolean kontFriksjon;
    private boolean inspeksjoner;

    //
    private String hexDigi;
        
    public XYMipssLoggChartData() {
    }

    public void setTidspunkt(Date tidspunkt) {
    	this.tidspunkt = DateFormatter.convertFromGMTtoCET(tidspunkt);
    }

    public Date getTidspunkt() {
        return tidspunkt;
    }

    public Double getHastighet() {
		return hastighet;
	}

	public void setHastighet(Double hastighet) {
		this.hastighet = hastighet;
	}

	public void setDigiByte(byte[] digi){
    	String s = "";
    	for (Byte b:digi){
    		s+=pad(Integer.toHexString(0xFF&b));
    	}
    	Integer dig = Integer.parseInt(s, 16);
    
    	if(s.equals("0000")){
    		setLivstegn(true);
    	}else
    		setLivstegn(false);
    		
    	setIgn((dig&0x0001) == 0x0001 ? true: false);
    	setLog((dig&0x0002) == 0x0002 ? true: false);
    	setDi2((dig&0x0004) == 0x0004 ? true: false);
    	setDi3((dig&0x0008) == 0x0008 ? true: false);
    	setDi4((dig&0x0010) == 0x0010 ? true: false);
    	setAn2((dig&0x0020) == 0x0020 ? true: false);
    	setAn3((dig&0x0040) == 0x0040 ? true: false);
    	setAn4((dig&0x0080) == 0x0080 ? true: false);
    	setSpreder((dig&0x0100) == 0x0100 ? true: false);
    	setEltrip((dig&0x0200) == 0x0200 ? true: false);
    	setUkjentSprederdata((dig&0x0400) == 0x0400 ? true: false);
    	setKontFriksjon((dig&0x0800) == 0x0800 ? true: false);    
    	setInspeksjoner((dig&0x1000) == 0x1000 ? true: false);
    	
    	setHexDigi(s);
    }
	
	private void setHexDigi(String digi){
		hexDigi = digi;
	}
	
	public String getHexDigi(){
		return hexDigi;
	}
    
    private String pad(String hx){
		int pads = 2-hx.length();
		String padded = "";
		for (int i=0;i<pads;i++){
			padded+="0";
		}
		padded+=hx;
		return padded;
		
	}

	public byte[] getDigiByte() {
		return digiByte;
	}

	public String getCen() {
		return cen;
	}

	public void setCen(String cen) {
		this.cen = cen;
	}
	

	public String getBreddegrad() {
		return breddegrad;
	}

	public void setBreddegrad(String breddegrad) {
		if(hexDigi.equals("0000")){
			this.breddegrad = "";
		} else {
			this.breddegrad = breddegrad;
		}		
	}

	public String getLengdegrad() {
		return lengdegrad;
	}

	public void setLengdegrad(String lengdegrad) {
		if(hexDigi.equals("0000")){
			this.lengdegrad = "";
		} else {
			this.lengdegrad = lengdegrad;
		}	
	}

	public boolean isGps() {
		return gps;
	}

	public void setGps(boolean gps) {
		this.gps = gps;
	}

	public boolean isLivstegn() {
		return livstegn;
	}

	public void setLivstegn(boolean livstegn) {
		this.livstegn = livstegn;
	}

	public boolean isIgn() {
		return ign;
	}

	public void setIgn(boolean ign) {
		this.ign = ign;
	}

	public boolean isLog() {
		return log;
	}

	public void setLog(boolean log) {
		this.log = log;
	}

	public boolean isDi2() {
		return di2;
	}

	public void setDi2(boolean di2) {
		this.di2 = di2;
	}

	public boolean isDi3() {
		return di3;
	}

	public void setDi3(boolean di3) {
		this.di3 = di3;
	}

	public boolean isDi4() {
		return di4;
	}

	public void setDi4(boolean di4) {
		this.di4 = di4;
	}

	public boolean isAn2() {
		return an2;
	}

	public void setAn2(boolean an2) {
		this.an2 = an2;
	}

	public boolean isAn3() {
		return an3;
	}

	public void setAn3(boolean an3) {
		this.an3 = an3;
	}

	public boolean isAn4() {
		return an4;
	}

	public void setAn4(boolean an4) {
		this.an4 = an4;
	}

	public boolean isSpreder() {
		return spreder;
	}

	public void setSpreder(boolean spreder) {
		this.spreder = spreder;
	}

	public boolean isEltrip() {
		return eltrip;
	}

	public void setEltrip(boolean eltrip) {
		this.eltrip = eltrip;
	}

	public boolean isUkjentSprederdata() {
		return ukjentSprederdata;
	}

	public void setUkjentSprederdata(boolean ukjentSprederdata) {
		this.ukjentSprederdata = ukjentSprederdata;
	}

	public boolean isKontFriksjon() {
		return kontFriksjon;
	}

	public void setKontFriksjon(boolean kontFriksjon) {
		this.kontFriksjon = kontFriksjon;
	}

	public boolean isInspeksjoner() {
		return inspeksjoner;
	}

	public void setInspeksjoner(boolean inspeksjoner) {
		this.inspeksjoner = inspeksjoner;
	}

}
