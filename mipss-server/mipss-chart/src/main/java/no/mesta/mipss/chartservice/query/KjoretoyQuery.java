package no.mesta.mipss.chartservice.query;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.chartservice.dataset.KjoretoyChartData;
import no.mesta.mipss.chartservice.values.CategoryChartDataComparator;
import no.mesta.mipss.chartservice.values.CategoryComparator;
import no.mesta.mipss.chartservice.values.ChartSequence;
import no.mesta.mipss.chartservice.values.ChartSeries;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.query.NativeQueryWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KjoretoyQuery{
    private Query query ;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
	private EntityManager em;

    
    public KjoretoyQuery(Long kontraktId, Date tidFra, Date tidTil, boolean kunProduksjon, boolean kunKontraktKjoretoy, EntityManager em, Kjoretoygruppe kjoretoygruppe) {
    	this.em = em;
		
    	String fraTid = MipssDateFormatter.formatDate(tidFra, MipssDateFormatter.DATE_TIME_FORMAT);
        String tilTid = MipssDateFormatter.formatDate(tidTil, MipssDateFormatter.DATE_TIME_FORMAT);
        int prod = kunProduksjon?1:0;
        
        
        StringBuilder q = new StringBuilder("select kjoretoy_info, ");
		q.append("to_char(tid_fra, 'dd.MM.yyyy hh24:mi:ss'), ");
		q.append("to_char(tid_til, 'dd.MM.yyyy hh24:mi:ss'), ");
		q.append("prod_type, ");
		q.append("farge, ");
		q.append("paa_kontrakt_flagg ");
		q.append("from table(rapport_funksjoner.lagkjoretoydataset(?1, to_date(?2, 'dd.mm.yyyy hh24:mi'),to_date(?3, 'dd.mm.yyyy hh24:mi'), ?4, ?5)) ");
		if (kunKontraktKjoretoy){
        	q.append("where paa_kontrakt_flagg=1 ");
        }
		q.append("order by kjoretoy_info, tid_fra, tid_til ");
        
		
		query = em.createNativeQuery(q.toString());
		query.setParameter(1, kontraktId);
		query.setParameter(2, fraTid);
		query.setParameter(3, tilTid);
		if (kjoretoygruppe!=null)
			query.setParameter(4, kjoretoygruppe.getId());
		else
			query.setParameter(4, null);
		
		query.setParameter(5, prod);
    }
    

    @SuppressWarnings("unchecked")
	public ChartSeries consumeResults(boolean hideId){
        final ChartSeries seriePaaKontrakt =new ChartSeries();
        final ChartSeries serieIkkePaaKontrakt = new ChartSeries();
        
        ChartSequence seq = new ChartSequence();
        
        int rowCount = 1;
        List<KjoretoyChartData> wrappedList = new NativeQueryWrapper<KjoretoyChartData>(query, KjoretoyChartData.class, new Class[]{
        	String.class, Date.class, Date.class, 
        	String.class, String.class, String.class}, 
        	"category", "startDate", "endDate", 
        	"produksjonType", "farge", "paaKontraktString").getWrappedList();
        
        //erstatter regnummer/eksterntnavn med et løpenummer
        if (hideId){
        	Map<String, Integer> replacedIds = new HashMap<String, Integer>();
        	int count = 0;
        	for (KjoretoyChartData data:wrappedList){
        		Integer id = replacedIds.get(data.getCategory());
        		if (id==null){
        			id = Integer.valueOf(count);
        			replacedIds.put(data.getCategory(), id);
        			count++;
        		}
        		data.setCategory(String.valueOf(id));
        	}
        }
        
        String prevEnhetId = "";
        boolean prevPaaKontrakt = false;
        for (KjoretoyChartData data:wrappedList){
        	if (rowCount==1){
        		prevEnhetId = data.getCategory();
        		
        	}
        	if (!prevEnhetId.equals(data.getCategory())){
        		if (prevPaaKontrakt){
        			seriePaaKontrakt.add(seq);
        		}else
        			serieIkkePaaKontrakt.add(seq);
        		seq = new ChartSequence();
        	}
        	seq.add(data);
        	prevEnhetId = data.getCategory();
        	prevPaaKontrakt = data.isPaaKontrakt();
        	
        	rowCount++;
        }
        
        if (rowCount>1){
        	if (prevPaaKontrakt)
        		seriePaaKontrakt.add(seq);
        	else
        		serieIkkePaaKontrakt.add(seq);
        	
        	Collections.sort(seq, new CategoryChartDataComparator());
        }
        
        CategoryComparator comp = new CategoryComparator();
        Collections.sort(seriePaaKontrakt, comp);
        Collections.sort(serieIkkePaaKontrakt, comp);
        seriePaaKontrakt.addAll(serieIkkePaaKontrakt);
        
        return seriePaaKontrakt;
    }
    
    
    
    @SuppressWarnings("unused")
	private ChartSeries getDummyData() throws ParseException {
        final ChartSeries serie =new ChartSeries();
        ChartSequence seq = new ChartSequence();
        
        DateFormat df = new SimpleDateFormat("yyyy-MM.dd HH:mm:ss");
        
        KjoretoyChartData data = null;
        data = new KjoretoyChartData(df.parse("2008-01.01 01:00:00"), df.parse("2008-01.01 01:00:15"), "TE01234", 0, "Strøing", "0x0000ff", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:00:45"), df.parse("2008-01.01 01:01:35"), "TE01234", 0, "Strøing", "0x0000ff", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:01:37"), df.parse("2008-01.01 01:04:03"), "TE01234", 0, "Strøing", "0x0000ff", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:04:37"), df.parse("2008-01.01 01:15:08"), "TE01234", 0, "Tomkjøring", "0xff0000", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:15:08"), df.parse("2008-01.01 01:35:01"), "TE01234", 0, "Strøing + Brøyting", "0xffff00", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:38:08"), df.parse("2008-01.01 01:59:49"), "TE01234", 0, "Strøing + Brøyting", "0xffff00", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:59:49"), df.parse("2008-01.01 02:05:39"), "TE01234", 0, "Sideskjær", "0xff00ff", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:06:01"), df.parse("2008-01.01 02:10:19"), "TE01234", 0, "Sideskjær", "0xff00ff", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:15:45"), df.parse("2008-01.01 02:24:39"), "TE01234", 0, "Sideskjær", "0xff00ff", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:24:39"), df.parse("2008-01.01 02:30:55"), "TE01234", 0, "Tomkjøring", "0xff0000", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:33:54"), df.parse("2008-01.01 02:37:32"), "TE01234", 0, "Tomkjøring", "0xff0000", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:37:33"), df.parse("2008-01.01 03:34:23"), "TE01234", 0, "Strøing", "0x0000ff", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 03:37:31"), df.parse("2008-01.01 03:56:44"), "TE01234", 0, "Strøing", "0x0000ff", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 03:56:44"), df.parse("2008-01.01 04:00:00"), "TE01234", 0, "Tomkjøring", "0x0000ff", false); seq.add(data);
        serie.add(seq);
        
        seq = new ChartSequence();
        data = new KjoretoyChartData(df.parse("2008-01.01 01:00:00"), df.parse("2008-01.01 01:10:15"), "ST56789", 1, "Tomkjøring", "0xff0000", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:10:20"), df.parse("2008-01.01 01:14:42"), "ST56789", 1, "Tomkjøring", "0xff0000", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:15:00"), df.parse("2008-01.01 01:20:34"), "ST56789", 1, "Brøyting", "0x00ff00", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:20:40"), df.parse("2008-01.01 01:25:12"), "ST56789", 1, "Brøyting", "0x00ff00", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:26:23"), df.parse("2008-01.01 01:33:34"), "ST56789", 1, "Brøyting", "0x00ff00", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:33:36"), df.parse("2008-01.01 01:42:12"), "ST56789", 1, "Brøyting", "0x00ff00", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 01:42:12"), df.parse("2008-01.01 02:23:34"), "ST56789", 1, "Tomkjøring", "0xff0000", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:24:42"), df.parse("2008-01.01 02:33:32"), "ST56789", 1, "Tomkjøring", "0xff0000", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:33:37"), df.parse("2008-01.01 02:44:37"), "ST56789", 1, "Tomkjøring", "0xff0000", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:45:34"), df.parse("2008-01.01 02:51:54"), "ST56789", 1, "Brøyting", "0x00ff00", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 02:51:54"), df.parse("2008-01.01 03:01:23"), "ST56789", 1, "Brøyting", "0x00ff00", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 03:02:13"), df.parse("2008-01.01 03:12:22"), "ST56789", 1, "Brøyting", "0x00ff00", true); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 03:12:22"), df.parse("2008-01.01 03:44:25"), "ST56789", 1, "Tomkjøring", "0xff0000", false); seq.add(data);
        data = new KjoretoyChartData(df.parse("2008-01.01 03:44:25"), df.parse("2008-01.01 04:00:00"), "ST56789", 1, "Strøing", "0x0000ff", true); seq.add(data);
        serie.add(seq);
        
        return serie;
    }
}
