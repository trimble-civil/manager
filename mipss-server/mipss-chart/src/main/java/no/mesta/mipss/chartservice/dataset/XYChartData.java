package no.mesta.mipss.chartservice.dataset;

import java.util.Date;


public class XYChartData{
    private Date domain;
    private double range;
    private boolean ignition;
    private boolean logging;
    
    public XYChartData() {
    }

    public void setDomain(Date domain) {
        this.domain = domain;
    }

    public Date getDomain() {
        return domain;
    }

    public void setRange(Double range) {
        this.range = range;
    }

    public double getRange() {
        return range;
    }

    public void setIgnition(Boolean ignition) {
        this.ignition = ignition;
    }

    public boolean getIgnition() {
        return ignition;
    }
    public void setDigiByte(byte[] digi){
    	String s = "";
    	for (Byte b:digi){
    		s+=pad(Integer.toHexString(0xFF&b));
    	}
    	Integer dig = Integer.parseInt(s, 16);
    	setIgnition((dig&0x0001)==0?false:true);
    	
    	//setDigi(Short.valueOf(s, 16));
    }
    private String pad(String hx){
		int pads = 2-hx.length();
		String padded = "";
		for (int i=0;i<pads;i++){
			padded+="0";
		}
		padded+=hx;
		return padded;
		
	}
    
    public static void main(String[] args) {
		String d = "0081";
		System.out.println(Short.valueOf(d, 16));
	}
    public void setDigi(Short digi){
    	setIgnition((digi&0x0001)==0?false:true);//logisk AND med 1 for å finne ut om LSB=0 eller 1
    }

    public void setLogging(boolean logging) {
        this.logging = logging;
    }

    public boolean isLogging() {
        return logging;
    }
}
