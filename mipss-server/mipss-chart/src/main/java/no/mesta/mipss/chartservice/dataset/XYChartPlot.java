package no.mesta.mipss.chartservice.dataset;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.FastXyPlot;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * @author harkul, larnym
 */
@SuppressWarnings("serial")
public class XYChartPlot extends FastXyPlot{
    
    private AbstractXYItemRenderer renderer;
    private int seriesCount;
    
    
    public XYChartPlot(	XYDataset dataset, ValueAxis timeAxis, AbstractXYItemRenderer renderer, String[] categoryItemsLabel, String rangeLabel) {
        super(dataset, timeAxis, new SymbolAxis(rangeLabel, categoryItemsLabel), renderer); 
        //SymbolAxis symbolaxis = new SymbolAxis(rangeLabel, categoryItemsLabel);
        
        seriesCount = dataset.getSeriesCount();
        
        /*for (int i=0;i<seriesCount;i++){
            renderer.setSeriesPaint(i, DEFAULT_COLOR);
            renderer.setSeriesShape(i, new Ellipse2D.Double(-1, -1, 1, 1));
        }*/
        /*renderer.setBasePaint(DEFAULT_COLOR);
        renderer.setBaseShape(new Ellipse2D.Double(-1, -1, 1, 1), false);
        renderer.setAutoPopulateSeriesShape(false);
        renderer.setAutoPopulateSeriesPaint(false);
        */
        setDataset(dataset);
        setDomainAxis(timeAxis);
        //setRangeAxis(symbolaxis);
        setRenderer(renderer);
    }
    
    
    /**
     * Lag et nytt xyplot med automatiske min-max verdier
     * @param dataset
     * @param timeAxis
     * @param renderer
     * @param rangeLabel
     */
    public XYChartPlot(XYSeriesCollection dataset, ValueAxis timeAxis, AbstractXYItemRenderer renderer, String rangeLabel) {
        super(dataset, timeAxis, new NumberAxis(rangeLabel), renderer); 
        ValueAxis valueAxis = new NumberAxis(rangeLabel);
        valueAxis.setAutoRange(true);
        
        seriesCount = dataset.getSeries().size();    
        
        /*for (int i=0;i<seriesCount;i++){
            renderer.setSeriesPaint(i, DEFAULT_COLOR);
            renderer.setSeriesShape(i, new Ellipse2D.Double(-1, -1, 1, 1));
        }*/
        /*renderer.setBasePaint(DEFAULT_COLOR);
        renderer.setBaseShape(new Ellipse2D.Double(-1, -1, 1, 1), false);
        renderer.setAutoPopulateSeriesShape(false);
        renderer.setAutoPopulateSeriesPaint(false);
        */
        
        setDataset(dataset);
        setDomainAxis(timeAxis);
        setRangeAxis(valueAxis);
        setRenderer(renderer);
    }
    
    /**
     * Lag et nytt xyplot med min-max verdier, og domenemarkeringer.
     * @param dataset
     * @param timeAxis
     * @param renderer
     * @param rangeLabel
     * @param minRange
     * @param maxRange
     * @param ignition
     */
    @SuppressWarnings("unchecked")
	public XYChartPlot(XYSeriesCollection dataset, ValueAxis timeAxis, AbstractXYItemRenderer renderer, String rangeLabel, int minRange, int maxRange, ArrayList ignition) {
        super(dataset, timeAxis, new NumberAxis(rangeLabel), renderer); 
        this.renderer = renderer;
        
        ValueAxis valueAxis = new NumberAxis(rangeLabel);
        valueAxis.setRange(minRange, maxRange);
        
        seriesCount = dataset.getSeries().size();    
        
        /*int itemcount = 0;
        for (int i=0;i<seriesCount;i++){
            renderer.setSeriesPaint(i, DEFAULT_COLOR);
            renderer.setSeriesShape(i, new Ellipse2D.Double(-1, -1, 1, 1));
            itemcount+=dataset.getItemCount(i);
        }*/
         /*renderer.setBasePaint(DEFAULT_COLOR);
         renderer.setBaseShape(new Ellipse2D.Double(-1, -1, 1, 1), false);
         renderer.setAutoPopulateSeriesShape(false);
         renderer.setAutoPopulateSeriesPaint(false);
         */
        
        if (ignition != null) {
        	for (int i=0;i<ignition.size();i++){
                addDomainMarker((Marker)ignition.get(i));
            }
		}       
        
        setDataset(dataset);
        setDomainAxis(timeAxis);
        setRangeAxis(valueAxis);
        setRenderer(renderer);
        
    }
    
    public void setColor(Color color){
        for (int i=0;i<seriesCount;i++){
            renderer.setSeriesPaint(i, color);
            renderer.setSeriesShape(i, new Ellipse2D.Double(-1, -1, 1, 1));
        }
    }
    
    /**
     * Lag et nytt xyplot uten markeringer.
     * @param dataset
     * @param timeAxis
     * @param renderer
     * @param rangeLabel
     * @param minRange
     * @param maxRange
     * @param ignition
     */
    @SuppressWarnings("unchecked")
	public XYChartPlot(XYSeriesCollection dataset, ValueAxis timeAxis, AbstractXYItemRenderer renderer, String rangeLabel, String [] labels) {
        super(dataset, timeAxis, new SymbolAxis(rangeLabel, labels), renderer); 
        this.renderer = renderer;
        
        seriesCount = dataset.getSeries().size();    
        
        setDataset(dataset);
        setDomainAxis(timeAxis);
        setRenderer(renderer);
        
    }
}
