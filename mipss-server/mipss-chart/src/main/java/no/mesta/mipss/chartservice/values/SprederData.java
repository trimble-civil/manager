package no.mesta.mipss.chartservice.values;

import no.mesta.mipss.common.ManagerConstants;

import java.util.Date;


public class SprederData {
    private Date tidFra;
    private Date tidTil;
    private int bredde;
    private String symmetri;
    private double doseringTotalt;
    private double doseringRes1;
    private double doseringLakeVekt;
    private double doseringLakeVol;
    private String metode;
    private boolean statusOn;
    private String status;
    
    public SprederData() {
    }


    public void setBredde(Integer bredde) {
        this.bredde = bredde;
    }

    public int getBredde() {
        return bredde;
    }

    public void setSymmetri(String symmetri) {
        this.symmetri = symmetri;
    }

    public String getSymmetri() {
        return symmetri;
    }

    public double getDosering() {
    	if (doseringTotalt != 0) {
			return doseringTotalt;
		}
        return doseringRes1;
    }


    public double getDoseringLake() {
    	if (doseringLakeVekt != 0) {
			return doseringLakeVekt;
		}
        return doseringLakeVol * ManagerConstants.WEIGHT_VOLUME_FACTOR;
    }
    
    
	public double getDoseringTotalt() {
		return doseringTotalt;
	}


	public void setDoseringTotalt(Double doseringTotalt) {
		this.doseringTotalt = doseringTotalt;
	}


	public double getDoseringRes1() {
		return doseringRes1;
	}


	public void setDoseringRes1(Double doseringRes1) {
		this.doseringRes1 = doseringRes1;
	}


	public double getDoseringLakeVekt() {
		return doseringLakeVekt;
	}


	public void setDoseringLakeVekt(Double doseringLakeVekt) {
		this.doseringLakeVekt = doseringLakeVekt;
	}


	public double getDoseringLakeVol() {
		return doseringLakeVol;
	}


	public void setDoseringLakeVol(Double doseringLakeVol) {
		this.doseringLakeVol = doseringLakeVol;
	}

	public void setMetode(String metode) {
        this.metode = metode;
    }

    public String getMetode() {
        return metode;
    }

    public void setTidFra(Date tidFra) {
        this.tidFra = tidFra;
    }

    public Date getTidFra() {
        return tidFra;
    }

    public void setTidTil(Date tidTil) {
        this.tidTil = tidTil;
    }

    public Date getTidTil() {
        return tidTil;
    }
    public void setStatus(String status){
    	this.status = status;
    	setStatusOn(status.equals("1"));
    }
    public void setStatusOn(boolean statusOn) {
        this.statusOn = statusOn;
    }

    public boolean isStatusOn() {
        return statusOn;
    }
}
