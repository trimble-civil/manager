package no.mesta.mipss.chartservice.query;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.chartservice.values.ChartSequence;
import no.mesta.mipss.chartservice.values.ChartSeries;
import no.mesta.mipss.chartservice.values.SprederData;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.query.NativeQueryWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SprederQuery{
    private Query query;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());

	private EntityManager em;
    
    public SprederQuery(Long dfuId, Date tidFra, Date tidTil, EntityManager em) {
        this.em = em;
        tidFra = MipssDateFormatter.convertFromCETtoGMT(tidFra);
        tidTil = MipssDateFormatter.convertFromCETtoGMT(tidTil);
        
        String fraTid = MipssDateFormatter.formatDate(tidFra, MipssDateFormatter.DATE_TIME_FORMAT);
        String tilTid = MipssDateFormatter.formatDate(tidTil, MipssDateFormatter.DATE_TIME_FORMAT);
        StringBuilder q = new StringBuilder();
        q.append("select status, ");
        q.append("to_char(cet_dato_tid, 'dd.mm.yyyy hh24:mi:ss'), ");
        q.append("bredde, ");
        q.append("symm, ");
        q.append("nvl(dos_tot,0) dos_tot, nvl(dos_res1,0) dos_res1, ");
        q.append("nvl(dos_flyt_vekt,0) dos_flyt_vekt, nvl(dos_flyt_vol,0) dos_flyt_vol ");
        q.append("from table(spreder_pk.spreder_tab(?1, to_date(?2, 'dd.mm.yyyy hh24:mi'), to_date(?3, 'dd.mm.yyyy hh24:mi')))");
        
        query = em.createNativeQuery(q.toString());
        query.setParameter(1, dfuId);
        query.setParameter(2, fraTid);
        query.setParameter(3, tilTid);
        log.debug("Query: {}", query);
    }

    public ChartSeries consumeResults() {
        final ChartSeries serie =new ChartSeries();
        ChartSequence seq = new ChartSequence();
        
        SprederData prev =  null;
        Date tidspunkt=null;
        
        List<SprederData> wrappedList = new NativeQueryWrapper<SprederData>(query, SprederData.class, new Class[]{
        	String.class, Date.class, Integer.class, 
        	String.class, Double.class, Double.class, Double.class, Double.class}, 
        	"status", "tidFra", "bredde", 
        	"symmetri", "doseringTotalt", "doseringRes1", "doseringLakeVekt", "doseringLakeVol").getWrappedList();
        
        for (SprederData data:wrappedList){
        	if (prev!=null){
        		prev.setTidTil(tidspunkt);
        	}
        	//lag ny serie
        	if (data.isStatusOn()==false&& (prev!=null && prev.isStatusOn())){
        		prev.setTidTil(tidspunkt);
        		prev = null;
        		serie.add(seq);
        		seq = new ChartSequence();
        	}
		  
        	if (data.isStatusOn())
        		seq.add(data);
        	prev = data;
        }
        serie.add(seq);
        
        return serie;
    }
}
