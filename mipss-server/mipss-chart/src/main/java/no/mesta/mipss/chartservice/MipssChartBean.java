package no.mesta.mipss.chartservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.chartservice.dataset.ChartFactory;
import no.mesta.mipss.chartservice.dataset.WeightDistributor;
import no.mesta.mipss.chartservice.dataset.XYMipssLoggChartData;
import no.mesta.mipss.chartservice.query.KjoretoyIdQuery;
import no.mesta.mipss.chartservice.query.MipssLoggQuery;
import no.mesta.mipss.db.exception.DAOException;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jfree.chart.JFreeChart;

@Stateless(name=MipssCharts.BEAN_NAME, mappedName="ejb/"+MipssCharts.BEAN_NAME)
public class MipssChartBean implements MipssCharts{
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
	
	private Logger log = LoggerFactory.getLogger(MipssChartBean.class);
	
	@SuppressWarnings("unchecked")
	public JFreeChart getChart(ChartQueryParams params) {
		log.debug("getChart("+params.getKjoretoyId()+", "+params.getFraDate()+", "+params.getTilDate()+") start");
		String navn ;
		try{
			
			Leverandor l = (Leverandor)em.createNamedQuery(Leverandor.FIND_BY_KJORETOY)
			.setParameter("kjoretoyId", params.getKjoretoyId())
			.getSingleResult();
			navn = l.getNavn();
		} catch (NoResultException e){
			navn = "Ukjent";
		}
		
		List<Installasjon> installasjon = em.createNamedQuery(Installasjon.QUERY_FIND).setParameter("kjoretoyId", params.getKjoretoyId()).setParameter("fraDato", params.getFraDate()).setParameter("tilDato", params.getTilDate()).getResultList();
		Installasjon inst = installasjon.get(0);
		
		log.debug("generating chart...");
		WeightDistributor dist = new WeightDistributor();
		JFreeChart graf =null;
		try {
			graf = ChartFactory.createGraf(params, inst.getDfuId(), navn,  dist, em);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		log.debug("getChart() end");
		return graf;
	}
	
	public JFreeChart getKjoretoyChart(ChartQueryParams params){
		log.debug("getKjoretoyChart("+params.getDriftkontraktId()+", "+params.getFraDate()+", "+params.getTilDate());
		
		Driftkontrakt kontrakt = em.find(Driftkontrakt.class, params.getDriftkontraktId());
		
		log.debug("generating chart...");
		WeightDistributor dist = new WeightDistributor();
		JFreeChart graf = null;
		try {
			graf = ChartFactory.createKjoretoyGraf(params, kontrakt.getTextForGUI(), dist, em);
		} catch (DAOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		log.debug("getKjoretoyChart() end");
		return graf;
	}
	
	@Override
	public JFreeChart getMipssLoggGraph(MipssLoggQueryParams params, List<XYMipssLoggChartData> data) {
		try {
			
			if (params.getDfuId() == null) {
				if(params.getRegNrValgt() || params.getSerieNrValgt() || params.getMaskinNrValgt()){					
					params.setDfuId(getDfuId(params));
				}else{
					params.setDfuId(Long.parseLong(params.getSokeTekst()));
				}
			}
			
			JFreeChart graf = ChartFactory.createMipssLoggGraf(params, data, em);
			return graf;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Finner riktig DFU ID basert på registrerings- maskin- eller serienummer
	 * @param sokeTekst
	 * @param fraDato
	 * @param tilDato
	 * @return dfuId
	 */
	public Long getDfuId(MipssLoggQueryParams params){		
		return new KjoretoyIdQuery(params, em).consumeResults();
	}

	/**
	 * @param params
	 * @return liste over alle produksjonspunkter
	 */
	@Override
	public List<XYMipssLoggChartData> getMipssLoggTableData(MipssLoggQueryParams params) {
		
		try{
			if (params.getDfuId() == null) {
				if(params.getRegNrValgt() || params.getSerieNrValgt() || params.getMaskinNrValgt()){					
					params.setDfuId(getDfuId(params));
				}else
					params.setDfuId(Long.parseLong(params.getSokeTekst()));
			}
			
			return new MipssLoggQuery(params, em).consumeResults();
		} catch (Exception e) {
			return null;
		}
		
	}
}
