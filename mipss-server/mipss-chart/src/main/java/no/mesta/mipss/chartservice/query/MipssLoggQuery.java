package no.mesta.mipss.chartservice.query;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.chartservice.dataset.XYMipssLoggChartData;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.util.DateFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spørring for å hente ut data om hvert produskjonspunnkt som vises i mipss logg.
 * @author larnym
 */
public class MipssLoggQuery {

	private Query query;
	private EntityManager em;
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public MipssLoggQuery(MipssLoggQueryParams params, EntityManager em) {
		this.em = em;
		
        StringBuilder qry = new StringBuilder();
        
        String fraTid = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(params.getFraDato()), MipssDateFormatter.DATE_TIME_FORMAT);
        String tilTid = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(params.getTilDato()), MipssDateFormatter.DATE_TIME_FORMAT);
        Long dfuId = params.getDfuId();
        
        qry.append("select to_char(p.gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), p.hastighet, p.digi, p.breddegrad, p.lengdegrad, pd.innstreng from prodpunkt p ");
        qry.append("left join prodpunktdetalj pd ");
        qry.append("on p.gmt_tidspunkt = pd.gmt_tidspunkt ");
        qry.append("and p.dfu_id = pd.dfu_id ");
        qry.append("where p.dfu_id = ?1 ");
        qry.append("and p.gmt_tidspunkt between to_date(?2, 'dd.mm.yyyy hh24:mi') ");
        qry.append("and to_date(?3, 'dd.mm.yyyy hh24:mi') order by p.gmt_tidspunkt");
        
        query = em.createNativeQuery(qry.toString());
        query.setParameter(1, dfuId);
        query.setParameter(2, fraTid);
        query.setParameter(3, tilTid);
        
        log.debug("Query: {}", query);
		
	}
	
	public List<XYMipssLoggChartData> consumeResults() {
        List<XYMipssLoggChartData> wrappedList = new NativeQueryWrapper<XYMipssLoggChartData>(query, XYMipssLoggChartData.class, new Class[]{
        	Date.class, Double.class, byte[].class, String.class, String.class, String.class}, 
        	"tidspunkt", "hastighet", "digiByte", "breddegrad", "lengdegrad", "cen").getWrappedList();
        
        return wrappedList;
    }	
}
