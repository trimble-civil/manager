package no.mesta.mipss.chartservice.values;

import java.util.ArrayList;

@SuppressWarnings({"serial" })
public class ChartSequence<T> extends ArrayList<T>{
    
    public ChartSequence() {
        super();
    }

	public final boolean add(final T item) {
        return super.add(item);
    }
    
    public final T elementAt(final int index) {
        return get(index);
    }
}
