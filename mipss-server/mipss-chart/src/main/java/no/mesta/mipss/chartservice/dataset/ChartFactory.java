package no.mesta.mipss.chartservice.dataset;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.chartservice.query.HastighetQuery;
import no.mesta.mipss.chartservice.query.KjoretoyQuery;
import no.mesta.mipss.chartservice.query.MetodeQuery;
import no.mesta.mipss.chartservice.query.ProduksjonQuery;
import no.mesta.mipss.chartservice.query.RodeQuery;
import no.mesta.mipss.chartservice.query.SprederQuery;
import no.mesta.mipss.chartservice.query.VeiQuery;
import no.mesta.mipss.chartservice.values.ChartSeries;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.db.exception.DAOException;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.jfree.util.ShapeUtilities;


public class ChartFactory {
	
	private static PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("chartText");   
    private static ChartFactory factory;
    private static Logger logger = LoggerFactory.getLogger(ChartFactory.class);
	private EntityManager em;
    
    public ChartFactory(EntityManager em) {
    	this.em = em;
	}

	/**
     * Lager en graf, gyldige verdier for type er hastighet, vei, rode, produksjon eller alle. veinummer
     * kan være null dersom grafen ikke skal inneholde veidata. 
     * @param type
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param veinummer
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static JFreeChart createGraf(ChartQueryParams params, Long dfuId, String leverandor, WeightDistributor dist, EntityManager em) throws Exception{

	//public static JFreeChart createGraf(Long dfuId, Long kjoretoyId, String leverandor, Date fraDato, Date tilDato, Boolean veinummer, Boolean hastighet, Boolean vei, Boolean rode, Boolean produksjon, Boolean sprederdata, WeightDistributor dist, EntityManager em) throws Exception{
        if (factory==null){
            factory = new ChartFactory(em);
        }
        Date fraDato = params.getFraDate();
        Date tilDato = params.getTilDate();
        Long kjoretoyId = params.getKjoretoyId();
        // lag plot
        DateAxis dateAxis = factory.getDefaultValueAxis(params.getFraDate(), params.getTilDate());
        CombinedDomainXYPlot cplot = new CombinedDomainXYPlot(dateAxis);
        
        CategoryChartPlot veiPlot = null;
        if (params.getVei()){
            veiPlot = factory.getVeiPlot(kjoretoyId, fraDato, tilDato, dateAxis, params.getVeinummer(), dist);
        }
                
        CategoryChartPlot rodePlot = null;
        if (params.getRode()){
            rodePlot = factory.getRodePlot(kjoretoyId, fraDato, tilDato, dateAxis, dist);
        }
        
        CategoryChartPlot produksjonPlot = null;
        if (params.getProduksjon()){
            produksjonPlot = factory.getProduksjonPlot(kjoretoyId, fraDato, tilDato, dateAxis, dist);
        }
        
        XYChartPlot breddePlot = null;
        XYChartPlot doseringPlot = null;
        XYChartPlot doseringlakePlot = null;
        CategoryChartPlot metodePlot = null;
        
        if (params.getSprederdata()){
            ChartSeries sprederData = new SprederQuery(dfuId, fraDato, tilDato, em).consumeResults();
            breddePlot = factory.getBreddePlot(sprederData, dateAxis, dist);
            breddePlot.getRangeAxis().resizeRange(2.0);
            
            doseringPlot = factory.getDoseringPlot(sprederData, dateAxis, dist);
            doseringPlot.getRangeAxis().resizeRange(2.0);
            doseringlakePlot = factory.getDoseringLakePlot(sprederData, dateAxis,dist);
            
            ChartSeries metodeData = new MetodeQuery(dfuId, fraDato, tilDato, em).consumeResults();
            metodePlot = factory.getMetodePlot(metodeData, dateAxis, dist);
            
        }else{
            dist.setTotalWeightCount(20);
        }
        
        // legg til subplots til kombinert plot
        if (params.getHastighet()){
            XYChartPlot hastighetPlot = factory.getHastighetPlot(dfuId, fraDato, tilDato, dateAxis, dist);
            cplot.add(hastighetPlot,dist.getWeight("hastighet"));   
        }
       
        if (params.getVei()){
            cplot.add(veiPlot, dist.getWeight("vei"));
        }
        if (params.getRode()){
            cplot.add(rodePlot, dist.getWeight("rode"));
        }
        if (params.getProduksjon()){
            cplot.add(produksjonPlot, dist.getWeight("produksjon"));
        }
        if (params.getSprederdata()){
            cplot.add(breddePlot, dist.getWeight("bredde"));
            cplot.add(doseringPlot, dist.getWeight("dosering"));
            cplot.add(doseringlakePlot,dist.getWeight("doseringlake"));
            cplot.add(metodePlot, dist.getWeight("metode"));
        }
        String df = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_DATE_TIME_FORMAT);
        String dt = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_DATE_TIME_FORMAT);
        Kjoretoy k = em.find(Kjoretoy.class, kjoretoyId);
        
        String id = params.getHideId()?"":k.getEksterntNavn();
        TextTitle mainTitle = new TextTitle(id + "("+df+"-"+dt+")", new Font("SansSerif", Font.BOLD, 18));
        
        TextTitle subTitle = new TextTitle(leverandor, new Font("SansSerif", Font.PLAIN, 15));
        
        ArrayList titleList = new ArrayList();
        titleList.add(mainTitle);
        if (!params.getHideId())
        	titleList.add(subTitle);
        
        JFreeChart chart = new JFreeChart("", new Font("SansSerif", Font.PLAIN, 18), cplot, false);
        chart.setSubtitles(titleList);
        chart.setRenderingHints(new RenderingHints(
                RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_OFF));
        
        return chart;
    }
    
    @SuppressWarnings("unchecked")
    public static JFreeChart createKjoretoyGraf(ChartQueryParams params, String kontraktNavn, WeightDistributor dist, EntityManager em) throws DAOException{

        if (factory==null){
            factory = new ChartFactory(em);
        }
        dist.setTotalWeightCount(3);
        DateAxis dateAxis = factory.getDefaultValueAxis(params.getFraDate(), params.getTilDate());
        CombinedDomainXYPlot cplot = new CombinedDomainXYPlot(dateAxis);
        logger.debug("get chartdata from db");
        long start = System.currentTimeMillis();
        ChartSeries dataset = new KjoretoyQuery(params.getDriftkontraktId(), params.getFraDate(), params.getTilDate(), params.getKunProduksjon(), params.getKunKontraktKjoretoy(), em, params.getKjoretoygruppe()).consumeResults(params.getHideId());
        logger.debug("db time:"+(System.currentTimeMillis()-start));
        CategoryChartPlot[] plot = factory.getKjoretoyPlot(dataset, dateAxis, dist);
        
        for (int i=0;i<plot.length;i++){
            cplot.add(plot[i], 20);
        }
        
        JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, cplot, false);
        chart.setRenderingHints(new RenderingHints(
                RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_OFF));

        LegendTitle legend = new LegendTitle(chart.getPlot());
        BlockContainer container = legend.getItemContainer();
        container.setFrame(new BlockBorder(1.0D, 1.0D, 1.0D, 1.0D));
        container.setPadding(2D, 10D, 5D, 2D);
        legend.setWrapper(container);
        legend.setPosition(RectangleEdge.BOTTOM);
        legend.setHorizontalAlignment(HorizontalAlignment.CENTER);
        legend.setBackgroundPaint(Color.white);
        
        String df = MipssDateFormatter.formatDate(params.getFraDate(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
        String dt = MipssDateFormatter.formatDate(params.getTilDate(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
        
        TextTitle mainTitle = new TextTitle(kontraktNavn+" ("+df+"-"+dt+")", new Font("SansSerif", Font.BOLD, 18));
        TextTitle subTitle = new TextTitle(resources.getGuiString("kjoretoygraf.info"), new Font("SansSerif", Font.PLAIN, 12));
               
        ArrayList titleList = new ArrayList();
        titleList.add(mainTitle);
        titleList.add(subTitle);
        chart.setSubtitles(titleList);
        chart.addLegend(legend);
               
        return chart;       
    }
    
    /**
     * Hastighets plot
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param dateAxis
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private XYChartPlot getHastighetPlot(Long dfuId, Date fraDato, Date tilDato, DateAxis dateAxis, WeightDistributor dist) throws Exception{
        ArrayList ignitionSetting = new ArrayList();
        ChartSeries hastighetData = new HastighetQuery(dfuId, fraDato, tilDato, em).consumeResults();
        XYSeriesCollection datasetHastighet = DatasetFactory.createHastighetDataset(hastighetData, ignitionSetting);
        
        XYChartPlot hPlot = new XYChartPlot(datasetHastighet, dateAxis, getDefaultXYRenderer(), "Km/t", 0, 100, ignitionSetting);
        dist.add("hastighet", 140);//, WeightEntry.WEIGHT_STATIC);
        return hPlot;
    }
    
    /**
     * Vei plot
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param dateAxis
     * @param veinummer
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private CategoryChartPlot getVeiPlot(Long dfuId, Date fraDato, Date tilDato, DateAxis dateAxis, boolean veinummer, WeightDistributor dist) throws Exception{
        ArrayList categories = new ArrayList();
        ChartSeries vegData = new VeiQuery(dfuId, fraDato, tilDato, veinummer, em).consumeResults();
        IntervalXYDataset datasetVeg = DatasetFactory.createCategoryDataset(vegData, categories);
        String[] cat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            cat[i]=(String)categories.get(i);
        }
        dist.add("vei", cat.length*20);//, WeightEntry.WEIGHT_DYNAMIC);
        
        CategoryChartPlot vPlot = new CategoryChartPlot(datasetVeg, dateAxis, getDefaultCategoryRenderer(), cat, "Veg");
        logger.debug("number of categories in vei plot: "+cat.length);
        return vPlot;
    }
    

	/**
     * Rode plot
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param dateAxis
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private CategoryChartPlot getRodePlot(Long dfuId, Date fraDato, Date tilDato, DateAxis dateAxis, WeightDistributor dist) throws Exception{
        ArrayList categories = new ArrayList();
        ChartSeries rodeData = new RodeQuery(dfuId, fraDato, tilDato, em).consumeResults(); 
        IntervalXYDataset datasetRode = DatasetFactory.createCategoryDataset(rodeData, categories);
        String[] rcat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            rcat[i]=(String)categories.get(i);
            if (rcat[i].length()>20)
                rcat[i]=rcat[i].substring(0,20)+"..";
        }
        dist.add("rode", rcat.length*20);//, WeightEntry.WEIGHT_DYNAMIC);
        CategoryChartPlot rPlot = new CategoryChartPlot(datasetRode, dateAxis, getDefaultCategoryRenderer(), rcat, "Rode");
        return rPlot;
    }
    /**
     * Produksjon plot
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param dateAxis
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private CategoryChartPlot getProduksjonPlot(Long dfuId, Date fraDato, Date tilDato, DateAxis dateAxis, WeightDistributor dist) throws Exception{
        ArrayList categories = new ArrayList();
        ChartSeries produksjonData = new ProduksjonQuery(dfuId, fraDato, tilDato, em).consumeResults();//TODO fetch data dao.getProduksjonData(kjoretoyId, fraDato, tilDato);
        IntervalXYDataset datasetProduksjon = DatasetFactory.createCategoryDataset(produksjonData, categories);
        String[] pcat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            pcat[i]=(String)categories.get(i);
        }
        dist.add("produksjon", pcat.length*20);//, WeightEntry.WEIGHT_DYNAMIC);
        CategoryChartPlot pPlot = new CategoryChartPlot(datasetProduksjon, dateAxis, getDefaultCategoryRenderer(), pcat, "Produksjon");
        return pPlot;
    }
    
    /**
     * Bredde plot
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param dateAxis
     * @return
     * @throws Exception
     */
    private XYChartPlot getBreddePlot(ChartSeries breddeData, DateAxis dateAxis, WeightDistributor dist) {
    	XYSeriesCollection datasetBredde = DatasetFactory.createBreddeDataset(breddeData);
    	
        XYChartPlot plot = new XYChartPlot(datasetBredde, dateAxis, getDefaultStepRenderer(), resources.getGuiString("spreder.bredde"));
        dist.add("bredde", 100);//, WeightEntry.WEIGHT_STATIC);
        return plot;
    }
    
    
    /**
     * Symmetri plot
     * @param symmetriData
     * @param dateAxis
     * @return
     */
    @SuppressWarnings("unchecked")
	private XYChartPlot getSymmetriPlot(ChartSeries symmetriData, DateAxis dateAxis, WeightDistributor dist){
        ArrayList categories = new ArrayList();
        XYSeriesCollection datasetSymmetri = DatasetFactory.createSymmetriDataset(symmetriData, categories);
        String[] scat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            scat[i]=(String)categories.get(i);
        }
        
        XYChartPlot plot = new XYChartPlot(datasetSymmetri, dateAxis, getDefaultStepRenderer(), scat, resources.getGuiString("spreder.strosektor"));
        dist.add("symmetri", 100);//, WeightEntry.WEIGHT_STATIC);
        return plot;
    }
    
    private XYChartPlot getDoseringPlot(ChartSeries doseringsData, DateAxis dateAxis, WeightDistributor dist){
    	XYSeriesCollection datasetHastighet = DatasetFactory.createDoseringDataset(doseringsData);
        XYChartPlot plot = new XYChartPlot(datasetHastighet, dateAxis, getDefaultStepRenderer(), resources.getGuiString("spreder.dosering"));
        dist.add("dosering", 100);//, WeightEntry.WEIGHT_STATIC);
        return plot;
    }
    private XYChartPlot getDoseringLakePlot(ChartSeries doseringLakeData, DateAxis dateAxis, WeightDistributor dist){
    	XYSeriesCollection datasetHastighet = DatasetFactory.createDoseringLakeDataset(doseringLakeData);
        XYChartPlot plot = new XYChartPlot(datasetHastighet, dateAxis, getDefaultStepRenderer(), resources.getGuiString("spreder.lake"));
        dist.add("doseringlake", 100);//, WeightEntry.WEIGHT_STATIC);
        return plot;
    }
    
    /**
     * Strømetode plot
     * @param kjoretoyId
     * @param fraDato
     * @param tilDato
     * @param dateAxis
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private CategoryChartPlot getMetodePlot(ChartSeries metodeData, DateAxis dateAxis, WeightDistributor dist) throws Exception{
        ArrayList categories = new ArrayList();
        IntervalXYDataset datasetMetode = DatasetFactory.createCategoryDataset(metodeData, categories);
        String[] pcat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            pcat[i]=(String)categories.get(i);
        }
        CategoryChartPlot pPlot = new CategoryChartPlot(datasetMetode, dateAxis, getDefaultCategoryRenderer(), pcat, resources.getGuiString("spreder.metode"));
        dist.add("metode", pcat.length*20);//, WeightEntry.WEIGHT_DYNAMIC);
        return pPlot;
    }
    
    
    
    @SuppressWarnings("unchecked")
	private CategoryChartPlot[] getKjoretoyPlot(ChartSeries kjoretoyData, DateAxis dateAxis, WeightDistributor dist){
        logger.debug("getKjoretoyPlot() start");
        long start = System.currentTimeMillis();
        ArrayList categories = new ArrayList();
        ArrayList[] markers = new ArrayList[kjoretoyData.size()];
        ArrayList labelColors = new ArrayList();
        HashMap produksjonsTyper = new HashMap();
        
        IntervalXYDataset[] datasetKjoretoy = DatasetFactory.createKjoretoyDataset(kjoretoyData, categories, produksjonsTyper, markers, labelColors);
        String[] cat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            cat[i]=(String)categories.get(i);
        }
        
        AbstractXYItemRenderer[] renderer = new AbstractXYItemRenderer[cat.length];//getDefaultCategoryRenderer();
        CategoryChartPlot[] plots = new CategoryChartPlot[cat.length];
        logger.debug(datasetKjoretoy.length+" "+cat.length+" "+renderer.length+" "+plots.length+" "+markers.length);
        for (int i=0;i<plots.length;i++){
            renderer[i] = getDefaultCategoryRenderer();
            plots[i] = new CategoryChartPlot(datasetKjoretoy[i], dateAxis, renderer[i], new String[]{cat[i]}, produksjonsTyper, markers[i], Boolean.valueOf((String)labelColors.get(i)));
        }
        
        Set types = produksjonsTyper.keySet();
        Iterator it = types.iterator();
        
        //vis kun 1 legend item pr type
        while (it.hasNext()){
            String type = (String)it.next();
            
            outer:
            for (int k=0;k<plots.length;k++){
                int datasets = plots[k].getDatasetCount();
                for (int i=0;i<datasets;i++){
                    XYIntervalSeriesCollection dataset = (XYIntervalSeriesCollection) plots[k].getDataset(i);
                    int series = dataset.getSeriesCount();
                    for (int j=0;j<series;j++){
                        if (dataset.getSeries(j).getKey().equals(type)){
                            plots[k].getRenderer().setSeriesVisibleInLegend(j, true);
                            break outer;
                        }
                    }
                }
            }
        }
        dist.add("kjoretoy", cat.length*20);//, WeightEntry.WEIGHT_DYNAMIC);
        
        logger.debug("getKjoretoyPlot() runtime:"+(System.currentTimeMillis()-start));
        return plots;
    }
    /**
     * Lager en tidsakse
     * @param fraDato
     * @param tilDato
     * @return
     */
    private DateAxis getDefaultValueAxis(Date fraDato, Date tilDato){
        DateAxis dateAxis = null;
    	dateAxis = new DateAxis("Tid");
    	dateAxis.setMinimumDate(fraDato);
      	dateAxis.setMaximumDate(tilDato);
        return dateAxis;     
    }
    
    private AbstractXYItemRenderer getDefaultStepRenderer(){
        AbstractXYItemRenderer renderer = new XYStepRenderer();
        renderer.setBaseOutlineStroke(new BasicStroke(1));
        renderer.setBasePaint(Color.blue);
        renderer.setAutoPopulateSeriesShape(false);
        renderer.setAutoPopulateSeriesPaint(false);
        return renderer;
    }
    
    private XYBarRenderer getDefaultCategoryRenderer(){
    	XYBarRenderer renderer = new XYBarRenderer();
        renderer.setUseYInterval(true);
        renderer.setMargin(0);
        renderer.setShadowVisible(false);
        renderer.setBarPainter(new StandardXYBarPainter());
        return renderer;
    }   
    
    private AbstractXYItemRenderer getDefaultXYRenderer(){
        AbstractXYItemRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setBaseOutlineStroke(new BasicStroke(1));
        renderer.setBasePaint(Color.blue);
        renderer.setBaseShape(new Ellipse2D.Double(-1, -1, 1, 1), false);
        renderer.setAutoPopulateSeriesShape(false);
        renderer.setAutoPopulateSeriesPaint(false);
        return renderer;
    }
    
    private AbstractXYItemRenderer getTriangleRenderer(Color baseColor, int antallSerier){
        AbstractXYItemRenderer renderer = new XYLineAndShapeRenderer(false, true);
        renderer.setBaseOutlineStroke(new BasicStroke(1));
        renderer.setBasePaint(baseColor);
        if (antallSerier > 1) {
            renderer.setSeriesPaint(1, Color.blue);
		}  
        Shape triangle = ShapeUtilities.createUpTriangle(3);
        renderer.setBaseShape(triangle, false);
        renderer.setAutoPopulateSeriesShape(false);
        renderer.setAutoPopulateSeriesPaint(false);
        return renderer;
    }
    
    /**
     * Oppretter grafer for å vise produksjon basert på produksjonspunkter
     * @param params
     * @param dist
     * @param em
     * @return JFreeChart
     */
    public static JFreeChart createMipssLoggGraf(MipssLoggQueryParams params, List<XYMipssLoggChartData> loggData, EntityManager em) throws Exception{

    	if (loggData == null) {
    		return null;
    	} else{

    		if (factory==null){
    			factory = new ChartFactory(em);
    		}

    		// lag plot
    		DateAxis dateAxis = factory.getDefaultValueAxis(params.getFraDato(), params.getTilDato());
    		CombinedDomainXYPlot cplot = new CombinedDomainXYPlot(dateAxis);

    		XYChartPlot hastighetPlot = factory.getHastighetPlotMipssLogg(loggData, dateAxis);
    		cplot.add(hastighetPlot,30);   

    		XYChartPlot gpsPlot = factory.getGpsPlot(loggData, dateAxis);
    		cplot.add(gpsPlot, 10);

    		CategoryChartPlot mipssLoggPlot = factory.getMipssLoggPlot(loggData, dateAxis);
    		cplot.add(mipssLoggPlot,70);

    		XYChartPlot cenPlot = factory.getCenPlot(loggData, dateAxis);
    		cplot.add(cenPlot, 5);      

    		hastighetPlot.setDomainCrosshairVisible(true);   
    		hastighetPlot.setDomainCrosshairLockedOnData(true);   

    		gpsPlot.setDomainCrosshairVisible(true);   
    		gpsPlot.setDomainCrosshairLockedOnData(true);   

    		mipssLoggPlot.setDomainCrosshairVisible(true);   
    		mipssLoggPlot.setDomainCrosshairLockedOnData(true);   

    		cenPlot.setDomainCrosshairVisible(true);   
    		cenPlot.setDomainCrosshairLockedOnData(true);   

    		String df = MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
    		String dt = MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);

    		TextTitle mainTitle = new TextTitle(params.getSokeTekst().toUpperCase() + " (DFU: " + params.getDfuId() + ") ("+df+"-"+dt+")", new Font("SansSerif", Font.BOLD, 18));

    		ArrayList<TextTitle> titleList = new ArrayList<TextTitle>();
    		titleList.add(mainTitle);

    		JFreeChart chart = new JFreeChart("", new Font("SansSerif", Font.PLAIN, 18), cplot, false);
    		chart.setSubtitles(titleList);
    		chart.setRenderingHints(new RenderingHints( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF));       

    		return chart;
    	}
    }

	/**
     * Mipss Logg plot
     * @param loggData
     * @param dateAxis
     * @return XYChartPlot
     * @throws Exception
     */
	private XYChartPlot getCenPlot(List<XYMipssLoggChartData> loggData, DateAxis dateAxis) throws Exception {
		
		XYSeriesCollection datasetLogg = DatasetFactory.createCenDatset(loggData);        
		String[]labels = {"CEN"};		
        XYChartPlot cPlot = new XYChartPlot(datasetLogg, dateAxis, getTriangleRenderer(Color.red, datasetLogg.getSeriesCount()), null, labels);
        return cPlot;		
	}
	
	@SuppressWarnings("unchecked")
	private XYChartPlot getHastighetPlotMipssLogg(List<XYMipssLoggChartData> loggData, DateAxis dateAxis) throws Exception{
		ArrayList ignitionSetting = new ArrayList();
		XYSeriesCollection datasetLogg = DatasetFactory.createHastighetloggDataset(loggData, ignitionSetting);
		XYChartPlot hPlot = new XYChartPlot(datasetLogg, dateAxis, getDefaultXYRenderer(), "Km/t", 0, 100, ignitionSetting);
        return hPlot;
    }
	
	/**
     * Mipss Logg plot
     * @param loggData
     * @param dateAxis
     * @return
     * @throws Exception
     */
	private XYChartPlot getGpsPlot(List<XYMipssLoggChartData> loggData, DateAxis dateAxis) throws Exception {

		XYSeriesCollection datasetLogg = DatasetFactory.createGpsDatset(loggData);
		String[]labels = {"GPS", "LIV"};		
		XYChartPlot cPlot = new XYChartPlot(datasetLogg, dateAxis, getTriangleRenderer(Color.green, datasetLogg.getSeriesCount()), "GPS", labels);
		return cPlot;		
	}

	/**
     * Mipss Logg plot
     * @param loggData
     * @param dateAxis
     * @return CategoryChartPlot
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private CategoryChartPlot getMipssLoggPlot(List<XYMipssLoggChartData> loggData, DateAxis dateAxis) throws Exception{
        ArrayList categories = new ArrayList();        
        categories.add("Inspek.");
        categories.add("Kont.friksj.");
        categories.add("Ukjent NMEA");
        categories.add("Eltrip");
        categories.add("Spreder");
        categories.add("AN4");
        categories.add("AN3");
        categories.add("AN2");
        categories.add("DI4");
        categories.add("DI3");
        categories.add("DI2");
        categories.add("LOG");
        categories.add("IGN");
        IntervalXYDataset datasetLogg = DatasetFactory.createMipssLoggDataset(loggData, categories);
        
        String[] pcat = new String[categories.size()];
        for (int i=0;i<categories.size();i++){
            pcat[i]=(String)categories.get(i);
        }
        
        CategoryChartPlot rPlot = new CategoryChartPlot(datasetLogg, dateAxis, getDefaultCategoryRenderer(), pcat, "Produksjonsdata");

        return rPlot;
    }    
}
