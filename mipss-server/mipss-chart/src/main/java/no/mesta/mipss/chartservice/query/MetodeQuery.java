package no.mesta.mipss.chartservice.query;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.chartservice.dataset.CategoryChartData;
import no.mesta.mipss.chartservice.values.CategoryChartDataComparator;
import no.mesta.mipss.chartservice.values.ChartSequence;
import no.mesta.mipss.chartservice.values.ChartSeries;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.query.NativeQueryWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetodeQuery{
	private Query query;
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private EntityManager em;

	public MetodeQuery(Long dfuId, Date tidFra, Date tidTil, EntityManager em){
		this.em = em;
		tidFra = MipssDateFormatter.convertFromCETtoGMT(tidFra);
		tidTil = MipssDateFormatter.convertFromCETtoGMT(tidTil);
		String fraTid = MipssDateFormatter.formatDate(tidFra, MipssDateFormatter.DATE_TIME_FORMAT);
		String tilTid = MipssDateFormatter.formatDate(tidTil, MipssDateFormatter.DATE_TIME_FORMAT);

		StringBuilder q = new StringBuilder();
		q.append("select kategori , ");
		q.append("to_char(cast(tid_fra as date), 'dd.MM.yyyy hh24:mi:ss') fra, ");
		q.append("to_char(cast(tid_til as date), 'dd.MM.yyyy hh24:mi:ss') til, ");
		q.append("sortering ");
		q.append("from table(rapport_funksjoner.lagmetodedataset(?1,to_date(?2, 'dd.mm.yyyy hh24:mi'),to_date(?3,'dd.mm.yyyy hh24:mi')))");
		query = em.createNativeQuery(q.toString());
		query.setParameter(1, dfuId);
		query.setParameter(2, fraTid);
		query.setParameter(3, tilTid);

		log.debug("Query: {}", query);
	}

	@SuppressWarnings("unchecked")
	public ChartSeries consumeResults(){
		final ChartSeries serie =new ChartSeries();
		List<CategoryChartData> wrappedList = new NativeQueryWrapper<CategoryChartData>(query, CategoryChartData.class, new Class[]{
			String.class, Date.class, Date.class, Integer.class}, 
			"category", "startDate", "endDate", "sortering").getWrappedList();

		ChartSequence seq = new ChartSequence();
		seq.addAll(wrappedList);

		Collections.sort(seq, new CategoryChartDataComparator());
		serie.add(seq);

		return serie;
	}
}
