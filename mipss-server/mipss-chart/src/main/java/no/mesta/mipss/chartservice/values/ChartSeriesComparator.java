package no.mesta.mipss.chartservice.values;

import java.util.Comparator;

import no.mesta.mipss.chartservice.dataset.CategoryChartData;

/**
 * Sorterer en ChartSeries collection, 
 * denne er avhengig av at ChartSequence objektene i collection er sortert. 
 */
@SuppressWarnings("unchecked")
public class ChartSeriesComparator implements Comparator{
    public ChartSeriesComparator() {
    }

    public int compare(Object o1, Object o2) {
        CategoryChartData d1 = (CategoryChartData)((ChartSequence)o1).get(0);
        CategoryChartData d2 = (CategoryChartData)((ChartSequence)o2).get(0);
        int c1 = d1.getSortering();
        int c2 = d2.getSortering();
        
        if (c1 == c2) {
            return 0;
        } else if (c1 < c2) {
            return -1;
        } else {
            return 1;
        }
    }
}