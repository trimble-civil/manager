package no.mesta.mipss.chartservice.dataset;

	import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.FastXyPlot;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.ui.Layer;


@SuppressWarnings("serial")
public class CategoryChartPlot extends FastXyPlot{
    public CategoryChartPlot(IntervalXYDataset dataset, ValueAxis timeAxis, AbstractXYItemRenderer renderer, String[] categoryItemsLabel, String categoryLabel) {
    	super(null, null, null, null);
        SymbolAxis symbolaxis = new SymbolAxis(categoryLabel, categoryItemsLabel);
        
        int seriesCount = dataset.getSeriesCount();
        for (int i=0;i<seriesCount;i++){
            String cat = (String)dataset.getSeriesKey(i);
            if (cat.toLowerCase().equals("sum")){
                renderer.setSeriesPaint(i, Color.black);
            }
        }
        setDataset(dataset); 
        setDomainAxis(timeAxis);
        setRangeAxis(symbolaxis);
        setRenderer(renderer);
    }
    
    @SuppressWarnings("unchecked")
	public CategoryChartPlot(IntervalXYDataset dataset, ValueAxis timeAxis, AbstractXYItemRenderer renderer, String[] categoryItemsLabel, HashMap produksjonsTyper, ArrayList markers, boolean labelColor){
    	super(null, null, null, null);
    	SymbolAxis axis = new SymbolAxis("", categoryItemsLabel);
        if(!labelColor)
            axis.setTickLabelPaint(Color.blue);
        
        XYIntervalSeriesCollection  collection = (XYIntervalSeriesCollection)dataset;
        int series = collection.getSeriesCount();
        for (int i=0;i<series;i++){
            XYIntervalSeries serie = collection.getSeries(i);
            String key = (String)serie.getKey();
            String color = (String)produksjonsTyper.get(key);
            renderer.setSeriesPaint(i, getColor(color));
            renderer.setSeriesVisibleInLegend(i, false);
        }
        
        setDataset(dataset);
        setDomainAxis(timeAxis);
        setRangeAxis(axis);
        setRenderer(renderer);
        
        if (markers!=null){
            Iterator it = markers.iterator();
            while (it.hasNext()){
                IntervalMarker marker = (IntervalMarker)it.next();
                addDomainMarker(marker, Layer.BACKGROUND);
            }
        }
    }
    

    private Color getColor(String color){
        int tiltakColor = 0;
        StringTokenizer st = new StringTokenizer(color, "|");
        tiltakColor = (Integer.decode((String)st.nextElement()));
        
        
        while (st.hasMoreElements()){
            tiltakColor =tiltakColor^(Integer.decode((String)st.nextElement()));
        }
        
        return new Color(tiltakColor);
    }
}
