package no.mesta.mipss.chartservice.query;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.chartservice.dataset.XYChartData;
import no.mesta.mipss.chartservice.values.ChartSequence;
import no.mesta.mipss.chartservice.values.ChartSeries;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.util.DateFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HastighetQuery{
	private Logger log = LoggerFactory.getLogger(this.getClass());
    private Query query;
	private EntityManager em;
    
    public HastighetQuery(Long dfuId, Date tidFra, Date tidTil, EntityManager em){
    	this.em = em;
        StringBuilder qry = new StringBuilder();
        
        String fraTid = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(tidFra), MipssDateFormatter.DATE_TIME_FORMAT);
        String tilTid = MipssDateFormatter.formatDate(DateFormatter.convertFromCETtoGMT(tidTil), MipssDateFormatter.DATE_TIME_FORMAT);
        
        qry.append("select to_char(gmt_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), nvl(hastighet, 0), digi from prodpunkt ");
        qry.append("where dfu_id = ?1 ");//qry.append(dfuId.intValue());
        qry.append("and gmt_tidspunkt between to_date(?2, 'dd.mm.yyyy hh24:mi') ");//qry.append(fraTid);
        qry.append("and to_date(?3, 'dd.mm.yyyy hh24:mi') order by gmt_tidspunkt");
        
        query = em.createNativeQuery(qry.toString());
        query.setParameter(1, dfuId);
        query.setParameter(2, fraTid);
        query.setParameter(3, tilTid);
        
        log.debug("Query: {}", query);
    }

    public ChartSeries consumeResults(){
        
        final ChartSeries serie =new ChartSeries();
        int rowCount=1;
        //konverter tilbake til CET
        TimeZone cetTime = TimeZone.getTimeZone("CET");
        DateFormat gmtFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        gmtFormat.setTimeZone(cetTime);
        
        NativeQueryWrapper<XYChartData> wrap = new NativeQueryWrapper<XYChartData>(query, XYChartData.class, new Class[]{
        	Date.class, Double.class, byte[].class}, 
        	"domain", "range", "digiByte");
//        wrap.setDateFormatter(gmtFormat);
        
        XYChartData prev = new XYChartData();
        ChartSequence seq = new ChartSequence();
        
        for (XYChartData data:wrap.getWrappedList()){
        	data.setDomain(DateFormatter.convertFromGMTtoCET(data.getDomain()));
            if (rowCount!=1){
                //if ((data.getDomain().getTime() - prev.getDomain().getTime()) > (1000*60*10)){
                  if (data.getIgnition()&&(!prev.getIgnition())){
                    serie.add(seq);
                    seq = new ChartSequence();
                }
            }
            seq.add(data);
            prev = data;
            
            rowCount++;
        }
        serie.add(seq);
        return serie;
        
        
//        while (rs.next()){
//            try{
//                XYChartData data = new XYChartData();
//                Date t = df.parse(rs.getString(1));
//                
//                String time = gmtFormat.format(t);
//                DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//                t = format.parse(time);
//                
//                data.setDomain(t);
//                data.setRange(rs.getDouble(2));
//                String bits = rs.getString(3);
//                short number = Short.valueOf(bits, 16).shortValue();
//                data.setIgnition((number&0x0001)==0?"av":"på");  //logisk AND med 1 for å finne ut om LSB=0 eller 1
//                
//                //data.setIgnition(number==0?"av":"på");
//                //data.setLogging((number==3)?true:false);
//                
//                if (rowCount!=1){
//                    //if ((data.getDomain().getTime() - prev.getDomain().getTime()) > (1000*60*10)){
//                      if (data.getIgnition().equals("på")&&prev.getIgnition().equals("av")){
//                        serie.add(seq);
//                        seq = new ChartSequence();
//                    }
//                }
//                //uncomment hvis ikke siste livstegn-punkter skal vises i graf. 
//                //if (number!=0){
//                    seq.add(data);
//                    prev = data;
//                //}
//                rowCount++;
//            }catch (ParseException e){
//                e.printStackTrace();
//            }
//        }
//        serie.add(seq);
//        if (rowCount < 1) {
//            throw new SQLException("nodatafound");
//        }
//        return serie;
    }
    
}
