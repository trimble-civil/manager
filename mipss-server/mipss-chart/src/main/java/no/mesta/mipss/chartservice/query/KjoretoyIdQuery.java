package no.mesta.mipss.chartservice.query;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.util.DateFormatter;

/**
 * Spørring for å finne dfu id basert på reg-, serie- eller maskinnummer
 * @author larnym
 */
public class KjoretoyIdQuery {

	private Query query;
	private EntityManager em;
	
	public KjoretoyIdQuery(MipssLoggQueryParams params, EntityManager em){
					
		this.em = em;

		StringBuilder qry = new StringBuilder();
		
		String fraTid = MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.DATE_FORMAT);
		String tilTid = MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.DATE_FORMAT);
		String regNr = params.getRegNr();
		if (params.getRegNrValgt()) {
			regNr = params.getSokeTekst();
		}     
        String serieNr = params.getSerieNr();
        Long maskinNr = params.getMaskinNr();
        
        if (params.getSerieNrValgt()) {
			qry.append("select dfu_id from installasjon_v ");
			qry.append("where UPPER(serienummer) = ?1 ");
			qry.append("order by aktiv_fra_dato desc");
			query = em.createNativeQuery(qry.toString());
			query.setParameter(1, serieNr.toUpperCase());
		} else{
			qry.append("select dfu_id from installasjon_v where ");
	        qry.append("(to_date(?1, 'dd/mm/YYYY') >= to_char(aktiv_fra_dato) ");
	        qry.append("and (to_date(?2, 'dd/mm/YYYY') <= to_char(aktiv_til_dato) or aktiv_til_dato is null)) ");
	        qry.append("and (UPPER(regnr) = ?3 ");
	        qry.append("OR UPPER(serienummer) = ?4 ");
	        if (maskinNr != null)
	        	qry.append("OR maskinnummer = ?5");
	        qry.append(")");
	        
	        query = em.createNativeQuery(qry.toString());
	        query.setParameter(1, fraTid);
	        query.setParameter(2, tilTid);
	        query.setParameter(3, regNr.toUpperCase());
	        query.setParameter(4, serieNr.toUpperCase());
	        if (maskinNr != null)
	        	query.setParameter(5, maskinNr);
		} 
	}
	
	public Long consumeResults() {
		try {
//			BigDecimal dfu = (BigDecimal) query.getSingleResult();
			List<BigDecimal> results = query.getResultList();
			BigDecimal dfu = results.get(0);
	        return dfu.longValue();
		} catch (NoResultException e) {
			return null;	
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}	
    }
}
