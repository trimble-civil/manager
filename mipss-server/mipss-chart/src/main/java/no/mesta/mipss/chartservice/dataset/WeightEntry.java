package no.mesta.mipss.chartservice.dataset;

public class WeightEntry {
    public static final int WEIGHT_STATIC = 0;
    public static final int WEIGHT_DYNAMIC = 1;
    
    private int weightType;
    private String subChartName;
    private double categoryCount;
    
    public WeightEntry(String subChartName, int weightType, double categoryCount) {
        this.weightType = weightType;
        this.subChartName = subChartName;
        this.categoryCount = categoryCount;
    }


    public void setWeightType(int weightType) {
        this.weightType = weightType;
    }

    public int getWeightType() {
        return weightType;
    }

    public void setSubChartName(String subChartName) {
        this.subChartName = subChartName;
    }

    public String getSubChartName() {
        return subChartName;
    }
    
    
    public int hashCode(){
        return subChartName.hashCode();
    }
    
    public boolean equals(Object o){
        return ((WeightEntry)o).getSubChartName().equals(getSubChartName());
    }
    
    public String toString(){
        return getSubChartName();
    }

    public void setCategoryCount(int categoryCount) {
        this.categoryCount = categoryCount;
    }

    public double getCategoryCount() {
        return categoryCount;
    }
}
