package no.mesta.mipss.chartservice.dataset;

import java.util.Date;

import org.jfree.data.xy.XYIntervalSeries;

/**
 * Klasse som benyttes for å lage en serie basert på prodpunktene.
 * @author larnym
 */
@SuppressWarnings("serial")
public class MipssLoggSeries extends XYIntervalSeries {
	
	private String kategori;
	private Date tilDato;
	private Date fraDato;
	private boolean avsluttet;
	
	public MipssLoggSeries(String kategori, Date fraDato) {	
		super(kategori);
		setKategori(kategori);
		setFraDato(fraDato);
		setTilDato(fraDato);
		setAvsluttet(false);
	}
	
	public String getKategori() {
		return kategori;
	}

	public void setKategori(String kategori) {
		this.kategori = kategori;
	}
	
	public Date getTilDato() {
		return tilDato;
	}
	
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	
	public Date getFraDato() {
		return fraDato;
	}
	
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	public boolean isAvsluttet() {
		return avsluttet;
	}

	public void setAvsluttet(boolean avsluttet) {
		this.avsluttet = avsluttet;
	}
}
