package no.mesta.mipss.chartservice.dataset;

import java.util.Date;

public class KjoretoyChartData extends CategoryChartData{
    private String produksjonType;
    private String farge;
    private boolean markering;
    private boolean paaKontrakt;
    
    public KjoretoyChartData(){
        
    }
    
    public KjoretoyChartData(Date startDate, Date endDate, String category, int sortering, String produksjonType, String farge, boolean paaKontrakt) {
        super(startDate, endDate, category, sortering);
        this.produksjonType = produksjonType;
        this.farge = farge;
        this.paaKontrakt = paaKontrakt;
    }

    public void setProduksjonType(String produksjonType) {
        this.produksjonType = produksjonType;
    }

    public String getProduksjonType() {
        return produksjonType;
    }

    public void setFarge(String farge) {
        this.farge = farge;
    }

    public String getFarge() {
        return farge;
    }

    public void setMarkering(boolean markering) {
        this.markering = markering;
    }

    public boolean isMarkering() {
        return markering;
    }
    
    public void setPaaKontraktString(String paaKontrakt){
    	setPaaKontrakt(paaKontrakt.equals("1")?true:false);
    }
    public void setPaaKontrakt(boolean paaKontrakt) {
        this.paaKontrakt = paaKontrakt;
    }

    public boolean isPaaKontrakt() {
        return paaKontrakt;
    }
}
