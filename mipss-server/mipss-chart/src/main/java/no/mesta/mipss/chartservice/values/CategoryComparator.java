package no.mesta.mipss.chartservice.values;

import java.util.Comparator;

import no.mesta.mipss.chartservice.dataset.CategoryChartData;

@SuppressWarnings("unchecked")
public class CategoryComparator implements Comparator{
    public CategoryComparator() {
    }

    public int compare(Object o1, Object o2) {
        CategoryChartData d1 = (CategoryChartData)((ChartSequence)o1).get(0);
        CategoryChartData d2 = (CategoryChartData)((ChartSequence)o2).get(0);
        return d1.getCategory().compareTo(d2.getCategory());
    }
}
