package no.mesta.mipss.chartservice;

import java.io.Serializable;
import java.util.Date;

/**
 * Søkeparametre som benyttes i forbindelse med opprettelse av grafer og tabell til Mipss Logg.
 * @author larnym
 */
@SuppressWarnings("serial")
public class MipssLoggQueryParams implements Serializable {
	
	private String sokeTekst;
	
	private Date fraDato;
	private Date tilDato;
	private Boolean regNrValgt = false;
	private Boolean dfuValgt = false;
	private Boolean serieNrValgt = false;
	private Boolean maskinNrValgt = false;
	
	private Long dfuId;
	private String regNr;
	private Long maskinNr;
	private String serieNr;
	
	
	public String getSokeTekst() {
		return sokeTekst;
	}
	
	public void setSokeTekst(String sokeTekst) {
		this.sokeTekst = sokeTekst.trim();
		
		//DU ER HER!!! BYGG OG DEPLOY PÅ NYTT!!!
		
		if (serieNrValgt) 
			setSerieNr(this.sokeTekst);
		else if (dfuValgt)
			setDfuId(Long.parseLong(this.sokeTekst));
		else if (regNrValgt)
			setRegNr(this.sokeTekst);
		else if (maskinNrValgt)
			setMaskinNr(Long.parseLong(this.sokeTekst));		
	}
	
	//DFU
	public Long getDfuId() {
		return dfuId;
	}

	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}
	
	//REGNR
	public String getRegNr() {
		return regNr;
	}

	public void setRegNr(String regNr) {
		this.regNr = regNr;	
	}
	
	//SERIENR
	public String getSerieNr() {
		return serieNr;
	}

	public void setSerieNr(String serieNr) {
		this.serieNr = serieNr;

	}
	
	//MASKINNR
	public Long getMaskinNr() {
		return maskinNr;
	}

	public void setMaskinNr(Long maskinNr) {
		this.maskinNr = maskinNr;
	}
	
	//DATO
	public Date getFraDato() {
		return fraDato;
	}
	
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	
	public Date getTilDato() {
		return tilDato;
	}
	
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	
	//RADIOBUTTONS		
	public Boolean getDfuValgt() {
		return dfuValgt;
	}
	
	public Boolean getRegNrValgt() {
		return regNrValgt;
	}
	
	public Boolean getSerieNrValgt() {
		return serieNrValgt;
	}
	
	public Boolean getMaskinNrValgt() {
		return maskinNrValgt;
	}
	
	public void setRegNrValgt(Boolean kjoretoyValgt) {
		this.regNrValgt = kjoretoyValgt;
		setRegNr(getSokeTekst());
		
		setMaskinNr(null);
		setDfuId(null);
		setSerieNr("");
	}
	
	public void setDfuValgt(Boolean dfuValgt) {
		this.dfuValgt = dfuValgt;
		try {
			setDfuId(Long.parseLong(getSokeTekst()));
		} catch (Exception e) {
		}
		
		setRegNr("");
		setMaskinNr(null);
		setSerieNr("");
	}

	public void setSerieNrValgt(Boolean serieNrValgt) {
		this.serieNrValgt = serieNrValgt;
		setSerieNr(getSokeTekst());
		
		setRegNr("");
		setMaskinNr(null);
		setDfuId(null);
	}

	public void setMaskinNrValgt(Boolean maskinNrValgt) {
		this.maskinNrValgt = maskinNrValgt;
		try {
			setMaskinNr(Long.parseLong(getSokeTekst()));
		} catch (Exception e) {
		}
		
		setRegNr("");
		setDfuId(null);
		setSerieNr("");
	}
}
