package no.mesta.mipss.chartservice;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.chartservice.dataset.XYMipssLoggChartData;

import org.jfree.chart.JFreeChart;


@Remote
public interface MipssCharts {
	public static final String BEAN_NAME ="MipssCharts";
	
	JFreeChart getChart(ChartQueryParams params);
	JFreeChart getKjoretoyChart(ChartQueryParams params);
	
	//MipssLogg
	List<XYMipssLoggChartData> getMipssLoggTableData(MipssLoggQueryParams params);
	JFreeChart getMipssLoggGraph(MipssLoggQueryParams params, List<XYMipssLoggChartData> data);
}
