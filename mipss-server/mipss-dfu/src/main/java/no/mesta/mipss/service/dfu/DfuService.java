package no.mesta.mipss.service.dfu;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

@Remote
public interface DfuService {
	public static final String BEAN_NAME = "DfuService";
	
	/**
	 * Søker etter et utvalg dfuer
	 * 
	 * @param arg
	 * @return
	 */
	public List<DfuInfoV> searchDfus(String arg);
	
	/**
	 * Henter et dfuindivid for administrasjon
	 * 
	 * @param id
	 * @return
	 */
	public Dfuindivid getDfuindividForAdmin(Long id);
	
	/**
	 * Henter alle Dfumodeller i basen
	 * 
	 * @return
	 */
	public List<Dfumodell> getDfumodellList();
	
	/**
	 * Returnerer en liste over kontrakter som ikke l
	 * 
	 * @return
	 */
	public List<Driftkontrakt> getListDriftkontrakt();
	
	/**
	 * Sletter en DfuKontrakt 
	 * 
	 * @param dfuKontrakt
	 */
	public void slettDfuKontrakt(DfuKontrakt dfuKontrakt);
	
	/**
	 * Lagrer et individ i basen
	 * 
	 * @param dfuindivid
	 * @return
	 */
	public Dfuindivid oppdaterDfuindivid(Dfuindivid dfuindivid, String bruker);
	
	/**
	 * Lager et nytt Dfuindivid. Dette bli ikke lagret i basen.
	 * 
	 * @return
	 */
	public Dfuindivid prepareNewDfuindivid(String bruker);
	
	/**
	 * Henter en liste over alle dfustatuser
	 * 
	 * @return
	 */
	public List<Dfustatus> getDfustatusList();
}
