package no.mesta.mipss.service.dfu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontraktPK;
import no.mesta.mipss.persistence.datafangsutstyr.Dfu_Status;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfulogg;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

@Stateless(name=DfuService.BEAN_NAME, mappedName="ejb/"+DfuService.BEAN_NAME)
public class DfuServiceBean implements DfuService {
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;

	/**{@inheritDoc}**/
	@SuppressWarnings("unchecked")
	public List<DfuInfoV> searchDfus(String arg) {
		List<DfuInfoV> retList = null;
		Query query = null;
		
		if(arg != null) {
			String argLower = arg.toLowerCase();
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			
			String queryString = "SELECT o FROM DfuInfoV o ";
			
			whereClauses.add("LOWER(o.modellNavn) LIKE :modellNavn");
			params.put("modellNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.serienummer) LIKE :serienummer");
			params.put("serienummer", "%" +  argLower + "%");
		
			whereClauses.add("LOWER(o.tlfnummer) LIKE :tlfnummer");
			params.put("tlfnummer", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.simkortnummer) LIKE :simkortnummer");
			params.put("simkortnummer", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.versjon) LIKE :versjon");
			params.put("versjon", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.statusNavn) LIKE :statusNavn");
			params.put("statusNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.regnr) LIKE :regnr");
			params.put("regnr", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.maskinnummer) LIKE :maskinnummer");
			params.put("maskinnummer", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.eksterntNavn) LIKE :eksterntNavn");
			params.put("eksterntNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.typeNavn) LIKE :typeNavn");
			params.put("typeNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.modell) LIKE :modell");
			params.put("modell", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.kontraktTekst) LIKE :kontraktTekst");
			params.put("kontraktTekst", "%" + argLower + "%");
			
			try {
				Long argLong = Long.valueOf(arg);
				whereClauses.add("o.id = :id");
				params.put("id", argLong);
			} catch (NumberFormatException e) {}
			
			query = createQueryFromString(queryString, whereClauses, params);
		} else {
			query = em.createNamedQuery(DfuInfoV.QUERY_FIND_ALL);
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		retList = query.getResultList();
		
		return retList;
	}
	
	private Query createQueryFromString(String queryString, List<String> whereClauses, Map<String, Object> params) {
		Iterator<String> whereIt = whereClauses.iterator();
		if(whereIt.hasNext()) queryString += " WHERE " + whereIt.next();
		while(whereIt.hasNext()) queryString += " OR " + whereIt.next();
		
		Query query = em.createQuery(queryString);
		for(Iterator<String> it = params.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			query.setParameter(key, params.get(key));
		}
		return query;
	}

	/**{@inheritDoc}**/
	public Dfuindivid getDfuindividForAdmin(Long id) {
		return (Dfuindivid)em.createNamedQuery(Dfuindivid.FIND_FOR_ADMIN).setParameter("id", id).setHint(QueryHints.REFRESH, HintValues.TRUE).getSingleResult();
	}

	/**{@inheritDoc}**/
	@SuppressWarnings("unchecked")
	public List<Dfumodell> getDfumodellList() {
		return em.createNamedQuery(Dfumodell.FIND_ALL).getResultList();
	}

	/**{@inheritDoc}**/
	@SuppressWarnings("unchecked")
	public List<Driftkontrakt> getListDriftkontrakt() {
		return em.createNamedQuery(Driftkontrakt.QUERY_FIND_ALL).getResultList();
	}

	/**{@inheritDoc}**/
	public void slettDfuKontrakt(DfuKontrakt dfuKontrakt) {
		if(dfuKontrakt != null && dfuKontrakt.getKontraktId() != null && dfuKontrakt.getDfuId() != null) {
			dfuKontrakt = em.find(DfuKontrakt.class, new DfuKontraktPK(dfuKontrakt.getDfuId(), dfuKontrakt.getKontraktId()));
			if(dfuKontrakt != null) {
				em.remove(dfuKontrakt);
				em.setFlushMode(FlushModeType.COMMIT);
				em.flush();
			}
		}
	}

	/**{@inheritDoc}**/
	public Dfuindivid oppdaterDfuindivid(Dfuindivid dfuindivid, String bruker) {
		dfuindivid.setEndretAv(bruker);
		dfuindivid.setEndretDato(Clock.now());
		dfuindivid = em.merge(dfuindivid);
		em.setFlushMode(FlushModeType.COMMIT);
		em.flush();
		return dfuindivid;
	}

	/**{@inheritDoc}**/
	public Dfuindivid prepareNewDfuindivid(String bruker) {
		Dfuindivid dfuindivid = new Dfuindivid();
		
		dfuindivid.setOpprettetDato(Clock.now());
		dfuindivid.setOpprettetAv(bruker);
		dfuindivid.setDfu_StatusList(new ArrayList<Dfu_Status>());
		dfuindivid.setDfuKontraktList(new ArrayList<DfuKontrakt>());
		dfuindivid.setDfuloggList(new ArrayList<Dfulogg>());
		dfuindivid.setInstallasjonList(new ArrayList<Installasjon>());
		dfuindivid.setInstallasjonList(new ArrayList<Installasjon>());
		dfuindivid.setInstallasjonList(new ArrayList<Installasjon>());
		dfuindivid.setSlettet(Boolean.FALSE);
		
		return dfuindivid;
	}

	/**{@inheritDoc}**/
	@SuppressWarnings("unchecked")
	public List<Dfustatus> getDfustatusList() {
		return em.createNamedQuery(Dfustatus.FIND_ALL).getResultList();
	}
}
