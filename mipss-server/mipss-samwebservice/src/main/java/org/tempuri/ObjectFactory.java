
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.smart.FileImport;
import org.datacontract.schemas._2004._07.smart.PointImport;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetDataResponseGetDataResult_QNAME = new QName("http://tempuri.org/", "GetDataResult");
    private final static QName _AddPointImportPointImport_QNAME = new QName("http://tempuri.org/", "pointImport");
    private final static QName _AddMapFileFileImport_QNAME = new QName("http://tempuri.org/", "fileImport");
    private final static QName _AddMapFileResponseAddMapFileResult_QNAME = new QName("http://tempuri.org/", "AddMapFileResult");
    private final static QName _AddPointImportResponseAddPointImportResult_QNAME = new QName("http://tempuri.org/", "AddPointImportResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetData }
     * 
     */
    public GetData createGetData() {
        return new GetData();
    }

    /**
     * Create an instance of {@link GetDataResponse }
     * 
     */
    public GetDataResponse createGetDataResponse() {
        return new GetDataResponse();
    }

    /**
     * Create an instance of {@link AddPointImport }
     * 
     */
    public AddPointImport createAddPointImport() {
        return new AddPointImport();
    }

    /**
     * Create an instance of {@link AddMapFile }
     * 
     */
    public AddMapFile createAddMapFile() {
        return new AddMapFile();
    }

    /**
     * Create an instance of {@link AddMapFileResponse }
     * 
     */
    public AddMapFileResponse createAddMapFileResponse() {
        return new AddMapFileResponse();
    }

    /**
     * Create an instance of {@link AddPointImportResponse }
     * 
     */
    public AddPointImportResponse createAddPointImportResponse() {
        return new AddPointImportResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetDataResult", scope = GetDataResponse.class)
    public JAXBElement<String> createGetDataResponseGetDataResult(String value) {
        return new JAXBElement<String>(_GetDataResponseGetDataResult_QNAME, String.class, GetDataResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PointImport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pointImport", scope = AddPointImport.class)
    public JAXBElement<PointImport> createAddPointImportPointImport(PointImport value) {
        return new JAXBElement<PointImport>(_AddPointImportPointImport_QNAME, PointImport.class, AddPointImport.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileImport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileImport", scope = AddMapFile.class)
    public JAXBElement<FileImport> createAddMapFileFileImport(FileImport value) {
        return new JAXBElement<FileImport>(_AddMapFileFileImport_QNAME, FileImport.class, AddMapFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "AddMapFileResult", scope = AddMapFileResponse.class)
    public JAXBElement<String> createAddMapFileResponseAddMapFileResult(String value) {
        return new JAXBElement<String>(_AddMapFileResponseAddMapFileResult_QNAME, String.class, AddMapFileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "AddPointImportResult", scope = AddPointImportResponse.class)
    public JAXBElement<String> createAddPointImportResponseAddPointImportResult(String value) {
        return new JAXBElement<String>(_AddPointImportResponseAddPointImportResult_QNAME, String.class, AddPointImportResponse.class, value);
    }

}
