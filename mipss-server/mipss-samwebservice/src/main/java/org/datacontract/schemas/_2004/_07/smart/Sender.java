
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Sender complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Sender">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="companyField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="timestampField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sender", propOrder = {
    "companyField",
    "timestampField"
})
public class Sender {

    @XmlElement(required = true, nillable = true)
    protected String companyField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestampField;

    /**
     * Gets the value of the companyField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyField() {
        return companyField;
    }

    /**
     * Sets the value of the companyField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyField(String value) {
        this.companyField = value;
    }

    /**
     * Gets the value of the timestampField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestampField() {
        return timestampField;
    }

    /**
     * Sets the value of the timestampField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestampField(XMLGregorianCalendar value) {
        this.timestampField = value;
    }

}
