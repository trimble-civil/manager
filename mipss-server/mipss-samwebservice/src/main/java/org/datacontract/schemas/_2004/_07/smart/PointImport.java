
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PointImport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PointImport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pointField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}ArrayOfPoint"/>
 *         &lt;element name="senderField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}Sender"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointImport", propOrder = {
    "pointField",
    "senderField"
})
public class PointImport {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfPoint pointField;
    @XmlElement(required = true, nillable = true)
    protected Sender senderField;

    /**
     * Gets the value of the pointField property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPoint }
     *     
     */
    public ArrayOfPoint getPointField() {
        return pointField;
    }

    /**
     * Sets the value of the pointField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPoint }
     *     
     */
    public void setPointField(ArrayOfPoint value) {
        this.pointField = value;
    }

    /**
     * Gets the value of the senderField property.
     * 
     * @return
     *     possible object is
     *     {@link Sender }
     *     
     */
    public Sender getSenderField() {
        return senderField;
    }

    /**
     * Sets the value of the senderField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sender }
     *     
     */
    public void setSenderField(Sender value) {
        this.senderField = value;
    }

}
