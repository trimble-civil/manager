
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Content complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Content">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dateFromField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dateToField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="fileContentField" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="fileNameField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fileTypeField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}FileType"/>
 *         &lt;element name="productionTypeField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Content", propOrder = {
    "dateFromField",
    "dateToField",
    "fileContentField",
    "fileNameField",
    "fileTypeField",
    "productionTypeField"
})
public class Content {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateFromField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateToField;
    @XmlElement(required = true, nillable = true)
    protected byte[] fileContentField;
    @XmlElement(required = true, nillable = true)
    protected String fileNameField;
    @XmlElement(required = true)
    protected FileType fileTypeField;
    @XmlElement(required = true, nillable = true)
    protected String productionTypeField;

    /**
     * Gets the value of the dateFromField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFromField() {
        return dateFromField;
    }

    /**
     * Sets the value of the dateFromField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFromField(XMLGregorianCalendar value) {
        this.dateFromField = value;
    }

    /**
     * Gets the value of the dateToField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateToField() {
        return dateToField;
    }

    /**
     * Sets the value of the dateToField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateToField(XMLGregorianCalendar value) {
        this.dateToField = value;
    }

    /**
     * Gets the value of the fileContentField property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFileContentField() {
        return fileContentField;
    }

    /**
     * Sets the value of the fileContentField property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFileContentField(byte[] value) {
        this.fileContentField = ((byte[]) value);
    }

    /**
     * Gets the value of the fileNameField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameField() {
        return fileNameField;
    }

    /**
     * Sets the value of the fileNameField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameField(String value) {
        this.fileNameField = value;
    }

    /**
     * Gets the value of the fileTypeField property.
     * 
     * @return
     *     possible object is
     *     {@link FileType }
     *     
     */
    public FileType getFileTypeField() {
        return fileTypeField;
    }

    /**
     * Sets the value of the fileTypeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileType }
     *     
     */
    public void setFileTypeField(FileType value) {
        this.fileTypeField = value;
    }

    /**
     * Gets the value of the productionTypeField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductionTypeField() {
        return productionTypeField;
    }

    /**
     * Sets the value of the productionTypeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductionTypeField(String value) {
        this.productionTypeField = value;
    }

}
