
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Coordinate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Coordinate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="latitudeField" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="longitudeField" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Coordinate", propOrder = {
    "latitudeField",
    "longitudeField"
})
public class Coordinate {

    protected double latitudeField;
    protected double longitudeField;

    /**
     * Gets the value of the latitudeField property.
     * 
     */
    public double getLatitudeField() {
        return latitudeField;
    }

    /**
     * Sets the value of the latitudeField property.
     * 
     */
    public void setLatitudeField(double value) {
        this.latitudeField = value;
    }

    /**
     * Gets the value of the longitudeField property.
     * 
     */
    public double getLongitudeField() {
        return longitudeField;
    }

    /**
     * Sets the value of the longitudeField property.
     * 
     */
    public void setLongitudeField(double value) {
        this.longitudeField = value;
    }

}
