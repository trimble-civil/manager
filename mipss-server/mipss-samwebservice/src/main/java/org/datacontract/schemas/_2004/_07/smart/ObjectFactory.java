
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.smart package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Priority_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "Priority");
    private final static QName _Coordinate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "Coordinate");
    private final static QName _ArrayOfPoint_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "ArrayOfPoint");
    private final static QName _PointImport_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "PointImport");
    private final static QName _Point_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "Point");
    private final static QName _Sender_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "Sender");
    private final static QName _Content_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "Content");
    private final static QName _FileImport_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "FileImport");
    private final static QName _FileType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "FileType");
    private final static QName _ArrayOfContent_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "ArrayOfContent");
    private final static QName _MessageType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Smart.Xsd", "MessageType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.smart
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Content }
     * 
     */
    public Content createContent() {
        return new Content();
    }

    /**
     * Create an instance of {@link ArrayOfContent }
     * 
     */
    public ArrayOfContent createArrayOfContent() {
        return new ArrayOfContent();
    }

    /**
     * Create an instance of {@link Coordinate }
     * 
     */
    public Coordinate createCoordinate() {
        return new Coordinate();
    }

    /**
     * Create an instance of {@link ArrayOfPoint }
     * 
     */
    public ArrayOfPoint createArrayOfPoint() {
        return new ArrayOfPoint();
    }

    /**
     * Create an instance of {@link Sender }
     * 
     */
    public Sender createSender() {
        return new Sender();
    }

    /**
     * Create an instance of {@link PointImport }
     * 
     */
    public PointImport createPointImport() {
        return new PointImport();
    }

    /**
     * Create an instance of {@link FileImport }
     * 
     */
    public FileImport createFileImport() {
        return new FileImport();
    }

    /**
     * Create an instance of {@link Point }
     * 
     */
    public Point createPoint() {
        return new Point();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Priority }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "Priority")
    public JAXBElement<Priority> createPriority(Priority value) {
        return new JAXBElement<Priority>(_Priority_QNAME, Priority.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Coordinate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "Coordinate")
    public JAXBElement<Coordinate> createCoordinate(Coordinate value) {
        return new JAXBElement<Coordinate>(_Coordinate_QNAME, Coordinate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "ArrayOfPoint")
    public JAXBElement<ArrayOfPoint> createArrayOfPoint(ArrayOfPoint value) {
        return new JAXBElement<ArrayOfPoint>(_ArrayOfPoint_QNAME, ArrayOfPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PointImport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "PointImport")
    public JAXBElement<PointImport> createPointImport(PointImport value) {
        return new JAXBElement<PointImport>(_PointImport_QNAME, PointImport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Point }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "Point")
    public JAXBElement<Point> createPoint(Point value) {
        return new JAXBElement<Point>(_Point_QNAME, Point.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sender }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "Sender")
    public JAXBElement<Sender> createSender(Sender value) {
        return new JAXBElement<Sender>(_Sender_QNAME, Sender.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Content }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "Content")
    public JAXBElement<Content> createContent(Content value) {
        return new JAXBElement<Content>(_Content_QNAME, Content.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileImport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "FileImport")
    public JAXBElement<FileImport> createFileImport(FileImport value) {
        return new JAXBElement<FileImport>(_FileImport_QNAME, FileImport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "FileType")
    public JAXBElement<FileType> createFileType(FileType value) {
        return new JAXBElement<FileType>(_FileType_QNAME, FileType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfContent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "ArrayOfContent")
    public JAXBElement<ArrayOfContent> createArrayOfContent(ArrayOfContent value) {
        return new JAXBElement<ArrayOfContent>(_ArrayOfContent_QNAME, ArrayOfContent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Smart.Xsd", name = "MessageType")
    public JAXBElement<MessageType> createMessageType(MessageType value) {
        return new JAXBElement<MessageType>(_MessageType_QNAME, MessageType.class, null, value);
    }

}
