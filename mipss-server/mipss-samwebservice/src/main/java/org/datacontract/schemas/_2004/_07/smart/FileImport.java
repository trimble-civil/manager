
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FileImport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FileImport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contentField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}ArrayOfContent"/>
 *         &lt;element name="senderField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}Sender"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileImport", propOrder = {
    "contentField",
    "senderField"
})
public class FileImport {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfContent contentField;
    @XmlElement(required = true, nillable = true)
    protected Sender senderField;

    /**
     * Gets the value of the contentField property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContent }
     *     
     */
    public ArrayOfContent getContentField() {
        return contentField;
    }

    /**
     * Sets the value of the contentField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContent }
     *     
     */
    public void setContentField(ArrayOfContent value) {
        this.contentField = value;
    }

    /**
     * Gets the value of the senderField property.
     * 
     * @return
     *     possible object is
     *     {@link Sender }
     *     
     */
    public Sender getSenderField() {
        return senderField;
    }

    /**
     * Sets the value of the senderField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sender }
     *     
     */
    public void setSenderField(Sender value) {
        this.senderField = value;
    }

}
