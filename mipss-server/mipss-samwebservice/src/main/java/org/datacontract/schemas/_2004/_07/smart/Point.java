
package org.datacontract.schemas._2004._07.smart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Point complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Point">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coordinateField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}Coordinate"/>
 *         &lt;element name="messageTypeField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}MessageType"/>
 *         &lt;element name="priorityField" type="{http://schemas.datacontract.org/2004/07/Smart.Xsd}Priority"/>
 *         &lt;element name="priorityFieldSpecified" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="remarksField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subTypeField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="timestampField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="vehicleIdField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Point", propOrder = {
    "coordinateField",
    "messageTypeField",
    "priorityField",
    "priorityFieldSpecified",
    "remarksField",
    "subTypeField",
    "timestampField",
    "vehicleIdField"
})
public class Point {

    @XmlElement(required = true, nillable = true)
    protected Coordinate coordinateField;
    @XmlElement(required = true)
    protected MessageType messageTypeField;
    @XmlElement(required = true)
    protected Priority priorityField;
    protected boolean priorityFieldSpecified;
    @XmlElement(required = true, nillable = true)
    protected String remarksField;
    @XmlElement(required = true, nillable = true)
    protected String subTypeField;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestampField;
    @XmlElement(required = true, nillable = true)
    protected String vehicleIdField;

    /**
     * Gets the value of the coordinateField property.
     * 
     * @return
     *     possible object is
     *     {@link Coordinate }
     *     
     */
    public Coordinate getCoordinateField() {
        return coordinateField;
    }

    /**
     * Sets the value of the coordinateField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Coordinate }
     *     
     */
    public void setCoordinateField(Coordinate value) {
        this.coordinateField = value;
    }

    /**
     * Gets the value of the messageTypeField property.
     * 
     * @return
     *     possible object is
     *     {@link MessageType }
     *     
     */
    public MessageType getMessageTypeField() {
        return messageTypeField;
    }

    /**
     * Sets the value of the messageTypeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageType }
     *     
     */
    public void setMessageTypeField(MessageType value) {
        this.messageTypeField = value;
    }

    /**
     * Gets the value of the priorityField property.
     * 
     * @return
     *     possible object is
     *     {@link Priority }
     *     
     */
    public Priority getPriorityField() {
        return priorityField;
    }

    /**
     * Sets the value of the priorityField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Priority }
     *     
     */
    public void setPriorityField(Priority value) {
        this.priorityField = value;
    }

    /**
     * Gets the value of the priorityFieldSpecified property.
     * 
     */
    public boolean isPriorityFieldSpecified() {
        return priorityFieldSpecified;
    }

    /**
     * Sets the value of the priorityFieldSpecified property.
     * 
     */
    public void setPriorityFieldSpecified(boolean value) {
        this.priorityFieldSpecified = value;
    }

    /**
     * Gets the value of the remarksField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarksField() {
        return remarksField;
    }

    /**
     * Sets the value of the remarksField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarksField(String value) {
        this.remarksField = value;
    }

    /**
     * Gets the value of the subTypeField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubTypeField() {
        return subTypeField;
    }

    /**
     * Sets the value of the subTypeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubTypeField(String value) {
        this.subTypeField = value;
    }

    /**
     * Gets the value of the timestampField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestampField() {
        return timestampField;
    }

    /**
     * Sets the value of the timestampField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestampField(XMLGregorianCalendar value) {
        this.timestampField = value;
    }

    /**
     * Gets the value of the vehicleIdField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleIdField() {
        return vehicleIdField;
    }

    /**
     * Sets the value of the vehicleIdField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleIdField(String value) {
        this.vehicleIdField = value;
    }

}
