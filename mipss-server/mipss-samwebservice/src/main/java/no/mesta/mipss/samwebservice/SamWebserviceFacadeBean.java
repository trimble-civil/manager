package no.mesta.mipss.samwebservice;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.util.ProxyUtil;

import org.datacontract.schemas._2004._07.smart.ArrayOfContent;
import org.datacontract.schemas._2004._07.smart.ArrayOfPoint;
import org.datacontract.schemas._2004._07.smart.Content;
import org.datacontract.schemas._2004._07.smart.FileImport;
import org.datacontract.schemas._2004._07.smart.Point;
import org.datacontract.schemas._2004._07.smart.PointImport;
import org.datacontract.schemas._2004._07.smart.Sender;
import org.tempuri.ContractorService;
import org.tempuri.IContractorService;

@Stateless(name=SamWebserviceFacade.BEAN_NAME, mappedName="ejb/"+SamWebserviceFacade.BEAN_NAME)
public class SamWebserviceFacadeBean implements SamWebserviceFacade, SamWebserviceFacadeLocal {
	@EJB
	private KonfigParamLocal konfig;
	
	/** @{inheritDoc} */
	public String send(List<Content> shapeFiler){
		
		FileImport fi = createFileImport(shapeFiler);
		IContractorService webservice = initWebservice();
		String v = webservice.addMapFile(fi);
		
		return v;
	}
	/** @{inheritDoc} */
	public String sendPoint(List<Point> pointList){
		PointImport pi = createPointImport(pointList);
		IContractorService webservice = initWebservice();
		String v = webservice.addPointImport(pi);
		return v;
	}
	
	/** @{inheritDoc} */
	public String toXML(Object fi){
		StringWriter w = new StringWriter();
		try {
			JAXBContext jc = JAXBContext.newInstance("org.datacontract.schemas._2004._07.smart");
			Marshaller m = jc.createMarshaller();
			m.marshal(fi, w);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return w.toString();
	}
	
	
	private IContractorService initWebservice(){
		String wsdlLocation = konfig.hentEnForApp("studio.samwebservice", "wsdlLocation").getVerdi();
		String namespace = konfig.hentEnForApp("studio.samwebservice", "qname_namespace").getVerdi();
		String localPart = konfig.hentEnForApp("studio.samwebservice", "qname_localPart").getVerdi();
		System.out.println("konfig fetched..");
		URL baseUrl = ContractorService.class.getResource(".");
				
		URL url=null;
		try {
			url = new URL(baseUrl, wsdlLocation);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException("Malformed URL for webservice: baseUrl:"+baseUrl+" wsdlLocation:"+wsdlLocation);
		}
		ContractorService cs = new ContractorService(url, new QName(namespace, localPart));
		System.out.println("inited...");
		IContractorService s = cs.getWSHttpBindingIContractorService();
		return s;
	}
	
	public static void main(String[] args) {
		URL url=null;
		URL baseUrl = ContractorService.class.getResource(".");
		try {
			url = new URL(baseUrl, "http://worm.no:443/SmartWS/ContractorService.svc?wsdl");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException("Malformed URL for webservice: baseUrl:"+baseUrl);
		}
		ContractorService cs = new ContractorService(url, new QName("http://tempuri.org/", "ContractorService"));
		System.out.println("inited...");
		IContractorService s = cs.getWSHttpBindingIContractorService();
	}
	/**
	 * Lager FileImport objektet som skal sendes over webservice
	 * @param shapeFiler
	 * @return
	 */
	private FileImport createFileImport(List<Content> shapeFiler){
		FileImport fi = new FileImport();
		fi.setSenderField(createSender());
		ArrayOfContent c = new ArrayOfContent();
		c.getContent().addAll(shapeFiler);
		fi.setContentField(c);
		return fi;
	}
	
	/**
	 * Lager PointImport objektet som skal sendes over webservice
	 * @param pointList
	 * @return
	 */
	private PointImport createPointImport(List<Point> pointList){
		PointImport p = new PointImport();
		p.setSenderField(createSender());
		ArrayOfPoint c = new ArrayOfPoint();
		c.getPoint().addAll(pointList);
		p.setPointField(c);
		return p;
	}
	
	/**
	 * Oppretter et Sender element, og setter avsenderverdi
	 * @return
	 */
	private Sender createSender(){
		Sender sender = new Sender();
		sender.setCompanyField("Mesta Drift AS");
		sender.setTimestampField(createCalendar());
		return sender;
	}
	
	/**
	 * Lager en XMLGregorianCalendar med dagens dato
	 * @return
	 */
	private XMLGregorianCalendar createCalendar(){
		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlCal=null;
		try {
			xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xmlCal;
	}
}
