package no.mesta.mipss.samwebservice;

import java.util.List;

import javax.ejb.Remote;

import org.datacontract.schemas._2004._07.smart.Content;
import org.datacontract.schemas._2004._07.smart.FileImport;
import org.datacontract.schemas._2004._07.smart.Point;

@Remote
public interface SamWebserviceFacade {
	public static final String BEAN_NAME="SamWebserviceFacadeBean";
	
	String send(List<Content> shapeFiler);
	String sendPoint(List<Point> pointList);
	String toXML(Object fi);
	
}
