package no.mesta.mipss.samwebservice;

import org.datacontract.schemas._2004._07.smart.Content;
import org.datacontract.schemas._2004._07.smart.Point;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface SamWebserviceFacadeLocal {
	String send(List<Content> shapeFiler);
	String sendPoint(List<Point> pointList);
	String toXML(Object fi);
}
