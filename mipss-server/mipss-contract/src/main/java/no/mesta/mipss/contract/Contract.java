package no.mesta.mipss.contract;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
//import no.mesta.mipss.persistence.mipss1.Bil;
//import no.mesta.mipss.persistence.mipss1.Produksjonstatus;
//import no.mesta.mipss.persistence.mipss1.StrekningTid;

@Remote
public interface Contract {
    //List<Kundekontrakt> getKontrakter();
    List<Rode> getRode(long kid, long rodetypeid);
    List<Rodetype> getRodetyper();
//    List<Bil> getKjoretoy(long kontraktId);
//    List<Produksjonstatus> getProduksjonStatus(String enhetId, String tidFra, String tidTil);
//    Produksjonstatus getProduksjonStatus(String enhetId, String tid);
//    List<StrekningTid> getStrekningTider(String enhetId, String tidFra, String tidTil);
}
