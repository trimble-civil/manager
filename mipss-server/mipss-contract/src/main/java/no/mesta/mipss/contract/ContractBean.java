package no.mesta.mipss.contract;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;

@Stateless(name="ContractBean" , mappedName="ejb/ContractBean")
public class ContractBean implements Contract {
//    @PersistenceContext(unitName="mipss1Persistence")
//    EntityManager em;
    
    public ContractBean() {
    }
    
/*    public List<Kundekontrakt> getKontrakter() {
        return em.createQuery("select o from Kundekontrakt o order by o.kontraktnummer").getResultList();
    }
  */  
    @SuppressWarnings("unchecked")
	public List<Rode> getRode(long kid, long rodetypeid){
    	throw new RuntimeException("not in use..");
//        return em.createQuery("select o from Rode o where o.kundekontrakt.kundekontraktId=:kid and o.rodetype.rodetypeId=:rid")
//            .setParameter("kid", kid)
//            .setParameter("rid", rodetypeid)
//            .getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Rodetype> getRodetyper() {
    	throw new RuntimeException("not in use..");
//        return em.createQuery("select o from Rodetype o").getResultList();
    }
    
//    public List<Bil> getKjoretoy(long kontraktId){
//        Query q = em.createNativeQuery("select ENHET_ID from table(mesta.gis_pk.bil_liste_f(?)) order by ENHET_ID")
//                                    .setParameter(1, kontraktId);
//        return new NativeQueryWrapper<Bil>(q, Bil.class, new Class[]{String.class}, "enhetId").getWrappedList();
//    }
    
//    public Produksjonstatus getProduksjonStatus(String enhetId, String tid){
//        Query q = em.createNativeQuery("select UNIT_ID, prod_status, course, to_char(timestamp, 'dd.mm.yyyy hh24:mi:ss'), latitude, longitude from table(mesta.gis_pk.vsh_f(?, TO_TIMESTAMP(?, 'DD.MM.RRRR HH24:MI:SS.FF'), TO_TIMESTAMP(?, 'DD.MM.RRRR HH24:MI:SS.FF'), 'y', null, null)) order by timestamp")
//        .setParameter(1, enhetId).setParameter(2, tid).setParameter(3, tid);
//        return new NativeQueryWrapper<Produksjonstatus>(q, Produksjonstatus.class, new Class[]{String.class, String.class, String.class, Date.class, String.class, String.class}, "enhetId", "produksjonstatus", "retning", "tidspunkt", "latitude", "longitude").getWrappedSingleResult();
//    }
    
//    public List<Produksjonstatus> getProduksjonStatus(String enhetId, String tidFra, String tidTil){
//        Query q = em.createNativeQuery("select PROD_STATUS, to_char(timestamp, 'DD.mm.yyyy HH24:mi:ss') from table(mesta.gis_pk.vsh_f(?, TO_TIMESTAMP(?, 'DD.MM.RRRR HH24:MI:SS.FF'), TO_TIMESTAMP(?, 'DD.MM.RRRR HH24:MI:SS.FF'), 'y', null, null)) where speed is not null order by timestamp")
//        .setParameter(1, enhetId).setParameter(2, tidFra).setParameter(3, tidTil);
//        return new NativeQueryWrapper<Produksjonstatus>(q, Produksjonstatus.class, new Class[]{String.class, String.class}, "produksjonstatus", "timestamp").getWrappedList();
//    }
//    
//    public List<StrekningTid> getStrekningTider(String enhetId, String tidFra, String tidTil){
//        Query q = em.createNativeQuery("select enhet_id, to_char(tid_start, 'dd.mm.yyyy hh24:mi:ss'), to_char(tid_stopp, 'dd.mm.yyyy hh24:mi:ss') from inndata.utstyr_vei_status where enhet_id=? and tid_start >= TO_TIMESTAMP(?, 'DD.MM.RRRR HH24:MI:SS') and tid_start <= TO_TIMESTAMP(?, 'DD.MM.RRRR HH24:MI:SS') order by enhet_id, tid_start")
//        .setParameter(1, enhetId).setParameter(2, tidFra).setParameter(3,tidTil);
//        return new NativeQueryWrapper<StrekningTid>(q, StrekningTid.class, new Class[]{String.class, Date.class, Date.class}, "enhetId", "tidStart", "tidStopp").getWrappedList();
//    }
    //select enhet_id, tid_start, tid_stopp, fylke, kommune, vei, hparsell, km_fra, km_til, prod_status from inndata.utstyr_vei_status_v where enhet_id = 'PD17176' and tid_start >= to_date('01.01.2008 23:50:00', 'DD.MM.RRRR HH24:MI:SS') and tid_start < to_date('01.01.2008 23:59:59', 'DD.MM.RRRR HH24:MI:SS') order by enhet_id, tid_start
}
