package no.mesta.mipss.kontraktdashboardservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.common.DetaljertProduksjon;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.StroMetodeMedMengde;
import no.mesta.mipss.driftslogg.PeriodeKontrakt;
import no.mesta.mipss.query.NativeQueryWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(name=KontraktdashboardService.BEAN_NAME, mappedName="ejb/"+KontraktdashboardService.BEAN_NAME)

public class KontraktdashboardBean implements KontraktdashboardService {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;

	@Override
	public int getAntallRSkjema(Long kontraktId, Date fraDato, Date tilDato) {	
		
		try{
			
			Query r11query = em.createNativeQuery(getQueryString("skred"))
			.setParameter(1, fraDato)
			.setParameter(2, tilDato)
			.setParameter(3, kontraktId);
			
			Query r5query = em.createNativeQuery(getQueryString("forsikringsskade"))
			.setParameter(1, fraDato)
			.setParameter(2, tilDato)
			.setParameter(3, kontraktId);
			
			Query r2query = em.createNativeQuery(getQueryString("hendelse"))
			.setParameter(1, fraDato)
			.setParameter(2, tilDato)
			.setParameter(3, kontraktId);
			
			
			BigDecimal antallR11 = (BigDecimal) r11query.getSingleResult();
			BigDecimal antallR5 = (BigDecimal) r5query.getSingleResult();
			BigDecimal antallR2 = (BigDecimal) r2query.getSingleResult();
			
			return antallR11.intValue() + antallR5.intValue() + antallR2.intValue();
			
		}catch (Exception e) {
			log.error("Unable to get number of R-forms", e);
			return -1;			
		}		
	}	
		
	private String getQueryString(String type){
		
		StringBuilder query = new StringBuilder();
		
		query.append("select count(*) from " + type + " x, sak s ");
        query.append("where x.rapportert_dato is null ");
        query.append("and x.opprettet_dato between ?1 and ?2 ");
        query.append("and x.sak_guid = s.guid ");
        query.append("and s.kontrakt_id = ?3 ");
        query.append("and x.slettet_dato is null");
        
        return query.toString();
	}

	@Override
	public Double getKjoretoyMedUkjentProdukjson(Long driftkontraktId) {
		
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT count(*) from (SELECT DISTINCT kjoretoy_id ");
		query.append("FROM tiltak ");
		query.append("WHERE fra_dato_tid > sysdate - 1 "); 
		query.append("AND prodtype_id      = 99 ");
		query.append("AND kjoretoy_id IN ");
		query.append("(SELECT kjoretoy_id FROM kontrakt_kjoretoy WHERE kontrakt_id = ?1) ");
		query.append("GROUP BY kjoretoy_id ");
		query.append("HAVING SUM((til_dato_tid - fra_dato_tid)*24*60) > 5)");

		Query ukjentProdQuery = em.createNativeQuery(query.toString())
		.setParameter(1, driftkontraktId);

		try {
			BigDecimal antKjoretoy = (BigDecimal) ukjentProdQuery.getSingleResult();
			return antKjoretoy.doubleValue();
		} catch (NoResultException e) {
			return 0.0;
		}
		
	}

	@Override
	public Double getKjoretoyMedUkjentStrometode(Long driftkontraktId) {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT count(*) from (select DISTINCT kjoretoy_id ");
		query.append("FROM tiltak ");
		query.append("WHERE fra_dato_tid > sysdate - 1 "); 
		query.append("AND stroprodukt_id   = 99 ");
		query.append("AND kjoretoy_id IN ");
		query.append("(SELECT kjoretoy_id FROM kontrakt_kjoretoy WHERE kontrakt_id = ?1) ");
		query.append("GROUP BY kjoretoy_id ");
		query.append("HAVING SUM((til_dato_tid - fra_dato_tid)*24*60) > 5)");

		Query ukjentStroQuery = em.createNativeQuery(query.toString())
		.setParameter(1, driftkontraktId);

		try{
			BigDecimal antKjoretoy = (BigDecimal) ukjentStroQuery.getSingleResult();
			return antKjoretoy.doubleValue();
		}
		catch (NoResultException e) {
			return 0.0;
		}
	}

	public Double getAntallAvvikMedStatusNyttavvik(Long driftkontraktId) {

		StringBuilder query = new StringBuilder();

		query.append("select count (*) ");
		query.append("from sak s ");
		query.append("join avvik av on av.sak_guid = s.guid "); 
		query.append("join avvik_status avst on avst.avvik_guid = av.guid ");
		query.append("where s.kontrakt_id = ?1 ");
		query.append("and avst.opprettet_dato = (select max (opprettet_dato) from avvik_status ");
		query.append("where avvik_guid = av.guid) ");
		query.append("and avst.status_id = 1");

		Query nyttavvikQuery = em.createNativeQuery(query.toString())
		.setParameter(1, driftkontraktId);

		try{
			BigDecimal antNyttAvvik = (BigDecimal) nyttavvikQuery.getSingleResult();
			return antNyttAvvik.doubleValue();
		}
		catch (NoResultException e) {
			return 0.0;
		}
	}	
	
	public Double getAntallIkkeProdsatteBestillinger(Long driftkontraktId) {
		
		StringBuilder query = new StringBuilder();
		
		query.append("select count (*) ");
		query.append("from kjoretoyordre o join kjoretoyordre_status s on s.ordre_id = o.id ");
		query.append("where     o.kontrakt_id = ?1 "); 
		query.append("and o.type_navn = 'Nymontering' ");
		query.append("and s.opprettet_dato = (select max (opprettet_dato) ");
		query.append("from kjoretoyordre_status ");
		query.append("where ordre_id = o.id) ");
		query.append("and status_ident = '30'");
	
		
		Query ikkeProdsattQuery = em.createNativeQuery(query.toString())
		.setParameter(1, driftkontraktId);
		
		try{
			BigDecimal antIkkeProdsatt = (BigDecimal) ikkeProdsattQuery.getSingleResult();
			return antIkkeProdsatt.doubleValue();
		}
		catch (NoResultException e) {
			return 0.0;
		}
	}
	
	
	/** {@inheritDoc} */
	public List<StroMetodeMedMengde> getMengdePrStrometode(Long driftkontraktId, Date fraDato, Date tilDato){
		ArrayList<StroMetodeMedMengde> stroMetoder = new ArrayList<StroMetodeMedMengde>();
		StringBuilder qb = new StringBuilder();
		
		//Hent ut alle benyttede strømetoder med mengder, over gitt periode...
		qb.append("select * from table(" );
		qb.append("dashbord_pk.stromengde_f(?1, ");
		qb.append("to_date(?2, 'dd.mm.yyyy hh24:mi:ss'), ");
		qb.append("to_date(?3, 'dd.mm.yyyy hh24:mi:ss')))");
		
		
		Query query = em.createNativeQuery(qb.toString());
		query.setParameter(1, driftkontraktId);
		
		String fraDatoString = MipssDateFormatter.formatDate(fraDato,MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String tilDatoString = MipssDateFormatter.formatDate(tilDato,MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		
		query.setParameter(2, fraDatoString);
		query.setParameter(3, tilDatoString);
		
		
		NativeQueryWrapper<StroMetodeMedMengde> n = new NativeQueryWrapper<StroMetodeMedMengde>(query, StroMetodeMedMengde.class, StroMetodeMedMengde.getPropertiesArray());
		
		
		
		List<StroMetodeMedMengde> resultListMendgePrStromengde = n.getWrappedList();
		return resultListMendgePrStromengde;
	}

	/** {@inheritDoc} */
	public List<DetaljertProduksjon> getDetaljertProduksjon(Long driftskontraktId, Date fraDato, Date tilDato) {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from table(");
		sb.append("dashbord_pk.prod_detaljert_f(?1, ");
		sb.append("to_date(?2, 'dd.mm.yyyy hh24:mi:ss'), ");
		sb.append("to_date(?3, 'dd.mm.yyyy hh24:mi:ss')))");

		String fraDatoString = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String tilDatoString = MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

		Query query = em.createNativeQuery(sb.toString());
		query.setParameter(1, driftskontraktId);
		query.setParameter(2, fraDatoString);
		query.setParameter(3, tilDatoString);

		NativeQueryWrapper<DetaljertProduksjon> n = new NativeQueryWrapper<DetaljertProduksjon>(
				query, DetaljertProduksjon.class, DetaljertProduksjon.getPropertiesArray());
		return n.getWrappedList();
	}

}
