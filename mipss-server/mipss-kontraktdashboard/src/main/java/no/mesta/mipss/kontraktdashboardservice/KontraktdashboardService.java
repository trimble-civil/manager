package no.mesta.mipss.kontraktdashboardservice;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.common.DetaljertProduksjon;
import no.mesta.mipss.common.StroMetodeMedMengde;
import no.mesta.mipss.driftslogg.PeriodeKontrakt;

/**
 * Tjeneste for å hente ut data som benyttes i dashboard-visningen
 * @author larnym
 */
@Remote
public interface KontraktdashboardService {
	public static final String BEAN_NAME = "KontraktdashboardBean";

	/**
	 * Metode som henter ut antall R-skjemaer(R11, R5 og R2) som finnes basert på en fra- og tildato.
	 * @param kontraktId
	 * @param fra
	 * @param til
	 * @return
	 */
	public int getAntallRSkjema(Long kontraktId, Date fra, Date til);
	
	/**
	 * Henter kjøretøy som har levert ukjent produksjon i mer enn 5 minutter.
	 * @param driftkontraktId
	 * @return
	 */
	public Double getKjoretoyMedUkjentProdukjson(Long driftkontraktId);
	
	/**
	 * Henter kjøretøy som har levert ukjent strøprodukt i mer enn 5 minutter.
	 * @param driftkontraktId
	 * @return
	 */
	public Double getKjoretoyMedUkjentStrometode(Long driftkontraktId);
	
	
	/**
	 * Henter alle avvik som har status "nytt avvik"
	 * @param driftkontraktId
	 * @return
	 */
	public Double getAntallAvvikMedStatusNyttavvik(Long driftkontraktId);
	
	/**
	 * Henter antall ordre som har status "deler sendt"
	 * @param driftkontraktId
	 * @return
	 */
	public Double getAntallIkkeProdsatteBestillinger(Long driftkontraktId);

	/**
	 * Henter ut data om strømetoder som er benyttet i perioden som dagSelector angir
	 * @param driftkontraktId
	 * @param fraDato
	 * @param tilDato
	 * @return
	 */
	public List<StroMetodeMedMengde> getMengdePrStrometode(Long driftkontraktId, Date fraDato, Date tilDato);

	/**
	 * Henter ut detaljert data om produksjon som har foregått for den gitte kontrakten i perioden som dagSelector angir
	 * @param driftskontraktId
	 * @param fraDato
	 * @param tilDato
	 * @return
	 */
	public List<DetaljertProduksjon> getDetaljertProduksjon(Long driftskontraktId, Date fraDato, Date tilDato);

}
