package no.mesta.mipss.brukerpreferanser;

public class BrukerpreferanserException extends RuntimeException {
	public BrukerpreferanserException(){
	}
	
	public BrukerpreferanserException(String message){
	}
	
	public BrukerpreferanserException(Throwable cause){
		super(cause);
	}
	
	public BrukerpreferanserException(String message, Throwable cause){
		super(message, cause);
	}
	
}
