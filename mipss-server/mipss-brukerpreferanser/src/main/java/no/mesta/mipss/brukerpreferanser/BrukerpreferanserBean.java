package no.mesta.mipss.brukerpreferanser;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.applikasjon.Brukerpreferanser;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Implementasjon av Brukerpreferanserinterface
 * @author OLEHEL
 */
@Stateless(name=BrukerpreferanserService.BEAN_NAME, mappedName="ejb/"+BrukerpreferanserService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class BrukerpreferanserBean implements BrukerpreferanserService{
	private static final Logger logger = LoggerFactory.getLogger(BrukerpreferanserBean.class);
	 @PersistenceContext(unitName="mipssPersistence")
	    EntityManager em;

	 
	@Override
	public Brukerpreferanser getPreferanserForBruker(Bruker bruker) {
		logger.debug("getPreferanserForBruker(" + bruker.getSignatur() + ")");
		Query query = em.createNamedQuery(Brukerpreferanser.QUERY_GET_FOR_BRUKER);
		query.setParameter("sig", bruker.getSignatur());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		Brukerpreferanser brukerpreferanse = null;
		try {
			brukerpreferanse = (Brukerpreferanser) query.getSingleResult();
		} catch (NoResultException e) {
			
			return null;
		}

		return brukerpreferanse;
	}

	@Override
	public Brukerpreferanser setPreferanserForBruker(Brukerpreferanser preferanser) {
		logger.debug("setPreferanserForBruker(" + preferanser.getSignatur() + " " + preferanser.getValgtKontraktId() + ")");
		Bruker bruker = new Bruker();
		bruker.setSignatur(preferanser.getSignatur());
		return em.merge(preferanser);
		
	}
}


