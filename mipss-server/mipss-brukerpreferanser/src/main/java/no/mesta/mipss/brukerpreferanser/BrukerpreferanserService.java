/**
 * 
 */
package no.mesta.mipss.brukerpreferanser;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.applikasjon.Brukerpreferanser;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

/**
 * Bean-interface for å bruke brukerpreferanser
 * @author OLEHEL
 */
@Remote
public interface BrukerpreferanserService {
	 public static final String BEAN_NAME = "BrukerpreferanserBean";
	 	/**
	 	 * Henter ut preferanseobjekt for gitt bruker
	 	 * @param bruker
	 	 * @return
	 	 */
	 	Brukerpreferanser getPreferanserForBruker(Bruker bruker);
	 	
	 	/**
	 	 * Setter preferanseverdier for tilhørende bruker.
	 	 * @param preferanser
	 	 * @return
	 	 */
	 	Brukerpreferanser setPreferanserForBruker(Brukerpreferanser preferanser);
	    
}
