package no.mesta.mipss.driftslogg;

import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.driftslogg.rapport.*;
import no.mesta.mipss.driftslogg.util.AgressoHelper;
import no.mesta.mipss.driftslogg.util.CalendarHelper;
import no.mesta.mipss.driftslogg.util.Resources;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.driftslogg.*;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.*;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.query.Rapp1Query;
import no.mesta.mipss.produksjonsrapport.query.Rapp1Query.Rapp1Type;
import no.mesta.mipss.query.AndOrBuilder;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.util.EmailHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static no.mesta.mipss.common.TripProcessStatus.*;
import static no.mesta.mipss.persistence.kjoretoy.KjoretoyRode.FIND_BY_KONTRAKT_AND_KJORETOY_AND_RODETILKNYTNINGSTYPE;
import static no.mesta.mipss.persistence.kjoretoy.KjoretoyRode.FIND_BY_KONTRAKT_AND_RODETILKNYTNINGSTYPE;

@Stateless(name=DriftsloggService.BEAN_NAME, mappedName="ejb/"+DriftsloggService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class DriftsloggBean implements DriftsloggService{

	private static final Logger log = LoggerFactory.getLogger(DriftsloggBean.class);

	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	@EJB private KonfigParamLocal konfig;
	@EJB private Rodegenerator rodeBean;

	private static final Predicate<Aktivitet> transferToAgressoPredicate = a -> {
		return a.getTrip().getProcessStatus() == PROCESSED_BY_CONTRACTOR
				&& a.getTrip().getPeriod().getStatus().getStatusType() == PeriodeStatus.StatusType.CLOSED
				&& a.isApprovedUE()
				&& a.getGodkjentMengde()!=null;
	};

	void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Leverandor> getLeverandorer(Long driftskontraktId) {
		Query query = em.createNamedQuery(Leverandor.FIND_BY_DRIFTSKONTRAKT).setParameter("kontraktId", driftskontraktId);
		try{
			List<Leverandor> resultList = query.getResultList();
			return resultList;
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<KontraktLeverandor> getKontraktLeverandorer(Long driftskontraktId) {
		Query query = em.createNamedQuery(KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT).setParameter("id", driftskontraktId);
		try{
			List<KontraktLeverandor> resultList = query.getResultList();
			return resultList;
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Levkontrakt> getKontrakterForLeverandor(String leverandorNr) {
		Query query = em.createNamedQuery(Levkontrakt.FIND_BY_LEVERANDOR_NR).setParameter("leverandorNr", leverandorNr);
		try{
			List<Levkontrakt> resultList = query.getResultList();
			return resultList;
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public List<FastprisStatus> sjekkFastpris(Driftkontrakt driftsKontrakt, Long mnd, Long aar) {
		String fastprisnavn = konfig.hentEnForApp("studio.mipss.driftslogg", "fastprisGruppeNavn").getVerdi();
		List<Leverandor> leverandorer = getLeverandorer(driftsKontrakt.getId());

		Map<Leverandor, List<AgrArtikkelV>> fastpriserArtikler = new HashMap<>();

		for (Leverandor enLeverandor:leverandorer){
			// Kaller ny query, som henter ut artikler med status 'N' og fradato mindre enn dagens dato og tildato større eller lik dagens dato.
			List<AgrArtikkelV> artikler = sokGyldigeAgrArtikkler(driftsKontrakt, enLeverandor);
			artikler = vaskArtiklerMotUgyldigeAvtaler(artikler,enLeverandor);
			for (AgrArtikkelV agrArtikkel:artikler){//hent alle fastprisavtaler for denne leverandï¿½ren
				if (agrArtikkel.getGruppeNavn().equals(fastprisnavn)){
					List<AgrArtikkelV> leverandorens = fastpriserArtikler.get(enLeverandor);
					if (leverandorens==null){
						leverandorens = new ArrayList<AgrArtikkelV>();
						fastpriserArtikler.put(enLeverandor, leverandorens);
					}
					leverandorens.add(agrArtikkel);
				}
			}
		}

		// Vi har et map over alle leverandørers fastprisavtaler.
		Date first = CalendarHelper.getFirstDateInMonth(mnd, aar);
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);
		List<FastprisStatus> fastpriserRegistrert = new ArrayList<>();
		for (Leverandor leverandor:fastpriserArtikler.keySet()){
			List<AgrArtikkelV> list = fastpriserArtikler.get(leverandor);
			for (AgrArtikkelV agrArtikkel:list){
				List<Aktivitet> sokAktiviteter = sokAktiviteterInnenforPeriode(leverandor.getNr(), agrArtikkel.getArtikkelIdent(), agrArtikkel.getLevkontraktIdent(), first, last);
				FastprisStatus fs = new FastprisStatus();
				fs.setAktiviteter(sokAktiviteter);
				fs.setArtikkel(agrArtikkel);
				fs.setLeverandor(leverandor);
				fastpriserRegistrert.add(fs);

			}

		}

		return fastpriserRegistrert;
	}

	/**
	 * Metoden henter ut alle gyldige leverandøravtaler/kontrakter
	 * Deretter sjekkes listen av artikler, slik at ingen artikler refererer til ugyldige leverandøravtaler.
	 * Dvs, dersom et element i Artikkel-listen viser til et element som ikke finnes i leverandøravtalelisten, skal den fjernes fra artikkel-listen.
	 * TODO: Kunne kanskje vært gjort som et join i sokGyldigeAgrArtikkler()?
	 * @param artikler
	 * @param enLeverandor
	 */
	private List<AgrArtikkelV>  vaskArtiklerMotUgyldigeAvtaler(List<AgrArtikkelV> artikler, Leverandor enLeverandor) {
		List<AgrArtikkelV> vasketArtikler = new ArrayList<AgrArtikkelV>();
		List<Levkontrakt> levkontraktList = getKontrakterForLeverandor(enLeverandor.getNr());
		for (AgrArtikkelV artikkel : artikler) {
			if(levKontraktListContains(artikkel.getLevkontraktIdent(), levkontraktList)){
				vasketArtikler.add(artikkel);
			}
		}
		return vasketArtikler;
	}

	/**
	 * Sjekker om levkontraktident finnes i en liste av levkontrakter.
	 * @param levkontraktIdent
	 * @param levkontraktList
	 * @return
	 */
	private boolean levKontraktListContains(String levkontraktIdent, List<Levkontrakt> levkontraktList) {
		for (Levkontrakt levkontrakt : levkontraktList) {
			if((levkontrakt.getFraDato().before(new Date()) && (levkontrakt.getLevkontraktIdent().equals(levkontraktIdent)))){
				return true;
			}
		}
		return false;
	}

	public void removeAktivitet(Aktivitet aktivitet){
		Aktivitet toRemove = em.find(Aktivitet.class, aktivitet.getId());
		em.remove(toRemove);
	}

	public Periode sokPeriode(Date fraDato, Date tilDato, String levkontraktIdent){
		try{
		Periode p = (Periode)em.createNamedQuery(Periode.FIND_PERIODE_BY_DATE_LEVKONTRAKT)
		.setParameter("fraDato", fraDato)
		.setParameter("tilDato", tilDato)
		.setParameter("levkontraktIdent", levkontraktIdent).setHint(QueryHints.REFRESH, HintValues.TRUE).getSingleResult();
		return p;
		} catch (NoResultException e){
			return null;
		}

	}

	public List<Aktivitet> sokAktiviteter(String levNr, String artikkelIdent, String levkontraktIdent, Date fra, Date til){
		Query query = em.createNamedQuery(Aktivitet.AKTIVITET_SOK_ARTIKKEL_LEVERANDOR)
		.setParameter("fraDato", fra)
		.setParameter("tilDato", til)
		.setParameter("ident", artikkelIdent)
		.setParameter("levNr", levNr)
		.setParameter("levkontraktIdent", levkontraktIdent);
		return query.getResultList();

	}

	public List<Aktivitet> sokAktiviteterInnenforPeriode(String levNr, String artikkelIdent, String levkontraktIdent, Date fra, Date til){
		Query query = em.createNamedQuery(Aktivitet.AKTIVITET_SOK_ARTIKKEL_LEVERANDOR_KUN_FRADATO)
		.setParameter("fraDato", fra)
		.setParameter("tilDato", til)
		.setParameter("ident", artikkelIdent)
		.setParameter("levNr", levNr)
		.setParameter("levkontraktIdent", levkontraktIdent);
		return query.getResultList();

	}

	@Override
	public List<Aktivitet> sokAktiviteter(DriftsloggQueryParams params) {
        Date fraDato = CalendarHelper.getFirstTimeInWeek(params.getMonth());
		Date tilDato = CalendarHelper.getLastTimeInWeek(params.getMonth());

		String query = getAktiviteterQuery(params);

		Query q = em.createQuery(query)
		.setHint(QueryHints.REFRESH, HintValues.TRUE)
		.setParameter("fraDato", fraDato)
		.setParameter("tilDato", tilDato)
		.setParameter("driftkontraktId", params.getKontrakt().getId());
		if (params.getLeverandor()!=null){
			q.setParameter("leverandorNr", params.getLeverandor().getNr());
		}
		if (params.getLevkontrakt()!=null){
			q.setParameter("levkontraktIdent", params.getLevkontrakt().getLevkontraktIdent());
		}
		if (params.getKjoretoy()!=null){
			q.setParameter("kjoretoyId", params.getKjoretoy().getId());
		}

		List<Aktivitet> resultList = q.getResultList();
		//Cache for ï¿½ slippe ï¿½ hente disse for hver aktivitet
		Map<Long, String> rodeNavnCache = new HashMap<Long, String>();
		Map<Long, String> kjoretoyNavnCache = new HashMap<Long, String>();
		Map<String, String> levNavnCache = new HashMap<String, String>();
		Map<AgrArtikkelVPK, AgrArtikkelV> artikkelCache = new HashMap<AgrArtikkelVPK, AgrArtikkelV>();
		List<String> levIkkeOverforeList = getLeverandorNummerSomIkkeSkalOverfores();
		updateCaches(resultList, rodeNavnCache, kjoretoyNavnCache, levNavnCache, artikkelCache, levIkkeOverforeList);

		return resultList;
	}


	private String getAktiviteterQuery(DriftsloggQueryParams params) {
		AndOrBuilder andOr = new AndOrBuilder();
		StringBuilder query = new StringBuilder();
		query.append("select o from Aktivitet o ");
		if (params.hasStatus()){
			query.append("join o.periode.statuser s ");
		}
		query.append("where o.fraDatoTid >= :fraDato and o.fraDatoTid <= :tilDato ");
		query.append("and o.periode.levkontrakt.driftkontraktId=:driftkontraktId ");
		if (params.getLeverandor()!=null){
			query.append("and o.periode.levkontrakt.leverandorNr=:leverandorNr ");
		}
		if (params.getLevkontrakt()!=null){
			query.append("and o.periode.levkontraktIdent=:levkontraktIdent ");
		}
		if (params.getKjoretoy()!=null){
			query.append("and o.kjoretoyId=:kjoretoyId ");
		}
		query.append("order by o.fraDatoTid");

		return query.toString();
	}

	private String getORDataQuery() {
        StringBuilder q = new StringBuilder();
        q.append("select rode_id, rode, prodtype navn, timer, km, stroprodukt, stroproduktgruppe, tonn_torrstoff tonn, liter_losning_totalt, kg_losning ");
        q.append("from table(driftslogg_pk.mipsstall_f(?,?,?,?,?)) ");
        return q.toString();
    }

	public List<MipssRodeTiltak> searchORData(Trip trip, Long contractId) {
		long vehicleId = 0;

		if (trip.getVehicle() != null) {
			vehicleId = trip.getVehicle().getId();
		}

        Query query = em.createNativeQuery(getORDataQuery())
                .setParameter(1, contractId)
                .setParameter(2, vehicleId)
                .setParameter(3, trip.getStartTime())
                .setParameter(4, trip.getEndTime())
                .setParameter(5, 0L);//for summering 0 = ikke sum 1 = kun sum

        return executeORDataQuery(query);
    }

	public List<MipssRodeTiltak> sokMipssData(Aktivitet aktivitet, Long driftkontraktId){
		Query query = em.createNativeQuery(getORDataQuery())
			.setParameter(1, driftkontraktId)
			.setParameter(2, aktivitet.getKjoretoyId())
			.setParameter(3, aktivitet.getFraDatoTid())
			.setParameter(4, aktivitet.getTilDatoTid())
			.setParameter(5, 0L);//for summering 0 = ikke sum 1 = kun sum

        return executeORDataQuery(query);
	}

	private List<MipssRodeTiltak> executeORDataQuery(Query query) {
        try {
            List<MipssTiltak> wrappedList = new NativeQueryWrapper<>(
                    query, MipssTiltak.class,
                    "rodeId", "rode", "navn", "timer", "km", "stroprodukt", "stroproduktgruppe", "tonn",
                    "literLosningTotalt", "kgLosning").getWrappedList();

            if (wrappedList == null) {
                return null;
            }

            return MipssTiltakUtil.convertTiltakToRodeTiltak(wrappedList);
        } catch (NoResultException e){
            return null;
        }
    }

	private String getKjoretoyNavnFromId(Long kjoretoyId){
		try{
			return (String)em.createQuery("select o.eksterntNavn from Kjoretoy o where o.id=:kjoretoyId").setParameter("kjoretoyId", kjoretoyId).getSingleResult();
		} catch(Exception e) {
			log.error("Unable to find name for vehicle with id {}", kjoretoyId);
			return "";
		}
	}

	public String getRodeNavnFromId(Long rodeId){
		return (String)em.createQuery("select o.navn from Rode o where o.id=:rodeId").setParameter("rodeId", rodeId).getSingleResult();
	}

	public String getLeverandorNavnFromNr(String nr){
		return (String)em.createQuery("select o.navn from Leverandor o where o.nr=:nr").setParameter("nr", nr).getSingleResult();
	}
	public AgrArtikkelV getArtikkelViewFromAktivitet(Aktivitet a){
		return em.find(AgrArtikkelV.class, new AgrArtikkelVPK(a.getPeriode().getLevkontraktIdent(), a.getArtikkelIdent()));
	}
	public AgrArtikkelV getArtikkelView(String levkontraktIdent, String artikkelIdent){
		return em.find(AgrArtikkelV.class, new AgrArtikkelVPK(levkontraktIdent, artikkelIdent));
	}

	@Override
	public void persistStromengder(List<Stroproduktmengde> changedEntities) {
		for (Stroproduktmengde s:changedEntities){
			em.merge(s);
		}
	}

	@Override
	public void persistAktiviteter(List<Aktivitet> changedEntities) {
		for (Aktivitet a:changedEntities){
			em.merge(a);
		}
	}

	@Override
    public Trip persistTrip(Trip trip) {
	    return em.merge(trip);
    }

	/**
	 * Henter nï¿½kkeltall for perioden.
	 *
	 * rodeId og kjoretoyId kan vï¿½re null.
	 */
	public Nokkeltall hentNokkelTall(DriftsloggQueryParams params, Long rodeId, Long kjoretoyId){
		Long rodetypeId = Long.valueOf(konfig.hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
		Long tomkjoringId = Long.valueOf(98);

		ProdrapportParams p = new ProdrapportParams();
		p.setDriftkontraktId(params.getKontrakt().getId());
		p.setRodetypeId(rodetypeId);
		p.setRodeId(rodeId);
		p.setFraDato(CalendarHelper.getFirstTimeInWeek(params.getMonth()));
		p.setTilDato(CalendarHelper.getLastTimeInWeek(params.getMonth()));
		p.setKunKontraktKjoretoy(true);

		Rapp1Query q = new Rapp1Query(p, em, Rapp1Type.LEVERANDOR);
		List<Rapp1> results = q.getResults();
		double kmMipss = 0;
		double kmTomMipss = 0;
		double sekundMipss = 0;
		double sekundTomMipss = 0;
		double tonnTorrMipss = 0;

		for (Rapp1 r:results){
			double tomkjoringKm=0;
			double tomkjoringTimer=0;
			if (r.getLeverandorNr().equals(Long.valueOf(params.getLeverandor().getNr()))){//riktig leverandï¿½r
				if (kjoretoyId!=null){
					if (!kjoretoyId.equals(r.getKjoretoyId())){//kun valgte kjï¿½retï¿½y, hvis valgt.
						continue;
					}
				}
				if (r.getKjortKm()!=null)
					kmMipss+=r.getKjortKm();
				if (r.getKjortSekund()!=null)
					sekundMipss+=r.getKjortSekund();
				for (R12Prodtype prod : r.getProdtyper()){
					if (prod.getId().equals(tomkjoringId)){
						if (prod.getKjortKm()!=null)
							tomkjoringKm +=prod.getKjortKm();
						if (prod.getKjortSekund()!=null)
							tomkjoringTimer+=prod.getKjortSekund();
					}
				}
				kmMipss -= tomkjoringKm;
				sekundMipss -= tomkjoringTimer;
				kmTomMipss +=tomkjoringKm;
				sekundTomMipss +=tomkjoringTimer;

				for (R12Stroprodukt stro:r.getStroprodukter()){
					if (stro.getMengdeTonn()!=null){
						tonnTorrMipss += stro.getMengdeTonn();
					}
				}
			}
		}
		List<Aktivitet> aktiviteter = sokAktiviteter(params);
		double tonnTorrLev = 0;
		double kmLev = 0;
		double timerLev = 0;
		double tonnTorrGodkjent = 0;
		double kmGodkjent = 0;
		double timerGodkjent = 0;
		List<Aktivitet> aktuelle = new ArrayList<Aktivitet>();
		for (Aktivitet a:aktiviteter){
			if (rodeId!=null&&!rodeId.equals(a.getRodeId())){
				continue;
			}
			if (kjoretoyId!=null&&!kjoretoyId.equals(a.getKjoretoyId())){
				continue;
			}

			if (a.getMeldtKm()!=null){
				kmLev += a.getMeldtKm();
			}
			if (a.getOmforentKm()!=null){
				kmGodkjent += a.getOmforentKm();
			}
			if (a.getEnhet().toLowerCase().startsWith("tim")){
				if (a.getMeldtMengde()!=null){
					timerLev += a.getMeldtMengde();
				}
				if (a.getOmforentMengde()!=null){
					timerGodkjent += a.getOmforentMengde();
				}
			}
			for (Stroproduktmengde sm:a.getStroprodukter()){
				double tonnTorr = 0;
				double tonnGodkjent = 0;

				if (sm.getMeldtMengdeTort()!=null){
					tonnTorr = sm.getMeldtMengdeTort();
				}
				if (sm.getMeldtMengdeVaatt()!=null){
					tonnTorr += tonnTorr(sm.getEnhetVaatt(), sm.getMeldtMengdeVaatt());
				}

				if (sm.getOmforentMengdeTort()!=null){
					tonnGodkjent=sm.getOmforentMengdeTort();
				}
				if (sm.getOmforentMengdeVaatt()!=null){
					tonnGodkjent+=tonnTorr(sm.getEnhetVaatt(), sm.getOmforentMengdeVaatt());
				}
				tonnTorrLev +=tonnTorr;
				tonnTorrGodkjent += tonnGodkjent;
			}
			aktuelle.add(a);
		}

		Nokkeltall tall = new Nokkeltall();
		tall.setKmMipss(kmMipss);
		tall.setSekundMipss(sekundMipss);
		tall.setKmTomMipss(kmTomMipss);
		tall.setSekundTomMipss(sekundTomMipss);
		tall.setTonnTorrMipss(tonnTorrMipss);
		tall.setTonnTorrLev(tonnTorrLev/1000d);
		tall.setKmLev(kmLev);
		tall.setTimerLev(timerLev);
		tall.setAktiviteter(aktuelle);
		tall.setKmGodkjent(kmGodkjent);
		tall.setTimerGodkjent(timerGodkjent);
		tall.setTonnTorrGodkjent(tonnTorrGodkjent/1000d);
		return tall;
	}

	private double tonnTorr(String enhetVatt, double mengde){
		if ("Liter".equals(enhetVatt)){
			mengde = mengde* ManagerConstants.WEIGHT_VOLUME_FACTOR;
		}
		mengde = mengde*0.23;
		return mengde;
	}


	public List<PeriodeSok> sokPeriodeStatus(DriftsloggQueryParams params){
		StringBuilder query = new StringBuilder();
		query.append("select o from Periode o ");
		query.append("where :uke between o.fraDato and o.tilDato ");
		if (params.getLeverandor()!=null){
			query.append("and o.levkontrakt.leverandorNr=:leverandorNr ");
		}
		if (params.getLevkontrakt()!=null){
			query.append("and o.levkontraktIdent=:levkontraktIdent ");
		}
		query.append("and o.levkontrakt.driftkontraktId=:driftkontraktId ");
		query.append("order by o.fraDato");


		Query q = em.createQuery(query.toString())
		.setHint(QueryHints.REFRESH, HintValues.TRUE)
		.setParameter("uke", params.getMonth())
		.setParameter("driftkontraktId", params.getKontrakt().getId());
		if (params.getLeverandor()!=null){
			q.setParameter("leverandorNr", params.getLeverandor().getNr());
		}
		if (params.getLevkontrakt()!=null){
			q.setParameter("levkontraktIdent", params.getLevkontrakt().getLevkontraktIdent());
		}
		List<Periode> resultList = q.getResultList();
		List<PeriodeSok> result = new ArrayList<PeriodeSok>();

		periodeToPeriodeSok(resultList, result);
		return result;
	}

	@Override
	public List<PeriodeSok> sokPeriodeStatusMedParams(Date uke, String levkontraktIdent) {
		Date first = CalendarHelper.getFirstDateInMonth(uke);
		StringBuilder query = new StringBuilder();
		query.append("select o from Periode o ");
		query.append("where :uke between o.fraDato and o.tilDato ");
		query.append("and o.levkontraktIdent=:levkontraktIdent ");
		query.append("order by o.fraDato");

		Query q = em.createQuery(query.toString())
		.setHint(QueryHints.REFRESH, HintValues.TRUE)
		.setParameter("uke", first)
		.setParameter("levkontraktIdent", levkontraktIdent);

		List<Periode> resultList = q.getResultList();
		List<PeriodeSok> result = new ArrayList<PeriodeSok>();

		periodeToPeriodeSok(resultList, result);
		return result;
	}

	private void periodeToPeriodeSok(List<Periode> resultList, List<PeriodeSok> result) {
		for (Periode p:resultList){
			PeriodeSok s = new PeriodeSok();
			s.setAntAktiviteter(Long.valueOf(p.getAktiviteter().size()));
			s.setId(p.getId());
			s.setKontrakt(p.getLevkontraktIdent());
			Leverandor lev = em.find(Leverandor.class, p.getLevkontrakt().getLeverandorNr());
			s.setLeverandor(lev.getNavn());
			s.setStatus(p.getStatus().getStatusType());

			result.add(s);
		}
	}

	@Override
	public List<LeverandorKontraktVO> sokPerioder(DriftsloggQueryParams params) {
        Date fraDato = CalendarHelper.getFirstTimeInWeek(params.getMonth());
		Date tilDato = CalendarHelper.getLastTimeInWeek(params.getMonth());

		StringBuilder query = new StringBuilder();

		query.append("select o from Periode o ");

		if (params.isVisAllePerioderVedGodkjenning()){ //mï¿½ ha med status for ï¿½ filtrere bort de som er godkjent.
			query.append("join o.statuser status ");
		}
		query.append("where o.fraDato <= :tilDato ");

		// hvis true, hentes alle perioder som ikke er godkjent uavhengig av leverandï¿½r.
		if (!params.isVisAllePerioderVedGodkjenning()){
			query.append("and o.tilDato >= :fraDato ");
			if (params.getLeverandor()!=null){
				query.append("and o.levkontrakt.leverandorNr=:leverandorNr ");
			}
			if (params.getLevkontrakt()!=null){
				query.append("and o.levkontraktIdent=:levkontraktIdent ");
			}
		}else{
			query.append("and status.opprettetDato=(select max(ps.opprettetDato) from PeriodeStatus ps where ps.periodeId=o.id)  ");
			query.append("and status.statusId<3 ");
		}
		query.append("and o.levkontrakt.driftkontraktId=:driftkontraktId ");
		query.append("order by o.fraDato");


		Query q = em.createQuery(query.toString())
		.setHint(QueryHints.REFRESH, HintValues.TRUE)
		.setParameter("tilDato", tilDato)
		.setParameter("driftkontraktId", params.getKontrakt().getId());


		if (!params.isVisAllePerioderVedGodkjenning()){
			q.setParameter("fraDato", fraDato);
			if (params.getLeverandor()!=null){
				q.setParameter("leverandorNr", params.getLeverandor().getNr());
			}
			if (params.getLevkontrakt()!=null){
				q.setParameter("levkontraktIdent", params.getLevkontrakt().getLevkontraktIdent());
			}
		}

		List<Periode> rList = q.getResultList();
		Map<Leverandor, List<Periode>> temp = new HashMap<Leverandor, List<Periode>>();
		for (Periode p:rList){
			p.getStatuser().size();
			if (p.getAktiviteter()!=null){
				for (Aktivitet a:p.getAktiviteter()){
					a.getStroprodukter().size();
				}
			}if (p.getAktiviteter()==null||p.getAktiviteter().isEmpty())
				continue;

			Leverandor l = new Leverandor();
			l.setNr(p.getLevkontrakt().getLeverandorNr());
			List<Periode> list = temp.get(l);
			if (list==null){
				list = new ArrayList<Periode>();
				l = getLeverandor(p.getLevkontrakt().getLeverandorNr());
				temp.put(l, list);
			}
			list.add(p);
		}
		Set<Leverandor> leverandorer = temp.keySet();
		List<LeverandorKontraktVO> kontrakter = new ArrayList<LeverandorKontraktVO>();
		for (Leverandor l:leverandorer){
			LeverandorKontraktVO vo = new LeverandorKontraktVO();
			vo.setLeverandor(l);
			vo.setKontrakter(temp.get(l));
			kontrakter.add(vo);
		}
		return kontrakter;
	}

	private Leverandor getLeverandor(String leverandorNr){
		Leverandor l = new Leverandor();
		l.setNr(leverandorNr);
		l.setNavn(getLeverandorNavnFromNr(leverandorNr));
		return l;
	}

	/**
	 * Henter kun ut gyldige artikler
	 * En forbedring slik den er i bruk i dag, ville vært et join mot leverandør, for å sjekke at artiklene peker mot gyldige leverandører også.
	 * @param dk
	 * @param leverandor
	 * @return
	 */
	private List<AgrArtikkelV> sokGyldigeAgrArtikkler(Driftkontrakt dk, Leverandor leverandor){
		String nr = dk.getKontraktnummer();
		if (nr.indexOf('-')!=-1){
			nr = nr.substring(0, nr.indexOf('-'));
		}
		nr +="%";

		StringBuilder q = new StringBuilder("select leverandor_nr, levkontrakt_ident, levkontrakt_beskrivelse, gruppe_navn, ");
		q.append("artikkel_ident, artikkel_navn, enhet, pris, fra_dato, til_dato ");
		q.append("from agr_artikkel_v ");
		q.append("where levkontrakt_ident like ?1 ");
		q.append("and leverandor_nr =?2 ");
		q.append("and artikkel_status = ?3 ");
		q.append("and fra_dato< ?4 ");
		q.append("and til_dato>= ?5 ");
		q.append("order by gruppe_navn ");

		Query query =  em.createNativeQuery(q.toString())
						.setParameter(1, nr)
						.setParameter(2, leverandor.getNr())
						.setParameter(3, 'N')
						.setParameter(4, new Date())
						.setParameter(5, new Date());

		return new NativeQueryWrapper<AgrArtikkelV>(query, AgrArtikkelV.class, "leverandorNr", "levkontraktIdent", "levkontraktBeskrivelse", "gruppeNavn", "artikkelIdent", "artikkelNavn", "enhet", "pris", "fraDato", "tilDato").getWrappedList();
	}

	@Override
	public List<AgrArtikkelV> sokAgrLevkontrakt(Driftkontrakt currentDriftkontrakt, Leverandor leverandor) {
		String nr = currentDriftkontrakt.getKontraktnummer();
		if (nr.indexOf('-')!=-1){
			nr = nr.substring(0, nr.indexOf('-'));
		}
		nr +="%";
		StringBuilder q = new StringBuilder();
		q.append("select distinct leverandor_nr, levkontrakt_ident, levkontrakt_beskrivelse, gyldig_fra_dato, gyldig_til_dato from agr_levkontrakt_v ");
		q.append("where levkontrakt_ident like ?1 ");
		q.append("and levkontrakt_ident not in ");
		q.append("	(select levkontrakt_ident from levkontrakt) ");
		q.append("and leverandor_nr = ?2 ");
		q.append("order by levkontrakt_ident ");

		Query query = em.createNativeQuery(q.toString()).setParameter(1, nr);
		query.setParameter(2, leverandor.getNr());
		return new NativeQueryWrapper<AgrArtikkelV>(query, AgrArtikkelV.class, "leverandorNr", "levkontraktIdent", "levkontraktBeskrivelse", "fraDato", "tilDato").getWrappedList();
	}

	@Override
	public List<AgrArtikkelV> getArtikler(Levkontrakt levkontrakt) {
		Query query = em.createNamedQuery(AgrArtikkelV.FIND_BY_LEV_KONTRAKT);
		query.setParameter("leverandorNr", levkontrakt.getLeverandorNr());
		query.setParameter("levkontraktIdent", levkontrakt.getLevkontraktIdent());

		return query.getResultList();
	}

	public List<Prodtype> getProdtypes() {
		Query query = em.createNamedQuery(Prodtype.FIND_ALL);

		return query.getResultList();
	}

	@Override
	public List<Samproduksjon> getSamproduksjon() {
		Query query = em.createNamedQuery(Samproduksjon.FIND_ALL);
		return query.getResultList();
	}

	public List<ArtikkelProdtype> getArtikkelProdtypeMapping(long kontraktId) {
		Query query = em.createNamedQuery(ArtikkelProdtype.FIND_BY_CONTRACT);
		query.setParameter("id", kontraktId);

		return query.getResultList();
	}

	public ArtikkelProdtype getArtikkelProdtypeMapping(String leverandorNr, long kontraktId, String levkontraktId, String artikkelId) {
		return em.find(ArtikkelProdtype.class, new ArtikkelProdtypeId(leverandorNr, kontraktId, levkontraktId, artikkelId));
	}

	public void saveArtikkelProdtypeMapping(ArtikkelProdtype mapping) {
		em.merge(mapping);
	}

	@Override
	/**
	 * Returnerer true dersom det finnes registrerte perioder pï¿½ angitte levkontrakt
	 */
	public boolean sjekkRegistrering(Levkontrakt toBeRemoved) {
		@SuppressWarnings("rawtypes")
		List resultList = em.createNativeQuery("select * from periode where levkontrakt_ident =?").setParameter(1, toBeRemoved.getLevkontraktIdent()).getResultList();
		return resultList.size()!=0;

	}

	/**
	 * Henter alle aktivitetene som skal sendes til Agresso.
	 *
	 * @param driftkontraktId
	 * @param mnd
	 * @param aar
	 * @param perioderSomManglerAttestant
	 * @return
	 */
	public List<MipssInputTreeView> getAktiviteterTilAgresso(Long driftkontraktId, Long mnd, Long aar, List<Long> perioderSomManglerAttestant, String brukerIdent){
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);
		List<PeriodeKontrakt> godkjentePerioder = getPerioder(last, driftkontraktId, perioderSomManglerAttestant);
		List<Periode> perioder = new ArrayList<Periode>();
		for (PeriodeKontrakt pk:godkjentePerioder){
			Periode periode = em.find(Periode.class, pk.getPeriodeId());
			perioder.add(periode);
		}

		return createMipssInputFromPerioder(perioder, mnd, aar, brukerIdent, transferToAgressoPredicate, a->"");
	}

	private List<MipssInputTreeView> createMipssInputFromPerioder(List<Periode> perioder, Long mnd, Long aar, String brukerIdent, Predicate<Aktivitet> tripPredicate, Function<Aktivitet, String> commentGenerator){
		AgressoHelper agr = new AgressoHelper(em);
		List<MipssInput> mipssInput = agr.createMipssInput(perioder, mnd, aar, brukerIdent, tripPredicate);
		//samle mipssinput pr leverandor
		Map<String, MipssInputTreeView> prLeverandor = groupBySupplier(mipssInput, commentGenerator);
		List<MipssInputTreeView> inputs = new ArrayList<MipssInputTreeView>();
		inputs.addAll(prLeverandor.values());
		return inputs;
	}

	private Map<String, MipssInputTreeView> groupBySupplier(List<MipssInput> mipssInput, Function<Aktivitet, String> commentGenerator) {
		Map<String, MipssInputTreeView> prLeverandor = new HashMap<>();
		List<String> levIkkeOverforeList = getLeverandorNummerSomIkkeSkalOverfores();
		for (MipssInput input:mipssInput){
			String leverandorNr = input.getAparId();
			input.setLevSkalOverfores(skalLeverandorOverfores(leverandorNr, levIkkeOverforeList)); //Settes på hvert element (aktivitet), da dette trengs i forbindelse med selve overføring av hver enkelt aktivitet til Agresso.

			MipssInputTreeView head = prLeverandor.get(leverandorNr);
			if (head==null){
				String leverandorNavn = getLeverandorNavnFromNr(leverandorNr);
				head = new MipssInputTreeView(leverandorNavn, leverandorNr);
				head.setOverforLeverandor(skalLeverandorOverfores(leverandorNr, levIkkeOverforeList)); //Settes på MipssInputTree's "hovedelement", altså det som viser leverandørnavnet.
				prLeverandor.put(leverandorNr, head);
			}
			AgrArtikkelV artikkelView = getArtikkelView(input.getContractId(), input.getArticle());
			if(artikkelView!=null) {
				addChild(input, head, artikkelView.getArtikkelNavn(), commentGenerator);
			} else if (input.getArticle() == null ) {
				addChild(input, head, "Intet oppgjor", commentGenerator);
			} else {
				log.error("Unable to find article with contract={} and article={}", input.getContractId(), input.getArticle());
			}
		}
		return prLeverandor;
	}

	private void addChild(MipssInput input, MipssInputTreeView head, String artikkelNavn, Function<Aktivitet, String> commentGenerator) {
		MipssInputTreeView view = new MipssInputTreeView(input);
		view.setArtikkel(artikkelNavn);
		view.setKommentar(commentGenerator.apply(input.getAktivitet()));
		head.addChild(view);
	}

	/**
	 * Henter alle aktivitetene som ikke skal overfï¿½res til agresso den valgte mï¿½ned
	 *
	 * Aktiviteter som har status < Godkjent
	 * Aktiviteter med enhetspris = 0
	 *
	 * @param driftkontraktId
	 * @param mnd
	 * @param aar
	 * @param perioderSomManglerAttestant
	 * @return
	 */
	//For å unngå at man driver å sjekker på leverandører som ikke skal overføres til agresso (REF MIPSS-1290), må man lage et opplegg
	//som sjekker leverandørnummer mot aktivitetene og unngår videre sjekk på disse, dersom leverandøren for aktiviteten ikke skal overføres.
	public List<MipssInputTreeView> getAktiviteterIkkeOverfores(Long driftkontraktId, Long mnd, Long aar, List<Long> perioderSomManglerAttestant, String brukerIdent){
		Map<String, MipssInputTreeView> aktiviteterPrLeverandor = new HashMap<String, MipssInputTreeView>();
		if (perioderSomManglerAttestant!=null&&!perioderSomManglerAttestant.isEmpty()){
			List<Periode> perioder = new ArrayList<Periode>();
			for (Long id:perioderSomManglerAttestant){
				Periode periode = em.find(Periode.class, id);
				perioder.add(periode);
			}
			List<MipssInputTreeView> mipssInput = createMipssInputFromPerioder(perioder, mnd, aar, brukerIdent, t->true, a->"Mangler attestant/vikar");
			mipssInput.forEach(m->getInputTreeView(aktiviteterPrLeverandor, m).addChild(m));
		}

		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);
		List<PeriodeKontrakt> allePerioder = getPerioder(last, driftkontraktId, perioderSomManglerAttestant);
		List<Periode> perioder = new ArrayList<Periode>();
		for (PeriodeKontrakt pk:allePerioder){
			Long periodeId = pk.getPeriodeId();
			Periode periode = em.find(Periode.class, periodeId);
			perioder.add(periode);
		}

		Function<Aktivitet, String> commentGenerator = a->{
			Double price = a.getEnhetsPris();
			Double roundedPrice = (Math.round(price * 100.00) / 100.00);

			String sep = "";
			StringBuilder sb = new StringBuilder();

			if(roundedPrice<0.01d) { sb.append(Resources.getResource("kommentar.price0")); sep = ", "; }
			if(a.getGodkjentMengde()==null) { sb.append(sep).append(Resources.getResource("kommentar.amount0")); sep = ", "; }
			if(!a.isApprovedUE()) { sb.append(sep).append(Resources.getResource("kommentar.notApproved")); sep = ", "; };
			if(a.getTrip().getProcessStatus()!=PROCESSED_BY_CONTRACTOR) { sb.append(sep).append(Resources.getResource("kommentar.tripNotProcessed")); sep = ", "; }
			if(a.getTrip().getPeriod().getStatus().getStatusType()!= PeriodeStatus.StatusType.CLOSED) { sb.append(sep).append(Resources.getResource("kommentar.periodOpen")); sep  =", "; };

			return sb.toString();
		};

		List<MipssInputTreeView> mipssInput = createMipssInputFromPerioder(perioder, mnd, aar, brukerIdent, transferToAgressoPredicate.negate(), commentGenerator);
		mipssInput.forEach(m->getInputTreeView(aktiviteterPrLeverandor, m).addChild(m));

		mipssInput.stream().filter(m->!m.getOverforLeverandor()).forEach(m->m.getMipssInputList().forEach(n->n.setKommentar(Resources.getResource("kommentar.noTransfer"))));

		return mipssInput;
	}

	private MipssInputTreeView getInputTreeView(Map<String, MipssInputTreeView> aktiviteterPrLeverandor, MipssInputTreeView tv) {
		MipssInputTreeView mipssInputTreeView = aktiviteterPrLeverandor.get(tv.getLeverandorNr());
		if (mipssInputTreeView==null){
            mipssInputTreeView = new MipssInputTreeView(tv.getLeverandor(), tv.getLeverandorNr());
            aktiviteterPrLeverandor.put(tv.getLeverandorNr(), mipssInputTreeView);
        }
		return mipssInputTreeView;
	}

	/**
	 * Sjekker om leverandøren skal overføres til Agresso eller ikke
	 * @param leverandorNr
	 * @return
	 */
	private boolean skalLeverandorOverfores(String leverandorNr, List<String> levIkkeOverforesList) {
		boolean skalOverfores = true;
		
		for (String leverandor : levIkkeOverforesList) {
			if (leverandor.equalsIgnoreCase(leverandorNr)) {
				skalOverfores = false;
			}
		}
		return skalOverfores;
	}
	
	private List<String> getLeverandorNrListFraKonfigparam(){
		List<String> ikkeGodkjenteLeverandorer = new ArrayList<String>();
		String leverandorer = konfig.hentEnForApp("studio.mipss.driftslogg", "dk_ikke_overfores_agresso").getVerdi();
		if (!leverandorer.contains(",")) {
			ikkeGodkjenteLeverandorer.add(leverandorer.trim());
		} else{
			ikkeGodkjenteLeverandorer.addAll(Arrays.asList(leverandorer.replaceAll(" ", "").split(",")));
		}
		return ikkeGodkjenteLeverandorer;
	}
	
	public List<String> getLeverandorNummerSomIkkeSkalOverfores() {
		return getLeverandorNrListFraKonfigparam();
	}
	
	public List<Leverandor> getLeverandorerSomIkkeSkalOverfores(Long driftkontraktId){
		
		List<Leverandor> kontraktLeverandorer = getLeverandorer(driftkontraktId);		
		List<Leverandor> leverandorer = new ArrayList<Leverandor>();
		
		for (String levnr : getLeverandorNummerSomIkkeSkalOverfores()) {		
			for (Leverandor kontraktLeverandor : kontraktLeverandorer) {
				if (kontraktLeverandor.getNr().equals(levnr)) {
					leverandorer.add(kontraktLeverandor);
				}
			}
		}		
		return leverandorer;
	}
	
	public List<PeriodeKontrakt> getStatusPerioder(Long driftkontraktId, Long mnd, Long aar){
		Date first = CalendarHelper.getFirstDateInMonth(mnd, aar);
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);
		return getPerioder(first, last, driftkontraktId, null, false);
	}

	/**
	 * Henter perioder som har status = "closed" frem til dags dato
	 * @param lastDate
	 * @param driftkontraktId
	 * @param perioderSomManglerAttestant
	 * @param ikkeGodkjent true dersom kun perioder som ikke er godkjent skal hentes
	 * @return
	 */
	public List<PeriodeKontrakt> getPerioder(Date firstDateInMonth, Date lastDate, Long driftkontraktId, List<Long> perioderSomManglerAttestant, boolean ikkeGodkjent){
		return getPerioder(firstDateInMonth, lastDate, driftkontraktId, perioderSomManglerAttestant, !ikkeGodkjent, 6);
	}

	public List<PeriodeKontrakt> getPerioder(Date lastDate, Long driftkontraktId, List<Long> perioderSomManglerAttestant){
		String periodeQuery = getPeriodeQuery(perioderSomManglerAttestant, true, "p.fra_dato <= ?", null);
		Query query = em.createNativeQuery(periodeQuery);
		query.setParameter(1, lastDate);
		query.setParameter(2, driftkontraktId);

		List<PeriodeKontrakt> perioder = new ArrayList<>();
		mapPerioder(query.getResultList(), perioder);

		return perioder;
	}

	public List<PeriodeKontrakt> getPerioder(Date firstDateInMonth, Date lastDate, Long driftkontraktId, List<Long> perioderSomManglerAttestant, boolean include, Integer... statuses){
		String periodeQuery = getPeriodeQuery(perioderSomManglerAttestant, include, "p.til_dato < ?", statuses);
		Query query = em.createNativeQuery(periodeQuery);
		query.setParameter(1, firstDateInMonth);
		query.setParameter(2, driftkontraktId);

		List<Object[]> periodeKontrakt = (List<Object[]>)query.getResultList();
		List<PeriodeKontrakt> perioder = new ArrayList<PeriodeKontrakt>();

		if (periodeKontrakt!=null){
			mapPerioder(periodeKontrakt, perioder);
		}

		periodeQuery = getPeriodeQuery(perioderSomManglerAttestant, include, "p.til_dato >= ? and p.fra_dato <= ?", statuses);
		query = em.createNativeQuery(periodeQuery);
		query.setParameter(1, firstDateInMonth);
		query.setParameter(2, lastDate);
		query.setParameter(3, driftkontraktId);
		
		List<Object[]> periodeIdsThisMonth =  (List<Object[]>)query.getResultList();
		mapPerioder(periodeIdsThisMonth, perioder);
		return perioder;
	}

	private void mapPerioder(List<Object[]> periodeKontrakt, List<PeriodeKontrakt> perioder) {
		for (Object[] b:periodeKontrakt){
            PeriodeKontrakt p = new PeriodeKontrakt();
            p.setPeriodeId(((BigDecimal)b[0]).longValue());
            p.setLevkontrakt(b[1].toString());
            perioder.add(p);
        }
	}

	private String getPeriodeQuery(List<Long> perioderSomManglerAttestant, boolean include, String whereClause, Integer... statuses) {
		StringBuilder sb = new StringBuilder();

		//henter alle perioder som er godkjente/ikke godkjente frem til den første inneværende måned.
		sb.append("select p.id, lk.levkontrakt_ident from periode p ");
		sb.append("join periode_status ps on ps.periode_id = p.id ");
		sb.append("join levkontrakt lk on p.levkontrakt_ident = lk.levkontrakt_ident ");
		sb.append("where ").append(whereClause);
		sb.append(" and ps.opprettet_dato=(select max(pst.opprettet_Dato) from Periode_Status pst where pst.periode_Id=p.id) ");
		if(statuses != null) {
			sb.append("and ps.status_id");
			if (!include) sb.append(" not");
			sb.append(" in (");
			String separator = "";
			for (Integer status : statuses) {
				sb.append(separator).append(status);
				separator = ",";
			}
			sb.append(")");
		}
		sb.append(" and exists (select * from trip where periode_id = p.id ) ");
		sb.append("and lk.driftkontrakt_id = ? ");
		if (perioderSomManglerAttestant!=null&&!perioderSomManglerAttestant.isEmpty()){
			String unntaPerioder = StringUtils.join(perioderSomManglerAttestant, ',');
			sb.append("and p.id not in ("+unntaPerioder+") ");
		}
		return sb.toString();
	}
	
	/**
	 * Returnerer true hvis brukeridenten ligger som attestant eller vikar i agresso
	 * @param brukerIdent
	 * @param levkontrakt_ident
	 * @return
	 */
	public boolean sjekkAttestering(String brukerIdent, String levkontrakt_ident){
		StringBuilder query = new StringBuilder();
		query.append("select * from agr_attestant_v where levkontrakt_ident = ?1 and (upper(attestant)=upper(?2) or upper(vikar)=upper(?2)) ");
		@SuppressWarnings("rawtypes")
		List resultList = em.createNativeQuery(query.toString()).setParameter(1, levkontrakt_ident).setParameter(2, brukerIdent).getResultList();
		return !resultList.isEmpty();
	}
	
	/**
	 * Overfï¿½rer periodene til agresso. Det som overfï¿½res er aktiviteter som er gjennomfï¿½rt innevï¿½rende mï¿½ned, samt
	 * eventuelle aktiviteter fra tidligere perioder som har fï¿½tt status=godkjent i ettertid.
	 *  
	 * OBS: Listen med periodeId'er som overfï¿½res vil inneholde aktiviteter som er for forrige mï¿½ned med status=sendt agresso. Disse aktivitetene vaskes bort.
	 *
	 * @param aktiviteter : listen med aktiviteter som skal overfï¿½res
	 * @param mnd : mï¿½neden som overfï¿½res
	 * @param aar : ï¿½ret som overfï¿½res
	 * @param brukerIdent : brukerens signatur fra rammeverket
	 */
	public int sendToIO(List<MipssInput> aktiviteter, Long mnd, Long aar, String brukerIdent){
		int totalInserted = 0;

		StringBuilder sb = new StringBuilder();
		sb.append("insert into mipss_input@AGRINT");
		sb.append("(apar_id, article, client, contract_id, line_no, order_id, unit_code, unit_price, value_1,");
		sb.append("mipss_aktivitet_id, deliv_date, text3, order_date, text4, text2) ");
		sb.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

		Set<Periode> perioder = new HashSet<>();
		String clientId = konfig.hentEnForApp("studio.mipss.driftslogg", "agressoClientId").getVerdi();
		Date overforingsTidspunkt = Clock.now();

		for (MipssInput m:aktiviteter) {
			int insertedRows = 0;
			
			String kjoretoyNavn = " ";
			Long kjoretoyId = m.getAktivitet().getKjoretoyId();
			if (kjoretoyId != null) {
				kjoretoyNavn = getKjoretoyNavnFromId(kjoretoyId);
			}
			
			if (m.skalOverfores()) {//ikke overfør der enhetspris = 0.01 eller der mengde = 0
				insertedRows = em.createNativeQuery(sb.toString())
						.setParameter(1, m.getAparId())
						.setParameter(2, m.getArticle())
						.setParameter(3, clientId)
						.setParameter(4, m.getContractId())
						.setParameter(5, m.getLineNo())
						.setParameter(6, m.getOrderId())
						.setParameter(7, m.getUnitCode())
						.setParameter(8, m.getUnitPrice())
						.setParameter(9, m.getValue1())
						.setParameter(10, m.getMipssAktivitetId())
						.setParameter(11, m.getDelivDate())
						.setParameter(12, m.getText3())
						.setParameter(13, overforingsTidspunkt)
						.setParameter(14, m.getAktivitet().getOpprettetAv())
						.setParameter(15, kjoretoyNavn)
						.executeUpdate();
				perioder.add(m.getAktivitet().getPeriode());
			}
			
			Aktivitet aktivitet = m.getAktivitet();
			aktivitet.setSendtAgresso(Boolean.TRUE);
			aktivitet.setSendtAgressoDato(overforingsTidspunkt);
			em.merge(aktivitet);

			totalInserted +=insertedRows;
		}

		PeriodestatusType statusGodkjent = em.find(PeriodestatusType.class, DriftsloggService.PERIODE_STATUSTYPE_SENT_IO_ID);

		for (Periode p : perioder) {
			PeriodeStatus sisteStatus = p.getStatus(); // legg på status sent_io hvis ikke siste status er sent_io.

			if (!sisteStatus.getStatusId().equals(DriftsloggService.PERIODE_STATUSTYPE_SENT_IO_ID)) {
				PeriodeStatus ps = createPeriodStatus(statusGodkjent, brukerIdent);
				ps.setPeriode(p);
				p.getStatuser().add(ps);
				em.merge(p);

				Query q = em.createQuery("SELECT o FROM Trip o WHERE o.period.id = :periodId")
						.setParameter("periodId", p.getId());

				List<Trip> trips = q.getResultList();

				for (Trip t : trips) {
					if (t.getProcessStatus().getLevel() == PROCESSED_BY_CONTRACTOR.getLevel()) {
						t.setProcessStatusId(SENT_TO_IO.getId());
						em.merge(t);
					}
				}
			}
		}

		return totalInserted;
	}

	public void flagActivitiesAsProcessed(List<MipssInput> activities, Long month, Long year){
	    if (activities != null) {
            Date firstDate = CalendarHelper.getFirstDateInMonth(month, year);
            Date lastDate = CalendarHelper.getLastDateInMonth(month, year);

            for (MipssInput m : activities) {
                Aktivitet activity = m.getAktivitet();

                if (activity != null && activity.getFraDatoTid() != null && activity.getTilDatoTid() != null &&
						activity.getFraDatoTid().after(firstDate) &&
						activity.getFraDatoTid().before(lastDate) &&
						activity.getTilDatoTid().after(firstDate)) {
                    activity.setSendtAgresso(Boolean.TRUE); // SENDT_AGRESSO_FLAGG = processed
                }
            }
        }
	}

	public void saveProcessedActivities(List<MipssInput> activities) {
	    if (activities != null) {
            for (MipssInput m : activities) {
                Aktivitet activity = m.getAktivitet();

                if (activity != null && activity.getSendtAgresso() != null && activity.getSendtAgresso()) {
                    em.merge(activity);
                }
            }
        }
	}

	/**
	 * Oppretter en ny periodestatus og setter statustype til sendt_til_agresso
	 * @param sendtAgr
	 * @param brukerIdent
	 * @return
	 */
	private PeriodeStatus createPeriodStatus(PeriodestatusType sendtAgr, String brukerIdent){
		PeriodeStatus ps = new PeriodeStatus();
		ps.setStatus(sendtAgr);
		ps.setOpprettetAv(brukerIdent);
		ps.setOpprettetDato(Clock.now());
		return ps;
	}
	
	@Override
	public List<Kjoretoy> getKjoretoyForLeverandor(String leverandorNr) {
		Query q = em.createNativeQuery("select id, regnr, maskinnummer, eksternt_navn from kjoretoy where leverandor_nr=? order by eksternt_navn").setParameter(1, leverandorNr);
		List<Kjoretoy> kjoretoy = new NativeQueryWrapper<Kjoretoy>(q, Kjoretoy.class, "id", "regnr", "maskinnummer", "eksterntNavn").getWrappedList();
		return kjoretoy;
	}
	
	@Override
	public List<Rode> getRodeForKontrakt(Long rodetypeId, Long driftkontraktId){
		Query q = em.createNativeQuery("select id, navn from rode where kontrakt_id = ?1 and type_id = ?2 and (sysdate < gyldig_til_dato or gyldig_til_dato is null) order by navn")
		.setParameter(1, driftkontraktId)
		.setParameter(2, rodetypeId);
		List<Rode> list = new NativeQueryWrapper<Rode>(q, Rode.class, "id", "navn").getWrappedList();
		return list;
	}
	
	public DriftsloggRapportData getRapport(DriftsloggQueryParams params, Date fraDato, Date tilDato) throws NoArtikkelException {
		List<Aktivitet> aktiviteter = em.createNamedQuery(Aktivitet.AKTIVITET_SOK_GODKJENT)
			.setParameter("fraDato", fraDato)
			.setParameter("tilDato", tilDato)
			.setParameter("driftkontraktId", params.getKontrakt().getId())
			.getResultList();
		
		ProdrapportParams p = new ProdrapportParams();
		Long rodetypeId = Long.valueOf(konfig.hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
		p.setDriftkontraktId(params.getKontrakt().getId());
		p.setRodetypeId(rodetypeId);
		p.setFraDato(fraDato);
		p.setTilDato(tilDato);
		List<Rapp1> results = new Rapp1Query(p, em, Rapp1Query.Rapp1Type.LEVERANDOR).getResults();
		
		//lag rapportdataene
		Map<String, String> leverandorNrCache = new HashMap<>();
		Map<Long, String> rodeNrCache = new HashMap<>();
		
		MengdeRapportHelper rapportHelper = new MengdeRapportHelper(this);
		Map<String, MengdeRapportObject> res = rapportHelper.getValuesFromAktiviteter(aktiviteter, leverandorNrCache);
		Map<String, MengdeRapportObject> rodeRes = rapportHelper.getRodeValuesFromAktiviteter(aktiviteter, rodeNrCache);
		rapportHelper.appendMipsstallFromRapp1(results, res);
		rapportHelper.appendMipsstallFromRapp1ToRoder(results, rodeRes);

		//R12 - rapport
		Set<Long> roder = new HashSet<>();
		for (Aktivitet a:aktiviteter){
			if (a.getRodeId()!=null){
				roder.add(a.getRodeId());
			}
		}
		List<Rode> roderIAktivitetene = new ArrayList<>();
		for (Long rodeId:roder){
			Rode r = em.find(Rode.class, rodeId);
			r.setRodeLengde(rodeBean.getRodeLengde(r));
			roderIAktivitetene.add(em.find(Rode.class, rodeId));
		}

		List<Rode> r12roder = rodeBean.getRodeForKontraktFull(params.getKontrakt().getId(), 96L, true, true);//96=r12 roder
		RodefordelingsHelper fordeling = new RodefordelingsHelper();
		//r12roder er detached, roderIAktivitetene er managed, skal dette beregnes annet sted mï¿½ noe mer data fetches for roderIAktivitet
		List<Fordelingsrode> fordelingsnokler = fordeling.fordel(r12roder, roderIAktivitetene);

		//fordel verdiene fra aktivitetene over pï¿½ r12-roder
		R12RapportHelper r12Helper = new R12RapportHelper(this, rapportHelper);
		Map<String, R12RapportObject> valuesFromAktiviteter = r12Helper.getValuesFromAktiviteter(aktiviteter, fordelingsnokler, leverandorNrCache);
		
		//Hent data fra Rapp1 og slï¿½ sammen med data fra aktivitetene
		Long r12RodetypeId = Long.valueOf(konfig.hentEnForApp("FELLES", "r12_rodetype_id").getVerdi());
		p.setRodetypeId(r12RodetypeId);
		List<Rapp1> r12Mipss = new Rapp1Query(p, em, Rapp1Query.Rapp1Type.LEVERANDOR).getResults();
		r12Helper.appendMipsstallFromRapp1(r12Mipss, valuesFromAktiviteter);
		
		//Flat ut map.
		List<R12RapportObject> r12Rapport = new ArrayList<>();
		for (String key:valuesFromAktiviteter.keySet()){
			R12RapportObject r12RapportObject = valuesFromAktiviteter.get(key);
			if (!r12RapportObject.isEmpty()){
				r12Rapport.add(r12RapportObject);
			}
		}
		r12Rapport.sort(new R12RapportObjectComparator());
		
		//Hent prisdata
		PrisrapportHelper pris = new PrisrapportHelper(this, rapportHelper);
		List<PrisrapportObject> prisrapport = pris.getValuesFromAktivitet(aktiviteter, leverandorNrCache);
		prisrapport.sort((o1, o2) -> {
			int val = o1.getLeverandor().compareTo(o2.getLeverandor());
			if (val == 0) {
				val = o1.getArtikkelNavn().compareTo(o2.getArtikkelNavn());
				if (val == 0) {
					val = o1.getGruppeNavn().compareTo(o2.getGruppeNavn());
				}
			}
			return val;
		});

		return new DriftsloggRapportData(r12Rapport, prisrapport);
	}

	public List<R12RapportObject> getVinterdriftsrapport(long kontraktId, Date fraDato, Date tilDato) throws NoArtikkelException {
		List<Aktivitet> aktiviteter = em.createNamedQuery(Aktivitet.AKTIVITET_SOK_GODKJENT)
				.setParameter("fraDato", fraDato)
				.setParameter("tilDato", tilDato)
				.setParameter("driftkontraktId", kontraktId)
				.getResultList();

		// Er det en grunn til at det skal være info? For meg høres det mer ut som debug
		log.debug("Antall aktiviteter: " + aktiviteter.size());
		for (Aktivitet a : aktiviteter) {
			log.trace("Aktivitet: " + a.getId() + ", " + a.getOmforentMengde() + ", " + a.getOpprettetAv());
		}

		ProdrapportParams p = new ProdrapportParams();
		Long rodetypeId = Long.valueOf(konfig.hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
		p.setDriftkontraktId(kontraktId);
		p.setRodetypeId(rodetypeId);
		p.setFraDato(fraDato);
		p.setTilDato(tilDato);

		//lag rapportdataene
		Map<String, String> leverandorNrCache = new HashMap<String, String>();

		//R12 - rapport
		Set<Long> roder = new HashSet<Long>();
		for (Aktivitet a : aktiviteter) {
			if (a.getRodeId() != null) {
				roder.add(a.getRodeId());
			}
		}

		List<Rode> roderIAktivitetene = new ArrayList<Rode>();
		for (Long rodeId : roder) {
			Rode r = em.find(Rode.class, rodeId);
			r.setRodeLengde(rodeBean.getRodeLengde(r));
			roderIAktivitetene.add(em.find(Rode.class, rodeId));
		}

		// TODO: parameterize the active "rode" used below (last argument)
		List<Rode> stroBroyteRoder = rodeBean.getRodeForKontraktFull(kontraktId, p.getRodetypeId(), true, true); //15 = strø- og brøyteroder

		RodefordelingsHelper fordeling = new RodefordelingsHelper();
		//r12roder er detached, roderIAktivitetene er managed, skal dette beregnes annet sted må noe mer data fetches for roderIAktivitet
		List<Fordelingsrode> fordelingsnokler = fordeling.fordel(stroBroyteRoder, roderIAktivitetene);

		//fordel verdiene fra aktivitetene over på r12-roder
		R12RapportHelper r12Helper = new R12RapportHelper(this, new MengdeRapportHelper(this));
		Map<String, R12RapportObject> valuesFromAktiviteter = r12Helper.getValuesFromAktiviteter(aktiviteter, fordelingsnokler, leverandorNrCache);

		//Hent data fra Rapp1 og slå sammen med data fra aktivitetene
		List<Rapp1> r12Mipss = new Rapp1Query(p, em, Rapp1Query.Rapp1Type.LEVERANDOR).getResults();
		r12Helper.appendMipsstallFromRapp1(r12Mipss, valuesFromAktiviteter);

		//Flat ut map.
		List<R12RapportObject> r12Rapport = new ArrayList<R12RapportObject>();
		for (String key : valuesFromAktiviteter.keySet()) {
			R12RapportObject r12RapportObject = valuesFromAktiviteter.get(key);

			if (!r12RapportObject.isEmpty()) {
				r12Rapport.add(r12RapportObject);
			}
		}

		Collections.sort(r12Rapport, new R12RapportObjectComparator());

		// TODO: remove this - added to avoid getting Excel-errors when generating empty reports.
		R12RapportObject r12obj = new R12RapportObject();
		r12obj.setLeverandor("Leverandørtest");
		r12obj.setR12rode("Rodetest");
		r12obj.addBroytingKmMipss(100.0);
		r12obj.addTungHovlingKmMipss(110.1);
		r12obj.addTungHovlingTimerMipss(110.2);
		r12obj.addSidePlogKmMipss(110.3);
		r12obj.addStroingTimerMipss(120.0);
		r12obj.addTonnTorrsaltMipss(130.0);
		r12obj.addTonnSlurryMipss(140.0);
		r12obj.addSaltlosningM3Mipss(150.0);
		r12obj.addTonnBefuktetSaltMipss(160.0);
		r12obj.addTonnSaltSandMipss(170.0);
		r12obj.addTonnRenSandMipss(180.0);
		r12obj.addTonnFastsandMipss(190.0);
		r12Rapport.add(r12obj);

		return r12Rapport;
	}

	@Override
	public boolean sjekkR12(DriftsloggQueryParams queryParams) {
		Driftkontrakt kontrakt = queryParams.getKontrakt();
		Long r12RodetypeId = Long.valueOf(96);
		
		long count = (Long)em.createNamedQuery(Rode.QUERY_COUNT_BY_KONTRAKT_AND_TYPE)
		.setParameter("kontraktId", kontrakt.getId())
		.setParameter("rodetypeId", r12RodetypeId).getSingleResult();
		
		return count >0;
	}
	
	public List<Stroprodukt> getAllStroprodukter() {
		Query q = em.createNamedQuery(Stroprodukt.QUERY_FIND_ALL);
		return q.getResultList();
	}

	@Override
	public List<VaerRapportObject> getVaerRapport(DriftsloggQueryParams queryParams, Date fraDato, Date tilDato) {
		List<VaerRapportObject> vaerRapportList = new ArrayList<>();

		List<Aktivitet> aktivitetList = getAktiviteterForVaerRapport(queryParams.getKontrakt().getId(), fraDato, tilDato);

		Map<Long, String> rodeNavnCache = new HashMap<>();
		Map<Long, Kjoretoy> kjoretoyCache = new HashMap<>();
		Map<String, Leverandor> leverandorCache = new HashMap<>();

		for (Aktivitet aktivitet : aktivitetList) {
			VaerRapportObject vaerInfo = new VaerRapportObject();
			vaerInfo.setTripId(aktivitet.getTrip().getId());

			int existingVaerIndex = vaerRapportList.indexOf(vaerInfo);

			String rode = "";
			if (aktivitet.getRodeId() != null) {
				rode = rodeNavnCache.computeIfAbsent(aktivitet.getRodeId(), k -> getRodeNavnFromId(aktivitet.getRodeId()));
			}

			if (existingVaerIndex >= 0) {
				vaerInfo = vaerRapportList.get(existingVaerIndex);

				if (vaerInfo.getRodeNavn() != null && !vaerInfo.getRodeNavn().contains(rode)) {
					vaerInfo.setRodeNavn(vaerInfo.getRodeNavn() + ", \"" + rode + "\"");
				} else if (!rode.isEmpty()) {
					vaerInfo.setRodeNavn("\"" + rode + "\"");
				} else {
					vaerInfo.setRodeNavn(null);
				}
			} else {
				if (vaerInfo.getTripId() != null) {
					Trip trip = getTripFromId(vaerInfo.getTripId());
                    oppdaterVaerInfo(vaerInfo, rode, trip, kjoretoyCache, leverandorCache);
					vaerRapportList.add(vaerInfo);
				}
			}
		}

		return sorterVaerRapport(vaerRapportList);
	}

	private List<Aktivitet> getAktiviteterForVaerRapport(Long driftkontraktId, Date fraDato, Date tilDato) {
		Query query = em.createNamedQuery(Aktivitet.AKTIVITET_SOK_GODKJENT)
				.setParameter("fraDato", fraDato)
				.setParameter("tilDato", tilDato)
				.setParameter("driftkontraktId", driftkontraktId);
		return query.getResultList();
	}

    private void oppdaterVaerInfo(VaerRapportObject vaerInfo, String rode, Trip trip, Map<Long, Kjoretoy> kjoretoyCache, Map<String, Leverandor> leverandorCache) {
	    if (trip != null) {
            if (trip.getVendorId() != null) {
                Leverandor lev = leverandorCache.computeIfAbsent(trip.getVendorId(), k -> getLeverandorFromNr(trip.getVendorId()));
                vaerInfo.setLeverandor(lev != null ? lev.getNavn() : null);
            }

            if (trip.getVehicle() != null) {
                Kjoretoy kjoretoy = kjoretoyCache.computeIfAbsent(trip.getVehicleId(), k -> getKjoretoyFromId(trip.getVehicleId()));

                if (kjoretoy != null) {
                    if (kjoretoy.getEksterntNavn() != null) {
                        vaerInfo.setKjoretoy(kjoretoy.getEksterntNavn());
                    } else if (kjoretoy.getMaskinnummerString() != null) {
                        vaerInfo.setKjoretoy(kjoretoy.getMaskinnummerString());
                    } else {
                        vaerInfo.setKjoretoy("Mangler visningsnavn");
                    }
                }
            } else {
				vaerInfo.setKjoretoy("Intet/annet kj\u00F8ret\u00F8y");
			}

            vaerInfo.setRodeNavn("\"" + rode + "\"");
            vaerInfo.setFraDato(trip.getStartTime());
            vaerInfo.setTilDato(trip.getEndTime());
            vaerInfo.setVaer(trip.getWeatherTypeName());
            vaerInfo.setTemperatur(trip.getWeatherTempName());
            vaerInfo.setKommentar(trip.getTripComment());
        }
    }

    private List<VaerRapportObject> sorterVaerRapport(List<VaerRapportObject> vaerRapportList) {
		vaerRapportList.sort(Comparator
				.comparing(VaerRapportObject::getFraDato, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(VaerRapportObject::getTilDato, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(VaerRapportObject::getLeverandor, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(VaerRapportObject::getKjoretoy, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(VaerRapportObject::getRodeNavn, Comparator.nullsLast(Comparator.naturalOrder()))
		);

		return vaerRapportList;
	}

	private Trip getTripFromId(Long tripId){
		return em.find(Trip.class, tripId);
	}

	private Kjoretoy getKjoretoyFromId(Long kjoretoyId) {
		return em.find(Kjoretoy.class, kjoretoyId);
	}

	private Leverandor getLeverandorFromNr(String leverandorNr) {
		return em.find(Leverandor.class, leverandorNr);
	}

	@Override
	public List<Aktivitet> getExportToExcelEgendefRapport(DriftsloggQueryParams params, Date fraDato, Date tilDato) {
				
		String query = getAktiviteterQuery(params);
		
		Query q = em.createQuery(query)
		.setParameter("fraDato", fraDato)
		.setParameter("tilDato", tilDato)
		.setParameter("driftkontraktId", params.getKontrakt().getId());
		
		if (params.getLeverandor() != null) {
			q.setParameter("leverandorNr", params.getLeverandor().getNr());
		}		
		if (params.getLevkontrakt() != null) {
			q.setParameter("levkontraktIdent", params.getLevkontrakt().getLevkontraktIdent());
		}		
		if (params.getKjoretoy() != null) {
			q.setParameter("kjoretoyId", params.getKjoretoy().getId());
		}
		
		List<Aktivitet> resultList = q.getResultList();
		//Cache for å slippe å hente disse for hver aktivitet
		Map<Long, String> rodeNavnCache = new HashMap<Long, String>();
		Map<Long, String> kjoretoyNavnCache = new HashMap<Long, String>();
		Map<String, String> levNavnCache = new HashMap<String, String>();
		Map<AgrArtikkelVPK, AgrArtikkelV> artikkelCache = new HashMap<AgrArtikkelVPK, AgrArtikkelV>();
		List<String> levIkkeOverforeList = getLeverandorNummerSomIkkeSkalOverfores();
		updateCaches(resultList, rodeNavnCache, kjoretoyNavnCache, levNavnCache, artikkelCache, levIkkeOverforeList);

		return resultList;
	}

	private void updateCaches(List<Aktivitet> resultList, Map<Long, String> rodeNavnCache, Map<Long, String> kjoretoyNavnCache, Map<String, String> levNavnCache, Map<AgrArtikkelVPK, AgrArtikkelV> artikkelCache, List<String> levIkkeOverforeList) {
		for (Aktivitet a:resultList){
			a.getPeriodeStatus();
			if (a.getKjoretoyId()!=null){
				String kjoretoy = kjoretoyNavnCache.get(a.getKjoretoyId());
				if (kjoretoy==null){
					kjoretoy = getKjoretoyNavnFromId(a.getKjoretoyId());
					kjoretoyNavnCache.put(a.getKjoretoyId(), kjoretoy);
				}
				a.setViewKjoretoyNavn(kjoretoy);
			}
			if (a.getRodeId()!=null){
				String rode = rodeNavnCache.get(a.getRodeId());
				if (rode==null){
					rode = getRodeNavnFromId(a.getRodeId());
					rodeNavnCache.put(a.getRodeId(), rode);
				}
				a.setViewRodeNavn(rode);
			}
			a.getStroprodukter().size();

			AgrArtikkelVPK agrArtikkelVPK = new AgrArtikkelVPK(a.getPeriode().getLevkontraktIdent(), a.getArtikkelIdent());
			AgrArtikkelV agrArtikkelV = artikkelCache.get(agrArtikkelVPK);
			if (agrArtikkelV==null){
				agrArtikkelV = getArtikkelViewFromAktivitet(a);
				artikkelCache.put(agrArtikkelVPK, agrArtikkelV);
			}
			a.setViewArtikkel(agrArtikkelV);

			String levnr = a.getPeriode().getLevkontrakt().getLeverandorNr();
			String leverandor = levNavnCache.get(levnr);
			if (leverandor==null){
				leverandor = getLeverandorNavnFromNr(levnr);
				levNavnCache.put(levnr, leverandor);
			}
			a.setViewLeverandorNavn(leverandor);
			a.setSkalOverforesTilAgresso(skalLeverandorOverfores(levnr, levIkkeOverforeList));
		}
	}

	@Override
	public Stroproduktmengde getStroproduktmengdeById(Long id) {
		
		try{
			Query query = em.createNamedQuery(Stroproduktmengde.QUERY_GET_ONE)
			.setParameter("id", id);
			Stroproduktmengde stroproduktmengde = (Stroproduktmengde)query.getSingleResult();
			return stroproduktmengde;
		} catch (NoResultException e) {
			return null;
		}	
	}

	@Override
	public List<ApprovedAmountData> getApprovedAmounts(DriftsloggQueryParams queryParams, Date fraDato, Date tilDato) {
		return new ApprovedAmountsHelper(queryParams, fraDato, tilDato, em).getApprovedAmounts();
	}

	@Override
	public List<DeviantActivity> getDeviantActivities(DriftsloggQueryParams queryParams, Date fromDateTime, Date toDateTime) {
		return new DeviantActivitiesHelper(queryParams, fromDateTime, toDateTime, em).getDeviantActivities();
	}

	public List<OutsideTripData> getAmountOutsideTrip(DriftsloggQueryParams queryParams, Date fromDate, Date toDate) {
		return new OutsideTripHelper(new OutsideTripDataFetcher(queryParams, fromDate, toDate, em)).getOutsideTrip();
	}

	@Override
	public List<KjoretoyRode> getConnectionsForContract(Long contractId) {
		Query query = em.createNamedQuery(FIND_BY_KONTRAKT_AND_RODETILKNYTNINGSTYPE)
				.setParameter("kontraktId", contractId)
				.setParameter("rodetilknytningstypeId", 1L);// 1 = "Brøyterode" i RODETILKNYTNINGSTYPE-tabellen

		return query.getResultList();
	}

	@Override
	public List<Trip> searchTrips(DriftsloggQueryParams params) {
		Date fromDateTime = CalendarHelper.getFirstTimeInMonth(params.getMonth());
		Date toDateTime = CalendarHelper.getLastTimeInMonth(params.getMonth());

		return searchTrips(params, fromDateTime, toDateTime);
	}

	@Override
	public List<Trip> searchTrips(DriftsloggQueryParams params, Date fromDateTime, Date toDateTime) {
		String query = getTripsQuery(params);

		Query q = em.createQuery(query)
				.setHint(QueryHints.REFRESH, HintValues.TRUE)
				.setParameter("startTime", fromDateTime)
				.setParameter("endTime", toDateTime)
				.setParameter("driftkontraktId", params.getKontrakt().getId());

		if (params.getLeverandor() != null) {
			q.setParameter("vendorId", params.getLeverandor().getNr());
		}

		if (params.getLevkontrakt() != null) {
			q.setParameter("levkontraktIdent", params.getLevkontrakt().getLevkontraktIdent());
		}

		if (params.getKjoretoy() != null) {
			q.setParameter("vehicleId", params.getKjoretoy().getId());
		}

		List<Trip> resultList = q.getResultList();

		for (Trip t : resultList) {
			for (Aktivitet a : t.getActivities()) {
				if (a.isManuallyCreated())
					t.setManuallyCreated(true);

				if (a.isProductionOutsideContract())
					t.setProductionOutsideContract(true);

				if (a.isProductionOutsideRode())
					t.setProductionOutsideRode(true);

				if (a.isArticleChanged())
					t.setArticleChanged(true);

				if (a.hasNoPayment())
					t.setHasNoPayment(true);

				if (!a.hasNoPayment() && a.isApprovedUE() && a.getEnhetsPris() != null && a.getOmforentMengde() != null) {
					t.addAmount(a.getEnhetsPris() * a.getOmforentMengde());
				}

				t.addHours(a.getActualDuration());

				if (a.isApprovedBuild()) {
					t.addPlowingKm(a.getR12_broyting());
					t.addSidePloughKm(a.getR12_sideplog());
					t.addHeavyPlaningKm(a.getR12_hovling());

					for (Stroproduktmengde gritProductAmount : a.getStroprodukter()) {
						if (gritProductAmount.getOmforentMengdeTort() != null) {
							t.addDryGrittingKg(gritProductAmount.getOmforentMengdeTort());
						}

						if (gritProductAmount.getOmforentMengdeVaatt() != null) {
							t.addWetGrittingLiter(gritProductAmount.getOmforentMengdeVaatt());
						}
					}
				}
			}
		}

		return resultList;
	}

	private String getTripsQuery(DriftsloggQueryParams params) {
		StringBuilder query = new StringBuilder();
		AndOrBuilder andOr = new AndOrBuilder();

		query.append("SELECT o FROM Trip o ");

		query.append("WHERE ");
		query.append("   o.startTime >= :startTime ");
		query.append("   AND ");
		query.append("   o.startTime <= :endTime ");
		query.append("   AND ");
		query.append("   o.contractorContract IS NOT null ");
		query.append("   AND ");
		query.append("   o.processStatusId IS NOT null ");
		query.append("   AND ");
		query.append("   o.period IS NOT null ");
		query.append("   AND ");
		query.append("   o.contractorContract.driftkontrakt.id = :driftkontraktId ");

		if (params.getLeverandor() != null) {
			query.append("AND o.vendorId = :vendorId ");
		}

		if (params.getLevkontrakt() != null) {
			query.append("AND o.contractorContract.levkontraktIdent = :levkontraktIdent ");
		}

		if (params.getKjoretoy() != null) {
			query.append("AND o.vehicle.id = :vehicleId ");
		}

		String tripProcessStatus = "o.processStatusId = ";

		if (params.hasStatus()) {
			if (params.isRegistered()) {
				andOr.add(tripProcessStatus + "'" + RETURNED_TO_SUBCONTRACTOR.getId() + "'");
				andOr.add(tripProcessStatus + "'" + PROCESSED_BY_SUBCONTRACTOR.getId() + "'");
			}

			if (params.isSubmitted()) {
				andOr.add(tripProcessStatus + "'" + SUBMITTED_BY_SUBCONTRACTOR.getId() + "'");
				andOr.add(tripProcessStatus + "'" + UNDER_PROCESS_BY_CONTRACTOR.getId() + "'");
			}

			if (params.isProcessed()) {
				andOr.add(tripProcessStatus + "'" + PROCESSED_BY_CONTRACTOR.getId() + "'");
			}

			if (params.isPaid()) {
			    andOr.add(tripProcessStatus + "'" + SENT_TO_IO.getId() + "'");
            }
		} else {
			andOr.add(tripProcessStatus + "'" + RETURNED_TO_SUBCONTRACTOR.getId() + "'");
			andOr.add(tripProcessStatus + "'" + PROCESSED_BY_SUBCONTRACTOR.getId() + "'");
			andOr.add(tripProcessStatus + "'" + SUBMITTED_BY_SUBCONTRACTOR.getId() + "'");
			andOr.add(tripProcessStatus + "'" + UNDER_PROCESS_BY_CONTRACTOR.getId() + "'");
			andOr.add(tripProcessStatus + "'" + PROCESSED_BY_CONTRACTOR.getId() + "'");
			andOr.add(tripProcessStatus + "'" + SENT_TO_IO.getId() + "'");
		}

		query.append(andOr.toString());
		query.append(" ORDER BY o.startTime ASC");

		return query.toString();
	}

	@Override
	public List<Trip> getTripsForPeriodId(Long periodId) {
		Query q = em.createQuery("SELECT o FROM Trip o WHERE o.period.id = :periodId")
				.setHint(QueryHints.REFRESH, HintValues.TRUE)
				.setParameter("periodId", periodId);

		return q.getResultList();
	}

	@Override
	public void closePeriods(List<PeriodeStatus> periodStatuses) {
		PeriodestatusType pst = em.find(PeriodestatusType.class, 6L); // Closed

		for (PeriodeStatus periodeStatus : periodStatuses) {
			periodeStatus.setStatus(pst);
			em.persist(periodeStatus);
		}
	}

	@Override
	public void openPeriods(List<PeriodeStatus> periodStatuses) {
		PeriodestatusType pst = em.find(PeriodestatusType.class, 5L); // Open

		for (PeriodeStatus periodeStatus : periodStatuses) {
			periodeStatus.setStatus(pst);
			em.persist(periodeStatus);
		}
	}

	@Override
	public List<AgrArtikkelV> getArticlesForTrip(Trip trip) {
		List<AgrArtikkelV> articlesForTrip = new ArrayList<>();
		List<AgrArtikkelV> validArticles = sokGyldigeAgrArtikkler(trip.getContractorContract().getDriftkontrakt(), trip.getContractorContract().getLeverandor());

		for (AgrArtikkelV article : validArticles) {
			if (article.getLevkontraktIdent().equalsIgnoreCase(trip.getContractorContract().getLevkontraktIdent())) {
				articlesForTrip.add(article);
			}
		}

		return articlesForTrip;
	}

	@Override
	public Stroproduktmengde addOrUpdateGritProductAmount(Stroproduktmengde gritProductAmount) {
		return em.merge(gritProductAmount);
	}

	@Override
	public Aktivitet addOrUpdateActivity(Aktivitet aktivitet) {
		return em.merge(aktivitet);
	}

	@Override
	public TripReturnCause addTripReturnCause(TripReturnCause cause) {
		return em.merge(cause);
	}

	public void sendMail(String sender, String recipient, String subject, String message) {
		EmailHelper emailHelper = new EmailHelper(em, konfig);

		emailHelper.setFrom(sender);
		emailHelper.addRecipient(recipient);
		emailHelper.setSubject(subject);
		emailHelper.setMessage(message);

		try {
			emailHelper.sendHtmlMail();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public KontraktLeverandor getContractContractor(Long contractId, String contractorId) {
		Query query = em.createNamedQuery(KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT_LEVERANDOR)
				.setParameter("id", contractId)
				.setParameter("leverandorNr", contractorId);

		return (KontraktLeverandor) query.getSingleResult();
	}

	private class R12RapportObjectComparator implements Comparator<R12RapportObject> {
		@Override
		public int compare(R12RapportObject o1, R12RapportObject o2) {
			int val = o1.getLeverandor().compareTo(o2.getLeverandor());
			if (val == 0) {
				return o1.getR12rode().compareTo(o2.getR12rode());
			}
			return val;
		}
	}

	@Override
	public List<KjoretoyRode> getVehicleRodeConnectionsForVehicle(Long contractId, Long vehicleId) {
		Query query = em.createNamedQuery(FIND_BY_KONTRAKT_AND_KJORETOY_AND_RODETILKNYTNINGSTYPE)
				.setParameter("kontraktId", contractId)
				.setParameter("kjoretoyId", vehicleId)
				.setParameter("rodetilknytningstypeId", 1L);// 1 = "Brøyterode" i RODETILKNYTNINGSTYPE-tabellen

		return query.getResultList();
	}

	@Override
	public int getNumberOfTripsRegisteredNotSubmitted(Long contractId) {
		return getNumberOfTripsWhere(contractId,
				"(o.processStatusId IN ('2a', '2b', '3') OR (o.processStatusId IS NULL AND o.tripStatus = 'finalized'))");
	}

	@Override
	public int getNumberOfTripsSubmittedNotProcessed(Long contractId) {
		return getNumberOfTripsWhere(contractId,
				"o.processStatusId IN ('4', '5')");
	}

	private int getNumberOfTripsWhere(Long contractId, String processStatus) {
	    StringBuilder sql = new StringBuilder();
	    sql.append("SELECT o FROM Trip o ");
	    sql.append("JOIN Levkontrakt lk ON o.vendorId = lk.leverandorNr ");
	    sql.append("WHERE ");
	    sql.append("   lk.driftkontraktId = :contractId ");
	    sql.append("   AND ");
	    sql.append("   o.startTime >= :firstDateOfMonth ");
        sql.append("   AND ");
	    sql.append("   o.startTime <= :lastDateOfMonth ");
	    sql.append("   AND ");
	    sql.append(processStatus);

        Date firstDate = CalendarHelper.getFirstDateCurrentMonth();
        Date lastDate = CalendarHelper.getLastTimeInMonth(CalendarHelper.getLastDateCurrentMonth());

        Query q = em.createQuery(sql.toString())
                .setParameter("contractId", contractId)
                .setParameter("firstDateOfMonth", firstDate)
                .setParameter("lastDateOfMonth", lastDate);

        return q.getResultList().size();
    }

}