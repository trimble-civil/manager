package no.mesta.mipss.driftslogg;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;

/**
 * Klasse som inneholder attributtene til import-tabellen til agresso
 *
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class MipssInput implements Serializable, IRenderableMipssEntity{

	/** Leverand�rnummer */
	private String aparId;
	/** Artikkel nr*/
	private String article;
	/** selskap 20 for mesta drift default*/
	private String client = "20";
	/** levkontraktIdent */
	private String contractId;
	/** L�penummer fra 1 og oppover*/
	private Long lineNo;
	/** Ordre id, kontraktId+leverandorNr+mnd+�r*/
	private BigInteger orderId;
	/** Enhet */
	private String unitCode;
	/** Enhetspris */
	private Double unitPrice;
	/** Antall enheter omforentMengde/omforentKm */
	private Double value1;
	/** aktivitet.id */
	private Long mipssAktivitetId;
	/** Dato for aktiviteten*/
	private Date delivDate;
	/** Benyttes til innlogget bruker */
	private String text3;

	private final Aktivitet aktivitet;

	public MipssInput(MipssInput input){
		this.aparId = input.aparId;
		this.article = input.article;
		this.client = input.client;
		this.contractId = input.contractId;
		this.lineNo = input.lineNo;
		this.orderId = input.orderId;
		this.unitCode = input.unitCode;
		this.unitPrice = input.unitPrice;
		this.value1 = input.value1;
		this.mipssAktivitetId = input.mipssAktivitetId;
		this.delivDate = input.delivDate;
		this.text3 = input.text3;
		this.aktivitet = input.aktivitet;
		this.artDescr = input.artDescr;
		this.curAmount =input.curAmount;
		this.currency= input.currency;
		this.extOrdRef = input.extOrdRef;
		this.longInfo1 = input.longInfo1;
		this.longInfo2 = input.longInfo2;
		this.shortInfo = input.shortInfo;
		this.text1 = input.text1;
		this.text2 = input.text2;
		this.text4 = input.text4;
		this.orderDate = input.orderDate;
		this.periode = input.periode;
		this.unitDescr = input.unitDescr;
	}
	/**
	 * Oppretter en ny mipssinput til agresso
	 *
	 * @param aktivitet aktiviteten som skal sendes over
	 * @param lineNo - linjenummeren p� ordren, fra 1 og oppover
	 * @param orderId - orderId (kontraktid+leverandorNr+mnd+�r)
	 */
	public MipssInput(Aktivitet aktivitet, Long lineNo, BigInteger orderId, String brukerIdent){
		this.aktivitet = aktivitet;
		this.text3 = brukerIdent.toUpperCase();
		this.aparId = aktivitet.getPeriode().getLevkontrakt().getLeverandorNr();
		this.article = aktivitet.getArtikkelIdent();
		this.client = "20";
		this.contractId = aktivitet.getPeriode().getLevkontraktIdent();
		this.lineNo = lineNo;
		this.orderId = orderId;
		this.unitCode = aktivitet.getEnhet();
		this.unitPrice = aktivitet.getEnhetsPris();
		this.value1 = aktivitet.getGodkjentMengde();
		this.mipssAktivitetId = aktivitet.getId();
		this.setDelivDate(aktivitet.getFraDatoTid());
	}
	/*
	 * Valgfrie felter
	 */

	/** Artikkelbeskrivelse, trenger ikke angis dersom artikkelnavn registrert p� artikkelen skal benyttes (alts� en mulighet for � overstyre p� den enkelte rad)*/
	private String artDescr;
	/** Bel�p i valuta, = antall enheter x enhetspris. Kan evt beregnes i Agresso*/
	private Double curAmount;
	/** Valuta, vil alltid v�re i NOK? */
	private String currency;
	/** Ekstern referanse, for eksempel til leverand�ren*/
	private String extOrdRef;
	/** Kan benyttes som informasjonsfelt*/
	private String longInfo1;
	/** Kan benyttes som informasjonsfelt*/
	private String longInfo2;
	/** Kan benyttes som informasjonsfelt*/
	private String shortInfo;
	/** Kan benyttes som informasjonsfelt*/
	private String text1;
	/** Kan benyttes som informasjonsfelt*/
	private String text2;
	/** Kan benyttes som informasjonsfelt*/
	private String text4;
	/** Ordredato. Hvis man skal spesifisere en dato for ordren angis det her. Hvis blank vil dagens dato bli benyttet.*/
	private Date orderDate;
	/** Periodetilh�righet for ordren. */
	private Long periode;
	/** Enhetsbeskrivelse, eks KM, = Kilometer*/
	private String unitDescr;

	//Brukes kun for � sjekke om en leverand�r skal overf�res til Agresso eller ikke
	private boolean levSkalOverfores = true;


	/**
	 * @return the aparId
	 */
	public String getAparId() {
		return aparId;
	}
	/**
	 * @param aparId the aparId to set
	 */
	public void setAparId(String aparId) {
		this.aparId = aparId;
	}
	/**
	 * @return the article
	 */
	public String getArticle() {
		return article;
	}
	/**
	 * @param article the article to set
	 */
	public void setArticle(String article) {
		this.article = article;
	}
	/**
	 * @return the client
	 */
	public String getClient() {
		return client;
	}
	/**
	 * @param client the client to set
	 */
	public void setClient(String client) {
		this.client = client;
	}
	/**
	 * @return the contract
	 */
	public String getContractId() {
		return contractId;
	}
	/**
	 * @param contract the contract to set
	 */
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	/**
	 * @return the lineNo
	 */
	public Long getLineNo() {
		return lineNo;
	}
	/**
	 * @param lineNo the lineNo to set
	 */
	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}
	/**
	 * @return the orderId
	 */
	public BigInteger getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(BigInteger orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the unitCode
	 */
	public String getUnitCode() {
		return unitCode;
	}
	/**
	 * @param unitCode the unitCode to set
	 */
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	/**
	 * @return the unitPrice
	 */
	public Double getUnitPrice() {
		return unitPrice == null ? 0 : unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the value1
	 */
	public Double getValue1() {
		return value1;
	}
	/**
	 * @param value1 the value1 to set
	 */
	public void setValue1(Double value1) {
		this.value1 = value1;
	}
	/**
	 * @return the mipssAktivitetId
	 */
	public Long getMipssAktivitetId() {
		return mipssAktivitetId;
	}
	/**
	 * @param mipssAktivitetId the mipssAktivitetId to set
	 */
	public void setMipssAktivitetId(Long mipssAktivitetId) {
		this.mipssAktivitetId = mipssAktivitetId;
	}
	/**
	 * @return the artDescr
	 */
	public String getArtDescr() {
		return artDescr;
	}
	/**
	 * @param artDescr the artDescr to set
	 */
	public void setArtDescr(String artDescr) {
		this.artDescr = artDescr;
	}
	/**
	 * @return the curAmount
	 */
	public Double getCurAmount() {
		return curAmount;
	}
	/**
	 * @param curAmount the curAmount to set
	 */
	public void setCurAmount(Double curAmount) {
		this.curAmount = curAmount;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the extOrdRef
	 */
	public String getExtOrdRef() {
		return extOrdRef;
	}
	/**
	 * @param extOrdRef the extOrdRef to set
	 */
	public void setExtOrdRef(String extOrdRef) {
		this.extOrdRef = extOrdRef;
	}
	/**
	 * @return the longInfo1
	 */
	public String getLongInfo1() {
		return longInfo1;
	}
	/**
	 * @param longInfo1 the longInfo1 to set
	 */
	public void setLongInfo1(String longInfo1) {
		this.longInfo1 = longInfo1;
	}
	/**
	 * @return the longInfo2
	 */
	public String getLongInfo2() {
		return longInfo2;
	}
	/**
	 * @param longInfo2 the longInfo2 to set
	 */
	public void setLongInfo2(String longInfo2) {
		this.longInfo2 = longInfo2;
	}
	/**
	 * @return the shortInfo
	 */
	public String getShortInfo() {
		return shortInfo;
	}
	/**
	 * @param shortInfo the shortInfo to set
	 */
	public void setShortInfo(String shortInfo) {
		this.shortInfo = shortInfo;
	}
	/**
	 * @return the text1
	 */
	public String getText1() {
		return text1;
	}
	/**
	 * @param text1 the text1 to set
	 */
	public void setText1(String text1) {
		this.text1 = text1;
	}
	/**
	 * @return the text2
	 */
	public String getText2() {
		return text2;
	}
	/**
	 * @param text2 the text2 to set
	 */
	public void setText2(String text2) {
		this.text2 = text2;
	}
	/**
	 * @return the text3
	 */
	public String getText3() {
		return text3;
	}
	/**
	 * @param text3 the text3 to set
	 */
	public void setText3(String text3) {
		this.text3 = text3;
	}
	/**
	 * @return the text4
	 */
	public String getText4() {
		return text4;
	}
	/**
	 * @param text4 the text4 to set
	 */
	public void setText4(String text4) {
		this.text4 = text4;
	}
	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * @return the periode
	 */
	public Long getPeriode() {
		return periode;
	}
	/**
	 * @param periode the periode to set
	 */
	public void setPeriode(Long periode) {
		this.periode = periode;
	}
	/**
	 * @return the unitDescr
	 */
	public String getUnitDescr() {
		return unitDescr;
	}
	/**
	 * @param unitDescr the unitDescr to set
	 */
	public void setUnitDescr(String unitDescr) {
		this.unitDescr = unitDescr;
	}

	public void setDelivDate(Date delivDate) {
		this.delivDate = delivDate;
	}

	public Date getDelivDate() {
		return delivDate;
	}
	public Aktivitet getAktivitet() {
		return aktivitet;
	}
	@Override
	public String getTextForGUI() {
		return null;
	}
	public boolean skalOverfores() {
		return erRateUlik0() && erPrisUlik0() && skalLeverandorOverfores() && erGodkjentUE();
	}

	public boolean erRateUlik0(){
		Double v = getValue1();
		return (v == null) ? false : v>0;
	}
	public boolean erPrisUlik0(){
		Double price = getUnitPrice();
		Double roundedPrice = (Math.round(price * 100.00) / 100.00);
		return roundedPrice > 0.01d;
	}

	public boolean erGodkjentUE() {
        return aktivitet.isApprovedUE();
    }

	public void setLevSkalOverfores(boolean levSkalOverfores) {
		this.levSkalOverfores = levSkalOverfores;
	}

	public Boolean skalLeverandorOverfores(){
		return levSkalOverfores;
	}
}