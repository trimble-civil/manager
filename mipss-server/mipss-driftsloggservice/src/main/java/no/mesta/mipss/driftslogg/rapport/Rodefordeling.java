package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;

import no.mesta.mipss.persistence.kontrakt.Rode;

@SuppressWarnings("serial")
public class Rodefordeling implements Serializable{

	private Rode r12rode; 
	private double fordeling; // i % 0-100 (0.0 - 1.0)
	
	
	public Rodefordeling(Rode r12rode, double fordeling){
		this.r12rode = r12rode;
		this.fordeling = fordeling;
	}
	
	public double getVerdi(double value){
		return value*fordeling;
	}
	public double getFordelingsnokkel(){
		return fordeling;
	}
	public Rode getR12rode(){
		return r12rode;
	}
	
}
