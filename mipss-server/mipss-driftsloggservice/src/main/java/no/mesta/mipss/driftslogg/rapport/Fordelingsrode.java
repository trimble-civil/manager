package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.kontrakt.Rode;

@SuppressWarnings("serial")
public class Fordelingsrode implements Serializable{

	private Rode stroBrotyeRode;//str� br�yterode
	private List<Rodefordeling> rodefordeling = new ArrayList<Rodefordeling>(); //r12roder %-vis fordelt p� str� br�yteroden
	
	public Fordelingsrode(Rode rode){
		this.stroBrotyeRode = rode;
	}
	
	/**
	 * Legg til en overlappende r12 rodefordeling.
	 * @param rf
	 */
	public void addRodefordeling(Rodefordeling rf){
		rodefordeling.add(rf);
	}
	
	/**
	 * Fordel verdien p� de overlappende r12-rodene.
	 * @param verdi
	 * @return
	 */
	public List<BeregnetFordeling> getBeregnetFordeling(double verdi){
		List<BeregnetFordeling> fordeling = new ArrayList<BeregnetFordeling>();
		for (Rodefordeling rf:rodefordeling){
			double beregnetVerdi = rf.getVerdi(verdi);
			Rode r12Rode = rf.getR12rode();
			fordeling.add(new BeregnetFordeling(r12Rode, beregnetVerdi));
		}
		return fordeling;
	}
	
	public Rode getStroBroyteRode(){
		return stroBrotyeRode;
	}
	
	public List<Rodefordeling> getFordelingsnokler(){
		return rodefordeling;
	}
	public List<Rode> getR12Roder(){
		List<Rode> rodelist = new ArrayList<Rode>();
		for (Rodefordeling rs:rodefordeling){
			rodelist.add(rs.getR12rode());
		}
		return rodelist;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stroBrotyeRode == null) ? 0 : stroBrotyeRode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fordelingsrode other = (Fordelingsrode) obj;
		if (stroBrotyeRode == null) {
			if (other.stroBrotyeRode != null)
				return false;
		} else if (!stroBrotyeRode.equals(other.stroBrotyeRode))
			return false;
		return true;
	}
	
	
}
