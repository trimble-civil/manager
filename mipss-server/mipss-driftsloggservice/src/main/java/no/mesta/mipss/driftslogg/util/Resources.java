package no.mesta.mipss.driftslogg.util;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class Resources {
    private static PropertyResourceBundleUtil properties;
    private static final String BUNDLE_NAME = "driftslogg";


    public static String getResource(String key){
        if (properties==null)
            properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
        return properties.getGuiString(key);
    }
}
