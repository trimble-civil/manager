package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.persistence.driftslogg.GritProductAmount;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class GritProductAmountsForActivitiesQuery {

    private Query q;

    public GritProductAmountsForActivitiesQuery(String activityIds, EntityManager em) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");
        sb.append("   aktivitet_id AS activityId,");
        sb.append("   stroprodukt_id AS gritProductId,");
        sb.append("   omforent_mengde_tort AS gritDry,");
        sb.append("   omforent_mengde_vaatt AS gritWet ");

        sb.append("FROM stroproduktmengde ");

        sb.append("WHERE aktivitet_id IN (").append(activityIds).append(")");

        q = em.createNativeQuery(sb.toString());
    }

    public List<GritProductAmount> getGritProductAmounts() {
        NativeQueryWrapper<GritProductAmount> qry = new NativeQueryWrapper<>(
                q,
                GritProductAmount.class,
                GritProductAmount.getPropertiesArray());

        return qry.getWrappedList();
    }

}