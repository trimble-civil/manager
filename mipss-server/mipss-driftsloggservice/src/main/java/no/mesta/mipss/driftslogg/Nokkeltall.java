package no.mesta.mipss.driftslogg;

import java.io.Serializable;
import java.util.List;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;

/**
 * Inneholder n�kkeltall for en leverand�r for en gitt periode. 
 * 
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class Nokkeltall implements Serializable {

	/** Km produksjon uten tomkj�ring i MIPSS */
	private double kmMipss = 0;
	/** Kun km tomkj�ring i MIPSS */
	private double kmTomMipss = 0;
	/** Antall sekunder med produksjon uten tomkj�ring */
	private double sekundMipss = 0;
	/** Antall sekunder med tomkj�ring */
	private double sekundTomMipss = 0;
	/** Antall tonn t�rrstoff */
	private double tonnTorrMipss = 0;
	
	private double tonnTorrLev;
	private double kmLev;
	private double timerLev;
	private double tonnTorrGodkjent;

	private double kmGodkjent;
	private double timerGodkjent;
	
	private List<Aktivitet> aktiviteter;
	
	/**
	 * @return the kmMipss
	 */
	public double getKmMipss() {
		return kmMipss;
	}


	/**
	 * @param kmMipss the kmMipss to set
	 */
	public void setKmMipss(double kmMipss) {
		this.kmMipss = kmMipss;
	}


	/**
	 * @return the kmTomMipss
	 */
	public double getKmTomMipss() {
		return kmTomMipss;
	}


	/**
	 * @param kmTomMipss the kmTomMipss to set
	 */
	public void setKmTomMipss(double kmTomMipss) {
		this.kmTomMipss = kmTomMipss;
	}


	/**
	 * @return the sekundMipss
	 */
	public double getSekundMipss() {
		return sekundMipss;
	}
	
	/**
	 * Henter antall timer med produksjon i MIPSS
	 * runder av til to desimaler
	 * @return
	 */
	public double getTimerMipss(){
		double timer = sekundMipss/3600;//3600 sekunder i en time
		return timer;
	}

	/**
	 * @param sekundMipss the sekundMipss to set
	 */
	public void setSekundMipss(double sekundMipss) {
		this.sekundMipss = sekundMipss;
	}


	/**
	 * @return the sekundTomMipss
	 */
	public double getSekundTomMipss() {
		return sekundTomMipss;
	}
	
	/**
	 * Henter antall timer med tomkj�ring i MIPSS
	 * runder av til to desimaler. 
	 * @return
	 */
	public double getTimerTomMipss(){
		double timer = sekundTomMipss/3600;//3600 sekunder i en time
		return timer;
	}

	private double roundToDecimals(double d, int c) {
		int temp=(int)((d*Math.pow(10,c)));
		return (((double)temp)/Math.pow(10,c));
	}
	/**
	 * @param sekundTomMipss the sekundTomMipss to set
	 */
	public void setSekundTomMipss(double sekundTomMipss) {
		this.sekundTomMipss = sekundTomMipss;
	}


	/**
	 * @return the tonnTorrMipss
	 */
	public double getTonnTorrMipss() {
		return tonnTorrMipss;
	}


	/**
	 * @param tonnTorrMipss the tonnTorrMipss to set
	 */
	public void setTonnTorrMipss(double tonnTorrMipss) {
		this.tonnTorrMipss = tonnTorrMipss;
	}


	public void setTimerLev(double timerLev) {
		this.timerLev = timerLev;
	}
	public double getTimerLev(){
		return timerLev;
	}
	public void setKmLev(double kmLev) {
		this.kmLev = kmLev;
	}
	public double getKmLev(){
		return kmLev;
	}

	public void setTonnTorrLev(double tonnTorrLev) {
		this.tonnTorrLev = tonnTorrLev;
	}
	public double getTonnTorrLev(){
		return tonnTorrLev;
	}


	public void setAktiviteter(List<Aktivitet> aktiviteter) {
		this.aktiviteter = aktiviteter;
	}


	public List<Aktivitet> getAktiviteter() {
		return aktiviteter;
	}
	
	/**
	 * @return the tonnTorrGodkjent
	 */
	public double getTonnTorrGodkjent() {
		return tonnTorrGodkjent;
	}


	/**
	 * @param tonnTorrGodkjent the tonnTorrGodkjent to set
	 */
	public void setTonnTorrGodkjent(double tonnTorrGodkjent) {
		this.tonnTorrGodkjent = tonnTorrGodkjent;
	}


	/**
	 * @return the kmGodkjent
	 */
	public double getKmGodkjent() {
		return kmGodkjent;
	}


	/**
	 * @param kmGodkjent the kmGodkjent to set
	 */
	public void setKmGodkjent(double kmGodkjent) {
		this.kmGodkjent = kmGodkjent;
	}


	/**
	 * @return the timerGodkjent
	 */
	public double getTimerGodkjent() {
		return timerGodkjent;
	}


	/**
	 * @param timerGodkjent the timerGodkjent to set
	 */
	public void setTimerGodkjent(double timerGodkjent) {
		this.timerGodkjent = timerGodkjent;
	}
}
