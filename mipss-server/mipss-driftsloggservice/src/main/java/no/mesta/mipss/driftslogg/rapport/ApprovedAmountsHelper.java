package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.util.CalendarHelper;
import no.mesta.mipss.persistence.driftslogg.ActivityRodeInfo;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import no.mesta.mipss.persistence.driftslogg.GritProductAmount;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.Stroinfo;
import no.mesta.mipss.produksjonsrapport.query.GritInfoForMeasuresQuery;
import no.mesta.mipss.produksjonsrapport.values.DryGritAmountsHelper;

import javax.persistence.EntityManager;
import java.util.*;

public class ApprovedAmountsHelper {

    private static final String INTENTION_PURPOSE = "Produksjon med inaktivt br\u00F8ytutstyr i forbindelse med br\u00F8yting/h\u00F8vling";
    private static final String MANUALLY_DISITRIBUTED = "[fordeles manuelt]";

    private List<ApprovedAmountData> approvedAmountData;

    /**
     * Used for testing purposes.
     */
    ApprovedAmountsHelper() {
    }

    public ApprovedAmountsHelper(DriftsloggQueryParams params, Date fraDato, Date tilDato, EntityManager em) {
        List<ApprovedActivity> approvedActivities = new ApprovedActivitiesQuery(params, fraDato, tilDato, em).getApprovedActivities();

        // Getting gritting amounts for activities where gritting has occurred.
        Map<Long, GritProductAmount> amountForActivityIds = getGritProductAmountsForActivities(approvedActivities, em);
        for (ApprovedActivity activity : approvedActivities) {
            activity.setGritProductAmount(amountForActivityIds.get(activity.getActivityId()));
        }

        // Removes activities with production type 98 where the intention flag is not set to true.
        removeIdleProduction(approvedActivities);

        // Temporary removes activities that hasn't automatically logged data. We need these at a later point.
        List<ApprovedActivity> activitiesNoProdData = findActivitiesWithoutProdData(approvedActivities);

        List<Long> dryMeasureIds = new ArrayList<>();
        List<Long> wetMeasureIds = new ArrayList<>();
        enrichAndExpandApprovedActivities(approvedActivities, params.getKontrakt().getId(), em, dryMeasureIds, wetMeasureIds);

        // Reintroduces the activities that hasn't automatically logged data.
        approvedActivities.addAll(activitiesNoProdData);

        approvedAmountData = aggregateApprovedAmountsData(
                approvedActivities,
                DryGritAmountsHelper.getDryGritAmounts(dryMeasureIds, em),
                getWetGritAmounts(wetMeasureIds, em)
        );

        sortApprovedAmountsData(approvedAmountData);
    }

    private void removeIdleProduction(List<ApprovedActivity> approvedActivities) {
        approvedActivities.removeIf(activity ->
                activity.getProdType() != null && activity.getProdType().equals("Tomkj\u00F8ring") &&
                (activity.getBillIntention() == null || !activity.getBillIntention()));
    }

    private List<ApprovedActivity> findActivitiesWithoutProdData(List<ApprovedActivity> approvedActivities) {
        List<ApprovedActivity> activitiesNoProdData = new ArrayList<>();

        Iterator<ApprovedActivity> it = approvedActivities.iterator();
        while (it.hasNext()) {
            ApprovedActivity activity = it.next();

            if ((activity.isManuallyCreated() != null && activity.isManuallyCreated()) ||
                    activity.getRodeId() == null || activity.getVehicleId() == null) {
                activitiesNoProdData.add(activity);
                it.remove();
            }
        }

        return activitiesNoProdData;
    }

    private void enrichAndExpandApprovedActivities(List<ApprovedActivity> approvedActivities, Long contractId,
                                                   EntityManager em, List<Long> dryMeasureIds, List<Long> wetMeasureIds) {
        List<ActivityRodeInfo> activityRodeInfo = new RodeInfoForActivitiesQuery(contractId, approvedActivities, em).getRodeInfo();
        List<Prodtype> prodTypes = em.createNamedQuery(Prodtype.FIND_ALL, Prodtype.class).getResultList();

        flagRodeInfoAsCoproduction(activityRodeInfo);

        ApprovedActivity activity;
        for (int i = 0; i < approvedActivities.size(); i++) {
            activity = approvedActivities.get(i);
            activity.setGritType(getGritTypeFromProdTypeString(activity.getProdType()));
            activity.setProdType(getCommaSeparatedProdTypes(activity.getProdType(), prodTypes));

            List<ActivityRodeInfo> rodeInfoList = findActivityRodeInfoFor(activityRodeInfo, activity);

            if (!rodeInfoList.isEmpty()) {
                ActivityRodeInfo info = rodeInfoList.get(0);

                activity.setActualDuration(CalendarHelper.getDurationInHours(info.getFromDateTime(), info.getToDateTime()));
                activity.setActivityRodeInfo(info);

                selectDryWetGritMeasureIds(info, dryMeasureIds, wetMeasureIds);

                int activityCounter = 0;
                for (int j = 1; j < rodeInfoList.size(); j++) {
                    info = rodeInfoList.get(j);
                    activity.addTotalProdStretchDistanceDriven(info.getProdStretchDistanceDriven());

                    ApprovedActivity activityProdStretch = populateApprovedActivity(activity, info);

                    selectDryWetGritMeasureIds(info, dryMeasureIds, wetMeasureIds);

                    if ((i + j) <= approvedActivities.size()) {
                        approvedActivities.add(i + j, activityProdStretch);
                    } else {
                        approvedActivities.add(activityProdStretch);
                    }

                    activityCounter++;
                }

                i += activityCounter;
            }
        }
    }

    private ApprovedActivity populateApprovedActivity(ApprovedActivity activity, ActivityRodeInfo info) {
        ApprovedActivity activityProdStretch = new ApprovedActivity();

        activityProdStretch.setVendorName(activity.getVendorName());
        activityProdStretch.setActivityId(activity.getActivityId());
        activityProdStretch.setVehicleId(activity.getVehicleId());
        activityProdStretch.setVehicleName(activity.getVehicleName());
        activityProdStretch.setFromDateTime(activity.getFromDateTime());
        activityProdStretch.setToDateTime(activity.getToDateTime());
        activityProdStretch.setRodeId(activity.getRodeId());
        activityProdStretch.setRodeName(activity.getRodeName());
        activityProdStretch.setR12Planing(activity.getR12Planing());
        activityProdStretch.setR12SidePlough(activity.getR12SidePlough());
        activityProdStretch.setR12HeavyPlaning(activity.getR12HeavyPlaning());
        activityProdStretch.setActivityRodeInfo(info);
        activityProdStretch.setApprovedBuild(activity.getApprovedBuild());
        activityProdStretch.setBillIntention(activity.getBillIntention());
        activityProdStretch.setR12Intention(activity.getR12Intention());
        activityProdStretch.setGritProductAmount(activity.getGritProductAmount());
        activityProdStretch.setRodeConnectionTypeId(activity.getRodeConnectionTypeId());
        activityProdStretch.setProdType(activity.getProdType());
        activityProdStretch.setGritType(activity.getGritType());
        activityProdStretch.setActualDuration(CalendarHelper.getDurationInHours(info.getFromDateTime(), info.getToDateTime()));

        return activityProdStretch;
    }

    private String getGritTypeFromProdTypeString(String prodType) {
        if (prodType != null && !prodType.isEmpty()) {
            int gritTypeIndex = prodType.indexOf(":");

            if (gritTypeIndex >= 0) {
                return prodType.substring(gritTypeIndex + 1, prodType.length()).trim();
            }
        }
        return null;
    }

    private String getCommaSeparatedProdTypes(String prodTypeNames, List<Prodtype> prodTypes) {
        if (prodTypeNames != null) {
            if (prodTypeNames.contains(":")) {
                prodTypeNames = prodTypeNames.substring(0, prodTypeNames.indexOf(":")).trim();
            }

            String[] names = prodTypeNames.split("\\+");

            StringBuilder prodTypeIds = new StringBuilder();
            String separator = "";
            for (String name : names) {
                for (Prodtype prodType : prodTypes) {
                    if (prodType.getNavn().equals(name)) {
                        prodTypeIds.append(separator).append(prodType.getId());
                        separator = ",";
                    }
                }
            }

            return prodTypeIds.toString();
        }
        return "";
    }

    private void selectDryWetGritMeasureIds(ActivityRodeInfo info, List<Long> dryMeasureIds, List<Long> wetMeasureIds) {
        if (info.getGritFlag()) {
            if (ManagerConstants.Stroprod.idTilEnum(info.getGritProdId()) == ManagerConstants.Stroprod.SALTLOSNING) {
                if (!wetMeasureIds.contains(info.getMeasureId())) {
                    wetMeasureIds.add(info.getMeasureId());
                }
            } else {
                if (!dryMeasureIds.contains(info.getMeasureId())) {
                    dryMeasureIds.add(info.getMeasureId());
                }
            }
        }
    }

    private List<ApprovedAmountData> aggregateApprovedAmountsData(List<ApprovedActivity> activities,
                                                                  Map<Long, Double> dryGritAmounts, Map<Long, Double> wetGritAmounts) {
        List<ApprovedAmountData> approvedAmountData = new ArrayList<>();

        flagCoproductionActivities(activities);
        List<ApprovedActivity> countedActivities = new ArrayList<>();

        for (ApprovedActivity activity : activities) {
            if (activity.getActivityRodeInfo() != null && (!activity.isCoproduction() ||
                    (activity.isCoproduction() && prodStrechActivityHasNotBeenCounted(activity, countedActivities))) &&
                isMatchingProdType(activity.getProdType(), activity.getActivityRodeInfo().getProdTypeId())) {
                ApprovedAmountData data = populateApprovedAmountData(activity);

                data.setLogged(true);
                data.setDistanceDriven(activity.getActivityRodeInfo().getDistanceDriven());
                data.addActualDuration(activity.getActualDuration());

                // This is the value from STROPRODUKTMENGDE which we want to distribute among the rows with the same activity id.
                if (activity.getGritProductAmount() != null) {
                    if (activity.getGritProductAmount().getDryGrit() != null && activity.getGritProductAmount().getDryGrit() != 0.0) {
                        data.setAmountDryGritted(activity.getGritProductAmount().getDryGrit());
                    }

                    if (activity.getGritProductAmount().getWetGrit() != null && activity.getGritProductAmount().getWetGrit() != 0.0) {
                        data.setAmountWetGritted(activity.getGritProductAmount().getWetGrit());
                    }
                }

                if (activity.getBillIntention() && activity.getR12Intention() != null && activity.getR12Intention() > 0) {
                    data.addIntensjonKm(activity.getActivityRodeInfo().getDistanceDriven());
                }

                countedActivities.add(activity);

                setPlaningPloughingData(activity, data);
                setWinterClassData(activity, data);
                setGritData(activity, dryGritAmounts, wetGritAmounts, data);

                addOrUpdateApprovedAmountData(approvedAmountData, data);
            } else if (activity.isManuallyCreated() != null && activity.isManuallyCreated()) {
                ApprovedAmountData data = populateApprovedAmountData(activity);

                setPlaningPloughingData(activity, data);
                setWinterClassData(activity, data);
                setGritData(activity, dryGritAmounts, wetGritAmounts, data);

                addOrUpdateApprovedAmountData(approvedAmountData, data);
            }
        }

        return approvedAmountData;
    }

    private void addOrUpdateApprovedAmountData(List<ApprovedAmountData> approvedAmountData, ApprovedAmountData data) {
        int index = approvedAmountData.indexOf(data);
        if (index >= 0) {
            ApprovedAmountData existing = approvedAmountData.get(index);
            existing.addApprovedAmountsFrom(data);
        } else {
            approvedAmountData.add(data);
        }
    }

    private ApprovedAmountData populateApprovedAmountData(ApprovedActivity activity) {
        ApprovedAmountData data = new ApprovedAmountData();

        data.setActivityId(activity.getActivityId());
        data.setKjoretoyId(activity.getVehicleId());
        data.setKjoretoynavn(activity.getVehicleName());
        data.setFromDateTime(MipssDateFormatter.formatDate(activity.getFromDateTime(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
        data.setToDateTime(MipssDateFormatter.formatDate(activity.getToDateTime(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
        data.setLeverandor(activity.getVendorName());
        data.setRodenavn(activity.getRodeName());
        data.setProdType(activity.getProdType());
        data.setR12Planing(activity.getR12Planing());
        data.setR12SidePlough(activity.getR12SidePlough());
        data.setR12HeavyPlaning(activity.getR12HeavyPlaning());

        if (activity.getRodeConnectionTypeId() != null) {
            if (activity.getRodeConnectionTypeId() == ManagerConstants.RodeConnectionType.PLOWING_RODE.getId()) {
                data.setOnPlowingRode("Ja");
            }
        }

        if (activity.getApprovedBuild() != null && activity.getApprovedBuild() && activity.getBillIntention() != null && activity.getBillIntention()) {
            data.setIntensjonFormal(INTENTION_PURPOSE);
        }

        return data;
    }

    private void flagRodeInfoAsCoproduction(List<ActivityRodeInfo> rodeInfos) {
        if (rodeInfos.size() > 1) {
            Iterator<ActivityRodeInfo> it = rodeInfos.iterator();

            ActivityRodeInfo previousRodeInfo = it.next();

            while (it.hasNext()) {
                ActivityRodeInfo currentRodeInfo = it.next();

                if (previousRodeInfo.getVehicleId().equals(currentRodeInfo.getVehicleId()) &&
                        previousRodeInfo.getFromDateTime().equals(currentRodeInfo.getFromDateTime()) &&
                        previousRodeInfo.getToDateTime().equals(currentRodeInfo.getToDateTime())) {
                    previousRodeInfo.setCoproduction(true);
                    currentRodeInfo.setCoproduction(true);
                }

                previousRodeInfo = currentRodeInfo;
            }
        }
    }

    private void flagCoproductionActivities(List<ApprovedActivity> activities) {
        if (activities.size() > 1) {
            Iterator<ApprovedActivity> it = activities.iterator();

            ApprovedActivity previousActivity = it.next();

            while (it.hasNext()) {
                ApprovedActivity currentActivity = it.next();

                if (previousActivity.getActivityRodeInfo() != null && currentActivity.getActivityRodeInfo() != null) {
                    ActivityRodeInfo previousRodeInfo = previousActivity.getActivityRodeInfo();
                    ActivityRodeInfo currentRodeInfo = currentActivity.getActivityRodeInfo();

                    if (previousActivity.getActivityId().equals(currentActivity.getActivityId()) &&
                            previousActivity.getVehicleId().equals(currentActivity.getVehicleId()) &&
                            previousRodeInfo.getFromDateTime().equals(currentRodeInfo.getFromDateTime()) &&
                            previousRodeInfo.getToDateTime().equals(currentRodeInfo.getToDateTime())) {
                        previousActivity.setCoproduction(true);
                        currentActivity.setCoproduction(true);
                    }
                }

                previousActivity = currentActivity;
            }
        }
    }

    private boolean prodStrechActivityHasNotBeenCounted(ApprovedActivity newActivityProdStretch, List<ApprovedActivity> activityProdStrecthesCounted) {
        for (ApprovedActivity activityProdStretch : activityProdStrecthesCounted) {
            if (activityProdStretch.getVehicleId().equals(newActivityProdStretch.getVehicleId()) &&
                    activityProdStretch.getActivityRodeInfo().getFromDateTime().equals(newActivityProdStretch.getActivityRodeInfo().getFromDateTime()) &&
                    activityProdStretch.getActivityRodeInfo().getToDateTime().equals(newActivityProdStretch.getActivityRodeInfo().getToDateTime())) {
                return false;
            }
        }
        return true;
    }

    private boolean isMatchingProdType(String activityProdTypes, Integer rodeInfoProdType) {
        if (activityProdTypes != null) {
            for (String prodType : activityProdTypes.split(",")) {
                Integer prodTypeId = Integer.parseInt(prodType);

                if (prodTypeId.equals(rodeInfoProdType)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void setWinterClassData(ApprovedActivity activity, ApprovedAmountData data) {
        if (activity.getActivityRodeInfo() != null) {
            ActivityRodeInfo rodeInfo = activity.getActivityRodeInfo();

            data.setVinterdriftklasse(rodeInfo.getWinterClass());
            data.setVegtype(rodeInfo.getRoadType());
            data.setFylke(rodeInfo.getCountyNumber());
        } else {
            data.setVinterdriftklasse(MANUALLY_DISITRIBUTED);
        }
    }

    private void setPlaningPloughingData(ApprovedActivity activity, ApprovedAmountData data) {
        Double planing = activity.getR12Planing();
        Double sideplough = activity.getR12SidePlough();
        Double heavyPlaning = activity.getR12HeavyPlaning();

        if (data.isLogged()) {
            Double distance = activity.getActivityRodeInfo().getDistanceDriven();
            updateCorrectPlaningPloughing(data, planing, sideplough, heavyPlaning, distance, distance, distance);
        } else {
            updateCorrectPlaningPloughing(data, planing, sideplough, heavyPlaning, planing, sideplough, heavyPlaning);
        }
    }

    private void updateCorrectPlaningPloughing(ApprovedAmountData data, Double planing, Double sideplough,
            Double heavyPlaning, Double planingValue, Double sideploughValue, Double heavyPlaningValue) {
        if (planing != null) { // BRØYTING/HØVLING
            data.addBroytingKm(planingValue);
        } else if (sideplough != null) { // SIDEPLOG
            data.addSideplogKm(sideploughValue);
        } else if (heavyPlaning != null) { // TUNG HØVLING
            data.addTungHovlingKm(heavyPlaningValue);
        }
    }

    private void setGritData(ApprovedActivity activity, Map<Long, Double> dryGritAmounts,
            Map<Long, Double> wetGritAmounts, ApprovedAmountData data) {
        Integer gritProductId = null;
        Double dry = null;
        Double wet = null;

        if (data.isLogged() && activity.getActivityRodeInfo() != null &&
                activity.getActivityRodeInfo().getGritProdId() != null &&
                activity.getActivityRodeInfo().getGritFlag()) {
            gritProductId = activity.getActivityRodeInfo().getGritProdId();

            dry = dryGritAmounts.get(activity.getActivityRodeInfo().getMeasureId());
            wet = wetGritAmounts.get(activity.getActivityRodeInfo().getMeasureId());

            // We need to weight the grit based on the actual distance driven within the rode stretch ("rodestrekning").
            Double weight = activity.getActivityRodeInfo().getDistanceDriven() / activity.getActivityRodeInfo().getProdStretchDistanceDriven();

            if (dry != null) {
                dry *= weight;
            }

            if (wet != null) {
                wet *= weight;
            }

            data.addLoggedAmountDryGritted(dry);
            data.addLoggedAmountWetGritted(wet);
        } else if (activity.getGritProductAmount() != null) {
            GritProductAmount grit = activity.getGritProductAmount();
            gritProductId = activity.getGritProductAmount().getGritProductId();

            dry = (grit.getDryGrit() != null) ? grit.getDryGrit() / 1000 : null;
            wet = (grit.getWetGrit() != null) ? grit.getWetGrit() / 1000 : null;
        }

        if (gritProductId != null) {
            ManagerConstants.Stroprod gritProduct = ManagerConstants.Stroprod.idTilEnum(gritProductId);

            switch (gritProduct) {
                case TORRSALT:
                    data.addTorrsaltTonn(dry);
                    break;
                case SLURRY:
                    data.addSlurryTonn(dry);
                    break;
                case SALTLOSNING:
                    data.addSaltlosningKubikk(wet);
                    break;
                case BEFUKTET_SALT:
                    data.addBefuktetSaltTonn(dry);
                    break;
                case SAND_MED_SALT:
                    data.addSandSaltTonn(dry);
                    break;
                case REN_SAND:
                    data.addRenSandTonn(dry);
                    break;
                case FAST_SAND:
                    data.addFastsandTonn(dry);
                    break;
                default:
                    break;
            }
        }
    }

    private void sortApprovedAmountsData(List<ApprovedAmountData> approvedAmountData) {
        approvedAmountData.sort(Comparator
                .comparing(ApprovedAmountData::getLeverandor, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(ApprovedAmountData::getRodenavn, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(ApprovedAmountData::getVinterdriftklasse, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(ApprovedAmountData::getFylke, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(ApprovedAmountData::getVegtype, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(ApprovedAmountData::getActivityId, Comparator.nullsLast(Comparator.naturalOrder()))
        );
    }

    List<ActivityRodeInfo> findActivityRodeInfoFor(List<ActivityRodeInfo> activityRodeInfos, ApprovedActivity activity) {
        List<ActivityRodeInfo> activityRodeInfosForActivity = new ArrayList<>();

        if (activity.getProdType() != null) {
            activity.setCoproduction(activity.getProdType().contains(","));
        }

        ManagerConstants.Stroprod gritProductActivity = ManagerConstants.Stroprod.navnTilEnum(activity.getGritType());

        List<List<ActivityRodeInfo>> listsOfSiblings = new ArrayList<>();
        List<ActivityRodeInfo> siblings = new ArrayList<>();

        for (int i = 0; i< activityRodeInfos.size(); i++) {
            ActivityRodeInfo activityRodeInfo = activityRodeInfos.get(i);

            if (activityRodeInfo.getVehicleId().equals(activity.getVehicleId()) &&
                    activityRodeInfo.getRodeId().equals(activity.getRodeId()) &&
                    activityRodeInfo.getFromDateTime().getTime() >= activity.getFromDateTime().getTime() &&
                    activityRodeInfo.getFromDateTime().getTime() <= activity.getToDateTime().getTime()) {
                if (activity.isCoproduction() && activityRodeInfo.isCoproduction()) {
                    if (hasCoproductionSiblings(activityRodeInfo, siblings) || siblings.isEmpty()) {
                        if (activityRodeInfo.getGritProdId() != null &&
                                new Long(activityRodeInfo.getGritProdId()).equals(gritProductActivity.id())) {
                            siblings.add(activityRodeInfo);
                        } else if (activityRodeInfo.getGritProdId() == null) {
                            siblings.add(activityRodeInfo);
                        }
                    } else {
                        listsOfSiblings.add(siblings);
                        siblings = new ArrayList<>();
                        i--;
                    }
                } else if (!activity.isCoproduction() && !activityRodeInfo.isCoproduction()) {
                    if (isMatchingProdType(activity.getProdType(), activityRodeInfo.getProdTypeId())) {
                        if (activityRodeInfo.getGritProdId() != null &&
                                new Long(activityRodeInfo.getGritProdId()).equals(gritProductActivity.id())) {
                            activityRodeInfosForActivity.add(activityRodeInfo);
                        } else if (activityRodeInfo.getGritProdId() == null) {
                            activityRodeInfosForActivity.add(activityRodeInfo);
                        }
                    }
                }
            }
        }

        listsOfSiblings.add(siblings);

        int expectedNumberOfCoproductions = activity.getProdType().split(",").length;
        for (List<ActivityRodeInfo> siblingsList : listsOfSiblings) {
            if (expectedNumberOfCoproductions == siblingsList.size()) {
                boolean allSiblingsMatchActivity = true;
                for (ActivityRodeInfo sibling : siblingsList) {
                    if (!isMatchingProdType(activity.getProdType(), sibling.getProdTypeId())) {
                        allSiblingsMatchActivity = false;
                        break;
                    }
                }

                if (allSiblingsMatchActivity) {
                    activityRodeInfosForActivity.addAll(siblingsList);
                }
            }
        }

        return activityRodeInfosForActivity;
    }

    private boolean hasCoproductionSiblings(ActivityRodeInfo activityRodeInfo, List<ActivityRodeInfo> activityRodeInfos) {
        for (ActivityRodeInfo sibling : activityRodeInfos) {
            if (activityRodeInfo.getVehicleId().equals(sibling.getVehicleId()) &&
                    activityRodeInfo.getRodeId().equals(sibling.getRodeId()) &&
                    activityRodeInfo.getFromDateTime().equals(sibling.getFromDateTime()) &&
                    activityRodeInfo.getToDateTime().equals(sibling.getToDateTime())) {
                return true;
            }
        }
        return false;
    }

    private Map<Long, Double> getWetGritAmounts(List<Long> measureIds, EntityManager em) {
        Map<Long, Double> wetGritAmounts = new HashMap<>();

        if (!measureIds.isEmpty()) {
            // We have to split up the data sent to the SQL-query to avoid hitting the 1000 argument limit of IN.
            int start;
            int stop = 0;

            int roundCounter = 1;
            int rounds = (int) Math.ceil(measureIds.size() / 1000d);

            while (roundCounter <= rounds) {
                start = stop;

                if (roundCounter == rounds) {
                    stop = measureIds.size();
                } else {
                    stop += 1000;
                }

                List<Long> tempList = measureIds.subList(start, stop);
                List<Stroinfo> wetGritAmountsRaw = new GritInfoForMeasuresQuery(tempList, em).getResults();

                for (Stroinfo info : wetGritAmountsRaw) {
                    Double amount = wetGritAmounts.get(info.getMeasureId());

                    if (amount != null) {
                        amount += convertBrineToAmountInCubic(info);
                    } else {
                        amount = convertBrineToAmountInCubic(info);
                    }

                    wetGritAmounts.put(info.getMeasureId(), amount);
                }

                roundCounter++;
            }
        }

        return wetGritAmounts;
    }

    private Double convertBrineToAmountInCubic(Stroinfo stroinfo) {
        if (stroinfo != null) {
            if (stroinfo.getLiterLosning() != null) {
                return stroinfo.getLiterLosning() / 1000;
            } else if (stroinfo.getKgLosning() != null) {
                return stroinfo.getKgLosning() / ManagerConstants.WEIGHT_VOLUME_FACTOR / 1000; // Density of brine/saline solution is 1.18
            } else {
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    private Map<Long, GritProductAmount> getGritProductAmountsForActivities(List<ApprovedActivity> activities, EntityManager em) {
        Map<Long, GritProductAmount> amountForActivityIds = new HashMap<>();

        if (!activities.isEmpty()) {
            // We have to split up the data sent to the SQL-query to avoid hitting the 1000 argument limit of IN.
            int start;
            int stop = 0;

            int roundCounter = 1;
            int rounds = (int) Math.ceil(activities.size() / 1000d);

            while (roundCounter <= rounds) {
                start = stop;

                if (roundCounter == rounds) {
                    stop = activities.size();
                } else {
                    stop += 1000;
                }

                List<ApprovedActivity> tempList = activities.subList(start, stop);
                StringBuilder activityIds = new StringBuilder();

                String separator = "";
                for (ApprovedActivity activity : tempList) {
                    activityIds.append(separator).append(activity.getActivityId());
                    separator = ",";
                }

                List<GritProductAmount> amounts = new GritProductAmountsForActivitiesQuery(activityIds.toString(), em).getGritProductAmounts();

                for (GritProductAmount amount : amounts) {
                    amountForActivityIds.put(amount.getActivityId(), amount);
                }

                roundCounter++;
            }
        }

        return amountForActivityIds;
    }

    public List<ApprovedAmountData> getApprovedAmounts() {
        return approvedAmountData;
    }

}