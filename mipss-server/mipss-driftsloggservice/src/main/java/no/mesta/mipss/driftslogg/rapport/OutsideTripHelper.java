package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Rode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.Date;

import static no.mesta.mipss.driftslogg.rapport.OutsideTripDataFetcher.*;

public class OutsideTripHelper {

    private static final Logger log = LoggerFactory.getLogger(OutsideTripHelper.class);

    private final OutsideTripDataFetcher dataFetcher;
    private final OutsideTripAggregator dataAggregator;

    public OutsideTripHelper(OutsideTripDataFetcher dataFetcher) {
        this.dataFetcher = dataFetcher;
        this.dataAggregator = new OutsideTripAggregator();
    }

    public List<OutsideTripData> getOutsideTrip() {
        log.info("Starting fetching of production outside trips");
        long before = System.currentTimeMillis();
        List<Vehicle> vehicles = dataFetcher.getVehicles();
        if(vehicles.size()==0) {
            return new ArrayList<>();
        }


        List<Trip> trips = dataFetcher.getTrips(vehicles);
        List<Rode> rodes = dataFetcher.getRodes();
        List<RodeConnection> rodeConnections = dataFetcher.getRodeConnections();

        List<Prodtype> prodtypes = dataFetcher.getProdtypes();

        List<ProductionData> productionData = dataFetcher.getProductionData(vehicles, rodes);

        Collections.sort(trips, new TripFromDateComparator());
        Collections.sort(productionData, new ProductionDataVehicleDateComparator());

        productionData = removeIncludedInTrip(productionData, trips);
        Map<DataKey, List<ProductionData>> groupedData = dataAggregator.groupByVehicleRode(productionData);
        List<OutsideTripData> result = dataAggregator.groupByTime(groupedData, trips, prodtypes);
        enrichData(result, rodeConnections, rodes, vehicles);
        sortResult(result);
        log.info("Finished fetching report in {}ms", System.currentTimeMillis()-before);
        return result;
    }

    private void sortResult(List<OutsideTripData> result) {
        result.sort(
                Comparator.comparing(OutsideTripData::getKjoretoyId)
                        .thenComparing(OutsideTripData::getFromDate)
                        .thenComparing(OutsideTripData::getRodeId)
                        .thenComparing(OutsideTripData::getVegtype)
        );
    }

    private void enrichData(List<OutsideTripData> result, List<RodeConnection> rodeConnections, List<Rode> rodes, List<Vehicle> vehicles) {
        for(OutsideTripData data : result) {
            Vehicle vehicle = getVehicle(vehicles, data.getKjoretoyId());
            Rode rode = getRode(rodes, data.getRodeId());
            boolean connection = getConnection(rodeConnections, data.getKjoretoyId(), data.getRodeId(), data.getFromDate());

            data.setKjoretoynavn(vehicle.name);
            data.setLeverandor(vehicle.supplier);
            data.setRodenavn(rode.getNavn());
            if(rode.getUtenforKontrakt()) data.setOutsideContract(true);
            if(connection) data.setOnPloughingRode(true);
        }
    }

    protected boolean getConnection(List<RodeConnection> rodeConnections, long vehicleId, long rodeId, Date date) {
        for(RodeConnection connection : rodeConnections) {
            if(connection.vehicleId == vehicleId && connection.rodeId == rodeId &&
                    isInsideConnection(date, connection)) {
                return true;
            }
        }
        return false;
    }

    private boolean isInsideConnection(Date date, OutsideTripHelper.RodeConnection connection) {
        return (connection.fromDate.compareTo(date) <= 0 && connection.toDate==null) ||
                (connection.fromDate.compareTo(date) <= 0 && connection.toDate.compareTo(date) >= 0);
    }

    protected Rode getRode(List<Rode> rodes, int rodeId) {
        for(Rode rode : rodes) {
            if(rode.getId()==rodeId) {
                return rode;
            }
        }
        log.error("Unable to find rode with id {}", rodeId);
        throw new IllegalArgumentException("Unable to find rode iwth id "+rodeId);
    }

    protected Vehicle getVehicle(List<Vehicle> vehicles, int vehicleId) {
        for(Vehicle vehicle : vehicles) {
            if(vehicle.vehicleId==vehicleId) {
                return vehicle;
            }
        }
        log.error("Unable to find vehicle with id {}", vehicleId);
        throw new IllegalArgumentException("Unable to find vehicle with id "+vehicleId);
    }



    protected List<ProductionData> removeIncludedInTrip(List<ProductionData> data, List<Trip> trips) {
        if (trips.isEmpty()) return data;

        Iterator<ProductionData> dataIt = data.iterator();
        Iterator<Trip> tripIt = trips.iterator();

        List<ProductionData> result = new ArrayList<>();
        Trip trip = tripIt.next();
        while (dataIt.hasNext()) {
            ProductionData current = dataIt.next();

            while (isAfterTrip(trip, current) && tripIt.hasNext()) {
                log.trace("{},{} is after {},{}-{}, comparing with next trip", new Object[]{current.getVehicleId(), current.getFromDate(), trip.getVehicle().getId(), trip.getStartTime(), trip.getEndTime()});
                trip = tripIt.next();
            }
            if (isBeforeTrip(trip, current) || isAfterTrip(trip, current)) {
                log.trace("Data for vehicle {}, fromDate={}, toDate={} added. Comparing with trip {} {} {}", new Object[]{current.getVehicleName(), current.getFromDate(), current.getToDate(), trip.getVehicle().getEksterntNavn(), trip.getStartTime(), trip.getEndTime()});
                result.add(current);
            }
        }

        log.info("Filtered {} rows down to {} that were outside of {} trips", new Object[]{data.size(), result.size(), trips.size()});
        return result;
    }

    private boolean isBeforeTrip(Trip trip, ProductionData current) {
        if (current.getVehicleId() < trip.getVehicleId()) return true;
        if (current.getVehicleId()==trip.getVehicleId() && current.getToDate().compareTo(trip.getStartTime())<=0) return true;
        return false;
    }

    private boolean isAfterTrip(Trip trip, ProductionData current) {
        if (current.getVehicleId() > trip.getVehicleId()) return true;
        if (current.getVehicleId()==trip.getVehicleId() && current.getFromDate().compareTo(trip.getEndTime())>=0) return true;
        return false;
    }

    private static int compareVehicleAndDate(Integer vehicle1, Date date1, Integer vehicle2, Date date2) {
        int result = vehicle1.compareTo(vehicle2);
        if(result!=0) return result;
        return date1.compareTo(date2);
    }

    static class TripFromDateComparator implements Comparator<Trip> {
        @Override
        public int compare(Trip o1, Trip o2) {
            return compareVehicleAndDate(o1.getVehicle().getId().intValue(), o1.getStartTime(), o2.getVehicle().getId().intValue(), o2.getStartTime());
        }
    }
    static class ProductionDataVehicleDateComparator implements Comparator<ProductionData> {
        @Override
        public int compare(ProductionData o1, ProductionData o2) {
            return compareVehicleAndDate(o1.getVehicleId(), o1.getFromDate(), o2.getVehicleId(), o2.getFromDate());
        }
    }

    static class RodeConnection {
        public final long vehicleId;
        public final long rodeId;
        public final Date fromDate;
        public final Date toDate;

        public RodeConnection(long vehicleId, long rodeId, Date fromDate, Date toDate) {
            this.vehicleId = vehicleId;
            this.rodeId = rodeId;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }
    }
}
