package no.mesta.mipss.driftslogg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.driftslogg.Periode;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

@SuppressWarnings("serial")
public class LeverandorKontraktVO implements Serializable{
	private Leverandor leverandor;
	private List<Periode> kontrakter = new ArrayList<Periode>();
	
	public void setLeverandor(Leverandor leverandor) {
		this.leverandor = leverandor;
	}
	public Leverandor getLeverandor() {
		return leverandor;
	}
	/**
	 * Perioden skal inneholde KUN 1 kontrakt.
	 * @param kontrakter
	 */
	public void setKontrakter(List<Periode> kontrakter) {
		this.kontrakter = kontrakter;
	}
	public List<Periode> getKontrakter() {
		return kontrakter;
	}
}
