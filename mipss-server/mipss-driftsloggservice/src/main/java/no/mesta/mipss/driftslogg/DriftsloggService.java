package no.mesta.mipss.driftslogg;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.driftslogg.rapport.*;
import no.mesta.mipss.persistence.driftslogg.*;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.*;
import no.mesta.mipss.persistence.stroing.Stroprodukt;

@Remote
public interface DriftsloggService {
	public static final String BEAN_NAME = "DriftsloggService";
	
	public static final Long PERIODE_STATUSTYPE_OPPRETTET_ID=1l;
	public static final Long PERIODE_STATUSTYPE_SENDT_TIL_GODKJENNING_ID=2l;
	public static final Long PERIODE_STATUSTYPE_GODKJENT_ID=3l;
	public static final Long PERIODE_STATUSTYPE_SENDT_TIL_AGRESSO_ID=4l;
	public static final Long PERIODE_STATUSTYPE_SENT_IO_ID = 7l;

	List<Leverandor> getLeverandorer(Long driftskontraktId);
	List<KontraktLeverandor> getKontraktLeverandorer(Long driftskontraktId);
	List<Levkontrakt> getKontrakterForLeverandor(String leverandorNr);
	/**
	 * Henter en oversikt over fastpriser registrert pr leverand�r
	 * @param dk
	 * @param mnd
	 * @param aar
	 * @return
	 */
	List<FastprisStatus> sjekkFastpris(Driftkontrakt dk, Long mnd, Long aar);

	List<MipssRodeTiltak> searchORData(Trip trip, Long driftkontraktId);
	List<MipssRodeTiltak> sokMipssData(Aktivitet aktivitet, Long driftkontraktId);
	List<Aktivitet> sokAktiviteter(DriftsloggQueryParams params);
	void persistStromengder(List<Stroproduktmengde> changedEntities);
	void persistAktiviteter(List<Aktivitet> changedEntities);
	List<LeverandorKontraktVO> sokPerioder(DriftsloggQueryParams queryParams);
	
	void removeAktivitet(Aktivitet aktivitet);
	/**
	 * Henter alle avtaler for angitt driftskontrakt og leverand�r
	 * @param currentDriftkontrakt
	 * @param leverandor
	 * @return
	 */
	List<AgrArtikkelV> sokAgrLevkontrakt(Driftkontrakt currentDriftkontrakt, Leverandor leverandor);

	List<AgrArtikkelV> getArtikler(Levkontrakt levkontrakt);

	List<Prodtype> getProdtypes();

	List<Samproduksjon> getSamproduksjon();

	List<ArtikkelProdtype> getArtikkelProdtypeMapping(long kontraktId);

	ArtikkelProdtype getArtikkelProdtypeMapping(String leverandorNr, long kontraktId, String levkontraktId, String artikkelId);

	void saveArtikkelProdtypeMapping(ArtikkelProdtype mapping);

	boolean sjekkRegistrering(Levkontrakt toBeRemoved);
	
	/**
	 * Henter leverand�rnummerne til alle leverand�rer som ikke skal overf�res til Agresso.
	 * @return
	 */
	List<String> getLeverandorNummerSomIkkeSkalOverfores();
	
	/**
	 * Henter alle leverand�rer som ikke skal overf�res til Agresso
	 * @return
	 */
	List<Leverandor> getLeverandorerSomIkkeSkalOverfores(Long driftkontraktId);
	
	/**
	 * Henter alle perioder som skal overf�res til agresso med tilh�rende levkontrakt_ident
	 * @param driftkontraktId
	 * @param mnd
	 * @param aar
	 * @return
	 */
	List<PeriodeKontrakt> getStatusPerioder(Long driftkontraktId, Long mnd, Long aar);
	
	List<PeriodeKontrakt> getPerioder(Date firstDateInMonth, Date lastDate, Long driftkontraktId, List<Long> perioderSomManglerAttestant, boolean ikkeGodkjent);

	List<PeriodeKontrakt> getPerioder(Date firstDateInMonth, Date lastDate, Long driftkontraktId, List<Long> perioderSomManglerAttestant, boolean include, Integer... statuses);
	
	
	/**
	 * Returnerer true hvis brukeridenten ligger som attestant eller vikar i agresso
	 * @param brukerIdent
	 * @param levkontrakt_ident
	 * @return
	 */
	boolean sjekkAttestering(String brukerIdent, String levkontrakt_ident);
	int sendToIO(List<MipssInput> aktiviteter, Long mnd, Long aar, String brukerIdent);
	void flagActivitiesAsProcessed(List<MipssInput> activities, Long month, Long year);
	void saveProcessedActivities(List<MipssInput> activities);
	List<MipssInputTreeView> getAktiviteterTilAgresso(Long driftkontraktId, Long mnd, Long aar, List<Long> perioderSomManglerAttestant, String brukerIdent);
	List<MipssInputTreeView> getAktiviteterIkkeOverfores(Long driftkontraktId, Long mnd, Long aar, List<Long> perioderSomManglerAttestant, String brukerIdent);
	/**
	 * Henter n�kkeltall for valgte leverand�r gitt uke.
	 * @param params
	 * @param rodeId
	 * @param kjoretoyId
	 * @return
	 */
	Nokkeltall hentNokkelTall(DriftsloggQueryParams params, Long rodeId, Long kjoretoyId);
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	List<PeriodeSok> sokPeriodeStatus(DriftsloggQueryParams params);
	
	List<Kjoretoy> getKjoretoyForLeverandor(String leverandorNr);
	List<Rode> getRodeForKontrakt(Long rodetypeId, Long driftkontraktId);
	
	DriftsloggRapportData getRapport(DriftsloggQueryParams params, Date fraDato, Date tilDato) throws NoArtikkelException;
	boolean sjekkR12(DriftsloggQueryParams queryParams);

	List<R12RapportObject> getVinterdriftsrapport(long kontraktId, Date fraDato, Date tilDato) throws NoArtikkelException;
	
	List<Stroprodukt> getAllStroprodukter();
	
	/**
	 * Returnerer en liste med perioder basert p� s�keparamterne uke og levkontraktid
	 * @param uke
	 * @param levkontraktIdent
	 */
	List<PeriodeSok> sokPeriodeStatusMedParams(Date uke, String levkontraktIdent);
	
	/**
	 * Returnerer en liste med v�rrapportbjekter som viser v�rdata rapportert inn av UE(DL-Web) basert p� dato og kontrakt.
	 */
	List<VaerRapportObject> getVaerRapport(DriftsloggQueryParams queryParams,Date fraDato, Date tilDato);
	
	/**
	 * Returnerer en liste med aktiviteter som tilh�rer en valgt kontrakt i et gitt tidsrom (ikke basert p� uker, men p� datoer)
	 * @param queryParams
	 * @param fraDato
	 * @param tilDato
	 * @return
	 */
	List<Aktivitet> getExportToExcelEgendefRapport(DriftsloggQueryParams queryParams, Date fraDato, Date tilDato);
	
	/**
	 * Returnerer en gitt stroproduktmengde basert p� id
	 * Benyttes f.eks til � hente ut opprinnelige data f�r en endring.
	 * @param id
	 * @return
	 */
	Stroproduktmengde getStroproduktmengdeById(Long id);

	List<ApprovedAmountData> getApprovedAmounts(DriftsloggQueryParams queryParams, Date fraDato, Date tilDato);

	List<DeviantActivity> getDeviantActivities(DriftsloggQueryParams queryParams, Date fraDato, Date tilDato);

	List<OutsideTripData> getAmountOutsideTrip(DriftsloggQueryParams queryParams, Date fromDate, Date toDate);

	List<KjoretoyRode> getConnectionsForContract(Long contractId);

	List<Trip> searchTrips(DriftsloggQueryParams params);

	List<Trip> searchTrips(DriftsloggQueryParams params, Date fromDate, Date toDate);

	Trip persistTrip(Trip trip);

	List<Trip> getTripsForPeriodId(Long periodId);

	void closePeriods(List<PeriodeStatus> periodStatuses);

	void openPeriods(List<PeriodeStatus> periodStatuses);

    List<AgrArtikkelV> getArticlesForTrip(Trip trip);

    Stroproduktmengde addOrUpdateGritProductAmount(Stroproduktmengde gritProductAmount);

    Aktivitet addOrUpdateActivity(Aktivitet aktivitet);

	TripReturnCause addTripReturnCause(TripReturnCause cause);

	void sendMail(String sender, String recipient, String subject, String message);

	KontraktLeverandor getContractContractor(Long contractId, String contractorId);

	List<KjoretoyRode> getVehicleRodeConnectionsForVehicle(Long contractId, Long vehicleId);

	int getNumberOfTripsRegisteredNotSubmitted(Long contractId);

	int getNumberOfTripsSubmittedNotProcessed(Long contractId);

}