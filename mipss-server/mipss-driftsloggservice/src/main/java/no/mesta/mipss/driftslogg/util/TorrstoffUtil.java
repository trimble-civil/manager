package no.mesta.mipss.driftslogg.util;

import no.mesta.mipss.common.ManagerConstants;

public class TorrstoffUtil {

	/**
	 * Beregner hvor mye t�rrstoff som ligger i v�tstoffet. 
	 * @param enhetVatt
	 * @param mengde
	 * @return
	 */
	public static double tonnTorr(String enhetVatt, Double mengde){
		if (mengde==null)
			return 0;
		if (enhetVatt==null)
			return 0;
		if (mengde==0)
			return 0;
		if (enhetVatt.equals("Liter")){
			mengde = mengde*ManagerConstants.WEIGHT_VOLUME_FACTOR;
		}
		mengde = mengde*0.23;
		return mengde;
	}
	
	/**
	 * Beregner om fra m3 til liter basert p� egenvekt
	 * @param enhet
	 * @param mengde
	 * @return
	 */
	public static double m3ToLiter(String enhet, Double mengde){
		if (mengde==null)
			return 0;
		if (enhet==null)
			return 0;
		if (mengde==0)
			return 0;
		if (enhet.equals("Liter")){
		}else{
			mengde = mengde/ ManagerConstants.WEIGHT_VOLUME_FACTOR;//liter
		}
		return mengde;
	}
}
