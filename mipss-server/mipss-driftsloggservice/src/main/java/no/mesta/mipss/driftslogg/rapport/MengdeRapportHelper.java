package no.mesta.mipss.driftslogg.rapport;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.driftslogg.NoArtikkelException;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.Stroproduktmengde;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;

import static no.mesta.mipss.common.TripProcessStatus.*;

public class MengdeRapportHelper {
	
	private final DriftsloggBean bean;

	private static final List<TripProcessStatus> approvedStatuses = Arrays.asList(
			PROCESSED_BY_CONTRACTOR, SENT_TO_IO, PAID
	);

	public MengdeRapportHelper(DriftsloggBean bean){
		this.bean = bean;
		
	}
	
	/**
	 * Legger til mipsstall til rapporten.
	 * @param rappResults
	 * @param leverandorer
	 */
	public void appendMipsstallFromRapp1(List<Rapp1> rappResults, Map<String, MengdeRapportObject> leverandorer){
		for (Rapp1 r:rappResults){
			getValuesForLeverandor(leverandorer, r);
		}
	}
	
	/**
	 * Legger til mipsstall til rapporten.
	 * @param rappResults
	 * @param roder
	 */
	public void appendMipsstallFromRapp1ToRoder(List<Rapp1> rappResults, Map<String, MengdeRapportObject> roder){
		for (Rapp1 r:rappResults){
			getValuesForRode(roder, r);
		}
	}
	
	/**
	 * Henter mipsstall fra en rad i rapp1 for leverandøren som matcher fra aktiviteten. 
	 * Dersom leverandøren ikke finnes i listen, har ikke leverandørn sendt inn brøytekort, ikke fått det godkjent enda eller mangler instrumentering.
	 * @param leverandorer
	 * @param rapp1Row
	 */
	private void getValuesForLeverandor(Map<String, MengdeRapportObject> leverandorer, Rapp1 rapp1Row) {
		String levNrNavn = getLevNrNavn(rapp1Row.getLeverandorNavn(), rapp1Row.getLeverandorNr().toString());
		MengdeRapportObject leverandorRapport = leverandorer.get(levNrNavn);
		if (leverandorRapport==null){//leverandor har ikke sendt inn brøytekort, evt er de ikke godkjent enda. opprett mipssdata
			leverandorRapport = new MengdeRapportObject();
			leverandorRapport.setLeverandor(levNrNavn);
			leverandorer.put(levNrNavn, leverandorRapport);
		}
		for (R12Prodtype pt:rapp1Row.getProdtyper()){
			getValuesFromProdtyper(pt, leverandorRapport);
		}
		for (R12Stroprodukt s:rapp1Row.getStroprodukter()){
			getValuesFromR12Stroprodukter(s, leverandorRapport);
		}
	}
	
	/**
	 * Henter mipsstall fra en rad i rapp1 for roden som matcher fra aktiviteten. 
	 * Dersom roden ikke finnes i listen, har ikke leverandøren sendt inn brøytekort, ikke fått det godkjent enda eller mangler instrumentering.
	 * @param roder
	 * @param rapp1Row
	 */
	private void getValuesForRode(Map<String, MengdeRapportObject> roder, Rapp1 rapp1Row) {
		String rodeNrNavn = getRodeNrNavn(rapp1Row.getRodeNavn(), rapp1Row.getRodeId().toString());
		MengdeRapportObject rodeRapport = roder.get(rodeNrNavn);
		if (rodeRapport==null){//leverandor har ikke sendt inn brøytekort, evt er de ikke godkjent enda. opprett mipssdata
			rodeRapport = new MengdeRapportObject();
			rodeRapport.setRode(rodeNrNavn);
			roder.put(rodeNrNavn, rodeRapport);
		}
		for (R12Prodtype pt:rapp1Row.getProdtyper()){
			getValuesFromProdtyper(pt, rodeRapport);
		}
		for (R12Stroprodukt s:rapp1Row.getStroprodukter()){
			getValuesFromR12Stroprodukter(s, rodeRapport);
		}
	}

	/**
	 * Henter alle verdiene fra aktiviteter og fordeler dem i et map pr leverandør.
	 * @param aktiviteter
	 * @return
	 * @throws NoArtikkelException
	 */
	public Map<String, MengdeRapportObject> getValuesFromAktiviteter(List<Aktivitet> aktiviteter, Map<String, String> leverandorNrCache) throws NoArtikkelException{
		Map<String, MengdeRapportObject> leverandorer = new HashMap<String, MengdeRapportObject>();

		for (Aktivitet aktivitet:aktiviteter){
			TripProcessStatus processStatus = aktivitet.getTrip().getProcessStatus();

			if (approvedStatuses.contains(processStatus) && aktivitet.isApprovedBuild()) {
				MengdeRapportObject leverandorRapport = leverandorer.get(aktivitet.getPeriode().getLevkontrakt().getLeverandorNr());
				if (leverandorRapport==null){
					leverandorRapport = new MengdeRapportObject();
					leverandorer.put(aktivitet.getPeriode().getLevkontrakt().getLeverandorNr(), leverandorRapport);
				}
				List<Stroproduktmengde> stroprodukter = aktivitet.getStroprodukter();
				getValuesFromStroprodukter(stroprodukter, leverandorRapport);

				AgrArtikkelV artikkel = bean.getArtikkelViewFromAktivitet(aktivitet);
				if (artikkel != null) {
					getValuesFromArtikkel(artikkel, aktivitet, leverandorRapport);
				}
				checkRemovedArticle(aktivitet, artikkel);
			}
		}
		
		Map<String, MengdeRapportObject> prLevNavn = new HashMap<String, MengdeRapportObject>();
		for (String levNr:leverandorer.keySet()){
			MengdeRapportObject rapp = leverandorer.get(levNr);
			String levNavn = leverandorNrCache.get(levNr);
			if (levNavn == null){
				levNavn = bean.getLeverandorNavnFromNr(levNr);
				leverandorNrCache.put(levNr, levNavn);
			}
			levNavn = getLevNrNavn(levNavn, levNr);
			rapp.setLeverandor(levNavn);
			prLevNavn.put(levNavn, rapp);
		}
		
		return prLevNavn;
	}

	private void checkRemovedArticle(Aktivitet aktivitet, AgrArtikkelV artikkel) throws NoArtikkelException {
		if ( artikkel == null && aktivitet.getArtikkelIdent() != null ) {
            String levk = aktivitet.getPeriode().getLevkontraktIdent();
            String artikkelId = aktivitet.getArtikkelIdent();
            String err = "Artikkelen : "+artikkelId+" er fjernet fra Agresso for avtale:"+levk;
            throw new NoArtikkelException(err);
        }
	}

	public String getLevNrNavn(String leverandorNavn, String leverandorNr){
		return leverandorNavn +" ("+leverandorNr+")";
	}
	
	private String getRodeNrNavn(String rodeNavn, String rodeNr) {
		// TODO Auto-generated method stub
		return rodeNavn + " (" + rodeNr + ")";
	}
	
	/**
	 * Henter alle verdiene fra aktiviteter og fordeler dem i et map pr rode.
	 * @param aktiviteter
	 * @return
	 * @throws NoArtikkelException
	 */
	public Map<String, MengdeRapportObject> getRodeValuesFromAktiviteter(List<Aktivitet> aktiviteter, Map<Long, String> rodeNrCache) throws NoArtikkelException{
		Map<Long, MengdeRapportObject> roder = new HashMap<Long, MengdeRapportObject>();
		
		for (Aktivitet aktivitet:aktiviteter){
			TripProcessStatus processStatus = aktivitet.getTrip().getProcessStatus();

			if (approvedStatuses.contains(processStatus) && aktivitet.isApprovedBuild()) {
				MengdeRapportObject rodeRapport = roder.get(aktivitet.getRodeId());
				if (rodeRapport==null){
					rodeRapport = new MengdeRapportObject();
					if(aktivitet.getRodeId() != null)
						roder.put(aktivitet.getRodeId(), rodeRapport);
				}
				List<Stroproduktmengde> stroprodukter = aktivitet.getStroprodukter();
				getValuesFromStroprodukter(stroprodukter, rodeRapport);
				
				AgrArtikkelV artikkel = bean.getArtikkelViewFromAktivitet(aktivitet);
				if (artikkel != null) {
					getValuesFromArtikkel(artikkel, aktivitet, rodeRapport);
				}
				checkRemovedArticle(aktivitet, artikkel);
			}
		}
		
		Map<String, MengdeRapportObject> prRodeNavn = new HashMap<String, MengdeRapportObject>();
		for (Long rodeId:roder.keySet()){
			MengdeRapportObject rapp = roder.get(rodeId);
			String rodeNavn = rodeNrCache.get(rodeId);
			if (rodeNavn == null){
				rodeNavn = bean.getRodeNavnFromId(rodeId);
				rodeNrCache.put(rodeId, rodeNavn);
			}
			rodeNavn = getRodeNavn(rodeNavn, rodeId);
			rapp.setRode(rodeNavn);
			prRodeNavn.put(rodeNavn, rapp);
		}
		
		return prRodeNavn;
	}
	
	public String getRodeNavn(String rodeNavn, Long rodeId){
		return rodeNavn +" ("+rodeId+")";
	}
	
	/**
	 * Henter verdier fra prodtypene: brøyting og høvling fra mipss-rapporten
	 * @param prodtype
	 * @param rapp
	 */
	private void getValuesFromProdtyper(R12Prodtype prodtype, MengdeRapportObject rapp){
		Double kjortKm = prodtype.getKjortKm();
		Double kjortSekund = prodtype.getKjortSekund().doubleValue();
		Long id = prodtype.getId();
		if (id==6||id==7){//brøyting eller høvling 
			rapp.addKmBroytMipss(kjortKm);
			rapp.addTimerBroytMipss(kjortSekund/60/60);
		}
		//Tung høvling
		if (id == 24) {
			rapp.addKmTungHovelMipss(kjortKm);
			rapp.addTimerTungHovelMipss(kjortSekund/60/60);
		}
	}
	/**
	 * Henter verdier fra stroprodukter fra mipss-rapporten
	 * @param s
	 * @param rapp
	 */
	private void getValuesFromR12Stroprodukter(R12Stroprodukt s, MengdeRapportObject rapp) {
		Double kjortSekund = s.getKjortSekund().doubleValue();
		Long id = s.getId();
		
		switch(id.intValue()){
		case 1: rapp.addTonnTorrsaltMipss(s.getMengdeTonn());break;
		case 2: rapp.addTonnSaltSandMipss(s.getMengdeTonn());break;
		case 3: rapp.addTonnRenSandMipss(s.getMengdeTonn());break;
		case 4: rapp.addTonnSlurryMipss(s.getMengdeTonn());break;
		case 5: rapp.addM3SaltlosningMipss(s.getMengdeM3());break;
		case 6: rapp.addTonnBefuktetSaltMipss(s.getMengdeTonn());break;
		case 7: rapp.addTonnFastsandMipss(s.getMengdeTonn());break;
		}
		rapp.addTimerStroMipss(kjortSekund/60/60);
		rapp.addKmStroMipss(s.getKjortKm());
	}
	/**
	 * Henter verdier fra rapporterte strøproduktmengder
	 * @param stroprodukter
	 * @param rapp
	 */
	private void getValuesFromStroprodukter(List<Stroproduktmengde> stroprodukter, MengdeRapportObject rapp){
		if (stroprodukter!=null){
			for (Stroproduktmengde sm:stroprodukter){
				Long id = sm.getStroproduktId();
				switch(id.intValue()){
				case 1: rapp.addTonnTorrsalt(sm.getOmforentMengdeTort());break;
				case 2: rapp.addTonnSaltSand(sm.getOmforentMengdeTort());break;
				case 3: rapp.addTonnRenSand(sm.getOmforentMengdeTort());break;
				case 4: rapp.addTonnSlurry(sm.getOmforentMengdeTort(), sm.getOmforentMengdeVaatt(), sm.getEnhetVaatt());break;
				case 5: rapp.addM3Saltlosning(sm.getOmforentMengdeVaatt(), sm.getEnhetVaatt());break;
				case 6: rapp.addTonnBefuktetSalt(sm.getOmforentMengdeTort(), sm.getOmforentMengdeVaatt(), sm.getEnhetVaatt());break;
				case 7: rapp.addTonnFastsand(sm.getOmforentMengdeTort());break;
				}
			}
		}
	}
	
	/**
	 * Henter verdier fra rapporterte aktiviteter. 
	 * Bruker kun verdier fra aktiviteter som er registrert på gruppene: Brøyting, samproduksjon, strøing. 
	 * Samproduksjon timer rapporteres kun som brøyting (ikke strøing).
	 * @param artikkel
	 * @param a
	 * @param rapp
	 */
	private void getValuesFromArtikkel(AgrArtikkelV artikkel, Aktivitet a, MengdeRapportObject rapp){
		String gruppeNavn = artikkel.getGruppeNavn().toLowerCase();
		if (gruppeNavn.equals(R12RapportHelper.PLOWING)||gruppeNavn.equals(R12RapportHelper.CO_PRODUCTION)){
			if (a.getEnhet().toLowerCase().startsWith("tim")){
				rapp.addTimerBroyt(a.getOmforentMengde());
				rapp.addKmBroyt(a.getOmforentKm());
			}
			if (a.getEnhet().toLowerCase().startsWith("km")){
				rapp.addKmBroyt(a.getOmforentKm());
			}
		}
		if (gruppeNavn.equals(R12RapportHelper.CO_PRODUCTION)){
			if (a.getEnhet().toLowerCase().startsWith("tim")){
				rapp.addTimerSamprod(a.getOmforentMengde());
			}

			rapp.addKmSamprod(a.getR12_broyting());
		}
		if (gruppeNavn.equals(R12RapportHelper.GRITTING)){
			if (a.getEnhet().toLowerCase().startsWith("tim")){
				rapp.addTimerStro(a.getOmforentMengde());
			}
			if (a.getEnhet().toLowerCase().startsWith("km")){
				rapp.addKmStro(a.getOmforentKm());
			}
		}
		if (gruppeNavn.equals(R12RapportHelper.HEAVY_GRADER)){
			if (a.getEnhet().toLowerCase().startsWith("tim")){
				rapp.addTimerTungHovel(a.getOmforentMengde());
			}
			if (a.getEnhet().toLowerCase().startsWith("km")){
				rapp.addKmTungHovel(a.getOmforentKm());
			}
		}
	}
}
