package no.mesta.mipss.driftslogg.util;

import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarHelper {

	public static Date getFirstTimeInWeek(Date date) {
		Calendar c = Calendar.getInstance(Locale.forLanguageTag("no"));
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
		c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        return c.getTime();
	}

	public static Date getLastTimeInWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(getFirstTimeInWeek(date));
        c.add(Calendar.DATE, 7);
        c.add(Calendar.SECOND, -1);
        return c.getTime();
	}

	public static Date getLastDateInMonth(Long mnd, Long aar) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.MONTH, mnd.intValue());
		c.set(Calendar.YEAR, aar.intValue());

		int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		c.set(Calendar.DAY_OF_MONTH, lastDay);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		return c.getTime();
	}

	public static Date getFirstDateInMonth(Long mnd, Long aar) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.MONTH, mnd.intValue());
		c.set(Calendar.YEAR, aar.intValue());
        return c.getTime();
	}

	public static Date getFirstDateCurrentMonth() {
		return getFirstDateInMonth(new Date());
	}

	public static Date getLastDateCurrentMonth() {
		return getLastDateInMonth(new Date());
	}

	public static ZonedDateTime convertDateWithTimeToZonedDateTime(Date d) {
		return ZonedDateTime.ofInstant(d.toInstant(), ZoneOffset.systemDefault());
	}

	public static ZonedDateTime convertDateToZonedDateTime(Date d) {
		return ZonedDateTime.ofInstant(d.toInstant(), ZoneOffset.systemDefault())
				.withHour(0).withMinute(0).withSecond(0).withNano(0);
	}

	public static Date getFirstDateInMonth(Date month) {
		ZonedDateTime start = convertDateToZonedDateTime(month).with(TemporalAdjusters.firstDayOfMonth());
		return Date.from(start.toInstant());
	}

	public static Date getLastDateInMonth(Date month) {
		ZonedDateTime end = convertDateToZonedDateTime(month).with(TemporalAdjusters.lastDayOfMonth());
		return Date.from(end.toInstant());
	}

	public static Date getFirstTimeInMonth(Date month) {
		return getFirstDateInMonth(month);
	}

	public static Date getLastTimeInMonth(Date month) {
		ZonedDateTime zoned = convertDateToZonedDateTime(month);
		zoned = zoned.with(TemporalAdjusters.lastDayOfMonth()).withHour(23).withMinute(59).withSecond(59).withNano(999999999);
		return Date.from(zoned.toInstant());
	}

	public static double getDurationInHours(Date fromDate, Date toDate) {
		ZonedDateTime from = CalendarHelper.convertDateWithTimeToZonedDateTime(fromDate);
		ZonedDateTime to = CalendarHelper.convertDateWithTimeToZonedDateTime(toDate);

		return ((double) Duration.between(from, to).getSeconds() / 3600);
	}

}