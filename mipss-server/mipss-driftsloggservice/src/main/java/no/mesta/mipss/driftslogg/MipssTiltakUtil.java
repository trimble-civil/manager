package no.mesta.mipss.driftslogg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import no.mesta.mipss.persistence.driftslogg.MipssTiltak;

public class MipssTiltakUtil {

	public static List<MipssRodeTiltak> convertTiltakToRodeTiltak(List<MipssTiltak> tiltak){
		Map<String, List<MipssTiltak>> rodetiltak = new HashMap<>();
		Map<String, List<MipssTiltak>> sum = new HashMap<>();

		for (MipssTiltak t:tiltak){
			if (t.getProdtype().equals("Br\u00F8yting intensjon")) {
				continue;
			}

			addToMap(t.getRode(), t, rodetiltak);
			addToMap(t.getProdtype(), t, sum);
		}
		
		List<MipssRodeTiltak> rt = new ArrayList<>();
		for (String key:rodetiltak.keySet()){
			List<MipssTiltak> list = rodetiltak.get(key);
			MipssRodeTiltak t = new MipssRodeTiltak();
			t.setRode(key);
			t.setTiltak(list);
			rt.add(t);
		}
		if (rt.isEmpty())
			return null;
		
		//Lag summer
		Set<String> keySet = sum.keySet();
		List<MipssTiltak> sumList = new ArrayList<>();
		for (String key:keySet){
			List<MipssTiltak> list = sum.get(key);
			MipssTiltak t = merge(list);
			sumList.add(t);
		}
		
		MipssRodeTiltak sumlinje = new MipssRodeTiltak();
		sumlinje.setRode("Sum");
		sumlinje.setTiltak(sumList);
		rt.add(sumlinje);
		return rt;
	}
	
	private static void addToMap(String key, MipssTiltak t, Map<String, List<MipssTiltak>> map){
		List<MipssTiltak> list = map.get(key);
		if (list==null){
			list = new ArrayList<MipssTiltak>();
			map.put(key, list);
		}
		list.add(t);
	}
	
	private static MipssTiltak merge(List<MipssTiltak> list){
		MipssTiltak t = new MipssTiltak();
		t.setRode("Sum");
		
		MipssTiltak siste = null;
		for (MipssTiltak tt:list){
			t.setKm(mergeDouble(t.getKm(), tt.getKm()));
			t.setTimer(mergeDouble(t.getTimer(), tt.getTimer()));
			t.setTonn(mergeDouble(t.getTonn(), tt.getTonn()));
			t.setLiterLosningTotalt(mergeDouble(t.getLiterLosningTotalt(), tt.getLiterLosningTotalt()));
			t.setKgLosning(mergeDouble(t.getKgLosning(), tt.getKgLosning()));
			siste = tt;
		}
		if (siste!=null){
			t.setNavn(siste.getNavn());
			t.setStroprodukt(siste.getStroprodukt());
			t.setStroproduktgruppe(siste.getStroproduktgruppe());
		}
		return t;
	}
	
	
	private static Double mergeDouble(Double d1, Double d2){
		if (d1==null&&d2==null){
			return null;
		}
		if (d1==null){
			return d2;
		}
		if (d2==null){
			return d1;
		}
		return d1+d2;
	}
}
