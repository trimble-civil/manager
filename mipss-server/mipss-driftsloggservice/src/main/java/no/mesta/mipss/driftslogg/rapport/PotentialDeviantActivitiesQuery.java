package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import no.mesta.mipss.persistence.driftslogg.PotentialDeviantActivity;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class PotentialDeviantActivitiesQuery {

    private Query q;

    public PotentialDeviantActivitiesQuery(DriftsloggQueryParams params, Date fraDato, Date tilDato, EntityManager em) {
        q = em.createNativeQuery(buildGetActivitiesQuery())
                .setParameter(1, params.getKontrakt().getId())
                .setParameter(2, fraDato)
                .setParameter(3, tilDato);
    }

    private String buildGetActivitiesQuery() {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT");
        sb.append(" l.navn AS vendorName,");
        sb.append(" k.eksternt_navn AS vehicleExternalName,");
        sb.append(" kjoretoy_id AS vehicleId,");
        sb.append(" a.id AS activityId,");
        sb.append(" a.trip_id,");
        sb.append(" fra_dato_tid AS fromDateTime,");
        sb.append(" til_dato_tid AS toDateTime,");
        sb.append(" r.navn AS rodeName,");
        sb.append(" production_type,");
        sb.append(" kommentar AS reporterComment,");
        sb.append(" intern_kommentar AS contractorComment,");
        sb.append(" manually_created AS manuallyCreated,");
        sb.append(" production_outside_contract,");
        sb.append(" production_outside_rode,");
        sb.append(" approved_build,");
        sb.append(" r12_broyting AS r12Planing,");
        sb.append(" r12_sideplog AS r12SidePlough,");
        sb.append(" r12_hovling AS r12HeavyPlaning ");

        sb.append("FROM aktivitet a ");

        sb.append("JOIN trip t ON a.trip_id = t.id ");
        sb.append("JOIN tripprocessstatus tps on tps.id=t.tripprocessstatus ");
        sb.append("JOIN periode p ON t.periode_id = p.id ");
        sb.append("JOIN levkontrakt lk ON p.levkontrakt_ident = lk.levkontrakt_ident ");
        sb.append("LEFT OUTER JOIN rode r ON a.rode_id = r.id ");
        sb.append("LEFT OUTER JOIN leverandor l ON t.vendor_id = l.nr ");
        sb.append("LEFT OUTER JOIN kjoretoy k ON a.kjoretoy_id = k.id ");

        sb.append("WHERE");
        sb.append(" tps.id in ('6','7','8a') ");
        sb.append(" AND lk.driftkontrakt_id = ?1");
        sb.append(" AND fra_dato_tid BETWEEN ?2 AND ?3 ");

        sb.append("ORDER BY");
        sb.append(" l.navn,");
        sb.append(" k.eksternt_navn,");
        sb.append(" fra_dato_tid");

        return sb.toString();
    }

    public List<PotentialDeviantActivity> getPotentialDeviantActivities() {
        NativeQueryWrapper<PotentialDeviantActivity> qry = new NativeQueryWrapper<>(
                q,
                PotentialDeviantActivity.class,
                PotentialDeviantActivity.getPropertiesArray());

        return qry.getWrappedList();
    }

}