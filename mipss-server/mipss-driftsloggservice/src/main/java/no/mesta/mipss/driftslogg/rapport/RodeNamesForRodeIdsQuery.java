package no.mesta.mipss.driftslogg.rapport;


import no.mesta.mipss.persistence.driftslogg.ActivityRodeIdName;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class RodeNamesForRodeIdsQuery {

    private Query q;

    public RodeNamesForRodeIdsQuery(List<ApprovedActivity> approvedActivities, EntityManager em) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT id AS rodeId, navn AS rodeName FROM rode WHERE ");

        String or = "";
        for (ApprovedActivity activity : approvedActivities) {
            sb.append(or);
            sb.append("id = ").append(activity.getRodeId());

            or = " OR ";
        }

        q = em.createNativeQuery(sb.toString());
    }

    public List<ActivityRodeIdName> getRodeIdNameList() {
        NativeQueryWrapper<ActivityRodeIdName> qry = new NativeQueryWrapper<>(
                q,
                ActivityRodeIdName.class,
                ActivityRodeIdName.getPropertiesArray());

        return qry.getWrappedList();
    }

}