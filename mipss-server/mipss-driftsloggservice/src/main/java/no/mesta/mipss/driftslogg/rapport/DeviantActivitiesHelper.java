package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import no.mesta.mipss.persistence.driftslogg.DeviantActivity;
import no.mesta.mipss.persistence.driftslogg.GritProductAmount;
import no.mesta.mipss.persistence.driftslogg.PotentialDeviantActivity;

import javax.persistence.EntityManager;
import javax.validation.constraints.Null;
import java.util.*;

public class DeviantActivitiesHelper {

    private static final String PROD_OUTSIDE_CONTRACT_MESSAGE = "Produsert utenfor kontraktsomr\u00E5de";
    private static final String PROD_OUTSIDE_RODE_MESSAGE = "Br\u00F8ytet utenfor tilknyttet br\u00F8yterode";
    private static final String MANUALLY_CREATED_MESSAGE = "Mangler automatisk datafangst";
    private static final String OVERRULED_MESSAGE = "Underkjent";

    private static final String GRITTING = "Str\u00F8ing";
    private static final String PLANING = "Br\u00F8yting";
    private static final String HEAVY_PLANING = "Tung h\u00F8vling";
    private static final String SIDE_PLOUGH = "Sideplog";
    private static final List<String> LEGAL_PROD_TYPES = Arrays.asList(GRITTING, PLANING, HEAVY_PLANING, SIDE_PLOUGH);

    private List<DeviantActivity> deviantActivities;

    public DeviantActivitiesHelper() {}

    public DeviantActivitiesHelper(DriftsloggQueryParams params, Date fromDateTime, Date toDateTime, EntityManager em) {
        deviantActivities = new ArrayList<>();

        List<PotentialDeviantActivity>  potentialDeviantActivities = new PotentialDeviantActivitiesQuery(
                params, fromDateTime, toDateTime, em).getPotentialDeviantActivities();

        for (PotentialDeviantActivity potential : potentialDeviantActivities) {
            if (isActivityDeviant(potential)) {
                deviantActivities.addAll(convertPotentialToDeviants(potential));
            }
        }

        HashMap<Long, GritProductAmount> amountForActivityIds = getGritProductAmountsForActivities(deviantActivities, em);

        for (DeviantActivity deviant : deviantActivities) {
            if (deviant.getGritType() != null) {
                GritProductAmount gritProductAmount = amountForActivityIds.get(deviant.getActivityId());
                findAmountForGrit(gritProductAmount, deviant);
            }
        }
    }

    private boolean isActivityDeviant(PotentialDeviantActivity activity) {
        if (activity != null) {
            Boolean reportedInDAU = isReportedInDAU(activity);
            Boolean isApprovedBuild = activity.isApprovedBuild() != null ? activity.isApprovedBuild() : Boolean.FALSE;

            boolean negativeDeviation = reportedInDAU && !isApprovedBuild;
            boolean positiveDeviation = !reportedInDAU && isApprovedBuild;

            activity.setNegativeDeviation(negativeDeviation);
            activity.setPositiveDeviation(positiveDeviation);

            return positiveDeviation || negativeDeviation;
        } else {
            return false;
        }
    }

    private Boolean isReportedInDAU(PotentialDeviantActivity activity) {
        Boolean reportedInDAU = Boolean.TRUE;

        if (activity != null) {
            if (activity.isProductionOutsideContract() != null) {
                reportedInDAU = !activity.isProductionOutsideContract();
            }

            if (reportedInDAU) {
                if (activity.isProductionOutsideRode() != null) {
                    reportedInDAU = !activity.isProductionOutsideRode();
                }
            }

            if (reportedInDAU) {
                if (activity.isManuallyCreated() != null) {
                    reportedInDAU = !activity.isManuallyCreated();
                }
            }
        }

        return reportedInDAU;
    }

    private List<DeviantActivity> convertPotentialToDeviants(PotentialDeviantActivity potential) {
        DeviantActivity deviant = new DeviantActivity();

        if (potential != null) {
            deviant.setContractorName(potential.getContractorName());
            deviant.setVehicleExternalName(potential.getVehicleExternalName());
            deviant.setVehicleId(potential.getVehicleId());
            deviant.setActivityId(potential.getActivityId());
            deviant.setTripId(potential.getTripId());
            deviant.setFromDateTime(MipssDateFormatter.formatDate(potential.getFromDateTime(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
            deviant.setToDateTime(MipssDateFormatter.formatDate(potential.getToDateTime(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
            deviant.setRodeName(potential.getRodeName());
            deviant.setDeviationMessage(findDeviationCause(potential));
            deviant.setReporterComment(potential.getReporterComment());
            deviant.setContractorComment(potential.getContractorComment());
            deviant.setNegativeDeviation(potential.hasNegativeDeviation());
        }

        return splitDeviant(potential, deviant);
    }

    List<DeviantActivity> splitDeviant(PotentialDeviantActivity potential, DeviantActivity deviant) {
        List<DeviantActivity> deviants = new ArrayList<>();

        if (potential != null && deviant != null) {
            String prodType = potential.getProductionType();
            String gritType = null;

            if (prodType != null) {
                int gritTypeIndex = prodType.indexOf(":");
                if(gritTypeIndex >= 0) {
                    gritType = prodType.substring(gritTypeIndex + 1, prodType.length()).trim();
                    prodType = prodType.substring(0, prodType.indexOf(':')).trim();
                }

                for (String prod : prodType.split("\\+")) {
                    if (isLegalProdType(prod)) {
                        DeviantActivity deviantSplit = new DeviantActivity(deviant);
                        deviantSplit.setProdType(prod);

                        findPlaningType(potential, deviantSplit);

                        if (deviantSplit.getProdType().equals(GRITTING)) {
                            if (!potential.isProductionOutsideRode()) {
                                deviantSplit.setGritType(gritType);
                            } else {
                                // No need to split this activity - the grit production have already been sent in using DAU.
                                continue;
                            }
                        }

                        deviants.add(deviantSplit);
                    }
                }
            } else {
                // No need to split this activity - it doesn't have any production string.
                deviants.add(deviant);
            }
        }

        return deviants;
    }

    private boolean isLegalProdType(String prodType) {
        return prodType != null && LEGAL_PROD_TYPES.contains(prodType);
    }

    private void findPlaningType(PotentialDeviantActivity potential, DeviantActivity deviant) {
        if (potential != null && deviant != null) {
            String prodType = deviant.getProdType();

            if (prodType != null) {
                Double planing = potential.getR12Planing();
                Double heavyPlaning = potential.getR12HeavyPlaning();
                Double sidePlough = potential.getR12SidePlough();

                Double amount = null;

                Double sign = 1.0;
                if (deviant.isNegativeDeviation()) {
                    sign = -1.0;
                }

                switch (prodType) {
                    case PLANING:
                        amount = planing != null ? planing * sign : null;
                        break;
                    case HEAVY_PLANING:
                        amount = heavyPlaning != null ? heavyPlaning * sign : null;
                        break;
                    case SIDE_PLOUGH:
                        amount = sidePlough != null ? sidePlough * sign : null;
                        break;
                    default:
                        break;
                }

                if (amount != null) {
                    deviant.setAmount(amount);
                    deviant.setUnit("km");
                }
            }
        }
    }

    private void findAmountForGrit(GritProductAmount gritProductAmount, DeviantActivity activity) {
        if (gritProductAmount != null && activity != null) {
            ManagerConstants.Stroprod gritProduct = ManagerConstants.Stroprod.idTilEnum(gritProductAmount.getGritProductId());

            Double dryGrit = gritProductAmount.getDryGrit();
            Double wetGrit = gritProductAmount.getWetGrit();

            Double amount = null;
            String unit = null;

            Double sign = 1.0;
            if (activity.isNegativeDeviation()) {
                sign = -1.0;
            }

            switch (gritProduct) {
                case TORRSALT:
                case SLURRY:
                case BEFUKTET_SALT:
                case SAND_MED_SALT:
                case REN_SAND:
                case FAST_SAND:
                    amount = dryGrit != null ? (dryGrit / 1000) * sign : null;
                    unit = "tonn";
                    break;
                case SALTLOSNING:
                    amount = wetGrit != null ? (wetGrit / 1000) * sign : null;
                    unit = "m3";
                    break;
                default:
                    break;
            }

            if (amount != null) {
                activity.setAmount(amount);
                activity.setUnit(unit);
            }
        }
    }

    private String findDeviationCause(PotentialDeviantActivity potential) {
        String cause = "";

        if (potential != null) {
            if (potential.hasNegativeDeviation()) {
                cause = OVERRULED_MESSAGE;
            } else if (potential.hasPositiveDeviation()) {
                StringBuilder sb = new StringBuilder();

                appendCause(sb, potential.isProductionOutsideContract(), PROD_OUTSIDE_CONTRACT_MESSAGE);
                appendCause(sb, potential.isProductionOutsideRode(), PROD_OUTSIDE_RODE_MESSAGE);
                appendCause(sb, potential.isManuallyCreated(), MANUALLY_CREATED_MESSAGE);

                cause = sb.toString();
            }
        }

        return cause;
    }

    private void appendCause(StringBuilder sb, Boolean validCause, String cause) {
        if (sb != null && validCause != null && cause != null && cause.length() >= 1) {
            String firstLetter = cause.substring(0, 1);
            String additionalCauseLowerCaseFirstLetter = cause.replaceFirst(firstLetter, firstLetter.toLowerCase());

            if (validCause) {
                if (sb.length() > 0) {
                    sb.append(", ");
                    sb.append(additionalCauseLowerCaseFirstLetter);
                } else {
                    sb.append(cause);
                }
            }
        }
    }

    private HashMap<Long, GritProductAmount> getGritProductAmountsForActivities(List<DeviantActivity> activities, EntityManager em) {
        HashMap<Long, GritProductAmount> amountForActivityIds = new HashMap<>();

        if (activities != null && em != null) {
            if (!activities.isEmpty()) {
                // We have to split up the data sent to the SQL-query to avoid hitting the 1000 argument limit of IN.
                int start;
                int stop = 0;

                int roundCounter = 1;
                int rounds = (int) Math.ceil(activities.size() / 1000d);

                while (roundCounter <= rounds) {
                    start = stop;

                    if (roundCounter == rounds) {
                        stop = activities.size();
                    } else {
                        stop += 1000;
                    }

                    List<DeviantActivity> tempList = activities.subList(start, stop);
                    String activityIds = "";

                    String separator = "";
                    for (DeviantActivity activity : tempList) {
                        activityIds += separator + activity.getActivityId();
                        separator = ",";
                    }

                    List<GritProductAmount> amounts = new GritProductAmountsForActivitiesQuery(activityIds, em).getGritProductAmounts();

                    for (GritProductAmount amount : amounts) {
                        amountForActivityIds.put(amount.getActivityId(), amount);
                    }

                    roundCounter++;
                }
            }
        }

        return amountForActivityIds;
    }

    public List<DeviantActivity> getDeviantActivities() {
        return deviantActivities;
    }

}