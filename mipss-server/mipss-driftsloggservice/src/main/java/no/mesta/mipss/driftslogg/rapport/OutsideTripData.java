package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OutsideTripData implements Serializable {

	private static final DateFormat df = new SimpleDateFormat("dd.MM.yy HH:mm");

	private final Integer kjoretoyId;
	private final Integer rodeId;
	private final String vinterdriftklasse;
	private final Integer fylke;
	private final String vegtype;
	private final Date fromDateTime;
	private final Date toDateTime;

	private String kjoretoynavn;
	private String rodenavn;
	private String leverandor;
	private boolean onPloughingRode;
	private boolean outsideContract;

	private boolean logged;
	private Double km;
	private Double broytingKm;
	private Double sideplogKm;
	private Double tungHovlingKm;
	private Double tungHovlingTimer;
	private Double stroingTimer;
	private Double torrsaltTonn;
	private Double slurryTonn;
	private Double saltlosningKubikk;
	private Double befuktetSaltTonn;
	private Double sandSaltTonn;
	private Double renSandTonn;
	private Double fastsandTonn;


	public OutsideTripData(Integer kjoretoyId, Integer rodeId, String vinterdriftklasse, Integer fylke, String vegtype, Date fromDateTime, Date toDateTime) {
		this.kjoretoyId = kjoretoyId;
		this.rodeId = rodeId;
		this.vinterdriftklasse = vinterdriftklasse;
		this.fylke = fylke;
		this.vegtype = vegtype;
		this.fromDateTime = fromDateTime;
		this.toDateTime = toDateTime;
	}

	public Integer getKjoretoyId() {
		return kjoretoyId;
	}

	public String getKjoretoynavn() {
		return kjoretoynavn;
	}

	public void setKjoretoynavn(String kjoretoynavn) {
		this.kjoretoynavn = kjoretoynavn;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public Integer getRodeId() {
		return rodeId;
	}

	public String getRodenavn() {
		return rodenavn;
	}

	public void setRodenavn(String rodenavn) {
		this.rodenavn = rodenavn;
	}

	public String getVinterdriftklasse() {
		return vinterdriftklasse;
	}

	public Integer getFylke() {
		return fylke;
	}

	public String getVegtype() {
		return vegtype;
	}

	public Double getKm() {
		return km;
	}

	public void addKm(Double km) {
		this.km = addDouble(this.km, km);
	}

	public String getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}

	public Double getBroytingKm() {
		return broytingKm;
	}

	public void addBroytingKm(Double broytingKm) {
		this.broytingKm = addDouble(this.broytingKm, broytingKm);
	}



	public Double getSideplogKm() {
		return sideplogKm;
	}

	public void addSideplogKm(Double sideplogKm) {
		this.sideplogKm = addDouble(this.sideplogKm, sideplogKm);
	}

	public Double getTungHovlingKm() {
		return tungHovlingKm;
	}

	public void addTungHovlingKm(Double tungHovlingKm) {
		this.tungHovlingKm = addDouble(this.tungHovlingKm, tungHovlingKm);
	}

	public Double getTungHovlingTimer() {
		return tungHovlingTimer;
	}

	public Double getStroingTimer() {
		return stroingTimer;
	}

	public Double getTorrsaltTonn() {
		return torrsaltTonn;
	}

	public void addTorrsaltTonn(Double torrsaltTonn) {
		this.torrsaltTonn = addDouble(this.torrsaltTonn, torrsaltTonn);
	}

	public Double getSlurryTonn() {
		return slurryTonn;
	}

	public void addSlurryTonn(Double slurryTonn) {
		this.slurryTonn = addDouble(this.slurryTonn, slurryTonn);
	}

	public Double getSaltlosningKubikk() {
		return saltlosningKubikk;
	}

	public void addSaltlosningKubikk(Double saltlosningKubikk) {
		this.saltlosningKubikk = addDouble(this.saltlosningKubikk, saltlosningKubikk);
	}

	public Double getBefuktetSaltTonn() {
		return befuktetSaltTonn;
	}

	public void addBefuktetSaltTonn(Double befuktetSaltTonn) {
		this.befuktetSaltTonn = addDouble(this.befuktetSaltTonn, befuktetSaltTonn);
	}

	public Double getSandSaltTonn() {
		return sandSaltTonn;
	}

	public void addSandSaltTonn(Double sandSaltTonn) {
		this.sandSaltTonn = addDouble(this.sandSaltTonn, sandSaltTonn);
	}

	public Double getRenSandTonn() {
		return renSandTonn;
	}

	public void addRenSandTonn(Double renSandTonn) {
		this.renSandTonn = addDouble(this.renSandTonn, renSandTonn);
	}

	public Double getFastsandTonn() {
		return fastsandTonn;
	}

	public void addFastsandTonn(Double fastsandTonn) {
		this.fastsandTonn = addDouble(this.fastsandTonn, fastsandTonn);
	}

	public void addTungHovlingTimer(Double tungHovlingTimer) {
		this.tungHovlingTimer = addDouble(this.tungHovlingTimer, tungHovlingTimer);
	}

	public void addStroingTimer(Double stroingTimer) {
		this.stroingTimer = addDouble(this.stroingTimer, stroingTimer);
	}

	public Date getFromDate() {
		return fromDateTime;
	}

	public Date getToDate() {
		return toDateTime;
	}

	public String getOnPloughingRode() {
		return onPloughingRode?"X":"";
	}

	public void setOnPloughingRode(boolean onPloughingRode) {
		this.onPloughingRode = onPloughingRode;
	}

	public String getOutsideContract() {
		return outsideContract?"X":"";
	}

	public void setOutsideContract(boolean outsideContract) {
		this.outsideContract = outsideContract;
	}

	public String getFromDateString() {
		return df.format(fromDateTime);
	}

	public String getToDateString() {
		return df.format(toDateTime);
	}

	private Double addDouble(Double current, Double next) {
		if(next==null || next == 0) {
			return current;
		} else if(current==null) {
			return next;
		} else {
			return current+next;
		}
	}
}