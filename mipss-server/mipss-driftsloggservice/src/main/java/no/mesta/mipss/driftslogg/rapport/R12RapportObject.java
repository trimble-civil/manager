package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

@SuppressWarnings("serial")
public class R12RapportObject implements Serializable{

	private String leverandor;
	private String r12rode;
	
	private Double broytingKm;
	private Double intentionKm;
	private Double plowingIntentionKm;
	private Double stroingTimer;
	private Double torrsaltTonn;
	private Double slurryTonn;
	private Double saltlosningM3;
	private Double befuktetSaltTonn;
	private Double sandMedSaltTonn;
	private Double rendSandTonn;
	private Double fastsandTonn;
	private Double tungHovlingTimer;
	private Double tungHovlingKm;
	private Double sidePlogKm;


	private Double broytingKmMipss;
	private Double stroingTimerMipss;
	private Double torrsaltTonnMipss;
	private Double slurryTonnMipss;
	private Double saltlosningM3Mipss;
	private Double befuktetSaltTonnMipss;
	private Double sandMedSaltTonnMipss;
	private Double rendSandTonnMipss;
	private Double fastsandTonnMipss;
	private Double tungHovlingTimerMipss;
	private Double tungHovlingKmMipss;
	private Double sidePlogKmMipss;

	private List<String> strobroyteroder = new ArrayList<String>();
	private List<Long> strobroyterodeId = new ArrayList<Long>();
	private List<Fordelingsrode> fordelingsroder = new ArrayList<Fordelingsrode>();

	public boolean isEmpty(){
		 return broytingKm==null && stroingTimer==null && torrsaltTonn==null && slurryTonn==null && saltlosningM3==null &&
		 befuktetSaltTonn==null && sandMedSaltTonn==null && rendSandTonn==null && fastsandTonn==null && tungHovlingTimer==null && sidePlogKm == null &&
		 tungHovlingKm == null && broytingKmMipss==null && stroingTimerMipss==null && torrsaltTonnMipss==null && slurryTonnMipss==null && 
		 saltlosningM3Mipss==null && befuktetSaltTonnMipss==null && sandMedSaltTonnMipss==null && rendSandTonnMipss==null && 
		 fastsandTonnMipss==null && tungHovlingTimerMipss == null && tungHovlingKmMipss == null && sidePlogKmMipss == null &&
		 intentionKm == null && plowingIntentionKm == null;
	}
	
	public R12RapportObject cloneForSum(){
		R12RapportObject r = new R12RapportObject();
		r.setR12rode(r12rode);
		return r;
	}
	public String getLeverandor() {
		return leverandor;
	}
	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}
	public void setR12rode(String r12rode) { 
		this.r12rode = r12rode; 
	}
	public String getR12rode() { return r12rode; }
	
	public Double getBroytingKm() { return broytingKm; }
	public Double getIntentionKm() { return intentionKm; }
	public Double getPlowingIntentionKm() { return addDouble(getBroytingKm(), getIntentionKm()); }
	public Double getStroingTimer() { return stroingTimer; }
	public Double getTorrsaltTonn() { return torrsaltTonn; }
	public Double getSlurryTonn() { return slurryTonn; }
	public Double getSaltlosningM3() { return saltlosningM3; }
	public Double getBefuktetSaltTonn() { return befuktetSaltTonn; }
	public Double getSandMedSaltTonn() { return sandMedSaltTonn; }
	public Double getRendSandTonn() { return rendSandTonn; }
	public Double getFastsandTonn() { return fastsandTonn; }
	public Double getTungHovlingTimer() {return tungHovlingTimer; }
	public Double getTungHovlingKm(){return tungHovlingKm; }
	
	public Double getBroytingKmMipss() { return broytingKmMipss; }
	public Double getStroingTimerMipss() { return stroingTimerMipss; }
	public Double getTorrsaltTonnMipss() { return torrsaltTonnMipss; }
	public Double getSlurryTonnMipss() { return slurryTonnMipss; }
	public Double getSaltlosningM3Mipss() { return saltlosningM3Mipss; }
	public Double getBefuktetSaltTonnMipss() { return befuktetSaltTonnMipss; }
	public Double getSandMedSaltTonnMipss() { return sandMedSaltTonnMipss; }
	public Double getRendSandTonnMipss() { return rendSandTonnMipss; }
	public Double getFastsandTonnMipss() { return fastsandTonnMipss; }
	public Double getTungHovlingTimerMipss() { return tungHovlingTimerMipss; }
	public Double getTungHovlingKmMipss() { return tungHovlingKmMipss; }
	public Double getSidePlogKmMipss() {
		return sidePlogKmMipss;
	}

	public void addBroytingKm(Double broytingKm){ 
		this.broytingKm = addDouble(this.broytingKm, broytingKm);
	}
	public void addIntentionKm(Double intentionKm){
		this.intentionKm = addDouble(this.intentionKm, intentionKm);
	}
	public void addPlowingIntentionKm(Double plowingIntentionKm){
		this.plowingIntentionKm = addDouble(this.plowingIntentionKm, plowingIntentionKm);
	}
	public void addTungHovlingTimer(Double tungHovlingTimer) {
		this.tungHovlingTimer = addDouble(this.tungHovlingTimer, tungHovlingTimer);
	}
	public void addSidePlogKm(double broytingKm) {
		this.sidePlogKm = addDouble(this.sidePlogKm, broytingKm);
	}
	public void addTungHovlingKm(Double tungHovlingKm) {
		this.tungHovlingKm = addDouble(this.tungHovlingKm, tungHovlingKm);
	}
	public void addStroingTimer(Double stroingTimer){
		this.stroingTimer = addDouble(this.stroingTimer, stroingTimer);
	}
	public void addTonnBefuktetSalt(Double tonnBefuktetSalt) {
		this.befuktetSaltTonn = addDoubleConvert(this.befuktetSaltTonn, tonnBefuktetSalt);
	}
	public void addTonnFastsand(Double tonnFastsand) {
		this.fastsandTonn = addDoubleConvert(this.fastsandTonn, tonnFastsand);
	}
	public void addTonnRenSand(Double tonnRenSand) {
		this.rendSandTonn = addDoubleConvert(this.rendSandTonn, tonnRenSand);
	}
	public void addTonnSaltSand(Double tonnSaltSand) {
		this.sandMedSaltTonn = addDoubleConvert(this.sandMedSaltTonn, tonnSaltSand);
	}
	public void addTonnSlurry(Double slurryTonn){
		this.slurryTonn = addDoubleConvert(this.slurryTonn, slurryTonn);
	}
	public void addTonnTorrsalt(Double tonnTorrsalt) {
		this.torrsaltTonn = addDoubleConvert(this.torrsaltTonn, tonnTorrsalt);
	}
	public void addSaltlosningM3(Double saltlosningM3) {
		this.saltlosningM3 = addDoubleConvert(this.saltlosningM3, saltlosningM3);
	}
	
	public void addBroytingKmMipss(Double broytingKmMipss){ 
		this.broytingKmMipss = addDouble(this.broytingKmMipss, broytingKmMipss);
	}
	public void addTungHovlingTimerMipss(Double tungHovlingTimerMipss){
		this.tungHovlingTimerMipss = addDouble(this.tungHovlingTimerMipss, tungHovlingTimerMipss);
	}
	
	public void addTungHovlingKmMipss(Double tungHovlingKmMipss){
		this.tungHovlingKmMipss = addDouble(this.tungHovlingKmMipss, tungHovlingKmMipss);
	}

	public void addSidePlogKmMipss(Double sidePlogKmMipss){
		this.sidePlogKmMipss = addDouble(this.sidePlogKmMipss, sidePlogKmMipss);
	}
	
	public void addStroingTimerMipss(Double stroingTimerMipss){
		this.stroingTimerMipss = addDouble(this.stroingTimerMipss, stroingTimerMipss);
	}
	public void addTonnBefuktetSaltMipss(Double tonnBefuktetSaltMipss) {
		this.befuktetSaltTonnMipss = addDouble(this.befuktetSaltTonnMipss, tonnBefuktetSaltMipss);
	}
	public void addTonnFastsandMipss(Double tonnFastsandMipss) {
		this.fastsandTonnMipss = addDouble(this.fastsandTonnMipss, tonnFastsandMipss);
	}
	public void addTonnRenSandMipss(Double tonnRenSandMipss) {
		this.rendSandTonnMipss = addDouble(this.rendSandTonnMipss, tonnRenSandMipss);
	}
	public void addTonnSaltSandMipss(Double tonnSaltSandMipss) {
		this.sandMedSaltTonnMipss = addDouble(this.sandMedSaltTonnMipss, tonnSaltSandMipss);
	}
	public void addTonnSlurryMipss(Double slurryTonnMipss){
		this.slurryTonnMipss = addDouble(this.slurryTonnMipss, slurryTonnMipss);
	}
	public void addTonnTorrsaltMipss(Double tonnTorrsaltMipss) {
		this.torrsaltTonnMipss = addDouble(this.torrsaltTonnMipss, tonnTorrsaltMipss);
	}
	public void addSaltlosningM3Mipss(Double saltlosningM3Mipss) {
		this.saltlosningM3Mipss = addDouble(this.saltlosningM3Mipss, saltlosningM3Mipss);
	}

	private Double addDoubleConvert(Double d1, Double d2){
		if (d1==null&&d2!=null){
			return d2/1000d;
		}
		if (d2!=null){
			return d1+(d2/1000d);
		}
		return d1;
	}
	private Double addDouble(Double d1, Double d2){
		if (d1==null){
			return d2;
		}
		if (d2!=null){
			return d1+d2;
		}
		return d1;
	}

	public void addRodenavn(String rodenavn){
		if (!strobroyteroder.contains(rodenavn)){
			strobroyteroder.add(rodenavn);
		}
	}
	
	public void addRode(Long rodeId){
		if (!strobroyterodeId.contains(rodeId)){
			strobroyterodeId.add(rodeId);
		}
	}
	public List<Long> getStrobroyteRodeIds(){
		return strobroyterodeId;
	}
	public String getStrobroytRoder(){
		List<String> roder = new ArrayList<String>();
		DecimalFormat f = new DecimalFormat("#.###");
		for (String rode:strobroyteroder){
			StringBuilder sb = new StringBuilder();
			sb.append(rode);
			for (Fordelingsrode fr:fordelingsroder){
				if (fr.getStroBroyteRode().getNavn().equals(rode)){
					List<Rodefordeling> fordelingsnokler = fr.getFordelingsnokler();
					for (Rodefordeling rf:fordelingsnokler){
						if (rf.getR12rode().getNavn().equals(r12rode)){
							double fordelingsnokkel = rf.getFordelingsnokkel();
							sb.append("(");
							sb.append(f.format(fordelingsnokkel*100)+"%");//PERCENT
							sb.append(")");
						}
					}
				}
			}
			roder.add(sb.toString());
		}
		return StringUtils.join(roder, ',');
	}
	
	public String getStrobroytRoderId(){
		return StringUtils.join(strobroyterodeId, ',');
	}
	public void addFordeling(Fordelingsrode fordeling) {
		if (!fordelingsroder.contains(fordeling)){
			fordelingsroder.add(fordeling);
		}
	}

}
