package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class ApprovedActivitiesQuery {

    private Query q;

    public ApprovedActivitiesQuery(DriftsloggQueryParams params, Date fraDato, Date tilDato, EntityManager em) {
        q = em.createNativeQuery(buildGetActivitiesQuery())
                .setParameter(1, params.getKontrakt().getId())
                .setParameter(2, params.getKontrakt().getId())
                .setParameter(3, fraDato)
                .setParameter(4, tilDato);
    }

    private String buildGetActivitiesQuery() {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT");
        sb.append(" a.id AS activityId,");
        sb.append(" l.navn AS vendorName,");
        sb.append(" a.kjoretoy_id AS vehicleId,");
        sb.append(" kt.eksternt_navn AS vehicleName,");
        sb.append(" a.rode_id,");
        sb.append(" r.navn AS rodeName,");
        sb.append(" fra_dato_tid AS fromDateTime,");
        sb.append(" til_dato_tid AS toDateTime,");
        sb.append(" manually_created AS manuallyCreated,");
        sb.append(" r12_broyting AS r12Planing,");
        sb.append(" r12_sideplog AS r12SidePlough,");
        sb.append(" r12_hovling AS r12HeavyPlaning,");
        sb.append(" approved_build,");
        sb.append(" fakturer_intensjon_flagg AS billIntention,");
        sb.append(" r12_intensjon_km AS r12Intention,");
        sb.append(" production_type AS prodType, ");
        sb.append(" kr.rodetilknytningstype_id AS rodeConnectionTypeId ");

        sb.append("FROM aktivitet a ");

        sb.append("JOIN trip t ON a.trip_id = t.id ");
        sb.append("JOIN tripprocessstatus tps on tps.id=t.tripprocessstatus ");
        sb.append("JOIN periode p ON t.periode_id = p.id ");
        sb.append("JOIN levkontrakt lk ON p.levkontrakt_ident = lk.levkontrakt_ident ");
        sb.append("JOIN leverandor l ON lk.leverandor_nr = l.nr ");
        sb.append("LEFT OUTER JOIN kjoretoy kt ON a.kjoretoy_id = kt.id ");
        sb.append("LEFT OUTER JOIN rode r ON a.rode_id = r.id ");
        sb.append("LEFT OUTER JOIN kjoretoy_rode kr ON kr.kjoretoy_id = a.kjoretoy_id AND kr.rode_id = r.id AND kr.kontrakt_id = ?1 AND fra_dato_tid BETWEEN kr.fra_tidspunkt AND kr.til_tidspunkt ");

        sb.append("WHERE");
        sb.append(" tps.id in ('6','7','8a') ");
        sb.append(" AND lk.driftkontrakt_id = ?2");
        sb.append(" AND fra_dato_tid BETWEEN ?3 AND ?4");
        sb.append(" AND approved_build = 1"); // approved build ("godkjent byggherre")

        return sb.toString();
    }

    public List<ApprovedActivity> getApprovedActivities() {
        NativeQueryWrapper<ApprovedActivity> qry = new NativeQueryWrapper<>(
                q,
                ApprovedActivity.class,
                ApprovedActivity.getPropertiesArray());

        return qry.getWrappedList();
    }

}