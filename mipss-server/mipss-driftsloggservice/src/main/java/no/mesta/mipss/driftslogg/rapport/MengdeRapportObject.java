package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;

import no.mesta.mipss.driftslogg.util.TorrstoffUtil;

@SuppressWarnings("serial")
public class MengdeRapportObject implements Serializable{
	private String leverandor;
	private String rode;
	private Double tonnSaltSand;
	private Double tonnRenSand;
	private Double tonnFastsand;
	private Double tonnTorrsalt;
	private Double tonnSlurry;
	private Double m3Saltlosning;
	private Double tonnBefuktetSalt;
	private Double kmBroyt;
	private Double timerBroyt;
	private Double kmStro;
	private Double timerStro;
	private Double timerSamprod;
	private Double kmSamprod;
	private Double kmTungHovel;
	private Double timerTungHovel;
	
	private Double tonnSaltSandMipss;
	private Double tonnRenSandMipss;
	private Double tonnFastsandMipss;
	private Double tonnTorrsaltMipss;
	private Double tonnSlurryMipss;
	private Double m3SaltlosningMipss;
	private Double tonnBefuktetSaltMipss;
	private Double kmBroytMipss;
	private Double timerBroytMipss;
	private Double kmStroMipss;
	private Double timerStroMipss;
	private Double timerSamprodMipss;
	private Double kmSamprodMipss;
	private Double timerTungHovelMipss;
	private Double kmTungHovelMipss;
	
	public boolean isEmpty(){
		 return tonnSaltSand==null && tonnRenSand==null && tonnFastsand==null && tonnTorrsalt==null &&
		 tonnSlurry==null && m3Saltlosning==null && tonnBefuktetSalt==null && kmBroyt==null &&
		 timerBroyt==null && timerStro==null && timerSamprod==null && kmSamprod==null &&
		 getKmStro() == null&& timerTungHovel == null && kmTungHovel == null &&
		
		 tonnSaltSandMipss==null && tonnRenSandMipss==null && tonnFastsandMipss==null && tonnTorrsaltMipss==null &&
		 tonnSlurryMipss==null && m3SaltlosningMipss==null && tonnBefuktetSaltMipss==null && kmBroytMipss==null &&
		 timerBroytMipss==null && timerStroMipss==null && timerSamprodMipss==null && kmSamprodMipss==null&&
		 getKmStroMipss() == null && timerTungHovelMipss == null && kmTungHovelMipss == null;
	}
	private Double addDouble(Double d1, Double d2){
		if (d1==null){
			return d2;
		}
		if (d2!=null){
			return d1+d2;
		}
		return d1;
	}
	private Double addDoubleConvert(Double d1, Double d2){
		if (d1==null){
			return d2/1000d;
		}
		if (d2!=null){
			return d1+(d2/1000d);
		}
		return d1;
	}
	/**
	 * @param kmBroyt the kmBroyt to set
	 */
	public void addKmBroyt(Double kmBroyt) {
		this.kmBroyt = addDouble(this.kmBroyt, kmBroyt);
//		if (this.kmBroyt==null){
//			this.kmBroyt= kmBroyt;
//		}
//		else if (this.kmBroyt!=null&&kmBroyt!=null){
//			this.kmBroyt=this.kmBroyt+kmBroyt;
//		}
	}
	/**
	 * @param kmBroytMipss the kmBroytMipss to set
	 */
	public void addKmBroytMipss(Double kmBroytMipss) {
		this.kmBroytMipss = addDouble(this.kmBroytMipss, kmBroytMipss);
	}
	/**
	 * @param kmSamprod the kmSamprod to set
	 */
	public void addKmSamprod(Double kmSamprod) {
		this.kmSamprod = addDouble(this.kmSamprod, kmSamprod);
	}
	/**
	 * @param kmSamprodMipss the kmSamprodMipss to set
	 */
	public void addKmSamprodMipss(Double kmSamprodMipss) {
		this.kmSamprodMipss = addDouble(this.kmSamprodMipss, kmSamprodMipss);
	}
	/**
	 * @param m3Saltlosning the m3Saltlosning to set
	 */
	public void addM3Saltlosning(Double m3SaltlosningVaatt, String enhet) {
		m3SaltlosningVaatt= TorrstoffUtil.m3ToLiter(enhet, m3SaltlosningVaatt);
		this.m3Saltlosning = addDoubleConvert(this.m3Saltlosning, m3SaltlosningVaatt);
	}
	/**
	 * @param m3SaltlosningMipss the m3SaltlosningMipss to set
	 */
	public void addM3SaltlosningMipss(Double m3SaltlosningMipss) {
		this.m3SaltlosningMipss = addDouble(this.m3SaltlosningMipss, m3SaltlosningMipss);
	}
	/**
	 * @param timerBroyt the timerBroyt to set
	 */
	public void addTimerBroyt(Double timerBroyt) {
		this.timerBroyt = addDouble(this.timerBroyt, timerBroyt);
	}
	/**
	 * @param timerBroytMipss the timerBroytMipss to set
	 */
	public void addTimerBroytMipss(Double timerBroytMipss) {
		this.timerBroytMipss = addDouble(this.timerBroytMipss, timerBroytMipss);
	}
	public void addTimerSamprod(Double timerSamprod) {
		this.timerSamprod = addDouble(this.timerSamprod, timerSamprod);
	}
	/**
	 * @param timerSamprodMipss the timerSamprodMipss to set
	 */
	public void addTimerSamprodMipss(Double timerSamprodMipss) {
		this.timerSamprodMipss = addDouble(this.timerSamprodMipss, timerSamprodMipss);
	}
	
	// TUNG VEIH�VEL
	public void addTimerTungHovelMipss(Double timerTungHovelMipss) {
		this.timerTungHovelMipss = addDouble(this.timerTungHovelMipss, timerTungHovelMipss);
	}
	
	public void addKmTungHovelMipss(Double kmTungHovelMipss) {
		this.kmTungHovelMipss = addDouble(this.kmTungHovelMipss, kmTungHovelMipss);
	}
	
	public void addTimerTungHovel(Double timerTungHovel){
		this.timerTungHovel = addDouble(this.timerTungHovel, timerTungHovel);
	}
	
	public void addKmTungHovel (Double kmTungHovel) {
		this.kmTungHovel = addDouble(this.kmTungHovel, kmTungHovel);
	}
	//SLUTT TUNG VEIH�VEL
	
	public void addTimerStro(Double timerStro){
		this.timerStro = addDouble(this.timerStro, timerStro);
	}
	/**
	 * @param timerStroMipss the timerBroytMipss to set
	 */
	public void addTimerStroMipss(Double timerStroMipss) {
		this.timerStroMipss = addDouble(this.timerStroMipss, timerStroMipss);
	}
	
	public void addKmStro(Double kmStro){
		this.kmStro = addDouble(this.kmStro, kmStro);
	}
	public void addKmStroMipss(Double kmStroMipss){
		this.kmStroMipss = addDouble(this.kmStroMipss, kmStroMipss);
	}
	/**
	 * @param tonnBefuktetSalt the tonnBefuktetSalt to set
	 */
	public void addTonnBefuktetSalt(Double tonnSaltTort, Double tonnSaltVatt, String enhet) {
		tonnSaltTort = tonnSaltTort+TorrstoffUtil.tonnTorr(enhet, tonnSaltVatt);
		this.tonnBefuktetSalt = addDoubleConvert(this.tonnBefuktetSalt, tonnSaltTort);

		
//		this.tonnBefuktetSalt = addDoubleConvert(this.tonnBefuktetSalt, tonnBefuktetSalt);
	}
	/**
	 * @param tonnBefuktetSaltMipss the tonnBefuktetSaltMipss to set
	 */
	public void addTonnBefuktetSaltMipss(Double tonnBefuktetSaltMipss) {
		this.tonnBefuktetSaltMipss = addDouble(this.tonnBefuktetSaltMipss, tonnBefuktetSaltMipss);
	}
	/**
	 * @param tonnFastsand the tonnFastsand to set
	 */
	public void addTonnFastsand(Double tonnFastsand) {
		this.tonnFastsand = addDoubleConvert(this.tonnFastsand, tonnFastsand);
	}
	/**
	 * @param tonnFastsandMipss the tonnFastsandMipss to set
	 */
	public void addTonnFastsandMipss(Double tonnFastsandMipss) {
		this.tonnFastsandMipss = addDouble(this.tonnFastsandMipss, tonnFastsandMipss);
	}
	/**
	 * @param tonnRenSand the tonnRenSand to set
	 */
	public void addTonnRenSand(Double tonnRenSand) {
		this.tonnRenSand = addDoubleConvert(this.tonnRenSand, tonnRenSand);
	}
	/**
	 * @param tonnRenSandMipss the tonnRenSandMipss to set
	 */
	public void addTonnRenSandMipss(Double tonnRenSandMipss) {
		this.tonnRenSandMipss = addDouble(this.tonnRenSandMipss, tonnRenSandMipss);
	}
	/**
	 * @param tonnSaltSand the tonnSaltSand to set
	 */
	public void addTonnSaltSand(Double tonnSaltSand) {
		this.tonnSaltSand = addDoubleConvert(this.tonnSaltSand, tonnSaltSand);
	}
	/**
	 * @param tonnSaltSandMipss the tonnSaltSandMipss to set
	 */
	public void addTonnSaltSandMipss(Double tonnSaltSandMipss) {
		this.tonnSaltSandMipss = addDouble(this.tonnSaltSandMipss, tonnSaltSandMipss);
	}
	
	/**
	 * @param tonnSlurry the tonnSlurry to set
	 */
	public void addTonnSlurry(Double tonnSlurryTort, Double tonnSlurryVatt, String enhet) {
		tonnSlurryTort = tonnSlurryTort+TorrstoffUtil.tonnTorr(enhet, tonnSlurryVatt);
		this.tonnSlurry = addDoubleConvert(this.tonnSlurry, tonnSlurryTort);
	}
	
	/**
	 * @param tonnSlurryMipss the tonnSlurryMipss to set
	 */
	public void addTonnSlurryMipss(Double tonnSlurryMipss) {
		this.tonnSlurryMipss = addDouble(this.tonnSlurryMipss, tonnSlurryMipss);
	}
	/**
	 * @param tonnTorrsalt the tonnTorrsalt to set
	 */
	public void addTonnTorrsalt(Double tonnTorrsalt) {
		this.tonnTorrsalt = addDoubleConvert(this.tonnTorrsalt, tonnTorrsalt);
	}

	
	/**
	 * @param tonnTorrsaltMipss the tonnTorrsaltMipss to set
	 */
	public void addTonnTorrsaltMipss(Double tonnTorrsaltMipss) {
		this.tonnTorrsaltMipss = addDouble(this.tonnTorrsaltMipss, tonnTorrsaltMipss);
	}
	/**
	 * @return the kmBroyt
	 */
	public Double getKmBroyt() {
		return kmBroyt;
	}
	/**
	 * @return the kmBroytMipss
	 */
	public Double getKmBroytMipss() {
		return kmBroytMipss;
	}
	/**
	 * @return the kmSamprod
	 */
	public Double getKmSamprod() {
		return kmSamprod;
	}
	/**
	 * @return the kmSamprodMipss
	 */
	public Double getKmSamprodMipss() {
		return kmSamprodMipss;
	}
	/**
	 * @return the leverandor
	 */
	public String getLeverandor() {
		return leverandor;
	}
	/**
	 * @return the m3Saltlosning
	 */
	public Double getM3Saltlosning() {
		return m3Saltlosning;
	}
	/**
	 * @return the m3SaltlosningMipss
	 */
	public Double getM3SaltlosningMipss() {
		return m3SaltlosningMipss;
	}
	/**
	 * @return the timerBroyt
	 */
	public Double getTimerBroyt() {
		return timerBroyt;
	}
	/**
	 * @return the timerBroytMipss
	 */
	public Double getTimerBroytMipss() {
		return timerBroytMipss;
	}
	public Double getTimerSamprod() {
		return timerSamprod;
	}
	/**
	 * @return the timerSamprodMipss
	 */
	public Double getTimerSamprodMipss() {
		return timerSamprodMipss;
	}	
	/**
	 * @return the kmTungHovel
	 */
	public Double getKmTungHovel() {
		return kmTungHovel;
	}
	/**
	 * @return the timerTungHovel
	 */
	public Double getTimerTungHovel() {
		return timerTungHovel;
	}
	/**
	 * @return the timerTungHovelMipss
	 */
	public Double getTimerTungHovelMipss() {
		return timerTungHovelMipss;
	}
	/**
	 * @return the kmTungHovelMipss
	 */
	public Double getKmTungHovelMipss() {
		return kmTungHovelMipss;
	}
	/**
	 * @return the timerBroyt
	 */
	public Double getTimerStro() {
		return timerStro;
	}
	/**
	 * @return the timerStroMipss
	 */
	public Double getTimerStroMipss() {
		return timerStroMipss;
	}
	/**
	 * @return the tonnBefuktetSalt
	 */
	public Double getTonnBefuktetSalt() {
		return tonnBefuktetSalt;
	}
	/**
	 * @return the tonnBefuktetSaltMipss
	 */
	public Double getTonnBefuktetSaltMipss() {
		return tonnBefuktetSaltMipss;
	}
	/**
	 * @return the tonnFastsand
	 */
	public Double getTonnFastsand() {
		return tonnFastsand;
	}
	/**
	 * @return the tonnFastsandMipss
	 */
	public Double getTonnFastsandMipss() {
		return tonnFastsandMipss;
	}
	/**
	 * @return the tonnRenSand
	 */
	public Double getTonnRenSand() {
		return tonnRenSand;
	}
	/**
	 * @return the tonnRenSandMipss
	 */
	public Double getTonnRenSandMipss() {
		return tonnRenSandMipss;
	}
	/**
	 * @return the tonnSaltSand
	 */
	public Double getTonnSaltSand() {
		return tonnSaltSand;
	}
	/**
	 * @return the tonnSaltSandMipss
	 */
	public Double getTonnSaltSandMipss() {
		return tonnSaltSandMipss;
	}
	/**
	 * @return the tonnSlurry
	 */
	public Double getTonnSlurry() {
		return tonnSlurry;
	}
	/**
	 * @return the tonnSlurryMipss
	 */
	public Double getTonnSlurryMipss() {
		return tonnSlurryMipss;
	}
	/**
	 * @return the tonnTorrsalt
	 */
	public Double getTonnTorrsalt() {
		return tonnTorrsalt;
	}
	/**
	 * @return the tonnTorrsaltMipss
	 */
	public Double getTonnTorrsaltMipss() {
		return tonnTorrsaltMipss;
	}
	/**
	 * @param leverandor the leverandor to set
	 */
	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}
	public Double getKmStro() {
		return kmStro;
	}
	public Double getKmStroMipss() {
		return kmStroMipss;
	}
	/**
	 * @return the rode
	 */
	public String getRode() {
		return rode;
	}
	/**
	 * @param rode the rode to set
	 */
	public void setRode(String rode) {
		this.rode = rode;
	}
	
}
