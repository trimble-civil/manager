package no.mesta.mipss.driftslogg.util;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.MipssInput;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.Periode;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AgressoHelper {

	private final EntityManager em;
	public AgressoHelper(EntityManager em){
		this.em = em;
		
	}
	
	/**
	 * Lager et map av leverand�renes perioder der key er leverand�rnummer og value er en liste av leverand�rens perioder perioder
	 * 
	 * @param periodeId alle periodene som skal v�re med i mappet
	 * @return
	 */
	public Map<String, List<Periode>> createMapOfLeverandorPeriode(List<Long> periodeId){
		Map<String, List<Periode>> leverandorPerioder = new HashMap<String, List<Periode>>();
		//sorter alle perioder pr leverand�r
		for (Long pid:periodeId){
			Periode periode = em.find(Periode.class, pid);
			String leverandorNr = periode.getLevkontrakt().getLeverandorNr();
			List<Periode> lp = leverandorPerioder.get(leverandorNr);
			if (lp==null){
				lp = new ArrayList<Periode>();
				leverandorPerioder.put(leverandorNr, lp);
			}
			lp.add(periode);
		}
		return leverandorPerioder;
	}
	
	
	/**
	 * Lager datasettet som skal legges inn i agresso
	 * 
	 * @param perioder periodene som skal overf�res (kan g� over m�nedsskiftet men kun aktiviteter innenfor m�neden vil bli med i datasettet)
	 * @param mnd m�neden som skal overf�res (0=Januar)
	 * @param aar �ret
	 * @param brukerIdent brukeren som legges inn som "oppretter"
	 * @param tripPredicate
	 * @return
	 */
	public List<MipssInput> createMipssInput(List<Periode> perioder, Long mnd, Long aar, String brukerIdent, Predicate<Aktivitet> tripPredicate){
		Map<String, AgressoTransferHolder> orderIds = getOrderIds();

		List<MipssInput> tilAgresso = new ArrayList<MipssInput>();
		for (Periode periode:perioder){//legg alle aktivitetene til leverand�ren i samme liste
			AgressoTransferHolder ordreId = orderIds.get(periode.getLevkontraktIdent());
			
			if (ordreId == null){
				ordreId = createOrderId(mnd, aar, periode);
				orderIds.put(periode.getLevkontraktIdent(), ordreId);
			}
			
			List<Aktivitet> aktiviteter = fjernAktiviteterFraForrigeMnd(periode, mnd, aar);
			aktiviteter = aktiviteter.stream().filter(a->tripPredicate.test(a)).collect(Collectors.toList());
			
			List<MipssInput> mipssInput = createMipssInputFromAktiviteter(aktiviteter, ordreId, brukerIdent);
			tilAgresso.addAll(mipssInput);
		}
		return tilAgresso;
	}

	private AgressoTransferHolder createOrderId(Long mnd, Long aar, Periode periode) {
		String leverandorNr = periode.getLevkontrakt().getLeverandorNr();
		Long driftkontraktId = periode.getLevkontrakt().getDriftkontraktId();

		AgressoTransferHolder ordreId;
		ordreId = new AgressoTransferHolder();

		String levkontraktIdent = periode.getLevkontraktIdent();

		//JALLA warning! m� v�re unik pr driftkontrakt leverand�r m�ned og �r + levkontrakt
		//kan ikke v�re lengre enn 20 tegn
		String mndStr = mnd<10?"0"+mnd:mnd+"";//padding av mnd
		String str =driftkontraktId+leverandorNr+mndStr+aar+levkontraktIdent;
		str = str.hashCode()+"";
		if (str.charAt(0)=='-')
            str = str.substring(1);
		ordreId.setOrderId(str);
		return ordreId;
	}

	private Map<String, AgressoTransferHolder> getOrderIds() {
		//Hente ut alle orderId's og max(line_no) som er i tabellen mipss_input@agrint.
		Query query = em.createNativeQuery("select order_id, contract_id, max(line_no) from mipss_input@agrint group by order_id, contract_id");
		List<AgressoTransferHolder> wrappedList = new NativeQueryWrapper<AgressoTransferHolder>
													(query, AgressoTransferHolder.class, new Class[]{
													String.class, String.class, Long.class},
													"orderId", "levKontraktIdent", "lineNo").getWrappedList();

		Map<String, AgressoTransferHolder> orderIds = new HashMap<String, AgressoTransferHolder>();

		for (AgressoTransferHolder ah : wrappedList) {
			orderIds.put(ah.getLevKontraktIdent(), ah);
		}
		return orderIds;
	}

	public static void main(String[] args) {
		String tsk = "1204-H�RADAL";
		String levkontraktInt = "";
		String tsk2 =""+levkontraktInt.hashCode();
		String bob ="7138696012011"+tsk;
		System.out.println(bob.hashCode());
		System.out.println(bob);

	}
	
	public AgressoTransferHolder createTH(){
		return new AgressoTransferHolder();
	}

	public List<MipssInput> createMipssInputFromAktiviteter(List<Aktivitet> aktiviteter, AgressoTransferHolder ordreId, String brukerIdent){
		List<MipssInput> tilAgresso = new ArrayList<MipssInput>();
		for (Aktivitet a:aktiviteter){
			if (a.getSendtAgresso()==null||a.getSendtAgresso().booleanValue()==false){
				tilAgresso.add(new MipssInput(a, ordreId.incLineNo(), new BigInteger(ordreId.getOrderId()), brukerIdent));
			}
		}
		return tilAgresso;
	}
	
	/**
	 * Fjerner aktiviteter fra perioden som er p� forrige m�ned dersom perioden har status=Sendt agresso.
	 * 
	 * @param periode
	 * @param mnd 
	 * @param aar
	 * @return
	 */
	public List<Aktivitet> fjernAktiviteterFraForrigeMnd(Periode periode, Long mnd, Long aar){
		List<Aktivitet> activities = new ArrayList<>();
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);

		for(Trip trip : periode.getTrips()) {
			if(trip.getProcessStatus()== TripProcessStatus.SENT_TO_IO || trip.getProcessStatus()==TripProcessStatus.PAID) {
				activities.addAll(beholdAktiviteterSomIkkeErSendtAgresso(trip.getActivities(), mnd, aar));
			} else if(periode.getTilDato().after(last)) {
				activities.addAll(beholdAktiviteterForDenneMnd(trip.getActivities(), mnd, aar));
			} else {
				activities.addAll(trip.getActivities());
			}
		}

		return activities;
	}
	
	public List<Aktivitet> beholdAktiviteterSomIkkeErSendtAgresso(List<Aktivitet> aktiviteter, Long mnd, Long aar){
		List<Aktivitet> ikkeSendteAktiviteter = new ArrayList<Aktivitet>();
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);

		for (Aktivitet a:aktiviteter){
			if (a.getSendtAgresso()!=null&&a.getSendtAgresso()){//er allerede overf�rt til agresso
				continue;
			}
			if (a.getFraDatoTid().after(last)){//ikke ta med aktiviteter som har fra_dato etter siste tidspunkt i m�neden.
				continue;
			}
			if (!a.getSendtAgresso()){
				ikkeSendteAktiviteter.add(a);
			}
		}
		return ikkeSendteAktiviteter;
	}
	/**
	 * Fjerner de aktivitetene som ikke er for innev�rende m�ned.
	 * @param aktiviteter
	 * @param mnd
	 * @param aar
	 * @return
	 */
	public List<Aktivitet> beholdAktiviteterForDenneMnd(List<Aktivitet> aktiviteter, Long mnd, Long aar){
		Date first = CalendarHelper.getFirstDateInMonth(mnd, aar);
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);
		List<Aktivitet> thisMonth = new ArrayList<Aktivitet>();
		for (Aktivitet a:aktiviteter){
			if (a.getSendtAgresso()!=null&&a.getSendtAgresso()){
				continue;
			}
			if (a.getFraDatoTid().after(first) && a.getFraDatoTid().before(last)){ //fraDato er innenfor m�neden
				thisMonth.add(a);
			}
			if (a.getFraDatoTid().equals(first)){
				thisMonth.add(a);
			}
			if (a.getFraDatoTid().equals(last)){
				thisMonth.add(a);
			}
		}
		return thisMonth;
	}
	
	private int nextRandomInt(int min, int max) {
		Random random = new Random();
		int randomNum = random.nextInt(max - min + 1) + min;
		return randomNum;
	}
	
	
}
