package no.mesta.mipss.driftslogg;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.VetoableChangeSource;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

public class DriftsloggQueryParams implements Serializable, PropertyChangeSource, VetoableChangeSource {

	private Driftkontrakt kontrakt;
	private Leverandor leverandor;
	private Levkontrakt levkontrakt;
	private Kjoretoy kjoretoy;
	private Date month;

	private boolean registered;
	private boolean submitted;
	private boolean processed;
	private boolean paid;

	private boolean visAllePerioderVedGodkjenning;
	private PropertyChangeSupport props;
	private VetoableChangeSupport vetos;

	public enum MENGDERAPPORT_TYPE{
		UKE,
		MND,
		EGENDEF
	}

	public DriftsloggQueryParams clone(){
		DriftsloggQueryParams p = new DriftsloggQueryParams();
		p.setKontrakt(kontrakt);
		p.setLeverandor(leverandor);
		p.setLevkontrakt(levkontrakt);
		p.setMonth(month);
		p.setRegistered(registered);
		p.setSubmitted(submitted);
		p.setProcessed(processed);
		p.setPaid(paid);
		p.setVisAllePerioderVedGodkjenning(visAllePerioderVedGodkjenning);
		return p;
	}
	
	/**
	 * @return the kontrakt
	 */
	public Driftkontrakt getKontrakt() {
		return kontrakt;
	}
	/**
	 * @return the leverandor
	 */
	public Leverandor getLeverandor() {
		return leverandor;
	}
	/**
	 * @return the levkontrakt
	 */
	public Levkontrakt getLevkontrakt() {
		return levkontrakt;
	}
	/**
	 * @return the month
	 */
	public Date getMonth() {
		return month;
	}

	public boolean isRegistered() {
		return registered;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public boolean isProcessed() {
		return processed;
	}

	public boolean isPaid() {
		return paid;
	}

	public boolean hasStatus(){
		return registered || submitted || processed || paid;
	}

	/**
	 * @param kontrakt the kontrakt to set
	 */
	public void setKontrakt(Driftkontrakt kontrakt) {
		Driftkontrakt old = this.kontrakt;
		this.kontrakt = kontrakt;
		try{
			getVetos().fireVetoableChange("kontrakt", old, kontrakt);
			getProps().firePropertyChange("kontrakt", old, kontrakt);
		} catch (PropertyVetoException e){
			this.kontrakt = old;
		}
	}
	/**
	 * @param leverandor the leverandor to set
	 */
	public void setLeverandor(Leverandor leverandor) {
		Leverandor old = this.leverandor;
		this.leverandor = leverandor;
		try{
			getVetos().fireVetoableChange("leverandor", old, leverandor);
			getProps().firePropertyChange("leverandor", old, leverandor);
		} catch (PropertyVetoException e){
			this.leverandor = old;
		}
	}
	/**
	 * @param levkontrakt the levkontrakt to set
	 */
	public void setLevkontrakt(Levkontrakt levkontrakt) {
		Levkontrakt old = this.levkontrakt;
		this.levkontrakt = levkontrakt;
		try{
			getVetos().fireVetoableChange("levkontrakt", old, levkontrakt);
			getProps().firePropertyChange("levkontrakt", old, levkontrakt);
		} catch (PropertyVetoException e){
			this.levkontrakt = old;
		}
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(Date month) {
		Date old = this.month;
		this.month = month;
		try{
			getVetos().fireVetoableChange("month", old, month);
			getProps().firePropertyChange("month", old, month);
		} catch (PropertyVetoException e){
			this.month = old;
		}
	}

	public void setRegistered(boolean registered) {
		boolean old = this.registered;
		this.registered = registered;
		try{
			getVetos().fireVetoableChange("registered", old, registered);
			getProps().firePropertyChange("registered", old, registered);
		}catch (PropertyVetoException e){
			this.registered = old;
		}
	}
	
	public void setSubmitted(boolean submitted) {
		boolean old = this.submitted;
		this.submitted = submitted;
		try{
			getVetos().fireVetoableChange("submitted", old, submitted);
			getProps().firePropertyChange("submitted", old, submitted);
		}catch (PropertyVetoException e){
			this.submitted = old;
		}
	}

	public void setProcessed(boolean processed) {
		boolean old = this.processed;
		this.processed = processed;
		try{
			getVetos().fireVetoableChange("processed", old, processed);
			getProps().firePropertyChange("processed", old, processed);
		}catch (PropertyVetoException e){
			this.processed = old;
		}
	}

	public void setPaid(boolean paid) {
		boolean old = this.paid;
		this.paid = paid;
		try{
			getVetos().fireVetoableChange("paid", old, paid);
			getProps().firePropertyChange("paid", old, paid);
		}catch (PropertyVetoException e){
			this.paid = old;
		}
	}
	
	protected PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}
		return props;
	}
	
	protected VetoableChangeSupport getVetos(){
		if (vetos==null)
			vetos = new VetoableChangeSupport(this);
		return vetos;
	}
	
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}
	
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}
	
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}
	@Override
	public void addVetoableChangeListener(VetoableChangeListener listener) {
		getVetos().addVetoableChangeListener(listener);
		
	}
	@Override
	public void addVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		getVetos().addVetoableChangeListener(propertyName, listener);
		
	}
	@Override
	public void removeVetoableChangeListener(VetoableChangeListener listener) {
		getVetos().removeVetoableChangeListener(listener);
		
	}
	@Override
	public void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		getVetos().removeVetoableChangeListener(propertyName, listener);
	}
	public void setVisAllePerioderVedGodkjenning(boolean visAllePerioderVedGodkjenning) {
		this.visAllePerioderVedGodkjenning = visAllePerioderVedGodkjenning;
	}
	public boolean isVisAllePerioderVedGodkjenning() {
		return visAllePerioderVedGodkjenning;
	}
	public Date now(){
		return Clock.now();
	}

	
	public void setKjoretoy(Kjoretoy kjoretoy) {
		Kjoretoy old = this.kjoretoy;
		this.kjoretoy = kjoretoy;
		try{
			getVetos().fireVetoableChange("kjoretoy", old, kjoretoy);
			getProps().firePropertyChange("kjoretoy", old, kjoretoy);
		} catch (PropertyVetoException e){
			this.kjoretoy = old;
		}
	}

	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}

}