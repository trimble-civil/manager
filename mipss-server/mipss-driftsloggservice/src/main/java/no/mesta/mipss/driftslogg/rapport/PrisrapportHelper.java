package no.mesta.mipss.driftslogg.rapport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;

import static no.mesta.mipss.common.TripProcessStatus.*;

public class PrisrapportHelper {

	private static final List<TripProcessStatus> approvedStatuses = Arrays.asList(
			PROCESSED_BY_CONTRACTOR, SENT_TO_IO, PAID
	);

	private final String NO_SETTLEMENT = "Intet oppgj�r";
	private final String UNIT_NA = "NA";
	
	private final DriftsloggBean bean;
	private final MengdeRapportHelper helper;

	public PrisrapportHelper(DriftsloggBean bean, MengdeRapportHelper helper){
		this.bean = bean;
		this.helper = helper;
		
	}
	public List<PrisrapportObject> getValuesFromAktivitet(List<Aktivitet> aktiviteter, Map<String, String> leverandorNrCache){
		List<PrisrapportObject> rapport = new ArrayList<PrisrapportObject>();
		
		for (Aktivitet aktivitet:aktiviteter){
			TripProcessStatus processStatus = aktivitet.getTrip().getProcessStatus();

			if (approvedStatuses.contains(processStatus) && aktivitet.isApprovedUE()) {
				PrisrapportObject pris = createPrisrapportObject(aktivitet);
				if (pris.getRate()!=null&&pris.getRate()==0.01d){//0.01 rater fra agresso skal h�ndteres som 0
					pris.setRate(0.0d);
				}
				
				Boolean found = false;
				for (PrisrapportObject prisrapportObject : rapport) {
					if (	prisrapportObject.getRate().doubleValue() == pris.getRate().doubleValue() &&
							prisrapportObject.getLeverandor().equals(pris.getLeverandor()) &&
							prisrapportObject.getArtikkelIdent().equals(pris.getArtikkelIdent())) {
						pris = prisrapportObject;
						found = true;
						break;
					}
				}
				
				if (!found) {
					AgrArtikkelV artikkel = bean.getArtikkelViewFromAktivitet(aktivitet);
					if (artikkel == null && aktivitet.getArtikkelIdent() == null) {
						pris.setGruppeNavn(NO_SETTLEMENT);
						pris.setArtikkelNavn(NO_SETTLEMENT);
						pris.setEnhet(UNIT_NA);
					} else if (artikkel != null) {
						pris.setGruppeNavn(artikkel.getGruppeNavn());
						pris.setArtikkelNavn(artikkel.getArtikkelNavn());
						pris.setEnhet(aktivitet.getEnhet());
						rapport.add(pris);
					} else {
						throw new IllegalArgumentException("An article should never be null when articleIdent is not");
					}
				}
				pris.addGodkjentMengde(aktivitet.getGodkjentMengde() == null ? 0.0 : aktivitet.getGodkjentMengde());
				pris.addRegistrertMengde(aktivitet.getUtfortMengde() == null ? 0.0 : aktivitet.getUtfortMengde());
			}
		}
		
		for(PrisrapportObject o:rapport){
			String levNavn = leverandorNrCache.get(o.getLeverandor());
			if (levNavn==null){
				levNavn = bean.getLeverandorNavnFromNr(o.getLeverandor());
				leverandorNrCache.put(o.getLeverandor(), levNavn);
			}
			levNavn = helper.getLevNrNavn(levNavn, o.getLeverandor());
			o.setLeverandor(levNavn);
		}
		
		return rapport;
	}
	
	private PrisrapportObject createPrisrapportObject(Aktivitet a){
		PrisrapportObject p = new PrisrapportObject();
		p.setLeverandor(a.getPeriode().getLevkontrakt().getLeverandorNr());
		p.setArtikkelIdent(a.getArtikkelIdent());
		p.setRate(a.getEnhetsPris());
		return p;
		
	}
}
