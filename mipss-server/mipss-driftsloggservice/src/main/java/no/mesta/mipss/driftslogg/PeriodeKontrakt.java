package no.mesta.mipss.driftslogg;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PeriodeKontrakt implements Serializable{
	private Long periodeId;
	private String levkontrakt;
	/**
	 * @return the periodeId
	 */
	public Long getPeriodeId() {
		return periodeId;
	}
	/**
	 * @param periodeId the periodeId to set
	 */
	public void setPeriodeId(Long periodeId) {
		this.periodeId = periodeId;
	}
	/**
	 * @return the levkontrakt
	 */
	public String getLevkontrakt() {
		return levkontrakt;
	}
	/**
	 * @param levkontrakt the levkontrakt to set
	 */
	public void setLevkontrakt(String levkontrakt) {
		this.levkontrakt = levkontrakt;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((levkontrakt == null) ? 0 : levkontrakt.hashCode());
		result = prime * result + ((periodeId == null) ? 0 : periodeId.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodeKontrakt other = (PeriodeKontrakt) obj;
		if (levkontrakt == null) {
			if (other.levkontrakt != null)
				return false;
		} else if (!levkontrakt.equals(other.levkontrakt))
			return false;
		if (periodeId == null) {
			if (other.periodeId != null)
				return false;
		} else if (!periodeId.equals(other.periodeId))
			return false;
		return true;
	}
	
}
