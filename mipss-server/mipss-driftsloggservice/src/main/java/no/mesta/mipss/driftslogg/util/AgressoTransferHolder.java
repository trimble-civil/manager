package no.mesta.mipss.driftslogg.util;

public class AgressoTransferHolder{
	private String orderId;
	private long lineNo = 0;
	private String levKontraktIdent;
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public long incLineNo(){
		lineNo++;
		return lineNo;
	}
	
	public void setLineNo(Long lineNo){
		this.lineNo = lineNo;
	}
	
	public String getLevKontraktIdent() {
		return levKontraktIdent;
	}
	
	public void setLevKontraktIdent(String levKontraktIdent) {
		this.levKontraktIdent = levKontraktIdent;
	}	
}