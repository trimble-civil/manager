package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;
import java.util.Date;

/**
 * Klasse som holder p� informasjon om en gitt v�r/temperatur-registrering gjort av UE i DL-web.
 */
public class VaerRapportObject implements Serializable{

	private Long tripId;
	private String leverandor;
	private String kjoretoy;
	private String rodeNavn;
	private Date fraDato;
	private Date tilDato;
	private String vaer;
	private String temperatur;
	private String kommentar;

	public Long getTripId() {
		return tripId;
	}

	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	public String getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}

	public String getKjoretoy() {
		return kjoretoy;
	}

	public void setKjoretoy(String kjoretoy) {
		this.kjoretoy = kjoretoy;
	}

	public String getRodeNavn() {
		return rodeNavn;
	}

	public void setRodeNavn(String rodeNavn) {
		this.rodeNavn = rodeNavn;
	}

	public Date getFraDato() {
		return fraDato;
	}

	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	public Date getTilDato() {
		return tilDato;
	}

	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}

	public String getVaer() {
		return vaer;
	}

	public void setVaer(String vaer) {
		this.vaer = vaer;
	}

	public String getTemperatur() {
		return temperatur;
	}

	public void setTemperatur(String temperatur) {
		this.temperatur = temperatur;
	}

	public String getKommentar() {
		return kommentar;
	}

	public void setKommentar(String kommentar) {
		this.kommentar = kommentar;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		VaerRapportObject that = (VaerRapportObject) o;

		return tripId != null ? tripId.equals(that.tripId) : that.tripId == null;
	}

	@Override
	public int hashCode() {
		return tripId != null ? tripId.hashCode() : 0;
	}

}