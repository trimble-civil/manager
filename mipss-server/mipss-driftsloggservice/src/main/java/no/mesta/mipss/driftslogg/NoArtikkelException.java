package no.mesta.mipss.driftslogg;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NoArtikkelException extends Exception implements Serializable{

	public NoArtikkelException(String message){
		super(message);
	}
}
