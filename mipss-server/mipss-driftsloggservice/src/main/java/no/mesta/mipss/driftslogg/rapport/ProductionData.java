package no.mesta.mipss.driftslogg.rapport;

import java.util.Date;

public class ProductionData {

    private final Integer prodStretchId;
    private final Date fromDate;
    private final Date toDate;
    private final double drivenKm;
    private final double drivenKmRode;
    private final int rodeId;
    private final int vehicleId;
    private final String vehicleName;
    private final int tiltakId;
    private final int prodTypeId;
    private final int grittingProductId;
    private final int county;
    private final String category;
    private final String winterMaintenanceClass;
    private double amount;
    private double dryFromSolution;
    private double solutionLiter;
    private double solutionRes2;
    private double solutionKg;

    public ProductionData(Integer prodStretchId, Date fromDate, Date toDate, double drivenKm, double drivenKmRode, int rodeId, int vehicleId, String vehicleName, int tiltakId, int prodTypeId, int grittingProductId, int county, String category, String winterMaintenanceClass) {
        this.prodStretchId = prodStretchId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.drivenKm = drivenKm;
        this.drivenKmRode = drivenKmRode;
        this.vehicleId = vehicleId;
        this.rodeId = rodeId;
        this.vehicleName = vehicleName;
        this.tiltakId = tiltakId;
        this.prodTypeId = prodTypeId;
        this.grittingProductId = grittingProductId;
        this.county = county;
        this.category = category;
        this.winterMaintenanceClass = winterMaintenanceClass;
    }

    public Integer getProdStretchId() {
        return prodStretchId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public double getDrivenKm() {
        return drivenKm;
    }

    public double getDrivenKmRode() {
        return drivenKmRode;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public int getProdTypeId() {
        return prodTypeId;
    }

    public int getGrittingProductId() {
        return grittingProductId;
    }

    public double getAmount() {
        return amount;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public double getDryFromSolution() {
        return dryFromSolution;
    }

    public double getSolutionLiter() {
        return solutionLiter;
    }

    public double getSolutionRes2() {
        return solutionRes2;
    }

    public double getSolutionKg() {
        return solutionKg;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setDryFromSolution(double dryFromSolution) {
        this.dryFromSolution = dryFromSolution;
    }

    public void setSolutionLiter(double solutionLiter) {
        this.solutionLiter = solutionLiter;
    }

    public void setSolutionRes2(double solutionRes2) {
        this.solutionRes2 = solutionRes2;
    }

    public void setSolutionKg(double solutionKg) {
        this.solutionKg = solutionKg;
    }

    public int getTiltakId() {
        return tiltakId;
    }

    public int getRodeId() {
        return rodeId;
    }

    public int getCounty() {
        return county;
    }

    public String getCategory() {
        return category;
    }

    public String getWinterMaintenanceClass() {
        return winterMaintenanceClass;
    }
}
