package no.mesta.mipss.driftslogg;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;

import no.mesta.mipss.persistence.IRenderableMipssEntity;


@SuppressWarnings("serial")
public class MipssInputTreeView implements IRenderableMipssEntity{
	private String leverandor;
	private String leverandorNr;
	private String kommentar;
	private List<MipssInputTreeView> mipssInputList = new ArrayList<MipssInputTreeView>();

	private MipssInput mipssInput;
	private String artikkel;
	private MipssInputTreeView parent;
	
	private boolean levSkalOverfores;
	
	public MipssInputTreeView(String leverandor, String leverandorNr){
		this.leverandor = leverandor;
		this.leverandorNr = leverandorNr;
	}
	
	public MipssInputTreeView(MipssInput mipssInput){
		this.mipssInput = mipssInput;
	}
	
	public void addChild(MipssInputTreeView view){
		mipssInputList.add(view);
		view.parent=this;
	}
	public MipssInputTreeView getParent(){
		return parent;
	}
	public List<MipssInputTreeView> getMipssInputList(){
		return mipssInputList;
	}
	public String getLeverandor(){
		return leverandor;
	}
	/**
	 * @return the mipssInput
	 */
	public MipssInput getMipssInput() {
		return mipssInput;
	}
	/**
	 * @return the aktivitetId
	 */
	public Long getAktivitetId() {
		if (mipssInput!=null){
			return mipssInput.getMipssAktivitetId();
		}
		return null;
	}
	/**
	 * @return the levkontraktIdent
	 */
	public String getLevkontraktIdent() {
		return mipssInput!=null?mipssInput.getContractId():null;
	}
	/**
	 * @return the artikkel
	 */
	public String getArtikkel() {
		return artikkel;
	}
	/**
	 * @param artikkel the artikkel to set
	 */
	public void setArtikkel(String artikkel) {
		this.artikkel = artikkel;
	}
	/**
	 * @return the enhet
	 */
	public String getEnhet() {
		return mipssInput!=null?mipssInput.getUnitCode():null;
	}
	/**
	 * @return the antall
	 */
	public Double getAntall() {
		return mipssInput!=null?mipssInput.getValue1():null;
	}
	/**
	 * @return the pris
	 */
	public Double getPris() {
		return mipssInput!=null?mipssInput.getUnitPrice():null;
	}
	/**
	 * @return the trip id
	 */
	public String getTripId() {
		if (mipssInput != null && mipssInput.getAktivitet() != null) {
			return String.valueOf(mipssInput.getAktivitet().getTrip().getId());
		}
		return null;
	}

	public Double getSum(){
		if (leverandor!=null){
			double sum = 0;
			for (MipssInputTreeView v:getMipssInputList()){
				if (v.getSum()!=null)
					sum+=v.getSum();
			}
			return sum;
		}
		if (getAntall()!=null){
			if (getPris()==0.01 || (Math.round(getPris() * 100.00) / 100.00) == 0.01){
				return 0d;
			}
			return getAntall()*getPris();
		}
		return null;
	}

	@Override
	public String getTextForGUI() {
		return null;
	}

	public String getInfo() {
		if (leverandor!=null){
			return leverandor;
		}
		if (getAktivitetId()!=null){
			return getAktivitetId().toString();
		}
		return null;
	}

	public void setKommentar(String kommentar) {
		this.kommentar = kommentar;
	}

	public String getKommentar() {
		return kommentar;
	}

	public String getLeverandorNr() {
		return leverandorNr;
	}
	
	public boolean equals(Object other){
		if (leverandor==null || ((MipssInputTreeView)other).leverandor==null)
			return super.equals(other);
		
		MipssInputTreeView o = (MipssInputTreeView) other;
		return o.leverandorNr.equals(leverandorNr);
	}
	
	public void setOverforLeverandor(boolean levSkalOverfores) {
		this.levSkalOverfores = levSkalOverfores;
	}
	
	public Boolean getOverforLeverandor(){
		return levSkalOverfores;
	}
}
