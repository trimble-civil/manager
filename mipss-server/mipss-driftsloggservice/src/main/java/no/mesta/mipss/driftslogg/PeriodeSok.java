package no.mesta.mipss.driftslogg;

import java.io.Serializable;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.PeriodeStatus.StatusType;

public class PeriodeSok implements Serializable, IRenderableMipssEntity{
	
	private Long id;
	private String leverandor;
	private String kontrakt;
	private Long antAktiviteter;
	private StatusType status;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the leverandor
	 */
	public String getLeverandor() {
		return leverandor;
	}
	/**
	 * @param leverandor the leverandor to set
	 */
	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}
	/**
	 * @return the kontrakt
	 */
	public String getKontrakt() {
		return kontrakt;
	}
	/**
	 * @param kontrakt the kontrakt to set
	 */
	public void setKontrakt(String kontrakt) {
		this.kontrakt = kontrakt;
	}
	/**
	 * @return the antAktiviteter
	 */
	public Long getAntAktiviteter() {
		return antAktiviteter;
	}
	/**
	 * @param antAktiviteter the antAktiviteter to set
	 */
	public void setAntAktiviteter(Long antAktiviteter) {
		this.antAktiviteter = antAktiviteter;
	}
	/**
	 * @return the status
	 */
	public StatusType getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(StatusType status) {
		this.status = status;
	}
	@Override
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
