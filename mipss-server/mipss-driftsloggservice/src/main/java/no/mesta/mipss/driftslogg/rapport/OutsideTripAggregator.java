package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

import static no.mesta.mipss.common.ManagerConstants.WEIGHT_VOLUME_FACTOR;

public class OutsideTripAggregator {

    private static final Logger log = LoggerFactory.getLogger(OutsideTripAggregator.class);

    private final Set<HandledTiltak> handledCombinations = new HashSet<>();

    protected Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupByVehicleRode(List<ProductionData> productionData) {
        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupedResults = new HashMap<>();
        for(ProductionData data : productionData) {
            OutsideTripDataFetcher.DataKey key = new OutsideTripDataFetcher.DataKey(data);
            if(!groupedResults.containsKey(key)) {
                groupedResults.put(key, new ArrayList<>());
            }
            groupedResults.get(key).add(data);
        }
        return groupedResults;
    }

    protected List<OutsideTripData> groupByTime(Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupedData, List<Trip> trips, List<Prodtype> prodtypes) {
        List<OutsideTripData> result = new ArrayList<>();

        for(OutsideTripDataFetcher.DataKey key : groupedData.keySet()) {
            List<ProductionData> dataList = groupedData.get(key);
            dataList.sort(Comparator.comparing(ProductionData::getFromDate));

            OutsideTripData current = null;
            for(ProductionData data : dataList) {
                Trip previousTrip = getPreviousTrip(trips, data);
                Trip nextTrip = getNextTrip(trips, data);

                Date startDate = calculateStartDate(data, previousTrip);
                Date endDate = calculateEndDate(data, nextTrip);

                if(current==null || !current.getFromDate().equals(startDate) || !current.getToDate().equals(endDate)) {
                    current = new OutsideTripData(data.getVehicleId(), data.getRodeId(), data.getWinterMaintenanceClass(), data.getCounty(), data.getCategory(), startDate, endDate);
                    result.add(current);
                }

                addProduction(current, data, prodtypes);
            }
        }

        return result;
    }

    protected void addProduction(OutsideTripData current, ProductionData data, List<Prodtype> prodtypes) {
        boolean added = false;
        current.addKm(data.getDrivenKmRode());

        Prodtype prodtype = getProdtype(prodtypes, data.getProdTypeId());

        if(prodtype.getBroyteFlagg()) {
            added = true;
            HandledTiltak key = new HandledTiltak(data.getRodeId(), data.getProdStretchId());
            if(!handledCombinations.contains(key)) {
                current.addBroytingKm(data.getDrivenKmRode());
                handledCombinations.add(key);
            }

            if(prodtype.getId()== ManagerConstants.ProdType.SIDEPLOG.getId()) { // Sideplog
                current.addSideplogKm(data.getDrivenKmRode());
            }
        }
        if(prodtype.getId()==ManagerConstants.ProdType.TUNG_HOVLING.getId()) { // H�vling, tung vegh�vel
            added = true;
            current.addTungHovlingKm(data.getDrivenKmRode());
            current.addTungHovlingTimer(getDurationInHours(data));
        }
        if(prodtype.getStroFlagg()) {
            added = true;
            current.addStroingTimer(getDurationInHours(data));

            double rodeFraction = data.getDrivenKmRode() / data.getDrivenKm();
            switch(ManagerConstants.Stroprod.idTilEnum(data.getGrittingProductId())) {
                case TORRSALT:
                    current.addTorrsaltTonn(data.getAmount()* rodeFraction);
                    break;
                case SAND_MED_SALT:
                    current.addSandSaltTonn(data.getAmount()* rodeFraction);
                    break;
                case REN_SAND:
                    current.addRenSandTonn(data.getAmount()* rodeFraction);
                    break;
                case SLURRY:
                    current.addSlurryTonn(data.getAmount()* rodeFraction);
                    break;
                case SALTLOSNING:
                    double amountToAdd;
                    if(data.getSolutionLiter()>0) {
                        amountToAdd = data.getSolutionLiter()/1000;
                    } else if(data.getSolutionRes2()>0) {
                        amountToAdd = data.getSolutionRes2()/1000;
                    } else {
                        amountToAdd = data.getSolutionKg() /1000 / WEIGHT_VOLUME_FACTOR;
                    }

                    current.addSaltlosningKubikk(amountToAdd* rodeFraction);
                    break;
                case BEFUKTET_SALT:
                    current.addBefuktetSaltTonn(data.getAmount()* rodeFraction);
                    break;
                case FAST_SAND:
                    current.addFastsandTonn(data.getAmount()* rodeFraction);
                    break;
                default:
                    added = false;
            }
        }
        if(!added) {
            log.error("Unable to add data from prodtype={}, grittingType={}", data.getProdTypeId(), data.getGrittingProductId());
        }
    }

    protected Double getDurationInHours(ProductionData data) {
        return ((double)(data.getToDate().getTime()-data.getFromDate().getTime()))/1000/3600;
    }

    protected Prodtype getProdtype(List<Prodtype> prodtypes, int id) {
        for(Prodtype prodtype : prodtypes) {
            if(id==prodtype.getId()) return prodtype;
        }
        throw new IllegalArgumentException("Unknown prodtype, "+id);
    }

    private Date calculateStartDate(ProductionData data, Trip trip) {
        Date date = toMidnight(data.getFromDate());

        if(trip==null || date.after(trip.getEndTime())) {
            return date;
        }
        return trip.getEndTime();
    }

    private Date calculateEndDate(ProductionData data, Trip trip) {
        Date date = toNextDay(data.getToDate());

        if(trip==null || date.before(trip.getStartTime())) {
            return date;
        }
        return trip.getStartTime();
    }

    private Date toMidnight(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    private Date toNextDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    private Trip getPreviousTrip(List<Trip> trips, ProductionData data) {
        Trip trip = null;
        List<Trip> vehicleTrips = trips.stream().filter(t -> t.getVehicleId() == data.getVehicleId() && t.getEndTime().compareTo(data.getFromDate())<=0).collect(Collectors.toList());
        for(Trip t: vehicleTrips) {
            if(trip==null || trip.getEndTime().before(t.getEndTime())) {
                trip = t;
            }
        }
        return trip;
    }

    private Trip getNextTrip(List<Trip> trips, ProductionData data) {
        Trip trip = null;
        List<Trip> vehicleTrips = trips.stream().filter(t->t.getVehicleId()==data.getVehicleId() && t.getStartTime().compareTo(data.getToDate())>=0).collect(Collectors.toList());
        for(Trip t : vehicleTrips) {
            if(trip==null || trip.getStartTime().after(t.getStartTime())) {
                trip = t;
            }
        }
        return trip;
    }

    private class HandledTiltak {
        private final int rodeId;
        private final int prodStretchId;

        public HandledTiltak(int rodeId, int prodStretchId) {
            this.rodeId = rodeId;
            this.prodStretchId = prodStretchId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HandledTiltak that = (HandledTiltak) o;

            if (rodeId != that.rodeId) return false;
            return prodStretchId == that.prodStretchId;
        }

        @Override
        public int hashCode() {
            int result = rodeId;
            result = 31 * result + prodStretchId;
            return result;
        }
    }
}
