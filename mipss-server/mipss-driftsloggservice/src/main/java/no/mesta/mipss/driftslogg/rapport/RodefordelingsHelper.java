package no.mesta.mipss.driftslogg.rapport;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.rodeservice.OverlappHelper;

public class RodefordelingsHelper {
	private OverlappHelper overlapp = new OverlappHelper();
	
	public RodefordelingsHelper(){
		
	}
	
	/**
	 * Lager fordelingsroder for alle str�-broyterodene
	 * @param r12roder
	 * @param strobroteroderMedRegistrering
	 * @return
	 */
	public List<Fordelingsrode> fordel(List<Rode> r12roder, List<Rode> strobroteroderMedRegistrering){
		List<Fordelingsrode> fordelingsroder = new ArrayList<Fordelingsrode>();
		
		for (Rode sbrode:strobroteroderMedRegistrering){
			if (sbrode.getId().equals(Long.valueOf(9465))){
				System.out.println("BABABABAB");
			}
			List<Rodefordeling> fordel = fordel(sbrode, r12roder);//finn hvilke r12roder denne roden overlapper
			if (fordel.isEmpty()){
				continue;
			}
			Fordelingsrode fr = new Fordelingsrode(sbrode);
			for (Rodefordeling fordeling:fordel){
				fr.addRodefordeling(fordeling);
			}
			fordelingsroder.add(fr);
		}
		return fordelingsroder;
	}
	
	/**
	 * Lager rodefordeling for angitte str�-br�yterode. 
	 * Finner hvilke R12roder denne roden overlapper med og lager en %vis fordelingsn�kkel
	 * @param sbrode
	 * @param r12roder
	 * @return
	 */
	private List<Rodefordeling> fordel(Rode sbrode, List<Rode> r12roder){
		List<Rodefordeling> rodefordeling = new ArrayList<Rodefordeling>();
		for (Rode r12rode:r12roder){
			List<Veinettveireferanse> veirefOverlapp= getOverlapp(sbrode, r12rode);
			if (veirefOverlapp==null||veirefOverlapp.isEmpty()){
				continue;
			}
			//vi har overlapp beregn hvor mye i prosent
			Double rodeLengdeIKm = sbrode.getRodeLengde();
			double overlappIKm = getKmOverlapp(veirefOverlapp);
			double fordeling = overlappIKm/rodeLengdeIKm;
			rodefordeling.add(new Rodefordeling(r12rode, fordeling));
		}
		return rodefordeling;
	}
	
	/**
	 * Beregn km overlapp p� roden
	 * @param veirefOverlapp
	 * @return
	 */
	private double getKmOverlapp(List<Veinettveireferanse> veirefOverlapp){
		double overlappIKm = 0;
		for (Veinettveireferanse v:veirefOverlapp){
			double fra = v.getFraKm();
			double til = v.getTilKm();
			overlappIKm+=Math.abs(til-fra);
		}
		return overlappIKm;
	}
	
	/**
	 * Finn overlapp mellom de to rodene
	 * @param sbrode
	 * @param r12rode
	 * @return
	 */
	private List<Veinettveireferanse> getOverlapp(Rode sbrode, Rode r12rode){
		return overlapp.sjekkOverlapp(getVeireferanseForRode(sbrode), getVeireferanseForRode(r12rode));
	}
	
	/**
	 * Sl� sammen alle veireferansene for en gitt rode til en enkelt liste
	 * @param rode
	 * @return
	 */
	private List<Veinettveireferanse> getVeireferanseForRode(Rode rode){
		List<Veinettveireferanse> veirefs = new ArrayList<Veinettveireferanse>();
		for (Veinettreflinkseksjon vr:rode.getVeinett().getVeinettreflinkseksjonList()){
			veirefs.addAll(vr.getVeinettveireferanseList());
		}
		return veirefs;
	}
}
