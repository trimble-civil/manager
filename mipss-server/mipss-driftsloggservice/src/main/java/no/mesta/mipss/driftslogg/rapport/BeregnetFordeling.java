package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.persistence.kontrakt.Rode;

public class BeregnetFordeling {

	private Rode r12rode;//r12rode
	private double beregnetVerdi;
	
	public BeregnetFordeling(Rode r12rode, double beregnetVerdi){
		this.r12rode = r12rode;
		this.beregnetVerdi = beregnetVerdi;
	}
	
	public Rode getR12rode(){
		return r12rode;
	}
	public double getBeregnetVerdi(){
		return beregnetVerdi;
	}
}
