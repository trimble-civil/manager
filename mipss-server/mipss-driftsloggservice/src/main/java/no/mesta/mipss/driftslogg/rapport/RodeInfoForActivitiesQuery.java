package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.driftslogg.ActivityRodeInfo;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

class RodeInfoForActivitiesQuery {

    private List<Query> queries;

    RodeInfoForActivitiesQuery(Long contractId, List<ApprovedActivity> approvedActivities, EntityManager em) {
        queries = createQueries(contractId, approvedActivities, em);
    }

    private List<Query> createQueries(Long contractId, List<ApprovedActivity> approvedActivities, EntityManager em) {
        List<Query> queries = new ArrayList<>();

        if (!approvedActivities.isEmpty()) {
            List<QueryDataItem> previousQueryDataItems = new ArrayList<>();
            List<QueryDataItem> currentQueryDataItems = new ArrayList<>();

            for (ApprovedActivity activity : approvedActivities) {
                String fromDateTime = convertToOracleDate(activity.getFromDateTime());
                String toDateTime = convertToOracleDate(activity.getToDateTime());

                QueryDataItem queryItem = new QueryDataItem(activity.getVehicleId(), activity.getRodeId(), fromDateTime, toDateTime);
                if (!currentQueryDataItems.contains(queryItem) && !previousQueryDataItems.contains(queryItem)) {
                    currentQueryDataItems.add(queryItem);
                    previousQueryDataItems.add(queryItem);

                    // We make queries with 50 activities at a time to avoid getting a PGA_AGGREGATE_LIMIT error from the database.
                    if (currentQueryDataItems.size() == 50) {
                        Query query = em.createNativeQuery(buildRodeInfoQuery(currentQueryDataItems))
                                .setParameter(1, contractId)
                                .setParameter(2, 15L);
                        queries.add(query);
                        currentQueryDataItems.clear();
                    }
                }
            }

            if (!currentQueryDataItems.isEmpty()) {
                Query query = em.createNativeQuery(buildRodeInfoQuery(currentQueryDataItems))
                        .setParameter(1, contractId)
                        .setParameter(2, 15L);
                queries.add(query);
            }
        }

        return queries;
    }

    private String buildRodeInfoQuery(List<QueryDataItem> queryDataItems) {
        StringBuilder q  = new StringBuilder();

        q.append("SELECT");
        q.append("   tt.kjoretoy_id AS vehicleId,");
        q.append("   r.id AS rodeId,");
        q.append("   ps.vinterdriftklasse AS winterClass,");
        q.append("   ps.fylkesnummer AS countyNumber,");
        q.append("   (ps.veikategori || ps.veistatus) AS roadType,");
        q.append("   tt.prodtype_id AS prodTypeId,");
        q.append("   tt.stroprodukt_id AS gritProdId,");
        q.append("   pt.stro_flagg AS gritFlag,");
        q.append("   pt.dau_broyte_flagg AS plowFlag,");
        q.append("   rs.fra_dato_tid AS fromDateTime,");
        q.append("   rs.til_dato_tid AS toDateTime,");
        q.append("   rs.kjort_km AS drivenKm,");
        q.append("   rs.ps_kjort_km AS prodStretchDrivenKm,");
        q.append("   tt.id AS measureId ");

        q.append("FROM rode r ");

        q.append("JOIN rodestrekning rs");
        q.append("   ON r.id = rs.rode_id ");
        q.append("JOIN prodstrekning ps");
        q.append("   ON rs.strekning_id = ps.id ");
        q.append("JOIN tiltak tt");
        q.append("   ON tt.strekning_id = ps.id ");
        q.append("JOIN prodtype pt");
        q.append("   ON tt.prodtype_id = pt.id ");

        q.append(buildRodeInfoWhere(queryDataItems));

        q.append("ORDER BY");
        q.append("   tt.kjoretoy_id,");
        q.append("   r.navn,");
        q.append("   ps.vinterdriftklasse,");
        q.append("   ps.fylkesnummer,");
        q.append("   (ps.veikategori || ps.veistatus),");
        q.append("   tt.fra_dato_tid,");
        q.append("   tt.prodtype_id");

        return q.toString();
    }

    private String buildRodeInfoWhere(List<QueryDataItem> queryDataItems) {
        StringBuilder q = new StringBuilder();

        q.append("WHERE");
        q.append("	 r.kontrakt_id = ?1");
        q.append("	 AND");
        q.append("	 r.type_id = ?2");
        q.append("	 AND");
        q.append("   pt.id IN (");
        q.append("      1, 5, 6, 7, 24, 98");
        q.append("   )");
        q.append("   AND");
        q.append("   (");

        String or = "";
        for (QueryDataItem queryDataItem : queryDataItems) {
            q.append("   ").append(or);
            q.append("   (");
            q.append("      tt.kjoretoy_id = ").append(queryDataItem.vehicleId);
            q.append("      AND");
            q.append("      rode_id = ").append(queryDataItem.rodeId);
            q.append("      AND");
            q.append("      (");
            q.append("         (");
            q.append("            tt.fra_dato_tid BETWEEN ").append(queryDataItem.fromDate).append(" AND ").append(queryDataItem.toDate);
            q.append("            AND");
            q.append("            tt.til_dato_tid <= ").append(queryDataItem.toDate);
            q.append("         )");
            q.append("         OR");
            q.append("         (");
            q.append("            tt.fra_dato_tid BETWEEN ").append(queryDataItem.fromDate).append(" AND ").append(queryDataItem.toDate);
            q.append("            AND");
            q.append("            tt.til_dato_tid > ").append(queryDataItem.toDate);
            q.append("         )");
            q.append("      )");
            q.append("   ) ");

            or = "OR";
        }

        q.append("   )");

        return q.toString();
    }

    private class QueryDataItem {

        private Integer vehicleId;
        private Integer rodeId;
        private String fromDate;
        private String toDate;

        QueryDataItem(Integer vehicleId, Integer rodeId, String fromDate, String toDate) {
            this.vehicleId = vehicleId;
            this.rodeId = rodeId;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            QueryDataItem that = (QueryDataItem) o;

            if (vehicleId != null ? !vehicleId.equals(that.vehicleId) : that.vehicleId != null) return false;
            if (rodeId != null ? !rodeId.equals(that.rodeId) : that.rodeId != null) return false;
            if (fromDate != null ? !fromDate.equals(that.fromDate) : that.fromDate != null) return false;
            return toDate != null ? toDate.equals(that.toDate) : that.toDate == null;
        }

        @Override
        public int hashCode() {
            int result = vehicleId != null ? vehicleId.hashCode() : 0;
            result = 31 * result + (rodeId != null ? rodeId.hashCode() : 0);
            result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
            result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
            return result;
        }

        public Integer getVehicleId() {
            return vehicleId;
        }

        public Integer getRodeId() {
            return rodeId;
        }

        public String getFromDate() {
            return fromDate;
        }

        public String getToDate() {
            return toDate;
        }

    }

    private String convertToOracleDate(Date date) {
        String formattedDate = MipssDateFormatter.formatDate(date, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

        StringBuilder sb = new StringBuilder();
        sb.append("TO_DATE('").append(formattedDate).append("', '").append(MipssDateFormatter.ORACLE_LONG_DATE_TIME_FORMAT).append("')");

        return sb.toString();
    }

    List<ActivityRodeInfo> getRodeInfo() {
        List<ActivityRodeInfo> rodeInfos = new ArrayList<>();

        if (!queries.isEmpty()) {
            Executor executor = Executors.newFixedThreadPool(Math.min(queries.size(), 100), Thread::new);

            List<CompletableFuture<List<ActivityRodeInfo>>> listFutureResult = queries.stream()
                    .map(query -> CompletableFuture.supplyAsync(() -> getActivityRodeInfo(query), executor))
                    .collect(Collectors.toList());

            rodeInfos = listFutureResult.stream()
                    .map(CompletableFuture::join)
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
        }

        return rodeInfos;
    }

    private List<ActivityRodeInfo> getActivityRodeInfo(Query query) {
        NativeQueryWrapper<ActivityRodeInfo> qry = new NativeQueryWrapper<>(
                query,
                ActivityRodeInfo.class,
                ActivityRodeInfo.getPropertiesArray());
        return qry.getWrappedList();
    }

}