package no.mesta.mipss.driftslogg.rapport;

import java.util.*;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.driftslogg.NoArtikkelException;
import no.mesta.mipss.driftslogg.util.TorrstoffUtil;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.Stroproduktmengde;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;

import static no.mesta.mipss.common.TripProcessStatus.*;
import static no.mesta.mipss.common.TripProcessStatus.PAID;
import static no.mesta.mipss.common.TripProcessStatus.SENT_TO_IO;

public class R12RapportHelper {

	private static final List<TripProcessStatus> approvedStatuses = Arrays.asList(
			PROCESSED_BY_CONTRACTOR, SENT_TO_IO, PAID
	);

	public static final String IDLE =          "tomkj\u00F8ring";
	public static final String PLOWING =       "br\u00F8yting";
	public static final String PLANING =       "h\u00F8vling";
	public static final String CO_PRODUCTION = "samproduksjon";
	public static final String HEAVY_GRADER =  "tung veih\u00F8vel"; // used in articles
	public static final String HEAVY_PLANING =  "tung h\u00F8vling"; // used in production types
	public static final String SIDE_PLOUGH =     "sideplog";
	public static final String GRITTING =      "str\u00F8ing";

	private final DriftsloggBean bean;
	private final MengdeRapportHelper helper;

	public R12RapportHelper(DriftsloggBean bean, MengdeRapportHelper helper) {
		this.bean = bean;
		this.helper = helper;
	}

	/**
	 * Legger til mipsstall til rapporten.
	 * @param rappResults
	 * @param leverandorer
	 */
	public void appendMipsstallFromRapp1(List<Rapp1> rappResults, Map<String, R12RapportObject> leverandorer){
		for (Rapp1 r:rappResults){
			getValuesForLeverandor(leverandorer, r);
		}
	}
	
	/**
	 * Henter mipsstall fra en rad i rapp1 for leverand�ren som matcher fra aktiviteten. 
	 * Dersom leverand�ren ikke finnes i listen, har ikke leverand�rn sendt inn br�ytekort, ikke f�tt det godkjent enda eller mangler instrumentering.
	 * @param leverandorer
	 * @param rapp1Row
	 */
	private void getValuesForLeverandor(Map<String, R12RapportObject> leverandorer, Rapp1 rapp1Row) {
		
		String key = rapp1Row.getLeverandorNr()+"|"+rapp1Row.getRodeNavn();
		R12RapportObject leverandorRapport = leverandorer.get(key);
		if (leverandorRapport==null){//leverandor har ikke sendt inn br�ytekort, evt er de ikke godkjent enda. opprett mipssdata
			leverandorRapport = new R12RapportObject();
			leverandorRapport.setLeverandor(helper.getLevNrNavn(rapp1Row.getLeverandorNavn(), rapp1Row.getLeverandorNr().toString()));
			leverandorRapport.setR12rode(rapp1Row.getRodeNavn());
			leverandorer.put(key, leverandorRapport);
		}
		for (R12Prodtype pt:rapp1Row.getProdtyper()){
			getValuesFromProdtyper(pt, leverandorRapport);
		}
		for (R12Stroprodukt s:rapp1Row.getStroprodukter()){
			getValuesFromR12Stroprodukter(s, leverandorRapport);
		}
	}
	/**
	 * Henter verdier fra prodtypene: br�yting og h�vling fra mipss-rapporten
	 * @param prodtype
	 * @param rapp
	 */
	private void getValuesFromProdtyper(R12Prodtype prodtype, R12RapportObject rapp){
		Double kjortKm = prodtype.getKjortKm();
		Double kjortTimer = prodtype.getKjortSekund().doubleValue();
		Long id = prodtype.getId();
		if (id==6||id==7){//br�yting eller h�vling 
			rapp.addBroytingKmMipss(kjortKm);
		}
		//Tung h�vling
		if (id == 24) {
			rapp.addTungHovlingTimerMipss(kjortTimer/60/60);
			rapp.addTungHovlingKmMipss(kjortKm);
		}
	}
	/**
	 * Henter verdier fra stroprodukter fra mipss-rapporten
	 * @param s
	 * @param rapp
	 */
	private void getValuesFromR12Stroprodukter(R12Stroprodukt s, R12RapportObject rapp) {
		Double kjortSekund = s.getKjortSekund().doubleValue();
		Long id = s.getId();
		boolean add = false;
		switch(id.intValue()){
		case 1: rapp.addTonnTorrsaltMipss(s.getMengdeTonn());add=true;break;
		case 2: rapp.addTonnSaltSandMipss(s.getMengdeTonn());add=true;break;
		case 3: rapp.addTonnRenSandMipss(s.getMengdeTonn());add=true;break;
		case 4: rapp.addTonnSlurryMipss(s.getMengdeTonn());add=true;break;
		case 5: rapp.addSaltlosningM3Mipss(s.getMengdeM3());add=true;break;
		case 6: rapp.addTonnBefuktetSaltMipss(s.getMengdeTonn());add=true;break;
		case 7: rapp.addTonnFastsandMipss(s.getMengdeTonn());add=true;break;
		}
		if (add)
			rapp.addStroingTimerMipss(kjortSekund/60/60);
	}
	/**
	 * Henter alle verdiene fra aktiviteter og fordeler dem i et map pr leverand�r.
	 * @param aktiviteter
	 * @return
	 * @throws NoArtikkelException
	 */
	public Map<String, R12RapportObject> getValuesFromAktiviteter(List<Aktivitet> aktiviteter, List<Fordelingsrode> fordelingsnokler, Map<String, String> leverandorNrCache) throws NoArtikkelException{
		Map<String, R12RapportObject> leverandorRoder = new HashMap<String, R12RapportObject>();
		
		for (Aktivitet aktivitet:aktiviteter){
			TripProcessStatus processStatus = aktivitet.getTrip().getProcessStatus();

			if (approvedStatuses.contains(processStatus) && aktivitet.isApprovedBuild()) {
				Fordelingsrode fordeling = getFordelingsrodeForAktivitet(fordelingsnokler, aktivitet);
				if (fordeling==null){//aktivitet uten rode ignorer.
					continue;
				}
				List<Rode> r12roder = fordeling.getR12Roder();
				List<R12RapportObject> r12ForAktivitet = new ArrayList<R12RapportObject>();
				
				for (Rode r12:r12roder){
					R12RapportObject r12RapportObject = leverandorRoder.get(aktivitet.getPeriode().getLevkontrakt().getLeverandorNr()+"|"+r12.getNavn());
					if (r12RapportObject==null){
						r12RapportObject = new R12RapportObject();
						r12RapportObject.setLeverandor(aktivitet.getPeriode().getLevkontrakt().getLeverandorNr());
						r12RapportObject.setR12rode(r12.getNavn());
						leverandorRoder.put(aktivitet.getPeriode().getLevkontrakt().getLeverandorNr()+"|"+r12.getNavn(), r12RapportObject);
					}
					r12RapportObject.addFordeling(fordeling);
					r12RapportObject.addRode(aktivitet.getRodeId());
					r12ForAktivitet.add(r12RapportObject);
				}
				List<Stroproduktmengde> stroprodukter = aktivitet.getStroprodukter();
				getValuesFromR12Stroprodukter(stroprodukter, fordeling, r12ForAktivitet);
				AgrArtikkelV artikkel = bean.getArtikkelViewFromAktivitet(aktivitet);
				if (artikkel != null) {
					getValuesFromArtikkel(artikkel, aktivitet, fordeling, r12ForAktivitet);
				}
				if ( artikkel==null && aktivitet.getArtikkelIdent() != null ){
					String levk = aktivitet.getPeriode().getLevkontraktIdent();
					String artikkelId = aktivitet.getArtikkelIdent();
					String err = "Artikkelen : "+artikkelId+" er fjernet fra Agresso for avtale:"+levk;
					throw new NoArtikkelException(err);
				}
			}
		}
		Map<String, R12RapportObject> prLevNavn = new HashMap<String, R12RapportObject>();
		Map<Long, String> rodenavnPrId = new HashMap<Long, String>();
		//Hent tekstinfo om leverand�rer og roder.
		for (String levNrR12Navn:leverandorRoder.keySet()){
			R12RapportObject rapp = leverandorRoder.get(levNrR12Navn);
			String levNavn = leverandorNrCache.get(rapp.getLeverandor());
			if (levNavn == null){
				levNavn = bean.getLeverandorNavnFromNr(rapp.getLeverandor());
				leverandorNrCache.put(rapp.getLeverandor(), levNavn);
			}
			List<Long> strobroyteRodeIds = rapp.getStrobroyteRodeIds();
			for (Long id:strobroyteRodeIds){
				String rodenavn = rodenavnPrId.get(id);
				if (rodenavn==null){
					rodenavn = bean.getRodeNavnFromId(id);
					rodenavnPrId.put(id, rodenavn);
				}
				rapp.addRodenavn(rodenavn);
			}
			levNavn = helper.getLevNrNavn(levNavn, rapp.getLeverandor());
			rapp.setLeverandor(levNavn);
			prLevNavn.put(levNrR12Navn, rapp);
		}
		return prLevNavn;
	}

	/**
	 * Finner fordelingsroden for aktivitetens rode
	 * @param fordelingsnokler
	 * @param aktivitet
	 * @return
	 */
	private Fordelingsrode getFordelingsrodeForAktivitet(List<Fordelingsrode> fordelingsnokler, Aktivitet aktivitet){
		for (Fordelingsrode fr: fordelingsnokler){
			if (fr.getStroBroyteRode().getId().equals(aktivitet.getRodeId())){
				return fr;
			}
		}
		return null;
	}
	
	/**
	 * Henter verdier fra stroprodukter fra mipss-rapporten
	 */
	private void getValuesFromR12Stroprodukter(List<Stroproduktmengde> stroprodukter, Fordelingsrode fordeling, List<R12RapportObject> r12ForAktivitet) {
		if (stroprodukter!=null){
			for (Stroproduktmengde s:stroprodukter){
				Long id = s.getStroproduktId();
				if (id==4){
					fordelSlurry(s, fordeling, r12ForAktivitet);
					continue;
				}
				if (id==5){
					fordelSaltlosning(s, fordeling, r12ForAktivitet);
					continue;
				}
				if (id==6){
					fordelBefuktetSalt(s, fordeling, r12ForAktivitet);
				}
				if (s.getOmforentMengdeTort() != null) {
					List<BeregnetFordeling> beregnetFordeling = fordeling.getBeregnetFordeling(s.getOmforentMengdeTort());
					for (BeregnetFordeling br : beregnetFordeling) {
						R12RapportObject r12Obj = getR12RapportObjectForFordeling(r12ForAktivitet, br.getR12rode());
						switch (id.intValue()) {
							case 1:
								r12Obj.addTonnTorrsalt(br.getBeregnetVerdi());
								break;
							case 2:
								r12Obj.addTonnSaltSand(br.getBeregnetVerdi());
								break;
							case 3:
								r12Obj.addTonnRenSand(br.getBeregnetVerdi());
								break;
							case 7:
								r12Obj.addTonnFastsand(br.getBeregnetVerdi());
								break;
						}
					}
				}
			}
		}

	}

	/**
	 * Henter verdier fra rapporterte aktiviteter. 
	 * Bruker kun verdier fra aktiviteter som er registrert p� produksjonstypene: Br�yting, h�vling, tungh�vling, og sideplog.
	 * @param artikkel
	 * @param a
	 * @param fordeling
	 * @param r12ForAktivitet
	 */
	
	void getValuesFromArtikkel(AgrArtikkelV artikkel, Aktivitet a, Fordelingsrode fordeling, List<R12RapportObject> r12ForAktivitet){
		//String productionType = a.getProductionType().toLowerCase();
		String productionType = a.getProductionType();

		int gritStartIndex = productionType.indexOf(":");
		if (gritStartIndex > 0) {
			productionType = productionType.substring(0, gritStartIndex).trim();
		}

		String[] productionTypes = productionType.split("\\+");

		// R12 amount is always in km - no matter if the UE has sat the unit to be in hours (TIM).

		boolean coproduction = false;
		if (!a.getEnhet().equalsIgnoreCase("STK")) {
			for (String prodType : productionTypes) {
				boolean isPlowingPlaning = false;
				boolean isHeavyPlaning = false;
				boolean isSidePlough = false;

				Double amount = 0.0;

				switch (prodType.toLowerCase()) {
					case PLOWING:
					case PLANING:
						isPlowingPlaning = true;
						amount = a.getR12_broyting();
						break;
					case HEAVY_PLANING:
						isHeavyPlaning = true;
						amount = a.getR12_hovling();
						break;
					case SIDE_PLOUGH:
						isSidePlough = true;
						amount = a.getR12_sideplog();
						break;
				}

				if (amount != null && amount != 0.0) {
					List<BeregnetFordeling> beregnetFordeling = fordeling.getBeregnetFordeling(amount);

					for (BeregnetFordeling br : beregnetFordeling) {
						R12RapportObject r12Obj = getR12RapportObjectForFordeling(r12ForAktivitet, br.getR12rode());

						if (isPlowingPlaning && !coproduction) {
							r12Obj.addBroytingKm(br.getBeregnetVerdi());
							coproduction = true;
						}

						if (isHeavyPlaning) {
							r12Obj.addTungHovlingKm(br.getBeregnetVerdi());
						}

						if (isSidePlough) {
							r12Obj.addSidePlogKm(br.getBeregnetVerdi());
						}
					}
				}

				if (a.isApprovedIntention() && a.getR12_intention() != null) {
					List<BeregnetFordeling> beregnetFordeling = fordeling.getBeregnetFordeling(a.getR12_intention());
					for (BeregnetFordeling br : beregnetFordeling) {
						R12RapportObject r12Obj = getR12RapportObjectForFordeling(r12ForAktivitet, br.getR12rode());
						r12Obj.addIntentionKm(br.getBeregnetVerdi());
					}
				}
			}
		}
	}

	/** *********************DIVERSE INTERNE UTILS************************* */
	
	private void fordelSlurry(Stroproduktmengde s, Fordelingsrode fordeling, List<R12RapportObject> r12ForAktivitet) {
		Double torr = s.getOmforentMengdeTort();
		Double vaatt = s.getOmforentMengdeVaatt();
		String enhet = s.getEnhetVaatt();
		double mengde = torr+TorrstoffUtil.tonnTorr(enhet, vaatt);
		List<BeregnetFordeling> beregnetFordeling = fordeling.getBeregnetFordeling(mengde);
		
		for (BeregnetFordeling br:beregnetFordeling){
			R12RapportObject r12Obj = getR12RapportObjectForFordeling(r12ForAktivitet, br.getR12rode());
			r12Obj.addTonnSlurry(br.getBeregnetVerdi());
		}
	}
	
	private void fordelBefuktetSalt(Stroproduktmengde s, Fordelingsrode fordeling, List<R12RapportObject> r12ForAktivitet) {
		Double torr = s.getOmforentMengdeTort();
		Double vaatt = s.getOmforentMengdeVaatt();
		String enhet = s.getEnhetVaatt();
		double mengde = torr+TorrstoffUtil.tonnTorr(enhet, vaatt);
		List<BeregnetFordeling> beregnetFordeling = fordeling.getBeregnetFordeling(mengde);
		
		for (BeregnetFordeling br:beregnetFordeling){
			R12RapportObject r12Obj = getR12RapportObjectForFordeling(r12ForAktivitet, br.getR12rode());
			r12Obj.addTonnBefuktetSalt(br.getBeregnetVerdi());
		}
	}

	private void fordelSaltlosning(Stroproduktmengde s, Fordelingsrode fordeling, List<R12RapportObject> r12ForAktivitet) {
		Double vaatt = s.getOmforentMengdeVaatt();
		String enhet = s.getEnhetVaatt();
		double mengde = TorrstoffUtil.m3ToLiter(enhet, vaatt);
		List<BeregnetFordeling> beregnetFordeling = fordeling.getBeregnetFordeling(mengde);
		for (BeregnetFordeling br:beregnetFordeling){
			R12RapportObject r12Obj = getR12RapportObjectForFordeling(r12ForAktivitet, br.getR12rode());
			r12Obj.addSaltlosningM3(br.getBeregnetVerdi());
		}
	}
	/**
	 * Henter det r12RapportObjectet som innehar r12Rode
	 * @param r12ForAktivitet
	 * @param r12rode
	 * @return
	 */
	private R12RapportObject getR12RapportObjectForFordeling(List<R12RapportObject> r12ForAktivitet, Rode r12rode) {
		R12RapportObject r12Obj = null;
		for (R12RapportObject r12:r12ForAktivitet){
			if (r12rode.getNavn().equals(r12.getR12rode())){
				r12Obj = r12;
				break;
			}
		}
		return r12Obj;
	}
}
