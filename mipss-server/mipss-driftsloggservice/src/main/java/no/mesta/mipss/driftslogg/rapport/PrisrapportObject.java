package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;

public class PrisrapportObject implements Serializable{

	private String leverandor;
	private String artikkelNavn;
	private String artikkelIdent;
	private String gruppeNavn;
	
	private String enhet;
	private Double godkjentMengde;
	private Double rate;
	private Double registrertMengde;
	private Double pris;
	
	public String getLeverandor() {
		return leverandor;
	}
	public String getGruppeNavn() {
		return gruppeNavn;
	}
	public String getArtikkelNavn() {
		return artikkelNavn;
	}
	public String getArtikkelIdent() {
		return artikkelIdent;
	}
	public String getEnhet() {
		return enhet;
	}
	public Double getGodkjentMengde() {
		return godkjentMengde;
	}
	public Double getRate() {
		return rate;
	}
	public Double getRegistrertMengde(){
		return registrertMengde;
	}
	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}
	public void setArtikkelNavn(String artikkelNavn) {
		this.artikkelNavn = artikkelNavn;
	}
	public void setArtikkelIdent(String artikkelIdent){
		this.artikkelIdent = artikkelIdent;
	}
	public void setGruppeNavn(String gruppeNavn) {
		this.gruppeNavn = gruppeNavn;
	}
	public void setEnhet(String enhet) {
		this.enhet = enhet;
	}
	public void addGodkjentMengde(Double godkjentMengde) {
		this.godkjentMengde = addDouble(this.godkjentMengde, godkjentMengde);
	}
	public void addRegistrertMengde(Double registrertMengde) {
		this.registrertMengde = addDouble(this.registrertMengde, registrertMengde);
	}
	public void setRate(Double rate) {
		if((Math.round(rate * 100.00) / 100.00) == 0.01d)
			this.rate = 0d;
		else
			this.rate = rate;
	}	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikkelIdent == null) ? 0 : artikkelIdent.hashCode());
		result = prime * result + ((leverandor == null) ? 0 : leverandor.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrisrapportObject other = (PrisrapportObject) obj;
		if (artikkelIdent == null) {
			if (other.artikkelIdent != null)
				return false;
		} else if (!artikkelIdent.equals(other.artikkelIdent))
			return false;
		if (leverandor == null) {
			if (other.leverandor != null)
				return false;
		} else if (!leverandor.equals(other.leverandor))
			return false;
		return true;
	}
	
	private Double addDouble(Double d1, Double d2){
		if (d1==null){
			return d2;
		}
		if (d2!=null){
			return d1+d2;
		}
		return d1;
	}
	public PrisrapportObject cloneForSum() {
		PrisrapportObject clone = new PrisrapportObject();
		clone.setGruppeNavn(getGruppeNavn());
		return clone;
	}
	public void addPris(Double pris) {
		this.pris = addDouble(this.pris, pris);
	}
	public Double getPris() {
		return pris;
	}
	
	
	
}
