package no.mesta.mipss.driftslogg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import no.mesta.mipss.persistence.driftslogg.PeriodeStatus;

/**
 * En status for ting som skal overf�res til agresso
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class SendAgressoStatus implements Serializable {

	/** Antall aktiviteter som vil bli overf�rt til agresso */
	private Integer antGodkjenteAktiviteter;
	/** Antall aktiviteter som ikke vil bli overf�rt agresso	 */
	private Integer antIkkeGodkjenteAktiviteter;
	/** Antall aktiviteter som blir overf�r fra en tidligere periode */
	private Integer antOverforteAktiviteter;
	
	/** F�rste dato for periodene som vil bli overf�rt til agresso */
	private Date fraDato;
	/** Siste dato for periodene som vil bli overf�rt til agresso */
	private Date tilDato;
	
	private List<PeriodeKontrakt> periodeId;
	
	/**
	 * @return the antGodkjenteAktiviteter
	 */
	public Integer getAntGodkjenteAktiviteter() {
		return antGodkjenteAktiviteter;
	}
	/**
	 * @param antGodkjenteAktiviteter the antGodkjenteAktiviteter to set
	 */
	public void setAntGodkjenteAktiviteter(Integer antGodkjenteAktiviteter) {
		this.antGodkjenteAktiviteter = antGodkjenteAktiviteter;
	}
	/**
	 * @return the antIkkeGodkjenteAktiviteter
	 */
	public Integer getAntIkkeGodkjenteAktiviteter() {
		return antIkkeGodkjenteAktiviteter;
	}
	/**
	 * @param antIkkeGodkjenteAktiviteter the antIkkeGodkjenteAktiviteter to set
	 */
	public void setAntIkkeGodkjenteAktiviteter(Integer antIkkeGodkjenteAktiviteter) {
		this.antIkkeGodkjenteAktiviteter = antIkkeGodkjenteAktiviteter;
	}
	/**
	 * @return the fraDato
	 */
	public Date getFraDato() {
		return fraDato;
	}
	/**
	 * @param fraDato the fraDato to set
	 */
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	/**
	 * @return the tilDato
	 */
	public Date getTilDato() {
		return tilDato;
	}
	/**
	 * @param tilDato the tilDato to set
	 */
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	public void setPeriodeId(List<PeriodeKontrakt> periodeId) {
		this.periodeId = periodeId;
	}
	public List<PeriodeKontrakt> getPeriodeId() {
		return periodeId;
	}
	public void setAntOverforteAktiviteter(Integer antOverforteAktiviteter) {
		this.antOverforteAktiviteter = antOverforteAktiviteter;
	}
	public Integer getAntOverforteAktiviteter() {
		return antOverforteAktiviteter;
	}
}
