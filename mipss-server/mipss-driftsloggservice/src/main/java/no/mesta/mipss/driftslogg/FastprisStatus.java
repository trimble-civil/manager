package no.mesta.mipss.driftslogg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

@SuppressWarnings("serial")
public class FastprisStatus implements Serializable, IRenderableMipssEntity{
	private Leverandor leverandor;
	private List<Aktivitet> aktiviteter;
	private AgrArtikkelV artikkel;
	
	private Double antallRegistrert;
	private Double antallGodkjent;
	
	private String uker = "";
	
	public Double getAntallRegistrert(){
		return antallRegistrert;
	}
	public Double getAntallGodkjent(){
		return antallGodkjent;
	}
	
	public Double getSumPris(){
		return artikkel.getPris()*antallGodkjent;
	}
	
	public Boolean getContainsEditable(){
		if (aktiviteter==null||aktiviteter.size()==0)
			return true;
		for (Aktivitet a:aktiviteter){
			if (a.getOpprettetAv().startsWith("MIPSS_")&&a.getSendtAgresso()==false)
				return true;
		}
		return false;
	}
	/**
	 * @return the leverandor
	 */
	public Leverandor getLeverandor() {
		return leverandor;
	}
	/**
	 * @param leverandor the leverandor to set
	 */
	public void setLeverandor(Leverandor leverandor) {
		this.leverandor = leverandor;
	}
	/**
	 * @return the aktivitet
	 */
	public List<Aktivitet> getAktiviteter() {
		return aktiviteter;
	}
	/**
	 * @param aktivitet the aktivitet to set
	 */
	public void setAktiviteter(List<Aktivitet> aktiviteter) {
		this.aktiviteter = aktiviteter;
		
		if (aktiviteter == null)
			return;
		
		double antall = 0;
		for (Aktivitet a : aktiviteter) {
			if (a.isApprovedUE()) {
				Double godkjentMengde = a.getGodkjentMengde();
				if (godkjentMengde != null)
					antall += godkjentMengde;
			}
		}
		antallGodkjent = antall;
		
		antall = 0;
		ArrayList<Integer> ukenummer = new ArrayList<Integer>();
		for (Aktivitet a:aktiviteter){
			Double omforentMengde = a.getOmforentMengde();
			antall +=omforentMengde;
			Calendar cal = Calendar.getInstance();
			cal.setTime(a.getFraDatoTid());
			ukenummer.add(cal.get(Calendar.WEEK_OF_YEAR));			
		}
		antallRegistrert = antall;
		
		//Sorterer ukenummer-listen og legger til en kommaseparert liste 
		//med ukenummerne hvor fastpris er registrert.		
		if(antallRegistrert > 0){
			Collections.sort(ukenummer);
			for (int i = 0; i < ukenummer.size(); i++) {
				int uke = ukenummer.get(i);
				if (i == 0)
					uker = uke + "";
				else
					uker += ", " + uke;
			}
		}		
	}
	/**
	 * @return the artikkel
	 */
	public AgrArtikkelV getArtikkel() {
		return artikkel;
	}
	/**
	 * @param artikkel the artikkel to set
	 */
	public void setArtikkel(AgrArtikkelV artikkel) {
		this.artikkel = artikkel;
	}
	@Override
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @return the uker
	 */
	public String getUker() {
		return uker;
	}
}
