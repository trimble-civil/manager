package no.mesta.mipss.driftslogg;

import java.io.Serializable;
import java.util.List;

import no.mesta.mipss.persistence.driftslogg.MipssTiltak;

public class MipssRodeTiltak implements Serializable {
	private String rode;
	private List<MipssTiltak> tiltak;
	
	public String getRode() {
		return rode;
	}
	public void setRode(String rode) {
		this.rode = rode;
	}
	public List<MipssTiltak> getTiltak() {
		return tiltak;
	}
	public void setTiltak(List<MipssTiltak> tiltak) {
		this.tiltak = tiltak;
	}
		
}
