package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class DriftsloggRapportData implements Serializable{

	private List<R12RapportObject> r12rapport;
	private List<PrisrapportObject> prisrapport;
	
	public DriftsloggRapportData(List<R12RapportObject> r12rapport, List<PrisrapportObject> prisrapport) {
		this.r12rapport = r12rapport;
		this.prisrapport = prisrapport;
	}

	public List<PrisrapportObject> getSumPrisArtikkelGruppe(){
		List<PrisrapportObject> sum = new ArrayList<>();
		
		for (PrisrapportObject rapp:prisrapport){
			PrisrapportObject gruppeSum = null;
			
			for (PrisrapportObject eksisterende:sum){
				if (eksisterende.getGruppeNavn().equals(rapp.getGruppeNavn())){
					gruppeSum = eksisterende;
					break;
				}
			}
			
			if (gruppeSum==null){
				gruppeSum = rapp.cloneForSum();
				sum.add(gruppeSum);
			}
			gruppeSum.addPris(rapp.getRate()*rapp.getGodkjentMengde());
		}
		return sum;
	}
	
	public List<R12RapportObject> getR12Summer(){
		List<R12RapportObject> sum = new ArrayList<R12RapportObject>();
		for (R12RapportObject r:r12rapport){
			R12RapportObject rodeSum = null;
			for (R12RapportObject rsum:sum){
				if (r.getR12rode().equals(rsum.getR12rode())){
					rodeSum = rsum;
					break;
				}
			}
			if (rodeSum==null){
				rodeSum = r.cloneForSum();
				sum.add(rodeSum);
			}
			rodeSum.addBroytingKm(r.getBroytingKm());
			if (r.getIntentionKm() != null)
				rodeSum.addIntentionKm(r.getIntentionKm());
			if (r.getPlowingIntentionKm() != null)
				rodeSum.addPlowingIntentionKm(r.getPlowingIntentionKm());
			if (r.getSaltlosningM3()!=null)
				rodeSum.addSaltlosningM3(r.getSaltlosningM3()*1000);
			rodeSum.addStroingTimer(r.getStroingTimer());
			if (r.getBefuktetSaltTonn()!=null)
				rodeSum.addTonnBefuktetSalt(r.getBefuktetSaltTonn()*1000);
			if (r.getFastsandTonn()!=null)
				rodeSum.addTonnFastsand(r.getFastsandTonn()*1000);
			if (r.getRendSandTonn()!=null)
				rodeSum.addTonnRenSand(r.getRendSandTonn()*1000);
			if (r.getSandMedSaltTonn()!=null)
				rodeSum.addTonnSaltSand(r.getSandMedSaltTonn()*1000);
			if (r.getSlurryTonn()!=null)
				rodeSum.addTonnSlurry(r.getSlurryTonn()*1000);
			if (r.getTorrsaltTonn()!=null)
				rodeSum.addTonnTorrsalt(r.getTorrsaltTonn()*1000);
			if (r.getTungHovlingTimer() != null)
				rodeSum.addTungHovlingTimer(r.getTungHovlingTimer());
			if (r.getTungHovlingKm() != null)
				rodeSum.addTungHovlingKm(r.getTungHovlingKm());
			
			rodeSum.addBroytingKmMipss(r.getBroytingKmMipss());
			if (r.getSaltlosningM3Mipss()!=null)
				rodeSum.addSaltlosningM3Mipss(r.getSaltlosningM3Mipss());
			rodeSum.addStroingTimerMipss(r.getStroingTimerMipss());
			if (r.getBefuktetSaltTonnMipss()!=null)
				rodeSum.addTonnBefuktetSaltMipss(r.getBefuktetSaltTonnMipss());
			if (r.getFastsandTonnMipss()!=null)
				rodeSum.addTonnFastsandMipss(r.getFastsandTonnMipss());
			if (r.getRendSandTonnMipss()!=null)
				rodeSum.addTonnRenSandMipss(r.getRendSandTonnMipss());
			if (r.getSandMedSaltTonnMipss()!=null)
				rodeSum.addTonnSaltSandMipss(r.getSandMedSaltTonnMipss());
			if (r.getSlurryTonnMipss()!=null)
				rodeSum.addTonnSlurryMipss(r.getSlurryTonnMipss());
			if (r.getTorrsaltTonnMipss()!=null)
				rodeSum.addTonnTorrsaltMipss(r.getTorrsaltTonnMipss());
			if (r.getTungHovlingTimerMipss() != null)
				rodeSum.addTungHovlingTimerMipss(r.getTungHovlingTimerMipss());
			if (r.getTungHovlingKmMipss() != null)
				rodeSum.addTungHovlingKmMipss(r.getTungHovlingKmMipss());
		}
		return sum;
	}

	/**
	 * @return the r12rapport
	 */
	public List<R12RapportObject> getR12rapport() {
		return r12rapport;
	}
	
	public List<PrisrapportObject> getPrisrapport(){
		return prisrapport;
	}
	
	public int size(){
		int size = 0;
		if (r12rapport!=null){
			int rs = r12rapport.size();
			size+=rs;
		}
		return size;
		
	}

}