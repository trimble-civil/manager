package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Rode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static no.mesta.mipss.common.TripProcessStatus.SUBMITTED_BY_SUBCONTRACTOR;
import static no.mesta.mipss.persistence.driftslogg.Trip.FIND_BY_VEHICLES_DATE;

public class OutsideTripDataFetcher {

    private static final Logger log = LoggerFactory.getLogger(OutsideTripDataFetcher.class);

    private static final int PLOUGHING_RODE_ID=15;
    private static final int PLOUGHING_RODE_CONNECTION_ID=1;
    private static final int ELIGIBLE_TRIP_STATUS_LEVEL = SUBMITTED_BY_SUBCONTRACTOR.getLevel();

    private final DriftsloggQueryParams params;
    private final Date fromDate;
    private final Date toDate;
    private final EntityManager em;

    private int rowCount = 0;

    public OutsideTripDataFetcher(DriftsloggQueryParams params, Date fromDate, Date toDate, EntityManager em) {
        this.params = params;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.em = em;
    }


    public List<ProductionData> getProductionData(List<Vehicle> vehicles, List<Rode> rodes) {
        long before = System.currentTimeMillis();
        rowCount = 0;
        StringBuilder sql = new StringBuilder();
        sql.append("select ps.id ps_id, rs.fra_dato_tid, rs.rode_id, rs.til_dato_tid, rs.kjort_km, rs.ps_kjort_km, tt.kjoretoy_id, tt.id tiltak_id, tt.prodtype_id, tt.stroprodukt_id, ps.fylkesnummer, ps.veikategori, ps.veistatus, ps.vinterdriftklasse ");
        sql.append("from prodstrekning ps join rodestrekning rs on rs.strekning_id=ps.id join tiltak tt on tt.strekning_id=ps.id ");
        sql.append("where ps.til_tidspunkt > ? and ps.fra_tidspunkt < ? and dfu_id in (");
        addConcatenatedList(vehicles, sql, v->(long)v.dfuId);
        sql.append(") and rs.rode_id in (");
        addConcatenatedList(rodes, sql, r->r.getId());
        sql.append(") and tt.kjoretoy_id in (");
        addConcatenatedList(vehicles, sql, v->(long)v.vehicleId);
        sql.append(") and tt.prodtype_id <98");

        Set<Integer> ids = new HashSet<>();
        List<ProductionData> result = new ArrayList<>();
        Connection conn = em.unwrap(Connection.class);
        try(PreparedStatement st = conn.prepareStatement(sql.toString())) {
            st.setFetchSize(10000);
            st.setTimestamp(1, new java.sql.Timestamp(fromDate.getTime()));
            st.setTimestamp(2, new java.sql.Timestamp(toDate.getTime()));
            try(ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    result.add(map(rs, vehicles, ids));
                }
            }
        } catch(SQLException e) {
            log.error("Unable to fetch production data for contract={}, fromDate={}, toDate={}", params.getKontrakt().getId(), fromDate, toDate);
            return new ArrayList<>();
        }
        log.info("Retrieved {} rows of production data  in {}ms", rowCount, System.currentTimeMillis()-before);

        if(!result.isEmpty()) {
            Map<Integer, List<ProductionData>> resultMap = mappify(result);
            addStroinfo(vehicles, ids, resultMap);
            addStromengde(vehicles, ids, resultMap);
        }
        return result;
    }

    private Map<Integer, List<ProductionData>> mappify(List<ProductionData> result) {
        Map<Integer, List<ProductionData>> resultMap = new HashMap<>();
        result.forEach(r -> {
                    if (!resultMap.containsKey(r.getTiltakId())) {
                        List<ProductionData> list = new ArrayList<ProductionData>();
                        resultMap.put(r.getTiltakId(), list);
                    }
                    resultMap.get(r.getTiltakId()).add(r);
                }
        );
        return resultMap;
    }

    private ProductionData map(ResultSet rs, List<Vehicle> vehicles, Set<Integer> ids) throws SQLException {
        Integer prodStretchId = rs.getInt("PS_ID");
        Date fromDate = rs.getTimestamp("FRA_DATO_TID");
        Date toDate = rs.getTimestamp("TIL_DATO_TID");
        double drivenKm = rs.getDouble("PS_KJORT_KM");
        double drivenKmRode = rs.getDouble("KJORT_KM");
        Integer vehicleId = rs.getInt("KJORETOY_ID");
        Integer rodeId = rs.getInt("RODE_ID");
        Integer tiltakId = rs.getInt("TILTAK_ID");
        Integer productionType = rs.getInt("PRODTYPE_ID");
        Integer grittingProductId = rs.getInt("STROPRODUKT_ID");
        String vehicleName = getVehicleName(vehicles, vehicleId);
        Integer county = rs.getInt("FYLKESNUMMER");
        String category = rs.getString("VEIKATEGORI");
        String status = rs.getString("VEISTATUS");
        String winterMaintenanceClass = rs.getString("VINTERDRIFTKLASSE");

        if(vehicleName==null) {
            log.error("Unable to find name for vehicle with id {}", vehicleId);
        }

        ids.add(tiltakId);
        rowCount++;
        return new ProductionData(prodStretchId, fromDate, toDate, drivenKm, drivenKmRode, rodeId, vehicleId, vehicleName, tiltakId, productionType, grittingProductId, county, category + status, winterMaintenanceClass);
    }

    private String getVehicleName(List<Vehicle> vehicles, long id) {
        for(Vehicle vehicle : vehicles) {
            if(vehicle.vehicleId==id) {
                return vehicle.name;
            }
        }
        return "";
    }

    public List<Vehicle> getVehicles() {
        long before = System.currentTimeMillis();

        String query = "select kv.kjoretoy_id, kv.dfu_id, kv.eksternt_navn, l.navn " +
                "from kjoretoy_v kv join kjoretoy k on kv.kjoretoy_id=k.id join leverandor l on l.nr=k.leverandor_nr " +
                "where kontrakt_id="+params.getKontrakt().getId();
        List<Vehicle> vehicles = new ArrayList<>();

        Connection conn = em.unwrap(Connection.class);
        try(Statement st = conn.createStatement()) {
            st.setFetchSize(10000);
            try (ResultSet rs = st.executeQuery(query)) {
                while (rs.next()) {
                    int id = rs.getInt("KJORETOY_ID");
                    int dfuId = rs.getInt("DFU_ID");
                    String name = rs.getString("EKSTERNT_NAVN");
                    String supplier = rs.getString("NAVN");
                    vehicles.add(new Vehicle(id, dfuId, name, supplier));
                }
            }
        } catch(SQLException e) {
            log.error("Unable to fetch vehicles for contract={}", params.getKontrakt().getId());
        }
        log.info("Finished fetching {} vehicles in {}ms", vehicles.size(), System.currentTimeMillis()-before);
        return vehicles;
    }

    /**
     * Returns a map from vehicle id to list of rodeIds the vehicle is connected to
     * @return A map from vehicle id to
     */
    public List<OutsideTripHelper.RodeConnection> getRodeConnections() {
        long before = System.currentTimeMillis();
        List<OutsideTripHelper.RodeConnection> result = new ArrayList<>();
        String query = "select kjoretoy_id, rode_id, fra_tidspunkt, til_tidspunkt from kjoretoy_rode where rode_id in (select id from rode where kontrakt_id=?) and rodetilknytningstype_id=?";

        Connection conn = em.unwrap(Connection.class);
        try(PreparedStatement st = conn.prepareStatement(query)) {
            st.setLong(1, params.getKontrakt().getId());
            st.setInt(2, PLOUGHING_RODE_CONNECTION_ID);
            st.setFetchSize(10000);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    long vehicleId = rs.getLong("KJORETOY_ID");
                    long rodeId = rs.getLong("RODE_ID");
                    Date fromDate = rs.getTimestamp("FRA_TIDSPUNKT");
                    Date toDate = rs.getTimestamp("TIL_TIDSPUNKT");

                    result.add(new OutsideTripHelper.RodeConnection(vehicleId, rodeId, fromDate, toDate));
                }
            }
        } catch(SQLException e) {
            log.error("Unable to fetch vehicles for contract={}", params.getKontrakt().getId());
        }

        log.info("Finished fetching {} connections in {}ms", result.size(), System.currentTimeMillis()-before);
        return result;
    }

    public List<Trip> getTrips(List<Vehicle> vehicles) {
        long before = System.currentTimeMillis();
        Query query = em.createNamedQuery(FIND_BY_VEHICLES_DATE);
        query.setParameter("vehicles", vehicles.stream().map(k->k.vehicleId).collect(Collectors.toList()));
        query.setParameter("startTime", fromDate);
        query.setParameter("endTime", toDate);

        List<Trip> result = query.getResultList();
        List<Trip> trips = result.stream().filter(t -> t.getProcessStatus().getLevel() >= ELIGIBLE_TRIP_STATUS_LEVEL).collect(Collectors.toList());
        log.info("Finished fetching {} trips in {}ms", trips.size(), System.currentTimeMillis()-before);
        return trips;
    }

    public List<Rode> getRodes() {
        Query query = em.createNamedQuery(Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE);
        query.setParameter("kontraktId", params.getKontrakt().getId());
        query.setParameter("rodetypeId", PLOUGHING_RODE_ID);

        return query.getResultList();
    }

    private <T> void addConcatenatedList(Collection<T> objects, StringBuilder sb, Function<T, Long> idFunction) {
        String separator;
        separator = "";
        for(T object : objects) {
            sb.append(separator).append(idFunction.apply(object));
            separator = ",";
        }
    }

    private void addStromengde(List<Vehicle> vehicles, Set<Integer> ids, Map<Integer, List<ProductionData>> productionData) {
        long before = System.currentTimeMillis();
        rowCount = 0;

        StringBuilder query = new StringBuilder();
        query.append("select tiltak_id, sm.stromengde amount ");
        query.append("from tiltak tt join stromengde sm on (tt.id=sm.tiltak_id) ");
        query.append("where tt.id between ? and ? and tt.kjoretoy_id in (");
        addConcatenatedList(vehicles, query, v->(long)v.vehicleId);
        query.append(")");

        Consumer<ResultSet> rowHandler = rs -> {
            try {
                int id = rs.getInt("tiltak_id");
                double amount = rs.getDouble("amount");
                if(productionData.containsKey(id)) {
                    for(ProductionData data : productionData.get(id)) {
                        data.setAmount(data.getAmount() + amount);
                    }
                }
                rowCount++;
            } catch(SQLException e) {
                log.error("Unable to add stromengde", e);
            }
        };

        int min = Collections.min(ids);
        int max = Collections.max(ids);

        execute(query.toString(), min, max, rowHandler);
        log.info("Finished fetching {} stromengde-rows with ids between {} and {} in {} ms", rowCount, min, max, System.currentTimeMillis()-before);
    }

    private void addStroinfo(List<Vehicle> vehicles, Set<Integer> ids, Map<Integer, List<ProductionData>> productionData) {
        long before = System.currentTimeMillis();
        rowCount = 0;
        StringBuilder query = new StringBuilder();
        query.append("select si.tiltak_id, si.kg_tort_fra_losning, si.liter_losning_totalt, si.liter_losning_res2, si.kg_losning ");
        query.append("from tiltak tt join stroinfo si on (tt.id=si.tiltak_id) ");
        query.append("where tt.id between ? and ? and tt.kjoretoy_id in (");
        addConcatenatedList(vehicles, query, v->(long)v.vehicleId);
        query.append(")");


        Consumer<ResultSet> rowHandler = rs -> {
            try {
                int id = rs.getInt("tiltak_id");
                double dryFromSolution = rs.getDouble("kg_tort_fra_losning");
                double solutionLiter = rs.getDouble("liter_losning_totalt");
                double solutionRes2 = rs.getDouble("liter_losning_res2");
                double solutionKg = rs.getDouble("kg_losning");

                if (productionData.containsKey(id)) {
                    for(ProductionData data : productionData.get(id)) {
                        data.setDryFromSolution(dryFromSolution);
                        data.setSolutionLiter(solutionLiter);
                        data.setSolutionRes2(solutionRes2);
                        data.setSolutionKg(solutionKg);
                    }
                }
                rowCount++;
            } catch(SQLException e) {
                log.error("Unable to add stroinfo", e);
            }
        };

        int min = Collections.min(ids);
        int max = Collections.max(ids);

        execute(query.toString(), min, max, rowHandler);
        log.info("Finished fetching {} stroinfo-objects with ids between {} and {} in {} ms", rowCount, min, max, System.currentTimeMillis()-before);
    }

    private void execute(String query, int min, int max, Consumer<ResultSet> rowHandler) {
        Connection conn = em.unwrap(Connection.class);
        try(PreparedStatement st = conn.prepareStatement(query)) {
            st.setInt(1, min);
            st.setInt(2, max);
            st.setFetchSize(10000);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    rowHandler.accept(rs);
                }
            }
        } catch(SQLException e) {
            log.error("Unable to fetch production data for contract={}, fromDate={}, toDate={}", params.getKontrakt().getId(), fromDate, toDate);
        }
    }

    public List<Prodtype> getProdtypes() {
        long before = System.currentTimeMillis();
        Query query = em.createNamedQuery(Prodtype.FIND_ALL);
        List results = query.getResultList();
        log.info("Fetched {} prodtypes in {}ms", results.size(), System.currentTimeMillis()-before);
        return results;
    }

    static class DataKey {
        private final int vehicleId;
        private final int rodeId;
        private final int county;
        private final String category;
        private final String winterClass;

        public DataKey(ProductionData data) {
            this.vehicleId = data.getVehicleId();
            this.rodeId = data.getRodeId();
            this.county = data.getCounty();
            this.category = data.getCategory();
            this.winterClass = data.getWinterMaintenanceClass();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DataKey dataKey = (DataKey) o;

            if (vehicleId != dataKey.vehicleId) return false;
            if (rodeId != dataKey.rodeId) return false;
            if (county != dataKey.county) return false;
            if (category != null ? !category.equals(dataKey.category) : dataKey.category != null) return false;
            return winterClass != null ? winterClass.equals(dataKey.winterClass) : dataKey.winterClass == null;
        }

        @Override
        public int hashCode() {
            int result = vehicleId;
            result = 31 * result + rodeId;
            result = 31 * result + county;
            result = 31 * result + (category != null ? category.hashCode() : 0);
            result = 31 * result + (winterClass != null ? winterClass.hashCode() : 0);
            return result;
        }
    }

    static class Vehicle {
        public final int vehicleId;
        public final int dfuId;
        public final String name;
        public final String supplier;

        public Vehicle(int vehicleId, int dfuId, String name, String supplier) {
            this.vehicleId = vehicleId;
            this.dfuId = dfuId;
            this.name = name;
            this.supplier = supplier;
        }
    }
}
