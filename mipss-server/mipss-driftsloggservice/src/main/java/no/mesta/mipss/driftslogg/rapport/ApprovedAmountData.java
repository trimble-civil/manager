package no.mesta.mipss.driftslogg.rapport;

import java.io.Serializable;

public class ApprovedAmountData implements Serializable {

	private Long activityId;
	private Integer kjoretoyId;
	private String kjoretoynavn;
	private boolean logged;
	private Integer rodeId;
	private String rodenavn;
	private String vinterdriftklasse;
	private Integer fylke;
	private String vegtype;
	private String fromDateTime;
	private String toDateTime;
	private Double km;
	private String leverandor;
	private Double broytingKm;
	private Double sideplogKm;
	private Double tungHovlingKm;
	private Double tungHovlingTimer;
	private Double intensjonKm;
	private Double stroingTimer;
	private Double torrsaltTonn;
	private Double slurryTonn;
	private Double saltlosningKubikk;
	private Double befuktetSaltTonn;
	private Double sandSaltTonn;
	private Double renSandTonn;
	private Double fastsandTonn;
	private String intensjonFormal;
	private Double distanceDriven;
	private Double r12Planing;
	private Double r12SidePlough;
	private Double r12HeavyPlaning;
	private Double amountDryGritted;
	private Double amountWetGritted;
	private Double loggedAmountDryGritted;
	private Double loggedAmountWetGritted;
	private String onPlowingRode;
	private Double actualDuration;
	private String prodType;

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public Integer getKjoretoyId() {
		return kjoretoyId;
	}

	public void setKjoretoyId(Integer kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public String getKjoretoynavn() {
		return kjoretoynavn;
	}

	public void setKjoretoynavn(String kjoretoynavn) {
		this.kjoretoynavn = kjoretoynavn;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public Integer getRodeId() {
		return rodeId;
	}

	public void setRodeId(Integer rodeId) {
		this.rodeId = rodeId;
	}

	public String getRodenavn() {
		return rodenavn;
	}

	public void setRodenavn(String rodenavn) {
		this.rodenavn = rodenavn;
	}

	public String getVinterdriftklasse() {
		return vinterdriftklasse;
	}

	public void setVinterdriftklasse(String vinterdriftklasse) {
		this.vinterdriftklasse = vinterdriftklasse;
	}

	public Integer getFylke() {
		return fylke;
	}

	public void setFylke(Integer fylke) {
		this.fylke = fylke;
	}

	public String getVegtype() {
		return vegtype;
	}

	public void setVegtype(String vegtype) {
		this.vegtype = vegtype;
	}

	public String getFromDateTime() {
		return fromDateTime;
	}

	public void setFromDateTime(String fromDateTime) {
		this.fromDateTime = fromDateTime;
	}

	public String getToDateTime() {
		return toDateTime;
	}

	public void setToDateTime(String toDateTime) {
		this.toDateTime = toDateTime;
	}

	public Double getKm() {
		return km;
	}

	public void setKm(Double km) {
		this.km = km;
	}

	public String getLeverandor() {
		return leverandor;
	}

	public void setLeverandor(String leverandor) {
		this.leverandor = leverandor;
	}

	public Double getBroytingKm() {
		return broytingKm;
	}

	public void setBroytingKm(Double broytingKm) {
		this.broytingKm = broytingKm;
	}

	public void addBroytingKm(Double broytingKm) {
		if (this.broytingKm != null && broytingKm != null) {
			this.broytingKm += broytingKm;
		} else if (this.broytingKm == null) {
			this.broytingKm = broytingKm;
		}
	}

	public Double getSideplogKm() {
		return sideplogKm;
	}

	public void setSideplogKm(Double sideplogKm) {
		this.sideplogKm = sideplogKm;
	}

	public void addSideplogKm(Double sideplogKm) {
		if (this.sideplogKm != null && sideplogKm != null) {
			this.sideplogKm += sideplogKm;
		} else if (this.sideplogKm == null) {
			this.sideplogKm = sideplogKm;
		}
	}

	public Double getTungHovlingKm() {
		return tungHovlingKm;
	}

	public void setTungHovlingKm(Double tungHovlingKm) {
		this.tungHovlingKm = tungHovlingKm;
	}

	public void addTungHovlingKm(Double tungHovlingKm) {
		if (this.tungHovlingKm != null && tungHovlingKm != null) {
			this.tungHovlingKm += tungHovlingKm;
		} else if (this.tungHovlingKm == null) {
			this.tungHovlingKm = tungHovlingKm;
		}
	}

	public Double getTungHovlingTimer() {
		return tungHovlingTimer;
	}

	public void addTungHovlingTimer(Double tungHovlingTimer) {
		if (this.tungHovlingTimer != null && tungHovlingTimer != null) {
			this.tungHovlingTimer += tungHovlingTimer;
		} else if (this.tungHovlingTimer == null) {
			this.tungHovlingTimer = tungHovlingTimer;
		}
	}

	public Double getIntensjonKm() {
		return intensjonKm;
	}

	public void setIntensjonKm(Double intensjonKm) {
		this.intensjonKm = intensjonKm;
	}

	public void addIntensjonKm(Double intensjonKm) {
		if (this.intensjonKm != null && intensjonKm != null) {
			this.intensjonKm += intensjonKm;
		} else if (this.intensjonKm == null) {
			this.intensjonKm = intensjonKm;
		}
	}

	public Double getStroingTimer() {
		return stroingTimer;
	}

	public void addStroingTimer(Double stroingTimer) {
		if (this.stroingTimer != null && stroingTimer != null) {
			this.stroingTimer += stroingTimer;
		} else if (this.stroingTimer == null) {
			this.stroingTimer = stroingTimer;
		}
	}

	public Double getTorrsaltTonn() {
		return torrsaltTonn;
	}

	public void setTorrsaltTonn(Double torrsaltTonn) {
		this.torrsaltTonn = torrsaltTonn;
	}

	public void addTorrsaltTonn(Double torrsaltTonn) {
		if (this.torrsaltTonn != null && torrsaltTonn != null) {
			this.torrsaltTonn += torrsaltTonn;
		} else if (this.torrsaltTonn == null) {
			this.torrsaltTonn = torrsaltTonn;
		}
	}

	public Double getSlurryTonn() {
		return slurryTonn;
	}

	public void setSlurryTonn(Double slurryTonn) {
		this.slurryTonn = slurryTonn;
	}

	public void addSlurryTonn(Double slurryTonn) {
		if (this.slurryTonn != null && slurryTonn != null) {
			this.slurryTonn += slurryTonn;
		} else if (this.slurryTonn == null) {
			this.slurryTonn = slurryTonn;
		}
	}

	public Double getSaltlosningKubikk() {
		return saltlosningKubikk;
	}

	public void setSaltlosningKubikk(Double saltlosningKubikk) {
		this.saltlosningKubikk = saltlosningKubikk;
	}

	public void addSaltlosningKubikk(Double saltlosningKubikk) {
		if (this.saltlosningKubikk != null && saltlosningKubikk != null) {
			this.saltlosningKubikk += saltlosningKubikk;
		} else if (this.saltlosningKubikk == null) {
			this.saltlosningKubikk = saltlosningKubikk;
		}
	}

	public Double getBefuktetSaltTonn() {
		return befuktetSaltTonn;
	}

	public void setBefuktetSaltTonn(Double befuktetSaltTonn) {
		this.befuktetSaltTonn = befuktetSaltTonn;
	}

	public void addBefuktetSaltTonn(Double befuktetSaltTonn) {
		if (this.befuktetSaltTonn != null && befuktetSaltTonn != null) {
			this.befuktetSaltTonn += befuktetSaltTonn;
		} else if (this.befuktetSaltTonn == null) {
			this.befuktetSaltTonn = befuktetSaltTonn;
		}
	}

	public Double getSandSaltTonn() {
		return sandSaltTonn;
	}

	public void setSandSaltTonn(Double sandSaltTonn) {
		this.sandSaltTonn = sandSaltTonn;
	}

	public void addSandSaltTonn(Double sandSaltTonn) {
		if (this.sandSaltTonn != null && sandSaltTonn != null) {
			this.sandSaltTonn += sandSaltTonn;
		} else if (this.sandSaltTonn == null) {
			this.sandSaltTonn = sandSaltTonn;
		}
	}

	public Double getRenSandTonn() {
		return renSandTonn;
	}

	public void setRenSandTonn(Double renSandTonn) {
		this.renSandTonn = renSandTonn;
	}

	public void addRenSandTonn(Double renSandTonn) {
		if (this.renSandTonn != null && renSandTonn != null) {
			this.renSandTonn += renSandTonn;
		} else if (this.renSandTonn == null) {
			this.renSandTonn = renSandTonn;
		}
	}

	public Double getFastsandTonn() {
		return fastsandTonn;
	}

	public void setFastsandTonn(Double fastsandTonn) {
		this.fastsandTonn = fastsandTonn;
	}

	public void addFastsandTonn(Double fastsandTonn) {
		if (this.fastsandTonn != null && fastsandTonn != null) {
			this.fastsandTonn += fastsandTonn;
		} else if (this.fastsandTonn == null) {
			this.fastsandTonn = fastsandTonn;
		}
	}

	public String getIntensjonFormal() {
		return intensjonFormal;
	}

	public void setIntensjonFormal(String intensjonFormal) {
		this.intensjonFormal = intensjonFormal;
	}

	public Double getDistanceDriven() {
		return distanceDriven;
	}

	public void setDistanceDriven(Double distanceDriven) {
		this.distanceDriven = distanceDriven;
	}

	public void addDistanceDriven(Double distanceDriven) {
		if (this.distanceDriven != null && distanceDriven != null) {
			this.distanceDriven += distanceDriven;
		} else if (this.distanceDriven == null) {
			this.distanceDriven = distanceDriven;
		}
	}

	public Double getR12Planing() {
		return r12Planing;
	}

	public void setR12Planing(Double r12Planing) {
		this.r12Planing = r12Planing;
	}

	public Double getR12SidePlough() {
		return r12SidePlough;
	}

	public void setR12SidePlough(Double r12SidePlough) {
		this.r12SidePlough = r12SidePlough;
	}

	public Double getR12HeavyPlaning() {
		return r12HeavyPlaning;
	}

	public void setR12HeavyPlaning(Double r12HeavyPlaning) {
		this.r12HeavyPlaning = r12HeavyPlaning;
	}

	public Double getAmountDryGritted() {
		return amountDryGritted;
	}

	public void setAmountDryGritted(Double amountDryGritted) {
		this.amountDryGritted = amountDryGritted;
	}

	public Double getAmountWetGritted() {
		return amountWetGritted;
	}

	public void setAmountWetGritted(Double amountWetGritted) {
		this.amountWetGritted = amountWetGritted;
	}

	public Double getLoggedAmountDryGritted() {
		return loggedAmountDryGritted;
	}

	public void setLoggedAmountDryGritted(Double loggedAmountDryGritted) {
		this.loggedAmountDryGritted = loggedAmountDryGritted;
	}

	public void addLoggedAmountDryGritted(Double loggedAmountDryGritted) {
		if (this.loggedAmountDryGritted != null && loggedAmountDryGritted != null) {
			this.loggedAmountDryGritted += loggedAmountDryGritted;
		} else if (this.loggedAmountDryGritted == null) {
			this.loggedAmountDryGritted = loggedAmountDryGritted;
		}
	}

	public Double getLoggedAmountWetGritted() {
		return loggedAmountWetGritted;
	}

	public void setLoggedAmountWetGritted(Double loggedAmountWetGritted) {
		this.loggedAmountWetGritted = loggedAmountWetGritted;
	}

	public void addLoggedAmountWetGritted(Double loggedAmountWetGritted) {
		if (this.loggedAmountWetGritted != null && loggedAmountWetGritted != null) {
			this.loggedAmountWetGritted += loggedAmountWetGritted;
		} else if (this.loggedAmountWetGritted == null) {
			this.loggedAmountWetGritted = loggedAmountWetGritted;
		}
	}

	public Double getActualDuration() {
		return actualDuration;
	}

	public void setActualDuration(Double actualDuration) {
		this.actualDuration = actualDuration;
	}

	public void addActualDuration(Double actualDuration) {
		if (this.actualDuration != null && actualDuration != null) {
			this.actualDuration += actualDuration;
		} else if (this.actualDuration == null) {
			this.actualDuration = actualDuration;
		}
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public void addApprovedAmountsFrom(ApprovedAmountData data) {
		this.addBroytingKm(data.getBroytingKm());
		this.addSideplogKm(data.getSideplogKm());
		this.addTungHovlingKm(data.getTungHovlingKm());
		this.addTungHovlingTimer(data.getTungHovlingTimer());
		this.addIntensjonKm(data.getIntensjonKm());

		this.addDistanceDriven(data.getDistanceDriven());
		this.addLoggedAmountDryGritted(data.getLoggedAmountDryGritted());
		this.addLoggedAmountWetGritted(data.getLoggedAmountWetGritted());

		this.addStroingTimer(data.getStroingTimer());
		this.addTorrsaltTonn(data.getTorrsaltTonn());
		this.addSlurryTonn(data.getSlurryTonn());
		this.addSaltlosningKubikk(data.getSaltlosningKubikk());
		this.addBefuktetSaltTonn(data.getBefuktetSaltTonn());
		this.addSandSaltTonn(data.getSandSaltTonn());
		this.addRenSandTonn(data.getRenSandTonn());
		this.addFastsandTonn(data.getFastsandTonn());
		this.addActualDuration(data.getActualDuration());
	}

	public String getOnPlowingRode() {
		return onPlowingRode;
	}

	public void setOnPlowingRode(String onPlowingRode) {
		this.onPlowingRode = onPlowingRode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ApprovedAmountData that = (ApprovedAmountData) o;

		if (!activityId.equals(that.activityId)) return false;
		if (rodeId != null ? !rodeId.equals(that.rodeId) : that.rodeId != null) return false;
		if (rodenavn != null ? !rodenavn.equals(that.rodenavn) : that.rodenavn != null) return false;
		if (vinterdriftklasse != null ? !vinterdriftklasse.equals(that.vinterdriftklasse) : that.vinterdriftklasse != null)
			return false;
		if (fylke != null ? !fylke.equals(that.fylke) : that.fylke != null) return false;
		if (vegtype != null ? !vegtype.equals(that.vegtype) : that.vegtype != null) return false;
		return leverandor.equals(that.leverandor);

	}

	@Override
	public int hashCode() {
		int result = activityId.hashCode();
		result = 31 * result + (rodeId != null ? rodeId.hashCode() : 0);
		result = 31 * result + (rodenavn != null ? rodenavn.hashCode() : 0);
		result = 31 * result + (vinterdriftklasse != null ? vinterdriftklasse.hashCode() : 0);
		result = 31 * result + (fylke != null ? fylke.hashCode() : 0);
		result = 31 * result + (vegtype != null ? vegtype.hashCode() : 0);
		result = 31 * result + leverandor.hashCode();
		return result;
	}

}