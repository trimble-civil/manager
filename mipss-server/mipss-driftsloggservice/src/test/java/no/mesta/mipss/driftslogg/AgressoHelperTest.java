package no.mesta.mipss.driftslogg;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;
import no.mesta.mipss.driftslogg.util.AgressoHelper;
import no.mesta.mipss.driftslogg.util.AgressoTransferHolder;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.driftslogg.Periode;
import no.mesta.mipss.persistence.driftslogg.Trip;

public class AgressoHelperTest extends TestCase {
	List<Aktivitet> aktiviteter = new ArrayList<Aktivitet>();
	
	public void setUp(){
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		try {
			Periode period = new Periode();
			period.setLevkontraktIdent("LK-101010");
			Levkontrakt lk = new Levkontrakt();
			lk.setLeverandorNr("10000");
			period.setLevkontrakt(lk);
			
			aktiviteter.add(create("Km", 10d, null, f.parse("01.01.2012 00:00"), f.parse("01.01.2012 12:12")));
			aktiviteter.add(create("Km", 10d, null, f.parse("01.02.2012 00:00"), f.parse("01.02.2012 12:12")));
			aktiviteter.add(create("Km", 10d, null, f.parse("01.02.2012 12:00"), f.parse("01.02.2012 13:12")));
			aktiviteter.add(create("Km", 10d, null, f.parse("01.03.2012 12:00"), f.parse("01.03.2012 13:12")));

			Trip trip = new Trip();
			trip.setPeriod(period);
			for (Aktivitet activity:aktiviteter){
				activity.setTrip(trip);
			}
			aktiviteter.get(0).setSendtAgresso(Boolean.TRUE);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private Aktivitet create(String enhet, Double km, Double mengde, Date fraDato, Date tilDato){
		Aktivitet a = new Aktivitet();
		a.setEnhet(enhet);
		a.setMeldtKm(km);
		a.setMeldtMengde(mengde);
		a.setFraDatoTid(fraDato);
		a.setTilDatoTid(tilDato);
		return a;
	}
	
	public void testbeholdAktiviteterForDenneMnd(){
		AgressoHelper h = new AgressoHelper(null);
		List<Aktivitet> akt = h.beholdAktiviteterForDenneMnd(aktiviteter, Long.valueOf(Calendar.FEBRUARY), Long.valueOf(2012));
		assertEquals(2, akt.size());
	}
	
	public void testBeholdAktiviteterForDenneMndIngenSkalBeholdesPgaTidligereSendtAgresso(){
		AgressoHelper h = new AgressoHelper(null);
		List<Aktivitet> akt = h.beholdAktiviteterForDenneMnd(aktiviteter, Long.valueOf(Calendar.JANUARY), Long.valueOf(2012));
		assertEquals(0, akt.size());//skal v�re 0 pga sendt_agresso_flagg

	}
	
	public void testBeholdAktiviteterForDenneMndIngenSkalBeholdesPgaFeilDato(){
		AgressoHelper h = new AgressoHelper(null);
		List<Aktivitet> akt = h.beholdAktiviteterForDenneMnd(aktiviteter, Long.valueOf(Calendar.APRIL), Long.valueOf(2012));
		assertEquals(0, akt.size());//skal v�re 0 pga at ingen aktiviteter finnes for APRIL
	}
	
	public void testCreateMipssInputFromAktiviteter(){
		AgressoHelper h = new AgressoHelper(null);
		AgressoTransferHolder th = new AgressoTransferHolder();
		th.setOrderId("1234");
		List<MipssInput> mipssinput = h.createMipssInputFromAktiviteter(aktiviteter, th, "test@mesta.no");
		assertEquals(3, mipssinput.size());
	}
}
