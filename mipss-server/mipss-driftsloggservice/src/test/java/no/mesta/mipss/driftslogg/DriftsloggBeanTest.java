package no.mesta.mipss.driftslogg;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.driftslogg.*;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.*;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class DriftsloggBeanTest extends TestCase {

    final String VENDOR_CONTRACT_IDENT = "1702-GS_R�RV";
    final String VENDOR_CONTRACT_IDENT_2 = "1702-10_PETT";
    final String VENDOR_NAME = "EDGAR SETTN�Y AS";
    final String VENDOR_NAME_2 = "JAN ERLING PETTERSEN";
    final String VENDOR_ID = "57240";
    final String VENDOR_ID_2 = "57241";
    final Long PERIOD_ID = 26922L;
    final Long PERIOD_ID_2 = 33404L;
    final Double ACTIVITY1_DISTANCE = 3.0;
    final Double ACTIVITY2_DISTANCE = 7.0;
    final Date FROM_DATE_OUTSIDE = getDate(2017, 1, 17, 20, 38);
    final Date FROM_DATE_INSIDE = getDate(2017, 2, 17, 20, 38);
    final Date TO_DATE_INSIDE = getDate(2017, 2, 10, 15, 59);
    final Date TO_DATE_OUTSIDE = getDate(2017, 3, 10, 15, 59);
    EntityManager em;

    final String ARTICLE_FASTPRIS_BROYTING_STROING = "FASTPRIS BR�YTING / STR�ING GS-VEGER / FORTAU";
    final String ARTICLE_STROING_MED_SAND = "STR�ING MED SAND";

    DriftsloggBean driftsloggBean;
    Levkontrakt lev1, lev2;
    Periode p,p2;
    Aktivitet a,b,c,d;
    MipssInputTreeView treeView, treeView2;

    @Before
    public void setUp() {
        driftsloggBean = new DriftsloggBean();
        em = mock(EntityManager.class);
        driftsloggBean.setEntityManager(em);

        lev1 = createVendorContract(VENDOR_ID, VENDOR_CONTRACT_IDENT);
        lev2 = createVendorContract(VENDOR_ID_2, VENDOR_CONTRACT_IDENT_2);
        p = createPeriod(VENDOR_CONTRACT_IDENT, PERIOD_ID, lev1);
        p2 = createPeriod(VENDOR_CONTRACT_IDENT_2, PERIOD_ID_2, lev1);
        a = createActivity(p);
        b = createActivity2(p);
        c = createActivityVendor2(p2);
        d = createActivity2Vendor2(p2);
        treeView = createTreeView(VENDOR_NAME, VENDOR_ID, ARTICLE_FASTPRIS_BROYTING_STROING);
        treeView2 = createTreeView(VENDOR_NAME_2, VENDOR_ID_2, ARTICLE_STROING_MED_SAND);
    }

    @Test
    public void testFlagActivitiesAsProcessed_bothDatesInside_flaggedActivity() {
        List<MipssInput> activities = createMipssInputList(FROM_DATE_INSIDE, TO_DATE_INSIDE);

        driftsloggBean.flagActivitiesAsProcessed(activities, 2L, 2017L);

        assertTrue(activities.get(0).getAktivitet().getSendtAgresso());
    }

    @Test
    public void testFlagActivitiesAsProcessed_fromDateOutsideToDateInside_nonFlaggedActivity() {
        List<MipssInput> activities = createMipssInputList(FROM_DATE_OUTSIDE, TO_DATE_INSIDE);

        driftsloggBean.flagActivitiesAsProcessed(activities, 2L, 2017L);

        assertFalse(activities.get(0).getAktivitet().getSendtAgresso());
    }

    @Test
    public void testFlagActivitiesAsProcessed_fromDateInsideToDateOutside_flaggedActivity() {
        List<MipssInput> activities = createMipssInputList(FROM_DATE_INSIDE, TO_DATE_OUTSIDE);

        driftsloggBean.flagActivitiesAsProcessed(activities, 2L, 2017L);

        assertTrue(activities.get(0).getAktivitet().getSendtAgresso());
    }

    @Test
    public void testFlagActivitiesAsProcessed_bothDatesOutside_nonFlaggedActivity() {
        List<MipssInput> activities = createMipssInputList(FROM_DATE_OUTSIDE, TO_DATE_OUTSIDE);

        driftsloggBean.flagActivitiesAsProcessed(activities, 2L, 2017L);

        assertFalse(activities.get(0).getAktivitet().getSendtAgresso());
    }

    @Test
    public void testFlagActivitiesAsProcessed_bothDatesNull_nonFlaggedActivity() {
        List<MipssInput> activities = createMipssInputList(null, null);

        driftsloggBean.flagActivitiesAsProcessed(activities, 2L, 2017L);

        assertFalse(activities.get(0).getAktivitet().getSendtAgresso());
    }

    @Test
    public void testSaveProcessedActivities_oneActivityFlagged_oneActivitySaved() {
        List<MipssInput> activities = createDynamicMipssInputList(false, null, true);

        driftsloggBean.saveProcessedActivities(activities);

        verify(em, times(1)).merge(any());
    }

    @Test
    public void testSaveProcessedActivities_allActivitiesFlagged_allActivitiesSaved() {
        List<MipssInput> activities = createDynamicMipssInputList(true, true, true);

        driftsloggBean.saveProcessedActivities(activities);

        verify(em, times(3)).merge(any());
    }

    @Test
    public void testSaveProcessedActivities_noActivitiesFlagged_noActivitiesSaved() {
        List<MipssInput> activities = createDynamicMipssInputList(false, false, false);

        driftsloggBean.saveProcessedActivities(activities);

        verify(em, times(0)).merge(any());
    }

    private Date getDate(int year, int month, int date, int hour, int minute) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(year, month, date, hour, minute);
        return gc.getTime();
    }

    private ArrayList<MipssInput> createDynamicMipssInputList(Boolean... sendtAgressoFlags) {
        ArrayList<MipssInput> mipssInputs = new ArrayList<>();

        Levkontrakt l = new Levkontrakt();
        l.setLeverandorNr(null);
        Periode period = new Periode();
        period.setLevkontrakt(l);
        Trip trip = new Trip();
        trip.setPeriod(period);

        for (Boolean flag : sendtAgressoFlags) {
            Aktivitet activity = new Aktivitet();
            activity.setTrip(trip);
            activity.setSendtAgresso(flag);

            MipssInput mipssInput = new MipssInput(activity, 0L, BigInteger.ZERO, "");
            mipssInputs.add(mipssInput);
        }

        return mipssInputs;
    }

    private ArrayList<MipssInput> createMipssInputList(Date fromDate, Date toDate) {
        ArrayList<MipssInput> mipssInputs = new ArrayList<>();

        Levkontrakt l = new Levkontrakt();
        l.setLeverandorNr(null);
        Periode period = new Periode();
        period.setLevkontrakt(l);
        Trip trip = new Trip();
        trip.setPeriod(period);

        Aktivitet activity = new Aktivitet();
        activity.setSendtAgresso(Boolean.FALSE);
        activity.setFraDatoTid(fromDate);
        activity.setTilDatoTid(toDate);
        activity.setTrip(trip);

        MipssInput mipssInput = new MipssInput(activity, 0L, BigInteger.ZERO, "");
        mipssInputs.add(mipssInput);

        return mipssInputs;
    }

    private Levkontrakt createVendorContract(String vendorId, String vendorContractIdent) {
        Levkontrakt l = new Levkontrakt();
        l.setLeverandorNr(vendorId);
        l.setLevkontraktIdent(vendorContractIdent);
        return l;
    }

    private MipssInputTreeView createTreeView(String vendorName, String vendorId, String article) {
        MipssInputTreeView treeView = new MipssInputTreeView(vendorName, vendorId);
        treeView.setArtikkel(article);
        treeView.setOverforLeverandor(true);
        return treeView;
    }

    private Aktivitet createActivityVendor2(Periode period) {
        Trip trip = new Trip();
        trip.setPeriod(period);
        Aktivitet activity = new Aktivitet();
        activity.setId(354901L);
        activity.setNewEntity(false);
        activity.setTrip(trip);
        activity.setOmforentMengde(ACTIVITY1_DISTANCE);
        activity.setEnhet("TIM");
        activity.setApprovedUE(true);
        return activity;
    }

    private Aktivitet createActivity2Vendor2(Periode period) {
        Trip trip = new Trip();
        trip.setPeriod(period);
        Aktivitet activity = new Aktivitet();
        activity.setId(352907L);
        activity.setNewEntity(false);
        activity.setTrip(trip);
        activity.setOmforentMengde(ACTIVITY2_DISTANCE);
        activity.setEnhet("TON");
        activity.setApprovedUE(true);
        return activity;
    }

    private Aktivitet createActivity(Periode period) {
        Trip trip = new Trip();
        trip.setPeriod(period);
        Aktivitet activity = new Aktivitet();
        activity.setId(273729L);
        activity.setNewEntity(false);
        activity.setTrip(trip);
        activity.setOmforentMengde(ACTIVITY1_DISTANCE);
        activity.setEnhet("KM");
        activity.setApprovedUE(true);
        return activity;
    }

    private Aktivitet createActivity2(Periode period) {
        Trip trip = new Trip();
        trip.setPeriod(period);
        Aktivitet a = new Aktivitet();
        a.setId(273730L);
        a.setNewEntity(false);
        a.setTrip(trip);
        a.setOmforentMengde(ACTIVITY2_DISTANCE);
        a.setEnhet("KM");
        a.setApprovedUE(true);
        return a;
    }

    private Periode createPeriod(String levkontraktIdent, Long periodId, Levkontrakt l) {
        Periode p = new Periode();
        p.setNewEntity(false);
        p.setLevkontraktIdent(levkontraktIdent);
        p.setLevkontrakt(l);
        p.setId(periodId);
        return p;
    }
}