package no.mesta.mipss.driftslogg.rapport;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.driftslogg.DeviantActivity;
import no.mesta.mipss.persistence.driftslogg.PotentialDeviantActivity;
import org.junit.Test;

import java.util.List;

public class DeviantActivitiesHelperTest extends TestCase {

    private static final String PLANING = "Br\u00F8yting";
    private static final String HEAVY_PLANING = "Tung h\u00F8vling";
    private static final String SIDE_PLOUGH = "Sideplog";
    private static final String GRITTING = "Str\u00F8ing";
    private static final String GRIT_TYPE_BRINE = "Saltl\u00F8sning";
    private static final String ILLEGAL_PROD_TYPE_ASPHALT_WORK = "Asfaltarbeid";
    private static final String ILLEGAL_PROD_TYPE_OTHER = "Annet";
    private static final String MALFORMED_PROD_TYPE_GRITTING = "Str�ing";
    private static final String MALFORMED_GRIT_TYPE_BRINE = "Sultl�sning";

    private DeviantActivitiesHelper helper;
    private PotentialDeviantActivity potential;
    private DeviantActivity deviant;

    public void setUp() {
        helper = new DeviantActivitiesHelper();
        potential = new PotentialDeviantActivity();
        potential.setProductionOutsideRode(false);
        deviant = new DeviantActivity();
    }

    @Test
    public void testSplitDeviant_prodTypeNoGrit_twoDeviants() {
        potential.setProductionType(joinProdTypes(null, PLANING, SIDE_PLOUGH));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(2, deviants.size());
    }

    @Test
    public void testSplitDeviant_prodTypeGritNoGritType_oneDeviant() {
        potential.setProductionType(GRITTING);

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(1, deviants.size());
        assertTrue(deviants.get(0).getGritType() == null);
    }

    @Test
    public void testSplitDeviant_prodTypeGritMissingGritType_oneDeviant() {
        potential.setProductionType(joinProdTypes(null, GRITTING));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(1, deviants.size());
        assertTrue(deviants.get(0).getGritType() == null);
    }

    @Test
    public void testSplitDeviant_prodTypeGrit_oneDeviantParsedGritType() {
        potential.setProductionType(joinProdTypes(GRIT_TYPE_BRINE, GRITTING));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(1, deviants.size());
        assertTrue(deviants.get(0).getGritType().equals(GRIT_TYPE_BRINE));
    }

    @Test
    public void testSplitDeviant_prodTypeGritProductionOutsideRode_noDeviant() {
        potential.setProductionType(joinProdTypes(GRIT_TYPE_BRINE, GRITTING));
        potential.setProductionOutsideRode(true);

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(0, deviants.size());
    }

    @Test
    public void testSplitDeviant_complexProdTypeWithGrit_threeDeviantsParsedGritType() {
        potential.setProductionType(joinProdTypes(GRIT_TYPE_BRINE, HEAVY_PLANING, SIDE_PLOUGH, GRITTING));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(3, deviants.size());
        assertTrue(deviants.get(2).getGritType().equals(GRIT_TYPE_BRINE));
    }

    @Test
    public void testSplitDeviant_complexProdTypeWithGritAndIllegalTypes_threeDeviantsParsedGritType() {
        potential.setProductionType(joinProdTypes(GRIT_TYPE_BRINE, ILLEGAL_PROD_TYPE_ASPHALT_WORK, HEAVY_PLANING,
                SIDE_PLOUGH, GRITTING, ILLEGAL_PROD_TYPE_OTHER));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(3, deviants.size());
        assertTrue(deviants.get(2).getGritType().equals(GRIT_TYPE_BRINE));
    }

    @Test
    public void testSplitDeviant_malformedProdType_noDeviant() {
        potential.setProductionType(MALFORMED_PROD_TYPE_GRITTING);

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(0, deviants.size());
    }

    @Test
    public void testSplitDeviant_malformedGritType_oneDeviant() {
        potential.setProductionType(joinProdTypes(MALFORMED_GRIT_TYPE_BRINE, GRITTING));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(1, deviants.size());
    }

    @Test
    public void testSplitDeviant_malformedProdTypeAndGritType_noDeviant() {
        potential.setProductionType(joinProdTypes(MALFORMED_GRIT_TYPE_BRINE, MALFORMED_PROD_TYPE_GRITTING));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(0, deviants.size());
    }

    @Test
    public void testSplitDeviant_complexProdTypeWithGritAndIllegalAndMalformedTypes_twoDeviants() {
        potential.setProductionType(joinProdTypes(GRIT_TYPE_BRINE, ILLEGAL_PROD_TYPE_ASPHALT_WORK, HEAVY_PLANING,
                SIDE_PLOUGH, ILLEGAL_PROD_TYPE_OTHER, MALFORMED_PROD_TYPE_GRITTING));

        List<DeviantActivity> deviants = helper.splitDeviant(potential, deviant);

        assertEquals(2, deviants.size());
    }

    private String joinProdTypes(String gritType, String... prodTypes) {
        StringBuilder sb = new StringBuilder();

        String separator = "";
        for (String prodType : prodTypes) {
            sb.append(separator);
            sb.append(prodType);
            separator = "+";
        }

        if (gritType != null) {
            sb.append(" : ");
            sb.append(gritType);
        }

        return sb.toString();
    }

}