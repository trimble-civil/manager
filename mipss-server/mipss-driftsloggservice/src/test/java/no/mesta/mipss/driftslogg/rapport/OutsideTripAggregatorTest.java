package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static no.mesta.mipss.driftslogg.rapport.OutsideTripHelperTest.getDate;
import static no.mesta.mipss.driftslogg.rapport.OutsideTripHelperTest.getProductionData;
import static no.mesta.mipss.driftslogg.rapport.OutsideTripHelperTest.getTrip;
import static org.junit.Assert.*;

public class OutsideTripAggregatorTest {

    private OutsideTripAggregator aggregator;
    private List<Prodtype> prodtypes;

    @Before
    public void setUp() throws Exception {
        aggregator = new OutsideTripAggregator();

        prodtypes = getProdtypes();
    }

    @Test
    public void canFindDurationForOneHour() throws Exception {
        Date fromDate = getDate(2017, 1, 1, 0);
        Date toDate = getDate(2017, 1, 1, 1);
        ProductionData data = new ProductionData(1, fromDate, toDate, 0, 0, 0, 0, null, 0, 0, 0, 0, null, null);
        assertEquals(1, aggregator.getDurationInHours(data), 0);
    }


    @Test
    public void canGroupDataTogether() throws Exception {
        Date fromDate = getDate(2017,1,1,0);
        Date midDate = getDate(2017,1,1,1);
        Date toDate = getDate(2017,1,1,2);

        List<ProductionData> data = new ArrayList<>();
        data.add(getProductionData(1, fromDate, midDate, 1, 1, 1, 1, 1));
        data.add(getProductionData(2, midDate, toDate, 1, 1, 1, 1, 1));

        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> dataKeyListMap = aggregator.groupByVehicleRode(data);
        assertEquals(1, dataKeyListMap.size());
    }

    @Test
    public void canSeparateTwoVehicles() throws Exception {
        Date fromDate = getDate(2017,1,1,0);
        Date midDate = getDate(2017,1,1,1);
        Date toDate = getDate(2017,1,1,2);

        List<ProductionData> data = new ArrayList<>();
        data.add(getProductionData(1, fromDate, midDate, 1, 1, 1, 1, 1));
        data.add(getProductionData(2, midDate, toDate, 1, 1, 2, 1, 1));

        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> dataKeyListMap = aggregator.groupByVehicleRode(data);
        assertEquals(2, dataKeyListMap.size());
    }

    @Test
    public void canAddPloughing() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(6, 0);
        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getKm(), 0);
        assertEquals(1, actual.getBroytingKm(), 0);
    }

    @Test
    public void canAddPlaning() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(24, 0);
        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getKm(), 0);
        assertEquals(1, actual.getTungHovlingKm(), 0);
    }

    @Test
    public void canAddDrySalt() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 1);
        data.setAmount(1);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getTorrsaltTonn(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSandWithSalt() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 2);
        data.setAmount(1);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getSandSaltTonn(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddPureSand() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 3);
        data.setAmount(1);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getRenSandTonn(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSlurry() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 4);
        data.setAmount(1);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getSlurryTonn(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSaltSolutionFromLiter() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 5);
        data.setSolutionLiter(1000);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getSaltlosningKubikk(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSaltSolutionFromRes2() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 5);
        data.setSolutionRes2(1000);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getSaltlosningKubikk(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSaltSolutionFromKg() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 5);
        data.setSolutionKg(1000);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1D/ ManagerConstants.WEIGHT_VOLUME_FACTOR, actual.getSaltlosningKubikk(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddMoistSalt() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 6);
        data.setAmount(1);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getBefuktetSaltTonn(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSolidSand() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(5, 7);
        data.setAmount(1);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getFastsandTonn(), 0);
        assertEquals(1, actual.getStroingTimer(), 0);
    }

    @Test
    public void canAddSideplough() throws Exception {
        OutsideTripData actual = new OutsideTripData(null, null, null, null, null, null, null);
        ProductionData data = getProdData(1, 0);

        aggregator.addProduction(actual, data, prodtypes);

        assertEquals(1, actual.getSideplogKm(), 0);
        assertEquals(1, actual.getBroytingKm(), 0);
    }

    @Test
    public void canGroupByTime() throws Exception {
        ProductionData data = getProdData(5, 0);

        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupedData = new HashMap<>();
        OutsideTripDataFetcher.DataKey key = new OutsideTripDataFetcher.DataKey(data);
        groupedData.put(key, Arrays.asList(data));

        List<OutsideTripData> actual = aggregator.groupByTime(groupedData, new ArrayList<>(), prodtypes);

        assertEquals(1, actual.size());
        assertEquals(getDate(2017,1,1,0), actual.get(0).getFromDate());
        assertEquals(getDate(2017,1,1,23, 59, 59), actual.get(0).getToDate());
    }

    @Test
    public void canGroupByTimeWithTripAfter() throws Exception {
        ProductionData data = getProdData(5, 0);

        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupedData = new HashMap<>();
        OutsideTripDataFetcher.DataKey key = new OutsideTripDataFetcher.DataKey(data);
        groupedData.put(key, Arrays.asList(data));

        ArrayList<Trip> trips = new ArrayList<>();
        trips.add(getTrip(getDate(2017,1,1,12), getDate(2017,1,1,13)));

        List<OutsideTripData> actual = aggregator.groupByTime(groupedData, trips, prodtypes);

        assertEquals(1, actual.size());
        assertEquals(getDate(2017,1,1,0), actual.get(0).getFromDate());
        assertEquals(getDate(2017,1,1,12), actual.get(0).getToDate());
    }

    @Test
    public void canGroupByTimeWithTripBefore() throws Exception {
        ProductionData data = new ProductionData(1, getDate(2017,1,1,1), getDate(2017,1,1,2), 1, 1, 1, 1, null, 1, 6, 0, 1, null, null);

        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupedData = new HashMap<>();
        OutsideTripDataFetcher.DataKey key = new OutsideTripDataFetcher.DataKey(data);
        groupedData.put(key, Arrays.asList(data));

        ArrayList<Trip> trips = new ArrayList<>();
        trips.add(getTrip(getDate(2017,1,1,0), getDate(2017,1,1,1)));

        List<OutsideTripData> actual = aggregator.groupByTime(groupedData, trips, prodtypes);

        assertEquals(1, actual.size());
        assertEquals(getDate(2017,1,1,1), actual.get(0).getFromDate());
        assertEquals(getDate(2017,1,1,23, 59, 59), actual.get(0).getToDate());
    }

    @Test
    public void canGroupByTimeBetweenTrips() throws Exception {
        ProductionData data = new ProductionData(1, getDate(2017,1,1,1), getDate(2017,1,1,2), 1, 1, 1, 1, null, 1, 6, 0, 1, null, null);

        Map<OutsideTripDataFetcher.DataKey, List<ProductionData>> groupedData = new HashMap<>();
        OutsideTripDataFetcher.DataKey key = new OutsideTripDataFetcher.DataKey(data);
        groupedData.put(key, Arrays.asList(data));

        ArrayList<Trip> trips = new ArrayList<>();
        trips.add(getTrip(getDate(2017,1,1,0), getDate(2017,1,1,1)));
        trips.add(getTrip(getDate(2017, 1, 1, 12), getDate(2017, 1, 1, 13)));

        List<OutsideTripData> actual = aggregator.groupByTime(groupedData, trips, prodtypes);

        assertEquals(1, actual.size());
        assertEquals(getDate(2017,1,1,1), actual.get(0).getFromDate());
        assertEquals(getDate(2017,1,1,12), actual.get(0).getToDate());
    }

    @Test(expected=IllegalArgumentException.class)
    public void throwsErrorOnUnknownProdtype() throws Exception {
        aggregator.getProdtype(prodtypes, -1);
    }

    @Test
    public void doesNotDoubleCountPloughingAndPlaning() throws Exception {
        OutsideTripData data = new OutsideTripData(1, 1, null, null, null, null, null);
        ProductionData ploughing = new ProductionData(1, getDate(2017, 1, 1, 0), getDate(2017, 1, 1, 1), 1, 1, 1, 1, null, 1, 6, 0, 0, null, null);
        ProductionData planing = new ProductionData(1, getDate(2017, 1, 1, 0), getDate(2017, 1, 1, 1), 1, 1, 1, 1, null, 2, 24, 0, 0, null, null);

        aggregator.addProduction(data, ploughing, prodtypes);
        aggregator.addProduction(data, planing, prodtypes);

        assertEquals(1, data.getBroytingKm(), 0);
    }

    public ProductionData getProdData(int prodTypeId, int grittingProductId) {
        return new ProductionData(1, getDate(2017,1,1,0), getDate(2017,1,1,1), 1, 1, 1, 1, null, 1, prodTypeId, grittingProductId, 0, null, null);
    }

    public static List<Prodtype> getProdtypes() {
        List<Prodtype> prodtypes = new ArrayList<>();
        prodtypes.add(new Prodtype(1L, "Sideplog", true, false, true));
        prodtypes.add(new Prodtype(5L, "Str�ing", true, true, false));
        prodtypes.add(new Prodtype(6L, "Br�yting", true, false, true));
        prodtypes.add(new Prodtype(24L, "Tung h�vling", true, false, true));
        return prodtypes;
    }
}