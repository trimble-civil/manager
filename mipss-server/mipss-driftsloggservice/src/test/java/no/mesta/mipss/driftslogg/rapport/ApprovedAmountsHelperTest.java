package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.persistence.driftslogg.ActivityRodeInfo;
import no.mesta.mipss.persistence.driftslogg.ApprovedActivity;
import org.junit.Before;
import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class ApprovedAmountsHelperTest {

    private ApprovedAmountsHelper approvedAmountsHelper;
    private List<ActivityRodeInfo> activityRodeInfos;

    @Before
    public void setUp() throws Exception {
        approvedAmountsHelper = new ApprovedAmountsHelper();
        activityRodeInfos = createData();
    }

    @Test
    public void findActivityRodeInfoFor_coproductionTwoTypes_twoActivityRodeInfos() throws Exception {
        ApprovedActivity approvedActivity = new ApprovedActivity();
        approvedActivity.setRodeId(1);
        approvedActivity.setVehicleId(1);
        approvedActivity.setFromDateTime(getDate(2017, 9, 11, 11, 30, 0));
        approvedActivity.setToDateTime(getDate(2017, 9, 11, 14, 30, 0));
        approvedActivity.setProdType("5,6");
        approvedActivity.setGritType("Befuktet salt");

        List<ActivityRodeInfo> actual = approvedAmountsHelper.findActivityRodeInfoFor(activityRodeInfos, approvedActivity);

        assertEquals(2, actual.size());
    }

    @Test
    public void findActivityRodeInfoFor_coproductionThreeTypes_threeActivityRodeInfos() throws Exception {
        ApprovedActivity approvedActivity = new ApprovedActivity();
        approvedActivity.setRodeId(1);
        approvedActivity.setVehicleId(1);
        approvedActivity.setFromDateTime(getDate(2017, 9, 11, 11, 30, 0));
        approvedActivity.setToDateTime(getDate(2017, 9, 11, 15, 30, 0));
        approvedActivity.setProdType("5,6,7");
        approvedActivity.setGritType("Befuktet salt");

        List<ActivityRodeInfo> actual = approvedAmountsHelper.findActivityRodeInfoFor(activityRodeInfos, approvedActivity);

        assertEquals(3, actual.size());
    }

    @Test
    public void findActivityRodeInfoFor_coproductionTwo_noActivityRodeInfos() throws Exception {
        ApprovedActivity approvedActivity = new ApprovedActivity();
        approvedActivity.setRodeId(1);
        approvedActivity.setVehicleId(1);
        approvedActivity.setFromDateTime(getDate(2017, 9, 11, 11, 30, 0));
        approvedActivity.setToDateTime(getDate(2017, 9, 11, 15, 30, 0));
        approvedActivity.setProdType("6,7");
        approvedActivity.setGritType("Befuktet salt");

        List<ActivityRodeInfo> actual = approvedAmountsHelper.findActivityRodeInfoFor(activityRodeInfos, approvedActivity);

        assertEquals(0, actual.size());
    }

    /* HELPER METHODS */

    private List<ActivityRodeInfo> createData() {
        List<ActivityRodeInfo> activityRodeInfos = new ArrayList<>();

        Date fromDate = getDate(2017, 9, 11, 12, 0, 0);
        Date toDate = getDate(2017, 9, 11, 13, 0, 0);

        activityRodeInfos.add(createActivityRodeInfo(
                1, 1, 5, 6, true, fromDate, toDate));
        activityRodeInfos.add(createActivityRodeInfo(
                1, 1, 6, null, true, fromDate, toDate));
        activityRodeInfos.add(createActivityRodeInfo(
                1, 1, 7, null, true, fromDate, toDate));

        fromDate = getDate(2017, 9, 11, 13, 0, 0);
        toDate = getDate(2017, 9, 11, 14, 0, 0);

        activityRodeInfos.add(createActivityRodeInfo(
                1, 1, 5, 6, true, fromDate, toDate));
        activityRodeInfos.add(createActivityRodeInfo(
                1, 1, 6, null, true, fromDate, toDate));

        return activityRodeInfos;
    }

    private ActivityRodeInfo createActivityRodeInfo(int vehicleId, int rodeId, int prodTypeId, Integer gritProdId,
                                                    boolean isCoproduction, Date fromDate, Date toDate) {
        ActivityRodeInfo activityRodeInfo = new ActivityRodeInfo();

        activityRodeInfo.setVehicleId(vehicleId);
        activityRodeInfo.setRodeId(rodeId);
        activityRodeInfo.setProdTypeId(prodTypeId);
        activityRodeInfo.setCoproduction(isCoproduction);
        activityRodeInfo.setGritProdId(gritProdId);
        activityRodeInfo.setFromDateTime(fromDate);
        activityRodeInfo.setToDateTime(toDate);

        return activityRodeInfo;
    }

    private Date getDate(int year, int month, int day, int hour, int minute, int second) {
        ZonedDateTime zoned = ZonedDateTime.of(
                year, month, day, hour, minute, second, 0, ZoneOffset.systemDefault());
        return Date.from(zoned.toInstant());
    }

}