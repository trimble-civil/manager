package no.mesta.mipss.driftslogg.util;

import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class CalendarHelperTest {

    @Test
    public void getFirstTimeInWeek() throws Exception {
        Date date = getDate(2017, 9, 22, 10, 30, 45, 123456);

        Date actual = CalendarHelper.getFirstTimeInWeek(date);

        Date expected = getDate(2017, 9, 18, 0, 0, 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getLastTimeInWeek() throws Exception {
        Date date = getDate(2017, 9, 22, 10, 30, 45, 123456);

        Date actual = CalendarHelper.getLastTimeInWeek(date);

        Date expected = getDate(2017, 9, 24, 23, 59, 59, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getLastDateInMonthGivenMonthYear() throws Exception {
        Date actual = CalendarHelper.getLastDateInMonth(8L, 2017L);

        Date expected = getDate(2017, 9, 30, 23, 59, 59, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getFirstDateInMonthGivenMontYear() throws Exception {
        Date actual = CalendarHelper.getFirstDateInMonth(8L, 2017L);

        Date expected = getDate(2017, 9, 1, 0, 0, 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void convertDateWithTimeToZonedDateTimeTruncatesNanosToMillis() throws Exception {
        Date date = getDate(2017, 9, 22, 10, 45, 30, 999999999);

        ZonedDateTime actual = CalendarHelper.convertDateWithTimeToZonedDateTime(date);

        ZonedDateTime expected = getZonedDateTime(2017, 9, 22, 10, 45, 30, 999000000);
        assertEquals(expected, actual);
    }

    @Test
    public void convertDateToZonedDateTime() throws Exception {
        Date date = getDate(2017, 9, 22, 9, 45, 30, 0);

        Date actual = CalendarHelper.getFirstDateInMonth(date);

        Date expected = getDate(2017, 9, 1, 0, 0, 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getFirstDateInMonth() throws Exception {
        Date date = getDate(2017, 9, 22, 9, 45, 30, 0);

        Date actual = CalendarHelper.getFirstDateInMonth(date);

        Date expected = getDate(2017, 9, 1, 0, 0, 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getLastDateInMonth() throws Exception {
        Date date = getDate(2017, 9, 22, 9, 45, 30, 0);

        Date actual = CalendarHelper.getLastDateInMonth(date);

        Date expected = getDate(2017, 9, 30, 0, 0, 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getFirstTimeInMonth() throws Exception {
        Date date = getDate(2017, 9, 22, 9, 45, 30, 0);

        Date actual = CalendarHelper.getFirstTimeInMonth(date);

        Date expected = getDate(2017, 9, 1, 0, 0, 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void getLastTimeInMonth() throws Exception {
        Date date = getDate(2017, 9, 22, 9, 45, 30, 0);

        Date actual = CalendarHelper.getLastTimeInMonth(date);

        Date expected = getDate(2017, 9, 30, 23, 59, 59, 999000000);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationInHours() throws Exception {
        Date from = getDate(2016, 8, 22, 9, 45, 30, 0);
        Date to = getDate(2016, 8, 22, 11, 30, 30, 0);

        double actual = CalendarHelper.getDurationInHours(from, to);

        assertEquals(1.75, actual, 0.001);
    }

    private Date getDate(int year, int month, int day, int hour, int minute, int second, int nano) {
        return Date.from(
                ZonedDateTime.of(year, month, day, hour, minute, second, nano,
                ZoneOffset.systemDefault()).toInstant());
    }

    private ZonedDateTime getZonedDateTime(int year, int month, int day, int hour, int minute, int second, int nano) {
        return ZonedDateTime.of(
                year, month, day, hour, minute, second, nano,
                ZoneOffset.systemDefault());
    }

}