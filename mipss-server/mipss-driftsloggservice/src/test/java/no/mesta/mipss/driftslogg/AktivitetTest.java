package no.mesta.mipss.driftslogg;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import junit.framework.TestCase;


public class AktivitetTest extends TestCase{

	public void testGetRiktigUtfortMengdeKm(){
		Aktivitet a = new Aktivitet();
		a.setEnhet("KM");
		a.setMeldtKm(100d);
		a.setMeldtMengde(200d);
		assertEquals(100d, a.getUtfortMengde());
	}
	
	public void testGetRiktigUtfortMengdeTime(){
		Aktivitet a = new Aktivitet();
		a.setEnhet("TIM");
		a.setMeldtKm(100d);
		a.setMeldtMengde(200d);
		assertEquals(200d,a.getUtfortMengde());
	}
	
	public void testGetRiktigGodkjentMengdeKm(){
		Aktivitet a = new Aktivitet();
		a.setEnhet("KM");
		a.setMeldtKm(100d);
		a.setOmforentMengde(50d);
		
		assertEquals(50d, a.getGodkjentMengde());
		a.setMeldtMengde(200d);
		
		assertEquals(50d, a.getGodkjentMengde());
	}
	
	public void testGetRiktigGodkjentMengdeTime(){
		Aktivitet a = new Aktivitet();
		a.setEnhet("TIM");
		a.setMeldtMengde(200d);
		a.setOmforentMengde(50d);
		assertEquals(50d, a.getGodkjentMengde());
		a.setMeldtKm(100d);
		assertEquals(50d, a.getGodkjentMengde());
	}
	
}
