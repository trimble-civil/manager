package no.mesta.mipss.driftslogg;

import java.util.List;

import junit.framework.TestCase;
import no.mesta.mipss.driftslogg.rapport.BeregnetFordeling;
import no.mesta.mipss.driftslogg.rapport.Fordelingsrode;
import no.mesta.mipss.driftslogg.rapport.Rodefordeling;
import no.mesta.mipss.persistence.kontrakt.Rode;

public class RodefordelingTest extends TestCase {

	public void testFordelTall(){
		Rode sbrode1 = new Rode();
		sbrode1.setNavn("SBRode 1");
		Fordelingsrode r1 = new Fordelingsrode(sbrode1);
		
		Rode r12rode1 = new Rode();
		r12rode1.setNavn("R12rode 1");
		
		Rode r12rode2 = new Rode();
		r12rode2.setNavn("R12rode 2");
		
		Rodefordeling r121 = new Rodefordeling(r12rode1, 0.2);
		Rodefordeling r122 = new Rodefordeling(r12rode2, 0.8);
		r1.addRodefordeling(r121);
		r1.addRodefordeling(r122);
		
		List<BeregnetFordeling> beregnetFordeling = r1.getBeregnetFordeling(1d);
		assertEquals(2, beregnetFordeling.size());

		BeregnetFordeling b1= beregnetFordeling.get(0);
		BeregnetFordeling b2 = beregnetFordeling.get(1);
		
		assertEquals(0.2d, b1.getBeregnetVerdi());
		assertEquals(0.8d, b2.getBeregnetVerdi());
		
		assertEquals(r12rode1.getNavn(), b1.getR12rode().getNavn());
		assertEquals(r12rode2.getNavn(), b2.getR12rode().getNavn());
		
	}
}
