package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Rode;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OutsideTripHelperTest {

    private OutsideTripDataFetcher dataFetcher;
    private OutsideTripHelper outsideTripHelper;

    private List<Rode> rodes;
    private List<OutsideTripDataFetcher.Vehicle> vehicles;
    private List<OutsideTripHelper.RodeConnection> connections;
    private List<Trip> trips;
    private List<ProductionData> data;

    @Before
    public void setUp() throws Exception {
        rodes = new ArrayList<>();
        rodes.add(new Rode(1, "Rode1", false));

        vehicles = new ArrayList<>();
        vehicles.add(new OutsideTripDataFetcher.Vehicle(1, 1, "Vehicle1", "Supplier1"));

        connections = new ArrayList<>();
        connections.add(new OutsideTripHelper.RodeConnection(1l, 1l, getDate(2017,1,1,0), getDate(2017,12,1,0)));

        trips = new ArrayList<>();
        trips.add(getTrip(getDate(2017,1,1,0), getDate(2017,1,1,1)));

        data = new ArrayList<>();
        data.add(getProductionData(1, getDate(2017, 1, 1, 2), getDate(2017, 1, 1, 3), 1, 1, 1, 6, 0));
        data.add(getProductionData(2, getDate(2017, 1, 1, 3), getDate(2017, 1, 1, 4), 1, 1, 1, 1, 0));

        dataFetcher = mock(OutsideTripDataFetcher.class);
        when(dataFetcher.getRodes()).thenReturn(rodes);
        when(dataFetcher.getVehicles()).thenReturn(vehicles);
        when(dataFetcher.getRodeConnections()).thenReturn(connections);
        when(dataFetcher.getTrips(vehicles)).thenReturn(trips);
        when(dataFetcher.getProdtypes()).thenReturn(OutsideTripAggregatorTest.getProdtypes());
        when(dataFetcher.getProductionData(vehicles, rodes)).thenReturn(data);

        outsideTripHelper = new OutsideTripHelper(dataFetcher);
    }

    @Test
    public void canFindConnection() throws Exception {
        assertTrue(outsideTripHelper.getConnection(connections, 1, 1, getDate(2017,1,1,1)));
    }

    @Test
    public void returnsFalseWhenConnectionNotFound() throws Exception {
        assertFalse(outsideTripHelper.getConnection(connections, -1, -1, getDate(2017,1,1,1)));
    }

    @Test
    public void returnsFalseWhenConnectionOutsideOfTime() throws Exception {
        assertFalse(outsideTripHelper.getConnection(connections, 1, 1, getDate(2016, 1, 1, 0)));
    }

    @Test
    public void handlesConnectionWithNullEndDate() throws Exception {
        connections.add(new OutsideTripHelper.RodeConnection(1, 2, getDate(2017,1,1,0), null));
        assertTrue(outsideTripHelper.getConnection(connections, 1, 2, getDate(2018,1,1,0)));
    }

    @Test
    public void canFindRode() throws Exception {
        assertEquals(rodes.get(0), outsideTripHelper.getRode(rodes, 1));
    }

    @Test(expected=IllegalArgumentException.class)
    public void throwsExceptionWhenRodeDoesNotExist() throws Exception {
        outsideTripHelper.getRode(rodes, -1);
    }

    @Test
    public void canFindVehicle() throws Exception {
        assertEquals(vehicles.get(0), outsideTripHelper.getVehicle(vehicles, 1));
    }

    @Test(expected=IllegalArgumentException.class)
    public void throwsExceptionWhenVehicleDoesNotExist() throws Exception {
        outsideTripHelper.getVehicle(vehicles, -1);
    }

    @Test
    public void willRemoveDataCoveredByTrip() throws Exception {
        Date fromDate = getDate(2017, 1, 1, 0);
        Date toDate = getDate(2017, 1, 1, 1);

        List<Trip> trips = new ArrayList<>();
        trips.add(getTrip(fromDate, toDate));

        List<ProductionData> data = new ArrayList<>();
        data.add(getProductionData(1, fromDate, toDate, 1, 1, 1, 1, 1));

        List<ProductionData> actual = outsideTripHelper.removeIncludedInTrip(data, trips);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void willNotRemoveDataStartingWhenTripEnds() throws Exception {
        Date tripFromDate = getDate(2017, 1, 1, 0);
        Date tripEndDate = getDate(2017, 1, 1, 1);
        Date prodToDate = getDate(2017, 1, 1, 2);

        List<Trip> trips = new ArrayList<>();
        trips.add(getTrip(tripFromDate, tripEndDate));

        List<ProductionData> data = new ArrayList<>();
        data.add(getProductionData(1, tripEndDate, prodToDate, 1, 1, 1, 1, 1));

        List<ProductionData> actual = outsideTripHelper.removeIncludedInTrip(data, trips);
        assertEquals(1, actual.size());
        assertEquals(data.get(0), actual.get(0));
    }

    @Test
    public void willNotRemoveDataEndingWhenTripBegins() throws Exception {
        Date tripFromDate = getDate(2017, 1, 1, 1);
        Date tripEndDate = getDate(2017, 1, 1, 2);
        Date prodFromDate = getDate(2017, 1, 1, 0);

        List<Trip> trips = new ArrayList<>();
        trips.add(getTrip(tripFromDate, tripEndDate));

        List<ProductionData> data = new ArrayList<>();
        data.add(getProductionData(1, prodFromDate, tripFromDate, 1, 1, 1, 1, 1));

        List<ProductionData> actual = outsideTripHelper.removeIncludedInTrip(data, trips);
        assertEquals(1, actual.size());
        assertEquals(data.get(0), actual.get(0));
    }

    @Test
    public void canGetEnrichedOutsideTripData() throws Exception {
        List<OutsideTripData> actual = outsideTripHelper.getOutsideTrip();

        assertEquals(1, actual.size());
        assertEquals(2, actual.get(0).getBroytingKm(), 0);
        assertEquals(1, actual.get(0).getSideplogKm(), 0);
        assertEquals("Vehicle1", actual.get(0).getKjoretoynavn());
        assertEquals("Supplier1", actual.get(0).getLeverandor());
        assertEquals("Rode1", actual.get(0).getRodenavn());
    }

    protected static ProductionData getProductionData(int prodStretchId, Date fromDate, Date toDate, double drivenKm, double drivenKmRode, int vehicleId, int prodTypeId, int grittingProductId) {
        ProductionData data = new ProductionData(prodStretchId, fromDate, toDate, drivenKm, drivenKmRode, 1, vehicleId, null, 0, prodTypeId, grittingProductId, 0, null, null);
        return data;
    }

    protected static Trip getTrip(Date fromDate, Date toDate) {
        Trip trip = new Trip();
        trip.setStartTime(fromDate);
        trip.setEndTime(toDate);
        trip.setVehicle(getVehicle(1));

        return trip;
    }

    protected static Kjoretoy getVehicle(long id) {
        Kjoretoy vehicle = new Kjoretoy();
        vehicle.setId(id);
        return vehicle;
    }

    protected static Date getDate(int year, int month, int day, int hour) {
        return getDate(year, month, day, hour, 0, 0);
    }

    protected static Date getDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }
}