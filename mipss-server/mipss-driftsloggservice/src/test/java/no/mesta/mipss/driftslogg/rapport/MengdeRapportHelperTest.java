package no.mesta.mipss.driftslogg.rapport;

import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.persistence.driftslogg.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

public class MengdeRapportHelperTest {

    public static final String VENDOR_NR = "100";
    public static final Long STROPRODID_GRITTING = 1L;
    public static final String VENDOR_NAME = "Hjalla";
    public static final String VENDOR_CONTRACT_IDENT = "100_4";
    public static final Double DRY_AMOUNT_GRITTING_KG = 10.0;

    private Levkontrakt levkontrakt;
    private Periode periode;
    private DriftsloggBean bean;
    private Aktivitet amock;
    private AgrArtikkelV agrArtikkel;
    private List<Aktivitet> activities;


    @Before
    public void setUp() {
        levkontrakt = new Levkontrakt();
        levkontrakt.setLeverandorNr(VENDOR_NR);
        levkontrakt.setLevkontraktIdent(VENDOR_CONTRACT_IDENT);

        periode = new Periode();
        periode.setLevkontrakt(levkontrakt);
        List<PeriodeStatus> statuser = new ArrayList<>();
        PeriodeStatus ps = new PeriodeStatus();
        PeriodestatusType status = new PeriodestatusType();
        status.setId(3L);
        ps.setStatus(status);
        statuser.add(ps);
        periode.setStatuser(statuser);

        bean = mock(DriftsloggBean.class);

        List<Stroproduktmengde> grittingAmounts = new ArrayList<>();
        Stroproduktmengde s = new Stroproduktmengde();
        s.setStroproduktId(STROPRODID_GRITTING);
        s.setOmforentMengdeTort(DRY_AMOUNT_GRITTING_KG);
        grittingAmounts.add(s);

        activities = new ArrayList<>();
        Trip trip = new Trip();
        trip.setProcessStatusId("7");
        amock = mock(Aktivitet.class);
        when(amock.getPeriode()).thenReturn(periode);
        when(amock.isApprovedBuild()).thenReturn(true);
        when(amock.getEnhet()).thenReturn("km");
        when(amock.getStroprodukter()).thenReturn(grittingAmounts);
        when(amock.getTrip()).thenReturn(trip);
        activities.add(amock);

        AgrArtikkelV agrArtikkel = new AgrArtikkelV();
        agrArtikkel.setPris(45.0);
        agrArtikkel.setGruppeNavn("Br?yting");
        when(bean.getArtikkelViewFromAktivitet(amock)).thenReturn(agrArtikkel);
    }

    @Test
    public void testAvoidNullPointerException() throws Exception {
        agrArtikkel = null;
        when(bean.getArtikkelViewFromAktivitet(amock)).thenReturn(agrArtikkel);

        MengdeRapportHelper mrh = new MengdeRapportHelper(bean);
        Map<String, String> leverandorNrCache = new HashMap<>();
        Map<String, MengdeRapportObject> valuesFromAktiviteter = mrh.getValuesFromAktiviteter(activities, leverandorNrCache);
        assertTrue(valuesFromAktiviteter.size() > 0);
    }

    @Test
    public void testApprovedBuildIsFalse() throws Exception {
        when(amock.isApprovedBuild()).thenReturn(false);

        MengdeRapportHelper mrh = new MengdeRapportHelper(bean);
        Map<String, String> leverandorNrCache = new HashMap<>();
        Map<String, MengdeRapportObject> valuesFromAktiviteter = mrh.getValuesFromAktiviteter(activities, leverandorNrCache);
        assertTrue(valuesFromAktiviteter.size() == 0);
    }

    @Test
    public void testApprovedBuildIsTrue() throws Exception {
        MengdeRapportHelper mrh = new MengdeRapportHelper(bean);
        Map<String, String> leverandorNrCache = new HashMap<>();
        leverandorNrCache.put(VENDOR_NR, VENDOR_NAME);

        Map<String, MengdeRapportObject> valuesFromAktiviteter = mrh.getValuesFromAktiviteter(activities, leverandorNrCache);
        String key = VENDOR_NAME + " (" + VENDOR_NR + ")";

        assertTrue(valuesFromAktiviteter.size() == 1);
        assertEquals(key, valuesFromAktiviteter.get(key).getLeverandor());
        assertEquals(DRY_AMOUNT_GRITTING_KG / 1000, valuesFromAktiviteter.get(key).getTonnTorrsalt(), 0.01);
    }
}