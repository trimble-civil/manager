package no.mesta.mipss.driftslogg.rapport;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.kontrakt.Rode;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class R12RapportHelperTest extends TestCase {

    Aktivitet a;
    AgrArtikkelV artikkel;
    R12RapportHelper r;
    Fordelingsrode broyteRode;
    List<R12RapportObject> r12ForAktivitet;

    @Before
    public void setUp() {
        r = new R12RapportHelper(null, null);

        Rode rode = new Rode();
        rode.setNavn("Tr�ndelag Nord DkD");
        broyteRode = new Fordelingsrode(rode);
        // Set distrubution to 50%
        broyteRode.addRodefordeling(new Rodefordeling(rode,0.5));

        a = new Aktivitet();
        a.setArtikkelIdent("20002");
        a.setEnhet("KM");
        a.setOmforentKm(40.0);

        r12ForAktivitet = new ArrayList<>();
        R12RapportObject r12 = new R12RapportObject();
        r12.setLeverandor("57275");
        r12.setR12rode("Tr�ndelag Nord DkD");
        r12ForAktivitet.add(r12);

        artikkel = new AgrArtikkelV();
        artikkel.setArtikkelIdent("20002");
        artikkel.setArtikkelNavn("BR�YTING");
    }

    @Test
    public void testGetIntention20kmDistribution50Percent() {
        a.setApprovedIntention(true);
        a.setR12_intention(20.0);
        a.setProductionType("tomkj\u00F8ring");
        r.getValuesFromArtikkel(artikkel, a, broyteRode, r12ForAktivitet);

        assertEquals(10.0, r12ForAktivitet.get(0).getIntentionKm(),0.01);
    }

    @Test
    public void testGetPloghingDistribution50Percent() {
        a.setR12_broyting(40.0);
        a.setProductionType("br\u00F8yting");
        r.getValuesFromArtikkel(artikkel, a, broyteRode, r12ForAktivitet);

        assertEquals(20.0, r12ForAktivitet.get(0).getBroytingKm(),0.01);
    }

    @Test
    public void testGetHeavyPloughingDistribution50Percent() {
        a.setR12_hovling(30.0);
        a.setProductionType("tung h\u00F8vling+st\u00F8ing+h\u00F8 : sand med salt");
        r.getValuesFromArtikkel(artikkel, a, broyteRode, r12ForAktivitet);

        assertEquals(15.0, r12ForAktivitet.get(0).getTungHovlingKm(),0.01);
    }

}