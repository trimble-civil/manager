@echo *************** BYGGER MIPSS-PERSISTENCE *************** 
cd ..\mipss-persistence
call mvn clean install -o
@echo *************** BYGGER STUDIO-WEBSTART-SERVLET *************** 
cd ..\studio-webstart-servlet
call mvn clean install -o
@echo *************** BYGGER MIPSS-SERVER *************** 
cd ..\mipss-server
call mvn clean install -o
