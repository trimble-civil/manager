/**
 * 
 */
package no.mesta.mipss.driftkontrakt;

import java.util.ArrayList;
import java.util.List;

import no.mesta.driftkontrakt.bean.ProsessHelper;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import junit.framework.TestCase;

/**
 * @author harkul
 *
 */
public class ProsessHelperTest extends TestCase {


	private List<Prosess> prosessListe;
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
//		Prosess p1 = create(3, "48", "Drens- og avløpsanlegg", null);
//		Prosess p22 = create(54, "48.2", "Fjerning av torvkanter", p1);
//		Prosess p3 = create(55, "48.21", "Fjerning av torvkanter under rekkverk på veger med fast grus", p22);
//		
//		Prosess p4 = create(61, "48.4", "Utskifting av stikkrenner", p1);
//		Prosess p5 = create(62, "48.41", "Utskifting", p4);
//		Prosess p6 = create(63, "48.411", "Innvendig diameter 300 mm", p5);
//		
//		Prosess p01 = create(27, "62.1", "Faste dekker", null);
//		Prosess p02 = create(64, "62.11", "Grunnpakke", p01);
//		Prosess p03 = create(65, "62.12", "Lapping av hull i asfaltdekker", p01);
//		Prosess p04 = create(66, "62.121", "Lapping med kalde masser", p03);
//		Prosess p05 = create(67, "62.122", "Lapping med varme masser", p03);
//		Prosess p06 = create(68, "62.1221", "Forenklet lapping", p05);
//		Prosess p07 = create(69, "62.1222", "Fullverdig lapping", p05);
//		
//		p05.getProsesser().add(p06);
//		p05.getProsesser().add(p07);
//		prosessListe = new ArrayList<Prosess>();
//		prosessListe.add(p1);
//		prosessListe.add(p22);
//		prosessListe.add(p3);
//		prosessListe.add(p4);
//		prosessListe.add(p5);
//		prosessListe.add(p6);
//		
//		prosessListe.add(p01);
//		prosessListe.add(p02);
//		prosessListe.add(p03);
//		prosessListe.add(p04);
//		prosessListe.add(p05);
////		prosessListe.add(p06);
//		prosessListe.add(p07);
		
		Prosess p0 = create(3, "48", "", null);
		Prosess p2 = create(53, "48.1", "", p0);
		Prosess p3 = create(54, "48.2", "", p0);
		Prosess p1 = create(55, "48.21", "", p3);
		Prosess p4 = create(56, "48.22", "", p3);
		Prosess p5 = create(57, "48.23", "", p3);
		Prosess p6 = create(58, "48.3", "", p0);
		Prosess p7 = create(59, "48.31", "", p6);
		Prosess p8 = create(60, "48.32", "", p6);
		Prosess p9 = create(61, "48.4", "", p0);
		
		prosessListe = new ArrayList<Prosess>();
		prosessListe.add(p1);
		prosessListe.add(p2);
		prosessListe.add(p3);
		prosessListe.add(p4);
		prosessListe.add(p5);
		prosessListe.add(p6);
		prosessListe.add(p7);
		prosessListe.add(p8);
		prosessListe.add(p9);
		prosessListe.add(p0);
		
	}
	
	public static void printChild(Prosess a, String tab){
		for (Prosess pp:a.getProsesser()){
			System.out.println(tab+pp.getProsessKode()+" "+pp.getNavn());
			printChild(pp, tab+"  ");
		}
	}
	public static void main(String[] args) {
		System.out.println("start");
		new ProsessHelperTest().testRelasjonerBlirRiktige();
	}
	public void testRelasjonerBlirRiktige(){
		System.out.println("test");
		try {
			setUp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Prosess> tree = new ProsessHelper().createTree(prosessListe);
		for (Prosess p:tree){
			System.out.println(p.getProsessKode()+" "+p.getNavn());
			printChild(p, " ");
		}
//		Prosess p = tree.get(0);
//		assertEquals(p.getId(), Long.valueOf(3));
//		assertEquals(p.getProsesser().size(), 2);
//		for (Prosess pp:p.getProsesser()){
//			assertEquals(pp.getEierprosess(), p);
//			if (pp.getId().equals(Long.valueOf(54))){
//				assertEquals(pp.getProsesser().get(0).getId(), Long.valueOf(55));
//			}
//			if (pp.getId().equals(Long.valueOf(61))){
//				assertEquals(pp.getProsesser().get(0).getId(), Long.valueOf(62));
//				assertEquals(pp.getProsesser().get(0).getProsesser().get(0).getId(), Long.valueOf(63));
//				assertEquals(pp.getProsesser().get(0).getEierprosess(), pp);
//				assertEquals(pp.getProsesser().get(0).getProsesser().get(0).getEierprosess(), pp.getProsesser().get(0));
//			}
//		}
	}
	
	private Prosess create(int id, String kode, String navn, Prosess eier){
		Prosess p = new Prosess();
		p.setId(Long.valueOf(id));
		p.setProsessKode(kode);
		p.setNavn(navn);
		p.setEierprosess(eier);
		return p;
		
	}

}
