package no.mesta.driftkontrakt.bean;

import no.mesta.mipss.filter.AbstractQueryFilter;



@SuppressWarnings("serial")
public class ProsjektQueryFilter extends AbstractQueryFilter {
	private String prosjektnr;
	private String prosjektnavn;
	private String ansvar;
	private String[] fields = {"prosjektnr", "prosjektnavn", "ansvar"};
	
	public ProsjektQueryFilter() {
	}
	
	public String getProsjektnr() {
		return prosjektnr;
	}
	
	public void setProsjektnr(String prosjektnr) {
		this.prosjektnr = prosjektnr;
	}
	
	public String getProsjektnavn() {
		return prosjektnavn;
	}
	
	public void setProsjektnavn(String prosjektnavn) {
		this.prosjektnavn = prosjektnavn;
	}
	
	public String getAnsvar() {
		return ansvar;
	}
	
	public void setAnsvar(String ansvar) {
		this.ansvar = ansvar;
	}

	public String[] getSearchableFields() {
		return fields;
	}
}
