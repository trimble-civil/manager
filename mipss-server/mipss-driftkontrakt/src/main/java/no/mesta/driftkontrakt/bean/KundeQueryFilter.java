package no.mesta.driftkontrakt.bean;

import no.mesta.mipss.filter.AbstractQueryFilter;



@SuppressWarnings("serial")
public class KundeQueryFilter extends AbstractQueryFilter {
	private String kundeNr;
	private String kundeNavn;
	private String[] fields = {"kundeNr", "kundeNavn"};
	
	public KundeQueryFilter() {
	}

	public String getKundeNr() {
		return kundeNr;
	}

	public void setKundeNr(String kundeNr) {
		this.kundeNr = kundeNr;
	}

	public String getKundeNavn() {
		return kundeNavn;
	}

	public void setKundeNavn(String kundeNavn) {
		this.kundeNavn = kundeNavn;
	}

	public String[] getSearchableFields() {
		return fields;
	}
}
