package no.mesta.driftkontrakt.bean;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyGruppering;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyPK;

@Stateless(name=KjoretoygruppeService.BEAN_NAME, mappedName="ejb/"+KjoretoygruppeService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class KjoretoygruppeBean implements KjoretoygruppeService {
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
	
	/** {@inheritDoc} */
	public List<Kjoretoygruppe> getKjoretoygruppe(Long driftkontraktId){
		return em.createNamedQuery(Kjoretoygruppe.FIND_BY_KONTRAKT)
			.setParameter("id", driftkontraktId)
			.setHint(QueryHints.REFRESH, HintValues.TRUE)
			.getResultList();
	}
	/** {@inheritDoc} */
    public List<KontraktKjoretoyGruppering> getKontraktKjoretoyGruppering(Long kjoretoygruppeId){
		return em.createNamedQuery(KontraktKjoretoyGruppering.FIND_BY_GRUPPE)
			.setParameter("id", kjoretoygruppeId)
			.setHint(QueryHints.REFRESH, HintValues.TRUE)
			.getResultList();
	}
	/** {@inheritDoc} */
	public KontraktKjoretoyGruppering peristKontraktKjoretoygruppering(KontraktKjoretoyGruppering kjoretoy){
		kjoretoy = em.merge(kjoretoy);
		em.flush();
		return kjoretoy;
	}
	/** {@inheritDoc} */
	public Kjoretoygruppe persistKjoretoygruppe(Kjoretoygruppe kjoretoygruppe){
		kjoretoygruppe = em.merge(kjoretoygruppe);
		em.flush();
		return kjoretoygruppe;
	}
	/** {@inheritDoc} */
    public void slettKontraktKjoretoyGruppering(KontraktKjoretoyGruppering kjoretoy){
    	kjoretoy = em.merge(kjoretoy);
    	em.remove(kjoretoy);
    	em.flush();
    }

	/** {@inheritDoc} */
    public void slettKjoretoygruppe(Long kjoretoygruppeId){
    	Kjoretoygruppe gruppe = em.find(Kjoretoygruppe.class, kjoretoygruppeId);
    	em.remove(gruppe);
    	em.flush();
    }
    /** {@inheritDoc} */
	public List<KontraktKjoretoy> getKjoretoyUtenforGruppering(Long kjoretoygruppeId, Long driftkontraktId) {
		return em.createNamedQuery(KontraktKjoretoy.FIND_BY_NOT_IN_GRUPPE)
			.setParameter("id", driftkontraktId)
			.setParameter("gruppeId", kjoretoygruppeId).getResultList();
	}
	/** {@inheritDoc} */
	public void updateKjoretoyGruppe(Kjoretoygruppe gruppe) {
		List<KontraktKjoretoyGruppering> oldKjoretoygruppeList = em.find(Kjoretoygruppe.class, gruppe.getId()).getKjoretoygruppe();
		deleteOldEntities(oldKjoretoygruppeList, gruppe.getKjoretoygruppe());
		em.merge(gruppe);
		em.flush();
	}
	public List<String> getKjoretoynavn(Long kjoretoygruppeId){
		return em.createNamedQuery(KontraktKjoretoyGruppering.FIND_KJORETOYNAVN_I_GRUPPE)
		.setParameter("gruppeId", kjoretoygruppeId)
		.getResultList();
	}
	/**
	 * Sletter objekter som ikke lenger skal våre med i relasjoner
	 * 
	 * @param old
	 * @param current
	 */
	private void deleteOldEntities(List old, List current) {
		for (Object o : old) {
			if (!current.contains(o)) {
				em.remove(o);
			}
		}
		em.flush();
	}	
	/** {@inheritDoc} */
	public KontraktKjoretoy getKontraktKjoretoy(Long driftkontraktId, Long kjoretoyId) {
		KontraktKjoretoyPK pk = new KontraktKjoretoyPK(driftkontraktId, kjoretoyId);
		return em.find(KontraktKjoretoy.class, pk);
	}
}
