package no.mesta.driftkontrakt.bean;

import no.mesta.mipss.filter.AbstractQueryFilter;



@SuppressWarnings("serial")
public class StabQueryFilter extends AbstractQueryFilter {
	private String navn;
	private String signatur;
	private String[] fields = {"navn", "signatur"};
	
	public StabQueryFilter() {
	}
	
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getSignatur() {
		return signatur;
	}

	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}

	public String[] getSearchableFields() {
		return fields;
	}
}
