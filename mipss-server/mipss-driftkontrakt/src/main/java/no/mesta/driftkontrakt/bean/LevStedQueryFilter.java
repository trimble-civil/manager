package no.mesta.driftkontrakt.bean;

import no.mesta.mipss.filter.AbstractQueryFilter;



@SuppressWarnings("serial")
public class LevStedQueryFilter extends AbstractQueryFilter {
	private String levNr;
	private String levNavn;
	private String adresse1;
	private String postnr;
	private String poststed;
	private String[] fields = {"levNr", "levNavn", "adresse1", "postnr", "poststed"};
	
	public LevStedQueryFilter() {
	}
	
	public String getLevNr() {
		return levNr;
	}

	public void setLevNr(String levNr) {
		this.levNr = levNr;
	}

	public String getLevNavn() {
		return levNavn;
	}

	public void setLevNavn(String levNavn) {
		this.levNavn = levNavn;
	}

	public String getAdresse1() {
		return adresse1;
	}

	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	public String getPostnr() {
		return postnr;
	}

	public void setPostnr(String postnr) {
		this.postnr = postnr;
	}

	public String getPoststed() {
		return poststed;
	}

	public void setPoststed(String poststed) {
		this.poststed = poststed;
	}

	public String[] getSearchableFields() {
		return fields;
	}
}
