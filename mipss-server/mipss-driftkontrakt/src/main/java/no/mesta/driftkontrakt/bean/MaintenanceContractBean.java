package no.mesta.driftkontrakt.bean;

import java.awt.Point;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoylogg;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyDogn;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyGruppering;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.kontrakt.Kontrakttype;
import no.mesta.mipss.persistence.kontrakt.KundeView;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRodePK;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.kontrakt.kontakt.Kontraktkontakttype;
import no.mesta.mipss.persistence.mipssfield.FavorittProsess;
import no.mesta.mipss.persistence.mipssfield.Favorittliste;
import no.mesta.mipss.persistence.mipssfield.KontraktProsess;
import no.mesta.mipss.persistence.mipssfield.Pdabruker;
import no.mesta.mipss.persistence.mipssfield.PdabrukerKontrakt;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Prosessett;
import no.mesta.mipss.persistence.organisasjon.ArbeidsordreView;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.util.DetachmentHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;

/**
 * Implementasjon av gitt interface... :-)
 * 
 * @author Harald A. Kulø
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Stateless(name=MaintenanceContract.BEAN_NAME, mappedName="ejb/"+MaintenanceContract.BEAN_NAME)
@SuppressWarnings("unchecked")
public class MaintenanceContractBean implements MaintenanceContract {
    @PersistenceContext(unitName="mipssPersistence")
    EntityManager em;
    
    @EJB
    MipssKjoretoy mipssKjoretoy;
    
    @EJB
    Rodegenerator rodegenerator;
    
    private static final Logger log = LoggerFactory.getLogger(MaintenanceContractBean.class);

	/**
	 * Sletter objekter som ikke lenger skal være med i relasjoner
	 * 
	 * @param old
	 * @param current
	 */
	@SuppressWarnings("rawtypes")
	private void deleteOldEntities(List old, List current) {
		for (Object o : old) {
			if (!current.contains(o)) {
				em.remove(o);
			}
		}
		em.flush();
	}
	

    /** {@inheritDoc} */
    public List<Driftkontrakt> getDriftkontraktList() {
        return em.createQuery("select dk from Driftkontrakt dk").getResultList();
    }

	/** {@inheritDoc} */
    public List<Driftkontrakt> getDriftkontraktList(Bruker bruker){
    	return em.createNamedQuery(KontraktStab.QUERY_FIND_BY_SIGNATUR).setParameter("signatur", bruker.getSignatur()).getResultList();
    }
	
    /** {@inheritDoc} */
	public List<Kjoretoyordre> getOrdreListForKontraktId(long kontraktId) {
    	Query q = em.createNamedQuery(Kjoretoyordre.FIND_BY_KONTRAKT).setParameter("id", kontraktId);
		return q.getResultList();
    }
    
    /** {@inheritDoc} */
	public List<Driftkontrakt> getDriftkontraktListByname() {
		Query q = em.createNamedQuery(Driftkontrakt.QUERY_FIND_ALL_BY_NAME);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Driftkontrakt> getValidDriftkontraktList() {
		Query q = em.createNamedQuery(Driftkontrakt.QUERY_FIND_VALID);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Kjoretoyordretype> getKjoretoyordretypeValidList() {
		Query q = em.createNamedQuery(Kjoretoyordretype.FIND_ALL_VALID);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Kjoretoyordrestatus> getKjoretoyordrestatusList() {
		Query q = em.createNamedQuery(Kjoretoyordrestatus.FIND_ALL_VALID);
		return q.getResultList();
	}
	
	/** {@inheritDoc} */
	public List<Driftdistrikt> getDriftdistriktList() {
		Query q = em.createNamedQuery(Driftdistrikt.FIND_ALL);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Kontrakttype> getKontrakttypeList() {
		Query q = em.createNamedQuery(Kontrakttype.FIND_ALL);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public Veinett getVeinettForDriftkontrakt(Long id) {
		return em.find(Veinett.class, id);
	}
	
	
	public Date getInaktivDateForLeverandor(Leverandor leverandor){
		try{
			Timestamp inaktiv = (Timestamp)em.createNamedQuery(LevStedView.QUERY_FIND_INAKTIVDATE).setParameter("leverandorNr", leverandor.getNr()).getSingleResult();
			DateTime inaktivNull = new DateTime(1900, 1, 1, 0, 0);
			DateTime i = new DateTime(inaktiv);
			if (i.getYear()==inaktivNull.getYear()){
				return null;
			}
			return inaktiv;
		} catch (Throwable t){
			return null;
		}
		
	}
	 
	/** {@inheritDoc} */
	public Driftkontrakt getDriftkontraktForAdministration(Long id) {
		log.debug("getDriftkontraktForAdministration start("+id+")");
		Driftkontrakt d = em.find(Driftkontrakt.class, id);		
		d.getKontraktProsessList().size();
		d.getFavorittlisteList().size();
		d.getDfuKontraktList().size();
		d.getKontraktKontaktypeList().size();
		for (KontraktLeverandor k:d.getKontraktLeverandorList()){
			k.getLevkontraktList().size();
			k.setAntallKjoretoy(getAntallKjoretoyForKontraktLeverandor(k));
			k.setInaktivDato(getInaktivDateForLeverandor(k.getLeverandor()));
		}
		d.getKontraktStabList().size();
		d.getKontraktloggList().size();
		d.getPdabrukerList().size();
		log.debug("getDriftkontraktForAdministration refresh("+id+")");
		log.debug("getDriftkontraktForAdministration finish ("+id+")");
		return d;
	}

	/** {@inheritDoc} */
	public Driftkontrakt oppdaterDriftkontrakt(Driftkontrakt driftkontrakt, String signatur) {
		
		//Sletter gamle relasjoner
		String idParameter = "id"; 
		for(KontraktLeverandor kontraktLeverandor : driftkontrakt.getKontraktLeverandorList()) {
			for(final LevProdtypeRode levProdtypeRode : kontraktLeverandor.getLeverandor().getSlettedeLevProdtypeRodeList()) {
				LevProdtypeRode fraDbLevProdtypeRode = em.find(LevProdtypeRode.class, 
						new LevProdtypeRodePK(levProdtypeRode.getLeverandorNr(), 
								levProdtypeRode.getRodeId(), 
								levProdtypeRode.getProdtypeId(), 
								levProdtypeRode.getFraDato()));
				//Om slettet LevProdtypeRode ikke er i basen, skal den ikke fjernes	
				if(fraDbLevProdtypeRode != null) {
					em.remove(fraDbLevProdtypeRode);
				}
			}
		}
		
		Query oldKontraktLeverandorQuery = em.createNamedQuery(KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT);
		oldKontraktLeverandorQuery.setParameter(idParameter, driftkontrakt.getId());
		List<KontraktLeverandor> oldKontraktLeverandorList = oldKontraktLeverandorQuery.getResultList();
		deleteOldEntities(oldKontraktLeverandorList, driftkontrakt.getKontraktLeverandorList());
		
		Query oldKontraktKontaktypeQuery = em.createNamedQuery(KontraktKontakttype.QUERY_FIND_FOR_KONTRAKT);
		oldKontraktKontaktypeQuery.setParameter(idParameter, driftkontrakt.getId());
		List<KontraktKontakttype> oldKontraktKontaktypeList = oldKontraktKontaktypeQuery.getResultList();
		deleteOldEntities(oldKontraktKontaktypeList, driftkontrakt.getKontraktKontaktypeList());
		
		Query oldKontraktStabQuery = em.createNamedQuery(KontraktStab.QUERY_FIND_FOR_KONTRAKT);
		oldKontraktStabQuery.setParameter(idParameter, driftkontrakt.getId());
		List<KontraktStab> oldKontraktStabList = oldKontraktStabQuery.getResultList();
		deleteOldEntities(oldKontraktStabList, driftkontrakt.getKontraktStabList());
		
		Query oldKontraktProsjektQuery = em.createNamedQuery(KontraktProsjekt.QUERY_FIND_FOR_KONTRAKT);
		oldKontraktProsjektQuery.setParameter(idParameter, driftkontrakt.getId());
		List<KontraktProsjekt> oldKontraktProsjektList = oldKontraktProsjektQuery.getResultList();
		deleteOldEntities(oldKontraktProsjektList, driftkontrakt.getProsjektKontraktList());
		
		Query oldPdabrukerKontraktQuery = em.createNamedQuery(PdabrukerKontrakt.QUERY_FIND_FOR_KONTRAKT);
		oldPdabrukerKontraktQuery.setParameter(idParameter, driftkontrakt.getId());
		List<PdabrukerKontrakt> oldPdabrukerKontraktList = oldPdabrukerKontraktQuery.getResultList();
		deleteOldEntities(oldPdabrukerKontraktList, driftkontrakt.getPdabrukerList());
		
		//Sletter evt FavorittProsesser som ikke er i slettede lister
		for(Favorittliste favorittliste : driftkontrakt.getFavorittlisteList()) {
			if(favorittliste.getLagreFavorittProsesser()) {
				Query oldFavorittProsessQuery = em.createNamedQuery(FavorittProsess.FIND_BY_FAVORITTLISTE);
				oldFavorittProsessQuery.setParameter(idParameter, favorittliste.getId());
				List<FavorittProsess> oldFavorittProsessList = oldFavorittProsessQuery.getResultList();
				deleteOldEntities(oldFavorittProsessList, favorittliste.getFavorittProsessList());
			}
		}
		
		Query oldFavorittlisteQuery = em.createNamedQuery(Favorittliste.FIND_BY_KONTRAKT);
		oldFavorittlisteQuery.setParameter(idParameter, driftkontrakt.getId());
		List<Favorittliste> oldFavorittlisteList = oldFavorittlisteQuery.getResultList();
		
		//Sletter FavorittProsess for slettede Favorittlister
		for(Favorittliste favorittliste : oldFavorittlisteList) {
			if(!driftkontrakt.getFavorittlisteList().contains(favorittliste)) {
				for(FavorittProsess favorittProsess : favorittliste.getFavorittProsessList()) {
					em.remove(favorittProsess);
				}
			}
		}
		
		//Fjerner de slettede Favorittlistene
		deleteOldEntities(oldFavorittlisteList, driftkontrakt.getFavorittlisteList());
		
		Query oldKontraktProsess = em.createNamedQuery(KontraktProsess.QUERY_FIND_BY_KONTRAKT);
		oldKontraktProsess.setParameter(idParameter, driftkontrakt.getId());
		List<KontraktProsess> oldKontraktProsessList = oldKontraktProsess.getResultList();
		deleteOldEntities(oldKontraktProsessList, driftkontrakt.getKontraktProsessList());
		
		driftkontrakt.setEndretAv(signatur);
		driftkontrakt.setEndretDato(Clock.now());
		
		driftkontrakt = em.merge(driftkontrakt);
		em.setFlushMode(FlushModeType.COMMIT);
        em.flush();
		return driftkontrakt;
	}

	/** {@inheritDoc} */
	public Driftkontrakt getDriftkontrakt(Long id) {
		return em.find(Driftkontrakt.class, id);
	}

	/** {@inheritDoc} */
	public List<Bruker> getBrukerList() {
		return em.createNamedQuery("Bruker.findAll").getResultList();
	}

	/** {@inheritDoc} */
	public List<KundeView> getKundeViewList(KundeQueryFilter kundeQueryFilter) {
		log.debug("getKundeViewList start("+kundeQueryFilter+")");
		List<KundeView> kundeViewList = null;
		
		if(kundeQueryFilter == null || !kundeQueryFilter.hasValues()) {
			kundeViewList = em.createNamedQuery(KundeView.QUERY_FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM KundeView o ";
			if(kundeQueryFilter.getKundeNr() != null && kundeQueryFilter.getKundeNr().length() > 0) {
				whereClauses.add("o.kundeNr = :nr");
				params.put("nr", kundeQueryFilter.getKundeNr());
			}
			
			if(kundeQueryFilter.getKundeNavn() != null && kundeQueryFilter.getKundeNavn().length() > 0) {
				whereClauses.add("UPPER(o.kundeNavn) LIKE :navn");
				params.put("navn", "%" + kundeQueryFilter.getKundeNavn().toUpperCase() + "%");
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params); 
			
			kundeViewList = query.getResultList();
		}
		log.debug("getKundeViewList finish");

		return kundeViewList;
	}
	/** {@inheritDoc} */
	public List<ArbeidsordreView> getArbeidsordreViewList(ProsjektQueryFilter prosjektQueryFilter) {
		log.debug("getArbeidsordreViewList start("+prosjektQueryFilter+")");

		List<ArbeidsordreView> arbeidsordreViewList = null;
		
		if(prosjektQueryFilter == null || !prosjektQueryFilter.hasValues()) {
			arbeidsordreViewList = em.createNamedQuery(ArbeidsordreView.QUERY_FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM ArbeidsordreView o ";
			if(prosjektQueryFilter.getProsjektnr() != null && prosjektQueryFilter.getProsjektnr().length() > 0) {
				whereClauses.add("o.arbeidsordrenr = :nr");
				params.put("nr", prosjektQueryFilter.getProsjektnr());
			}
			
			if(prosjektQueryFilter.getProsjektnavn() != null && prosjektQueryFilter.getProsjektnavn().length() > 0) {
				whereClauses.add("UPPER(o.prosjektnavn) LIKE :navn");
				params.put("navn", "%" + prosjektQueryFilter.getProsjektnavn().toUpperCase() + "%");
			}
			
			if(prosjektQueryFilter.getAnsvar() != null && prosjektQueryFilter.getAnsvar().length() > 0) {
				whereClauses.add("o.ansvar = :ansvar");
				params.put("ansvar", prosjektQueryFilter.getAnsvar());
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params); 
			
			arbeidsordreViewList = query.getResultList();
		}
		log.debug("getArbeidsordreViewList finish");

		return arbeidsordreViewList;
	}
	/** {@inheritDoc} */
	public List<ProsjektView> getProsjektViewList(ProsjektQueryFilter prosjektQueryFilter) {
		log.debug("getKundeViewList start("+prosjektQueryFilter+")");

		List<ProsjektView> prosjektViewList = null;
		
		if(prosjektQueryFilter == null || !prosjektQueryFilter.hasValues()) {
			prosjektViewList = em.createNamedQuery(ProsjektView.QUERY_FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM ProsjektView o ";
			if(prosjektQueryFilter.getProsjektnr() != null && prosjektQueryFilter.getProsjektnr().length() > 0) {
				whereClauses.add("o.prosjektnr = :nr");
				params.put("nr", prosjektQueryFilter.getProsjektnr());
			}
			
			if(prosjektQueryFilter.getProsjektnavn() != null && prosjektQueryFilter.getProsjektnavn().length() > 0) {
				whereClauses.add("UPPER(o.prosjektnavn) LIKE :navn");
				params.put("navn", "%" + prosjektQueryFilter.getProsjektnavn().toUpperCase() + "%");
			}
			
			if(prosjektQueryFilter.getAnsvar() != null && prosjektQueryFilter.getAnsvar().length() > 0) {
				whereClauses.add("o.ansvar = :ansvar");
				params.put("ansvar", prosjektQueryFilter.getAnsvar());
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params); 
			
			prosjektViewList = query.getResultList();
		}
		log.debug("getKundeViewList finish");

		return prosjektViewList;
	}
	
	/** {@inheritDoc} */
	public List<Bruker> getBrukerList(StabQueryFilter stabQueryFilter) {
		log.debug("getBrukerList start("+stabQueryFilter+")");

		List<Bruker> brukerList = null;
		
		if(stabQueryFilter == null || !stabQueryFilter.hasValues()) {
			brukerList = em.createNamedQuery("Bruker.findAll").getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM Bruker o ";
			
			if(stabQueryFilter.getNavn() != null && stabQueryFilter.getNavn().length() > 0) {
				whereClauses.add("UPPER(o.navn) LIKE :navn");
				params.put("navn", "%" + stabQueryFilter.getNavn().toUpperCase() + "%");
			}
			
			if(stabQueryFilter.getSignatur() != null && stabQueryFilter.getSignatur().length() > 0) {
				whereClauses.add("UPPER(o.signatur) LIKE :signatur");
				params.put("signatur", "%" + stabQueryFilter.getSignatur().toUpperCase() + "%");
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params); 
			
			brukerList = query.getResultList();
		}
		log.debug("getBrukerList finish");

		return brukerList;
	}
	
	/** {@inheritDoc} */
	public List<LevStedView> getLevStedViewList(LevStedQueryFilter levStedQueryFilter, boolean removeDuplicates) {
		log.debug("getBrukerList start("+levStedQueryFilter+")");

		List<LevStedView> levStedViewList = null;
		Set<String> levNrSet = new HashSet<String>();
		
		if(levStedQueryFilter == null || !levStedQueryFilter.hasValues()) {
			levStedViewList = em.createNamedQuery(LevStedView.QUERY_FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM LevStedView o ";
			if(levStedQueryFilter.getLevNr() != null && levStedQueryFilter.getLevNr().length() > 0) {
				whereClauses.add("o.levNr = :nr");
				params.put("nr", levStedQueryFilter.getLevNr());
			}
			
			if(levStedQueryFilter.getLevNavn() != null && levStedQueryFilter.getLevNavn().length() > 0) {
				whereClauses.add("UPPER(o.levNavn) LIKE :navn");
				params.put("navn", "%" + levStedQueryFilter.getLevNavn().toUpperCase() + "%");
			}
			
			if(levStedQueryFilter.getAdresse1() != null && levStedQueryFilter.getAdresse1().length() > 0) {
				whereClauses.add("UPPER(o.adresse1) LIKE :adresse1");
				params.put("adresse1", "%" + levStedQueryFilter.getAdresse1().toUpperCase() + "%");
			}
			
			if(levStedQueryFilter.getPostnr() != null && levStedQueryFilter.getPostnr().length() > 0) {
				whereClauses.add("o.postnr = :postnr");
				params.put("postnr", levStedQueryFilter.getPostnr());
			}
			
			if(levStedQueryFilter.getPoststed() != null && levStedQueryFilter.getPoststed().length() > 0) {
				whereClauses.add("UPPER(o.poststed) LIKE :poststed");
				params.put("poststed", "%" + levStedQueryFilter.getPoststed().toUpperCase() + "%");
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params);
			
			levStedViewList = query.getResultList();
		}
		
		if(removeDuplicates) {
			for(Iterator<LevStedView> it = levStedViewList.iterator(); it.hasNext();) {
				LevStedView levStedView = it.next();
				if(levNrSet.contains(levStedView.getLevNr())) {
					it.remove();
				} else {
					levNrSet.add(levStedView.getLevNr());
				}
			}
		}
		log.debug("getBrukerList finish");

		return levStedViewList;
	}
	
	/** {@inheritDoc} */
	public List<LevStedView> getLevStedViewListUnique(LevStedQueryFilter levStedQueryFilter) {
		return getLevStedViewList(levStedQueryFilter, true);
	}
	
	/** {@inheritDoc} */
	public List<Rodetype> getRodetypeForKontrakt(Long kontraktId) {
		return rodegenerator.getRodetypeForKontrakt(kontraktId);
	}
	
	/** {@inheritDoc} */
	public List<Rode> getRodeList(Long kontraktId, Long typeId) {
		return em.createNamedQuery(Rode.QUERY_FIND_BY_KONTRAKT_AND_TYPE)
		.setParameter("kontraktId", kontraktId)
		.setParameter("rodetypeId", typeId)
		.getResultList();
	}
	
	public List<LevProdtypeRode> getLeverandorList(Long rodeId){
		return em.createNamedQuery(LevProdtypeRode.QUERY_FIND_BY_RODE)
		.setParameter("id", rodeId)
		.getResultList();
		
	}
	
	public List<Leverandor> getLeverandorListKontrakt(Long kontraktId){
		Set<Leverandor> leverandorList = new HashSet<Leverandor>();
		List<KontraktLeverandor> levProdtypeList = em.createNamedQuery(KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT)
		.setParameter("id", kontraktId)
		.getResultList();
		for (KontraktLeverandor lp:levProdtypeList){
			Leverandor l = lp.getLeverandor();
			leverandorList.add(l);
		}
		return new ArrayList<Leverandor>(leverandorList);
	}
	
	/** {@inheritDoc} */
	public Leverandor opprettLeverandor(String leverandorNr) {
		//Sjekker om leverandøren eksisterer
		Leverandor leverandor = em.find(Leverandor.class, leverandorNr);
		
		if(leverandor == null) {
			LevStedQueryFilter levStedQueryFilter = new LevStedQueryFilter();
			levStedQueryFilter.setLevNr(leverandorNr.toString());
			List<LevStedView> levStedViewList = getLevStedViewList(levStedQueryFilter, true);
			if(levStedViewList != null && levStedViewList.size() > 0) {
				LevStedView levStedView = levStedViewList.get(0);
				leverandor = new Leverandor();
				leverandor.setNr(levStedView.getLevNr());
				leverandor.setNavn(levStedView.getLevNavn());
				leverandor.setValgbarFlagg(Boolean.TRUE);
				em.merge(leverandor);
			}
		}
		
		return leverandor;
	}
	/** {@inheritDoc} */
	public void opprettKontraktKjoretoy(Long kontraktId, Long kjoretoyId, Long ansvarlig, String brukerSign){
		
		Kjoretoy kjoretoy = em.find(Kjoretoy.class, kjoretoyId);
		em.refresh(kjoretoy); //Kjører denne for å være sikker på å ha en fersk kopi fra basen

		Driftkontrakt d = em.find(Driftkontrakt.class, kontraktId);
			
		KontraktKjoretoy kk =new KontraktKjoretoy();
		kk.setKjoretoy(kjoretoy);
		kk.setDriftkontrakt(d);
		kk.setAnsvarligFlagg(ansvarlig);
		
		kjoretoy.getKontraktKjoretoyList().size();
		kjoretoy.getKontraktKjoretoyList().add(kk);
				
		Kjoretoylogg logg = new Kjoretoylogg();
		logg.setOpprettetAv(brukerSign);
		logg.setOpprettetDato(Clock.now());
		logg.setTekst("Koblet til kontrakt:"+d.getKontraktnummer()+" "+d.getKontraktnavn()+" ansvarlig="+(ansvarlig==1?"Ja":"Nei"));
		kjoretoy.addKjoretoylogg(logg);
		oppdaterKjoretoy(kjoretoy);
		
		
		Leverandor leverandor = kjoretoy.getLeverandor();
		try{
			Object obj = em.createNamedQuery(KontraktLeverandor.QUERY_FIND_FOR_KONTRAKT_LEVERANDOR)
			.setParameter("id", d.getId())
			.setParameter("leverandorNr", leverandor.getNr())
			.getSingleResult();
			System.out.println(obj);
		} catch (NoResultException e){
			KontraktLeverandor kontraktLeverandor = new KontraktLeverandor();
			kontraktLeverandor.setDriftkontrakt(d);
			kontraktLeverandor.setLeverandor(leverandor);
			kontraktLeverandor.setFraDato(d.getGyldigFraDato());
			kontraktLeverandor.setTilDato(d.getGyldigTilDato());
			leverandor.getKontraktLeverandorList().add(kontraktLeverandor);
			d.getKontraktLeverandorList().add(kontraktLeverandor);
		}
	}
	
	public void fjernKontraktKjoretoy(Long kontraktId, Long kjoretoyId, String brukerSign){

		//sletter tilknyttede gruppering for dette kjøretøyet på angitt kontrakt
		try{
			List<KontraktKjoretoyGruppering> gruppering = em.createNamedQuery(KontraktKjoretoyGruppering.FIND_BY_KONTRAKT_KJORETOY)
			.setParameter("id", kontraktId)
			.setParameter("kjoretoyId", kjoretoyId).getResultList();
			for (KontraktKjoretoyGruppering gruppe:gruppering){
				em.remove(gruppe);
			} em.flush();
		} catch (NoResultException e){
			//DO NOTHING..
		}

		Kjoretoy kjoretoy = em.find(Kjoretoy.class, kjoretoyId);
		em.refresh(kjoretoy); //Kjører denne for å være sikker på å ha en fersk kopi fra basen
		KontraktKjoretoy kontraktKjoretoy = (KontraktKjoretoy)em.createNamedQuery(KontraktKjoretoy.FIND_BY_KJORETOY_KONTRAKT)
		.setParameter("kjoretoyId", kjoretoyId).setParameter("kontraktId", kontraktId).getSingleResult();

		Driftkontrakt d = kontraktKjoretoy.getDriftkontrakt();
		Kjoretoylogg logg = new Kjoretoylogg();
		logg.setKjoretoy(kjoretoy);
		logg.setOpprettetAv(brukerSign);
		logg.setOpprettetDato(Clock.now());
		logg.setTekst("Koblet fra kontrakt:"+d.getKontraktnummer()+" "+d.getKontraktnavn()+" ansvarlig="+(kontraktKjoretoy.getAnsvarligFlagg()==1?"Ja":"Nei"));
		kjoretoy.addKjoretoylogg(logg);		
		kjoretoy.getKontraktKjoretoyList().remove(kontraktKjoretoy);
		mipssKjoretoy.updateKjoretoy(kjoretoy);
	}
	
	
	/** {@inheritDoc} */
	public Boolean sjekkOmKontraktKjoretoyFinnes(Long kontraktId, Long kjoretoyId){
		Boolean rvl = Boolean.TRUE;
		try{
			em.createNamedQuery(Kjoretoy.FIND_BY_KONTRAKT_AND_KJORETOY)
			.setParameter("id", kontraktId)
			.setParameter("kid", kjoretoyId)
			.getSingleResult();
		} catch (NoResultException e){
			rvl = Boolean.FALSE;
		}
		return rvl;
	}
    /** {@inheritDoc} */
    public List<Prodtype> getProdtypeList() {
    	return em.createNamedQuery(Prodtype.FIND_ALL).getResultList();
    }
	
    /** {@inheritDoc} */
    public List<Kontraktkontakttype> getKontraktkontakttypeList() {
    	return em.createNamedQuery(Kontraktkontakttype.FIND_ALL).getResultList();
    }
    
	private Query createQueryFromString(String queryString, List<String> whereClauses, Map<String, Object> params) {
		Iterator<String> whereIt = whereClauses.iterator();
		if(whereIt.hasNext()) queryString += " WHERE " + whereIt.next();
		while(whereIt.hasNext()) queryString += " AND " + whereIt.next();
		
		Query query = em.createQuery(queryString);
		for(Iterator<String> it = params.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			query.setParameter(key, params.get(key));
		}
		return query;
	}

	public List<Driftkontrakt> getDriftkontraktOnLocation(Point point) {
		String query = "SELECT d FROM Driftkontrakt d where d.areal.x1<:px and d.areal.y1>:py and d.areal.x1+d.areal.deltaX>:px and d.areal.y1-d.areal.deltaY<:py";
		return em.createQuery(query).setParameter("px", point.getX()).setParameter("py", point.getY()).getResultList();
	}
	
	public Driftkontrakt getDriftkontraktOnVeiref(Veinettreflinkseksjon veiref, Point point) {
		List<Driftkontrakt> list = getDriftkontraktOnLocation(point);
		for (Driftkontrakt d:list){
			System.out.println(d);
			List<Veinettreflinkseksjon> resultList = em.createQuery("SELECT v from Veinettreflinkseksjon v where v.veinett.id =:id").setParameter("id", d.getVeinettId()).getResultList();
			sortReflinkseksjon(resultList);
			if (matchInList(veiref, resultList)){
				return d;
			}
		}
		return null;
	}
	
	private boolean matchInList(Veinettreflinkseksjon veiref, List<Veinettreflinkseksjon> list){
		for (Veinettreflinkseksjon v:list){			
			if (match(veiref, v)){
				return true;
			}
		}
		return false;
	}
	private boolean match(Veinettreflinkseksjon veiref, Veinettreflinkseksjon v){
		if (v.getReflinkIdent().longValue()!=veiref.getReflinkIdent().longValue()){
			return false;
		}
		double fra = v.getFra();
		double til = v.getTil();

		//overlapp
		if ( (fra>=veiref.getFra()&&fra<=veiref.getTil()) || (til<=veiref.getTil()&&til>=veiref.getFra())){
			return true;
		}
		return false;
	}
	
	/**
	 * Sorterer listen etter 1. reflinkId 2. fra 3. til
	 * @param list
	 */
    private void sortReflinkseksjon(List<Veinettreflinkseksjon> list){
    	
    	Collections.sort(list, new Comparator<Veinettreflinkseksjon>(){
			public int compare(Veinettreflinkseksjon o1, Veinettreflinkseksjon o2) {
				long vn1 = o1.getReflinkIdent();
				long vn2 = o2.getReflinkIdent();
				
				double fra1 = o1.getFra();
				double fra2 = o2.getFra();
				
				double til1 = o1.getTil();
				double til2 = o2.getTil();
				
				int result = (vn1==vn2)? 0 : ((vn1<vn2)?-1:1);
				if (result==0){
					result = (fra1==fra2)?0: ((fra1<fra2)?-1:1);
					if (result==0){
						result = (til1==til2)?0:((til1<til2)?-1:1);
					}
				}
				return result;
			}
    	});
    }

	public Kjoretoy getKjoretoy(Long kjoretoyId) {
		return mipssKjoretoy.getWholeKjoretoy(kjoretoyId);
	}

	public List<Installasjon> getInstallasjon(Long kjoretoyId, Date fra, Date til){
		List<Installasjon> installasjon = em.createNamedQuery(Installasjon.QUERY_FIND).setParameter("kjoretoyId", kjoretoyId).setParameter("fraDato", fra).setParameter("tilDato", til).getResultList();
		return installasjon;
		
	}
	public Kjoretoy oppdaterKjoretoy(Kjoretoy kjoretoy) {
		kjoretoy = mipssKjoretoy.updateKjoretoy(kjoretoy);		
		return kjoretoy;
	}

	/** {@inheritDoc} */
	public List<KontraktKjoretoyInfo> getKontraktKjoretoyInfo(Long kontraktId, Date prodFromDate, Date prodToDate) {
		StringBuilder q = new StringBuilder();
		q.append("select i.kjoretoy_id, i.ansvarlig_flagg, i.regnr, i.maskinnummer, i.eksternt_navn, i.type, i.merke, i.modell, i.aar, ");
		q.append("i.leverandor_nr, i.leverandor_navn, i.leverandor_valgbar_flagg, i.bestilling_dato, i.bestilling_status, i.bestillingstatus_dato, ");
		q.append("i.ukjent_produksjon_flagg, i.ukjent_strometode_flagg, i.dfu_id, i.dfu_serienummer, i.dfu_tlfnummer, ");
		q.append("i.dfu_statusnavn, i.dfu_siste_livstegn_dato, i.dfu_siste_posisjon_dato, i.dfu_siste_prod_posisjon_dato, ");
		q.append("i.aktuell_konfig_klartekst, i.aktuell_stroperiode_klartekst, i.aktuell_stroperiode_fra_dato, ");
		q.append("i.aktuell_stroperiode_til_dato, i.spr_med_datafangst_flagg, i.spr_uten_datafangst_flagg, ");
		q.append("i.aktuell_klippeperiode_tekst, i.aktuell_klippeperiode_fra_dato, i.aktuell_klippeperiode_til_dato, i.kantklipp_flagg, ");
		q.append("ke.navn KJORETOY_EIER_NAVN ");
		q.append("from table (kjoretoy_pk.kontrakt_kjoretoy_info(?1, ?2, ?3)) i "); 
		q.append("left join kjoretoy k on k.id=kjoretoy_id ");
		q.append("left join kjoretoyeier ke on k.eier_id = ke.id ");
		
		Query query = em.createNativeQuery(q.toString(), "KontraktKjoretoyInfoResult");
		query.setParameter(1, kontraktId);
		query.setParameter(2, prodFromDate);
		query.setParameter(3, prodToDate);
		return query.getResultList();
	}
	/** {@inheritDoc} */
	public List<Kjoretoy> getKjoretoyForKontraktLeverandor(KontraktLeverandor kl){
		List<KontraktKjoretoy> results = em.createNamedQuery(KontraktKjoretoy.FIND_BY_KONTRAKT_LEVERANDOR)
			.setParameter("id", kl.getKontraktId())
			.setParameter("leverandorNr", kl.getLeverandorNr())
			.getResultList();
		List<Kjoretoy> kjoretoy = new ArrayList<Kjoretoy>();
		for (KontraktKjoretoy k:results){
			kjoretoy.add(k.getKjoretoy());
		}
		return kjoretoy;
	}
	/** {@inheritDoc} */
	public Long getAntallKjoretoyForKontraktLeverandor(KontraktLeverandor kl){
		Long antall = (Long)em.createNamedQuery(KontraktKjoretoy.FIND_BY_KONTRAKT_LEVERANDOR_ANTALL)
		.setParameter("id", kl.getDriftkontrakt().getId())
		.setParameter("leverandorNr", kl.getLeverandorNr()).getSingleResult();
		return antall;
	}

	/** {@inheritDoc} */
	public List<KontraktKjoretoyDogn> getKontraktKjoretoyDognListAkk(Long kontraktId, Date fromDogn, Date toDogn) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		StringBuilder sb = new StringBuilder("select kontrakt_id, kjoretoy_id, sum(tomkjoring_km) tomkjoring, sum(total_km) total ");
		sb.append("from kontrakt_kjoretoy_dogn ");
		sb.append("where kontrakt_id = ?1 ");
		sb.append("and dogn >= to_date(?2, 'dd.MM.yyyy') ");
		sb.append("and dogn <= to_date(?3, 'dd.MM.yyyy') ");
		sb.append("group by kontrakt_id, kjoretoy_id");
		
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, kontraktId);
		q.setParameter(2, sdf.format(fromDogn));
		q.setParameter(3, sdf.format(toDogn));
		
		List<KontraktKjoretoyDogn> kontraktkjoretoyDognList = 
			new NativeQueryWrapper<KontraktKjoretoyDogn>(q, KontraktKjoretoyDogn.class, new Class[]{Long.class, Long.class, Double.class, Double.class}, 
					"kontraktId", "kjoretoyId", "tomkjoringKm", "totalKm").getWrappedList();
    	
		return kontraktkjoretoyDognList;
	}

	/** {@inheritDoc} */
	public Long getKontraktKjoretoyProduksjonAntallOverPeriod(Long kontraktId, Date fraDato, Date tilDato) {
		StringBuilder sb = new StringBuilder("select count(*) ");
		sb.append("from kjoretoy k ");
		sb.append("where id in ");
		sb.append("(select kjoretoy_id ");
        sb.append("from kjoretoy_v  ");
        sb.append("where kontrakt_id = ?1) ");
		sb.append("and exists ");
		sb.append("(select 'x' ");
		sb.append("from rode rd ");
		sb.append("join rodestrekning rs on rs.rode_id = rd.id ");
		sb.append("join tiltak tt on rs.strekning_id = tt.strekning_id ");
		sb.append("where     rd.kontrakt_id = ?1 "); 
		sb.append("and rd.type_id = 15 "); // konfigparam FELLES/stro_broyte_rodetype_id 
		sb.append("and tt.kjoretoy_id = k.id ");
		sb.append("and tt.fra_dato_tid >= ?2 ");
		sb.append("and tt.prodtype_id != 98) "); // 98 = tomkjøring    

		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, kontraktId);
		q.setParameter(2, fraDato);
		return ((BigDecimal)q.getSingleResult()).longValue();
	}
	/** {@inheritDoc} **/
	public Long getAntallBroytekilometerForKontrakt(Long kontraktId, Date fraDato, Date tilDato){
		StringBuilder sb = new StringBuilder("select sum (rs.kjort_km) "); //#km brøytet
		sb.append("from rodestrekning rs ");
		sb.append("join tiltak tt on rs.strekning_id = tt.strekning_id ");
		sb.append("join rode rd on rs.rode_id = rd.id ");
        sb.append("where rd.kontrakt_id = ?1 ");
        sb.append("and rd.type_id = 15 "); // konfigparam FELLES/stro_broyte_rodetype_id
        sb.append("and rs.fra_dato_tid >= ?2 "); 
		sb.append("and tt.prodtype_id in (select id ");
		sb.append("from prodtype ");
		sb.append("where dau_broyte_flagg = 1) "); //  -- Sideplog, brøyting, høvling

		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, kontraktId);
		q.setParameter(2, fraDato);
		Object result = q.getSingleResult();
		if(result!=null){
			return ((BigDecimal)q.getSingleResult()).longValue();
		}else{
			return new Long(0);
		}
	}

	/** {@inheritDoc} */
	public Long getKontraktKjoretoyTotaltAntall(Long kontraktId){
		StringBuilder sb = new StringBuilder("select count(*) ");
		sb.append("from kjoretoy_v ");
		sb.append("where kontrakt_id = ?1 ");
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, kontraktId);
		return ((BigDecimal)q.getSingleResult()).longValue() ;
	}
	
	/** {@inheritDoc} */
	public List<KontraktKontakttype> getKontraktKontaktypeListForKontrakt(Long kontraktId) {
		Query kontraktKontaktypeQuery = em.createNamedQuery(KontraktKontakttype.QUERY_FIND_FOR_KONTRAKT);
		kontraktKontaktypeQuery.setParameter("id", kontraktId);
		return kontraktKontaktypeQuery.getResultList();
	}

	/** {@inheritDoc} */
	public List<Prosess> getProsessForKontrakt(Long kontraktId, boolean gyldigForArbeid){
		Query kontraktProsessQuery = gyldigForArbeid?em.createNamedQuery(KontraktProsess.QUERY_FIND_BY_KONTRAKT_GYLDIG_FOR_ARBEID):em.createNamedQuery(KontraktProsess.QUERY_FIND_BY_KONTRAKT);
		kontraktProsessQuery.setParameter("id", kontraktId);
		List<KontraktProsess> alleProsesser = kontraktProsessQuery.getResultList();
		List<Prosess> kontraktensProsesser = new ArrayList<Prosess>();
		for (KontraktProsess kp:alleProsesser){
			kontraktensProsesser.add(kp.getProsess());
		}
		
		return kontraktensProsesser;//
	}
	public Prosess getProsessMedId(Long prosessId){
		return em.find(Prosess.class, prosessId);
	}
	/** {@inheritDoc} */
	@SuppressWarnings("rawtypes")
	public Boolean getProsessIBrukPaKontrakt(Long driftkontraktId, Long prosessId){
		StringBuilder sb = new StringBuilder();
		sb.append("select ( ");
		sb.append("  case when exists( ");
		sb.append("    select 'inspeksjon' ");
		sb.append("      from inspeksjon i ");
		sb.append("     join inspeksjon_prosess ip on ip.inspeksjon_guid= i.guid and ip.prosess_id=?1 and i.slettet_dato is null "); 
		sb.append("     join driftkontrakt d on d.id= i.kontrakt_id and d.id=?2 ");
		sb.append("    UNION ");
		sb.append("    select 'punktreg' from avvik a ");
		sb.append("		join sak s on s.guid = a.sak_guid ");
		sb.append("      where a.prosess_id = ?1 ");
		sb.append("      and s.kontrakt_id=?2 ");
		sb.append("      and a.slettet_dato is null ");
		sb.append("  ) ");
		sb.append("  then 1 else 0 end ");
		sb.append("  ) prosess_i_bruk "); 
		sb.append(" from dual ");
		
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, prosessId);
		q.setParameter(2, driftkontraktId);
		
		List resultList = q.getResultList();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			BigDecimal prosessIBruk= (BigDecimal)o;
    			return Boolean.valueOf(prosessIBruk.intValue()==1);
    		}
    	}
    	return Boolean.FALSE;
	}
	/** {@inheritDoc} */
	@SuppressWarnings("rawtypes")
	public Boolean getProsessErDelAvFavorittListe(Long driftkontraktId, Long prosessId){
		StringBuilder sb = new StringBuilder();
		sb.append("select ( ");
		sb.append("  case when exists( ");
		sb.append(" select * from prosess p ");
		sb.append(" join favoritt_prosess fp on fp.prosess_id=p.id and fp.prosess_id=?1 ");
		sb.append(" join favorittliste fl on fl.id= fp.liste_id and fl.kontrakt_id=?2 ");
		sb.append(" ) then 1 else 0 end) i_favoritt_liste ");
		sb.append(" from dual ");
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, prosessId);
		q.setParameter(2, driftkontraktId);
		List resultList = q.getResultList();
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			BigDecimal prosessIBruk= (BigDecimal)o;
    			return Boolean.valueOf(prosessIBruk.intValue()==1);
    		}
    	}
    	return Boolean.FALSE;
	}
	/** {@inheritDoc} */
	public List<KontraktProsess> getKontraktProsessForKontrakt(Long kontraktId){
		Query kontraktProsessQuery = em.createNamedQuery(KontraktProsess.QUERY_FIND_BY_KONTRAKT);
		kontraktProsessQuery.setParameter("id", kontraktId);
		return kontraktProsessQuery.getResultList();
	}
	/** {@inheritDoc} */
	public List<Prosessett> getAlleProsessett(){
		Query q = em.createNamedQuery(Prosessett.QUERY_FIND_ALL);
		List<Prosessett> prosessettList = q.getResultList();
		for (Prosessett p:prosessettList){
			p.getProsessList().size();
		}
		return prosessettList;
	}
	/** {@inheritDoc} */
	public List<Prosessett> getAlleProsessettBaked(){
		Query q = em.createNamedQuery(Prosessett.QUERY_FIND_ALL);
		
		List<Prosessett> prosessettList = q.getResultList();
		List<Prosessett> newList = new ArrayList<Prosessett>();
		
		for (Prosessett p:prosessettList){
			List<Prosess> tempProsessList = new ArrayList<Prosess>();	
			for (Prosess pp:p.getProsessList()){
				pp = (Prosess) DetachmentHelper.detach(pp);
				tempProsessList.add(pp);
			}
			p = (Prosessett)DetachmentHelper.detach(p);
			p.setProsessList(new ProsessHelper().createTree(tempProsessList));
			newList.add(p);
		}
		
		return newList;
	}
	
	/** {@inheritDoc} */
	public List<FavorittProsess> getFavorittProsessListForFavorittliste(Long listeId) {
		Query favorittProsessQuery = em.createNamedQuery(FavorittProsess.FIND_BY_FAVORITTLISTE);
		favorittProsessQuery.setParameter("id", listeId);
		return favorittProsessQuery.getResultList();
	}

	/** {@inheritDoc} */
	public List<LevProdtypeRode> getLevProdtypeRodeListForLeverandorAndKontrakt(String leverandorNr, Long kontraktId) {
		Query levProdtypeRodeQuery = em.createNamedQuery(LevProdtypeRode.QUERY_FIND_FOR_LEV_AND_KONTRAKT);
		levProdtypeRodeQuery.setParameter("leverandorNr", leverandorNr);
		levProdtypeRodeQuery.setParameter("kontraktId", kontraktId);
		return levProdtypeRodeQuery.getResultList();
	}

	/** {@inheritDoc} */
	public Driftkontrakt getDriftkontraktForKjoretoysordre(Long kjoretoyordreId) {
		String query = "select distinct d from Driftkontrakt d join fetch d.kontraktKontaktypeList, KontraktKjoretoy kk, Kjoretoyordre ko " +
				"where d.id = kk.kontraktId " +
				"and kk.kjoretoyId = ko.kjoretoy.id " +
				"and kk.ansvarligFlagg = 1 " +
				"and ko.id = :id";
		Driftkontrakt d;
		try {
			d = (Driftkontrakt)em.createQuery(query).setParameter("id", kjoretoyordreId).getSingleResult();
		} catch (NoResultException e) {
			d = null;
		}
		return d;
	}

	@Override
	public List<Pdabruker> sokPdabruker(String navn) {
		List<Pdabruker> pdabrukere = em.createNamedQuery(Pdabruker.QUERY_FIND_BY_NAVN).setParameter("navn", "%"+navn.toLowerCase()+"%").getResultList();
		
		return pdabrukere;
	}

	@Override
	public void updatePdabruker(Pdabruker pdabruker) {
		em.merge(pdabruker);
		em.flush();
	}
	
	/** {@inheritDoc} */
//	public boolean isLeverandorOverlappendePeriode(LevStedView levStedView, Driftkontrakt driftkontrakt, Date datoFra, Date datoTil){
//		Driftkontrakt d = em.merge(driftkontrakt);
//		if(d.getKontraktLeverandorList() != null && d.getKontraktLeverandorList().size() > 0) {
//			for(KontraktLeverandor kontraktLeverandor : d.getKontraktLeverandorList()) {
//				if(levStedView.getLevNr().equals(kontraktLeverandor.getLeverandorNr()) 
//						&& overlappingPeriods(kontraktLeverandor.getFraDato(), kontraktLeverandor.getTilDato(), datoFra, datoTil)) {
//					return false;
//				}
//			}
//		}
//		
//		return true;
//	}
	
//	public Driftkontrakt getKontraktLeverandorEager(Driftkontrakt d){
//		d = em.merge(d);
//		if (d.getKontraktLeverandorList()!=null){
//			d.getKontraktLeverandorList().size();
//		}
//		return d;
//	}
//	public static boolean overlappingPeriods(Date period1Start, Date period1End, Date period2Start, Date period2End) {
//		long l1 = period1End.getTime() - period2Start.getTime();
//		long l2 = period2End.getTime() - period1Start.getTime();
//		long l3 = period1End.getTime() - period1Start.getTime() + period2End.getTime() - period2Start.getTime();
//		return l1 < l3 && l2 < l3;
//	}
}
