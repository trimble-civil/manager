package no.mesta.driftkontrakt.bean;

import java.awt.Point;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyDogn;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.Kontrakttype;
import no.mesta.mipss.persistence.kontrakt.KundeView;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.kontrakt.kontakt.Kontraktkontakttype;
import no.mesta.mipss.persistence.mipssfield.FavorittProsess;
import no.mesta.mipss.persistence.mipssfield.KontraktProsess;
import no.mesta.mipss.persistence.mipssfield.Pdabruker;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Prosessett;
import no.mesta.mipss.persistence.organisasjon.ArbeidsordreView;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;

/**
 * Bean interface for å bruke kontrakter
 * 
 * @author Harald A Kulø
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Remote
public interface MaintenanceContract {
    public static final String BEAN_NAME = "MaintenanceContractBean";
    
    
    
	public List<Installasjon> getInstallasjon(Long kjoretoyId, Date fra, Date til);
    /**
     * Henter driftkontraktene som er tilknyttet brukeren.
     * @param bruker
     * @return
     */
    List<Driftkontrakt> getDriftkontraktList(Bruker bruker);
    
    public List<Driftkontrakt> getDriftkontraktList();
    
    public List<Kjoretoyordre> getOrdreListForKontraktId(long kontraktId);
    
	/**
	 * Henter alle registrerte driftskontrakter sortert etter navn.
	 * @return
	 */
	public List<Driftkontrakt> getDriftkontraktListByname();
	
	/**
	 * Finner en driftkontrakt på en gitt geoposisjon, kan returnere flere driftkontrakter
	 * ettersom 
	 * @param point
	 * @return
	 */
	List<Driftkontrakt> getDriftkontraktOnLocation(Point point);
	
	/**
	 * Finner ut hvilken driftkontrakt som har et veinett som veiref er en del av
	 * @param veiref
	 * @param point
	 * @return
	 */
	Driftkontrakt getDriftkontraktOnVeiref(Veinettreflinkseksjon veiref, Point point);
	/**
	 * Henter alle kontrakter som enten ikke har en tilDato eller som
	 * hvis tildato er gyldig etter dagens dato.
	 * @param limit
	 * @return
	 */
	public List<Driftkontrakt> getValidDriftkontraktList();
	
	
	/**
	 * Henter alle {@code Kjoretoyordretype} i databasen som
	 * er merket som valgbar.
	 * @return
	 */
	public List<Kjoretoyordretype> getKjoretoyordretypeValidList();
	
	/**
	 * Henter alle {@code Kjoretoyordrestatus} i databasen s
	 * @return
	 */
	public List<Kjoretoyordrestatus> getKjoretoyordrestatusList();
	
	/**
	 * Henter alle driftsdistrikt
	 * @return
	 */
	public List<Driftdistrikt> getDriftdistriktList();
	
	/**
	 * Henter alle kontrakttyper
	 * @return
	 */
	public List<Kontrakttype> getKontrakttypeList();
	
	/**
	 * Henter {@code Veinett} for id 
	 * @return
	 */
	public Veinett getVeinettForDriftkontrakt(Long id);
	
	/**
	 * Henter en driftskontrakt med alt som trengs for å administrere den
	 * @return
	 */
	public Driftkontrakt getDriftkontraktForAdministration(Long id);
	
	/**
	 * Oppdaterer en driftskontrakt
	 * @param driftkontrakt
	 * @return
	 */
	public Driftkontrakt oppdaterDriftkontrakt(Driftkontrakt driftkontrakt, String signatur);
	
	/**
	 * Henter en driftkontrakt
	 * @param id
	 * @return
	 */
	public Driftkontrakt getDriftkontrakt(Long id);
	
	/**
	 * Henter brukere i systemet
	 * @return
	 */
	public List<Bruker> getBrukerList();
	
	/**
	 * Henter kunder basert på søkeverdier i filter. Brukes gjennom reflection
	 * 
	 * @param kundeQueryFilter
	 * @return
	 */
	public List<KundeView> getKundeViewList(KundeQueryFilter kundeQueryFilter);
	
	/**
	 * Henter arbeidsordre basert på søkeverdier i filter
	 * NB:prosjektQueryFilter.prosjektnr blir brukt som arbeidsordrenr
	 * @param prosjektQueryFilter
	 * @return
	 */
	List<ArbeidsordreView> getArbeidsordreViewList(ProsjektQueryFilter prosjektQueryFilter);
	/**
	 * Henter prosjekt basert på søkeverdier i filter
	 * @param prosjektQueryFilter
	 * @return
	 */
	public List<ProsjektView> getProsjektViewList(ProsjektQueryFilter prosjektQueryFilter);
	
	/**
	 * Henter brukere basert på søkeverdier i filter
	 * @param prosjektQueryFilter
	 * @return
	 */
	public List<Bruker> getBrukerList(StabQueryFilter stabQueryFilter);
	
	/**
	 * Henter leverandører basert på søkeverdier i filter
	 * @param levStedQueryFilter
	 * @return
	 */
	public List<LevStedView> getLevStedViewList(LevStedQueryFilter levStedQueryFilter, boolean removeDuplicates);
	
	/**
	 * Henter leverandører basert på søkeverdier i filter. Fjerner duplikater
	 * @param levStedQueryFilter
	 * @return
	 */
	public List<LevStedView> getLevStedViewListUnique(LevStedQueryFilter levStedQueryFilter);
	
	/**
	 * Henter rodetyper for en kontrakt
	 * @param kontraktId
	 * @return
	 */
	public List<Rodetype> getRodetypeForKontrakt(Long kontraktId);
	
	/**
	 * Henter roder for en kontrakt basert på rodetype
	 * @param kontraktId
	 * @param typeId
	 * @return
	 */
	public List<Rode> getRodeList(Long kontraktId, Long typeId);
	
	/**
	 * Henter leverandører som er tilknyttet en rode
	 * @param rodeId
	 * @return
	 */
	public List<LevProdtypeRode> getLeverandorList(Long rodeId);
	
	/**
	 * Henter leverandører som er tilknyttet kontrakten
	 * @param kontraktId
	 * @return
	 */
	public List<Leverandor> getLeverandorListKontrakt(Long kontraktId);
	
	/**
	 * Oppretter en leverandør og returnerer denne. Dersom leverandøren finnes returneres denne. 
	 * @param leverandorNr
	 * @return
	 */
	public Leverandor opprettLeverandor(String leverandorNr);
	
	/**
	 * Knytter et kjøretøy til en kontrakt
	 * @param kontraktId
	 * @param kjoretoyId
	 * @param ansvarlig 1 om denne kontrakten er ansvarlig
	 */
	void opprettKontraktKjoretoy(Long kontraktId, Long kjoretoyId, Long ansvarlig, String brukerSign);
	
	/**
	 * Sjekker om et kjøretøy er tilknyttet kontrakten 
	 * 
	 * @param kontraktId 
	 * @param kjoretoyId
	 * @return true hvis kjøretøyet er tilknyttet
	 */
	Boolean sjekkOmKontraktKjoretoyFinnes(Long kontraktId, Long kjoretoyId);
	
	/**
	 * Kobler et kjøretøy fra en kontrakt
	 * @param kontraktId
	 * @param kjoretoyId
	 * @param brukerSign
	 */
	void fjernKontraktKjoretoy(Long kontraktId, Long kjoretoyId, String brukerSign);
	
	/**
	 * Returnerer liste med alle prodtyper
	 * @return
	 */
	public List<Prodtype> getProdtypeList();
	
	/**
	 * Returnerer liste med alle kontraktkontakttyper
	 * @return
	 */
	public List<Kontraktkontakttype> getKontraktkontakttypeList();
	
	/**
	 * Lagrer et kjøretøy
	 * @param kjoretoy
	 * @return
	 */
	public Kjoretoy oppdaterKjoretoy(Kjoretoy kjoretoy);
	
	/**
	 * Henter et kjøretøy
	 * @param kjoretoyId
	 * @return
	 */
	public Kjoretoy getKjoretoy(Long kjoretoyId);
	
	/**
	 * 
	 * @param kontraktId
	 * @param prodFromDate 
	 * @param prodToDate 
	 * @return
	 */
	public List<KontraktKjoretoyInfo> getKontraktKjoretoyInfo(Long kontraktId, Date prodFromDate, Date prodToDate);
	
	/**
	 * Henter alle kjøretøyene en leverandør har tilknyttet en gitt driftkontrakt
	 * @param kl
	 * @return
	 */
	List<Kjoretoy> getKjoretoyForKontraktLeverandor(KontraktLeverandor kl);
	
	/**
	 * Henter antall kjøretøy en leverandør har tilknyttet en gitt kontrakt.
	 * @param kl
	 * @return
	 */
	Long getAntallKjoretoyForKontraktLeverandor(KontraktLeverandor kl);
	
	/**
	 * Henter ut KontraktKjoretoyDogn for en kontrakt for en gitt periode.
	 * Dersom perioden strekker seg over flere døgn vil data for hvert kjøretøy akkumuleres opp i ett KontraktKjoretoyDogn
	 * 
	 * @param kontraktId
	 * @param fromDogn
	 * @param toDogn
	 * @return
	 */
	public List<KontraktKjoretoyDogn> getKontraktKjoretoyDognListAkk(Long kontraktId, Date fromDogn, Date toDogn);
	
	/**
	 * Spesialmetode som teller opp antall kjøretøy som har produksjon innenfor denne kontrakten, innenfor angitt periode.
	 * Metoden kalles fra Dashboard og skal returnere datasett over spesifikke perioder.
	 * @param  fraDato
	 * @param tilDato
	 * @param dagSelector = Siden kl 15:00 i går, i går (24t) (kan utvides)
	 * @return
	 */
	public Long getKontraktKjoretoyProduksjonAntallOverPeriod(Long kontraktId,  Date fraDato, Date tilDato);
	
	/**
	 * Søker opp og summerer antall kilometer brøyting for en gitt kontrakt over et tidsrom
	 * @param kontraktId
	 * @param fraDato 
	 * @param tilDato
	 * @return
	 */
	public Long getAntallBroytekilometerForKontrakt(Long kontraktId,  Date fraDato, Date tilDato);
	
	/**
	 * Henter ut det totale antall kjøretøy på en kontrakt.
	 * @param kontraktId
	 * @return
	 */
	public Long getKontraktKjoretoyTotaltAntall(Long kontraktId);
	
	/**
	 * Henter alle KontraktKontaktyper for en kontrakt
	 * 
	 * @param kontraktId
	 * @return
	 */
	public List<KontraktKontakttype> getKontraktKontaktypeListForKontrakt(Long kontraktId);	
	
	/**
	 * Henter alle prosessettene, prosessene ligger som en flat liste
	 * @return
	 */
	List<Prosessett> getAlleProsessett();
	/**
	 * Henter alle prosessettene, sørger for at prosessene kun har underprosesser som inngår i prosessettet. 
	 * Samtlige underprosesser vil få med alle eierprosessene opp til hovedprosessen
	 * @return
	 */
	List<Prosessett> getAlleProsessettBaked();
	/**
	 * Henter prosesser som er knyttet til kontrakten. 
	 * 
	 * @param kontraktId
	 * @param gyldigForArbeid true hvis man skal ha med prosesser som er gyldige for arbeid (false gir alle prosesser tilknyttet kontrakten)
	 * @return
	 */
	List<Prosess> getProsessForKontrakt(Long kontraktId, boolean gyldigForArbeid);
	
	/**
	 * Henter en prosess med gitt id
	 * @param prosessId
	 * @return
	 */
	Prosess getProsessMedId(Long prosessId);
	/**
	 * Sjekker om det er gjort funn eller inspeksjoner på prosessId og angitt kontrakt
	 * @param driftkontraktId
	 * @param prosessId
	 * @return
	 */
	Boolean getProsessIBrukPaKontrakt(Long driftkontraktId, Long prosessId);

	/**
	 * Sjekker om prosessen er en del av kontraktens favorittliste
	 * @param driftkontraktId
	 * @param prosessId
	 * @return
	 */
	Boolean getProsessErDelAvFavorittListe(Long driftkontraktId, Long prosessId);
	
	/**
	 * Henter koblingen mellom kontrakt og prosess for en gitt kontrakt
	 * @param kontraktId
	 * @return
	 */
	List<KontraktProsess> getKontraktProsessForKontrakt(Long kontraktId);
	/**
	 * Henter alle FavorittProsesser for en Favorittliste
	 * 
	 * @param listeId
	 * @return
	 */
	public List<FavorittProsess> getFavorittProsessListForFavorittliste(Long listeId);
	
	/**
	 * Returnerer alle LevProdtypeRode for en gitt leverandør på en git kontrakt
	 * 
	 * @param leverandorId
	 * @param kontraktId
	 * @return
	 */
	public List<LevProdtypeRode> getLevProdtypeRodeListForLeverandorAndKontrakt(String leverandorNr, Long kontraktId);
	
	/**
	 * Henter en kontrakt for en kjøretøysordre
	 * 
	 * @param kjoretoyordreId
	 * @return
	 */
	public Driftkontrakt getDriftkontraktForKjoretoysordre(Long kjoretoyordreId);
	
	/**
	 * Søker etter brukere i ad eller i mipss.pdabruker tabellen.
	 * @param navn
	 * @return
	 */
	List<Pdabruker> sokPdabruker(String navn);
	
	void updatePdabruker(Pdabruker pdabruker);

	/**
	 * Henter ut Agresso inaktivdato for en leverandør
	 * @param leverandor
	 * @return
	 */
	public Date getInaktivDateForLeverandor(Leverandor leverandor);

	
	
	/**
	 * Sjekker om en leverandør overlapper i periode.
	 * @param levStedView
	 * @param datoFra
	 * @param datoTil
	 */
//	boolean isLeverandorOverlappendePeriode(LevStedView levStedView, Driftkontrakt driftkontrakt, Date datoFra, Date datoTil);
	
	/**
	 * Henter ut kontrakt_leverandor for driftkontrakten
	 * @param d
	 * @return
	 */
//	Driftkontrakt getKontraktLeverandorEager(Driftkontrakt d);

}
