package no.mesta.driftkontrakt.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.mesta.mipss.persistence.mipssfield.Prosess;
/**
 * Hjelpeklasse for å bygge en prosess-stuktur
 * @author harkul
 *
 */
public class ProsessHelper {
	/**
	 * Fjerner alle underprosessene på samtliger prosesser. Disse kan ikke være med i 
	 * dette utvalget ettersom det er kontraktens prosesser som er interessante.
	 * @param p
	 */
	private void clearChilds(Prosess p){
		p.setProsesser(new ArrayList<Prosess>());
		for (Prosess pp:p.getProsesser()){
			pp.setProsesser(new ArrayList<Prosess>());
			clearChilds(pp);
		}
		
	}
	
	/**
	 * Lag trestuktur av prosessene. Dersom Prosess er en underprosess skal alle prosessene i treet legges med ned til roten.
	 * @param unsorted
	 * @return
	 */
	public List<Prosess> createTree(List<Prosess> unsorted){
		for (Prosess p:unsorted){
			clearChilds(p);
		}
		List<Prosess> sorted = new ArrayList<Prosess>();
		for (Prosess a:unsorted){
			Prosess owner = addToList(sorted, getRoot(a));
			if (a.getEierprosess()!=null)
				addPathToOwner(owner, a);
		}
		sortTree(sorted);
		return sorted;
	}
	
	private void sortTree(List<Prosess> prosess){
		Collections.sort(prosess, new Comparator<Prosess>() {
			public int compare(Prosess p1, Prosess p2) {
				return p1.getProsessKode().compareTo(p2.getProsessKode());
			}
		});
		for (Prosess p:prosess){
			sortTree(p.getProsesser());
		}
	}
	
	/**
	 * Oppretter stien mellom Prosess og owner
	 * @param owner
	 * @param a
	 */
	private void addPathToOwner(Prosess owner, Prosess a){
		if (!a.getEierprosess().equals(owner)){
			if (!a.getEierprosess().getProsesser().contains(a)){
				a.getEierprosess().getProsesser().add(a);
			}
			addPathToOwner(owner, a.getEierprosess());
		}else{
			//ROOT
			if (!owner.getProsesser().contains(a)){
				owner.getProsesser().add(a);
			}
		}
	}
	
	/**
	 * Legg til prosessen i listen hvis den ikke finnes fra før. 
	 * @param l
	 * @param a
	 * @return en clone av prosessen hvis den ikke ligger i listen fra før, eller en referanse til Prosess som ligger i listen
	 */
	private Prosess addToList(List<Prosess> l, Prosess a){
		//Prosess clone = (Prosess)a.clone();
		if (!l.contains(a)){
			l.add(a);
		}else{
			a = l.get(l.indexOf(a));
		}
		
		return a;
	}
	
	/**
	 * Henter den øverste rotnoden til a
	 * @param a
	 * @return
	 */
	private Prosess getRoot(Prosess a){
		Prosess root = a.getEierprosess();
		if (root==null){
			return a;
		}else{
			return getRoot(root);
		}
	}
}
