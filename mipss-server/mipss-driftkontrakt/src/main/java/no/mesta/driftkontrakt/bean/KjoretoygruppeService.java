package no.mesta.driftkontrakt.bean;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyGruppering;

@Remote
public interface KjoretoygruppeService {
    public static final String BEAN_NAME = "KjoretoyGruppeService";

    
    /**
     * Lagrer en kobling mellom kj�ret�y og kj�ret�ygruppering, ny eller en oppdatering
     * 
     * @param kjoretoy
     * @return
     */
    KontraktKjoretoyGruppering peristKontraktKjoretoygruppering(KontraktKjoretoyGruppering kjoretoy);
    
    /**
     * Lagrer en kjøretøygruppe, ny eller oppdatering
     * 
     * @param gruppe
     * @return
     */
    Kjoretoygruppe persistKjoretoygruppe(Kjoretoygruppe kjoretoygruppe);
    /**
     * Henter kjoretoygruppene til valgt kontrakt
     * @param driftkontraktId
     * @return
     */
    List<Kjoretoygruppe> getKjoretoygruppe(Long driftkontraktId);
    /**
     * Henter kjøretøyene som tilhører grupperingen
     * @param kjoretoygruppeId Id til Kjoretoygruppe
     * @return
     */
    List<KontraktKjoretoyGruppering> getKontraktKjoretoyGruppering(Long kjoretoygruppeId);
    
    /**
     * Sletter en KontraktKjoretoyGruppering
     * @param kjoretoy
     */
    void slettKontraktKjoretoyGruppering(KontraktKjoretoyGruppering kjoretoy);
    	
    /**
     * Sletter en hel kjøretøygruppe
     * @param kjoretogruppeId Id til den valgte Kjoretoygruppe
     */
    void slettKjoretoygruppe(Long kjoretoygruppeId);
    
    /**
     * Henter en liste med KontraktKjoretoy der KontraktKjoretoy ikke finnes i Kjoretoygruppen
     * @param kjoretoygruppeId
     * @param driftkontraktId
     * @return
     */
	List<KontraktKjoretoy> getKjoretoyUtenforGruppering(Long kjoretoygruppeId, Long driftkontraktId);
	
	/**
	 * Oppdaterer Kjoretoygruppering, oppdaterer også relasjonene til KontraktKjoretoyGruppering
	 * @param gruppe
	 */
	void updateKjoretoyGruppe(Kjoretoygruppe gruppe);
	
	/**
	 * Henter kjøretøynavn til kjøretøyene i gruppen
	 * @param kjoretoygruppeId
	 * @return
	 */
	List<String> getKjoretoynavn(Long kjoretoygruppeId);
	/**
	 * henter et KontraktKjoretoy med angitt kjoretoyId og driftkontraktId
	 * @param driftkontraktId
	 * @param kjoretoyId
	 * @return
	 */
	KontraktKjoretoy getKontraktKjoretoy(Long driftkontraktId, Long kjoretoyId);
}
