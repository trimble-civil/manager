package no.mesta.mipss.mipssfield.vo;

import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Resultat etter å ha manipulert hendelse i databasen
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class HendelsePunktregResult implements Serializable {
    private String guidHendelse;
    private String guidPunktreg;
    private Hendelse hendelse;
    private HendelseSokView hendelseSokView;
    private Avvik punktreg;
    private PunktregSok punktregSok;

    /**
     * Konstruktør
     *
     * @param hendelse
     */
    public HendelsePunktregResult(Hendelse hendelse) {
        this.hendelse = hendelse;
        guidHendelse = hendelse.getGuid();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof HendelsePunktregResult)) {
            return false;
        }

        HendelsePunktregResult other = (HendelsePunktregResult) o;

        return new EqualsBuilder().append(guidHendelse, other.getGuidHendelse()).append(guidPunktreg,
                other.guidPunktreg).isEquals();
    }

    /**
     * GUID
     *
     * @return
     */
    public String getGuidHendelse() {
        return guidHendelse;
    }

    public String getGuidPunktreg() {
        return guidPunktreg;
    }

    /**
     * Retur objektet
     *
     * @return
     */
    public Hendelse getHendelse() {
        return hendelse;
    }

    public HendelseSokView getHendelseSokView() {
        return hendelseSokView;
    }

    public Avvik getPunktreg() {
        return punktreg;
    }

    public PunktregSok getPunktregSok() {
        return punktregSok;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(guidHendelse).append(guidPunktreg).toHashCode();
    }

    /**
     * GUID
     *
     * @param guidHendelse
     */
    public void setGuidHendelse(String guidHendelse) {
        this.guidHendelse = guidHendelse;
    }

    public void setGuidPunktreg(String guidPunktreg) {
        this.guidPunktreg = guidPunktreg;
    }

    /**
     * Retur objektet
     *
     * @param hendelse
     */
    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
    }

    public void setHendelseSokView(HendelseSokView hendelseSokView) {
        this.hendelseSokView = hendelseSokView;
    }

    public void setPunktreg(Avvik punktreg) {
        this.punktreg = punktreg;
    }

    public void setPunktregSok(PunktregSok punktregSok) {
        this.punktregSok = punktregSok;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("guidHendelse", guidHendelse).append("hendelse", hendelse).append(
                "hendelseSokView", hendelseSokView).append("guidPunktreg", guidPunktreg).append("punktreg", punktreg)
                .append("punktregSok", punktregSok).toString();
    }
}
