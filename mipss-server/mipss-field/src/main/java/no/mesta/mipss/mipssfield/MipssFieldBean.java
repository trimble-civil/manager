package no.mesta.mipss.mipssfield;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mipssfield.vo.*;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.webservice.NvdbWebserviceLocal;
import no.mesta.mipss.webservice.ReflinkStrekning;
import no.mesta.mipss.webservice.SerializableReflinkSection;
import no.mesta.mipss.webservice.SerializableRoadRef;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.Veireferanse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Implementasjon for bønne som implementerer DB funksjonalitet for VMP
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Stateless(name = MipssField.BEAN_NAME, mappedName = "ejb/" + MipssField.BEAN_NAME)
public class MipssFieldBean implements MipssField, MipssFieldLocal {
    /**
     * Bygger en select
     *
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    class SelectQueryBuilder {
        private List<String> asc = new ArrayList<String>();
        private List<String> from = new ArrayList<String>();
        private List<String> join = new ArrayList<String>();
        private List<String> operator = new ArrayList<String>();
        private List<String> orderBy = new ArrayList<String>();
        private List<String> select = new ArrayList<String>();
        private List<String> where = new ArrayList<String>();

        public void addFrom(String s) {
            from.add(s);
        }

        public void addJoin(String s) {
            join.add(s);
        }

        public void addOrderBy(String o, String a) {
            orderBy.add(o);
            asc.add(a);
        }

        public void addSelect(String s) {
            select.add(s);
        }

        public void addWhere(String w, String o) {
            where.add(w);
            operator.add(o);
        }

        @Override
        public String toString() {
            StringBuffer query = new StringBuffer("SELECT\n");

            int i = 0;
            for (String s : select) {
                i++;
                query.append("\t" + s);

                if (i == select.size()) {
                    query.append("\n");
                } else if (select.size() > 1) {
                    query.append(",\n");
                }
            }

            query.append("FROM\n");
            i = 0;
            for (String s : from) {
                i++;
                query.append("\t" + s);

                if (i == from.size()) {
                    query.append("\n");
                } else if (from.size() > 1) {
                    query.append(",\n");
                }
            }

            for (String s : join) {
                query.append("\t" + s + "\n");
            }

            if (where.size() > 0) {
                query.append("WHERE\n");
                i = 0;
                for (String s : where) {
                    i++;
                    if (i > 1) {
                        query.append("\t" + operator.get(i - 1) + " ");
                    }

                    if (i == 1) {
                        query.append("\t" + s);
                    } else {
                        query.append(s);
                    }

                    query.append("\n");
                }
            }

            if (orderBy.size() > 0) {
                query.append("ORDER BY\n");
                i = 0;
                for (String s : orderBy) {
                    i++;
                    query.append("\t" + s + " " + asc.get(i - 1));
                    if (i < orderBy.size()) {
                        query.append(",");
                    }
                    query.append("\n");
                }
            }

            return query.toString();
        }
    }

    private static final String AND_OPERATOR = " AND ";
    private static final String LEFT_P = "(";
    private static final Logger logger = LoggerFactory.getLogger(MipssFieldBean.class);
    private static final String OR_OPERATOR = " OR ";
    private static final String RIGHT_P = ")";

    @PersistenceContext(unitName = "mipssPersistence")
    EntityManager em;

    @EJB
    private NvdbWebserviceLocal nvdbService;

    public MipssFieldBean() {
    }

    public Forsikringsskade addForsikringsskade(Hendelse hendelse, Forsikringsskade f, String brukerSign) {
        Hendelse serverHendelse = em.find(Hendelse.class, hendelse.getGuid());
        em.refresh(serverHendelse);

        f.setEndretAv(brukerSign);
        f.setEndretDato(Clock.now());
        serverHendelse.getOwnedMipssEntity().setEndretAv(brukerSign);
        serverHendelse.getOwnedMipssEntity().setEndretDato(Clock.now());
        //TODO utkommentert pga modellendring
        //		serverHendelse.setForsikringsskade(f);
        //TODO utkommentert pga modellendring
//		f.setHendelse(serverHendelse);
        em.persist(f);
        em.merge(serverHendelse);

        em.setFlushMode(FlushModeType.COMMIT);
        em.flush();

        return f;
    }

    @Override
    public HendelsePunktregResult addPunktreg(Hendelse hendelse, Avvik punktreg, Bruker bruker) {
        Hendelse serverHendelse = em.find(Hendelse.class, hendelse.getGuid());
        em.refresh(serverHendelse);

        Avvik serverPunktreg = em.find(Avvik.class, punktreg.getGuid());
        em.refresh(serverPunktreg);
        //TODO utkommentert pga modellendring
//		serverPunktreg.setHendelse(serverHendelse);
        serverPunktreg = em.merge(serverPunktreg);
        //TODO utkommentert pga modellendring
//		serverHendelse.addPunktreg(punktreg);
        serverHendelse.getOwnedMipssEntity().setEndretAv(bruker.getSignatur());
        serverHendelse.getOwnedMipssEntity().setEndretDato(Clock.now());
        serverHendelse = em.merge(serverHendelse);

        em.setFlushMode(FlushModeType.COMMIT);
        em.flush();

        PunktregSok punktregSok = hentPunktregSok(serverPunktreg.getGuid());
        HendelseSokView hendelseSok = hentHendelseSok(serverHendelse.getGuid());

        HendelsePunktregResult result = new HendelsePunktregResult(serverHendelse);
        result.setGuidHendelse(serverHendelse.getGuid());
        result.setGuidPunktreg(serverPunktreg.getGuid());
        result.setHendelse(serverHendelse);
        result.setPunktreg(serverPunktreg);
        result.setHendelseSokView(hendelseSok);
        result.setPunktregSok(punktregSok);

        return result;
    }

    @Override
    public Avvik avsluttPunktreg(String punkt, String tekst, String signatur, Date lukketDato) {
        lukketDato = (lukketDato == null ? Clock.now() : lukketDato);
        AvvikstatusType lukketStatus = em.find(AvvikstatusType.class, AvvikstatusType.LUKKET_STATUS);
        Avvik punktreg = em.find(Avvik.class, punkt);
        AvvikStatus status = new AvvikStatus();
        status.setAvvik(punktreg);
        status.setAvvikstatusType(lukketStatus);
        status.setFritekst(tekst);
        status.setOpprettetAv(signatur);
        status.setOpprettetDato(lukketDato);
        punktreg.addStatus(status);
        punktreg.setLukketDato(lukketDato);

        return punktreg;
    }

    @Override
    public Punktstedfest calculatePunktstedfest(Punktveiref veiref) {
        logger.debug("calculatePunktstedfest(" + veiref + ") start");
        Veireferanse veireferanse = getVeireferanseInstance(veiref);

        List<ReflinkSeksjon> seksjoner = SerializableReflinkSection.createWebserviceSections(nvdbService.getReflinkSectionsWithinRoadref(new SerializableRoadRef(veireferanse)));

        if (seksjoner == null || seksjoner.isEmpty()) {
            logger.warn("calculatePunktstedfest(" + veiref + ") no sections found");
            return null;
        } else if (seksjoner != null && seksjoner.size() > 1) {
            throw new IllegalStateException("More than one ReflinkSeksjon returned!");
        } else {
            Punktstedfest stedfest = new Punktstedfest();

            List<ReflinkStrekning> strekninger = nvdbService.getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(seksjoner));
            if (strekninger == null || strekninger.isEmpty()) {
                logger.warn("calculatePunktstedfest(" + veiref + ") no stretches found");
                return null;
            } else if (strekninger != null && strekninger.size() > 1) {
                throw new IllegalStateException("More than ReflinkStrekning one returned!");
            } else {
                ReflinkStrekning strekning = strekninger.get(0);
                stedfest.setX(strekning.getVectors()[0].getX());
                stedfest.setY(strekning.getVectors()[0].getY());
                stedfest.setSnappetX(stedfest.getX());
                stedfest.setSnappetY(stedfest.getY());
                stedfest.setSnappetH(null);
                stedfest.setReflinkIdent((long) strekning.getReflinkId());

                return stedfest;
            }
        }
    }

    @Override
    public PunktregSok hentPunktregSok(Long id) {
        try {
            Avvik funn = (Avvik) em.createQuery("select o from Punktreg o where o.refnummer=:id").setParameter("id", id).getSingleResult();
            String status = funn.getStatus().getAvvikstatusType().getStatus();
            Query query = em.createNativeQuery("select * from table (feltstudio_pk.punktreg_sok(" + funn.getSak().getKontraktId()
                    + ", null, null, '" + status + "', 1)) where refnummer = '" + id + "'", "PunktregSokResult");
            PunktregSok sok = (PunktregSok) query.getSingleResult();
            return sok;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public HendelseSokView hentHendelseSok(Long id) {
        try {
            HendelseSokView hendelse = (HendelseSokView) em.createQuery("select o from HendelseSokView o where o.refnummer=:id").setParameter("id", id).getSingleResult();
            return hendelse;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public InspeksjonSokView hentInspeksjonSok(Long id) {
        try {
            InspeksjonSokView inspeksjon = (InspeksjonSokView) em.createQuery("select o from InspeksjonSokView o where o.refnummer=:id").setParameter("id", id).getSingleResult();
            return inspeksjon;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public HendelsePunktregResult clearHendelse(Avvik punktreg, Bruker bruker) {
        Avvik serverPunktreg = em.find(Avvik.class, punktreg.getGuid());
        em.refresh(serverPunktreg);
        //TODO utkommentert pga modellendring
//		Hendelse serverHendelse = em.find(Hendelse.class, serverPunktreg.getHendelseGuid());
//		em.refresh(serverHendelse);
//		serverHendelse.removePunktreg(serverPunktreg);
//		serverPunktreg.setHendelse(null);
//		serverHendelse = em.merge(serverHendelse);
//		serverPunktreg = em.merge(serverPunktreg);
//
//		em.setFlushMode(FlushModeType.COMMIT);
//		em.flush();
//
//		PunktregSok punktregSok = hentPunktregSok(serverPunktreg.getGuid());
//		HendelseSokView hendelseSok = hentHendelseSok(serverHendelse.getGuid());
//
//		HendelsePunktregResult result = new HendelsePunktregResult(serverHendelse);
//		result.setGuidHendelse(serverHendelse.getGuid());
//		result.setGuidPunktreg(serverPunktreg.getGuid());
//		result.setHendelse(serverHendelse);
//		result.setPunktreg(serverPunktreg);
//		result.setHendelseSokView(hendelseSok);
//		result.setPunktregSok(punktregSok);

//		return result;
        return null;
    }

    private String createGroup(List<?> ider, boolean ignoreString) {
        String sql = "";
        int i = 0;
        for (Object id : ider) {
            if (id instanceof String && !ignoreString) {
                sql += "'" + id + "'";
            } else {
                sql += id;
            }
            if (i < ider.size() - 1) {
                sql += ",";
            }
            i++;
        }
        return sql;
    }

    private String createGroup(Object[] ider, boolean ignoreString) {
        String sql = "";
        int i = 0;
        for (Object id : ider) {
            if (id instanceof String && !ignoreString) {
                sql += "'" + id + "'";
            } else {
                sql += id;
            }
            if (i < ider.length - 1) {
                sql += ",";
            }
            i++;
        }

        return sql;
    }

    private String createSqlIn(List<?> guider) {
        String sql = " in (" + createGroup(guider, false) + RIGHT_P;
        return sql;
    }

    private String createSqlIn(Object[] ider) {
        String sql = " in (" + createGroup(ider, false) + RIGHT_P;
        return sql;
    }

    /**
     * Sletter objekter som ikke lenger skal våre med i relasjoner
     *
     * @param old
     * @param current
     */
    private void deleteOldEntities(List<?> old, List<?> current) {
        for (Object o : old) {
            if (!current.contains(o)) {
                logger.debug("deleting: " + o);
                em.remove(o);
            }
        }
    }

    @Override
    public Double getInspisertVeiLengde(String guid, String veikategori) {
        final String nativeQuery = "SELECT feltstudio_pk.inspisert_strekning_km ('" + guid + "', '" + veikategori
                + "') AS insp FROM dual";

        BigDecimal r = (BigDecimal) em.createNativeQuery(nativeQuery).getSingleResult();

        Double length = r.doubleValue();

        return length;
    }

    private Veireferanse getVeireferanseInstance(Punktveiref veiref) {
        Veireferanse veireferanse = new Veireferanse();
        veireferanse.setFrahp(veiref.getHp());
        veireferanse.setTilhp(veireferanse.getFrahp());
        veireferanse.setFraMeter(veiref.getMeter());
        veireferanse.setTilMeter(veireferanse.getFraMeter());
        veireferanse.setFylkenummer(veiref.getFylkesnummer());
        veireferanse.setKommunenummer(veiref.getKommunenummer());
        veireferanse.setVegkategori(veiref.getVeikategori().charAt(0));
        veireferanse.setVegnummer(veiref.getVeinummer());
        veireferanse.setVegstatus(veiref.getVeistatus().charAt(0));
        veireferanse.setVegstatusoverordnet(veireferanse.getVegstatus());

        return veireferanse;
    }

    @Override
    public List<PunktregSok> gjennopprettFunn(List<String> guider) {
        logger.debug("gjennopprettFunn(" + guider + ") start");
        String gjenopprettFunnSQL = "update punktreg set slettet_av = null, slettet_dato = null where guid";
        gjenopprettFunnSQL += createSqlIn(guider);
        Query gjenopprettFunn = em.createNativeQuery(gjenopprettFunnSQL);
        int rowsFunn = gjenopprettFunn.executeUpdate();
        logger.debug("gjenopprettet " + rowsFunn + " funn");

        // Les de endrede funn
        List<PunktregSok> funn = new ArrayList<PunktregSok>();
        for (String guid : guider) {
            funn.add(hentPunktregSok(guid));
        }

        return funn;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<HendelseSokView> gjenopprettHendelser(List<String> guider) {
        logger.debug("gjenopprettHendelser(" + guider + ") start");
        String gjenopprettHendelserSQL = "update hendelse set slettet_av = null, slettet_dato = null where guid";
        gjenopprettHendelserSQL += createSqlIn(guider);
        Query gjenopprettHendelser = em.createNativeQuery(gjenopprettHendelserSQL);
        int rowsHendelser = gjenopprettHendelser.executeUpdate();
        logger.debug("gjenopprettet " + rowsHendelser + " hendelser");

        String gjenopprettFunnSQL = "update punktreg set slettet_av = null, slettet_dato = null where hendelse_guid";
        gjenopprettFunnSQL += createSqlIn(guider);
        Query gjenopprettFunn = em.createNativeQuery(gjenopprettFunnSQL);
        int rowsFunn = gjenopprettFunn.executeUpdate();
        logger.debug("gjenopprettet " + rowsFunn + " funn");

        String lesHendelserPQL = "select h from HendelseSokView h where h.guid";
        lesHendelserPQL += createSqlIn(guider);
        Query lesHendelser = em.createQuery(lesHendelserPQL);
        List<HendelseSokView> hendelser = lesHendelser.getResultList();

        return hendelser;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<InspeksjonSokView> gjenopprettInspeksjoner(List<String> guider) {
        logger.debug("gjenopprettInspeksjoner(" + guider + ") start");
        String gjenopprettInspeksjonerSQL = "update inspeksjon set slettet_av = null, slettet_dato = null where guid";
        gjenopprettInspeksjonerSQL += createSqlIn(guider);
        Query gjenopprettInspeksjoner = em.createNativeQuery(gjenopprettInspeksjonerSQL);
        int rowsInspeksjoner = gjenopprettInspeksjoner.executeUpdate();
        logger.debug("gjenopprettet " + rowsInspeksjoner + " inspeksjoner");

        String gjenopprettHendelserSQL = "update hendelse set slettet_av = null, slettet_dato = null where inspeksjon_guid";
        gjenopprettHendelserSQL += createSqlIn(guider);
        Query gjenopprettHendelser = em.createNativeQuery(gjenopprettHendelserSQL);
        int rowsHendelser = gjenopprettHendelser.executeUpdate();
        logger.debug("gjenopprettet " + rowsHendelser + " hendelser");

        String gjenopprettFunnSQL = "update punktreg set slettet_av = null, slettet_dato = null where inspeksjon_guid";
        gjenopprettFunnSQL += createSqlIn(guider);
        Query gjenopprettFunn = em.createNativeQuery(gjenopprettFunnSQL);
        int rowsFunn = gjenopprettFunn.executeUpdate();
        logger.debug("gjenopprettet " + rowsFunn + " funn");

        String lesInspeksjonerPQL = "select i from InspeksjonSokView i where i.guid";
        lesInspeksjonerPQL += createSqlIn(guider);
        Query lesInspeksjoner = em.createQuery(lesInspeksjonerPQL);
        List<InspeksjonSokView> InspeksjonSokView = lesInspeksjoner.getResultList();

        return InspeksjonSokView;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Hendelseaarsak> hentAlleHendelsesAarsaker() {
        return em.createNamedQuery(Hendelseaarsak.QUERY_FIND_ALL).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Prosess> hentAlleProsesser() {
        logger.trace("hentAlleProsesser() start");
        return em.createNamedQuery(Prosess.QUERY_FIND_ALL).getResultList();
    }

    @Override
    public Prosess hentProsessMedId(Long id) {
        return em.find(Prosess.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Avviktilstand> hentAllePunktregtilstandtype() {
        Query query = em.createNamedQuery(Avviktilstand.QUERY_FIND_ALL);
        return query.getResultList();
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<AvvikstatusType> hentAlleStatusTyper() {
        Query query = em.createNamedQuery(AvvikstatusType.QUERY_FIND_ALL);
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Temperatur> hentAlleTemperatur() {
        return em.createNamedQuery(Temperatur.QUERY_FIND_ALL).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Trafikktiltak> hentAlleTrafikktiltaktyper() {
        return em.createNamedQuery(Trafikktiltak.QUERY_FIND_ALL).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Vaer> hentAlleVaer() {
        return em.createNamedQuery(Vaer.QUERY_FIND_ALL).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Vind> hentAlleVind() {
        return em.createNamedQuery(Vind.QUERY_FIND_ALL).getResultList();
    }

    public Forsikringsskade hentForsikringsskade(String guid) {
        return em.find(Forsikringsskade.class, guid);
    }

    @SuppressWarnings("unchecked")
    public List<String> hentForsikringsskadeGuid(List<String> hendelseGuid) {
        String ids = StringUtils.join(hendelseGuid, ',');
        String qry = "select fs.guid from hendelse h join forsikringsskade fs on fs.hendelse_guid=h.guid where h.guid in(?)";
        return em.createNativeQuery(qry)
                .setParameter(1, ids)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<String> hentSkredGuid(List<String> hendelseGuid) {
        String ids = StringUtils.join(hendelseGuid, ',');
        String qry = "select s.guid from hendelse h join skred s on s.hendelse_guid=h.guid where h.guid in(?)";
        return em.createNativeQuery(qry)
                .setParameter(1, ids)
                .getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Avvik> hentFunn(List<String> guids) {
        logger.debug("hentFunn(" + (guids != null ? String.valueOf(guids.size()) : "null") + ") start");
        if (guids == null || guids.size() == 0) {
            return new ArrayList<Avvik>();
        }

        int size = guids.size();
        int reminder = size % 1000;
        int pages = (size - reminder) / 1000;
        List<Avvik> result = new ArrayList<Avvik>();
        for (int page = 0; page <= pages; page++) {
            logger.debug("fetching page: " + page);
            int fromIndex = page * 1000;
            int pageLength = 1000;
            int toIndex = fromIndex + pageLength;
            if (toIndex > guids.size()) {
                toIndex = guids.size();
            }
            List<String> ids = guids.subList(fromIndex, toIndex);
            SelectQueryBuilder queryBuilder = new SelectQueryBuilder();
            queryBuilder.addFrom("Punktreg p");
            queryBuilder.addSelect("p");
            queryBuilder.addWhere("p.guid " + createSqlIn(ids), AND_OPERATOR);

            Query dbQuery = em.createQuery(queryBuilder.toString());
            dbQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
            List<Avvik> dbList = dbQuery.getResultList();
            result.addAll(dbList);
        }
        return result;
    }

    @Override
    public Hendelse hentHendelse(String guid) {
        final String method = "hentHendelse(" + guid + ")";
        logger.debug(method + " start");

        Hendelse hendelse = em.find(Hendelse.class, guid);
        em.refresh(hendelse);
        logger.debug(method + "hendelse hentet ");
        // Init bilder
        hendelse.getBilder().size();
        if (hendelse.getSak().getKontrakt() != null)
            hendelse.getSak().getKontrakt().getByggeleder();

        if (logger.isDebugEnabled()) {
            logger.debug(method + " done with " + hendelse);
        }
        return hendelse;
    }

    @Override
    public Hendelse hentHendelseSkred(String guid) {
        Hendelse hendelse = em.find(Hendelse.class, guid);
        em.refresh(hendelse);
        hendelse.getBilder().size();
        if (hendelse.getSak().getKontrakt() != null)
            hendelse.getSak().getKontrakt().getByggeleder();
        //TODO utkommentert pga modellendring
//		if (hendelse.getSkred()!=null){
//			hendelse.getSkred().getSkredEgenskapList().size();
//		}
        return hendelse;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Bilde> hentHendelseBilder(String guid) {
        Query bildeQuery = em.createNamedQuery(Hendelse.QUERY_GET_BILDER);
        bildeQuery.setParameter(1, guid);
        bildeQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
        return bildeQuery.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Hendelse> hentHendelser(List<String> guids) {
        logger.debug("hentHendelser(" + (guids != null ? String.valueOf(guids.size()) : "null") + ")");
        if (guids == null || guids.size() == 0) {
            return new ArrayList<Hendelse>();
        }

        int size = guids.size();
        int reminder = size % 1000;
        int pages = (size - reminder) / 1000;
        List<Hendelse> result = new ArrayList<Hendelse>();
        for (int page = 0; page <= pages; page++) {
            logger.debug("fetching page " + page);
            int fromIndex = page * 1000;
            int pageLength = 1000;
            int toIndex = fromIndex + pageLength;
            if (toIndex > guids.size()) {
                toIndex = guids.size();
            }
            List<String> ids = guids.subList(fromIndex, toIndex);
            SelectQueryBuilder queryBuilder = new SelectQueryBuilder();
            queryBuilder.addFrom("Hendelse h");
            queryBuilder.addSelect("DISTINCT h");
            queryBuilder.addWhere("h.guid " + createSqlIn(ids), AND_OPERATOR);
            Query dbQuery = em.createQuery(queryBuilder.toString());
            dbQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
            List<Hendelse> hendelser = dbQuery.getResultList();

            for (Hendelse h : hendelser) {
                Query bildeQuery = em.createNamedQuery(Hendelse.QUERY_GET_BILDER);
                bildeQuery.setParameter(1, h.getGuid());
                List<Bilde> bilder = bildeQuery.getResultList();
                Collections.sort(bilder);
                h.setBilder(bilder);
            }
            result.addAll(hendelser);
        }

        return result;
    }

    public List<Hendelse> hentHendelserForRapport(List<String> guids) {
        List<Hendelse> hendelser = hentHendelser(guids);
        for (Hendelse h : hendelser) {
            h.getSak().getKontrakt().getByggeleder();
        }
        return hendelser;
    }

    @Override
    public HendelseSokView hentHendelseSok(String guid) {
        final String method = "hentHendelseSok(";
        logger.debug(method + guid + ") start");

        HendelseSokView hendelse = em.find(HendelseSokView.class, guid);

        logger.debug(method + ") done with " + hendelse);
        return hendelse;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<HPReport> hentHPReport(List<String> guids) {
        logger.debug("hentHPReport(" + (guids != null ? String.valueOf(guids.size()) : "null") + ") start");

        if (guids == null || guids.size() == 0) {
            return new ArrayList<HPReport>();
        }
        int size = guids.size();
        int reminder = size % 1000;
        int pages = (size - reminder) / 1000;
        List<HPReport> result = new ArrayList<HPReport>();
        for (int page = 0; page <= pages; page++) {
            logger.debug("fetching page: " + page);
            int fromIndex = page * 1000;
            int pageLength = 1000;
            int toIndex = fromIndex + pageLength;
            if (toIndex > guids.size()) {
                toIndex = guids.size();
            }
            List<String> ids = guids.subList(fromIndex, toIndex);
            SelectQueryBuilder queryBuilder = new SelectQueryBuilder();
            queryBuilder.addFrom("InspeksjonSokView i");
            queryBuilder
                    .addSelect("DISTINCT NEW no.mesta.mipss.mipssfield.vo.HPReport(p.inspeksjonSok, p.fraTidspunkt, p.tilTidspunkt, p.fylkesnummer, p.kommunenummer, p.veikategori, p.veistatus, p.veinummer, p.hp, p.fraKm, p.tilKm)");
            queryBuilder.addJoin("LEFT JOIN i.strekninger p");
            queryBuilder.addWhere("p.hp < 400", AND_OPERATOR);
            queryBuilder.addWhere("i.guid " + createSqlIn(ids), AND_OPERATOR);
            queryBuilder.addOrderBy("p.fraTidspunkt", "ASC");

            logger.debug("Executing query: \n" + queryBuilder.toString());
            List<HPReport> report = (List<HPReport>) em.createQuery(queryBuilder.toString()).getResultList();
            if (logger.isDebugEnabled()) {
                logger.debug("Retreived " + (report != null ? String.valueOf(report.size()) : "null"));
            }
            result.addAll(report);
        }
        logger.debug("hentHPReport(" + (guids != null ? String.valueOf(guids.size()) : "null") + ") end with "
                + (result == null ? "null" : String.valueOf(result.size())));
        return result;
    }

    @Override
    public Inspeksjon hentInspeksjon(String guid) {
        Inspeksjon inspeksjon = em.find(Inspeksjon.class, guid);
        //TODO utkommentert pga modellendringer
//		inspeksjon.getPunktregistreringer().size();
        inspeksjon.getStrekninger().size();
        //TODO utkommentert pga modellendringer
//		inspeksjon.getHendelser().size();
        em.refresh(inspeksjon);

        return inspeksjon;
    }

    public Inspeksjon hentInspeksjonForRapport(String guid) {
        String q = "select refnummer, to_char(fra_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), to_char(til_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), opprettet_av, beskrivelse from Inspeksjon where guid=?1";
        Query qu = em.createNativeQuery(q);
        qu.setParameter(1, guid);
        Inspeksjon i = new NativeQueryWrapper<Inspeksjon>(qu, Inspeksjon.class,
                new Class[]{Long.class, Date.class, Date.class,
                        String.class, String.class},
                "refnummer", "fraTidspunkt", "tilTidspunkt",
                "opprettetAv", "beskrivelse").getWrappedSingleResult();

        i.setStrekninger(hentProdstrekningForInspeksjonRapport(guid));
        //TODO utkommentert pga modellendringer
//		i.setPunktregistreringer(hentPunktregForInspeksjonRapport(guid));
        return i;
    }

    private List<Prodstrekning> hentProdstrekningForInspeksjonRapport(String guid) {
        String q = "select fylkesnummer, kommunenummer, veinummer, veikategori, veistatus, hp, fra_km, til_km, to_char(fra_tidspunkt, 'dd.mm.yyyy hh24:mi:ss'), to_char(til_tidspunkt, 'dd.mm.yyyy hh24:mi:ss') from prodstrekning where inspeksjon_guid=?1";
        Query qu = em.createNativeQuery(q);
        qu.setParameter(1, guid);

        return new NativeQueryWrapper<Prodstrekning>(qu, Prodstrekning.class,
                new Class[]{Long.class, Long.class, Long.class, String.class, String.class, Long.class, Double.class,
                        Double.class, Date.class, Date.class},
                "fylkesnummer", "kommunenummer", "veinummer", "veikategori", "veistatus", "hp", "fraKm", "tilKm", "fraTidspunkt", "tilTidspunkt").getWrappedList();
    }

    private List<Avvik> hentPunktregForInspeksjonRapport(String guid) {
        StringBuilder sb = new StringBuilder();
        sb.append("select p.guid, p.refnummer, p.opprettet_av, p.beskrivelse, to_char(p.tiltaks_dato, 'dd.mm.yyyy hh24:mi:ss'), ");
        sb.append("to_char(ps.opprettet_dato,'dd.mm.yyyy hh24:mi:ss'), ps.status, pp.prosess_kode ");
        sb.append("from punktreg p ");
        sb.append("join punktreg_status ps on p.guid = ps.punktreg_guid ");
        sb.append("join prosess pp on pp.id= p.prosess_id ");
        sb.append("where ps.opprettet_dato=(select max(opprettet_dato) from punktreg_status where punktreg_guid=p.guid) ");
        sb.append("and p.inspeksjon_guid=?1");
        Query q = em.createNativeQuery(sb.toString());
        q.setParameter(1, guid);

        List<Avvik> punktregs = new NativeQueryWrapper<Avvik>(q, Avvik.class,
                new Class[]{String.class, Long.class, String.class, String.class,
                        Date.class, Date.class, String.class, String.class},
                "guid", "refnummer", "opprettetAv", "beskrivelse",
                "tiltaksDato", "sisteStatusDatoRapport", "sisteStatusRapport", "prosessRapport").getWrappedList();
        for (Avvik p : punktregs) {
            try {
                p.setStedfestingForRapport((Punktstedfest) em.createNamedQuery(Punktstedfest.QUERY_FIND_BY_AVVIK).setParameter("guid", p.getGuid()).getSingleResult());
            } catch (NoResultException e) {
                //TODO remove!.
                e.printStackTrace();
            }
        }
        return punktregs;
        //prosessRapport
        //sisteStatusRapport;
        //sisteStatusDatoRapport;
    }

    @Override
    public InspeksjonSokView hentInspeksjonSok(String guid) {
        final String method = "hentInspeksjonSok(";
        logger.debug(method + guid + ") start");

        InspeksjonSokView inspeksjon = em.find(InspeksjonSokView.class, guid);

        logger.debug(method + ") done with " + inspeksjon);
        return inspeksjon;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <E extends MipssEntityBean<E>> List<E> hentObjekter(Class<E> clazz, List<String> guids) {
        logger.debug("hentObjekter(" + clazz + ", " + (guids != null ? String.valueOf(guids.size()) : "null")
                + ") start");

        if (guids == null || guids.size() == 0) {
            return new ArrayList<E>();
        }

        int size = guids.size();
        int reminder = size % 1000;
        int pages = (size - reminder) / 1000;
        List<E> result = new ArrayList<E>();
        for (int page = 0; page <= pages; page++) {
            logger.debug("fetching page " + page);
            int fromIndex = page * 1000;
            int pageLength = 1000;
            int toIndex = fromIndex + pageLength;
            if (toIndex > guids.size()) {
                toIndex = guids.size();
            }
            List<String> ids = guids.subList(fromIndex, toIndex);
            SelectQueryBuilder select = new SelectQueryBuilder();
            select.addSelect("o");
            select.addFrom(clazz.getSimpleName() + " o");
            select.addWhere("o.guid " + createSqlIn(ids), AND_OPERATOR);

            String selectString = select.toString();
            Query query = em.createQuery(selectString);

            logger.debug("Will execute query: " + selectString);
            List<E> entities = query.getResultList();
            result.addAll(entities);
        }
        if (clazz.equals(Forsikringsskade.class)) {
            for (E f : result) {
                MipssEntityBean me = (MipssEntityBean) f;
                Forsikringsskade fs = (Forsikringsskade) me;
                //eager fetching..
                fs.getSak().getKontrakt().getByggeleder();
                //TODO utkommentert pga modellendring
//				if (fs.getHendelse()!=null){
//					if (fs.getHendelse().getKontrakt()!=null){
//						fs.getHendelse().getKontrakt().getByggeleder();
//					}
//				}
            }
        }
        if (clazz.equals(Skred.class)) {
            for (E f : result) {
                MipssEntityBean me = (MipssEntityBean) f;
                Skred fs = (Skred) me;
                //eager fetching..
                fs.getSak().getStedfesting().size();
                //TODO utkommentert pga modellendring
//				if (fs.getHendelse()!=null){
//					fs.getHendelse().getStedfestingList().size();
//				}
            }
        }
        logger.debug("hentObjekter(" + clazz + ", " + (guids != null ? String.valueOf(guids.size()) : "null")
                + ") done with " + (result == null ? "null" : String.valueOf(result.size())));
        return result;
    }

    @Override
    public Avvik hentPunktreg(String guid) {
        Avvik punkt = em.find(Avvik.class, guid);
        em.refresh(punkt);
        return punkt;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<AvvikBilde> hentPunktregBildeInfo(String guid) {
        Query query = em.createNamedQuery(AvvikBilde.QUERY_FIND_PUNKT_BILDER);
        query.setParameter("pguid", guid);
        query.setHint(QueryHints.REFRESH, HintValues.TRUE);

        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Bilde> hentPunktregBilder(String guid) {
        Query query = em.createNamedQuery(AvvikBilde.QUERY_FIND_PUNKT_BILDER);
        query.setParameter("pguid", guid);
        query.setHint(QueryHints.REFRESH, HintValues.TRUE);

        List<Bilde> bilder = new ArrayList<Bilde>();
        for (AvvikBilde pb : (List<AvvikBilde>) query.getResultList()) {
            bilder.add(pb.getBilde());
        }

        return bilder;
    }

    @Override
    public PunktregSok hentPunktregSok(String guid) {
        logger.debug("hentPunktregSok(" + guid + ") start");
        Avvik funn = em.find(Avvik.class, guid);

        String status = funn.getStatus().getAvvikstatusType().getStatus();
        Query query = em.createNativeQuery("select * from table (feltstudio_pk.punktreg_sok(" + funn.getSak().getKontraktId()
                + ", null, null, '" + status + "', 1)) where PUNKTREG_GUID = '" + guid + "'", "PunktregSokResult");
        PunktregSok sok = (PunktregSok) query.getSingleResult();

        logger.debug("hentPunktregSok(" + guid + ") end with " + sok);
        return sok;
    }

    @Override
    public AvvikstatusType hentStatusType(String status) {
        return em.find(AvvikstatusType.class, status);
    }

    /**
     * Flytter klokkeslettet til en dato til siste mulige sekund
     *
     * @param d
     * @return
     */
    private Date lastSecond(Date d) {
        logger.debug("lastSecond(" + d + ") start");
        Date result = null;

        if (d != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            c.roll(Calendar.SECOND, 59 - c.get(Calendar.SECOND));
            c.roll(Calendar.MINUTE, 59 - c.get(Calendar.MINUTE));
            c.roll(Calendar.HOUR_OF_DAY, 23 - c.get(Calendar.HOUR_OF_DAY));

            result = c.getTime();
        }

        logger.debug("lastSecond(" + d + ") gives: " + result);
        return result;
    }

    @Override
    public PunktregResult lukkFunn(String punkt, String tekst, String signatur, Date lukketDato) {
        Avvik p = avsluttPunktreg(punkt, tekst, signatur, lukketDato);

        String status = p.getStatus().getAvvikstatusType().getStatus();
        Query query = em.createNativeQuery("select * from table (feltstudio_pk.punktreg_sok(" + p.getSak().getKontraktId()
                        + ", null, null, '" + status + "', 1)) where PUNKTREG_GUID = '" + p.getGuid() + "'",
                "PunktregSokResult");
        PunktregSok v = (PunktregSok) query.getSingleResult();

        PunktregResult result = new PunktregResult();
        result.setGuidPunktreg(v.getPunktregGuid());
        result.setView(v);
        result.setPunktreg(p);

        return result;
    }

    @SuppressWarnings("unchecked")
    public Forsikringsskade oppdaterForsikringsskade(Forsikringsskade f, String bruker) {
        if (f != null) {
            logger.debug("Oppdaterer forsikringsskade:" + f.getGuid());
            Query kontaktQuery = em.createNamedQuery(SkadeKontakt.QUERY_FIND_BY_FORSIKRINGSSKADE).setParameter("guid", f.getGuid());
            List<SkadeKontakt> kontakt = kontaktQuery.getResultList();
            logger.debug("fant :" + kontakt.size() + " skadekontakter, current:" + f.getSkadeKontakter().size());
            deleteOldEntities(kontakt, f.getSkadeKontakter());
            for (SkadeKontakt k : f.getSkadeKontakter()) {
                em.merge(k);
            }
            f.setEndretAv(bruker);
            f.setEndretDato(Clock.now());
            f = em.merge(f);
            //eager fetching..
            f.getSak().getKontrakt().getByggeleder();
            //TODO utkommentert pga modellendring
//			if (f.getHendelse()!=null){
//				if (f.getHendelse().getKontrakt()!=null){
//					f.getHendelse().getKontrakt().getByggeleder();
//				}
//			}
            em.flush();
        }
        return f;
    }

    @Override
    @SuppressWarnings("unchecked")
    public HendelseResult oppdaterHendelse(Hendelse hendelse, String bruker) {
        if (logger.isDebugEnabled()) {
            logger.debug("oppdaterHendelse(" + hendelse + ") start");
        }
        // INFO: Dette Må kjøre FØR MERGE. Dette er fordi at persistence
        // provideren får ikke med seg disse endringene
        // og oppdager de kun når den kjører SELECT SQL mot basen.
        // JPA toplink hÅndterer ikke relasjoner så derfor må vi selv greie ut
        // av de
        final String hguid = "hguid";
        Query tiltakQuery = em.createNamedQuery(HendelseTrafikktiltak.QUERY_FIND_FOR_INCIDENT);
        tiltakQuery.setParameter(hguid, hendelse.getGuid());
        List<HendelseTrafikktiltak> tiltak = tiltakQuery.getResultList();
        deleteOldEntities(tiltak, hendelse.getTiltak());

        // Nå er vi klar til å lagre den nye informasjonen
        hendelse.getOwnedMipssEntity().setEndretAv(bruker);
        hendelse.getOwnedMipssEntity().setEndretDato(Clock.now());

        //hvis sendt til elrapp og en gammel instans fra guid blir fors�kt lagret.
        Hendelse oldhendelse = em.find(Hendelse.class, hendelse.getGuid());
        if (oldhendelse != null)
            if (oldhendelse.getElrappDokumentIdent() != null && hendelse.getElrappDokumentIdent() == null) {
                hendelse.setElrappDokumentIdent(oldhendelse.getElrappDokumentIdent());
                hendelse.setElrappVersjon(oldhendelse.getElrappVersjon());
                hendelse.setRapportertDato(oldhendelse.getRapportertDato());
            }

        hendelse = em.merge(hendelse);
        em.setFlushMode(FlushModeType.COMMIT);
        em.flush();

        HendelseResult result = new HendelseResult(hendelse);
        result.setHendelseSokView(em.find(HendelseSokView.class, hendelse.getGuid()));

        if (logger.isDebugEnabled()) {
            logger.debug("oppdaterHendelse(" + hendelse + ") end with " + result);
        }
        return result;
    }

    public Hendelse oppdaterHendelseSkred(Hendelse hendelse, String bruker) {
        //TODO utkommentert pga modellendring
//		List<SkredEgenskapverdi> skredEgenskapList = hendelse.getSkred().getSkredEgenskapList();
//		List<SkredEgenskapverdi> old = em.createNamedQuery(SkredEgenskapverdi.QUERY_FIND_BY_SKRED_GUID)
//										.setParameter("skredGuid", hendelse.getSkred().getGuid())
//										.getResultList();
//		deleteOldEntities(old, skredEgenskapList);
//		
//		hendelse = em.merge(hendelse);
//		em.setFlushMode(FlushModeType.COMMIT);
//		em.flush();
//		return hendelse;
        return null;
    }

    @Override
    public InspeksjonResult oppdaterInspeksjon(Inspeksjon inspeksjon, String bruker) {
        if (logger.isDebugEnabled()) {
            logger.debug("oppdaterInspeksjon(" + inspeksjon + ") start");
        }

        inspeksjon.getOwnedMipssEntity().setEndretDato(Clock.now());
        inspeksjon.getOwnedMipssEntity().setEndretAv(bruker);
        Inspeksjon i = em.merge(inspeksjon);
        em.setFlushMode(FlushModeType.COMMIT);
        em.flush();

        InspeksjonResult result = new InspeksjonResult(i);
        InspeksjonSokView view = em.find(InspeksjonSokView.class, i.getGuid());
        result.setInspeksjonSokView(view);

        return result;
    }

    @Override
    public PunktregResult oppdaterPunktreg(Avvik punkt) {
        if (logger.isDebugEnabled()) {
            logger.debug("oppdaterPunktreg(" + punkt + ") start");
        }
        // INFO: Dette Må kjøre FØR MERGE. Dette er fordi at persistence
        // provideren får ikke med seg disse endringene
        // og oppdager de kun når den kjører SELECT SQL mot basen.
        // JPA toplink hÅndterer ikke relasjoner så derfor må vi selv greie ut
        // av de
        // hk: er dette like aktuelt med EclipseLink(?)
        final String pguid = "pguid";

        Query bildeQuery = em.createNamedQuery(AvvikBilde.QUERY_FIND_FOR_AVVIK);
        bildeQuery.setParameter(pguid, punkt.getGuid());
        List<AvvikBilde> bilder = bildeQuery.getResultList();
        if (logger.isDebugEnabled()) {
            logger.debug("oldPics: " + bilder);
            logger.debug("newPics: " + punkt.getBilder());
        }
        deleteOldEntities(bilder, punkt.getBilder());

        Query statusQuery = em.createNamedQuery(AvvikStatus.QUERY_FIND_FOR_AVVIK);
        statusQuery.setParameter(pguid, punkt.getGuid());
        List<AvvikStatus> statuser = statusQuery.getResultList();
        deleteOldEntities(statuser, punkt.getStatuser());

        // Nå er vi klar til å lagre den nye informasjonen
        Avvik p = em.merge(punkt);
        em.setFlushMode(FlushModeType.COMMIT);
        em.flush();


        String status = p.getStatus().getAvvikstatusType().getStatus();
        Query query = em.createNativeQuery("select * from table (feltstudio_pk.punktreg_sok(" + p.getSak().getKontraktId()
                        + ", null, null, '" + status + "', 1)) where PUNKTREG_GUID = '" + p.getGuid() + "'",
                "PunktregSokResult");
        PunktregSok v = (PunktregSok) query.getSingleResult();

        PunktregResult result = new PunktregResult();
        result.setGuidPunktreg(p.getGuid());
        result.setPunktreg(p);
        result.setView(v);

        if (logger.isDebugEnabled()) {
            logger.debug("oppdaterPunktreg(" + punkt + ") end with " + result);
        }
        return result;
    }


    @Override
    public AvvikStatus oppdaterPunktStatus(AvvikStatus status) {
        status = em.merge(status);
        return status;
    }

    @Override
    public HendelsePunktregResult removePunktreg(Hendelse hendelse, Avvik punktreg, Bruker bruker) {
        Hendelse serverHendelse = em.find(Hendelse.class, hendelse.getGuid());
        em.refresh(serverHendelse);
        //TODO utkommentert pga modellendring
//		Avvik serverPunktreg = em.find(Avvik.class, punktreg.getGuid());
//		em.refresh(serverPunktreg);
//		serverPunktreg.setHendelse(null);
//		serverPunktreg = em.merge(serverPunktreg);
//
//		serverHendelse.removePunktreg(punktreg);
//		serverHendelse.getOwnedMipssEntity().setEndretAv(bruker.getSignatur());
//		serverHendelse.getOwnedMipssEntity().setEndretDato(Clock.now());
//		serverHendelse = em.merge(serverHendelse);
//
//		em.setFlushMode(FlushModeType.COMMIT);
//		em.flush();
//
//		PunktregSok punktregSok = hentPunktregSok(serverPunktreg.getGuid());
//		HendelseSokView hendelseSok = hentHendelseSok(serverHendelse.getGuid());
//
//		HendelsePunktregResult result = new HendelsePunktregResult(serverHendelse);
//		result.setGuidHendelse(serverHendelse.getGuid());
//		result.setGuidPunktreg(serverPunktreg.getGuid());
//		result.setHendelse(serverHendelse);
//		result.setPunktreg(serverPunktreg);
//		result.setHendelseSokView(hendelseSok);
//		result.setPunktregSok(punktregSok);
//
//		return result;
        return null;
    }

    @Override
    public HendelsePunktregResult setHendelse(Avvik punktreg, Hendelse hendelse, Bruker bruker) {
        Avvik serverPunktreg = em.find(Avvik.class, punktreg.getGuid());
        em.refresh(serverPunktreg);

        Hendelse serverHendelse = em.find(Hendelse.class, hendelse.getGuid());
        em.refresh(serverHendelse);
        //TODO utkommentert pga modellendring
//		if (!serverHendelse.getPunktregistreringer().contains(serverPunktreg)) {
//			serverHendelse.addPunktreg(serverPunktreg);
//			serverPunktreg.setHendelse(serverHendelse);
//		}

        serverHendelse = em.merge(serverHendelse);
        serverPunktreg = em.merge(serverPunktreg);

        em.setFlushMode(FlushModeType.COMMIT);
        em.flush();

        PunktregSok punktregSok = hentPunktregSok(serverPunktreg.getGuid());
        HendelseSokView hendelseSok = hentHendelseSok(serverHendelse.getGuid());

        HendelsePunktregResult result = new HendelsePunktregResult(serverHendelse);
        result.setGuidHendelse(serverHendelse.getGuid());
        result.setGuidPunktreg(serverPunktreg.getGuid());
        result.setHendelse(serverHendelse);
        result.setPunktreg(serverPunktreg);
        result.setHendelseSokView(hendelseSok);
        result.setPunktregSok(punktregSok);

        return result;
    }

    @Override
    public List<PunktregSok> slettFunn(List<String> guider, String bruker) {
        logger.debug("slettFunn(" + guider + "," + bruker + ") start");
        // utfør native kall for å slette funn
        String slettFunnSQL = "update punktreg set slettet_av = '" + bruker + "', slettet_dato = sysdate where guid";
        slettFunnSQL += createSqlIn(guider);
        Query slettFunn = em.createNativeQuery(slettFunnSQL);
        int rowsFunn = slettFunn.executeUpdate();
        logger.debug("slettet " + rowsFunn + " funn");

        // Les de endrede funn
        List<PunktregSok> funn = new ArrayList<PunktregSok>();
        for (String guid : guider) {
            funn.add(hentPunktregSok(guid));
        }

        return funn;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<HendelseSokView> slettHendelser(List<String> guider, boolean slettFunn, String bruker) {
        logger.debug("slettHendelser(" + guider + "," + slettFunn + "," + bruker + ") start");
        // utfør native kall for å slette hendelser
        String slettHendelserSQL = "update hendelse set slettet_av = '" + bruker
                + "', slettet_dato = sysdate where guid";
        slettHendelserSQL += createSqlIn(guider);
        Query slettHendelser = em.createNativeQuery(slettHendelserSQL);
        int rowsHendelser = slettHendelser.executeUpdate();
        logger.debug("slettet " + rowsHendelser + " hendelser");

        // og kanskje slett h også
        if (slettFunn) {
            String slettFunnSql = "update punktreg set slettet_av = '" + bruker
                    + "', slettet_dato = sysdate where hendelse_guid";
            slettFunnSql += createSqlIn(guider);
            Query slettFunnQuery = em.createNativeQuery(slettFunnSql);
            int rowsFunn = slettFunnQuery.executeUpdate();
            logger.debug("slettet " + rowsFunn + " funn");
        }

        // Lag en query for å hente de endrede hendelsene
        String lesHendelserPQL = "select h from HendelseSokView h where h.guid";
        lesHendelserPQL += createSqlIn(guider);
        Query lesHendelser = em.createQuery(lesHendelserPQL);
        List<HendelseSokView> hendelser = lesHendelser.getResultList();

        return hendelser;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<InspeksjonSokView> slettInspeksjoner(List<String> guider, boolean slettHendelserOgFunn, String bruker) {
        logger.debug("slettInspeksjoner(" + guider + "," + slettHendelserOgFunn + "," + bruker + ") start");
        // Utfør native kall for å slette inspeksjoner
        String slettInspeksjonerSQL = "update inspeksjon set slettet_av = '" + bruker
                + "', slettet_dato = sysdate where guid";
        slettInspeksjonerSQL += createSqlIn(guider);
        Query slettInspeksjoner = em.createNativeQuery(slettInspeksjonerSQL);
        int rowsInspeksjoner = slettInspeksjoner.executeUpdate();
        logger.debug("slettet " + rowsInspeksjoner + " inspeksjoner");

        // og kanskje slett h og f også
        if (slettHendelserOgFunn) {
            String slettHendelserSql = "update hendelse set slettet_av = '" + bruker
                    + "', slettet_dato = sysdate where inspeksjon_guid";
            slettHendelserSql += createSqlIn(guider);
            Query slettHendelser = em.createNativeQuery(slettHendelserSql);
            int rowsHendelser = slettHendelser.executeUpdate();
            logger.debug("slettet " + rowsHendelser + " hendelser");

            String slettFunnSql = "update punktreg set slettet_av = '" + bruker
                    + "', slettet_dato = sysdate where inspeksjon_guid";
            slettFunnSql += createSqlIn(guider);
            Query slettFunn = em.createNativeQuery(slettFunnSql);
            int rowsFunn = slettFunn.executeUpdate();
            logger.debug("slettet " + rowsFunn + " funn");
        }

        // Lag en query for å hente de endrede inspeksjonene
        String lesInspeksjonerPQL = "select i from InspeksjonSokView i where i.guid";
        lesInspeksjonerPQL += createSqlIn(guider);
        Query lesInspeksjoner = em.createQuery(lesInspeksjonerPQL);
        List<InspeksjonSokView> inspeksjoner = lesInspeksjoner.getResultList();

        // voila
        return inspeksjoner;
    }

    @Override
    public PunktregResult slettSisteStatus(String guid) {
        Avvik p = em.find(Avvik.class, guid);
        em.refresh(p);

        AvvikStatus ps = p.getStatus();
        if (ps == null) {
            throw new IllegalStateException("No status to delete!");
        }

        p.removeStatus(ps);
        em.remove(ps);

        String status = p.getStatus().getAvvikstatusType().getStatus();
        Query query = em.createNativeQuery("select * from table (feltstudio_pk.punktreg_sok(" + p.getSak().getKontraktId()
                        + ", null, null, '" + status + "', 1)) where PUNKTREG_GUID = '" + p.getGuid() + "'",
                "PunktregSokResult");
        PunktregSok v = (PunktregSok) query.getSingleResult();

        PunktregResult result = new PunktregResult();
        result.setGuidPunktreg(v.getPunktregGuid());
        result.setView(v);
        result.setPunktreg(p);

        return result;
    }

    @SuppressWarnings("unchecked")
    public List<PunktregSok> sokFunn(PunktregQueryFilter filter) {
        logger.debug("sokFunn(" + filter + ") start");
        List<PunktregSok> funn = null;

        if (filter == null) {
            throw new IllegalArgumentException("Filter cannot be null");
        } else {
            if (filter.getKontraktId() == null) {
                throw new IllegalArgumentException("filter.kontraktId cannot be null");
            }

            String toDate;
            String fromDate = "null";
            String statuser = "null";
            String sisteStatus;

            if (filter.getTilDato() != null) {
                toDate = "to_date('"
                        + MipssDateFormatter.formatDate(lastSecond(filter.getTilDato()),
                        MipssDateFormatter.SHORT_DATE_TIME_FORMAT) + "', 'DD.MM.YY HH24:MI')";
            } else {
                toDate = "to_date('"
                        + MipssDateFormatter.formatDate(lastSecond(Clock.now()),
                        MipssDateFormatter.SHORT_DATE_TIME_FORMAT) + "', 'DD.MM.YY HH24:MI')";
            }

            if (filter.getFraDato() != null) {
                fromDate = "to_date('"
                        + MipssDateFormatter.formatDate(filter.getFraDato(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT)
                        + "', 'DD.MM.YY HH24:MI')";
            }

            if (filter.getStatuser() != null && filter.getStatuser().length > 0) {
                statuser = "'" + createGroup(filter.getStatuser(), true) + "'";
            }

            if (filter.isSisteStatus()) {
                sisteStatus = "1";
            } else {
                sisteStatus = "0";
            }

            SelectQueryBuilder query = new SelectQueryBuilder();
            query.addFrom("table (feltstudio_pk.punktreg_sok(" + filter.getKontraktId() + ", " + fromDate + ", "
                    + toDate + ", " + statuser + ", " + sisteStatus + "))");
            query.addSelect("*");

            boolean filterProsess = filter.getProsessId() != null && filter.getProsessId().length > 0;
            boolean filterUnderprosess = filter.getUnderprosessId() != null && filter.getUnderprosessId().length > 0;
            if (filterProsess || filterUnderprosess || filter.isIngenProsesser()) {
                String prosessWhere = null;
                String underprosessWhere = null;
                if (filterProsess) {
                    prosessWhere = "PROSESS_ID";
                    prosessWhere += createSqlIn(filter.getProsessId());
                }

                if (filterUnderprosess) {
                    underprosessWhere = "UNDERPROSESS_ID";
                    underprosessWhere += createSqlIn(filter.getUnderprosessId());
                }

                if (filter.isIngenProsesser()) {
                    String noProsessWhere = "PROSESS_ID IS NULL";
                    if (filterProsess && filterUnderprosess) {
                        String where = ("(" + prosessWhere + " OR " + underprosessWhere + " OR " + noProsessWhere + ")");
                        query.addWhere(where, AND_OPERATOR);
                    } else if (filterProsess) {
                        String where = ("(" + prosessWhere + " OR " + noProsessWhere + ")");
                        query.addWhere(where, AND_OPERATOR);
                    } else if (filterUnderprosess) {
                        String where = ("(" + underprosessWhere + " OR " + noProsessWhere + ")");
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(noProsessWhere, AND_OPERATOR);
                    }
                } else {
                    if (filterProsess && filterUnderprosess) {
                        String where = ("(" + prosessWhere + " OR " + underprosessWhere + ")");
                        query.addWhere(where, AND_OPERATOR);
                    } else if (filterProsess) {
                        query.addWhere(prosessWhere, AND_OPERATOR);
                    } else if (filterUnderprosess) {
                        query.addWhere(underprosessWhere, AND_OPERATOR);
                    }
                }
            }

            boolean filterAarsaker = filter.getAarsaker() != null && filter.getAarsaker().length > 0;
            if (filterAarsaker || filter.isIngenAarsaker()) {
                String whereAarsak = null;
                if (filterAarsaker) {
                    whereAarsak = "AARSAK_ID";
                    whereAarsak += createSqlIn(filter.getAarsaker());
                }

                if (filter.isIngenAarsaker()) {
                    String ingenAarsak = "AARSAK_ID IS NULL";
                    if (filterAarsaker) {
                        String where = LEFT_P + whereAarsak + OR_OPERATOR + ingenAarsak + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(ingenAarsak, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(whereAarsak, AND_OPERATOR);
                }
            }

            boolean filterTilstand = filter.getTilstander() != null && filter.getTilstander().length > 0;
            if (filterTilstand || filter.isIngenTilstander()) {
                String whereTilstand = null;
                if (filterTilstand) {
                    whereTilstand = "TILSTANDTYPE_ID";
                    whereTilstand += createSqlIn(filter.getTilstander());
                }

                if (filter.isIngenTilstander()) {
                    String ingenTilstand = "TILSTANDTYPE_ID IS NULL";
                    if (filterTilstand) {
                        String where = LEFT_P + whereTilstand + OR_OPERATOR + ingenTilstand + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(ingenTilstand, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(whereTilstand, AND_OPERATOR);
                }
            }

            if (filter.getAndreFiltre() != null) {
                for (PunktregStatusFilter f : filter.getAndreFiltre()) {
                    switch (f) {
                        case TRAFIKKSIKKERHET:
                            query.addWhere("TRAFIKKSIKKERHET_FLAGG = 1", AND_OPERATOR);
                            break;
                        case ETTERSLEP:
                            query.addWhere("ETTERSLEP_FLAGG = 1", AND_OPERATOR);
                            break;
                        case AVVIK_UTENFOR_KONTRAKT:
                            query.addWhere("AVVIK_UTENFOR_KONTRAKT_FLAGG = 1", AND_OPERATOR);
                            break;
                        case FORSIKRINGSSAK:
                            query.addWhere("FORSIKRINGSSKADE_FLAGG = 1", AND_OPERATOR);
                            break;
                        default:
                            break;
                    }
                }
            }

            if ((filter.getVeiInfo() != null && !filter.getVeiInfo().isBlank())
                    || (filter.getOrigo() != null && filter.getBoxReach() > 0)) {
                VeiInfo veiInfo = filter.getVeiInfo();

                if (veiInfo != null && veiInfo.getFylkesnummer() != 0) {
                    query.addWhere("FYLKESNUMMER = " + veiInfo.getFylkesnummer() + " ", AND_OPERATOR);
                }

                if (veiInfo != null && veiInfo.getKommunenummer() != 0) {
                    query.addWhere("KOMMUNENUMMER= " + veiInfo.getKommunenummer() + " ", AND_OPERATOR);
                }

                if (veiInfo != null && !StringUtils.isBlank(veiInfo.getVeikategori())) {
                    query.addWhere("VEIKATEGORI= '" + veiInfo.getVeikategori() + "' ", AND_OPERATOR);
                }

                if (veiInfo != null && !StringUtils.isBlank(veiInfo.getVeistatus())) {
                    query.addWhere("VEISTATUS= '" + veiInfo.getVeistatus() + "' ", AND_OPERATOR);
                }

                if (veiInfo != null && veiInfo.getVeinummer() != 0) {
                    query.addWhere("VEINUMMER= " + veiInfo.getVeinummer() + " ", AND_OPERATOR);
                }

                if (veiInfo != null && veiInfo.getHp() != 0) {
                    query.addWhere("HP= " + veiInfo.getHp() + " ", AND_OPERATOR);
                }

                if (filter.getBoxReach() > 0 && filter.getOrigo() != null) {
                    query.addWhere("X >= " + (filter.getOrigo().getX() - filter.getBoxReach()), AND_OPERATOR);

                    query.addWhere("X <= " + (filter.getOrigo().getX() + filter.getBoxReach()), AND_OPERATOR);

                    query.addWhere("Y >= " + (filter.getOrigo().getY() - filter.getBoxReach()), AND_OPERATOR);

                    query.addWhere("Y <= " + (filter.getOrigo().getY() + filter.getBoxReach()), AND_OPERATOR);
                }
            }

            boolean filterPda = filter.getDfuer() != null && filter.getDfuer().length > 0;
            if (filterPda || filter.isIngenDfuer()) {
                String pdaWhere = null;
                if (filterPda) {
                    pdaWhere = "PDA_DFU_IDENT";
                    pdaWhere += createSqlIn(filter.getDfuer());
                }

                if (filter.isIngenDfuer()) {
                    String noPdaWhere = "(PDA_DFU_IDENT IS NULL OR PDA_DFU_IDENT = -1)";
                    if (filterPda) {
                        String where = LEFT_P + pdaWhere + OR_OPERATOR + noPdaWhere + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(noPdaWhere, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(pdaWhere, AND_OPERATOR);
                }
            }

            if (filter.isKunSlettede()) {
                String whereSlettede = "SLETTET_DATO IS NOT NULL";
                query.addWhere(whereSlettede, AND_OPERATOR);
            } else {
                String whereIkkeSlettede = "SLETTET_DATO IS NULL";
                query.addWhere(whereIkkeSlettede, AND_OPERATOR);
            }

            query.addOrderBy("SISTE_STATUS_DATO", "DESC");

            String queryString = query.toString();
            Query dbQuery = em.createNativeQuery(queryString, "PunktregSokResult");
            logger.debug("sokFunn Will execute query:\n " + queryString + "\n)");
            funn = dbQuery.getResultList();
        }

        if (filter.isFristdatoOverskredet()) {
            List<PunktregSok> filtrertPaFristList = new ArrayList<PunktregSok>();
            for (PunktregSok ps : funn) {
                if (!ps.getSisteStatus().toLowerCase().equals("lukket")) {
                    if (ps.getTiltaksDato() != null && ps.getTiltaksDato().before(new Date())) {
                        filtrertPaFristList.add(ps);
                    }
                }
            }
            logger.debug("sokFunn() filtrert på frist list:" + filtrertPaFristList.size());
            funn = filtrertPaFristList;
        }

        logger.debug("sokFunn(" + filter + ") done with " + funn.size() + " hits");
        return funn;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<HendelseSokView> sokHendelser(HendelseQueryFilter filter) {
        final String method = "sokHendelser(";
        logger.debug(method + filter + ") start");
        List<HendelseSokView> hendelser = null;

        if (filter == null) {
            hendelser = em.createNamedQuery(HendelseSokView.QUERY_FIND_ALL).getResultList();
        } else {
            Map<String, Object> queryParams = new HashMap<String, Object>();
            SelectQueryBuilder query = new SelectQueryBuilder();
            query.addFrom("HendelseSokView h");
            query.addSelect("h");

            if (filter.getKontraktId() != null) {
                query.addWhere("h.kontraktId = " + filter.getKontraktId(), AND_OPERATOR);
            }

            boolean filterPda = filter.getDfuer() != null && filter.getDfuer().length > 0;
            if (filterPda || filter.isIngenDfuer()) {
                String pdaWhere = null;
                if (filterPda) {
                    pdaWhere = "h.pdaDfuIdent";
                    pdaWhere += createSqlIn(filter.getDfuer());
                }

                if (filter.isIngenDfuer()) {
                    String noPdaWhere = "(h.pdaDfuIdent IS NULL OR h.pdaDfuIdent = -1)";
                    if (filterPda) {
                        String where = LEFT_P + pdaWhere + OR_OPERATOR + noPdaWhere + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(noPdaWhere, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(pdaWhere, AND_OPERATOR);
                }
            }

            boolean filterTyper = filter.getTyper() != null && filter.getTyper().length > 0;
            if (filterTyper || filter.isIngenTyper()) {
                String typerWhere = null;
                if (filterTyper) {
                    typerWhere = "h.typeId";
                    typerWhere += createSqlIn(filter.getTyper());
                }

                if (filter.isIngenTyper()) {
                    String ingenTyper = "h.typeId IS NULL";
                    if (filterTyper) {
                        String where = LEFT_P + typerWhere + OR_OPERATOR + ingenTyper + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(ingenTyper, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(typerWhere, AND_OPERATOR);
                }
            }

            boolean filterAarsaker = filter.getAarsaker() != null && filter.getAarsaker().length > 0;
            if (filterAarsaker || filter.isIngenAarsaker()) {
                String whereAarsak = null;
                if (filterAarsaker) {
                    whereAarsak = "h.aarsakId";
                    whereAarsak += createSqlIn(filter.getAarsaker());
                }

                if (filter.isIngenAarsaker()) {
                    String ingenAarsak = "h.aarsakId IS NULL";
                    if (filterAarsaker) {
                        String where = LEFT_P + whereAarsak + OR_OPERATOR + ingenAarsak + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(ingenAarsak, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(whereAarsak, AND_OPERATOR);
                }
            }

            boolean filterTiltak = filter.getTrafikktiltak() != null && filter.getTrafikktiltak().length > 0;
            if (filterTiltak || filter.isIngenTrafikktiltak()) {
                String tiltakWhere = null;
                if (filterTiltak) {
                    query.addJoin("LEFT JOIN h.tiltak t");
                    tiltakWhere = "t.typeId";
                    tiltakWhere += createSqlIn(filter.getTrafikktiltak());
                }

                if (filter.isIngenTrafikktiltak()) {
                    String noTiltakWhere = "h.tiltak IS EMPTY";
                    if (filterTiltak) {
                        String where = ("(" + tiltakWhere + OR_OPERATOR + noTiltakWhere + RIGHT_P);
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(noTiltakWhere, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(tiltakWhere, AND_OPERATOR);
                }
            }

            if (filter.isKunSlettede()) {
                String whereSlettede = "h.slettetDato IS NOT NULL";
                query.addWhere(whereSlettede, AND_OPERATOR);
            } else {
                String whereIkkeSlettede = "h.slettetDato IS NULL";
                query.addWhere(whereIkkeSlettede, AND_OPERATOR);
            }

            if (filter.getFraDato() != null && filter.getTilDato() != null) {
                query.addWhere("h.opprettetDato BETWEEN :fromDate AND :toDate", AND_OPERATOR);
                queryParams.put("fromDate", filter.getFraDato());
                queryParams.put("toDate", lastSecond(filter.getTilDato()));
            } else if (filter.getFraDato() != null) {
                query.addWhere("h.opprettetDato >= :fromDate", AND_OPERATOR);
                queryParams.put("fromDate", filter.getFraDato());
            } else if (filter.getTilDato() != null) {
                query.addWhere("h.opprettetDato <= :toDate", AND_OPERATOR);
                queryParams.put("toDate", lastSecond(filter.getTilDato()));
            }

            if (!StringUtils.isBlank(filter.getSkadested())) {
                query.addWhere("h.skadested LIKE :skadested", AND_OPERATOR);
                queryParams.put("skadested", filter.getSkadested());
            }
            if (filter.isForsikringssak() != null) {
                if (filter.isForsikringssak()) {
                    query.addWhere("h.r5 = 1", AND_OPERATOR);
                }
            }
            if (filter.getSkred() != null) {
                if (filter.getSkred()) {
                    query.addWhere("h.r11 = 1", AND_OPERATOR);
                }
            }

            if ((filter.getVeiInfo() != null && !filter.getVeiInfo().isBlank())
                    || (filter.getOrigo() != null && filter.getBoxReach() > 0)) {
                VeiInfo veiInfo = filter.getVeiInfo();

                if (veiInfo != null && veiInfo.getFylkesnummer() != 0) {
                    query.addWhere("h.fylkesnummer = :fnr ", AND_OPERATOR);
                    queryParams.put("fnr", veiInfo.getFylkesnummer());
                }

                if (veiInfo != null && veiInfo.getKommunenummer() != 0) {
                    query.addWhere("h.kommunenummer = :knr ", AND_OPERATOR);
                    queryParams.put("knr", veiInfo.getKommunenummer());
                }

                if (veiInfo != null && !StringUtils.isBlank(veiInfo.getVeikategori())) {
                    query.addWhere("h.veikategori = :vk ", AND_OPERATOR);
                    queryParams.put("vk", veiInfo.getVeikategori().toUpperCase());
                }

                if (veiInfo != null && !StringUtils.isBlank(veiInfo.getVeistatus())) {
                    query.addWhere("h.veistatus = :vs ", AND_OPERATOR);
                    queryParams.put("vs", veiInfo.getVeistatus().toUpperCase());
                }

                if (veiInfo != null && veiInfo.getVeinummer() != 0) {
                    query.addWhere("h.veinummer = :vn ", AND_OPERATOR);
                    queryParams.put("vn", veiInfo.getVeinummer());
                }

                if (veiInfo != null && veiInfo.getHp() != 0) {
                    query.addWhere("h.fraHp= :hp ", AND_OPERATOR);
                    queryParams.put("hp", veiInfo.getHp());
                }

                if (filter.getBoxReach() > 0 && filter.getOrigo() != null) {
                    query.addWhere("h.fraX >= :minx ", AND_OPERATOR);
                    queryParams.put("minx", filter.getOrigo().getX() - filter.getBoxReach());

                    query.addWhere("h.fraX <= :maxx ", AND_OPERATOR);
                    queryParams.put("maxx", filter.getOrigo().getX() + filter.getBoxReach());

                    query.addWhere("h.fraY >= :miny ", AND_OPERATOR);
                    queryParams.put("miny", filter.getOrigo().getY() - filter.getBoxReach());

                    query.addWhere("h.fraY <= :maxy ", AND_OPERATOR);
                    queryParams.put("maxy", filter.getOrigo().getY() + filter.getBoxReach());
                }
            }

            query.addOrderBy("h.opprettetDato", "DESC");

            String queryString = query.toString();
            Query dbQuery = em.createQuery(queryString);

            logger.debug("Setting queryParams");
            if (queryParams.size() > 0) {
                Iterator<String> keys = queryParams.keySet().iterator();
                while (keys.hasNext()) {
                    String key = keys.next();
                    Object value = queryParams.get(key);
                    if (value instanceof Date) {
                        dbQuery.setParameter(key, (Date) value, TemporalType.TIMESTAMP);
                    } else {
                        dbQuery.setParameter(key, value);
                    }
                }
            }

            logger.debug(method + "Will execute query:\n " + queryString + "\n)");
            hendelser = dbQuery.getResultList();
        }

        logger.debug(method + filter + ") done");
        return hendelser;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<InspeksjonSokView> sokInspeksjoner(InspeksjonQueryFilter filter) {
        logger.debug("sokInspeksjoner(" + filter + ") start");
        List<InspeksjonSokView> inspeksjoner;

        if (filter == null) {
            inspeksjoner = em.createNamedQuery(InspeksjonSokView.QUERY_FIND_ALL).getResultList();
        } else {
            Map<String, Object> queryParams = new HashMap<String, Object>();
            SelectQueryBuilder query = new SelectQueryBuilder();
            query.addFrom("InspeksjonSokView i");
            query.addSelect("DISTINCT i");
            if (filter.getKontraktId() != null) {
                query.addWhere("i.kontraktId = " + filter.getKontraktId(), AND_OPERATOR);
            }

            boolean filterProsess = filter.getProsessId() != null && filter.getProsessId().length > 0;
            if (filterProsess || filter.isIngenProsesser()) {
                String prosessWhere = null;
                String underprosessWhere = null;

                if (filterProsess) {
                    query.addJoin("LEFT JOIN i.prosesser p");
                    prosessWhere = "p.prosessId";
                    prosessWhere += createSqlIn(filter.getProsessId());
                }


                if (filter.isIngenProsesser()) {
                    String noProsessWhere = null;
                    if (filterProsess) {
                        noProsessWhere = "i.prosesser IS EMPTY";
                        String where = LEFT_P + prosessWhere + OR_OPERATOR + noProsessWhere + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        noProsessWhere = "(i.prosesser IS EMPTY AND i.underprosesser IS EMPTY)";
                        query.addWhere(noProsessWhere, AND_OPERATOR);
                    }
                } else {
                    if (filterProsess) {
                        query.addWhere(prosessWhere, AND_OPERATOR);
                    }
                }
            }

            boolean filterPda = filter.getDfuer() != null && filter.getDfuer().length > 0;
            if (filterPda || filter.isIngenDfuer()) {
                String pdaWhere = null;
                if (filterPda) {
                    pdaWhere = "i.pdaDfuIdent";
                    pdaWhere += createSqlIn(filter.getDfuer());
                }

                if (filter.isIngenDfuer()) {
                    String noPdaWhere = "(i.pdaDfuIdent IS NULL OR i.pdaDfuIdent = -1)";
                    if (filterPda) {
                        String where = LEFT_P + pdaWhere + OR_OPERATOR + noPdaWhere + RIGHT_P;
                        query.addWhere(where, AND_OPERATOR);
                    } else {
                        query.addWhere(noPdaWhere, AND_OPERATOR);
                    }
                } else {
                    query.addWhere(pdaWhere, AND_OPERATOR);
                }
            }

            if (filter.isKunSlettede()) {
                String whereSlettede = "i.slettetDato IS NOT NULL";
                query.addWhere(whereSlettede, AND_OPERATOR);
            } else {
                String whereIkkeSlettede = "i.slettetDato IS NULL";
                query.addWhere(whereIkkeSlettede, AND_OPERATOR);
            }

            if (filter.isKunEtterslep()) {
                String kunEtterslep = "i.etterslepFlagg = 1";
                query.addWhere(kunEtterslep, AND_OPERATOR);
            }

            if (filter.getFraDato() != null && filter.getTilDato() != null) {
                query.addWhere("i.fra BETWEEN :fromDate AND :toDate", AND_OPERATOR);
                queryParams.put("fromDate", filter.getFraDato());
                queryParams.put("toDate", lastSecond(filter.getTilDato()));
            } else if (filter.getFraDato() != null) {
                query.addWhere("i.fra >= :fromDate", AND_OPERATOR);
                queryParams.put("fromDate", filter.getFraDato());
            } else if (filter.getTilDato() != null) {
                query.addWhere("i.fra <= :toDate", AND_OPERATOR);
                queryParams.put("toDate", lastSecond(filter.getTilDato()));
            }

            if (filter.getVeiInfo() != null && !filter.getVeiInfo().isBlank()) {
                VeiInfo veiInfo = filter.getVeiInfo();
                query.addJoin("LEFT JOIN i.strekninger s");

                if (veiInfo.getFylkesnummer() != 0) {
                    query.addWhere("s.fylkesnummer = :fnr ", AND_OPERATOR);
                    queryParams.put("fnr", veiInfo.getFylkesnummer());
                }

                if (veiInfo.getKommunenummer() != 0) {
                    query.addWhere("s.kommunenummer = :knr ", AND_OPERATOR);
                    queryParams.put("knr", veiInfo.getKommunenummer());
                }

                if (!StringUtils.isBlank(veiInfo.getVeikategori())) {
                    query.addWhere("s.veikategori = :vk ", AND_OPERATOR);
                    queryParams.put("vk", String.valueOf(veiInfo.getVeikategori()).toUpperCase());
                }

                if (!StringUtils.isBlank(veiInfo.getVeistatus())) {
                    query.addWhere("s.veistatus = :vs ", AND_OPERATOR);
                    queryParams.put("vs", veiInfo.getVeistatus().toUpperCase());
                }

                if (veiInfo.getVeinummer() != 0) {
                    query.addWhere("s.veinummer = :vn ", AND_OPERATOR);
                    queryParams.put("vn", veiInfo.getVeinummer());
                }

                if (veiInfo.getHp() != 0) {
                    query.addWhere("s.hp = :hp ", AND_OPERATOR);
                    queryParams.put("hp", veiInfo.getHp());
                }
            }
            query.addOrderBy("i.opprettetDato", "DESC");

            String queryString = query.toString();
            Query dbQuery = em.createQuery(queryString);

            logger.debug("Setting queryParams");
            if (queryParams.size() > 0) {
                Iterator<String> keys = queryParams.keySet().iterator();
                while (keys.hasNext()) {
                    String key = keys.next();
                    Object value = queryParams.get(key);
                    if (value instanceof Date) {
                        dbQuery.setParameter(key, (Date) value, TemporalType.TIMESTAMP);
                    } else {
                        dbQuery.setParameter(key, value);
                    }
                }
            }

            logger.debug("Will execute query: " + queryString);
            inspeksjoner = dbQuery.getResultList();
        }

        logger.debug("sokInspeksjoner(" + filter + ") done, with "
                + (inspeksjoner == null ? "null" : String.valueOf(inspeksjoner.size())));
        return inspeksjoner;
    }

    @Override
    public boolean validatePunktveiref(Punktveiref veiref) {
        logger.debug("validatePunktveiref(" + veiref + ") start");
        Veireferanse veireferanse = getVeireferanseInstance(veiref);

        List<ReflinkSeksjon> list = SerializableReflinkSection.createWebserviceSections(nvdbService.getReflinkSectionsWithinRoadref(new SerializableRoadRef(veireferanse)));

        if (logger.isTraceEnabled()) {
            logger.trace("frahp:    " + veireferanse.getFrahp());
            logger.trace("tilhp:    " + veireferanse.getTilhp());
            logger.trace("frameter: " + veireferanse.getFraMeter());
            logger.trace("tilmeter: " + veireferanse.getTilMeter());
            logger.trace("fylke:    " + veireferanse.getFylkenummer());
            logger.trace("kommune:  " + veireferanse.getKommunenummer());
            logger.trace("veikat:   " + veireferanse.getVegkategori());
            logger.trace("veinr:    " + veireferanse.getVegnummer());
            logger.trace("veistat:  " + veireferanse.getVegstatus());
            logger.trace("veistato: " + veireferanse.getVegstatusoverordnet());
            logger.trace("List: {}", list);
        }
        return list != null && !list.isEmpty();
    }

    public List<Skred> hentAlleSkred() {
        return em.createNamedQuery(Skred.QUERY_FIND_ALL).getResultList();
    }

    public Long getDriftkontraktIdForHendelse(Long refnummer) {
        Long dkId = null;
        try {
            dkId = (Long) em.createQuery("SELECT h.kontraktId from Hendelse h where h.refnummer=:refnummer")
                    .setParameter("refnummer", refnummer)
                    .getSingleResult();
        } catch (NoResultException e) {
        }
        return dkId;
    }

    public Long getDriftkontraktIdForInspeksjon(Long refnummer) {
        Long dkId = null;
        try {
            dkId = (Long) em.createQuery("SELECT h.kontraktId from Inspeksjon h where h.refnummer=:refnummer")
                    .setParameter("refnummer", refnummer)
                    .getSingleResult();
        } catch (NoResultException e) {
        }
        return dkId;
    }

    public Long getDriftkontraktIdForPunktreg(Long refnummer) {
        Long dkId = null;
        try {
            dkId = (Long) em.createQuery("SELECT h.kontraktId from Punktreg h where h.refnummer=:refnummer")
                    .setParameter("refnummer", refnummer)
                    .getSingleResult();
        } catch (NoResultException e) {
        }
        return dkId;
    }
}
