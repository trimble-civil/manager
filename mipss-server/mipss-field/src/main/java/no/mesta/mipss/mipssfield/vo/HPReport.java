package no.mesta.mipss.mipssfield.vo;

import no.mesta.mipss.persistence.mipssfield.InspeksjonSokProsess;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Representerer en rad i rapport over inspeksjon på Hp'er
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class HPReport implements Serializable {
    private Date fra;
    private Double fraKm;
    private InspeksjonSokView inspeksjon;
    private Date til;
    private Double tilKm;
    private VeiInfo veiInfo = new VeiInfo();

    /**
     * Konstruktør som benyttes av JPA spørring
     *
     * @param guid
     * @param fra
     * @param til
     * @param fylkesnummer
     * @param kommunenummer
     * @param veikategori
     * @param veistatus
     * @param veinummer
     * @param hp
     * @param fraKm
     * @param tilKm
     */
    public HPReport(InspeksjonSokView inspeksjon, final Date fra, final Date til, final int fylkesnummer,
                    final int kommunenummer, final String veikategori, final String veistatus, final int veinummer,
                    final int hp, final Double fraKm, final Double tilKm) {
        this.inspeksjon = inspeksjon;
        this.fra = fra;
        this.til = til;
        this.fraKm = fraKm;
        this.tilKm = tilKm;

        veiInfo.setFylkesnummer(fylkesnummer);
        veiInfo.setKommunenummer(kommunenummer);
        veiInfo.setVeikategori(veikategori);
        veiInfo.setVeistatus(veistatus);
        veiInfo.setVeinummer(veinummer);
        veiInfo.setHp(hp);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof HPReport)) {
            return false;
        }

        HPReport other = (HPReport) o;

        return new EqualsBuilder().append(inspeksjon.getGuid(), other.inspeksjon.getGuid()).append(veiInfo.getHp(),
                other.veiInfo.getHp()).isEquals();
    }

    /**
     * Tidspunktet inspeksjonen startet
     *
     * @return
     */
    public Date getFra() {
        return fra;
    }

    /**
     * Fra km
     *
     * @return
     */
    public Double getFraKm() {
        return fraKm;
    }

    /**
     * Fylke hvor veien som er inspisert er i
     *
     * @return
     */
    public int getFylkesnummer() {
        return veiInfo.getFylkesnummer();
    }

    /**
     * Guid for inspeksjonen
     *
     * @return
     */
    public String getGuid() {
        return inspeksjon.getGuid();
    }

    /**
     * Hovedparsell nummeret
     *
     * @return
     */
    public int getHp() {
        return veiInfo.getHp();
    }

    /**
     * Skriver ut en GUI vennlig text av stedfestingen
     *
     * @return
     */
    public String getHPText() {
        return veiInfo.getVei() + " hp" + veiInfo.getHp();
    }

    /**
     * Inspeksjonen
     *
     * @return
     */
    public InspeksjonSokView getInspeksjon() {
        return inspeksjon;
    }

    /**
     * Kommunen hvor veien som er inspisert i
     *
     * @return
     */
    public int getKommunenummer() {
        return veiInfo.getKommunenummer();
    }

    /**
     * Hvem utførte inspeksjonen
     *
     * @return
     */
    public String getOpprettet_av() {
        return inspeksjon.getOpprettetAv();
    }

    /**
     * Opprettet dato
     *
     * @return
     */
    public Date getOpprettet_dato() {
        return inspeksjon.getOpprettetDato();
    }

    /**
     * Inspiserte prosesser
     *
     * @return
     */
    public List<InspeksjonSokProsess> getProsesser() {
        return inspeksjon.getProsesser();
    }

    /**
     * Gir en tekststring med prosesskoder
     *
     * @return
     */
    public String getProsesserGUIText() {
        return inspeksjon.getProsesserTekst();
    }

    /**
     * Temperatur under inspeksjonen
     *
     * @return
     */
    public String getTemperaturNavn() {
        return inspeksjon.getTemperatur();
    }

    /**
     * Tidspunktet inspeksjonen sluttet
     *
     * @return
     */
    public Date getTil() {
        return til;
    }

    /**
     * Til km
     *
     * @return
     */
    public Double getTilKm() {
        return tilKm;
    }

    /**
     * Været under inspeksjonen
     *
     * @return
     */
    public String getVaerNavn() {
        return inspeksjon.getVaer();
    }

    /**
     * Veikategorien som er inspisert
     *
     * @return
     */
    public String getVeikategori() {
        return veiInfo.getVeikategori();
    }

    /**
     * Veinummeret
     *
     * @return
     */
    public int getVeinummer() {
        return veiInfo.getVeinummer();
    }

    /**
     * Alltid V for vei....
     *
     * @return
     */
    public String getVeistatus() {
        return veiInfo.getVeistatus();
    }

    /**
     * Vind under inspeksjonen
     *
     * @return
     */
    public String getVindNavn() {
        return inspeksjon.getVind();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(inspeksjon.getGuid()).append(veiInfo.getHp()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("inspeksjon", inspeksjon).append("fra", fra).append("til", til).append(
                "veiInfo", veiInfo).toString();
    }
}
