package no.mesta.mipss.mipssfield.vo;

import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Resultat etter å ha manipulert punktreg i db
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PunktregResult implements Serializable {
    private String guidPunktreg = null;
    private Avvik punktreg = null;
    private PunktregSok view = null;

    /**
     * Konstruktør
     */
    public PunktregResult() {
    }

    /**
     * Resultatets GUID
     *
     * @param guidPunktreg
     */
    public void setGuidPunktreg(String guidPunktreg) {
        this.guidPunktreg = guidPunktreg;
    }

    /**
     * Resultatets GUID
     *
     * @return
     */
    public String getGuidPunktreg() {
        return guidPunktreg;
    }

    /**
     * Funn
     *
     * @param punktreg
     */
    public void setPunktreg(Avvik punktreg) {
        this.punktreg = punktreg;
    }

    /**
     * Funn
     *
     * @return
     */
    public Avvik getPunktreg() {
        return punktreg;
    }

    /**
     * View rad for funnet
     *
     * @param view
     */
    public void setView(PunktregSok view) {
        this.view = view;
    }

    /**
     * Viewrad for funnet
     *
     * @return
     */
    public PunktregSok getView() {
        return view;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof PunktregResult)) {
            return false;
        }

        PunktregResult other = (PunktregResult) o;

        return new EqualsBuilder().append(guidPunktreg, other.getGuidPunktreg()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(guidPunktreg).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("guidPunktreg", guidPunktreg).append("punktreg", punktreg).append(
                "view", view).toString();
    }
}
