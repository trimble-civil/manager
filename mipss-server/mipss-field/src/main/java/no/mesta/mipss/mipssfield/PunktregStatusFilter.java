package no.mesta.mipss.mipssfield;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.IRenderableMipssEntity;


/**
 * Enum for filtre i funn skjermbilde
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public enum PunktregStatusFilter implements IRenderableMipssEntity {
    ETTERSLEP("backlog"),
    AVVIK_UTENFOR_KONTRAKT("extraWork"),
    TRAFIKKSIKKERHET("trafficSafety"),
    FORSIKRINGSSAK("insuranceClaim");

    private String text = null;
    private static transient PropertyResourceBundleUtil props = null;

    private PunktregStatusFilter(String text) {
        this.text = text;
    }

    /**
     * GUI teksten for enum verdi
     *
     * @return
     * @see no.mesta.mipss.persistence.IRenderableMipssEntity
     */
    public String getTextForGUI() {
        return getProps().getGuiString(text);
    }

//    /**
//     * Alle filtre i denne enum
//     * 
//     * @return
//     */
//    public static PunktregStatusFilter[] alle() {
//        return values();
//    }

    private static PropertyResourceBundleUtil getProps() {
        if (props == null) {
            props = PropertyResourceBundleUtil.getPropertyBundle("mipssFieldTexts");
        }

        return props;
    }
}
