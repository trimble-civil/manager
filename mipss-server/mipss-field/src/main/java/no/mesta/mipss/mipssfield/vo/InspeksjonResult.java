package no.mesta.mipss.mipssfield.vo;

import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Resultat etter å ha manipulert hendelse i databasen
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class InspeksjonResult implements Serializable {
    private String guidInspeksjon;
    private Inspeksjon inspeksjon;
    private InspeksjonSokView inspeksjonSokView;

    /**
     * Konstruktør
     *
     * @param hendelse
     */
    public InspeksjonResult(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
        guidInspeksjon = inspeksjon.getGuid();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof InspeksjonResult)) {
            return false;
        }

        InspeksjonResult other = (InspeksjonResult) o;

        return new EqualsBuilder().append(guidInspeksjon, other.guidInspeksjon).isEquals();
    }

    /**
     * GUID
     *
     * @return
     */
    public String getGuidInspeksjon() {
        return guidInspeksjon;
    }

    /**
     * Retur objektet
     *
     * @return
     */
    public Inspeksjon getInspeksjon() {
        return inspeksjon;
    }

    public InspeksjonSokView getInspeksjonSokView() {
        return inspeksjonSokView;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(guidInspeksjon).toHashCode();
    }

    /**
     * GUID
     *
     * @param guidHendelse
     */
    public void setGuidInspeksjon(String guidInspeksjon) {
        this.guidInspeksjon = guidInspeksjon;
    }

    /**
     * Retur objektet
     *
     * @param hendelse
     */
    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
    }

    public void setInspeksjonSokView(InspeksjonSokView inspeksjonSokView) {
        this.inspeksjonSokView = inspeksjonSokView;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("guidInspeksjon", guidInspeksjon).append("inspeksjon", inspeksjon)
                .append("inspeksjonSokView", inspeksjonSokView).toString();
    }
}
