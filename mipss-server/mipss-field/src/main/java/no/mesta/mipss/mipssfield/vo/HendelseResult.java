package no.mesta.mipss.mipssfield.vo;

import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Resultat etter å ha manipulert hendelse i databasen
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class HendelseResult implements Serializable {
    private String guidHendelse;
    private Hendelse hendelse;
    private HendelseSokView hendelseSokView;

    /**
     * Konstruktør
     *
     * @param hendelse
     */
    public HendelseResult(Hendelse hendelse) {
        this.hendelse = hendelse;
        guidHendelse = hendelse.getGuid();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof HendelseResult)) {
            return false;
        }

        HendelseResult other = (HendelseResult) o;

        return new EqualsBuilder().append(guidHendelse, other.getGuidHendelse()).isEquals();
    }

    /**
     * GUID
     *
     * @return
     */
    public String getGuidHendelse() {
        return guidHendelse;
    }

    /**
     * Retur objektet
     *
     * @return
     */
    public Hendelse getHendelse() {
        return hendelse;
    }

    public HendelseSokView getHendelseSokView() {
        return hendelseSokView;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(guidHendelse).toHashCode();
    }

    /**
     * GUID
     *
     * @param guidHendelse
     */
    public void setGuidHendelse(String guidHendelse) {
        this.guidHendelse = guidHendelse;
    }

    /**
     * Retur objektet
     *
     * @param hendelse
     */
    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
    }

    public void setHendelseSokView(HendelseSokView hendelseSokView) {
        this.hendelseSokView = hendelseSokView;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("guidHendelse", guidHendelse).append("hendelse", hendelse).append(
                "hendelseSokView", hendelseSokView).toString();
    }
}
