package no.mesta.mipss.mipssfield;

import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface MipssFieldLocal {
    List<InspeksjonSokView> sokInspeksjoner(InspeksjonQueryFilter filter);

    List<HendelseSokView> sokHendelser(HendelseQueryFilter filter);

    Hendelse hentHendelse(String guid);
}
