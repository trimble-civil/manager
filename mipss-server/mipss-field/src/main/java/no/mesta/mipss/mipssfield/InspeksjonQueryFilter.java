package no.mesta.mipss.mipssfield;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * Filter som begrenser søk etter inspeksjoner
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class InspeksjonQueryFilter extends FilterBase implements Serializable {
    private static final String FIELD_KUNETTERSLEP = "kunEtterslep";
    private boolean kunEtterslep;

    /**
     * Konstruktør
     */
    public InspeksjonQueryFilter(Date d) {
        super(d);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof InspeksjonQueryFilter)) {
            return false;
        }

        InspeksjonQueryFilter other = (InspeksjonQueryFilter) o;

        return new EqualsBuilder().appendSuper(super.equals(other)).append(kunEtterslep, other.kunEtterslep).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().appendSuper(super.hashCode()).append(kunEtterslep).toHashCode();
    }

    public boolean isKunEtterslep() {
        return kunEtterslep;
    }

    public void setKunEtterslep(boolean kunEtterslep) {
        boolean oldValue = this.kunEtterslep;
        this.kunEtterslep = kunEtterslep;
        getProps().firePropertyChange(FIELD_KUNETTERSLEP, oldValue, kunEtterslep);
    }
}
