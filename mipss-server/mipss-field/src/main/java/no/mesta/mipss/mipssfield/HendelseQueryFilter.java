package no.mesta.mipss.mipssfield;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;

/**
 * Filter som begrenser søk etter inspeksjoner
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class HendelseQueryFilter extends FilterBase implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(HendelseQueryFilter.class);

    private static final String FIELD_NAME_AARSAKER = "aarsaker";
    private static final String FIELD_NAME_FORSIKRINGSSAK = "forsikringssak";
    private static final String FIELD_NAME_SKRED = "skred";
    private static final String FIELD_NAME_INGENAARSAKER = "ingenAarsaker";
    private static final String FIELD_NAME_INGENTRAFIKKTILTAK = "ingenTrafikktiltak";
    private static final String FIELD_NAME_INGENTYPER = "ingenTyper";
    private static final String FIELD_NAME_KOMMUNE = "kommune";
    private static final String FIELD_NAME_SKADESTED = "skadested";
    private static final String FIELD_NAME_TRAFIKKTILTAK = "trafikktiltak";
    private static final String FIELD_NAME_TYPER = "typer";
    private Long[] aarsaker;
    private Boolean forsikringssak;
    private Boolean skred;
    private boolean ingenAarsaker;
    private boolean ingenTrafikktiltak;
    private boolean ingenTyper;
    private String kommune;
    private String skadested;
    private Long[] trafikktiltak;
    private Long[] typer;

    /**
     * Konstruktør
     */
    public HendelseQueryFilter(Date d) {
        super(d);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof HendelseQueryFilter)) {
            return false;
        }

        HendelseQueryFilter other = (HendelseQueryFilter) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(kommune, other.kommune)
                .append(skadested, other.skadested)
                .append(forsikringssak, other.forsikringssak)
                .append(skred, other.skred)
                .append(typer, other.typer)
                .append(aarsaker, other.aarsaker)
                .append(trafikktiltak, other.trafikktiltak)
                .append(ingenAarsaker, other.ingenAarsaker)
                .append(ingenTyper, other.ingenTyper)
                .append(ingenTrafikktiltak, other.ingenTrafikktiltak)
                .isEquals();
    }

    public Long[] getAarsaker() {
        return aarsaker;
    }

    /**
     * Kommune
     *
     * @return
     */
    public String getKommune() {
        return kommune;
    }

    /**
     * Skadested
     *
     * @return
     */
    public String getSkadested() {
        return skadested;
    }

    public Long[] getTrafikktiltak() {
        return trafikktiltak;
    }

    public Long[] getTyper() {
        return typer;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().appendSuper(super.hashCode())
                .append(skadested)
                .append(kommune)
                .append(forsikringssak)
                .append(skred)
                .append(typer)
                .append(aarsaker)
                .append(trafikktiltak)
                .append(ingenAarsaker)
                .append(ingenTyper)
                .append(ingenTrafikktiltak)
                .toHashCode();
    }

    /**
     * Om det skal søkes på forsikringssaker
     *
     * @return
     */
    public Boolean isForsikringssak() {
        return forsikringssak;
    }

    public Boolean getSkred() {
        return skred;
    }

    public boolean isIngenAarsaker() {
        return ingenAarsaker;
    }

    public boolean isIngenTrafikktiltak() {
        return ingenTrafikktiltak;
    }

    public boolean isIngenTyper() {
        return ingenTyper;
    }

    /**
     * Årsaker til hendelse
     *
     * @param aarsaker
     */
    public void setAarsaker(Long[] aarsaker) {
        Long[] old = this.aarsaker;
        this.aarsaker = aarsaker;
        getProps().firePropertyChange(FIELD_NAME_AARSAKER, old, aarsaker);
    }

    /**
     * Om det skal søkes på forsikringssaker
     *
     * @param forsikringssak
     */
    public void setForsikringssak(Boolean forsikringssak) {
        Boolean old = this.forsikringssak;
        this.forsikringssak = forsikringssak;
        getProps().firePropertyChange(FIELD_NAME_FORSIKRINGSSAK, old, forsikringssak);
    }

    public void setSkred(Boolean skred) {
        Boolean old = this.skred;
        this.skred = skred;
        getProps().firePropertyChange(FIELD_NAME_SKRED, old, skred);
    }

    public void setIngenAarsaker(boolean ingenAarsaker) {
        log.trace("setIngenAarsaker({})", ingenAarsaker);
        boolean old = this.ingenAarsaker;
        this.ingenAarsaker = ingenAarsaker;
        getProps().firePropertyChange(FIELD_NAME_INGENAARSAKER, old, ingenAarsaker);
    }

    public void setIngenTrafikktiltak(boolean ingenTrafikktiltak) {
        log.trace("setIngenTrafikktiltak({})", ingenTrafikktiltak);
        boolean old = this.ingenTrafikktiltak;
        this.ingenTrafikktiltak = ingenTrafikktiltak;
        getProps().firePropertyChange(FIELD_NAME_INGENTRAFIKKTILTAK, old, ingenTrafikktiltak);
    }

    public void setIngenTyper(boolean ingenTyper) {
        log.trace("setIngenTyper({})", ingenTyper);
        boolean old = this.ingenTyper;
        this.ingenTyper = ingenTyper;
        getProps().firePropertyChange(FIELD_NAME_INGENTYPER, old, ingenTyper);
    }

    /**
     * Kommune
     *
     * @param kommune
     */
    public void setKommune(String kommune) {
        String old = this.kommune;
        this.kommune = kommune;
        getProps().firePropertyChange(FIELD_NAME_KOMMUNE, old, kommune);
    }

    /**
     * Skadested
     *
     * @param skadested
     */
    public void setSkadested(String skadested) {
        String old = this.skadested;
        this.skadested = skadested;
        getProps().firePropertyChange(FIELD_NAME_SKADESTED, old, skadested);
    }

    public void setTrafikktiltak(Long[] trafikktiltak) {
        log.trace("setTrafikktiltak({})", (Object[]) trafikktiltak);
        Long[] old = this.trafikktiltak;
        this.trafikktiltak = trafikktiltak;
        getProps().firePropertyChange(FIELD_NAME_TRAFIKKTILTAK, old, trafikktiltak);
    }

    /**
     * Typer hendelse
     *
     * @param typer
     */
    public void setTyper(Long[] typer) {
        Long[] old = this.typer;
        this.typer = typer;
        getProps().firePropertyChange(FIELD_NAME_TYPER, old, typer);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).appendSuper(super.toString())
                .append(FIELD_NAME_SKADESTED, skadested)
                .append(FIELD_NAME_KOMMUNE, kommune)
                .append(FIELD_NAME_FORSIKRINGSSAK, forsikringssak)
                .append(FIELD_NAME_SKRED, skred)
                .append(FIELD_NAME_TYPER, typer)
                .append(FIELD_NAME_AARSAKER, aarsaker)
                .append(FIELD_NAME_TRAFIKKTILTAK, trafikktiltak)
                .append(FIELD_NAME_INGENAARSAKER, ingenAarsaker)
                .append(FIELD_NAME_INGENTYPER, ingenTyper)
                .append(FIELD_NAME_INGENTRAFIKKTILTAK, ingenTrafikktiltak)
                .toString();
    }
}
