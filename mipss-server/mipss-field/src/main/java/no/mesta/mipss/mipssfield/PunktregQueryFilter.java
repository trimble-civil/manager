package no.mesta.mipss.mipssfield;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;

/**
 * Filter som begrenser søket
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PunktregQueryFilter extends FilterBase implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(PunktregQueryFilter.class);

    private static final String FIELD_NAME_AARSAKER = "aarsaker";
    private static final String FIELD_NAME_ANDREFILTRE = "andreFiltre";
    private static final String FIELD_NAME_INGENAARSAKER = "ingenAarsaker";
    private static final String FIELD_NAME_INGENTILSTANDER = "ingenTilstander";
    private static final String FIELD_NAME_SISTESTATUS = "sisteStatus";
    private static final String FIELD_NAME_STATUSER = "statuser";
    private static final String FIELD_NAME_TILSTANDER = "tilstander";
    private static final String FIELD_NAME_FRISTDATO = "fristdatoOverskredet";

    private Long[] aarsaker;
    private PunktregStatusFilter[] andreFiltre;
    private boolean ingenAarsaker;
    private boolean ingenTilstander;
    private boolean sisteStatus = true;
    private boolean fristdatoOverskredet;
    private String[] statuser;
    private Long[] tilstander;

    /**
     * Konstruktør
     */
    public PunktregQueryFilter(Date d) {
        super(d);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof PunktregQueryFilter)) {
            return false;
        }

        PunktregQueryFilter other = (PunktregQueryFilter) o;

        return new EqualsBuilder().appendSuper(super.equals(other)).append(andreFiltre, other.getAndreFiltre()).append(
                tilstander, other.tilstander).append(statuser, other.statuser).append(aarsaker, other.aarsaker).append(
                ingenAarsaker, other.ingenAarsaker).append(ingenTilstander, other.ingenTilstander).append(sisteStatus, other.sisteStatus).append(fristdatoOverskredet, other.fristdatoOverskredet).isEquals();
    }

    public Long[] getAarsaker() {
        return aarsaker;
    }

    /**
     * Andre filere
     *
     * @return
     */
    public PunktregStatusFilter[] getAndreFiltre() {
        return andreFiltre;
    }

    public String[] getStatuser() {
        return statuser;
    }

    public Long[] getTilstander() {
        return tilstander;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().appendSuper(super.hashCode()).append(andreFiltre).append(tilstander).append(
                statuser).append(aarsaker).append(ingenAarsaker).append(ingenTilstander).append(sisteStatus).append(fristdatoOverskredet).toHashCode();
    }

    public boolean isIngenAarsaker() {
        return ingenAarsaker;
    }

    public boolean isIngenTilstander() {
        return ingenTilstander;
    }

    public boolean isSisteStatus() {
        return sisteStatus;
    }

    public boolean isFristdatoOverskredet() {
        return fristdatoOverskredet;
    }

    public void setAarsaker(Long[] aarsaker) {
        log.trace("setAarsaker({})", (Object[]) aarsaker);
        Long[] old = this.aarsaker;
        this.aarsaker = aarsaker;
        getProps().firePropertyChange(FIELD_NAME_AARSAKER, old, aarsaker);
    }

    /**
     * Andre filtere
     *
     * @param andreFiltre
     */
    public void setAndreFiltre(PunktregStatusFilter[] andreFiltre) {
        log.trace("setAndreFiltre({})", (Object[]) andreFiltre);
        PunktregStatusFilter[] old = this.andreFiltre;
        this.andreFiltre = andreFiltre;
        getProps().firePropertyChange(FIELD_NAME_ANDREFILTRE, old, andreFiltre);
    }

    public void setIngenAarsaker(boolean ingenAarsaker) {
        log.trace("setIngenAarsaker({})", ingenAarsaker);
        boolean old = this.ingenAarsaker;
        this.ingenAarsaker = ingenAarsaker;
        getProps().firePropertyChange(FIELD_NAME_INGENAARSAKER, old, ingenAarsaker);
    }

    public void setIngenTilstander(boolean ingenTilstander) {
        log.trace("setIngenTilstander({})", ingenTilstander);
        boolean old = this.ingenTilstander;
        this.ingenTilstander = ingenTilstander;
        getProps().firePropertyChange(FIELD_NAME_INGENTILSTANDER, old, ingenTilstander);
    }

    public void setSisteStatus(boolean sisteStatus) {
        log.trace("setSisteStatus({})", sisteStatus);
        boolean old = this.sisteStatus;
        this.sisteStatus = sisteStatus;
        getProps().firePropertyChange(FIELD_NAME_SISTESTATUS, old, sisteStatus);
    }

    public void setFristdatoOverskredet(boolean fristdatoOverskredet) {
        boolean old = this.fristdatoOverskredet;
        this.fristdatoOverskredet = fristdatoOverskredet;
        getProps().firePropertyChange(FIELD_NAME_FRISTDATO, old, fristdatoOverskredet);
    }

    public void setStatuser(String[] statuser) {
        log.trace("setStatuser({})", (Object[]) statuser);
        String[] old = this.statuser;
        this.statuser = statuser;
        getProps().firePropertyChange(FIELD_NAME_STATUSER, old, statuser);
    }

    public void setTilstander(Long[] tilstander) {
        log.trace("setTilstander({})", (Object[]) tilstander);
        Long[] old = this.tilstander;
        this.tilstander = tilstander;
        getProps().firePropertyChange(FIELD_NAME_TILSTANDER, old, tilstander);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).appendSuper(super.toString()).append(FIELD_NAME_ANDREFILTRE, andreFiltre)
                .append(FIELD_NAME_TILSTANDER, tilstander).append(FIELD_NAME_STATUSER, statuser).append(
                        FIELD_NAME_AARSAKER, aarsaker).append(FIELD_NAME_INGENAARSAKER, ingenAarsaker).append(
                        FIELD_NAME_INGENTILSTANDER, ingenTilstander).append(FIELD_NAME_SISTESTATUS, sisteStatus).append(FIELD_NAME_FRISTDATO, fristdatoOverskredet).toString();
    }
}
