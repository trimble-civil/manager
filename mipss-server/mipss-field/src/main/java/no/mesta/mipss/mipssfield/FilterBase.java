package no.mesta.mipss.mipssfield;

import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * Basisfilter for søk etter funn, hendelser og inspeksjoner
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class FilterBase implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(FilterBase.class);

    private static final String FIELD_NAME_DFUER = "dfuer";
    private static final String FIELD_NAME_FRADATO = "fraDato";
    private static final String FIELD_NAME_INGENDFUER = "ingenDfuer";
    private static final String FIELD_NAME_INGENPROSESSER = "ingenProsesser";
    private static final String FIELD_NAME_KONTRAKTID = "kontraktId";
    private static final String FIELD_NAME_KUNSLETTEDE = "kunSlettede";
    private static final String FIELD_NAME_METERFRA = "meterFra";
    private static final String FIELD_NAME_METERTIL = "meterTil";
    private static final String FIELD_NAME_PROSESSID = "prosessId";
    private static final String FIELD_NAME_UNDERPROSESSID = "underprosessId";
    private static final String FIELD_NAME_TIDLIGSTFRADATO = "tidligstFraDato";
    private static final String FIELD_NAME_TILDATO = "tilDato";
    private static final String FIELD_NAME_VEIINFO = "veiInfo";
    private double boxReach;
    private Long[] dfuer;
    private Date fraDato;
    private boolean ingenDfuer;
    private boolean ingenProsesser;
    private Long kontraktId;
    private boolean kunSlettede;
    private transient Logger logger;
    private Long meterFra;
    private Long meterTil;
    private Point origo;
    private transient PropertyChangeSupport props = new PropertyChangeSupport(this);
    private Long[] prosessId;
    private Long[] underprosessId;
    private Date tidligsteFraDato;
    private Date tilDato;
    private VeiInfo veiInfo;

    /**
     * Konstruktør
     */
    public FilterBase(Date d) {
        tidligsteFraDato = d;
    }

    /**
     * Delegate
     *
     * @param l
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        getProps().addPropertyChangeListener(l);
    }

    /**
     * Delegate
     *
     * @param l
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
     * PropertyChangeListener)
     */
    public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
        getProps().addPropertyChangeListener(propName, l);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof FilterBase)) {
            return false;
        }

        FilterBase other = (FilterBase) o;

        return new EqualsBuilder().append(kontraktId, other.kontraktId).append(fraDato, other.fraDato).append(tilDato,
                other.tilDato).append(veiInfo, other.veiInfo).append(meterFra, other.meterFra).append(meterTil,
                other.meterTil)
                .append(prosessId, other.prosessId)
                .append(underprosessId, other.underprosessId)
                .append(ingenProsesser, other.ingenProsesser).append(
                        tidligsteFraDato, other.tidligsteFraDato).append(dfuer, other.dfuer).append(ingenDfuer,
                        other.ingenDfuer).append(kunSlettede, other.kunSlettede).append(origo, other.origo).append(boxReach,
                        other.boxReach).isEquals();
    }

    public double getBoxReach() {
        return boxReach;
    }

    public Long[] getDfuer() {
        return dfuer;
    }

    public Date getFraDato() {
        if (fraDato == null) {
            return tidligsteFraDato;
        } else {
            if (tidligsteFraDato == null) {
                return fraDato;
            } else {
                if (fraDato.before(tidligsteFraDato)) {
                    return tidligsteFraDato;
                } else {
                    return fraDato;
                }
            }
        }
    }

    public Long getKontraktId() {
        return kontraktId;
    }

    public Long getMeterFra() {
        return meterFra;
    }

    public Long getMeterTil() {
        return meterTil;
    }

    public Point getOrigo() {
        return origo;
    }

    protected PropertyChangeSupport getProps() {
        return props;
    }

    /**
     * Prosess
     *
     * @return
     */
    public Long[] getProsessId() {
        return prosessId;
    }

    public Long[] getUnderprosessId() {
        return underprosessId;
    }

    public Date getTidligsteFraDato() {
        return tidligsteFraDato;
    }

    public Date getTilDato() {
        return tilDato;
    }

    public VeiInfo getVeiInfo() {
        return veiInfo;
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kontraktId).append(fraDato).append(tilDato).append(veiInfo)
                .append(meterFra).append(meterTil).append(prosessId).append(underprosessId)
                .append(ingenProsesser).append(dfuer).append(
                        ingenDfuer).append(kunSlettede).append(origo).append(boxReach).toHashCode();
    }

    public boolean isIngenDfuer() {
        return ingenDfuer;
    }

    public boolean isIngenProsesser() {
        return ingenProsesser;
    }

    public boolean isKunSlettede() {
        return kunSlettede;
    }

    /**
     * Delegate
     *
     * @param l
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        getProps().removePropertyChangeListener(l);
    }

    /**
     * Delegate
     *
     * @param l
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
     * PropertyChangeListener)
     */
    public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
        getProps().removePropertyChangeListener(propName, l);
    }

    public void setBoxReach(double d) {
        boxReach = d;
    }

    public void setDfuer(Long[] dfuer) {
        log.trace("setDfuer({})", (Object[]) dfuer);
        Long[] old = this.dfuer;
        this.dfuer = dfuer;
        getProps().firePropertyChange(FIELD_NAME_DFUER, old, dfuer);
    }

    public void setFraDato(Date fraDato) {
        log.trace("setFraDato({})", fraDato);
        Date old = this.fraDato;
        this.fraDato = fraDato;
        getProps().firePropertyChange(FIELD_NAME_FRADATO, old, fraDato);
    }

    public void setIngenDfuer(boolean ingenDfuer) {
        boolean old = this.ingenDfuer;
        this.ingenDfuer = ingenDfuer;
        getProps().firePropertyChange(FIELD_NAME_INGENDFUER, old, ingenDfuer);
    }

    public void setIngenProsesser(boolean ingenProsesser) {
        log.trace("setIngenProsesser({})", ingenProsesser);
        boolean old = this.ingenProsesser;
        this.ingenProsesser = ingenProsesser;
        getProps().firePropertyChange(FIELD_NAME_INGENPROSESSER, old, ingenProsesser);
    }

    public void setKontraktId(Long kontraktId) {
        log.trace("setKontraktId({})", kontraktId);
        Long old = this.kontraktId;
        this.kontraktId = kontraktId;
        getProps().firePropertyChange(FIELD_NAME_KONTRAKTID, old, kontraktId);
    }

    public void setKunSlettede(boolean kunSlettede) {
        log.trace("setKunSlettede({})", kunSlettede);
        boolean oldValue = this.kunSlettede;
        this.kunSlettede = kunSlettede;
        getProps().firePropertyChange(FIELD_NAME_KUNSLETTEDE, oldValue, kunSlettede);
    }

    public void setMeterFra(Long meterFra) {
        log.trace("setMeterFra({})", meterFra);
        Long old = this.meterFra;
        this.meterFra = meterFra;
        getProps().firePropertyChange(FIELD_NAME_METERFRA, old, meterFra);
    }

    public void setMeterTil(Long meterTil) {
        log.trace("setMeterTil({})", meterTil);
        Long old = this.meterTil;
        this.meterTil = meterTil;
        getProps().firePropertyChange(FIELD_NAME_METERTIL, old, meterTil);
    }

    public void setOrigo(Point origo) {
        this.origo = origo;
    }

    /**
     * Prosess
     *
     * @param prosessId
     */
    public void setProsessId(Long[] prosessId) {
        log.trace("setProsessId({})", (Object[]) prosessId);
        Long[] old = this.prosessId;
        this.prosessId = prosessId;
        getProps().firePropertyChange(FIELD_NAME_PROSESSID, old, prosessId);
    }

    public void setUnderprosessId(Long[] underprosessId) {
        Long[] old = this.underprosessId;
        this.underprosessId = underprosessId;
        getProps().firePropertyChange(FIELD_NAME_UNDERPROSESSID, old, underprosessId);
    }

    public void setTidligsteFraDato(Date tidligsteFraDato) {
        log.trace("setTidligsteFraDato({})", tidligsteFraDato);
        Date old = this.tidligsteFraDato;
        this.tidligsteFraDato = tidligsteFraDato;
        getProps().firePropertyChange(FIELD_NAME_TIDLIGSTFRADATO, old, tidligsteFraDato);
    }

    public void setTilDato(Date tilDato) {
        log.trace("setTilDato({})", tilDato);
        Date old = this.tilDato;
        this.tilDato = tilDato;
        getProps().firePropertyChange(FIELD_NAME_TILDATO, old, tilDato);
    }

    public void setVeiInfo(VeiInfo veiInfo) {
        log.trace("setVeiInfo({})", veiInfo);
        VeiInfo old = this.veiInfo;
        this.veiInfo = veiInfo;
        getProps().firePropertyChange(FIELD_NAME_VEIINFO, old, veiInfo);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(FIELD_NAME_FRADATO, fraDato).append(FIELD_NAME_KONTRAKTID, kontraktId)
                .append(FIELD_NAME_METERFRA, meterFra).append(FIELD_NAME_METERTIL, meterTil).append(
                        FIELD_NAME_TIDLIGSTFRADATO, tidligsteFraDato).append(FIELD_NAME_TILDATO, tilDato).append(
                        FIELD_NAME_VEIINFO, veiInfo).append(FIELD_NAME_PROSESSID, prosessId).append(FIELD_NAME_UNDERPROSESSID, underprosessId).append(
                        FIELD_NAME_INGENPROSESSER, ingenProsesser).append(FIELD_NAME_DFUER, dfuer).append(
                        FIELD_NAME_INGENDFUER, ingenDfuer).append(FIELD_NAME_KUNSLETTEDE, kunSlettede).toString();
    }
}
