package no.mesta.mipss.mipssfield;

import no.mesta.mipss.mipssfield.vo.*;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;

import javax.ejb.Remote;
import java.util.Date;
import java.util.List;

/**
 * Interface for Server side bønne som henter ut data for VMP
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Remote
public interface MipssField {
    String BEAN_NAME = "MipssFieldBean";

    Long getDriftkontraktIdForPunktreg(Long refnummer);

    Long getDriftkontraktIdForHendelse(Long refnummer);

    Long getDriftkontraktIdForInspeksjon(Long refnummer);

    /**
     * Legger til en ny forsikringsskade til angitt hendelse
     *
     * @param hendelse
     * @param f
     * @param brukerSign
     * @return
     */
    Forsikringsskade addForsikringsskade(Hendelse hendelse, Forsikringsskade f, String brukerSign);

    /**
     * Knytter et funn til en hendelse
     *
     * @param hendelse
     * @param punktreg
     * @param bruker
     * @return
     */
    HendelsePunktregResult addPunktreg(Hendelse hendelse, Avvik punktreg, Bruker bruker);

    /**
     * Avslutter et funn, men returerer ikke oppdatert view
     *
     * @param punkt
     * @param tekst
     * @param signatur
     * @return
     */
    Avvik avsluttPunktreg(String punkt, String tekst, String signatur, Date lukketDato);

    /**
     * Beregner en stedfesting for veiref
     * <p/>
     * Hverken stedfest og veiref er lagret i databasen etter denne operasjonen
     *
     * @param veiref
     * @return
     */
    Punktstedfest calculatePunktstedfest(Punktveiref veiref);

    /**
     * Fjerner hendelsen fra et funn
     *
     * @param punktreg
     * @param bruker
     * @return
     */
    HendelsePunktregResult clearHendelse(Avvik punktreg, Bruker bruker);

    /**
     * Lengden på inspisert vei for en inspeksjon og en gitt kategori
     *
     * @param guid
     * @param veikategori
     * @return
     */
    Double getInspisertVeiLengde(String guid, String veikategori);

    /**
     * Gjenoppretter funn
     *
     * @param guider
     * @return
     */
    List<PunktregSok> gjennopprettFunn(List<String> guider);

    /**
     * Gjenoppretter hendelser
     *
     * @param guider
     * @return
     */
    List<HendelseSokView> gjenopprettHendelser(List<String> guider);

    /**
     * Gjenoppretter inspeksjoner
     *
     * @param guider
     * @return
     */
    List<InspeksjonSokView> gjenopprettInspeksjoner(List<String> guider);

    /**
     * Henter listen over alle hendelsesårsak typene
     *
     * @return
     */
    List<Hendelseaarsak> hentAlleHendelsesAarsaker();

    /**
     * Henter alle definisjonene på prosesser ut
     *
     * @return
     */
    List<Prosess> hentAlleProsesser();

    /**
     * Henter en prosess med angitt id
     *
     * @param id
     * @return
     */
    Prosess hentProsessMedId(Long id);

    /**
     * Henter PunktregSok basert på refnummer
     *
     * @param id
     * @return
     */
    PunktregSok hentPunktregSok(Long id);

    /**
     * Henter InspeksjonSokView basert på refnummer
     *
     * @param id
     * @return
     */
    InspeksjonSokView hentInspeksjonSok(Long id);

    /**
     * Henter HendelseSokView basert på refnummer
     *
     * @param id
     * @return
     */
    HendelseSokView hentHendelseSok(Long id);

    /**
     * Henter listen over tilstand typene
     *
     * @return
     */
    List<Avviktilstand> hentAllePunktregtilstandtype();


    /**
     * Henter alle status typer
     *
     * @return
     */
    List<AvvikstatusType> hentAlleStatusTyper();

    /**
     * Henter alle temperatur typer
     *
     * @return
     */
    List<Temperatur> hentAlleTemperatur();

    /**
     * Henter alle trafikktiltakstypene
     *
     * @return
     */
    List<Trafikktiltak> hentAlleTrafikktiltaktyper();

    /**
     * Henter alle værtyper
     *
     * @return
     */
    List<Vaer> hentAlleVaer();

    /**
     * Henter alle vind typer
     *
     * @return
     */
    List<Vind> hentAlleVind();

    /**
     * Henter en forsikringsskade med angitt guid
     *
     * @param guid
     * @return
     */
    Forsikringsskade hentForsikringsskade(String guid);

    List<String> hentForsikringsskadeGuid(List<String> hendelseGuid);

    /**
     * Henter en liste med funn basert på en liste av nøkler
     *
     * @param guids
     * @return
     */
    List<Avvik> hentFunn(List<String> guids);

    /**
     * Henter en hendelse
     *
     * @param guid
     * @return
     */
    Hendelse hentHendelse(String guid);

    /**
     * Henter en hendelse og eager-fetcher skred-data
     *
     * @param guid
     * @return
     */
    Hendelse hentHendelseSkred(String guid);

    /**
     * Henter alle bildene for en hendelse
     *
     * @param guid
     * @return
     */
    List<Bilde> hentHendelseBilder(String guid);

    /**
     * Henter en liste med hendelser basert på en liste av nøkler. Henter noen
     * assosiasjoner EAGER.
     *
     * @param guids
     * @return
     */
    List<Hendelse> hentHendelser(List<String> guids);

    /**
     * Henter hendelser for kontrakten, der byggeleder er EAGER fetchet.
     *
     * @param guids
     * @return
     */
    List<Hendelse> hentHendelserForRapport(List<String> guids);

    /**
     * Henter et view objekt for en hendelse
     *
     * @param guid
     * @return
     */
    HendelseSokView hentHendelseSok(String guid);

    /**
     * Henter ut HP rapport
     *
     * @param guids
     * @return
     */
    List<HPReport> hentHPReport(List<String> guids);

    /**
     * Henter en inspeksjon på bakgrunn av en guid
     *
     * @param guid
     * @return
     */
    Inspeksjon hentInspeksjon(String guid);

    /**
     * Henter inspeksjon med begrensede data ( de som skal med i inspeksjonsrapporten)
     *
     * @param guid
     * @return
     */
    Inspeksjon hentInspeksjonForRapport(String guid);

    /**
     * Henter et view objekt for inspeksjon
     *
     * @param guid
     * @return
     */
    InspeksjonSokView hentInspeksjonSok(String guid);

    /**
     * Henter ut en liste av objekter
     *
     * @param <E>
     * @param clazz
     * @param guids
     * @return
     */
    <E extends MipssEntityBean<E>> List<E> hentObjekter(Class<E> clazz, List<String> guids);

    /**
     * Leser et punktreg ut fra databasen
     *
     * @param guid
     * @return
     */
    Avvik hentPunktreg(String guid);

    /**
     * Henter liste over bilde med status informasjon
     *
     * @param guid
     * @return
     */
    List<AvvikBilde> hentPunktregBildeInfo(String guid);

    /**
     * Henter bildene for et punktreg
     *
     * @param guid til punktreg
     * @return
     */
    List<Bilde> hentPunktregBilder(String guid);

    /**
     * Leser en rad fra view
     *
     * @param guid
     * @return
     */
    PunktregSok hentPunktregSok(String guid);

    /**
     * Henter en status type
     *
     * @param status
     * @return
     */
    AvvikstatusType hentStatusType(String status);

    /**
     * Lukker et punkt i databasen. Designdokumentet 6.4.2
     *
     * @param punkt
     * @param tekst
     * @param signatur
     * @return
     */
    PunktregResult lukkFunn(String punkt, String tekst, String signatur, Date lukketDato);

    /**
     * Oppdaterer en hendelse i databasen
     *
     * @param hendelse
     * @return
     */
    HendelseResult oppdaterHendelse(Hendelse hendelse, String bruker);

    /**
     * Oppdaterer en Hendelse med tilhørende Skred og relasjoner
     *
     * @param hendelse
     * @param bruker
     */
    Hendelse oppdaterHendelseSkred(Hendelse hendelse, String bruker);


    /**
     * Oppdaterer en inspeksjon i databasen
     *
     * @param inspeksjon
     * @return
     */
    InspeksjonResult oppdaterInspeksjon(Inspeksjon inspeksjon, String bruker);

    /**
     * Oppdaterer et funn
     *
     * @param punkt
     * @return
     */
    PunktregResult oppdaterPunktreg(Avvik punkt);

    /**
     * Oppdaterer en statusoppføring for et funn
     *
     * @param status
     * @return
     */
    AvvikStatus oppdaterPunktStatus(AvvikStatus status);

    /**
     * Oppdagerer en forsikringsskade
     *
     * @param f
     * @param bruker
     * @return
     */
    Forsikringsskade oppdaterForsikringsskade(Forsikringsskade f, String bruker);

    /**
     * Frakobler et funn fra en hendelse
     *
     * @param hendelse
     * @param punktreg
     * @param bruker
     * @return
     */
    HendelsePunktregResult removePunktreg(Hendelse hendelse, Avvik punktreg, Bruker bruker);

    /**
     * Setter hendelsen på et funn
     *
     * @param punktreg
     * @param hendelse
     * @param bruker
     * @return
     */
    HendelsePunktregResult setHendelse(Avvik punktreg, Hendelse hendelse, Bruker bruker);

    /**
     * Sletter funn
     *
     * @param guider
     * @return
     */
    List<PunktregSok> slettFunn(List<String> guider, String bruker);

    /**
     * Sletter hendelser
     *
     * @param guider
     * @param slettFunn
     * @return
     */
    List<HendelseSokView> slettHendelser(List<String> guider, boolean slettFunn, String bruker);

    /**
     * Sletter inspeksjoner
     *
     * @param guider
     * @param slettHendelserOgFunn
     * @return
     */
    List<InspeksjonSokView> slettInspeksjoner(List<String> guider, boolean slettHendelserOgFunn, String bruker);

    /**
     * Sletter siste status til et funn
     *
     * @param guid
     * @return
     */
    PunktregResult slettSisteStatus(String guid);

    /**
     * Søker etter funn
     *
     * @param filter
     * @return
     */
    List<PunktregSok> sokFunn(PunktregQueryFilter filter);

    /**
     * Søker etter hendelser
     *
     * @param filter
     * @return
     */
    List<HendelseSokView> sokHendelser(HendelseQueryFilter filter);

    /**
     * Henter en filtrert liste av inspeksjoner
     *
     * @param filter
     * @return
     */
    List<InspeksjonSokView> sokInspeksjoner(InspeksjonQueryFilter filter);

    /**
     * Sjekker om det er en gyldig veiref
     *
     * @param veiref
     * @return
     */
    boolean validatePunktveiref(Punktveiref veiref);

    List<Skred> hentAlleSkred();

    /**
     * Henter skred guid tilknyttet en Hendelse
     *
     * @param list
     * @return
     */
    List<String> hentSkredGuid(List<String> list);
}
