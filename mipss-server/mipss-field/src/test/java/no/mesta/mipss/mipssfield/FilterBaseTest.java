package no.mesta.mipss.mipssfield;

import junit.framework.TestCase;
import no.mesta.mipss.persistence.Clock;

import java.util.Calendar;
import java.util.Date;

/**
 * Tester dato fra dato reglene
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class FilterBaseTest extends TestCase {
    private Date oneYearAgo;
    private Date twoYearsAgo;
    private Date oneMontAgo;

    public void setUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Clock.now());
        calendar.add(Calendar.DAY_OF_YEAR, -365);

        oneYearAgo = calendar.getTime();

        calendar.setTime(Clock.now());
        calendar.add(Calendar.MONTH, -1);
        oneMontAgo = calendar.getTime();

        calendar.setTime(Clock.now());
        calendar.add(Calendar.YEAR, -2);
        twoYearsAgo = calendar.getTime();
    }

    public void testGetFromDateOneMonthBack() {
        FilterBase filter = new FilterBase(oneYearAgo);
        filter.setFraDato(oneMontAgo);

        assertEquals(oneMontAgo, filter.getFraDato());
    }

    public void testGetFromDateTwoYearsBack() {
        FilterBase filter = new FilterBase(oneYearAgo);
        filter.setFraDato(twoYearsAgo);

        assertEquals(oneYearAgo, filter.getFraDato());
    }

    public void testGetFromDateDefaultDate() {
        FilterBase filter = new FilterBase(oneYearAgo);

        assertEquals(oneYearAgo, filter.getFraDato());
    }
}
