package no.mesta.mipss.entityservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;

@Stateless(name = MipssGrunndataService.BEAN_NAME, mappedName="ejb/"+MipssGrunndataService.BEAN_NAME)
public class MipssGrunndataServiceBean implements MipssGrunndataService{

	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<MipssEntityBean<?>> getEntityList(String clazz ){
		StringBuilder sb = new StringBuilder();
		sb.append("Select o from ");
		sb.append(clazz);
		sb.append(" o");
		Query qry = em.createQuery(sb.toString());
		qry.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return qry.getResultList();
	}

	public void persistEntities(List<MipssEntityBean<?>> entityList) {
		for (MipssEntityBean<?> entity:entityList){
			entity = em.merge(entity);
			em.persist(entity);
		}
		em.flush();
		
	}

	public void removeEntities(List<MipssEntityBean<?>> entityList) {
		for (MipssEntityBean<?> entity:entityList){
			entity = em.merge(entity);
			em.remove(entity);
		}
		em.flush();
		
	}
	
	 
	
}
