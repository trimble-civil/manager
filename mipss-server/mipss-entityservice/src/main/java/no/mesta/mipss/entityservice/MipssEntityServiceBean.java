package no.mesta.mipss.entityservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.persistence.VersionColumn;
import no.mesta.mipss.persistence.applikasjon.Appstatus;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;
import no.mesta.mipss.persistence.areal.Areal;
import no.mesta.mipss.persistence.areal.Arealtype;
import no.mesta.mipss.persistence.datafangsutstyr.Dfukategori;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.datafangsutstyr.Ioteknologi;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuleverandor;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Doktype;
import no.mesta.mipss.persistence.dokarkiv.Ikon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sesong;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrprodusent;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Driftregion;
import no.mesta.mipss.persistence.kontrakt.Kjoretoykontakttype;
import no.mesta.mipss.persistence.kontrakt.Kontrakttype;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Skadekontakttype;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.persistence.organisasjon.Prosjekt;
import no.mesta.mipss.persistence.stroing.Stroproduktgruppe;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;
import no.mesta.mipss.persistence.veinett.Veinetttype;

/**
 * Tilbyr enkle CRUD-operasjoner på entiteter. Til bruk til vedlikehold av grunndata og konfig param.
 * 
 * @author Christian Wiik ()
 */
@Stateless(name = MipssEntityService.BEAN_NAME, mappedName="ejb/"+MipssEntityService.BEAN_NAME)
@SuppressWarnings("unchecked")
public class MipssEntityServiceBean implements MipssEntityService {
	
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;

	@Override
	public List<Appstatus> getAppstatusList() {
		return em.createNamedQuery(Appstatus.QUERY_FIND_ALL).getResultList();
	}
	
	@Override
	public List<Arbeidsfunksjon> getArbeidsfunksjonList() {
		return em.createNamedQuery(Arbeidsfunksjon.FIND_ALL).getResultList();
	}
	
	@Override
	public List<Areal> getArealList() {
		return em.createNamedQuery(Areal.FIND_ALL).getResultList();
	}

	@Override
	public List<Arealtype> getArealtypeList() {
		return em.createNamedQuery(Arealtype.FIND_ALL).getResultList();
	}

	@Override
	public List<Dfukategori> getDfukategoriList() {
		return em.createNamedQuery(Dfukategori.FIND_ALL).getResultList();
	}

	@Override
	public List<Dfuleverandor> getDfuleverandorList() {
		return em.createNamedQuery(Dfuleverandor.FIND_ALL).getResultList();
	}

	@Override
	public List<Dfumodell> getDfumodellList() {
		return em.createNamedQuery(Dfumodell.FIND_ALL).getResultList();
	}

	@Override
	public List<Dfustatus> getDfustatusList() {		
		return em.createNamedQuery(Dfustatus.FIND_ALL).getResultList();
	}

	@Override
	public List<Dfuvarekatalog> getDfuvarekatalogList() {
		return em.createNamedQuery(Dfuvarekatalog.FIND_ALL).getResultList();		
	}

	@Override
	public List<Dokformat> getDokformatList() {
		return em.createNamedQuery(Dokformat.FIND_ALL).getResultList();
	}

	@Override
	public List<Doktype> getDoktypeList() {
		return em.createNamedQuery(Doktype.FIND_ALL).getResultList();
	}

	@Override
	public List<Driftdistrikt> getDriftdistriktList() {
		return em.createNamedQuery(Driftdistrikt.FIND_ALL).getResultList();
	}

	@Override
	public List<Driftregion> getDriftregionList() {
		return em.createNamedQuery(Driftregion.FIND_ALL).getResultList();
	}

	@Override
	public List<Hendelseaarsak> getHendelsesaarsakList() {		
		return em.createNamedQuery(Hendelseaarsak.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Ikon> getIkonList() {
		return em.createNamedQuery(Ikon.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Ioteknologi> getIoteknologiList() {		
		return em.createNamedQuery(Ioteknologi.FIND_ALL).getResultList();
	}

	@Override
	public List<Kjoretoymodell> getKjoretoymodellList() {		
		return em.createNamedQuery(Kjoretoymodell.FIND_ALL).getResultList();		
	}

	@Override
	public List<Kjoretoyordrestatus> getKjoretoyordrestatusList() {
		return em.createNamedQuery(Kjoretoyordrestatus.FIND_ALL).getResultList();
	}

	@Override
	public List<Kjoretoytype> getKjoretoytypeList() {		
		return em.createNamedQuery(Kjoretoytype.FIND_ALL).getResultList();
	}

	@Override
	public List<Konfigparam> getKonfigparamList() {
		return em.createNamedQuery(Konfigparam.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Kjoretoykontakttype> getKontakttypeList() {
		return em.createNamedQuery(Kjoretoykontakttype.FIND_ALL).getResultList();
	}

	@Override
	public List<Kontrakttype> getKontrakttypeList() {
		return em.createNamedQuery(Kontrakttype.FIND_ALL).getResultList();
	}

	@Override
	public List<Leverandor> getLeverandorList() {
		return em.createNamedQuery(Leverandor.FIND_ALL).getResultList();
	}

	@Override
	public List<Menygruppe> getMenygruppeList() {
		return em.createNamedQuery(Menygruppe.FIND_ALL).getResultList();
	}

	@Override
	public List<Prodtype> getProdtypeList() {		
		return em.createNamedQuery(Prodtype.FIND_ALL).getResultList();
	}

	@Override
	public List<Prosess> getProsessList() {		
		return em.createNamedQuery(Prosess.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Prosjekt> getProsjektList() {
		return em.createNamedQuery(Prosjekt.FIND_ALL).getResultList();
	}

	@Override
	public List<AvvikStatus> getPunktregStatusList() {		
		return em.createNamedQuery(AvvikStatus.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Avviktilstand> getPunktregtilstandtypeList() {		
		return em.createNamedQuery(Avviktilstand.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Rode> getRodeList() {
		return em.createNamedQuery(Rode.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Rodetype> getRodetypeList() {
		return em.createNamedQuery(Rodetype.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Selskap> getSelskapList() {
		return em.createNamedQuery(Selskap.FIND_ALL).getResultList();
	}

	@Override
	public List<Sensortype> getSensortypeList() {		
		return em.createNamedQuery(Sensortype.FIND_ALL).getResultList();
	}

	@Override
	public List<Sesong> getSesongList() {		
		return em.createNamedQuery(Sesong.FIND_ALL).getResultList();
	}

	@Override
	public List<Skadekontakttype> getSkadekontakttypeList() {
		return em.createNamedQuery(Skadekontakttype.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Stroproduktgruppe> getStroproduktgruppeList() {
		return em.createNamedQuery(Stroproduktgruppe.FIND_ALL).getResultList();		
	}

	@Override
	public List<Tilgangsnivaa> getTilgangsnivaaList() {
		return em.createNamedQuery(Tilgangsnivaa.FIND_ALL).getResultList();
	}

	@Override
	public List<Trafikktiltak> getTrafikktiltaktypeList() {
		return em.createNamedQuery(Trafikktiltak.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public List<Utstyrgruppe> getUtstyrgruppeList() {
		return em.createNamedQuery(Utstyrgruppe.FIND_ALL).getResultList();
	}

	@Override
	public List<Utstyrmodell> getUtstyrmodellList() {		
		return em.createNamedQuery(Utstyrmodell.FIND_ALL).getResultList();
	}
	
	@Override
	public List<Utstyrprodusent> getUtstyrprodusentList() {		
		return em.createNamedQuery(Utstyrprodusent.FIND_ALL).getResultList();
	}
	
	@Override
	public List<Utstyrsubgruppe> getUtstyrsubgruppeList() {
		return em.createNamedQuery(Utstyrsubgruppe.FIND_ALL).getResultList();
	}
	
	@Override
	public List<Veinetttype> getVeinetttypeList() {
		return em.createNamedQuery(Veinetttype.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public void persistAppstatusList(List<Appstatus> appstatusList) {
		for (Appstatus appstatus : appstatusList) {
			appstatus = em.merge(appstatus);
			em.persist(appstatus);
		}
	}

	@Override
	public void persistArbeidsfunksjonList(List<Arbeidsfunksjon> arbeidsfunksjonList) {
		for (Arbeidsfunksjon arbeidsfunksjon : arbeidsfunksjonList) {
			arbeidsfunksjon = em.merge(arbeidsfunksjon);
			em.persist(arbeidsfunksjon);
		}
	}

	@Override
	public void persistArealList(List<Areal> arealList) {
		for (Areal areal : arealList) {
			areal = em.merge(areal);
			em.persist(areal);
		}
	}

	@Override
	public void persistArealtypeList(List<Arealtype> arealtypeList) {
		for (Arealtype arealtype : arealtypeList) {
			arealtype = em.merge(arealtype);
			em.persist(arealtype);
		}
	}

	@Override
	public void persistDfukategoriList(List<Dfukategori> dfukategoriList) {
		for (Dfukategori dfukategori : dfukategoriList) {
			dfukategori = em.merge(dfukategori);
			em.persist(dfukategori);
		}
	}

	public void persistDfuleverandorList(List<Dfuleverandor> dfuleverandorList) {
		for (Dfuleverandor dfuleverandor : dfuleverandorList) {
			dfuleverandor = em.merge(dfuleverandor);
			em.persist(dfuleverandor);
		}
	}

	@Override
	public void persistDfumodellList(List<Dfumodell> dfumodellList) {
		for (Dfumodell dfumodell : dfumodellList) {
			dfumodell = em.merge(dfumodell);
			em.persist(dfumodell);
		}
	}

	@Override
	public void persistDfustatusList(List<Dfustatus> dfustatusList) {
		for (Dfustatus dfustatus : dfustatusList) {
			dfustatus = em.merge(dfustatus);
			em.persist(dfustatus);
		}
	}

	@Override
	public void persistDfuvarekatalogList(List<Dfuvarekatalog> dfuvarekatalogList) {
		for (Dfuvarekatalog dfuvarekatalog : dfuvarekatalogList) {
			dfuvarekatalog = em.merge(dfuvarekatalog);
			em.persist(dfuvarekatalog);
		}
	}

	@Override
	public void persistDokformatList(List<Dokformat> dokformatList) {
		for (Dokformat dokformat : dokformatList) {
			dokformat = em.merge(dokformat);
			em.persist(dokformat);
		}
	}

	@Override
	public void persistDoktypeList(List<Doktype> doktypeList) {
		for (Doktype doktype : doktypeList) {
			doktype = em.merge(doktype);
			em.persist(doktype);
		}
	}

	public void persistDriftdistriktList(List<Driftdistrikt> driftdistriktList) {
		for (Driftdistrikt driftdistrikt : driftdistriktList) {
			driftdistrikt = em.merge(driftdistrikt);
			em.persist(driftdistrikt);
		}
	}

	public void persistDriftregionList(List<Driftregion> driftregionList) {
		for (Driftregion driftregion : driftregionList) {
			driftregion = em.merge(driftregion);
			em.persist(driftregion);
		}
	}

	public void persistHendelsesaarsakList(List<Hendelseaarsak> hendelsesaarsakList) {
		for (Hendelseaarsak hendelsesaarsak : hendelsesaarsakList) {
			hendelsesaarsak = em.merge(hendelsesaarsak);
			em.persist(hendelsesaarsak);
		}
	}

	public void persistIkonList(List<Ikon> ikonList) {
		for (Ikon ikon : ikonList) {
			ikon = em.merge(ikon);
			em.persist(ikon);
		}
	}

	@Override
	public void persistIoteknologiList(List<Ioteknologi> ioteknologiList) {
		for (Ioteknologi ioteknologi : ioteknologiList) {
			ioteknologi = em.merge(ioteknologi);
			em.persist(ioteknologi);
		}
	}

	@Override
	public void persistKjoretoymodellList(List<Kjoretoymodell> kjoretoymodellList) {
		for (Kjoretoymodell kjoretoymodell : kjoretoymodellList) {
			kjoretoymodell = em.merge(kjoretoymodell);
			em.persist(kjoretoymodell);
		}
	}

	@Override
	public void persistKjoretoyordrestatusList(List<Kjoretoyordrestatus> kjoretoyordrestatusList) {
		for (Kjoretoyordrestatus kjoretoyordrestatus : kjoretoyordrestatusList) {
			kjoretoyordrestatus = em.merge(kjoretoyordrestatus);
			em.persist(kjoretoyordrestatus);
		}
	}

	@Override
	public void persistKjoretoytypeList(List<Kjoretoytype> kjoretoytypeList) {
		for (Kjoretoytype kjoretoytype : kjoretoytypeList) {
			kjoretoytype = em.merge(kjoretoytype);
			em.persist(kjoretoytype);
		}
	}

	@Override
	public void persistKonfigparamList(List<Konfigparam> konfigparamList) {
		for (Konfigparam konfigparam : konfigparamList) {
			if (konfigparam.getVersion()==null){
				VersionColumn c = new VersionColumn();
				c.setVersion(1);
				konfigparam.setVersion(c);
				em.persist(konfigparam);
			}else{
				konfigparam = em.merge(konfigparam);
				em.persist(konfigparam);
			}
		}
	}

	@Override
	public void persistKontakttypeList(List<Kjoretoykontakttype> kontakttypeList) {
		for (Kjoretoykontakttype kontakttype : kontakttypeList) {
			kontakttype = em.merge(kontakttype);
			em.persist(kontakttype);
		}
	}

	@Override
	public void persistKontrakttypeList(List<Kontrakttype> kontrakttypeList) {
		for (Kontrakttype kontrakttype : kontrakttypeList) {
			kontrakttype = em.merge(kontrakttype);
			em.persist(kontrakttype);
		}
	}

	@Override
	public void persistLeverandorList(List<Leverandor> leverandorList) {
		for (Leverandor leverandor : leverandorList) {
			leverandor = em.merge(leverandor);
			em.persist(leverandor);
		}
	}

	@Override
	public void persistMenygruppeList(List<Menygruppe> menygruppeList) {
		for (Menygruppe menygruppe : menygruppeList) {
			menygruppe = em.merge(menygruppe);
			em.persist(menygruppe);
		}
	}

	@Override
	public void persistProdtypeList(List<Prodtype> prodtypeList) {
		for (Prodtype prodtype : prodtypeList) {
			prodtype = em.merge(prodtype);
			em.persist(prodtype);
		}
	}

	@Override
	public void persistProsessList(List<Prosess> prosessList) {
		for (Prosess prosess : prosessList) {
			prosess = em.merge(prosess);
			em.persist(prosess);
		}
	}

	@Override
	public void persistProsjektList(List<Prosjekt> prosjektList) {
		for (Prosjekt prosjekt : prosjektList) {
			prosjekt = em.merge(prosjekt);
			em.persist(prosjekt);
		}
	}

	@Override
	public void persistPunktregStatusList(List<AvvikStatus> punktregStatusList) {
		for (AvvikStatus punktregStatus : punktregStatusList) {
			punktregStatus = em.merge(punktregStatus);
			em.persist(punktregStatus);
		}
	}

	@Override
	public void persistPunktregtilstandtypeList(List<Avviktilstand> punktregtilstandtypeList) {
		for (Avviktilstand punktregtilstandtype : punktregtilstandtypeList) {
			punktregtilstandtype = em.merge(punktregtilstandtype);
			em.persist(punktregtilstandtype);
		}
	}

	@Override
	public void persistRodeList(List<Rode> rodeList) {
		for (Rode rode : rodeList) {
			rode = em.merge(rode);
			em.persist(rode);
		}
	}

	@Override
	public void persistRodetypeList(List<Rodetype> rodetypeList) {
		for (Rodetype rodetype : rodetypeList) {
			rodetype = em.merge(rodetype);
			em.persist(rodetype);
		}
	}

	@Override
	public void persistSelskapList(List<Selskap> selskapList) {
		for (Selskap selskap : selskapList) {
			selskap = em.merge(selskap);
			em.persist(selskap);
		}
	}

	@Override
	public void persistSensortypeList(List<Sensortype> sensortypeList) {
		for (Sensortype sensortype : sensortypeList) {
			sensortype = em.merge(sensortype);
			em.persist(sensortype);
		}
	}

	@Override
	public void persistSesongList(List<Sesong> sesongList) {
		for (Sesong sesong : sesongList) {
			sesong = em.merge(sesong);
			em.persist(sesong);
		}
	}

	@Override
	public void persistSkadekontakttypeList(List<Skadekontakttype> skadekontakttypeList) {
		for (Skadekontakttype skadekontakttype : skadekontakttypeList) {
			skadekontakttype = em.merge(skadekontakttype);
			em.persist(skadekontakttype);
		}
	}

	@Override
	public void persistStroproduktgruppeList(List<Stroproduktgruppe> stroproduktgruppeList) {
		for (Stroproduktgruppe stroproduktgruppe : stroproduktgruppeList) {
			stroproduktgruppe = em.merge(stroproduktgruppe);
			em.persist(stroproduktgruppe);
		}
	}

	@Override
	public void persistTilgangsnivaaList(List<Tilgangsnivaa> tilgangsnivaaList) {
		for (Tilgangsnivaa tilgangsnivaa : tilgangsnivaaList) {
			tilgangsnivaa = em.merge(tilgangsnivaa);
			em.persist(tilgangsnivaa);
		}
	}

	@Override
	public void persistTrafikktiltaktypeList(List<Trafikktiltak> trafikktiltaktypeList) {
		for (Trafikktiltak trafikktiltaktype : trafikktiltaktypeList) {
			trafikktiltaktype = em.merge(trafikktiltaktype);
			em.persist(trafikktiltaktype);
		}
	}

	@Override
	public void persistUtstyrgruppeList(List<Utstyrgruppe> utstyrgruppeList) {
		for (Utstyrgruppe utstyrgruppe : utstyrgruppeList) {
			utstyrgruppe = em.merge(utstyrgruppe);
			em.persist(utstyrgruppe);
		}
	}
	
	@Override
	public void persistUtstyrmodellList(List<Utstyrmodell> utstyrmodellList) {
		for (Utstyrmodell utstyrmodell : utstyrmodellList) {
			try {
				utstyrmodell = em.merge(utstyrmodell);
				em.persist(utstyrmodell);
			} catch (OptimisticLockException e) {
				//throw new EntityServiceException(pa.get ClassUtils.getShortCanonicalName(Utstyrmodell.class), e);
			}
			
		}
	}
	
	@Override
	public void persistUtstyrprodusentList(List<Utstyrprodusent> utstyrprodusentList) {
		for (Utstyrprodusent utstyrprodusent : utstyrprodusentList) {
			utstyrprodusent = em.merge(utstyrprodusent);
			em.persist(utstyrprodusent);
		}
	}
	
	@Override
	public void persistUtstyrsubgruppeList(List<Utstyrsubgruppe> utstyrsubgruppeList) {
		for (Utstyrsubgruppe utstyrsubgruppe : utstyrsubgruppeList) {
			utstyrsubgruppe = em.merge(utstyrsubgruppe);
			em.persist(utstyrsubgruppe);
		}
	}

	@Override
	public void persistVeinetttypeList(List<Veinetttype> veinetttypeList) {
		for (Veinetttype veinetttype : veinetttypeList) {
			veinetttype = em.merge(veinetttype);
			em.persist(veinetttype);
		}
	}

	@Override
	public void removeAppstatusList(List<Appstatus> appstatusList) {
		for (Appstatus appstatus : appstatusList) {
			appstatus = em.merge(appstatus);
			em.remove(appstatus);
		}	
	}
	@Override
	public void removeArealList(List<Areal> arealList) {
		for (Areal areal : arealList) {
			areal = em.merge(areal);
			em.remove(areal);
		}
	}

	@Override
	public void removeArealtypeList(List<Arealtype> arealtypeList) {
		for (Arealtype arealtype : arealtypeList) {
			arealtype = em.merge(arealtype);
			em.remove(arealtype);
		}
	}


	@Override
	public void removeDfukategoriList(List<Dfukategori> dfukategoriList) {
		for (Dfukategori dfukategori : dfukategoriList) {
			dfukategori = em.merge(dfukategori);
			em.remove(dfukategori);
		}
	}

	@Override
	public void removeDfuleverandorList(List<Dfuleverandor> dfuleverandorList) {
		for (Dfuleverandor dfuleverandor : dfuleverandorList) {
			dfuleverandor = em.merge(dfuleverandor);
			em.remove(dfuleverandor);
		}
	}

	@Override
	public void removeDfumodellList(List<Dfumodell> dfumodellList) {
		for (Dfumodell dfumodell : dfumodellList) {
			dfumodell = em.merge(dfumodell);
			em.remove(dfumodell);
		}
	}
	
	@Override
	public void removeDfustatusList(List<Dfustatus> dfustatusList) {
		for (Dfustatus dfustatus : dfustatusList) {
			dfustatus = em.merge(dfustatus);
			em.remove(dfustatus);
		}
	}

	@Override
	public void removeDfuvarekatalogList(List<Dfuvarekatalog> dfuvarekatalogList) {
		for (Dfuvarekatalog dfuvarekatalog : dfuvarekatalogList) {
			dfuvarekatalog = em.merge(dfuvarekatalog);
			em.remove(dfuvarekatalog);
		}
	}

	@Override
	public void removeDokformatList(List<Dokformat> dokformatList) {
		for (Dokformat dokformat : dokformatList) {
			dokformat = em.merge(dokformat);
			em.remove(dokformat);
		}
	}

	@Override
	public void removeDoktypeList(List<Doktype> doktypeList) {
		for (Doktype doktype : doktypeList) {
			doktype = em.merge(doktype);
			em.remove(doktype);
		}		
	}

	@Override
	public void removeDriftdistriktList(List<Driftdistrikt> driftdistriktList) {
		for (Driftdistrikt driftdistrikt : driftdistriktList) {
			driftdistrikt = em.merge(driftdistrikt);
			em.remove(driftdistrikt);
		}
	}

	@Override
	public void removeDriftregionList(List<Driftregion> driftregionList) {
		for (Driftregion driftregion : driftregionList) {
			driftregion = em.merge(driftregion);
			em.remove(driftregion);
		}
	}
	
	@Override
	public void removeHendelsesaarsakList(List<Hendelseaarsak> hendelsesaarsakList) {
		for (Hendelseaarsak hendelsesaarsak : hendelsesaarsakList) {
			hendelsesaarsak = em.merge(hendelsesaarsak);
			em.remove(hendelsesaarsak);
		}
	}

	@Override
	public void removeIkonList(List<Ikon> ikonList) {
		for (Ikon ikon : ikonList) {
			ikon = em.merge(ikon);
			em.remove(ikon);
		}
	}

	@Override
	public void removeIoteknologiList(List<Ioteknologi> ioteknologiList) {
		for (Ioteknologi ioteknologi : ioteknologiList) {
			ioteknologi = em.merge(ioteknologi);
			em.remove(ioteknologi);
		}
	}

	@Override
	public void removeKjoretoymodellList(List<Kjoretoymodell> kjoretoymodellList) {
		for (Kjoretoymodell kjoretoymodell : kjoretoymodellList) {
			kjoretoymodell = em.merge(kjoretoymodell);
			em.remove(kjoretoymodell);
		}
	}

	@Override
	public void removeKjoretoyordrestatusList(List<Kjoretoyordrestatus> kjoretoyordrestatusList) {
		for (Kjoretoyordrestatus kjoretoyordrestatus : kjoretoyordrestatusList) {
			kjoretoyordrestatus = em.merge(kjoretoyordrestatus);
			em.remove(kjoretoyordrestatus);
		}
	}

	@Override
	public void removeKjoretoytypeList(List<Kjoretoytype> kjoretoytypeList) {
		for (Kjoretoytype kjoretoytype : kjoretoytypeList) {
			kjoretoytype = em.merge(kjoretoytype);
			em.remove(kjoretoytype);
		}
	}

	@Override
	public void removeKonfigparamList(List<Konfigparam> konfigparamList) {
		for (Konfigparam konfigparam : konfigparamList) {
			konfigparam = em.merge(konfigparam);
			em.remove(konfigparam);
		}
	}

	@Override
	public void removeKontakttypeList(List<Kjoretoykontakttype> kontakttypeList) {
		for (Kjoretoykontakttype kontakttype : kontakttypeList) {
			kontakttype = em.merge(kontakttype);
			em.remove(kontakttype);
		}
	}
	
	@Override
	public void removeKontrakttypeList(List<Kontrakttype> kontrakttypeList) {
		for (Kontrakttype kontrakttype : kontrakttypeList) {
			kontrakttype = em.merge(kontrakttype);
			em.remove(kontrakttype);
		}
	}
	
	@Override
	public void removeLeverandorList(List<Leverandor> leverandorList) {
		for (Leverandor leverandor : leverandorList) {
			leverandor = em.merge(leverandor);
			em.remove(leverandor);
		}		
	}
	
	@Override
	public void removeMenygruppeList(List<Menygruppe> menygruppeList) {
		for (Menygruppe menygruppe : menygruppeList) {
			menygruppe = em.merge(menygruppe);
			em.remove(menygruppe);
		}	
	}
	
	@Override
	public void removeProdtypeList(List<Prodtype> prodtypeList) {
		for (Prodtype prodtype : prodtypeList) {
			prodtype = em.merge(prodtype);
			em.remove(prodtype);
		}
	}

	@Override
	public void removeProsessList(List<Prosess> prosessList) {
		for (Prosess prosess : prosessList) {
			prosess = em.merge(prosess);
			em.remove(prosess);
		}
	}
	
	@Override
	public void removeProsjektList(List<Prosjekt> prosjektList) {
		for (Prosjekt prosjekt : prosjektList) {
			prosjekt = em.merge(prosjekt);
			em.remove(prosjekt);
		}
	}

	@Override
	public void removePunktregStatusList(List<AvvikStatus> punktregstatusList) {
		for (AvvikStatus punktregstatus : punktregstatusList) {
			punktregstatus = em.merge(punktregstatus);
			em.remove(punktregstatus);
		}
	}

	@Override
	public void removePunktregtilstandtypeList(List<Avviktilstand> punktregtilstandtypeList) {
		for (Avviktilstand punktregtilstandtype : punktregtilstandtypeList) {
			punktregtilstandtype = em.merge(punktregtilstandtype);
			em.remove(punktregtilstandtype);
		}
	}

	@Override
	public void removeRodeList(List<Rode> rodeList) {
		for (Rode rode : rodeList) {
			rode = em.merge(rode);
			em.remove(rode);
		}
	}

	@Override
	public void removeRodetypeList(List<Rodetype> rodetypeList) {
		for (Rodetype rodetype : rodetypeList) {
			rodetype = em.merge(rodetype);
			em.remove(rodetype);
		}
	}

	@Override
	public void removeSensortypeList(List<Sensortype> sensortypeList) {
		for (Sensortype sensortype : sensortypeList) {
			sensortype = em.merge(sensortype);
			em.remove(sensortype);
		}
	}

	@Override
	public void removeSesongList(List<Sesong> sesongList) {
		for (Sesong sesong : sesongList) {
			sesong = em.merge(sesong);
			em.remove(sesong);
		}
	}
	
	@Override
	public void removeSkadekontakttypeList(List<Skadekontakttype> skadekontakttypeList) {
		for (Skadekontakttype skadekontakttype : skadekontakttypeList) {
			skadekontakttype = em.merge(skadekontakttype);
			em.remove(skadekontakttype);
		}
	}

	@Override
	public void removeStroproduktgruppeList(List<Stroproduktgruppe> stroproduktgruppeList) {
		for (Stroproduktgruppe stroproduktgruppe : stroproduktgruppeList) {
			stroproduktgruppe = em.merge(stroproduktgruppe);
			em.remove(stroproduktgruppe);
		}
	}

	@Override
	public void removeTilgangsnivaaList(List<Tilgangsnivaa> tilgangsnivaaList) {
		for (Tilgangsnivaa tilgangsnivaa : tilgangsnivaaList) {
			tilgangsnivaa = em.merge(tilgangsnivaa);
			em.remove(tilgangsnivaa);
		}	
	}

	@Override
	public void removeTrafikktiltaktypeList(List<Trafikktiltak> trafikktiltaktypeList) {
		for (Trafikktiltak trafikktiltaktype : trafikktiltaktypeList) {
			trafikktiltaktype = em.merge(trafikktiltaktype);
			em.remove(trafikktiltaktype);
		}
	}

	@Override
	public void removeUtstyrgruppeList(List<Utstyrgruppe> utstyrgruppeList) {
		for (Utstyrgruppe utstyrgruppe : utstyrgruppeList) {
			utstyrgruppe = em.merge(utstyrgruppe);
			em.remove(utstyrgruppe);
		}
	}

	@Override
	public void removeUtstyrmodellList(List<Utstyrmodell> utstyrmodellList) {
		for (Utstyrmodell utstyrmodell : utstyrmodellList) {
			utstyrmodell = em.merge(utstyrmodell);
			em.remove(utstyrmodell);
		}
	}

	@Override
	public void removeUtstyrprodusentList(List<Utstyrprodusent> utstyrprodusentList) {
		for (Utstyrprodusent utstyrprodusent : utstyrprodusentList) {
			utstyrprodusent = em.merge(utstyrprodusent);
			em.remove(utstyrprodusent);
		}
	}

	@Override
	public void removeUtstyrsubgruppeList(List<Utstyrsubgruppe> utstyrsubgruppeList) {
		for (Utstyrsubgruppe utstyrsubgruppe : utstyrsubgruppeList) {
			utstyrsubgruppe = em.merge(utstyrsubgruppe);
			em.remove(utstyrsubgruppe);
		}
	}

	@Override
	public void removeVeinetttypeList(List<Veinetttype> veinetttypeList) {
		for (Veinetttype veinetttype : veinetttypeList) {
			veinetttype = em.merge(veinetttype);
			em.remove(veinetttype);
		}
	}
	
}