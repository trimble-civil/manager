package no.mesta.mipss.entityservice.exception;

import no.mesta.mipss.common.exception.ServiceException;

/**
 * <p>Exception til bruk i {@link MipssEntityService} tjenesten.</p>
 * 
 * @author Christian Wiik (Mesan)
 */
public class EntityServiceException extends ServiceException {
	
	private static final long serialVersionUID = 6976633056944799134L;

	/**
	 * Konstruerer en <tt>EntityServiceException</tt> med feilmelding og årsak.
	 * 
	 * @param message - feilmeldingen til denne feilen.
	 * @param cause - årsaken bak denne feilen.
	 */
	public EntityServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Konstruerer en <tt>EntityServiceException</tt> med feilmelding.
	 * 
	 * @param message - feilmeldingen til denne feilen.
	 */
	public EntityServiceException(String message) {
		super(message);
	}

}