package no.mesta.mipss.entityservice;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.applikasjon.Appstatus;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;
import no.mesta.mipss.persistence.areal.Areal;
import no.mesta.mipss.persistence.areal.Arealtype;
import no.mesta.mipss.persistence.datafangsutstyr.Dfukategori;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.datafangsutstyr.Ioteknologi;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuleverandor;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Doktype;
import no.mesta.mipss.persistence.dokarkiv.Ikon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sesong;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrprodusent;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Driftregion;
import no.mesta.mipss.persistence.kontrakt.Kjoretoykontakttype;
import no.mesta.mipss.persistence.kontrakt.Kontrakttype;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Skadekontakttype;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.persistence.organisasjon.Prosjekt;
import no.mesta.mipss.persistence.stroing.Stroproduktgruppe;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;
import no.mesta.mipss.persistence.veinett.Veinetttype;

/**
 * Tilbyr enkle CRUD-operasjoner på entiteter. Til bruk til vedlikehold av grunndata og konfig param.
 * 
 * @author Christian Wiik (Mesan)
 */
@Remote
public interface MipssEntityService {
	
	public static final String BEAN_NAME = "MipssEntityServiceBean";
	
	/**
     * Henter alle appstatus entiteter.
     * 
     * @return alle entiteter av type <tt>Appstatus</tt>.
     */
	List<Appstatus> getAppstatusList();
	
	/**
     * Henter alle arbeidsfunksjon-type entiteter.
     * 
     * @return alle entiteter av type <tt>Arbeidsfunksjon</tt>.
     */
    List<Arbeidsfunksjon> getArbeidsfunksjonList();

    /**
     * Henter alle areal entiteter.
     * 
     * @return alle entiteter av type <tt>Areal</tt>.
     */    
	List<Areal> getArealList();
	
    /**
     * Henter alle areal-type entiteter.
     * 
     * @return alle entiteter av type <tt>Arealtype</tt>.
     */ 
	List<Arealtype> getArealtypeList();
    
	/**
     * Henter alle dfu-kategori entiteter.
     * 
     * @return alle entiteter av type <tt>Dfukategori</tt>.
     */
    List<Dfukategori> getDfukategoriList();
    
    /**
     * Henter alle dfu-leverandør entiteter.
     * 
     * @return alle entiteter av type <tt>Dfuleverandor</tt>.
     */
    List<Dfuleverandor> getDfuleverandorList();
    
    /**
     * Henter alle dfu-modell entiteter.
     * 
     * @return alle entiteter av type <tt>Dfumodell</tt>.
     */
    List<Dfumodell> getDfumodellList();
	
	/**
     * Henter alle dfu-status entiteter.
     * 
     * @return alle entiteter av type <tt>Dfustatus</tt>.
     */
    List<Dfustatus> getDfustatusList();

	/**
     * Henter alle dfu-varekatalog entiteter.
     * 
     * @return alle entiteter av type <tt>Dfuvarekatalog</tt>.
     */
    List<Dfuvarekatalog> getDfuvarekatalogList();
	
	/**
     * Henter alle dokformat entiteter.
     * 
     * @return alle entiteter av type <tt>Dokformat</tt>.
     */
    List<Dokformat> getDokformatList();
	
	/**
     * Henter alle doktype entiteter.
     * 
     * @return alle entiteter av type <tt>Doktype</tt>.
     */
    List<Doktype> getDoktypeList();
    
    /**
     * Henter alle driftdistrikt entiteter.
     * 
     * @return alle entiteter av type <tt>Driftdistrikt</tt>.
     */
    List<Driftdistrikt> getDriftdistriktList();
    
    /**
     * Henter alle driftregion entiteter.
     * 
     * @return alle entiteter av type <tt>Driftregion</tt>.
     */
    List<Driftregion> getDriftregionList();

    /**
     * Henter alle hendelses-årsak entiteter.
     * 
     * @return alle entiteter av type <tt>Hendelsesaarsak</tt>.
     */
    List<Hendelseaarsak> getHendelsesaarsakList();
    
    /**
     * Henter alle ikon entiteter.
     * 
     * @return alle entiteter av type <tt>Ikon</tt>.
     */
    List<Ikon> getIkonList();
    
    /**
     * Henter alle io-teknologi entiteter.
     * 
     * @return alle entiteter av type <tt>Ioteknologi</tt>.
     */
    List<Ioteknologi> getIoteknologiList();

    /**
     * Henter alle kjøretøy-modell entiteter.
     * 
     * @return alle entiteter av type <tt>Kjoretoymodell</tt>.
     */
    List<Kjoretoymodell> getKjoretoymodellList();
    
    /**
     * Henter alle kjøretøyordre-status entiteter.
     * 
     * @return alle entiteter av type <tt>Kjoretoyordrestatus</tt>.
     */
    List<Kjoretoyordrestatus> getKjoretoyordrestatusList();

    /**
     * Henter alle kjøretøy-type entiteter.
     * 
     * @return alle entiteter av type <tt>Kjoretoytype</tt>.
     */
    List<Kjoretoytype> getKjoretoytypeList();
    
    /**
     * Henter alle konfig-param entiteter.
     * 
     * @return alle entiteter av type <tt>Konfigparam</tt>.
     */
    List<Konfigparam> getKonfigparamList();
    
	/**
     * Henter alle kontakt-type entiteter.
     * 
     * @return alle entiteter av type <tt>Kontakttype</tt>.
     */
    List<Kjoretoykontakttype> getKontakttypeList();

    /**
     * Henter alle kontrakt-type entiteter.
     * 
     * @return alle entiteter av type <tt>Kontrakttype</tt>.
     */
    List<Kontrakttype> getKontrakttypeList();
    
    /**
     * Henter alle leverandør entiteter.
     * 
     * @return alle entiteter av type <tt>Leverandor</tt>.
     */
    List<Leverandor> getLeverandorList();

    /**
     * Henter alle menygruppe entiteter.
     * 
     * @return alle entiteter av type <tt>Menygruppe</tt>.
     */
	List<Menygruppe> getMenygruppeList();

    /**
     * Henter alle prod-type entiteter.
     * 
     * @return alle entiteter av type <tt>Prodtype</tt>.
     */
    List<Prodtype> getProdtypeList();
    
    /**
     * Henter alle prosess entiteter.
     * 
     * @return alle entiteter av type <tt>Prosess</tt>.
     */
    List<Prosess> getProsessList();

    /**
     * Henter alle prosjekt entiteter.
     * 
     * @return alle entiteter av type <tt>Prosjekt</tt>.
     */
    List<Prosjekt> getProsjektList();
    
    /**
     * Henter alle punktreg-status entiteter.
     * 
     * @return alle entiteter av type <tt>PunktregStatus</tt>.
     */
    List<AvvikStatus> getPunktregStatusList();

    /**
     * Henter alle punktregtilstand-type entiteter.
     * 
     * @return alle entiteter av type <tt>Punktregtilstandtype</tt>.
     */
    List<Avviktilstand> getPunktregtilstandtypeList();
    
    /**
     * Henter alle rode entiteter.
     * 
     * @return alle entiteter av type <tt>Rode</tt>.
     */
    List<Rode> getRodeList();
    
    /**
     * Henter alle rode-type entiteter.
     * 
     * @return alle entiteter av type <tt>Rodetype</tt>.
     */
    List<Rodetype> getRodetypeList();
    
    /**
     * Henter alle selskap entiteter.
     * 
     * @return alle entiteter av type <tt>Selskap</tt>.
     */
    List<Selskap> getSelskapList();
    
    /**
     * Henter alle sensor-type entiteter.
     * 
     * @return alle entiteter av type <tt>Sensortype</tt>.
     */
    List<Sensortype> getSensortypeList();

    /**
     * Henter alle sesong entiteter.
     * 
     * @return alle entiteter av type <tt>Sesong</tt>.
     */    
    List<Sesong> getSesongList();
    
    /**
     * Henter alle skadekontakt-type entiteter.
     * 
     * @return alle entiteter av type <tt>Skadekontakttype</tt>.
     */
    List<Skadekontakttype> getSkadekontakttypeList();

    /**
     * Henter alle strøprodukt-gruppe entiteter.
     * 
     * @return alle entiteter av type <tt>Stroproduktgruppe</tt>.
     */
    List<Stroproduktgruppe> getStroproduktgruppeList();
    
    /**
     * Henter alle tilgangsnivå entiteter.
     * 
     * @return alle entiteter av type <tt>Tilgangsnivaa</tt>.
     */
	List<Tilgangsnivaa> getTilgangsnivaaList();

    /**
     * Henter alle trafikktiltak-type entiteter.
     * 
     * @return alle entiteter av type <tt>Trafikktiltaktype</tt>.
     */
    List<Trafikktiltak> getTrafikktiltaktypeList();
    
    /**
     * Henter alle utstyr-gruppe entiteter.
     * 
     * @return alle entiteter av type <tt>Utstyrgruppe</tt>.
     */
    List<Utstyrgruppe> getUtstyrgruppeList();
    
    /**
     * Henter alle utstyr-modell entiteter.
     * 
     * @return alle entiteter av type <tt>Utstyrmodell</tt>.
     */
    List<Utstyrmodell> getUtstyrmodellList();

    /**
     * Henter alle utstyr-produsent entiteter.
     * 
     * @return alle entiteter av type <tt>Utstyrprodusent</tt>.
     */
    List<Utstyrprodusent> getUtstyrprodusentList();
    
    /**
     * Henter alle utstyr-subgruppe entiteter.
     * 
     * @return alle entiteter av type <tt>Utstyrsubgruppe</tt>.
     */
    List<Utstyrsubgruppe> getUtstyrsubgruppeList();
    
    /**
     * Henter alle veinett-type entiteter.
     * 
     * @return alle entiteter av type <tt>Veinetttype</tt>.
     */
    List<Veinetttype> getVeinetttypeList();
    
    /**
     * Legger til en gitt liste med appstatus entiteter.
     * 
     * @param appstatusList - liste med entiteter av type <tt>Appstatus</tt>.
     */
    void persistAppstatusList(List<Appstatus> appstatusList);
    
    /**
     * Legger til en gitt liste med arbeidsfunksjon-type entiteter.
     * 
     * @param arbeidsfunksjonList - liste med entiteter av type <tt>Arbeidsfunksjon</tt>.
     */
    void persistArbeidsfunksjonList(List<Arbeidsfunksjon> arbeidsfunksjonList);
    
    /**
     * Legger til en gitt liste med areal entiteter.
     * 
     * @param arealList - liste med entiteter av type <tt>Areal</tt>.
     */
    void persistArealList(List<Areal> arealList);
    
    /**
     * Legger til en gitt liste med areal-type entiteter.
     * 
     * @param arealtypeList - liste med entiteter av type <tt>Arealtype</tt>.
     */
    void persistArealtypeList(List<Arealtype> arealtypeList);
    
    /**
     * Legger til en gitt liste med dfu-kategori entiteter.
     * 
     * @param dfukategoriList - liste med entiteter av type <tt>Dfukategori</tt>.
     */
    void persistDfukategoriList(List<Dfukategori> dfukategoriList);

    /**
     * Legger til en gitt liste med dfu-leverandør entiteter.
     * 
     * @param dfuleverandorList - liste med entiteter av type <tt>Dfuleverandor</tt>.
     */
    void persistDfuleverandorList(List<Dfuleverandor> dfuleverandorList);
    
    /**
     * Legger til en gitt liste med dfu-modell entiteter.
     * 
     * @param dfumodellList - liste med entiteter av type <tt>Dfumodell</tt>.
     */
    void persistDfumodellList(List<Dfumodell> dfumodellList);

    /**
     * Legger til en gitt liste med dfu-status entiteter.
     * 
     * @param dfustatusList - liste med entiteter av type <tt>Dfustatus</tt>.
     */
    void persistDfustatusList(List<Dfustatus> dfustatusList);
    
    /**
     * Legger til en gitt liste med dfu-varekatalog entiteter.
     * 
     * @param dfuvarekatalogList - liste med entiteter av type <tt>Dfuvarekatalog</tt>.
     */
    void persistDfuvarekatalogList(List<Dfuvarekatalog> dfuvarekatalogList);

    /**
     * Legger til en gitt liste med dokformat entiteter.
     * 
     * @param dokformatList - liste med entiteter av type <tt>Dokformat</tt>.
     */
    void persistDokformatList(List<Dokformat> dokformatList);
    
    /**
     * Legger til en gitt liste med doktype entiteter.
     * 
     * @param doktypeList - liste med entiteter av type <tt>Doktype</tt>.
     */
    void persistDoktypeList(List<Doktype> doktypeList);

    /**
     * Legger til en gitt liste med driftdistrikt entiteter.
     * 
     * @param driftdistriktList - liste med entiteter av type <tt>Driftdistrikt</tt>.
     */
    void persistDriftdistriktList(List<Driftdistrikt> driftdistriktList);
    
    /**
     * Legger til en gitt liste med driftregion entiteter.
     * 
     * @param driftregionList - liste med entiteter av type <tt>Driftregion</tt>.
     */
    void persistDriftregionList(List<Driftregion> driftregionList);

    /**
     * Legger til en gitt liste med hendelses-årsak entiteter.
     * 
     * @param hendelsesaarsakList - liste med entiteter av type <tt>Hendelsesaarsak</tt>.
     */
    void persistHendelsesaarsakList(List<Hendelseaarsak> hendelsesaarsakList);
    

    /**
     * Legger til en gitt liste med ikon entiteter.
     * 
     * @param ikonList - liste med entiteter av type <tt>Ikon</tt>.
     */
    void persistIkonList(List<Ikon> ikonList);

    /**
     * Legger til en gitt liste med io-teknologi entiteter.
     * 
     * @param ioteknologiList - liste med entiteter av type <tt>Ioteknologi</tt>.
     */
    void persistIoteknologiList(List<Ioteknologi> ioteknologiList);

    /**
     * Legger til en gitt liste med kjøretøy-modell entiteter.
     * 
     * @param kjoretoymodellList - liste med entiteter av type <tt>Kjoretoymodell</tt>.
     */
    void persistKjoretoymodellList(List<Kjoretoymodell> kjoretoymodellList);
    
    /**
     * Legger til en gitt liste med kjøretøyordre-status entiteter.
     * 
     * @param kjoretoyordrestatusList - liste med entiteter av type <tt>Kjoretoyordrestatus</tt>.
     */
    void persistKjoretoyordrestatusList(List<Kjoretoyordrestatus> kjoretoyordrestatusList);    

    /**
     * Legger til en gitt liste med kjøretøy-type entiteter.
     * 
     * @param kjoretoytypeList - liste med entiteter av type <tt>Kjoretoytype</tt>.
     */
    void persistKjoretoytypeList(List<Kjoretoytype> kjoretoytypeList);
    
    /**
     * Legger til en gitt liste med konfig-param entiteter.
     * 
     * @param konfigparamList - liste med entiteter av type <tt>Konfigparam</tt>.
     */
    void persistKonfigparamList(List<Konfigparam> konfigparamList);
    
    /**
     * Legger til en gitt liste med kontakt-type entiteter.
     * 
     * @param kontakttypeList - liste med entiteter av type <tt>Kontakttype</tt>.
     */
    void persistKontakttypeList(List<Kjoretoykontakttype> kontakttypeList);

    /**
     * Legger til en gitt liste med kontrakt-type entiteter.
     * 
     * @param kontrakttypeList - liste med entiteter av type <tt>Kontrakttype</tt>.
     */
    void persistKontrakttypeList(List<Kontrakttype> kontrakttypeList);
    
    /**
     * Legger til en gitt liste med leverandør entiteter.
     * 
     * @param leverandorList - liste med entiteter av type <tt>Leverandor</tt>.
     */
    void persistLeverandorList(List<Leverandor> leverandorList);

    /**
     * Legger til en gitt liste med menygruppe entiteter.
     * 
     * @param menygruppeList - liste med entiteter av type <tt>Menygruppe</tt>.
     */
    void persistMenygruppeList(List<Menygruppe> menygruppeList);

    /**
     * Legger til en gitt liste med prod-type entiteter.
     * 
     * @param prodtypeList - liste med entiteter av type <tt>Prodtype</tt>.
     */
    void persistProdtypeList(List<Prodtype> prodtypeList);
    
    /**
     * Legger til en gitt liste med prosess entiteter.
     * 
     * @param prosessList - liste med entiteter av type <tt>Prosess</tt>.
     */
    void persistProsessList(List<Prosess> prosessList);

    /**
     * Legger til en gitt liste med prosjekt entiteter.
     * 
     * @param prosjektList - liste med entiteter av type <tt>Prosjekt</tt>.
     */
    void persistProsjektList(List<Prosjekt> prosjektList);
    
    /**
     * Legger til en gitt liste med punktreg-status entiteter.
     * 
     * @param punktregStatusList - liste med entiteter av type <tt>PunktregStatus</tt>.
     */
    void persistPunktregStatusList(List<AvvikStatus> punktregStatusList);

    /**
     * Legger til en gitt liste med punktregtilstand-type entiteter.
     * 
     * @param punktregtilstandtypeList - liste med entiteter av type <tt>Punktregtilstandtype</tt>.
     */
    void persistPunktregtilstandtypeList(List<Avviktilstand> punktregtilstandtypeList);
    
    /**
     * Legger til en gitt liste med rode entiteter.
     * 
     * @param rodeList - liste med entiteter av type <tt>Rode</tt>.
     */
    void persistRodeList(List<Rode> rodeList);
    
    /**
     * Legger til en gitt liste med rode-type entiteter.
     * 
     * @param rodetypeList - liste med entiteter av type <tt>Rodetype</tt>.
     */
    void persistRodetypeList(List<Rodetype> rodetypeList);

    /**
     * Legger til en gitt liste med selskap entiteter.
     * 
     * @param selskapList - liste med entiteter av type <tt>Selskap</tt>.
     */
    void persistSelskapList(List<Selskap> selskapList);
    
    /**
     * Legger til en gitt liste med sensor-type entiteter.
     * 
     * @param sensortypeList - liste med entiteter av type <tt>Sensortype</tt>.
     */
    void persistSensortypeList(List<Sensortype> sensortypeList);
    
    /**
     * Legger til en gitt liste med sesong entiteter.
     * 
     * @param sesongList - liste med entiteter av type <tt>Sesong</tt>.
     */
    void persistSesongList(List<Sesong> sesongList);
    
    /**
     * Legger til en gitt liste med skadekontakt-type entiteter.
     * 
     * @param skadekontakttypeList - liste med entiteter av type <tt>Skadekontakttype</tt>.
     */
    void persistSkadekontakttypeList(List<Skadekontakttype> skadekontakttypeList);

    /**
     * Legger til en gitt liste med strøprodukt-gruppe entiteter.
     * 
     * @param stroproduktgruppeList - liste med entiteter av type <tt>Stroproduktgruppe</tt>.
     */
    void persistStroproduktgruppeList(List<Stroproduktgruppe> stroproduktgruppeList);
    
    /**
     * Legger til en gitt liste med tilgangsnivå entiteter.
     * 
     * @param tilgangsnivaaList - liste med entiteter av type <tt>Tilgangsnivaa</tt>.
     */
    void persistTilgangsnivaaList(List<Tilgangsnivaa> tilgangsnivaaList);

    /**
     * Legger til en gitt liste med trafikktiltak-type entiteter.
     * 
     * @param trafikktiltaktypeList - liste med entiteter av type <tt>Trafikktiltaktype</tt>.
     */
    void persistTrafikktiltaktypeList(List<Trafikktiltak> trafikktiltaktypeList);
    
    /**
     * Legger til en gitt liste med utstyr-gruppe entiteter.
     * 
     * @param utstyrgruppeList - liste med entiteter av type <tt>Utstyrgruppe</tt>.
     */
    void persistUtstyrgruppeList(List<Utstyrgruppe> utstyrgruppeList);
    
    /**
     * Legger til en gitt liste med utstyr-modell entiteter.
     * 
     * @param utstyrmodellList - liste med entiteter av type <tt>Utstyrmodell</tt>.
     */
    void persistUtstyrmodellList(List<Utstyrmodell> utstyrmodellList);

    /**
     * Legger til en gitt liste med utstyr-produsent entiteter.
     * 
     * @param utstyrprodusentList - liste med entiteter av type <tt>Utstyrprodusent</tt>.
     */
    void persistUtstyrprodusentList(List<Utstyrprodusent> utstyrprodusentList);
    
    /**
     * Legger til en gitt liste med utstyr-subgruppe entiteter.
     * 
     * @param utstyrsubgruppeList - liste med entiteter av type <tt>Utstyrsubgruppe</tt>.
     */
    void persistUtstyrsubgruppeList(List<Utstyrsubgruppe> utstyrsubgruppeList);

    /**
     * Legger til en gitt liste med veinett-type entiteter.
     * 
     * @param veinetttypeList - liste med entiteter av type <tt>Veinetttype</tt>.
     */
    void persistVeinetttypeList(List<Veinetttype> veinetttypeList);
    
    /**
     * Sletter en gitt liste med appstatus entiteter.
     * 
     * @param appstatusList - liste med entiteter av type <tt>Appstatus</tt>.
     */
    void removeAppstatusList(List<Appstatus> appstatusList);
    
    /**
     * Sletter en gitt liste med areal entiteter.
     * 
     * @param arealList - liste med entiteter av type <tt>Areal</tt>.
     */
    void removeArealList(List<Areal> arealList);
    
    /**
     * Sletter en gitt liste med areal-type entiteter.
     * 
     * @param arealtypeList - liste med entiteter av type <tt>Arealtype</tt>.
     */
    void removeArealtypeList(List<Arealtype> arealtypeList);

    /**
     * Sletter en gitt liste med dfu-kategori entiteter.
     * 
     * @param dfukategoriList - liste med entiteter av type <tt>Dfukategori</tt>.
     */
    void removeDfukategoriList(List<Dfukategori> dfukategoriList);

    /**
     * Sletter en gitt liste med dfu-leverandør entiteter.
     * 
     * @param dfuleverandorList - liste med entiteter av type <tt>Dfuleverandor</tt>.
     */
    void removeDfuleverandorList(List<Dfuleverandor> dfuleverandorList);
    
    /**
     * Sletter en gitt liste med dfu-modell entiteter.
     * 
     * @param dfumodellList - liste med entiteter av type <tt>Dfumodell</tt>.
     */
    void removeDfumodellList(List<Dfumodell> dfumodellList);

    /**
     * Sletter en gitt liste med dfu-status entiteter.
     * 
     * @param dfustatusList - liste med entiteter av type <tt>Dfustatus</tt>.
     */
    void removeDfustatusList(List<Dfustatus> dfustatusList);
    
    /**
     * Sletter en gitt liste med dfu-varekatalog entiteter.
     * 
     * @param dfuvarekatalogList - liste med entiteter av type <tt>Dfuvarekatalog</tt>.
     */
    void removeDfuvarekatalogList(List<Dfuvarekatalog> dfuvarekatalogList);

    /**
     * Sletter en gitt liste med dokformat entiteter.
     * 
     * @param dokformatList - liste med entiteter av type <tt>Dokformat</tt>.
     */
    void removeDokformatList(List<Dokformat> dokformatList);
    
    /**
     * Sletter en gitt liste med doktype entiteter.
     * 
     * @param doktypeList - liste med entiteter av type <tt>Doktype</tt>.
     */
    void removeDoktypeList(List<Doktype> doktypeList);    

    /**
     * Sletter en gitt liste med driftdistrikt entiteter.
     * 
     * @param driftdistriktList - liste med entiteter av type <tt>Driftdistrikt</tt>.
     */
    void removeDriftdistriktList(List<Driftdistrikt> driftdistriktList);
    
    /**
     * Sletter en gitt liste med driftregion entiteter.
     * 
     * @param driftregionList - liste med entiteter av type <tt>Driftregion</tt>.
     */
    void removeDriftregionList(List<Driftregion> driftregionList);

    /**
     * Sletter en gitt liste med hendelses-årsak entiteter.
     * 
     * @param hendelsesaarsakList - liste med entiteter av type <tt>Hendelsesaarsak</tt>.
     */
    void removeHendelsesaarsakList(List<Hendelseaarsak> hendelsesaarsakList);
    
    /**
     * Sletter en gitt liste med ikon entiteter.
     * 
     * @param ikonList - liste med entiteter av type <tt>Ikon</tt>.
     */
    void removeIkonList(List<Ikon> ikonList);

    /**
     * Sletter en gitt liste med io-teknologi entiteter.
     * 
     * @param ioteknologiList - liste med entiteter av type <tt>Ioteknologi</tt>.
     */
    void removeIoteknologiList(List<Ioteknologi> ioteknologiList);

    /**
     * Sletter en gitt liste med kjøretøy-modell entiteter.
     * 
     * @param kjoretoymodellList - liste med entiteter av type <tt>kjoretoymodellList</tt>.
     */
    void removeKjoretoymodellList(List<Kjoretoymodell> kjoretoymodellList);
    
    /**
     * Sletter en gitt liste med kjøretøyordre-status entiteter.
     * 
     * @param kjoretoyordrestatusList - liste med entiteter av type <tt>Kjoretoyordrestatus</tt>.
     */
    void removeKjoretoyordrestatusList(List<Kjoretoyordrestatus> kjoretoyordrestatusList);

    /**
     * Sletter en gitt liste med kjøretøy-typ entiteter.
     * 
     * @param kjoretoytypeList - liste med entiteter av type <tt>Kjoretoytype</tt>.
     */
    void removeKjoretoytypeList(List<Kjoretoytype> kjoretoytypeList);
    
    /**
     * Sletter en gitt liste med konfig-param entiteter.
     * 
     * @param konfigparamList - liste med entiteter av type <tt>Konfigparam</tt>.
     */
    void removeKonfigparamList(List<Konfigparam> konfigparamList);
    
    /**
     * Sletter en gitt liste med kontakt-type entiteter.
     * 
     * @param kontakttypeList - liste med entiteter av type <tt>Kontakttype</tt>.
     */
    void removeKontakttypeList(List<Kjoretoykontakttype> kontakttypeList);

    /**
     * Sletter en gitt liste med kontrakt-type entiteter.
     * 
     * @param kontrakttypeList - liste med entiteter av type <tt>Kontrakttype</tt>.
     */
    void removeKontrakttypeList(List<Kontrakttype> kontrakttypeList);
    
    /**
     * Sletter en gitt liste med leverandør entiteter.
     * 
     * @param leverandorList - liste med entiteter av type <tt>Leverandor</tt>.
     */
    void removeLeverandorList(List<Leverandor> leverandorList);

    /**
     * Sletter en gitt liste med menygruppe entiteter.
     * 
     * @param menygruppeList - liste med entiteter av type <tt>Menygruppe</tt>.
     */
    void removeMenygruppeList(List<Menygruppe> menygruppeList);
    
    /**
     * Sletter en gitt liste med prod-type entiteter.
     * 
     * @param prodtypeList - liste med entiteter av type <tt>Prodtype</tt>.
     */
    void removeProdtypeList(List<Prodtype> prodtypeList);

    /**
     * Sletter en gitt liste med prosess entiteter.
     * 
     * @param prosessList - liste med entiteter av type <tt>Prosess</tt>.
     */
    void removeProsessList(List<Prosess> prosessList);
    
    /**
     * Sletter en gitt liste med prosjekt entiteter.
     * 
     * @param prosjektList - liste med entiteter av type <tt>Prosjekt</tt>.
     */
    void removeProsjektList(List<Prosjekt> prosjektList);

    /**
     * Sletter en gitt liste med punktreg-status entiteter.
     * 
     * @param punktregStatusList - liste med entiteter av type <tt>PunktregStatus</tt>.
     */
    void removePunktregStatusList(List<AvvikStatus> punktregStatusList);
    
    /**
     * Sletter en gitt liste med punktregtilstand-type entiteter.
     * 
     * @param punktregtilstandtypeList - liste med entiteter av type <tt>Punktregtilstandtype</tt>.
     */
    void removePunktregtilstandtypeList(List<Avviktilstand> punktregtilstandtypeList);

    /**
     * Sletter en gitt liste med rode entiteter.
     * 
     * @param rodeList - liste med entiteter av type <tt>Rode</tt>.
     */
    void removeRodeList(List<Rode> rodeList);

    /**
     * Sletter en gitt liste med rode-type entiteter.
     * 
     * @param rodetypeList - liste med entiteter av type <tt>Rodetype</tt>.
     */
    void removeRodetypeList(List<Rodetype> rodetypeList);
    
    /**
     * Sletter en gitt liste med sensor-type entiteter.
     * 
     * @param sensortypeList - liste med entiteter av type <tt>Sensortype</tt>.
     */
    void removeSensortypeList(List<Sensortype> sensortypeList);

    /**
     * Sletter en gitt liste med selskap entiteter.
     * 
     * @param selskapList - liste med entiteter av type <tt>Selskap</tt>.
     */
//    void removeSelskapList(List<Selskap> selskapList);
    
    /**
     * Sletter en gitt liste med sesong entiteter.
     * 
     * @param sesongList - liste med entiteter av type <tt>Sesong</tt>.
     */
    void removeSesongList(List<Sesong> sesongList);
    
    /**
     * Sletter en gitt liste med skadekontakt-type entiteter.
     * 
     * @param skadekontakttypeList - liste med entiteter av type <tt>Skadekontakttype</tt>.
     */
    void removeSkadekontakttypeList(List<Skadekontakttype> skadekontakttypeList);

    /**
     * Sletter en gitt liste med strøprodukt-gruppe entiteter.
     * 
     * @param stroproduktgruppeList - liste med entiteter av type <tt>Stroproduktgruppe</tt>.
     */
    void removeStroproduktgruppeList(List<Stroproduktgruppe> stroproduktgruppeList);
    
    /**
     * Sletter en gitt liste med tilgangsnivå entiteter.
     * 
     * @param tilgangsnivaaList - liste med entiteter av type <tt>Tilgangsnivaa</tt>.
     */
    void removeTilgangsnivaaList(List<Tilgangsnivaa> tilgangsnivaaList);

    /**
     * Sletter en gitt liste med trafikktiltak-type entiteter.
     * 
     * @param trafikktiltaktypeList - liste med entiteter av type <tt>Trafikktiltaktype</tt>.
     */
    void removeTrafikktiltaktypeList(List<Trafikktiltak> trafikktiltaktypeList);
    
    /**
     * Sletter en gitt liste med utstyr-gruppe entiteter.
     * 
     * @param utstyrgruppeList - liste med entiteter av type <tt>Utstyrgruppe</tt>.
     */
    void removeUtstyrgruppeList(List<Utstyrgruppe> utstyrgruppeList);
    
    /**
     * Sletter en gitt liste med utstyr-modell entiteter.
     * 
     * @param utstyrmodellList - liste med entiteter av type <tt>Utstyrmodell</tt>.
     */
    void removeUtstyrmodellList(List<Utstyrmodell> utstyrmodellList);

    /**
     * Sletter en gitt liste med utstyr-produsent entiteter.
     * 
     * @param utstyrprodusentList - liste med entiteter av type <tt>Utstyrprodusent</tt>.
     */
    void removeUtstyrprodusentList(List<Utstyrprodusent> utstyrprodusentList);
    
    /**
     * Sletter en gitt liste med utstyr-subgruppe entiteter.
     * 
     * @param utstyrsubgruppeList - liste med entiteter av type <tt>Utstyrsubgruppe</tt>.
     */
    void removeUtstyrsubgruppeList(List<Utstyrsubgruppe> utstyrsubgruppeList);

    /**
     * Sletter en gitt liste med veinett-type entiteter.
     * 
     * @param veinetttypeList - liste med entiteter av type <tt>Veinetttype</tt>.
     */
    void removeVeinetttypeList(List<Veinetttype> veinetttypeList);
    
}