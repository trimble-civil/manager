package no.mesta.mipss.entityservice;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.MipssEntityBean;

@Remote
public interface MipssGrunndataService {
	public static final String BEAN_NAME="GrunndataServiceBean";
	
	public List<MipssEntityBean<?>> getEntityList(String clazz );
	public void persistEntities(List<MipssEntityBean<?>> entityList);
	public void removeEntities(List<MipssEntityBean<?>> entityList);
	
}
