package no.mesta.mipss.query;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jms.IllegalStateException;

public class NestedTableDescriptor<T> {
	private Class<T> javaClass;
	private String[] fields;
	private Class[] parameterTypes;
	private List<Method> methods;
	private List<NestedTableDescriptor<?>> descriptors;
	private Iterator<NestedTableDescriptor<?>> iterator;
	
	public NestedTableDescriptor(Class<T> javaClass, String... fields){
		this.javaClass = javaClass;
		this.fields = fields;
		
		methods = new ArrayList<Method>();
		parameterTypes = new Class[fields.length];
		
		for (int i=0;i<fields.length;i++){
            try{
            	String field = fields[i];
            	String accessor = getFieldAccessor("set", field);
                Class<?> paramClass = javaClass.getDeclaredField(field).getType();
                parameterTypes[i] = paramClass;
                Method m = javaClass.getMethod(accessor, new Class<?>[]{paramClass});
                methods.add(m);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            } catch (NoSuchFieldException e){
            	throw new RuntimeException(e);
            }
        }
	}

	public Class<T> getJavaClass() {
		return javaClass;
	}

	public void setJavaClass(Class<T> javaClass) {
		this.javaClass = javaClass;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}
	
	public List<Method> getFieldAccessorMethods(){
		return methods;
	}
	
	public Class[] getParameterTypes(){
		return parameterTypes;
	}
	
	private String getFieldAccessor(String prefix, String suffix){
        String fieldName = prefix+ (""+suffix.charAt(0)).toUpperCase()+suffix.substring(1);
        return fieldName;
    }
	
	public void addDescriptor(NestedTableDescriptor<?> subdescriptor){
		if (this.descriptors==null)
			descriptors = new ArrayList<NestedTableDescriptor<?>>();
		descriptors.add(subdescriptor);
	}
	
	public Iterator<NestedTableDescriptor<?>> getDescriptorIterator(){
		if (this.descriptors==null)
			throw new RuntimeException("Mangler subdescriptor for entitet");
		if (iterator==null)
			iterator = this.descriptors.iterator();
		return iterator;
	}

	public void initDescriptorIterator() {
		if (this.descriptors!=null)
			iterator = this.descriptors.iterator();
		
	}
}
