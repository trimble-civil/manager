package no.mesta.mipss.query;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.jboss.jca.adapters.jdbc.WrappedConnection;

import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse som h�ndterer <code>Struct</code> og <code>Array</code> typer
 * definert i databasen.
 *
 * @author harkul
 *
 */
public class StructHelper {

    private static final Logger log = LoggerFactory.getLogger(StructHelper.class);

	public static final String VEISTREKNING_TYPE = "VEISTREKNING_TYPE";
	public static final String VEISTREKNING_LISTE = "VEISTREKNING_LISTE";
	public static final String BRUKER_TYPE = "BRUKER_TYPE";
	public static final String BRUKER_LISTE = "BRUKER_LISTE";
	public static final String PDA_TYPE = "PDA_TYPE";
	public static final String PDA_LISTE = "PDA_LISTE";
	public static final String PROSESS_TYPE = "PROSESS_TYPE";
	public static final String PROSESS_LISTE = "PROSESS_LISTE";

	private Connection connection;
    private static boolean isLocalMode;

	public StructHelper() throws SQLException {
		Context ctx;
		try {
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:/mipssDS");
			connection = ds.getConnection();
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Oppretter en <code>StructDescriptor</code> med angitt navn. Navnet er det
	 * som er definert i databasen
	 *
	 * @param name
	 *            objektnavnet i databasen
	 * @return
	 * @throws SQLException
	 */
	private StructDescriptor createDescriptor(String name) throws SQLException {

		Connection conToUse = getUnderlyingConnection();

		StructDescriptor veiDesc = StructDescriptor.createDescriptor(name,
				conToUse);
		return veiDesc;
	}

	/**
	 * Oppretter en <code>ArrayDescriptor</code> med angitt navn. Navnet er det
	 * som er definert i databasen
	 *
	 * @param name
	 *            tabellobjektet i databasen
	 * @return
	 * @throws SQLException
	 */
	private ArrayDescriptor createArrayDescriptor(String name) throws SQLException {
		Connection conToUse = getUnderlyingConnection();

        ArrayDescriptor arrDescr = ArrayDescriptor.createDescriptor(name, conToUse);
		return arrDescr;
	}

    private Connection getUnderlyingConnection() throws SQLException {
        Connection conToUse;
        try {
            log.debug("Class name: {}", connection.getClass().getName());
            conToUse = connection;
            while(conToUse.getClass().getName().contains("Wrapped")) {
                org.jboss.jca.adapters.jdbc.jdk7.WrappedConnectionJDK7 wrapped = (org.jboss.jca.adapters.jdbc.jdk7.WrappedConnectionJDK7) conToUse;
			    conToUse = wrapped.getUnderlyingConnection();
                log.debug("Underlying connection: {}", conToUse.getClass().getName());
            }
        } catch(NoClassDefFoundError e) {
            log.error("No class definition found for WrappedConnection, are you running local debug?", e);
            conToUse = this.connection;
        }
        return conToUse;
    }

    public Array createProsessArray(List<Long> prosessList) throws SQLException {
		if (prosessList == null)
			prosessList = new ArrayList<Long>();

		Struct[] prosesser = new Struct[prosessList.size()];
		for (int i = 0; i < prosesser.length; i++) {
			prosesser[i] = createProsess(prosessList.get(i));
		}
		return createArray(PROSESS_LISTE, prosesser);

	}

	/**
	 * Lager en pda-struct
	 *
	 * @param prosessId
	 * @return
	 * @throws SQLException
	 */
	public Struct createProsess(Long prosessId) throws SQLException {
		Object[] ar = new Object[1];
		ar[0] = prosessId;
		return createStruct(createDescriptor(PROSESS_TYPE), ar);
	}

	/**
	 * Lager en pda-sql-Array
	 *
	 * @param pdaIdentList
	 * @return
	 * @throws SQLException
	 */
	public Array createPdaArray(List<Long> pdaIdentList) throws SQLException {
		if (pdaIdentList == null)
			pdaIdentList = new ArrayList<Long>();

		Struct[] pdas = new Struct[pdaIdentList.size()];
		for (int i = 0; i < pdas.length; i++) {
			pdas[i] = createPda(pdaIdentList.get(i));
		}
		return createArray(PDA_LISTE, pdas);
	}

	/**
	 * Lager en pda-struct
	 *
	 * @param pdaIdent
	 * @return
	 * @throws SQLException
	 */
	public Struct createPda(Long pdaIdent) throws SQLException {
		Object[] ar = new Object[1];
		ar[0] = pdaIdent;
		return createStruct(createDescriptor(PDA_TYPE), ar);
	}

	/**
	 * Lager en bruker-sql-array
	 *
	 * @param brukerList
	 * @return
	 * @throws SQLException
	 */
	public Array createBrukerArray(List<String> brukerList) throws SQLException {
		if (brukerList == null)
			brukerList = new ArrayList<String>();

		Struct[] brukere = new Struct[brukerList.size()];
		for (int i = 0; i < brukerList.size(); i++) {
			brukere[i] = createBruker(brukerList.get(i));
		}
		return createArray(BRUKER_LISTE, brukere);
	}

	/**
	 * Lager en bruker-Struct
	 *
	 * @param bruker
	 * @return
	 * @throws SQLException
	 */
	public Struct createBruker(String bruker) throws SQLException {
		Object[] ar = new Object[1];
		ar[0] = bruker;
		return createStruct(createDescriptor(BRUKER_TYPE), ar);
	}

	/**
	 * Lager en sql-array basert på angitt descritorNav og Struct array
	 *
	 * @param descriptorName
	 *            navnet på ARRAY/TABLE typen i databasen
	 * @param array
	 * @return
	 * @throws SQLException
	 */
	private Array createArray(String descriptorName, Struct[] array)
			throws SQLException {
		ArrayDescriptor arDesc = createArrayDescriptor(descriptorName);
		Array sqlArray = createArray(array, arDesc);
		return sqlArray;
	}

	/**
	 * Lager en <code>Array</code> av <code>Veinettveireferanse</code>.
	 * Konverterer f�rst hver <code>Veinettveireferanse</code> til en
	 * <code>Struct</code>, deretter legges disse til i en <code>Array</code>
	 *
	 * @param veirefList
	 *            listen med veireferanser som skal konverteres til Struct og
	 *            Array
	 * @param inkluderKm
	 *            true hvis veireferansene skal ha med km fra og til.
	 * @return
	 * @throws SQLException
	 */
	public Array createVeireferanseArray(List<Veinettveireferanse> veirefList,
			boolean inkluderKm) throws SQLException {
		if (veirefList == null)
			veirefList = new ArrayList<Veinettveireferanse>();
		Struct[] veirefs = new Struct[veirefList.size()];
		for (int i = 0; i < veirefList.size(); i++) {
			veirefs[i] = createVeireferanse(veirefList.get(i), inkluderKm);
		}
		ArrayDescriptor arDescr = createArrayDescriptor(VEISTREKNING_LISTE);
		Array array = createArray(veirefs, arDescr);
		return array;
	}

	private ARRAY createArray(Struct[] veirefs, ArrayDescriptor arDescr)
			throws SQLException {
		Connection conToUse = null;

        if(!isLocalMode) {
            if (connection instanceof WrappedConnection) {
                return new ARRAY(arDescr, getUnderlyingConnection(), veirefs);
            }
        }
        return new ARRAY(arDescr, connection, veirefs);
	}

	/**
	 * Lager en <code>Struct</code> av en <code>Veinettveireferanse</code>.
	 *
	 * @param v
	 *            veireferansen som skal konverteres
	 * @param inkluderKm
	 *            true hvis veireferansen skal ha med km fra og til
	 * @return
	 * @throws SQLException
	 */
	public Struct createVeireferanse(Veinettveireferanse v, boolean inkluderKm)
			throws SQLException {
		return createStruct(createDescriptor(VEISTREKNING_TYPE),
				convertVeinettveireferanseToOjbect(v, inkluderKm));
	}

	private Struct createStruct(StructDescriptor descriptor, Object[] obj)
			throws SQLException {
		return new STRUCT(descriptor, getUnderlyingConnection(), obj);
	}

	/**
	 * Oppretter en object[] av veinettvereferansen der indexene i array matcher
	 * rekkef�lgen objektet er definert i databasen.
	 *
	 * @param vr
	 * @param inkluderKm
	 * @return
	 */
	private Object[] convertVeinettveireferanseToOjbect(Veinettveireferanse vr,
			boolean inkluderKm) {
		Object[] ar = new Object[8];
		ar[0] = vr.getFylkesnummer();
		ar[1] = vr.getKommunenummer();
		ar[2] = vr.getVeikategori();
		ar[3] = vr.getVeistatus();
		ar[4] = vr.getVeinummer();
		ar[5] = vr.getHp();
		if (inkluderKm) {
			ar[6] = vr.getFraKm();
			ar[7] = vr.getTilKm();
		} else {
			ar[6] = null;
			ar[7] = null;
		}
		return ar;
	}

	public void goodnight(){
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

    public static void setLocalMode(boolean localMode) {
        isLocalMode = localMode;
    }

}
