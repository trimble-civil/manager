package no.mesta.mipss.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.konfigparam.KonfigParamLocal;

public class EmailHelper {
	private String from = "produksjonsstyring@mesta.no";
	private List<String> recipients = new ArrayList<String>();
	private String subject;
	private String message;
	private final EntityManager em;
	private final KonfigParamLocal params;
	
	public EmailHelper(EntityManager em, KonfigParamLocal params){
		this.em=em;
		this.params=params;
		this.setFrom(getSupportEpostAdresse());
	}
	
	/**
	 * Setter avsenderen av e-posten, standard er mipss@mesta.no
	 * @param from
	 */
	public void setFrom(String from){
		this.from = from;
	}

	/**
	 * Setter emnet til e-posten
	 * @param subject
	 */
	public void setSubject(String subject){
		this.subject = subject;
	}
	
	/**
	 * Setter innholdet til meldingen
	 * @param message
	 */
	public void setMessage(String message){
		this.message = message;
	}
	
	/**
	 * Legger til en mottaker i listen
	 * @param recipient
	 */
	public void addRecipient(String recipient){
		recipients.add(recipient);
	}

	/**
	 * Lager et Message-objekt basert på data som har blitt satt tidligere.
	 * @return
	 * @throws MessagingException
	 */
	private Message createMessage() throws MessagingException {
		String smtpServer = (String)em.createNativeQuery("select tekst from epostelement where navn='SMTP'").getSingleResult();
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpServer);

		Session session = Session.getDefaultInstance(props, null);
		Message msg = new MimeMessage(session);

		InternetAddress addressFrom = new InternetAddress(from);
		msg.setFrom(addressFrom);

		InternetAddress[] addressTo = new InternetAddress[recipients.size()];
		for (int i = 0; i < recipients.size(); i++){
			addressTo[i] = new InternetAddress(recipients.get(i));
		}

		msg.setRecipients(Message.RecipientType.TO, addressTo);
		msg.setSubject(subject);

		return msg;
	}

	/**
	 * Sender e-postadressen
	 * @throws MessagingException
	 */
	public void sendMail() throws MessagingException {
		Message msg = createMessage();
		msg.setContent(message, "text/plain");
		Transport.send(msg);
	}

	public void sendHtmlMail() throws MessagingException {
		Message msg = createMessage();
		msg.setContent(message, "text/html; charset=utf-8");
		Transport.send(msg);
	}
	
	public String getSupportEpostAdresse(){
		String epostAdresse = params.hentEnForApp("studio.mipss", "supportEpost").getVerdi();
		    	
    	if (epostAdresse == null || epostAdresse.equals("")) {
			return PropertyResourceBundleUtil.getPropertyBundle("mipssCommonText").getGuiString("defaultEmail");
		}
    	
    	return epostAdresse;
    }
}
