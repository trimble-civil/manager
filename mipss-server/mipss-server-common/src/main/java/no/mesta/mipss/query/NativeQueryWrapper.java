package no.mesta.mipss.query;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 * En wrapperklasse for NativeQueries som returnerer en liste med wrappede objekter. Denne klassen
 * fungerer som en wrapper for kall mot tabellfunksjoner i Oracle. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("unchecked")
public class NativeQueryWrapper<T> {
    private String dateMask = "dd.MM.yyyy HH:mm:ss";
        
    protected List<T> wrappedList;
    protected List<Method> methods = new ArrayList<Method>();
    protected Class<T> clazz;
    protected Class<?>[] parameterTypes;
    protected Query query;
    private DateFormat dateFormatter;
	private List<NestedTableDescriptor<?>> nestedTableDescriptors;
	private List queryResults;
    
    /**
     * Konstruktør
     * @param query nativeQuery'n som inneholder resultatsettet som skal konverteres
     * @param clazz klassen som listen som returneres skal wrappes til
     * @param parameterTypes hvilke klasser parameterne til set-metodene består av
     * @param fields navnet på feltene som skal få verdiene, må være i samme rekkefølge som resultatsettet
     */
    public NativeQueryWrapper(Query query, Class<T> clazz, Class<?>[] parameterTypes, String... fields) {
        this.clazz = clazz;
        this.parameterTypes = parameterTypes;
        this.query = query;
        
        for (int i=0;i<fields.length;i++){
            String accessor = getFieldAccessor("set", fields[i]);
            try{
            	
                Method m = clazz.getMethod(accessor, new Class<?>[]{parameterTypes[i]});
                methods.add(m);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }
        
    /**
     * Forenklet konstruktør som selv finner ut instansvariablenes klasser
     * @param query
     * @param clazz
     * @param fields
     */
	public NativeQueryWrapper(Query query, Class<T> clazz, String... fields) {
        this.clazz = clazz;
        
        parameterTypes = new Class[fields.length];
        this.query = query;
                
        for (int i=0;i<fields.length;i++){
            try{
            	String field = fields[i];
            	String accessor = getFieldAccessor("set", field);
                Class<?> paramClass = clazz.getDeclaredField(field).getType();
                parameterTypes[i] = paramClass;
                Method m = clazz.getMethod(accessor, new Class<?>[]{paramClass});
                methods.add(m);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            } catch (NoSuchFieldException e){
            	throw new RuntimeException(e);
            }
        }
    }
	
	/**
	 * Forutsetter at sp�rringen er gjort og resultatene ligger i listen queryResults
	 * 
	 * @param queryResults
	 * @param clazz
	 * @param fields
	 */
	public NativeQueryWrapper(List queryResults, Class<T> clazz, String... fields){
		this((Query)null, clazz, fields);
		this.queryResults = queryResults;
	}
	
    /**
     * Oppretter en ny instans med felttyper definert på en enklere måte.
     * @param query som skal fyres av
     * @param clazz for entiteten som mottar resultatene
     * @param types fro flttypene med sine navn.
     */
    public NativeQueryWrapper(Query query, Class<T> clazz, EntityField[] types) {
        this.clazz = clazz;
        this.query = query;
        
        parameterTypes = new Class[types.length];
        
        int i = 0;
        //henter alle set metoder
        for(EntityField field : types) {
            String accessor = getFieldAccessor("set", field.name);
            try{
                Method m = clazz.getMethod(accessor, new Class<?>[]{field.type});
                methods.add(m);
            } 
            catch (NoSuchMethodException e) {
            	throw new RuntimeException(e);
            }
            parameterTypes[i++] = field.type;
        }
    }
    
	public void addNestedTableDescriptor(NestedTableDescriptor<?> descr){
		if (nestedTableDescriptors==null)
			nestedTableDescriptors = new ArrayList<NestedTableDescriptor<?>>();
		nestedTableDescriptors.add(descr);
	}
	
    private List getQueryResults(){
    	if (queryResults!=null)
    		return queryResults;
    	return query.getResultList();
    }
    
    public void setQueryResults(List queryResults){
    	this.queryResults = queryResults;
    }
    /**
     * Henter listen med de wrappede PoJOene
     * @return
     */
	public List<T> getWrappedList(){
    	
        wrappedList = new ArrayList<T>();
        List result = getQueryResults();
        Iterator it = result.iterator();
        try {
            while (it.hasNext()){
            	Iterator<NestedTableDescriptor<?>> nestedIterator=null; 
            	if (nestedTableDescriptors!=null){
            		nestedIterator = nestedTableDescriptors.iterator();
            	}
                Object[] currentRow = (Object[])it.next();
                T entity = clazz.newInstance();
                int parameterIndex=0;
                
                for (Method classMethod: methods){
                	Object value = getValue(currentRow, parameterIndex, nestedIterator);
                    if (value!=null){
                    	try{
                    		classMethod.invoke(entity, new Object[]{value});
                    	} catch (Throwable t){
                    		t.printStackTrace();
                    		System.out.println(entity+" "+value);
                    	}
                    }
                    parameterIndex++;
                }
                wrappedList.add(entity);
            }
        } catch (IllegalAccessException e) {
        	throw new RuntimeException(e);
        } catch (InstantiationException e) {
        	throw new RuntimeException(e);
        } /*catch (InvocationTargetException e) {
        	throw new RuntimeException(e);
        } */
        
        return wrappedList;
    }

	private Object getValue(Object[] currentRow, int i, Iterator<NestedTableDescriptor<?>> nestedIterator) {
		// byte[] kan brukes som de kommer fra rammeverket
		if (parameterTypes[i] == byte[].class) {
			return getByteArray(currentRow[i]);
		}

		/*
		 * Tar det første elementet i en String[] - denne er oversatt fra en MIPSS.STRINGLIST i databasen.
		 * Grunnen til at vi bare tar med oss det første elementet er fordi denne listen alltid inneholder ett element
		 * hvor teksten er separert med " + ".
		 */
		if (currentRow[i] instanceof String[]) {
			String[] stringList = (String[]) currentRow[i];
			return stringList[0];
		}

		// Håndter nøstede tabeller
		if (currentRow[i] instanceof Object[]) {
			return createNestedObjects((Object[]) currentRow[i], nestedIterator);
		}

		if (currentRow[i] instanceof Timestamp) {
			return currentRow[i];
		}

		// Alle andre verdier
		return getInstance("" + currentRow[i], parameterTypes[i]);
	}
    /**
     * Lager en liste med objekter av den n�stede tabellen. 
     * Støtter ikke brukerdefinerte oracle objekter i første nivå. Kun lister.
     * 
     * @return
     */
	private List createNestedObjects(Object[] nestedEntities, Iterator<NestedTableDescriptor<?>> nestedIterator){
		List nestedList = new ArrayList();

		if (nestedIterator != null && nestedIterator.hasNext()) {
			NestedTableDescriptor<?> descriptor = nestedIterator.next();
			Class<?> clazz = descriptor.getJavaClass();
			try {
				for (Object entityArray : nestedEntities) {
					if (entityArray == null)
						continue;
					Object[] nestedRow = (Object[]) entityArray;
					Object nestedEntity = clazz.newInstance();
					int parameterIndex = 0;
					descriptor.initDescriptorIterator();
					for (Method nestedClassMethod : descriptor.getFieldAccessorMethods()) {
						Object value = null;
						if (nestedRow[parameterIndex] instanceof Object[]) {
							value = createNestedObject((Object[]) nestedRow[parameterIndex], descriptor.getDescriptorIterator().next());
						} else {
							value = getInstance("" + nestedRow[parameterIndex], descriptor.getParameterTypes()[parameterIndex]);
						}
						if (value != null) {
							nestedClassMethod.invoke(nestedEntity, new Object[]{value});
						}
						parameterIndex++;
					}
					nestedList.add(nestedEntity);
				}
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		}

		return nestedList;
	}
    /**
     * Lager et objekt som enten kan v�re en liste med objekter eller et komplekts objekt
     * 
     * @param entity
     * @param descriptor
     * @return
     */
	private Object createNestedObject(Object[] entity, NestedTableDescriptor<?> descriptor){
		Class<?> clazz = descriptor.getJavaClass();
		Object nestedEntity = null;
		List nestedEntities = null;
		try {
			int parameterIndex = 0;
			for (Object row : entity){
				if (row instanceof Object[]){
					Object value = createNestedObject((Object[])row, descriptor);
					if (nestedEntities==null)
						nestedEntities = new ArrayList();
					nestedEntities.add(value);
				}else{
					//hent kun ut de verdiene som er spesifisert i descriptoren.
					if (parameterIndex<descriptor.getFieldAccessorMethods().size()){
						Method nestedClassMethod = descriptor.getFieldAccessorMethods().get(parameterIndex);
						Object value = getInstance(""+entity[parameterIndex], descriptor.getParameterTypes()[parameterIndex]);
						if (value!=null){
							if (nestedEntity==null)
								nestedEntity = clazz.newInstance();
			            	nestedClassMethod.invoke(nestedEntity, new Object[]{value});
			            }
					}else{
						//ingen flere verdier i resultatsettet som er interessant ifht descriptor
						break;
					}
					parameterIndex++;
				}
			}
		} catch (Throwable e) {
			throw new RuntimeException(e);
		} 
		return nestedEntities==null?(nestedEntity==null?nestedEntities:nestedEntity):(nestedEntities);
	}
    
    protected byte[] getByteArray(Object o){
    	return (byte[])o;
    }
    
	public T getWrappedSingleResult(){
    	try{
    		Object[] v = (Object[])query.getSingleResult();
	        T entity = null;
	        Iterator<NestedTableDescriptor<?>> nestedIterator=null; 
        	if (nestedTableDescriptors!=null){
        		nestedIterator = nestedTableDescriptors.iterator();
        	}
	        try {
	            entity = clazz.newInstance();
	            int i=0;
	            for (Method m:methods){
	            	Object value = getValue(v, i, nestedIterator);
                    if (value!=null){
                    	m.invoke(entity, new Object[]{value});
                    }
                    i++;
	            }
	        } catch (IllegalAccessException e) {
	        	throw new RuntimeException(e);
	        } catch (InstantiationException e) {
	        	throw new RuntimeException(e);
	        } catch (InvocationTargetException e) {
	        	throw new RuntimeException(e);
	        }
	        return entity;
    	}catch (NoResultException e){
    		return null;
    	}
    }
    
    /**
     * Konverter en verdi til en konkret klasse. For datoer m� man sette <code>dateMask</code>
     * 
     * @param value verdien som skal konverteres
     * @param clazz klassen verdien skal konverteres til. 
     * @return et instansiert objekt av <code>clazz</code>
     */
    protected Object getInstance(String value, Class<?> clazz){
    	if (value.equals("null"))
    		return null;
        if (clazz == Integer.class)
            return Integer.valueOf(value);
        if (clazz == int.class)
        	return Integer.parseInt(value);
        if (clazz ==Long.class)
            return Long.valueOf(value);
        if (clazz ==Boolean.class){
            if (value.equals("1"))
            	return Boolean.TRUE;
        	return Boolean.valueOf(value);
        }
        if (clazz == Short.class){
        	return Short.valueOf(value);
        }
        if (clazz ==String.class){
            if (value.equals("null"))
            	return null;
        	return value;
        }
        if (clazz ==Date.class){
        	try {
	        	if (getDateFormatter()==null){
		            SimpleDateFormat sf = new SimpleDateFormat(dateMask, new Locale("no", "no"));
		            return sf.parse(value);
	        	}else{
	        		return getDateFormatter().parse(value);
	        	}
        	} catch (ParseException e) {
        		throw new RuntimeException(e);
            }
        }
        if (clazz ==Double.class)
            return Double.valueOf(value);
        if (clazz ==double.class)
        	return Double.parseDouble(value);
        return null;
    }
    
    /**
     * Henter et metodenavn til POJOen
     * @param prefix get eller set
     * @param suffix field navnet
     * @return
     */
    private String getFieldAccessor(String prefix, String suffix){
        String fieldName = prefix+ (""+suffix.charAt(0)).toUpperCase()+suffix.substring(1);
        return fieldName;
    }
    
    /**
     * Setter masken for datoformatering, default maske er: <code>DD.MM.yyyy HH:mm:ss</code>
     * @param dateMask
     */
    public void setDateMask(String dateMask){
        this.dateMask = dateMask;
    }
    
    public void setDateFormatter(DateFormat dateFormatter) {
		this.dateFormatter = dateFormatter;
	}

	public DateFormat getDateFormatter() {
		return dateFormatter;
	}

	/**
     * Hjelpeklasse for å definere felttype og feltnavn ett sted.
     * @author jorge
     */
    public static class EntityField {
    	private Class<?> type = null;
    	private String name = null;
    	public EntityField(Class<?> type, String name) {
    		this.type = type;
    		this.name = name;
    	}
    }
}
