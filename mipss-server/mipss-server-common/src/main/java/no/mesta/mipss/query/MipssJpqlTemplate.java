package no.mesta.mipss.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
 
/**
 *
 * @author Arnfinn Sandnes, Mesan AS
 */
public class MipssJpqlTemplate<E> implements JpqlTemplate<E>{
 
    private EntityManager entityManger;
 
    public MipssJpqlTemplate(EntityManager entityManger) {
        this.entityManger = entityManger;
    }
     
    @Override
    public List<E> query(String queryString, JpqlRowMapper<E> rowMapper) {
        List<E> resultatListe = new ArrayList<E>();
        Query query = entityManger.createQuery(queryString);
        List<Object[]> resultater = query.getResultList();
        for (Object[] rs : resultater) {
            resultatListe.add(rowMapper.mapRow(rs));
        }
        return resultatListe;
    }
 
    @Override
    public E queryForObject(String queryString, JpqlRowMapper<E> rowMapper) {
        E returObjekt = null;
        List<E> resultatListe = query(queryString, rowMapper);
        if (resultatListe.size() == 1) {
            returObjekt = resultatListe.get(0);
        } else if (resultatListe.size() > 1) {
            throw new RuntimeException("Query : " + queryString + " returnerer " + resultatListe.size() + " forekomster i stedet for 1.");
        }
        return returObjekt;
    }
 
    @Override
    public int update(String queryString, Map<String,Object> args) {
        int antall = 0;
        Query query = entityManger.createQuery(queryString);
        for(Map.Entry<String,Object> entry:args.entrySet()){
            query.setParameter(entry.getKey(), entry.getValue());
        }
        antall = query.executeUpdate();
        return antall;
    }
 
    @Override
    public List<E> query(String queryString, Map<String, Object> parametre, JpqlRowMapper<E> rowMapper) {
        List<E> resultatListe = new ArrayList<E>();
        Query query = entityManger.createQuery(queryString);
        for (Map.Entry<String, Object> entry : parametre.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        List<Object[]> resultater = query.getResultList();
        for (Object[] rs : resultater) {
            resultatListe.add(rowMapper.mapRow(rs));
        }
        return resultatListe;
    }
}