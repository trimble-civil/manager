package no.mesta.mipss.query;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.eclipse.persistence.internal.helper.ConversionManager;
import org.eclipse.persistence.internal.sessions.AbstractRecord;
import org.eclipse.persistence.internal.sessions.AbstractSession;
import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.queries.DatabaseQuery;
import org.eclipse.persistence.queries.QueryRedirector;
import org.eclipse.persistence.sessions.Record;
import org.eclipse.persistence.sessions.Session;

@SuppressWarnings("serial")
public class BeanRedirector<T> implements QueryRedirector{
	
	private Class<?>[] parameterTypes;
	private Constructor<T> constructor;
	private ConversionManager converter;

	/**
	 * Oppretter en ny instans av BeanRedirector
	 * @param query JPQL som skal kjøres
	 * @param clazz klassen som skal produseres
	 */
	public BeanRedirector(Query query, Class<T> clazz){
		constructor = (Constructor<T>) clazz.getConstructors()[0];
		
		parameterTypes = constructor.getParameterTypes();
		converter = ConversionManager.getDefaultManager();
		DatabaseQuery databaseQuery = JpaHelper.getDatabaseQuery(query);  
	    databaseQuery.setRedirector(this);  
	}
	
	@SuppressWarnings("unchecked")
	public Object invokeQuery(DatabaseQuery query, Record arguments, Session session) {
		List<T> resultList = new ArrayList<T>();
		List<Object[]> rows = (List<Object[]>) query.execute((AbstractSession) session, (AbstractRecord) arguments);
		for (Object[] currentRow : rows) { 
			Object[] constructorArgumentValues= new Object[parameterTypes.length];
			for (int i=0;i<currentRow.length;i++){
				Object value = currentRow[i];
				constructorArgumentValues[i] =convertObjectToInstance(value, i);
			}
			try {
				resultList.add(constructor.newInstance(constructorArgumentValues));
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		}
		return resultList;
	}
	/**
	 * Konverterer et objekt til en annen type 
	 * @param value objektet som skal konverteres
	 * @param index til klassen i parameterTypes[]
	 * @return
	 */
	private Object convertObjectToInstance(Object value, int index){
		Class<?> valueClass = parameterTypes[index];
		return converter.convertObject(value, valueClass); 
	}
}
