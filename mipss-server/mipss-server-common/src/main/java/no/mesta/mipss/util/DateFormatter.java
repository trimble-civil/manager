package no.mesta.mipss.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateFormatter {
	private static final Logger logger = LoggerFactory.getLogger(DateFormatter.class);
	/**
	 * Konverter en dato fra GMT til CET
	 * @param gmtDate
	 * @return
	 */
	public static Date convertFromGMTtoCET(Date gmtDate){
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone("CET"));
    	c.setTime(gmtDate);
    	int d = c.get(Calendar.DAY_OF_MONTH);
    	int m = c.get(Calendar.MONTH);
    	int y = c.get(Calendar.YEAR);
    	int hh = c.get(Calendar.HOUR_OF_DAY);
    	int mm = c.get(Calendar.MINUTE);
    	int ss = c.get(Calendar.SECOND);
    	int ms = c.get(Calendar.MILLISECOND);
    	Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    	cal.set(Calendar.DAY_OF_MONTH, d);
    	cal.set(Calendar.MONTH, m);
    	cal.set(Calendar.YEAR, y);
    	cal.set(Calendar.HOUR_OF_DAY, hh);
    	cal.set(Calendar.MINUTE, mm);
    	cal.set(Calendar.SECOND, ss);
    	cal.set(Calendar.MILLISECOND, ms);
		
    	return cal.getTime();
		
	}
	/**
	 * Konverter en dato fra CET til GMT
	 * @param cetDate
	 * @return
	 */
	public static Date convertFromCETtoGMT(Date cetDate){
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    	c.setTime(cetDate);
    	int d = c.get(Calendar.DAY_OF_MONTH);
    	int m = c.get(Calendar.MONTH);
    	int y = c.get(Calendar.YEAR);
    	int hh = c.get(Calendar.HOUR_OF_DAY);
    	int mm = c.get(Calendar.MINUTE);
    	int ss = c.get(Calendar.SECOND);
    	int ms = c.get(Calendar.MILLISECOND);
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, d);
    	cal.set(Calendar.MONTH, m);
    	cal.set(Calendar.YEAR, y);
    	cal.set(Calendar.HOUR_OF_DAY, hh);
    	cal.set(Calendar.MINUTE, mm);
    	cal.set(Calendar.SECOND, ss);
    	cal.set(Calendar.MILLISECOND, ms);
    	
    	return cal.getTime();
		
	}
	public static Date round(Date date, boolean first){
		if(date == null) {
			return null;
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, first?0:59);
		cal.set(Calendar.MINUTE, first?0:59);
		cal.set(Calendar.HOUR, first?0:23);
		return new Date(cal.getTimeInMillis());
	}
	public static void main(String[] args){
		Date cet = new Date();
		logger.debug("dato nå:"+cet);
		Date gmt = convertFromCETtoGMT(cet);
		logger.debug("dato som gmt:"+gmt);
		logger.debug("gmt til cet:"+convertFromGMTtoCET(gmt));
	}
}
