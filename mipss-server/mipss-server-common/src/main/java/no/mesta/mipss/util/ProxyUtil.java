package no.mesta.mipss.util;

import java.util.Properties;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.konfigparam.KonfigParam;

/**
 * Util for configuring proxy settings. The settings are stored in the <tt>proxySettings.properties</tt> file.
 * 
 */
public class ProxyUtil {
	
	private static final String HTTP_PROXYHOST = "http.proxyHost";
	private static final String HTTP_PROXYHOSTSAM = "http.proxyHostSAM";
	private static final String HTTP_PROXYPORT = "http.proxyPort";
	private static final String HTTPS_PROXYHOST = "https.proxyHost";
	private static final String HTTPS_PROXYPORT = "https.proxyPort";
	private static final String HTTP_NONPROXYHOST = "http.nonProxyHosts";
	private static final String MIPSS_SERVER = "mipss.server";
	
	public static boolean PROXY_DISABLED = true;
	
	private KonfigParam params = BeanUtil.lookup(KonfigParam.BEAN_NAME, KonfigParam.class);
	private boolean proxyEnabled;
	
	private static ProxyUtil instance;
	
	/**
	 * Enables the HTTP proxy settings as configured in the <tt>proxySettings.properties</tt> file.
	 */
	public void enableProxy() {
		if (PROXY_DISABLED)
			return;
		Properties systemSettings = System.getProperties();
		systemSettings.put(HTTP_PROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYHOST).getVerdi());
		systemSettings.put(HTTP_PROXYPORT, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYPORT).getVerdi());
		systemSettings.put(HTTPS_PROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYHOST).getVerdi());
		systemSettings.put(HTTPS_PROXYPORT, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYPORT).getVerdi());
		systemSettings.put(HTTP_NONPROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_NONPROXYHOST).getVerdi());
		System.setProperties(systemSettings);
		setProxyEnabled(true);
	}
	
	public void enableProxySAM() {
		if (PROXY_DISABLED)
			return;
		Properties systemSettings = System.getProperties();
		systemSettings.put(HTTP_PROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYHOSTSAM).getVerdi());
		systemSettings.put(HTTP_PROXYPORT, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYPORT).getVerdi());
		systemSettings.put(HTTPS_PROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYHOSTSAM).getVerdi());
		systemSettings.put(HTTPS_PROXYPORT, params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYPORT).getVerdi());
		systemSettings.put(HTTP_NONPROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_NONPROXYHOST).getVerdi());
		System.setProperties(systemSettings);
		setProxyEnabled(true);
	}

	public String getProxyHost(){
		return params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYHOST).getVerdi();
	}
	public String getProxyPort(){
		return params.hentEnForApp(MIPSS_SERVER, HTTP_PROXYPORT).getVerdi();
	}
	/**
	 * Disables HTTP proxy configuration.
	 */
	public void disableProxy() {
		Properties systemSettings = System.getProperties();
		systemSettings.put(HTTP_PROXYHOST, "");
		systemSettings.put(HTTP_PROXYPORT, "");
		systemSettings.put(HTTPS_PROXYHOST, "");
		systemSettings.put(HTTPS_PROXYPORT, "");
		systemSettings.put(HTTP_NONPROXYHOST, "");
		System.setProperties(systemSettings);
		setProxyEnabled(false);
		
	}

	/**
	 * Determines whether or not the proxy is enabled.
	 * 
	 * @return proxyEnabled - whether or not the proxy is enabled. 
	 * 
	 */
	public boolean isProxyEnabled() {
		return proxyEnabled;
	}

	/**
	 * Enables / disables the proxy.
	 */
	private void setProxyEnabled(boolean proxyEnabled) {
		this.proxyEnabled = proxyEnabled;
	}
	
	/**
	 * Returns a <tt>ProxyUtil</tt> instance.
	 */
	public static ProxyUtil getInstance() {
		if(instance == null) {
			instance = new ProxyUtil();
		}
		
		return instance;
	}

}