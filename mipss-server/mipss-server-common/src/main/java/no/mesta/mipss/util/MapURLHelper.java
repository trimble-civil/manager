package no.mesta.mipss.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * Brukes for å hente ut og manipulere URL'er og kall for å kunne hente ut map-tiles i systemet.
 * Innført i forhold til token-kall for uthenting av nye kartdata.
 * @author OLEHEL
 *
 */
public class MapURLHelper {
	
	
	 /**
     * Denne metoden kobler seg opp mot server og henter ut en token-string, som så benyttes videre for å hente ut tiles ol.
     * Kalles kun dersom token ikke eksisterer enda.
     * 
     * @param username2
     * @param password2
     * @return
     */
	public static String hentTokenString(String token, String tokenURL, String username, String password) {

		if(token.isEmpty() || token==null){
			//Creates the correct URL for the token request
			URL tokenUrl;
			URLConnection httpConnection;
			HashMap result = new HashMap();
			try {
				tokenUrl = new URL(tokenURL);
				httpConnection = (URLConnection) (tokenUrl).openConnection();
				//Adds username and password to the header
				httpConnection.setRequestProperty("username", javax.xml.bind.DatatypeConverter.printBase64Binary(username.getBytes()));
				httpConnection.setRequestProperty("password", javax.xml.bind.DatatypeConverter.printBase64Binary(password.getBytes()));
				//Reads the response from the token request
				BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
				result =  new ObjectMapper().readValue(in.readLine(), HashMap.class);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			token =(String)result.get("tokenString"); 
		}
		return token;
	}

}
