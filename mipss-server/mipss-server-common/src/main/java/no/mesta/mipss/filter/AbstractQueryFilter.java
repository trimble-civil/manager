package no.mesta.mipss.filter;

import java.io.Serializable;


import no.mesta.mipss.common.PropertyResourceBundleUtil;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Property;

public abstract class AbstractQueryFilter implements Serializable {
	private static transient PropertyResourceBundleUtil props = null;
	
	/**
	 * Returnerer feltene i bønnen som kan søkes på 
	 * @return
	 */
	public abstract String[] getSearchableFields();
	
	public String getLabelForField(String field) {
		return getProps().getGuiString(field);
	}
	
	@SuppressWarnings("unchecked")
	public boolean hasValues() {
		boolean hasValues = false;
		String[] fields = getSearchableFields();
		for(String field : fields) {
			Property p = BeanProperty.create(field);
			Object o = p.getValue(this);
			if(o != null) {
				hasValues = true; 
			}
		}
		
		return hasValues;
	}
	
    private static PropertyResourceBundleUtil getProps() {
        if(props == null) {
            props = PropertyResourceBundleUtil.getPropertyBundle("queryFilter");
        }
        return props;
    }
}
