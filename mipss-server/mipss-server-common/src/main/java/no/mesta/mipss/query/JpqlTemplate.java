package no.mesta.mipss.query;

import java.util.List;
import java.util.Map;
 
/**
 * Inspirert av JdbcTemplate fra Spring.
 * @author Arnfinn Sandnes, Mesan AS
 */
public interface JpqlTemplate<E> {
 
    /**
     * Henter en liste av objekter ut fra resultatsett av spørring uten parametre.
     * @param queryString JPQL-query.
     * @param rowMapper Implementasjon som mapper en rad i spørringen til et objekt.
     * @return En liste av objekter.
     */
    List<E> query(String queryString, JpqlRowMapper<E> rowMapper);
 
    /**
     * Henter en liste av objekter ut fra resultatsett av spørring med parametre.
     * @param queryString JPQL-query.
     * @param parametre En liste med key/value-parametre.
     * @param rowMapper Implementasjon som mapper en rad i spørringen til et objekt.
     * @return 
     */
    List<E> query(String queryString, Map<String, Object> parametre, JpqlRowMapper<E> rowMapper);
 
    /**
     * Henter ett objekter ut fra resultat av spørring.
     * @param queryString JPQL-query.
     * @param rowMapper Implementasjon som mapper en rad i spørringen til et objekt.
     * @return 
     */
    E queryForObject(String queryString, JpqlRowMapper<E> rowMapper);
 
    /**
     * Modifiser data.
     * @param sql JPQL for modifisering.
     * @param parametre En liste med key/value-parametre.
     * @return 
     */
    int update(String sql, Map<String, Object> args);
}