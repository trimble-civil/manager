package no.mesta.mipss.query;

import java.util.ArrayList;
import java.util.List;

public class AndOrBuilder {
	private static final String OR = "OR ";
	private static final String AND = "AND ";
	private static final String SPACE = " ";
	private static final String NL ="\n";
	private static final String P_START = "(";
	private static final String P_END = ")"; 
	private List<String> andOrList = new ArrayList<String>();
	
	public void add(String andOr){
		andOrList.add(andOr);
	}
	
	public String toString(){
		StringBuilder andOr = new StringBuilder();
		boolean surround = andOrList.size()>1;
		
		for (int i=0;i<andOrList.size();i++){
			if (i==0){
				andOr.append(AND);
				if (surround){
					andOr.append(P_START);
				}
			}
			else{
				andOr.append(OR);
			}
			andOr.append(andOrList.get(i));
			andOr.append(SPACE);
			andOr.append(NL);
		}
		if (surround){
			andOr.append(P_END);
		}
		return andOr.toString();
	}
}
