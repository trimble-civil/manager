package no.mesta.mipss.query;
/**
 * Interface for mapping av en rad i resultatsettet
 * av en spørring med JPQL til et java-objekt(E).
 * 
 * @author Arnfinn Sandnes, Mesan AS
 */

public interface JpqlRowMapper<E> {
	E mapRow(Object[] rs);
}