package no.mesta.mipss.query;

import java.util.ArrayList;
import java.util.List;

public class WhereAndBuilder{
	private static final String WHERE = "where ";
	private static final String AND = "AND ";
	private static final String SPACE = " ";
	private static final String NL ="\n";
	private List<String> whereList = new ArrayList<String>();
	public void addWhereAnd(String whereOrAnd){
		whereList.add(whereOrAnd);
	}
	
	public String toString(){
		StringBuilder whereAnd = new StringBuilder();
		for (int i=0;i<whereList.size();i++){
			if (i==0)
				whereAnd.append(WHERE);
			else
				whereAnd.append(AND);
			whereAnd.append(whereList.get(i));
			whereAnd.append(SPACE);
			whereAnd.append(NL);
		}
		return whereAnd.toString();
	}
}