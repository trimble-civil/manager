package no.mesta.mipss.util;

import java.io.Serializable;

import org.apache.commons.lang.SerializationUtils;

public class DetachmentHelper<T> {

	public static Object detach(Object entity){
		return SerializationUtils.clone((Serializable)entity);
		
	}
}
