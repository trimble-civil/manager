package no.mesta.mipss.service.epost;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssEpostEntity;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(name=EpostService.BEAN_NAME, mappedName="ejb/"+EpostService.BEAN_NAME)
public class EpostServiceBean implements EpostService{
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<MipssEpostEntity> getEpostList(Long kontraktId) {
		
		// henter alle leverandorer knyttet til kontrakt
		StringBuilder sb= new StringBuilder();
		sb.append("Select k from KontraktLeverandor k ");
		sb.append("where k.driftkontrakt.id=:kontraktId ");
		sb.append("and k.fraDato<=:dato and k.tilDato>=:dato ");
		sb.append("or k.driftkontrakt.id=:kontraktId ");
		sb.append("and k.fraDato<=:dato and k.tilDato is null");
		Query q = em.createQuery(sb.toString());
		q.setParameter("kontraktId", kontraktId);
		q.setParameter("dato", Clock.now());
		List<KontraktLeverandor> leverandorList = q.getResultList();
		
		// henter alle kjoretoyeiere som har kjoretoy knyttet til kontrakt
		sb = new StringBuilder();
		sb.append("select distinct f.kjoretoyeier from Kjoretoy f ");
		sb.append("join f.kontraktKjoretoyList kk ");
		sb.append("where kk.kontraktId=:kontraktId ");
		sb.append("and f.kjoretoyeier is not null");
		q = em.createQuery(sb.toString());
		q.setParameter("kontraktId", kontraktId);
		List<Kjoretoyeier> eierList = q.getResultList();
		
		/*
		select distinct ke.navn from kjoretoyeier ke
		left join kjoretoy k on ke.id = k.eier_id
		where k.leverandor_nr=36531
		*/
		
		List<MipssEpostEntity> sorted = new ArrayList<MipssEpostEntity>();
		for (KontraktLeverandor kl:leverandorList){
			Leverandor l = kl.getLeverandor();
			sorted.add(kl);
			sb = new StringBuilder();
			//henter navn til alle kjoretoyeiere som er knyttet til leverandoren
			sb.append("select distinct ke.navn from Kjoretoyeier ke ");
			sb.append("join ke.kjoretoyList kl ");
			sb.append("where kl.leverandor.nr=:levNr");
			
			q = em.createQuery(sb.toString());
			q.setParameter("levNr", l.getNr());
			List<String> keList = q.getResultList();
			log.debug("Hentet:"+keList.size()+" kjoretoyeiere for leverandor:"+kl.getNavn()+" "+l.getNr());
			for (Kjoretoyeier ke:eierList){
				if (keList.contains(ke.getNavn())){
					sorted.add(ke);
					log.debug(ke.getNavn()+" lagt til");
				}
			}
		}
		
		return sorted;
	}

	public void saveEpostList(List<MipssEpostEntity> list) {
		for (MipssEpostEntity e:list){
			MipssEpostEntity merged = em.merge(e);
			em.persist(merged);
		}
	}

	public void sendEkstraDagsrapport(Date date, Long kontraktId, List<MipssEpostEntity> ekstraList) {
		List<String> kjoretoyeiere = null;
		List<String> leverandorer = null;
		
		if (ekstraList!=null){
			for (MipssEpostEntity e:ekstraList){
				if (e instanceof KontraktLeverandor){
					if (leverandorer == null)
						leverandorer = new ArrayList<String>();
					leverandorer.add(((KontraktLeverandor)e).getLeverandorNr());
				}
				if (e instanceof Kjoretoyeier){
					if (kjoretoyeiere == null)
						kjoretoyeiere = new ArrayList<String>();
					kjoretoyeiere.add(((Kjoretoyeier)e).getId().toString());
				}
			}
		}
		
		String ekstraLeverandorer = StringUtils.join(leverandorer, "|");
		String ekstraKjoretoyeiere = StringUtils.join(kjoretoyeiere, "|");
		
		String dato = MipssDateFormatter.formatDate(date, MipssDateFormatter.DATE_FORMAT);
		
		StringBuilder sb = new StringBuilder();
		sb.append("select d.epostRodetypeId from Driftkontrakt d ");
		sb.append("where d.id=:kontraktId ");
		Query q = em.createQuery(sb.toString());
		q.setParameter("kontraktId", kontraktId);
		Long epostRodetypeId = (Long)q.getSingleResult();
		
		log.debug("sender e-post for kontrakt:"+kontraktId+" rodetypeid="+epostRodetypeId+" dato:"+dato+" levs:"+ekstraLeverandorer+" eiere:"+ekstraKjoretoyeiere);
		
		String qry = "{ call epost_pk.send(?1, ?2, null, to_date(?3,'DD.MM.YYYY'), to_date(?3,'DD.MM.YYYY'), ?4, ?5) }";
		q = em.createNativeQuery(qry);
		q.setParameter(1, kontraktId);
		q.setParameter(2, epostRodetypeId);
		q.setParameter(3, dato);
		q.setParameter(4, ekstraLeverandorer);
		q.setParameter(5, ekstraKjoretoyeiere);
		q.executeUpdate();
	}

	
}
/**
sb = new StringBuilder();
sb.append("select distinct ke.navn from kjoretoyeier ke ");
sb.append("left join kjoretoy k on ke.id = k.eier_id ");
sb.append("where k.leverandor_nr=?1");
q = em.createNativeQuery(sb.toString());
q.setParameter(1, l.getNr());
List<String> keList = getKjoretoyEierFromQuery(q);
*/
