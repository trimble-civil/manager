package no.mesta.mipss.service.epost;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.MipssEpostEntity;

@Remote
public interface EpostService {
	public static final String BEAN_NAME = "EpostService";
	
	/**
	 * Henter epostlisten for en kontrakt.
	 * @param kontraktId
	 * @return
	 */
	public List<MipssEpostEntity> getEpostList(Long kontraktId);
	
	/**
	 * Lagrer entitetene i listen. Listen skal kun inneholde
	 * de MipssEpostEntitetene som er endret!. 
	 * @param list
	 */
	public void saveEpostList(List<MipssEpostEntity> list);
	
	/**
	 * Sender en ekstra dagsrapport for kontrakten
	 * @param date Datoen for datagrunnlaget til rapporten
	 * @param kontraktId id til kontrakten
	 */
	public void sendEkstraDagsrapport(Date date, Long kontraktId, List<MipssEpostEntity> ekstraList);
}
