package no.mesta.mipss.produksjonsrapport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Leverandor;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.excel.LeverandorRapport;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class Testrapport {

	public static void main(String[] args) throws IOException {
		
		new LeverandorRapport(Leverandor.getTestData(), null).finalizeAndOpen();
		
//		Map<Long, List<Rapp1>> listePrLeverandor = createListePrLeverandor(testData);
//		
//		List<Leverandor> levList = new ArrayList<Leverandor>();
//		Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
//		Map<Long, R12Stroprodukt> stroproduktMap = new HashMap<Long, R12Stroprodukt>();
//		for (Long key:listePrLeverandor.keySet()){
//			List<Rapp1> list = listePrLeverandor.get(key);
//			Leverandor l = new Leverandor(list);
//			levList.add(l);
//			//opprett et map av alle prodtyper
//			List<R12Prodtype> prodtypeList = l.getProdtyper();
//			for (R12Prodtype p:prodtypeList){
//				R12Prodtype r12Prodtype = prodtypeMap.get(p.getId());
//				if (r12Prodtype==null){	//klon den og gjør klar for header og summering
//					R12Prodtype pt = createProdtypePlaceholder(p);
//					prodtypeMap.put(pt.getId(), pt);
//				}
//			}
//			//opprett et map av alle strøprodukter
//			List<R12Stroprodukt> stroproduktListe = l.getStroprodukter();
//			for (R12Stroprodukt s:stroproduktListe){
//				R12Stroprodukt stro = stroproduktMap.get(s.getId());
//				if (stro==null){//klon den og gjør klar for header og summering
//					R12Stroprodukt st = createStroproduktPlaceholder(s);
//					stroproduktMap.put(st.getId(), st);
//				}
//			}
//		}
//		
//		List<R12Prodtype> alleProdtyper = extractAndSortProdtyper(prodtypeMap);
//		List<R12Stroprodukt> alleStroprodukter = extractAndSortStroprodukter(stroproduktMap);
//		
//		//summer og organiser prodtypene og stroproduktene slik at de ligger i samme rekkefølge for samtlige leverandører
//		for (Leverandor l:levList){
//			l.organizeProdtyper(alleProdtyper);
//			l.organizeStroprodukt(alleStroprodukter);
//		}
//		
//		Map<String, List> map = new HashMap<String, List>();
//		map.put("records", levList);
//		map.put("prodtyper", alleProdtyper);
//		map.put("stroprodukter", alleStroprodukter);
//		File tempFile = File.createTempFile("testrapp", ".xls");
//		HSSFWorkbook wb = createWorkbook(tempFile, getExcelTemplate("leverandor.xls"));
////		
//		XLSTransformer t= new XLSTransformer();
//		t.transformWorkbook(wb, map);
//		
//		int startRow = 10;
//		int startCol = 6;
//		
//		
//		closeWorkbook(wb, tempFile);
//		Desktop desktop = Desktop.getDesktop();
//		if(desktop.isSupported(Desktop.Action.OPEN)) {
//			try {
//				desktop.open(tempFile);
//			} catch (IOException e) {
//				throw new IllegalStateException(e);
//			}
//		}
		
	}

	/**
	 * Trekker ut alle strøproduktene fra sMap og sorterer dem i hht seq
	 * @param stroproduktMap
	 * @return
	 */
	private static List<R12Stroprodukt> extractAndSortStroprodukter(Map<Long, R12Stroprodukt> stroproduktMap) {
		List<R12Stroprodukt> alleStroprodukter = new ArrayList<R12Stroprodukt>();
		for (Long key:stroproduktMap.keySet()){
			R12Stroprodukt st = stroproduktMap.get(key);
			alleStroprodukter.add(st);
		}
		
		Collections.sort(alleStroprodukter,  new Comparator<R12Stroprodukt>(){
			public int compare(R12Stroprodukt o1, R12Stroprodukt o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}});
		return alleStroprodukter;
	}
	
	/**
	 * Trekker ut alle prodtypene fra sMap og sorterer dem i hht seq
	 * @param prodtypeMap
	 * @return
	 */
	private static List<R12Prodtype> extractAndSortProdtyper(Map<Long, R12Prodtype> prodtypeMap) {
		List<R12Prodtype> alleProdtyper = new ArrayList<R12Prodtype>();
		for (Long key:prodtypeMap.keySet()){
			R12Prodtype pt = prodtypeMap.get(key);
			alleProdtyper.add(pt);
		}
		Collections.sort(alleProdtyper,  new Comparator<R12Prodtype>(){
			public int compare(R12Prodtype o1, R12Prodtype o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}});
		return alleProdtyper;
	}

	/**
	 * Lager et stroprodukt 
	 * @param s
	 * @return
	 */
	private static R12Stroprodukt createStroproduktPlaceholder(R12Stroprodukt s) {
		R12Stroprodukt st = new R12Stroprodukt();
		st.setId(s.getId());
		st.setSeq(s.getSeq());
		st.setHeader(s.getHeader());
		return st;
	}

	private static R12Prodtype createProdtypePlaceholder(R12Prodtype p) {
		R12Prodtype pt = new R12Prodtype();
		pt.setId(p.getId());
		pt.setSeq(p.getSeq());
		pt.setHeader(p.getHeader());
		return pt;
	}
	
	public static Map<Long, List<Rapp1>> createListePrLeverandor(List<Rapp1> data){
		Map<Long, List<Rapp1>> listePrLeverandor = new HashMap<Long, List<Rapp1>>();
		for (Rapp1 r:data){
			Long leverandorNr = r.getLeverandorNr();
			List<Rapp1> list = listePrLeverandor.get(leverandorNr);
			if (list==null){
				list = new ArrayList<Rapp1>();
				listePrLeverandor.put(leverandorNr, list);
			}
			list.add(r);
		}
		return listePrLeverandor;
	}
	
	public static InputStream getExcelTemplate(String fileName){
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(fileName);
		if (url != null) {
			try{
			URLConnection connection = url.openConnection();
			if (connection != null) {
			    connection.setUseCaches(false);
			    return connection.getInputStream();
			}
			} catch (Exception e){
				throw new RuntimeException(e);
			}
		}
		return null;
	}
	
	public static HSSFWorkbook createWorkbook(File tempFile, InputStream is){
		try {
			POIFSFileSystem fs = new POIFSFileSystem(is);
			HSSFWorkbook workbook = new  HSSFWorkbook(fs, true);
			return workbook;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void closeWorkbook(HSSFWorkbook workbook, File tempFile) {
		try {
			FileOutputStream dos = new FileOutputStream(tempFile);
			workbook.write(dos);
			dos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
