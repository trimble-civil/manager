package no.mesta.mipss.produksjonsrapport.values;

import java.io.File;
import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.rapportfunksjoner.Veirapp;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.Veirapport;
import no.mesta.mipss.produksjonsrapport.query.VeirapportQuery;
import no.mesta.mipss.produksjonsrapport.web.ExcelReportHSSFHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VeirapportHandler extends ExcelReportHSSFHandler{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");

	private final EntityManager em;

	public ProdrapportParams params;
    
	public VeirapportHandler(final File file, EntityManager em){
		super(file);
		this.em = em;
	}
	
	public final byte[] createReport(ProdrapportParams params) throws Exception{
		this.params = params;
		params.setFrekvens(false);
		params.setRapportType(ProdrapportParams.RapportType.VEI.toString());
		List<Veirapp> data = new VeirapportQuery(params, em).consumeResults();
		if (data==null||data.isEmpty()){
        	return null;
        }
		Veirapport rapp = new Veirapport(data, params);
		return rapp.toByteArray();
	}
}
