package no.mesta.mipss.produksjonsrapport.excel;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.MipssDateFormatter;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

public class ExcelPOIHelperHSSF {

	private File tempFile;
	private String dateFormat = MipssDateFormatter.LONG_DATE_TIME_FORMAT;
	private Styles styles;
	private Map<String,List<HSSFCellStyle>> cellStyleCache = new HashMap<String, List<HSSFCellStyle>>();
	
	public ExcelPOIHelperHSSF(){
		
	}
	public ExcelPOIHelperHSSF(HSSFWorkbook workbook){
		styles = new Styles(workbook);
	}
	
	public InputStream getExcelTemplate(String fileName){
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(fileName);
		if (url != null) {
			try{
			URLConnection connection = url.openConnection();
			if (connection != null) {
			    connection.setUseCaches(false);
			    return connection.getInputStream();
			}
			} catch (Exception e){
				throw new RuntimeException(e);
			}
		}
		return null;
	}
	
	public HSSFWorkbook createWorkbook(File tempFile, InputStream is){
		this.tempFile = tempFile;
		try {
//			POIFSFileSystem fs = new POIFSFileSystem(is);
			HSSFWorkbook workbook = new  HSSFWorkbook(is);
			styles = new Styles(workbook);
			return workbook;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void closeWorkbook(Workbook workbook) {
		try {
			FileOutputStream dos = new FileOutputStream(tempFile);
			workbook.write(dos);
			dos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public File getExcelFile(){
		return tempFile;
	}
	public Sheet getSheet(int index, Workbook workbook){
		
		return workbook.getSheetAt(index);
	}
	public HSSFSheet createSheet(String sheetName, HSSFWorkbook workbook){
		return workbook.createSheet(sheetName);
	}
	
	public void setCellStyle(HSSFCellStyle style, HSSFRow row, int col){
		HSSFCell cell = row.getCell(col);
		if (cell==null)
			cell = row.createCell(col);
		cell.setCellStyle(style);
	}
	/**
	 * Setter samme excel-style på alle cellene i en CellRangeAddress, sammenslåtte celler får ikke nødvendigvis samme stil. Bruk derfor denne
	 * metoden for å sette samme stil på de sammenslåtte cellene.
	 * @param sheet
	 * @param region
	 * @param style
	 */
	public void styleMergedRegion(HSSFSheet sheet, CellRangeAddress region, HSSFCellStyle style){
		int col = region.getFirstColumn();
		int row = region.getFirstRow();
		int lastCol = region.getLastColumn();
		int lastRow = region.getLastRow();
		int colSpan = lastCol-col;
		int rowSpan = lastRow-row;
		for (int i=0;i<=colSpan;i++){
			for (int j=0;j<=rowSpan;j++){
				HSSFRow r = getRow(sheet, row+j);
				setCellStyle(style, r, col+i);
			}
		}
	}
	
	public void setDefaultBorders(Row row, int lastCol){
		for (int i=0;i<=lastCol;i++){
			Cell cell = row.getCell((short)i);
			if (cell==null){
				cell = row.createCell((short)i);
				cell.setCellStyle(styles.getTextStyle(true));
			}
			cell.getCellStyle().setBorderLeft(HSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setBorderTop(HSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setBorderRight(HSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setBorderBottom(HSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setLeftBorderColor(HSSFColor.GREY_40_PERCENT.index);
			cell.getCellStyle().setTopBorderColor(HSSFColor.GREY_40_PERCENT.index);
			cell.getCellStyle().setRightBorderColor(HSSFColor.GREY_40_PERCENT.index);
			cell.getCellStyle().setBottomBorderColor(HSSFColor.GREY_40_PERCENT.index);
		}
	}
	public void addBorder(HSSFRow row, int col){
		HSSFCell cell = row.getCell(col);
		cell.getCellStyle().setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setBorderTop(HSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setBorderRight(HSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setBorderBottom(HSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setLeftBorderColor(HSSFColor.BLACK.index);
		cell.getCellStyle().setTopBorderColor(HSSFColor.BLACK.index);
		cell.getCellStyle().setRightBorderColor(HSSFColor.BLACK.index);
		cell.getCellStyle().setBottomBorderColor(HSSFColor.BLACK.index);
	}
	public int writeDouble(HSSFRow row, int col, Double data, HSSFCellStyle style){
		if (style==null){
			return writeData(row, col, data);
		}
		HSSFCell cell = row.createCell((short)col);
		cell.setCellStyle(styles.getDecimalStyleShort(false));
		cell.setCellValue((Double)data);
		return computeNewColWidth(0, data);
	}
	
	public int writeData(Row row, int col, Object data, boolean wrapText){
		int colWidth = writeData(row, col, data);
		Cell cell = row.getCell((short) col);
		if (cell!=null)
			cell.getCellStyle().setWrapText(true);
		return colWidth;
	}
	
	public void setCellStyle(HSSFRow row, int col,HSSFCellStyle style){
		HSSFCell cell = row.getCell(col);
		cell.setCellStyle(style);
		
	}
	
	public int writeDataBordered(HSSFRow row, int col, Object data){
		int colWidth = 0;
		if (data!=null){
			HSSFCell cell = row.createCell((short) col);
			Class<?> clazz = data.getClass();
			
			if (clazz.equals(String.class)){
				cell.setCellStyle(styles.getTextStyle(true));
				cell.setCellValue(new HSSFRichTextString(data.toString()));
			}else if (clazz.equals(Long.class)){
				cell.setCellStyle(styles.getNumStyle(true));
				cell.setCellValue((Long)data);
			}else if (clazz.equals(Double.class)){
				cell.setCellStyle(styles.getDecimalStyle(true));
				cell.setCellValue((Double)data);
			}else if (clazz.equals(Integer.class)){
				cell.setCellStyle(styles.getNumStyle(true));
				cell.setCellValue((Integer)data);
			}else if (clazz.equals(Date.class)){
				cell.setCellStyle(styles.getDatoStyle(dateFormat, true));
				cell.setCellValue((Date)data);
			}else if (clazz.equals(Boolean.class)){
				cell.setCellStyle(styles.getNumStyle(true));
				Boolean value = (Boolean) data;
				Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
				cell.setCellValue(number);
			}else{
				try{
					Integer value = Integer.parseInt(data.toString());
					cell.setCellStyle(styles.getNumStyle(true));
					cell.setCellValue(value);
				}catch (NumberFormatException e){
					try{
						Double value = Double.parseDouble(data.toString());
						cell.setCellStyle(styles.getDecimalStyle(true));
						cell.setCellValue(value);
						
					}catch (NumberFormatException ex){
						ex.printStackTrace();
						cell.setCellStyle(styles.getTextStyle(true));
						cell.setCellValue(new HSSFRichTextString(data.toString()));
					}
				}
				
			}
			
			colWidth = computeNewColWidth(colWidth, data);
		}
		return colWidth;
	}
	public int writeData(Row row, int col, Object data){
		int colWidth = 0;
		
		if (data!=null){
			Cell cell = row.createCell((short) col);
			Class<?> clazz = data.getClass();
			if (clazz.equals(String.class)){
				cell.setCellStyle(styles.getTextStyle(false));
				cell.setCellValue(new HSSFRichTextString(data.toString()));
			}else if (clazz.equals(Long.class)){
				cell.setCellStyle(styles.getNumStyle(false));
				cell.setCellValue((Long)data);
			}else if (clazz.equals(Double.class)){
				cell.setCellStyle(styles.getDecimalStyle(false));
				cell.setCellValue((Double)data);
			}else if (clazz.equals(Integer.class)){
				cell.setCellStyle(styles.getNumStyle(false));
				cell.setCellValue((Integer)data);
			}else if (clazz.equals(Date.class)){
				cell.setCellStyle(styles.getDatoStyle(dateFormat, false));
				cell.setCellValue((Date)data);
			}else if (clazz.equals(Timestamp.class)){
				cell.setCellStyle(styles.getDatoStyle(dateFormat, false));
				cell.setCellValue((Date)data);
			}else if (clazz.equals(Boolean.class)){
				cell.setCellStyle(styles.getNumStyle(false));
				Boolean value = (Boolean) data;
				Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
				cell.setCellValue(number);
			}else{
				try{
					Integer value = Integer.parseInt(data.toString());
					cell.setCellStyle(styles.getNumStyle(false));
					cell.setCellValue(value);
				}catch (NumberFormatException e){
					try{
						Double value = Double.parseDouble(data.toString());
						cell.setCellStyle(styles.getDecimalStyle(false));
						cell.setCellValue(value);
						
					}catch (NumberFormatException ex){
						ex.printStackTrace();
						cell.setCellStyle(styles.getTextStyle(false));
						cell.setCellValue(new HSSFRichTextString(data.toString()));
					}
				}
				
			}
			
			colWidth = computeNewColWidth(colWidth, data);
		}
		return colWidth;
	}
	
	private int computeNewColWidth(int colWidth, Object data) {
		int width = 0;
		if (data instanceof String) {
			width = ((String) data).length();
			width = width > 200 ? 200 : width;
		} else if (data instanceof Long) {
			width = String.valueOf((Long) data).length();
		} else if (data instanceof Double) {
			width = String.valueOf((Double) data).length();
		} else if (data instanceof Integer) {
			width = String.valueOf((Integer) data).length();
		} else if (data instanceof Date) {
			width = MipssDateFormatter.SHORT_DATE_TIME_FORMAT.length();
		} else if (data instanceof Boolean) {
			width = 2;
		}

		return Math.max(colWidth, width);
	}
	public HSSFRow createRow(Sheet sheet, int row){
		return (HSSFRow)sheet.createRow(row);
	}
	public HSSFRow getRow(Sheet sheet, int row){
		HSSFRow xrow = (HSSFRow)sheet.getRow(row);
		if (xrow==null)
			xrow = (HSSFRow)sheet.createRow(row);
		return xrow;
	}
	/**
	 * Legger til en celle i arket
	 * @param sheet
	 * @param cell
	 */
	public HSSFCell createCell(HSSFRow row, int column) {
		
		return row.createCell((short)column);
			
	}

	
	public CellRangeAddress mergeCells(HSSFSheet sheet, int colFrom, int colTo, int rowFrom, int rowTo){
		CellRangeAddress region = new CellRangeAddress(rowFrom, rowTo, colFrom, colTo);
		sheet.addMergedRegion(region);
		return region;
	}
	public void setDateFormat(String dateFormat){
		this.dateFormat = dateFormat;
	}
	
	public void setCellValue(HSSFCell cell, String value){
		cell.setCellValue(new HSSFRichTextString(value));
	}
	public void createLinkCell(HSSFRow row, int column, String title, String target, boolean border){
		HSSFCell linkCell = row.createCell((short)column);
		linkCell.setCellStyle(styles.getHyperlinkStyle(border));
	    String formula = "HYPERLINK(" + target + ", " + title + ")";
	    linkCell.setCellFormula(formula);
	}
	public void linkify(HSSFCell cell, String title, String target){
		cell.setCellStyle(styles.getHyperlinkStyle(false));
		String formula = "HYPERLINK(" + target + ", " + title + ")";
	    cell.setCellFormula(formula);
	}
	
	public Styles getStyles(){
		return styles;
	}
	
	public HSSFCellStyle getTitleStyle(boolean border){
		return styles.getTextStyle(border);
	}
	public HSSFCellStyle getDecimalStyleShort(boolean border){
		return styles.getDecimalStyleShort(border);
	}
	public HSSFCellStyle getDecimalStyle(boolean border){
		return styles.getDecimalStyle(border);
	}
	public HSSFCellStyle getHeaderStyle(boolean border){
		return styles.getHeaderStyle(border);
	}
	public HSSFCellStyle getDatoStyle(String dateFormat, boolean border){
		return styles.getDatoStyle(dateFormat, border);
	}
	public HSSFCellStyle getDatoStyleSum(String dateFormat){
		return styles.getDatoStyleSum(dateFormat);
	}
	public HSSFCellStyle getDecimalStyleSum(){
		return styles.getDecimalStyleSum();
	}
	public HSSFCellStyle getTextStyleSum(){
		return styles.getTextStyleSum();
	}
	
	public HSSFCellStyle getHeaderStyleLeft(){
		return styles.getHeaderStyleLeft();
	}
	
	private class Styles{
		public  HSSFCellStyle titleStyle;
		public  HSSFCellStyle hyperlinkStyle;
		public  HSSFCellStyle textStyle;
		public  HSSFCellStyle textStyleSum;
		public  HSSFCellStyle numStyle;
		public  HSSFCellStyle decimalStyle;
		public  HSSFCellStyle decimalStyleSum;
		public  HSSFCellStyle decimalStyleShort;
		public  HSSFCellStyle headerStyle;
		public  HSSFCellStyle headerStyleLeft;
		
		private final HSSFWorkbook workbook;
		private HSSFFont fontText;
		private HSSFCellStyle textStyleBorder;
		private HSSFCellStyle numStyleBorder;
		private HSSFCellStyle decimalStyleBorder;
		private HSSFCellStyle decimalStyleShortBorder;
		private HSSFCellStyle hyperlinkStyleBorder;
		private HSSFCellStyle headerStyleBorder;
		private Color mestaBlue = new Color(0,0,153);
		public Styles(HSSFWorkbook workbook){
	        this.workbook = workbook;
			fontText = workbook.createFont();
	        fontText.setFontHeightInPoints((short)10);
	        fontText.setFontName("Arial");
	        fontText.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
	        fontText.setColor(IndexedColors.BLACK.index);
	        
	        HSSFFont fontNumber = workbook.createFont();
	        fontNumber.setFontHeightInPoints((short)10);
	        fontNumber.setFontName("Arial");
	        fontNumber.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        fontNumber.setColor(IndexedColors.BLACK.index);
	        
	        HSSFFont hfontReport = workbook.createFont();
	        hfontReport.setFontHeightInPoints((short)14);
	        hfontReport.setFontName("Arial");
	        hfontReport.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        hfontReport.setColor(IndexedColors.BLACK.index);
	        
	        HSSFFont fontHyperlink = workbook.createFont();
	        fontHyperlink.setFontHeightInPoints((short)10);
	        fontHyperlink.setFontName("Arial");
	        fontHyperlink.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
	        fontHyperlink.setUnderline(HSSFFont.U_SINGLE);
	        fontHyperlink.setColor(IndexedColors.BLUE.index);
	        
	        HSSFFont fontHeader = workbook.createFont();
	        fontHeader.setFontHeightInPoints((short)10);
	        fontHeader.setFontName("Arial");
	        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        fontHeader.setColor(IndexedColors.WHITE.index);
	        
			textStyle = getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, false, 
					HSSFColor.WHITE.index, 
					HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
			
			numStyle = getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_LEFT, false, 
					HSSFColor.WHITE.index, 
	                HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
			
			decimalStyle = getCellStyle("0.000", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
					false, HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
			decimalStyleSum = getCellStyle("0.000", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
					false, HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_DOUBLE, 
                    HSSFCellStyle.BORDER_THICK, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
			textStyleSum = getCellStyle("", fontNumber, HSSFCellStyle.ALIGN_LEFT, 
					false, HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_DOUBLE, 
                    HSSFCellStyle.BORDER_THICK, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
			decimalStyleShort = getCellStyle("0.0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
					false, HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
			
	        hyperlinkStyle = getCellStyle("", fontHyperlink, HSSFCellStyle.ALIGN_LEFT, false, 
	        		HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
	        
	        titleStyle = getReportHeadingStyle(hfontReport, HSSFCellStyle.ALIGN_LEFT);

	        headerStyle = getCellStyle("", fontHeader, HSSFCellStyle.ALIGN_CENTER, true, 
	        		HSSFColor.BLUE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
	        
	        headerStyleLeft = getCellStyle("", fontHeader, HSSFCellStyle.ALIGN_LEFT, true, 
	        		HSSFColor.BLUE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,false, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE, 
                    HSSFCellStyle.BORDER_NONE);
	        
	        
	        textStyleBorder = getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, false, 
	        		HSSFColor.WHITE.index, 
					HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR);
			
			numStyleBorder = getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_LEFT, false, 
					HSSFColor.WHITE.index, 
	                HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR);
			
			decimalStyleBorder = getCellStyle("0.000", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
					false, HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR);
			
			decimalStyleShortBorder = getCellStyle("0.0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
					false, HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR);
			
	        hyperlinkStyleBorder = getCellStyle("", fontHyperlink, HSSFCellStyle.ALIGN_LEFT, false, 
	        		HSSFColor.WHITE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR, 
                    HSSFCellStyle.BORDER_HAIR);
	        
	        headerStyleBorder = getCellStyle("", fontHeader, HSSFCellStyle.ALIGN_CENTER, true, 
	        		HSSFColor.BLUE.index, 
                    HSSFCellStyle.SOLID_FOREGROUND,true, 
                    HSSFCellStyle.BORDER_THIN, 
                    HSSFCellStyle.BORDER_THIN, 
                    HSSFCellStyle.BORDER_THIN, 
                    HSSFCellStyle.BORDER_THIN);
	        
	        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	        headerStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
//	        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//	        headerStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
	        headerStyle.setLeftBorderColor(HSSFColor.WHITE.index);
	        headerStyle.setRightBorderColor(HSSFColor.WHITE.index);
//	        headerStyle.setBottomBorderColor(new HSSFColor(Color.white));
//	        headerStyle.setTopBorderColor(new HSSFColor(Color.white));
	        
	        headerStyleLeft.setBottomBorderColor(HSSFColor.WHITE.index);
	        headerStyleLeft.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		}
		public HSSFCellStyle getTextStyleSum() {
			return textStyleSum;
		}
		public HSSFCellStyle getHeaderStyleLeft() {
			return headerStyleLeft;
		}
		public HSSFCellStyle getTextStyle(boolean border) {
			if (border)
				return textStyleBorder;
			return textStyle;
		}
		public HSSFCellStyle getNumStyle(boolean border) {
			if (border)
				return numStyleBorder;
			return numStyle;
		}
		public HSSFCellStyle getTitleStyle() {
			return titleStyle;
		}
		public HSSFCellStyle getHyperlinkStyle(boolean border){
			if (border)
				return hyperlinkStyleBorder;
			return hyperlinkStyle;
		}
		public HSSFCellStyle getDecimalStyleShort(boolean border){
			if (border)
				return decimalStyleShortBorder;
			return decimalStyleShort;
		}
		public HSSFCellStyle getHeaderStyle(boolean border){
			if (border)
				return headerStyleBorder;
			return headerStyle;
		}
		Map<String, HSSFCellStyle> datoStyleCache = new HashMap<String, HSSFCellStyle>();
		Map<String, HSSFCellStyle> datoStyleCacheBorder = new HashMap<String, HSSFCellStyle>();
		Map<String, HSSFCellStyle> datoStyleCacheSum = new HashMap<String, HSSFCellStyle>();
		
		public HSSFCellStyle getDatoStyle(String dateFormat, boolean border) {
			if (border){
				HSSFCellStyle s = datoStyleCacheBorder.get(dateFormat);
				if (s==null){
					s =getCellStyle(dateFormat, fontText, HSSFCellStyle.ALIGN_LEFT, 
		                    false, HSSFColor.WHITE.index, 
		                    HSSFCellStyle.SOLID_FOREGROUND, true, 
		                    HSSFCellStyle.BORDER_HAIR, 
	                        HSSFCellStyle.BORDER_HAIR, 
	                        HSSFCellStyle.BORDER_HAIR, 
	                        HSSFCellStyle.BORDER_HAIR);
					datoStyleCacheBorder.put(dateFormat, s);
				}
				return s;
			}
			HSSFCellStyle s = datoStyleCache.get(dateFormat);
			if (s==null){
				s =getCellStyle(dateFormat, fontText, HSSFCellStyle.ALIGN_LEFT, 
	                    false, HSSFColor.WHITE.index, 
	                    HSSFCellStyle.SOLID_FOREGROUND, false, 
	                    HSSFCellStyle.BORDER_NONE, 
                        HSSFCellStyle.BORDER_NONE, 
                        HSSFCellStyle.BORDER_NONE, 
                        HSSFCellStyle.BORDER_NONE);
				datoStyleCache.put(dateFormat, s);
			}
			return s;
		}
		public HSSFCellStyle getDatoStyleSum(String dateFormat) {
			HSSFCellStyle s = datoStyleCacheSum.get(dateFormat);
			if (s==null){
				s =getCellStyle(dateFormat, fontText, HSSFCellStyle.ALIGN_LEFT, 
	                    false, HSSFColor.WHITE.index, 
	                    HSSFCellStyle.SOLID_FOREGROUND, true, 
	                    HSSFCellStyle.BORDER_DOUBLE, 
                        HSSFCellStyle.BORDER_THICK, 
                        HSSFCellStyle.BORDER_NONE, 
                        HSSFCellStyle.BORDER_NONE);
				datoStyleCacheSum.put(dateFormat, s);
			}
			return s;
		}
		public HSSFCellStyle getDecimalStyle(boolean border) {
			if (border)
				return decimalStyleBorder;
			return decimalStyle;
		}
		public HSSFCellStyle getDecimalStyleSum() {
			return decimalStyleSum;
		}
		 
		
		private final HSSFCellStyle getCellStyle(String dataFormat, HSSFFont font, short alignment, boolean fillColor, short forgroundColor, short fillPattern,
				boolean border, 
				short borderBottom, 
                short borderTop, 
                short borderLeft, 
                short borderRight) {
			
			HSSFCellStyle cellStyle = workbook.createCellStyle();
//			cellStyle.setFillForegroundColor(new HSSFColor(Color.black));
			cellStyle.setFont(font);
			cellStyle.setAlignment(alignment);
			if (fillColor) {
				cellStyle.setFillForegroundColor(forgroundColor);
				cellStyle.setFillPattern(fillPattern);
			}
			
			if (dataFormat.length() > 0) {
				HSSFDataFormat cellFormat = workbook.createDataFormat();
				cellStyle.setDataFormat(cellFormat.getFormat(dataFormat));
			}
			if (border) {
	            cellStyle.setBorderBottom(borderBottom);
	            cellStyle.setBorderLeft(borderLeft);
	            cellStyle.setBorderRight(borderRight);
	            cellStyle.setBorderTop(borderTop);
	        }
			return cellStyle;
		}
		
		private final HSSFCellStyle getReportHeadingStyle(HSSFFont font, short alignment) {
			HSSFCellStyle hcellStyle = workbook.createCellStyle();
			hcellStyle.setFont(font);
			hcellStyle.setAlignment(alignment);
			return hcellStyle;
		}
	}
}
