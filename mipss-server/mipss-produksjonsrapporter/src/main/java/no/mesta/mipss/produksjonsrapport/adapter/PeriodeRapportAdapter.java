package no.mesta.mipss.produksjonsrapport.adapter;

import java.util.Vector;

import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.TiltakSequence;

import org.apache.poi.hssf.usermodel.HSSFSheet;


//~--- non-JDK imports --------------------------------------------------------


public class PeriodeRapportAdapter implements KontraktAdapterInterface {
    private TiltakSequence globalDynamicHeaderList;
    private KontraktRapport rapport;

    public PeriodeRapportAdapter(KontraktRapport rapport, 
                                 TiltakSequence globalDynamicHeaderList) {
        this.rapport = rapport;
        this.globalDynamicHeaderList = globalDynamicHeaderList;
    }

    public int createReportHeader(HSSFSheet sheet, String rapportNavn, 
                                  String kontrakt, String rodeTypeNavn, 
                                  String rodeNavn, String rodeLengde, 
                                  String rapportDato, String periode, 
                                  int rowCount, KontraktRapportObject o) {
        return rapport.createReportHeader(sheet, rapportNavn, kontrakt, 
                                          rodeTypeNavn, rodeNavn, rodeLengde, 
                                          rapportDato, periode, rowCount);
    }

    public int createTableHeader(HSSFSheet sheet, int rowCount, 
                                 Vector<String> fixedHeaderList, 
                                 TiltakSequence dynamicHeaderList, boolean forside) {
        return rapport.createTableHeader(sheet, rowCount, fixedHeaderList, 
                                         dynamicHeaderList, forside);
    }

    public void createDataRows(HSSFSheet currentSheet, String nameCurrentSheet, String linkLabel, HSSFSheet forsideSheet, IndexedSequence sequenceData, TiltakSequence dynamicHeaderList, int rowCount, int rowCountForside) {                           
        int count = rapport.createDataRows(currentSheet, sequenceData, dynamicHeaderList, rowCount);
        KontraktRapportObject obj = ((KontraktRapportObject)sequenceData.getAt(0));
        String enhetsID = nameCurrentSheet;
        int antallEnhetsPerioder = sequenceData.size();
        // sum for enhet
        createSumRow(currentSheet, dynamicHeaderList, enhetsID, linkLabel, obj, antallEnhetsPerioder, count, rowCountForside, false, false);
    }

    public void createSumRow(HSSFSheet sheet, TiltakSequence dynamicHeaderList, 
                             String enhetsID, String linkLabel, KontraktRapportObject obj, 
                             int antallEnhetsPerioder, int rowCount, 
                             int rowCountGlobal, boolean globalSum, 
                             boolean forside) {
        rapport.createSumRow(sheet, dynamicHeaderList, globalDynamicHeaderList, 
                             enhetsID, linkLabel, obj, antallEnhetsPerioder, rowCount, 
                             rowCountGlobal, globalSum, forside, 0);
    }
}


