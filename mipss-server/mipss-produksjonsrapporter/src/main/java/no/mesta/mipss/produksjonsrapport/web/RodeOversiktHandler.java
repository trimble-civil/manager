package no.mesta.mipss.produksjonsrapport.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.persistence.EntityManager;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.adapter.RodeOversiktRapport;
import no.mesta.mipss.produksjonsrapport.adapter.RodeOversiktRapportAdapter;
import no.mesta.mipss.produksjonsrapport.query.RodeOversiktQuery;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportHelper;
import no.mesta.mipss.produksjonsrapport.values.MestaLogo;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningObject;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningSequence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;



public class RodeOversiktHandler extends ExcelReportHSSFHandler {
    private Logger log = LoggerFactory.getLogger(this.getClass());
	private final EntityManager em;
    
	public RodeOversiktHandler(final File file, EntityManager em) {
        super(file);
		this.em = em;
    }

    public final byte[] createReport(ProdrapportParams params) throws Exception {
    	byte[] report = null;
        String roder = "";
        
        if (params.isAlleInkAvsl()) { // alle roder eksl. avsluttede
            roder = "Alle roder inkl. avsluttede";
        } else{ // alle roder inkl. avsluttede
            roder = "Alle roder eksl. avsluttede";
        } 
        if (params.getRodetypeId()!=null){
        	Rodetype rt = em.find(Rodetype.class, params.getRodetypeId());
            roder +=" / "+rt.getNavn();
        }
        RodeStrekningSequence sequence = new RodeOversiktQuery(params, em).consumeResults();
        log.debug("sequence:"+sequence);
        if (sequence!=null)
        	log.debug("sequence size:"+sequence.size());
        // hvis data finnes, lag excel rapport
        if ((sequence != null) && (sequence.size() > 0)) {
            report = createExcelReport(sequence, file, roder);
        }
        return report;
    }

    /**
     * Genererer XLS rapport for tiltak
     * @throws java.lang.Exception
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException
     * @param file File
     * @param result result
     */
    private byte[] createExcelReport(RodeStrekningSequence result, 
                                   File file, String roder) throws FileNotFoundException, 
                                                     IOException, Exception {
        HSSFWorkbook workbook = null;
        ByteArrayOutputStream ps = new ByteArrayOutputStream();
        try {

            workbook = new HSSFWorkbook();

            ArrayList sheetList = makeRodeLists(result);
            // Oppretter et tomt excelark for forside
            HSSFSheet arkForside = workbook.createSheet("Forside");
            
            int image = workbook.addPicture(MestaLogo.getLogo(), HSSFWorkbook.PICTURE_TYPE_PNG);
            HSSFClientAnchor anchor = new HSSFClientAnchor(0,0,0,255, (short)3,1,(short)4,4);
            anchor.setAnchorType( 2 );
            
            HSSFPatriarch patriarch = arkForside.createDrawingPatriarch();
            patriarch.createPicture(anchor, image);
            
            RodeOversiktRapportAdapter adapter = new RodeOversiktRapportAdapter(new RodeOversiktRapport(workbook));
            int forsideRowCount = 0;
            log.debug("sheetListSize:"+sheetList.size());
            // lager et ark i XLS fil for hver rode
            for (int i = 0; i < sheetList.size(); i++) {
            
                RodeStrekningSequence rodeStrekningSequence = (RodeStrekningSequence)sheetList.get(i);
                RodeStrekningObject rodeStrekningObject = (RodeStrekningObject)rodeStrekningSequence.getAt(0);
                
                //lag header på forsiden i første iterasjon
                if (i==0){
                    forsideRowCount = adapter.createForsideHeader(arkForside, rodeStrekningObject, roder);
                    forsideRowCount = adapter.createForsideTableHeader(arkForside, forsideRowCount);
                }
               
                String rodeNavnTable = i+"_"+ KontraktRapportHelper.parseString(rodeStrekningObject.getRodeNavn()) ;
                if (rodeNavnTable.length()>30){
                    rodeNavnTable = rodeNavnTable.substring(0, 30);
                }
                log.debug(rodeNavnTable);
                HSSFSheet sheet = workbook.createSheet(rodeNavnTable);
                patriarch = sheet.createDrawingPatriarch();
                anchor = new HSSFClientAnchor(255,0,0,255, (short)3,1,(short)4,4);
                anchor.setAnchorType( 2 );
                patriarch.createPicture(anchor, image);
                
                int rowCount = 0;

                // lag rapport header og returnerer neste rad
                rowCount = adapter.createReportHeader(workbook, sheet, rodeStrekningObject, rowCount, arkForside, forsideRowCount, rodeNavnTable);
                forsideRowCount++;
                
                // lag table header og returnerer neste rad
                rowCount = adapter.createTableHeader(workbook, sheet, rowCount);

                // lag rapport data rader
                rowCount++;
                adapter.createDataRows(workbook, sheet, rodeStrekningSequence, rowCount);
                // setter kolonnebredde
                sheet.setDefaultColumnWidth((short)15);
                sheet.setColumnWidth((short)0,(short)(30*256));
                
                adjustPrintSetup(sheet, 10, file.getName(), 6, true);
            }
            
            //summering av rodelengde, er litt unsafe dersom det kommer flere kolonner... 
            HSSFCell sumRodeLengdeCell = arkForside.getRow(3).getCell((short)1);
            sumRodeLengdeCell.setCellFormula("sum(e8:e"+forsideRowCount+")");
            
            arkForside.setDefaultColumnWidth((short)18);
            arkForside.setColumnWidth((short)0,(short)(27*256));
            arkForside.setColumnWidth((short)1,(short)(15*256));
            arkForside.setColumnWidth((short)2,(short)(15*256));
            arkForside.setColumnWidth((short)4,(short)(15*256));
            arkForside.setColumnWidth((short)5,(short)(60*256));
            arkForside.setColumnWidth((short)6,(short)(50*256));
            
            adjustPrintSetup(arkForside, 7, file.getName(), 6, true);
            workbook.write(ps);
            return ps.toByteArray();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("temp. fil ikke funnet");
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
        	log.debug("closing ps");
            ps.close();
        }
    }

    private ArrayList makeRodeLists(RodeStrekningSequence result) {
        int rodeID = 0;
        ArrayList sheetLists = new ArrayList();
        RodeStrekningSequence sequence = new RodeStrekningSequence();
        RodeStrekningObject object = null;
        if ((result != null) && (result.size() > 0)) {
            rodeID = Integer.parseInt(((RodeStrekningObject)result.getAt(0)).getRodeID());
        }
        for (int i = 0; i < result.size(); i++) {
            object = (RodeStrekningObject)result.getAt(i);
            int rode_ID = Integer.parseInt(object.getRodeID());
            if (rodeID != rode_ID) {
                rodeID = rode_ID;
                sheetLists.add(sequence);
                sequence = new RodeStrekningSequence();
            }
            if (rodeID == rode_ID) {
                sequence.add(object);
            }
        }
        // add last element
        sheetLists.add(sequence);
        return sheetLists;
    }
}


