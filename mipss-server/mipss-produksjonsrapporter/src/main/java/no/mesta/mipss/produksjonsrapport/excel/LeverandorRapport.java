package no.mesta.mipss.produksjonsrapport.excel;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.Leverandor;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;

public class LeverandorRapport extends Rapp1Rapport{
	private final List<Rapp1> data;
	private Map<String, Object> map;
	private String templateFile = "leverandor_forside";
	private List<Rapp1Object> leverandorList;
	
	public static void main(String[] args) throws IOException {
		ProdrapportParams params = new ProdrapportParams();
		params.setFraDato(Clock.now());
		params.setTilDato(Clock.now());
		params.setGjennomsnitt(true);
		params.setProsent(true);
		params.setRodeNavn("Testrode");
		params.setKontraktNavn("0123-Bobs lucky contract");
		params.setRodetypeNavn("Testrodetype");
		params.getPeriode();
		new LeverandorRapport(Leverandor.getTestData(), params).finalizeAndOpen();
	}
	
	public LeverandorRapport(List<Rapp1> data, ProdrapportParams params){
		super(params);
		this.data = data;
		initData();
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 6;
		startRow = 9;
		freezePaneCol=3;
		writeReport(leverandorList);
		
		createSubsheets();
	}
	private void createSubsheets(){
		int sheetIndex = 1;
		for (Rapp1Object lev:leverandorList){
			new LeverandorRapportSheet(lev, params, excel, workbook, sheetIndex);
			sheetIndex++;
		}
	}
	
	private void initData() {
		Map<Long, List<Rapp1>> listePrLeverandor = RapportHelper.createListePrLeverandor(data, params);
		Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
		Map<Long, R12Stroprodukt> stroproduktMap = new HashMap<Long, R12Stroprodukt>();
		leverandorList = RapportHelper.createForsideData(prodtypeMap, stroproduktMap, listePrLeverandor, Rapp1Object.class);
		alleProdtyper = RapportHelper.extractAndSortProdtyper(prodtypeMap);
		alleStroprodukter = RapportHelper.extractAndSortStroprodukter(stroproduktMap);
		
		//summer og organiser prodtypene og stroproduktene slik at de ligger i samme rekkefølge for samtlige leverandører
		for (Rapp1Object l:leverandorList){
			l.organizeProdtyper(alleProdtyper);
			l.organizeStroprodukt(alleStroprodukter);
		}
		
		map = new HashMap<String, Object>();
		map.put("rapp1Objects", leverandorList);
		map.put("prodtyper", alleProdtyper);
		map.put("params", params);
		metadata = new ReportMetadata(leverandorList, params);
	}

	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {}
}
