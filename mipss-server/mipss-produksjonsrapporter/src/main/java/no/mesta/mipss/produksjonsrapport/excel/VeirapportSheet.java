package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.List;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Veirapp;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.ReportMetadataItem.VALUE_NAMES;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class VeirapportSheet extends Rapp1Rapport{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");
	private String templateFile = "veirapport";
	private HashMap<String, Object> map;
	private final List<Veirapp> data;

	public VeirapportSheet(List<Veirapp> data, ProdrapportParams params){
		super(params);
		this.data = data;
		file = RapportHelper.createTempFile(templateFile);
		map = new HashMap<String, Object>();
		map.put("params", params);
		
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		excel.setDateFormat("dd.MM.yyyy");
		startCol = 5;
		startRow = 9;
		initData();
		
	}
	public XSSFSheet getSheet(){
		return workbook.getSheetAt(0);
	}
	private void initData(){
		metadata = new ReportMetadata(data, params);
		XSSFSheet sheet = getSheet();
		XSSFRow row1 = excel.getRow(sheet, startRow);
		XSSFRow row2 = excel.getRow(sheet, startRow+2);
		//KJORETOY, DOGN, VEINUMMER
		writeHeaderCell(row1, 0, resources.getGuiString("reportHeader.kjoretoy"));
		writeHeaderCell(row1, 1, resources.getGuiString("reportHeader.dogn"));
		writeHeaderCell(row1, 2, "Veinr");
		writeHeaderCell(row1, 3, "Total");
		writeHeaderCell(row2, 3, "Km");
		writeHeaderCell(row2, 4, "Timer");
		
		CellRangeAddress mergeCells = excel.mergeCells(sheet, 0, 0, startRow, startRow+2);
		excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
		mergeCells = excel.mergeCells(sheet, 1, 1, startRow, startRow+2);
		excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
		mergeCells = excel.mergeCells(sheet, 2, 2, startRow, startRow+2);
		excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
		mergeCells = excel.mergeCells(sheet, 3, 4, startRow, startRow+1);
		excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
		
		int curRow = writeHeaders(startRow, startCol)+3;
		curRow = writeData(curRow, data, startCol);
		writeSum(curRow, curRow-1, startCol);
		writeSum(startRow+3, curRow-1, startCol);
		
		if (params.isGjennomsnitt()){
			addConditionalFormatting(startRow, curRow, VALUE_NAMES.Kmt);
		}
		
		createBackLink(8, 0);
	}
	
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		Veirapp v = (Veirapp)data;
		excel.writeData(dataRow, 0, v.getKjoretoyNavn());
		excel.writeData(dataRow, 1, v.getDogn());
		excel.writeData(dataRow, 2, v.getVeinummer());
		excel.writeData(dataRow, 3, v.getTotalKjortKm());
//		double tid =(double)v.getTotalKjortSekund()/(double)86400;
		excel.writeData(dataRow, 4, v.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 4);
	}

	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 0);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 1);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 2);
		addSumCell(3, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(4, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}

}
