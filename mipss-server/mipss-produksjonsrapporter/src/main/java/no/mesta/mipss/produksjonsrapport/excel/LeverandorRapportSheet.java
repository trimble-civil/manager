package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LeverandorRapportSheet extends Rapp1Rapport{

	private final Rapp1Object leverandor;
	private String templateFile = "leverandor_sheet";
	int startRow = 9;
	int startCol = 7;
	private Map<String, Object> dataMap;
	
	public LeverandorRapportSheet(Rapp1Object leverandor, ProdrapportParams params){
		super(params);
		this.leverandor = leverandor;
		this.params = params;
		initData();
		
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, dataMap);
		excel = new ExcelPOIHelper(workbook);
		
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, leverandor.getData(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(0), 13);
	}

	public LeverandorRapportSheet(Rapp1Object leverandor, ProdrapportParams params, ExcelPOIHelper excel, XSSFWorkbook workbook, int sheetIndex){
		super(params);
		this.leverandor = leverandor;
		initData();
		
		this.sheetIndex = sheetIndex;
		
		this.excel = excel;
		this.workbook = workbook;
		file = RapportHelper.createTempFile(templateFile);
		
		
		copyIntoMainWorkbook(leverandor.getNavn(), templateFile, dataMap);

		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, leverandor.getData(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(0), 13);
	}
	public XSSFSheet getSheet(){
		return workbook.getSheetAt(0);
	}
	private void initData() {
		metadata = new ReportMetadata(leverandor.getData(), params);
		
		dataMap = new HashMap<String,Object>();
		dataMap.put("data", leverandor.getData());
		dataMap.put("rapp1Object", leverandor);
		dataMap.put("params", params);
	}
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {}
}
