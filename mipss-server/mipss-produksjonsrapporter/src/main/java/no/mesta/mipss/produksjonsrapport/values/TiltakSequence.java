package no.mesta.mipss.produksjonsrapport.values;


//~--- non-JDK imports --------------------------------------------------------


public class TiltakSequence extends IndexedSequence {
    public TiltakSequence() {
        super();
    }

    public TiltakSequence(final int initialCapacity) {
        super(initialCapacity);
    }

    public final void add(final TiltakObject tiltakObject) {
        append(tiltakObject);
    }

    public final TiltakObject elementAt(final int index) {
        return (TiltakObject)getAt(index);
    }
    
    public TiltakSequence getTiltakListeFor(String navn){
        for (int i=0;i<elements.size();i++){
            TiltakObject o = elementAt(i);
            if (o.getNavn().equals(navn))
                if (o.getGlobalTiltakListe()!=null)
                    return o.getGlobalTiltakListe();
        }
        return null;
    }
}


