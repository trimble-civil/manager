package no.mesta.mipss.produksjonsrapport.web;
import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.LeverandorRapport;
import no.mesta.mipss.produksjonsrapport.query.Rapp1Query;


public class LeverandorRapportHandler extends ExcelReportHSSFHandler{
    private EntityManager em;
	private ProdrapportParams params;

	public LeverandorRapportHandler(EntityManager em) {
		super(null);
        this.em = em;
    }

    /**
     * Bygger rapport
     * @param request
     * @throws Exception
     */
    public byte[] createReport(ProdrapportParams params) throws Exception {
                                                                       
        this.params = params;
        params.setFrekvens(false);
        params.setRapportType(ProdrapportParams.RapportType.LEVERANDOR.toString());
        List<Rapp1> results = new Rapp1Query(params, em, Rapp1Query.Rapp1Type.LEVERANDOR).getResults();
        if (results==null||results.isEmpty()){
        	return null;
        }
        LeverandorRapport rapp = new LeverandorRapport(results, params);
        return rapp.toByteArray();
    }
}


