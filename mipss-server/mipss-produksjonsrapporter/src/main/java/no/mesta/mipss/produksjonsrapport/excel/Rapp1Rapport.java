package no.mesta.mipss.produksjonsrapport.excel;

import java.awt.Desktop;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Materiale;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.ReportMetadataItem.VALUE_NAMES;

import org.apache.poi.hssf.record.CFRuleRecord.ComparisonOperator;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingRule;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFontFormatting;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheetConditionalFormatting;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public abstract class Rapp1Rapport {
	protected static final String DATO_FORMAT = "[hh]:mm:ss";
	protected ExcelPOIHelper excel;
	protected XSSFWorkbook workbook;
	protected ReportMetadata metadata;
	protected List<R12Prodtype> alleProdtyper;
	protected List<R12Stroprodukt> alleStroprodukter;
	protected File file;
	protected ProdrapportParams params;
	protected String sumRodelengdeCell="b8";
	protected String rodelengdeKolonne="b";
	protected int startCol = 7;
	protected int startRow = 9;
	private XSSFDrawing drawingPatriarch;
	protected int sheetIndex = -1;
	protected int autoFilterRow = 13;
	protected int freezePaneRow = 14;
	protected int freezePaneCol = 4;
	protected boolean createLinks = true;
	
	public Rapp1Rapport(ProdrapportParams params){
		this.params = params;
	}
	
	protected void copyIntoMainWorkbook(String sheetName, String templateName, Map<String, Object> dataMap){
		XSSFSheet sheet = getSheetFromFile(templateName, dataMap);
		XSSFSheet newSheet = excel.createSheet(RapportHelper.parseString(sheetName), workbook);
		Util.copySheets(newSheet, sheet);
//		addAutoFilter(newSheet, 13);
		newSheet.createFreezePane(freezePaneCol, freezePaneRow);
	}
	
	protected boolean isSheetNamePresent(String sheetName){
		return workbook.getSheet(sheetName)!=null;
	}
	private XSSFSheet getSheetFromFile(String templateName, Map<String, Object> dataMap){
		return RapportHelper.readTemplate(templateName, dataMap).getSheetAt(0);
	}
	
	protected int writeReport(List<? extends Rapp1Object> rapp1List){
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, rapp1List, startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		if (createLinks)
			createLinks(startRow+5, 0);
		addAutoFilter(workbook.getSheetAt(sheetIndex==-1?0:sheetIndex), autoFilterRow);
		workbook.getSheetAt(sheetIndex==-1?0:sheetIndex).createFreezePane(freezePaneCol, freezePaneRow);
		if (params.isGjennomsnitt()){
			addConditionalFormatting(startRow, endDataRow, VALUE_NAMES.Kmt);
		}
		return endDataRow;
	}
	
	/**
	 * Legger på betinget formatering på excelarket pr kolonne data 
	 * Kolonnen som skal formatteres er av typen som er definert i VALUE_NAME
	 * 
	 * TODO generaliser litt til.. 
	 * @param startRow
	 * @param endRow
	 * @param type
	 */
	protected void addConditionalFormatting(int startRow, int endRow, VALUE_NAMES type){
		List<Integer> columns = new ArrayList<Integer>();
		for (ReportMetadataItem i:metadata.getItems()){
			int col = i.getColumn();
			for (VALUE_NAMES v:i.getValueNames()){
				if (v.equals(type)){
					columns.add(col);
				}
				col++;
			}
		}
		for (Integer i:columns){
			addFontConditionalFormatting(excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook), startRow, endRow, i, ComparisonOperator.BETWEEN, "100", "1000", IndexedColors.RED.index );
		}
	}
	/**
	 * Skriver tabellheadere
	 * @return
	 */
	protected int writeHeaders(int startRow, int startCol){
		XSSFSheet forside = excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook);
		int rowIndex = startRow;
		int colIndex = startCol;
		
		XSSFRow headerRow1 = excel.getRow(forside, rowIndex);
		XSSFRow headerRow2 = excel.getRow(forside, rowIndex+1);
		XSSFRow headerRow3 = excel.getRow(forside, rowIndex+2);
		
		for (ReportMetadataItem i:metadata.getItems()){
			i.setColumn(colIndex);
			String head = i.getHeader();
			int colSpan = i.getColSpan();
				
			if (i.getComment()!= null) {
				createComment(i.getComment(), forside, colIndex, rowIndex);
			}			
			writeHeaderCell(headerRow1, colIndex, head);
			CellRangeAddress mergedCells = null;
			if (i.getTyper()==null) {
				mergedCells = excel.mergeCells(forside, colIndex, colIndex+colSpan-1, rowIndex, rowIndex+1);
			} else{
				mergedCells = excel.mergeCells(forside, colIndex, colIndex+colSpan-1, rowIndex, rowIndex);
			}
			excel.styleMergedRegion(forside, mergedCells, excel.getHeaderStyle(false));
			
			int tCol = 0;
			for (VALUE_NAMES typeHeadEnum :i.getValueNames()){
				String typeHead = typeHeadEnum.getTitle();
				writeHeaderCell(headerRow3, colIndex+tCol, typeHead);
				tCol++;
			}
			if (i.getTyper()!=null){
				int c=0; 
				for (ReportMetadataItem ii:i.getTyper()){
					int cSpan = ii.getColSpan();
					ii.setColumn(colIndex+c);
					XSSFCell headerCell = writeHeaderCell(headerRow2, colIndex+c, ii.getHeader());
					String commentStr = ii.getComment();
					if (commentStr!=null){
						Comment comment = createComment(commentStr, forside, headerCell.getColumnIndex(), headerRow2.getRowNum());
						headerCell.setCellComment(comment);
					}
					
					excel.mergeCells(forside, colIndex+c, colIndex+c+cSpan-1, rowIndex+1, rowIndex+1);
					
					int itCol = 0;
					for (VALUE_NAMES typeHeadEnum :ii.getValueNames()){
						String typeHead = typeHeadEnum.getTitle();
						XSSFCell headerTypeCell = writeHeaderCell(headerRow3, colIndex+c+itCol, typeHead);
						if (typeHeadEnum.equals(VALUE_NAMES.Tonn)){
							String commentForTonn = ii.getCommentForTonn();
							if (commentForTonn!=null){
								Comment comment = createComment(commentForTonn, forside, headerTypeCell.getColumnIndex(), headerRow3.getRowNum());
								headerTypeCell.setCellComment(comment);
							}
						}
						if (typeHeadEnum.equals(VALUE_NAMES.M3)){
							String commentForM3 = ii.getCommentForM3();
							if (commentForM3!=null){
								Comment comment = createComment(commentForM3, forside, headerTypeCell.getColumnIndex(), headerRow3.getRowNum());
								headerTypeCell.setCellComment(comment);
							}
						}
						itCol++;
					}
					
					c+=cSpan;
				}
			}
			colIndex+=colSpan;
		}
		return rowIndex+2;
	}
	/**
	 * Lager en excel-kommentar til cellen
	 * 
	 * @param text
	 * @param sheet
	 * @param colIndex
	 * @param rowIndex
	 * @return
	 */
	private Comment createComment(String text, XSSFSheet sheet, int colIndex, int rowIndex){
		if (drawingPatriarch==null){
			drawingPatriarch = sheet.createDrawingPatriarch();
		}
		Dimension size = calcCommentSize(text);
		XSSFClientAnchor anc = new XSSFClientAnchor(10,10,10,10, colIndex, rowIndex, colIndex+size.width, rowIndex+size.height);
		XSSFComment comment = drawingPatriarch.createCellComment(anc);
		comment.setString(text);
		return comment;
	}
	/**
	 * Beregner størrelsen på kommentaren til cellen
	 * @param text
	 * @return
	 */
	private Dimension calcCommentSize(String text){
		int charsPrCell = 6;
		int cellsWide = 3;
		
		int charsPrRow = cellsWide*charsPrCell;
		int rowCount = (text.length()/charsPrRow)+1;
		
		return new Dimension(cellsWide,rowCount);
	}
	
	protected abstract void writeCustomData(XSSFRow dataRow, Rapp1Entity data);
	/**
	 * Skriver dataradene
	 * @param row
	 * @return
	 */
	protected int writeData(int row, List<? extends Rapp1Entity> data, int startCol){
		XSSFSheet forside = excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook);
		for (Rapp1Entity l:data){
			XSSFRow dataRow = excel.getRow(forside, row);
			writeCustomData(dataRow, l);
			int lastColumn = 0;
			for (R12Prodtype p:l.getProdtyper()){
				ReportMetadataItem m = metadata.getMetadataItem(p.getHeader());
				int column = m.getColumn();
				if (!p.getStroFlagg()){
					if (p.getProdtyper()==null){
						int kmCellIdx = -1;
						int tidCellIdx = -1;
						if (m.isValuePresent(VALUE_NAMES.Km)){
							kmCellIdx = column;
							excel.writeData(dataRow, column++, p.getKjortKm());
						}
						if (m.isValuePresent(VALUE_NAMES.Timer)){
							tidCellIdx = column;
							excel.writeData(dataRow, column++, p.getTid());
							excel.setCellStyle(dataRow, column-1, excel.getDatoStyle(DATO_FORMAT, false));
						}
						if (m.isValuePresent(VALUE_NAMES.M2)){
							excel.writeData(dataRow, column++, p.getMengdeM2());
							excel.setCellStyle(excel.getDecimalStyleShort(false), dataRow, column-1);
						}						
						if (m.isValuePresent(VALUE_NAMES.Kmt)){
							column = writeKmt(dataRow, kmCellIdx, tidCellIdx, column);
							excel.setCellStyle(excel.getDecimalStyle(false), dataRow, column-1);
						}
						if (m.isValuePresent(VALUE_NAMES.Prosent)){
							column = writeProsent(dataRow, startCol-2, kmCellIdx, column);
							excel.setCellStyle(excel.getDecimalStyle(false), dataRow, column-1);
						}
						if (m.isValuePresent(VALUE_NAMES.Frekvens)){
							String rodelengdeCell = rodelengdeKolonne!=null?rodelengdeKolonne+(dataRow.getRowNum()+1):sumRodelengdeCell;
							column = writeFrekvens(dataRow, kmCellIdx, column, rodelengdeCell);
							excel.setCellStyle(excel.getDecimalStyle(false), dataRow, column-1);
						}
					}else{
						excel.writeData(dataRow, column++, p.getKjortKm());
						for (R12Prodtype pt:p.getProdtyper()){
							ReportMetadataItem metaSub = m.getMetadataForType(pt.getHeader());
							int col = metaSub.getColumn();
							excel.writeData(dataRow, column++, pt.getKjortKm());
							excel.writeData(dataRow, column++, pt.getTid());
							excel.setCellStyle(dataRow, column-1, excel.getDatoStyle(DATO_FORMAT, false));
							excel.writeData(dataRow, column++, pt.getMengdeM2());							
						}
					}
				}
				else{
					for (R12Stroprodukt s:l.getStroprodukter()){
						ReportMetadataItem mType = m.getMetadataForType(s.getHeader());
						column = mType.getColumn();
						int kmCellIdx = -1;
						int tidCellIdx = -1;
						if (mType.isValuePresent(VALUE_NAMES.Km)){
							kmCellIdx = column;
							excel.writeData(dataRow, column++, s.getKjortKm());
						}
						if (mType.isValuePresent(VALUE_NAMES.Timer)){
							tidCellIdx = column;
							excel.writeData(dataRow, column++, s.getTid());
							excel.setCellStyle(dataRow, column-1, excel.getDatoStyle(DATO_FORMAT, false));
						}
						if (mType.isValuePresent(VALUE_NAMES.Kmt)){
							column = writeKmt(dataRow, kmCellIdx, tidCellIdx, column);
							excel.setCellStyle(excel.getDecimalStyleShort(false), dataRow, column-1);
						}
						if (mType.isValuePresent(VALUE_NAMES.Prosent)){
							column = writeProsent(dataRow, startCol-2, kmCellIdx, column);
							excel.setCellStyle(excel.getDecimalStyleShort(false), dataRow, column-1);
						}
						if (mType.isValuePresent(VALUE_NAMES.Frekvens)){
							String rodelengdeCell = rodelengdeKolonne!=null?rodelengdeKolonne+(dataRow.getRowNum()+1):sumRodelengdeCell;
							column = writeFrekvens(dataRow, kmCellIdx, column, rodelengdeCell);
							excel.setCellStyle(excel.getDecimalStyle(false), dataRow, column-1);
						}
						if (mType.isValuePresent(VALUE_NAMES.M3)){
							excel.writeData(dataRow, column++, s.getMengdeM3());
							excel.setCellStyle(excel.getDecimalStyle(false), dataRow, column-1);
						/*	
						 Fix for å ha egne farger for M3 tall her...ikke aktuelt nå men kanskje senere.
						  if(s.getId()!=5){
								excel.setCellStyle(excel.getM3InfoStyle(), dataRow, column-1);
							}else{
								excel.setCellStyle(excel.getDecimalStyle(false), dataRow, column-1);
							} */
						}
						if (mType.isValuePresent(VALUE_NAMES.Tonn)){
							excel.writeData(dataRow, column++, s.getMengdeTonn());
						}
					}
				}
				if (l.getMaterialer()!=null&&l.getMaterialer().size()>0){
					ReportMetadataItem mi = metadata.getMetadataItem("Utlagte mengder");
					for (Materiale mat:l.getMaterialer()){
						ReportMetadataItem metadataForMateriale = mi.getMetadataForType(mat.getHeader());
						column = metadataForMateriale.getColumn();
						if (metadataForMateriale.isValuePresent(VALUE_NAMES.Tonn)){
							excel.writeData(dataRow, column++, mat.getTonn());
						}
					}
				}
				if (column>lastColumn){
					lastColumn = column;
				}
			}
			if (l.getPaaKontrakt()!=null&&l.getPaaKontrakt()){
				
			}else{
				excel.createBakgrunnStyle(dataRow, lastColumn);
			}
			row++;
		}
		return row;
	}
	/**
	 * ComparisonOperator.BETWEEN
	 * IndexedColors.RED.index
	 * @param sheet
	 * @param startRow
	 * @param endRow
	 * @param colIndex
	 * @param comparisonOperator
	 * @param value1
	 * @param value2
	 */
	protected void addFontConditionalFormatting(XSSFSheet sheet, int startRow, int endRow, int colIndex, byte comparisonOperator, String value1, String value2, short color){
		XSSFSheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
        XSSFConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule(comparisonOperator, value1, value2);
        XSSFFontFormatting fontFormat = rule1.createFontFormatting();
        fontFormat.setFontColorIndex(color);
        CellRangeAddress region = new CellRangeAddress(startRow, endRow, colIndex, colIndex);
        sheetCF.addConditionalFormatting(new CellRangeAddress[]{region}, rule1);
	}
	
	private int writeKmt(XSSFRow dataRow, int kmCellIdx, int tidCellIdx, int kmtColumnIdx){
		if (kmCellIdx==-1||tidCellIdx==-1)
			return kmtColumnIdx+1;
		CellReference refKm = new CellReference(dataRow.getRowNum(), kmCellIdx);
		CellReference refTid = new CellReference(dataRow.getRowNum(), tidCellIdx);
        String kmtFormel = "IF(OR(" + refKm.formatAsString() + "=0," + refTid.formatAsString() + "=0),0," + refKm.formatAsString() + "/(" + refTid.formatAsString() + "*24))";
        XSSFCell kmtCell = excel.createCell(dataRow, kmtColumnIdx);
        kmtCell.setCellFormula(kmtFormel);
        return kmtColumnIdx+1;
	}
	
	private int writeProsent(XSSFRow dataRow, int kmTotalCellIdx, int kmCellIdx, int prosentColumnIdx){
		if (kmCellIdx==-1||kmTotalCellIdx==-1){
			return prosentColumnIdx+1;
		}
		CellReference refKmTotal = new CellReference(dataRow.getRowNum(), kmTotalCellIdx);
		CellReference refKm = new CellReference(dataRow.getRowNum(), kmCellIdx);
		String prosentFormel = refKm.formatAsString()+"/"+refKmTotal.formatAsString()+" * 100";
		XSSFCell prosentCell = excel.createCell(dataRow, prosentColumnIdx);
        prosentCell.setCellFormula(prosentFormel);
        return prosentColumnIdx+1;
	}
	
	private int writeFrekvens(XSSFRow dataRow, int kmCellIdx, int frekvensColumnIdx, String rodelengdeCellRef){
		if (kmCellIdx==-1||frekvensColumnIdx==-1){
			return frekvensColumnIdx+1;
		}
		CellReference refKm = new CellReference(dataRow.getRowNum(), kmCellIdx);
		String frekvensFormel = "IF(OR(" + refKm.formatAsString() + "=0,"+rodelengdeCellRef+"=0),0,(" + refKm.formatAsString() + "/"+rodelengdeCellRef+"))";
		XSSFCell frekvensCell = excel.createCell(dataRow, frekvensColumnIdx);
        frekvensCell.setCellFormula(frekvensFormel);
        return frekvensColumnIdx+1;
	}
	
	protected abstract void writeCustomSum(XSSFRow dataRow, int startRow, int endRow);
	/**
	 * Skriver sumradene
	 * @param row
	 * @return
	 */
	protected int writeSum(int sumRow, int endRow, int startCol){
		XSSFSheet forside = excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook);
		int startRow = endRow-metadata.getDataRowCount()+1;
		int column = 0;
		XSSFRow dataRow = excel.getRow(forside, sumRow);
		writeCustomSum(dataRow, startRow, endRow);
		for (ReportMetadataItem i:metadata.getItems()){
			if(i.getTyper()==null){
				column = i.getColumn();
				int kmCellIdx = column;
				if (i.isValuePresent(VALUE_NAMES.Km)){
					column = addSumCell(column, startRow, endRow, dataRow, excel.getDecimalStyleSum());//KM
				}
				int tidCellIdx = column;
				if (i.isValuePresent(VALUE_NAMES.Timer)){
					column = addSumCell(column, startRow, endRow, dataRow,  excel.getDatoStyleSum(DATO_FORMAT));//TID
				}
				if (i.isValuePresent(VALUE_NAMES.M2)){
					column = addSumCell(column, startRow, endRow, dataRow, excel.getDecimalStyleShortSum());//M2
				}
				if (i.isValuePresent(VALUE_NAMES.Kmt)){
					column = writeKmt(dataRow, kmCellIdx, tidCellIdx, column);
					excel.setCellStyle(excel.getDecimalStyleSum(), dataRow, column-1);
				}
				if (i.isValuePresent(VALUE_NAMES.Prosent)){
					column = writeProsent(dataRow, startCol-2, kmCellIdx, column);
					excel.setCellStyle(excel.getDecimalStyleSum(), dataRow, column-1);
				}
				if (i.isValuePresent(VALUE_NAMES.Frekvens)){
					column = writeFrekvens(dataRow, kmCellIdx, column, sumRodelengdeCell);
					excel.setCellStyle(excel.getDecimalStyleSum(), dataRow, column-1);
				}
			}else{
				for (ReportMetadataItem ii:i.getTyper()){
					column = ii.getColumn();
					int kmCellIdx = column;
					if (ii.isValuePresent(VALUE_NAMES.Km)){
						column = addSumCell(column, startRow, endRow, dataRow, excel.getDecimalStyleSum());//KM
					}
					int tidCellIdx = column;
					if (ii.isValuePresent(VALUE_NAMES.Timer)){
						column = addSumCell(column, startRow, endRow, dataRow, excel.getDatoStyleSum(DATO_FORMAT));//TID
					}
					if (ii.isValuePresent(VALUE_NAMES.M2)){
						column = addSumCell(column, startRow, endRow, dataRow, excel.getDecimalStyleSum());//m2
					}					
					if (ii.isValuePresent(VALUE_NAMES.Kmt)){
						column = writeKmt(dataRow, kmCellIdx, tidCellIdx, column);
						excel.setCellStyle(excel.getDecimalStyleSum(), dataRow, column-1);
					}
					if (ii.isValuePresent(VALUE_NAMES.Prosent)){
						column = writeProsent(dataRow, startCol-2, kmCellIdx, column);
						excel.setCellStyle(excel.getDecimalStyleSum(), dataRow, column-1);
					}
					if (ii.isValuePresent(VALUE_NAMES.Frekvens)){
						column = writeFrekvens(dataRow, kmCellIdx, column, sumRodelengdeCell);
						excel.setCellStyle(excel.getDecimalStyleSum(), dataRow, column-1);
					}
					if (ii.isValuePresent(VALUE_NAMES.M3)){
						column = addSumCell(column, startRow, endRow, dataRow, excel.getDecimalStyleSum());//M3
					}
					if (ii.isValuePresent(VALUE_NAMES.Tonn)){
						column = addSumCell(column, startRow, endRow, dataRow, excel.getDecimalStyleSum());//Tonn
					}
				}
			}
		}
		return endRow++;
	}
	
	/**
	 * Lager en sumkolonne på angitt rad og kolonne, bruker startRow for å finne første raden
	 * @param sheet
	 * @param endRow
	 * @param column
	 * @param startRow
	 * @param sumRow
	 * @return
	 */
	protected int addSumCell(int column, int startRow, int endRow, XSSFRow sumRow, XSSFCellStyle style) {
		CellReference startCell = new CellReference(startRow, column);
		CellReference endCell = new CellReference(endRow, column);
		String formula  = "SUBTOTAL(9," + startCell.formatAsString() + ":" + endCell.formatAsString() + ")";
		XSSFCell sumCell = excel.createCell(sumRow, column);
		sumCell.setCellFormula(formula);
		if (style!=null)
			sumCell.setCellStyle(style);
		column++;
		return column;
	}
	
	/**
	 * Lager linker fra forsiden til A1 på underarkene
	 * @param startDataRow
	 * @param column
	 */
	protected void createLinks(int startDataRow, int column){
		for (int i=0; i< metadata.getDataRowCount();i++){
			XSSFRow row = excel.getRow(excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook), startDataRow+i);
			XSSFCell cell = row.getCell(column);
			XSSFRichTextString richStringCellValue = cell.getRichStringCellValue();
			String title = richStringCellValue.getString();
			String target = RapportHelper.parseString(title);
			title = "\""+title+"\"";
			target = "\"#"+target+"!A1\"";
			excel.linkify(cell, title, target);
		}
	}
	
	protected void createBackLink(int rowIndex, int colIndex){
		XSSFRow row = (XSSFRow)excel.getRow(excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook), rowIndex);
		if (row==null){
			row = excel.createRow(excel.getSheet(sheetIndex==-1?0:sheetIndex, workbook), rowIndex);
		}
		XSSFCell cell = row.getCell(colIndex);
		String title = "\"<- Til forsiden\"";
		String target = "\"#forside!A1\"";
		if (cell==null){
			excel.createLinkCell(row, colIndex, title, target, false);
		}else{
			excel.linkify(cell, title, target);
		}
	}
	
	protected void addAutoFilter(XSSFSheet sheet, int autoFilterRow){
		int lastCellNum = sheet.getRow(autoFilterRow-1).getLastCellNum()-1;
		sheet.setAutoFilter(new CellRangeAddress(autoFilterRow, autoFilterRow, 0, lastCellNum));
	}
	public void finalizeAndOpen() {
		RapportHelper.closeWorkbook(workbook, file);
		Desktop desktop = Desktop.getDesktop();
		if(desktop.isSupported(Desktop.Action.OPEN)) {
			try {
				desktop.open(file);
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}
	}
	
	public byte[] toByteArray() throws IOException{
		byte[] byteArray = RapportHelper.toByteArray(workbook);
		return byteArray;
	}
	
	public XSSFCell writeHeaderCell(XSSFRow row, int col, String value){
		XSSFCell headerCell = excel.createCell(row, col);
		excel.setCellValue(headerCell, value);

		headerCell.setCellStyle(excel.getHeaderStyle(false));
		return headerCell;
	}
}
