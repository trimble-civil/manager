package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class RodeRapportSheet extends Rapp1Rapport{

	private final Rapp1Object rode;
	private String templateFile = "rode_sheet";
	int startRow = 9;
	int startCol = 7;
	private Map<String, Object> dataMap;
	
	public RodeRapportSheet(Rapp1Object rode, ProdrapportParams params){
		super(params);
		this.rode = rode;
		initData();
		rodelengdeKolonne=null;		
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, dataMap);
		excel = new ExcelPOIHelper(workbook);
		
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, rode.getData(), startCol);
		writeSum(endDataRow, endDataRow-1,startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(0), 13);
	}
	public RodeRapportSheet(Rapp1Object rode, ProdrapportParams params, ExcelPOIHelper excel, XSSFWorkbook workbook, int sheetIndex){
		super(params);
		this.rode = rode;
		initData();
		rodelengdeKolonne=null;
		this.sheetIndex = sheetIndex;
		
		this.excel = excel;
		this.workbook = workbook;
		file = RapportHelper.createTempFile(templateFile);
		copyIntoMainWorkbook(rode.getRodenavn(), templateFile, dataMap);
		
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, rode.getData(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(sheetIndex==-1?0:sheetIndex), 13);
	}
	
	public XSSFSheet getSheet(){
		return workbook.getSheetAt(0);
	}
	private void initData() {
		metadata = new ReportMetadata(rode.getData(), params);
		
		dataMap = new HashMap<String,Object>();
		dataMap.put("data", rode.getData());
		dataMap.put("rapp1Object", rode);
		dataMap.put("params", params);
	}
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		Rapp1 o = (Rapp1)data;
		excel.writeData(dataRow, 0, o.getKjoretoyNavn());
		excel.writeData(dataRow, 1, o.getLeverandorNavn());
		excel.writeData(dataRow, 2, o.getFraDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 2);
		excel.writeData(dataRow, 3, o.getTilDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 3);
		excel.writeData(dataRow, 4, o.getSamprodFlagg());
		excel.writeData(dataRow, 5, o.getKjortKm());
		excel.writeData(dataRow, 6, o.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 6);
	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		for (int i=0;i<6;i++){
			excel.setCellStyle(excel.getTextStyleSum(), dataRow, i);
		}
		addSumCell(5, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(6, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}
}
