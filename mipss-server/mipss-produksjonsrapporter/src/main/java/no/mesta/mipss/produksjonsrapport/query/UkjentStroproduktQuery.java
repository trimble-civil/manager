package no.mesta.mipss.produksjonsrapport.query;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.persistence.rapportfunksjoner.UkjentStroprodukt;
import no.mesta.mipss.query.NativeQueryWrapper;

public class UkjentStroproduktQuery {
	
	private Query query;
	private final EntityManager em;

	public UkjentStroproduktQuery(Long kjoretoyId, Date fraDato, Date tilDato, EntityManager em) {
		this.em = em;
		
		StringBuilder q  = new StringBuilder();
		
		q.append("SELECT kontrakt_id, kontrakt, kjoretoy_id, regnr, maskinnr, dfu_id, serienr, spreder_type, " +
				"konsolltekst_tort, konsolltekst_losning, inn_tort, inn_losning, mesta_tort, " +
				"mesta_losning, melding, fra_dato_tid, til_dato_tid, antall_tiltak, sum_timer, " +
				"tonn_tort_totalt, tonn_tort_fra_losning, m3_losning, tonn_losning " +
				"FROM table(adminrapp_pk.ukjent_stroprodukt_f(?1, ?2, ?3))");
		
		query = em.createNativeQuery(q.toString())
		.setParameter(1, fraDato)
		.setParameter(2, tilDato)
		.setParameter(3, kjoretoyId);	
	}

	public List<UkjentStroprodukt> getResults() {
		NativeQueryWrapper<UkjentStroprodukt> qry = new NativeQueryWrapper<UkjentStroprodukt>(
    			query, 
    			UkjentStroprodukt.class, 
    			UkjentStroprodukt.getPropertiesArray());
    	List<UkjentStroprodukt> result = qry.getWrappedList();

    	return result; //Returner listen med data
	}
}
