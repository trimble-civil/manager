package no.mesta.mipss.produksjonsrapport.query;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.persistence.rapportfunksjoner.Akkumulert;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.query.NativeQueryWrapper;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.joda.time.DateTime;
import org.joda.time.Months;

public class AkkumulertQuery {
	private Query query;
	private final EntityManager em;
	private final ProdrapportParams params; 
	
	public AkkumulertQuery(ProdrapportParams params, EntityManager em){
		this.params = params;
		this.em = em;
		Date fraDato = params.getFraDato();
		Date tilDato = params.getTilDato();
		Calendar c = Calendar.getInstance();
		c.setTime(fraDato);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		fraDato = c.getTime();
		c.setTime(tilDato);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		tilDato = c.getTime();
		
		
		StringBuilder q  = new StringBuilder();
		q.append("SELECT ID, KONTRAKT_ID, DOGN, VEIKATEGORI||VEISTATUS||VEINUMMER VEI, PRODTYPE_ID, ");
		q.append("STROPRODUKT_ID, KJORT_KM, KJORT_SEKUND, akk.materiale_id, akk.stromengde FROM AKKUMULERT ");
		q.append("left join akk_mengde akk on akk.akk_id = id ");
		q.append("WHERE KONTRAKT_ID = ?1 ");//kontrakt_id
		q.append("and dogn between ?2 and ?3 "); //fra_dato, til_dato 
		q.append("ORDER BY VEI, dogn, prodtype_id, stroprodukt_id");
		
		query = em.createNativeQuery(q.toString())
		.setParameter(1, params.getDriftkontraktId())
		.setParameter(2, fraDato)
		.setParameter(3, tilDato);
		
	}

	public List<Rapp1> getResults(){
		NativeQueryWrapper<Akkumulert> qry = new NativeQueryWrapper<Akkumulert>(
    			query, 
    			Akkumulert.class, 
    			Akkumulert.getPropertiesArray());
    	qry.setDateMask("yyyy-MM-dd HH:mm:ss");
    	List<Akkumulert> result = qry.getWrappedList();

    	List<R12Stroprodukt> stroCache = new ArrayList<R12Stroprodukt>();
    	List<R12Prodtype> prodCache = new ArrayList<R12Prodtype>();
    	List<Rapp1> list = new ArrayList<Rapp1>();
    	Rapp1 prev = null;
    	for (Akkumulert a:result){
    		Rapp1 rapp = new Rapp1();
    		rapp.setFraDato(a.getDogn());
    		rapp.setTilDato(a.getDogn());
    		rapp.setVei(a.getVei());
    		
    		boolean ny = false;
    		if (params.isSumMnd()){
    			if (isEqualMonth(rapp, prev)){
    				rapp = prev;
    			}else{
    				ny = true;
    			}
    		}
    		else{
    			if (isEqualDate(rapp, prev)){ //samme vei
        			rapp = prev;
    			}else{
    				ny = true;
    			}
    		}
    		if (ny){
				//ny vei og/eller ny periode.
				rapp.setProdtyper(new ArrayList<R12Prodtype>());
				rapp.setStroprodukter(new ArrayList<R12Stroprodukt>());
				list.add(rapp);
    		}
    		
    		
    		R12Prodtype eksisterende = getExistingProdtype(a.getProdtypeId(), rapp);
    		if (eksisterende!=null){//prodtypen finnes allerede for denne veien, legg til nye tall
    			eksisterende.addKjortKm(a.getKjortKm());
    			eksisterende.addKjortSekund(a.getKjortSekund());
    		}
    		else{
	    		//hent prodtype fra databasen og legg til produksjon
	    		R12Prodtype prodtype = getProdtypeFromId(a.getProdtypeId(), prodCache);
	    		prodtype.setKjortKm(a.getKjortKm());
	    		prodtype.setKjortSekund(a.getKjortSekund());
	    		rapp.getProdtyper().add(prodtype);
    		}
    		
    		rapp.addKjortKm(a.getKjortKm());
    		rapp.addKjortSekund(a.getKjortSekund());
    		
    		if (a.getStroproduktId()!=null){//har stroprodukt tilknyttet, hent stroprodukt og legg til mengde
    			R12Stroprodukt eksisterendeStro = getExistingStroprodukt(a.getStroproduktId(), rapp);
    			if (eksisterendeStro!=null){//strøproduktet finnes allerede for denne veien, legg til nye tall
    				eksisterendeStro.addKjortKm(a.getKjortKm());
    				eksisterendeStro.addKjortSekund(a.getKjortSekund());
    				eksisterendeStro.addMengdeTonn(a.getStromengde());
    			}
    			else{ //nytt strøprodukt
	    			R12Stroprodukt stroprodukt = getStroproduktFromId(a.getStroproduktId(), stroCache);
	    			stroprodukt.setMengdeTonn(a.getStromengde());
	    			stroprodukt.setKjortKm(a.getKjortKm());
	    			stroprodukt.setKjortSekund(a.getKjortSekund());
	    			rapp.getStroprodukter().add(stroprodukt);
    			}
    		}
    		prev = rapp;
    		
    	}
		return list;
	}
	
	private R12Stroprodukt getExistingStroprodukt(Long stroproduktId, Rapp1 rapp) {
		for (R12Stroprodukt r:rapp.getStroprodukter()){
			if (r.getId().equals(stroproduktId))
				return r;
		}
		return null;
	}

	private R12Prodtype getExistingProdtype(Long prodtypeId, Rapp1 rapp) {
		for (R12Prodtype r:rapp.getProdtyper()){
			if (r.getId().equals(prodtypeId))
				return r;
		}
		return null;
	}

	private R12Stroprodukt getStroproduktFromId(Long stroproduktId, List<R12Stroprodukt> stroCache){
		R12Stroprodukt temp = new R12Stroprodukt();
		temp.setId(stroproduktId);
		if (stroCache.contains(temp)){
			return stroCache.get(stroCache.indexOf(temp)).clone();
		}
		StringBuilder query = new StringBuilder();
		query.append("SELECT ID, NAVN, GUI_REKKEFOLGE FROM STROPRODUKT ");
		query.append("where id = ?");
		Query q = em.createNativeQuery(query.toString()).setParameter(1, stroproduktId);
		NativeQueryWrapper<R12Stroprodukt> qry = new NativeQueryWrapper<R12Stroprodukt>(
    			q, 
    			R12Stroprodukt.class, 
    			"id", "header", "seq");
		R12Stroprodukt stro = qry.getWrappedSingleResult();
		stroCache.add(stro);
		return stro;
	}
	
	private R12Prodtype getProdtypeFromId(Long prodtypeId, List<R12Prodtype> prodCache){
		R12Prodtype temp = new R12Prodtype();
		temp.setId(prodtypeId);
		if (prodCache.contains(temp)){
			return prodCache.get(prodCache.indexOf(temp)).clone();
		}
		StringBuilder query = new StringBuilder();
		query.append("SELECT ID, NAVN, GUI_REKKEFOLGE, STRO_FLAGG FROM prodtype ");
		query.append("where id = ?");
		Query q = em.createNativeQuery(query.toString()).setParameter(1, prodtypeId);
		NativeQueryWrapper<R12Prodtype> qry = new NativeQueryWrapper<R12Prodtype>(
    			q, 
    			R12Prodtype.class, 
    			"id", "header", "seq", "stroFlagg");
		R12Prodtype prod = qry.getWrappedSingleResult();
		prodCache.add(prod);
		return prod;
	}
	
	private boolean isEqualDate(Rapp1 r1, Rapp1 r2){
		if (r1==null||r2==null)
			return false;
		return new EqualsBuilder()
		.append(r1.getFraDato(), r2.getFraDato())
		.append(r1.getVei(), r2.getVei())
		.isEquals();
	}
	
	private boolean isEqualMonth(Rapp1 r1, Rapp1 r2){
		if (r1==null||r2==null)
			return false;
		DateTime d = new DateTime(r1.getFraDato());
		Months diff = Months.monthsBetween(d, new DateTime(r2.getFraDato()));
		int monthsDiff = diff.getMonths();
		return new EqualsBuilder()
		.append(monthsDiff, 0)
		.append(r1.getVei(), r2.getVei())
		.isEquals();
	}
}
