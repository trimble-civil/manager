package no.mesta.mipss.produksjonsrapport;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kontrakt.SlettFeltstatistikk;
import no.mesta.mipss.persistence.rapportfunksjoner.Hullrapport;
import no.mesta.mipss.persistence.rapportfunksjoner.KapasitetRapportObject;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallDatakvalitet;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.persistence.rapportfunksjoner.RegionTall;

@Remote
public interface AdminrapportService {
	public static final String BEAN_NAME = "AdminrapportService";
	
	List<Hullrapport> hentHullrapport();
	/**
	 * Henter statistikk for en kontrakt med gitt id. 
	 * 
	 * @param driftkontraktId
	 * @return
	 */
	NokkeltallFelt hentTilstandsrapportKontrakt(Long driftkontraktId, Date fraDato, Date tilDato);
	NokkeltallFelt hentDekningsgradTilDashboard(Long driftkontraktId, Date fraDato, Date tilDato);
	NokkeltallDatakvalitet hentDatakvalitetstall(Long driftkontraktId);
	
	List<NokkeltallDatakvalitet> hentDatakvalitetstall(String brukerSign);
	List<NokkeltallFelt> hentTilstandsrapportKontrakt(String brukerSign, Date fraDato, Date tilDato);
	NokkeltallFelt hentAntRetKmKontForKontrakt(Long driftkontraktId, Date fraDato, Date tilDato);
	
	List<Long> hentKontrakttilgang(String brukerSign);
	List<NokkeltallFelt> hentTilstandsrapportGrunndata(Long year, Long month);
	
	List<RegionTall> hentTilstandsrapport(Long year, Long month);
	Map<String, Vector<?>> hentOversiktSynkprodpunkt();
	Map<String, Vector<?>> hentSpokelsesrapport();
	Map<String, Vector<?>> hentSprederrapport(String serienummer, Long kontraktId, Date fraDato, Date tilDato)throws Exception;
	Map<String, Vector<?>> hentSprederdetalj(String serienummer, Date fraDato, Date tilDato)throws Exception;
	
	void persistSlettFeltstatistikk(SlettFeltstatistikk stat);
	List<SlettFeltstatistikk> getAlleAktiveSlettingerStatistikk();
	List<KapasitetRapportObject> getKapasitetsRapportData(Date fraDato, Date tilDato, boolean detaljertRapport);
	Map<String, Vector<?>> hentVeinettManglerRapport();
	Map<String, Vector<?>> hentVeinettTilleggRapport();
	
	Map<String, Vector<?>> hentUkjenteStroprodukterAdmin(Date fraDato, Date tilDato);	
}
