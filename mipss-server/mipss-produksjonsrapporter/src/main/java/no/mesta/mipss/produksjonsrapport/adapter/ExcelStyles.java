package no.mesta.mipss.produksjonsrapport.adapter;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;


//~--- non-JDK imports --------------------------------------------------------


/**
 * @author Luc Quan Vu, TietoEnator
 * @since 27.02.2006
 */
public final class ExcelStyles {
    private HSSFFont fontNumber;
    private HSSFFont fontText;
    private HSSFWorkbook workbook;

    public ExcelStyles(final HSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    public ExcelStyles(final HSSFWorkbook workbook, final HSSFFont fontText, 
                       final HSSFFont fontNumber) {
        this.workbook = workbook;
        this.fontText = fontText;
        this.fontNumber = fontNumber;
    }

    public final HSSFCellStyle getDateStyle() {

        // lag Style dato-celler med borders rundt
        HSSFCellStyle datocellStyle = workbook.createCellStyle();

        datocellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        datocellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        datocellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        datocellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFDataFormat datoFormat = workbook.createDataFormat();

        // System.out.println("builtinFormats: " + datoFormat.getFormat("dd.mm.yyyy HH:MM"));
        // datocellStyle.setDataFormat(datoFormat.getFormat("m/d/yy h:mm"));
        datocellStyle.setDataFormat(datoFormat.getFormat("dd.mm.yyyy HH:MM:SS"));
        datocellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
        datocellStyle.setFont(fontText);
        datocellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
        datocellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        return datocellStyle;
    }

    public final HSSFCellStyle getNumberStyle() {

        // lag Style tall-celler med borders rundt
        HSSFCellStyle tallcellStyle = workbook.createCellStyle();

        tallcellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        tallcellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        tallcellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        tallcellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        HSSFDataFormat tallFormat = workbook.createDataFormat();

        tallcellStyle.setDataFormat(tallFormat.getFormat("0.000"));
        tallcellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
        tallcellStyle.setFont(fontNumber);
        tallcellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
        tallcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        return tallcellStyle;
    }

    public final HSSFCellStyle getTextStyle() {

        // lag Style text-celler med borders rundt
        HSSFCellStyle textcellStyle = workbook.createCellStyle();

        textcellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        textcellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        textcellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        textcellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        textcellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
        textcellStyle.setFont(fontText);
        textcellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
        textcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        return textcellStyle;
    }

    public final HSSFCellStyle getReportHeadingStyle(HSSFFont font, 
                                                     short alignment) {
        HSSFCellStyle hcellStyle = workbook.createCellStyle();

        // font
        hcellStyle.setFont(font);

        // alignment
        hcellStyle.setAlignment(alignment);

        return hcellStyle;
    }

    public final HSSFCellStyle getCellStyle(String dataFormat, HSSFFont font, 
                                            short alignment, boolean fillColor, 
                                            short forgroundColor, 
                                            short fillPattern, boolean border, 
                                            short border_bottom, 
                                            short border_top, 
                                            short border_left, 
                                            short border_right) {
        HSSFCellStyle cellStyle = workbook.createCellStyle();

        cellStyle.setFont(font);
        cellStyle.setAlignment(alignment);

        if (fillColor) {
            cellStyle.setFillForegroundColor(forgroundColor);
            cellStyle.setFillPattern(fillPattern);
        }
    
        if (border) {
            cellStyle.setBorderBottom(border_bottom);
            cellStyle.setBorderLeft(border_left);
            cellStyle.setBorderRight(border_right);
            cellStyle.setBorderTop(border_top);
        }

        if (dataFormat.length() > 0) {
            HSSFDataFormat cellFormat = workbook.createDataFormat();
            cellStyle.setDataFormat(cellFormat.getFormat(dataFormat));
        }

        return cellStyle;
    }
    
    public final HSSFCellStyle copyCellStyle(HSSFCellStyle style){
        HSSFCellStyle newStyle = workbook.createCellStyle();
        newStyle.setFont(workbook.getFontAt(style.getFontIndex()));
        newStyle.setAlignment(style.getAlignment());
        newStyle.setFillForegroundColor(style.getFillForegroundColor());
        newStyle.setFillPattern(style.getFillPattern());
        newStyle.setBorderBottom(style.getBorderBottom());
        newStyle.setBorderLeft(style.getBorderLeft());
        newStyle.setBorderRight(style.getBorderRight());
        newStyle.setBorderTop(style.getBorderTop());
        newStyle.setDataFormat(style.getDataFormat());
        return newStyle;
    }
}


