package no.mesta.mipss.produksjonsrapport.adapter;


import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;


public class RapportStyle {
    protected HSSFCellStyle hReportcellStyle;
    
    protected HSSFCellStyle numberStyle;
    protected HSSFCellStyle numberStyleBlue;
    protected HSSFCellStyle numberStyleSum;
    
    protected HSSFCellStyle textStyleSum;
    protected HSSFCellStyle textStyle;
    protected HSSFCellStyle textStyleBlue;
    
    protected HSSFCellStyle datoStyle;
    protected HSSFCellStyle datoStyleBlue;
    protected HSSFCellStyle datoStyleSum;
    
    protected HSSFCellStyle tidStyle;
    protected HSSFCellStyle tidStyleBlue;
    protected HSSFCellStyle tidStyleSum;
    
    protected HSSFCellStyle kmtStyle;
    protected HSSFCellStyle kmtStyleBlue;
    protected HSSFCellStyle kmtStyleSum;
    
    protected HSSFCellStyle prosentRed;
    protected HSSFCellStyle prosentYellow;
    protected HSSFCellStyle prosentGreen;
    
    protected HSSFCellStyle prosentRedBlue;
    protected HSSFCellStyle prosentYellowBlue;
    protected HSSFCellStyle prosentGreenBlue;
    
    protected HSSFCellStyle frekvensStyle;
    protected HSSFCellStyle frekvensStyleBlue;
    protected HSSFCellStyle frekvensStyleSum;
    
    protected HSSFCellStyle hReportdatoStyle;
    protected HSSFCellStyle hReportTextStyle;
    protected HSSFCellStyle hReportTextStyleUnderline;
    protected HSSFCellStyle hReportDatoStyleUnderline;

    
    protected HSSFCellStyle hTablecellStyle;
    protected HSSFCellStyle hTablecellStyleGreen;
    protected HSSFCellStyle hReportNumberStyle;
    protected HSSFCellStyle hReportNumberStyleUnderline;
    
    protected HSSFCellStyle hReportPresicionNumberStyle;
    
    protected HSSFCellStyle redStyle ;
    protected HSSFCellStyle hyperlinkStyle;
    public HSSFCellStyle getTextStyle() {
		return textStyle;
	}

	protected HSSFCellStyle hyperlinkStylePlain;
    
    protected HSSFFont fontNumberRed;
    protected HSSFFont fontNumber;
    protected HSSFFont fontTextBlue;
    
    protected HSSFFont offKontraktFont;

    protected HSSFCellStyle dateStyle;

    protected HSSFCellStyle hTablecellStyleBorder;

	protected HSSFCellStyle hTablecellStyleYellow;

	private HSSFCellStyle numStyle;

	private HSSFCellStyle centerStyle;
    
    /* protected HSSFCellStyle blueStyle ;
    protected HSSFCellStyle greenStyle ;
     protected HSSFCellStyle yellowStyle;
     */
    
    public RapportStyle(HSSFWorkbook workbook) {
        ExcelStyles xStyle = new ExcelStyles(workbook);

        // lager fonter for rapporten      
        HSSFFont fontText = workbook.createFont();
        fontText.setFontHeightInPoints((short)10);
        fontText.setFontName("Arial");
        fontText.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        fontText.setColor(HSSFColor.BLACK.index);
        
        fontTextBlue = workbook.createFont();
        fontTextBlue.setFontHeightInPoints((short)10);
        fontTextBlue.setFontName("Arial");
        fontTextBlue.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        fontTextBlue.setColor(HSSFColor.BLUE.index);
        
        HSSFFont fontHyperlink = workbook.createFont();
        fontHyperlink.setFontHeightInPoints((short)10);
        fontHyperlink.setFontName("Arial");
        fontHyperlink.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        fontHyperlink.setUnderline(HSSFFont.U_SINGLE);
        fontHyperlink.setColor(HSSFColor.BLUE.index);

        HSSFFont fontHReport = workbook.createFont();
        fontHReport.setFontHeightInPoints((short)10);
        fontHReport.setFontName("Arial");
        fontHReport.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        fontHReport.setColor(HSSFColor.BLACK.index);

        HSSFFont hfontReport = workbook.createFont();
        hfontReport.setFontHeightInPoints((short)14);
        hfontReport.setFontName("Arial");
        hfontReport.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        hfontReport.setColor(HSSFColor.BLACK.index);

        // lag font for tall
        fontNumber = workbook.createFont();
        fontNumber.setFontHeightInPoints((short)10);
        fontNumber.setFontName("Arial");
        fontNumber.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        fontNumber.setColor(HSSFColor.BLACK.index);
        
        // lag font for røde tall
        fontNumberRed = workbook.createFont();
        fontNumberRed.setFontHeightInPoints((short)10);
        fontNumberRed.setFontName("Arial");
        fontNumberRed.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        fontNumberRed.setColor(HSSFColor.RED.index);
        
        offKontraktFont = workbook.createFont();
        offKontraktFont.setFontHeightInPoints((short)10);
        offKontraktFont.setFontName("Arial");
        offKontraktFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        offKontraktFont.setColor(HSSFColor.BLUE.index);
        
        
        HSSFFont hfontTable = workbook.createFont();
        hfontTable.setFontHeightInPoints((short)12);
        hfontTable.setFontName("Arial");
        hfontTable.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        hfontTable.setColor(HSSFColor.WHITE.index);

        HSSFFont hfontTableBold = workbook.createFont();
        hfontTableBold.setFontHeightInPoints((short)12);
        hfontTableBold.setFontName("Arial");
        hfontTableBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        hfontTableBold.setColor(HSSFColor.WHITE.index);
        
        
        // styles for heading
        hReportcellStyle = xStyle.getReportHeadingStyle(hfontReport, HSSFCellStyle.ALIGN_LEFT);

        
        redStyle =xStyle.getCellStyle("0", fontNumberRed, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        

        /*blueStyle = xStyle.getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        greenStyle = xStyle.getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.GREEN.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);   
                                    
        yellowStyle = xStyle.getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.YELLOW.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);                                      
        */                      
        numberStyle = xStyle.getCellStyle("0.000", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        centerStyle = xStyle.getCellStyle("0.000", fontNumber, HSSFCellStyle.ALIGN_CENTER, 
                true, HSSFColor.WHITE.index, 
                HSSFCellStyle.SOLID_FOREGROUND, true, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN);
        
        numStyle = xStyle.getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_LEFT, true, 
					                HSSFColor.WHITE.index, 
					                HSSFCellStyle.SOLID_FOREGROUND, true, 
					                HSSFCellStyle.BORDER_THIN, 
					                HSSFCellStyle.BORDER_THIN, 
					                HSSFCellStyle.BORDER_THIN, 
					                HSSFCellStyle.BORDER_THIN);
        
        numberStyleBlue = xStyle.getCellStyle("0.000", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        kmtStyle = xStyle.getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        kmtStyleBlue = xStyle.getCellStyle("0", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        prosentRed = xStyle.getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.PLUM.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        prosentYellow = xStyle.getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.YELLOW.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        prosentGreen = xStyle.getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.LIGHT_GREEN.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        prosentRedBlue = xStyle.getCellStyle("0", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.PLUM.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        prosentYellowBlue = xStyle.getCellStyle("0", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.YELLOW.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        prosentGreenBlue = xStyle.getCellStyle("0", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.LIGHT_GREEN.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        frekvensStyle = xStyle.getCellStyle("0.0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        frekvensStyleBlue = xStyle.getCellStyle("0.0", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        kmtStyleSum = xStyle.getCellStyle("0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_DOUBLE, 
                                    HSSFCellStyle.BORDER_THICK, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        numberStyleSum = xStyle.getCellStyle("0.000", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_DOUBLE, 
                                    HSSFCellStyle.BORDER_THICK, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        frekvensStyleSum = xStyle.getCellStyle("0.0", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_DOUBLE, 
                                    HSSFCellStyle.BORDER_THICK, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        textStyleSum = xStyle.getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_DOUBLE, 
                                    HSSFCellStyle.BORDER_THICK, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);

        textStyle = xStyle.getCellStyle("", fontText, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        textStyleBlue = xStyle.getCellStyle("", fontTextBlue, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        hyperlinkStyle = xStyle.getCellStyle("", fontHyperlink, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        hyperlinkStylePlain = xStyle.getCellStyle("", fontHyperlink, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
                                    
        datoStyle = xStyle.getCellStyle("dd.mm.yyyy hh:mm", fontText, HSSFCellStyle.ALIGN_LEFT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        dateStyle = xStyle.getCellStyle("dd.mm.yyyy", fontText, HSSFCellStyle.ALIGN_LEFT, 
                true, HSSFColor.WHITE.index, 
                HSSFCellStyle.SOLID_FOREGROUND, true, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN);

        datoStyleBlue = xStyle.getCellStyle("dd.mm.yyyy hh:mm", fontTextBlue, HSSFCellStyle.ALIGN_LEFT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        datoStyleSum = xStyle.getCellStyle("dd.mm.yyyy hh:mm", fontText, HSSFCellStyle.ALIGN_LEFT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_DOUBLE, 
                                    HSSFCellStyle.BORDER_THICK, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);

        tidStyle = xStyle.getCellStyle("[hh]:mm:ss", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
        
        tidStyleBlue = xStyle.getCellStyle("[hh]:mm:ss", offKontraktFont, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);
                                    
        tidStyleSum = xStyle.getCellStyle("[hh]:mm:ss", fontNumber, HSSFCellStyle.ALIGN_RIGHT, 
                                    true, HSSFColor.WHITE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_DOUBLE, 
                                    HSSFCellStyle.BORDER_THICK, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_THIN);

        // style for dato
        hReportdatoStyle = xStyle.getCellStyle("dd.mm.yyyy hh:mm", fontHReport, HSSFCellStyle.ALIGN_LEFT, 
                                    true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
        
        hReportDatoStyleUnderline = xStyle.getCellStyle("dd.mm.yyyy hh:mm", fontHReport, HSSFCellStyle.ALIGN_LEFT, 
                                    true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
                                    
        hReportTextStyle = xStyle.getCellStyle("dd.mm.yyyy", fontHReport, HSSFCellStyle.ALIGN_LEFT, 
                                    true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
        
        hReportTextStyleUnderline = xStyle.getCellStyle("dd.mm.yyyy", fontHReport, HSSFCellStyle.ALIGN_LEFT, 
                                    true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
                                
        hReportNumberStyle = xStyle.getCellStyle("0.0", fontHReport, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
                                    
        hReportNumberStyleUnderline = xStyle.getCellStyle("0.0", fontHReport, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);                                    
    
        hReportPresicionNumberStyle = xStyle.getCellStyle("0.000", fontHReport, HSSFCellStyle.ALIGN_LEFT, true, 
                                    HSSFColor.LIGHT_CORNFLOWER_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE);
        
        // style for text getCellStyle(String dataFormat, HSSFFont font, short alignment, boolean fillColor, short forgroundColor, short fillPattern, boolean border, short border_bottom, short border_top, short border_left, short border_right)

        hTablecellStyle = xStyle.getCellStyle("", hfontTable, HSSFCellStyle.ALIGN_CENTER, 
                                    true, HSSFColor.DARK_BLUE.index, 
                                    HSSFCellStyle.SOLID_FOREGROUND, true, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_NONE, 
                                    HSSFCellStyle.BORDER_THIN, 
                                    HSSFCellStyle.BORDER_NONE);
                                    
        hTablecellStyle.setLeftBorderColor(HSSFColor.WHITE.index);
        
        hTablecellStyleGreen = xStyle.getCellStyle("", hfontTable, HSSFCellStyle.ALIGN_CENTER, 
                true, HSSFColor.GREEN.index, 
                HSSFCellStyle.SOLID_FOREGROUND, true, 
                HSSFCellStyle.BORDER_NONE, 
                HSSFCellStyle.BORDER_NONE, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_NONE);
        
        hTablecellStyleYellow = xStyle.getCellStyle("", hfontTable, HSSFCellStyle.ALIGN_CENTER, 
                true, HSSFColor.YELLOW.index, 
                HSSFCellStyle.SOLID_FOREGROUND, true, 
                HSSFCellStyle.BORDER_NONE, 
                HSSFCellStyle.BORDER_NONE, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_NONE);
        
        
        hTablecellStyleBorder = xStyle.getCellStyle("", hfontTableBold, HSSFCellStyle.ALIGN_LEFT, 
                true, HSSFColor.DARK_BLUE.index, 
                HSSFCellStyle.SOLID_FOREGROUND, true, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN, 
                HSSFCellStyle.BORDER_THIN);
    }
    
    public HSSFCellStyle getNumberStyle() {
		return numberStyle;
	}
    public HSSFCellStyle getCenterStyle() {
		return centerStyle;
	}

	public HSSFCellStyle gethReportcellStyle() {
		return hReportcellStyle;
	}

	public HSSFCellStyle getNumberStyleSum(){
        return numberStyleSum;
    }

	public HSSFCellStyle getDatoStyle() {
		return datoStyle;
	}
	
	public HSSFCellStyle getTidStyle() {
		return tidStyle;
	}

	public HSSFCellStyle gethReportTextStyle() {
		return hReportTextStyle;
	}

	public HSSFCellStyle gethTablecellStyle() {
		return hTablecellStyle;
	}

	public HSSFCellStyle getNumStyle() {
		return numStyle;
	}

	public HSSFCellStyle getTidStyleSum() {
		return tidStyleSum;
	}
}
