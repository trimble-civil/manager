package no.mesta.mipss.produksjonsrapport.values;

import java.util.Hashtable;


public class TiltakMengde extends TiltakObject {
    private String enhet;
    private double mengde;
    
    
    public void addSubtype(TiltakObject obj, boolean total) {
        if (tiltakListe==null){
            tiltakListe = new Hashtable();
        }
        tiltakListe.put(obj.getNavn(), obj);
        addGlobalTiltak(obj);
    }
    
    public void setMengde(double mengde) {
        this.mengde = mengde;
    }

    public double getMengde() {
        return mengde;
    }

    public void setEnhet(String enhet) {
        this.enhet = enhet;
    }

    public String getEnhet() {
        return enhet;
    }
    public int hashCode(){
    	return getNavn().hashCode();
    }
    public boolean equals(Object o){
    	if (!(o instanceof TiltakMengde))
    		return false;
    	if (o==this)
    		return true;
    	TiltakMengde other = (TiltakMengde) o;
    	return other.getNavn().equals(getNavn());
    }
}


