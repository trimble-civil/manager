package no.mesta.mipss.produksjonsrapport.query;

import no.mesta.mipss.persistence.rapportfunksjoner.LeverandorVinterdrift;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;

/**
 * Brukes i sammenheng med Vinterdrift for å finne leverandørnavn og -nummer.
 */
public class LeverandorForVinterdriftQuery {

	private Query query;

	public LeverandorForVinterdriftQuery(Long kontraktId, Date fraDato, Date tilDato, int kjoretoyId, EntityManager em) {
		StringBuilder q = new StringBuilder();

		q.append("SELECT");
		q.append("   leverandor_navn,");
		q.append("   leverandor_nr ");

		q.append("FROM TABLE(KJORETOY_PK.kontrakt_kjoretoy_info(?1, ?2, ?3)) ");

		q.append("WHERE");
		q.append("   kjoretoy_id = ?4");

		query = em.createNativeQuery(q.toString())
				.setParameter(1, kontraktId)
				.setParameter(2, fraDato)
				.setParameter(3, tilDato)
				.setParameter(4, kjoretoyId);
	}

	public LeverandorVinterdrift getLeverandor() {
		NativeQueryWrapper<LeverandorVinterdrift> qry = new NativeQueryWrapper<LeverandorVinterdrift>(
				query,
				LeverandorVinterdrift.class,
				LeverandorVinterdrift.getPropertiesArray());

		return qry.getWrappedSingleResult();
	}

}