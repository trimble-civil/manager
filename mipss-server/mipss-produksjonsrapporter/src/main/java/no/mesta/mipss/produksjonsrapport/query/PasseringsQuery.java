package no.mesta.mipss.produksjonsrapport.query;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.rapportfunksjoner.Passering;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.query.NativeQueryWrapper;

public class PasseringsQuery {
	private Query query;
	private final EntityManager em;
	private final ProdrapportParams params; 
	
	public PasseringsQuery(ProdrapportParams params, EntityManager em){
		this.params = params;
		this.em = em;
		
		StringBuilder q  = new StringBuilder();
		
		q.append("SELECT kjoretoy_id, kjoretoy_navn, leverandor_nr, leverandor_navn, ansvarlig_kontrakt_flagg, retning, tidspunkt, prodtype_id, prodtype_navn, stroprodukt_id, stroprodukt_navn FROM table(rapport_funksjoner.passerings_rapp(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, to_date(?9, 'dd.mm.yyyy hh24:mi:ss'), to_date(?10, 'dd.mm.yyyy hh24:mi:ss')))");
		
		query = em.createNativeQuery(q.toString())
		.setParameter(1, params.getDriftkontraktId())
		.setParameter(2, params.getFylkesnummer())
		.setParameter(3, params.getKommunenummer())
		.setParameter(4, params.getVeikategori())
		.setParameter(5, params.getVeistatus())
		.setParameter(6, params.getVeinummer())
		.setParameter(7, params.getHp())
		.setParameter(8, params.getKm())
		.setParameter(9, MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT))
		.setParameter(10, MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
	}

	public List<Passering> getResults(){
		NativeQueryWrapper<Passering> qry = new NativeQueryWrapper<Passering>(
    			query, 
    			Passering.class, 
    			Passering.getPropertiesArray());
    	qry.setDateMask("yyyy-MM-dd HH:mm:ss");
    	List<Passering> result = qry.getWrappedList();

    	return result; //Returner listen med data
	}
}
