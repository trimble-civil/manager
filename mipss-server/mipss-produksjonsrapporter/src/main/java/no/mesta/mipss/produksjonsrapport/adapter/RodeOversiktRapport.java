package no.mesta.mipss.produksjonsrapport.adapter;


import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.produksjonsrapport.values.EntityCopy;
import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportHelper;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningObject;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningSequence;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.Region;


/**
 * MIPSS1 port
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RodeOversiktRapport extends RapportStyle{
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");

    public RodeOversiktRapport(HSSFWorkbook workbook){
        super(workbook);
    }
    
    public int createForsideReportHeader(HSSFSheet sheet, RodeStrekningObject rodeObject, String roder){
        int rowCount = 0;
        HSSFRow fRow = sheet.createRow(rowCount++);
        fRow.createCell((short)0).setCellValue(new HSSFRichTextString("Rodeoversikt, forside"));
        fRow.getCell((short)0).setCellStyle(hReportcellStyle);
        
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        fRow = sheet.createRow(rowCount++);
        fRow.createCell((short)0).setCellValue(new HSSFRichTextString("Kontrakt:"));
        fRow.getCell((short)0).setCellStyle(hReportTextStyle);
        fRow.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getKontraktNR()+ " - "+rodeObject.getKontraktNavn()));
        fRow.getCell((short)1).setCellStyle(hReportTextStyle);
        
        if (roder.length()==0){
            roder = rodeObject.getRodeNavn();
        }
        
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        fRow = sheet.createRow(rowCount++);
        fRow.createCell((short)0).setCellValue(new HSSFRichTextString("Rode:"));
        fRow.getCell((short)0).setCellStyle(hReportTextStyleUnderline);
        fRow.createCell((short)1).setCellValue(new HSSFRichTextString(roder));
        fRow.getCell((short)1).setCellStyle(hReportTextStyleUnderline);
        
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        fRow = sheet.createRow(rowCount++);
        fRow.createCell((short)0).setCellValue(new HSSFRichTextString("Sum rodelengde:"));
        fRow.getCell((short)0).setCellStyle(hReportTextStyle);
        fRow.createCell((short)1);
        fRow.getCell((short)1).setCellStyle(hReportNumberStyle);
        
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        fRow = sheet.createRow(rowCount++);
        fRow.createCell((short)0).setCellValue(new HSSFRichTextString("Rapportdato:"));
        fRow.getCell((short)0).setCellStyle(hReportTextStyle);
        fRow.createCell((short)1).setCellValue(new HSSFRichTextString(KontraktRapportHelper.getDatoTid()));
        fRow.getCell((short)1).setCellStyle(hReportdatoStyle);
        
        rowCount++;
        
        return rowCount;
    }
    
    
    public int createRodeOversiktReportHeader(HSSFWorkbook workbook, 
                                              HSSFSheet sheet, int rowCount, 
                                              EntityCopy object, HSSFSheet arkForside, int forsideRowCount, String arkNavn) {
                                              
        int forsideColumnIndex = 0;
        RodeStrekningObject rodeObject = (RodeStrekningObject)object;
        
        // lag rapport tittel rad
        HSSFRow row1 = sheet.createRow(rowCount++);
        row1.createCell((short)0).setCellValue(new HSSFRichTextString("Rodeoversikt, detaljark"));
        row1.getCell((short)0).setCellStyle(hReportcellStyle);

        // Kontrakt
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        HSSFRow row2 = sheet.createRow(rowCount++);
        row2.createCell((short)0).setCellValue(new HSSFRichTextString("Kontrakt: "));
        row2.getCell((short)0).setCellStyle(hReportTextStyle);
        row2.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getKontraktNR() + " - " + rodeObject.getKontraktNavn()));
        row2.getCell((short)1).setCellStyle(hReportTextStyle);

        // Rode
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        HSSFRow row3 = sheet.createRow(rowCount++);
        row3.createCell((short)0).setCellValue(new HSSFRichTextString("Rode: "));
        row3.getCell((short)0).setCellStyle(hReportTextStyleUnderline);
        row3.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getRodeNavn()));
        row3.getCell((short)1).setCellStyle(hReportTextStyleUnderline);
        
        HSSFRow fRow = arkForside.createRow((short)forsideRowCount++);
        String target = "\"#" + arkNavn + "!A1\"";
        String label = "\"" + rodeObject.getRodeNavn() + "\"";
        String formula = "HYPERLINK(" + target + ", " + label + ")";
        fRow.createCell((short)forsideColumnIndex).setCellFormula(formula);
        fRow.getCell((short)forsideColumnIndex).setCellStyle(hyperlinkStyle);
        forsideColumnIndex++;
        
        // Gyldig fra
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        HSSFRow row4 = sheet.createRow(rowCount++);
        row4.createCell((short)0).setCellValue(new HSSFRichTextString("RodeGyldigFra: "));
        row4.getCell((short)0).setCellStyle(hReportTextStyle);
        row4.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getRodeDatoGyldigFra().split(" ")[0]));
        row4.getCell((short)1).setCellStyle(hReportTextStyle);
        
        //forside
        HSSFCell gyldigFraCell = fRow.createCell((short)forsideColumnIndex);
        gyldigFraCell.setCellValue(new HSSFRichTextString(rodeObject.getRodeDatoGyldigFra().split(" ")[0]));
        gyldigFraCell.setCellStyle(datoStyle);
        forsideColumnIndex++;

        // Gyldig til
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3)); 
        HSSFRow row5 = sheet.createRow(rowCount++);
        row5.createCell((short)0).setCellValue(new HSSFRichTextString("RodeGyldigTil: "));
        row5.getCell((short)0).setCellStyle(hReportTextStyle);
        row5.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getRodeDatoGyldigTil().split(" ")[0]));
        row5.getCell((short)1).setCellStyle(hReportTextStyle);
        
        //forside
        HSSFCell gyldigTilCell = fRow.createCell((short)forsideColumnIndex);
        gyldigTilCell.setCellValue(new HSSFRichTextString(rodeObject.getRodeDatoGyldigTil().split(" ")[0]));
        gyldigTilCell.setCellStyle(datoStyle);
        forsideColumnIndex++;
        
         // Rodetype
         sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3)); 
         HSSFRow  row6 = sheet.createRow(rowCount++);
         row6.createCell((short)0).setCellValue(new HSSFRichTextString("Rodetype: "));
         row6.getCell((short)0).setCellStyle(hReportTextStyle);
         row6.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getRodeType()));
         row6.getCell((short)1).setCellStyle(hReportTextStyle);
         
         //forside
         HSSFCell rodeTypeCell = fRow.createCell((short)forsideColumnIndex);
         rodeTypeCell.setCellValue(new HSSFRichTextString(rodeObject.getRodeType()));
         rodeTypeCell.setCellStyle(textStyle);
         forsideColumnIndex++;
        
         // Rodelengde
         sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3)); 
         row6 = sheet.createRow(rowCount++);
         row6.createCell((short)0).setCellValue(new HSSFRichTextString("Rodelengde: "));
         row6.getCell((short)0).setCellStyle(hReportNumberStyle);
         row6.createCell((short)1).setCellValue((Double.valueOf(rodeObject.getRodeLengde()).doubleValue()));
         row6.getCell((short)1).setCellStyle(hReportNumberStyle);
        
         //forside
         HSSFCell rodeLengdeCell = fRow.createCell((short)forsideColumnIndex);
         rodeLengdeCell.setCellValue((Double.valueOf(rodeObject.getRodeLengde()).doubleValue()));
         rodeLengdeCell.setCellStyle(numberStyle);
         forsideColumnIndex++;
        
         //Beskrivelse
         sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3)); 
         row6 = sheet.createRow(rowCount++);
         row6.createCell((short)0).setCellValue(new HSSFRichTextString("Beskrivelse: "));
         row6.getCell((short)0).setCellStyle(hReportTextStyle);
         row6.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getBeskrivelse()));
         row6.getCell((short)1).setCellStyle(hReportTextStyle);
         row6.getCell((short)1).getCellStyle().setWrapText(true);
          
         //forside
         HSSFCell beskrivelseCell = fRow.createCell((short)forsideColumnIndex);
         beskrivelseCell.setCellValue(new HSSFRichTextString(rodeObject.getBeskrivelse()));
         beskrivelseCell.setCellStyle(textStyle);
         beskrivelseCell.getCellStyle().setWrapText(true);
         forsideColumnIndex++;
         
         //forside
         HSSFCell leverandorerCell = fRow.createCell((short)forsideColumnIndex);
         leverandorerCell.setCellValue(new HSSFRichTextString(rodeObject.getLeverandorer()));
         leverandorerCell.setCellStyle(textStyle);
         leverandorerCell.getCellStyle().setWrapText(true);
         forsideColumnIndex++;
         
         //Leverandører
         sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3)); 
         HSSFRow row7 = sheet.createRow(rowCount++);
         row7.createCell((short)0).setCellValue(new HSSFRichTextString(props.getGuiString("leverandorer")));
         row7.getCell((short)0).setCellStyle(hReportTextStyle);
         row7.getCell((short)0).getCellStyle().setWrapText(true);
         row7.createCell((short)1).setCellValue(new HSSFRichTextString(rodeObject.getLeverandorer()));
         row7.getCell((short)1).setCellStyle(hReportTextStyle);
         row7.getCell((short)1).getCellStyle().setWrapText(true);
         
         //Rapportdato
         sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3)); 
         HSSFRow row8 = sheet.createRow(rowCount++);
         row8.createCell((short)0).setCellValue(new HSSFRichTextString("Rapportdato: "));
         row8.getCell((short)0).setCellStyle(hReportTextStyle);
         row8.createCell((short)1).setCellValue(new HSSFRichTextString(KontraktRapportHelper.getDatoTid()));
         row8.getCell((short)1).setCellStyle(hReportdatoStyle);
         

         
        HSSFRow forsideLinkRow = sheet.createRow(rowCount++);
        HSSFCell forsideLink = forsideLinkRow.createCell((short)0);
        String forsideTarget = "\"#forside!A1\"";
        String forsideLabel = "\"Til forsiden\"";
        formula = "HYPERLINK(" + forsideTarget + ", " + forsideLabel + ")";
        forsideLink.setCellFormula(formula);
        forsideLink.setCellStyle(hyperlinkStylePlain);
        
        return rowCount;
    }
    
    public int createForsideTableHeader(HSSFSheet arkForside, int rowCount){
        int index = 0;
        HSSFRow headerRow = arkForside.createRow(rowCount);
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString("Rodenavn"));
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString("Gyldig fra"));
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString("Gyldig til"));
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString("Rodetype"));
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString("Rodelengde"));
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString("Beskrivelse"));
        headerRow.createCell((short)index++).setCellValue(new HSSFRichTextString(props.getGuiString("leverandorer")));
        
        for (int i = 0; i < index; i++) {
            HSSFCell cell = headerRow.getCell((short)i);
            cell.setCellStyle(hTablecellStyle);
        }
        rowCount++;
        return rowCount;
    }
    
    public int createRodeOversiktTableHeader(HSSFWorkbook workbook, 
                                             HSSFSheet sheet, int rowCount) {
        boolean fillColor = true;
        boolean haveBorders = true;
        
        HSSFRow headerRow = sheet.createRow(rowCount);

        // lag header celler
        headerRow.createCell((short)0).setCellValue(new HSSFRichTextString("Fylke"));
        headerRow.createCell((short)1).setCellValue(new HSSFRichTextString("Kommunenr"));
        headerRow.createCell((short)2).setCellValue(new HSSFRichTextString("Vei"));
        headerRow.createCell((short)3).setCellValue(new HSSFRichTextString("Hovedparsell"));
        headerRow.createCell((short)4).setCellValue(new HSSFRichTextString("KmFra"));
        headerRow.createCell((short)5).setCellValue(new HSSFRichTextString("KmTil"));
        
        for (int i = 0; i < 6; i++) {
            HSSFCell cell = headerRow.getCell((short)i);
            cell.setCellStyle(hTablecellStyle);
        }

        // returnerer neste rad nummer i workbook
        return rowCount++;
    }

    void createRodeOversiktDataRows(HSSFWorkbook workbook, HSSFSheet sheet, 
                                    IndexedSequence sequence, int rowCount) {
        // iterer data sequence og legg data i workbook
        RodeStrekningSequence result = (RodeStrekningSequence)sequence;

        for (int i = 0; i < result.size(); i++) {
            HSSFRow dataRow = sheet.createRow(rowCount);
            final RodeStrekningObject each = result.elementAt(i);

            createRow(dataRow, each, datoStyle, textStyle, numberStyle);
            rowCount++;
        }
    }

    private void createRow(HSSFRow dataRow, RodeStrekningObject rowItem, 
                           HSSFCellStyle datoStyle, HSSFCellStyle textStyle, 
                           HSSFCellStyle numberStyle) {
        int index = 0;
        
        HSSFCell fylkesNavn = dataRow.createCell((short)index++);
        fylkesNavn.setCellValue(new HSSFRichTextString(rowItem.getFylkesNR() + " - " + rowItem.getFylkesNavn()));
        fylkesNavn.setCellStyle(textStyle);
        
        HSSFCell kommuneNummer = dataRow.createCell((short)index++);
        kommuneNummer.setCellValue(new HSSFRichTextString(rowItem.getKommuneNR()));
        kommuneNummer.setCellStyle(textStyle);
        
        HSSFCell vei = dataRow.createCell((short)index++);
        vei.setCellValue(new HSSFRichTextString(rowItem.getVeiKategori() + rowItem.getVeiStatus() + rowItem.getVeiNR()));
        vei.setCellStyle(textStyle);
        
        HSSFCell hparsell = dataRow.createCell((short)index++);
        hparsell.setCellValue(new HSSFRichTextString(rowItem.getHparsell()));
        hparsell.setCellStyle(textStyle);

        HSSFCell kmFra = dataRow.createCell((short)index++);
        // kmFra.setCellValue(rowItem.getKmFra());
        kmFra.setCellValue(Double.valueOf(rowItem.getKmFra()).doubleValue());
        kmFra.setCellStyle(textStyle);

        HSSFCell kmTil = dataRow.createCell((short)index++);
        // kmTil.setCellValue(rowItem.getKmTil());
        kmTil.setCellValue(Double.valueOf(rowItem.getKmTil()).doubleValue());
        kmTil.setCellStyle(textStyle);
    }
}