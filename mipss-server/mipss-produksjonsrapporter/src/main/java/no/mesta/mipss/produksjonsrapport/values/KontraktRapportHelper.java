package no.mesta.mipss.produksjonsrapport.values;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------


public class KontraktRapportHelper {

    /**
     * Returnerer en liste med KontraktRapportSequence objekter. Denne listen
     * er sortert på regnr. Det opprettes en ny KontraktRapportSequence for hvert
     * nye regnr i result listen. 
     * 
     * KontraktRapportSequence result -må være sortert på regnr. 
     * Følgende Lister :
     * A,B,B,A,C,C vil gi 4 KontraktRapportSequence objekter i ArrayListen
     * A,A,B,B,C,C vil gi 3 KontraktRapportSequence objekter i ArrayListen
     * 
     * @param result
     * @param globalDynamicHeaderList
     * @return
     */
    @SuppressWarnings("unchecked")
	public static List<KontraktRapportSequence> getKontraktLists(KontraktRapportSequence result, TiltakSequence globalDynamicHeaderList) throws Exception{
        String enhetID = "";
        ArrayList<KontraktRapportSequence> kontraktList = new ArrayList<KontraktRapportSequence>();
        KontraktRapportSequence sequence = new KontraktRapportSequence(globalDynamicHeaderList);
        KontraktRapportObject object = null;

        // Henter ut første regnr fra liste (som er sortert på regnr)
        if ((result != null) && (result.size() > 0)) {
            enhetID = ((KontraktRapportObject)result.getAt(0)).getEnhetsID();
        }
        for (int i = 0; i < result.size(); i++) {
            object = (KontraktRapportObject)result.getAt(i);
            
            String enhet_ID = object.getEnhetsID();

            if (!enhet_ID.equals(enhetID)) {

                // nytt regnr, opprett ny liste for dette kjøretøyet
                enhetID = enhet_ID;
                kontraktList.add(sequence);
                sequence = new KontraktRapportSequence(globalDynamicHeaderList);
                // test
                //object.createTiltakFromXML();
                //sequence.updateTiltakTypeListForEnhet(object, object.getEnhetsID());
            }
            if (enhet_ID.equals(enhetID)) {
                // test
                object.createTiltakFromXML();
                sequence.updateTiltakTypeListForEnhet(object, object.getEnhetsID());
                // fortsattt samme regnr, legg dette objektet til gjeldende liste
                sequence.add(object);
            }
        }
        globalDynamicHeaderList.sort(new TiltakComparator());

        // add last element
        kontraktList.add(sequence);

        return kontraktList;
    }

    public static String replaceInvalidCharacters(String tekstStreng) {
        char charList[] = { '/', '*', '?', '[', ']', '\\' };

        for (int i = 0; i < charList.length; i++) {
            tekstStreng = tekstStreng.replace(charList[i], ' ');
        }

        return tekstStreng;
    }

    public static String parseString(String tekstStreng) {
        String tekst = tekstStreng.toUpperCase();
        StringBuilder newTekst = new StringBuilder();
        for (int i=0;i<tekst.length();i++){
            char tegn = tekst.charAt(i);
            //tillat kun 0-9 og A-Z
            if ((tegn>47 && tegn<58) || (tegn>64 && tegn<91)){ 
                newTekst.append(""+tegn);
            }
        }
        /*
        for (int i = 0; i < tekstStreng.length(); i++) {
            char tegn = tekstStreng.charAt(i);
            if (!((47 < tegn) && (tegn < 58)) && !((64 < tegn) && (tegn < 91))) {

                // tekst = tekst.replaceAll(String.valueOf(String.valueOf(tegn)), "");
                if (tegn!=197)
                    tekst = tekst.replace(tegn, ' ');
                tekst = tekst.replaceAll(" ", "");
            }
        }
        */
      /*  if (tekst.length() > 15) {
            tekst = tekst.substring(0, 14);
        }
*/
        // M - maskin
        if (isNumeric(newTekst.toString())) {
            //tekst = "M" + tekst;
            newTekst.insert(0, 'M');
        }
        if (newTekst.length()>29)
            newTekst.setLength(29);
        
        
        return newTekst.toString();
        //return tekst;
    }

    public static boolean isNumeric(String str) {
        try {
            Long.parseLong(str);

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static String getDatoTid() {
        DateFormat datoFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date dato = new Date();

        return datoFormat.format(dato);
    }
}


