package no.mesta.mipss.produksjonsrapport.excel;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import no.mesta.mipss.persistence.rapportfunksjoner.Passering;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;

public class Passeringsrapport extends Rapp1Rapport{

	private final List<Passering> data;
	private String templateFile = "passeringsrapport";
	
	private HashMap<String, Object> map;
	
	public Passeringsrapport(List<Passering> results, ProdrapportParams params) {
		super(params);
		this.data = results;
		initData();
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 5;
		startRow = 6;
		autoFilterRow=10;
	}

	private void initData() {
		Collections.sort(data, new Comparator<Passering>() {
		    public int compare(Passering m1, Passering m2) {
		        return m1.getTidspunkt().compareTo(m2.getTidspunkt());
		    }
		});
		map = new HashMap<String, Object>();
		map.put("passeringer", data);
		map.put("params", params);
	}

	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		// TODO Auto-generated method stub
		
	}
}
