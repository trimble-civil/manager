package no.mesta.mipss.produksjonsrapport.adapter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportSequence;
import no.mesta.mipss.produksjonsrapport.values.TiltakMengde;
import no.mesta.mipss.produksjonsrapport.values.TiltakObject;
import no.mesta.mipss.produksjonsrapport.values.TiltakProduksjon;
import no.mesta.mipss.produksjonsrapport.values.TiltakSequence;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.Region;

public abstract class KontraktRapport extends RapportStyle{

    protected HSSFWorkbook workbook;
    protected PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");
	private final ProdrapportParams params;
    
    public KontraktRapport(HSSFWorkbook workbook, ProdrapportParams params) {
        super(workbook);
        this.workbook = workbook;
		this.params = params;
        
    }

    /**
     * Lager headeren til rapporten
     * 
     * @param sheet excelarket der denne headeren skal lages
     * @param rapportNavn navnet på rapporten
     * @param kontrakt hvilken kontrakt dette gjelder
     * @param rodeTypeNavn navn på rodetypen
     * @param rodeNavn navn på rode
     * @param rodeLengde lengde på rode
     * @param rapportDato datoen denne rapporten blir generert ex: 16.08.2007 13:20
     * @param periode hvilken periode rapporten gjelder for, ex : 01.01.2007 - 31.01.2007
     * @param rowCount teller for hvilken rad som skal lages
     * @return neste rad index
     */
    public int createReportHeader(HSSFSheet sheet, String rapportNavn, 
                                  String kontrakt, String rodeTypeNavn, 
                                  String rodeNavn, String rodeLengde, 
                                  String rapportDato, String periode, 
                                  int rowCount) {
    
        // lag rapport tittel rad
        sheet.addMergedRegion(new Region(rowCount, (short)0, rowCount, (short)3));

        HSSFRow row1 = sheet.createRow(rowCount++);
        row1.createCell((short)0).setCellValue(new HSSFRichTextString(rapportNavn));
        row1.getCell((short)0).setCellStyle(hReportcellStyle);
        int si = workbook.getSheetIndex("Forside");
        if (si!=-1){
            HSSFSheet forside = workbook.getSheetAt(si);
            if (!sheet.equals(forside)){
            	if (!params.getKunKontraktKjoretoy()){
	                row1 = sheet.createRow(rowCount++);
	                HSSFRichTextString text = new HSSFRichTextString(resources.getGuiString("KontraktRapport.forsideInfoText"));
	                text.applyFont(fontTextBlue);
	                row1.createCell((short)0).setCellValue(text);
	                row1.getCell((short)0).setCellStyle(textStyle);
            	}else{
            		rowCount++;
            	}
            }else{
                rowCount++;
            }
        }
        else{
            rowCount++;
        }
        //Kontrakt

        //int rowFrom, short colFrom, int rowTo, short colTo
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));

        HSSFRow row2 = sheet.createRow(rowCount++);
        row2.createCell((short)0).setCellValue(new HSSFRichTextString("Kontrakt: "));
        row2.getCell((short)0).setCellStyle(hReportTextStyle);
        row2.createCell((short)1).setCellValue(new HSSFRichTextString(kontrakt));
        row2.getCell((short)1).setCellStyle(hReportTextStyle);

        //Rodetype
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));

        HSSFRow row3 = sheet.createRow(rowCount++);
        row3.createCell((short)0).setCellValue(new HSSFRichTextString("Rodetype: "));
        row3.getCell((short)0).setCellStyle(hReportTextStyle);
        row3.createCell((short)1).setCellValue(new HSSFRichTextString(rodeTypeNavn));
        row3.getCell((short)1).setCellStyle(hReportTextStyle);

        //Rode
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));

        HSSFRow row4 = sheet.createRow(rowCount++);
        row4.createCell((short)0).setCellValue(new HSSFRichTextString("Rode: "));
        row4.getCell((short)0).setCellStyle(hReportTextStyle);
        row4.createCell((short)1).setCellValue(new HSSFRichTextString(rodeNavn));
        row4.getCell((short)1).setCellStyle(hReportTextStyle);

        

        //Periode
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));
        
        HSSFRow row6 = sheet.createRow(rowCount++);
        row6.createCell((short)0).setCellValue(new HSSFRichTextString("Periode: "));
        row6.getCell((short)0).setCellStyle(hReportTextStyleUnderline); 
        row6.createCell((short)1).setCellValue(new HSSFRichTextString(periode));
        row6.getCell((short)1).setCellStyle(hReportTextStyleUnderline);
        
        //Rapportdato
        sheet.addMergedRegion(new Region(rowCount, (short)1, rowCount, (short)3));

        HSSFRow row5 = sheet.createRow(rowCount++);
        row5.createCell((short)0).setCellValue(new HSSFRichTextString("Rapportdato: "));
        row5.getCell((short)0).setCellStyle(hReportTextStyle);
        row5.createCell((short)1).setCellValue(new HSSFRichTextString(rapportDato));
        row5.getCell((short)1).setCellStyle(hReportdatoStyle);
        
        rowCount++;

        return rowCount;
    }
    public int createReportHeader(HSSFSheet sheet, String rapportNavn, 
            String kontrakt, String rodeTypeNavn, 
            String rodeNavn, String rodeLengde, 
            String rapportDato, String periode,  
            int rowCount, KontraktRapportObject o){
    	return createReportHeader(sheet, rapportNavn, kontrakt, rodeTypeNavn, rodeNavn, rodeLengde, rapportDato, periode, rowCount);
    }
    /**
     * Lager headeren til tabellen
     * 
     * @param sheet excelarket der headeren skal
     * @param rowCount teller for hvilken rad som skal lages
     * @param fixedHeaderList den faste delen av rapportheaderen
     * @param dynamicHeaderList den dynamiske delen av rapportheaderen
     * @return neste rad index
     */
    public int createTableHeader(HSSFSheet sheet, int rowCount, 
                                 Vector<String> fixedHeaderList, 
                                 TiltakSequence dynamicHeaderList, boolean forside) {
        HSSFRow headerRow = sheet.createRow(rowCount);
        rowCount++;
        HSSFRow headerRowSub = sheet.createRow(rowCount);
        rowCount++;
        HSSFRow headerRowMain = sheet.createRow(rowCount);
        
        
        //int rowFrom, short colFrom, int rowTo, short colTo
        //sheet.addMergedRegion(new Region(headerRowMain.getRowNum(), 
        //    (short) 0, headerRowMain.getRowNum(), (short) (fixedHeaderList.length-1)));

        headerRowMain.createCell((short)(0)).setCellStyle(hTablecellStyle);

        // lag header celler
        for (int i = 0; i < fixedHeaderList.size(); i++) {
            createEmptyCell(headerRowSub, i, 1);
            headerRow.createCell((short)i).setCellValue(new HSSFRichTextString((String)fixedHeaderList.get(i)));
            headerRowMain.createCell((short)i);
            HSSFCell cell = headerRow.getCell((short)i);
            HSSFCell cellMain = headerRowMain.getCell((short)i);
            cell.setCellStyle(hTablecellStyle);
            cellMain.setCellStyle(hTablecellStyle);
            
            
        }
        
        int totalHeaderSize = createTotalHeaders(sheet, headerRow, headerRowMain, fixedHeaderList.size(), forside);
        createEmptyCell(headerRowSub, fixedHeaderList.size(), totalHeaderSize);
        
        int countHeader = 0;
        
        for (int i = 0; i < dynamicHeaderList.size(); i++) {
            TiltakObject tiltakObject = (TiltakObject)dynamicHeaderList.getAt(i);
            
            String headerName = tiltakObject.getNavn();
            if (tiltakObject instanceof TiltakProduksjon){
                countHeader = createProduksjonHeader(i, headerRow, headerRowMain, headerRowSub, fixedHeaderList.size()+totalHeaderSize, countHeader, headerName, sheet, tiltakObject.getGlobalTiltakListe());
                
            } else if (tiltakObject instanceof TiltakMengde){
                countHeader = createMengdeHeader(i, headerRow, headerRowMain, headerRowSub, fixedHeaderList.size()+totalHeaderSize, countHeader, headerName, sheet, tiltakObject.getGlobalTiltakListe());
            }
        }
        // returnerer neste rad i workbook .
        return rowCount++;
    }
    /**
     * Lag header for totalkolonnene. Disse overskrives av subklasser for å legge til flere kolonner under totalt
     * kolonnen
     * @param sheet
     * @param headerRow
     * @param headerRowMain
     * @param fixedHeaderList
     */
    protected int createTotalHeaders(HSSFSheet sheet, HSSFRow headerRow, HSSFRow headerRowMain, int fixedHeaderList, boolean forside){
        int row = headerRow.getRowNum();
        short colFrom = (short) (fixedHeaderList);
        short colTo = (short) (fixedHeaderList+1);
        
        //slå sammen to celler
        sheet.addMergedRegion(new Region(row, colFrom, row, colTo));
        
        createCell(headerRow, (short) (fixedHeaderList), "Total", hTablecellStyle);
        createCell(headerRowMain, (short) (fixedHeaderList), "    Km    ", hTablecellStyle);
        createCell(headerRowMain, (short) (fixedHeaderList+1), "  Timer  ", hTablecellStyle);
        
        return 2;
    }
    private void createEmptyCell(HSSFRow headerRowSub, int index, int qty){
        for (int i=0; i<qty; i++)
            createCell(headerRowSub, (short)(index+i)," ", hTablecellStyle);
    }
    
    /**
     * Legg til den dynamiske delen av rapporten. Denne metoden kalles dersom 
     * tiltakstype er produksjon f.eks 200 km. Denne metoden skal overskrives av subklasser
     * for å legge til flere headere på tiltakene. f.eks. km/t
     * 
     * @param i teller som holder styr på hvilken rad som skal lages
     * @param headerRow header raden
     * @param fixedHeaderList den faste delen av rapporten
     * @param countHeader header teller som holder styr på hvilken kolonne som skal lages
     * @param headerName navnet på headeren
     * @param headerRowMain hovedheader.
     * @param sheet excel arket som skal inneholde dataene
     * 
     * @deprecated
     */
    protected int createTiltakProduksjonHeader(int i, HSSFRow headerRow, int fixedHeaderList, int countHeader, String headerName, HSSFRow headerRowMain, HSSFSheet sheet, boolean forside){
        int rowFrom = headerRow.getRowNum();
        short colFrom = (short) (i+ fixedHeaderList + countHeader);
        int rowTo = headerRow.getRowNum();
        short colTo = (short) (i + fixedHeaderList + countHeader + 1);
        
        //slå sammen to celler
        sheet.addMergedRegion(new Region(rowFrom, colFrom, rowTo, colTo));

        createCell(headerRow, (short) (i + fixedHeaderList + countHeader), headerName, hTablecellStyle);
        createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), "    Km    ", hTablecellStyle);
        countHeader++;
        createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), "  Timer  ", hTablecellStyle);
        
        return countHeader;
        
    }
    
    /**
     * Header metode for å få inn produkt i rapportene. 
     * @param i
     * @param headerRow
     * @param headerRowMain
     * @param headerRowSub
     * @param fixedHeaderList
     * @param countHeader
     * @param headerName
     * @param sheet
     * @param tiltaksListe
     * @return
     */
    protected int createProduksjonHeader(int i, HSSFRow headerRow, HSSFRow headerRowMain, HSSFRow headerRowSub, int fixedHeaderList, int countHeader, String headerName, HSSFSheet sheet, TiltakSequence tiltaksListe){
        if (tiltaksListe==null)
            tiltaksListe=new TiltakSequence();
        
        int row = headerRow.getRowNum();
        short colFrom = (short)(i+fixedHeaderList+countHeader);
        short colTo = (short)(i+fixedHeaderList+countHeader+(tiltaksListe.size()*2));
        
        if (tiltaksListe.size()==0)
            colTo++;
        else
            colTo--;
            
        sheet.addMergedRegion(new Region(row, colFrom, row, colTo));
        
        createCell(headerRow, (short)(i+fixedHeaderList + countHeader), headerName, hTablecellStyle);
        int j = 0;
        do{
            int r = headerRowSub.getRowNum();
            short cFrom = (short)(i+fixedHeaderList+countHeader+j);
            short cTo = (short)(i+fixedHeaderList+countHeader+j+1);
            sheet.addMergedRegion(new Region(r,cFrom,r,cTo));
            
            String navn = " ";
            if (tiltaksListe.size()!=0)
                navn = ((TiltakProduksjon)tiltaksListe.getAt(j)).getNavn();
                
            createCell(headerRowSub, (short)cFrom, navn, hTablecellStyle);
            createCell(headerRowMain, (short)(i+fixedHeaderList+countHeader+j), "    Km    ", hTablecellStyle);
            countHeader++;
            createCell(headerRowMain, (short)(i+fixedHeaderList+countHeader+j), "  Timer  ", hTablecellStyle);
            j++;
        }while(j<tiltaksListe.size());
        
        if (tiltaksListe.size()>0)
            countHeader+=j-1;//legg til antall subheadere til kolonnetelleren
        return countHeader;
    }
    
    /**
     * Legg til den dynamiske delen av rapporten. Denne metoden kalles dersom 
     * tiltakstypen er mengde f.eks 400kg sand
     * 
     * @param i teller som holder styr på hvilken rad som skal lages
     * @param headerRow header raden
     * @param fixedHeaderList den faste delen av rapporten
     * @param countHeader header teller som holder styr på hvilken kolonne som skal lages
     * @param headerName navnet på headeren
     * @param headerRowMain hovedheader.
     * @param tiltakObject tiltaksobjektet som inneholder data om mengde
     * @deprecated
     */
    private int createTiltakMengdeHeader(int i, HSSFRow headerRow, int fixedHeaderList,int countHeader, String headerName,HSSFRow headerRowMain, TiltakObject tiltakObject){
        createCell(headerRow, (short) (i + fixedHeaderList + countHeader), headerName, hTablecellStyle);
        createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), ""+((TiltakMengde)tiltakObject).getEnhet()+" ", hTablecellStyle);
        
       // System.out.println("headerName;"+headerName+" "+((TiltakMengde)tiltakObject).getEnhet());
        
        return countHeader;
    }
    /**
     * Ny header metode for å støtte kravet om å få inn produkt i rapportene. 
     * @param i
     * @param headerRow
     * @param headerRowMain
     * @param headerRowSub
     * @param fixedHeaderList
     * @param countHeader
     * @param headerName
     * @param liste
     * @return
     */
    private int createMengdeHeader(int i, HSSFRow headerRow, HSSFRow headerRowMain, HSSFRow headerRowSub, int fixedHeaderList, int countHeader, String headerName, HSSFSheet sheet, TiltakSequence liste){
        if (liste==null)
            liste = new TiltakSequence();
        
        int row = headerRow.getRowNum();
        short colFrom = (short)(i+fixedHeaderList+countHeader);
        short colTo = (short)(i+fixedHeaderList+countHeader+(liste.size())-1);
        sheet.addMergedRegion(new Region(row, colFrom, row, colTo));
        createCell(headerRow, (short)(i+fixedHeaderList + countHeader), headerName, hTablecellStyle);
        
        int j =0;
        do{
            String navn = " ";
            String enhet = " ";
            if (liste.size()!=0){
                navn = ((TiltakMengde)liste.getAt(j)).getNavn();
                enhet = ((TiltakMengde)liste.getAt(j)).getEnhet();
            }
            
            createCell(headerRowSub, (short)(i+fixedHeaderList+countHeader+j), navn, hTablecellStyle);
            createCell(headerRowMain, (short)(i+fixedHeaderList+countHeader+j), "  "+enhet+"  ", hTablecellStyle);
            j++;
        } while(j<liste.size());
        if (liste.size()>0)
            countHeader+=j-1;//legg til antall subheadere til kolonnetelleren
        //createCell(headerRow, (short) (i + fixedHeaderList + countHeader), "", hTablecellStyle);
        //createCell(headerRowSub, (short) (i + fixedHeaderList + countHeader), headerName, hTablecellStyle);
        //createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), "  "+((TiltakMengde)tiltakObject).getEnhet()+"  ", hTablecellStyle);
        
        return countHeader;
    }
    
    /**
     * Lager dataradene til rapporten. 
     * 
     * @param sheet excelarket som skal inneholde radene
     * @param sequenceData objektet som inneholder data til den faste delen av rapporten
     * @param dynamicHeaderList objektet som inneholder data til den dynamiske delen av rapporten
     * @param rowCount teller for å holde styr på hvilken rad som lages
     * @return neste rad index
     */
    public int createDataRows(HSSFSheet sheet, IndexedSequence sequenceData, 
                              TiltakSequence dynamicHeaderList, int rowCount) {

        
        // iterer data sequence og legg data i workbook
        for (int i = 0; i < sequenceData.size(); i++) {
            HSSFRow dataRow = sheet.createRow(rowCount);
            
            final KontraktRapportObject each = (KontraktRapportObject)sequenceData.getAt(i);
            createRow(dataRow, each, dynamicHeaderList, rowCount, (KontraktRapportSequence)sequenceData);
            /*Iterator it = dataRow.cellIterator();
            while (it.hasNext()){
                HSSFCell cell = (HSSFCell)it.next();
                System.out.print(cell+"\n");
            }
            */
            rowCount++;
        }

        return rowCount;
    }

    /**
     * Lager en kolonne for den faste delen av rapporten som ikke ingår i forsiden. 
     * 
     * @param dataRow raden denne cellen skal lages på
     * @param rapportObject rapportobjektet som inneholder metodene for å hente data
     * @param columnNR i hvilken kolonne denne cellen skal lages
     * @return neste kolonne index
     */
    protected abstract int createEnhetColumns(HSSFRow dataRow, 
                                              KontraktRapportObject rapportObject, 
                                              int columnNR);

    /**
     * Lager en rad i rapporten. Denne raden bygger på data som er satt på av de respiktive
     * implementasjonsklassene, med vekt på metoden createEnhetColumns. Deretter
     * legges det til standardkolonnene tidstart,tidstopp og totalt kjørte km og tid. Til slutt legges
     * den dynamiske delen av rapporten opp vha data i dynamicHeaderList
     * 
     * @param dataRow raden som skal lages
     * @param rapportObject objektet som inneholder data til den faste delen av rapporten
     * @param dynamicHeaderList objektet som inneholder data til den dynamiske delen av rapporten
     * @param rowCount teller for rad
     */
    protected void createRow(HSSFRow dataRow, KontraktRapportObject rapportObject, TiltakSequence dynamicHeaderList, int rowCount, KontraktRapportSequence krseq) {
        int columnNR = 0;
        
        columnNR = createColumnsEnhet(dataRow, rapportObject, columnNR, rowCount);
        
        //TiltakSequence tiltakSequence = rapportObject.getTiltakListe();
        for (int i = 0; i < dynamicHeaderList.size(); i++) {
            
            TiltakObject tiltakHeader = (TiltakObject)dynamicHeaderList.getAt(i);
            TiltakObject tiltakObject = rapportObject.getTiltak(tiltakHeader.getNavn());
            if (tiltakObject !=null ){
                //Hvis tiltaket innholder subtiltak må det hentes ut en liste
                //som inneholder alle disse. deretter kalles createRowProduksjon hvis 
                //det finnes data for det aktuelle subtiltaket. Hvis tiltaket er null 
                //lages det tomme celler for å fylle ut arket slik at neste kolonne 
                //blir under riktig tiltak. 
                
                //tiltak med subtyper
                if (tiltakObject.getTiltakListe()!=null){
                    TiltakSequence dynamicSubHeaderList = krseq.getSubTiltakTypeList(tiltakObject.getNavn());
                    
                    for (int j=0;j<dynamicSubHeaderList.size();j++){
                        TiltakObject sheader = (TiltakObject)dynamicSubHeaderList.getAt(j);
                        TiltakObject sprod = tiltakObject.getTiltak(sheader.getNavn());
                        if (sprod!=null){
                            if (tiltakObject instanceof TiltakProduksjon)
                                columnNR = createRowProduksjon(dataRow, sprod, columnNR, rowCount, rapportObject.isPaaKontrakt());    
                            else if (tiltakObject instanceof TiltakMengde){
                                columnNR = createRowMengde(dataRow, sprod, columnNR, rowCount, rapportObject.isPaaKontrakt());
                            }
                        }
                        //subtiltak inneholder ikke data, lag tomme celler
                        else{
                            HSSFCell tiltakCellKMEmpty = dataRow.createCell((short)columnNR);
                            tiltakCellKMEmpty.setCellStyle(textStyle);
                            columnNR++;
                            if (tiltakObject instanceof TiltakProduksjon){
                                columnNR = createFyllCeller(dataRow, columnNR);
                            }
                        }
                    }
                }
                //tiltak uten subtyper
                else{
                    if (tiltakObject instanceof TiltakProduksjon)
                        columnNR = createRowProduksjon(dataRow, tiltakObject, columnNR, rowCount, rapportObject.isPaaKontrakt());
                    if (tiltakObject instanceof TiltakMengde)
                        columnNR = createRowMengde(dataRow, tiltakObject, columnNR, rowCount, rapportObject.isPaaKontrakt());
                }
                
            //tiltak inneholder ikke data, lag tomme celler
            } else {
                //1 hvis tiltak ikke har subtyper, ellers <antall subtyper>     
                int size = (tiltakHeader.getGlobalTiltakListe()==null)?1:tiltakHeader.getGlobalTiltakListe().size();
                for (int j=0;j<size;j++){
                    HSSFCell tiltakCellKMEmpty = dataRow.createCell((short)columnNR);
                    tiltakCellKMEmpty.setCellStyle(textStyle);
                    columnNR++;
                    if (tiltakHeader instanceof TiltakProduksjon)
                        columnNR = createFyllCeller(dataRow, columnNR);
                }
            }
        }
    }
    /**
     * Lag tomme celler. Disse lages for å fylle rapporten slik at
     * tomme celler får riktig stilart.
     * @param dataRow
     * @param columnNR
     * @return
     */
    protected int createFyllCeller(HSSFRow dataRow, int columnNR){
        HSSFCell tiltakCellTidEmpty = dataRow.createCell((short)columnNR);
        tiltakCellTidEmpty.setCellStyle(textStyle);
        columnNR++;
        return columnNR;
    }
    
    
    /**
     * Lag datakolonner for den rapportspesifikke- og den faste delen av rapporten.
     * Denne metoden kan overskrives for å legge til flere datakolonner i den faste delen av rapporten, f.eks. median av km/t
     * @param dataRow
     * @param rapportObject
     * @param columnNR
     * @return
     */
    protected int createColumnsEnhet(HSSFRow dataRow,KontraktRapportObject rapportObject, int columnNR, int rowCount){
        
        //Bygg summeringsdelen av den faste delen av rapporten
        columnNR = createEnhetColumns(dataRow, rapportObject, columnNR);
        columnNR = createTidStart(dataRow, rapportObject, columnNR);
        columnNR = createTidStopp(dataRow, rapportObject, columnNR);
        columnNR = createTotalKjort(dataRow, rapportObject, columnNR);
        columnNR = createTotalTid(dataRow, rapportObject, columnNR);
        return columnNR;
    }
    /**
     * Lag dataceller for tiltakstype produksjon. Denne metoden kan overskrives for å 
     * legge til flere kolonner for en tiltakstype. f.eks. km/t
     * 
     * @param dataRow
     * @param tiltakObject
     * @param columnNR
     * @param rowCount
     * @return
     */
    protected int createRowProduksjon(HSSFRow dataRow, TiltakObject tiltakObject, int columnNR, int rowCount, boolean paaKontrakt){
        
        //KM
        HSSFCell tiltakCellKM = dataRow.createCell((short)columnNR);
        tiltakCellKM.setCellValue(((TiltakProduksjon)tiltakObject).getKm());
        tiltakCellKM.setCellStyle(paaKontrakt?numberStyle:numberStyleBlue);
        
        columnNR++;

        //TID
        HSSFCell tiltakCellTid = dataRow.createCell((short)columnNR);
        tiltakCellTid.setCellStyle(paaKontrakt?tidStyle:tidStyleBlue);
        String tid = ((TiltakProduksjon)tiltakObject).getTid();
        evalTIMEFunction(tid, tiltakCellTid);
        //tiltakCellTid.setCellFormula("TIME(" + tid + ")");
        //tiltakCellTid.setCellFormula(tid.replaceAll(";", ":"));
        columnNR++;
        return columnNR;
    }
    
    /**
     * Lag dataceller for tiltakstype mengde. Denne metoden kan overskrives for å 
     * legge til flere kolonner for en mengdetype.
     * 
     * @param dataRow
     * @param tiltakObject
     * @param columnNR
     * @param rowCount
     * @return
     */
    protected int createRowMengde(HSSFRow dataRow, TiltakObject tiltakObject, int columnNR, int rowCount, boolean paaKontrakt){
        HSSFCell tiltakCellMengde = dataRow.createCell((short)columnNR);
        tiltakCellMengde.setCellValue(((TiltakMengde)tiltakObject).getMengde());
        tiltakCellMengde.setCellStyle(paaKontrakt?numberStyle:numberStyleBlue);
        columnNR++;
        
        return columnNR;
    }
    /**
     * Lag datacelle for tid-start kolonnen
     * @param dataRow
     * @param rapportObject
     * @param columnNR
     * @return
     */
    protected int createTidStart(HSSFRow dataRow,KontraktRapportObject rapportObject, int columnNR){
    
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            //tidStart
            Date tidStart = dateFormat.parse(rapportObject.getTidStart());
            HSSFCell tidStartCell = dataRow.createCell((short)columnNR);
            tidStartCell.setCellValue(tidStart);
            tidStartCell.setCellStyle(rapportObject.isPaaKontrakt()?datoStyle:datoStyleBlue);
            columnNR++;
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return columnNR;
    }
    /**
     * Lag datacelle for tid-stopp kolonnen
     * @param dataRow
     * @param rapportObject
     * @param columnNR
     * @return
     */
    protected int createTidStopp(HSSFRow dataRow,KontraktRapportObject rapportObject, int columnNR){
    
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            //tidStopp
            Date tidStopp = dateFormat.parse(rapportObject.getTidStopp());
            HSSFCell tidStoppCell = dataRow.createCell((short)columnNR);
            tidStoppCell.setCellValue(tidStopp);
            tidStoppCell.setCellStyle(rapportObject.isPaaKontrakt()?datoStyle:datoStyleBlue);
            columnNR++;

        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return columnNR;
    }
    /**
     * Lag datacelle for total kjørte KM
     * @param dataRow
     * @param rapportObject
     * @param columnNR
     * @return
     */
    protected int createTotalKjort(HSSFRow dataRow,KontraktRapportObject rapportObject, int columnNR){
        //totaltKjortKM
        HSSFCell totaltKM = dataRow.createCell((short)columnNR);
        totaltKM.setCellValue(rapportObject.getTotaltKjortKM());
        totaltKM.setCellStyle(rapportObject.isPaaKontrakt()?numberStyle:numberStyleBlue);
        columnNR++;
        
        return columnNR;
    }
    /**
     * Lag datacelle for total kjørt i tid
     * @param dataRow
     * @param rapportObject
     * @param columnNR
     * @return
     */
    protected int createTotalTid(HSSFRow dataRow,KontraktRapportObject rapportObject, int columnNR){
        //totaltKjørtTid
        HSSFCell totaltTID = dataRow.createCell((short)columnNR);
        String totaltid = rapportObject.getTotaltKjortTid();
        totaltTID.setCellStyle(rapportObject.isPaaKontrakt()?tidStyle:tidStyleBlue);
        evalTIMEFunction(totaltid, totaltTID);
        columnNR++;
        
        return columnNR;
    }
    /**
     * Denne metoden brukes til å bestemme om en celle som inneholder TID (enten timer eller tidspunkt) skal 
     * bruke TIME-funksjonen eller ikke i Excel. Dersom tt (timer) er mer enn 23 må vi regne om tt:mm:ss til 
     * sekunder og la Excel håndtere tallet vha formatering av cellene. 
     * @param totaltid
     * @param tidCell
     * @return
     */
    protected void evalTIMEFunction(String totaltid, HSSFCell tidCell){
        try{
            StringTokenizer tid = new StringTokenizer(totaltid, ";");
            int timer = Integer.parseInt(tid.nextElement().toString());
            //Siden TIME funksjonen til Excel representerer et tidspunkt, vil den ikke fungere til å vise varighet der timer overskrider 23.
            //f.eks vil =TIME(50;30;20) gi resultatet 02:30:20
            if (timer>=24){
                
                int min = Integer.parseInt(tid.nextElement().toString());
                int sec = Integer.parseInt(tid.nextElement().toString());
                min += timer*60;
                double time = (min*60)+sec;
                tidCell.setCellFormula(time/86400+"");
            }else{
            	totaltid = totaltid.replaceAll(";", ",");
                tidCell.setCellFormula("TIME("+totaltid+")");   
            }
        }catch (NumberFormatException e){
            e.printStackTrace();
            //bruk TIME funksjonen dersom formatering mislykkes
             tidCell.setCellFormula("TIME("+totaltid+")");
        }
    }
    /**
     * Lager en kolonne for den faste delen av rapporten. Dersom denne cellen
     * tilhører summeringsdelen av rapporten lages det en tom celle.
     * @param sumRow summeringsraden
     * @param obj RapportObjektet som inneholder metodene for å hente data til cellen
     * @param columnNR i hvilken kolonne denne cellen skal lages 
     * @param globalSum (hack?)
     * @param forside false hvis dette ikke er forsiden 
     * @return
     */
    protected abstract int createEnhetSumColumns(HSSFRow sumRow, 
                                                 KontraktRapportObject obj, 
                                                 int columnNR, 
                                                 boolean globalSum, 
                                                 boolean forside);

    
    public void createSumRow(HSSFSheet sheet, TiltakSequence dynamicHeaderList, 
                             TiltakSequence globalDynamicHeaderList, 
                             String enhetsID, String linkLabel, KontraktRapportObject obj, 
                             int antallEnhetsPerioder, int rowCount, 
                             int rowCountGlobal, boolean globalSum, 
                             boolean forside, int formulaIndex){
    	HSSFRow sumRow;
        HSSFCell textCell;
        
        int columnNR = 0;

        if (globalSum) {
        	sumRow = sheet.createRow(rowCountGlobal);
            textCell = sumRow.createCell((short)(columnNR));
            textCell.setCellStyle(hyperlinkStyle);
            String target = "\"#" + enhetsID + "!A1\"";
            String label = "\"" + linkLabel + "\"";
            String formula = "HYPERLINK(" + target + ", " + label + ")";
            textCell.setCellFormula(formula);
        } else {
        	sumRow = sheet.createRow(rowCount);
        	textCell = sumRow.createCell((short)(columnNR));
        	textCell.setCellStyle(textStyleSum);
        	globalDynamicHeaderList = dynamicHeaderList;
        	textCell.setCellValue(new HSSFRichTextString("Sum:"));
        }
        columnNR++;
    	createSumRows(sheet, dynamicHeaderList, globalDynamicHeaderList, enhetsID, linkLabel, obj, antallEnhetsPerioder, rowCount, rowCountGlobal, globalSum, forside, formulaIndex, columnNR, sumRow);
    	
//    	sumRow = sheet.createRow(rowCount-antallEnhetsPerioder-1);
    	columnNR=0;
    	if (!globalSum){
    		sumRow = sheet.createRow(rowCount-antallEnhetsPerioder-2);
        	textCell = sumRow.createCell((short)(columnNR));
        	textCell.setCellStyle(textStyleSum);
        	globalDynamicHeaderList = dynamicHeaderList;
        	textCell.setCellValue(new HSSFRichTextString("Sum:"));
    	}
    	columnNR=1;
    	createSumRows(sheet, dynamicHeaderList, globalDynamicHeaderList, enhetsID, linkLabel, obj, antallEnhetsPerioder, rowCount, rowCountGlobal, globalSum, forside, formulaIndex, columnNR, sumRow);
    	
    }
    /**
     * Lager summeringsraden på rapporten. Dette gjelder summeringsrader pr ark
     * samt en rad pr ark på forsiden. globalsum er true dersom summeringen
     * gjelder summering av selve forsiden og ikke pr.ark.
     * 
     * @param sheet 
     * @param dynamicHeaderList
     * @param globalDynamicHeaderList
     * @param enhetsID
     * @param obj
     * @param antallEnhetsPerioder
     * @param rowCount
     * @param rowCountGlobal
     * @param globalSum
     * @param forside
     * @param formulaIndex
     * 
     */
    protected void createSumRows(HSSFSheet sheet, TiltakSequence dynamicHeaderList, 
                             TiltakSequence globalDynamicHeaderList, 
                             String enhetsID, String linkLabel, KontraktRapportObject obj, 
                             int antallEnhetsPerioder, int rowCount, 
                             int rowCountGlobal, boolean globalSum, 
                             boolean forside, int formulaIndex, int columnNR, HSSFRow sumRow) {
//        rowCount++;
       
        
        
        //lag summeringskolonner for den faste delen av rapporten
        columnNR = createSumEnhetColumns(columnNR, rowCount, antallEnhetsPerioder, enhetsID, obj, formulaIndex, globalSum, forside, sumRow);
    
        
        int formulaColumn = columnNR + formulaIndex;
        
        //legg til summeringskolonner for den dynamiske delen av rapporten
        for (int i = 0; i < globalDynamicHeaderList.size(); i++) {
        
            TiltakObject tiltakHeader = (TiltakObject)globalDynamicHeaderList.getAt(i);
            boolean tiltakExistsForEnhet = true;    
            if (globalSum) {
                tiltakExistsForEnhet = KontraktRapportSequence.valueExistsForEnhet(dynamicHeaderList, tiltakHeader);
            }
            
            //lag summeringsrad dersom data finnes for kolonnen
            if (tiltakExistsForEnhet){
                int size = tiltakHeader.getGlobalTiltakListe()==null?1:tiltakHeader.getGlobalTiltakListe().size();
                for (int j=0;j<size;j++){
                    //håndter forsiden
                    if (globalSum){
                        
                        //inneholder subtyper, det må sjekkes om subtiltakene inneholder data, dersom det ikke gjør det må 
                        //tomme celler lages. 
                        if (tiltakHeader.getTiltakListe()!=null){
                            //subtiltak
                            TiltakObject tiltakObject = (TiltakObject)tiltakHeader.getTiltakListe().getAt(j);
                            //sjekk om subtiltaket inneholder data. 
                            TiltakSequence headerList = dynamicHeaderList.getTiltakListeFor(tiltakHeader.getNavn());
                            boolean tiltakforenhet = KontraktRapportSequence.valueExistsForEnhet(headerList, tiltakObject);
                            //hvis det finnes data for subtiltaket
                            if (tiltakforenhet){
                                if (tiltakObject instanceof TiltakProduksjon){
                                    int col = columnNR;
                                    columnNR = createSumProduksjon(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, forside);        
                                    formulaColumn+=(columnNR-col)+1;
                                }
                                else if (tiltakObject instanceof TiltakMengde){
                                    columnNR = createSumMengde(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow);
                                    formulaColumn+=1;
                                }    
                            }
                            //hvis det ikke finnes data for subtype, lag tomme celler
                            else{
                                HSSFCell emptyCell = sumRow.createCell((short)(i+j + columnNR));
                                emptyCell.setCellStyle(textStyle);
                                
                                if (tiltakObject instanceof TiltakProduksjon){
                                    columnNR++;
                                    createFyllCeller(sumRow, i+j+columnNR); 
                                }
                            }
                        //inneholder ingen subtyper
                        }else{
                            if (tiltakHeader instanceof TiltakProduksjon){
                                int col = columnNR;
                                columnNR = createSumProduksjon(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, forside);        
                                formulaColumn+=(columnNR-col)+1;
                            }
                            else if (tiltakHeader instanceof TiltakMengde){
                                columnNR = createSumMengde(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow);
                                formulaColumn+=1;
                            }
                        }
                    //håndter detaljarkene
                    }else{
                        if (tiltakHeader instanceof TiltakProduksjon){
                            int col = columnNR;
                            columnNR = createSumProduksjon(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, forside);        
                            formulaColumn+=(columnNR-col)+1;
                        }
                        if (tiltakHeader instanceof TiltakMengde){
                            columnNR = createSumMengde(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow);
                            formulaColumn+=1;
                        }
                    }
                }
                columnNR+=(tiltakHeader.getGlobalTiltakListe()==null)?0:tiltakHeader.getGlobalTiltakListe().size()-1;
            }
            
            //inneholder ikke data
            else {
                int size = (tiltakHeader.getGlobalTiltakListe()==null)?1:tiltakHeader.getGlobalTiltakListe().size();
                for (int j=0;j<size;j++){
                    HSSFCell emptyCell = sumRow.createCell((short)(i+j + columnNR));
                    emptyCell.setCellStyle(textStyle);
                    if (tiltakHeader instanceof TiltakProduksjon){
                        columnNR++;
                        createFyllCeller(sumRow, i+j+columnNR);
                    }
                }
                columnNR+=(tiltakHeader.getGlobalTiltakListe()==null)?0:tiltakHeader.getGlobalTiltakListe().size()-1;
                
            }
        }
    }
    
    /**
     * Bygg sammen summeringskolonner for den faste delen av rapporten.
     * Først bygges summeringskolonner for den delen som er spesifikk for de forskjellige rapportene, deretter
     * legges det til en del som er felles for samtlige rapporter. Denne metoden kan overskrives for å legge
     * til flere summeringskolonner i den faste delen av rapporten, f.eks. median av km/t
     * 
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param obj
     * @param formulaIndex
     * @param globalSum
     * @param forside
     * @param sumRow
     * @return
     */
    protected int createSumEnhetColumns(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, KontraktRapportObject obj, int formulaIndex, boolean globalSum, boolean forside, HSSFRow sumRow){
        HSSFCellStyle styleNR;
        HSSFCellStyle styleDato;
        HSSFCellStyle styleTid;
        if (globalSum) {
            styleNR = numberStyle;
            styleDato = datoStyle;
            styleTid = tidStyle;
        }
        else{
            styleNR = numberStyleSum;
            styleDato = datoStyleSum;
            styleTid = tidStyleSum;
        }
        
        //Bygg summeringsdelen av den faste delen av rapporten
        columnNR = createEnhetSumColumns(sumRow, obj, columnNR, globalSum, forside);
        columnNR = createSumTidStart(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleDato);
        columnNR = createSumTidStopp(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleDato);
        if (forside)
        	columnNR = createSamprod(columnNR, rowCount, sumRow, obj);
        columnNR = createSumTotalKjort(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleNR);
        columnNR = createSumTotalTid(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleTid);
        
        return columnNR;
    }
    

	/**
     * Lag summeringsrad for tiltakstype produksjon, denne metoden kan overskrives
     * i subklasser for å legge til flere kolonner f.eks km/t
     * @param i
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaColumn
     * @param globalSum
     * @param sumRow
     * @return
     */
    protected int createSumProduksjon(int i, int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaColumn, boolean globalSum, HSSFRow sumRow, boolean forside){
        HSSFCellStyle styleNR;
        HSSFCellStyle styleTid;
        
        if (globalSum) {
            styleNR = numberStyle;
            styleTid = tidStyle;
        }else{
            styleNR = numberStyleSum;
            styleTid = tidStyleSum;
        }
       
        //Summering for KM kolonnen
        columnNR = createSumKM(i, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow,styleNR);
        formulaColumn++;
        columnNR++;
        
        //Summering for TID kolonnen
        columnNR = createSumTID(i, columnNR, rowCount,antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, styleTid);
        
        
        return columnNR;
    }
    /**
     * Lag summeringsrad for tiltakstype mengde
     * @param i
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaColumn
     * @param globalSum
     * @param sumRow
     * @return
     */
    protected int createSumMengde(int i, int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaColumn, boolean globalSum, HSSFRow sumRow){
        HSSFCellStyle styleNR;
        if (globalSum) {
            styleNR = numberStyle;
        }else{
            styleNR = numberStyleSum;
        }
        
        CellReference startFormulaMengde = new CellReference(rowCount-antallEnhetsPerioder, i+columnNR);
        CellReference stoppFormulaMengde = new CellReference(rowCount-1, i+columnNR);
        
        //String startformulaMengde = (char)(65 + i + columnNR) + "" + (rowCount - antallEnhetsPerioder);
        //String stoppFormulaMengde = (char)(65 + i + columnNR) + "" + (rowCount - 1);
        
        String formulaMengdeSum = enhetsID + "!" + new CellReference(rowCount, formulaColumn).formatAsString(); //(char)(65 + formulaColumn) + "" + (rowCount + 1);
        formulaColumn++;
        String formulaMengde = "sum(" + enhetsID + "!" + startFormulaMengde.formatAsString() + ":" + stoppFormulaMengde.formatAsString() + ")";
        //String formulaMengde = "sum("+startformulaMengde+":"+stoppFormulaMengde+")";

        if (globalSum)
            formulaMengde = formulaMengdeSum;

        HSSFCell formulaCellMengde = sumRow.createCell((short)(i + columnNR));
        formulaCellMengde.setCellFormula(formulaMengde);
        formulaCellMengde.setCellStyle(styleNR);
        
        return columnNR;
    }
    
    /**
     * Lager en summeringscelle for tid start kolonnen
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaIndex
     * @param globalSum
     * @param sumRow
     * @param styleDato
     * @return
     */
    protected int createSumTidStart(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaIndex, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleDato){
        CellReference startFormulaTid = new CellReference(rowCount-antallEnhetsPerioder, columnNR);
        CellReference stoppFormulaTid = new CellReference(rowCount -1, columnNR);
        
       // String startformulaTidStart = (char)(65 + columnNR) + "" + (rowCount - antallEnhetsPerioder);
        //String stoppFormulaTidStart = (char)(65 + columnNR) + "" + (rowCount - 1);
        String formulaTidStartSum = enhetsID + "!" + new CellReference(rowCount, columnNR + formulaIndex+1).formatAsString();// (char)(65 + columnNR + formulaIndex) + "" + (rowCount + 1); //referanse til sumformel celle
        
        String formulaTidStart = "MINA(" + enhetsID + "!" + startFormulaTid.formatAsString() + ":" + stoppFormulaTid.formatAsString() + ")";
        //String formulaTidStart = "MINA("+startformulaTidStart+":"+stoppFormulaTidStart+")";

        if (globalSum)
            formulaTidStart = formulaTidStartSum;

        HSSFCell formulaCellTidStart = sumRow.createCell((short)(columnNR));
        formulaCellTidStart.setCellFormula(formulaTidStart);
        formulaCellTidStart.setCellStyle(styleDato);
        columnNR++;
        return columnNR;
    }
    
    protected int createSamprod(int columnNR, int rowCount, HSSFRow sumRow, KontraktRapportObject obj) {
		HSSFCell samprodCell = sumRow.createCell((short)(columnNR));
		if (obj==null){
			samprodCell.setCellValue(new HSSFRichTextString(""));
			samprodCell.setCellStyle(getTidStyleSum());
		}else{
			samprodCell.setCellValue(new HSSFRichTextString("1".equals(obj.getSamprod())?"X":""));
			samprodCell.setCellStyle(getCenterStyle());
		}
		columnNR++;
		return columnNR;
	}
    /**
     * Lager en summeringscelle for tid stopp kolonnen
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaIndex
     * @param globalSum
     * @param sumRow
     * @param styleDato
     * @return
     */
    protected int createSumTidStopp(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaIndex, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleDato){
        CellReference startFormulaTid = new CellReference(rowCount-antallEnhetsPerioder, columnNR);
        CellReference stoppFormulaTid = new CellReference(rowCount -1, columnNR);
        
        //String startformulaTidStopp = (char)(65 + columnNR) + "" + (rowCount - antallEnhetsPerioder);
        //String stoppFormulaTidStopp = (char)(65 + columnNR) + "" + (rowCount - 1);
        
        String formulaTidStoppSum = enhetsID + "!" + new CellReference(rowCount, columnNR + formulaIndex+1).formatAsString();// (char)(65 + columnNR + formulaIndex) + "" + (rowCount + 1);
        String formulaTidStopp = "MAXA(" + enhetsID + "!" + startFormulaTid.formatAsString() + ":" + stoppFormulaTid.formatAsString() + ")";
        //String formulaTidStopp = "MAXA("+startformulaTidStopp+":"+stoppFormulaTidStopp+")";

        if (globalSum)
            formulaTidStopp = formulaTidStoppSum;

        HSSFCell formulaCellTidStopp = sumRow.createCell((short)(columnNR));
        formulaCellTidStopp.setCellFormula(formulaTidStopp);
        formulaCellTidStopp.setCellStyle(styleDato);
        columnNR++;
        return columnNR;
    }
    
    /**
     * Lager en summeringscelle for TotalKjørt kolonnen
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaIndex
     * @param globalSum
     * @param sumRow
     * @param styleNR
     * @return
     */
    protected int createSumTotalKjort(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaIndex, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleNR){
        CellReference startFormulaKjort = new CellReference(rowCount-antallEnhetsPerioder, columnNR);
        CellReference stoppFormulaKjort = new CellReference(rowCount -1, columnNR);
        
        //String startformulaKjort = (char)(65 + columnNR) + "" + (rowCount - antallEnhetsPerioder);
        //String stoppFormulaKjort = (char)(65 + columnNR) + "" + (rowCount - 1);
        
        String formulaKjortSum = enhetsID + "!" + new CellReference(rowCount, columnNR + formulaIndex).formatAsString();// (char)(65 + columnNR + formulaIndex) + "" + (rowCount + 1);
        String formulaKjort = "SUM(" + enhetsID + "!" + startFormulaKjort.formatAsString() + ":" + stoppFormulaKjort.formatAsString() + ")";
        

        if (globalSum)
            formulaKjort = formulaKjortSum;

        HSSFCell formulaCellKjort = sumRow.createCell((short)(columnNR));
        formulaCellKjort.setCellFormula(formulaKjort);
        formulaCellKjort.setCellStyle(styleNR);
        columnNR++;
        return columnNR;
    }
    
    /**
     * Lager en summeringscelle for totaltid kolonnen
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaIndex
     * @param globalSum
     * @param sumRow
     * @param styleTID
     * @return
     */
    protected int createSumTotalTid(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaIndex, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleTID) {
         CellReference startFormulaTid = new CellReference(rowCount-antallEnhetsPerioder , columnNR);
         CellReference stoppFormulaTid = new CellReference(rowCount -1, columnNR);
         
         //String startformulaKjort = (char)(65 + columnNR) + "" + (rowCount - antallEnhetsPerioder);
         //String stoppFormulaKjort = (char)(65 + columnNR) + "" + (rowCount - 1);
         
         String formulaKjortSum = enhetsID + "!" + new CellReference(rowCount, columnNR + formulaIndex).formatAsString();// (char)(65 + columnNR + formulaIndex) + "" + (rowCount + 1);
         String formulaKjort = "SUM(" + enhetsID + "!" + startFormulaTid.formatAsString() + ":" + stoppFormulaTid.formatAsString() + ")";
         

         if (globalSum)
             formulaKjort = formulaKjortSum;

         HSSFCell formulaCellKjort = sumRow.createCell((short)(columnNR));
         formulaCellKjort.setCellFormula(formulaKjort);
         formulaCellKjort.setCellStyle(styleTID);
         columnNR++;
         return columnNR;
     }
     
    /**
     * Lager en summeringscelle for KM kolonnen
     * 
     * @param i
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaColumn
     * @param globalSum
     * @param sumRow
     * @param styleNR
     * @return index til neste rad
     */
    protected int createSumKM(int i, int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaColumn, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleNR ){
        CellReference startFormulaKM = new CellReference(rowCount-antallEnhetsPerioder, i + columnNR);
        CellReference stoppFormulaKM = new CellReference(rowCount -1, i + columnNR);
        
        //String startFormulaKM = (char)(65 + i + columnNR) + "" + (rowCount - antallEnhetsPerioder);
        //String stoppFormulaKM = (char)(65 + i + columnNR) + "" + (rowCount - 1);
        String formulaTiltakKMSum = enhetsID + "!" + new CellReference(rowCount, formulaColumn).formatAsString();// (char)(65 + formulaColumn) + "" + (rowCount + 1);
        
        String formulaTiltakKM = "sum(" + enhetsID + "!" + startFormulaKM.formatAsString() + ":" + stoppFormulaKM.formatAsString() + ")";
        //String formulaTiltakKM = "sum("+startformulaKM+":"+stoppFormulaKM+")";       

        if (globalSum){
            //System.out.println(formulaTiltakKMSum+"-"+i+"-"+columnNR);
            formulaTiltakKM = formulaTiltakKMSum;
        }
        
        HSSFCell formulaCellKM = sumRow.createCell((short)(i + columnNR));
       // System.out.println("formula:"+formulaTiltakKM);
        formulaCellKM.setCellFormula(formulaTiltakKM);
        formulaCellKM.setCellStyle(styleNR);
        return columnNR;
    }
    
    /**
     * Lager en summeringscelle for TID kolonnen
     * 
     * @param i teller for hvilket tiltak som lages
     * @param columnNR hvilken kolonnen 
     * @param rowCount hvilken rad summeringsraden skal være på
     * @param antallEnhetsPerioder 
     * @param enhetsID 
     * @param formulaColumn 
     * @param globalSum
     * @param sumRow
     * @param styleTid
     * @return index til neste rad
     */
    protected int createSumTID(int i, int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaColumn, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleTid){
        CellReference startFormulaTid = new CellReference(rowCount-antallEnhetsPerioder, i + columnNR);
        CellReference stoppFormulaTid = new CellReference(rowCount -1, i + columnNR);
        
        //String startformulaTid = (char)(65 + i + columnNR) + "" + (rowCount - antallEnhetsPerioder);
        //String stoppFormulaTid = (char)(65 + i + columnNR) + "" + (rowCount - 1);
        String formulaTiltakTidSum = enhetsID + "!" + new CellReference(rowCount, formulaColumn).formatAsString();// (char)(65 + formulaColumn) + "" + (rowCount + 1);

        String formulaTiltakTid = "sum(" + enhetsID + "!" + startFormulaTid.formatAsString() + ":" + stoppFormulaTid.formatAsString() + ")";

        if (globalSum)
            formulaTiltakTid = formulaTiltakTidSum;

        HSSFCell formulaCellTid = sumRow.createCell((short)(i + columnNR));
        formulaCellTid.setCellFormula(formulaTiltakTid);
        formulaCellTid.setCellStyle(styleTid);
        return columnNR;
    }
    
    
    
    
    /**
     * Lag en ny celle
     * @param row raden cellen skal lages på 
     * @param column hvilken kolonne cellen skal være på 
     * @param value String verdien til cellen
     * @param style bestemmer utseende til cellen
     */
    protected void createCell(HSSFRow row, short column, String value, HSSFCellStyle style){
        HSSFCell cell = row.createCell(column);
        cell.setCellValue(new HSSFRichTextString(value));
        cell.setCellStyle(style);
        
    }
    
    /**
     * Lag en ny celle
     * @param row raden cellen skal lages på 
     * @param column hvilken kolonne cellen skal være på 
     * @param value double verdien til cellen
     * @param style bestemmer utseende til cellen
     */
    protected void createCell(HSSFRow row, short column, double value, HSSFCellStyle style){
        HSSFCell cell = row.createCell(column);
        cell.setCellValue(value);
        cell.setCellStyle(style);
   }
    /**
     * Regner ut km/h 
     * @param tid
     * @return
     */
    protected double calcKMt(String tid, double km){
        if (km<=0)
            return 0;
            
        //long timer = System.nanoTime();
        
        //13320682 ns
        String[] time = tid.split(";");   
        double hour = Integer.parseInt(time[0]);
        double min = Integer.parseInt(time[1]);
        double sec = Integer.parseInt(time[2]);
        //System.out.println("HOUR:"+hour+":"+min+":"+sec);
        min+=(hour*60);
        sec+=(min*60);
        
        double kmh =0;
        
        //regn ut hastigheten
        if (sec>0){
            kmh = (km/sec)*3600;
        }
        //res +=System.nanoTime()-timer;
        //System.out.println(res+" ns "); //38273 | 10057
        return kmh;
    }
}
