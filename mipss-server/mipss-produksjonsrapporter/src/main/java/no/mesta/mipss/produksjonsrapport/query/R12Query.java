package no.mesta.mipss.produksjonsrapport.query;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.persistence.rapportfunksjoner.R12;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NestedTableDescriptor;

public class R12Query {

	private Query query;
	public R12Query(ProdrapportParams params, EntityManager em){
		StringBuilder q = new StringBuilder("select rodenavn, rode_id, veikategori, veinummer,fra_dato_tid, til_dato_tid, totalt_kjort_km, totalt_kjort_sekund, ");
		q.append("prodtyper, stroprodukter from table(rapport_funksjoner.r12(?1, ?2, ?3, ?4, ?5, ?6))");
		
		query = em.createNativeQuery(q.toString());
		query.setParameter(1, params.getDriftkontraktId());
		query.setParameter(2, params.getRodetypeId());
		query.setParameter(3, params.getRodeId());
		query.setParameter(4, params.getFraDato());
		query.setParameter(5, params.getTilDato());
		if (params.getKunKontraktKjoretoy()){
    		query.setParameter(6, 1);
        }else{
        	query.setParameter(6, 0);
        }
	}
	public List<R12> getResults(){
		NestedTableDescriptor<R12Prodtype> prodtype = new NestedTableDescriptor<R12Prodtype>(R12Prodtype.class, "id", "header", "seq", "stroFlagg", "kjortKm", "kjortSekund");
		NestedTableDescriptor<R12Stroprodukt> stroprodukt = new NestedTableDescriptor<R12Stroprodukt>(R12Stroprodukt.class, "id", "header", "seq", "kjortKm", "kjortSekund", "mengdeTonn", "mengdeM3");
		NativeQueryWrapper<R12> wrapper = new NativeQueryWrapper<R12>(query, R12.class, R12.getPropertiesArray());
		wrapper.addNestedTableDescriptor(prodtype);
		wrapper.addNestedTableDescriptor(stroprodukt);
		System.out.println("R12Query.getResults() - starting query");
		List<R12> wrappedList = wrapper.getWrappedList();
		System.out.println("R12Query.getResults() - query done..");
		return wrappedList;
	}
}
