package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;

/**
 * 
 * @author jk getted from
 *         http://jxls.cvs.sourceforge.net/jxls/jxls/src/java/org/
 *         jxls/util/Util.java?revision=1.8&view=markup by Leonid Vysochyn and
 *         modified (adding styles copying) modified by Philipp Löpmeier
 *         (replacing deprecated classes and methods, using generic types)
 */
public final class Util {

	/**
	 * DEFAULT CONSTRUCTOR.
	 */
	private Util() {
	}

	/**
	 * @param newSheet
	 *            the sheet to create from the copy.
	 * @param sheet
	 *            the sheet to copy.
	 */
	public static void copySheets(Sheet newSheet, Sheet sheet) {
		copySheets(newSheet, sheet, true);
	}

	/**
	 * @param newSheet
	 *            the sheet to create from the copy.
	 * @param sheet
	 *            the sheet to copy.
	 * @param copyStyle
	 *            true copy the style.
	 */
	public static void copySheets(Sheet newSheet, Sheet sheet, boolean copyStyle) {
		int maxColumnNum = 0;
		
		for (int i=0;i<sheet.getNumMergedRegions();i++){
			CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
			CellRangeAddress newMergedRegion = new CellRangeAddress(mergedRegion.getFirstRow(), mergedRegion.getLastRow(), mergedRegion.getFirstColumn(), mergedRegion.getLastColumn());
			newSheet.addMergedRegion(newMergedRegion);
		}
		Map<Integer, CellStyle> styleMap = (copyStyle) ? new HashMap<Integer, CellStyle>() : null;
		for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
			Row srcRow = sheet.getRow(i);
			Row destRow = newSheet.createRow(i);
			if (srcRow != null) {
				Util.copyRow(sheet, newSheet, srcRow, destRow, styleMap);
				if (srcRow.getLastCellNum() > maxColumnNum) {
					maxColumnNum = srcRow.getLastCellNum();
					
				}
			}
		}
		
		for (int i = 0; i <= maxColumnNum; i++) {
			newSheet.setColumnWidth(i, sheet.getColumnWidth(i));
		}
		SheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
		if (sheetCF!=null){
			SheetConditionalFormatting newSheetCF = newSheet.getSheetConditionalFormatting();
			for (int i=0;i<sheetCF.getNumConditionalFormattings();i++){
				newSheetCF.addConditionalFormatting(sheetCF.getConditionalFormattingAt(i));
			}
		}
		
		mergedRegions = new TreeSet<CellRangeAddressWrapper>();
	}

	/**
	 * @param srcSheet
	 *            the sheet to copy.
	 * @param destSheet
	 *            the sheet to create.
	 * @param srcRow
	 *            the row to copy.
	 * @param destRow
	 *            the row to create.
	 * @param styleMap
	 *            -
	 */
	private static Set<CellRangeAddressWrapper> mergedRegions = new TreeSet<CellRangeAddressWrapper>();
	public static void copyRow(Sheet srcSheet, Sheet destSheet, Row srcRow, Row destRow, Map<Integer, CellStyle> styleMap) {
		// manage a list of merged zone in order to not insert two times a
		// merged zone
		
		destRow.setHeight(srcRow.getHeight());
		// pour chaque row
		for (int j = srcRow.getFirstCellNum(); j <= srcRow.getLastCellNum(); j++) {
			if (j == -1) {
				continue;
			}
			Cell oldCell = srcRow.getCell(j); // ancienne cell
			Cell newCell = destRow.getCell(j); // new cell
			if (oldCell != null) {
				if (newCell == null) {
					newCell = destRow.createCell(j);
				}
				// copy chaque cell
				copyCell(oldCell, newCell, styleMap);
//				// copy les informations de fusion entre les cellules
//				System.out.println("row num: " + srcRow.getRowNum() + " , col: " + (short) oldCell.getColumnIndex());
//				CellRangeAddress mergedRegion = getMergedRegion(srcSheet, srcRow.getRowNum(), (short) oldCell.getColumnIndex());
//
//				if (mergedRegion != null) {
//					System.out.println("Selected merged region: "+ mergedRegion.toString());
//					CellRangeAddress newMergedRegion = new CellRangeAddress(mergedRegion.getFirstRow(), mergedRegion.getLastRow(), mergedRegion.getFirstColumn(), mergedRegion.getLastColumn());
//					CellRangeAddressWrapper wrapper = new CellRangeAddressWrapper(newMergedRegion);
//					if (isNewMergedRegion(wrapper, mergedRegions)) {
//						System.out.println("New merged region: "+ newMergedRegion.toString());
//						mergedRegions.add(wrapper);
//						destSheet.addMergedRegion(wrapper.range);
//					}
//				}
			}
		}

	}

	/**
	 * @param oldCell
	 * @param newCell
	 * @param styleMap
	 */
	public static void copyCell(Cell oldCell, Cell newCell, Map<Integer, CellStyle> styleMap) {
		if (styleMap != null) {
			if (oldCell.getSheet().getWorkbook() == newCell.getSheet().getWorkbook()) {
				newCell.setCellStyle(oldCell.getCellStyle());
			} else {
				int stHashCode = oldCell.getCellStyle().hashCode();
				CellStyle newCellStyle = styleMap.get(stHashCode);
				if (newCellStyle == null) {
					newCellStyle = newCell.getSheet().getWorkbook().createCellStyle();
					newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
//					newCellStyle.setBorderBottom(oldCell.getCellStyle().getBorderBottom());
//					newCellStyle.setBorderTop(oldCell.getCellStyle().getBorderTop());
//					newCellStyle.setBorderLeft(oldCell.getCellStyle().getBorderLeft());
//					newCellStyle.setBorderRight(oldCell.getCellStyle().getBorderRight());
//					
//					newCellStyle.setTopBorderColor(oldCell.getCellStyle().getTopBorderColor());
//					newCellStyle.setLeftBorderColor(oldCell.getCellStyle().getLeftBorderColor());
//					newCellStyle.setBottomBorderColor(oldCell.getCellStyle().getBottomBorderColor());
//					newCellStyle.setRightBorderColor(oldCell.getCellStyle().getRightBorderColor());
					
					styleMap.put(stHashCode, newCellStyle);
				}
				newCell.setCellStyle(newCellStyle);
			}
		}
		switch (oldCell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			newCell.setCellValue(oldCell.getStringCellValue());
			break;
		case Cell.CELL_TYPE_NUMERIC:
			newCell.setCellValue(oldCell.getNumericCellValue());
			break;
		case Cell.CELL_TYPE_BLANK:
			newCell.setCellType(Cell.CELL_TYPE_BLANK);
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			newCell.setCellValue(oldCell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_ERROR:
			newCell.setCellErrorValue(oldCell.getErrorCellValue());
			break;
		case Cell.CELL_TYPE_FORMULA:
			newCell.setCellFormula(oldCell.getCellFormula());
			break;

		default:
			break;
		}

	}

	/**
	 * Récupère les informations de fusion des cellules dans la sheet source
	 * pour les appliquer à la sheet destination... Récupère toutes les zones
	 * merged dans la sheet source et regarde pour chacune d'elle si elle se
	 * trouve dans la current row que nous traitons. Si oui, retourne l'objet
	 * CellRangeAddress.
	 * 
	 * @param sheet
	 *            the sheet containing the data.
	 * @param rowNum
	 *            the num of the row to copy.
	 * @param cellNum
	 *            the num of the cell to copy.
	 * @return the CellRangeAddress created.
	 */
	public static CellRangeAddress getMergedRegion(Sheet sheet, int rowNum,
			short cellNum) {
		for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
			CellRangeAddress merged = sheet.getMergedRegion(i);
			if (merged.isInRange(rowNum, cellNum)) {
				return merged;
			}
		}
		return null;
	}

	/**
	 * Check that the merged region has been created in the destination sheet.
	 * 
	 * @param newMergedRegion
	 *            the merged region to copy or not in the destination sheet.
	 * @param mergedRegions
	 *            the list containing all the merged region.
	 * @return true if the merged region is already in the list or not.
	 */
	private static boolean isNewMergedRegion(
			CellRangeAddressWrapper newMergedRegion,
			Set<CellRangeAddressWrapper> mergedRegions) {
		return !mergedRegions.contains(newMergedRegion);
	}

}
