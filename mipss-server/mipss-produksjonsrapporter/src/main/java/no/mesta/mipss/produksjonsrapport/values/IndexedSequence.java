package no.mesta.mipss.produksjonsrapport.values;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;


//~--- JDK imports ------------------------------------------------------------


@SuppressWarnings("serial")
public abstract class IndexedSequence implements Serializable, Cloneable {
    // #DC endret fra private til protected for at sub-klasser skal kunne bruke denne direkte...
    // protected java.util.Vector elements = null;
    @SuppressWarnings("unchecked")
	protected ArrayList elements = null;

    /**
     * Constructs an empty <code>IndexedSequence</code> with default
     * initial capacity and default initial capasity increment.
     *
     */
    @SuppressWarnings("unchecked")
	public IndexedSequence() {
        super();
        elements = new ArrayList();
    }

    /**
     * Constructs an empty <code>IndexedSequence</code> with given
     * initial capacity and default initial capasity increment.
     *
     */
    @SuppressWarnings("unchecked")
	public IndexedSequence(int initialCapasity) {
        super();
        elements = new ArrayList(initialCapasity);
    }

    @SuppressWarnings("unchecked")
	public void addAll(Collection c) {
        elements.addAll(c);
    }

    @SuppressWarnings("unchecked")
	public void addAll(int index, Collection c) {
        elements.addAll(index, c);
    }

    /**
     * Appends the specified element to the end of this <code>IndexedSequence</code>.
     *
     * @param obj element to be appended to this <code>IndexedSequence</code>.
     *
     */
    @SuppressWarnings("unchecked")
	protected final void append(Object obj) {
        elements.add(obj);
    }

    /**
     * Returns the component at the specified index.<p>
     *
     * @param      index   an index into this <code>IndexedSequence</code>.
     * @return     the component at the specified index.
     * @exception  ArrayIndexOutOfBoundsException  if the <tt>index</tt>
     *             is negative or not less than the current size of this
     *             <tt>IndexedSequence</tt> object.
     */
    public final Object getAt(int index) throws java.lang.ArrayIndexOutOfBoundsException {
        return elements.get(index);
    }

    @SuppressWarnings("unchecked")
	public final void insertAt(int index, 
                               EntityCopy item) throws java.lang.IndexOutOfBoundsException {
        elements.add(index, item);
    }

    /**
     * Remove element at given position
     *
     */
    public final void removeAt(int index) throws java.lang.ArrayIndexOutOfBoundsException {
        elements.remove(index);
    }

    /**
     * Tests if this <code>IndexedSequence</code>has no components.
     *
     * @return  <code>true</code> if and only if this <code>IndexedSequence</code> has
     *          no components, that is, its size is zero;
     *          <code>false</code> otherwise.
     */
    public final boolean isEmpty() {
        return elements.isEmpty();
    }

    /**
     * Insert the method's description here.
     * Creation date: (02/03/01 10:12:29 PM)
     * @return int
     */
    public final int size() {
        return elements.size();
    }

    @SuppressWarnings("unchecked")
	public final void sort(Comparator comparator) {
        final int size = elements.size();
        final Object a[] = elements.toArray();

        if (comparator == null) {
            Arrays.sort(a);
        } else {
            Arrays.sort(a, comparator);
        }

        for (int i = 0; i < size; i++) {
            elements.set(i, a[i]);
        }
    }

    /**
     * Clones a IndexSequence, calls clone on each element contained i the sequence
     *
     *
     */
    @SuppressWarnings("unchecked")
	public Object clone() {
        IndexedSequence sequence = null;

        try {
            final int size = elements.size();

            sequence = (IndexedSequence)super.clone();
            sequence.elements = new ArrayList(size + 1);

            for (int i = 0, n = size; i < n; i++) {
                EntityCopy entity = (EntityCopy)elements.get(i);

                sequence.append(entity.clone());
            }
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.getMessage());
        } catch (ClassCastException ce) {
            throw new Error(ce.getMessage());
        }

        return sequence;
    }

    public boolean includeInComboBox(Object o) {
        return true;
    }

    @SuppressWarnings("unchecked")
	public ArrayList getArrayList() {
        return elements;
    }

    @SuppressWarnings("unchecked")
	public void setArrayList(ArrayList list) {
        this.elements = list;
    }

    public void clear() {
        this.elements.clear();
    }
}


