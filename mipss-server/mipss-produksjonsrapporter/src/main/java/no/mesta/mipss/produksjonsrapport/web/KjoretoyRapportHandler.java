package no.mesta.mipss.produksjonsrapport.web;

import java.io.File;
import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.KjoretoyRapport;
import no.mesta.mipss.produksjonsrapport.query.Rapp1Query;

public class KjoretoyRapportHandler extends ExcelReportHSSFHandler {
    private EntityManager em;
    
    public KjoretoyRapportHandler(final File file, EntityManager em) {
        super(file);
        this.em = em;
    }

    public byte[] createReport(ProdrapportParams params) throws Exception {
    	params.setFrekvens(false);
    	params.setRapportType(ProdrapportParams.RapportType.KJORETOY.toString());
        List<Rapp1> results = new Rapp1Query(params, em, Rapp1Query.Rapp1Type.KJORETOY).getResults();
        if (results==null||results.isEmpty()){
        	return null;
        }
        KjoretoyRapport rapp = new KjoretoyRapport(results, params);
        return rapp.toByteArray();
    }
}


