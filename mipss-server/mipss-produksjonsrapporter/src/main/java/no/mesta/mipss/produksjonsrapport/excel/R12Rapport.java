package no.mesta.mipss.produksjonsrapport.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.rapportfunksjoner.R12;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Object;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class R12Rapport extends Rapp1Rapport{

	private final List<R12> data;
//	private ExcelHelper excel = new ExcelHelper();
	private static final Long ID_BROYTING = Long.valueOf(6);
	private static final Long ID_HOVLING = Long.valueOf(7);
	private String templateFile = "r12_forside";
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");
//	private static final int ROWS_BETWEEN_SUBREPORTS = 3;
	
	public static void main(String[] args) throws ParseException, FileNotFoundException, IOException, ClassNotFoundException {
		SimpleDateFormat f = new SimpleDateFormat("ddMMyy hhmmss");
		Date fraDato = f.parse("130111 000000");
		Date tilDato = f.parse("130111 235959");
		ProdrapportParams p = new ProdrapportParams();
		p.setDriftkontraktId(Long.valueOf(10));
		p.setKontraktNavn("123-kontrakten");
		p.setRodetypeId(Long.valueOf(15));
		p.setRodeNavn("<Alle valgt>");
//		p.setGjennomsnitt(true);
//		p.setProsent(true);
		p.setRodetypeNavn("Strøing og brøyting trallalalalal");
		p.setFraDato(fraDato);	
		p.setTilDato(tilDato);
		
		FileInputStream in = new FileInputStream("C:/code/mipss-studio/studio-webstart-client/myobject.data");
		ObjectInputStream oin = new ObjectInputStream(in);
		List<R12> objs = (List<R12>) oin.readObject();
		R12Rapport rapp = new R12Rapport(objs, p);
		rapp.createReport();
		rapp.finalizeAndOpen();
	}
	
	public R12Rapport(List<R12> data, ProdrapportParams params){
		super(params);
		this.data = data;
		joinBroytingHovling();
	}
	
	private void joinBroytingHovling(){
		for (R12 d:data){
			List<R12Prodtype> prodtyper = d.getProdtyper();
			R12Prodtype bh = null;
			List<R12Prodtype> toRemove = new ArrayList<R12Prodtype>();
			for (R12Prodtype pt:prodtyper){
				if (pt.getId().equals(ID_BROYTING)||pt.getId().equals(ID_HOVLING)){
					if (bh==null)
						bh = createBroytingHovling();
					bh.addProdtype(pt);
					toRemove.add(pt);
				}
			}
			if (bh!=null){
				for (R12Prodtype pt:bh.getProdtyper()){
					bh.addKjortKm(pt.getKjortKm());
				}
				prodtyper.removeAll(toRemove);
				prodtyper.add(bh);
			}
		}
	}
	private R12Prodtype createBroytingHovling(){
		R12Prodtype bh = new R12Prodtype();
		bh.setId(Long.valueOf(106));
		bh.setSeq(Long.valueOf(6));
		bh.setHeader(resources.getGuiString("header.bh"));
		bh.setStroFlagg(Boolean.FALSE);
		return bh;
	}
	public void createReport(){
		
		Map<Long, List<R12>> listePrRode = RapportHelper.createListePrRode(data);
		Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
		Map<Long, R12Stroprodukt> stroproduktMap = new HashMap<Long, R12Stroprodukt>();
		
		List<R12Object> rodeListe = RapportHelper.createForsideR12(prodtypeMap, stroproduktMap, listePrRode);
		List<R12Prodtype> alleProdtyper = RapportHelper.extractAndSortProdtyper(prodtypeMap);
		List<R12Stroprodukt> alleStroprodukter = RapportHelper.extractAndSortStroprodukter(stroproduktMap);
		//summer og organiser prodtypene og stroproduktene slik at de ligger i samme rekkefølge for samtlige leverandører
		for (R12Object l:rodeListe){
			l.organizeProdtyper(alleProdtyper);
			l.organizeStroprodukt(alleStroprodukter);
			List<R12Prodtype> prodtyper = l.getProdtyper();
			
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		//map.put("prodtyper", alleProdtyper);
		map.put("params", params);
		metadata = new ReportMetadata(rodeListe, params);
		
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 3;
		startRow = 9;
		
		writeReport(rodeListe);
		
		for (R12Object data:rodeListe){
			XSSFSheet sheet = new R12RapportSheet(data, params).getSheet();
			XSSFSheet newSheet = excel.createSheet(RapportHelper.parseString(data.getRodenavn()), workbook);
			Util.copySheets(newSheet, sheet);
			addAutoFilter(newSheet, 13);
			newSheet.createFreezePane(0, 14);
		}
		
//		List<R12> ev = getVeiPrKategori(data, "E"); 
//		List<R12> fv = getVeiPrKategori(data, "F");
//		List<R12> rv = getVeiPrKategori(data, "R");
//		List<R12> kv = getVeiPrKategori(data, "K");
//		ReportMetadata evMeta = new ReportMetadata(ev, params);
//		ReportMetadata fvMeta = new ReportMetadata(fv, params);
//		ReportMetadata rvMeta = new ReportMetadata(rv, params);
//		ReportMetadata kvMeta = new ReportMetadata(kv, params);
//		if (ev.size()>0)
//			row = createSubrapport(sheet, row, ev, "Europavei",  evMeta)+ROWS_BETWEEN_SUBREPORTS;
//		if (fv.size()>0)
//			row = createSubrapport(sheet, row, fv, "Fylkesvei",  fvMeta)+ROWS_BETWEEN_SUBREPORTS;
//		if (rv.size()>0)
//			row = createSubrapport(sheet, row, rv, "Riksvei", 	 rvMeta)+ROWS_BETWEEN_SUBREPORTS;
//		if (kv.size()>0)
//			row = createSubrapport(sheet, row, kv, "Kommunevei", kvMeta);
//		
//		
//		excel.closeWorkbook(wb);
//		DesktopHelper.openFile(excel.getExcelFile());
	}

	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		R12Object d= (R12Object)data;
		excel.writeData(dataRow, 0, d.getRodenavn());
		excel.writeData(dataRow, 1, d.getKm());
		excel.writeData(dataRow, 2, d.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 2);
	}

	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 0);
		addSumCell(1, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(2, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}
	
	
}
