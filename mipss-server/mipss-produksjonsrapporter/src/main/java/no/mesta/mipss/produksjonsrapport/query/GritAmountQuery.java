package no.mesta.mipss.produksjonsrapport.query;

import no.mesta.mipss.persistence.driftslogg.GritAmount;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class GritAmountQuery {

    private Query q;

    public GritAmountQuery(List<Long> measureIds, EntityManager em) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT tiltak_id AS measureId, stromengde AS gritAmount FROM stromengde WHERE tiltak_id IN (");

        String separator = "";
        for (Long measureId : measureIds) {
            sb.append(separator).append(measureId);
            separator = ",";
        }
        sb.append(")");

        q = em.createNativeQuery(sb.toString());
    }

    public List<GritAmount> getGritAmount() {
        NativeQueryWrapper<GritAmount> qry = new NativeQueryWrapper<>(
                q,
                GritAmount.class,
                GritAmount.getPropertiesArray());

        return qry.getWrappedList();
    }

}