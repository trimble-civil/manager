package no.mesta.mipss.produksjonsrapport.values;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;

public class VeirapportObject implements Serializable{

	private String regnr;
	private Date dato;
	private String veistatus;
	private int veinummer;
	
	private double totalKjort;
	private int sekunderKjort;
	private String prodtype;
	private Double stromengde;
	private String materiale;
	private Integer sorteringProdtype;
	private Integer sorteringMateriale;
	
	//TODO refactor dette når vi lager ny spørring!!
	private List<VeiTiltakObject> tiltakList = new ArrayList<VeiTiltakObject>();
	private List<TiltakMengde> mengdeList = new ArrayList<TiltakMengde>();
	private List<TiltakProduksjon> prodList = new ArrayList<TiltakProduksjon>();
	
	public VeiTiltakObject create(){
		VeiTiltakObject o = new VeiTiltakObject();
		o.setMateriale(materiale);
		o.setProdtype(prodtype);
		o.setSekunderKjort(sekunderKjort);
		o.setSorteringMateriale(sorteringMateriale);
		o.setSorteringProdtype(sorteringProdtype);
		o.setStromengde(stromengde);
		o.setTotalKjort(totalKjort);
		return o;
	}
	public String getRegnr() {
		return regnr;
	}
	public Date getDato() {
		return dato;
	}
	public String getVeistatus() {
		return veistatus;
	}
	public int getVeinummer() {
		return veinummer;
	}
	public void setRegnr(String regnr) {
		this.regnr = regnr;
	}
	public void setDato(Date dato) {
		this.dato = dato;
	}
	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}
	public void setVeinummer(int veinummer) {
		this.veinummer = veinummer;
	}
	
	public boolean equals(Object other){
		if (!(other instanceof VeirapportObject)){
			return false;
		}
		VeirapportObject o = (VeirapportObject)other;
		
		return new EqualsBuilder()
			.append(getRegnr(), o.getRegnr())
			.append(getVeistatus(), o.getVeistatus())
			.append(getVeinummer(), o.getVeinummer())
			.append(getDato(), o.getDato())
			.append(getSekunderKjort(), o.getSekunderKjort())
			.isEquals();
	}
	
	public void addTiltak(VeiTiltakObject t){
		tiltakList.add(t);
	}
	public double getTotalKjort() {
		return totalKjort;
	}
	public int getSekunderKjort() {
		return sekunderKjort;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setTotalKjort(double totalKjort) {
		this.totalKjort = totalKjort;
	}
	public void setSekunderKjort(int sekunderKjort) {
		this.sekunderKjort = sekunderKjort;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public Double getStromengde() {
		return stromengde;
	}
	public String getMateriale() {
		return materiale;
	}
	public void setStromengde(Double stromengde) {
		this.stromengde = stromengde;
	}
	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}
	public void setSorteringProdtype(Integer sorteringProdtype) {
		this.sorteringProdtype = sorteringProdtype;
	}
	public Integer getSorteringProdtype() {
		return sorteringProdtype;
	}
	public void setSorteringMateriale(Integer sorteringMateriale) {
		this.sorteringMateriale = sorteringMateriale;
	}
	public Integer getSorteringMateriale() {
		return sorteringMateriale;
	}
	public List<VeiTiltakObject> getTiltakList(){
		return tiltakList;
	}
	public List<TiltakMengde> getMengdeList(){
		return mengdeList;
	}
	public List<TiltakProduksjon> getProdList(){
		return prodList;
	}
}
