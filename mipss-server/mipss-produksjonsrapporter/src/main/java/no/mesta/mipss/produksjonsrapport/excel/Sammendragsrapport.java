package no.mesta.mipss.produksjonsrapport.excel;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.persistence.rapportfunksjoner.Veirapp;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;

public class Sammendragsrapport extends Rapp1Rapport{

	private final List<Rapp1> data;
	private String templateFile = "sammendragsrapport";
	
	private List<Rapp1Object> veiListe;
	private HashMap<String, Object> map;
	
	public Sammendragsrapport(List<Rapp1> data, ProdrapportParams params) {
		super(params);
		this.data = data;
		initData();
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 5;
		startRow = 6;
		autoFilterRow=10;
		freezePaneRow=11;
		freezePaneCol=0;
//		createLinks=false;
		writeReport(veiListe);
		createSubsheets();
	}
	private void createSubsheets(){
		int sheetIndex = 1;
		for (Rapp1Object vei:veiListe){
			new SammendragsrapportSheet(vei, params, excel, workbook, sheetIndex);
			sheetIndex++;
		}
	}
	private void initData() {
		Map<String, List<Rapp1>> listePrVei = RapportHelper.createListePrVei(data, params);
		Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
		Map<Long, R12Stroprodukt> stroproduktMap = new HashMap<Long, R12Stroprodukt>();
		
		veiListe = RapportHelper.createForsideData(prodtypeMap, stroproduktMap, listePrVei, Rapp1Object.class);
		alleProdtyper = RapportHelper.extractAndSortProdtyper(prodtypeMap);
		alleStroprodukter = RapportHelper.extractAndSortStroprodukter(stroproduktMap);
		
		Collections.sort(veiListe, new Comparator<Rapp1Object>(){
			@Override
			public int compare(Rapp1Object o1, Rapp1Object o2) {
				int c = o1.getVei().compareTo(o2.getVei());
				if (c==0){
					c = o1.getFraDato().compareTo(o2.getFraDato());
				}
				return c;
			}
		});
		//summer og organiser prodtypene og stroproduktene slik at de ligger i samme rekkefølge
		for (Rapp1Object l:veiListe){
			l.organizeProdtyper(alleProdtyper);
			l.organizeStroprodukt(alleStroprodukter);
		}
		
		map = new HashMap<String, Object>();
		map.put("rapp1Objects", veiListe);
		map.put("prodtyper", alleProdtyper);
		map.put("params", params);
		metadata = new ReportMetadata(veiListe, params);
	}

	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		if (params.isSumMnd()){
			excel.setCellStyle(excel.getDatoStyle("yyyy mmmm", false), dataRow, 1);
			excel.setCellStyle(excel.getDatoStyle("yyyy mmmm", false), dataRow, 2);
		}

	}

	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
	}

}
