package no.mesta.mipss.produksjonsrapport.values;

import java.util.Hashtable;


/**
 * Denne klassen inneholder data for et bestemt tiltak, med subtyper (dersom det finnes) og der
 * tiltaktypen er produksjon
 */
public class TiltakProduksjon extends TiltakObject {
    private String formelKM;
    private String formelTid;
    private double km;
    private String tid;
    private int sekunder;
    
    public void addSubtype(TiltakObject type, boolean createTotal){
        if (tiltakListe==null){
            tiltakListe = new Hashtable();
            if (createTotal){
                TiltakProduksjon total = new TiltakProduksjon();
                total.setFormelKM(formelKM);
                total.setFormelTid(formelTid);
                total.setID(ID);
                total.setKm(km);
                total.setLastModified(lastModified);
                total.setNavn("Total");
                total.setSortering(1);
                total.setTid(tid);
                //total.setTiltak(tiltak);
                tiltakListe.put(total.getNavn(), total);
                addGlobalTiltak(total);
            }
        }
        tiltakListe.put(type.getNavn(), type);
        addGlobalTiltak(type);
    }
    
    public void setKm(double km) {
        this.km = km;
    }

    public double getKm() {
        return km;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTid() {
        return tid;
    }

    public void setFormelKM(String formelKM) {
        this.formelKM = formelKM;
    }

    public String getFormelKM() {
        return formelKM;
    }

    public void setFormelTid(String formelTid) {
        this.formelTid = formelTid;
    }

    public String getFormelTid() {
        return formelTid;
    }

	public void setSekunder(int sekunder) {
		this.sekunder = sekunder;
		if (sekunder>0){
			int hours = sekunder / 3600,
			remainder = sekunder % 3600,
			minutes = remainder / 60,
			seconds = remainder % 60;
	
			setTid( (hours < 10 ? "0" : "") + hours
			+ ":" + (minutes < 10 ? "0" : "") + minutes
			+ ":" + (seconds< 10 ? "0" : "") + seconds );
		}else{
			setTid("00:00:00");
		}
	}
	public int hashCode(){
    	return getNavn().hashCode();
    }
    public boolean equals(Object o){
    	if (!(o instanceof TiltakProduksjon))
    		return false;
    	if (o==this)
    		return true;
    	TiltakProduksjon other = (TiltakProduksjon) o;
    	return other.getNavn().equals(getNavn());
    }
	public int getSekunder() {
		return sekunder;
	}
}


