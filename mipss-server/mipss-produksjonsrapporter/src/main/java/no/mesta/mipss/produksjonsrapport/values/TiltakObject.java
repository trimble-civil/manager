package no.mesta.mipss.produksjonsrapport.values;

import java.util.Hashtable;


//~--- non-JDK imports --------------------------------------------------------


public abstract class TiltakObject extends EntityCopy {
    protected int ID;
    protected String navn;
    protected int sortering;
    protected int tiltak;

    protected Hashtable tiltakListe;
    protected Hashtable globalList;
    
    public abstract void addSubtype(TiltakObject obj, boolean total);
    
    public void addAllSubtype(TiltakSequence seq, boolean createTotal){
        if (seq  !=null)
            for (int i=0;i<seq.size();i++){
                TiltakObject o = (TiltakObject)seq.getAt(i);
                addSubtype(o, createTotal);
            }
    }
    public TiltakObject getTiltak(String name){
        if (tiltakListe==null)
            return null;
            
        return (TiltakObject)tiltakListe.get(name);
    }
    
    public TiltakSequence getTiltakListe() {
        TiltakSequence seq=null;
        if (tiltakListe!=null){
            seq = new TiltakSequence();
            seq.addAll(tiltakListe.values());
            seq.sort(new TiltakComparator());
        }
        return seq;
    }
    
    public void addGlobalTiltak(TiltakObject subtype){
        if (globalList==null){
            globalList = new Hashtable();
        }
        globalList.put(subtype.getNavn(), subtype);
    }
    
    public TiltakSequence getGlobalTiltakListe(){
        TiltakSequence seq=null;
        if (globalList!=null){
            seq = new TiltakSequence();
            seq.addAll(globalList.values());
            seq.sort(new TiltakComparator());
        }
        return seq;
    }
    
    public TiltakObject() {
        super(0);
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public String getNavn() {
        return navn;
    }

    public void setSortering(int sortering) {
        this.sortering = sortering;
    }

    public int getSortering() {
        return sortering;
    }
    
    public int hashCode(){
    	return getNavn().hashCode();
    }
    public boolean equals(Object o){
    	if (!(o instanceof TiltakObject))
    		return false;
    	if (o==this)
    		return true;
    	TiltakObject other = (TiltakObject) o;
    	return other.getNavn().equals(getNavn());
    }
}


