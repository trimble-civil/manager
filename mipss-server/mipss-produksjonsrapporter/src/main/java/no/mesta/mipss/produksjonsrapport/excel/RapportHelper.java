package no.mesta.mipss.produksjonsrapport.excel;

import java.awt.Desktop;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;
import no.mesta.mipss.persistence.rapportfunksjoner.Materiale;
import no.mesta.mipss.persistence.rapportfunksjoner.R12;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Object;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Veirapp;
import no.mesta.mipss.persistence.rapportfunksjoner.VeirappObject;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class RapportHelper {

	/**
	 * Lager et map der rodeid er nøkkel og verdi er en liste med R12 på samme rode.
	 * 
	 * @param data
	 * @return
	 */
	public static Map<Long, List<R12>> createListePrRode(List<R12> data){
		Map<Long, List<R12>> listePrRode = new HashMap<Long, List<R12>>();
		for (R12 r:data){
			Long rodeId = r.getRodeId();
			List<R12> list = listePrRode.get(rodeId);
			if (list==null){
				list = new ArrayList<R12>();
				listePrRode.put(rodeId, list);
			}
			list.add(r);
		}
		return listePrRode;
	}
	/**
	 * Lager et map med en liste av Rapp1 pr leverandør. Key er leverandørnummer
	 * 
	 * @param data
	 * @param params 
	 * @return
	 */
	public static Map<Long, List<Veirapp>> createListePrVeinummer(List<Veirapp> data){
		Map<Long, List<Veirapp>> listePrVei = new HashMap<Long, List<Veirapp>>();
		for (Veirapp r:data){
			Long veinummer = r.getVeinummer();
			List<Veirapp> list = listePrVei.get(veinummer);
			if (list==null){
				list = new ArrayList<Veirapp>();
				listePrVei.put(veinummer, list);
			}
			list.add(r);
		}
		return listePrVei;
	}
	
	/**
	 * Lager et map med en liste av Rapp1 pr leverandør. Key er leverandørnummer
	 * 
	 * @param data
	 * @param params 
	 * @return
	 */
	public static Map<String, List<Veirapp>> createListePrVeikategori(List<Veirapp> data){
		Map<String, List<Veirapp>> listePrVei = new HashMap<String, List<Veirapp>>();
		for (Veirapp r:data){
			String veikat = r.getVeikategori();
			List<Veirapp> list = listePrVei.get(veikat);
			if (list==null){
				list = new ArrayList<Veirapp>();
				listePrVei.put(veikat, list);
			}
			list.add(r);
		}
		return listePrVei;
	}
	
	/**
	 * Lager et map med en liste av Rapp1 pr leverandør. Key er leverandørnummer
	 * 
	 * @param data
	 * @param params 
	 * @return
	 */
	public static Map<Long, List<Rapp1>> createListePrLeverandor(List<Rapp1> data, ProdrapportParams params){
		Map<Long, List<Rapp1>> listePrLeverandor = new HashMap<Long, List<Rapp1>>();
		
		Map<String, Integer> navnCounter = new HashMap<String, Integer>();
		
		for (Rapp1 r:data){
			Long leverandorNr = r.getLeverandorNr();
			String leverandorNavn = r.getLeverandorNavn();
			
			List<Rapp1> list = listePrLeverandor.get(leverandorNr);
			if (list==null){
				list = new ArrayList<Rapp1>();
				listePrLeverandor.put(leverandorNr, list);
				//Hvis samme navn finnes fra før
				//endre navn til _1, _2 osv.. for å unngå faneark med samme identitet.
				if (navnCounter.containsKey(leverandorNavn)){
					//finnes fra før inkrementer med 1 og gi leverandøren nytt navn
					Integer counter = navnCounter.get(leverandorNavn);
					counter++;
					navnCounter.put(leverandorNavn, counter);
					r.setLeverandorNavn(r.getLeverandorNavn()+"_"+counter);
				}else{
					navnCounter.put(leverandorNavn, 1);
				}
			}
			list.add(r);
		}
		return listePrLeverandor;
	}
	
	/**
	 * Lager et map med en liste av Rapp1 pr leverandør. Key er leverandørnummer
	 * 
	 * @param data
	 * @param params 
	 * @return
	 */
	public static Map<String, List<Rapp1>> createListePrVei(List<Rapp1> data, ProdrapportParams params){
		Map<String, List<Rapp1>> listePrVei = new HashMap<String, List<Rapp1>>();
		for (Rapp1 r:data){
			String vei = r.getVei();
			List<Rapp1> list = listePrVei.get(vei);
			if (list==null){
				list = new ArrayList<Rapp1>();
				listePrVei.put(vei, list);
			}
			list.add(r);
		}
		return listePrVei;
	}
	
	/**
	 * Lager et map med en liste av Rapp1 pr leverandør. Key er leverandørnummer
	 * 
	 * @param data
	 * @param params 
	 * @return
	 */
	public static Map<Long, List<Rapp1>> createListePrKjoretoy(List<Rapp1> data, ProdrapportParams params){
		Map<Long, List<Rapp1>> listePrKjoretoy = new HashMap<Long, List<Rapp1>>();
		Map<String, Integer> navnCounter = new HashMap<String, Integer>();

		for (Rapp1 r:data){
			Long kjoretoyId = r.getKjoretoyId();
			String kjoretoy = r.getKjoretoyNavn();
			List<Rapp1> list = listePrKjoretoy.get(kjoretoyId);
			if (list==null){
				list = new ArrayList<Rapp1>();
				listePrKjoretoy.put(kjoretoyId, list);
				
				//Hvis samme navn finnes fra før
				//endre navn til _1, _2 osv.. for å unngå faneark med samme identitet.
				if (navnCounter.containsKey(kjoretoy)){
					//finnes fra før inkrementer med 1 og gi leverandøren nytt navn
					Integer counter = navnCounter.get(kjoretoy);
					counter++;
					navnCounter.put(kjoretoy, counter);
					r.setKjoretoyNavn(r.getKjoretoyNavn()+"_"+counter);
				}else{
					navnCounter.put(kjoretoy, 1);
				}
			}
			list.add(r);
		}
		return listePrKjoretoy;
	}
	
	/**
	 * Lager et map med en liste av Rapp1 pr leverandør. Key er leverandørnummer
	 * 
	 * @param data
	 * @param params 
	 * @return
	 */
	public static Map<Long, List<Rapp1>> createListePrRode(List<Rapp1> data, ProdrapportParams params){
		Map<Long, List<Rapp1>> listePrRode = new HashMap<Long, List<Rapp1>>();
		Map<String, Integer> navnCounter = new HashMap<String, Integer>();
		for (Rapp1 r:data){
			Long rodeId = r.getRodeId();
			String rodeNavn = r.getRodeNavn();
			List<Rapp1> list = listePrRode.get(rodeId);
			if (list==null){
				list = new ArrayList<Rapp1>();
				listePrRode.put(rodeId, list);
				
				//Hvis samme navn finnes fra før
				//endre navn til _1, _2 osv.. for å unngå faneark med samme identitet.
				if (navnCounter.containsKey(rodeNavn)){
					//finnes fra før inkrementer med 1 og gi leverandøren nytt navn
					Integer counter = navnCounter.get(rodeNavn);
					counter++;
					navnCounter.put(rodeNavn, counter);
					r.setRodeNavn(r.getRodeNavn()+"_"+counter);
				}else{
					navnCounter.put(rodeNavn, 1);
				}
			}
			list.add(r);
		}
		return listePrRode;
	}
	
	/**
	 * Trekker ut alle strøprodukter og prodtyper i egne maps og returnerer en liste med objekter til rapporten.
	 * 
	 * @param <T>
	 * @param prodtypeMapOut
	 * @param stroproduktMapOut
	 * @param sortertListe
	 * @param entity
	 * @return
	 */
	public static<T extends Rapp1Entity> List<T> createForsideData(Map<Long, R12Prodtype> prodtypeMapOut, Map<Long, R12Stroprodukt> stroproduktMapOut, Map<? extends Object, List<Rapp1>> sortertListe, Class<T> entity){
		List<T> entityList = new ArrayList<T>();
		Constructor<T> constructor=null;
		try {
			 constructor = entity.getConstructor(List.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (Object key:sortertListe.keySet()){
			List<Rapp1> list = sortertListe.get(key);
			T obj=null;
			try {
				obj = constructor.newInstance(list);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			entityList.add(obj);
			
			//opprett et map av alle prodtyper
			List<R12Prodtype> prodtypeList = obj.getProdtyper();
			cloneProdtyper(prodtypeMapOut, prodtypeList);
			
			//opprett et map av alle strøprodukter
			List<R12Stroprodukt> stroproduktListe = obj.getStroprodukter();
			cloneStroprodukt(stroproduktMapOut, stroproduktListe);
		}
		return entityList;
	}
	
	/**
	 * Trekker ut alle materialene og prodtyper i egne maps og returnerer en liste med objekter til rapporten.
	 * 
	 * @param <T>
	 * @param prodtypeMapOut
	 * @param stroproduktMapOut
	 * @param sortertListe
	 * @param entity
	 * @return
	 */
	public static<T extends Rapp1Entity> List<T> createForsideDataMateriale(Map<Long, R12Prodtype> prodtypeMapOut, Map<Long, Materiale> materialeMapOut, List<Veirapp> sortertListe, Class<T> entity){
		List<T> entityList = new ArrayList<T>();
		Constructor<T> constructor=null;
		try {
			 constructor = entity.getConstructor(List.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Veirapp> list = sortertListe;
		T obj=null;
		try {
			obj = constructor.newInstance(list);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		entityList.add(obj);
		
		//opprett et map av alle prodtyper
		List<R12Prodtype> prodtypeList = obj.getProdtyper();
		cloneProdtyper(prodtypeMapOut, prodtypeList);
		
		//opprett et map av alle strøprodukter
		List<Materiale> materialeListe = obj.getMaterialer();
		cloneMateriale(materialeMapOut, materialeListe);
		return entityList;
	}
	
	
	public static List<R12Object> createForsideR12(Map<Long, R12Prodtype> prodtypeMapOut, Map<Long, R12Stroprodukt> stroproduktMapOut, Map<? extends Object, List<R12>> sortertListe){
		List<R12Object> entityList = new ArrayList<R12Object>();
		for (Object key:sortertListe.keySet()){
			List<R12> list = sortertListe.get(key);
			R12Object obj = new R12Object(list);
			entityList.add(obj);
			
			//opprett et map av alle prodtyper
			List<R12Prodtype> prodtypeList = obj.getProdtyper();
			cloneProdtyper(prodtypeMapOut, prodtypeList);
			
			//opprett et map av alle strøprodukter
			List<R12Stroprodukt> stroproduktListe = obj.getStroprodukter();
			cloneStroprodukt(stroproduktMapOut, stroproduktListe);
		}
		return entityList;
	}
	
	
	private static void cloneMateriale(Map<Long, Materiale> materialeMapOut,
			List<Materiale> materialeListe) {
		if (materialeListe!=null){
			for (Materiale s:materialeListe){
				Materiale stro = materialeMapOut.get(s.getId());
				if (stro==null){//klon den og gjør klar for header og summering
					Materiale st = createMaterialePlaceholder(s);
					materialeMapOut.put(st.getId(), st);
				}
			}
		}
	}
	private static void cloneProdtyper(Map<Long, R12Prodtype> prodtypeMapOut, List<R12Prodtype> prodtypeList) {
		if (prodtypeList!=null){
			for (R12Prodtype p:prodtypeList){
				R12Prodtype r12Prodtype = prodtypeMapOut.get(p.getId());
				if (r12Prodtype==null){	//klon den og gjør klar for header og summering
					R12Prodtype pt = createProdtypePlaceholder(p);
					prodtypeMapOut.put(pt.getId(), pt);
				}
			}
		}
	}
	
	private static void cloneStroprodukt(Map<Long, R12Stroprodukt> stroproduktMapOut, List<R12Stroprodukt> stroproduktListe) {
		for (R12Stroprodukt s:stroproduktListe){
			R12Stroprodukt stro = stroproduktMapOut.get(s.getId());
			if (stro==null){//klon den og gjør klar for header og summering
				R12Stroprodukt st = createStroproduktPlaceholder(s);
				stroproduktMapOut.put(st.getId(), st);
			}
		}
	}
	public static List<VeirappObject> createForsideDataVeirapport(Map<Long, List<Veirapp>> listeprVeinummer, Map<Long, R12Prodtype> prodtypeMap, Map<Long, Materiale> materialeMap) {
		List<VeirappObject> entityList = new ArrayList<VeirappObject>();
		for (Long key:listeprVeinummer.keySet()){
			List<Veirapp> list = listeprVeinummer.get(key);
			VeirappObject obj = new VeirappObject(list);
			entityList.add(obj);
		}
		Collections.sort(entityList, new Comparator<VeirappObject>(){
			@Override
			public int compare(VeirappObject o1, VeirappObject o2) {
				return o1.getVeinummer().compareTo(o2.getVeinummer());
			}
		});
		return entityList;
	}
	
	
	
	/**
	 * Trekker ut alle strøprodukter og prodtyper i egne maps
	 * 
	 * @param <T>
	 * @param prodtypeMapOut
	 * @param stroproduktMapOut
	 * @param data
	 * @return
	 */
	public static void extractProdtyperOgStroData(Map<Long, R12Prodtype> prodtypeMapOut, Map<Long, R12Stroprodukt> stroproduktMapOut, List<Rapp1> data){
		
		for (Rapp1 d:data){
			//opprett et map av alle prodtyper
			List<R12Prodtype> prodtypeList = d.getProdtyper();
			for (R12Prodtype p:prodtypeList){
				R12Prodtype r12Prodtype = prodtypeMapOut.get(p.getId());
				if (r12Prodtype==null){	//klon den og gjør klar for header og summering
					R12Prodtype pt = createProdtypePlaceholder(p);
					prodtypeMapOut.put(pt.getId(), pt);
				}
			}
			//opprett et map av alle strøprodukter
			List<R12Stroprodukt> stroproduktListe = d.getStroprodukter();
			cloneStroprodukt(stroproduktMapOut, stroproduktListe);
		}
			
	}
	
	/**
	 * Trekker ut alle prodtypene fra sMap og sorterer dem i hht seq
	 * @param prodtypeMap
	 * @return
	 */
	public static List<R12Prodtype> extractAndSortProdtyper(Map<Long, R12Prodtype> prodtypeMap) {
		List<R12Prodtype> alleProdtyper = new ArrayList<R12Prodtype>();
		for (Long key:prodtypeMap.keySet()){
			R12Prodtype pt = prodtypeMap.get(key);
			alleProdtyper.add(pt);
		}
		Collections.sort(alleProdtyper,  new Comparator<R12Prodtype>(){
			public int compare(R12Prodtype o1, R12Prodtype o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}});
		return alleProdtyper;
	}
	
	/**
	 * Trekker ut alle materialene fra sMap og sorterer dem i hht seq
	 * @param materialeMap
	 * @return
	 */
	public static List<Materiale> extractAndSortMateriale(Map<Long, Materiale> materialeMap) {
		List<Materiale> alleProdtyper = new ArrayList<Materiale>();
		for (Long key:materialeMap.keySet()){
			Materiale pt = materialeMap.get(key);
			alleProdtyper.add(pt);
		}
		Collections.sort(alleProdtyper,  new Comparator<Materiale>(){
			public int compare(Materiale o1, Materiale o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}});
		return alleProdtyper;
	}
	
	/**
	 * Trekker ut alle strøproduktene fra sMap og sorterer dem i hht seq
	 * @param stroproduktMap
	 * @return
	 */
	public static List<R12Stroprodukt> extractAndSortStroprodukter(Map<Long, R12Stroprodukt> stroproduktMap) {
		List<R12Stroprodukt> alleStroprodukter = new ArrayList<R12Stroprodukt>();
		for (Long key:stroproduktMap.keySet()){
			R12Stroprodukt st = stroproduktMap.get(key);
			alleStroprodukter.add(st);
		}
		
		Collections.sort(alleStroprodukter,  new Comparator<R12Stroprodukt>(){
			public int compare(R12Stroprodukt o1, R12Stroprodukt o2) {
				return o1.getSeq().compareTo(o2.getSeq());
			}});
		return alleStroprodukter;
	}
	/**
	 * Lager et Materiale 
	 * @param s
	 * @return
	 */
	private static Materiale createMaterialePlaceholder(Materiale s) {
		Materiale st = new Materiale();
		st.setId(s.getId());
		st.setSeq(s.getSeq());
		st.setHeader(s.getHeader());
		return st;
	}
	/**
	 * Lager et stroprodukt 
	 * @param s
	 * @return
	 */
	private static R12Stroprodukt createStroproduktPlaceholder(R12Stroprodukt s) {
		R12Stroprodukt st = new R12Stroprodukt();
		st.setId(s.getId());
		st.setSeq(s.getSeq());
		st.setHeader(s.getHeader());
		return st;
	}
	/**
	 * Lager en prodtype
	 * @param s
	 * @return
	 */
	private static R12Prodtype createProdtypePlaceholder(R12Prodtype p) {
		
		R12Prodtype pt = new R12Prodtype();
		pt.setId(p.getId());
		pt.setSeq(p.getSeq());
		pt.setHeader(p.getHeader());
		if (p.getProdtyper()!=null){
			for (R12Prodtype pp: p.getProdtyper()){
				pt.addProdtype(createProdtypePlaceholder(pp));
			}
		}
		return pt;
	}
	
	/**
	 * Leser templaten fra disken og putter inn de data som ligger i map
	 * 
	 * Obs: workbook fra denne funksjonen er åpen og må lukkes før den kan ferdigstilles.
	 * @param templateFile
	 * @param map
	 * @return
	 */
	public static XSSFWorkbook readTemplate(String templateFile, Map<String, ? extends Object> map){
		XSSFWorkbook wb =	RapportHelper.createWorkbook(RapportHelper.getExcelTemplate(templateFile+".xlsx"));
		XLSTransformer t= new XLSTransformer();
		t.transformWorkbook(wb, map);
		return wb;
	}
	/**
	 * Leser templaten fra disken og putter inn de data som ligger i map
	 * 
	 * Obs: workbook fra denne funksjonen er åpen og må lukkes før den kan ferdigstilles.
	 * @param templateFile
	 * @param map
	 * @return
	 */
	public static XSSFWorkbook readTemplate(String templateFile, Map<String, ? extends Object> map, String... spreadSheetsToRemove){
		XSSFWorkbook wb =	RapportHelper.createWorkbook(RapportHelper.getExcelTemplate(templateFile+".xlsx"));
		XLSTransformer t= new XLSTransformer();
		t.setSpreadsheetsToRemove(spreadSheetsToRemove);
		t.transformWorkbook(wb, map);
		
		return wb;
	}
	public static File createTempFile(String templateFile){
		File tempFile=null;
		try {
			tempFile = File.createTempFile(templateFile, ".xlsx");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return tempFile;
	}
	
	public static String parseString(String tekstStreng) {
        String tekst = tekstStreng.toUpperCase();
        StringBuilder newTekst = new StringBuilder();
        for (int i=0;i<tekst.length();i++){
            char tegn = tekst.charAt(i);
            
            //erstatt underscore med whitespace
            if (tegn==32){
                newTekst.append("_");
            }
            //legg til gyldige tegn, tillater kun 0-9 og A-Z
            else if ((tegn>47 && tegn<58) || (tegn>64 && tegn<91)){ 
                newTekst.append(""+tegn);
            }
            //erstatt alle andre tegn med x
            else
                newTekst.append("x");
        }
        /*
        for (int i = 0; i < tekstStreng.length(); i++) {
            char tegn = tekstStreng.charAt(i);
            if (!((47 < tegn) && (tegn < 58)) && !((64 < tegn) && (tegn < 91))) {

                // tekst = tekst.replaceAll(String.valueOf(String.valueOf(tegn)), "");
                if (tegn!=197)
                    tekst = tekst.replace(tegn, ' ');
                tekst = tekst.replaceAll(" ", "");
            }
        }
        */
      /*  if (tekst.length() > 15) {
            tekst = tekst.substring(0, 14);
        }
    */
        // M - maskin
        if (isNumeric(newTekst.toString())) {
            //tekst = "M" + tekst;
            newTekst.insert(0, 'M');
        }
        if (newTekst.length()>29)
            newTekst.setLength(29);
        
        
        return newTekst.toString();
        //return tekst;
    }
    public static boolean isNumeric(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
	public static InputStream getExcelTemplate(String fileName){
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(fileName);
		if (url != null) {
			try{
			URLConnection connection = url.openConnection();
			if (connection != null) {
			    connection.setUseCaches(false);
			    return connection.getInputStream();
			}
			} catch (Exception e){
				throw new RuntimeException(e);
			}
		}
		return null;
	}
	
	public static XSSFWorkbook createWorkbook(InputStream is){
		try {
//			POIFSFileSystem fs = new POIFSFileSystem(is);
			
			XSSFWorkbook workbook = new  XSSFWorkbook(is);
			return workbook;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void closeWorkbook(XSSFWorkbook workbook, File tempFile) {
		try {
			FileOutputStream dos = new FileOutputStream(tempFile);
			workbook.write(dos);
			dos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void finalizeAndOpen(XSSFWorkbook workbook, File file){
		RapportHelper.closeWorkbook(workbook, file);
		Desktop desktop = Desktop.getDesktop();
		if(desktop.isSupported(Desktop.Action.OPEN)) {
			try {
				desktop.open(file);
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}
	}
	public static byte[] toByteArray(XSSFWorkbook workbook) throws IOException{
		ByteArrayOutputStream ps = new ByteArrayOutputStream();
		workbook.write(ps);
		byte[] rapport = ps.toByteArray();
		ps.close();
        return rapport;
	}

	
	
}
