package no.mesta.mipss.produksjonsrapport.web;

import java.io.File;

import javax.persistence.EntityManager;

import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

public class DAURapportHandler extends RodeRapportHandler{
    public DAURapportHandler(final File file, EntityManager em ) {
        super(file, em, true);
    }
    
    public byte[] createReport(ProdrapportParams params) throws Exception {
    	return super.createReport(params);
    }
}

