package no.mesta.mipss.produksjonsrapport.query;

import no.mesta.mipss.persistence.rapportfunksjoner.Stroinfo;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Used to fetch grit info from STROINFO given corresponding measure ids (tiltak-id-er).
 */
public class GritInfoForMeasuresQuery {

	private Query query;

	public GritInfoForMeasuresQuery(List<Long> measureIds, EntityManager em) {
		StringBuilder q = new StringBuilder();
		q.append("SELECT tiltak_id AS measure_id, liter_losning_totalt AS liter_losning, kg_losning FROM stroinfo WHERE tiltak_id IN (");

		String separator = "";
		for (Long measureId : measureIds) {
			q.append(separator).append(measureId);

			separator = ",";
		}

		q.append(")");

		query = em.createNativeQuery(q.toString());
	}

	public List<Stroinfo> getResults() {
		NativeQueryWrapper<Stroinfo> qry = new NativeQueryWrapper<>(
				query,
				Stroinfo.class,
				Stroinfo.getPropertiesArray());

		return qry.getWrappedList();
	}

}