package no.mesta.mipss.produksjonsrapport.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.persistence.rapportfunksjoner.R12;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.R12Rapport;
import no.mesta.mipss.produksjonsrapport.query.R12Query;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NestedTableDescriptor;

public class R12Handler extends ExcelReportHSSFHandler{
	private EntityManager em;
	private ProdrapportParams params;
	
	public R12Handler(final File file, EntityManager em){
		super(file);
		this.em = em;
	}
	@Override
	public byte[] createReport(ProdrapportParams params) throws Exception {
		this.params = params;
		List<R12> list = getData();
//		FileOutputStream f_out = new FileOutputStream("myobject.data");
//		ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
//		obj_out.writeObject ( list);
//		obj_out.close();
//		f_out.close();
		
		byte[] report = null;
		if (list!=null&&list.size()>0){
			report = createExcelReport(list, file);
		}
		return report;
	}

	private List<R12> getData(){
//		StringBuilder q = new StringBuilder("select rodenavn, rode_id, veikategori, veinummer,fra_dato_tid, til_dato_tid, totalt_kjort_km, totalt_kjort_sekund, ");
////		q.append("prodtyper, stroprodukter from table(rapport_funksjoner.r12(185, 18, null, to_date('01.01.2009','dd.mm.yyyy'), to_date('31.01.2009','dd.mm.yyyy')))");
//		q.append("prodtyper, stroprodukter from table(rapport_funksjoner.r12(?1, ?2, ?3, ?4, ?5, ?6))");
//		
//		//185, 18, null, 01012009, 31012009
//		Query query = em.createNativeQuery(q.toString());
//		query.setParameter(1, params.getDriftkontraktId());
//		query.setParameter(2, params.getRodetypeId());
//		query.setParameter(3, params.getRodeId());
//		query.setParameter(4, params.getFraDato());
//		query.setParameter(5, params.getTilDato());
//		if (params.getKunKontraktKjoretoy()){
//    		query.setParameter(6, 1);
//        }else{
//        	query.setParameter(6, 0);
//        }
//		NestedTableDescriptor<R12Prodtype> prodtype = new NestedTableDescriptor<R12Prodtype>(R12Prodtype.class, "id", "header", "seq", "stroFlagg", "kjortKm", "kjortSekund");
//		NestedTableDescriptor<R12Stroprodukt> stroprodukt = new NestedTableDescriptor<R12Stroprodukt>(R12Stroprodukt.class, "id", "header", "seq", "kjortKm", "kjortSekund", "mengdeTonn", "mengdeM3");
//		NativeQueryWrapper<R12> wrapper = new NativeQueryWrapper<R12>(query, R12.class, R12.getPropertiesArray());
//		wrapper.addNestedTableDescriptor(prodtype);
//		wrapper.addNestedTableDescriptor(stroprodukt);
//		
//		List<R12> wrappedList = wrapper.getWrappedList();
		return new R12Query(params, em).getResults();
//		return wrappedList;
	}
	
	public byte[] createExcelReport(List<R12> list, File file) throws FileNotFoundException, IOException, Exception {
		R12Rapport rapp = new R12Rapport(list, params);
		rapp.createReport();
        return rapp.toByteArray();
	}
	
}
