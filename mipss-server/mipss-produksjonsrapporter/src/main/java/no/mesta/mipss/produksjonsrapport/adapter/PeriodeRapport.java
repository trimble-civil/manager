package no.mesta.mipss.produksjonsrapport.adapter;

import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------
 //TODO rapportene må skrives om hvis de skal endres på

public class PeriodeRapport extends KontraktRapport {
    public PeriodeRapport(HSSFWorkbook workbook, ProdrapportParams params) {
        super(workbook, params);
    }

    protected int createEnhetColumns(HSSFRow dataRow, 
                                     KontraktRapportObject rapportObject, 
                                     int columnNR) {
        String enhet = rapportObject.getEnhetsID();
        String datoEnhet = null;
        String datoEnhet2 = null;

        if (enhet.equals("1")) {
            datoEnhet = rapportObject.getDag();
            datoEnhet2 = rapportObject.getMnd();
        } else if (enhet.equals("2")) {
            datoEnhet = rapportObject.getUke();
            datoEnhet2 = rapportObject.getAar();
        } else if (enhet.equals("3")) {
            datoEnhet = rapportObject.getMnd();
            datoEnhet2 = rapportObject.getAar();
        }

        // Datoenhet1
        HSSFCell datoEnhetCell = dataRow.createCell((short)columnNR);

        datoEnhetCell.setCellValue(new HSSFRichTextString(datoEnhet));
        datoEnhetCell.setCellStyle(textStyle);
        columnNR++;

        // Datoenhet2
        HSSFCell datoEnhet2Cell = dataRow.createCell((short)columnNR);

        datoEnhet2Cell.setCellValue(new HSSFRichTextString(datoEnhet2));
        datoEnhet2Cell.setCellStyle(textStyle);
        columnNR++;

        return columnNR;
    }

    protected int createEnhetSumColumns(HSSFRow sumRow, 
                                        KontraktRapportObject obj, 
                                        int columnNR, boolean globalSum, 
                                        boolean forside) {
        HSSFCell emptyCell = sumRow.createCell((short)(columnNR));

        columnNR++;

        if (globalSum) {
        } else {
            emptyCell.setCellStyle(textStyleSum);
        }

        return columnNR;
    }
}


