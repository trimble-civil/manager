package no.mesta.mipss.produksjonsrapport.values;

import no.mesta.mipss.persistence.driftslogg.GritAmount;
import no.mesta.mipss.produksjonsrapport.query.GritAmountQuery;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DryGritAmountsHelper {

    public static Map<Long, Double> getDryGritAmounts(List<Long> measureIds, EntityManager em) {
        Map<Long, Double> dryGritAmounts = new HashMap<>();

        if (!measureIds.isEmpty()) {
            // We have to split up the data sent to the SQL-query to avoid hitting the 1000 argument limit of IN.
            int start;
            int stop = 0;

            int roundCounter = 1;
            int rounds = (int) Math.ceil(measureIds.size() / 1000d);

            while (roundCounter <= rounds) {
                start = stop;

                if (roundCounter == rounds) {
                    stop = measureIds.size();
                } else {
                    stop += 1000;
                }

                List<Long> tempList = measureIds.subList(start, stop);
                List<GritAmount> dryGritAmountsRaw = new GritAmountQuery(tempList, em).getGritAmount();

                for (GritAmount grit : dryGritAmountsRaw) {
                    Double amount = dryGritAmounts.get(grit.getMeasureId());

                    if (amount != null) {
                        // Measures with grit type "sand med salt" will produce two separate entries in STROMENGDE, and
                        // this is why we need to add them together here.
                        amount += grit.getGritAmount();
                    } else {
                        amount = grit.getGritAmount();
                    }

                    dryGritAmounts.put(grit.getMeasureId(), amount);
                }

                roundCounter++;
            }
        }

        return dryGritAmounts;
    }

}