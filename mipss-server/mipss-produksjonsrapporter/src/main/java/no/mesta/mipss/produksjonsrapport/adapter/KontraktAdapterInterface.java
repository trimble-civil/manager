package no.mesta.mipss.produksjonsrapport.adapter;

import java.util.Vector;

import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.TiltakSequence;

import org.apache.poi.hssf.usermodel.HSSFSheet;


//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------


public interface KontraktAdapterInterface {
    public int createReportHeader(HSSFSheet sheet, String rapportNavn, 
                                  String kontrakt, String rodeTypeNavn, 
                                  String rodeNavn, String rodeLengde, 
                                  String rapportDato, String periode, 
                                  int rowCount, KontraktRapportObject o);

    public int createTableHeader(HSSFSheet sheet, int rowCount, 
                                 Vector<String> fixedHeaderList, 
                                 TiltakSequence dynamicHeaderList, boolean forside);


    public void createDataRows(HSSFSheet sheet, String nameCurrentSheet, String linkLabel,
                               HSSFSheet forsideSheet, 
                               IndexedSequence sequenceData, 
                               TiltakSequence dynamicHeaderList, int rowCount, 
                               int startIndex);


    public void createSumRow(HSSFSheet sheet, TiltakSequence dynamicHeaderList, 
                             String enhetsID, String linkLabel, KontraktRapportObject obj, 
                             int antallEnhetsPerioder, int rowCount, 
                             int rowCountGlobal, boolean globalSum, 
                             boolean forside);
}