package no.mesta.mipss.produksjonsrapport.query;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportSequence;
import no.mesta.mipss.query.NativeQueryWrapper;

public class KontraktPeriodeQuery {
	private Query query;
    private String FROM_CLAUSE = "";
    private final String ORDER_CLAUSE = "ORDER BY RECORD_TYPE, FRA_DATO_TID ";
    private final String SELECT_CLAUSE = 
        "SELECT RECORD_TYPE, TO_CHAR(FRA_DATO_TID,'DD.MM.YYYY HH24:MI'), " + 
        "TO_CHAR(TIL_DATO_TID,'DD.MM.YYYY HH24:MI')," + 
        "TO_CHAR(FRA_DATO_TID, 'DD') AS DAG, " + 
        "TO_CHAR(FRA_DATO_TID, 'WW') AS UKE, " + 
        "TO_CHAR(FRA_DATO_TID, 'MON') AS MND, " + 
        "TO_CHAR(FRA_DATO_TID, 'YYYY') AS AAR, " + "TOTALT_KJORT, TOTALT_TID, XMLDOK_TILTAK ";
    
    public KontraktPeriodeQuery(ProdrapportParams params, EntityManager em) {
    	FROM_CLAUSE = 
            "FROM TABLE(RAPPORT_FUNKSJONER.RAPP2(?1, ?2, ?3, TO_DATE(?4, 'DD.MM.YYYY hh24:mi:ss'), TO_DATE(?5, 'DD.MM.YYYY hh24:mi:ss'), ?6)) ";
//    	String q = SELECT_CLAUSE + FROM_CLAUSE + ORDER_CLAUSE;
    	StringBuilder q = new StringBuilder();
    	q.append(SELECT_CLAUSE);
    	q.append(FROM_CLAUSE);
    	q.append(ORDER_CLAUSE);
    	query= em.createNativeQuery(q.toString());
    	query.setParameter(1, params.getDriftkontraktId());
    	query.setParameter(2, params.getRodetypeId());
    	query.setParameter(3, params.getRodeId());
    	query.setParameter(4, MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
    	query.setParameter(5, MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
    	if (params.getKunKontraktKjoretoy()){
    		query.setParameter(6, 1);
        }else{
        	query.setParameter(6, 0);
        }
    }

    public IndexedSequence consumeResults(){
        final KontraktRapportSequence sequence = new KontraktRapportSequence(null);
        int rowCount = 0;

        
    	List<KontraktRapportObject> wrappedList = new NativeQueryWrapper<KontraktRapportObject>(
    			query, 
    			KontraktRapportObject.class, 
    			new Class[]{
    				String.class, String.class, String.class, 
    				String.class, String.class, String.class, 
    				String.class, Double.class, String.class,
    				String.class}, 
    			"enhetsID", "tidStart", "tidStopp", 
    			"dag", "uke", "mnd", 
    			"aar","totaltKjortKM", "totaltKjortTid", 
    			"tiltakXML").getWrappedList();
    	
        for (KontraktRapportObject o:wrappedList){
        	sequence.add(o);
        	rowCount++;
        }
        return sequence;
    }
}


