package no.mesta.mipss.produksjonsrapport.query;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningObject;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningSequence;
import no.mesta.mipss.query.NativeQueryWrapper;

/**
 * MIPSS1 port
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RodeOversiktQuery {
   
	private EntityManager em;
	private final ProdrapportParams params;
    
    private StringBuilder qry = new StringBuilder();

    public RodeOversiktQuery(ProdrapportParams params, EntityManager em){
		this.params = params;
		this.em = em;
		qry.append("select kk.kontraktnummer, kk.kontraktnavn, ");
		qry.append("rd.id, rd.navn, (select vl.lengde_km from veinettlengde_v vl where vl.veinett_id= rd.veinett_id) rodelengde, ");
		qry.append("to_char(rd.gyldig_fra_dato, 'dd.mm.yyyy'), to_char(rd.gyldig_til_dato, 'dd.mm.yyyy'), v.id, ");
		qry.append("v.veikategori, v.veistatus, v.veinummer, v.hp, ");
		qry.append("v.kommunenummer, v.fylkesnummer, fy.navn, ");
		qry.append("v.fra_km, v.til_km, rt.navn, rd.beskrivelse, ");
		qry.append("concat_rode_leverandor(rd.id) leverandorer ");
		
		qry.append("from driftkontrakt kk ");
		qry.append("join rode rd on rd.kontrakt_id=kk.id ");
		qry.append("join veinettveireferanse v on v.rode_id=rd.id ");
		qry.append("join rodetype rt on rd.type_id=rt.id ");
		qry.append("join fylke fy on fy.fylkenummer= v.fylkesnummer ");
		qry.append("where kk.id = ?1 ");
		if (!params.isAlleInkAvsl()){
			qry.append("and sysdate < rd.gyldig_til_dato ");
		}
		if (params.getRodetypeId()!=null){
			qry.append("and rd.type_id = ?2 ");
		}
		qry.append("order by rd.id, rd.type_id, v.veikategori, v.veistatus, ");
		qry.append("v.veinummer asc, v.hp asc, v.fylkesnummer asc, v.fra_km asc, ");
		qry.append("v.til_km asc ");
    }
    
    public RodeStrekningSequence consumeResults() {
		Query q = em.createNativeQuery(qry.toString());
		q.setParameter(1, params.getDriftkontraktId());
		if (params.getRodetypeId()!=null)
			q.setParameter(2, params.getRodetypeId());
		
		List<RodeStrekningObject> wrappedList = new NativeQueryWrapper<RodeStrekningObject>(
				q, 
				RodeStrekningObject.class,
				new Class[]{
					String.class, String.class, String.class,
					String.class, String.class, String.class,
					String.class, String.class, String.class, 
					String.class, String.class, String.class,
					String.class, String.class, String.class, 
					String.class, String.class, String.class,
					String.class, String.class
				},
				"kontraktNR", "kontraktNavn", "rodeID",
				"rodeNavn", "rodeLengde", "rodeDatoGyldigFra",
				"rodeDatoGyldigTil", "rodeStrekningID", "veiKategori",
				"veiStatus", "veiNR", "hparsell",
				"kommuneNR", "fylkesNR", "fylkesNavn",
				"kmFra", "kmTil", "rodeType",
				"beskrivelse", "leverandorer"
		).getWrappedList();
        final RodeStrekningSequence sequence = new RodeStrekningSequence();
        for (RodeStrekningObject o:wrappedList){
        	sequence.add(o);
        }
        return sequence;
    }
}


