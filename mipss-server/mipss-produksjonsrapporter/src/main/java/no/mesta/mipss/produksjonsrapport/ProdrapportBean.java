package no.mesta.mipss.produksjonsrapport;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.accesscontrol.ServerLogger;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.persistence.Daubestilling;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.rapportfunksjoner.*;
import no.mesta.mipss.produksjonsrapport.query.*;
import no.mesta.mipss.produksjonsrapport.values.DryGritAmountsHelper;
import no.mesta.mipss.produksjonsrapport.values.VeirapportHandler;
import no.mesta.mipss.produksjonsrapport.web.DAURapportHandler;
import no.mesta.mipss.produksjonsrapport.web.ExcelReportHSSFHandler;
import no.mesta.mipss.produksjonsrapport.web.KjoretoyRapportHandler;
import no.mesta.mipss.produksjonsrapport.web.LeverandorRapportHandler;
import no.mesta.mipss.produksjonsrapport.web.PasseringsrapportHandler;
import no.mesta.mipss.produksjonsrapport.web.PeriodeRapportHandler;
import no.mesta.mipss.produksjonsrapport.web.R12Handler;
import no.mesta.mipss.produksjonsrapport.web.RodeOversiktHandler;
import no.mesta.mipss.produksjonsrapport.web.RodeRapportHandler;
import no.mesta.mipss.produksjonsrapport.web.SammendragsrapportHandler;
import no.mesta.mipss.produksjonsrapport.web.SamproduksjonHandler;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NestedTableDescriptor;
import no.mesta.mipss.common.ManagerConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(name=Prodrapport.BEAN_NAME, mappedName="ejb/"+Prodrapport.BEAN_NAME)
public class ProdrapportBean implements Prodrapport {

	private static final Integer PRODTYPE_SIDE_PLOUGH = 1;
	private static final Integer PRODTYPE_PLOWING = 6;
	private static final Integer PRODTYPE_PLANING = 7;

	private Logger log = LoggerFactory.getLogger(ProdrapportBean.class);
    private Map<Integer, ManagerConstants.Stroprod> stroprodtypeCache = new HashMap<>();
    private Map<Integer, String> leverandorCache = new HashMap<>();
	
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;

	@EJB
	private ServerLogger serverLogger;
	
	@Deprecated
	public List<Veirapp> getRapportObjects(ProdrapportParams params){
		try {
			return new VeirapportQuery(params, em).consumeResults();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	/**
	 * TODO REMOVE
	 */
	public List<Rapp1> getAkkumulert(ProdrapportParams params){
		Driftkontrakt d = em.find(Driftkontrakt.class, params.getDriftkontraktId());
		Rodetype rt = em.find(Rodetype.class, params.getRodetypeId());
		
		params.setKontraktNavn(d.getKontraktnavn());
		params.setRodetypeNavn(rt.getTextForGUI());
		
		String periode = MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT)+" - "+MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
		params.setPeriode(periode);
        return new SammendragsrapportHandler(em).getResults(params);
	}
	
	public byte[] getRapport(ProdrapportParams params)throws Exception {
		
		log.debug("Get rapport:"+params);
		serverLogger.start(params.getBrukerSign());
		
		byte[] report = null;
		Driftkontrakt d = em.find(Driftkontrakt.class, params.getDriftkontraktId());
		Rodetype rt = em.find(Rodetype.class, params.getRodetypeId());
		
		params.setKontraktNavn(d.getKontraktnavn());
		params.setRodetypeNavn(rt.getTextForGUI());
		
		String periode = MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT)+" - "+MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT);
		params.setPeriode(periode);
        String tempFileName = generateFileName(params.getType()+" ", d.getKontraktnummer());
        
		try {
			File file = File.createTempFile(tempFileName, ".csv", getTempDir());
			log.debug("Created tempfile:"+file);
	        String fileid = file.getName();
	        fileid = fileid.replaceAll(".csv", "");
	        log.debug("fileId:"+fileid);
	        ExcelReportHSSFHandler handler = null;
	        
	        switch (params.getType()) {
			case KJORETOY:
				handler = new KjoretoyRapportHandler(file, em);
				log.debug("lager kjøretøyrapport");
				break;
			case RODE:
				handler = new RodeRapportHandler(file, em, false);
				log.debug("lager roderapport");
				break;
			case PERIODE:
				handler = new PeriodeRapportHandler(file, em);
				log.debug("lager perioderapport");
				break;
			case LEVERANDOR:
				handler = new LeverandorRapportHandler(em);
				log.debug("lager leverandørrapport");
				break;
			case DAU:
				handler = new DAURapportHandler(file, em);
				log.debug("lager daurapport");
				break;
			case VEI:
				handler = new VeirapportHandler(file, em);
				log.debug("lager veirapport");
				break;
			case SAMPRODUKSJON:
				handler = new SamproduksjonHandler(file, em);
				log.debug("lager samproduksjonsrapport");
				break;
			case R12:
				handler = new R12Handler(file, em);
				log.debug("lager r12 grunnlag");
				break;
			case SAMMENDRAG:
				handler = new SammendragsrapportHandler(em);
				log.debug("lager sammendragsrapport");
				break;
			case PASSERING:
				handler = new PasseringsrapportHandler(em);
				log.debug("lager passeringsrapport");
				break;
			default:
				break;
			}
	        report = handler.createReport(params);
		} catch (IOException e) {
			serverLogger.logg(LoggType.HENT_RAPPORT_FEIL, e.getMessage());
			throw e;
		} catch (Exception e) {
			serverLogger.logg(LoggType.HENT_RAPPORT_FEIL, e.getMessage());
			throw e;
		}
		serverLogger.logg(LoggType.HENT_RAPPORT, params.getType().toString());
		return report;
	}
	
	
	public byte[] getRodeoversiktRapport(ProdrapportParams params) throws Exception{
		serverLogger.start(params.getBrukerSign());
		byte[] report = null;

		String tempFileName = generateFileName(params.getType()+" ", params.getDriftkontraktId()+"");
		File file=null;
		try {
			file = File.createTempFile(tempFileName, ".csv", getTempDir());
			String fileid = file.getName();
	        fileid = fileid.replaceAll(".csv", "");
	        RodeOversiktHandler handler = new RodeOversiktHandler(file, em);
	        log.debug("file created:"+file);
	        report = handler.createReport(params);
	        log.debug("report:"+report);
		} catch (IOException e) {
			serverLogger.logg(LoggType.HENT_RAPPORT_FEIL, e.getMessage());
			throw e;
		} catch (Exception e) {
			serverLogger.logg(LoggType.HENT_RAPPORT_FEIL, e.getMessage());
			throw e;
		}
		serverLogger.logg(LoggType.HENT_RAPPORT, "RODEOVERSIKT");
        return report;
	}
	
	public byte[] getVeirapport(ProdrapportParams params) throws Exception{
		serverLogger.start(params.getBrukerSign());
		byte[] report = null;
		String tempFileName = generateFileName("Veirapport ", params.getDriftkontraktId()+"");
		File file=null;
		try {
			file = File.createTempFile(tempFileName, ".csv", getTempDir());
			String fileid = file.getName();
	        fileid = fileid.replaceAll(".csv", "");
	        VeirapportHandler handler = new VeirapportHandler(file, em);
	        log.debug("file created:"+file);
	        report = handler.createReport(params);
	        log.debug("report:"+report);
		} catch (IOException e) {
			serverLogger.logg(LoggType.HENT_RAPPORT_FEIL, e.getMessage());
			throw e;
		} catch (Exception e) {
			serverLogger.logg(LoggType.HENT_RAPPORT_FEIL, e.getMessage());
			throw e;
		}
		serverLogger.logg(LoggType.HENT_RAPPORT, "Veirapport");

        return report;
	}
	
	public void sendDauBestilling(Daubestilling daubestilling){
		em.persist(daubestilling);
		em.flush();
	}
	@Deprecated
	public List<R12> getR12Grunnlag(ProdrapportParams params){
		serverLogger.start(params.getBrukerSign());
		
		StringBuilder q = new StringBuilder("select rodenavn, rode_id, veikategori, veinummer,fra_dato_tid, til_dato_tid, totalt_kjort_km, totalt_kjort_sekund, ");
		q.append("prodtyper, stroprodukter from table(rapport_funksjoner.r12(?1, ?2, ?3, ?4, ?5, ?6))");
		
		Query query = em.createNativeQuery(q.toString());
		query.setParameter(1, params.getDriftkontraktId());
		query.setParameter(2, params.getRodetypeId());
		query.setParameter(3, params.getRodeId());
		query.setParameter(4, params.getFraDato());
		query.setParameter(5, params.getTilDato());
		if (params.getKunKontraktKjoretoy()){
    		query.setParameter(6, 1);
        }else{
        	query.setParameter(6, 0);
        }
		NestedTableDescriptor<R12Prodtype> prodtype = new NestedTableDescriptor<R12Prodtype>(R12Prodtype.class, "id", "header", "seq", "stroFlagg", "kjortKm", "kjortSekund");
		NestedTableDescriptor<R12Stroprodukt> stroprodukt = new NestedTableDescriptor<R12Stroprodukt>(R12Stroprodukt.class, "id", "header", "seq", "kjortKm", "kjortSekund", "mengdeTonn", "mengdeM3");
		NativeQueryWrapper<R12> wrapper = new NativeQueryWrapper<R12>(query, R12.class, R12.getPropertiesArray());
		wrapper.addNestedTableDescriptor(prodtype);
		wrapper.addNestedTableDescriptor(stroprodukt);
		
		List<R12> wrappedList = wrapper.getWrappedList();
		serverLogger.logg(LoggType.HENT_RAPPORT, "R12Grunnlag");
		return wrappedList;
	}
    /**
     * Henter fildirektory utfra servletkontekst
     * @return File
     */
    private File getTempDir() {
		String tempdir = System.getProperty("java.io.tmpdir");
    	return new File(tempdir);
    }
    
    private String generateFileName(String rapportType, String kontraktNr){
        String fileName = "";
        Calendar c = Calendar.getInstance();
        fileName +=c.get(Calendar.YEAR)+"-";
        fileName +=(c.get(Calendar.MONTH)+1)+"-";
        fileName +=c.get(Calendar.DAY_OF_MONTH)+"-";
        fileName +=rapportType+"-";
        fileName +=kontraktNr;
        return fileName;
    }
    
    public List<UkjentStroprodukt> genererUkjentStroproduktRapport(Long kontraktId, Date fraDato, Date tilDato, Long kjoretoyId){
    	List<UkjentStroprodukt> list;
    	
    	//Dersom  man skal ta ut en liste for alle kjøretøy med ukjent produksjon i en konntrakt
    	//Brukes ved uthenting av rapporten fra PRODUSKJONSRAPPORTER
    	if (kontraktId != null) {   		
	    	list = new ArrayList<UkjentStroprodukt>();
	    		
	    	List<UkjentStroprodukt> all = new UkjentStroproduktQuery(null, fraDato, tilDato, em).getResults();
	    	
	    	for (UkjentStroprodukt ukjentStroprodukt : all) {
	    		if (ukjentStroprodukt.getKontraktId() != null) {
	    			if (ukjentStroprodukt.getKontraktId().longValue() == kontraktId.longValue()) {
						list.add(ukjentStroprodukt);
					}
				}
	    	}
	     //Brukes ved uthenting av rapporten fra DRIFTSLOGG og KJØRETØYSOVERSIKTEN
		} else{
	    	list = new UkjentStroproduktQuery(kjoretoyId, fraDato, tilDato, em).getResults();
		}
    	
//		new UkjentStroproduktRapport(list, fraDato, tilDato, kjoretoyId).finalizeAndOpen();
		
		return list;
    }

	public List<Vinterdrift> genererVinterdriftklasserRapport(Driftkontrakt kontrakt, Long rodeType, Long rode, Date fraDato, Date tilDato) {
        List<Vinterdrift> vinterdriftklasser = new ArrayList<Vinterdrift>();

        // Finner listen med gyldige rodetilknytninger for denne kontrakten i det angitte tidsrommet. Rodetilknytningene
        // brukes i sammenheng med inkludering og kalkulering av brøyting.
        List<KjoretoyRode> rodetilknytninger = getRodetilknytningerForKontrakt(kontrakt, fraDato, tilDato);

		List<Vinterdrift> vinterdriftTiltak;
		if (rode == null) {
            // Henter vinterdrift for alle roder tilknyttet kontrakten.
            vinterdriftTiltak = new VinterdriftQuery(kontrakt.getId(), rodeType, fraDato, tilDato, em).getResults();
		} else {
            // Henter vinterdrift for én spesifikk rode tilknyttet kontrakten.
            vinterdriftTiltak = new VinterdriftQuery(kontrakt.getId(), rodeType, rode, fraDato, tilDato, em).getResults();
		}

        // Vi sørger for å knytte riktig kjøretøy opp mot korrekt leverandørnavn/leverandørnummer for klassene. Dersom
        // det viser seg at vi har med et kjøretøy som ikke er tilknyttet en kontrakt, så fjernes vinterdriftklassen.
        oppdaterLeverandorNavn(vinterdriftTiltak, kontrakt.getId(), fraDato, tilDato);

		List<Long> dryMeasureIds = new ArrayList<>();
		List<Long> wetMeasureIds = new ArrayList<>();

		for (Vinterdrift vd : vinterdriftTiltak) {
			if (vd.getStroFlagg() != null && vd.getStroFlagg()) {
				if (ManagerConstants.Stroprod.idTilEnum(vd.getStroproduktId()) == ManagerConstants.Stroprod.SALTLOSNING) {
					if (!wetMeasureIds.contains(vd.getTiltakId())) {
						wetMeasureIds.add(vd.getTiltakId());
					}
				} else {
					if (!dryMeasureIds.contains(vd.getTiltakId())) {
						dryMeasureIds.add(vd.getTiltakId());
					}
				}
			}
		}

		Map<Long, Double> dryGritAmounts = DryGritAmountsHelper.getDryGritAmounts(dryMeasureIds, em);
		Map<Long, Stroinfo> wetGrytAmounts = getWetGritAmounts(wetMeasureIds, em);

		List<Vinterdrift> muligSamproduksjonListe = new ArrayList<>();

		for (Vinterdrift nyKlasse : vinterdriftTiltak) {
			if (tiltakIkkeInkludert(muligSamproduksjonListe, nyKlasse)) {
				if (vinterdriftklasser.contains(nyKlasse)) {
					// Dersom vinterdriftklassen finnes fra før, så oppdaterer vi eksisterende klasse.
					oppdaterKlasse(vinterdriftklasser.get(vinterdriftklasser.indexOf(nyKlasse)), nyKlasse, rodetilknytninger, dryGritAmounts, wetGrytAmounts);
				} else {
					// Hvis vinterdriftsklassen ikke finnes fra før, så oppdaterer den seg selv.
					oppdaterKlasse(nyKlasse, nyKlasse, rodetilknytninger, dryGritAmounts, wetGrytAmounts);

					// Vi sjekker om den nye vinterklassen vår faktisk inneholder noe data før vi legger den til i listen.
					if (nyKlasse.hasData()) {
						vinterdriftklasser.add(nyKlasse);
					}
				}

				// Vedlikeholder en liste med prosesserte tiltak slik at vi unngår problematikk rundt samproduksjon
				// hvor flere distanser legges til for den samme turen.
				muligSamproduksjonListe.add(nyKlasse);
			}
		}

		// Tømmer cachen slik at kjøretøy tilknyttet andre leverandører på andre kontrakter vises riktig.
		leverandorCache.clear();

		return sorterVinterdriftsklasser(vinterdriftklasser);
	}

	private List<Vinterdrift> sorterVinterdriftsklasser(List<Vinterdrift> vinterdriftklasser) {
		vinterdriftklasser.sort(Comparator
				.comparing(Vinterdrift::getLeverandor, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(Vinterdrift::getRodenavn, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(Vinterdrift::getVinterdriftklasse, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(Vinterdrift::getFylke, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(Vinterdrift::getVegtype, Comparator.nullsLast(Comparator.naturalOrder()))
		);
		return vinterdriftklasser;
	}

	private boolean tiltakIkkeInkludert(List<Vinterdrift> muligSamproduksjon, Vinterdrift nyKlasse) {
		for (Vinterdrift tiltak : muligSamproduksjon) {
			if (
					nyKlasse.getBroyteFlagg() &&
							(nyKlasse.getProdtypeId() == PRODTYPE_PLOWING || nyKlasse.getProdtypeId() == PRODTYPE_PLANING) &&
							(tiltak.getProdtypeId() == PRODTYPE_PLOWING || tiltak.getProdtypeId() == PRODTYPE_PLANING) &&
							nyKlasse.getRodeId() == tiltak.getRodeId() &&
							nyKlasse.getLeverandor().equalsIgnoreCase(tiltak.getLeverandor()) &&
							nyKlasse.getKjoretoyId() == tiltak.getKjoretoyId() &&
							nyKlasse.getFraDatoRodestrekning().equals(tiltak.getFraDatoRodestrekning()) &&
							nyKlasse.getTilDatoRodestrekning().equals(tiltak.getTilDatoRodestrekning())) {
				// Over definerer vi et samproduksjonstiltak bestående av høvling/brøyting
				return false;
			}
		}
		return true;
	}

    private List<KjoretoyRode> getRodetilknytningerForKontrakt(Driftkontrakt kontrakt, Date fraDato, Date tilDato) {
        List<KjoretoyRode> rodetilknytninger = new ArrayList<KjoretoyRode>();

        // Henter ut alle rodetilknytninger med tilknytningstype "brøyterode" for den gitte kontrakten.
        Query query = em.createNamedQuery(KjoretoyRode.FIND_BY_KONTRAKT_AND_RODETILKNYTNINGSTYPE)
                .setParameter("kontraktId", kontrakt.getId())
                .setParameter("rodetilknytningstypeId", 1L); // 1 = "Brøyterode" i RODETILKNYTNINGSTYPE-tabellen
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
        List<KjoretoyRode> rodetilknytningerTemp = query.getResultList();

        // Vi ønsker bare å ha med rodetilknytninger som ligger innenfor kontraktens gyldighet og det tidsrommet som er
        // definert av brukeren i GUI-et.
        for (KjoretoyRode rodetilknytning : rodetilknytningerTemp) {
            if (gyldigDatointervall(rodetilknytning, fraDato, tilDato, kontrakt)) {
                rodetilknytninger.add(rodetilknytning);
            }
        }

        return rodetilknytninger;
    }

    /**
     * Hjelpemetode for getRodetilknytningerForKontrakt til å sjekke om rodetilknytningens datoer faller innenfor
     * korrekt tidsrom (etter kontraktstart, før fra-dato, før kontraktslutt, og før valgt til-dato).
     * @param rodetilknytning
     * @param fraDato
     * @param tilDato
     * @return
     */
    private boolean gyldigDatointervall(KjoretoyRode rodetilknytning, Date fraDato, Date tilDato, Driftkontrakt kontrakt) {
        boolean gyldighet = false;

        // En rodetilknytning behøver ikke å ha et til-tidspunkt, så dette må spesialbehandles.
        if (rodetilknytning.getTilTidspunkt() != null) {
            if (
                    rodetilknytning.getFraTidspunkt().getTime() >= kontrakt.getGyldigFraDato().getTime() &&
                    rodetilknytning.getTilTidspunkt().getTime() <= kontrakt.getGyldigTilDato().getTime() &&
                    fraDato.getTime() >= rodetilknytning.getFraTidspunkt().getTime() &&
                    tilDato.getTime() <= rodetilknytning.getTilTidspunkt().getTime()) {
                gyldighet = true;
            }
        } else {
            if (
                    rodetilknytning.getFraTidspunkt().getTime() >= kontrakt.getGyldigFraDato().getTime() &&
                    fraDato.getTime() >= rodetilknytning.getFraTidspunkt().getTime()) {
                gyldighet = true;
            }
        }

        return gyldighet;
    }

	private void oppdaterKlasse(Vinterdrift eksisterende, Vinterdrift ny, List<KjoretoyRode> rodetilknytninger,
			Map<Long, Double> dryGritAmounts, Map<Long, Stroinfo> wetGrytAmounts) {
		// BRØYTING/HØVLING/SIDEPLOG/SWEEPER
		if (ny.getBroyteFlagg()) {
            oppdaterBroyting(eksisterende, ny, rodetilknytninger);
		}

		// TUNG HØVLING
		if (ny.getProdtypeId() == 24) {
			oppdaterTungHovling(eksisterende, ny);
		}

		// STRØING
		if (ny.getStroFlagg()) {
			oppdaterStroing(eksisterende, ny, dryGritAmounts, wetGrytAmounts);
		}
	}

	private void oppdaterBroyting(Vinterdrift eksisterende, Vinterdrift ny, List<KjoretoyRode> rodetilknytninger) {
		// Brøyting tas bare med på vinterdriftklasserapporten dersom det finnes en rodetilknytning for kombinasjonen av
		// kjøretøyet, roden, og kontrakten for rodetilknytningstype "Brøyterode"
		for (KjoretoyRode rodetilknytning : rodetilknytninger) {
			if (
					rodetilknytning.getKjoretoyId() == ny.getKjoretoyId() &&
					rodetilknytning.getRodeId() == ny.getRodeId() &&
					rodetilknytning.getFraTidspunkt().getTime() <= ny.getFraDatoRodestrekning().getTime()) {

				if (rodetilknytning.getTilTidspunkt() == null || rodetilknytning.getTilTidspunkt().getTime() >= ny.getTilDatoRodestrekning().getTime()) {
                    oppdaterKorrektBroytekolonne(eksisterende, ny);
                    break;
                }
			}
		}
	}

	private void oppdaterKorrektBroytekolonne(Vinterdrift eksisterende, Vinterdrift ny) {
		if (ny.getProdtypeId() != PRODTYPE_SIDE_PLOUGH) {
			eksisterende.addBroytingKm(ny.getKm());
		} else {
			eksisterende.addSideplogKm(ny.getKm());
		}
	}

	private void oppdaterTungHovling(Vinterdrift eksisterende, Vinterdrift ny) {
		eksisterende.addTungHovlingKm(ny.getKm());
		eksisterende.addTungHovlingTimer(
                kalkulerTimer(ny.getTilDatoRodestrekning(), ny.getFraDatoRodestrekning()));
	}

	private void oppdaterStroing(Vinterdrift eksisterende, Vinterdrift ny, Map<Long, Double> dryGritAmounts,
			Map<Long, Stroinfo> wetGrytAmounts) {
		ManagerConstants.Stroprod stroprodtype;

        // Cacher strøprodukter som allerede har blitt identifisert slik at vi slipper å iterere gjennom listen med
        // Strotype-enumer hver gang.
        if (stroprodtypeCache.containsKey(ny.getStroproduktId())) {
            stroprodtype = stroprodtypeCache.get(ny.getStroproduktId());
        } else {
            stroprodtype = ManagerConstants.Stroprod.idTilEnum(ny.getStroproduktId());
            stroprodtypeCache.put(ny.getStroproduktId(), stroprodtype);
        }

		double mengde = 0.0;

		if (stroprodtype != ManagerConstants.Stroprod.SALTLOSNING) {
			Double torrMengde = dryGritAmounts.get(ny.getTiltakId());

			if (torrMengde != null) {
				mengde = torrMengde;
			}
		}

		// Vi må vekte tonn/m3 på prodstrekningene som tiltaket har foregått på i tilfeller hvor de strekker seg over
		// flere rodestrekninger.
		double vekting = ny.getKm() / ny.getKmProdstrekning();
		mengde *= vekting;

		double saltlosning;

        switch (stroprodtype) {
            case TORRSALT:
                eksisterende.addTorrsaltTonn(mengde);
                break;
            case SLURRY:
                eksisterende.addSlurryTonn(mengde);
                break;
            case SALTLOSNING:
                saltlosning = saltlosningTilKubikk(ny.getTiltakId(), wetGrytAmounts) * vekting;
                eksisterende.addSaltlosningKubikk(saltlosning);
                break;
            case BEFUKTET_SALT:
                eksisterende.addBefuktetSaltTonn(mengde);
                break;
            case SAND_MED_SALT:
                eksisterende.addSandSaltTonn(mengde);
                break;
            case REN_SAND:
                eksisterende.addRenSandTonn(mengde);
                break;
            case FAST_SAND:
                eksisterende.addFastsandTonn(mengde);
                break;
            default:
                break;
        }

        eksisterende.addStroingTimer(
                kalkulerTimer(ny.getTilDatoRodestrekning(), ny.getFraDatoRodestrekning()));
	}

	private double saltlosningTilKubikk(Long tiltakId, Map<Long, Stroinfo> wetGrytAmounts) {
		double losning = 0;

		Stroinfo stroinfo = wetGrytAmounts.get(tiltakId);
		if (stroinfo != null) {
			if (stroinfo.getLiterLosning() != null) {
				losning = stroinfo.getLiterLosning() / 1000;
			} else if (stroinfo.getKgLosning() != null) {
				losning = stroinfo.getKgLosning() / ManagerConstants.WEIGHT_VOLUME_FACTOR / 1000; // Egenvekt for saltløsning er 1.18
            }
		}

		return losning;
	}

	private double kalkulerTimer(Date til, Date fra) {
		double tidTimer = 0.0;

		if (til != null && fra != null && fra.before(til)) {
			long tidMs = til.getTime() - fra.getTime();
			double tidSek = tidMs / 1000;
			tidTimer = tidSek / 3600;
		}

		return tidTimer;
	}

	private void oppdaterLeverandorNavn(List<Vinterdrift> vinterdriftklasser, Long kontraktId, Date fraDato, Date tilDato) {
		String leverandor;

		Iterator<Vinterdrift> it = vinterdriftklasser.iterator();
		while(it.hasNext()) {
			Vinterdrift vinterdrift = it.next();

			if (leverandorCache.containsKey(vinterdrift.getKjoretoyId())) {
				leverandor = leverandorCache.get(vinterdrift.getKjoretoyId());

				if (leverandor != null) {
					vinterdrift.setLeverandor(leverandor);
				} else {
					it.remove();
				}
			} else {
				LeverandorVinterdrift lev = new LeverandorForVinterdriftQuery(
						kontraktId, fraDato, tilDato, vinterdrift.getKjoretoyId(), em).getLeverandor();

				if (lev != null) {
					// Kjøretøyet er fortsatt knyttet til kontrakten og vi henter ut leverandørnavn/-nummer.
					leverandor = lev.getNavn() + " (" + lev.getNummer() + ")";
					vinterdrift.setLeverandor(leverandor);
				} else {
					// Hvis det viser ser at kjøretøyet ikke er tilknyttet kontrakten så skal det ikke være med på rapporten.
					it.remove();
					leverandor = null;
				}

				leverandorCache.put(vinterdrift.getKjoretoyId(), leverandor);
			}
		}
	}

	private Map<Long, Stroinfo> getWetGritAmounts(List<Long> measureIds, EntityManager em) {
		Map<Long, Stroinfo> wetGritAmounts = new HashMap<>();

		if (!measureIds.isEmpty()) {
			// We have to split up the data sent to the SQL-query to avoid hitting the 1000 argument limit of IN.
			int start;
			int stop = 0;

			int roundCounter = 1;
			int rounds = (int) Math.ceil(measureIds.size() / 1000d);

			while (roundCounter <= rounds) {
				start = stop;

				if (roundCounter == rounds) {
					stop = measureIds.size();
				} else {
					stop += 1000;
				}

				List<Long> tempList = measureIds.subList(start, stop);
				List<Stroinfo> wetGritAmountsRaw = new GritInfoForMeasuresQuery(tempList, em).getResults();

				for (Stroinfo info : wetGritAmountsRaw) {
					wetGritAmounts.put(info.getMeasureId(), info);
				}

				roundCounter++;
			}
		}

		return wetGritAmounts;
	}

}