package no.mesta.mipss.produksjonsrapport.excel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.rapportfunksjoner.Materiale;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.ReportMetadataItem.VALUE_NAMES;

public class ReportMetadata {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");

	private List<ReportMetadataItem> items;
	private int totalColumnCount;
	private int staticColumnCount = 0;
	private int dataRowCount;
	private final ProdrapportParams params;
	
	public ReportMetadata(List<? extends Rapp1Entity> veidata, ProdrapportParams params){
		this.params = params;
		init(veidata);
		calc();
	}
	
	/**
	 * Hent alle metadataene som trengs for rapporten
	 * @param veidata
	 */
	private void init(List<? extends Rapp1Entity> veidata) {
		List<ReportMetadataItem> alleTiltak = new ArrayList<ReportMetadataItem>();
		ReportMetadataItem stro=null;
		for (Rapp1Entity r:veidata){
			dataRowCount++;
			if (r.getProdtyper()!=null){
				for (R12Prodtype p:r.getProdtyper()){
					ReportMetadataItem t = new ReportMetadataItem();
					if (p.getId() == 0) { //BRØYTING/HØVLING INTENSJON
						t.setComment(getCommentForProdtype(p, null));
					}
					t.setHeader(p.getHeader());
					if (!alleTiltak.contains(t)){
						t.setSeq(p.getSeq().intValue());
						alleTiltak.add(t);
					}else{
						t = alleTiltak.get(alleTiltak.indexOf(t));
					}
					
					if (p.getStroFlagg()&&stro==null){
						stro = t;
					}
					
					if (!p.getStroFlagg()){
						if (p.getProdtyper()==null){
							t.addValueName(VALUE_NAMES.Km);
							t.addValueName(VALUE_NAMES.Timer);
						}else{ //undertyper for r12prodtyper
							createMetadataForSubprodtyper(t, p);
						}
						
						// Legger til Klipping-kolonnen hvis id = 9 (KLIPPING) 
						// og dersom rapporten skal inneholde M2.
						if (p.getId() == 9 && showM2()) {
							t.addValueName(VALUE_NAMES.M2);
						}	
						
						if (params.isProsent()){
							t.addValueName(VALUE_NAMES.Prosent);
						}
						if (params.isGjennomsnitt()){
							t.addValueName(VALUE_NAMES.Kmt);
						}
						if (params.isFrekvens()){
							t.addValueName(VALUE_NAMES.Frekvens);
						}
					}				
				}
			}
			if (r.getMaterialer()!=null&&r.getMaterialer().size()>0){
				ReportMetadataItem mengder = new ReportMetadataItem();
				mengder.setHeader("Utlagte mengder");
				mengder.setSeq(Integer.MAX_VALUE);
				mengder.setTyper(new ArrayList<ReportMetadataItem>());
				if (!alleTiltak.contains(mengder)){
					alleTiltak.add(mengder);
				}else{
					mengder = alleTiltak.get(alleTiltak.indexOf(mengder));
				}
				for (Materiale m:r.getMaterialer()){
					ReportMetadataItem mat = new ReportMetadataItem();
					mat.setHeader(m.getHeader());
					if (!mengder.getTyper().contains(mat)){
						mengder.getTyper().add(mat);
					}else{
						mat = mengder.getTyper().get(mengder.getTyper().indexOf(mat));
					}
					mat.setSeq(m.getSeq().intValue());
					mat.addValueName(VALUE_NAMES.Tonn);
				}
			}
			if (stro!=null){
				createMetadataForSubtyper(stro, r);
			}
		}
		setReportMetadataItem(alleTiltak);
		Comparator<ReportMetadataItem> comparator = new Comparator<ReportMetadataItem>(){
			@Override
			public int compare(ReportMetadataItem o1, ReportMetadataItem o2) {
				return Integer.valueOf(o1.getSeq()).compareTo(o2.getSeq());
			}
		};
		Collections.sort(alleTiltak, comparator);
		if (stro!=null&&stro.getTyper()!=null)
			Collections.sort(stro.getTyper(), comparator);
	}
	private void createMetadataForSubprodtyper(ReportMetadataItem prodtyper, R12Prodtype r){
		if (r==null){
		}
		if (r.getProdtyper()==null){
			return;
		}
		if (prodtyper.getTyper()==null){
			prodtyper.setTyper(new ArrayList<ReportMetadataItem>());
		}
		
		for (R12Prodtype s:r.getProdtyper()){
			ReportMetadataItem m = new ReportMetadataItem();
			m.setHeader(s.getHeader());
			
			if (!prodtyper.getTyper().contains(m)){
				m.setSeq(s.getSeq().intValue());
				prodtyper.getTyper().add(m);
			}else{
				m = prodtyper.getTyper().get(prodtyper.getTyper().indexOf(m));
			}
			if (s.getKjortKm()!=null&&s.getKjortKm()>Double.valueOf(0))
				m.addValueName(VALUE_NAMES.Km);
			if (s.getKjortSekund()!=null&&s.getKjortSekund()>Long.valueOf(0))
				m.addValueName(VALUE_NAMES.Timer);
			if (s.getMengdeM2()!=null&&s.getMengdeM2()>Long.valueOf(0))
				m.addValueName(VALUE_NAMES.M2);
		}
		ReportMetadataItem total = new ReportMetadataItem();
		total.setHeader("Total");
		total.addValueName(VALUE_NAMES.Km);
		if (!prodtyper.getTyper().contains(total)){
			total.setSeq(0);
			prodtyper.getTyper().add(0,total);
		}
	}
	
	private String getCommentForProdtype(R12Prodtype s, String ext){
		String key = "reportCommentsProdtype."+s.getId().intValue();
		if (ext!=null){
			key+=ext;
		}
		return resources.getGuiString(key);
	}
	
	/**
	 * Hent metadata for subtypene til denne raden
	 * @param stro
	 * @param r
	 */
	private void createMetadataForSubtyper(ReportMetadataItem stro, Rapp1Entity r) {
		if (r==null){
		}
		if (r.getStroprodukter()==null){
			return;
		}
		if (stro.getTyper()==null){
			stro.setTyper(new ArrayList<ReportMetadataItem>());
		}
//		ReportMetadataItem total = new ReportMetadataItem();
//		total.setHeader("Total");
//		total.addValueName(VALUE_NAMES.Km);
//		total.addValueName(VALUE_NAMES.Timer);
//		total.setSeq(0);
//		if (!stro.getTyper().contains(total)){
//			stro.getTyper().add(total);
//		}
		for (R12Stroprodukt s:r.getStroprodukter()){
			ReportMetadataItem m = new ReportMetadataItem();
			m.setHeader(s.getHeader());
			m.setComment(getCommentForStroProdukt(s, null));
			m.setCommentForTonn(getCommentForStroProdukt(s, ".tonn"));
			//Sjekk på evt id'er
			if(s.getId()!=5)
			{
				m.setCommentForM3(getCommentForStroProdukt(s, ".m3"));
			}
			
			if (!stro.getTyper().contains(m)){
				m.setSeq(s.getSeq().intValue());
				stro.getTyper().add(m);
			}else{
				m = stro.getTyper().get(stro.getTyper().indexOf(m));
			}
			if (s.getKjortKm()!=null&&s.getKjortKm()>Double.valueOf(0))
				m.addValueName(VALUE_NAMES.Km);
			if (s.getKjortSekund()!=null&&s.getKjortSekund()>Long.valueOf(0))
				m.addValueName(VALUE_NAMES.Timer);
			if (params.isProsent()){
				m.addValueName(VALUE_NAMES.Prosent);
			}
			if (params.isGjennomsnitt()){
				m.addValueName(VALUE_NAMES.Kmt);
			}
			if (params.isFrekvens()){
				m.addValueName(VALUE_NAMES.Frekvens);
			}
			if (s.getMengdeM3()!=null&&s.getMengdeM3()>Double.valueOf(0))
				m.addValueName(VALUE_NAMES.M3);
			if (s.getMengdeTonn()!=null&&s.getMengdeTonn()>Double.valueOf(0))
				m.addValueName(VALUE_NAMES.Tonn);
			
		}
	}
	
	private String getCommentForStroProdukt(R12Stroprodukt s, String ext){
		String key = "reportComments."+s.getId().intValue();
		if (ext!=null){
			key+=ext;
		}
		return resources.getGuiString(key);
	}

	public void setReportMetadataItem(List<ReportMetadataItem> items){
		this.items = items;
	}
	public List<ReportMetadataItem> getItems(){
		return items;
	}
	
	public ReportMetadataItem getMetadataItem(String header){
		ReportMetadataItem i = new ReportMetadataItem();
		i.setHeader(header);
		return items.get(items.indexOf(i));
	}
	
	
	public int getTotalColumnCount(){
		return totalColumnCount;
	}
	
	public int getDataRowCount(){
		return dataRowCount;
	}
	private void calc(){
		int totColumn = 0;
		for (ReportMetadataItem i:items){
			totColumn+=i.getColSpan();
		}
		totalColumnCount = totColumn+staticColumnCount;
	}
	
	private boolean showM2(){
		if (params.getRapportType() == ProdrapportParams.RapportType.KJORETOY.toString() || 
				params.getRapportType() == ProdrapportParams.RapportType.LEVERANDOR.toString() ||
				params.getRapportType() == ProdrapportParams.RapportType.RODE.toString() ||
				params.getRapportType() == ProdrapportParams.RapportType.VEI.toString()) {
			return true;
		} else {
			return false;
		}
	}
}
