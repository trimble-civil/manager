package no.mesta.mipss.produksjonsrapport.query;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportSequence;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NestedTableDescriptor;

public class Rapp1Query{
	private Query query;
    private static final String LEVERANDOR_ORDER_BY = "leverandor_navn, kjoretoy_navn, fra_dato_tid, til_dato_tid ";
    private static final String KJORETOY_ORDER_BY = "kjoretoy_navn, fra_dato_tid, til_dato_tid ";//ORDER BY KJORETOY_NAVN, FRA_DATO_TID, TIL_DATO_TID ";
	private static final String RODE_ORDER_BY = "rodenavn, fra_dato_tid, til_dato_tid";
	private final Rapp1Type type;
	public enum Rapp1Type{
		LEVERANDOR,
		KJORETOY,
		RODE
	}
	
    public Rapp1Query(ProdrapportParams params, EntityManager em, Rapp1Type type){
        
        this.type = type;
		StringBuilder qr = new StringBuilder();
        qr.append("select t1.rode_id, t1.rodenavn, t1.kjoretoy_id, t1.kjoretoy_navn, t1.paa_kontrakt_flagg, ");
        qr.append("t1.leverandor_nr, t1.leverandor_navn, t1.samproduksjon_flagg, t1.fra_dato_tid, t1.til_dato_tid, ");
        qr.append("t1.totalt_kjort_km, t1.totalt_kjort_sekund, t1.prodtyper, t1.stroprodukter, ");
        if (type.equals(Rapp1Type.RODE)){
        	qr.append("t2.lengde_km ");
        }else{
        	qr.append("0 ");
        }
        qr.append("from table(rapport_funksjoner.rapp1(?1, ?2, ?3, ?4, ?5)) t1 ");
        if (type.equals(Rapp1Type.RODE)){
        	qr.append(", veinettlengde_v t2 where t2.veinett_id=(select veinett_id from rode where id=t1.rode_id) ");
        }
        if (params.getKunKontraktKjoretoy()&&(type.equals(Rapp1Type.RODE))){
        	qr.append("and paa_kontrakt_flagg=1 ");
        }else if (params.getKunKontraktKjoretoy()){
        	qr.append("where paa_kontrakt_flagg=1 ");
        }
        qr.append("order by ");
        
        switch (type){
	        case LEVERANDOR: qr.append(LEVERANDOR_ORDER_BY);break;
	        case KJORETOY: qr.append(KJORETOY_ORDER_BY);break;
	        case RODE: qr.append(RODE_ORDER_BY);break;
        }
        
        query = em.createNativeQuery(qr.toString());
        query.setParameter(1, params.getDriftkontraktId());
        query.setParameter(2, params.getRodetypeId());
        query.setParameter(3, params.getRodeId());
        query.setParameter(4, params.getFraDato());
        query.setParameter(5, params.getTilDato());
    }

    public List<Rapp1> getResults(){
    	NativeQueryWrapper<Rapp1> qry = new NativeQueryWrapper<Rapp1>(
    			query, 
    			Rapp1.class, 
    			Rapp1.getPropertiesArray());
    	qry.setDateMask("yyyy-MM-dd HH:mm:ss");
		NestedTableDescriptor<R12Prodtype> prodtype = new NestedTableDescriptor<R12Prodtype>(R12Prodtype.class, R12Prodtype.getPropertiesArray());
		NestedTableDescriptor<R12Stroprodukt> stroprodukt = new NestedTableDescriptor<R12Stroprodukt>(R12Stroprodukt.class, R12Stroprodukt.getPropertiesArray());
		qry.addNestedTableDescriptor(prodtype);
		qry.addNestedTableDescriptor(stroprodukt);
		
		System.out.println("Rapp1Query.getResults() - starting query");
		List<Rapp1> wrappedList = qry.getWrappedList();
		System.out.println("Rapp1Query.getResults() - query done..");
		return wrappedList;
    }
    @Deprecated
    public IndexedSequence consumeResults(){
    	return null;
    }
}



