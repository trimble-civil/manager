package no.mesta.mipss.produksjonsrapport.web;

import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.Sammendragsrapport;
import no.mesta.mipss.produksjonsrapport.query.AkkumulertQuery;

public class SammendragsrapportHandler extends ExcelReportHSSFHandler{

	private final EntityManager em;
	private ProdrapportParams params;

	public SammendragsrapportHandler(EntityManager em) {
		super(null);
		this.em = em;
	}

	@Override
	public byte[] createReport(ProdrapportParams params) throws Exception {
		this.params = params;
		AkkumulertQuery q  = new AkkumulertQuery(params, em);
		List<Rapp1> results = q.getResults();
		Sammendragsrapport r = new Sammendragsrapport(results, params);
		return r.toByteArray();
	}
	/**
	 * TODO REMOVE
	 * @return
	 */
	public List<Rapp1> getResults(ProdrapportParams params) {
		AkkumulertQuery q  = new AkkumulertQuery(params, em);
		List<Rapp1> results = q.getResults();
		return results;
	}

}
