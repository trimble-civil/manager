package no.mesta.mipss.produksjonsrapport.values;

import java.io.Serializable;


//~--- JDK imports ------------------------------------------------------------


/**
 * Base for dataItems used as a base-class for data held by entity-beans
 */
public abstract class EntityCopy implements Serializable, Cloneable {
    protected long NULL_DATE = -1;
    protected long lastModified;

    /**
     * EntityCopy constructs a new EntityCopy .
     */
    public EntityCopy(long lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * Returns a long value representing how many times the item has been updated
     * @return long
     */
    public final long getLastModified() {
        return lastModified;
    }

    public final void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * Returns the String representation of this entity for use in ComboBoxes
     */
    public String getComboString() {
        return "";
    }

    /**
     * Returns the String representation of this entitys unique reference.
     * Used as value parameter in html
     */
    public String getComboKey() {
        return "";
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}


