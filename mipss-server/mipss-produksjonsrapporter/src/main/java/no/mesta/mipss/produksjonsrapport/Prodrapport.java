package no.mesta.mipss.produksjonsrapport;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.Daubestilling;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.rapportfunksjoner.*;

@Remote
public interface Prodrapport {
	public static final String BEAN_NAME="ProdrapportBean";
	List<Veirapp> getRapportObjects(ProdrapportParams params);
	byte[] getRapport(ProdrapportParams params) throws Exception;
	void sendDauBestilling(Daubestilling daubestilling);
	/**
	 * Lager en rodeoversiktrapport
	 * 
	 * @param params parametere til rapporten
	 * 			av propertiesene til denne klassen benyttes :
	 * 			driftkontraktId 
	 * 			rodetypeId kan være null og betyr alle rodetyper
	 * 			alleInkAvsl true om alle inklusive avsluttede skal være med
	 * 						false hvis kun aktive rode skal være med i rapporten
	 * @return
	 */
	byte[] getRodeoversiktRapport(ProdrapportParams params) throws Exception;
	
	/**
	 * TODO REMOVE
	 * @param params
	 * @return
	 */
	List<Rapp1> getAkkumulert(ProdrapportParams params);
	/**
	 * Lager en rapport over tiltak summert på veinummer
	 * @param params parametere til rapporten 
	 * 			driftkontraktId
	 * 			rodetypeId
	 * 			datoFra
	 * 			datoTil
	 * @return
	 * @throws Exception
	 */
	byte[] getVeirapport(ProdrapportParams params) throws Exception;
	
	List<R12> getR12Grunnlag(ProdrapportParams params);
	
	/**
	 * Lager en rapport over ukjent strøprodukter i en gitt periode, for en gitt kontrakt/kjøretøy.
	 * @param kontraktId
	 * @param fraDato
	 * @param tilDato
	 * @param kjoretoyId
	 */
	List<UkjentStroprodukt> genererUkjentStroproduktRapport(Long kontraktId, Date fraDato, Date tilDato, Long kjoretoyId);

	/**
	 * Lager en rapport over vinterdriftklasser for en gitt kontrakt/rodetype i en gitt periode fordelt på leverandør,
	 * rode, vinterdriftklasse, fylke, og vegtype.
	 * @param kontrakt
	 * @param rodeType
	 * @param fraDato
	 * @param tilDato
     * @return
     */
	List<Vinterdrift> genererVinterdriftklasserRapport(Driftkontrakt kontrakt, Long rodeType, Long rode, Date fraDato, Date tilDato);
}
