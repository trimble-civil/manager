package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class KjoretoyRapportSheet extends Rapp1Rapport{

	private final Rapp1Object kjoretoy;
	private String templateFile = "kjoretoy_sheet";
	int startRow = 9;
	int startCol = 7;
	private Map<String, Object> dataMap;
	
	public KjoretoyRapportSheet(Rapp1Object kjoretoy, ProdrapportParams params){
		super(params);
		this.kjoretoy = kjoretoy;
		initData();
		
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, dataMap);
		
		excel = new ExcelPOIHelper(workbook);
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, kjoretoy.getData(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(0), 13);
	}
	
	public KjoretoyRapportSheet(Rapp1Object kjoretoy, ProdrapportParams params, ExcelPOIHelper excel, XSSFWorkbook workbook, int sheetIndex){
		super(params);
		this.kjoretoy = kjoretoy;
		initData();
		this.sheetIndex = sheetIndex;
		
		this.excel = excel;
		this.workbook = workbook;
		file = RapportHelper.createTempFile(templateFile);
		String sheetName = kjoretoy.getKjoretoy();
		copyIntoMainWorkbook(sheetName, templateFile, dataMap);
		
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, kjoretoy.getData(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(sheetIndex==-1?0:sheetIndex), 13);
	}

	public XSSFSheet getSheet(){
		return workbook.getSheetAt(0);
	}
	private void initData() {
		metadata = new ReportMetadata(kjoretoy.getData(), params);
		dataMap = new HashMap<String,Object>();
		dataMap.put("data", kjoretoy.getData());
		dataMap.put("rapp1Object", kjoretoy);
		dataMap.put("params", params);
	}

	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		Rapp1 d= (Rapp1)data;
		excel.writeData(dataRow, 0, d.getLeverandorNavn());
		excel.writeData(dataRow, 1, d.getRodeNavn());
		excel.writeData(dataRow, 2, d.getFraDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 2);
		excel.writeData(dataRow, 3, d.getTilDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 3);
		excel.writeData(dataRow, 4, d.getSamprodFlagg());
		excel.writeData(dataRow, 5, d.getKjortKm());
		excel.writeData(dataRow, 6, d.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 6);
	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 0);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 1);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 2);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 3);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 4);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 5);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 6);
		
		addSumCell(5, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(6, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}
}
