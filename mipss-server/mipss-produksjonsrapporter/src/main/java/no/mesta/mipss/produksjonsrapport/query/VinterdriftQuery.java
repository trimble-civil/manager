package no.mesta.mipss.produksjonsrapport.query;

import no.mesta.mipss.persistence.rapportfunksjoner.Vinterdrift;
import no.mesta.mipss.query.NativeQueryWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class VinterdriftQuery {

	private Query query;

	public VinterdriftQuery(Long kontraktId, Long rodeType, Date fraDato, Date tilDato, EntityManager em) {
		StringBuilder q = buildQuery();

		leggTilRekkefolge(q);

		query = em.createNativeQuery(q.toString())
				.setParameter(1, kontraktId)
				.setParameter(2, rodeType)
				.setParameter(3, fraDato)
				.setParameter(4, tilDato);
		query.setHint("eclipselink.jdbc.fetch-size", "1000");
	}

	public VinterdriftQuery(Long kontraktId, Long rodeType, Long rode, Date fraDato, Date tilDato, EntityManager em) {
		StringBuilder q = buildQuery();

		q.append("	 AND");
		q.append("   r.id = ?5 ");

		leggTilRekkefolge(q);

		query = em.createNativeQuery(q.toString())
				.setParameter(1, kontraktId)
				.setParameter(2, rodeType)
				.setParameter(3, fraDato)
				.setParameter(4, tilDato)
				.setParameter(5, rode);
		query.setHint("eclipselink.jdbc.fetch-size", "1000");
	}

	private StringBuilder buildQuery() {
		StringBuilder q  = new StringBuilder();

		q.append("SELECT");
		q.append("   tt.kjoretoy_id,");
		q.append("   r.id AS rode_id,");
		q.append("   r.navn AS rodenavn,");
		q.append("   ps.vinterdriftklasse,");
		q.append("   ps.fylkesnummer,");
		q.append("   (ps.veikategori || ps.veistatus) AS vegtype,");
		q.append("   rs.fra_dato_tid AS fra_dato_rodestrekning,");
		q.append("   rs.til_dato_tid AS til_dato_rodestrekning, ");
		q.append("   rs.kjort_km AS km,");
		q.append("   rs.ps_kjort_km AS km_prodstrekning,");
		q.append("   tt.prodtype_id,");
		q.append("   tt.utstyr_id,");
		q.append("   tt.stroprodukt_id,");
		q.append("   pt.stro_flagg,");
		q.append("   pt.dau_broyte_flagg AS broyte_flagg,");
		q.append("   tt.id AS tiltak_id ");

		q.append("FROM rode r ");

		q.append("JOIN rodestrekning rs");
		q.append("   ON r.id = rs.rode_id ");
		q.append("JOIN prodstrekning ps");
		q.append("   ON rs.strekning_id = ps.id ");
		q.append("JOIN tiltak tt");
		q.append("   ON tt.strekning_id = ps.id ");
		q.append("JOIN prodtype pt");
		q.append("   ON tt.prodtype_id = pt.id ");

		q.append("WHERE");
		q.append("	 r.kontrakt_id = ?1");
		q.append("	 AND");
		q.append("	 r.type_id = ?2");
		q.append("	 AND");
		q.append("	 tt.fra_dato_tid >= ?3");
		q.append("	 AND");
		q.append("   tt.til_dato_tid <= ?4");
		q.append("	 AND");
		q.append("   r.utenfor_kontrakt_flagg = 0");
		q.append("   AND");
		q.append("   tt.prodtype_id != 98 ");

		return q;
	}

	private void leggTilRekkefolge(StringBuilder q) {
		q.append("ORDER BY");
		q.append("   r.id,");
		q.append("   tt.kjoretoy_id,");
		q.append("   tt.id");
	}

	public List<Vinterdrift> getResults() {
		NativeQueryWrapper<Vinterdrift> qry = new NativeQueryWrapper<>(
				query,
				Vinterdrift.class,
				Vinterdrift.getPropertiesArray());

		return qry.getWrappedList();
	}

}