package no.mesta.mipss.produksjonsrapport.values;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.MipssDateFormatter;

public class VeirapportForsideObject {
	
	private String type;
	private int veinummer;
	private Date datoFra;
	private Date datoTil;
	private VeirapportObject veirappObject;
	
	private List<TiltakObject> tiltakList;
	
	public VeirapportForsideObject(String type, int veinummer, Date datoFra){
		this.type = type;
		this.veinummer = veinummer;
		this.datoFra = datoFra;
	}
	
	public String getType(){
		return type;
	}
	public int getVeinummer(){
		return veinummer;
	}
	public String getTidsrom(){
		String fra = MipssDateFormatter.formatDate(datoFra, MipssDateFormatter.DATE_FORMAT);
		String til = MipssDateFormatter.formatDate(datoTil!=null?datoTil:datoFra, MipssDateFormatter.DATE_FORMAT);
		return fra+"-"+til;
	}
	
	public void setDatoTil(Date datoTil){
		//ikke sett denne dersom datoTil er et tidspunkt i den allerede satte perioden
		if (this.datoTil!=null&&(this.datoTil.after(datoTil)&&this.datoFra.before(datoTil)))
			return;
		//datoTil skal alltid komme etter datoFra.
		if (datoTil.before(datoFra)){
			Date temp = datoFra;
			datoFra = datoTil;
			this.datoTil = temp;
		}else
			this.datoTil = datoTil;
	}
	
	public void addTiltak(TiltakObject tiltak){
		if (tiltakList == null)
			tiltakList = new ArrayList<TiltakObject>();
		
		tiltakList.add(tiltak);
	}
	public void addTiltak(List<TiltakObject> tiltak){
		if (tiltakList == null)
			tiltakList = new ArrayList<TiltakObject>();
		for (TiltakObject o:tiltak){
			if (tiltakList.contains(o)){
				TiltakObject t = tiltakList.get(tiltakList.indexOf(o));
				if (t instanceof TiltakProduksjon){
					TiltakProduksjon old = (TiltakProduksjon) t;
					TiltakProduksjon current = (TiltakProduksjon) o;
					old.setKm(old.getKm()+current.getKm());
					old.setSekunder(old.getSekunder()+current.getSekunder());
				}
				if (t instanceof TiltakMengde){
					TiltakMengde old=(TiltakMengde) t;
					TiltakMengde current = (TiltakMengde) o;
					old.setMengde(old.getMengde()+current.getMengde());
				}
			}else{
				tiltakList.add(o);
			}
		}
	}
	
	public TiltakObject getTiltak(String navn, Class type){
		for (TiltakObject o:tiltakList)
			if (o.getNavn().equals(navn)&&o.getClass().equals(type))
				return o;
		return null;
	}
	
	public List<TiltakObject> getTiltak(){
		return tiltakList;
	}
	public int hashCode(){
		return (type+""+veinummer).hashCode();
	}
	
	public boolean equals(Object o){
		if (!(o instanceof VeirapportForsideObject))
			return false;
		if (o==this)
			return true;
		VeirapportForsideObject other = (VeirapportForsideObject) o;
		
		return other.getType().equals(getType())&&other.getVeinummer()==getVeinummer();
	}

	public void setVeirappObject(VeirapportObject veirappObject) {
		if (this.veirappObject!=null){
			this.veirappObject.setTotalKjort(this.veirappObject.getTotalKjort()+veirappObject.getTotalKjort());
			this.veirappObject.setSekunderKjort(this.veirappObject.getSekunderKjort()+veirappObject.getSekunderKjort());
		}else{
			this.veirappObject = veirappObject;
		}
	}

	public VeirapportObject getVeirappObject() {
		return veirappObject;
	}
	
}
