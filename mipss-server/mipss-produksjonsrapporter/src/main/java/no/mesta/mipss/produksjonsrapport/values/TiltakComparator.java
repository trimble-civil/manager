package no.mesta.mipss.produksjonsrapport.values;

import java.io.Serializable;

import java.util.Comparator;


//~--- JDK imports ------------------------------------------------------------


public class TiltakComparator<T extends TiltakObject> implements Comparator<T>, Serializable {

    public int compare(T obj1, T obj2) {
        int tiltak1Sortering = obj1.getSortering();
        int tiltak2Sortering = obj2.getSortering();
        
        if (tiltak1Sortering == tiltak2Sortering) {
            return 0;
        } else if (tiltak1Sortering < tiltak2Sortering) {
            return -1;
        } else {
            return 1;
        }
    }
}


