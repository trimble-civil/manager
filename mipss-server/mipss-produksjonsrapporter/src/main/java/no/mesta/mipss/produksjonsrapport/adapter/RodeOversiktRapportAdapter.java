package no.mesta.mipss.produksjonsrapport.adapter;

import no.mesta.mipss.produksjonsrapport.values.EntityCopy;
import no.mesta.mipss.produksjonsrapport.values.IndexedSequence;
import no.mesta.mipss.produksjonsrapport.values.RodeStrekningObject;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * MIPSS1 port
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RodeOversiktRapportAdapter {
    private RodeOversiktRapport rodeOversiktRapport;

    public RodeOversiktRapportAdapter(RodeOversiktRapport rodeOversiktRapport) {
        this.rodeOversiktRapport = rodeOversiktRapport;
    }

    public int createReportHeader(HSSFWorkbook workbook, HSSFSheet sheet, 
                                  EntityCopy object, 
                                  int rowCount, HSSFSheet arkForside, int forsideRowCount, String arkNavn) throws Exception {
        int rowIndex = 0;

        try {
            rowIndex = rodeOversiktRapport.createRodeOversiktReportHeader(workbook, sheet, rowCount, object, arkForside, forsideRowCount, arkNavn);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }

        return rowIndex;
    }
    public int createForsideHeader(HSSFSheet arkForside, EntityCopy object, String roder){
        return rodeOversiktRapport.createForsideReportHeader(arkForside, (RodeStrekningObject)object, roder);
    }
    
    public int createTableHeader(HSSFWorkbook workbook, HSSFSheet sheet, int rowCount) {
        return rodeOversiktRapport.createRodeOversiktTableHeader(workbook, sheet, rowCount);
    }
    public int createForsideTableHeader(HSSFSheet arkForside, int forsideRowCount){
        return rodeOversiktRapport.createForsideTableHeader(arkForside, forsideRowCount);
    }
    public void createDataRows(HSSFWorkbook workbook, HSSFSheet sheet, IndexedSequence sequence, int rowCount) {
        rodeOversiktRapport.createRodeOversiktDataRows(workbook, sheet, sequence, rowCount);
    }
}


