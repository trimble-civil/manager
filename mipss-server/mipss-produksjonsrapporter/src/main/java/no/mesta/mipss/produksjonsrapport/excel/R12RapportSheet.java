package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.R12;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Object;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class R12RapportSheet extends Rapp1Rapport{
	private String templateFile = "r12_forside";
	private Map<String, Object> dataMap;
	private final R12Object data;
	public R12RapportSheet(R12Object data, ProdrapportParams params){
		super(params);
		this.data = data;
		initData();
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, dataMap);
		excel = new ExcelPOIHelper(workbook);
		startRow = 9;
		startCol = 3;
		
		excel.getRow(getSheet(), 9).getCell(0).setCellValue("Veinummer");
		
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, data.getR12Data(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(8,0);
		addAutoFilter(workbook.getSheetAt(0), 13);
	}
	
	public XSSFSheet getSheet(){
		return workbook.getSheetAt(0);
	}
	private void initData() {
		metadata = new ReportMetadata(data.getR12Data(), params);
		dataMap = new HashMap<String,Object>();
		dataMap.put("params", params);
	}
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		R12 d= (R12)data;
		String vei = d.getVeikategori()+"V"+d.getVeinummer();
		excel.writeData(dataRow, 0, vei);
		excel.writeData(dataRow, 1, d.getTotalKjortKm());
		double tid = (double)d.getTotalKjortSekund().doubleValue()/(double)86400;
		excel.writeData(dataRow, 2, tid);
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 2);
		
	}

	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 0);
		addSumCell(1, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(2, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}

}
