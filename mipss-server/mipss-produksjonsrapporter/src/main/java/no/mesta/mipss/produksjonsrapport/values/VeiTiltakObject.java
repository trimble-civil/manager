package no.mesta.mipss.produksjonsrapport.values;

import java.io.Serializable;

public class VeiTiltakObject implements Serializable{
	private double totalKjort;
	private int sekunderKjort;
	private String prodtype;
	private Double stromengde;
	private String materiale;
	private Integer sorteringProdtype;
	private Integer sorteringMateriale;
	
	
	public double getTotalKjort() {
		return totalKjort;
	}
	public void setTotalKjort(double totalKjort) {
		this.totalKjort = totalKjort;
	}
	public int getSekunderKjort() {
		return sekunderKjort;
	}
	public void setSekunderKjort(int sekunderKjort) {
		this.sekunderKjort = sekunderKjort;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public Double getStromengde() {
		return stromengde;
	}
	public void setStromengde(Double stromengde) {
		this.stromengde = stromengde;
	}
	public String getMateriale() {
		return materiale;
	}
	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}
	public Integer getSorteringProdtype() {
		return sorteringProdtype;
	}
	public void setSorteringProdtype(Integer sorteringProdtype) {
		this.sorteringProdtype = sorteringProdtype;
	}
	public Integer getSorteringMateriale() {
		return sorteringMateriale;
	}
	public void setSorteringMateriale(Integer sorteringMateriale) {
		this.sorteringMateriale = sorteringMateriale;
	}
}
