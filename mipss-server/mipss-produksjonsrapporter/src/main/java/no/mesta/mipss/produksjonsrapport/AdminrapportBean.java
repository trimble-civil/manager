package no.mesta.mipss.produksjonsrapport;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;
import no.mesta.mipss.persistence.kontrakt.SlettFeltstatistikk;
import no.mesta.mipss.persistence.rapportfunksjoner.DatakvalitetHelper;
import no.mesta.mipss.persistence.rapportfunksjoner.Hullrapport;
import no.mesta.mipss.persistence.rapportfunksjoner.InspeksjonStatF;
import no.mesta.mipss.persistence.rapportfunksjoner.KapasitetRapportObject;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallDatakvalitet;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallKjoretoy;
import no.mesta.mipss.persistence.rapportfunksjoner.RegionTall;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.RolleBruker;
import no.mesta.mipss.query.NativeQueryWrapper;

import org.apache.commons.lang.StringUtils;

@Stateless(name=AdminrapportService.BEAN_NAME, mappedName="ejb/"+AdminrapportService.BEAN_NAME)

public class AdminrapportBean implements AdminrapportService{
	@PersistenceContext(unitName="mipssPersistence")
    private EntityManager em;
	@EJB
	private KonfigParamLocal konfig;
	
	public List<Hullrapport> hentHullrapport() {
		StringBuilder query = new StringBuilder();
		query.append("select d.kontraktnummer, d.kontraktnavn, km_overlapp, km_hull, km_utenfor "); 
		query.append("from driftkontrakt d left join ");
		query.append("(select kontraktnummer, ");
		query.append("sum(hull) km_hull ");
		query.append("from table(valider_pk.hull_stro_broyte) "); 
		query.append("group by kontraktnummer) h on h.kontraktnummer = d.kontraktnummer "); 
		query.append("left join ");
		query.append("(select kontraktnummer, "); 
		query.append("sum(overlapp) km_overlapp "); 
		query.append("from table(valider_pk.overlapp_stro_broyte) "); 
		query.append("group by kontraktnummer) o on o.kontraktnummer = d.kontraktnummer ");
		query.append("left join ");
		query.append("(select kontraktnummer, "); 
		query.append("sum(utenfor) km_utenfor "); 
		query.append("from table(valider_pk.stro_broyte_utenfor_kontrakt) "); 
		query.append("group by kontraktnummer) u on u.kontraktnummer = d.kontraktnummer ");
		
		query.append("where km_overlapp is not null or km_hull is not null or km_utenfor is not null ");
		query.append("order by d.kontraktnummer");
		
		Query q = em.createNativeQuery(query.toString());
		NativeQueryWrapper<Hullrapport> w = new NativeQueryWrapper<Hullrapport>(q, Hullrapport.class, Hullrapport.getPropertyArray());
		return w.getWrappedList(); 
	}

	@Override
	public List<NokkeltallFelt> hentTilstandsrapportGrunndata(Long year, Long month) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT N.KONTRAKT_ID, D.KONTRAKTNUMMER||' - '||D.KONTRAKTNAVN KONTRAKTNAVN, (SELECT SUM(ABS(V.FRA_KM-V.TIL_KM)) FROM VEINETT_V V WHERE V.VEINETT_ID=D.VEINETT_ID) KONTRAKTVEINETT_KM, DR.ID REGION_ID, ");
		query.append("  DR.NAVN REGIONNAVN, N.FRA_DATO, N.TIL_DATO, N.ANT_INSP_OPPRETTET_ALLE, N.ANT_INSP_OPPRETTET, N.ANT_R2_OPPRETTET_ALLE, N.ANT_R2_RAPPORTERT_ALLE, N.ANT_R2_OPPRETTET, N.ANT_R2_RAPPORTERT,  ");
		query.append("N.ANT_R5_OPPRETTET_ALLE, N.ANT_R5_RAPPORTERT_ALLE, ANT_R5_OPPRETTET, N.ANT_R5_RAPPORTERT, N.ANT_R11_OPPRETTET_ALLE, N.ANT_R11_RAPPORTERT_ALLE, N.ANT_R11_OPPRETTET, N.ANT_R11_RAPPORTERT,  ");
		query.append("  N.ANT_TILLEGGSJOBBER_ALLE, N.ANT_TILLEGGSJOBBER, N.ANT_AVVIK_OPPRETTET_ALLE, N.ANT_AVVIK_LUKKET_ALLE, ANT_AVVIK_OPPRETTET, N.ANT_AVVIK_LUKKET, N.ANT_MANGLER_ALLE,  ");
		query.append("  N.ANT_MANGLER_LUKKET_ALLE, N.ANT_MANGLER, N.ANT_MANGLER_AAPNE, N.ANT_MANGLER_LUKKET, N.ANT_DAU_BEST_ALLE, N.ANT_DAU_BEST, N.ANT_RETARDASJON_ALLE, N.KM_KONT_FRIKSJON_TOTALT, N.ANT_RETARDASJON, ");
		query.append("  N.KM_KONT_FRIKSJON ");
		query.append("FROM NOKKELTALL_FELT N ");
		query.append("JOIN DRIFTKONTRAKT D ON D.ID = N.KONTRAKT_ID ");
		query.append("JOIN DRIFTDISTRIKT DD ON DD.ID = D.DISTRIKT_ID ");
		query.append("JOIN DRIFTREGION DR ON DR.ID = DD.REGION_ID ");
		query.append("WHERE D.KUNDE_NR=?3 ");
		query.append("AND D.GYLDIG_TIL_DATO>?1 ");
		query.append("AND D.GYLDIG_FRA_DATO<?2 ");
		query.append("AND N.TIL_DATO >= ?1 ");//TO_DATE('01.01.2012 00:00:00', 'dd.mm.yyyy hh24:mi:ss') ");
		query.append("AND N.FRA_DATO <= ?2 ");//TO_DATE('31.01.2012 00:00:00', 'dd.mm.yyyy hh24:mi:ss') ");
		query.append("AND N.FRA_DATO >= ?1 ");//TO_DATE('01.01.2012 00:00:00', 'dd.mm.yyyy hh24:mi:ss') ");
		query.append("order by D.KONTRAKTNUMMER, fra_dato ");
		
		Date datoFra = getFirstDateInMonth(year, month);
		Calendar cal = Calendar.getInstance(); //må se på hele måneden, for å få til dette må datoFra være siste i forrige måned.
		cal.setTime(datoFra);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		datoFra = cal.getTime();
		Date datoTil = getLastDateInMonth(year, month);
		
		Query q = em.createNativeQuery(query.toString())
			.setParameter(1, datoFra)
			.setParameter(2, datoTil)
			.setParameter(3, 1001); //kun for svv kontrakter
		
		NativeQueryWrapper<NokkeltallFelt> n = new NativeQueryWrapper<NokkeltallFelt>(q, NokkeltallFelt.class, NokkeltallFelt.getPropertiesArray());
		return n.getWrappedList();
	}
	
	
	@SuppressWarnings("unchecked")
	public NokkeltallDatakvalitet hentDatakvalitetstall(Long driftkontraktId){
		StringBuilder query = new StringBuilder();
		query.append("select kjoretoy_id, dfu_serienummer, dfu_siste_livstegn_dato, dfu_siste_posisjon_dato, spr_uten_datafangst_flagg, aktuell_stroperiode_klartekst, aktuell_stroperiode_fra_dato, aktuell_stroperiode_til_dato,  ");
		query.append("ukjent_strometode_flagg, ukjent_produksjon_flagg, bestillingstatus_dato, bestilling_status, aktuell_konfig_klartekst ");
		query.append("from table(kjoretoy_pk.kontrakt_kjoretoy_info(?1, ?2)) ");
		
		Query q = em.createNativeQuery(query.toString(), "KontraktKjoretoyInfoResult")
			.setParameter(1, driftkontraktId)
			.setParameter(2, new Date());
		List<KontraktKjoretoyInfo> resultList = q.getResultList();
		
//		Driftkontrakt kontrakt = em.find(Driftkontrakt.class, driftkontraktId);
		String kontraktnavn = (String)em.createNativeQuery("select kontraktnummer||' - '||kontraktnavn from driftkontrakt where id = ?").setParameter(1, driftkontraktId).getSingleResult();
//		String kontraktnavn = kontrakt.getKontraktnummer()+" - "+kontrakt.getKontraktnavn();
		NokkeltallDatakvalitet kvalitet = new NokkeltallDatakvalitet();
		kvalitet.setKontraktnavn(kontraktnavn);
		
		DatakvalitetHelper dkhelper = new DatakvalitetHelper();
		for(KontraktKjoretoyInfo info:resultList){
			//Dersom kjøretøyet ikke har serienummer skal kun ikke produksjonssatte bestilliger sjekkes. 
			if (StringUtils.isBlank(info.getDfuSerienummer())){
				if (dkhelper.checkIkkeProduksjonssatteBestillinger(info)){
					kvalitet.incAntIkkeProduksjonsatteBestillinger();
					kvalitet.incAntTotalKjoretoyMFeil();
				}
				continue;
			}
			boolean feil = false;
			Date sisteLivstegn = info.getDfuSisteLivtegnDato();
			if (dkhelper.checkSisteLivstegn3d(sisteLivstegn)){
				kvalitet.incAntKjoretoyUtenSisteLivstegn3d();
				feil = true;
			}
			if (dkhelper.checkSisteLivstegn14d(sisteLivstegn)){
				kvalitet.incAntKjoretoyUtenSisteLivstegn14d();
				feil = true;
			}
			
			Date sistePosisjonDato = info.getDfuSistePosisjonDatoCET();
			if (dkhelper.checkSisteGPSPosisjon3d(sistePosisjonDato)){
				kvalitet.incAntKjoretoyUtenSisteGps3d();
				feil = true;
			}
			if (dkhelper.checkSisteGPSPosisjon14d(sistePosisjonDato)){
				kvalitet.incAntKjoretoyUtenSisteGps14d();
				feil = true;
			}
			
			if (dkhelper.checkManuellSprederUtenGyldigStroperiode(info)){
				kvalitet.incAntKjoretoyManuellSprederUtenGyldigStroperiode();
				feil = true;
			}
			
			if (dkhelper.checkUkjentStrometode(info.getUkjentStrometode())){
				kvalitet.incAntKjoretoyUkjentStrometode();
				feil = true;
			}
			if (dkhelper.checkUkjentProduksjon(info.getUkjentProduksjon())){
				kvalitet.incAntKjoretoyUkjentProduksjon();
				feil = true;
			}
			if (dkhelper.checkIkkeProduksjonssatteBestillinger(info)){
				kvalitet.incAntIkkeProduksjonsatteBestillinger();
				feil = true;
			}
			if (feil){
				kvalitet.incAntTotalKjoretoyMFeil();
			}
		}
		
		return kvalitet;
	}
	
	public NokkeltallFelt hentDekningsgradTilDashboard(Long driftkontraktId, Date fraDato, Date tilDato){
		
		StringBuilder qry = new StringBuilder();
		qry.append("SELECT veikategori, veinett_lengde, insp_km, dekningsgrad ");
		qry.append("from table(statistikk_pk.insp_stat_f(?1, ?2, ?3))"); //1:kontraktid, 2,3 dato fra/til
		Query q = em.createNativeQuery(qry.toString())
			.setParameter(1, driftkontraktId)
			.setParameter(2, fraDato)
			.setParameter(3, tilDato);
		NativeQueryWrapper<InspeksjonStatF> n = new NativeQueryWrapper<InspeksjonStatF>(q, InspeksjonStatF.class, InspeksjonStatF.getPropertiesArray());
		List<InspeksjonStatF> dekningsgradKontrakt = n.getWrappedList();
		
		
		NokkeltallFelt nokkeltall = new NokkeltallFelt();
		
		for (InspeksjonStatF i:dekningsgradKontrakt){
			if (i.getVeikategori().equals("E")){
				nokkeltall.setVeinettLengdeEv(i.getVeinettLengde());
				nokkeltall.setInspKmEv(i.getInspKm());
			}
			if (i.getVeikategori().equals("R")){
				nokkeltall.setVeinettLengdeRv(i.getVeinettLengde());
				nokkeltall.setInspKmRv(i.getInspKm());
			}
			if (i.getVeikategori().equals("F")){
				nokkeltall.setVeinettLengdeFv(i.getVeinettLengde());
				nokkeltall.setInspKmFv(i.getInspKm());
			}
			if (i.getVeikategori().equals("K")){
				nokkeltall.setVeinettLengdeKv(i.getVeinettLengde());
				nokkeltall.setInspKmKv(i.getInspKm());
			}		
		}
		
		return nokkeltall;
	}
	
	
	//TIL DASHBOARDET
	public NokkeltallFelt hentTilstandsrapportKontrakt(Long driftkontraktId, Date fraDato, Date tilDato){
		
		StringBuilder query = new StringBuilder();
		query.append("select n.kontrakt_id, d.kontraktnummer||' - '||d.kontraktnavn kontraktnavn, (select sum(abs(v.fra_km-v.til_km)) from veinett_v v where v.veinett_id=d.veinett_id) kontraktveinett_km, dr.id region_id, "); 
		query.append("dr.navn regionnavn, n.fra_dato, n.til_dato, n.ant_insp_opprettet_alle, n.ant_insp_opprettet, n.ant_r2_opprettet_alle, n.ant_r2_rapportert_alle, n.ant_r2_opprettet, n.ant_r2_rapportert, ");  
		query.append("n.ant_r5_opprettet_alle, n.ant_r5_rapportert_alle, ant_r5_opprettet, n.ant_r5_rapportert, n.ant_r11_opprettet_alle, n.ant_r11_rapportert_alle, n.ant_r11_opprettet, n.ant_r11_rapportert, ");  
		query.append("n.ant_tilleggsjobber_alle, n.ant_tilleggsjobber, n.ant_avvik_opprettet_alle, n.ant_avvik_lukket_alle, ant_avvik_opprettet, n.ant_avvik_lukket, n.ant_mangler_alle,  ");
		query.append("n.ant_mangler_lukket_alle, n.ant_mangler, n.ant_mangler_aapne, n.ant_mangler_lukket, n.ant_dau_best_alle, n.ant_dau_best, n.ant_retardasjon_alle, n.km_kont_friksjon_totalt, n.ant_retardasjon, "); 
		query.append("n.km_kont_friksjon ");
		query.append("from table(statistikk_pk.felt_stat_f(?1, ?2, ?3)) n "); //kontraktId, fraDato, tilDato
		query.append("join driftkontrakt d on d.id = n.kontrakt_id ");
		query.append("join driftdistrikt dd on dd.id = d.distrikt_id ");
		query.append("join driftregion dr on dr.id = dd.region_id ");
		
		NokkeltallFelt nokkeltall =null;
		try{
			nokkeltall = (NokkeltallFelt)em.createNativeQuery(query.toString(), NokkeltallFelt.FIELD_RESULT_MAPPING)
			.setParameter(1, driftkontraktId)
			.setParameter(2, fraDato)
			.setParameter(3, tilDato).getSingleResult();
		} catch (NoResultException e){
			nokkeltall = new NokkeltallFelt();
			String kontraktnavn = (String)em.createNativeQuery("select kontraktnummer||' - '||kontraktnavn from driftkontrakt where id = ?").setParameter(1, driftkontraktId).getSingleResult();
			nokkeltall.setKontraktnavn(kontraktnavn);
			return nokkeltall;
		}
		StringBuilder qry = new StringBuilder();
		qry.append("SELECT veikategori, veinett_lengde, insp_km, dekningsgrad ");
		qry.append("from table(statistikk_pk.insp_stat_f(?1, ?2, ?3))"); //1:kontraktid, 2,3 dato fra/til
		Query q = em.createNativeQuery(qry.toString())
			.setParameter(1, driftkontraktId)
			.setParameter(2, fraDato)
			.setParameter(3, tilDato);
		NativeQueryWrapper<InspeksjonStatF> n = new NativeQueryWrapper<InspeksjonStatF>(q, InspeksjonStatF.class, InspeksjonStatF.getPropertiesArray());
		List<InspeksjonStatF> dekningsgradKontrakt = n.getWrappedList();
		
		oppdaterMedDekningsgrad(dekningsgradKontrakt, nokkeltall);
		
		return nokkeltall;
	}
	
	/**
	 * Kjører samme query som for tilstandsrapporten, men henter kun ut det man skal ha tak i, dataene knas og vaskes heller ikke.
	 * Gjort for å optimalisere spørringen og generell behandling litt, da dashboardet, som denne kalles fra, kjører en del kompliserte queries.
	 */
	public NokkeltallFelt hentAntRetKmKontForKontrakt(Long driftkontraktId, Date fraDato, Date tilDato){
		StringBuilder query = new StringBuilder();
		query.append("select n.kontrakt_id, dr.id region_id, "); 
		query.append("n.ant_retardasjon_alle, n.km_kont_friksjon_totalt, n.ant_retardasjon, "); 
		query.append("n.km_kont_friksjon ");
		query.append("from table(statistikk_pk.felt_stat_f(?1, ?2, ?3)) n "); //kontraktId, fraDato, tilDato
		query.append("join driftkontrakt d on d.id = n.kontrakt_id ");
		query.append("join driftdistrikt dd on dd.id = d.distrikt_id ");
		query.append("join driftregion dr on dr.id = dd.region_id ");
		
		try{
			NokkeltallFelt nokkeltall =null;
			nokkeltall = (NokkeltallFelt)em.createNativeQuery(query.toString(), NokkeltallFelt.FIELD_RESULT_MAPPING)
				.setParameter(1, driftkontraktId)
				.setParameter(2, fraDato)
				.setParameter(3, tilDato).getSingleResult();
			return nokkeltall;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	/**
	 * Henter brukerens kontrakt tilgang. Ser på rolle og kontraktstilhørighet
	 */
	public List<Long> hentKontrakttilgang(String brukerSign){
		Bruker bruker = em.find(Bruker.class, brukerSign);
		String rolleNavnAlleKontrakter = konfig.hentEnForApp("mipss.driftkontrakt", "rolleNavnForAlleKontrakter").getVerdi();
		boolean alleKontrakter = false;
		List<RolleBruker> roller = bruker.getRolleBrukerList();
		for (RolleBruker rb:roller){
			if (rb.getRolle().getNavn().equals(rolleNavnAlleKontrakter)){
				alleKontrakter = true;
				break;
			}
		}
		if (alleKontrakter){
			return getAlleKontraktId();
		}
		
		
		return getKontrakterForBruker(brukerSign);
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Henter liste med kontrakt_id der brukeren er knyttet til kontrakten. 
	 * For komplett liste over kontrakter brukeren har tilgang til benyttes hentKontrakttilgang(String brukerSign)
	 */
	private List<Long> getKontrakterForBruker(String brukerSign){
		StringBuilder sb = new StringBuilder();
		sb.append("select ks.kontrakt_id ");
		sb.append("	from kontrakt_stab ks ");
		sb.append(" join driftkontrakt k on k.id = ks.kontrakt_id ");
		sb.append(" where lower(ks.signatur) = lower(?) ");
		sb.append(" order by k.kontraktnummer ");
		try{
			List<BigDecimal> list= em.createNativeQuery(sb.toString()).setParameter(1, brukerSign).getResultList();
			List<Long> kontrakter = new ArrayList<Long>();
			for (BigDecimal b:list){
				kontrakter.add(b.longValue());
			}
			return kontrakter;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<Long> getAlleKontraktId(){
		StringBuilder sb = new StringBuilder();
		sb.append("select id ");
        sb.append("	from driftkontrakt ");
        sb.append("	where type_navn = 'Funksjonskontrakt' ");
        sb.append("	and gyldig_fra_dato < sysdate order by kontraktnummer");
        
        try{
	        List<BigDecimal> list=  em.createNativeQuery(sb.toString()).getResultList();
	        List<Long> ids = new ArrayList<Long>();
	        for (BigDecimal d:list){
	        	ids.add(d.longValue());
	        }
	        return ids;
        } catch (Exception e){
        	e.printStackTrace();
        	return null;
        }
	}
	
	public List<NokkeltallFelt> hentTilstandsrapportKontrakt(String brukerSign, Date fraDato, Date tilDato){
		List<NokkeltallFelt> rapp = new ArrayList<NokkeltallFelt>();
		List<Long> kontrakterForBruker = hentKontrakttilgang(brukerSign);
		if (kontrakterForBruker==null)
			return null;
		for (Long id:kontrakterForBruker){
			rapp.add(hentTilstandsrapportKontrakt(id, fraDato, tilDato));
		}
		return rapp;
	}
	
	public List<NokkeltallDatakvalitet> hentDatakvalitetstall(String brukerSign){
		List<NokkeltallDatakvalitet> rapp = new ArrayList<NokkeltallDatakvalitet>();
		List<Long> kontrakterForBruker = hentKontrakttilgang(brukerSign);
		if (kontrakterForBruker==null)
			return null;
		for (Long id:kontrakterForBruker){
			rapp.add(hentDatakvalitetstall(id));
		}
		return rapp;
	}
	
	private Date getFirstDateInMonth(Long year, Long month){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year.intValue());
		c.set(Calendar.MONTH, month.intValue());
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
	private Date getLastDateInMonth(Long year, Long month){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year.intValue());
		c.set(Calendar.MONTH, month.intValue());
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return c.getTime();
	}
	public List<RegionTall> hentTilstandsrapport(Long year, Long month) {
		List<NokkeltallFelt> grunndata = hentTilstandsrapportGrunndata(year, month);
		List<RegionTall> regionTall = new ArrayList<RegionTall>();
		
		List<NokkeltallFelt> tempPrKontrakt = new ArrayList<NokkeltallFelt>();
		NokkeltallFelt prev=null;
		for (NokkeltallFelt tf:grunndata){
			if (prev==null){
				prev = tf;
				tempPrKontrakt.add(tf);
				continue;
			}
			if (tf.getKontraktId().equals(prev.getKontraktId())){
				tempPrKontrakt.add(tf);
			}else{ //ny kontrakt, slå sammen alle kontraktene som er samlet hittil.
				joinNokkeltallMedRegion(regionTall, tempPrKontrakt);
			}
			prev = tf;
		}
		if (!tempPrKontrakt.isEmpty()){
			joinNokkeltallMedRegion(regionTall, tempPrKontrakt);
		}
		Date firstDateInMonth = getFirstDateInMonth(year, month);
		StringBuilder qryInspeksjon = new StringBuilder();
		qryInspeksjon.append("SELECT veikategori, veilengde_km, inspisert_km ");
		qryInspeksjon.append("from nokkeltall_inspeksjon ");
		qryInspeksjon.append(" where kontrakt_id = ?1 and maaned = ?2 ");
		
		StringBuilder qryKjoretoy = new StringBuilder();
		qryKjoretoy.append("SELECT kontrakt_id, maaned, antall_kjoretoy, antall_kjoretoy_uten_livstegn, opprettet_dato ");
		qryKjoretoy.append("from nokkeltall_kjoretoy ");
		qryKjoretoy.append(" where kontrakt_id =?1 and maaned = ?2 ");
		
		for (RegionTall rt:regionTall){
			for (NokkeltallFelt nf:rt.getKontrakter()){
				Query q = em.createNativeQuery(qryInspeksjon.toString())
				.setParameter(1, nf.getKontraktId())
				.setParameter(2, firstDateInMonth);
				NativeQueryWrapper<InspeksjonStatF> n = new NativeQueryWrapper<InspeksjonStatF>(q, InspeksjonStatF.class, InspeksjonStatF.getPropertiesArray());
				List<InspeksjonStatF> dekningsgradKontrakt = n.getWrappedList();
				oppdaterMedDekningsgrad(dekningsgradKontrakt, nf);
				
				Query qk = em.createNativeQuery(qryKjoretoy.toString())
				.setParameter(1, nf.getKontraktId())
				.setParameter(2, firstDateInMonth);
				NativeQueryWrapper<NokkeltallKjoretoy> kjoretoy = new NativeQueryWrapper<NokkeltallKjoretoy>(qk, NokkeltallKjoretoy.class, NokkeltallKjoretoy.getPropertiesArray());
				NokkeltallKjoretoy nokkeltallKjoretoy = kjoretoy.getWrappedSingleResult();
				if (nokkeltallKjoretoy!=null)
					oppdaterMedKjoretoy(nokkeltallKjoretoy, nf);
				
			}
		}
		return regionTall;
	}
	
	public Map<String, Vector<?>> hentSpokelsesrapport(){
		try(Connection connection  = getDatabaseConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("select * from spokelse_v")) {
			ResultSetMetaData metaData = rs.getMetaData();
			return getTableDataFromResultSet(rs, metaData);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return new HashMap<String, Vector<?>>();
	}

	private void oppdaterMedDekningsgrad(List<InspeksjonStatF> insp, NokkeltallFelt kontrakt){
		for (InspeksjonStatF i:insp){
			if (i.getVeikategori().equals("E")){
				kontrakt.setVeinettLengdeEv(i.getVeinettLengde());
				kontrakt.setInspKmEv(i.getInspKm());
			}
			if (i.getVeikategori().equals("R")){
				kontrakt.setVeinettLengdeRv(i.getVeinettLengde());
				kontrakt.setInspKmRv(i.getInspKm());
			}
			if (i.getVeikategori().equals("F")){
				kontrakt.setVeinettLengdeFv(i.getVeinettLengde());
				kontrakt.setInspKmFv(i.getInspKm());
			}
			if (i.getVeikategori().equals("K")){
				kontrakt.setVeinettLengdeKv(i.getVeinettLengde());
				kontrakt.setInspKmKv(i.getInspKm());
			}
		}
	}
	
	private void oppdaterMedKjoretoy(NokkeltallKjoretoy nokkeltallKjoretoy, NokkeltallFelt kontrakt){
		Long antKjoretoy = nokkeltallKjoretoy.getAntKjoretoy();
		Long antKjoretoyUtenLivstegn = nokkeltallKjoretoy.getAntKjoretoyUtenLivstegn();
		kontrakt.setAntKjoretoy(antKjoretoy);
		kontrakt.setAntKjoretoyUtenLivstegn(antKjoretoyUtenLivstegn);
		
	}
	/**
	 * Slårn sammen nokkeltall for en kontrakt og oppretter eller legger til tallene i en region
	 * @param regionTall
	 * @param tempPrKontrakt
	 */
	private void joinNokkeltallMedRegion(List<RegionTall> regionTall, List<NokkeltallFelt> tempPrKontrakt) {
		NokkeltallFelt kontrakt = joinNokkeltall(tempPrKontrakt);
		RegionTall region = getRegiontallMedId(kontrakt.getRegionId(), regionTall);
		if (region==null){
			region = new RegionTall();
			region.setRegionId(kontrakt.getRegionId());
			region.setRegionnavn(kontrakt.getRegionnavn());
			regionTall.add(region);
		}
		region.addNokkeltallFelt(kontrakt);
		tempPrKontrakt.clear();
	}
	
	/**
	 * Returnerer regionen med gitt id eller null hvis den ikke er opprettet  (enda)
	 * @param regionId
	 * @param alle
	 * @return
	 */
	private RegionTall getRegiontallMedId(Long regionId, List<RegionTall> alle){
		for (RegionTall rt:alle){
			if (rt.getRegionId().equals(regionId)){
				return rt;
			}
		}
		return null;
	}
	
	/**
	 * Slår sammen alle tallene for en liste med nøkkeltall
	 * @param inList
	 * @return
	 */
	private NokkeltallFelt joinNokkeltall(List<NokkeltallFelt> inList){
		NokkeltallFelt nf = new NokkeltallFelt();
		for (NokkeltallFelt n:inList){
			nf.add(n);
		}
		return nf;
	}

	private Map<String, Vector<?>> getTableDataFromResultSet(ResultSet rs, ResultSetMetaData metaData) throws SQLException{
		Map<String, Vector<?>> tableData = new HashMap<String, Vector<?>>();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}
		tableData.put("columns", columnNames);
		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				Object object = rs.getObject(columnIndex);
				
				//DIGI
				if (object instanceof byte[]) {
					String s = "";
					if (object != null) {
						byte [] digiArray = (byte [])object;
						
				    	for (Byte b:digiArray){
				    		s+=pad(Integer.toHexString(0xFF&b));
				    	}
					}
					vector.add(s);
				}else{
					vector.add(object);
				}						
			}
			data.add(vector);
		}
		tableData.put("data", data);
		return tableData;
	}
	
	private String pad(String hx){
		int pads = 2-hx.length();
		String padded = "";
		for (int i=0;i<pads;i++){
			padded+="0";
		}
		padded+=hx;
		return padded;	
	}
	
	private Connection getDatabaseConnection() throws Exception{
		InitialContext ctx = new InitialContext();
		DataSource ds = (DataSource) ctx.lookup("java:/mipssDS");
		Connection connection= ds.getConnection();
		return connection;
	}
	
	@Override
	public Map<String, Vector<?>> hentSprederrapport(String serienummer, Long kontraktId, Date fraDato, Date tilDato) throws Exception  {
		Dfuindivid dfu = (Dfuindivid)em.createQuery("SELECT o from Dfuindivid o where o.serienummer=:sn").setParameter("sn", serienummer).getSingleResult();
		StringBuilder q = new StringBuilder();
		q.append("select * from table(valider_pk.sjekk_spreder(?, ?, to_date(?, 'dd.mm.yyyy hh24:mi:ss'), to_date(?, 'dd.mm.yyyy hh24:mi:ss')))");

		try(Connection connection = getDatabaseConnection();
			PreparedStatement st = connection.prepareStatement(q.toString())) {

			st.setLong(1, dfu.getId());
			st.setLong(2, kontraktId);
			st.setString(3, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
			st.setString(4, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

			try(ResultSet rs = st.executeQuery()) {
				ResultSetMetaData metaData = rs.getMetaData();
				return getTableDataFromResultSet(rs, metaData);
			}
		}
	}

	@Override
	public Map<String, Vector<?>> hentSprederdetalj(String serienummer, Date fraDato, Date tilDato) throws Exception {
		Dfuindivid dfu = (Dfuindivid)em.createQuery("SELECT o from Dfuindivid o where o.serienummer=:sn").setParameter("sn", serienummer).getSingleResult();
		StringBuilder q = new StringBuilder();
		q.append("select * from table(valider_pk.sprederdetaljer(?, to_date(?, 'dd.mm.yyyy hh24:mi:ss'), to_date(?, 'dd.mm.yyyy hh24:mi:ss'))) ");

		try(Connection connection  = getDatabaseConnection();
			PreparedStatement st = connection.prepareStatement(q.toString())) {
			st.setInt(1, dfu.getId().intValue());
			st.setString(2, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
			st.setString(3, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
			try(ResultSet rs = st.executeQuery()) {
				ResultSetMetaData metaData = rs.getMetaData();
				Map<String, Vector<?>> tableDataFromResultSet = getTableDataFromResultSet(rs, metaData);
				return tableDataFromResultSet;
			}
		}
	}

	@Override
	public Map<String, Vector<?>> hentOversiktSynkprodpunkt() {
		try (Connection connection  = getDatabaseConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("select * from table(Valider_pk.oversikt_synkprodpunkt_f) order by 1")) {
			
			ResultSetMetaData metaData = rs.getMetaData();
			return getTableDataFromResultSet(rs, metaData);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return new HashMap<String, Vector<?>>();
	}
	
	public void persistSlettFeltstatistikk(SlettFeltstatistikk stat){
	//sjekk først at statistikken ikke ligger der..
		List<SlettFeltstatistikk> alleAktiveSlettingerStatistikk = getAlleAktiveSlettingerStatistikk();
		for (SlettFeltstatistikk sf:alleAktiveSlettingerStatistikk){
			if (sf.getKontraktId().equals(stat.getKontraktId()))//Finnes fra før, ikke legg inn
					return;
		}
		em.persist(stat);
	}
	@SuppressWarnings("unchecked")
	public List<SlettFeltstatistikk> getAlleAktiveSlettingerStatistikk(){
		List<SlettFeltstatistikk> resultList = em.createNamedQuery(SlettFeltstatistikk.QUERY_FIND_ALL_ACTIVE).getResultList();
		return resultList;
	}

	@Override
	public List<KapasitetRapportObject> getKapasitetsRapportData(Date fraDato, Date tilDato, boolean detaljertRapport) {
		
		List<KapasitetRapportObject> completeList = new ArrayList<KapasitetRapportObject>();
		
		List<Long> kontrakter = getAlleKontraktId();
		
		int hentDetaljertRapport = detaljertRapport ? 1 : 0;
		
		for (Long kontraktId : kontrakter) {
			String query = "select * from table(kapasitet_pk.kapasitet_f(?, to_date(?, 'dd.mm.yyyy hh24:mi:ss'), to_date(?, 'dd.mm.yyyy hh24:mi:ss'), " + hentDetaljertRapport +"))";
			Query qk = em.createNativeQuery(query)
			.setParameter(1, kontraktId)
			.setParameter(2, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT))
			.setParameter(3, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

			NativeQueryWrapper<KapasitetRapportObject> kapasiteter = new NativeQueryWrapper<KapasitetRapportObject>(qk, KapasitetRapportObject.class, KapasitetRapportObject.getPropertiesArray());
 			List<KapasitetRapportObject> kapasitetListeForKontrakt = kapasiteter.getWrappedList();
			
			for (KapasitetRapportObject kapasitetRapportObject : kapasitetListeForKontrakt) {
				completeList.add(kapasitetRapportObject);
			}			
		}
		
		return completeList;
	}

	@Override
	public Map<String, Vector<?>> hentVeinettManglerRapport() {
		try(Connection connection  = getDatabaseConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("select * from table(Veinett_pk.diff_veinett1)")) {

			ResultSetMetaData metaData = rs.getMetaData();
			return getTableDataFromResultSet(rs, metaData);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return new HashMap<String, Vector<?>>();
	}

	@Override
	public Map<String, Vector<?>> hentVeinettTilleggRapport() {
		try (Connection connection  = getDatabaseConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("select * from table(Veinett_pk.diff_veinett2)")) {
			
			ResultSetMetaData metaData = rs.getMetaData();
			return getTableDataFromResultSet(rs, metaData);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return new HashMap<String, Vector<?>>();
	}

	@Override
	public Map<String, Vector<?>> hentUkjenteStroprodukterAdmin(Date fraDato, Date tilDato) {
		String query = "select * from table(adminrapp_pk.ukjent_stroprodukt_f(to_date(?, 'dd.mm.yyyy hh24:mi:ss'), to_date(?, 'dd.mm.yyyy hh24:mi:ss')))";
		try (Connection connection  = getDatabaseConnection();
			PreparedStatement st = connection.prepareStatement(query)) {
			st.setString(1, MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
			st.setString(2, MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
			try(ResultSet rs = st.executeQuery()) {
				ResultSetMetaData metaData = rs.getMetaData();
				return getTableDataFromResultSet(rs, metaData);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return new HashMap<String, Vector<?>>();
	}

	public KonfigParamLocal getKonfig() {
		return konfig;
	}

	public void setKonfig(KonfigParamLocal konfig) {
		this.konfig = konfig;
	}
}
