package no.mesta.mipss.produksjonsrapport.excel;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.Leverandor;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class RodeRapport extends Rapp1Rapport{
	private final List<Rapp1> data;
	private Map<String, Object> map;
	private String templateFile = "rode_forside";
	private List<Rapp1Object> rodeList;
	
	public static void main(String[] args) throws IOException {
		ProdrapportParams params = new ProdrapportParams();
		params.setFraDato(Clock.now());
		params.setTilDato(Clock.now());
		params.setGjennomsnitt(true);
		params.setProsent(true);
		params.setFrekvens(true);
		params.setRodeNavn("Testrode");
		params.setKontraktNavn("0123-Bobs lucky contract");
		new RodeRapport(Leverandor.getTestData(), params, true).finalizeAndOpen();
	}
	
	public RodeRapport(List<Rapp1> data, ProdrapportParams params, boolean dau){
		super(params);
		this.data = data;
		this.params = params;
		initData();
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 7;
		startRow = 9;
		int endDataRow = writeReport(rodeList);
		int startDataRow = startRow +5;
		writeRodeLengdeHeader(startDataRow, endDataRow);
		if (dau){
			workbook.getSheetAt(0).getRow(0).getCell(0).setCellValue(new XSSFRichTextString("Kontrakt - DAU-rapport"));
		}
		createSubsheets();
		
//		for (Rapp1Object rode:rodeList){
//			XSSFSheet sheet = new RodeRapportSheet(rode, params).getSheet();
//			XSSFSheet newSheet = excel.createSheet(RapportHelper.parseString(rode.getRodenavn()), workbook);
//			Util.copySheets(newSheet, sheet);
//			addAutoFilter(newSheet, 13);
//			newSheet.createFreezePane(0, 14);
//		}
	}
	
	private void createSubsheets(){
		int sheetIndex = 1;
		for (Rapp1Object rode:rodeList){
			new RodeRapportSheet(rode, params, excel, workbook, sheetIndex);
			sheetIndex++;
		}
	}
	
	private void initData() {
		Map<Long, List<Rapp1>> listePrRode = RapportHelper.createListePrRode(data, params);
		Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
		Map<Long, R12Stroprodukt> stroproduktMap = new HashMap<Long, R12Stroprodukt>();
		rodeList = RapportHelper.createForsideData(prodtypeMap, stroproduktMap, listePrRode, Rapp1Object.class);
		alleProdtyper = RapportHelper.extractAndSortProdtyper(prodtypeMap);
		alleStroprodukter = RapportHelper.extractAndSortStroprodukter(stroproduktMap);
		
		//summer og organiser prodtypene og stroproduktene slik at de ligger i samme rekkefølge for samtlige leverandører
		for (Rapp1Object l:rodeList){
			l.organizeProdtyper(alleProdtyper);
			l.organizeStroprodukt(alleStroprodukter);
		}
		
		map = new HashMap<String, Object>();
		map.put("rode", rodeList);
		map.put("prodtyper", alleProdtyper);
		map.put("params", params);
		metadata = new ReportMetadata(rodeList, params);
	}
	
	private void writeRodeLengdeHeader(int startDataRow, int endDataRow){
		XSSFRow row = workbook.getSheetAt(0).getRow(7);
		addSumCell(1, startDataRow, endDataRow, row, null);
	}
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {	
		Rapp1Object o = (Rapp1Object)data;
		excel.writeData(dataRow, 0, o.getRodenavn());
		excel.writeData(dataRow, 1, o.getRodelengde());
		excel.writeData(dataRow, 2, o.getFraDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 2);
		excel.writeData(dataRow, 3, o.getTilDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 3);
		excel.writeData(dataRow, 4, o.getSamprodFlagg());
		excel.writeData(dataRow, 5, o.getKm());
		excel.writeData(dataRow, 6, o.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 6);
	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		for (int i=0;i<6;i++){
			excel.setCellStyle(excel.getTextStyleSum(), dataRow, i);
		}
		addSumCell(5, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(6, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}
}
