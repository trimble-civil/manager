package no.mesta.mipss.produksjonsrapport.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.TiltakComparator;
import no.mesta.mipss.produksjonsrapport.values.TiltakMengde;
import no.mesta.mipss.produksjonsrapport.values.TiltakObject;
import no.mesta.mipss.produksjonsrapport.values.TiltakProduksjon;
import no.mesta.mipss.produksjonsrapport.values.VeiTiltakObject;
import no.mesta.mipss.produksjonsrapport.values.VeirapportForsideObject;
import no.mesta.mipss.produksjonsrapport.values.VeirapportObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.Region;

public class VeiRapport extends KontraktRapport {

	private final ProdrapportParams params;

	public VeiRapport(HSSFWorkbook workbook, ProdrapportParams params) {
		super(workbook, params);
		this.params = params;
	}

	@Override
	protected int createEnhetColumns(HSSFRow dataRow,
			KontraktRapportObject rapportObject, int columnNR) {
		return 0;
	}

	@Override
	protected int createEnhetSumColumns(HSSFRow sumRow,
			KontraktRapportObject obj, int columnNR, boolean globalSum,
			boolean forside) {
		return 0;
	}

	public int createReportHeader(HSSFSheet sheet, String rapportNavn,
			String kontrakt, String rodeTypeNavn, String rodeNavn,
			String rodeLengde, String rapportDato, String periode, int rowCount) {
		// lag rapport tittel rad
		sheet.addMergedRegion(new Region(rowCount, (short) 0, rowCount,
				(short) 3));

		HSSFRow row1 = sheet.createRow(rowCount++);
		row1.createCell((short) 0).setCellValue(new HSSFRichTextString(rapportNavn));
		row1.getCell((short) 0).setCellStyle(hReportcellStyle);

		rowCount++;

		sheet.addMergedRegion(new Region(rowCount, (short) 1, rowCount, (short) 3));

		HSSFRow row2 = sheet.createRow(rowCount++);
		row2.createCell((short) 0).setCellValue(new HSSFRichTextString("Kontrakt: "));
		row2.getCell((short) 0).setCellStyle(hReportTextStyle);
		row2.createCell((short) 1).setCellValue(new HSSFRichTextString(kontrakt));
		row2.getCell((short) 1).setCellStyle(hReportTextStyle);

		// Rodetype
		sheet.addMergedRegion(new Region(rowCount, (short) 1, rowCount,(short) 3));

		HSSFRow row3 = sheet.createRow(rowCount++);
		row3.createCell((short) 0).setCellValue(new HSSFRichTextString("Rodetype: "));
		row3.getCell((short) 0).setCellStyle(hReportTextStyle);
		row3.createCell((short) 1).setCellValue(new HSSFRichTextString(rodeTypeNavn));
		row3.getCell((short) 1).setCellStyle(hReportTextStyle);

		// Rode
		sheet.addMergedRegion(new Region(rowCount, (short) 1, rowCount, (short) 3));

		HSSFRow row4 = sheet.createRow(rowCount++);
		row4.createCell((short) 0).setCellValue(new HSSFRichTextString("Rode: "));
		row4.getCell((short) 0).setCellStyle(hReportTextStyle);
		row4.createCell((short) 1).setCellValue(new HSSFRichTextString(rodeNavn));
		row4.getCell((short) 1).setCellStyle(hReportTextStyle);

		// Periode
		sheet.addMergedRegion(new Region(rowCount, (short) 1, rowCount,(short) 3));

		HSSFRow row6 = sheet.createRow(rowCount++);
		row6.createCell((short) 0).setCellValue(new HSSFRichTextString("Periode: "));
		row6.getCell((short) 0).setCellStyle(hReportTextStyleUnderline);
		row6.createCell((short) 1).setCellValue(new HSSFRichTextString(periode));
		row6.getCell((short) 1).setCellStyle(hReportTextStyleUnderline);

		// Rapportdato
		sheet.addMergedRegion(new Region(rowCount, (short) 1, rowCount, (short) 3));

		HSSFRow row5 = sheet.createRow(rowCount++);
		row5.createCell((short) 0).setCellValue(new HSSFRichTextString("Rapportdato: "));
		row5.getCell((short) 0).setCellStyle(hReportTextStyle);
		row5.createCell((short) 1).setCellValue(new HSSFRichTextString(rapportDato));
		row5.getCell((short) 1).setCellStyle(hReportdatoStyle);

		rowCount++;

		return rowCount;
	}
	
	protected int createTotalHeaders(HSSFSheet sheet, HSSFRow headerRow, HSSFRow headerRowMain, short fixedHeaderList){
        int row = headerRow.getRowNum();
        short colFrom = fixedHeaderList;
        short colTo = (short)(fixedHeaderList+1);
        
        //slå sammen to celler
        sheet.addMergedRegion(new Region(row, colFrom, row, colTo));
        
        createCell(headerRow, fixedHeaderList, "Total", hTablecellStyle);
        createCell(headerRowMain, fixedHeaderList, "    Km    ", hTablecellStyle);
        createCell(headerRowMain, (short) (fixedHeaderList+1), "  Timer  ", hTablecellStyle);
        
        return 2;
    }
	
	public void createSheet(HSSFSheet sheet, List<VeirapportObject> list, int row){
		//TODO rename disse listene til alleTyperAv<type>List
		List<TiltakProduksjon> tiltakList = new ArrayList<TiltakProduksjon>();
		List<TiltakMengde> mengdeList = new ArrayList<TiltakMengde>();
		
		for (VeirapportObject o:list){
			for (TiltakMengde m:o.getMengdeList()){
				if (!mengdeList.contains(m)){
					mengdeList.add(m);
				}
			}
			
			for (TiltakProduksjon p:o.getProdList()){
				if (!tiltakList.contains(p)){
					tiltakList.add(p);
				}
			}
		}
		
		for (VeirapportObject o:list){
			Collections.sort(o.getProdList(), new TiltakComparator<TiltakProduksjon>());
			Collections.sort(o.getMengdeList(), new TiltakComparator<TiltakMengde>());
		}
		Collections.sort(tiltakList, new TiltakComparator<TiltakProduksjon>());
		Collections.sort(mengdeList, new TiltakComparator<TiltakMengde>());
		
		Collections.sort(list, new SheetComparator<VeirapportObject>());
		int headerRow = row+1;
		int columns = 0;
		int datarow = headerRow+5;
		int startRow = datarow;
		int sumRow = headerRow+3;
		
		for (VeirapportObject o:list){
			columns=0;
			HSSFRow data = sheet.createRow(datarow++);
			HSSFCell regnr = data.createCell((short)columns++);
			regnr.setCellStyle(textStyle);
			regnr.setCellValue(new HSSFRichTextString(o.getRegnr()));
			
			HSSFCell dogn = data.createCell((short)columns++);
			dogn.setCellStyle(textStyle);
			dogn.setCellValue(new HSSFRichTextString(MipssDateFormatter.formatDate(o.getDato(), MipssDateFormatter.DATE_FORMAT)));
			
			HSSFCell veinr = data.createCell((short)columns++);
			veinr.setCellStyle(textStyle);
			veinr.setCellValue(new HSSFRichTextString(o.getVeistatus()+""+o.getVeinummer()));
			
			for (TiltakProduksjon prod:tiltakList){
				boolean added=false;
				for (TiltakProduksjon p: o.getProdList()){
					if (prod.getNavn().equals(p.getNavn())){
						HSSFCell km = data.createCell((short)columns++);
						HSSFCell tid = data.createCell((short)columns++);
						km.setCellStyle(numberStyle);
						tid.setCellStyle(tidStyle);
						km.setCellValue(p.getKm());
//						tid.setCellFormula("TIME("+sec2Hms(p.getSekunder())+")");
						tid.setCellFormula(p.getSekunder()+"/86400");
						added=true;
					}						
				}
				if (!added){
					data.createCell((short)columns++).setCellStyle(textStyle);
					data.createCell((short)columns++).setCellStyle(textStyle);
				}
			}
			
			for (TiltakMengde mm:mengdeList){
				boolean added=false;
				for (TiltakMengde m:o.getMengdeList()){
					if (m.getNavn().equals(mm.getNavn())){
						HSSFCell mengde = data.createCell((short)columns++);
						mengde.setCellStyle(numberStyle);
						mengde.setCellValue(m.getMengde());
						added = true;
					}
				}
				if (!added){
					data.createCell((short)columns++).setCellStyle(textStyle);
				}
			}
		}
		HSSFRow header1Row = sheet.createRow(headerRow);
		HSSFRow header2Row = sheet.createRow(headerRow+1);
		HSSFRow header3Row = sheet.createRow(headerRow+2);
		
		short column =0;
		HSSFCell regnr = header1Row.createCell(column++);
		HSSFCell dogn = header1Row.createCell(column++);
		HSSFCell veinr = header1Row.createCell(column++);

		regnr.setCellStyle(hTablecellStyle);
		dogn.setCellStyle(hTablecellStyle);
		veinr.setCellStyle(hTablecellStyle);
		
		regnr.setCellValue(new HSSFRichTextString(resources.getGuiString("reportHeader.kjoretoy")));
		dogn.setCellValue(new HSSFRichTextString(resources.getGuiString("reportHeader.dogn")));
		veinr.setCellValue(new HSSFRichTextString("Veinr"));
		column=0;
		
		header2Row.createCell(column++).setCellStyle(hTablecellStyle);
		header2Row.createCell(column++).setCellStyle(hTablecellStyle);
		header2Row.createCell(column++).setCellStyle(hTablecellStyle);
		
		column=0;
		header3Row.createCell(column++).setCellStyle(hTablecellStyle);
		header3Row.createCell(column++).setCellStyle(hTablecellStyle);
		header3Row.createCell(column++).setCellStyle(hTablecellStyle);
		
		
		for (TiltakProduksjon p:tiltakList){
			short col = column;
			HSSFCell tiltak = header1Row.createCell(column++);
			sheet.addMergedRegion(new Region(header1Row.getRowNum(), (short)(column-1), header1Row.getRowNum(), column));
			tiltak.setCellStyle(hTablecellStyle);
			tiltak.setCellValue(new HSSFRichTextString(p.getNavn()));
			
			column=col;
			HSSFCell km = header3Row.createCell(column++);
			HSSFCell tid = header3Row.createCell(column++);
			km.setCellStyle(hTablecellStyle);
			tid.setCellStyle(hTablecellStyle);
			km.setCellValue(new HSSFRichTextString("Km"));
			tid.setCellValue(new HSSFRichTextString(" Timer "));
			
			column=col;
			header2Row.createCell(column++).setCellStyle(hTablecellStyle);
			header2Row.createCell(column++).setCellStyle(hTablecellStyle);
		}
		if (!mengdeList.isEmpty()){
			HSSFCell mengder = header1Row.createCell(column);
			mengder.setCellStyle(hTablecellStyle);
			mengder.setCellValue(new HSSFRichTextString("Utlagte mengder"));
			sheet.addMergedRegion(new Region(header1Row.getRowNum(), column, header1Row.getRowNum(), (short) (column+mengdeList.size()-1)));
			
			for (TiltakMengde m:mengdeList){
				HSSFCell type = header2Row.createCell(column);
				HSSFCell enhet = header3Row.createCell(column);
				type.setCellStyle(hTablecellStyle);
				enhet.setCellStyle(hTablecellStyle);
				type.setCellValue(new HSSFRichTextString(m.getNavn()));
				enhet.setCellValue(new HSSFRichTextString(m.getEnhet()));
				column++;
			}
		}
		//TODO sumrows..
		createSumRow(sheet, tiltakList, mengdeList, datarow, startRow, datarow+1);
		createSumRow(sheet, tiltakList, mengdeList, datarow, startRow, sumRow);
		
	}

	private void createSumRow(HSSFSheet sheet,
			List<TiltakProduksjon> tiltakList, List<TiltakMengde> mengdeList,
			int datarow, int startRow, int sumRow) {
		
		short sumCell = 0;
		HSSFRow sumrow = sheet.createRow(sumRow);
		
		
		HSSFCell sum = sumrow.createCell(sumCell++);
		sum.setCellStyle(textStyleSum);
		sum.setCellValue(new HSSFRichTextString("Sum:"));
		
		sumrow.createCell(sumCell++).setCellStyle(textStyleSum);
		sumrow.createCell(sumCell++).setCellStyle(textStyleSum);
		
		for (TiltakProduksjon p:tiltakList){
			HSSFCell sumKm = sumrow.createCell(sumCell++);
			HSSFCell sumTid = sumrow.createCell(sumCell++);
			sumKm.setCellStyle(numberStyleSum);
			sumTid.setCellStyle(tidStyleSum);
			CellReference refKmStart = new CellReference(startRow, sumKm.getCellNum());
			CellReference refTidStart = new CellReference(startRow, sumTid.getCellNum());
			CellReference refKmEnd = new CellReference(datarow-1, sumKm.getCellNum());
			CellReference refTidEnd = new CellReference(datarow-1, sumTid.getCellNum());
			sumKm.setCellFormula("SUM("+refKmStart+":"+refKmEnd+")");
			sumTid.setCellFormula("SUM("+refTidStart+":"+refTidEnd+")");
		}
		for (TiltakMengde m:mengdeList){
			HSSFCell sumMengde = sumrow.createCell(sumCell++);
			sumMengde.setCellStyle(numberStyleSum);
			CellReference refMengdeStart = new CellReference(startRow, sumMengde.getCellNum());
			CellReference refMengdeEnd = new CellReference(datarow-1, sumMengde.getCellNum());
			
			sumMengde.setCellFormula("SUM("+refMengdeStart+":"+refMengdeEnd+")");
		}
	}
	
	public int createForsideType(HSSFSheet sheet, String type, int row, List<VeirapportForsideObject> list, List<TiltakProduksjon> tiltakProdList, List<TiltakMengde> tiltakMengdeList){
		int headerRow = row+1;
		int columns = 0;
		int datarow = headerRow+6;
		int startRow = datarow;
		int sumRow = headerRow+4;
		
		//lager datarader
		Collections.sort(list, new ForsideComparator<VeirapportForsideObject>());
		for (VeirapportForsideObject o:list){
			columns=0;
			HSSFRow data = sheet.createRow(datarow++);
			HSSFCell veinr = data.createCell((short)columns);
			veinr.setCellStyle(kmtStyle);
			veinr.setCellValue(o.getVeinummer());
			
			HSSFCell periode = data.createCell((short)++columns);
			periode.setCellStyle(textStyle);
			periode.setCellValue(new HSSFRichTextString(o.getTidsrom()));
			
			HSSFCell totalKm = data.createCell((short)++columns);
			totalKm.setCellStyle(numberStyle);
			
			HSSFCell totalTid = data.createCell((short)++columns);
			totalTid.setCellStyle(tidStyle);
			
			columns = createTiltakData(tiltakProdList, columns, datarow, o, data, totalKm, totalTid);
			columns = createMengdeData(tiltakMengdeList, columns, o, data);
		}
		
		HSSFRow titleRow = sheet.createRow(headerRow);
		HSSFRow header1Row = sheet.createRow(headerRow+1);
		HSSFRow header2Row = sheet.createRow(headerRow+2);
		HSSFRow header3Row = sheet.createRow(headerRow+3);
		
		sheet.addMergedRegion(new Region(headerRow, (short)0, headerRow, (short)columns));
		
		short col = 0;
		//første rad i header
		HSSFCell subtitle = titleRow.createCell(col);
		subtitle.setCellValue(new HSSFRichTextString(type));
		subtitle.setCellStyle(hTablecellStyleBorder);
		
		//andre rad i header
		col =0;
		HSSFCell veinrHead = header1Row.createCell(col);
		HSSFCell periodeHead = header1Row.createCell(++col);
		HSSFCell totalHead = header1Row.createCell(++col);
		sheet.addMergedRegion(new Region(headerRow+1, col, headerRow+1, (short)(col+1)));
		
		veinrHead.setCellValue(new HSSFRichTextString("Veinummer"));
		periodeHead.setCellValue(new HSSFRichTextString("Periode"));
		totalHead.setCellValue(new HSSFRichTextString("Total"));
		veinrHead.setCellStyle(hTablecellStyle);
		periodeHead.setCellStyle(hTablecellStyle);
		totalHead.setCellStyle(hTablecellStyle);
		
		//tredje rad i header
		col = 2;
		HSSFCell totalKmHead = header3Row.createCell(col);
		HSSFCell totalTidHead = header3Row.createCell(++col);
		
		totalKmHead.setCellValue(new HSSFRichTextString("Km"));
		totalTidHead.setCellValue(new HSSFRichTextString(" Timer "));
		totalKmHead.setCellStyle(hTablecellStyle);
		totalTidHead.setCellStyle(hTablecellStyle);
		
		for (int i=0;i<col+1;i++){
			header2Row.createCell((short)i).setCellStyle(hTablecellStyle);
		}
		header3Row.createCell((short)0).setCellStyle(hTablecellStyle);
		header3Row.createCell((short)1).setCellStyle(hTablecellStyle);
		
		
		col = 3;
		col = createTiltakHeaders(sheet, tiltakProdList, headerRow, header1Row, header2Row, header3Row, col);
		createMengdeHeaders(sheet, tiltakMengdeList, headerRow, header1Row, header2Row, header3Row, col);
		
		
		datarow++;
		createForsideSumRow(sheet, tiltakProdList, tiltakMengdeList, datarow, startRow, datarow);
		datarow = createForsideSumRow(sheet, tiltakProdList, tiltakMengdeList, datarow, startRow, sumRow);
		
		return datarow;
	}

	private int createForsideSumRow(HSSFSheet sheet,
			List<TiltakProduksjon> tiltakProdList,
			List<TiltakMengde> tiltakMengdeList, int datarow, int startRow, int sumRow) {
		datarow++;
		HSSFRow sumrow = sheet.createRow(sumRow);
		short sumCell = 0;
		HSSFCell sumText = sumrow.createCell(sumCell++);
		sumText.setCellStyle(textStyleSum);
		sumText.setCellValue(new HSSFRichTextString("Sum:"));
		sumrow.createCell(sumCell++).setCellStyle(textStyleSum);
		
		HSSFCell sumTotalKm = sumrow.createCell(sumCell++);
		HSSFCell sumTotalTid = sumrow.createCell(sumCell++);
		
		sumTotalKm.setCellStyle(numberStyleSum);
		sumTotalTid.setCellStyle(tidStyleSum);
		
		CellReference refKmTotalStart = new CellReference(startRow, sumTotalKm.getCellNum());
		CellReference refTidTotalStart = new CellReference(startRow, sumTotalTid.getCellNum());
		CellReference refKmTotalEnd = new CellReference(datarow-2, sumTotalKm.getCellNum());
		CellReference refTidTotalEnd = new CellReference(datarow-2, sumTotalTid.getCellNum());
		
		sumTotalKm.setCellFormula("SUM("+refKmTotalStart+":"+refKmTotalEnd+")");
		sumTotalTid.setCellFormula("SUM("+refTidTotalStart+":"+refTidTotalEnd+")");
		
		sumCell = createTiltakSum(tiltakProdList, datarow, startRow, sumrow, sumCell, sumTotalKm);
		createMengdeSum(tiltakMengdeList, datarow, startRow, sumrow, sumCell);
		return datarow;
	}
	
	/**
	 * Lager tiltak datarader
	 * @param tiltakProdList
	 * @param columns
	 * @param datarow
	 * @param o
	 * @param data
	 * @param totalKm
	 * @param totalTid
	 * @return
	 */
	private int createTiltakData(List<TiltakProduksjon> tiltakProdList, int columns, int datarow, VeirapportForsideObject o, HSSFRow data, HSSFCell totalKm, HSSFCell totalTid) {
		for (int i=0;i<tiltakProdList.size();i++){
			String navn = tiltakProdList.get(i).getNavn();
			TiltakObject t = o.getTiltak(navn, TiltakProduksjon.class);
			if (t==null){
				data.createCell((short)++columns).setCellStyle(textStyle);
				data.createCell((short)++columns).setCellStyle(textStyle);
				if (params.isProsent()){
					data.createCell((short)++columns).setCellStyle(textStyle);	
				}
					
				if (params.isGjennomsnitt()){
					data.createCell((short)++columns).setCellStyle(textStyle);
				}
			}else{
				if (t instanceof TiltakProduksjon){
					TiltakProduksjon p = (TiltakProduksjon) t;
					HSSFCell km = data.createCell((short)++columns);
					km.setCellStyle(numberStyle);
					km.setCellValue(p.getKm());
				
					HSSFCell tid = data.createCell((short)++columns);
					tid.setCellStyle(tidStyle);
					tid.setCellFormula(p.getSekunder()+"/86400");
					//tid.setCellFormula("TIME("+p.getTid().replaceAll(":", ";")+")");
					
					CellReference refKmTotal = new CellReference(datarow-1, totalKm.getCellNum());
					CellReference refKM = new CellReference(datarow-1, km.getCellNum());
					CellReference refTID = new CellReference(datarow-1, tid.getCellNum());
					if (params.isProsent()){
						HSSFCell prosent = data.createCell((short)++columns);
						prosent.setCellStyle(kmtStyle);
						String prosentFormel = refKM.toString()+"/"+refKmTotal.toString()+" * 100";
						prosent.setCellFormula(prosentFormel);
					}
					if (params.isGjennomsnitt()){
						HSSFCell gjennomsnitt = data.createCell((short)++columns);
						gjennomsnitt.setCellStyle(kmtStyle);
						String formel = "IF(OR(" + refKM.toString() + "=0;" + refTID.toString() + "=0);0;" + refKM.toString() + "/(" + refTID.toString() + "*24))";
						gjennomsnitt.setCellFormula(formel);
						//sett rød farge på cellen hvis farten overskrider 100 km/t
			            if (calcKmt(p.getSekunder(),p.getKm())>100){
			            	gjennomsnitt.setCellStyle(redStyle);
			            }
					}
				}
			}
		}
//		totalKm.setCellValue(totalKmKjort);
//		totalTid.setCellFormula("TIME("+sec2Hms(totalTidSekunder)+")");
//		totalTid.setCellFormula(totalTidSekunder+"/86400");
		totalKm.setCellValue(o.getVeirappObject().getTotalKjort());
		totalTid.setCellFormula(o.getVeirappObject().getSekunderKjort()+"/86400");
		return columns;
	}

	/**
	 * Lager mengde datarader
	 * @param tiltakMengdeList
	 * @param columns
	 * @param o
	 * @param data
	 * @return
	 */
	private int createMengdeData(List<TiltakMengde> tiltakMengdeList, int columns, VeirapportForsideObject o, HSSFRow data) {
		for (int i=0;i<tiltakMengdeList.size();i++){
			String navn = tiltakMengdeList.get(i).getNavn();
			TiltakObject t = o.getTiltak(navn, TiltakMengde.class);
			if (t==null){
				data.createCell((short)++columns).setCellStyle(textStyle);
			}else{
				if (t instanceof TiltakMengde){
					TiltakMengde m=(TiltakMengde) t;
					HSSFCell mengde = data.createCell((short)++columns);
					mengde.setCellStyle(numberStyle);
					mengde.setCellValue(m.getMengde());
				}
			}
		}
		return columns;
	}
	
	/**
	 * Lager tiltak headere
	 * @param sheet
	 * @param tiltakProdList
	 * @param headerRow
	 * @param header1Row
	 * @param header2Row
	 * @param header3Row
	 * @param col
	 * @return
	 */
	private short createTiltakHeaders(HSSFSheet sheet, List<TiltakProduksjon> tiltakProdList, int headerRow, HSSFRow header1Row, HSSFRow header2Row, HSSFRow header3Row, short col) {
		short mergeColspan = 1;
		mergeColspan += params.isProsent()?1:0;
		mergeColspan += params.isGjennomsnitt()?1:0;
		for (TiltakProduksjon o:tiltakProdList){
			HSSFCell ttitle = header1Row.createCell(++col);
			ttitle.setCellStyle(hTablecellStyle);
			ttitle.setCellValue(new HSSFRichTextString(o.getNavn()));
			sheet.addMergedRegion(new Region(headerRow+1, (short)col, headerRow+1, (short)(col+mergeColspan)));
			
			header2Row.createCell(col).setCellStyle(hTablecellStyle);
			header2Row.createCell((short)(col+1)).setCellStyle(hTablecellStyle);
			short c = 1;
			if (params.isProsent()){
				c++;
				header2Row.createCell((short)(col+c)).setCellStyle(hTablecellStyle);
			}
			if (params.isGjennomsnitt()){
				c++;
				header2Row.createCell((short)(col+c)).setCellStyle(hTablecellStyle);
			}
			
			HSSFCell km = header3Row.createCell(col);
			HSSFCell tid = header3Row.createCell(++col);
			km.setCellStyle(hTablecellStyle);
			tid.setCellStyle(hTablecellStyle);
			km.setCellValue(new HSSFRichTextString("Km"));
			tid.setCellValue(new HSSFRichTextString(" Timer "));
			
			if (params.isProsent()){
				HSSFCell prosent = header3Row.createCell(++col);
				prosent.setCellStyle(hTablecellStyle);
				prosent.setCellValue(new HSSFRichTextString(" % "));
			}
			if (params.isGjennomsnitt()){
				HSSFCell gjennomsnitt = header3Row.createCell(++col);
				gjennomsnitt.setCellStyle(hTablecellStyle);
				gjennomsnitt.setCellValue(new HSSFRichTextString("km/t"));
			}
		}
		return col;
	}

	/**
	 * Lager mengde headere
	 * @param sheet
	 * @param tiltakMengdeList
	 * @param headerRow
	 * @param header1Row
	 * @param header2Row
	 * @param header3Row
	 * @param col
	 */
	private void createMengdeHeaders(HSSFSheet sheet, List<TiltakMengde> tiltakMengdeList, int headerRow, HSSFRow header1Row, HSSFRow header2Row, HSSFRow header3Row, short col) {
		boolean titleCreated = false;
		for (TiltakMengde m:tiltakMengdeList){
			col++;
			if (!titleCreated){
				sheet.addMergedRegion(new Region(headerRow+1, (short)col, headerRow+1, (short)(col+tiltakMengdeList.size()-1)));
				HSSFCell ttitle = header1Row.createCell(col);
				ttitle.setCellStyle(hTablecellStyle);
				ttitle.setCellValue(new HSSFRichTextString("Utlagte mengder"));
				titleCreated = true;
			}
			HSSFCell unit = header2Row.createCell(col);
			unit.setCellStyle(hTablecellStyle);
			unit.setCellValue(new HSSFRichTextString(m.getNavn()));
			
			HSSFCell value  = header3Row.createCell(col);
			value.setCellStyle(hTablecellStyle);
			value.setCellValue(new HSSFRichTextString(m.getEnhet()));
		}
	}

	/**
	 * Lager mengde sumrader
	 * @param tiltakMengdeList
	 * @param datarow
	 * @param startRow
	 * @param sumrow
	 * @param sumCell
	 */
	private void createMengdeSum(List<TiltakMengde> tiltakMengdeList, int datarow, int startRow, HSSFRow sumrow, short sumCell) {
		for (TiltakMengde m:tiltakMengdeList){
			HSSFCell sumMengde = sumrow.createCell(sumCell++);
			sumMengde.setCellStyle(numberStyleSum);
			CellReference refMengdeStart = new CellReference(startRow, sumMengde.getCellNum());
			CellReference refMengdeEnd = new CellReference(datarow-2, sumMengde.getCellNum());
			
			sumMengde.setCellFormula("SUM("+refMengdeStart+":"+refMengdeEnd+")");
		}
	}
	
	/**
	 * Lager tiltak sumrader
	 * @param tiltakProdList
	 * @param datarow
	 * @param startRow
	 * @param sumrow
	 * @param sumCell
	 * @param sumTotalKm
	 * @return
	 */
	private short createTiltakSum(List<TiltakProduksjon> tiltakProdList, int datarow, int startRow, HSSFRow sumrow, short sumCell, HSSFCell sumTotalKm) {
		for (TiltakProduksjon p:tiltakProdList){
			HSSFCell sumKm = sumrow.createCell(sumCell++);
			HSSFCell sumTid = sumrow.createCell(sumCell++);
			sumKm.setCellStyle(numberStyleSum);
			sumTid.setCellStyle(tidStyleSum);
			CellReference refKmStart = new CellReference(startRow, sumKm.getCellNum());
			CellReference refTidStart = new CellReference(startRow, sumTid.getCellNum());
			CellReference refKmEnd = new CellReference(datarow-2, sumKm.getCellNum());
			CellReference refTidEnd = new CellReference(datarow-2, sumTid.getCellNum());
			sumKm.setCellFormula("SUM("+refKmStart+":"+refKmEnd+")");
			sumTid.setCellFormula("SUM("+refTidStart+":"+refTidEnd+")");
			
			CellReference refTotalKM = new CellReference(datarow-1, sumTotalKm.getCellNum());
			CellReference refSumKm = new CellReference(datarow-1, sumKm.getCellNum());
			CellReference refSumTid = new CellReference(datarow-1, sumTid.getCellNum());
			
			if (params.isProsent()){
				HSSFCell sumProsent = sumrow.createCell(sumCell++);
				sumProsent.setCellStyle(kmtStyleSum);
				String formula  = refSumKm.toString()+"/"+refTotalKM.toString()+" * 100";
				sumProsent.setCellFormula(formula);				
			}
			
			if (params.isGjennomsnitt()){
				HSSFCell sumGjennomsnitt = sumrow.createCell(sumCell++);
				sumGjennomsnitt.setCellStyle(kmtStyleSum);
				String formel = "IF(OR(" + refSumKm.toString() + "=0;" + refSumTid.toString() + "=0);0;" + refSumKm.toString() + "/(" + refSumTid.toString() + "*24))";
				sumGjennomsnitt.setCellFormula(formel);
			}
		}
		return sumCell;
	}

	public String sec2Hms(int sekunder){
		int hours = sekunder / 3600,
		remainder = sekunder % 3600,
		minutes = remainder / 60,
		seconds = remainder % 60;

		return( (hours < 10 ? "0" : "") + hours
		+ ";" + (minutes < 10 ? "0" : "") + minutes
		+ ";" + (seconds< 10 ? "0" : "") + seconds );
		
	}
	private class SheetComparator <T extends VeirapportObject> implements Comparator<T>, Serializable{
		public int compare(T o1, T o2) {
	        String v1 = o1.getRegnr();
	        String v2 = o2.getRegnr();
	        return v1.compareTo(v2);
		}
	}
	
	private class ForsideComparator <T extends VeirapportForsideObject> implements Comparator<T>, Serializable{
		public int compare(T o1, T o2) {
	        int v1 = o1.getVeinummer();
	        int v2 = o2.getVeinummer();
	        if (v1 == v2) {
	            return 0;
	        } else if (v1 < v2) {
	            return -1;
	        } else {
	            return 1;
	        }
		}
	}
	private double calcKmt(int sekunder, double km){
    	double kmt = 0;
    	if (sekunder>0){
            kmt = (km/sekunder)*3600;
        }
    	return kmt;
    }
}
