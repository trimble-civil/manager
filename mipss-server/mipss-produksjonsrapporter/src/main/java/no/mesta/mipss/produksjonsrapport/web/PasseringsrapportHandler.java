package no.mesta.mipss.produksjonsrapport.web;

import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.persistence.rapportfunksjoner.Passering;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.Passeringsrapport;
import no.mesta.mipss.produksjonsrapport.query.AkkumulertQuery;
import no.mesta.mipss.produksjonsrapport.query.PasseringsQuery;

public class PasseringsrapportHandler extends ExcelReportHSSFHandler{

	private final EntityManager em;
	private ProdrapportParams params;

	public PasseringsrapportHandler(EntityManager em) {
		super(null);
		this.em = em;
	}

	@Override
	public byte[] createReport(ProdrapportParams params) throws Exception {
		this.params = params;
		
		PasseringsQuery q = new PasseringsQuery(params, em);	
		List<Passering> results = q.getResults();
		
		if (results==null||results.isEmpty()){
        	return null;
        }
		Passeringsrapport r = new Passeringsrapport(results, params);
		return r.toByteArray();
	}

	public List<Rapp1> getResults(ProdrapportParams params) {
		AkkumulertQuery q  = new AkkumulertQuery(params, em);
		List<Rapp1> results = q.getResults();
		return results;
	}
}
