package no.mesta.mipss.produksjonsrapport.web;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

//~--- non-JDK imports --------------------------------------------------------


//import org.apache.poi.hssf.usermodel.HSSFWorkbook;

//~--- JDK imports ------------------------------------------------------------


/**
 * @author Luc Quan Vu, TietoEnator
 * @since 20.01.2006
 */
public abstract class ExcelReportHSSFHandler {
    protected static final int BUFFER_SIZE = 4096;
    protected final byte[] buffer = new byte[BUFFER_SIZE];
    protected final File file;
    private int maxColumnsPrSheet = 18;
    
    public ExcelReportHSSFHandler(final File file) {
        this.file = file;

    }
    
    /**
     * Juster antall ark som skal brukes til utskift av rapporten. Det er 
     * max 18 kolonner pr ark i bredden, liggende utskrift og legg til topptekst
     * med sidenummer og bunntekst med filnavn
     */
    protected void adjustPrintSetup(HSSFSheet sheet, int headerRow, String fileName, boolean landscape){
        fileName = fileName.substring(0, fileName.indexOf(" "));//crop filename at first whitespace
        
        HSSFRow row = sheet.getRow(headerRow);
        if (row!=null){
            short columnCount = row.getLastCellNum();
            int printSheetCount = 1;
            for(int i=1;;i++){
                if (maxColumnsPrSheet * i>columnCount){
                    printSheetCount = i;
                    break;
                }
            }
            HSSFPrintSetup ps = sheet.getPrintSetup();
            ps.setFitWidth((short)printSheetCount);
            ps.setFitHeight(Short.MAX_VALUE);
            ps.setLandscape(landscape);
            sheet.setAutobreaks(true);
            ps.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
            
            HSSFFooter footer = sheet.getFooter();
            
            footer.setCenter(fileName);
            HSSFHeader header = sheet.getHeader();
            header.setCenter("Side: "+HSSFHeader.page()+" av "+HSSFHeader.numPages());
        }
    }
    
    /**
     * Overloaded for å kunne justere antallet kolonner som skal komprimeres på ett ark
     * @param sheet
     * @param headerRow
     * @param fileName
     * @param maxColumnsPrSheet
     */
    protected void adjustPrintSetup(HSSFSheet sheet, int headerRow, String fileName, int maxColumnsPrSheet, boolean landscape){
        this.maxColumnsPrSheet = maxColumnsPrSheet;
        this.adjustPrintSetup(sheet, headerRow, fileName, landscape);
    }
    
    public abstract byte[] createReport(ProdrapportParams params) throws Exception;

    /**
     *  
     *   @param request
     *   @param response
     *   @throws Exception
     */
    protected void execute() throws Exception {
        if (!isReadable()) {

            return;
        }

        // HSSFWorkbook workbook = null;
        OutputStream output = null;
        InputStream input = null;
        
        try {

            // lag en outputstream hvor data skal skrives til bulk for bulk à 4096 bytes
            output = createOutputStream();
            
            // lag en Inputstream av den temporær filen som inneholder data generert fra DAO
            input = createInputStream();

            // les data fra InputStream til en buffer på max 4096 bytes og returnerer antall bytes som er lest
            int bufferSize = input.read(buffer);
            
            
            
            if (bufferSize != -1) {
//                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
//                response.setHeader("Cache-Control", "post-check=0, pre-check=0");
//                response.setHeader("Pragma", "no-cache");
//                response.setHeader("Content-Disposition", "attachment; filename=report.xls");
//                response.setHeader("refresh", ""+1);
//                response.setContentType("application/vnd.ms-excel");
                
                while (bufferSize != -1) {

                    // skriver ut data fra buffer på max 4096 bytes til OutputStream
                    // System.out.println("bufferSize before output.write(): " + bufferSize);
                    output.write(buffer, 0, bufferSize);

                    // leser neste bulk av data fra inputstream til en ny buffer på max 4096 bytes
                    bufferSize = input.read(buffer);

                    // System.out.println("bufferSize after input.read(): " + bufferSize);
                }
            }
                     
            output.flush();
            
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (output != null) {

                // When gzip is used close must be called !!
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected final boolean isReadable() {
        if (!file.exists()) {
            return false;
        }

        if (file.isDirectory()) {
            return false;
        }

        return file.canRead();
    }

//    /**
//     *    Test to see if the browser will support gzip.
//     *    The browser settings can be suppressed by setting ?encoding=none
//     *    at the end of the URL
//     *   @param request
//     *   @return
//     */
//    private static boolean isAcceptionGZip(final HttpServletRequest request) {
//        final String encodings = request.getHeader("Accept-Encoding");
//        final String encodeFlag = request.getParameter("encoding");
//
//        return ((encodings != null) && (encodings.indexOf("gzip") != -1) && 
//                (!"none".equals(encodeFlag)));
//    }

    /**
     *
     * @return
     * @throws FileNotFoundException
     */
    protected InputStream createInputStream() throws FileNotFoundException {

        // lag en bufferoutputstream fra den temporær filen som inneholder data fra DAO
        // data skal leses fra filen bulk for bulk à 4096 bytes
        return new BufferedInputStream(new FileInputStream(file), BUFFER_SIZE);
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    protected final OutputStream createOutputStream() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        return out;
    }

    protected String replaceInvalidCharacters(String tekstStreng) {
        char charList[] = { '/', '*', '?', '[', ']', '\\' };

        // Tekststreng kan ikke være over 31 tegn
        if (tekstStreng.length() > 31) {
            tekstStreng = tekstStreng.substring(0, 30);
        }

        // Fjerner ugyldige tegn
        for (int i = 0; i < charList.length; i++) {
            tekstStreng = tekstStreng.replace(charList[i], ' ');
        }

        return tekstStreng;
    }
}


