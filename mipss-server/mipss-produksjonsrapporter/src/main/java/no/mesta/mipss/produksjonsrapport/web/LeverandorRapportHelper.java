package no.mesta.mipss.produksjonsrapport.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportSequence;
import no.mesta.mipss.produksjonsrapport.values.TiltakComparator;
import no.mesta.mipss.produksjonsrapport.values.TiltakSequence;


//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------


public class LeverandorRapportHelper {

    /**
     * Returnerer en liste med KontraktRapportSequence objekter. Denne listen
     * er sortert på regnr. Det opprettes en ny KontraktRapportSequence for hvert
     * regnr. 
     * @param result
     * @param globalDynamicHeaderList
     * @return
     */
    public static ArrayList getKontraktLists(KontraktRapportSequence result, 
                                             TiltakSequence globalDynamicHeaderList) throws Exception{
        String leverandor = null;
        ArrayList kontraktList = new ArrayList();
        KontraktRapportSequence sequence = new KontraktRapportSequence(globalDynamicHeaderList);
        KontraktRapportObject object = null;

        // Henter ut første leverandør fra liste (som er sortert på regnr)
        if ((result != null) && (result.size() > 0)) {
            leverandor = ((KontraktRapportObject)result.getAt(0)).getLeverandorNavn();
        }
        
        Iterator it = result.getArrayList().iterator();
        while (it.hasNext()){
            object = (KontraktRapportObject)it.next();
            
            String curLeverandor = object.getLeverandorNavn();
            
            
            // ny leverandør, opprett ny liste for denne leverandøren
            if (!curLeverandor.equals(leverandor)){
                
                leverandor = curLeverandor;
                kontraktList.add(sequence);
                sequence = new KontraktRapportSequence(globalDynamicHeaderList);

                object.createTiltakFromXML();
                sequence.updateTiltakTypeListForEnhet(object, object.getLeverandorNavn());
                sequence.add(object);
            }
          
            // samme leverandør, legg dette objektet til gjeldende liste
            else {
                object.createTiltakFromXML();
                sequence.updateTiltakTypeListForEnhet(object, object.getLeverandorNavn());
                sequence.add(object);
            }
        }
        
        globalDynamicHeaderList.sort(new TiltakComparator());
        /*Iterator it2 = globalDynamicHeaderList.getArrayList().iterator();
        while (it2.hasNext()){
            System.out.println(((TiltakObject)it2.next()).getNavn());
        }
        */
        // add last element
        kontraktList.add(sequence);
        
        return kontraktList;
    }

    public static String replaceInvalidCharacters(String tekstStreng) {
        char charList[] = { '/', '*', '?', '[', ']', '\\' };
        for (int i = 0; i < charList.length; i++) {
            tekstStreng = tekstStreng.replace(charList[i], ' ');
        }

        return tekstStreng;
    }

    public static String parseString(String tekstStreng) throws Exception {
        String tekst = tekstStreng.toUpperCase();
        StringBuilder newTekst = new StringBuilder();
        for (int i=0;i<tekst.length();i++){
            char tegn = tekst.charAt(i);
            
            //erstatt underscore med whitespace
            if (tegn==32){
                newTekst.append("_");
            }
            //legg til gyldige tegn, tillater kun 0-9 og A-Z
            else if ((tegn>47 && tegn<58) || (tegn>64 && tegn<91)){ 
                newTekst.append(""+tegn);
            }
            //erstatt alle andre tegn med x
            else
                newTekst.append("x");
        }
        /*
        for (int i = 0; i < tekstStreng.length(); i++) {
            char tegn = tekstStreng.charAt(i);
            if (!((47 < tegn) && (tegn < 58)) && !((64 < tegn) && (tegn < 91))) {

                // tekst = tekst.replaceAll(String.valueOf(String.valueOf(tegn)), "");
                if (tegn!=197)
                    tekst = tekst.replace(tegn, ' ');
                tekst = tekst.replaceAll(" ", "");
            }
        }
        */
      /*  if (tekst.length() > 15) {
            tekst = tekst.substring(0, 14);
        }
    */
        // M - maskin
        if (isNumeric(newTekst.toString())) {
            //tekst = "M" + tekst;
            newTekst.insert(0, 'M');
        }
        if (newTekst.length()>29)
            newTekst.setLength(29);
        
        
        return newTekst.toString();
        //return tekst;
    }

    public static boolean isNumeric(String str) {
        try {
            Long.parseLong(str);

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static String getDatoTid() {
        DateFormat datoFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date dato = new Date();

        return datoFormat.format(dato);
    }
}


