package no.mesta.mipss.produksjonsrapport.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Materiale;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Veirapp;
import no.mesta.mipss.persistence.rapportfunksjoner.VeirappObject;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.ReportMetadataItem.VALUE_NAMES;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class Veirapport extends Rapp1Rapport{
	private String templateFile = "veirapport";
	private final List<Veirapp> data;
	private HashMap<String, Object> map;
	private List<Materiale> alleMaterialer;
	private List<VeirappObject> veirappList;
	
	public static void main(String[] args) throws ParseException, FileNotFoundException, IOException, ClassNotFoundException {
		SimpleDateFormat f = new SimpleDateFormat("ddMMyy hhmmss");
		Date fraDato = f.parse("060111 000000");
		Date tilDato = f.parse("060111 235959");
		ProdrapportParams p = new ProdrapportParams();
		p.setDriftkontraktId(Long.valueOf(22));
		p.setKontraktNavn("123-kontrakten");
		p.setRodetypeId(Long.valueOf(15));
		p.setRodeNavn("<Alle valgt>");
		p.setGjennomsnitt(true);
		p.setProsent(true);
		p.setRodetypeNavn("Strøing og brøyting trallalalalal");
		p.setFraDato(fraDato);	
		p.setTilDato(tilDato);
		
		FileInputStream in = new FileInputStream("C:/code/mipss-studio/studio-webstart-client/rappObjects.ser");
		ObjectInputStream oin = new ObjectInputStream(in);
		List<Veirapp> objs = (List<Veirapp>) oin.readObject();
		Veirapport v = new Veirapport(objs, p);
		v.finalizeAndOpen();
	}
	public Veirapport(List<Veirapp> data, ProdrapportParams params) {
		super(params);
		this.data = data;
		file = RapportHelper.createTempFile(templateFile);
		map = new HashMap<String, Object>();
		map.put("params", params);
		
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 6;
		startRow = 9;
		freezePaneCol=0;
		initData();
//		writeReport(veirappList);
	}

	private void initData() {
		
		Map<String, List<Veirapp>> listePrVei= RapportHelper.createListePrVeikategori(data);
		int startRow = 9;
		int curRow = startRow;
		XSSFSheet sheet = workbook.getSheetAt(0);
		String[] order = new String[]{"E", "R", "F", "K"};
		Map<String, String> keyToValue = new HashMap<String, String>();
		keyToValue.put("E", "Europavei");
		keyToValue.put("R", "Riksvei");
		keyToValue.put("F", "Fylkesvei");
		keyToValue.put("K", "Kommunevei");
		for (String key:order){
			List<Veirapp> listeAvKategori = listePrVei.get(key);
			if (listeAvKategori==null)
				continue;
			Collections.sort(listeAvKategori, new Comparator<Veirapp>(){
				@Override
				public int compare(Veirapp o1, Veirapp o2) {
					return o1.getKjoretoyNavn().compareTo(o2.getKjoretoyNavn());
				}
			});
			Map<Long, List<Veirapp>> listePrVeinummer = RapportHelper.createListePrVeinummer(listeAvKategori);
			Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
			Map<Long, Materiale> materialeMap = new HashMap<Long, Materiale>();
			
			List<VeirappObject> veinummerList = RapportHelper.createForsideDataVeirapport(listePrVeinummer, prodtypeMap, materialeMap);
			List<Materiale> alleMaterialer = RapportHelper.extractAndSortMateriale(materialeMap);
			List<R12Prodtype> alleProdtyper = RapportHelper.extractAndSortProdtyper(prodtypeMap);
			
			for (VeirappObject v:veinummerList){
				v.organizeProdtyper(alleProdtyper);
				v.organizeMateriale(alleMaterialer);
			}
			metadata = new ReportMetadata(veinummerList, params);
			int totalColumnCount = metadata.getTotalColumnCount();
			startRow = curRow;
			XSSFRow row0 = excel.getRow(sheet, curRow);
			excel.mergeCells(sheet, 0, totalColumnCount, curRow, curRow);
			writeHeaderCell(row0, 0, keyToValue.get(key));
			for (int i=0;i<=totalColumnCount;i++){
				excel.setCellStyle(excel.getHeaderStyleLeft(), row0, i);
			}
			
			curRow++;
			XSSFRow row1 = excel.getRow(sheet, curRow);
			XSSFRow row2 = excel.getRow(sheet, curRow+2);
			int column = 0;
			writeHeaderCell(row1, column++, "Veinummer");
			writeHeaderCell(row1, column++, "Periode");
			
			CellRangeAddress mergeCells = excel.mergeCells(sheet, 0, 0, curRow, curRow+2);
			excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
			mergeCells = excel.mergeCells(sheet, 1, 1, curRow, curRow+2);
			excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
			
			writeHeaderCell(row1, column, "Total");
			writeHeaderCell(row2, column++, "Km");
			writeHeaderCell(row2, column++, "Timer");
			mergeCells = excel.mergeCells(sheet, 2, 3, curRow, curRow+1);
			excel.styleMergedRegion(sheet, mergeCells, excel.getHeaderStyle(false));
			
			curRow = writeHeaders(curRow, column)+2;
			curRow = writeData(curRow, veinummerList, 4);
			curRow = writeSum(curRow, curRow-1, 4)+3;
			
			if (params.isGjennomsnitt()){
				addConditionalFormatting(startRow+4, curRow-2, VALUE_NAMES.Kmt);
			}
			VeirapportSheet veirapportSheet = new VeirapportSheet(listeAvKategori, params);
			XSSFSheet newSheet = excel.createSheet(RapportHelper.parseString(keyToValue.get(key)), workbook);
			Util.copySheets(newSheet, veirapportSheet.getSheet());
			addAutoFilter(newSheet, 13);
			newSheet.createFreezePane(0, 14);
		}
	}
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		VeirappObject d= (VeirappObject)data;
		excel.writeData(dataRow, 0, d.getVeinummer());
		excel.writeData(dataRow, 1, d.getPeriode());
		excel.writeData(dataRow, 2, d.getKm());
		excel.writeData(dataRow, 3, d.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 3);
	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 0);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 1);
		addSumCell(2, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(3, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}

}
