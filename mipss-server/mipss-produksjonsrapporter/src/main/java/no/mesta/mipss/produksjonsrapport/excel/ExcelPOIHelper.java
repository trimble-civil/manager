package no.mesta.mipss.produksjonsrapport.excel;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.common.MipssDateFormatter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelPOIHelper {

	private File tempFile;
	private String dateFormat = MipssDateFormatter.LONG_DATE_TIME_FORMAT;
	private Styles styles;
	private Map<String,List<XSSFCellStyle>> cellStyleCache = new HashMap<String, List<XSSFCellStyle>>();
	
	public ExcelPOIHelper(){
		
	}
	public ExcelPOIHelper(XSSFWorkbook workbook){
		styles = new Styles(workbook);
	}
	
	public InputStream getExcelTemplate(String fileName){
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(fileName);
		if (url != null) {
			try{
			URLConnection connection = url.openConnection();
			if (connection != null) {
			    connection.setUseCaches(false);
			    return connection.getInputStream();
			}
			} catch (Exception e){
				throw new RuntimeException(e);
			}
		}
		return null;
	}
	
	public XSSFWorkbook createWorkbook(File tempFile, InputStream is){
		this.tempFile = tempFile;
		try {
//			POIFSFileSystem fs = new POIFSFileSystem(is);
			XSSFWorkbook workbook = new  XSSFWorkbook(is);
			styles = new Styles(workbook);
			return workbook;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void closeWorkbook(Workbook workbook) {
		try {
			FileOutputStream dos = new FileOutputStream(tempFile);
			workbook.write(dos);
			dos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public File getExcelFile(){
		return tempFile;
	}
	public Sheet getSheet(int index, Workbook workbook){
		return workbook.getSheetAt(index);
	}
	public XSSFSheet getSheet(int index, XSSFWorkbook workbook){
		return workbook.getSheetAt(index);
	}
	public XSSFSheet createSheet(String sheetName, XSSFWorkbook workbook){
		return workbook.createSheet(sheetName);
	}

	public void setCellStyle(XSSFCellStyle style, XSSFRow row, int col){
		XSSFCell cell = row.getCell(col);
		if (cell==null)
			cell = row.createCell(col);
		cell.setCellStyle(style);
	}
	public void setRowStyle(XSSFCellStyle style, XSSFRow row){
		row.setRowStyle(style);
	}
	/**
	 * Setter samme excel-style på alle cellene i en CellRangeAddress, sammenslåtte celler får ikke nødvendigvis samme stil. Bruk derfor denne
	 * metoden for å sette samme stil på de sammenslåtte cellene.
	 * @param sheet
	 * @param region
	 * @param style
	 */
	public void styleMergedRegion(XSSFSheet sheet, CellRangeAddress region, XSSFCellStyle style){
		int col = region.getFirstColumn();
		int row = region.getFirstRow();
		int lastCol = region.getLastColumn();
		int lastRow = region.getLastRow();
		int colSpan = lastCol-col;
		int rowSpan = lastRow-row;
		for (int i=0;i<=colSpan;i++){
			for (int j=0;j<=rowSpan;j++){
				XSSFRow r = getRow(sheet, row+j);
				setCellStyle(style, r, col+i);
			}
		}
	}
	/**
	 * WARNING: denne funksjonen er TREG.
	 * @param row
	 * @param lastCol
	 */
	public void setDefaultBorders(Row row, int lastCol){
		for (int i=0;i<=lastCol;i++){
			Cell cell = row.getCell((short)i);
			if (cell==null){
				cell = row.createCell((short)i);
				cell.setCellStyle(styles.getTextStyle(true));
			}
			cell.getCellStyle().setBorderLeft(XSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setBorderTop(XSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setBorderRight(XSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setBorderBottom(XSSFCellStyle.BORDER_THIN);
			cell.getCellStyle().setLeftBorderColor(new XSSFColor(Color.lightGray).getIndexed());
			cell.getCellStyle().setTopBorderColor(new XSSFColor(Color.lightGray).getIndexed());
			cell.getCellStyle().setRightBorderColor(new XSSFColor(Color.lightGray).getIndexed());
			cell.getCellStyle().setBottomBorderColor(new XSSFColor(Color.lightGray).getIndexed());
		}
	}
	public void addBorder(XSSFRow row, int col){
		XSSFCell cell = row.getCell(col);
		cell.getCellStyle().setBorderLeft(XSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setBorderTop(XSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setBorderRight(XSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setBorderBottom(XSSFCellStyle.BORDER_THIN);
		cell.getCellStyle().setLeftBorderColor(new XSSFColor(Color.black));
		cell.getCellStyle().setTopBorderColor(new XSSFColor(Color.black));
		cell.getCellStyle().setRightBorderColor(new XSSFColor(Color.black));
		cell.getCellStyle().setBottomBorderColor(new XSSFColor(Color.black));
	}
	public int writeDouble(XSSFRow row, int col, Double data, XSSFCellStyle style){
		if (style==null){
			return writeData(row, col, data);
		}
		XSSFCell cell = row.createCell((short)col);
		cell.setCellStyle(styles.getDecimalStyleShort(false));
		cell.setCellValue((Double)data);
		return computeNewColWidth(0, data);
	}
	
	public int writeData(Row row, int col, Object data, boolean wrapText){
		int colWidth = writeData(row, col, data);
		Cell cell = row.getCell((short) col);
		if (cell!=null)
			cell.getCellStyle().setWrapText(true);
		return colWidth;
	}
	
	public void setCellStyle(XSSFRow row, int col,XSSFCellStyle style){
		XSSFCell cell = row.getCell(col);
		cell.setCellStyle(style);
		
	}
	
	public int writeDataBordered(XSSFRow row, int col, Object data){
		int colWidth = 0;
		if (data!=null){
			XSSFCell cell = row.createCell((short) col);
			Class<?> clazz = data.getClass();
			
			if (clazz.equals(String.class)){
				cell.setCellStyle(styles.getTextStyle(true));
				cell.setCellValue(new XSSFRichTextString(data.toString()));
			}else if (clazz.equals(Long.class)){
				cell.setCellStyle(styles.getNumStyle(true));
				cell.setCellValue((Long)data);
			}else if (clazz.equals(Double.class)){
				cell.setCellStyle(styles.getDecimalStyle(true));
				cell.setCellValue((Double)data);
			}else if (clazz.equals(Integer.class)){
				cell.setCellStyle(styles.getNumStyle(true));
				cell.setCellValue((Integer)data);
			}else if (clazz.equals(Date.class)){
				cell.setCellStyle(styles.getDatoStyle(dateFormat, true));
				cell.setCellValue((Date)data);
			}else if (clazz.equals(Boolean.class)){
				cell.setCellStyle(styles.getNumStyle(true));
				Boolean value = (Boolean) data;
				Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
				cell.setCellValue(number);
			}else{
				try{
					Integer value = Integer.parseInt(data.toString());
					cell.setCellStyle(styles.getNumStyle(true));
					cell.setCellValue(value);
				}catch (NumberFormatException e){
					try{
						Double value = Double.parseDouble(data.toString());
						cell.setCellStyle(styles.getDecimalStyle(true));
						cell.setCellValue(value);
						
					}catch (NumberFormatException ex){
						ex.printStackTrace();
						cell.setCellStyle(styles.getTextStyle(true));
						cell.setCellValue(new XSSFRichTextString(data.toString()));
					}
				}
				
			}
			
			colWidth = computeNewColWidth(colWidth, data);
		}
		return colWidth;
	}
	public int writeData(Row row, int col, Object data){
		int colWidth = 0;
		
		if (data!=null){
			Cell cell = row.createCell((short) col);
			Class<?> clazz = data.getClass();
			if (clazz.equals(String.class)){
				cell.setCellStyle(styles.getTextStyle(false));
				cell.setCellValue(new XSSFRichTextString(data.toString()));
			}else if (clazz.equals(Long.class)){
				cell.setCellStyle(styles.getNumStyle(false));
				cell.setCellValue((Long)data);
			}else if (clazz.equals(Double.class)){
				cell.setCellStyle(styles.getDecimalStyle(false));
				cell.setCellValue((Double)data);
			}else if (clazz.equals(Integer.class)){
				cell.setCellStyle(styles.getNumStyle(false));
				cell.setCellValue((Integer)data);
			}else if (clazz.equals(Date.class)){
				cell.setCellStyle(styles.getDatoStyle(dateFormat, false));
				cell.setCellValue((Date)data);
			}else if (clazz.equals(Timestamp.class)){
				cell.setCellStyle(styles.getDatoStyle(dateFormat, false));
				cell.setCellValue((Date)data);
			}else if (clazz.equals(Boolean.class)){
				cell.setCellStyle(styles.getNumStyle(false));
				Boolean value = (Boolean) data;
				Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
				cell.setCellValue(number);
			}else{
				try{
					Integer value = Integer.parseInt(data.toString());
					cell.setCellStyle(styles.getNumStyle(false));
					cell.setCellValue(value);
				}catch (NumberFormatException e){
					try{
						Double value = Double.parseDouble(data.toString());
						cell.setCellStyle(styles.getDecimalStyle(false));
						cell.setCellValue(value);
						
					}catch (NumberFormatException ex){
						ex.printStackTrace();
						cell.setCellStyle(styles.getTextStyle(false));
						cell.setCellValue(new XSSFRichTextString(data.toString()));
					}
				}
				
			}
			
			colWidth = computeNewColWidth(colWidth, data);
		}
		return colWidth;
	}
	
	private int computeNewColWidth(int colWidth, Object data) {
		int width = 0;
		if (data instanceof String) {
			width = ((String) data).length();
			width = width > 200 ? 200 : width;
		} else if (data instanceof Long) {
			width = String.valueOf((Long) data).length();
		} else if (data instanceof Double) {
			width = String.valueOf((Double) data).length();
		} else if (data instanceof Integer) {
			width = String.valueOf((Integer) data).length();
		} else if (data instanceof Date) {
			width = MipssDateFormatter.SHORT_DATE_TIME_FORMAT.length();
		} else if (data instanceof Boolean) {
			width = 2;
		}

		return Math.max(colWidth, width);
	}
	public XSSFRow createRow(Sheet sheet, int row){
		return (XSSFRow)sheet.createRow(row);
	}
	public XSSFRow getRow(Sheet sheet, int row){
		XSSFRow xrow = (XSSFRow)sheet.getRow(row);
		if (xrow==null)
			xrow = (XSSFRow)sheet.createRow(row);
		return xrow;
	}
	/**
	 * Legger til en celle i arket
	 * @param sheet
	 * @param cell
	 */
	public XSSFCell createCell(XSSFRow row, int column) {
		XSSFCell cell = row.getCell(column);
		if (cell==null)
			return row.createCell((short)column);
		return cell;
	}

	
	public CellRangeAddress mergeCells(XSSFSheet sheet, int colFrom, int colTo, int rowFrom, int rowTo){
		CellRangeAddress region = new CellRangeAddress(rowFrom, rowTo, colFrom, colTo);
		sheet.addMergedRegion(region);
		return region;
	}
	public void setDateFormat(String dateFormat){
		this.dateFormat = dateFormat;
	}
	
	public void setCellValue(XSSFCell cell, String value){
		cell.setCellValue(new XSSFRichTextString(value));
	}
	public void createLinkCell(XSSFRow row, int column, String title, String target, boolean border){
		XSSFCell linkCell = row.createCell((short)column);
		linkCell.setCellStyle(styles.getHyperlinkStyle(border));
	    String formula = "HYPERLINK(" + target + ", " + title + ")";
	    linkCell.setCellFormula(formula);
	}
	public void linkify(XSSFCell cell, String title, String target){
		cell.setCellStyle(styles.getHyperlinkStyle(false));
		String formula = "HYPERLINK(" + target + ", " + title + ")";
	    cell.setCellFormula(formula);
	}
	
	public Styles getStyles(){
		return styles;
	}
	
	public XSSFCellStyle getTitleStyle(boolean border){
		return styles.getTextStyle(border);
	}
	public XSSFCellStyle getDecimalStyleShort(boolean border){
		return styles.getDecimalStyleShort(border);
	}
	public XSSFCellStyle getDecimalStyle(boolean border){
		return styles.getDecimalStyle(border);
	}
	
	public XSSFCellStyle getM3InfoStyle(){
		return styles.getM3InfoStyle();
	}
	
	public XSSFCellStyle getHeaderStyle(boolean border){
		return styles.getHeaderStyle(border);
	}
	public XSSFCellStyle getDatoStyle(String dateFormat, boolean border){
		return styles.getDatoStyle(dateFormat, border);
	}
	public XSSFCellStyle getPaaKontraktStyle(){
		return styles.getPaaKontraktStyle();
	}
	public XSSFCellStyle getDatoStyleSum(String dateFormat){
		return styles.getDatoStyleSum(dateFormat);
	}
	public XSSFCellStyle getDecimalStyleM3Sum(){
		return styles.getDecimalStyleM3Sum();
	}
	public XSSFCellStyle getDecimalStyleSum(){
		return styles.getDecimalStyleSum();
	}
	public XSSFCellStyle getDecimalStyleShortSum(){
		return styles.getDecimalStyleShortSum();
	}
	public XSSFCellStyle getTextStyleSum(){
		return styles.getTextStyleSum();
	}
	
	public XSSFCellStyle getHeaderStyleLeft(){
		return styles.getHeaderStyleLeft();
	}
	
	private class Styles{
		public  XSSFCellStyle titleStyle;
		public  XSSFCellStyle hyperlinkStyle;
		public  XSSFCellStyle textStyle;
		public  XSSFCellStyle textStyleSum;
		public  XSSFCellStyle numStyle;
		public  XSSFCellStyle decimalStyle;
		public  XSSFCellStyle decimalStyleSum;
		public  XSSFCellStyle decimalStyleM3Sum;
		public  XSSFCellStyle decimalStyleShort;
		public  XSSFCellStyle decimalStyleShortSum;
		public  XSSFCellStyle headerStyle;
		public  XSSFCellStyle headerStyleLeft;
		
		private final XSSFWorkbook workbook;
		private XSSFFont fontText;
		private XSSFFont m3InfoText;
		
		private XSSFCellStyle textStyleBorder;
		private XSSFCellStyle numStyleBorder;
		private XSSFCellStyle decimalStyleBorder;
		private XSSFCellStyle decimalStyleShortBorder;
		private XSSFCellStyle hyperlinkStyleBorder;
		private XSSFCellStyle headerStyleBorder;
		private XSSFCellStyle paaKontraktStyle;
		private XSSFCellStyle m3InfoStyle;
		
		
		private Color mestaBlue = new Color(0,0,153);
		public Styles(XSSFWorkbook workbook){
	        this.workbook = workbook;
			fontText = workbook.createFont();
	        fontText.setFontHeightInPoints((short)10);
	        fontText.setFontName("Arial");
	        fontText.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
	        fontText.setColor(IndexedColors.BLACK.index);
	        
	        m3InfoText = workbook.createFont();
	        m3InfoText.setFontHeightInPoints((short)10);
	        m3InfoText.setFontName("Arial");
	        m3InfoText.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
	        m3InfoText.setColor(IndexedColors.GREY_40_PERCENT.index);
	        
	        
	        XSSFFont fontNumber = workbook.createFont();
	        fontNumber.setFontHeightInPoints((short)10);
	        fontNumber.setFontName("Arial");
	        fontNumber.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	        fontNumber.setColor(IndexedColors.BLACK.index);
	        
	        XSSFFont hfontReport = workbook.createFont();
	        hfontReport.setFontHeightInPoints((short)14);
	        hfontReport.setFontName("Arial");
	        hfontReport.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	        hfontReport.setColor(IndexedColors.BLACK.index);
	        
	        XSSFFont fontHyperlink = workbook.createFont();
	        fontHyperlink.setFontHeightInPoints((short)10);
	        fontHyperlink.setFontName("Arial");
	        fontHyperlink.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
	        fontHyperlink.setUnderline(XSSFFont.U_SINGLE);
	        fontHyperlink.setColor(IndexedColors.BLUE.index);
	        
	        XSSFFont fontHeader = workbook.createFont();
	        fontHeader.setFontHeightInPoints((short)10);
	        fontHeader.setFontName("Arial");
	        fontHeader.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	        fontHeader.setColor(IndexedColors.WHITE.index);
	        
			textStyle = getCellStyle("", fontText, XSSFCellStyle.ALIGN_LEFT, false, 
					new XSSFColor(Color.white), 
					XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			
			numStyle = getCellStyle("0", fontNumber, XSSFCellStyle.ALIGN_LEFT, false, 
					new XSSFColor(Color.white), 
	                XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			
			decimalStyle = getCellStyle("0.000", fontNumber, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			decimalStyleSum = getCellStyle("0.000", fontNumber, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_DOUBLE, 
                    XSSFCellStyle.BORDER_THICK, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			decimalStyleM3Sum = getCellStyle("0.000", m3InfoText, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.gray), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_DOUBLE, 
                    XSSFCellStyle.BORDER_THICK, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
					decimalStyleM3Sum.setBottomBorderColor(IndexedColors.BLACK.index);
					decimalStyleM3Sum.setTopBorderColor(IndexedColors.BLACK.index);
			
			textStyleSum = getCellStyle("", fontNumber, XSSFCellStyle.ALIGN_LEFT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_DOUBLE, 
                    XSSFCellStyle.BORDER_THICK, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			decimalStyleShort = getCellStyle("0.0", fontNumber, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			decimalStyleShortSum = getCellStyle("0.0", fontNumber, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_DOUBLE, 
                    XSSFCellStyle.BORDER_THICK, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
			
	        hyperlinkStyle = getCellStyle("", fontHyperlink, XSSFCellStyle.ALIGN_LEFT, false, 
	        		new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
	        
	        titleStyle = getReportHeadingStyle(hfontReport, XSSFCellStyle.ALIGN_LEFT);

	        headerStyle = getCellStyle("", fontHeader, XSSFCellStyle.ALIGN_CENTER, true, 
	        		new XSSFColor(mestaBlue), 
                    XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
	        
	        headerStyleLeft = getCellStyle("", fontHeader, XSSFCellStyle.ALIGN_LEFT, true, 
	        		new XSSFColor(mestaBlue), 
                    XSSFCellStyle.SOLID_FOREGROUND,false, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE, 
                    XSSFCellStyle.BORDER_NONE);
	        
	        
	        textStyleBorder = getCellStyle("", fontText, XSSFCellStyle.ALIGN_LEFT, false, 
	        		new XSSFColor(Color.white), 
					XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR);
			
			numStyleBorder = getCellStyle("0", fontNumber, XSSFCellStyle.ALIGN_LEFT, false, 
					new XSSFColor(Color.white), 
	                XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR);
			
			decimalStyleBorder = getCellStyle("0.000", fontNumber, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR);
			
			decimalStyleShortBorder = getCellStyle("0.0", fontNumber, XSSFCellStyle.ALIGN_RIGHT, 
					false, new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR);
			
	        hyperlinkStyleBorder = getCellStyle("", fontHyperlink, XSSFCellStyle.ALIGN_LEFT, false, 
	        		new XSSFColor(Color.white), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR, 
                    XSSFCellStyle.BORDER_HAIR);
	        
	        headerStyleBorder = getCellStyle("", fontHeader, XSSFCellStyle.ALIGN_CENTER, true, 
	        		new XSSFColor(mestaBlue), 
                    XSSFCellStyle.SOLID_FOREGROUND,true, 
                    XSSFCellStyle.BORDER_THIN, 
                    XSSFCellStyle.BORDER_THIN, 
                    XSSFCellStyle.BORDER_THIN, 
                    XSSFCellStyle.BORDER_THIN);
	        
	        headerStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
	        headerStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
//	        headerStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
//	        headerStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
	        headerStyle.setLeftBorderColor(new XSSFColor(Color.white));
	        headerStyle.setRightBorderColor(new XSSFColor(Color.white));
//	        headerStyle.setBottomBorderColor(new XSSFColor(Color.white));
//	        headerStyle.setTopBorderColor(new XSSFColor(Color.white));
	        
	        headerStyleLeft.setBottomBorderColor(new XSSFColor(Color.white));
	        headerStyleLeft.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
	        
	        
	        
	        paaKontraktStyle = workbook.createCellStyle();
	        paaKontraktStyle.setFillBackgroundColor(new XSSFColor(new Color(255,100,100)));
	        paaKontraktStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
	        paaKontraktStyle.setBottomBorderColor(new XSSFColor(Color.blue));
	        
	        m3InfoStyle = workbook.createCellStyle();
	        m3InfoStyle.setFillForegroundColor(new XSSFColor(new Color(200, 200, 200)));
	        m3InfoStyle.setFont(m3InfoText);
	        
		}
		public XSSFCellStyle getM3InfoStyle() {
			return m3InfoStyle;
		}
		
		public XSSFCellStyle getPaaKontraktStyle() {
			return paaKontraktStyle;
		}
		public XSSFCellStyle getTextStyleSum() {
			return textStyleSum;
		}
		public XSSFCellStyle getHeaderStyleLeft() {
			return headerStyleLeft;
		}
		public XSSFCellStyle getTextStyle(boolean border) {
			if (border)
				return textStyleBorder;
			return textStyle;
		}
		public XSSFCellStyle getNumStyle(boolean border) {
			if (border)
				return numStyleBorder;
			return numStyle;
		}
		public XSSFCellStyle getTitleStyle() {
			return titleStyle;
		}
		public XSSFCellStyle getHyperlinkStyle(boolean border){
			if (border)
				return hyperlinkStyleBorder;
			return hyperlinkStyle;
		}
		public XSSFCellStyle getDecimalStyleShort(boolean border){
			if (border)
				return decimalStyleShortBorder;
			return decimalStyleShort;
		}
		public XSSFCellStyle getHeaderStyle(boolean border){
			if (border)
				return headerStyleBorder;
			return headerStyle;
		}
		Map<String, XSSFCellStyle> datoStyleCache = new HashMap<String, XSSFCellStyle>();
		Map<String, XSSFCellStyle> datoStyleCacheBorder = new HashMap<String, XSSFCellStyle>();
		Map<String, XSSFCellStyle> datoStyleCacheSum = new HashMap<String, XSSFCellStyle>();
		
		public XSSFCellStyle getDatoStyle(String dateFormat, boolean border) {
			if (border){
				XSSFCellStyle s = datoStyleCacheBorder.get(dateFormat);
				if (s==null){
					s =getCellStyle(dateFormat, fontText, XSSFCellStyle.ALIGN_LEFT, 
		                    false, new XSSFColor(Color.white), 
		                    XSSFCellStyle.SOLID_FOREGROUND, true, 
		                    XSSFCellStyle.BORDER_HAIR, 
	                        XSSFCellStyle.BORDER_HAIR, 
	                        XSSFCellStyle.BORDER_HAIR, 
	                        XSSFCellStyle.BORDER_HAIR);
					datoStyleCacheBorder.put(dateFormat, s);
				}
				return s;
			}
			XSSFCellStyle s = datoStyleCache.get(dateFormat);
			if (s==null){
				s =getCellStyle(dateFormat, fontText, XSSFCellStyle.ALIGN_LEFT, 
	                    false, new XSSFColor(Color.white), 
	                    XSSFCellStyle.SOLID_FOREGROUND, false, 
	                    XSSFCellStyle.BORDER_NONE, 
                        XSSFCellStyle.BORDER_NONE, 
                        XSSFCellStyle.BORDER_NONE, 
                        XSSFCellStyle.BORDER_NONE);
				datoStyleCache.put(dateFormat, s);
			}
			return s;
		}
		public XSSFCellStyle getDatoStyleSum(String dateFormat) {
			XSSFCellStyle s = datoStyleCacheSum.get(dateFormat);
			if (s==null){
				s =getCellStyle(dateFormat, fontText, XSSFCellStyle.ALIGN_LEFT, 
	                    false, new XSSFColor(Color.white), 
	                    XSSFCellStyle.SOLID_FOREGROUND, true, 
	                    XSSFCellStyle.BORDER_DOUBLE, 
                        XSSFCellStyle.BORDER_THICK, 
                        XSSFCellStyle.BORDER_NONE, 
                        XSSFCellStyle.BORDER_NONE);
				datoStyleCacheSum.put(dateFormat, s);
			}
			return s;
		}
		public XSSFCellStyle getDecimalStyle(boolean border) {
			if (border)
				return decimalStyleBorder;
			return decimalStyle;
		}
		
		public XSSFCellStyle getDecimalStyleM3Sum() {
			return decimalStyleM3Sum;
		}

		public XSSFCellStyle getDecimalStyleSum() {
			return decimalStyleSum;
		}
		public XSSFCellStyle getDecimalStyleShortSum() {
			return decimalStyleShortSum;
		}
		 
		
		private final XSSFCellStyle getCellStyle(String dataFormat, XSSFFont font, short alignment, boolean fillColor, XSSFColor forgroundColor, short fillPattern,
				boolean border, 
				short borderBottom, 
                short borderTop, 
                short borderLeft, 
                short borderRight) {
			
			XSSFCellStyle cellStyle = workbook.createCellStyle();
//			cellStyle.setFillForegroundColor(new XSSFColor(Color.black));
			cellStyle.setFont(font);
			cellStyle.setAlignment(alignment);
			if (fillColor) {
				cellStyle.setFillForegroundColor(forgroundColor);
				cellStyle.setFillPattern(fillPattern);
			}
			
			if (dataFormat.length() > 0) {
				XSSFDataFormat cellFormat = workbook.createDataFormat();
				cellStyle.setDataFormat(cellFormat.getFormat(dataFormat));
			}
			if (border) {
	            cellStyle.setBorderBottom(borderBottom);
	            cellStyle.setBorderLeft(borderLeft);
	            cellStyle.setBorderRight(borderRight);
	            cellStyle.setBorderTop(borderTop);
	        }
			return cellStyle;
		}
		
		private final XSSFCellStyle getReportHeadingStyle(XSSFFont font, short alignment) {
			XSSFCellStyle hcellStyle = workbook.createCellStyle();
			hcellStyle.setFont(font);
			hcellStyle.setAlignment(alignment);
			return hcellStyle;
		}
	}
	List<XSSFCellStyle> styleCacheOrig = new ArrayList<XSSFCellStyle>();
	List<XSSFCellStyle> styleCacheBg = new ArrayList<XSSFCellStyle>();
	public void createBakgrunnStyle(XSSFRow dataRow, int lastCellNum) {
		for (int i=0;i<lastCellNum;i++){
			XSSFCell cell = dataRow.getCell(i);
			if (cell==null){
				cell = dataRow.createCell(i);
			}
			XSSFCellStyle styleToApply = null;
			if (cell!=null){
				XSSFCellStyle style = cell.getCellStyle();
				if (styleCacheOrig.contains(style)){
					styleToApply =styleCacheBg.get(styleCacheOrig.indexOf(style));
					System.out.println("NEW");
				}else{
					XSSFCellStyle bgStyle = dataRow.getSheet().getWorkbook().createCellStyle();
					bgStyle.cloneStyleFrom(style);
					bgStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
					bgStyle.setFillForegroundColor(new XSSFColor(new Color(255, 231, 231)));
					styleCacheBg.add(bgStyle);
					styleToApply = bgStyle;
				}
				cell.setCellStyle(styleToApply);
			}
		}
		
	}
}
