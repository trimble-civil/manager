package no.mesta.mipss.produksjonsrapport.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.adapter.DetaljertPeriodeRapport;
import no.mesta.mipss.produksjonsrapport.adapter.PeriodeRapport;
import no.mesta.mipss.produksjonsrapport.adapter.PeriodeRapportAdapter;
import no.mesta.mipss.produksjonsrapport.query.KontraktPeriodeQuery;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportHelper;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportSequence;
import no.mesta.mipss.produksjonsrapport.values.MestaLogo;
import no.mesta.mipss.produksjonsrapport.values.TiltakSequence;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------


public class PeriodeRapportHandler extends ExcelReportHSSFHandler {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("prodrappText");
    private EntityManager em;
	private ProdrapportParams params;

	public PeriodeRapportHandler(final File file, EntityManager em) {
        super(file);
        this.em = em;
    }

	public byte[] createReport(ProdrapportParams params) throws Exception {
        this.params = params;
		KontraktRapportSequence result = null;

        result = (KontraktRapportSequence)new KontraktPeriodeQuery(params, em).consumeResults();
        byte[] report = null;
        
        // hvis data finnes, lag excel rapport
        if ((result != null) && (result.size() > 0)) {
            report = createExcelReport(result, params, file);
        }
        return report;
    }

	private byte[] createExcelReport(KontraktRapportSequence result, 
			ProdrapportParams rapportForm, 
			File file) throws FileNotFoundException, 
                             IOException, Exception {
        HSSFWorkbook workbook = null;
        ByteArrayOutputStream ps = new ByteArrayOutputStream();
        try {

            // lag en PrintStream
            workbook = new HSSFWorkbook();

            TiltakSequence globalDynamicHeaderList = new TiltakSequence();
            List<KontraktRapportSequence> periodeList = KontraktRapportHelper.getKontraktLists(result, globalDynamicHeaderList);
            
            TiltakSequence dynamicHeaderList;
            
            Vector<String> fixedHeaderList = null;
            
            
            String rapportNavn = resources.getGuiString("PeriodeRapportHandler.rapportTitle");
            String kontrakt = rapportForm.getKontraktNavn();
            String rodeType = rapportForm.getRodetypeNavn();
            String rodeNavn = rapportForm.getRodeNavn();
            String rapportDato = KontraktRapportHelper.getDatoTid();
            String periode = rapportForm.getPeriode();
            boolean gjennomsnitt = rapportForm.isGjennomsnitt();
            boolean prosent = rapportForm.isProsent();
            Vector<String> dagFixedHeaderList = new Vector<String>();
            dagFixedHeaderList.add(resources.getGuiString("PeriodeRapportHandler.dag"));
            dagFixedHeaderList.add(resources.getGuiString("PeriodeRapportHandler.mnd"));
            dagFixedHeaderList.add("Tid fra");
            dagFixedHeaderList.add("Tid til");
            
            Vector<String> ukeFixedHeaderList = new Vector<String>();
            ukeFixedHeaderList.add(resources.getGuiString("PeriodeRapportHandler.uke"));
            ukeFixedHeaderList.add(resources.getGuiString("PeriodeRapportHandler.aar"));
            ukeFixedHeaderList.add("Tid fra");
            ukeFixedHeaderList.add("Tid til");
            
            Vector<String> mndFixedHeaderList = new Vector<String>();
            mndFixedHeaderList.add(resources.getGuiString("PeriodeRapportHandler.mnd"));
            mndFixedHeaderList.add(resources.getGuiString("PeriodeRapportHandler.aar"));
            mndFixedHeaderList.add("Tid fra");
            mndFixedHeaderList.add("Tid til");
            
            PeriodeRapportAdapter adapter = null;
            
            if (gjennomsnitt||prosent){
                adapter = new PeriodeRapportAdapter(new DetaljertPeriodeRapport(workbook, gjennomsnitt, prosent, params), globalDynamicHeaderList);
            }
            else
                adapter = new PeriodeRapportAdapter(new PeriodeRapport(workbook, params), globalDynamicHeaderList);
            
            HSSFClientAnchor anchor = new HSSFClientAnchor(0,0,0,255, (short)3,2,(short)4,4);
            anchor.setAnchorType( 2 );
            
            for (int i = 0; i < periodeList.size(); i++) {
                KontraktRapportSequence periodeSequence = (KontraktRapportSequence)periodeList.get(i);
                KontraktRapportObject object = (KontraktRapportObject)periodeSequence.getAt(0);
                String enhet = object.getEnhetsID();
                if (enhet.equals("1")) {
                    rapportNavn = resources.getGuiString("PeriodeRapportHandler.rapportNavn.sistemnd");
                    fixedHeaderList = dagFixedHeaderList;
                    enhet = resources.getGuiString("PeriodeRapportHandler.dag");
                } else if (enhet.equals("2")) {
                    rapportNavn = resources.getGuiString("PeriodeRapportHandler.rapportNavn.uke");
                    fixedHeaderList = ukeFixedHeaderList;
                    enhet = resources.getGuiString("PeriodeRapportHandler.uke");
                } else if (enhet.equals("3")) {
                    rapportNavn = resources.getGuiString("PeriodeRapportHandler.rapportNavn.mnd");
                    fixedHeaderList = mndFixedHeaderList;
                    enhet = resources.getGuiString("PeriodeRapportHandler.mnd");
                }

                // String arkPeriode = KontraktRapportHelper.parseString(enhet);
                String arkPeriode = enhet;

                dynamicHeaderList = periodeSequence.getTiltakTypeList(object.getEnhetsID());

                // Oppretter et ark for hver periode (dag, uke, mnd)
                HSSFSheet sheet = workbook.createSheet(arkPeriode);
                //int freezeColumn = detaljert?12:10;
                sheet.createFreezePane(0,11);
                sheet.setDefaultColumnWidth((short)17);
                
                int rowCount = 0;

                // lag rapport header og returnerer neste rad
                rowCount = adapter.createReportHeader(sheet, rapportNavn, kontrakt, rodeType, rodeNavn, "", rapportDato, periode, rowCount, object);

                // lag table header og returnerer neste rad
                rowCount = adapter.createTableHeader(sheet, rowCount, fixedHeaderList, dynamicHeaderList, false);
                rowCount+=3;

                // lag rapportens datarader
                adapter.createDataRows(sheet, arkPeriode, "", null, periodeSequence, dynamicHeaderList, rowCount, 0);
                
                //juster kolonnebredde
                int headerRow = 10;//detaljert?11:9;
                HSSFRow row = sheet.getRow(headerRow);
                if (row!=null){
                    for (int j=4;j<row.getLastCellNum()+1;j++){
                        sheet.autoSizeColumn((short)j);    
                    }
                }
                adjustPrintSetup(sheet, headerRow, file.getName(), true);
                //legg til en logo
                int image = workbook.addPicture(MestaLogo.getLogo(), HSSFWorkbook.PICTURE_TYPE_PNG);
                HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
                patriarch.createPicture(anchor, image);
            }

            workbook.write(ps);
            return ps.toByteArray();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("temp. fil ikke funnet");
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();

            throw new Exception(e);
        } finally {
            ps.close();
        }
    }
}


