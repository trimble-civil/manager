package no.mesta.mipss.produksjonsrapport.excel;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.rapportfunksjoner.Leverandor;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Object;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class KjoretoyRapport extends Rapp1Rapport{
	private final List<Rapp1> data;
	private Map<String, Object> map;
	private String templateFile = "kjoretoy_forside";
	
	private List<Rapp1Object> kjoretoyList;
	
	public static void main(String[] args) throws IOException {
		ProdrapportParams params = new ProdrapportParams();
		params.setFraDato(Clock.now());
		params.setTilDato(Clock.now());
		params.setGjennomsnitt(true);
		params.setProsent(true);
		params.setRodeNavn("Testrode");
		params.setKontraktNavn("0123-Bobs lucky contract");
		new KjoretoyRapport(Leverandor.getTestData(), params).finalizeAndOpen();
	}
	
	public KjoretoyRapport(List<Rapp1> data, ProdrapportParams params){
		super(params);
		this.data = data;
		this.params = params;
		initData();
		file = RapportHelper.createTempFile(templateFile);
		workbook = RapportHelper.readTemplate(templateFile, map);
		excel = new ExcelPOIHelper(workbook);
		startCol = 7;
		startRow = 9;
		writeReport(kjoretoyList);
		createSubsheets();
	}
	
	private void createSubsheets(){
		int sheetIndex = 1;
		for (Rapp1Object kjoretoy:kjoretoyList){
			System.out.println("KjoretoyRapport.Creating sheet:"+kjoretoy.getKjoretoy());
			//FASTER
			new KjoretoyRapportSheet(kjoretoy, params, excel, workbook, sheetIndex);
			//WAY SLOWER
//			XSSFSheet sheet = new KjoretoyRapportSheet(kjoretoy, params).getSheet();
//			XSSFSheet newSheet = excel.createSheet(RapportHelper.parseString(kjoretoy.getKjoretoy()), workbook);
//			Util.copySheets(newSheet, sheet);
//			addAutoFilter(newSheet, 13);
//			newSheet.createFreezePane(0, 14);
			
			
//			System.out.println("KjoretoyRapport.Creating sheet:"+kjoretoy.getKjoretoy()+" done...");
			sheetIndex++;
		}
	}
	
	private void initData() {
		Map<Long, List<Rapp1>> listePrKjoretoy = RapportHelper.createListePrKjoretoy(data, params);
		Map<Long, R12Prodtype> prodtypeMap = new HashMap<Long, R12Prodtype>();
		Map<Long, R12Stroprodukt> stroproduktMap = new HashMap<Long, R12Stroprodukt>();
		kjoretoyList = RapportHelper.createForsideData(prodtypeMap, stroproduktMap, listePrKjoretoy, Rapp1Object.class);
		alleProdtyper = RapportHelper.extractAndSortProdtyper(prodtypeMap);
		alleStroprodukter = RapportHelper.extractAndSortStroprodukter(stroproduktMap);
		
		//summer og organiser prodtypene og stroproduktene slik at de ligger i samme rekkefølge for samtlige leverandører
		for (Rapp1Object l:kjoretoyList){
			l.organizeProdtyper(alleProdtyper);
			l.organizeStroprodukt(alleStroprodukter);
		}
		
		map = new HashMap<String, Object>();
		map.put("rapp1Objects", kjoretoyList);
		map.put("prodtyper", alleProdtyper);
		map.put("params", params);
		metadata = new ReportMetadata(kjoretoyList, params);
	}

	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {
		Rapp1Object d= (Rapp1Object)data;
		excel.writeData(dataRow, 0, d.getKjoretoy());
		excel.writeData(dataRow, 1, d.getNavn());
		excel.writeData(dataRow, 2, d.getFraDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 2);
		excel.writeData(dataRow, 3, d.getTilDato());
		excel.setCellStyle(excel.getDatoStyle("dd.mm.yyyy hh:mm", false), dataRow, 3);
		excel.writeData(dataRow, 4, d.getSamprodFlagg());
		excel.writeData(dataRow, 5, d.getKm());
		excel.writeData(dataRow, 6, d.getTid());
		excel.setCellStyle(excel.getDatoStyle("[hh]:mm:ss", false), dataRow, 6);
	}

	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {
		excel.writeData(dataRow, 0, "Sum:");
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 0);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 1);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 2);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 3);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 4);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 5);
		excel.setCellStyle(excel.getTextStyleSum(), dataRow, 6);
		
		addSumCell(5, startRow, endRow, dataRow, excel.getDecimalStyleSum());
		addSumCell(6, startRow, endRow, dataRow, excel.getDatoStyleSum("[hh]:mm:ss"));
	}
}
