package no.mesta.mipss.produksjonsrapport.excel;

import java.util.ArrayList;
import java.util.List;

public class ReportMetadataItem {
	private String header;
	private int column;
	private int colSpan;
	private List<ReportMetadataItem> typer;
	private int seq;
	private List<VALUE_NAMES> valueNames = new ArrayList<VALUE_NAMES>();
	private boolean vNamesOrdered;
	private String commentForStroProdukt;
	private String commentForTonn;
	private String commentForM3;
	
	public enum VALUE_NAMES{
		Km(10, "Km"),
		Timer(20, "Timer"),
		Kmt(30, "Km/t"),
		Prosent(40, "%"),
		Frekvens(50, "Frekvens"),
		M3(60, "M3"),
		Tonn(70, "Tonn"),
		M2(80, "M2");
		
		private int seq;
		private String title;
		
		private VALUE_NAMES(int seq, String title){
			this.seq = seq;
			this.title = title;
		}
		public String getTitle(){
			return title;
		}
		public int getSeq(){
			return seq;
		}
	}
	
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public int getColSpan() {
		if (colSpan==0)
			colSpan =  getsubTypeColSpan()+valueNames.size();
		return colSpan;
	}
	
	private int getsubTypeColSpan(){
		if (typer==null)
			return 0;
		int colSpanSub = 0;
		for (ReportMetadataItem item:typer){
			colSpanSub+= item.getColSpan();
		}
		return colSpanSub;
	}
	
	public int getColumn() {
		return column;
	}
	public void setColumn(int col) {
		this.column = col;
	}
	public List<ReportMetadataItem> getTyper() {
		return typer;
	}
	public void setTyper(List<ReportMetadataItem> typer) {
		this.typer = typer;
	}
	
	public ReportMetadataItem getMetadataForType(String header){
		ReportMetadataItem item = new ReportMetadataItem();
		item.setHeader(header);
		if (typer==null){
			System.out.println();
		}
		return typer.get(typer.indexOf(item));
	}
	public void addValueName(VALUE_NAMES name){
		if (!valueNames.contains(name)){
			valueNames.add(name);
		}
	}
	
	private void orderValueNames(){
		List<VALUE_NAMES> vNames = new ArrayList<VALUE_NAMES>();
		if (valueNames.contains(VALUE_NAMES.Km)){
			vNames.add(VALUE_NAMES.Km);
		}
		if (valueNames.contains(VALUE_NAMES.Timer)){
			vNames.add(VALUE_NAMES.Timer);
		}
		if (valueNames.contains(VALUE_NAMES.M2)){
			vNames.add(VALUE_NAMES.M2);
		}
		if (valueNames.contains(VALUE_NAMES.Kmt)){
			vNames.add(VALUE_NAMES.Kmt);
		}
		if (valueNames.contains(VALUE_NAMES.Prosent)){
			vNames.add(VALUE_NAMES.Prosent);
		}
		if (valueNames.contains(VALUE_NAMES.Frekvens)){
			vNames.add(VALUE_NAMES.Frekvens);
		}
		if (valueNames.contains(VALUE_NAMES.M3)){
			vNames.add(VALUE_NAMES.M3);
		}
		if (valueNames.contains(VALUE_NAMES.Tonn)){
			vNames.add(VALUE_NAMES.Tonn);
		}		
		valueNames = vNames;
	}
	public List<VALUE_NAMES> getValueNames(){
		if (!vNamesOrdered){
			orderValueNames();
			vNamesOrdered = true;
		}
		return valueNames;
	}
	
	public boolean isValuePresent(VALUE_NAMES valueName){
		return valueNames.contains(valueName);
	}
	@Override
	public boolean equals(Object o){
		if (!(o instanceof ReportMetadataItem))
			return false;
		ReportMetadataItem other = (ReportMetadataItem) o;
		return other.header.equals(header);
	}
	@Override
	public int hashCode(){
		return header.hashCode();
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getSeq() {
		return seq;
	}
	public void setComment(String commentForStroProdukt) {
		this.commentForStroProdukt = commentForStroProdukt;
	}
	public String getComment(){
		return commentForStroProdukt;
	}
	public void setCommentForTonn(String commentForTonn){
		this.commentForTonn = commentForTonn;
	}
	public String getCommentForTonn(){
		return commentForTonn;
	}
	public void setCommentForM3(String commentForM3) {
		this.commentForM3 = commentForM3;
	}
	public String getCommentForM3() {
		return commentForM3;
	}
}
