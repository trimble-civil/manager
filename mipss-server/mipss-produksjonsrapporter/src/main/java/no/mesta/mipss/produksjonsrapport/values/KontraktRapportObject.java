package no.mesta.mipss.produksjonsrapport.values;

import java.awt.Polygon;
import java.io.IOException;
import java.io.StringReader;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Stroprodukt;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------


public class KontraktRapportObject extends EntityCopy {
    protected String aar;
    protected String dag;
    protected String datoEnhet1;
    protected String datoEnhet2;
    protected String enhetsID;
    protected String kjoretoyID;
    protected String leverandorNavn;
    protected String mnd;
    protected String rodeID;
    protected String rodeLengde;
    protected String rodeNavn;
    protected String tidStart;
    protected String tidStopp;
    protected Hashtable tiltakListe;
    protected String tiltakXML;
    protected double tomkjoringKM;
    protected double totaltKjortKM;
    protected String totaltKjortTid;
    protected String uke;
    protected boolean paaKontrakt=true;
    protected String leverandorer;
    private String samprod;
    
    private List<R12Prodtype> prodtyper;
	private List<R12Stroprodukt> stroprodukter;
    
    public KontraktRapportObject() {
        super(0);
        tiltakListe = new Hashtable();
    }

    public String[] getTiltakKeys() {
        Enumeration enumeration = tiltakListe.keys();
        String[] keyList = new String[tiltakListe.size()];
        int i = 0;

        while (enumeration.hasMoreElements()) {
            keyList[i++] = (String)enumeration.nextElement();
        }

        return keyList;
    }

    public TiltakSequence getTiltakListe() {
        TiltakSequence seq = new TiltakSequence();

        seq.addAll(tiltakListe.values());

        return seq;
    }
    


    /** 
     * inneholder subtiltaksliste indeksert på tiltak
     */   

    /** 
     * Lag tiltaksliste fra XML. 
     * 
     * Eksempel på XML:
     *<?xml version="1.0"?>
     * <tiltak>
     *  <type>
     *   <tt_id>1</tt_id>
     *   <tt_seq>95</tt_seq>
     *   <tt_navn>Strøing</tt_navn>
     *   <kjort>2.043</kjort>
     *   <tid>00;01;42</tid>
     *
     *   <subtype>
     *    <sp_id>5</sp_id>
     *    <sp_seq>125</sp_seq>
     *    <sp_navn>Saltløsning</sp_navn>
     *    <kjort>1.435</kjort>
     *    <tid>00;00;49</tid>
     *   </subtype>
     *
     *   <subtype>
     *    <sp_id>6</sp_id>
     *    <sp_seq>120</sp_seq>
     *    <sp_navn>Befuktet salt</sp_navn>
     *    <kjort>0.608</kjort>
     *    <tid>00;00;52</tid>
     *   </subtype>
     *  </type>
     *
     *  <type>
     *   <tt_id>98</tt_id>
     *   <tt_seq>20</tt_seq>
     *   <tt_navn>Tomkjøring</tt_navn>
     *   <kjort>0.159</kjort>
     *   <tid>00;00;07</tid>
     *  </type>
     *  
     *  <forbruk>
     *   <fb_id>9999</fb_id>
     *   <fb_seq>9999</fb_seq>
     *   <fb_navn>Utlagte mengder</fb_navn>
     *   
     *   <materiale>
     *    <matr_id>2</matr_id>
     *    <matr_seq>1020</matr_seq>
     *    <matr_navn>NaCl</matr_navn>
     *    <mengde>3.485</mengde>
     *    <enhet>Tonn</enhet>
     *   </materiale>
     *   
     *   <materiale>
     *    <matr_id>99</matr_id>
     *    <matr_seq>1099</matr_seq>
     *    <matr_navn>Ukjent</matr_navn>
     *    <mengde>57.116</mengde>
     *    <enhet>Tonn</enhet>
     *   </materiale>
     *  </forbruk>
     * </tiltak>
     */
    public void createTiltakFromXML() {
        try {
            long start = System.currentTimeMillis();
            
            if (tiltakXML == null) {
                return;
            }
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(tiltakXML)));

            document.normalize();

            // tiltak node
            Element rootElement = document.getDocumentElement();
            NodeList listeType = rootElement.getElementsByTagName("type");
            createTiltakProduksjon(listeType);
            // tiltaktype materiale
            NodeList listeMengde = rootElement.getElementsByTagName("forbruk");
            createTiltakMengde(listeMengde);
            
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        } catch (ParserConfigurationException pcf) {
            pcf.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Lag tiltak, kall denne metoden dersom tiltakstype er produksjon. Tiltakene legges til i tiltakslisten
     * @param listeType innholder alle <type> elementene
     */
    private void createTiltakProduksjon(NodeList listeType){
        for (int i = 0; i < listeType.getLength(); i++) {
            Node typeNode = listeType.item(i);
            NodeList typeList = typeNode.getChildNodes();
            TiltakProduksjon tiltakProduksjon = new TiltakProduksjon();
            
            for (int j = 0; j < typeList.getLength(); j++) {
                Node node = typeList.item(j);
                String navn = node.getNodeName();
                
                if (navn.equals("tt_id")) {
                    tiltakProduksjon.setID(Integer.parseInt(node.getFirstChild().getNodeValue()));
                } else if (navn.equals("tt_seq")) {
                    tiltakProduksjon.setSortering(Integer.parseInt(node.getFirstChild().getNodeValue()));
                } else if (navn.equals("tt_navn")) {
                    tiltakProduksjon.setNavn(node.getFirstChild().getNodeValue());
                } else if (navn.equals("kjort")) {
                    tiltakProduksjon.setKm(Double.parseDouble(node.getFirstChild().getNodeValue().replace(',', '.')));
                } else if (navn.equals("tid")) {
                    tiltakProduksjon.setTid(node.getFirstChild().getNodeValue());
                } else if (navn.equals("subtype")){
                    tiltakProduksjon.addSubtype(createTiltakSubtype(node.getChildNodes()), true);
                }
            }
            addTiltak(tiltakProduksjon);
        }
    }
    private void createTiltakProduksjon(){
    	for (R12Prodtype p:getProdtyper()){
    		TiltakProduksjon tp = new TiltakProduksjon();
    		tp.setID(p.getId().intValue());
    		tp.setSortering(p.getSeq().intValue());
    		tp.setNavn(p.getHeader());
    		tp.setKm(p.getKjortKm());
    		tp.setTid(String.valueOf(p.getKjortSekund()));//TODO fikse denne. formatet skal vel være 00:00:00??
    		
    		addTiltak(tp);
    	}
    }
    /**
     * Lag et tiltakProduksjons objekt som en subtype av 'type'. 
     * <type>
     * <tt_id>1</tt_id>
     * <tt_seq>95</tt_seq>
     * <tt_navn>Strøing</tt_navn>
     * <kjort>2.043</kjort>
     * <tid>00;01;42</tid>
     * 
     * <subtype>
     *   <sp_id>5</sp_id>
     *   <sp_seq>125</sp_seq>
     *   <sp_navn>Saltløsning</sp_navn>
     *   <kjort>1.435</kjort>
     *   <tid>00;00;49</tid>
     * </subtype>
     * </type>
     * @param subtype innholder et <subtype> element
     * @return
     */
    private TiltakProduksjon createTiltakSubtype(NodeList subtype){
        
       TiltakProduksjon subtiltak = new TiltakProduksjon(); 
       for (int j=0;j<subtype.getLength();j++){
            Node node = subtype.item(j);
            String navn = node.getNodeName();
            if(navn.equals("sp_id")){
                subtiltak.setID(Integer.parseInt(node.getFirstChild().getNodeValue()));
            } else if (navn.equals("sp_seq")){
                subtiltak.setSortering(Integer.parseInt(node.getFirstChild().getNodeValue()));
            } else if (navn.equals("sp_navn")){
                subtiltak.setNavn(node.getFirstChild().getNodeValue());
            } else if (navn.equals("kjort")){
                subtiltak.setKm(Double.parseDouble(node.getFirstChild().getNodeValue().replace(',','.')));
            } else if (navn.equals("tid")){
                subtiltak.setTid(node.getFirstChild().getNodeValue());
            }
        }
        return subtiltak;
    }
    
    /**
     * Lag tiltak, kall denne metoden dersom tiltakstype er mengde. Tiltakene legges til i tiltakslisten
     * 
     * <forbruk>
     *   <fb_id>9999</fb_id>
     *   <fb_seq>9999</fb_seq>
     *   <fb_navn>Utlagte mengder</fb_navn>
     *   
     *   <materiale>
     *    <matr_id>2</matr_id>
     *    <matr_seq>1020</matr_seq>
     *    <matr_navn>NaCl</matr_navn>
     *    <mengde>3.485</mengde>
     *    <enhet>Tonn</enhet>
     *   </materiale>
     *   
     *   <materiale>
     *    <matr_id>99</matr_id>
     *    <matr_seq>1099</matr_seq>
     *    <matr_navn>Ukjent</matr_navn>
     *    <mengde>57.116</mengde>
     *    <enhet>Tonn</enhet>
     *   </materiale>
     *  </forbruk>
     *  @param listeMengde på nivået &gt;forbruk&lt; i XML. Normalt vil det kun finnes en av disse pr XML, men metoden støtter flere
     */
    private void createTiltakMengde(NodeList listeMengde){
        
        for (int i = 0; i < listeMengde.getLength(); i++) {
            Node forbrukNode = listeMengde.item(i);
            NodeList typeList = forbrukNode.getChildNodes();
            TiltakMengde tiltakMengde = new TiltakMengde();
            
            for (int j=0; j<typeList.getLength(); j++){
                Node node = typeList.item(j);
                String navn = node.getNodeName();
                if (navn.equals("fb_id")){
                    tiltakMengde.setID(Integer.parseInt(node.getFirstChild().getNodeValue()));
                }else if (navn.equals("fb_seq")){
                    tiltakMengde.setSortering(Integer.parseInt(node.getFirstChild().getNodeValue()));
                }else if (navn.equals("fb_navn")){
                    tiltakMengde.setNavn(node.getFirstChild().getNodeValue());
                }else if (navn.equals("materiale")){
                    tiltakMengde.addSubtype(createMateriale(node.getChildNodes()), false);
                }
            }
            addTiltak(tiltakMengde);
        }
    }
    
    /**
     * Lag et tiltak der typen skal være materiale.
     * 
     * <materiale>
     *    <matr_id>99</matr_id>
     *    <matr_seq>1099</matr_seq>
     *    <matr_navn>Ukjent</matr_navn>
     *    <mengde>57.116</mengde>
     *    <enhet>Tonn</enhet>
     * </materiale>
     * 
     * @param listeMateriale er på nivå <materiale> i XML. 
     */
    private TiltakMengde createMateriale(NodeList listeMateriale){
        TiltakMengde tiltakMengde = new TiltakMengde();
        for (int j = 0; j < listeMateriale.getLength(); j++) {
            Node node = listeMateriale.item(j);
            String navn = node.getNodeName();
            if (navn.equals("matr_id")) {
                tiltakMengde.setID(Integer.parseInt(node.getFirstChild().getNodeValue()));
            } else if (navn.equals("matr_seq")) {
                
                tiltakMengde.setSortering(Integer.parseInt(node.getFirstChild().getNodeValue()));
            } else if (navn.equals("matr_navn")) {
                tiltakMengde.setNavn(node.getFirstChild().getNodeValue());
            } else if (navn.equals("mengde")) {
                tiltakMengde.setMengde(Double.parseDouble(node.getFirstChild().getNodeValue()));
            } else if (navn.equals("enhet")) {
                tiltakMengde.setEnhet(node.getFirstChild().getNodeValue());
            }
        }
        return tiltakMengde;
    }
    
    public void addTiltak(TiltakObject object) {
        tiltakListe.put(object.getNavn(), object);
    }

    public TiltakObject getTiltak(String tiltakKey) {
        return (TiltakObject)tiltakListe.get(tiltakKey);
    }

    public void setEnhetsID(String enhetsID) {
        this.enhetsID = enhetsID;
    }

    public String getEnhetsID() {
        return enhetsID;
    }

    public void setTidStart(String tidStart) {
        this.tidStart = tidStart;
    }

    public String getTidStart() {
        return tidStart;
    }

    public void setTidStopp(String tidStopp) {
        this.tidStopp = tidStopp;
    }

    public String getTidStopp() {
        return tidStopp;
    }

    public void setTotaltKjortKM(Double totaltKjortKM) {
        this.totaltKjortKM = totaltKjortKM;
    }

    public double getTotaltKjortKM() {
        return totaltKjortKM;
    }

    public void setTotaltKjortTid(String totaltKjortTid) {
        this.totaltKjortTid = totaltKjortTid;
    }

    public String getTotaltKjortTid() {
        return totaltKjortTid;
    }

    public void setTomkjoringKM(double tomkjoringKM) {
        this.tomkjoringKM = tomkjoringKM;
    }

    public double getTomkjoringKM() {
        return tomkjoringKM;
    }

    public void setTiltakXML(String tiltakXML) {
        this.tiltakXML = tiltakXML;
    }

    public String getTiltakXML() {
        return tiltakXML;
    }

    public void setLeverandorNavn(String leverandorNavn) {
        this.leverandorNavn = leverandorNavn;
    }

    public String getLeverandorNavn() {
        return leverandorNavn;
    }

    public void setRodeNavn(String rodeNavn) {
        this.rodeNavn = rodeNavn;
    }

    public String getRodeNavn() {
        return rodeNavn;
    }

    public void setKjoretoyID(String kjoretoyID) {
        this.kjoretoyID = kjoretoyID;
    }

    public String getKjoretoyID() {
        return kjoretoyID;
    }

    public void setRodeLengde(String rodeLengde) {
        this.rodeLengde = rodeLengde;
    }

    public String getRodeLengde() {
        return rodeLengde;
    }

    public void setRodeID(String rodeID) {
        this.rodeID = rodeID;
    }

    public String getRodeID() {
        return rodeID;
    }

    public void setDatoEnhet1(String datoEnhet1) {
        this.datoEnhet1 = datoEnhet1;
    }

    public String getDatoEnhet1() {
        return datoEnhet1;
    }

    public void setDatoEnhet2(String datoEnhet2) {
        this.datoEnhet2 = datoEnhet2;
    }

    public String getDatoEnhet2() {
        return datoEnhet2;
    }

    public void setDag(String dag) {
        this.dag = dag;
    }

    public String getDag() {
        return dag;
    }

    public void setUke(String uke) {
        this.uke = uke;
    }

    public String getUke() {
        return uke;
    }

    public void setMnd(String mnd) {
        this.mnd = mnd;
    }

    public String getMnd() {
        return mnd;
    }

    public void setAar(String aar) {
        this.aar = aar;
    }

    public String getAar() {
        return aar;
    }

/*    public static void main(String[] args) {
        KontraktRapportObject object = new KontraktRapportObject();

        object.createTiltakFromXML();
    }
    */

    public void setErPaaKontrakt(String value){
    	boolean v=value.equals("1")?true:false;
    	setPaaKontrakt(v);
    }
    public void setPaaKontrakt(boolean paaKontrakt) {
        this.paaKontrakt = paaKontrakt;
    }

    public boolean isPaaKontrakt() {
        return paaKontrakt;
    }
    public void setLeverandorer(String leverandorer){
    	this.leverandorer = leverandorer;
    }
    public String getLeverandorer(){
    	return leverandorer;
    }

	public void setSamprod(String samprod) {
		this.samprod = samprod;
	}

	public String getSamprod() {
		return samprod;
	}
	
	/** TODO starten på refactor av rapporter */
	public List<R12Prodtype> getProdtyper(){
		return prodtyper;
	}
	
	public void setProdtyper(List<R12Prodtype> prodtyper){
		this.prodtyper = prodtyper;
	}

	public void setStroprodukter(List<R12Stroprodukt> stroprodukter) {
		this.stroprodukter = stroprodukter;
	}

	public List<R12Stroprodukt> getStroprodukter() {
		return stroprodukter;
	}
}


