package no.mesta.mipss.produksjonsrapport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

import org.apache.commons.lang.builder.ToStringBuilder;


public class ProdrapportParams implements Serializable{

	public enum RapportType{
		KJORETOY, 
		LEVERANDOR, 
		PERIODE, 
		RODE,
		DAU, 
		VEI,
		VINTERDRIFT,
		SAMPRODUKSJON, 
		R12,
		SAMMENDRAG,
		PASSERING,
		UKJENT_STROPRODUKT
	}
	
	private Long driftkontraktId; 
	private Long rodetypeId; 
	private Long rodeId;
	private Date fraDato; 
	private Date tilDato;
	private String rapportType;
	private boolean kunKontraktKjoretoy;
	private boolean gjennomsnitt;
	private boolean prosent;
	private boolean frekvens;
	
	/** for rodeoversiktrapporten */
	private boolean alleInkAvsl;
	
	/** for sammendragsrapporten */
	private boolean sumMnd;
	
	private String kontraktNavn;
	private String rodetypeNavn;
	private String rodeNavn;
	private String periode; 
	
	private RapportType type;
	
	private Long kjoretoyId;
	private String brukerSign;
	private List<Veinettveireferanse> veiListe;
	private Boolean inkluderKmForVeiliste = Boolean.FALSE;
	
	/**for passeringsrapporten*/
	
	private Long fylkesnummer;
	private Long kommunenummer;
	private String veikategori;
	private String veistatus;
	private Long veinummer;
	private Long hp;
	private Double km;
	private String fullVeiRef;
	
	public ProdrapportParams(){
		
	}
	public String getVeirefstring(){
		StringBuilder sb = new StringBuilder();
		if (veiListe==null||veiListe.size()==0){
			sb.append("Alle veier");
		}else{
			for (Veinettveireferanse v:veiListe){
				String vei = v.getFylkesnummer()+"/"+v.getKommunenummer()+" "+v.getVeikategori()+v.getVeistatus()+v.getVeinummer()+" hp "+v.getHp();
				if (v.getFraKm()!=null&&v.getTilKm()!=null){
					vei+=" m "+v.getFraKm()+"-"+v.getTilKm();
				}
				sb.append(vei+", ");
			}
		}
		return sb.toString();
	}
	public Long getDriftkontraktId() {
		return driftkontraktId;
	}

	public void setDriftkontraktId(Long driftkontraktId) {
		this.driftkontraktId = driftkontraktId;
	}

	public Long getRodetypeId() {
		return rodetypeId;
	}

	public void setRodetypeId(Long rodetypeId) {
		this.rodetypeId = rodetypeId;
	}

	public Long getRodeId() {
		return rodeId;
	}

	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}

	public Date getFraDato() {
		return fraDato;
	}

	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}

	public Date getTilDato() {
		return tilDato;
	}

	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	
	public boolean getKunKontraktKjoretoy(){
		return kunKontraktKjoretoy;
	}
	public void setKunKontraktKjoretoy(boolean kunKontraktKjoretoy){
		this.kunKontraktKjoretoy = kunKontraktKjoretoy;
	}

	public boolean isGjennomsnitt() {
		return gjennomsnitt;
	}

	public void setGjennomsnitt(boolean gjennomsnitt) {
		this.gjennomsnitt = gjennomsnitt;
	}

	public boolean isProsent() {
		return prosent;
	}

	public void setProsent(boolean prosent) {
		this.prosent = prosent;
	}

	public void setFrekvens(boolean frekvens) {
		this.frekvens = frekvens;
	}

	public boolean isFrekvens() {
		return frekvens;
	}

	public void setRapportType(String rapportType) {
		this.rapportType = rapportType;
	}

	public String getRapportType() {
		return rapportType;
	}
	
	/**
	 * OBS Legges til i EJB som parametere til "gamle" rapporter.. 
	 * Dette er ikke en klient parameter.
	 * @param
	 * @return
	 */
	public void setKontraktNavn(String kontraktNavn){
		this.kontraktNavn = kontraktNavn;
	}
	/**
	 * OBS Legges til i EJB som parametere til "gamle" rapporter.. 
	 * Dette er ikke en klient parameter.
	 * @param
	 * @return
	 */
	public void setRodetypeNavn(String rodetypeNavn){
		this.rodetypeNavn = rodetypeNavn;
	}
	/**
	 * OBS Legges til i EJB som parametere til "gamle" rapporter.. 
	 * Dette er ikke en klient parameter.
	 * @param
	 * @return
	 */
	public void setRodeNavn(String rodeNavn){
		this.rodeNavn = rodeNavn;
	}
	/**
	 * OBS Legges til i EJB som parametere til "gamle" rapporter.. 
	 * Dette er ikke en klient parameter.
	 * @param
	 * @return
	 */
	public void setPeriode(String periode){
		this.periode = periode;
	}
	
	public String getKontraktNavn() {
		return kontraktNavn;
	}

	public String getRodetypeNavn() {
		return rodetypeNavn;
	}

	public String getRodeNavn() {
		return rodeNavn;
	}

	public String getPeriode() {
		return periode;
	}
	
	public void setType(RapportType type) {
		this.type = type;
	}

	public RapportType getType() {
		return type;
	}

	public String toString(){
		return new ToStringBuilder(this)
			.append("driftkontraktId", getDriftkontraktId())
			.append("rodetypeId", getRodetypeId())
			.append("rodeId", getRodeId())
			.append("fraDato", getFraDato())
			.append("tilDato", getTilDato())
			.append("isGjennomsnitt", isGjennomsnitt())
			.append("isProsent", isProsent())
			.toString();
	}

	public void setAlleInkAvsl(boolean alleInkAvsl) {
		this.alleInkAvsl = alleInkAvsl;
	}

	public boolean isAlleInkAvsl() {
		return alleInkAvsl;
	}

	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}

	public Long getKjoretoyId() {
		return kjoretoyId;
	}

	public void setBrukerSign(String brukerSign) {
		this.brukerSign = brukerSign;
	}

	public String getBrukerSign() {
		return brukerSign;
	}

	public void setVeiliste(List<Veinettveireferanse> veiListe) {
		this.veiListe = veiListe;
	}
	public List<Veinettveireferanse> getVeiListe(){
		return veiListe;
	}

	public void setInkluderKmForVeiliste(Boolean inkluderKmForVeiliste) {
		this.inkluderKmForVeiliste = inkluderKmForVeiliste;
	}
	public Boolean getInkluderKmForVeiliste(){
		return inkluderKmForVeiliste;
	}
	public Date getNow(){
		return Clock.now();
	}
	public void setSumMnd(boolean sumMnd) {
		this.sumMnd = sumMnd;
	}
	public boolean isSumMnd() {
		return sumMnd;
	}
	public Long getFylkesnummer() {
		return fylkesnummer;
	}
	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}
	public Long getKommunenummer() {
		return kommunenummer;
	}
	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}
	public String getVeikategori() {
		return veikategori;
	}
	public void setVeikategori(String veikategori) {
		this.veikategori = veikategori;
	}
	public String getVeistatus() {
		return veistatus;
	}
	public void setVeistatus(String veistatus) {
		this.veistatus = veistatus;
	}
	public Long getVeinummer() {
		return veinummer;
	}
	public void setVeinummer(Long veinummer) {
		this.veinummer = veinummer;
	}
	public Long getHp() {
		return hp;
	}
	public void setHp(Long hp) {
		this.hp = hp;
	}
	public Double getKm() {
		return km;
	}
	public void setKm(Double km) {
		this.km = km;
	}
	
	public String getFullVeiRef(){
		Double meter = getKm() * 1000;
		return getFylkesnummer() + "/" + getKommunenummer() + " " + getVeikategori() + getVeistatus() + getVeinummer() + " HP" + getHp() + " m:" + meter.intValue();
	}
}
