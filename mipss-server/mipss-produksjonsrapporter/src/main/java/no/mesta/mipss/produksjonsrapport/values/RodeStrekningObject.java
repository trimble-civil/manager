package no.mesta.mipss.produksjonsrapport.values;

/**
 * MIPSS1 port..
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RodeStrekningObject extends EntityCopy {
    private String fylkesNR;
    private String fylkesNavn;
    private String hparsell;
    private String kmFra;
    private String kmTil;
    private String kommuneNR;
    private String kontraktNR;
    private String kontraktNavn;
    private String rodeDatoGyldigFra;
    private String rodeDatoGyldigTil;
    private String rodeID;
    private String rodeLengde;
    private String rodeNavn;
    private String rodeStrekningID;
    private String veiFunksjon;
    private String veiKategori;
    private String veiNR;
    private String veiStatus;
    private String rodeType;
    private String beskrivelse;
    private String leverandorer;
    
    public RodeStrekningObject() {
        super(0);
    }

    public void setKontraktNR(String kontraktNR) {
        this.kontraktNR = kontraktNR;
    }

    public String getKontraktNR() {
        return kontraktNR;
    }

    public void setKontraktNavn(String kontraktNavn) {
        this.kontraktNavn = kontraktNavn;
    }

    public String getKontraktNavn() {
        return kontraktNavn;
    }

    public void setRodeID(String rodeID) {
        this.rodeID = rodeID;
    }

    public String getRodeID() {
        return rodeID;
    }

    public void setRodeNavn(String rodeNavn) {
        this.rodeNavn = rodeNavn;
    }

    public String getRodeNavn() {
        return rodeNavn;
    }

    public void setRodeLengde(String rodeLengde) {
        this.rodeLengde = rodeLengde;
    }

    public String getRodeLengde() {
        return rodeLengde;
    }

    public void setRodeDatoGyldigFra(String rodeDatoGyldigFra) {
        this.rodeDatoGyldigFra = rodeDatoGyldigFra;
    }

    public String getRodeDatoGyldigFra() {
        return rodeDatoGyldigFra;
    }

    public void setRodeDatoGyldigTil(String rodeDatoGyldigTil) {
        this.rodeDatoGyldigTil = rodeDatoGyldigTil;
    }

    public String getRodeDatoGyldigTil() {
        return rodeDatoGyldigTil;
    }

    public void setRodeStrekningID(String rodeStrekningID) {
        this.rodeStrekningID = rodeStrekningID;
    }

    public String getRodeStrekningID() {
        return rodeStrekningID;
    }

    public void setVeiKategori(String veiKategori) {
        this.veiKategori = veiKategori;
    }

    public String getVeiKategori() {
        return veiKategori;
    }

    public void setVeiStatus(String veiStatus) {
        this.veiStatus = veiStatus;
    }

    public String getVeiStatus() {
        return veiStatus;
    }

    public void setVeiNR(String veiNR) {
        this.veiNR = veiNR;
    }

    public String getVeiNR() {
        return veiNR;
    }

    public void setHparsell(String hparsell) {
        this.hparsell = hparsell;
    }

    public String getHparsell() {
        return hparsell;
    }

    public void setKommuneNR(String kommuneNR) {
        this.kommuneNR = kommuneNR;
    }

    public String getKommuneNR() {
        return kommuneNR;
    }

    public void setFylkesNR(String fylkesNR) {
        this.fylkesNR = fylkesNR;
    }

    public String getFylkesNR() {
        return fylkesNR;
    }

    public void setFylkesNavn(String fylkesNavn) {
        this.fylkesNavn = fylkesNavn;
    }

    public String getFylkesNavn() {
        return fylkesNavn;
    }

    public void setVeiFunksjon(String veiFunksjon) {
        this.veiFunksjon = veiFunksjon;
    }

    public String getVeiFunksjon() {
        return veiFunksjon;
    }

    public void setKmFra(String kmFra) {
        this.kmFra = kmFra;
    }

    public String getKmFra() {
        return kmFra;
    }

    public void setKmTil(String kmTil) {
        this.kmTil = kmTil;
    }

    public String getKmTil() {
        return kmTil;
    }

    public void setRodeType(String rodeType) {
        this.rodeType = rodeType;
    }

    public String getRodeType() {
        return rodeType;
    }

    public void setBeskrivelse(String beskrivelse) {
        this.beskrivelse = beskrivelse;
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }
    
    public void setLeverandorer(String leverandorer){
    	this.leverandorer = leverandorer;
    }
    public String getLeverandorer(){
    	return leverandorer;
    }
}


