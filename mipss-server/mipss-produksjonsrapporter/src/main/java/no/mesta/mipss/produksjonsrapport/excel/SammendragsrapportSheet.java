package no.mesta.mipss.produksjonsrapport.excel;

import java.util.HashMap;
import java.util.Map;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Entity;
import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1Object;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SammendragsrapportSheet extends Rapp1Rapport{

	private final Rapp1Object vei;
	private String templateFile = "sammendragsrapport_sheet";
	int startRow = 6;
	int startCol = 4;
	private Map<String, Object> dataMap;
	
	public SammendragsrapportSheet(Rapp1Object vei, ProdrapportParams params, ExcelPOIHelper excel, XSSFWorkbook workbook, int sheetIndex){
		super(params);
		this.vei = vei;
		initData();
		
		this.sheetIndex = sheetIndex;
		
		this.excel = excel;
		this.workbook = workbook;
		this.file = RapportHelper.createTempFile(templateFile);
		this.freezePaneRow=11;
		this.freezePaneCol=0;
		
		
		copyIntoMainWorkbook(vei.getVei(), templateFile, dataMap);
		
		int row = writeHeaders(startRow, startCol);
		int endDataRow = writeData(row+3, vei.getData(), startCol);
		writeSum(endDataRow, endDataRow-1, startCol);
		writeSum(row+1, endDataRow-1, startCol);
		createBackLink(5,0);
		addAutoFilter(workbook.getSheetAt(sheetIndex), 10);
	}
	public XSSFSheet getSheet(){
		return workbook.getSheetAt(0);
	}
	private void initData() {
		metadata = new ReportMetadata(vei.getData(), params);
		dataMap = new HashMap<String,Object>();
		dataMap.put("data", vei.getData());
		dataMap.put("rapp1Object", vei);
		dataMap.put("params", params);
	}
	@Override
	protected void writeCustomData(XSSFRow dataRow, Rapp1Entity data) {	
		if (params.isSumMnd())
			excel.setCellStyle(excel.getDatoStyle("yyyy mmmm", false), dataRow, 1);
		
	}
	@Override
	protected void writeCustomSum(XSSFRow dataRow, int startRow, int endRow) {}
}
