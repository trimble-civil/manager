package no.mesta.mipss.produksjonsrapport.query;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.persistence.rapportfunksjoner.Materiale;
import no.mesta.mipss.persistence.rapportfunksjoner.R12Prodtype;
import no.mesta.mipss.persistence.rapportfunksjoner.Veirapp;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.TiltakMengde;
import no.mesta.mipss.produksjonsrapport.values.TiltakProduksjon;
import no.mesta.mipss.produksjonsrapport.values.VeiTiltakObject;
import no.mesta.mipss.produksjonsrapport.values.VeirapportObject;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NestedTableDescriptor;
import no.mesta.mipss.query.StructHelper;

public class VeirapportQuery {
	private final ProdrapportParams params;
	private final EntityManager em;

	public VeirapportQuery(ProdrapportParams params, EntityManager em){
		this.params = params;
		this.em = em;
	}
	
	public List<Veirapp> consumeResults() throws SQLException{
        StructHelper structHelper = new StructHelper();

        Array array = structHelper.createVeireferanseArray(params.getVeiListe(), params.getInkluderKmForVeiliste());

		structHelper.goodnight();
		
		StringBuilder q = new StringBuilder("select veikategori, veinummer, kjoretoy_id, kjoretoy_navn, dogn, totalt_kjort_km, totalt_kjort_sekund, ");
		q.append("prodtyper, materialer, paa_kontrakt_flagg from table(rapport_funksjoner.veirapp(?1, ?2, ?3, ?4, ?5, ?6)) ");
		if (params.getKunKontraktKjoretoy()){
        	q.append("where paa_kontrakt_flagg=1 ");
        }
		q.append("order BY  dogn, veinummer, kjoretoy_navn");
		
		//185, 18, null, to_date('01.01.2009', 'dd.mm.yyyy'), to_date('31.01.2009', 'dd.mm.yyyy'))
		Query query = em.createNativeQuery(q.toString());
		query.setParameter(1, params.getDriftkontraktId());
		query.setParameter(2, params.getRodetypeId());
		query.setParameter(3, params.getRodeId());
		query.setParameter(4, array);
		query.setParameter(5, params.getFraDato());
		query.setParameter(6, params.getTilDato());
		
		
		NestedTableDescriptor<R12Prodtype> prodtype = new NestedTableDescriptor<R12Prodtype>(R12Prodtype.class, "id", "header", "seq", "stroFlagg", "kjortKm", "kjortSekund", "mengdeM2");
		NestedTableDescriptor<Materiale> materiale = new NestedTableDescriptor<Materiale>(Materiale.class, "id", "header", "seq", "tonn");
		NativeQueryWrapper<Veirapp> wrapper = new NativeQueryWrapper<Veirapp>(query, Veirapp.class, "veikategori", "veinummer", "kjoretoyId", "kjoretoyNavn", "dogn", "totalKjortKm","totalKjortSekund", "prodtyper", "materialer", "paaKontraktFlagg");
		wrapper.addNestedTableDescriptor(prodtype);
		wrapper.addNestedTableDescriptor(materiale);
		
		List<Veirapp> wrappedList = wrapper.getWrappedList();
		return wrappedList;
//		List<VeirapportObject> veirapp = new ArrayList<VeirapportObject>();
//		for (Veirapp v:wrappedList){
//			VeirapportObject o = new VeirapportObject();
//			o.setDato(v.getDogn());
//			o.setRegnr(v.getKjoretoyNavn());
//			o.setVeinummer(v.getVeinummer().intValue());
//			o.setVeistatus(v.getVeikategori());
//			o.setTotalKjort(v.getTotalKjortKm());
//			o.setSekunderKjort(v.getTotalKjortSekund().intValue());
//			List<R12Prodtype> prodtyper = v.getProdtyper();
//			for (R12Prodtype p:prodtyper){
//				//TODO nuke
//				VeiTiltakObject vt = new VeiTiltakObject();
//				vt.setProdtype(p.getHeader());
//				vt.setSekunderKjort(p.getKjortSekund().intValue());
//				vt.setTotalKjort(p.getKjortKm());
//				vt.setSorteringProdtype(p.getSeq().intValue());
//				o.addTiltak(vt);
//				// end nuke
//				
//				TiltakProduksjon tp = new TiltakProduksjon();
//				tp.setSortering(vt.getSorteringProdtype());
//				tp.setKm(vt.getTotalKjort());
//				tp.setSekunder(vt.getSekunderKjort());
//				tp.setNavn(vt.getProdtype());
//				if (!o.getProdList().contains(p)){
//					o.getProdList().add(tp);
//				}
//			}
//			for (Materiale m:v.getMaterialer()){
//				//TODO nuke
//				VeiTiltakObject vt = new VeiTiltakObject();
//				vt.setMateriale(m.getHeader());
//				vt.setStromengde(m.getTonn());
//				vt.setSorteringMateriale(m.getSeq().intValue());
//				o.addTiltak(vt);
//				// end nuke				
//				TiltakMengde tm = new TiltakMengde();
//				tm.setSortering(vt.getSorteringMateriale());
//				tm.setEnhet("Tonn");
//				tm.setNavn(vt.getMateriale());
//				tm.setMengde(vt.getStromengde());
//				o.getMengdeList().add(tm);
//			}
//			veirapp.add(o);
//			
//		}
//		return veirapp;
	}
	
	private Object[] convertVeinettveireferanseToOjbect(Veinettveireferanse vr, boolean inkluderKm){
		Object[] ar = new Object[8];
		ar[0]=vr.getFylkesnummer();
		ar[1]=vr.getKommunenummer();
		ar[2]=vr.getVeikategori();
		ar[3]=vr.getVeistatus();
		ar[4]=vr.getVeinummer();
		ar[5]=vr.getHp();
		if (inkluderKm){
			ar[6]=vr.getFraKm();
			ar[7]=vr.getTilKm();
		}else{
			ar[6]=null;
			ar[7]=null;
		}
		return ar;
	}
}
