package no.mesta.mipss.produksjonsrapport.values;

import java.util.ArrayList;
import java.util.Hashtable;

//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------


public class KontraktRapportSequence extends IndexedSequence {
    protected TiltakSequence globalTiltakList;
    protected Hashtable table;

    public KontraktRapportSequence(final int initialCapacity) {
        super(initialCapacity);
        table = new Hashtable();
    }

    public KontraktRapportSequence(TiltakSequence globalTiltakList) {
        super();
        table = new Hashtable();
        this.globalTiltakList = globalTiltakList;
    }

    public final void add(final KontraktRapportObject object) {
        append(object);
    }
    
    /**
     * @param object
     * @param key
     */
    public void updateTiltakTypeListForEnhet(KontraktRapportObject object, String key) throws Exception{
        TiltakSequence seq = object.getTiltakListe();
        for (int i = 0; i < seq.size(); i++) {
            TiltakObject obj = (TiltakObject)seq.getAt(i);
            addTiltakTypeForEnhet(/*enhetsID*/key, obj);
            updateSubTiltakTypeListForTiltak(obj, obj.getNavn());
        }
    }
    /**************************************************************/
    private Hashtable subtable = new Hashtable();
    public void updateSubTiltakTypeListForTiltak(TiltakObject obj, String key){
        TiltakSequence seq = obj.getTiltakListe();
        if (seq!=null){
            for (int i=0;i<seq.size();i++){
                TiltakObject sub = (TiltakObject)seq.getAt(i);
                
                addSubTiltakTypeForTiltak(key, sub);
            }
        }
    }
    
    private void addSubTiltakTypeForTiltak(String key, TiltakObject value) {
        //System.out.println("addsub:"+key+" value:"+value.getNavn());
        if (!subtable.containsKey(key)) {
            TiltakSequence list = new TiltakSequence();
            list.add(value);
            subtable.put(key, list);
        } else {
            TiltakSequence list = (TiltakSequence)subtable.get(key);
            if (!valueExistsForEnhet(list, value)) {
                list.add(value);
                subtable.put(key, list);
            }
        }
    }
    public TiltakSequence getSubTiltakTypeList(String tiltak) {
        TiltakSequence tiltakSequence = (TiltakSequence)subtable.get(tiltak);
        tiltakSequence.sort(new TiltakComparator());
        return tiltakSequence;
    }
    /*************************************************************/
    
    public final KontraktRapportObject elementAt(final int index) {
        return (KontraktRapportObject)getAt(index);
    }
    private void addTiltakTypeForEnhet(String key, TiltakObject value)  throws Exception{
        //print informasjon om value
       /* if (value instanceof TiltakProduksjon){
            if (((TiltakProduksjon)value).getTiltakListe()!=null){
                //System.out.println("KEY:"+key);
                System.out.println("<type>:"+value.getNavn()+":"+((TiltakProduksjon)value).getKm());
                for (int i=0;i<((TiltakProduksjon)value).getTiltakListe().size();i++){
                    TiltakProduksjon tp = (TiltakProduksjon)((TiltakProduksjon)value).getTiltakListe().getAt(i);
                    System.out.println("\t<subtype>:"+tp.getNavn()+" - "+tp.getKm()+" - "+tp.getTid()+" - "+tp.getSortering());
                }
            }
        }*/
        addTiltakGlobalList(value);
        
        if (!table.containsKey(key)) {
            TiltakSequence list = new TiltakSequence();

            list.add(value);
            table.put(key, list);
        } else {
            TiltakSequence list = (TiltakSequence)table.get(key);
            
            if (!valueExistsForEnhet(list, value)) {
                list.add(value);
                table.put(key, list);
            }else{
                //tiltak finnes, legg til subtiltaklisten fra values;
                //hent tiltak og legg til tiltaklisten fra value dersom tiltak sin liste ikke er null
                for (int i=0;i<list.size();i++){
                    TiltakObject o = (TiltakObject)list.getAt(i);
                    if (o.getNavn().equals(value.getNavn())){
                        if (o.getTiltakListe()!=null){
                            for (int j=0; j<value.getTiltakListe().size(); j++){
                                TiltakObject subtiltak = (TiltakObject)value.getTiltakListe().getAt(j);
                                o.addGlobalTiltak(subtiltak);         
                                    
                            }
                        }
                    }
                }
            }
        }
    }

    private void addTiltakGlobalList(TiltakObject value)  throws Exception{
        if (globalTiltakList == null) {
            globalTiltakList = new TiltakSequence();
        }

        if (!valueExistsForEnhet(globalTiltakList, value)) {
            globalTiltakList.add(value);
        }else{
            //value kan ha subtyper. Dersom den har det, må nye subtyper legges til values. 
            for (int i = 0; i < globalTiltakList.size(); i++) {
                TiltakObject current = (TiltakObject)globalTiltakList.getAt(i);
                if (current.getNavn().equals(value.getNavn())) {
                    TiltakSequence seq = value.getTiltakListe();
                    if (seq!=null){
                        //Lag nye Tiltaksobjekter for å bruke på forsiden, dette fordi Tiltakobjektene er mutable 
                        //Vi ønsker ikke å endre orginalobjektets tiltakliste. 
                        TiltakObject cur =null;
                        if (current instanceof TiltakProduksjon)
                            cur = new TiltakProduksjon();
                        else if (current instanceof TiltakMengde)
                            cur = new TiltakMengde();
                                    
                        TiltakSequence ss = current.getTiltakListe();
                        cur.addAllSubtype(ss, false);
                        cur.addAllSubtype(seq, false);
                        cur.setID(current.getID());
                        cur.setNavn(current.getNavn());
                        cur.setSortering(current.getSortering());
                        globalTiltakList.getArrayList().set(i, cur);
                    }
                }
            }
        }
    }


    public TiltakSequence getTiltakTypeList(String enhetsID) {
        TiltakSequence tiltakSequence = (TiltakSequence)table.get(enhetsID);
        if (tiltakSequence!=null)
            tiltakSequence.sort(new TiltakComparator());
        return tiltakSequence;
    }

    public ArrayList getHeaderNames(String regNR) {
        ArrayList list = (ArrayList)table.get(regNR);
        String[] sortList = new String[3];

        for (int i = 0; i < list.size(); i++) {
            String header = (String)list.get(i);

            // sorterer liste
            if (header.equals("PløyingKM")) {
                sortList[0] = header;
            } else if (header.equals("StrøingKM")) {
                sortList[1] = header;
            } else if (header.equals("RyddingKM")) {
                sortList[2] = header;
            }
        }

        list = new ArrayList();

        for (int i = 0; i < sortList.length; i++) {
            if (sortList[i] != null) {
                list.add(sortList[i]);
            }
        }

        return list;
    }

    public static boolean valueExistsForEnhet(TiltakSequence list, 
                                              TiltakObject newValue) {
        boolean result = false;
        /*if (list==null){
            return result;
        }*/
        for (int i = 0; i < list.size(); i++) {
            TiltakObject value = (TiltakObject)list.getAt(i);

            if (value.getNavn().equals(newValue.getNavn())) {
                return true;
            }
        }

        return result;
    }

    private boolean valueExistsForEnhet(ArrayList list, String newValue) {
        boolean result = false;

        for (int i = 0; i < list.size(); i++) {
            String value = (String)list.get(i);

            if (value.equals(newValue)) {
                return true;
            }
        }

        return result;
    }

    private boolean isNumeric(String str) {
        try {
            Long.parseLong(str);

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}


