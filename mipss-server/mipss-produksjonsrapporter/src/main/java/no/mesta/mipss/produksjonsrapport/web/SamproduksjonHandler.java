package no.mesta.mipss.produksjonsrapport.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;
import no.mesta.mipss.produksjonsrapport.query.SamproduksjonQuery;
import no.mesta.mipss.produksjonsrapport.values.SamproduksjonObject;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SamproduksjonHandler extends ExcelReportHSSFHandler{
	private EntityManager em;
	private ProdrapportParams params;
	
	public SamproduksjonHandler(final File file, EntityManager em){
		super(file);
		this.em = em;
	}
	@Override
	public byte[] createReport(ProdrapportParams params) throws Exception {
		this.params = params;
		List<SamproduksjonObject> list = new SamproduksjonQuery(params, em).consumeResults();
		byte[] report = null;
		if (list!=null&&list.size()>0){
			report = createExcelReport(list, file);
		}
		return report;
	}

	public byte[] createExcelReport(List<SamproduksjonObject> list, File file) throws FileNotFoundException, IOException, Exception {
		System.out.println("Lager samproduksjonsrapport");
		String periodeText = MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)+"-"+MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		params.setPeriode(periodeText);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("data", list);
		map.put("params", params);
		
		XSSFWorkbook workbook = RapportHelper.readTemplate("samprod_template", map);
		return RapportHelper.toByteArray(workbook);
		
	}
	
}
