package no.mesta.mipss.produksjonsrapport.values;

/**
 * MIPSS1 port
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RodeStrekningSequence extends IndexedSequence {
    public RodeStrekningSequence() {
        super();
    }

    public RodeStrekningSequence(final int initialCapasity) {
        super(initialCapasity);
    }

    public final void add(final RodeStrekningObject rodeStrekningObject) {
        append(rodeStrekningObject);
    }

    public final RodeStrekningObject elementAt(final int index) {
        return (RodeStrekningObject)getAt(index);
    }
}