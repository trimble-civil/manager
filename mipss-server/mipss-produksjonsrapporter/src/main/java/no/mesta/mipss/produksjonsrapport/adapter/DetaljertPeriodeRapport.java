package no.mesta.mipss.produksjonsrapport.adapter;

import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportObject;
import no.mesta.mipss.produksjonsrapport.values.KontraktRapportSequence;
import no.mesta.mipss.produksjonsrapport.values.TiltakMengde;
import no.mesta.mipss.produksjonsrapport.values.TiltakObject;
import no.mesta.mipss.produksjonsrapport.values.TiltakProduksjon;
import no.mesta.mipss.produksjonsrapport.values.TiltakSequence;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.Region;

//TODO rapportene må skrives om hvis de skal endres på
public class DetaljertPeriodeRapport extends PeriodeRapport {
    private boolean gjennomsnitt;
    private boolean prosent;
    
    public DetaljertPeriodeRapport(HSSFWorkbook workbook, boolean gjennomsnitt, boolean prosent, ProdrapportParams params) {
        super(workbook, params);
        this.gjennomsnitt = gjennomsnitt;
        this.prosent = prosent;
    }
    
    public int createReportHeader(HSSFSheet sheet, String rapportNavn, 
                                  String kontrakt, String rodeTypeNavn, 
                                  String rodeNavn, String rodeLengde, 
                                  String rapportDato, String periode, 
                                  int rowCount) {
        
        
        int r = super.createReportHeader(sheet, rapportNavn, kontrakt, rodeTypeNavn, rodeNavn, rodeLengde, rapportDato, periode, rowCount);                                  
        
        String info1 = resources.getGuiString("DetaljertRapport.hastighetInfo");
        String info2 = " (Unntak: Sum Km/t)";
        if (!gjennomsnitt){
            info1 = " ";
            info2 = " ";
        }
        int curlength = info1.length();
        
        HSSFRichTextString kmtinfo = new HSSFRichTextString(info1+info2) ;
        kmtinfo.applyFont(0, curlength, fontNumberRed);
        kmtinfo.applyFont(curlength+1, kmtinfo.getString().length(), fontNumber);
        
        //info rad
        HSSFRow row = sheet.createRow(--r);
        row.createCell((short)0).setCellValue(kmtinfo);
        sheet.addMergedRegion(new Region(r, (short)0, r, (short)3));
        r++;
        
        return r;
        //fontNumberRed/fontNumber
        
                                  
    }
    /**
     * Legg til den dynamiske delen av rapporten. Denne metoden kalles dersom 
     * tiltakstype er produksjon f.eks 200 km. Denne metoden skal overskrives av subklasser
     * for å legge til flere headere på tiltakene. f.eks. km/t
     * 
     * @param i teller som holder styr på hvilken rad som skal lages
     * @param headerRow header raden
     * @param fixedHeaderList den faste delen av rapporten
     * @param countHeader header teller som holder styr på hvilken kolonne som skal lages
     * @param headerName navnet på headeren
     * @param headerRowMain hovedheader.
     * @param sheet excel arket som skal inneholde dataene
     * @deprecated
     */
    protected int createTiltakProduksjonHeader(int i, HSSFRow headerRow, int fixedHeaderList, int countHeader, String headerName, HSSFRow headerRowMain, HSSFSheet sheet, boolean forside){
        int rowFrom = headerRow.getRowNum();
        short colFrom = (short) (i+ fixedHeaderList + countHeader);
        int rowTo = headerRow.getRowNum();
        short colTo = (short) (i + fixedHeaderList + countHeader + 2);
        
        //slå sammen to celler
        sheet.addMergedRegion(new Region(rowFrom, colFrom, rowTo, colTo));

        createCell(headerRow, (short) (i + fixedHeaderList + countHeader), headerName, hTablecellStyle);
        createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), "    Km    ", hTablecellStyle);
        countHeader++;
        createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), "  Timer  ", hTablecellStyle);
        if (gjennomsnitt){
            countHeader++;
            createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader), " Km/t ", hTablecellStyle);
        }
        
        return countHeader;
        
    }
    /**
     * Ny header metode for å støtte kravet om å få inn produkt i rapportene. 
     * @param i
     * @param headerRow
     * @param headerRowMain
     * @param headerRowSub
     * @param fixedHeaderList
     * @param countHeader
     * @param headerName
     * @param sheet
     * @param tiltaksListe
     * @return
     */
    protected int createProduksjonHeader(int i, HSSFRow headerRow, HSSFRow headerRowMain, HSSFRow headerRowSub, int fixedHeaderList, int countHeader, String headerName, HSSFSheet sheet, TiltakSequence tiltaksListe){
        if (tiltaksListe==null)
            tiltaksListe=new TiltakSequence();
        
        int mul =2;
        if (gjennomsnitt)
            mul++;
        
        if (prosent){//&& headerName.equals("Tomkjøring")){
            mul++;
        }
        
        int row = headerRow.getRowNum();
        short colFrom = (short)(i+fixedHeaderList+countHeader);
        short colTo = (short)(i+fixedHeaderList+countHeader+(tiltaksListe.size()*mul));
        if (tiltaksListe.size()==0)
            colTo+=mul-1;
        else
            colTo--;
            
        sheet.addMergedRegion(new Region(row, colFrom, row, colTo));
        
        createCell(headerRow, (short)(i+fixedHeaderList + countHeader), headerName, hTablecellStyle);
        int j = 0;
        do{
            int r = headerRowSub.getRowNum();
            short cFrom = (short)(i+fixedHeaderList+countHeader+j);
            short cTo = (short)(i+fixedHeaderList+countHeader+j+mul-1);
            sheet.addMergedRegion(new Region(r,cFrom,r,cTo));
            String navn = " ";
            if (tiltaksListe.size()!=0)
                navn = ((TiltakProduksjon)tiltaksListe.getAt(j)).getNavn();
                
            createCell(headerRowSub, (short)cFrom, navn, hTablecellStyle);
            createCell(headerRowMain, (short)(i+fixedHeaderList+countHeader+j), "    Km    ", hTablecellStyle);
            countHeader++;
            createCell(headerRowMain, (short)(i+fixedHeaderList+countHeader+j), "  Timer  ", hTablecellStyle);
            if (gjennomsnitt){
                countHeader++;
                createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader+j), " Km/t ", hTablecellStyle);
            }
            if (prosent){
                countHeader++;
                createCell(headerRowMain, (short) (i + fixedHeaderList + countHeader+j), " % ", hTablecellStyle);
            }
            j++;
        }while(j<tiltaksListe.size());
        countHeader+=j-1;//legg til antall subheadere til kolonnetelleren
        return countHeader;
    }
    /**
     * Lag datakolonner for den rapportspesifikke- og den faste delen av rapporten.
     * Denne metoden kan overskrives for å legge til flere datakolonner i den faste delen av rapporten, f.eks. median av km/t
     * @param dataRow
     * @param rapportObject
     * @param columnNR
     * @return
     */
    protected int createColumnsEnhet(HSSFRow dataRow,KontraktRapportObject rapportObject, int columnNR, int rowCount){
        
        //Bygg summeringsdelen av den faste delen av rapporten
        columnNR = createEnhetColumns(dataRow, rapportObject, columnNR);
        columnNR = createTidStart(dataRow, rapportObject, columnNR);
        columnNR = createTidStopp(dataRow, rapportObject, columnNR);
        short refkm = (short)columnNR;
        columnNR = createTotalKjort(dataRow, rapportObject, columnNR);
        short reftid = (short)columnNR;
        columnNR = createTotalTid(dataRow, rapportObject, columnNR);
        //create total km/h
        if (gjennomsnitt)
            columnNR = createTotalKmt(dataRow, columnNR, rowCount,rapportObject, refkm, reftid);
        
        return columnNR;
    }
    
    /**
     * Lag dataceller for tiltakstype produksjon. Denne metoden kan overskrives for å 
     * legge til flere kolonner for en tiltakstype. f.eks. km/t
     * 
     * @param dataRow
     * @param tiltakObject
     * @param columnNR
     * @param rowCount 
     * @return
     */
    protected int createRowProduksjon(HSSFRow dataRow, TiltakObject tiltakObject, int columnNR, int rowCount, boolean paaKontrakt){
        //KM
        HSSFCell tiltakCellKM = dataRow.createCell((short)columnNR);
        double km = ((TiltakProduksjon)tiltakObject).getKm();
        //System.out.print("km="+km+"\t\t");
        tiltakCellKM.setCellValue(km);
        tiltakCellKM.setCellStyle(paaKontrakt?numberStyle:numberStyleBlue);
        short refkm = (short)columnNR;
        columnNR++;

        //TID
        HSSFCell tiltakCellTid = dataRow.createCell((short)columnNR);
        tiltakCellTid.setCellStyle(paaKontrakt?tidStyle:tidStyleBlue);
        String tid = ((TiltakProduksjon)tiltakObject).getTid();
        //System.out.println("tid="+tid+"\t\t");
        evalTIMEFunction(tid, tiltakCellTid);
        //tiltakCellTid.setCellFormula("TIME(" + tid + ")");
        columnNR++;
        
        if (gjennomsnitt){
            //KM/T
            HSSFCell tiltakCellKmt = dataRow.createCell((short)columnNR);
            tiltakCellKmt.setCellStyle(paaKontrakt?kmtStyle:kmtStyleBlue);
            CellReference refKM = new CellReference(rowCount, tiltakCellKM.getCellNum());
            CellReference refTID = new CellReference(rowCount, tiltakCellTid.getCellNum());
    
            //formelen regner ut gjennomsnittsfarten til kjøretøyet, dersom en av verdiene er 0 vil det skrives 0 i cellen
            String formel = "IF(OR(" + refKM.toString() + "=0;" + refTID.toString() + "=0);0;" + refKM.toString() + "/(" + refTID.toString() + "*24))";
            tiltakCellKmt.setCellFormula(formel);
            columnNR++;
            
            //sett rød farge på cellen hvis farten overskrider 100 km/t
            double kmt = calcKMt(tid, km);
            if (kmt>100){
                tiltakCellKmt.setCellStyle(redStyle);
            }
        }
        if (prosent){
            columnNR = createProsent(dataRow, columnNR, paaKontrakt, (short)4, refkm);
        } 
        
        return columnNR;
    }
    private int createProsent(HSSFRow dataRow, int columnNR, boolean paaKontrakt, short refKmTotal, short refKm){
        HSSFCell prosentCell = dataRow.createCell((short)columnNR);
        prosentCell.setCellStyle(paaKontrakt?kmtStyle:kmtStyleBlue);
        
        CellReference refTotalKM = new CellReference(dataRow.getRowNum(), refKmTotal);
        CellReference refKM = new CellReference(dataRow.getRowNum(), refKm);
        
        String prosentFormel = refKM.toString()+"/"+refTotalKM.toString()+" * 100";
        prosentCell.setCellFormula(prosentFormel);
        columnNR++;
        
        return columnNR;
        
        
    }

    /**
     * Lag header for totalkolonnene. 
     * kolonnen
     * @param sheet
     * @param headerRow
     * @param headerRowMain
     * @param fixedHeaderList
     */
    protected int createTotalHeaders(HSSFSheet sheet, HSSFRow headerRow, HSSFRow headerRowMain, int fixedHeaderList, boolean forside){
        int row = headerRow.getRowNum();
        short colFrom = (short) (fixedHeaderList);
        int mul = 1;
        int headCount = 0;
        
        
        createCell(headerRow, (short) (fixedHeaderList), "Total", hTablecellStyle);
        createCell(headerRowMain, (short) (fixedHeaderList), "    Km    ", hTablecellStyle);
        headCount++;
        createCell(headerRowMain, (short) (fixedHeaderList+1), "  Timer  ", hTablecellStyle);
        
        if(gjennomsnitt){
            headCount++;
            createCell(headerRowMain, (short) (fixedHeaderList+headCount), " Km/t ", hTablecellStyle);
            mul++;
        }
        
        short colTo = (short) (fixedHeaderList+mul);
        //slå sammen to celler
        sheet.addMergedRegion(new Region(row, colFrom, row, colTo));
        headCount++;
        return headCount;
    }
    /**
     * Bygg sammen summeringskolonner for den faste delen av rapporten.
     * Først bygges summeringskolonner for den delen som er spesifikk for de forskjellige rapportene, deretter
     * legges det til en del som er felles for samtlige rapporter. Denne metoden kan overskrives for å legge
     * til flere summeringskolonner i den faste delen av rapporten, f.eks. median av km/t
     * 
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param obj
     * @param formulaIndex
     * @param globalSum
     * @param forside
     * @param sumRow
     * @return
     */
    protected int createSumEnhetColumns(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, KontraktRapportObject obj, int formulaIndex, boolean globalSum, boolean forside, HSSFRow sumRow){
        HSSFCellStyle styleNR;
        HSSFCellStyle styleDato;
        HSSFCellStyle styleTid;
        HSSFCellStyle styleKMT;
        
        if (globalSum) {
            styleNR = numberStyle;
            styleDato = datoStyle;
            styleTid = tidStyle;
            styleKMT = kmtStyle;
        }
        else{
            styleNR = numberStyleSum;
            styleDato = datoStyleSum;
            styleTid = tidStyleSum;
            styleKMT = kmtStyleSum;
        }
        
        //Bygg summeringsdelen av den faste delen av rapporten
        columnNR = createEnhetSumColumns(sumRow, obj, columnNR, globalSum, forside);
        columnNR = createSumTidStart(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleDato);
        columnNR = createSumTidStopp(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleDato);
        short refkm = (short)columnNR;
        columnNR = createSumTotalKjort(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleNR);
        short reftid = (short)columnNR;
        columnNR = createSumTotalTid(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleTid);
        if (gjennomsnitt)
            columnNR = createSumTotalKMT(columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaIndex, globalSum, sumRow, styleKMT, refkm, reftid);
        
        return columnNR;
    }
    
    private int createSumTotalKMT(int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaIndex, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleKMT, short refkm, short reftid) {
        
        //CellReference kmtStart = new CellReference(rowCount - antallEnhetsPerioder-1, columnNR);
        //CellReference kmtStopp = new CellReference(rowCount - 2, columnNR);
        
        String medianFormulaSum = enhetsID + "!" + new CellReference(rowCount, columnNR+formulaIndex).toString();
        //String medianFormula = "MEDIAN(" + kmtStart.toString() + ":" + kmtStopp.toString() + ")";

        CellReference kmref = new CellReference(rowCount, refkm);
        CellReference tidref = new CellReference(rowCount, reftid);
        String medianFormula = "IF(OR("+kmref.toString()+"=0;"+tidref.toString()+"=0);0;"+kmref.toString()+"/("+tidref.toString()+"*24))";
        if (globalSum){
            medianFormula = medianFormulaSum;
            
        }

        HSSFCell formulaCellKmt = sumRow.createCell((short)(columnNR));
        formulaCellKmt.setCellFormula(medianFormula);
        formulaCellKmt.setCellStyle(styleKMT);
        columnNR++;
        return columnNR;
    }
    
    /**
     * Lag summeringsrad for tiltakstype produksjon, denne metoden kan overskrives
     * i subklasser for å legge til flere kolonner f.eks km/t
     * @param i
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param enhetsID
     * @param formulaColumn
     * @param globalSum
     * @param sumRow
     * @return
     */
     protected int createSumProduksjon(int i, int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaColumn, boolean globalSum, HSSFRow sumRow, boolean forside){
         HSSFCellStyle styleNR;
         HSSFCellStyle styleTid;
         HSSFCellStyle styleKMT;
         
         if (globalSum) {
             styleNR = numberStyle;
             styleTid = tidStyle;
             styleKMT = kmtStyle;
         }else{
             styleNR = numberStyleSum;
             styleTid = tidStyleSum;
             styleKMT = kmtStyleSum;
         }
         //Summering for KM kolonnen
         columnNR = createSumKM(i,columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow,styleNR);
         formulaColumn++;
         columnNR++;
         
         //Summering for TID kolonnen
         columnNR = createSumTID(i, columnNR, rowCount,antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, styleTid);
         
         //Summering for KM/h kolonnen
         if(gjennomsnitt){
             formulaColumn++;
             columnNR++;
             columnNR = createSumKMT(i, columnNR, rowCount,antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, styleKMT);
             formulaColumn++;
         }
         if(prosent){
             formulaColumn++;
             columnNR++;
             columnNR = createSumProsent(i, columnNR, rowCount, globalSum, sumRow, styleKMT, forside?(short)4:(short)4);
         }
         return columnNR;
     }
    private int createSumProsent(int i, int columnNR, int rowCount, boolean globalSum, HSSFRow sumRow, HSSFCellStyle style, short refKmTotal){
        String prosentFormula = "";
        
        if (globalSum){
            int row = sumRow.getRowNum()+1;
            int kmdiff = gjennomsnitt?3:2;
            CellReference km = new CellReference(sumRow.getRowNum(), 4);
            CellReference refKM = new CellReference(sumRow.getRowNum(), columnNR-kmdiff+i);
            prosentFormula = refKM.toString()+"/"+km.toString()+" * 100";
        }
        else{
            int kmdiff = gjennomsnitt?3:2;
            
            CellReference refTotalKM = new CellReference(sumRow.getRowNum(), refKmTotal);
            CellReference refKM = new CellReference(sumRow.getRowNum(), columnNR-kmdiff+i);
            prosentFormula = refKM.toString()+"/"+refTotalKM.toString()+" * 100";
        }
        HSSFCell formulaCellFrekvens = sumRow.createCell((short)(i + columnNR));
        //formulaCellFrekvens.setCellValue("sumFrekv");
        formulaCellFrekvens.setCellFormula(prosentFormula);
        formulaCellFrekvens.setCellStyle(style);
        return columnNR;
        
    }
    /**
     * Lag en summeringscelle for km/t kolonnen
     * @param i
     * @param columnNR
     * @param rowCount
     * @param antallEnhetsPerioder
     * @param formulaColumn
     * @param globalSum
     * @param sumRow
     * @return
     */
    private int createSumKMT(int i, int columnNR, int rowCount, int antallEnhetsPerioder, String enhetsID, int formulaColumn, boolean globalSum, HSSFRow sumRow, HSSFCellStyle styleKMT) {
        //CellReference kmtStart = new CellReference(rowCount - antallEnhetsPerioder-1, i + columnNR);
        //CellReference kmtStopp = new CellReference(rowCount - 2, i + columnNR);

        String medianFormulaSum = enhetsID + "!" + new CellReference(rowCount, formulaColumn).toString();//referanse til sumformel på enhetsarket
        
        //String medianFormula = "MEDIAN(" + kmtStart.toString() + ":" + kmtStopp.toString() + ")";
        
        CellReference refsumKM = new CellReference(rowCount, (i+columnNR)-2);//KM kolonne
        CellReference refsumTID = new CellReference(rowCount, (i+columnNR)-1);//TID kolonne
        
        String medianFormula = "IF(OR("+refsumKM.toString()+"=0;"+refsumTID.toString()+"=0);0;"+refsumKM.toString()+"/("+refsumTID.toString()+"*24))";
        
        if (globalSum)
            medianFormula = medianFormulaSum;
        
            
        HSSFCell formulaCellKmt = sumRow.createCell((short)(i + columnNR));

        formulaCellKmt.setCellFormula(medianFormula);
        formulaCellKmt.setCellStyle(styleKMT);
        return columnNR;
    }
    
    private int createTotalKmt(HSSFRow dataRow, int columnNR, int rowCount, KontraktRapportObject rapportObject, short refkm, short reftid){
        HSSFCell totalKMT = dataRow.createCell((short)columnNR);
        totalKMT.setCellStyle(kmtStyle);
        CellReference refTotalKM = new CellReference(dataRow.getRowNum(), refkm);
        CellReference refTotalTID = new CellReference(rowCount, reftid);
        
        String totalFormel = "IF(OR(" + refTotalKM.toString() + "=0;" + refTotalTID.toString() + "=0);0;" + refTotalKM.toString() + "/(" + refTotalTID.toString() + "*24))";
        totalKMT.setCellFormula(totalFormel);
        columnNR++;
        
        String tid = rapportObject.getTotaltKjortTid();
        double km = rapportObject.getTotaltKjortKM();
        //sett rød farge på cellen hvis farten overskrider 100 km/t
        if (calcKMt(tid,km)>100){
            totalKMT.setCellStyle(redStyle);
        }
        return columnNR;
    }
    

     /**
      * Lag tomme celler. Disse lages for å fylle rapporten slik at
      * tomme celler får riktig stilart.
      * @param dataRow
      * @param columnNR
      * @return
      */
     protected int createFyllCeller(HSSFRow dataRow, int columnNR){
        HSSFCell tiltakCellTidEmpty = dataRow.createCell((short)columnNR);
        tiltakCellTidEmpty.setCellStyle(textStyle);
        columnNR++;
        if(gjennomsnitt){
            tiltakCellTidEmpty = dataRow.createCell((short)columnNR);
            tiltakCellTidEmpty.setCellStyle(textStyle);
            columnNR++;
        }
        if (prosent){
            tiltakCellTidEmpty = dataRow.createCell((short)columnNR);
            tiltakCellTidEmpty.setCellStyle(textStyle);
            columnNR++;
        }
        return columnNR;
     }
     
    
    /**
     * Lager summeringsraden på rapporten. Dette gjelder summeringsrader pr ark
     * samt en rad pr ark på forsiden. globalsum er true dersom summeringen
     * gjelder summering av selve forsiden og ikke pr.ark.
     * 
     * NOTE: denne metoden er lik superklassen sin createSumRow metode
     * med unntak av generering av tomme celler, markert med X1 og X2
     * 
     * @param sheet 
     * @param dynamicHeaderList
     * @param globalDynamicHeaderList
     * @param enhetsID
     * @param obj
     * @param antallEnhetsPerioder
     * @param rowCount
     * @param rowCountGlobal
     * @param globalSum
     * @param forside
     * @param formulaIndex
     * 
     */
     public void createSumRow(HSSFSheet sheet, TiltakSequence dynamicHeaderList, 
                              TiltakSequence globalDynamicHeaderList, 
                              String enhetsID, String linkLabel, KontraktRapportObject obj, 
                              int antallEnhetsPerioder, int rowCount, 
                              int rowCountGlobal, boolean globalSum, 
                              boolean forside, int formulaIndex) {
         rowCount++;
         HSSFRow sumRow;
         HSSFCell textCell;

         int columnNR = 0;

         if (globalSum) {
             sumRow = sheet.createRow(rowCountGlobal);
             textCell = sumRow.createCell((short)(columnNR));
             textCell.setCellStyle(hyperlinkStyle);

             String target = "\"#" + enhetsID + "!A1\"";
             String label = "\"" + linkLabel + "\"";
             String formula = "HYPERLINK(" + target + ", " + label + ")";
             textCell.setCellFormula(formula);
         } else {
             sumRow = sheet.createRow(rowCount);
             textCell = sumRow.createCell((short)(columnNR));
             textCell.setCellStyle(textStyleSum);
             globalDynamicHeaderList = dynamicHeaderList;
             textCell.setCellValue(new HSSFRichTextString("Sum:"));
         }
         
         columnNR++;
         
         
         //lag summeringskolonner for den faste delen av rapporten
         columnNR = createSumEnhetColumns(columnNR, rowCount, antallEnhetsPerioder, enhetsID, obj, formulaIndex, globalSum, forside, sumRow);
     
         
         int formulaColumn = columnNR + formulaIndex;
         
         //legg til summeringskolonner for den dynamiske delen av rapporten
         for (int i = 0; i < globalDynamicHeaderList.size(); i++) {
         
             TiltakObject tiltakHeader = (TiltakObject)globalDynamicHeaderList.getAt(i);
             boolean tiltakExistsForEnhet = true;    
             if (globalSum) {
                 tiltakExistsForEnhet = KontraktRapportSequence.valueExistsForEnhet(dynamicHeaderList, tiltakHeader);
             }
             
             //lag summeringsrad dersom data finnes for kolonnen
             if (tiltakExistsForEnhet){
                 int size = tiltakHeader.getGlobalTiltakListe()==null?1:tiltakHeader.getGlobalTiltakListe().size();
                 for (int j=0;j<size;j++){
                     //håndter forsiden
                     if (globalSum){
                         
                         //inneholder subtyper, det må sjekkes om subtiltakene inneholder data, dersom det ikke gjør det må 
                         //tomme celler lages. 
                         if (tiltakHeader.getTiltakListe()!=null){
                             //subtiltak
                             TiltakObject tiltakObject = (TiltakObject)tiltakHeader.getTiltakListe().getAt(j);
                             //sjekk om subtiltaket inneholder data. 
                             TiltakSequence headerList = dynamicHeaderList.getTiltakListeFor(tiltakHeader.getNavn());
                             boolean tiltakforenhet = KontraktRapportSequence.valueExistsForEnhet(headerList, tiltakObject);
                             //hvis det finnes data for subtiltaket
                             if (tiltakforenhet){
                                 if (tiltakObject instanceof TiltakProduksjon){
                                     int col = columnNR;
                                     columnNR = createSumProduksjon(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, forside);        
                                     formulaColumn+=(columnNR-col)+1;
                                 }
                                 else if (tiltakObject instanceof TiltakMengde){
                                     columnNR = createSumMengde(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow);
                                     formulaColumn+=1;
                                 }    
                             }
                             //hvis det ikke finnes data for subtype, lag tomme celler
                             else{//X1
                                 HSSFCell emptyCell = sumRow.createCell((short)(i+j + columnNR));
                                 emptyCell.setCellStyle(textStyle);
                                 if (tiltakHeader instanceof TiltakProduksjon){
                                     columnNR++;
                                     emptyCell = sumRow.createCell((short)(i+j + columnNR));
                                     emptyCell.setCellStyle(textStyle);
                                     if (gjennomsnitt){
                                         columnNR++;
                                         emptyCell = sumRow.createCell((short)(columnNR+i+j));
                                         emptyCell.setCellStyle(textStyle);
                                         //createFyllCeller(sumRow, i+j+columnNR);
                                     }
                                     if (prosent){
                                         columnNR++;
                                         emptyCell = sumRow.createCell((short)(columnNR+i+j));
                                         emptyCell.setCellStyle(textStyle);
                                     }
                                 }
                             }
                         //inneholder ingen subtyper
                         }else{
                             if (tiltakHeader instanceof TiltakProduksjon){
                                 int col = columnNR;
                                 columnNR = createSumProduksjon(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, forside);        
                                 formulaColumn+=(columnNR-col)+1;
                             }
                             else if (tiltakHeader instanceof TiltakMengde){
                                 columnNR = createSumMengde(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow);
                                 formulaColumn+=1;
                             }
                         }
                     //håndter detaljarkene
                     }else{
                         if (tiltakHeader instanceof TiltakProduksjon){
                             int col = columnNR;
                             columnNR = createSumProduksjon(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow, forside);        
                             formulaColumn+=(columnNR-col)+1;
                         }
                         else if (tiltakHeader instanceof TiltakMengde){
                             columnNR = createSumMengde(i+j, columnNR, rowCount, antallEnhetsPerioder, enhetsID, formulaColumn, globalSum, sumRow);
                             formulaColumn+=1;
                         }
                     }
                 }
                 columnNR+=(tiltakHeader.getGlobalTiltakListe()==null)?0:tiltakHeader.getGlobalTiltakListe().size()-1;
             }
             
             //inneholder ikke data
             else {//X2
                 //tomme celler for tiltak                  
                 int size = (tiltakHeader.getGlobalTiltakListe()==null)?1:tiltakHeader.getGlobalTiltakListe().size();
                 for (int j=0;j<size;j++){
                     HSSFCell emptyCell = sumRow.createCell((short)(i+j + columnNR));
                     emptyCell.setCellStyle(textStyle);
                     if (tiltakHeader instanceof TiltakProduksjon){
                         columnNR++;
                         emptyCell = sumRow.createCell((short)(i+j + columnNR));
                         emptyCell.setCellStyle(textStyle);
                         if (gjennomsnitt){
                            columnNR++;
                            emptyCell = sumRow.createCell((short)(columnNR+i+j));
                            emptyCell.setCellStyle(textStyle);
                         }
                         if (prosent){
                             columnNR++;
                             emptyCell = sumRow.createCell((short)(columnNR+i+j));
                             emptyCell.setCellStyle(textStyle);
                         }
                         //createFyllCeller(sumRow, i+j+columnNR);
                     }
                 }
                 columnNR+=(tiltakHeader.getGlobalTiltakListe()==null)?0:tiltakHeader.getGlobalTiltakListe().size()-1;
             }
         }
     }

     
     

     
     }


