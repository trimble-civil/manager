package no.mesta.mipss.produksjonsrapport.query;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.values.SamproduksjonObject;
import no.mesta.mipss.query.NativeQueryWrapper;

public class SamproduksjonQuery {

	private StringBuilder query = new StringBuilder();
	private final ProdrapportParams params;
	private final EntityManager em;

	public SamproduksjonQuery(ProdrapportParams params, EntityManager em){
		
		this.params = params;
		this.em = em;
		query.append("select kjoretoy_id, kjoretoy_navn, ");
		query.append("rode_id, rodenavn, ");
		query.append("to_char(fra_dato_tid, 'dd.mm.yyyy hh24:mi:ss'), ");
		query.append("to_char(til_dato_tid,'dd.mm.yyyy hh24:mi:ss'), ");
		query.append("fylkesnummer, kommunenummer, ");
		query.append("veikategori||veistatus||veinummer vei, ");
		query.append("hp, fra_km, til_km, prodstatus ");
		query.append("from table(rapport_funksjoner.rapp3(#kontraktId, #rodetypeId, #rodeId, #kjoretoyId, to_date(#fraTidspunkt, 'dd.mm.yyyy hh24:mi:ss'), to_date(#tilTidspunkt, 'dd.mm.yyyy hh24:mi:ss')))");
		
	}
	
	public List<SamproduksjonObject> consumeResults(){
		Query q = em.createNativeQuery(query.toString());
		String fraDato = MipssDateFormatter.formatDate(params.getFraDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String tilDato = MipssDateFormatter.formatDate(params.getTilDato(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		q.setParameter("kontraktId", params.getDriftkontraktId());
		q.setParameter("rodetypeId", params.getRodetypeId());
		q.setParameter("rodeId", params.getRodeId());
		q.setParameter("kjoretoyId", params.getKjoretoyId());
		q.setParameter("fraTidspunkt", fraDato);
		q.setParameter("tilTidspunkt", tilDato);
		
		NativeQueryWrapper<SamproduksjonObject> wrapper=new NativeQueryWrapper<SamproduksjonObject>(
				q,
				SamproduksjonObject.class,  
				"kjoretoyId", "kjoretoyNavn", "rodeId", 
				"rodenavn", "fraDato", "tilDato",
				"fylkesnummer", "kommunenummer",
				"vei", "hp", "fraKm", "tilKm", "prodstatus");
		
		
		wrapper.setDateFormatter(new SimpleDateFormat(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

		return wrapper.getWrappedList();
	}
}
