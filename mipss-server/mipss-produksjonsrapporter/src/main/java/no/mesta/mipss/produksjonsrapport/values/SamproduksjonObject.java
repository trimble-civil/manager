package no.mesta.mipss.produksjonsrapport.values;

import java.util.Date;

public class SamproduksjonObject {

	private Long kjoretoyId;
	private String kjoretoyNavn;
	private Long rodeId;
	private String rodenavn;
	
	private Date fraDato;
	private Date tilDato;
	private Long fylkesnummer;
	private Long kommunenummer;
	private String vei;
	private Long hp;
	private Double fraKm;
	private Double tilKm;
	private String prodstatus;
	
	public String getKjoretoyNavn() {
		return kjoretoyNavn;
	}
	public void setKjoretoyNavn(String kjoretoyNavn) {
		this.kjoretoyNavn = kjoretoyNavn;
	}
	public Date getFraDato() {
		return fraDato;
	}
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	public Date getTilDato() {
		return tilDato;
	}
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	
	public Double getSumTid(){
		return Math.abs((double)(tilDato.getTime()-fraDato.getTime()))/1000d/86400d;
	}
	public Double getSumKm(){
		return Math.abs(fraKm-tilKm);
	}
	public Long getFylkesnummer() {
		return fylkesnummer;
	}
	public void setFylkesnummer(Long fylkesnummer) {
		this.fylkesnummer = fylkesnummer;
	}
	public Long getKommunenummer() {
		return kommunenummer;
	}
	public void setKommunenummer(Long kommunenummer) {
		this.kommunenummer = kommunenummer;
	}
	public String getVei() {
		return vei;
	}
	public void setVei(String vei) {
		this.vei = vei;
	}
	public Long getHp() {
		return hp;
	}
	public void setHp(Long hp) {
		this.hp = hp;
	}
	public Double getFraKm() {
		return fraKm;
	}
	public void setFraKm(Double fraKm) {
		this.fraKm = fraKm;
	}
	public Double getTilKm() {
		return tilKm;
	}
	public void setTilKm(Double tilKm) {
		this.tilKm = tilKm;
	}
	public String getProdstatus() {
		return prodstatus;
	}
	public void setProdstatus(String prodstatus) {
		this.prodstatus = prodstatus;
	}
	public void setRodeId(Long rodeId) {
		this.rodeId = rodeId;
	}
	public Long getRodeId() {
		return rodeId;
	}
	public void setRodenavn(String rodenavn) {
		this.rodenavn = rodenavn;
	}
	public String getRodenavn() {
		return rodenavn;
	}
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
}
