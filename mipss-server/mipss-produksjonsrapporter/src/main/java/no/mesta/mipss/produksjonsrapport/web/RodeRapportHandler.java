package no.mesta.mipss.produksjonsrapport.web;

import java.io.File;
import java.util.List;

import javax.persistence.EntityManager;

import no.mesta.mipss.persistence.rapportfunksjoner.Rapp1;
import no.mesta.mipss.produksjonsrapport.ProdrapportParams;
import no.mesta.mipss.produksjonsrapport.excel.RodeRapport;
import no.mesta.mipss.produksjonsrapport.query.Rapp1Query;

public class RodeRapportHandler extends ExcelReportHSSFHandler {
	private EntityManager em;
    protected String rapportNavn = "Kontrakt - Rode";
	private final boolean dau;
    public RodeRapportHandler(final File file, EntityManager em, boolean dau ) {
        super(file);
        this.em = em;
		this.dau = dau;
    }

    public byte[] createReport(ProdrapportParams params) throws Exception {
    	params.setRapportType(ProdrapportParams.RapportType.RODE.toString());
        List<Rapp1> results = new Rapp1Query(params, em, Rapp1Query.Rapp1Type.RODE).getResults();
        if (results==null||results.isEmpty()){
        	return null;
        }
        RodeRapport rapp = new RodeRapport(results, params, dau);
        return rapp.toByteArray();
    }

}


