package no.mesta.mipss.accesscontrol;

import no.mesta.mipss.persistence.BrukerLogg;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.persistence.Clock;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

/**
 * Loggerklasse for å logge ting som skjer på serveren. 
 * 
 * en instans benyttes pr. loggevent. Kall metoden start før du gjør kallet som skal logges for å få med tidsbruk. 
 * @author harkul
 *
 */
@Stateless
public class MipssServerLogger implements ServerLogger {

	@EJB
	private AccessControl bean;
	private Long start;
	private String bruker;

	public void start(String bruker){
		start = System.currentTimeMillis();
		this.bruker = bruker;
	}
	
	public void logg(final LoggType type, final String tekst){
		long time = 0;
		if (start!=null){
			time = System.currentTimeMillis()-start;
		}
		
		BrukerLogg l = new BrukerLogg();
		l.setDato(Clock.now());
		l.setSignatur(bruker);
		l.setVarighet(time);
		l.setBeskrivelse(tekst);
		bean.logg(l, type);
	}
}
