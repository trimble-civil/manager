package no.mesta.mipss.accesscontrol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.query.NativeQueryWrapper;

@Stateless(name=BrukerLoggService.BEAN_NAME, mappedName="ejb/"+BrukerLoggService.BEAN_NAME)
public class BrukerLoggBean implements BrukerLoggService {

	@PersistenceContext(unitName = "mipssPersistence")
	EntityManager em;
	
	private static final String DAG= "yyyy-MM-dd";
	private static final String UKE= "yyyy-ww";
	private static final String MND= "yyyy-MM";
	
	public List<LoginStat> getLoginPrDag(){
		String q = getLoginQuery(DAG);
		return getResultsLogin(em.createNativeQuery(q), DAG);
	}
	public List<LoginStat> getLoginPrUke(){
		String q = getLoginQuery("yyyy-iw");
		return getResultsLogin(em.createNativeQuery(q), UKE);
	}
	public List<LoginStat> getLoginPrMnd(){
		String q = getLoginQuery(MND);
		return getResultsLogin(em.createNativeQuery(q), MND);
	}
	
	public List<LoginStat> getLoginPrDag(String sign){
		String q = getLoginBrukerQuery(DAG, sign);
		return getResultsLoginBruker(em.createNativeQuery(q), DAG);
	}
	public List<LoginStat> getLoginPrUke(String sign){
		String q = getLoginBrukerQuery("yyyy-iw", sign);
		return getResultsLoginBruker(em.createNativeQuery(q), UKE);
	}
	public List<LoginStat> getLoginPrMnd(String sign){
		String q = getLoginBrukerQuery(MND, sign);
		return getResultsLoginBruker(em.createNativeQuery(q), MND);
	}
	
	public Map<Date, List<ModulStat>> getModulPrDag(){
		String q = getModulQuery(DAG);
		Query query = em.createNativeQuery(q);
		query.setParameter(1, "MODUL_START");
		return getResultsModul(query, DAG);
	}
	
	public Map<Date, List<ModulStat>>  getModulPrUke(){
		String q = getModulQuery("yyyy-iw");
		Query query = em.createNativeQuery(q);
		query.setParameter(1, "MODUL_START");
		return getResultsModul(query, UKE);
	}
	
	public Map<Date, List<ModulStat>>  getModulPrMnd(){
		String q = getModulQuery(MND);
		Query query = em.createNativeQuery(q);
		query.setParameter(1, "MODUL_START");
		return getResultsModul(query, MND);
	}
	
	public Map<Date, List<ModulStat>> getRapportPrDag(){
		String q = getModulQuery(DAG);
		Query query = em.createNativeQuery(q);
		query.setParameter(1, "HENT_RAPPORT");
		return getResultsModul(query, DAG);
	}
	
	public Map<Date, List<ModulStat>>  getRapportPrUke(){
		String q = getModulQuery("yyyy-iw");
		Query query = em.createNativeQuery(q);
		query.setParameter(1, "HENT_RAPPORT");
		return getResultsModul(query, UKE);
	}
	
	public Map<Date, List<ModulStat>>  getRapportPrMnd(){
		String q = getModulQuery(MND);
		Query query = em.createNativeQuery(q);
		query.setParameter(1, "HENT_RAPPORT");
		return getResultsModul(query, MND);
	}
	
	public Map<Date, List<ModulStat>> getModulPrDag(String sign){
		String q = getModulQuery(DAG, sign);
		return getResultsModul(em.createNativeQuery(q), DAG);
	}
	public Map<Date, List<ModulStat>>  getModulPrUke(String sign){
		String q = getModulQuery("yyyy-iw", sign);
		return getResultsModul(em.createNativeQuery(q), UKE);
	}
	public Map<Date, List<ModulStat>>  getModulPrMnd(String sign){
		String q = getModulQuery(MND, sign);
		return getResultsModul(em.createNativeQuery(q), MND);
	}
	
	public Map<String, Integer> getMestAktiveBrukere(){
		Query q = em.createNativeQuery("select signatur, beskrivelse from brukerlogg where brukerloggtype_navn = 'LOGG_UT' and opprettet_dato > sysdate-30");
		List<BrukerStat> w = new NativeQueryWrapper<BrukerStat>(q, BrukerStat.class, 
				new Class[]{String.class, String.class},
				"sign", "beskrivelse").getWrappedList();
		
		Map<String, Integer> brukerMap = new HashMap<String, Integer>();
		for (BrukerStat s:w){
			Integer cx = brukerMap.get(s.getSign());
			if (cx==null){
				cx = getValueFromString(s.getBeskrivelse());
			}else
				cx += getValueFromString(s.getBeskrivelse());
			brukerMap.put(s.getSign(), cx);
		}
		return brukerMap;
	}
	private Integer getValueFromString(String str){
		String[] s = str.split("/");
		String[] i = s[1].split(":");
		return Integer.valueOf(i[1]);
	}
	
	private Map<Date, List<ModulStat>>  getResultsModul(Query q, String mask){
		NativeQueryWrapper<ModulStat> w = new NativeQueryWrapper<ModulStat>(q, ModulStat.class, 
				new Class[]{Date.class, Integer.class, String.class},
				"dato", "antall", "modul");
		w.setDateMask(mask);
		List<ModulStat> mList = w.getWrappedList();
		Map<Date, List<ModulStat>> mMap = new HashMap<Date, List<ModulStat>>();
		for (ModulStat m:mList){
			List<ModulStat> ml = mMap.get(m.getDato());
			if (ml == null){
				ml = new ArrayList<ModulStat>();
			}
			ml.add(m);
			mMap.put(m.getDato(), ml);
		}
		return mMap;
	}
	
	private String getModulQuery(String dateMask){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT TO_CHAR(opprettet_dato, '"+dateMask+"') dato, ");
		sb.append("COUNT(id) antall_startet, ");
		sb.append("beskrivelse ");
		sb.append("FROM brukerlogg ");
		sb.append("where brukerloggtype_navn=? ");
		sb.append("GROUP BY TO_CHAR(opprettet_dato, '"+dateMask+"'), ");
		sb.append("beskrivelse ");
		sb.append("ORDER BY 1 ASC ");
		return sb.toString();
		
	}
	
	private String getModulQuery(String dateMask, String bruker){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT TO_CHAR(opprettet_dato, '");
		sb.append(dateMask);
		sb.append("') dato, ");
		sb.append("COUNT(id) antall_startet, ");
		sb.append("beskrivelse ");
		sb.append("FROM brukerlogg ");
		sb.append("where brukerloggtype_navn='MODUL_START' ");
		sb.append("and signatur='");
		sb.append(bruker);
		sb.append("' GROUP BY TO_CHAR(opprettet_dato, '");
		sb.append(dateMask);
		sb.append("'), beskrivelse ");
		sb.append("ORDER BY 1 ASC ");
		return sb.toString();
		
	}
	
	
	private String getLoginQuery(String dateMask){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT TO_CHAR(opprettet_dato, '");
		sb.append(dateMask);
		sb.append("') dato, ");
		sb.append("COUNT(id) antall_innlogginger, ");
		sb.append("COUNT(DISTINCT signatur) unike_brukere ");
		sb.append("FROM brukerlogg ");
		sb.append("where brukerloggtype_navn='LOGIN_AUTO' ");
		sb.append("GROUP BY TO_CHAR(opprettet_dato, '");
		sb.append(dateMask);
		sb.append("') ");
		sb.append("ORDER BY 1 ASC");
		return sb.toString();
	}
	
	private List<LoginStat> getResultsLogin(Query q, String mask){
		NativeQueryWrapper<LoginStat> w = new NativeQueryWrapper<LoginStat>(q, LoginStat.class, 
				new Class[]{Date.class, Integer.class, Integer.class},
				"dato", "antall", "unike");
		w.setDateMask(mask);
		return w.getWrappedList();
	}
	
	private String getLoginBrukerQuery(String dateMask, String sign){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT TO_CHAR(opprettet_dato, '");
		sb.append(dateMask);
		sb.append("') dato,");
		sb.append("COUNT(id) antall_innlogginger ");
		sb.append("FROM brukerlogg ");
		sb.append("where brukerloggtype_navn='LOGIN_AUTO' ");
		sb.append("and signatur='");
		sb.append(sign);
		sb.append("' GROUP BY TO_CHAR(opprettet_dato, '");
		sb.append(dateMask);
		sb.append("'), beskrivelse ");
		sb.append("ORDER BY 1 ASC");
		return sb.toString();
	}
	private List<LoginStat> getResultsLoginBruker(Query q, String mask){
		NativeQueryWrapper<LoginStat> w = new NativeQueryWrapper<LoginStat>(q, LoginStat.class, 
				new Class[]{Date.class, Integer.class},
				"dato", "antall");
		w.setDateMask(mask);
		return w.getWrappedList();
	}
	
	public List<InnloggedeBrukereVO> getInnloggedeBrukere() {
		StringBuilder sb = new StringBuilder();
		sb.append("select dagensBrukere.signatur sign, to_char(sisteLogin.opprettetDato, 'dd.mm.yyyy hh24:mi:ss') sisteLogin, to_char(sisteDatoLogg.opprettetDato, 'dd.mm.yyyy hh24:mi:ss') sisteLogg, to_char(sisteLogut.opprettetDato, 'dd.mm.yyyy hh24:mi:ss') sisteLogut from (select distinct signatur from brukerlogg where trunc(opprettet_dato)=trunc(sysdate)) dagensBrukere ");
		sb.append("join (select signatur, max(opprettet_dato) opprettetDato from brukerlogg where trunc(opprettet_dato)=trunc(sysdate) group by signatur) sisteDatoLogg on sisteDatoLogg.signatur=dagensBrukere.signatur ");
		sb.append("join (select signatur, max(opprettet_dato) opprettetDato from brukerlogg b1 where trunc(opprettet_dato)=trunc(sysdate) and b1.brukerloggtype_navn='LOGIN_AUTO' group by signatur) sisteLogin on sisteLogin.signatur=dagensBrukere.signatur ");
		sb.append("left outer join (select signatur, max(opprettet_dato) opprettetDato from brukerlogg b1 where trunc(opprettet_dato)=trunc(sysdate) and b1.brukerloggtype_navn='LOGG_UT' group by signatur) sisteLogut on sisteLogut.signatur=dagensBrukere.signatur ");
		sb.append("where nvl(sisteLogut.opprettetDato, sysdate-1) < sisteLogin.opprettetDato ");
		sb.append("order by sign");

		Query q1 = em.createNativeQuery(sb.toString());
		
		List<InnloggedeBrukereVO> loggedIn = new NativeQueryWrapper<InnloggedeBrukereVO>(q1, InnloggedeBrukereVO.class, 
				new Class[]{String.class, Date.class, Date.class},
				"signatur", "dato", "sisteLoggTid").getWrappedList();
		return loggedIn;
	}
	public List<String> getBrukereILogg() {
		Query q = em.createNativeQuery("select distinct signatur from brukerlogg where brukerloggtype_navn != 'LOGIN_FEILET' order by signatur");
		q.getResultList();
		
		Vector<?> v = (Vector<?>)q.getResultList();
		List<String> brukerList = new ArrayList<String>();
		for (Object o:v){
			brukerList.add((String)o);
		}
		return brukerList;
	}
}
