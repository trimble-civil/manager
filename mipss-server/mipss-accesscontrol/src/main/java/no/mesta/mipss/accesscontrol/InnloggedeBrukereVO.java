package no.mesta.mipss.accesscontrol;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class InnloggedeBrukereVO implements Serializable{

	private String signatur;
	private Date dato;
	private Date sisteLoggTid;
	
	public String getSignatur() {
		return signatur;
	}
	public void setSignatur(String signatur) {
		this.signatur = signatur;
	}
	public Date getDato() {
		return dato;
	}
	public void setDato(Date dato) {
		this.dato = dato;
	}
	public Date getSisteLoggTid() {
		return sisteLoggTid;
	}
	public void setSisteLoggTid(Date sisteLoggTid) {
		this.sisteLoggTid = sisteLoggTid;
	}
	public int hashCode(){
		return getSignatur().hashCode();
	}
	public boolean equals(Object o){
		if (o==this)
			return true;
		if (!(o instanceof InnloggedeBrukereVO))
			return false;
		InnloggedeBrukereVO other = (InnloggedeBrukereVO)o;
		return other.getSignatur().equals(getSignatur());
	}
	public int compareTo(InnloggedeBrukereVO other){
		return signatur.compareTo(other.getSignatur());
	}
	
	
}
