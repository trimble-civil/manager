package no.mesta.mipss.accesscontrol;

import no.mesta.mipss.persistence.BrukerLogg;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;

import javax.ejb.Remote;

@Remote
public interface AccessControlLocal {
	void logg(BrukerLogg logg, LoggType type);
}
