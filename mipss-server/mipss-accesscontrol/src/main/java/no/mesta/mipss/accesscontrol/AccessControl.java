package no.mesta.mipss.accesscontrol;

import java.util.List;

import javax.ejb.Remote;
import javax.security.auth.login.LoginException;

import no.mesta.mipss.persistence.BrukerLogg;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;

/**
 * Bønne implementasjon som står for bruker vedlikehold og tilgangskontroll
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Remote
public interface AccessControl {
	public static final String BEAN_NAME = "AccessControlBean";
	public final static Tilgangsnivaa NO_ACCESS = new Tilgangsnivaa("INGEN");
	public final static Tilgangsnivaa READ = new Tilgangsnivaa("LESE");
	public final static Tilgangsnivaa WRITE = new Tilgangsnivaa("SKRIVE");

	/**
	 * Finner et tilgangspunkt gitt en mottaker plugin og en avsendertype
	 * 
	 * @param mottaker
	 * @param melding
	 * @return
	 */
	public Tilgangspunkt finnMeldingspunkt(String mottaker, String melding);

	/**
	 * Henter en liste over brukere fra databasen
	 * 
	 * @return
	 */
	public List<Bruker> getBrukere();

	/**
	 * Henter modullisten fra databasen
	 * 
	 * @return
	 */
	public List<Modul> getModuler();

	/**
	 * Henter en liste over roller fra databasen
	 * 
	 * @return
	 */
	public List<Rolle> getRoller();

	/**
	 * Henter selskaper
	 * 
	 * @return
	 */
	public List<Selskap> getSelskaper();

	/**
	 * Henter en liste over tilgangsnivåer fra databasen
	 * 
	 * @return
	 */
	public List<Tilgangsnivaa> getTilgangsNivaaer();

	/**
	 * Sjekker om en bruker har tilgang til en gitt modul.
	 * 
	 * Kaster exception hvis negativt filter er satt eller om tilgang ikke er
	 * satt. Tilgang regnes som positiv hvis den er satt til LESE eller SKRIVE.
	 * 
	 * @param signatur
	 * @param moduleId
	 * @throws PluginAccessException
	 *             kastes om brukeren ikke har tilgang
	 */
	public void hasAccess(String signatur, Long moduleId) throws PluginAccessException;

	/**
	 * Henter listen over alle tilgangspunkt
	 * 
	 * @return
	 */
	public List<Tilgangspunkt> hentAlleTilgangspunkt();

	/**
	 * Henter en bruker
	 * 
	 * @param signatur
	 * @return
	 */
	public Bruker hentBruker(String signatur);

	/**
	 * Henter en bruker fra databasen
	 * 
	 * @param signatur
	 * @param refresh
	 * @return
	 */
	public Bruker hentBruker(String signatur, boolean refresh);

	/**
	 * Henter en modul
	 * 
	 * @param modulId
	 * @return
	 */
	public Modul hentModul(Long modulId);

	/**
	 * Henter en rolle
	 * 
	 * @param rolleId
	 * @param refresh
	 * @return
	 */
	public Rolle hentRolle(Long rolleId, boolean refresh);

	/**
	 * Lagrer en ny bruker i databasen
	 * 
	 * @param bruker
	 * @return
	 */
	public Bruker lagreNyBruker(Bruker bruker, String adminSignatur);

	/**
	 * Lagrer en ny rolle i databasen
	 * 
	 * @param rolle
	 * @return
	 */
	public Rolle lagreNyRole(Rolle rolle, String adminSignatur);

	/**
	 * Oppdaterer en bruker i databasen
	 * 
	 * @param bruker
	 * @param adminSignatur
	 * @return
	 */
	public Bruker oppdaterBruker(Bruker bruker, String adminSignatur);
	
	/**
	 * Oppdaterer kun elrappIdent på bruker. 
	 * @param bruker
	 * @param sign
	 */
	void oppdaterElrappNavn(Bruker bruker, String sign);


	/**
	 * Oppdaterer en eksisterende rolle
	 * 
	 * @param rolle
	 * @return
	 */
	public Rolle oppdaterRolle(Rolle rolle, String adminSignatur);

	/**
	 * Sletter en bruker
	 * 
	 * @param signatur
	 */
	public String slettBruker(String signatur);

	/**
	 * Sletter en rolle
	 * 
	 * @param rolleId
	 */
	public void slettRolle(Long rolleId);
	
	/**
	 * Keep alive message
	 */
	public String ping();
	
	void logg(BrukerLogg logg, LoggType type);

	String getDbName();

	String getDbPlatform();

	String getDbHost();

	String getDbSchema();
	
	Bruker authBruker(String signatur, String passordHash) throws LoginException;
}
