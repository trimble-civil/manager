package no.mesta.mipss.accesscontrol;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class ModulStat implements Serializable{

	private Date dato;
	private Integer antall;
	private String modul;
	
	public Date getDato() {
		return dato;
	}
	public void setDato(Date dato) {
		this.dato = dato;
	}
	public Integer getAntall() {
		return antall;
	}
	public void setAntall(Integer antall) {
		this.antall = antall;
	}
	public String getModul() {
		return modul;
	}
	public void setModul(String modul) {
		this.modul = modul;
	}
	
	public int hashCode(){
		return getModul().hashCode();
	}
	public boolean equals(Object o){
		if (o==this)
			return true;
		if (!(o instanceof ModulStat)){
			return false;
		}
		ModulStat other = (ModulStat) o;
		return other.getModul().equals(getModul());
	}
}
