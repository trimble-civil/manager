package no.mesta.mipss.accesscontrol;

import no.mesta.mipss.persistence.BrukerLoggType;

import javax.ejb.Local;
import javax.ejb.Remote;

@Remote
public interface ServerLogger {

    void start(String bruker);
    void logg(final BrukerLoggType.LoggType type, final String tekst);
}
