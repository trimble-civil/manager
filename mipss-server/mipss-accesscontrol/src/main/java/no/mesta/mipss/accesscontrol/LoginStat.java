package no.mesta.mipss.accesscontrol;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class LoginStat implements Serializable{

	
	private Date dato;
	private Integer antall;
	private Integer unike;
	
	
	public LoginStat(){
		
	}
	
	public Date getDato() {
		return dato;
	}

	public void setDato(Date dato) {
		this.dato = dato;
	}

	public Integer getAntall() {
		return antall;
	}

	public void setAntall(Integer antall) {
		this.antall = antall;
	}

	public Integer getUnike() {
		return unike;
	}

	public void setUnike(Integer unike) {
		this.unike = unike;
	}
}
