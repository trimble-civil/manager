package no.mesta.mipss.accesscontrol;

/**
 * Exception som kastes når en bruker ikke har tilgang til en modul
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class PluginAccessException extends Exception {
	/**
	 * Konstruktør
	 * 
	 */
	public PluginAccessException() {
	}

	/**
	 * Konstruktør
	 * 
	 * @param msg
	 */
	public PluginAccessException(String msg) {
		super(msg);
	}
}
