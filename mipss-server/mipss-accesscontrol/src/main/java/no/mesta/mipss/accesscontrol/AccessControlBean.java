package no.mesta.mipss.accesscontrol;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.security.auth.login.LoginException;

import no.mesta.mipss.persistence.BrukerLogg;
import no.mesta.mipss.persistence.BrukerLoggType;
import no.mesta.mipss.persistence.BrukerLoggType.LoggType;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.EagerFetcher;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.tilgangskontroll.BrukerTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Rolle;
import no.mesta.mipss.persistence.tilgangskontroll.RolleBruker;
import no.mesta.mipss.persistence.tilgangskontroll.RolleTilgang;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.tilgangskontroll.SelskapBruker;
import no.mesta.mipss.persistence.tilgangskontroll.Tilgangsnivaa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bønne implementasjon som står for bruker vedlikehold og tilgangskontroll
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Stateless(name = AccessControl.BEAN_NAME, mappedName="ejb/"+AccessControl.BEAN_NAME)
public class AccessControlBean implements AccessControl, AccessControlLocal {
	private static final Logger logger = LoggerFactory.getLogger(AccessControlBean.class);
	@PersistenceContext(unitName = "mipssPersistence")
	EntityManager em;

	public AccessControlBean() {
	}

	/**
	 * Sletter objekter som ikke lenger skal våre med i relasjoner
	 * 
	 * @param old
	 * @param current
	 */
	private void deleteOldEntities(List<?> old, List<?> current) {
		for (Object o : old) {
			if (!current.contains(o)) {
				logger.debug("deleting: " + o);
				em.remove(o);
			}
		}
	}

	/** {@inheritDoc} */
	public Tilgangspunkt finnMeldingspunkt(String mottaker, String melding) {
		Query query;
		if(melding == null) {
			query = em.createNamedQuery(Tilgangspunkt.QUERY_FIND_MESSAGE_POINT_NULL);
		} else {
			query = em.createNamedQuery(Tilgangspunkt.QUERY_FIND_MESSAGE_POINT);
			query.setParameter("message", melding);
		}
		query.setParameter("target", mottaker);
		

		Tilgangspunkt tp = null;
		try{
			tp = (Tilgangspunkt) query.getSingleResult();
		} catch (NoResultException nr){}
		
		return tp;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Bruker> getBrukere() {
		logger.trace("getBrukere() start");
		List<Bruker> brukere = em.createNamedQuery("Bruker.findAll").getResultList();
		logger.trace("getBrukere() slutt, fant " + (brukere == null ? "null" : brukere.size() + " brukere"));
		return brukere;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Modul> getModuler() {
		logger.trace("getModuler() start");
		List<Modul> moduler = em.createQuery("select o from Modul o").getResultList();
		logger.trace("getModuler() slutt, fant " + (moduler == null ? "null" : moduler.size() + "moduler"));
		return moduler;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Rolle> getRoller() {
		logger.trace("getRoller() start");
		List<Rolle> roller = em.createQuery("select o from Rolle o").getResultList();
		for (Rolle r:roller){
			r.getRolleTilgangList().size();
		}
		logger.trace("getRoller() slutt, fant " + (roller == null ? "null" : roller.size() + " roller"));
		return roller;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Selskap> getSelskaper() {
		logger.trace("getSelskaper() start");
		List<Selskap> selskaper = em.createQuery("select o from Selskap o").getResultList();
		logger.trace("getSelskaper() slutt, fant " + (selskaper == null ? "null" : selskaper.size() + "selskaper"));
		return selskaper;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Tilgangsnivaa> getTilgangsNivaaer() {
		logger.trace("getTilgangsNivaaer() start");
		List<Tilgangsnivaa> tilgangsnivaaer = em.createQuery("select o from Tilgangsnivaa o").getResultList();
		logger.trace("getTilgangsNivaaer() slutt, fant "
				+ (tilgangsnivaaer == null ? "null" : tilgangsnivaaer.size() + " niv�er"));
		return tilgangsnivaaer;
	}

	/** {@inheritDoc} */
	public void hasAccess(String signatur, Long moduleId) throws PluginAccessException {
		signatur = signatur.toLowerCase();
		logger.debug("hasAccess(" + signatur + ", " + moduleId + ") start");
		
		StringBuilder sqlRolle = new StringBuilder();
		sqlRolle.append("select rt.nivaa from rolle r ");
		sqlRolle.append("join rolle_tilgang rt on rt.rolle_id= r.id ");
		sqlRolle.append("join rolle_bruker rb on r.id= rb.rolle_id ");
		sqlRolle.append("where rb.signatur=? ");
		sqlRolle.append("and rt.modul_id=? ");
		
		StringBuilder sqlBruker = new StringBuilder();
		sqlBruker.append("select t.nivaa from bruker_tilgang bt "); 
		sqlBruker.append("join tilgangsnivaa t on t.nivaa=bt.nivaa ");
		sqlBruker.append("where bt.signatur=? and bt.modul_id=? ");
		
		Query qBruker = em.createNativeQuery(sqlBruker.toString());
		qBruker.setParameter(1, signatur);
		qBruker.setParameter(2, moduleId);
		
		Query qRolle = em.createNativeQuery(sqlRolle.toString());
		qRolle.setParameter(1, signatur);
		qRolle.setParameter(2, moduleId);
		
		boolean foundAccess = false;
		
		List roller = null;
		try{
			roller = qRolle.getResultList();
		} catch (NoResultException e){}
		
		List bruker = null;
		try{
			bruker = qBruker.getResultList();
		} catch (NoResultException e){}

		if (roller!=null){
			foundAccess = hasAccess(roller, foundAccess);
		}
		if (bruker!=null){
			foundAccess = hasAccess(bruker, foundAccess);
		}
		
		if (!foundAccess) {
			logger.debug("hasAccess(" + signatur + ", " + moduleId + ") finished: No access");
			throw new PluginAccessException("Bruker " + signatur + " har ikke tilgang til modulen ");//satt til " + modul.getNavn());
		}
		logger.debug("hasAccess(" + signatur + ", " + moduleId + ") finished: access");
	}
	
	/**
	 * Sjekker om noen har tilgang til en modul, dersom en bruker er tilknyttet flere roller
	 * og en rolle er satt til å ikke ha tilgang til modulen vil brukeren bli nektet tilgang. 
	 * Det eneste som kan overstyre dette er om brukeren har satt en brukertilgang som tilsier 
	 * annerledes.
	 * @param q
	 * @return
	 */
	private boolean hasAccess(List resultList, boolean axx){
    	if (!resultList.isEmpty()){
    		for (Object o:resultList){
    			String tilgang = (String)o;
    			if (tilgang.equals(AccessControl.READ.getNivaa())||tilgang.equals(AccessControl.WRITE.getNivaa())){
    				axx=true;
    			}
    			if (tilgang.equals(AccessControl.NO_ACCESS.getNivaa())){
    				return false;
    			}
    		}
    	}
    	return axx;
	}
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Tilgangspunkt> hentAlleTilgangspunkt() {
		Query query = em.createNamedQuery(Tilgangspunkt.QUERY_FIND_ALL);
		return query.getResultList();
	}

	/** {@inheritDoc} */
	public Bruker hentBruker(String signatur) {
		signatur = signatur.toLowerCase();
		logger.trace("getBruker(" + signatur + ") start");
		Bruker bruker = em.find(Bruker.class, signatur);
		em.refresh(bruker);
		
		
		EagerFetcher.fetch(bruker);
		
		for (RolleBruker rb:bruker.getRolleBrukerList()){
			EagerFetcher.fetch(rb.getRolle());
//			rb.getRolle().getRolleTilgangList().size();
//			rb.getRolle().getRollemenyList().size();
		}
		logger.trace("getBruker(" + signatur + ") ferdig");
		return bruker;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public Bruker hentBruker(String signatur, boolean refresh) {
		logger.debug("hentBruker(" + signatur + ", " + refresh + ") start");
		signatur = signatur.toLowerCase();
		Bruker bruker = em.find(Bruker.class, signatur);

		if (refresh) {
			em.refresh(bruker);
			
			// Henter alle selskaper
			Query selskapQuery = em.createNamedQuery(SelskapBruker.QUERY_SELSKAPBRUKER_GET_FOR_USER);
			selskapQuery.setParameter("signatur", signatur);
			List<SelskapBruker> brukerSelskaper = selskapQuery.getResultList();
			bruker.setSelskapBrukerList(brukerSelskaper);

			// Henter alle roller
			Query rolleQuery = em.createNamedQuery(RolleBruker.QUERY_ROLLEBRUKER_GET_FOR_USER);
			rolleQuery.setParameter("signatur", signatur);
			List<RolleBruker> brukerRoller = rolleQuery.getResultList();
			bruker.setRolleBrukerList(brukerRoller);

			// Henter alle tilganger
			Query tilgangQuery = em.createNamedQuery(BrukerTilgang.QUERY_BRUKERTILGANG_GET_FOR_USER);
			tilgangQuery.setParameter("signatur", signatur);
			List<BrukerTilgang> brukerTilganger = tilgangQuery.getResultList();
			bruker.setBrukerTilgangList(brukerTilganger);
			for (BrukerTilgang bt : bruker.getBrukerTilgangList()) {
				bt.getModul().getNavn();
				bt.getTilgangsnivaa().getNivaa();
			}
		}
		bruker.getRolleBrukerList().size();
		for (RolleBruker rb:bruker.getRolleBrukerList()){
			rb.getRolle().getRolleTilgangList().size();
		}
		bruker.getBrukerTilgangList().size();
		bruker.getBrukermenyList().size();
		bruker.getSelskapBrukerList().size();
		bruker.getStabfunksjoner().size();
		logger.debug("hentBruker(" + signatur + "," + refresh + ") slutt, fant "
				+ (bruker == null ? "null" : bruker.toString()));
		return bruker;
	}

	/** {@inheritDoc} */
	public Modul hentModul(Long modulId) {
		return em.find(Modul.class, modulId);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public Rolle hentRolle(Long rolleId, boolean refresh) {
		logger.debug("hentRolle(" + rolleId + ", " + refresh + ") start");
		Rolle rolle = em.find(Rolle.class, rolleId);
		if(refresh) {
			em.refresh(rolle);
			
			// Henter alle brukere
			Query brukerQuery = em.createNamedQuery(RolleBruker.QUERY_ROLLEBRUKER_GET_FOR_ROLE);
			brukerQuery.setParameter("rolleId", rolleId);
			List<RolleBruker> brukere = brukerQuery.getResultList();
			rolle.setRolleBrukerList(brukere);
			
			// Henter alle tilganger
			Query tilgangQuery = em.createNamedQuery(RolleTilgang.QUERY_ROLLETILGANG_GET_FOR_ROLE);
			tilgangQuery.setParameter("rolleId", rolleId);
			List<RolleTilgang> tilganger = tilgangQuery.getResultList();
			rolle.setRolleTilgangList(tilganger);			
		}
		return rolle;
	}

	/** {@inheritDoc} */
	public Bruker lagreNyBruker(Bruker bruker, String adminSignatur) {
		bruker.setSignatur(bruker.getSignatur().toLowerCase());
		bruker.setOpprettetAv(adminSignatur);
		bruker.setOpprettetDato(Clock.now());
		em.persist(bruker);
		return bruker;
	}

	/** {@inheritDoc} */
	public Rolle lagreNyRole(Rolle rolle, String adminSignatur) {
		rolle.setOpprettetAv(adminSignatur);
		rolle.setOpprettetDato(Clock.now());
		em.persist(rolle);
		return rolle;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public Bruker oppdaterBruker(Bruker bruker, String adminSignatur) {
		final String method = "oppdatetBruker(";
		logger.trace(method + bruker + ") start");
		bruker.setSignatur(bruker.getSignatur().toLowerCase());
		// INFO: Dette Må kjøre FØR MERGE. Dette er fordi at persistence
		// provideren får ikke med seg disse endringene
		// og oppdager de kun når den kjører SELECT SQL mot basen.
		// JPA toplink hÅndterer ikke relasjoner så derfor må vi selv greie ut
		// av de
		final String signaturParamName = "signatur";
		Query oldUserRolesQuery = em.createNamedQuery(RolleBruker.QUERY_ROLLEBRUKER_GET_FOR_USER);
		oldUserRolesQuery.setParameter(signaturParamName, bruker.getSignatur());
		List<RolleBruker> oldUserRoles = oldUserRolesQuery.getResultList();
		deleteOldEntities(oldUserRoles, bruker.getRolleBrukerList());

		Query oldUserCompaniesQuery = em.createNamedQuery(SelskapBruker.QUERY_SELSKAPBRUKER_GET_FOR_USER);
		oldUserCompaniesQuery.setParameter(signaturParamName, bruker.getSignatur());
		List<SelskapBruker> oldUserCompanies = oldUserCompaniesQuery.getResultList();
		deleteOldEntities(oldUserCompanies, bruker.getSelskapBrukerList());

		Query oldUserLevelsQuery = em.createNamedQuery(BrukerTilgang.QUERY_BRUKERTILGANG_GET_FOR_USER);
		oldUserLevelsQuery.setParameter(signaturParamName, bruker.getSignatur());
		List<BrukerTilgang> oldUserLevels = oldUserLevelsQuery.getResultList();
		deleteOldEntities(oldUserLevels, bruker.getBrukerTilgangList());

		Query oldStabQuery = em.createNamedQuery(KontraktStab.QUERY_FIND_FOR_USER);
		oldStabQuery.setParameter(signaturParamName, bruker.getSignatur());
		List<KontraktStab> oldStab = oldStabQuery.getResultList();
		deleteOldEntities(oldStab, bruker.getStabfunksjoner());

		bruker = em.merge(bruker);
		bruker.setEndretAv(adminSignatur);
		bruker.setEndretDato(Clock.now());
		logger.trace(method + bruker + ") ferdig");
		return bruker;
	}
	
	public void oppdaterElrappNavn(Bruker bruker, String sign){
		Bruker b = em.find(Bruker.class, bruker.getSignatur());
		b.setElrappIdent(bruker.getElrappIdent());
		b.setEndretAv(sign);
		b.setEndretDato(Clock.now());
		em.persist(b);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public Rolle oppdaterRolle(Rolle rolle, String adminSignatur) {
		// INFO: Dette Må kjøre FØR MERGE. Dette er fordi at persistence
		// provideren får ikke med seg disse endringene
		// og oppdager de kun når den kjører SELECT SQL mot basen.
		// JPA toplink hÅndterer ikke relasjoner så derfor må vi selv greie ut
		// av de
		final String rolleParamName = "rolleId";
		Query oldRoleUsersQuery = em.createNamedQuery(RolleBruker.QUERY_ROLLEBRUKER_GET_FOR_ROLE);
		oldRoleUsersQuery.setParameter(rolleParamName, rolle.getId());
		List<RolleBruker> oldRoleUsers = oldRoleUsersQuery.getResultList();
		deleteOldEntities(oldRoleUsers, rolle.getRolleBrukerList());

		Query oldLevelsQuery = em.createNamedQuery(RolleTilgang.QUERY_ROLLETILGANG_GET_FOR_ROLE);
		oldLevelsQuery.setParameter(rolleParamName, rolle.getId());
		List<RolleTilgang> oldLevels = oldLevelsQuery.getResultList();
		deleteOldEntities(oldLevels, rolle.getRolleTilgangList());

		rolle = em.merge(rolle);
		rolle.setEndretAv(adminSignatur);
		rolle.setEndretDato(Clock.now());
		return rolle;
	}

	/**
	 * @param em
	 */
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	/** {@inheritDoc} */
	public String slettBruker(String signatur) {
		final String method = "slettBruker(";
		signatur = signatur.toLowerCase();
		logger.trace(method + signatur + ") start");
		Bruker bruker = em.find(Bruker.class, signatur);
		String message = null;
		try{
			em.remove(bruker);
			em.flush();
		}catch (Throwable t){
			message = t.getMessage();
		}
		logger.trace(method + signatur + ") ferdig");
		return message;
	}

	/** {@inheritDoc} */
	public void slettRolle(Long rolleId) {
		Rolle rolle = em.find(Rolle.class, rolleId);
		em.remove(rolle);
	}
	
	public String ping(){
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return null;
		}
//		return "pong?";
	}
	
	public String getDbName(){
		String q = "select name from v$database";
		String res = getStringFromQuery(q);
		return res;
	}
	public String getDbPlatform(){
		String q = "select platform_name from v$database";
		String res = getStringFromQuery(q);
		return res;
	}
	public String getDbHost(){
		String q = "select sys_context('USERENV', 'SERVER_HOST') from dual";
		String res = getStringFromQuery(q);
		return res;
	}
	public String getDbSchema(){
		String q = "select sys_context('USERENV', 'CURRENT_SCHEMA') from dual";
		String res = getStringFromQuery(q);
		return res;
	}
	
	
	private String getStringFromQuery(String q){
		String res = null;
		try{
			Object object = em.createNativeQuery(q).getSingleResult();
			res = object.toString();
		} catch (Throwable t){
			t.printStackTrace();
		}
		return res;
	}

	public void logg(BrukerLogg logg, LoggType type) {
//		logg.setType(loggType.getNavn());
		BrukerLoggType loggtype = getType(type);
		if (loggtype.getAktiv()){
			logg.setType(type.toString());
			em.persist(logg);
			em.flush();
		}
	}

	private BrukerLoggType getType(LoggType type){
		switch (type){
			case LOGIN_AUTO: return em.find(BrukerLoggType.class, "LOGIN_AUTO");
			case LOGIN_MANUELL: return em.find(BrukerLoggType.class, "LOGIN_MANUELL");
			case LOGIN_FEILET: return em.find(BrukerLoggType.class, "LOGIN_FEILET");
			case LOGG_UT: return em.find(BrukerLoggType.class, "LOGG_UT");
			case MODUL_START: return em.find(BrukerLoggType.class, "MODUL_START");
			case MODUL_FLOAT: return em.find(BrukerLoggType.class, "MODUL_FLOAT");
			case MODUL_AVSLUTT: return em.find(BrukerLoggType.class, "MODUL_AVSLUTT");
			case MODUL_IKKE_TILGANG: return em.find(BrukerLoggType.class, "MODUL_IKKE_TILGANG");
			case HENT_RAPPORT: return em.find(BrukerLoggType.class, "HENT_RAPPORT");
			case HENT_RAPPORT_FEIL: return em.find(BrukerLoggType.class, "HENT_RAPPORT_FEIL");
			case BRUKERSTOTTE: return em.find(BrukerLoggType.class, "BRUKERSTOTTE");
		}
		return null;
	}

	@Override
	public Bruker authBruker(String signatur, String passordHash) throws LoginException {
		Bruker bruker = em.find(Bruker.class, signatur);
		//brukeren finnes ikke i databasen
		if (bruker!=null){
			if (bruker.getPassordHashed().equals(passordHash)){
				return bruker;
			}
		}
		throw new LoginException("Feil brukernavn eller passord");
	}
}
