package no.mesta.mipss.accesscontrol;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

@Remote
public interface BrukerLoggService {
	public static final String BEAN_NAME = "BrukerLogg";
	
	List<LoginStat> getLoginPrDag();
	List<LoginStat> getLoginPrUke();
	List<LoginStat> getLoginPrMnd();
	
	List<LoginStat> getLoginPrDag(String sign);
	List<LoginStat> getLoginPrUke(String sign);
	List<LoginStat> getLoginPrMnd(String sign);
	
	Map<Date, List<ModulStat>> getRapportPrDag();
	Map<Date, List<ModulStat>> getRapportPrUke();
	Map<Date, List<ModulStat>> getRapportPrMnd();


	List<InnloggedeBrukereVO> getInnloggedeBrukere();
	Map<String, Integer> getMestAktiveBrukere();
	
	Map<Date, List<ModulStat>> getModulPrDag();
	Map<Date, List<ModulStat>>  getModulPrUke();
	Map<Date, List<ModulStat>>  getModulPrMnd();
	
	Map<Date, List<ModulStat>> getModulPrDag(String sign);
	Map<Date, List<ModulStat>>  getModulPrUke(String sign);
	Map<Date, List<ModulStat>>  getModulPrMnd(String sign);
	
	List<String> getBrukereILogg();
}
