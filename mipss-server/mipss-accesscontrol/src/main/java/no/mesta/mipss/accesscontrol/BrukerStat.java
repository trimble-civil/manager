package no.mesta.mipss.accesscontrol;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class BrukerStat implements Serializable{

	public String sign;
	public String beskrivelse;
	public Integer score;
	public Date sisteLogg;
	
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getBeskrivelse() {
		return beskrivelse;
	}
	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Date getSisteLogg(){
		return sisteLogg;
	}
	public void setSisteLogg(Date sisteLogg){
		this.sisteLogg = sisteLogg;
	}
	
	public int hashCode(){
		return sign.hashCode();
	}
	public boolean equals(Object o){
		if (o==this)
			return true;
		if (!(o instanceof BrukerStat)){
			return false;
		}
		BrukerStat other = (BrukerStat)o;
		return other.getSign().equals(getSign());
	}
}
