package no.mesta.mipss.menunode;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.applikasjon.Applikasjon;
import no.mesta.mipss.persistence.applikasjon.Menypunkt;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;

/**
 * Bønne for å hente opp menyer
 * 
 * @author Harald A. Kulø
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Remote
public interface ApplikasjonLoader {
    public static final String BEAN_NAME = "ApplikasjonLoader";

    Applikasjon getApplikasjon(Long id);
    List<Menypunkt> getMenypunktAll();
    Tilgangspunkt getTilgangspunkt(String tilgangspunkt);
}
