package no.mesta.mipss.menunode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.applikasjon.Applikasjon;
import no.mesta.mipss.persistence.applikasjon.Bibliotek;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;
import no.mesta.mipss.persistence.applikasjon.Menypunkt;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.dokarkiv.Ikon;

public class ApplikasjonLoaderLocalMock implements ApplikasjonLoader{
    public ApplikasjonLoaderLocalMock() {
    }

    public List<Bibliotek> getBibliotekForMenypunkt(Integer id) {
        return null;
    }

    public Applikasjon getApplikasjon(Long id) {
        return null;
    }

    public List<Menypunkt> getMenypunktAll() {
        List<Menypunkt> list = new ArrayList<Menypunkt>();
        
        list.add(getMp(getIkon("lupeark", 6), "Fuksjonskontrakt", getMGOppsett()));
        list.add(getMp(getIkon("lupeark", 6), "Roder", getMGOppsett()));
        list.add(getMpapp(getIkon("lupeark", 6), "Dagsrapport - mottakere", getMGOppsett(),"no.mesta.mipss.dagsrapport.DagsrapportPanel"));
        list.add(getMp(getIkon("bil", 2), "Instrumentering - prognose", getMGOppsett()));
        list.add(getMp(getIkon("bil", 2), "Instrumentering - bestill", getMGOppsett()));
        list.add(getMp(getIkon("bil", 2), "Kjøretøy", getMGOppsett()));
        list.add(getMp(getIkon("lupe", 5), "Kontroller og statistikk", getMGOppsett()));
        
        list.add(getMp(getIkon("info", 4), "Historisk statistikk", getMGProd()));
        list.add(getMp(getIkon("info", 4), "Værdata", getMGProd(), "http://www.yr.no/sted/Norge/Buskerud/R%C3%B8yken/Slemmestad/"));
        list.add(getMp(getIkon("info", 4), "Friksjonsmålinger", getMGProd()));
        list.add(getMp(getIkon("pda", 7), "Registrerte funn", getMGProd()));
        list.add(getMpapp(getIkon("jorden", 8), "Kartvisning", getMGProd(), "no.mesta.mipss.toolkit.gis.MapPanelSplitPane"));
        list.add(getMp(getIkon("notis", 10), "Arealrapporter", getMGProd()));
        list.add(getMpapp(getIkon("notis", 10), "Veirapporter (Excel)", getMGProd(), "no.mesta.mipss.excelrapport.ExcelrapportUI"));
        list.add(getMpapp(getIkon("graf", 9), "Grafiske rapporter", getMGProd(), "no.mesta.mipss.grafiskrapport.GrafiskRapportUI"));
        
        return list;
    }
    
    private Menypunkt getMpapp(Ikon ikon, String visningsnavn, Menygruppe gruppe, String klassenavn){
        Applikasjon app = new Applikasjon();
        Tilgangspunkt tp = getTp(klassenavn, 2, app);
        Menypunkt m = new Menypunkt();
        m.setIkon(ikon);
        m.setVisningsnavn(visningsnavn);
        m.setTresti(visningsnavn);
        m.setMenygruppe(gruppe);
        m.setTilgangspunkt(tp);
        return m;
    }
    
    private Menypunkt getMp(Ikon ikon, String visningsnavn, Menygruppe gruppe){
        Menypunkt m = new Menypunkt();
        m.setIkon(ikon);
        m.setVisningsnavn(visningsnavn);
        m.setTresti(visningsnavn);
        m.setMenygruppe(gruppe);
        return m;
    }
    private Menypunkt getMp(Ikon ikon, String visningsnavn, Menygruppe gruppe, String url){
        Applikasjon app = new Applikasjon();
        //app.setFilnavn("browser.jar");
        Tilgangspunkt tp = getTp("no.mesta.mipss.toolkit.browser.MipssBrowser", 1, app); 
        Menypunkt m = new Menypunkt();
        m.setIkon(ikon);
        m.setUrl(url);
        m.setVisningsnavn(visningsnavn);
        m.setTresti(visningsnavn);
        m.setMenygruppe(gruppe);
        m.setTilgangspunkt(tp);
        return m;
    }
    private Menygruppe getMGOppsett(){
        return getMG("MIPSS-oppsett", 1);
    }
    private Menygruppe getMGProd(){
        return getMG("Produksjonsstyring", 2);
    }

    private Tilgangspunkt getTp(String klassenavn, long id, Applikasjon app){
        Tilgangspunkt tp = new Tilgangspunkt();
        tp.setKlassenavn(klassenavn);
        tp.setId(id);
        return tp;
    }
    
    private Menygruppe getMG(String tekst, long id){
        Menygruppe mg = new Menygruppe();
        mg.setNavn(tekst);
        mg.setId(id);
        return mg;
    }
    
    private Ikon getIkon(String filename, long id){
        Ikon ikon = new Ikon();
        try {
            ikon.setIkonLob(readIkon(filename));
            ikon.setId(id);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return ikon;
        
    }
    
    private byte[] readIkon(String filename) throws FileNotFoundException, 
                                                    IOException {
        
        //URL url = new URL("no/mesta/mipss/studio/swing/images/"+filename+".png");
        File file = new File(System.getProperty("user.dir")+"/src/no/mesta/mipss/studio/swing/images/"+filename+".png");
        System.out.println(file.toString());
        FileInputStream f = new FileInputStream(file);
        byte[] bytes = new byte[f.available()];
        f.read(bytes);
        f.close();
        return bytes;
    }
    
    public Tilgangspunkt getTilgangspunkt(String klassenavn) {
        Tilgangspunkt tp = new Tilgangspunkt();
        tp.setKlassenavn(klassenavn);
        
        return null;
    }
}
