package no.mesta.mipss.menunode;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.applikasjon.Applikasjon;
import no.mesta.mipss.persistence.applikasjon.Menypunkt;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementasjon av bønne for å hente opp menyer
 * 
 * @author Harald A. Kulø
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Stateless(name=ApplikasjonLoader.BEAN_NAME, mappedName="ejb/"+ApplikasjonLoader.BEAN_NAME)
public class ApplikasjonLoaderBean implements ApplikasjonLoader {

    private static final Logger log = LoggerFactory.getLogger(ApplikasjonLoaderBean.class);
    
    @PersistenceContext(unitName="mipssPersistence")
    EntityManager menuNodeManager;
    public ApplikasjonLoaderBean() {
    }

    public void setMenuNodeManager(EntityManager em) {
        menuNodeManager = em;
    }
    
    public EntityManager getMenuNodeManager() {
        return menuNodeManager;
    }

    /**
     * Henter en applikasjon med en gitt id
     * @param id id til applikasjonen
     * @return Applikasjonen
     */
    public Applikasjon getApplikasjon(Long id) {
        Applikasjon app = menuNodeManager.find(Applikasjon.class, id);
        menuNodeManager.refresh(app);
        return app;
    }


    
    @SuppressWarnings("unchecked")
	public List<Menypunkt> getMenypunktAll(){
    
        Query query= menuNodeManager.createNamedQuery(Menypunkt.QUERY_FIND_ALL);
        query.setHint(QueryHints.REFRESH, HintValues.TRUE);
        List<Menypunkt> list = query.getResultList();
        log.debug("query done, doing refresh");
        for  (Menypunkt m:list){
            if (m!=null){
                menuNodeManager.refresh(m);
                if (m.getIkon()!=null)
                	menuNodeManager.refresh(m.getIkon());
                if (m.getTilgangspunkt()!=null)
                	menuNodeManager.refresh(m.getTilgangspunkt());
            }
            log.debug("refreshed");
        }
        log.debug("done");
        return list;
    }

    public Tilgangspunkt getTilgangspunkt(String tilgangspunkt) {
		return (Tilgangspunkt)menuNodeManager.createQuery("select tp from Tilgangspunkt tp where tp.klassenavn=:tilgangspunkt and tp.fellesFlagg=1").setParameter("tilgangspunkt", tilgangspunkt).getSingleResult();
    }
}
