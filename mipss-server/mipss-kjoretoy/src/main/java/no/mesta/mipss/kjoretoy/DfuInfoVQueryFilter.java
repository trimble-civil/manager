package no.mesta.mipss.kjoretoy;

import no.mesta.mipss.filter.AbstractQueryFilter;

@SuppressWarnings("serial")
public class DfuInfoVQueryFilter extends AbstractQueryFilter {
	private String serienummer;
    private String tlfnummer;
    private String leverandorNavn;
    private Boolean skjulMonterteDfu;
    private String[] fields = {"serienummer", "tlfnummer"};
	
	public DfuInfoVQueryFilter(Boolean skjulMonterteDfu) {
		this.skjulMonterteDfu = skjulMonterteDfu;
	}
	
	public String getSerienummer() {
		return serienummer;
	}

	public void setSerienummer(String serienummer) {
		this.serienummer = serienummer;
	}

	public String getTlfnummer() {
		return tlfnummer;
	}

	public void setTlfnummer(String tlfnummer) {
		this.tlfnummer = tlfnummer;
	}

	public String getLeverandorNavn() {
		return leverandorNavn;
	}

	public void setLeverandorNavn(String leverandorNavn) {
		this.leverandorNavn = leverandorNavn;
	}

	public Boolean skjulMonterteDfu() {
		return skjulMonterteDfu;
	}

	@Override
	public String[] getSearchableFields() {
		return fields;
	}
}
