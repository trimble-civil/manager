package no.mesta.mipss.kjoretoy.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;

public class IOPortValidator {
	private Kjoretoy kjoretoy;
	private KjoretoyUtstyrIo endretKjoretoyUtstyrIo;
	private Long nyIOPort;
	private Date utstyrperiodeFra;
	private Date utstyrperiodeTil;
	private Date ioPeriodeFra;
	private Date ioPeriodeTil;
	private boolean harPeriodeEntitet;
	
	/**
	 * Sjekker om io-oppsett i endret KjoretoyUtstyrIo krasjer med eksisterende oppsett på Kjoretoy
	 * 
	 * @param kjoretoy
	 * @param endretKjoretoyUtstyrIo
	 */
	public IOPortValidator(Kjoretoy kjoretoy, KjoretoyUtstyrIo endretKjoretoyUtstyrIo) {
		this.kjoretoy = kjoretoy;
		this.endretKjoretoyUtstyrIo = endretKjoretoyUtstyrIo;
		this.harPeriodeEntitet = true;
	}
	
	/**
	 * Sjekker om io-oppsett i endret Utstyrperiode krasjer med eksisterende oppsett på Kjoretoy
	 * 
	 * @param kjoretoy
	 * @param endretUtstyrperiode
	 */
	public IOPortValidator(Kjoretoy kjoretoy) {
		this.kjoretoy = kjoretoy;
		this.harPeriodeEntitet = true;
	}
	
	/**
	 * Sjekker om eksisterende perioder krasjer med port og datoer som sender inn
	 * 
	 * @param kjoretoy
	 * @param nyIOPort
	 * @param utstyrperiodeFra
	 * @param utstyrperiodeTil
	 * @param ioPeriodeFra
	 * @param ioPeriodeTil
	 */
	public IOPortValidator(Kjoretoy kjoretoy, Long nyIOPort, Date utstyrperiodeFra, Date utstyrperiodeTil, Date ioPeriodeFra, Date ioPeriodeTil) {
		this.kjoretoy = kjoretoy;
		this.nyIOPort = nyIOPort;
		this.utstyrperiodeFra = utstyrperiodeFra;
		this.utstyrperiodeTil = utstyrperiodeTil;
		this.ioPeriodeFra = ioPeriodeFra;
		this.ioPeriodeTil = ioPeriodeTil;
		this.harPeriodeEntitet = false;
	}
	
	/**
	 * Returnerer true om det ikke er noen krasj i portene 
	 * 
	 * @return
	 */
	public boolean isValid() {
		Map<Long, List<IOPeriode>> mapNye = null;
		if(harPeriodeEntitet) {
			mapNye = getNyePerioderMap(endretKjoretoyUtstyrIo);
		} else {
			mapNye = getNyePerioderMap(nyIOPort, utstyrperiodeFra, utstyrperiodeTil, ioPeriodeFra, ioPeriodeTil);
		}
		Map<Long, List<IOPeriode>> mapEksisterende = getEksisterendePerioderMap(kjoretoy, endretKjoretoyUtstyrIo);	
		return checkMaps(mapEksisterende, mapNye);
	}
	
	/**
	 * Henter ut eksiterende perioder mappet på port
	 * 
	 * @param kjoretoy
	 * @param endretKjoretoyUtstyrIo
	 * @param endretUtstyrperiode
	 * @return
	 */
	private Map<Long, List<IOPeriode>> getEksisterendePerioderMap(Kjoretoy kjoretoy, KjoretoyUtstyrIo endretKjoretoyUtstyrIo) {
		Map<Long, List<IOPeriode>> mapEksisterende = new HashMap<Long, List<IOPeriode>>();
		
		//Looper på kjøretøyutstyr og legger inn perioder og porter i Hashmap
		for(KjoretoyUtstyr kjoretoyUtstyr : kjoretoy.getKjoretoyUtstyrList()) {
			for(KjoretoyUtstyrIo kjoretoyUtstyrIo : kjoretoyUtstyr.getKjoretoyUtstyrIoList()) {
				if(!kjoretoyUtstyrIo.equals(endretKjoretoyUtstyrIo)) {
					Long portNr = kjoretoyUtstyrIo.getBitNr();
					if(portNr != null) {
						IOPeriode ioPeriode = getIOPeriode(kjoretoyUtstyrIo);
						if(ioPeriode != null) {
							if(mapEksisterende.containsKey(portNr)) {
								mapEksisterende.get(portNr).add(ioPeriode);
							} else {
								List<IOPeriode> tmpList = new ArrayList<IOPeriode>();
								tmpList.add(ioPeriode);
								mapEksisterende.put(portNr, tmpList);
							}
						}
					}
				}
			}
		}
		
		return mapEksisterende;
	}
	
	/**
	 * Henter ut nyer perioder mappet på port
	 * 
	 * @param endretKjoretoyUtstyrIo
	 * @param endretUtstyrperiode
	 * @return
	 */
	private Map<Long, List<IOPeriode>> getNyePerioderMap(KjoretoyUtstyrIo endretKjoretoyUtstyrIo) {
		Map<Long, List<IOPeriode>> mapNye = new HashMap<Long, List<IOPeriode>>();
		
		//Bygger opp liste over perioder i den endrede/nye entiteten
		if(endretKjoretoyUtstyrIo != null) {
			Long endretPortNr = endretKjoretoyUtstyrIo.getBitNr();
			if(endretPortNr != null) {
				IOPeriode ioPeriode = getIOPeriode(endretKjoretoyUtstyrIo);
				if(ioPeriode != null) {
					if(mapNye.containsKey(endretPortNr)) {
						mapNye.get(endretPortNr).add(ioPeriode);
					} else {
						List<IOPeriode> tmpList = new ArrayList<IOPeriode>();
						tmpList.add(ioPeriode);
						mapNye.put(endretPortNr, tmpList);
					}
				}
			}
		}
		
		return mapNye;
	}
	
	private Map<Long, List<IOPeriode>> getNyePerioderMap(Long nyIOPort, Date utstyrperiodeFra, Date utstyrperiodeTil, Date ioPeriodeFra, Date ioPeriodeTil) {
		Map<Long, List<IOPeriode>> mapNye = new HashMap<Long, List<IOPeriode>>();
		
		if(nyIOPort != null && overlappingPeriods(ioPeriodeFra, ioPeriodeTil, utstyrperiodeFra, utstyrperiodeTil)) {
			Date startDato = ioPeriodeFra.after(utstyrperiodeFra) ? ioPeriodeFra : utstyrperiodeFra;
			Date sluttDato = null;
			if(ioPeriodeTil == null) {
				sluttDato = utstyrperiodeTil;
			} else if(utstyrperiodeTil == null) {
				sluttDato = ioPeriodeTil;
			} else {
				sluttDato = ioPeriodeTil.before(utstyrperiodeTil) ? ioPeriodeTil : utstyrperiodeTil; 
			}
			IOPeriode ioPeriode = new IOPeriode();
			ioPeriode.startDato = startDato;
			ioPeriode.sluttDato = sluttDato;
			List<IOPeriode> list = new ArrayList<IOPeriode>();
			list.add(ioPeriode);
			mapNye.put(nyIOPort, list);
		}
		
		return mapNye;
	}
	
	/**
	 * Sjekker etter kollisjon mellom nye og eksiterende perioder 
	 * 
	 * @param mapEksisterende
	 * @param mapNye
	 * @return
	 */
	private boolean checkMaps(Map<Long, List<IOPeriode>> mapEksisterende, Map<Long, List<IOPeriode>> mapNye) {
		//Looper på periodene og sjekker etter overlappinger
		for(Long portNrNye : mapNye.keySet()) {
			for(IOPeriode ioPeriodeNye : mapNye.get(portNrNye)) {
				if(mapEksisterende.containsKey(portNrNye)) {
					for(IOPeriode ioPeriodeEksisterende : mapEksisterende.get(portNrNye)) {
						if(overlappingPeriods(ioPeriodeNye.startDato, ioPeriodeNye.sluttDato, ioPeriodeEksisterende.startDato, ioPeriodeEksisterende.sluttDato)) {
							return false;
						}
					}
				}
			}
		}
		
		return true;
	}
	
	private IOPeriode getIOPeriode(KjoretoyUtstyrIo kjoretoyUtstyrIo) {
		IOPeriode ioPeriode = new IOPeriode();
		ioPeriode.startDato = kjoretoyUtstyrIo.getFraDato();
		ioPeriode.sluttDato = kjoretoyUtstyrIo.getTilDato();
		return ioPeriode;
	}
	
	private boolean overlappingPeriods(Date p1Start, Date p1Slutt, Date p2Start, Date p2Slutt) {
		long fra1 = p1Start != null ? p1Start.getTime() : Long.MIN_VALUE+9;
		long til1 = p1Slutt != null ? p1Slutt.getTime() : Long.MAX_VALUE-9;
		long fra2 = p2Start != null ? p2Start.getTime() : Long.MIN_VALUE+9;
		long til2 = p2Slutt != null ? p2Slutt.getTime() : Long.MAX_VALUE-9;
		
		long l1 = til1 - fra2;
		long l2 = til2 - fra1;
		long l3 = til1 - fra1 + til2 - fra2;
		
		return l1 < l3 && l2 < l3;
	}
	
	private class IOPeriode {
		public Date startDato;
		public Date sluttDato;
	}
}
