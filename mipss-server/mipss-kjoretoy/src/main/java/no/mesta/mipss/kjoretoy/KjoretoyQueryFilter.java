package no.mesta.mipss.kjoretoy;

import no.mesta.mipss.filter.AbstractQueryFilter;

@SuppressWarnings("serial")
public class KjoretoyQueryFilter extends AbstractQueryFilter {
	private String eksterntNavn;
	private String regNr;
	private String[] fields = {"eksterntNavn", "regNr"};
	
    public KjoretoyQueryFilter() {
    }
	
	public String getEksterntNavn() {
		return eksterntNavn;
	}

	public void setEksterntNavn(String eksterntNavn) {
		this.eksterntNavn = eksterntNavn;
	}

	public String getRegNr() {
		return regNr;
	}

	public void setRegNr(String regNr) {
		this.regNr = regNr;
	}

	@Override
	public String[] getSearchableFields() {
		return fields;
	}
}
