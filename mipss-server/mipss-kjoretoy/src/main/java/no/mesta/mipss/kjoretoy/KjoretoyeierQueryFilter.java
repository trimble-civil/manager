package no.mesta.mipss.kjoretoy;

import no.mesta.mipss.filter.AbstractQueryFilter;

@SuppressWarnings("serial")
public class KjoretoyeierQueryFilter extends AbstractQueryFilter {
    private String navn;
    private String adresse;
    private String postnummer;
    private String poststed;
    private String[] fields = {"navn", "adresse", "postnummer", "poststed"};
    
    public KjoretoyeierQueryFilter() {
    }
    
	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPostnummer() {
		return postnummer;
	}

	public void setPostnummer(String postnummer) {
		this.postnummer = postnummer;
	}

	public String getPoststed() {
		return poststed;
	}

	public void setPoststed(String poststed) {
		this.poststed = poststed;
	}

	@Override
	public String[] getSearchableFields() {
		return fields;
	}
}
