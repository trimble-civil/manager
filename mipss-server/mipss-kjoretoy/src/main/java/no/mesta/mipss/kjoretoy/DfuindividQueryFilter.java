package no.mesta.mipss.kjoretoy;

import no.mesta.mipss.filter.AbstractQueryFilter;

@SuppressWarnings("serial")
public class DfuindividQueryFilter extends AbstractQueryFilter {
    private String serienummer;
    private String tlfnummer;
    private String modellNavn;
    private String[] fields = {"serienummer", "tlfnummer", "modellNavn"};
	
    public DfuindividQueryFilter() {
    }
    
	public String getSerienummer() {
		return serienummer;
	}

	public void setSerienummer(String serienummer) {
		this.serienummer = serienummer;
	}

	public String getTlfnummer() {
		return tlfnummer;
	}

	public void setTlfnummer(String tlfnummer) {
		this.tlfnummer = tlfnummer;
	}

	public String getModellNavn() {
		return modellNavn;
	}

	public void setModellNavn(String modellNavn) {
		this.modellNavn = modellNavn;
	}

	@Override
	public String[] getSearchableFields() {
		return fields;
	}
}
