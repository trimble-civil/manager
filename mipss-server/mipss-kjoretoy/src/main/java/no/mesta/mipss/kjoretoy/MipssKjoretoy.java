package no.mesta.mipss.kjoretoy;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.Digidok;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInstallasjonV;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfukategori;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.datafangsutstyr.Ioteknologi;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Doktype;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyBilde;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.KjoretoymodellNavn;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreInfoV;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrView;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrprodusent;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.persistence.kontrakt.Kjoretoykontakttype;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.Rodetilknytningstype;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;

/**
 * EJB sesjonsb�nne for behandling av kj�ret�y, kj�ret�yutstyr, installajon og tilst�ttende informasjon.
 * @author jorge
 *
 */
@Remote
public interface MipssKjoretoy {
	public static final String BEAN_NAME = "MipssKjoretoyBean";
	
	/**
	 * 
	 * @param kjoretoy
	 */
	Kjoretoy deleteKjoretoy(KjoretoyListV kjoretoy, String brukerSign);
    /**
     * Henter en enkel liste over alle kj�ret�y i databasen.
     * Liten er enkel fordi de ikke inneholder filtrert informasjon som
     * n�v�rende status, utstyr, o.l.
     * @return listen
     */
    public List<Kjoretoy> getKjoretoyList ();
    
    /**
     * LAgrer et nytt kj�ret�y.
     * @param kjoretoy som skal lagres
     * @return det lagrede kj�ret�yet
     */
    public Kjoretoy addKjoretoy(Kjoretoy kjoretoy);
    
    /**
     * Oppdaterer eksisterende kjoretoy.
     * @param kjoretoy
     * @return det oppdaterte kjoretoy. 
     */
    public Kjoretoy updateKjoretoy(Kjoretoy kjoretoy);
    
    /**
     * S�ker idatabasen vha. et database VIEW.
     * @return
     */
    public List<KjoretoyListV> getKjoretoyListVList(String arg);

    /**
     * Henter en liste over alle modeller registrert i databasen som
     * er meket som valgbare.
     * @return listen
     */
    public List<Kjoretoymodell> getKjoretoymodellList();
    
    /**
     * Henter en liste med ikke-JPA objekter som representerer en struktur
     * med modellnavn med sine modell�r som i sin tur inneholder sin Kjoretoymodell.
     * @return
     */
    public List<KjoretoymodellNavn> getKjoretoymodellNavnList();
    
    /**
     * Henter �rtallet til den eldste modellen som har gyldig valgbar_fagg.
     * @return det eldste �rstallet blant modellene.
     */
    public Long getOldestKjoretoymodellAar();
    
    /**
     * Henter listen over alle kj�ret�ytyper i databasen som er merket
     * som valgbare.
     * @return
     */
    public List<Kjoretoytype> getKjoretoytypeList();
    
    /**
     * Henter en liste over alle bilder for et kj�ret�y.
     * @param kjoretoyId
     * @return listen
     */
    public List<KjoretoyBilde> getKjoretoyBildeList(long kjoretoyId);
    
    /**
     * Henter alle registrerte eiere.
     * @return eierlisten.
     */
    public List<Kjoretoyeier> getKjoretoyeierList();
    
    /**
     * Henter et komplett kj�ret�y-entitet med relaterte lister.
     * Noen av lister til mange-til-mange forhold er det ukke nesiktsmessig � hente her.
     * De blir hentet i egne foresp�rsler.
     * @param id n�kkelen til kj�ret�yet.
     * @return entiteten med komplete lister.
     */
    public Kjoretoy getWholeKjoretoy(long id);
    
    /**
     * Frisker entiteten opp igjen ver � hente info fra databasen. 
     * @param k Kjoretoyet som skal friskes opp.
     * @return det oppfriskede k.
     */
    public Kjoretoy refreshKjoretoy(Kjoretoy k);
    
    /**
     * Henter ut listen av alle Bilde-entiteter koblet til kj�ret�yet med levert id.
     * @param kjoretoyId n�kkelen til kj�ret�yet.
     * @return listen av bilder eller null.
     */
    public List<Bilde> getBildeListForKjoretoy(Long kjoretoyId);
    
    /**
     * Henter ut dokumentlisten for det angitte kj�ret�yet.
     * @param kjoretoyId n�kkelen til kj�ret�yet
     * @return listen av dokumenter eller null.
     */
    public List<Dokument> getDokumentListForKjoretoy(Long kjoretoyId);
    
    /**
     * Henter ut alle eksisterende installasjoner for den leverte kj�ret�y ID.
     * @param kjoretoyId
     * @return listen eller null.
     */
    public List<DfuInstallasjonV> getDfuInstallasjonVList(Long kjoretoyId);
    
//    /**
//     * Henter ut utstyrslisten for den leverte <code>kjoretoyId</code>.
//     * @param kjoretoyId
//     * @return listen eller null.
//     */
//    public List<KjoretoyUtstyrV> getKjoretoyUtstyrVList(Long kjoretoyId);
    
    /**
     * Henter ut instansen av KjoretoyUtstyr for den leverte <code>utstyrId</code>.
     * @param utstyrId
     * @return instansen eller null.
     */
    public KjoretoyUtstyr getKjoretoyUtstyr(Long utstyrId);
    
    /**
     * Henter ut alle registrerte selskaper.
     * @return listen med selskapene eller null.
     */
    public List<Selskap> getSelskapeList();
    
    /**
     * Henter ut installasjonen for den leverte n�kkelen i INSTALLASJOn tabellen.
     * @param installasjonsId
     * @return installasjonsobjektet eller null.
     */
    public Installasjon getInstallasjon(Long installasjonsId);
    
    /**
     * Henter ut alle Dfu modeller i databasen.
     * @return
     */
    public List<Dfumodell> getDfumodellList();
    
    /**
     * Henter ut alle Dfu modeller for en leverandør
     * @return
     */
    public List<Dfumodell> getDfumodellListForLeverandor(String leverandorNavn);
    
    /**
     * Henter alle DFU individer i databasen.
     * @return
     */
    public List<Dfuindivid> getDfuindividList();
    
    Dfuindivid getDfuindivid(Long id);
    
    /**
     * Henter DFU enheten installert p� det leverte kj�ret�yet.
     * @param id
     * @return en Dduindivd eller NULL.
     */
    public Dfuindivid getDfuindividForKjoretoy(Long id);
    
    /**
     * Henter ut alle DFU statuser fra databasen.
     * @return
     */
    public List<Dfustatus> getDfustatusList();
    
    /**
     * Henter ut alle <tt>IOteknologi</tt> fra databasen.
     * @return
     */
    public List<Ioteknologi> getIoteknologiList();
    
    /**
     * Henter ut alle <tt>Dfukategori</tt> fra databasen.
     * @return
     */
    public List<Dfukategori> getDfukategoriList();
    
    /**
     * Henter ut hele Utstyrgruppelisten fra databasen
     * @return
     */
    public List<Utstyrgruppe> getUtstyrgruppeList();

    /**
     * Henter ut hele Utstyrsubgruppelisten fra databasen
     * @return
     */
    public List<Utstyrsubgruppe> getUtstyrsubgruppeList();

    /**
     * Henter ut hele Utstyrmodellisten fra databasen
     * @return
     */
    public List<Utstyrmodell> getUtstyrmodellList();
   
    /**
     * Henter ut listen over alle Utstyrsubgruppe forekomster for den leverte 
     * gruppe ID.
     * @return listen eller null.
     */
    public List<Utstyrsubgruppe> getUtstyrsubgruppeListFor(Long gruppeId);
    
    /**
     * Henter ut listen over alle modeller som sorterer under den angitte subgruppen.
     * @param subgruppeId
     * @return listen eller null.
     */
    public List<Utstyrmodell> getUtstyrmodellListFor(Long subgruppeId);
    
    /**
     * Oppdaterer en allerede persistert entitet av type {@code KjoretoyUtstyr}.
     * @param ku entiteten som skal oppdateres
     * @return den oppdaterte entiteten p� synkronisert med databasen
     */
    public KjoretoyUtstyr updateKjoretoyUtstyr(KjoretoyUtstyr ku);
    
    /**
     * Legger til en ny entitet av type {@code KjoretoyUtstyr} med alle koblingene
     * til databasen.
     * @param ku
     * @return den persisterte entiteten.
     */
    public KjoretoyUtstyr addKjoretoyUtstyr(KjoretoyUtstyr ku);
    
    /**
     * Sletter den leverte entiteten fra databasen.
     * @param ku
     */
    public void removeKjoretoyUtstyr(KjoretoyUtstyr ku);
    
    void removeOrdre(Kjoretoyordre kjoretoyordre);
    
    /**
     * Henter listen med {@code KjoretoyUtstyr} for {@code Kjoretoy}et med den leverte n�kkelen.
     * @param kjoretoyId
     * @return listen, som kan v�re tom.
     */
    public List<KjoretoyUtstyr> getKjoretoyUtstyrListForKjoretoy(Long kjoretoyId);
    
    /**
     * Sletter KjoretoyUtstyret identifiert ved id.
     * @param id
     */
    public void removeKjoretoyUtstyrWithId(long id);
    
    /**
     * Henter en frisk instans av samme {@code KjoretoyUtstyr}.
     * @param ku
     * @return
     */
    public KjoretoyUtstyr refreshKjoretoyUtstyr(KjoretoyUtstyr ku);
    
    /**
     * Henter ut listen over alle forekomsetr ev {@code Sensortype}.
     * @return
     */
    public List<Sensortype> getSensortypeList();
    
    /**
     * Henter listen med Ioliste objekter som er definert for DFU modellen som er 
     * installert i det leverte kj�ret�yet.
     * @param id databasen�kkel for kj�ret�yet
     * @return listen
     */
    public List<Ioliste> getIoListeForKjoretoy(Long id);

	/**
	 * Lagrer det leverte {@code KjoretoyUtstyrIo} i databasen.
	 * Utf�rer en validering av periodeoverlappinger.
	 * @param kuio
	 */
	public KjoretoyUtstyrIo addKjoretoyUtstyrIo(KjoretoyUtstyrIo kuio);

	/**
	 * Henter listen over alle {@code Utstyrperiode}r for det leverte {@code KjoretoyUtstyr}.
	 * @param KjoretoyUtstyrId
	 * @return
	 */
	public List<KjoretoyUtstyrIo> getKjoretoyUtstyrIoListFor(Long KjoretoyUtstyrId);
	
	/**
	 * Henter listen IOliste for det angitte kj�ret�yet.
	 * F�lger forretningsregelen som sier at man f�rst skal hente IOlistene fra DFU installasjonen.
	 * Hvis denne installasjonen mangler, skal man pr�ve � hente dem fra Utstyrkonfigurasjonen.
	 * @param kjoretoyId n�kkelen for kj�ret�yet.
	 * @return listen kan v�re tom.
	 */
	public List<Ioliste> getIolisteListForKjoretoy(Long kjoretoyId);

	/**
	 * Filtrerer listen med {@code Ioliste}r slik at det kun returneres de som kan 
	 * knyttes til et {@code KjoretoyUtstyr} ut fra at man m� ha en bitnummer,
	 * enten fra {@code Utstyrgruppe}n eller fra {@code Ioliste}n.
	 * @param utstyr
	 * @return validert liste
	 */
	public List<Ioliste> getValidatedIolisteListForUtstyr(KjoretoyUtstyr utstyr);
	
	/**
	 * Oppdaterer en eksisterende {@code KjoretoyUtstyrIo}.
	 * 	 * Utf�rer en validering av periodeoverlappinger.
	 * @param mipssEntity 
	 */
	public KjoretoyUtstyrIo updateKjoretoyUtstyrIo(KjoretoyUtstyrIo io);
	
	/**
	 * Henter en liste med utflatede versjoner av KjoretoyUtstyr for det kj�ret�yet
	 * representer av kjoretoyId.
	 * @param kjoretoyId
	 * @return alltid en liste.
	 */
	public List<KjoretoyUtstyrView> getKjoretoyUtstyrViewList(Long kjoretoyId);
	
	/**
	 * Henter listen over alle registrerte kontakttyper.
	 * @return listen.
	 */
	public List<Kjoretoykontakttype> getKontakttypeList();
	
	/**
	 * Henter listen over alle utstyrsprodusenter.
	 * 
	 * @return liste med alle utstyrprodusenter.
	 */
	public List<Utstyrprodusent> getUtstyrProdusentList();	 
	
	/**
	 * Henter en instans av Dokformat for den leverte filnavnsuffix.
	 * @param navn
	 * @return instansen kan være tom hvis ikke funnet.
	 */
	public Dokformat getDokformat(String ext);
	
	/**
	 * Henter en instans avDoktype for det leverte navnet.
	 * @param navn
	 * @return instansen kan være tom hvis ikke funnet.
	 */
	public Doktype getDoktype(String navn);
	
	/**
	 * Henter en liste over alle kontrakter som er knyttet til et kjøretøy
	 * @param kjoretoyId
	 * @return Liste av kontrakter
	 */
	public List<KontraktKjoretoy> getKontraktKjoretoyList(Long kjoretoyId);
	
	/**
	 * Henter liste over kjøretøyseiere basert på verdiene i filteret
	 * @param kjoretoyeierQueryFilter
	 * @return
	 */
	public List<Kjoretoyeier> getKjoretoyeierList(KjoretoyeierQueryFilter kjoretoyeierQueryFilter);
	
	/**
	 * Oppdaterer isntallasjon i basen
	 * 
	 * @param installasjon
	 * @return
	 */
	public Installasjon updateInstallasjon(Installasjon installasjon, String brukerSign);
	/**
	 * Sletter en installasjon
	 * @param installasjon
	 */
	public void removeInstallasjon(Long installasjonId);
	
	/**
	 * Henter liste over Dfuindivid basert på verdier i filter
	 * 
	 * @param dfuindividQueryFilter
	 * @return
	 */
	public List<Dfuindivid> getDfuindividList(DfuindividQueryFilter dfuindividQueryFilter);
	
	/**
	 * Henter liste over Dfuindivid basert på verdier i filter
	 * 
	 * @param dfuindividQueryFilter
	 * @return
	 */
	public List<DfuInfoV> getDfuInfoVList(DfuInfoVQueryFilter dfuInfoVQueryFilter);
	
	/**
	 * Henter liste over kjøretøy basert på verdiene i filteret
	 * @param kjoretoyQueryFilter
	 * @return
	 */
	public List<Kjoretoy> getKjoretoyList(KjoretoyQueryFilter kjoretoyQueryFilter);
	
	/**
	 * Søker opp kjøretøysordre basert på søkekriteriet
	 * 
	 * @param arg
	 * @return
	 */
	public List<KjoretoyordreInfoV> searchKjoretoyordre(String arg, List<String> statusIdenter);
	
	/**
	 * Henter en kjøretøysordre for administrasjon
	 * 
	 * @param id
	 * @return
	 */
	public Kjoretoyordre getKjoretoyordreForAdmin(Long id);
	
	/**
	 * Oppdaterer ordren i basen
	 * 
	 * @param kjoretoyordre
	 * @return
	 */
	public Kjoretoyordre oppdaterKjoretoyordre(Kjoretoyordre kjoretoyordre, String bruker);
	
	/**
	 * Henter alle strøprodukt som kan velges av brukere
	 * 
	 * @return
	 */
	public List<Stroprodukt> getStroproduktValgbareList();

	/**
	 * Henter alle rodetilknytningstyper som kan velges av brukere
	 *
	 * @return
	 */
	public List<Rodetilknytningstype> getRodetilknytningstypeValgbareList();
	
	/**
	 * Henter alle dfuvarekatalog(linjer)
	 * 
	 * @return
	 */
	public List<Dfuvarekatalog> getDfuvarekatalogList();
	
	/**
	 * Henter alle dfuvarekatalog(linjer) for en leverandør
	 * 
	 * @return
	 */
	public List<Dfuvarekatalog> getDfuvarekatalogListForLeverandor(String leverandorNavn);
	
	/**
	 * Henter en KjoretoyordreInfoV basert på ordreid
	 * 
	 * @param ordreId
	 * @return
	 */
	public KjoretoyordreInfoV getKjoretoyordreInfoV(Long ordreId);

	/**
	 * Henter en Ioliste for en id
	 * 
	 * @param id
	 * @return
	 */
	public Ioliste getIolisteForId(Long id);
	
	/**
	 * Henter en Sensortype for et navn
	 * 
	 * @param navn
	 * @return
	 */
	public Sensortype getSensortypeForNavn(String navn);

	/**
	 * Oppdaterer et kjørerøy og sletter KjoretoyordreUtstyr i listen.
	 * OBS! Antar at KjoretoyordreUtstyr i listen er fjernet fra ordren på kjørerøyet!
	 * 
	 * @param kjoretoy
	 * @param kjoretoyordreUtstyrSomSkalSlettes Kan være null
	 * @return
	 */
	public Kjoretoy oppdaterKjoretoyUnderBestilling(Kjoretoy kjoretoy, List<KjoretoyordreUtstyr> kjoretoyordreUtstyrSomSkalSlettes);
	
	/**
	 * Henter en liste med kj�ret�y som har gjort friksjonsm�linger i angitt periode p� angitt kontrakt
	 * @param kontraktId 
	 * @param fraTidspunkt
	 * @param tilTidspunkt
	 * @return
	 */
	public List<KjoretoyDfuVO> getDfuMedFriksjon(Long kontraktId, Date fraTidspunkt, Date tilTidspunkt);
	
	/**
	 * Henter ut datek sin id for et gitt KjoretoyUtstyr, hvis det ikke finnes returneres null
	 * @param ko
	 * @return
	 */
	Long getDatekIdForKjoretoyUtstyr(KjoretoyUtstyr  ko);
	public String prodsettKjoretoyDatek(String serienummer);

	void sendFeilEpost(String body, String subject);

	List<Digidok> getDigiDok();
	
	/**
	 * Henter en spesifikk kjæretøyeier basert på id
	 * @param id
	 * @return
	 */
	public Kjoretoyeier getKjoretoyeierById(Long id);
}