package no.mesta.mipss.kjoretoy;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.util.EmailHelper;

@Stateless
public class EpostHandler {
	private static final String STUDIO_MONTERING = "studio.montering";
	@PersistenceContext(unitName="mipssPersistence")
	private EntityManager em;
	@EJB
	private KonfigParamLocal konfig;

	private final PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssKjoretoyTexts");
	
	/**
	 * Sender en epost hvis kjøretøyordren har fått ny status
	 * @param kjoretoyordre
	 */
	public void sendEpostvarsel(Kjoretoyordre kjoretoyordre){
		String ordreStatusDelerSendt = konfig.hentEnForApp(STUDIO_MONTERING, "ordreStatusDelerSendt").getVerdi();
		String statusIdent = kjoretoyordre.getSisteKjoretoyordrestatus().getIdent();
		if (statusIdent.equals(ordreStatusDelerSendt)){
			sendEpostDelerSendt(kjoretoyordre);
		}
		
		String ordreOpprettet = konfig.hentEnForApp(STUDIO_MONTERING, "ordreStatusOpprettet").getVerdi();
		if (statusIdent.equals(ordreOpprettet)){
			sendEpostOpprettet(kjoretoyordre);
		}
		
		String ordreStatusDelerBestilt = konfig.hentEnForApp(STUDIO_MONTERING, "ordreStatusDelerBestilt").getVerdi();
		if (statusIdent.equals(ordreStatusDelerBestilt)){
			sendEpostDelerBestilt(kjoretoyordre);
		}
	}
	/**
	 * Send epost når en ordre har fått status delet bestilt
	 * @param kjoretoyordre
	 */
	private void sendEpostDelerBestilt(Kjoretoyordre kjoretoyordre) {
		String recipient = konfig.hentEnForApp(STUDIO_MONTERING, "epostMottakerDelerBestilt").getVerdi();
		String dato = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String subject = props.getGuiString("epost.subject.delerBest", new Object[]{kjoretoyordre.getId().toString()});
		String body = props.getGuiString("epost.body.delerBest", new Object[]{dato});
		sendEpost(subject, body, recipient);
	}
	
	/**
	 * Send epost når en ordre har blitt opprettet
	 * @param kjoretoyordre
	 */
	private void sendEpostOpprettet(Kjoretoyordre kjoretoyordre) {
		String recipient = konfig.hentEnForApp(STUDIO_MONTERING, "epostMottakerOpprettet").getVerdi();
		String dato = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String subject = props.getGuiString("epost.subject.opprettet", new Object[]{kjoretoyordre.getId().toString()});
		String body = props.getGuiString("epost.body.opprettet", new Object[]{dato, kjoretoyordre.getId().toString()});
		sendEpost(subject, body, recipient);
	}

	/**
	 * Send e-post til kontrakstleder ved statusendring
	 * @param kjoretoyordre
	 */
	private void sendEpostDelerSendt(Kjoretoyordre kjoretoyordre){
		KontraktKontakttype driftsleder = kjoretoyordre.getDriftkontrakt().getDriftsleder();
		String epost = driftsleder.getEpostAdresse();
		
		String dato = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String subject = props.getGuiString("epost.subject", new Object[]{kjoretoyordre.getId().toString()});
		String body = props.getGuiString("epost.body", new Object[]{dato, kjoretoyordre.getId().toString()});
		sendEpost(subject, body, epost);
	}
	
	/**
	 * Sender en epost
	 * @param subject
	 * @param body
	 * @param recipient
	 */
	public void sendEpost(String subject, String body, String recipient){
		EmailHelper emailHelper = new EmailHelper(em, konfig);
		emailHelper.setSubject(subject);
		emailHelper.setMessage(body);
		emailHelper.addRecipient(recipient);
		try {
			emailHelper.sendMail();
		} catch (MessagingException e1) {
			e1.printStackTrace();
		}
	}
}
