package no.mesta.mipss.kjoretoy;

import javax.ejb.Remote;
import java.util.Date;
import java.util.List;

@Remote
public interface MipssKjoretoyLocal {
	/**
	 * Henter en liste med kjøretøy som har gjort friksjonsmålinger i angitt periode på angitt kontrakt
	 * @param kontraktId 
	 * @param fraTidspunkt
	 * @param tilTidspunkt
	 * @return
	 */
	public List<KjoretoyDfuVO> getDfuMedFriksjon(Long kontraktId, Date fraTidspunkt, Date tilTidspunkt);
}
