package no.mesta.mipss.kjoretoy;

import java.io.Serializable;
import java.util.Date;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

public class KjoretoyDfuVO implements Serializable, IRenderableMipssEntity{

	private Kjoretoy kjoretoy;
	private Dfuindivid dfu;
	private Long kjoretoyId;
	private Long dfuId;
	private Date fraDato;
	private Date tilDato;
	
	public Date getFraDato() {
		return fraDato;
	}
	public void setFraDato(Date fraDato) {
		this.fraDato = fraDato;
	}
	public Date getTilDato() {
		return tilDato;
	}
	public void setTilDato(Date tilDato) {
		this.tilDato = tilDato;
	}
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}
	public void setKjoretoy(Kjoretoy kjoretoy) {
		this.kjoretoy = kjoretoy;
	}
	public Dfuindivid getDfu() {
		return dfu;
	}
	public void setDfu(Dfuindivid dfu) {
		this.dfu = dfu;
	}
	public void setKjoretoyId(Long kjoretoyId) {
		this.kjoretoyId = kjoretoyId;
	}
	public Long getKjoretoyId() {
		return kjoretoyId;
	}
	public void setDfuId(Long dfuId) {
		this.dfuId = dfuId;
	}
	public Long getDfuId() {
		return dfuId;
	}
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getEksterntNavn(){
		return kjoretoy.getEksterntNavn();
	}
}
