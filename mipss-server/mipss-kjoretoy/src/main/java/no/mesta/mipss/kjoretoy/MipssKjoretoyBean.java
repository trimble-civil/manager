/**
 * 
 */
package no.mesta.mipss.kjoretoy;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.kjoretoy.util.IOPortValidator;
import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.Digidok;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInstallasjonV;
import no.mesta.mipss.persistence.datafangsutstyr.Dfu_Status;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfukategori;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.datafangsutstyr.Ioteknologi;
import no.mesta.mipss.persistence.datafangsutstyr.Monteringsinfo;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestillingmaster;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.DfulevBestilling;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Doktype;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.persistence.exceptions.IolisteOverlappingException;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyBilde;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.KjoretoymodellNavn;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreInfoV;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreUtstyr;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreUtstyrPK;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrView;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrprodusent;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.persistence.kontrakt.*;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.query.NativeQueryWrapper;
import no.mesta.mipss.query.NativeQueryWrapper.EntityField;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.lf5.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementerer grensesnittet {@code MipssKjoretoy}.
 * @author jorge
 *
 */
@Stateless(name=MipssKjoretoy.BEAN_NAME, mappedName="ejb/"+MipssKjoretoy.BEAN_NAME)
@SuppressWarnings("unchecked")
public class MipssKjoretoyBean implements MipssKjoretoy, MipssKjoretoyLocal {
	private PropertyResourceBundleUtil props = PropertyResourceBundleUtil.getPropertyBundle("mipssKjoretoyTexts");
	private static final String searchQuoteEnd = "%' ";
    private Logger logger = LoggerFactory.getLogger(this.getClass());
	@EJB
	private EpostHandler epostHandler;
	@EJB
	private KonfigParamLocal konfig;
    
    // context definisjonen M� f�lge etter loggeren, elelrs f�r man InsertionException!
    @PersistenceContext(unitName="mipssPersistence")
    EntityManager em;
    
    public MipssKjoretoyBean() {
    }
    /**
     * Setter slettet_flagg på kjøretøy..
     */
    public Kjoretoy deleteKjoretoy(KjoretoyListV kjoretoy, String brukerSign){
    	Kjoretoy k = em.find(Kjoretoy.class, kjoretoy.getId());
    	k.setSlettetFlagg(Long.valueOf(1));
    	k.getOwnedMipssEntity().setEndretAv(brukerSign);
    	k.getOwnedMipssEntity().setEndretDato(Clock.now());
    	return k;
    }
	/**
	 * Sletter objekter som ikke lenger skal våre med i relasjoner
	 * 
	 * @param old
	 * @param current
	 */
	private void deleteOldEntities(List old, List current) {
		for (Object o : old) {
			if (!current.contains(o)) {
				em.remove(o);
			}
		}
	}
    
	/**
	 * {@inheritDoc}
	 */
    public List<Kjoretoy> getKjoretoyList() {
        return em.createNamedQuery(Kjoretoy.FIND_ALL).getResultList();
    }
    
    /**
	 * {@inheritDoc}
	 */
    public Kjoretoy addKjoretoy(Kjoretoy kjoretoy) {
    	Leverandor lev = kjoretoy.getLeverandor();
    	//legger inn leverandøren i kontraktenes leverandørliste.
    	if (lev!=null){
	    	Leverandor l = em.find(Leverandor.class, lev.getNr());
	    	if (kjoretoy.getKontraktKjoretoyList()!=null){
		    	for (KontraktKjoretoy k:kjoretoy.getKontraktKjoretoyList()){
		    		Driftkontrakt d = em.find(Driftkontrakt.class, k.getDriftkontrakt().getId());
		    		
		    		KontraktLeverandor kontraktLeverandor = new KontraktLeverandor();
					kontraktLeverandor.setDriftkontrakt(d);
					kontraktLeverandor.setLeverandor(l);
					kontraktLeverandor.setFraDato(d.getGyldigFraDato());
					kontraktLeverandor.setTilDato(d.getGyldigTilDato());
					if (!l.getKontraktLeverandorList().contains(kontraktLeverandor)){
						l.getKontraktLeverandorList().add(kontraktLeverandor);
						em.merge(l);
					}
					if (!d.getKontraktLeverandorList().contains(kontraktLeverandor)){
						d.getKontraktLeverandorList().add(kontraktLeverandor);
						em.merge(d);
					}
		    	}
	    	}
    	}
    	em.persist(kjoretoy);
        em.flush();
        em.refresh(kjoretoy);
        return kjoretoy;
    }
    
    public Kjoretoy updateKjoretoy(Kjoretoy kjoretoy) {
    	logger.debug("Oppdaterer kjøretøyet: " + kjoretoy);
    	
    	Query oldInstallasjon = em.createNamedQuery(Installasjon.QUERY_FIND_BY_KJORETOY).setParameter("id", kjoretoy.getId());
    	List<Installasjon> oldList = oldInstallasjon.getResultList();
    	deleteOldEntities(oldList, kjoretoy.getInstallasjonList());
    	
		kjoretoy = em.merge(kjoretoy);
		em.setFlushMode(FlushModeType.COMMIT);
        em.flush();
		
    	return kjoretoy;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<KjoretoyListV> getKjoretoyListVList(String arg) {
        if (arg == null) {
            arg = "";
        }
        if (arg.length() == 0) {
            return em.createQuery("select o from KjoretoyListV o").getResultList();
        }
        else {
            return getKjoretoyListVListFor(arg);
        }
    }
    
    /**
     * søker ved hjelp av database view Kjoretoy_List_V etter alle frokomster i viewet
     * som inneholder argumentet på en eller annen måte.
     * @param arg
     * @return listen
     */
    private List<KjoretoyListV> getKjoretoyListVListFor(String arg) {    	
    	String argLow = arg.toLowerCase();
    	    	
		StringBuffer buf = new StringBuffer("select unique ID, EKSTERNT_NAVN, MASKINNUMMER, REGNR, OPPRETTET_AV, to_char(ENDRET_DATO, 'DD.MM.YYYY HH24:MI:SS'), TYPE_NAVN, MERKE, MODELL, AAR, EIER_NAVN, DFU_STATUS, to_char(SISTE_LIVSTEGN_DATO, 'DD.MM.YYYY HH24:MI:SS'), DFU_SERIENUMMER, DFU_TLFNUMMER, ANS_KONTRAKTNUMMER, ANS_KONTRAKTNAVN, DFU_ID, SLETTET_FLAGG from kjoretoy_list_v where ");
		buf.append("lower(eksternt_navn) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(type_navn) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(regnr) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(merke) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(modell) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(eier_navn) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(dfu_status) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(dfu_serienummer) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(dfu_tlfnummer) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(ans_kontraktnummer) like '%").
			append(argLow).
			append(searchQuoteEnd).
			append("or lower(ans_kontraktnavn) like '%").
			append(argLow).
			append(searchQuoteEnd);
		try {
			Long.valueOf(arg);
			buf.append("or maskinnummer like '%").
				append(argLow).
				append(searchQuoteEnd).
				append("or aar like '%").
				append(argLow).
				append(searchQuoteEnd);
		}
		catch (NumberFormatException e) {
			// La være å lete i numeriske felt
		}
		Query q = em.createNativeQuery(buf.toString());
		
		//OBS: rekkefølgen med fiel parametrene må stemme overens med selve resultatet på querien
		return new NativeQueryWrapper<KjoretoyListV>(q, KjoretoyListV.class, new EntityField[]{
				new EntityField(Long.class, "id"),
				new EntityField(String.class, "eksterntNavn"),
				new EntityField(Long.class, "maskinnummer"),
				new EntityField(String.class, "regnr"),
				new EntityField(String.class, "opprettetAv"),
				new EntityField(Date.class, "endretDato"),
				new EntityField(String.class, "typeNavn"),
				new EntityField(String.class, "merke"),
				new EntityField(String.class, "modell"),
				new EntityField(Long.class, "aar"),
				new EntityField(String.class, "eierNavn"),				
				new EntityField(String.class, "dfuStatus"),
				new EntityField(Date.class, "sisteLivstegnDato"),
				new EntityField(String.class, "dfuSerienummer"),
				new EntityField(String.class, "dfuTlfnummer"),
				new EntityField(String.class, "ansKontraktnummer"),
				new EntityField(String.class, "ansKontraktnavn"),
				new EntityField(Long.class, "dfuId"),
				new EntityField(Long.class, "slettet")}).getWrappedList();
    }

	/**
	 * {@inheritDoc}
	 */
    public List<Kjoretoymodell> getKjoretoymodellList() {
    	return em.createNamedQuery(Kjoretoymodell.FIND_VALID).getResultList();
    }
    
    /**
	 * {@inheritDoc}
	 */
    public List<KjoretoymodellNavn> getKjoretoymodellNavnList() {
    	List<Kjoretoymodell> wholeList = em.createNamedQuery(Kjoretoymodell.FIND_VALID).getResultList();
    	Map<String, KjoretoymodellNavn> organizer = new TreeMap<String, KjoretoymodellNavn>();
    	
    	for (Kjoretoymodell m : wholeList) {
    		if(organizer.containsKey(m.getTextForGUI())) {
    			organizer.get(m.getTextForGUI()).add(m);
    		}
    		else {
    			organizer.put(m.getTextForGUI(), new KjoretoymodellNavn(m));
    		}
    	}
    	return new ArrayList<KjoretoymodellNavn>(organizer.values());
    }
    
    /**
	 * {@inheritDoc}
	 */
    public Long getOldestKjoretoymodellAar() {
    	Query q = em.createNativeQuery("select * from kjoretoymodell where aar = (select min(aar) from kjoretoymodell where aar is not null and valgbar_flagg = 1)");
    	Kjoretoymodell m = new NativeQueryWrapper<Kjoretoymodell>(q, Kjoretoymodell.class, new EntityField[]{}).getWrappedSingleResult();
    	if(m == null || m.getAar() == null) {
    		return Long.valueOf("1900");
    	}
    	return Long.valueOf(m.getAar());
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Kjoretoytype> getKjoretoytypeList() {
        return em.createQuery("select t from Kjoretoytype t where t.valgbarFlagg = 1 order by t.navn").getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<KjoretoyBilde> getKjoretoyBildeList(long kjoretoyId) {
        Query bilder = em.createNamedQuery(KjoretoyBilde.QUERY_BILDER_FOR_KJORETOY);
        bilder.setParameter("kjoretoyId", kjoretoyId);
        return bilder.getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Kjoretoyeier> getKjoretoyeierList() {
        return em.createNamedQuery(Kjoretoyeier.FIND_ALL).getResultList();
    }
    
    public List<Kjoretoyeier> searchKjoretoyEier(String search) {
    	String lowSearch = search.toLowerCase();
    	
    	StringBuffer buf = new StringBuffer("select * from kjoretoyeier where lower(navn) like '%");
    	buf.append(lowSearch).
    		append(searchQuoteEnd).
    		append("or lower(adresse) like '%").
    		append(lowSearch).
    		append(searchQuoteEnd).
    		append("or lower(epost_adresse) like '%").
    		append(lowSearch).
    		append(searchQuoteEnd).
    		append("or lower(poststed) like '%").
    		append(lowSearch).
    		append(searchQuoteEnd);
    	try {
    		Long number = Long.valueOf(search);
    		buf.append("or postnummer like '%").
    			append(number).
    			append(searchQuoteEnd).
    			append("or tlfnummer1 like '%").
    			append(number).
    			append(searchQuoteEnd).
    			append("or tlfnummer2 like '").
    			append(number).
    			append("%'");
    	}
    	catch (NumberFormatException e) {
			// La være å lete i numeriske felt
		}
    	
    	Query q = em.createNativeQuery(buf.toString());
    	return new NativeQueryWrapper<Kjoretoyeier>(q, Kjoretoyeier.class, new Class[]{String.class}, lowSearch).getWrappedList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public Kjoretoy getWholeKjoretoy(long id) {
        Query q = em.createNamedQuery(Kjoretoy.FIND_RICH);
        q.setParameter("id", id);
        
        Kjoretoy kjoretoy;
        try {
            kjoretoy = (Kjoretoy) q.getSingleResult();
            kjoretoy.getKlippeperiodeList().size();
            em.refresh(kjoretoy);
        }
        catch (NoResultException e) {
        	logger.warn("Kjoretoy for id " + id + " ikke funnet");
            return null;
        }
        
        return kjoretoy;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public Kjoretoy refreshKjoretoy(Kjoretoy k) {
    	return getWholeKjoretoy(k.getId());
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Bilde> getBildeListForKjoretoy(Long kjoretoyId) {
        Query q = em.createNamedQuery(Bilde.GET_FOR_KJORETOY);
        q.setParameter("id", kjoretoyId);
        return q.getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Dokument> getDokumentListForKjoretoy(Long kjoretoyId) {
        Query q = em.createNamedQuery(Dokument.FIND_FOR_KJORETOY);
        q.setParameter("id", kjoretoyId);
        return q.getResultList();
    }
    
    
	/**
	 * {@inheritDoc}
	 */
    public List<DfuInstallasjonV> getDfuInstallasjonVList(Long kjoretoyId) {
        Query q = em.createQuery("select i from DfuInstallasjonV i where i.kjoretoyId = :id");
        q.setParameter("id", kjoretoyId);
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        List<DfuInstallasjonV> dfus = q.getResultList();
        
        // kopier over til eget set og unng� duplikater
        Set<DfuInstallasjonV> dfuSet = new CopyOnWriteArraySet<DfuInstallasjonV>();
        for (DfuInstallasjonV inst : dfus) {
            if (!dfuSet.add(inst)) {
                updateStatusList(dfuSet, inst);
            }
        }
        return new ArrayList(dfuSet);
    }
    
    /**
     * G�r sekvensielt gjennom settet og oppdaterer statuslisten til den f�rste forekomsten i listen
     * som er equals() den leverte "inst".
     * @param dfuSet
     * @param inst
     */
    private void updateStatusList(Set<DfuInstallasjonV> dfuSet, DfuInstallasjonV inst) {
        for (DfuInstallasjonV setInst: dfuSet) {
            if (setInst.equals(inst)) {
                setInst.addStatus(inst.getSqlViewStatus());
                break;
            }
        }
    }
    
//	/**
//	 * {@inheritDoc}
//	 */
//    public List<KjoretoyUtstyrV> getKjoretoyUtstyrVList(Long kjoretoyId) {
//        Query q = em.createNamedQuery(KjoretoyUtstyrV.FOR_KJORETOY);
//        q.setParameter("id", kjoretoyId);
//        return q.getResultList();
//    }
    
	/**
	 * {@inheritDoc}
	 */
    public KjoretoyUtstyr getKjoretoyUtstyr(Long utstyrId) {
        KjoretoyUtstyr ku = em.find(KjoretoyUtstyr.class, utstyrId);
        return ku;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<KjoretoyUtstyr> getKjoretoyUtstyrListForKjoretoy(Long kjoretoyId) {
    	Query q = em.createNamedQuery(KjoretoyUtstyr.LIST_FOR_KJORETOY);
    	q.setParameter("id", kjoretoyId);
    	List<KjoretoyUtstyr> list = q.getResultList();
    	if(list == null) {
    		list = new Vector<KjoretoyUtstyr>();
    	}
    	return list;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Selskap> getSelskapeList() {
        return em.createQuery("select o from Selskap o order by o.navn").getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public Installasjon getInstallasjon(Long installasjonsId) {
        Query q = em.createQuery("select i from Installasjon i where i.id = :id");
        q.setParameter("id", installasjonsId);
        
        Installasjon inst = null;
        try {
            inst = (Installasjon) q.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        inst.getDfumodell();
        Dfuindivid individ = inst.getDfuindivid();
        if (individ != null) {
            individ.setCurrentStatus(getCurrentStatusForIndivid(individ));
        }
        return inst;
    }
    
    /**
     * Henter status for den leverte DFU individ ut fra statuslisten i mange-til-mange
     * tabellen DFU_STATUS
     * @param individ
     * @return
     */
    private Dfustatus getCurrentStatusForIndivid(Dfuindivid individ) {
        List<Dfu_Status> stats = individ.getDfu_StatusList();
        if (stats != null && stats.size() > 0) {
            return em.find(Dfustatus.class, stats.get(0).getDfustatus().getNavn());
        }
        return null;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Dfumodell> getDfumodellList() {
        Query q = em.createQuery("select m from Dfumodell m order by m.navn");
        return q.getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Dfuindivid> getDfuindividList() {
        Query q = em.createNamedQuery(Dfuindivid.QUERY_FIND_ALL);
        q.setMaxResults(50);
        return q.getResultList();
    }
    public Dfuindivid getDfuindivid(Long id){
    	Dfuindivid i =  em.find(Dfuindivid.class, id);
    	i.getInstallasjonList().size();
    	return i;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public Dfuindivid getDfuindividForKjoretoy(Long id) {
        Query q = em.createNamedQuery(Dfuindivid.FIND_FOR_KJORETOY);
        q.setParameter("id", id);
        q.setParameter("now", Clock.now());
        try {
            return (Dfuindivid)q.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Dfustatus> getDfustatusList() {
        Query q = em.createQuery("select s from Dfustatus s order by s.navn");
        return q.getResultList();
    }
    
    /** {@inheritDoc} */
    public List<Ioteknologi> getIoteknologiList() {
    	return em.createNamedQuery(Ioteknologi.FIND_ALL).getResultList();
    	
    }
    
    /** {@inheritDoc} */
    public List<Dfukategori> getDfukategoriList() {
    	return em.createNamedQuery(Dfukategori.FIND_ALL).getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Utstyrgruppe> getUtstyrgruppeList() {
        Query q = em.createNamedQuery(Utstyrgruppe.FIND_VALGBAR);
        return q.getResultList();
    }

	/**
	 * {@inheritDoc}
	 */
	public List<Utstyrsubgruppe> getUtstyrsubgruppeList() {
        Query q = em.createNamedQuery(Utstyrsubgruppe.FIND_VALGBAR);
        return q.getResultList();
    }

	/**
	 * {@inheritDoc}
	 */
    public List<Utstyrmodell> getUtstyrmodellList() {
        Query q = em.createNamedQuery(Utstyrmodell.FIND_VALGBAR);
        return q.getResultList();
    }

	/**
	 * {@inheritDoc}
	 */
    public List<Utstyrsubgruppe> getUtstyrsubgruppeListFor(Long gruppeId) {
        Query q = em.createNamedQuery(Utstyrsubgruppe.FOR_GRUPPE);
        q.setParameter("id", gruppeId);
        List<Utstyrsubgruppe> list = q.getResultList();
        Collections.sort(list, new Comparator<Utstyrsubgruppe>() {
			public int compare(Utstyrsubgruppe o1, Utstyrsubgruppe o2) {
				if(o1.getNavn() == null && o2.getNavn() == null) {
					return 0;
				} else if(o1.getNavn() == null) {
					return -1;
				} else if(o2.getNavn() == null) {
					return 1;
				} else {
					return o1.getNavn().toLowerCase().compareTo(o2.getNavn().toLowerCase());
				}
			}
        });
        return list;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Utstyrmodell> getUtstyrmodellListFor(Long subgruppeId) {
        Query q = em.createNamedQuery(Utstyrmodell.FOR_SUBGRUPPE);
        q.setParameter("id", subgruppeId);
        List<Utstyrmodell> list = q.getResultList();
        Collections.sort(list, new Comparator<Utstyrmodell>() {
			public int compare(Utstyrmodell o1, Utstyrmodell o2) {
				if(o1.getNavn() == null && o2.getNavn() == null) {
					return 0;
				} else if(o1.getNavn() == null) {
					return -1;
				} else if(o2.getNavn() == null) {
					return 1;
				} else {
					return o1.getNavn().toLowerCase().compareTo(o2.getNavn().toLowerCase());
				}
			}
        });
        return list;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public KjoretoyUtstyr updateKjoretoyUtstyr(KjoretoyUtstyr ku) {
        return em.merge(ku);
    }
    
	/**
	 * {@inheritDoc}
	 */
    public KjoretoyUtstyr addKjoretoyUtstyr(KjoretoyUtstyr ku) {
        em.persist(ku);
        em.flush();
        em.refresh(ku);
        return ku;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public void removeKjoretoyUtstyr(KjoretoyUtstyr ku) {
    	ku = em.merge(ku);
        em.remove(ku);
        em.flush();
    }
    
    public void removeOrdre(Kjoretoyordre kjoretoyordre){
    	kjoretoyordre = em.merge(kjoretoyordre);
    	em.remove(kjoretoyordre);
    }
	/**
	 * {@inheritDoc}
	 */
    public void removeKjoretoyUtstyrWithId(long id) {
        removeKjoretoyUtstyr(em.find(KjoretoyUtstyr.class, id));
    }
    
	/**
	 * {@inheritDoc}
	 */
    public KjoretoyUtstyr refreshKjoretoyUtstyr(KjoretoyUtstyr ku) {
        return getKjoretoyUtstyr(ku.getId());
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Sensortype> getSensortypeList() {
        Query q = em.createNamedQuery(Sensortype.FIND_ALL);
        return q.getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Ioliste> getIoListeForKjoretoy(Long id) {
        Query q = em.createNamedQuery(Ioliste.FIND_FOR_KJORETOY);
        q.setParameter("id", id);
        q.setParameter("now", Clock.now());
        return q.getResultList();
    }

	/**
	 * {@inheritDoc}
	 */
    public void setEm(EntityManager em) {
        this.em = em;
    }

	/**
	 * {@inheritDoc}
	 */
    public EntityManager getEm() {
        return em;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public KjoretoyUtstyrIo addKjoretoyUtstyrIo(KjoretoyUtstyrIo kuio) {
    	// oppdater hele kj�ret�yet for � teste periodeoverlapping med frisk data
		if(kuio.getKjoretoyUtstyr() == null || kuio.getKjoretoyUtstyr().getKjoretoy() == null) {
			throw new IllegalStateException("KjoretoyUtstyrIo mangler referanser til sitt KjoretoyUtstyr eller sitt Kjoretoy") ;
		}
		/** TODO HACKY STUFF for å opprettholde referansene i cachen!!!!*/
		KjoretoyUtstyr merge = em.merge(kuio.getKjoretoyUtstyr());
		if (!merge.getKjoretoyUtstyrIoList().contains(kuio))
			merge.getKjoretoyUtstyrIoList().add(kuio);
		/** END TODO */
		
		
		Kjoretoy kjoretoy = em.find(Kjoretoy.class, kuio.getKjoretoyUtstyr().getKjoretoyId());
		em.refresh(kjoretoy);
		IOPortValidator ioPortValidator = new IOPortValidator(kjoretoy, kuio);
		if(!ioPortValidator.isValid()) {
			throw new IolisteOverlappingException("Ikke tillatt overlapping av to perioder for samme port: " + kuio);
		}
		
		//em.refresh(kuio.getKjoretoyUtstyr().getKjoretoy());
    	
    	// lagre den nye IOen
		kuio = em.merge(kuio);
//		em.persist(kuio);
    	em.flush();
    	em.refresh(kuio);
    	return kuio;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<KjoretoyUtstyrIo> getKjoretoyUtstyrIoListFor(Long KjoretoyUtstyrId) {
    	Query q = em.createNamedQuery(KjoretoyUtstyrIo.LIST_FOR_KJORETOYUTSTYR);
    	q.setParameter("id", KjoretoyUtstyrId);
    	return q.getResultList();
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Ioliste> getIolisteListForKjoretoy(Long kjoretoyId) {
    	List<Ioliste> ios = null;
    	Dfuindivid dfu = null;
    	
    	// prøv å hente DFU enheten først: den har prioritet ift. listen fra utstyrmodeellen
    	try {
    		dfu = getDfuindividForKjoretoy(kjoretoyId);
    	}
    	catch (NonUniqueResultException e) {
			dfu = null;
		}
    	// hent listen med iolister fra dfumodellen
    	if (dfu != null && dfu.getDfumodell() != null) {
            ios = dfu.getDfumodell().getIolisteList();
        }
    	
    	// alternativt hent fra modellen hvis enheten ikke registrert
    	if (ios == null || ios.size() == 0) {            
        	ios = getIoListeForKjoretoy(kjoretoyId);
        }
    	
    	if(ios == null) {
    		ios = new Vector<Ioliste>();
    	}
    	
    	return ios;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<Ioliste> getValidatedIolisteListForUtstyr(KjoretoyUtstyr utstyr) {
    	List<Ioliste> ios = getIolisteListForKjoretoy(utstyr.getKjoretoy().getId());
    	
    	// hent gruppen uansett om den er registrert i KJORETOY_UTSTYR
    	Utstyrgruppe gruppe = utstyr.getUtstyrgruppeForGui();
    	
    	// hvis gruppen har definert sin serieport, kan alle iolister v�re med
    	if(gruppe != null && gruppe.getSerieportBitNr() != null) {
    		return ios;
    	}
    	
    	// eller ta med kun de iolister som har BitNr definert
    	List<Ioliste> valids = new Vector<Ioliste>();
    	for(Ioliste io : ios) {
    		if(io.getBitNr() != null) {
    			valids.add(io);
    		}
    	}
    	return valids;
    }

	/**
	 * {@inheritDoc}
	 */
	public KjoretoyUtstyrIo updateKjoretoyUtstyrIo(KjoretoyUtstyrIo io) {
		// oppdater hele kj�ret�yet for � teste periodeoverlapping med frisk data
		if(io.getKjoretoyUtstyr() == null || io.getKjoretoyUtstyr().getKjoretoy() == null) {
			throw new IllegalStateException("KjoretoyUtstyrIo mangler referanser til sitt KjoretoyUtstyr eller sitt Kjoretoy") ;
		}
		
		Kjoretoy kjoretoy = em.find(Kjoretoy.class, io.getKjoretoyUtstyr().getKjoretoyId());
		em.refresh(kjoretoy);
		IOPortValidator ioPortValidator = new IOPortValidator(kjoretoy, io);
		if(!ioPortValidator.isValid()) {
			throw new IolisteOverlappingException("Ikke tillatt overlapping av to perioder for samme port: " + io);
		}
		//em.refresh(io.getKjoretoyUtstyr().getKjoretoy());
		
		KjoretoyUtstyrIo newIo = em.merge(io);
		em.flush();
		return newIo;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<KjoretoyUtstyrView> getKjoretoyUtstyrViewList(Long kjoretoyId) {
		List<KjoretoyUtstyr> utstyrList = getKjoretoyUtstyrListForKjoretoy(kjoretoyId);
		List<KjoretoyUtstyrView> views = new ArrayList<KjoretoyUtstyrView>();
		
		for(KjoretoyUtstyr ku : utstyrList) {
			views.add(new KjoretoyUtstyrView(ku));
		}
		return views;
	}
	
	/** {@inheritDoc} */
	public List<Kjoretoykontakttype> getKontakttypeList() {
		Query q = em.createNamedQuery(Kjoretoykontakttype.FIND_ALL);
		return q.getResultList();
	}
	/** {@inheritDoc} */
	public List<Utstyrprodusent> getUtstyrProdusentList() {
		Query q = em.createNamedQuery(Utstyrprodusent.FIND_ALL);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public Dokformat getDokformat(String ext) {
		Query q = em.createNamedQuery(Dokformat.FIND_BY_EXT);
		q.setParameter("ext", ext);
		try {
			Dokformat format = (Dokformat) q.getSingleResult();
			return format;
		}
		catch (Exception e) {
			return null;
		}
	}

	/** {@inheritDoc} */
	public Doktype getDoktype(String navn) {
		Query q = em.createNamedQuery(Doktype.FIND_NAME);
		q.setParameter("navn", navn);
		try {
			Doktype type = (Doktype) q.getSingleResult();
			return type;
		}
		catch (Exception e) {
			return null;
		}
	}

	/** {@inheritDoc} */
	public List<KontraktKjoretoy> getKontraktKjoretoyList(Long kjoretoyId) {
		Query q = em.createNamedQuery(KontraktKjoretoy.FIND_BY_KJORETOY);
        q.setParameter("id", kjoretoyId);
        return q.getResultList();
	}

	public List<Kjoretoyeier> getKjoretoyeierList(KjoretoyeierQueryFilter kjoretoyeierQueryFilter) {
		List<Kjoretoyeier> kjoretoyeierList = null;
		
		if(kjoretoyeierQueryFilter == null || !kjoretoyeierQueryFilter.hasValues()) {
			kjoretoyeierList = em.createNamedQuery(Kjoretoyeier.FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM Kjoretoyeier o ";
			if(kjoretoyeierQueryFilter.getNavn() != null && kjoretoyeierQueryFilter.getNavn().length() > 0) {
				whereClauses.add("UPPER(o.navn) LIKE :navn");
				params.put("navn", "%" + kjoretoyeierQueryFilter.getNavn().toUpperCase() + "%");
			}
			
			if(kjoretoyeierQueryFilter.getAdresse() != null && kjoretoyeierQueryFilter.getAdresse().length() > 0) {
				whereClauses.add("UPPER(o.adresse) LIKE :adresse");
				params.put("adresse", "%" + kjoretoyeierQueryFilter.getAdresse().toUpperCase() + "%");
			}
			
			if(kjoretoyeierQueryFilter.getPostnummer() != null && kjoretoyeierQueryFilter.getPostnummer().length() > 0) {
				Long postnummer = null;
				try {
					postnummer = Long.parseLong(kjoretoyeierQueryFilter.getPostnummer());
				} catch(NumberFormatException nfe) {
					postnummer = null;
				}
				
				if(postnummer != null) {
					whereClauses.add("o.postnummer = :postnummer");
					params.put("postnummer", postnummer);
				}
			}
			
			if(kjoretoyeierQueryFilter.getPoststed() != null && kjoretoyeierQueryFilter.getPoststed().length() > 0) {
				whereClauses.add("UPPER(o.poststed) LIKE :poststed");
				params.put("poststed", "%" + kjoretoyeierQueryFilter.getPoststed().toUpperCase() + "%");
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params); 
			
			kjoretoyeierList = query.getResultList();
		}
		
		return kjoretoyeierList;
	}
	
	private Query createQueryFromString(String queryString, List<String> whereClauses, Map<String, Object> params) {
		Iterator<String> whereIt = whereClauses.iterator();
		if(whereIt.hasNext()) queryString += " WHERE " + whereIt.next();
		while(whereIt.hasNext()) queryString += " AND " + whereIt.next();
		
		Query query = em.createQuery(queryString);
		for(Iterator<String> it = params.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			query.setParameter(key, params.get(key));
		}
		return query;
	}

	private Query createOredQueryFromString(String queryString, List<String> whereClauses, Map<String, Object> params) {
		Iterator<String> whereIt = whereClauses.iterator();
		if(whereIt.hasNext()) queryString += " WHERE " + whereIt.next();
		while(whereIt.hasNext()) queryString += " OR " + whereIt.next();
		
		Query query = em.createQuery(queryString);
		for(Iterator<String> it = params.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			query.setParameter(key, params.get(key));
		}
		return query;
	}
	
	/** {@inheritDoc} */
	public Installasjon updateInstallasjon(Installasjon installasjon, String brukerSign) {
		if (installasjon.getId()==null){//NY
			em.persist(installasjon);
			return installasjon;
		}
		else{
			installasjon.setEndretAv(brukerSign);
			installasjon.setEndretDato(Clock.now());
			
			installasjon = em.merge(installasjon);
			em.setFlushMode(FlushModeType.COMMIT);
	        em.flush();
	        em.refresh(installasjon.getKjoretoy());
			return installasjon;
		}
	}
	public void removeInstallasjon(Long installasjonId){
		Installasjon i = em.find(Installasjon.class, installasjonId);
		if (i.getMonteringsinfo()!=null){
			em.remove(i.getMonteringsinfo());
			em.flush();
		}
		i.getKjoretoy().getInstallasjonList().remove(i);
		em.remove(i);
	}

	public List<Dfuindivid> getDfuindividList(DfuindividQueryFilter dfuindividQueryFilter) {
		List<Dfuindivid> dfuindividList = null;
		
		if(dfuindividQueryFilter == null || !dfuindividQueryFilter.hasValues()) {
			dfuindividList = em.createNamedQuery(Dfuindivid.QUERY_FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM Dfuindivid o ";
			if(dfuindividQueryFilter.getSerienummer() != null && dfuindividQueryFilter.getSerienummer().length() > 0) {
				whereClauses.add("UPPER(o.serienummer) LIKE :serienummer");
				params.put("serienummer", "%" + dfuindividQueryFilter.getSerienummer().toUpperCase() + "%");
			}
			
			if(dfuindividQueryFilter.getTlfnummer() != null && dfuindividQueryFilter.getTlfnummer().length() > 0) {
				Long tlfnummer = null;
				try {
					tlfnummer = Long.parseLong(dfuindividQueryFilter.getTlfnummer());
				} catch(NumberFormatException nfe) {
					tlfnummer = null;
				}
				
				if(tlfnummer != null) {
					whereClauses.add("o.tlfnummer = :tlfnummer");
					params.put("tlfnummer", tlfnummer);
				}
			}
			
			if(dfuindividQueryFilter.getModellNavn() != null && dfuindividQueryFilter.getModellNavn().length() > 0) {
				whereClauses.add("UPPER(o.modellNavn) LIKE :modellNavn");
				params.put("modellNavn", "%" + dfuindividQueryFilter.getModellNavn().toUpperCase() + "%");
			}
			
			Query query = createQueryFromString(queryString, whereClauses, params); 
			
			dfuindividList = query.getResultList();
		}
		
		return dfuindividList;
	}
	
	/** {@inheritDoc} */
	public List<Kjoretoy> getKjoretoyList(KjoretoyQueryFilter kjoretoyQueryFilter) {
		List<Kjoretoy> kjoretoyList = null;
		
		if(kjoretoyQueryFilter == null || !kjoretoyQueryFilter.hasValues()) {
			kjoretoyList = em.createNamedQuery(Kjoretoy.FIND_ALL).getResultList();
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			String queryString = "SELECT o FROM Kjoretoy o ";
			if(kjoretoyQueryFilter.getEksterntNavn() != null && kjoretoyQueryFilter.getEksterntNavn().length() > 0) {
				whereClauses.add("UPPER(o.eksterntNavn) = :eksterntNavn");
				params.put("eksterntNavn", kjoretoyQueryFilter.getEksterntNavn().toUpperCase());
			}
			if(kjoretoyQueryFilter.getRegNr() != null && kjoretoyQueryFilter.getRegNr().length() > 0) {
				whereClauses.add("UPPER(o.regnr) = :regnr");
				params.put("regnr", kjoretoyQueryFilter.getRegNr().toUpperCase());
			}
			
			Query query = createOredQueryFromString(queryString, whereClauses, params); 
			
			kjoretoyList = query.getResultList();
		}
		
		return kjoretoyList;
	}

	public List<KjoretoyordreInfoV> searchKjoretoyordre(String arg, List<String> statusIdenter) {
		List<KjoretoyordreInfoV> retList = null;
		Query query = null;
		
		if(arg != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			List<String> whereClauses = new ArrayList<String>();
			
			String queryString = "SELECT o FROM KjoretoyordreInfoV o ";
			
			String argLower = arg.toLowerCase();
			
			whereClauses.add("LOWER(o.typeNavn) LIKE :typeNavn");
			params.put("typeNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.prosjektNr) LIKE :prosjektNr");
			params.put("prosjektNr", "%" +  argLower + "%");
		
			whereClauses.add("LOWER(o.signatur) LIKE :signatur");
			params.put("signatur", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.statusNavn) LIKE :statusNavn");
			params.put("statusNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.eksterntNavn) LIKE :eksterntNavn");
			params.put("eksterntNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.regnr) LIKE :regnr");
			params.put("regnr", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.kjoretoyTypeNavn) LIKE :kjoretoyTypeNavn");
			params.put("kjoretoyTypeNavn", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.modell) LIKE :modell");
			params.put("modell", "%" + argLower + "%");
			
			whereClauses.add("LOWER(o.navn) LIKE :navn");
			params.put("navn", "%" + argLower + "%");
			
			try {
				Long argLong = Long.valueOf(arg);
				whereClauses.add("o.maskinnummer = :maskinnummer");
				params.put("maskinnummer", argLong.toString());
				
				whereClauses.add("o.id = :id");
				params.put("id", argLong);
			} catch (NumberFormatException e) {
				// La være å lete i numeriske felt
			}
			
			query = createOredQueryFromString(queryString, whereClauses, params);
		} else {
			query = em.createNamedQuery(KjoretoyordreInfoV.QUERY_FIND_ALL);
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		retList = query.getResultList();
		
		//Fikk trøbbel med "where ... in ..." Bør kanskje sjekkes nærmere...
		if(statusIdenter != null) {
			Set<String> set = new HashSet<String>();
			set.addAll(statusIdenter);
			List<KjoretoyordreInfoV> tmpList = new ArrayList<KjoretoyordreInfoV>();
			for(KjoretoyordreInfoV k : retList) {
				if(statusIdenter.contains(k.getStatusIdent())) {
					tmpList.add(k);
				}
			}
			retList = tmpList;
		}
		
		return retList;
	}

	public Kjoretoyordre getKjoretoyordreForAdmin(Long id) {
		Kjoretoyordre kjoretoyordre = em.find(Kjoretoyordre.class, id);
		em.refresh(kjoretoyordre);
		
		/** START "Eager loading" **/
		for(Dfubestillingmaster dm : kjoretoyordre.getDfubestillingmasterList()) {
			for(DfulevBestilling dlb : dm.getDfulevBestillingList()) {
				dlb.getDfubestvarelinjerList().size();
			}
		}
		kjoretoyordre.getKjoretoyordreUtstyrList().size();
		if(kjoretoyordre.getDriftkontrakt() != null) {
			kjoretoyordre.getDriftkontrakt().getDriftsleder();
		}
		/** SLUTT "Eager loading" **/
		
		if(kjoretoyordre.getProsjektNr() != null) {
			try {
				Query q = em.createNamedQuery(ProsjektView.QUERY_FIND_BY_PROSJEKTNR).setParameter("prosjektnr", kjoretoyordre.getProsjektNr());
				
				ProsjektView selectedProsjektView = null;
				
				List<ProsjektView> prosjektViewList = q.getResultList();
				for(ProsjektView prosjektView : prosjektViewList) {
					if(prosjektView.getAnsvarFradato().before(Clock.now()) 
							&& (prosjektView.getAnsvarTildato() == null || prosjektView.getAnsvarTildato().after(Clock.now()))) {
						selectedProsjektView = prosjektView;
						break;
					}
				}
				
				if(selectedProsjektView != null) {
					kjoretoyordre.setProsjektnavn(selectedProsjektView.getProsjektnavn());
					kjoretoyordre.setAnsvar(selectedProsjektView.getAnsvar());
				} else {
					kjoretoyordre.setProsjektnavn(null);
					kjoretoyordre.setAnsvar(null);
				}
			} catch (Exception e) {
				kjoretoyordre.setProsjektnavn(null);
				kjoretoyordre.setAnsvar(null);
			}
		}
		
		return kjoretoyordre;
	}

	public Kjoretoyordre oppdaterKjoretoyordre(Kjoretoyordre kjoretoyordre, String bruker) {
		kjoretoyordre.getOwnedMipssEntity().setEndretAv(bruker);
		kjoretoyordre.getOwnedMipssEntity().setEndretDato(Clock.now());
		
		boolean sendEpost = false;
		if (kjoretoyordre.getId()!=null){
			Kjoretoyordre o = em.find(Kjoretoyordre.class, kjoretoyordre.getId());
			if (!(o.getSisteKjoretoyordrestatus().equals(kjoretoyordre.getSisteKjoretoyordrestatus()))){
				sendEpost=true;
			}
		}else{
			sendEpost = true;
		}
		kjoretoyordre = em.merge(kjoretoyordre);
		em.setFlushMode(FlushModeType.COMMIT);
		em.flush();
		if (sendEpost){
			epostHandler.sendEpostvarsel(kjoretoyordre);
		}
		
		return kjoretoyordre;
	}

	public List<Stroprodukt> getStroproduktValgbareList() {
		Query q = em.createNamedQuery(Stroprodukt.QUERY_FIND_FORETTERHENGENDE);
		return q.getResultList();
	}

	public List<Rodetilknytningstype> getRodetilknytningstypeValgbareList() {
		Query q = em.createNamedQuery(Rodetilknytningstype.FIND_ALL);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Dfuvarekatalog> getDfuvarekatalogList() {
		Query q = em.createNamedQuery(Dfuvarekatalog.FIND_ALL);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Dfumodell> getDfumodellListForLeverandor(String leverandorNavn) {
		Query q = em.createNamedQuery(Dfumodell.FIND_BY_LEVERANDOR);
		q.setParameter("leverandorNavn", leverandorNavn);
		return q.getResultList();
	}

	/** {@inheritDoc} */
	public List<Dfuvarekatalog> getDfuvarekatalogListForLeverandor(String leverandorNavn) {
		Query q = em.createNamedQuery(Dfuvarekatalog.FIND_BY_LEVERANDOR);
		q.setParameter("leverandorNavn", leverandorNavn);
		return q.getResultList();
	}

	public List<DfuInfoV> getDfuInfoVList(DfuInfoVQueryFilter dfuInfoVQueryFilter) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<String> whereClauses = new ArrayList<String>();
		String queryString = "SELECT o FROM DfuInfoV o ";
		if(dfuInfoVQueryFilter.getSerienummer() != null && dfuInfoVQueryFilter.getSerienummer().length() > 0) {
			whereClauses.add("UPPER(o.serienummer) LIKE :serienummer");
			params.put("serienummer", dfuInfoVQueryFilter.getSerienummer().toUpperCase() + "%");
		}
		
		if(dfuInfoVQueryFilter.getTlfnummer() != null && dfuInfoVQueryFilter.getTlfnummer().length() > 0) {
			whereClauses.add("UPPER(o.tlfnummer) LIKE :tlfnummer");
			params.put("tlfnummer", dfuInfoVQueryFilter.getTlfnummer().toUpperCase() + "%");
		}
		
		if(dfuInfoVQueryFilter.getLeverandorNavn() != null && dfuInfoVQueryFilter.getLeverandorNavn().length() > 0) {
			whereClauses.add("o.leverandorNavn = :leverandorNavn");
			params.put("leverandorNavn", dfuInfoVQueryFilter.getLeverandorNavn());
		}
		
		if(dfuInfoVQueryFilter.skjulMonterteDfu()) {
			whereClauses.add("(o.installasjonAktivFraDato IS NULL " +
					"OR (o.installasjonAktivTilDato IS NOT NULL AND o.installasjonAktivTilDato <= CURRENT_TIMESTAMP))");
		}
		
		Query query = createQueryFromString(queryString, whereClauses, params); 
		return query.getResultList();
	}

	/** {@inheritDoc} */
	public KjoretoyordreInfoV getKjoretoyordreInfoV(Long ordreId) {
		Query query = em.createNamedQuery(KjoretoyordreInfoV.QUERY_FIND_BY_ID).setParameter("id", ordreId); 
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return (KjoretoyordreInfoV)query.getSingleResult();
	}

	/** {@inheritDoc} */
	public Ioliste getIolisteForId(Long id) {
		return em.find(Ioliste.class, id);
	}
	
	/** {@inheritDoc} */
	public Sensortype getSensortypeForNavn(String navn) {
		return em.find(Sensortype.class, navn);
	}

	/** {@inheritDoc} */
	public Kjoretoy oppdaterKjoretoyUnderBestilling(Kjoretoy kjoretoy,
			List<KjoretoyordreUtstyr> kjoretoyordreUtstyrSomSkalSlettes) {
		
		for (Installasjon i:kjoretoy.getInstallasjonList()){
			if (i.getId()==null){
				Monteringsinfo mi = i.getMonteringsinfo();
				mi.setInstallasjon(null);
				i.setMonteringsinfo(null);
				em.persist(i);
				em.flush();
				
				mi.setId(i.getId());
				i.setMonteringsinfo(mi);
				mi.setInstallasjon(i);
			}
		}
		if(kjoretoyordreUtstyrSomSkalSlettes != null) {
			for(KjoretoyordreUtstyr kjoretoyordreUtstyr : kjoretoyordreUtstyrSomSkalSlettes) {
				KjoretoyordreUtstyr slettes = em.find(KjoretoyordreUtstyr.class, 
						new KjoretoyordreUtstyrPK(kjoretoyordreUtstyr.getOrdreId(), kjoretoyordreUtstyr.getUtstyrId()));
				em.remove(slettes);
			}
		}
		
		Kjoretoy k =  updateKjoretoy(kjoretoy);
		Kjoretoyordre ko = getSisteKjoretoyordreIListe(k.getKjoretoyordreList());
		epostHandler.sendEpostvarsel(ko);
		return k;
	}
	
	private Kjoretoyordre getSisteKjoretoyordreIListe(List<Kjoretoyordre> ordreList){
		Kjoretoyordre ko = null;
		for (Kjoretoyordre o:ordreList){
			if (ko==null||o.getOwnedMipssEntity().getOpprettetDato().after(ko.getOwnedMipssEntity().getOpprettetDato())){
				ko = o;
			}
		}
		return ko;
	}
	public List<KjoretoyDfuVO> getDfuMedFriksjon(Long kontraktId, Date fraTidspunkt, Date tilTidspunkt){
		StringBuilder q = new StringBuilder();
		q.append("select kjoretoy_id, dfu_id, to_char(fra_dato_tid, 'dd.mm.yyyy hh24:mi:ss'), to_char(til_dato_tid, 'dd.mm.yyyy hh24:mi:ss') ");//kjoretoy_id, dfu_id, fra_dato_tid, til_dato_tid 
		q.append("from table (friksjon_pk.kontrakt_kjoretoy_f (?1, to_date (?2, 'dd.mm.yyyy hh24:mi:ss'), to_date(?3, 'dd.mm.yyyy hh24:mi:ss')))");//kontraktid, fradato, tildato
		String fraDato = MipssDateFormatter.formatDate(fraTidspunkt, MipssDateFormatter.DATE_FORMAT);
		String tilDato = MipssDateFormatter.formatDate(tilTidspunkt, MipssDateFormatter.DATE_FORMAT);
		Query query = em.createNativeQuery(q.toString());
		query.setParameter(1, kontraktId);
		query.setParameter(2, fraDato);
		query.setParameter(3, tilDato);
		
		List<KjoretoyDfuVO> list = new NativeQueryWrapper<KjoretoyDfuVO>(query, KjoretoyDfuVO.class, "kjoretoyId", "dfuId", "fraDato", "tilDato").getWrappedList();
		for (KjoretoyDfuVO k:list){
			Dfuindivid d = em.find(Dfuindivid.class, k.getDfuId());
			Kjoretoy kj = em.find(Kjoretoy.class, k.getKjoretoyId());
			k.setDfu(d);
			k.setKjoretoy(kj);
		}
		
		return list;
	}
	
	public Long getDatekIdForKjoretoyUtstyr(KjoretoyUtstyr  ko){
		Long utstyrgruppeId = ko.getUtstyrgruppe()!=null?ko.getUtstyrgruppe().getId():null;
		Long utstyrsubgruppeId = ko.getUtstyrsubgruppe()!=null?ko.getUtstyrsubgruppe().getId():null;
		Long utstyrModellId = ko.getUtstyrmodell()!=null?ko.getUtstyrmodell().getId():null;
		
		StringBuilder query = new StringBuilder();
		int pi = 1;
		query.append("select datek_id from datek_utstyr_link ");
		if (utstyrgruppeId==null){
			query.append("where utstyrgruppe_id is null ");
		}else{
			query.append("where utstyrgruppe_id = ?"+ pi++ +" ");
		}
		
		if (utstyrsubgruppeId==null){
			query.append("and utstyrsubgruppe_id is null ");
		}else{
			query.append("and utstyrsubgruppe_id = ?"+pi++ +" ");
		}
		if (utstyrModellId==null){
			query.append("and utstyrmodell_id is null ");
		}else{
			query.append("and utstyrmodell_id = ?"+pi++ +" ");
		}
		
		
		Query q = em.createNativeQuery(query.toString());
		int param = 1;
		if (utstyrgruppeId!=null)
			q.setParameter(param++, utstyrgruppeId);
		if (utstyrsubgruppeId!=null)
			q.setParameter(param++, utstyrsubgruppeId);
		if (utstyrModellId!=null)
			q.setParameter(param++, utstyrModellId);
		try{
			BigDecimal value =  (BigDecimal)q.getSingleResult();
			return value.longValue();
		}catch (NoResultException e){
			return null;
		}
		
	}

	public static void main(String[] args) {
		DefaultHttpClient client = new DefaultHttpClient();
		String url = "http://mest-ap004.vegprod.no:8080/Mesta/integration/unit/123123123/operating";
		HttpPost method = new HttpPost(url);
		try {
			HttpResponse response = client.execute((HttpUriRequest) method);
			System.out.println(response.getStatusLine().getStatusCode());
			InputStream rstream = response.getEntity().getContent();
			byte[] bytes = StreamUtils.getBytes(rstream);
			String s = new String(bytes);
			System.out.println(s);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public String prodsettKjoretoyDatek(String serienummer) {
		String urlTemp = konfig.hentEnForApp("studio.montering", "urlTemplateDatek").getVerdi();
		urlTemp = urlTemp.replace("<sn>", serienummer);
		
		DefaultHttpClient client = new DefaultHttpClient();
		String url = urlTemp;
		HttpPost method = new HttpPost(url);
		try {
			HttpResponse response = client.execute((HttpUriRequest) method);
			InputStream rstream = response.getEntity().getContent();
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode!=200){
				byte[] bytes = StreamUtils.getBytes(rstream);
				String s = new String(bytes);
				return s;
			}
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}	
	public void sendFeilEpost(String body, String subject) {
		String til = konfig.hentEnForApp("studio.mipss", "supportEpost").getVerdi();
		if (til == null || til.isEmpty()) {
			til = "mipss@mesta.no";
		}
		epostHandler.sendEpost(subject, body, til);
	}

	@Override
	public List<Digidok> getDigiDok() {
		return em.createNamedQuery(Digidok.QUERY_FIND_ALL).getResultList();
	}

	@Override
	public Kjoretoyeier getKjoretoyeierById(Long id) {
		try{
			Query q = em.createNamedQuery(Kjoretoyeier.FIND_BY_ID);
			q.setParameter("id", id);
			return (Kjoretoyeier) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
