package no.mesta.mipss.konfigparam;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.applikasjon.Konfigparam;
@Remote
public interface KonfigParamLocal {

	/** {@inheritDoc} */
	List<Konfigparam> hentAlleKonfigparam();

	/** {@inheritDoc} */
	List<Konfigparam> hentKonfigForApplikasjon(String app);

	/** {@inheritDoc} */
	Konfigparam lagreKonfig(Konfigparam konfig);

	/** {@inheritDoc} */
	Konfigparam oppdaterKonfig(Konfigparam konfig);

	/** {@inheritDoc} */
	void slettKonfig(String app, String navn);

	/** {@inheritDoc} */
	Konfigparam hentEnForApp(String app, String navn);

}