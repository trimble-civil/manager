package no.mesta.mipss.konfigparam;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.KonfigparamPK;
import no.mesta.mipss.persistence.applikasjon.Konfigparamlogg;
import no.mesta.mipss.persistence.config.HintValues;
import no.mesta.mipss.persistence.config.QueryHints;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Brukes til å lese opp konfig fra databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Stateless(name = KonfigParam.BEAN_NAME, mappedName="ejb/"+KonfigParam.BEAN_NAME)

public class KonfigParamBean implements KonfigParam, KonfigParamLocal {
	private static final Logger log = LoggerFactory.getLogger(KonfigParamBean.class);

	@PersistenceContext(unitName = "mipssPersistence")
	EntityManager em;

	public KonfigParamBean() {
	}
	
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<String> hentApplikasjonListe() {
		log.debug("hentApplikasjonListe()");
		Query query = em.createNamedQuery(Konfigparam.QUERY_GET_ALL_APPLICATIONS);
		return query.getResultList();
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Konfigparam> hentAlleKonfigparam() {
		log.debug("hentAlleKonfigparam()");
		Query query = em.createNamedQuery(Konfigparam.QUERY_FIND_ALL);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	/** {@inheritDoc} */
	public Konfigparam hentEnForApp(String app, String navn) {
		log.debug("hentEnForApp({}, {})", app, navn);
		Query query = em.createNamedQuery(Konfigparam.QUERY_GET_ONE);
		query.setParameter("app", app);
		query.setParameter("navn", navn);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		Konfigparam konfig = null;
		try {
			konfig = (Konfigparam) query.getSingleResult();
		} catch (NoResultException e) {
			log.error("Unable to find konfigparam for app={}, name={}", app, navn);
			throw new KonfigParamException(e);
		}

		return konfig;
	}
	
	public String hentVerdiForApp(String app, String navn){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT VERDI ");
		sb.append("FROM KONFIGPARAM WHERE APPLIKASJON = ?1 AND NAVN = ?2");
		Query q = em.createNativeQuery(sb.toString());
		q.setParameter(1, app);
		q.setParameter(2, navn);
		try{
			String res = (String)q.getSingleResult();
		return res;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Konfigparam henEnForApp(KonfigparamPK key) {
		log.debug("hentEnForApp(" + key.getApplikasjon() + ", " + key.getNavn() + ") (primary key class used)");
		Query query = em.createNamedQuery(Konfigparam.QUERY_GET_ONE);
		query.setParameter("app", key.getApplikasjon());
		query.setParameter("navn", key.getNavn());
		
		Konfigparam konfig = null;
		
		try {
			konfig = (Konfigparam) query.getSingleResult();
		} catch (NoResultException e) {
			e = null; // Det er ok at det ikke kommer noen resultat
		}
		
		return konfig;
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Konfigparam> hentKonfigForApplikasjon(String app) {
		log.debug("hentKonfigForApplikasjon(" + app + ")");
		Query query = em.createNamedQuery(Konfigparam.QUERY_GET_FOR_APPLICATION);
		query.setParameter("app", app);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	/** {@inheritDoc} */
	public Konfigparam lagreKonfig(Konfigparam konfig) {
		if (log.isDebugEnabled()) {
			log.debug("lagreKonfig(" + konfig + ")");
		}
		em.persist(konfig);
		return konfig;
	}

	/** {@inheritDoc} */
	public Konfigparam oppdaterKonfig(Konfigparam konfig) {
		if (log.isDebugEnabled()) {
			log.debug("oppdaterKonfig(" + konfig + ")");
		}
		konfig = em.merge(konfig);
		return konfig;
	}

	/** {@inheritDoc} */
	public void slettKonfig(String app, String navn) {
		log.debug("slettKonfig(" + app + "," + navn + ")");
		Query query = null;

		if (StringUtils.isBlank(app) && StringUtils.isBlank(navn)) {
			throw new IllegalArgumentException("All params cannot be blank!");
		} else if (StringUtils.isNotBlank(app) && StringUtils.isBlank(navn)) {
			query = em.createNamedQuery(Konfigparam.DELETE_FOR_APPLICATION);
			query.setParameter("app", app);
		} 
		 else if (StringUtils.isNotBlank(app) && StringUtils.isNotBlank(navn)) {
			query = em.createNamedQuery(Konfigparam.DELETE);
			query.setParameter("app", app);
			query.setParameter("navn", navn);
		 }
		 else {
			throw new IllegalArgumentException("Illegal use of arguments!");
		}

		query.executeUpdate();
	}
	
	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<Konfigparamlogg> hentLoggForKonfigparam(Konfigparam p){
		return em.createNamedQuery(Konfigparamlogg.QUERY_GET_HISTORY_FOR_APP_NAVN)
		.setParameter("applikasjon", p.getApplikasjon())
		.setParameter("navn", p.getNavn()).getResultList();
	}
}
