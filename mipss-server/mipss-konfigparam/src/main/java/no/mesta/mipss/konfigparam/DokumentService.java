package no.mesta.mipss.konfigparam;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.dokarkiv.Dokument;

@Remote
public interface DokumentService {
	public static final String BEAN_NAME = "DokumentBean";
	
	List<Dokument> getBrukerstotteDokumenter();
}
