package no.mesta.mipss.konfigparam;

public class KonfigParamException extends RuntimeException {

	
	public KonfigParamException() {
	}

	public KonfigParamException(String message) {
		super(message);
	}

	public KonfigParamException(Throwable cause) {
		super(cause);
	}

	public KonfigParamException(String message, Throwable cause) {
		super(message, cause);
	}

}
