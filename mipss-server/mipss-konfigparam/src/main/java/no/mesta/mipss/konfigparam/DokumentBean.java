package no.mesta.mipss.konfigparam;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import no.mesta.mipss.persistence.dokarkiv.Dokument;

@Stateless(name = DokumentService.BEAN_NAME, mappedName="ejb/"+DokumentService.BEAN_NAME)
public class DokumentBean implements DokumentService{
	@PersistenceContext(unitName = "mipssPersistence")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Dokument> getBrukerstotteDokumenter() {
		Query q = em.createNamedQuery(Dokument.FIND_BY_DOKTYPE).setParameter("doktypenavn", Dokument.DOKTYPE_BRUKERSTOTTE);
		return q.getResultList();
	}

}
