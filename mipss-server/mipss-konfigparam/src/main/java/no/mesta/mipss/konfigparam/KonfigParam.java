package no.mesta.mipss.konfigparam;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.KonfigparamPK;
import no.mesta.mipss.persistence.applikasjon.Konfigparamlogg;

/**
 * Brukes til å lese opp konfig fra databasen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@Remote
public interface KonfigParam {
	public static final String BEAN_NAME = "KonfigParamBean";

	/**
	 * Henter alle applikasjoner som er tilordnet konfig params.
	 * 
	 * @return liste med applikasjoner tilordnet ett eller flere konfig params.
	 */
	public List<String> hentApplikasjonListe();
	
	/**
	 * Henter alle konfigparam fra databasen
	 * 
	 * @return
	 */
	public List<Konfigparam> hentAlleKonfigparam();
	
	String hentVerdiForApp(String app, String navn);
	/**
	 * Henter en generell konfigurasjon for en app
	 * 
	 * @param app
	 * @param navn
	 * @return
	 */
	public Konfigparam hentEnForApp(String app, String navn);
	
	
	/**
	 * Henter konfigurasjons parameter for en applikasjon
	 * 
	 * @param app - applikasjonens navn
	 * @param navn - parameterets navn
	 * @return KonfigParam entitet med parameterets verdi.
	 */
	public Konfigparam henEnForApp(KonfigparamPK key);


	/**
	 * Henter alle konfigparam fra databasen for en app/modul
	 * 
	 * @param app
	 * @return
	 */
	public List<Konfigparam> hentKonfigForApplikasjon(String app);

	/**
	 * Lagrer en konfigurasjon til databasen
	 * 
	 * @param konfig
	 * @return
	 */
	public Konfigparam lagreKonfig(Konfigparam konfig);

	/**
	 * Oppdaterer en konfigurasjon i databasen
	 * 
	 * @param konfig
	 * @return
	 */
	public Konfigparam oppdaterKonfig(Konfigparam konfig);

	/**
	 * Sletter en konfigurasjon i databasen
	 * 
	 * @param app
	 * @param navn
	 * @param signatur
	 */
	public void slettKonfig(String app, String navn);
	
	/**
	 * Henter konfigparams fra konfigparamlogg tabellen
	 * @param p
	 * @return
	 */
	List<Konfigparamlogg> hentLoggForKonfigparam(Konfigparam p);
}
