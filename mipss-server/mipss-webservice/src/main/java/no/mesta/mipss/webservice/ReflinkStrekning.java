package no.mesta.mipss.webservice;

import java.awt.Point;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

@SuppressWarnings("serial")
public class ReflinkStrekning implements Serializable{
	private int reflinkId;
	private double fra;
	private double til;
	private Point[] vectors;
	private List<Veinettveireferanse> veirefList ;
	
	public String getVeikategori(){
		String kat = "";
		if (veirefList.size()>0){
			kat = veirefList.get(0).getVeikategori();
		}
		return kat;
	}
	/**
	 * @return the reflinkId
	 */
	public int getReflinkId() {
		return reflinkId;
	}
	/**
	 * @param reflinkId the reflinkId to set
	 */
	public void setReflinkId(int reflinkId) {
		this.reflinkId = reflinkId;
	}
	/**
	 * @return the fra
	 */
	public double getFra() {
		return fra;
	}
	/**
	 * @param fra the fra to set
	 */
	public void setFra(double fra) {
		this.fra = fra;
	}
	/**
	 * @return the til
	 */
	public double getTil() {
		return til;
	}
	/**
	 * @param til the til to set
	 */
	public void setTil(double til) {
		this.til = til;
	}
	/**
	 * @return the vectors
	 */
	public Point[] getVectors() {
		return vectors;
	}
	/**
	 * @param vectors the vectors to set
	 */
	public void setVectors(Point[] vectors) {
		this.vectors = vectors;
	}
	
	public boolean equals(Object o){
		if (o==this) return true;
		if (!(o instanceof ReflinkStrekning)) return false;
		ReflinkStrekning other = (ReflinkStrekning)o;
		return (	reflinkId == other.getReflinkId() && 
					fra == other.getFra() && 
					til == other.getTil());
	}
	public void setVeirefList(List<Veinettveireferanse> veirefList) {
		this.veirefList = veirefList;
	}
	public List<Veinettveireferanse> getVeirefList() {
		return veirefList;
	}
}
