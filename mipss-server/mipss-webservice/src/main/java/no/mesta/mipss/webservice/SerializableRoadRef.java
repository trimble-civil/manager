package no.mesta.mipss.webservice;

import no.mesta.mipss.webservice.service.Veireferanse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SerializableRoadRef implements Serializable {

    private final int county;
    private final int municipality;
    private final int category;
    private final int status;
    private final int statusOver;
    private final int number;
    private final int fromParcel;
    private final int fromMeter;
    private final int toParcel;
    private final int toMeter;

    public SerializableRoadRef(int county, int municipality, int category, int status, int statusOver, int number, int fromParcel, int fromMeter, int toParcel, int toMeter) {
        this.county = county;
        this.municipality = municipality;
        this.category = category;
        this.status = status;
        this.statusOver = statusOver;
        this.number = number;
        this.fromParcel = fromParcel;
        this.fromMeter = fromMeter;
        this.toParcel = toParcel;
        this.toMeter = toMeter;
    }

    public SerializableRoadRef(Veireferanse roadRef) {
        county = roadRef.getFylkenummer();
        municipality = roadRef.getKommunenummer();
        category = roadRef.getVegkategori();
        status = roadRef.getVegstatus();
        statusOver = roadRef.getVegstatusoverordnet();
        number = roadRef.getVegnummer();
        fromParcel = roadRef.getFrahp();
        fromMeter = roadRef.getFraMeter();
        toParcel = roadRef.getTilhp();
        toMeter = roadRef.getTilMeter();
    }

    public int getCounty() {
        return county;
    }

    public int getMunicipality() {
        return municipality;
    }

    public int getCategory() {
        return category;
    }

    public int getStatus() {
        return status;
    }

    public int getNumber() {
        return number;
    }

    public int getFromParcel() {
        return fromParcel;
    }

    public int getFromMeter() {
        return fromMeter;
    }

    public int getToParcel() {
        return toParcel;
    }

    public int getToMeter() {
        return toMeter;
    }

    public int getStatusOver() {
        return statusOver;
    }

    public static List<SerializableRoadRef> createSerializableRoadRef(List<Veireferanse> roadRefs) {
        List<SerializableRoadRef> result = new ArrayList<>();

        for(Veireferanse roadRef : roadRefs) {
            result.add(new SerializableRoadRef(roadRef));
        }

        return result;
    }

    public static List<Veireferanse> createWebserviceRoadRefs(List<SerializableRoadRef> roadRefs) {
        List<Veireferanse> result = new ArrayList<>();

        for(SerializableRoadRef roadRef : roadRefs) {
            result.add(roadRef.toWebserviceObject());
        }

        return result;
    }

    public Veireferanse toWebserviceObject() {

        Veireferanse veireferanse = new Veireferanse();
        veireferanse.setFrahp(getFromParcel());
        veireferanse.setFraMeter(getFromMeter());
        veireferanse.setFylkenummer(getCounty());
        veireferanse.setKommunenummer(getMunicipality());
        veireferanse.setTilhp(getToParcel());
        veireferanse.setTilMeter(getToMeter());
        veireferanse.setVegkategori(getCategory());
        veireferanse.setVegnummer(getNumber());
        veireferanse.setVegstatus(getStatus());
        veireferanse.setVegstatusoverordnet(getStatusOver());
        return veireferanse;
    }
}
