package no.mesta.mipss.webservice;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.swing.event.ListSelectionEvent;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.service.ArrayOfReflinkSeksjon;
import no.mesta.mipss.webservice.service.ArrayOfVeireferanse;
import no.mesta.mipss.webservice.service.GisServices;
import no.mesta.mipss.webservice.service.GisServicesSoap;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.Veireferanse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mipss1Converter {
	
	private Logger log = LoggerFactory.getLogger(Mipss1Converter.class);
    private GisServicesSoap gisService;
	private final Konfigparam wsdlLocation;
    
    public Mipss1Converter(Konfigparam wsdlLocation){
		this.wsdlLocation = wsdlLocation;
    	
    }
	public static void main(String[] args) {
		//NvdbWebservice n = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
		
		Properties systemSettings = System.getProperties();
		systemSettings.put("http.proxyHost", "144.193.101.139");
		systemSettings.put("http.proxyPort", "8080");
//		systemSettings.put(HTTP_NONPROXYHOST, params.hentEnForApp(MIPSS_SERVER, HTTP_NONPROXYHOST).getVerdi());
		System.setProperties(systemSettings);
		
		Veireferanse v = new Veireferanse();
		v.setFylkenummer(2);
		v.setKommunenummer(0);
		v.setVegkategori('E');
		v.setVegstatus('V');
		v.setVegnummer(18);
		v.setFrahp(1);
		v.setTilhp(1);
		v.setFraMeter(0);
		v.setTilMeter(100);
		/*
		v.setFylkesnummer(Long.valueOf(14));
		v.setKommunenummer(Long.valueOf(0));
		v.setVeikategori("R");
		v.setVeistatus("V");
		v.setVeinummer(Long.valueOf(5));
		v.setHp(Long.valueOf(7));
		v.setFraKm(Double.valueOf(0));
		v.setTilKm(Double.valueOf(549));
		*/
		Konfigparam wsdl = new Konfigparam();
		
		wsdl.setVerdi("http://10.60.77.203/mipssws/GisServices.asmx?WSDL");
		Mipss1Converter c = new Mipss1Converter(wsdl);
		List<Veireferanse> vl = new ArrayList<Veireferanse>();
		vl.add(v);
		List<Veinettreflinkseksjon> vr = c.getVeinettreflinkseksjonFromVeireferanseList(vl, "test");
		
		System.out.println(vr);

	}
    
    /**
     * Lager en liste av veinettreflinkseksjoner med tilh�rende veinettveireferanser. 
     * 
     * @param veireferanseList
     * @return
     */
	public List<Veinettreflinkseksjon> getVeinettreflinkseksjonFromVeireferanseList(List<Veireferanse> veireferanseList, String signatur){
		log.debug("getVeinettreflinkseksjonFromVeirefereanseList()"+veireferanseList.size());
    	ArrayList<Veinettreflinkseksjon> veinettreflinkseksjonList = new ArrayList<Veinettreflinkseksjon>();
    	
    	for (Veireferanse vei : veireferanseList){
    		List<ReflinkSeksjon> reflinkSeksjonList = getGisService().getReflinkSectionsWithinRoadref(vei).getReflinkSeksjon();
    		log.debug(" F="+vei.getFylkenummer()+" K="+vei.getKommunenummer()+" Kat="+(char)(vei.getVegkategori())+" StO="+(char)(vei.getVegstatusoverordnet())+" St="+(char)(vei.getVegstatus())+" NR="+vei.getVegnummer()+" FraHP="+vei.getFrahp()+" FraKM="+vei.getFraMeter()+" TilHp="+vei.getTilhp()+" TilKm="+vei.getTilMeter());
    		log.debug("\tantall reflinkseksjoner="+reflinkSeksjonList.size());
    		Veinettreflinkseksjon veinettreflinkseksjon = null;
    		if (reflinkSeksjonList.size()==1){
    			veinettreflinkseksjon = getVeinettreflinkseksjon(reflinkSeksjonList.get(0), signatur);
    			Veinettveireferanse vv = getVeinettveireferanse(vei, signatur);
    			vv.setVeinettreflinkseksjon(veinettreflinkseksjon);
    			
    			veinettreflinkseksjon.getVeinettveireferanseList().add(vv);
    			veinettreflinkseksjonList.add(veinettreflinkseksjon);
    			
    		}else if(reflinkSeksjonList.size()!=0){
    			log.debug("Mer enn 1 reflinkseksjon tilbake");
    			for (ReflinkSeksjon ref:reflinkSeksjonList){
    				
    				ArrayOfReflinkSeksjon reflinkArray = new ArrayOfReflinkSeksjon();
    				reflinkArray.getReflinkSeksjon().add(ref);

    				//outparameterer fra webservicen
    				Holder<ArrayOfVeireferanse> results = new Holder<ArrayOfVeireferanse>();
    				Holder<Boolean> historicFlag = new Holder<Boolean>();
    				getGisService().getRoadrefsWithinReflinkSections(reflinkArray, results, historicFlag);
    				if (results.value!=null){
	    				List<Veireferanse> veireferanse = results.value.getVeireferanse();
	    				
	    				//historicFlag er true dersom veinettet som sendes inn er en del av et veinett som 
	    				//er endret i NVDB
	    				
	    				//lagre ref
	    				veinettreflinkseksjon = getVeinettreflinkseksjon(ref, signatur);
	    				for (Veireferanse v:veireferanse){
	    					Veinettveireferanse vv = getVeinettveireferanse(v, signatur);
	    					vv.setVeinettreflinkseksjon(veinettreflinkseksjon);
	    					veinettreflinkseksjon.getVeinettveireferanseList().add(vv);
	    				}
	    				log.debug("antall veireferanser:"+veireferanse.size());
	    				veinettreflinkseksjonList.add(veinettreflinkseksjon);
    				}else{
    					log.error("Results from webservice is null, this is wrong and should not happen! reflinkseksjon: id="+ref.getReflinkId()+" fra="+ref.getFra()+" til="+ref.getTil());
    				}
    			}
    		}
    	}
    	return veinettreflinkseksjonList;
    }
	
    /**
     * Gj�r on et ReflinkSeksjon objekt fra webservice til et Veinettreflinkseksjon objekt
     * til databasen.
     * @param reflinkSeksjon
     * @return
     */
    private Veinettreflinkseksjon getVeinettreflinkseksjon(ReflinkSeksjon reflinkSeksjon, String signatur){
    	Veinettreflinkseksjon v = new Veinettreflinkseksjon();
    	v.setFra(reflinkSeksjon.getFra());
    	v.setTil(reflinkSeksjon.getTil());
    	v.setReflinkIdent(new Long(reflinkSeksjon.getReflinkId()));
    	v.getOwnedMipssEntity().setOpprettetAv(signatur);
    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	return v;
    }
    /**
     * Konverterer et Veireferanse object fra webservice til et Veinettveireferanse objekt
     * til databasen 
     * @param veireferanse
     * @return
     */
    private Veinettveireferanse getVeinettveireferanse(Veireferanse veireferanse, String signatur){
    	if (veireferanse==null)
    		throw new IllegalArgumentException("Veireferanse kan ikke v�re null!");
    	
    	Veinettveireferanse v= new Veinettveireferanse();
    	v.setFraKm((((double)veireferanse.getFraMeter())/1000));
    	v.setFylkesnummer(new Long(veireferanse.getFylkenummer()));
    	v.setHp(new Long(veireferanse.getFrahp())); //TODO her er det noe muffens.. finnes bare en HP i databasen WS returnerer fra og til HP
    	v.setKommunenummer(new Long(veireferanse.getKommunenummer()));
    	v.setTilKm((((double)veireferanse.getTilMeter())/1000));
    	v.setVeikategori((char)veireferanse.getVegkategori()+"");
    	v.setVeinummer(new Long(veireferanse.getVegnummer()));
    	v.setVeistatus((char)veireferanse.getVegstatus()+"");
    	v.getOwnedMipssEntity().setOpprettetAv(signatur);
    	v.getOwnedMipssEntity().setOpprettetDato(Clock.now());
    	return v;
    }
    
//    /**
//     * Konverterer et VeireferanseVO object til et Veinettveireferanse objekt
//     * @param veireferanse
//     * @return
//     */
//    private Veinettveireferanse getVeinettveireferanse(VeireferanseVO veireferanse){
//    	Veinettveireferanse v= new Veinettveireferanse();
//    	v.setFraKm((double)(veireferanse.getFraMeter()/1000));
//    	v.setFylkesnummer(new Long(veireferanse.getFylkenummer()));
//    	v.setHp(new Long(veireferanse.getFrahp())); //TODO her er det noe muffens.. finnes bare en HP i databasen WS returnerer fra og til HP
//    	v.setKommunenummer(new Long(veireferanse.getKommunenummer()));
//    	v.setTilKm((double)(veireferanse.getTilMeter()/1000));
//    	v.setVeikategori((char)veireferanse.getVegkategori());
//    	v.setVeinummer(new Long(veireferanse.getVegnummer()));
//    	v.setVeistatus((char)veireferanse.getVegstatus());
//    	
//    	return v;
//    }
    private GisServicesSoap getGisService(){
		URL baseUrl = no.mesta.mipss.webservice.service.GisServices.class.getResource(".");
		try{
			URL url = new URL(baseUrl, wsdlLocation.getVerdi());
			
	    	if (gisService==null){
	    		gisService = new GisServices(url, new QName("http://www.mesta.no/GisEngineWebServices/", "GisServices")).getGisServicesSoap();
	    	}else{
	    		if (!wsdlLocation.equals(this.wsdlLocation)){
	    			gisService= new GisServices(url, new QName("http://www.mesta.no/GisEngineWebServices/", "GisServices")).getGisServicesSoap();
	    		}
	    	}
    	} catch (Exception e){
    		log.error("Webservice not available!!!", e);
    	}
    	return gisService;
    }
//    private GisServicesSoap getGisService(){
//    	
//    	
//    	if (gisService==null){
//    		gisService = new GisServices().getGisServicesSoap();
//    	}
//    	
//    	return gisService;
//    }
}
