package no.mesta.mipss.webservice;

import java.util.Date;

import javax.ejb.Remote;


@Remote
public interface Produksjon {
    public static final String BEAN_NAME = "ProduksjonBean";
//    /**
//     * Henter produserte strekninger for en dfu innenfor et gitt tidsrom
//     * @param dfuId id til dfu
//     * @param fraTidspunkt f�rste tidspunkt for s�king etter produksjon
//     * @param tilTidspunkt andre tidspunkt for s�king etter produksjon
//     * @return
//     */
//    List<ProdReflinkStrekning> getProduksjonStrekninger(Long dfuId, Date fraTidspunkt, Date tilTidspunkt);
////    List<Prodstrekning> getProduksjonStrekningList(Long dfuId, Date fraTidspunkt, Date tilTidspunkt);
//    
//    /**
//     * Henter en liste med prodpunkter for en dfu innenfor et gitt tidsrom
//     * @param dfuId id til dfu
//     * @param fraTidspunkt f�rste tidspunkt for s�king etter produksjon
//     * @param tilTidspunkt siste tidspunkt for s�king etter produksjon
//     * @return
//     */
//    List<ProdpunktVO> getProduksjonPunkt(Long dfuId, Date fraTidspunkt, Date tilTidspunkt);
    
    /**
     * Dette er kun en test for kv�rnemotoren... 
     */
    void testAvProdReflinkseksjon(Date fra, Date til);
    long testKonverterKontraktveinett(Long id);
}
