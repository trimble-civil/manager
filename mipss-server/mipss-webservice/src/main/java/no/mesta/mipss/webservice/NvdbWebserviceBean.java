package no.mesta.mipss.webservice;

import no.mesta.mipss.konfigparam.KonfigParamLocal;
import no.mesta.mipss.persistence.kvernetf1.Prodreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import java.awt.*;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

@Stateless (name = NvdbWebservice.BEAN_NAME, mappedName="ejb/"+NvdbWebservice.BEAN_NAME)
public class NvdbWebserviceBean implements NvdbWebservice, NvdbWebserviceLocal {
	private static final Logger log = LoggerFactory.getLogger(NvdbWebservice.class);
	private GisServicesSoap gisService;
	@EJB
	private KonfigParamLocal konfig;
	private String wsdlLocation;
	
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getFeaturesWithinReflinkSectionsFromProdreflinkseksjon(int, java.util.List, java.util.List)
	 */
	public void getFeaturesWithinReflinkSectionsFromProdreflinkseksjon(int featureTypeId, List<Integer> attributeTypeIds, List<Prodreflinkseksjon> reflinkSeksjon){
		//TODO implement this..
		throw new RuntimeException("Not yet implemented");
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getFeaturesWithinReflinkSectionsFromVeinettreflinkseksjon(int, java.util.List, java.util.List)
	 */
	public List<GetFeaturesWithinReflinkSectionsResult> getFeaturesWithinReflinkSectionsFromVeinettreflinkseksjon(int featureTypeId, List<Integer> attributeTypeIds, List<Veinettreflinkseksjon> reflinkSeksjon){
		List<ReflinkSeksjon> reflinkSeksjoner = NvdbTranslator.translateFromVeinettreflinkseksjonToReflinkSeksjonList(reflinkSeksjon);
		ArrayOfReflinkSeksjon ar = new ArrayOfReflinkSeksjon();
		ar.getReflinkSeksjon().addAll(reflinkSeksjoner);
		ArrayOfInt aint = new ArrayOfInt();
		aint.getInt().addAll(attributeTypeIds);
		ArrayOfGetFeaturesWithinReflinkSectionsResult res = getGisService().getFeaturesWithinReflinkSections(featureTypeId, aint, ar);
		List<GetFeaturesWithinReflinkSectionsResult> features = res.getGetFeaturesWithinReflinkSectionsResult();
		if (features.isEmpty())
			return null;
		return features;
	}

	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getGeometryWithinReflinkSectionsFromProdreflinkseksjon(java.util.List)
	 */
	public List<ReflinkStrekning> getGeometryWithinReflinkSectionsFromProdreflinkseksjon(List<Prodreflinkseksjon> reflinkSeksjon){
		List<ReflinkSeksjon> reflinkSeksjoner = NvdbTranslator.translateFromProdreflinkSeksjonToReflinkseksjon(reflinkSeksjon);
		return getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinkSeksjoner));
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(java.util.List)
	 */
	public List<ReflinkStrekning> getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(List<Veinettreflinkseksjon> reflinkSeksjon){
		List<ReflinkSeksjon> reflinkSeksjoner = NvdbTranslator.translateFromVeinettreflinkseksjonToReflinkSeksjonList(reflinkSeksjon);
		return getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinkSeksjoner));
	}
	
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getGeometryWithinReflinkSections(java.util.List)
	 */
	public List<ReflinkStrekning> getGeometryWithinReflinkSections(List<SerializableReflinkSection> reflinkSeksjoner){
		ArrayOfReflinkSeksjon arrayOfReflinkSeksjon = new ArrayOfReflinkSeksjon();
		arrayOfReflinkSeksjon.getReflinkSeksjon().addAll(SerializableReflinkSection.createWebserviceSections(reflinkSeksjoner));
		long start = System.currentTimeMillis();
		GisServicesSoap service = getGisService();
		ArrayOfReflinkSeksjonGeometryVectors temp = service.getGeometryWithinReflinkSections(arrayOfReflinkSeksjon);
		List<ReflinkSeksjonGeometryVectors> geometryVectors = temp.getReflinkSeksjonGeometryVectors();
		log.debug("WebService brukte:"+(System.currentTimeMillis()-start)+"ms til konvertering av "+reflinkSeksjoner.size()+" reflinksek. til "+geometryVectors.size()+" geometry vectors");
		return NvdbTranslator.translateFromGeometryVectorToPoint(geometryVectors);
	}
	
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getReflinkSectionsFromGeometry(java.util.List)
	 */
	public List<Veinettreflinkseksjon> getReflinkSectionsFromGeometry(List<Point> vector){
		ArrayOfGeometryVector arrayOfGeometryVector = new ArrayOfGeometryVector();
		arrayOfGeometryVector.getGeometryVector().addAll(NvdbTranslator.translateFromPointToGeometryVector(vector));
		List<ReflinkSeksjon> reflinkSeksjon = getGisService().getReflinkSectionsFromGeometry(arrayOfGeometryVector).getReflinkSeksjon();
		return NvdbTranslator.translateFromReflinkSeksjonToVeinettreflinkseksjonList(reflinkSeksjon);
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getReflinkSectionsFromGeometry(java.util.List)
	 */
	public Veinettreflinkseksjon getReflinkSectionFromGeometry(Point point){
		log.debug("getReflinkSectionFromGeometry:"+point);
		ReflinkSeksjon rs = getGisService().getReflinkSectionFromGeometry(point.x, point.y);
		log.debug("getReflinkSectionFromGeometry got:id="+rs.getReflinkId()+" fra:"+rs.getFra()+" til:"+rs.getTil());
		return NvdbTranslator.translateFromReflinkSeksjonToVeinettreflinkseksjon(rs);
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getReflinkSectionsWithinHp(int, double, int)
	 */
	public List<Veinettreflinkseksjon> getReflinkSectionsWithinHp(int reflinkId, double reflinkPos, int direction){
		ReflinkSectionDirection dir = null;
		switch (direction){
			case 0 : dir = ReflinkSectionDirection.FROM_START_HP_TO_POSITION;
			break;
			case 1 : dir = ReflinkSectionDirection.FROM_POSITION_TO_END_HP;
			break;
			case 2 : dir = ReflinkSectionDirection.ALL_HP;
			break;
		}
		List<ReflinkSeksjon> reflinkSeksjon = getGisService().getReflinkSectionsWithinHp(reflinkId, reflinkPos, dir).getReflinkSeksjon();
		return NvdbTranslator.translateFromReflinkSeksjonToVeinettreflinkseksjonList(reflinkSeksjon);
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getReflinkSectionsWithinRoadref(no.mesta.mipss.persistence.veinett.Veinettveireferanse)
	 */
	public List<Veinettreflinkseksjon> getReflinkSectionsWithinRoadref(Veinettveireferanse veinettreferanse){
		log.debug("getReflinkSectionsWithinRoadref:"+veinettreferanse.getFraKm());
		Veireferanse veinett = NvdbTranslator.translateFromVeinettveireferanseToVeireferanse(veinettreferanse);
		log.debug("Translated:"+pVeireferanse(veinett));
		List<ReflinkSeksjon> reflinkSeksjon = getGisService().getReflinkSectionsWithinRoadref(veinett).getReflinkSeksjon();
		log.debug(reflinkSeksjon.size()+" konvertert");
		return NvdbTranslator.translateFromReflinkSeksjonToVeinettreflinkseksjonList(reflinkSeksjon);
	}
	
	public List<SerializableReflinkSection> getReflinkSectionsWithinRoadref(SerializableRoadRef roadRef){
		Veireferanse veiref = roadRef.toWebserviceObject();
		log.debug("Konverterer veireferanse:" + pVeireferanse(veiref));
		List<ReflinkSeksjon> reflinkSeksjon = getGisService().getReflinkSectionsWithinRoadref(veiref).getReflinkSeksjon();
		log.debug("Antall konvertert:"+reflinkSeksjon.size());
		for(ReflinkSeksjon r : reflinkSeksjon) {
			log.debug("Reflink:" + pReflinkSeksjon(r));
		}
		return SerializableReflinkSection.createSerializableSections(reflinkSeksjon);
	}
	
	private String pVeireferanse(Veireferanse v){
		return v.getFylkenummer()+"/"+v.getKommunenummer()+" "+(char)v.getVegkategori()+""+(char)v.getVegstatus()+v.getVegnummer()+"("+v.getFrahp()+": "+v.getFraMeter()+"-"+v.getTilhp()+":"+v.getTilMeter()+")";
	}
	
	private String pReflinkSeksjon(ReflinkSeksjon r) {
		return r.getReflinkId()+":"+r.getFra()+"-"+r.getTil()+"("+r.getDirection()+")";
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getRoadrefsWithinReflinkSectionsFromProdreflinkseksjon(java.util.List)
	 */
	public List<Veinettveireferanse> getRoadrefsWithinReflinkSectionsFromProdreflinkseksjon(List<Prodreflinkseksjon> reflinkSeksjon){
		List<SerializableReflinkSection> list = SerializableReflinkSection.createSerializableSections(NvdbTranslator.translateFromProdreflinkSeksjonToReflinkseksjon(reflinkSeksjon));
		return getRoadrefsWithinReflinkSecations(list);
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(java.util.List)
	 */
	public List<Veinettveireferanse> getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(List<Veinettreflinkseksjon> reflinkSeksjon){
		List<SerializableReflinkSection> list = SerializableReflinkSection.createSerializableSections(NvdbTranslator.translateFromVeinettreflinkseksjonToReflinkSeksjonList(reflinkSeksjon));
		return getRoadrefsWithinReflinkSecations(list);
	}
	
	/* (non-Javadoc)
	 * @see no.mesta.mipss.webservice.NvdbWebservice#getRoadrefsWithinReflinkSecations(java.util.List)
	 */
	public List<Veinettveireferanse> getRoadrefsWithinReflinkSecations(List<SerializableReflinkSection> list){
		List<ReflinkSeksjon> reflinkSections = SerializableReflinkSection.createWebserviceSections(list);
		log.debug("Henter veireferanse i reflink:");
		for(ReflinkSeksjon r : reflinkSections) {
			log.debug("Reflink:" + pReflinkSeksjon(r));
		}
		ArrayOfReflinkSeksjon arrayOfReflinkSeksjon = new ArrayOfReflinkSeksjon();
		arrayOfReflinkSeksjon.getReflinkSeksjon().addAll(reflinkSections);
        Holder<ArrayOfVeireferanse> results = new Holder<ArrayOfVeireferanse>();
        Holder<Boolean> historicFlag = new Holder<Boolean>();
		getGisService().getRoadrefsWithinReflinkSections(arrayOfReflinkSeksjon, results, historicFlag);
		if (results.value==null) {
			log.debug("Fant ingen veireferanser");
			return null;
		}
				
		List<Veinettveireferanse> veinettveireferanseList = NvdbTranslator.translateFromVeireferanseToVeinettveireferanseList(results.value.getVeireferanse());
		log.debug("Fant veireferanser:");
		for(Veinettveireferanse v : veinettveireferanseList) {
			log.debug("Konverterer veireferanse:" + v.toString());
		}
		
		return veinettveireferanseList;
	}
	/**
	 * Instansierer gisServicen
	 * @return
	 */
	private GisServicesSoap getGisService(){
		URL baseUrl = no.mesta.mipss.webservice.service.GisServices.class.getResource(".");
		long now = System.currentTimeMillis();
		String wsdlLocation = null;
		if (now-prevUpdate>1000*30){
			wsdlLocation = konfig.hentEnForApp("studio.nvdbwebservice", "wsdlLocation").getVerdi();
			prevUpdate = now;
		}else{
			wsdlLocation = this.wsdlLocation;
		}
		
		try{
			URL url = new URL(baseUrl, wsdlLocation);
			
	    	if (gisService==null){
	    		gisService = new GisServices(url).getGisServicesSoap();
	    	}else{
	    		if (!wsdlLocation.equals(this.wsdlLocation)){
	    			gisService= new GisServices(url, new QName("http://www.mesta.no/GisEngineWebServices/", "GisServices")).getGisServicesSoap();
	    		}
	    	}
    	} catch (Exception e){
    		log.error("Webservice not available!!!", e);
    	}
    	this.wsdlLocation = wsdlLocation;
    	return gisService;
    }
	
	private long prevUpdate;
	
	public boolean isNvdbLoaded(){
		log.info("Checking if Webservice is available");
		Veinettveireferanse vr = new Veinettveireferanse();
		vr.setHp(Long.valueOf(1));
		vr.setFylkesnummer(Long.valueOf(2));
		vr.setKommunenummer(Long.valueOf(0));
		vr.setFraKm(Double.valueOf(0));
		vr.setTilKm(Double.valueOf(1));
		vr.setVeikategori("E");
		vr.setVeistatus("V");
		vr.setVeinummer(Long.valueOf(16));

		List<Veinettreflinkseksjon> reflinkSectionsWithinRoadref = getReflinkSectionsWithinRoadref(vr);
		
		return reflinkSectionsWithinRoadref.size()==1;
	}

    /**
     * For use in testing
     */
    public void setKonfig(KonfigParamLocal konfig) {
        this.konfig = konfig;
    }
	
}
