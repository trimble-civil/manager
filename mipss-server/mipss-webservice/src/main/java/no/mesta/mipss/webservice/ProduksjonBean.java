package no.mesta.mipss.webservice;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kvernetf1.Prodreflinkseksjon;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.webservice.service.ArrayOfReflinkSeksjon;
import no.mesta.mipss.webservice.service.ArrayOfVeireferanse;
import no.mesta.mipss.webservice.service.GisServices;
import no.mesta.mipss.webservice.service.GisServicesSoap;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.Veireferanse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(name=Produksjon.BEAN_NAME, mappedName="ejb/"+Produksjon.BEAN_NAME)
public class ProduksjonBean implements Produksjon{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    @PersistenceContext(unitName="mipssPersistence")
    EntityManager em;
    
    @EJB NvdbWebserviceLocal service;
    public ProduksjonBean() {
    }
    
    
    
//    /** {@inheritDoc} */
//    @SuppressWarnings("unchecked")
//    public List<ProdReflinkStrekning> getProduksjonStrekninger(Long dfuId, Date fraTidspunkt, Date tilTidspunkt) {
//    	long start = System.currentTimeMillis();
//        List<Prodstrekning> list = em.createNamedQuery(Prodstrekning.QUERY_FIND_LAZY)
//                                                        .setParameter("dfuId", dfuId)
//                         .setParameter("fraTidspunkt", fraTidspunkt)
//                                                        .setParameter("tilTidspunkt", tilTidspunkt)
//                                                        .getResultList();
//        log.debug("Time to get from DB:"+(System.currentTimeMillis()-start));
//        log.debug("antall strekninger:"+list.size());
//        
//        List<ReflinkSeksjon> refList = new ArrayList<ReflinkSeksjon>();
//        for (Prodstrekning prodstrekning: list){
//            refList.addAll(convertProdreflinkseksjonToReflinkSeksjon(prodstrekning.getProdreflinkseksjonList()));
//        }
//        log.debug("antall reflinkseksjoner inn til webservice:"+refList.size());
//        start = System.currentTimeMillis();
//        List<ReflinkStrekning> pointList = service.getGeometryWithinReflinkSections(refList);
//        log.debug("Konverterte "+pointList.size()+" ReflinkSeksjoner p� "+(System.currentTimeMillis()-start)+" ms");
//        List<ProdReflinkStrekning> strekning = mergeReflinkStrekningAndReflinkseksjon(list, pointList);
//        return strekning;
//    }
//    
//    /**
//     * Slår sammen listen med prodstrekninger og listen med ReflinkStrekninger.
//     * @param prodList
//     * @param pointList
//     * @return
//     */
//    private List<ProdReflinkStrekning> mergeReflinkStrekningAndReflinkseksjon(List<Prodstrekning> prodList, List<ReflinkStrekning> pointList){
//    	
//    	List<ProdReflinkStrekning> strekningList = new ArrayList<ProdReflinkStrekning>();
//    	for (Prodstrekning p:prodList){
//    		ProdReflinkStrekning prodReflinkStrekning = new ProdReflinkStrekning();
//    		prodReflinkStrekning.setProdstrekning(p);
//    		for (Prodreflinkseksjon pr:p.getProdreflinkseksjonList()){
//    			boolean found = false;
//	    		for (ReflinkStrekning strekning: pointList){
//		    		if (pr.getReflinkIdent()==strekning.getReflinkId()&& 
//		    			pr.getFra()==strekning.getFra() &&
//		    			pr.getTil()==strekning.getTil()){
//		    			prodReflinkStrekning.addReflinkStrekning(strekning);
////		    			log.debug("Found match for "+pr.getReflinkIdent()+" "+pr.getFra()+"-"+pr.getTil());
//		    			found = true;
//		    		}
//	    		}
//	    		if (!found){
//	    			log.error("No match found for:"+pr.getReflinkIdent()+" "+pr.getFra()+"-"+pr.getTil());
////	    			for (ReflinkStrekning st: pointList){
////	    				log.debug(":"+st.getReflinkId()+" "+st.getFra()+"-"+st.getTil());
////	    			}
//	    		}
//    		}
//    		strekningList.add(prodReflinkStrekning);
//    	}
//    	return strekningList;
//    	
//    }
//    
//
////    /** {@inheritDoc} */
////    @SuppressWarnings("unchecked")
////    public List<Prodstrekning> getProduksjonStrekningList(Long dfuId, Date fraTidspunkt, Date tilTidspunkt){
////    	List<Prodstrekning> list = em.createNamedQuery(Prodstrekning.QUERY_FIND)
////        .setParameter("dfuId", dfuId)
////        .setParameter("fraTidspunkt", fraTidspunkt)
////        .setParameter("tilTidspunkt", tilTidspunkt)
////        .getResultList();
////    	for (Prodstrekning p:list){
////    		log.debug("Antall reflinks for "+p.getId()+" "+p.getProdreflinkseksjonList().size());
////    	}
////    	return list;
////    }
//    
//    /** {@inheritDoc} */
//    @SuppressWarnings("unchecked")
//    public List<ProdpunktVO> getProduksjonPunkt(Long dfuId, Date fraTidspunkt, Date tilTidspunkt){
//        List<Prodpunktinfo> prodpunktList = em.createNamedQuery(Prodpunktinfo.QUERY_FIND_PRODPUNKTINFO)
//                            .setParameter("dfuId", dfuId)
//                            .setParameter("fraTidspunkt", convertFromCETtoGMT(fraTidspunkt))
//                            .setParameter("tilTidspunkt", convertFromCETtoGMT(tilTidspunkt)).getResultList();
//        List<ProdpunktVO> punktList = new ArrayList<ProdpunktVO>();
//        for (Prodpunktinfo i:prodpunktList){
//        	ProdpunktVO pp = new ProdpunktVO();
//        	pp.setProdpunkt(i);
//        	//legger på prodtype på punktene, dette må gjøres for punkter som ikke ligger innenfor et tiltak
//        	//
////        	if (i.getProdpunkt()!=null){
////        		pp.setProdtype(getProdtypeFromProdpunkt(i.getProdpunkt()));
////        	}
//        	punktList.add(pp);
//        }
//        return punktList;
//    }
//    
//    /**
//     * Henter prodtypene for et gitt prodpunkt
//     * @param p
//     * @return
//     */
//    public List<Prodtype> getProdtypeFromProdpunkt(Prodpunkt p){
//    	String id = null;
//    	if (p.getDfu()!=null)
//    		id = p.getDfu().getId()+"";
//    	if (id==null)
//    		return null;
//    	
//    	SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
//    	String time = format.format(p.getGmtTidspunkt());
//    	Query query = em.createNativeQuery("select finn_prod_status("+id+", '"+p.getDigi()+"', to_date('"+time+"', 'dd.MM.yyyy hh24:mi:ss')) from dual");
//    	List<StringVO> pids = new NativeQueryWrapper<StringVO>(query, StringVO.class, new Class[]{String.class}, "id").getWrappedList();
//    	List<Prodtype> prodtypes = new ArrayList<Prodtype>();
//    	
//    	if (pids!=null){
//    		for (StringVO i:pids){
//    			List<Long> longs = parseValue(i.getId());
//    			for (Long l:longs){
//    				Prodtype pr = em.find(Prodtype.class, l);
//    				prodtypes.add(pr);
//    			}
//    		}
//    	}
//    	return prodtypes;
//    }
//
//    private List<Long> parseValue(String value){
//    	List<Long> longs = new ArrayList<Long>();
//    	String[] s = value.split(";");
//    	for (String st:s){
//    		try{
//    			Long l = Long.parseLong(st.trim());
//    			if (l.longValue()>0)
//    				longs.add(l);
//    		}catch (Exception e){
//    			e.printStackTrace();
//    		}
//    	}
//    	return longs;
//    	
//    }
//    public static void main(String[] a){
//    	Long l = Long.parseLong(" -1");
//    	System.out.println(l);
//    }
//    /**
//     * Konverterer en Date fra CET til GMT tidssone
//     * @param date
//     * @return
//     */
//    private Date convertFromCETtoGMT(Date date){
//    	Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
//    	c.setTime(date);
//    	int d = c.get(Calendar.DAY_OF_MONTH);
//    	int m = c.get(Calendar.MONTH);
//    	int y = c.get(Calendar.YEAR);
//    	int hh = c.get(Calendar.HOUR_OF_DAY);
//    	int mm = c.get(Calendar.MINUTE);
//    	int ss = c.get(Calendar.SECOND);
//    	int ms = c.get(Calendar.MILLISECOND);
//    	Calendar cal = Calendar.getInstance();
//    	cal.set(Calendar.DAY_OF_MONTH, d);
//    	cal.set(Calendar.MONTH, m);
//    	cal.set(Calendar.YEAR, y);
//    	cal.set(Calendar.HOUR_OF_DAY, hh);
//    	cal.set(Calendar.MINUTE, mm);
//    	cal.set(Calendar.SECOND, ss);
//    	cal.set(Calendar.MILLISECOND, ms);
//    	return cal.getTime();
//    }
//    /**
//     * Konverterer Prodreflinkseksjon som benyttes mot databasen til Reflinkseksjon som benyttes av webservice
//     * @param list
//     * @return
//     */
//    private List<ReflinkSeksjon> convertProdreflinkseksjonToReflinkSeksjon(List<Prodreflinkseksjon> list){
//        List<ReflinkSeksjon> reflinkseksjoner = new ArrayList<ReflinkSeksjon>();
//        
//        for (Prodreflinkseksjon p:list){
//            ReflinkSeksjon ref = new ReflinkSeksjon();
//            double fraTemp = p.getFra();
//            if (p.getTil()<p.getFra()){
//            	ref.setFra(p.getTil());
//                ref.setTil(p.getFra());
//                p.setTil(p.getFra());
//                p.setFra(fraTemp);
//            }else{
//	            ref.setFra(p.getFra());
//	            ref.setTil(p.getTil());
//            }
//            ref.setReflinkId(p.getReflinkIdent());
//            
//            reflinkseksjoner.add(ref);
//        }
//        return reflinkseksjoner;
//    }
//    
////    /**
////     * Konverterer en prodstrekning til prodstrekningVO
////     * @param p
////     * @return
////     */
////    private ProdstrekningVO createProdstrekningVO(Prodstrekning p){
////    	ProdstrekningVO pv = new ProdstrekningVO();
////    	pv.setDfuId(p.getDfuId());
////    	pv.setDigi(p.getDigi());
////    	pv.setFraTidspunkt(p.getFraTidspunkt());
////    	pv.setTilTidspunkt(p.getTilTidspunkt());
////    	pv.setFylkesnummer(p.getFylkesnummer());
////    	pv.setKommunenummer(p.getKommunenummer());
////    	pv.setHp(p.getHp());
////    	pv.setFraKm(p.getFraKm());
////    	pv.setTilKm(p.getTilKm());
////    	pv.setVeikategori(pv.getVeikategori());
////    	pv.setVeistatus(p.getVeistatus());
////    	pv.setVeinummer(p.getVeinummer());
////    	pv.setHastighetMax(p.getHastighetMax());
////    	pv.setHastighetMin(p.getHastighetMin());
////    	pv.setId(p.getId());
////    	pv.setInspeksjonGuid(p.getInspeksjonGuid());
////    	pv.setOpprettetDato(p.getOpprettetDato());
////    	pv.setOpprinnelse(p.getOpprinnelse());
////    	pv.setSistePunktReflinkIdent(p.getSistePunktReflinkIdent());
////    	pv.setSistePunktReflinkPosisjon(p.getSistePunktReflinkPosisjon());
////    	pv.setTemperatur(p.getTemperatur());
////    	return pv;
////    }
////    
    /*****************************************DETTE H�RER IKKE HJEMME HER!!!!!!!!!!!!!!!!!!!!!!!!!!!!*******************************/
    /**
     * Dette er kun til midlertidig testing og ble lagt her fordi det passet. !. nukes n�r den ikke trengs..(evt flyttes til mer egnet testb�nne)
     */
    @SuppressWarnings("unchecked")
	public void testAvProdReflinkseksjon(Date fra, Date til){
    	log.debug("Starter test av prodreflinkseksjon");
//    	Calendar fra = Calendar.getInstance();
//    	fra.set(Calendar.YEAR, 1990);
//    	Calendar til = Calendar.getInstance();
    	String wsdlUrl = "http://mest-ap005.vegprod.no/mipssws/GisServices.asmx?WSDL";
    	URL baseUrl = no.mesta.mipss.webservice.service.GisServices.class.getResource(".");
    	GisServicesSoap service =null;
    	try{
			URL url = new URL(baseUrl, wsdlUrl);
			service = new GisServices(url, new QName("http://www.mesta.no/GisEngineWebServices/", "GisServices")).getGisServicesSoap();
    	} catch (Exception e){
    		log.error("Webservice not available!!!", e);
    	}
    	
    	List<Prodstrekning> list = em.createNamedQuery(Prodstrekning.QUERY_FIND).setParameter("dfuId", 7).setParameter("fraTidspunkt", fra).setParameter("tilTidspunkt", til).getResultList();
    	SimpleDateFormat fm = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    	for (Prodstrekning p:list){
    		double m = 0;
    		if (p.getFraKm()<p.getTilKm()){
    			m = (p.getTilKm()-p.getFraKm())*1000;
    		}else
    			m = (p.getFraKm()-p.getTilKm())*1000;
    		String r = "Prodstrekning:"+p.getFylkesnummer()+"/"+p.getKommunenummer()+" "+p.getVeikategori()+""+p.getVeistatus()+""+p.getVeinummer()+"("+p.getHp()+": "+p.getFraKm()+"-"+p.getTilKm()+") "+ "( "+m+"m) fratid:"+fm.format(p.getFraTidspunkt())+" tiltid:"+fm.format(p.getTilTidspunkt());
    		System.out.println(r);
    		Int mtr = new Int();
    		for (Prodreflinkseksjon pr:p.getProdreflinkseksjonList()){
    			ReflinkSeksjon rs = new ReflinkSeksjon();
    			rs.setFra(pr.getFra());
    			rs.setTil(pr.getTil());
    			rs.setReflinkId(pr.getReflinkIdent());
    			List<Veireferanse> veireferanseList = new ArrayList<Veireferanse>();
    			String meter = getMeter(rs, service, mtr, veireferanseList);
    			System.out.println("\t"+pr.getReflinkIdent()+": "+pr.getFra()+"-"+pr.getTil()+" "+meter);
    			for (Veireferanse vrl:veireferanseList){
    				String rr = vrl.getFylkenummer()+"/"+vrl.getKommunenummer()+" "+((char)vrl.getVegkategori())+""+((char)vrl.getVegstatus())+""+vrl.getVegnummer()+"("+vrl.getFrahp()+"-"+vrl.getTilhp()+": "+vrl.getFraMeter()+"-"+vrl.getTilMeter()+") ";
    				String warning = "";
    				if (vrl.getFrahp()!=vrl.getTilhp())
    					warning="WARN: fra hp er ikke lik til hp";
    				System.out.println("\t\t"+rr+" "+warning);
    			}
    		}
    		if (((int)Math.round(m)-mtr.getI())!=0){
    			System.out.println("\t\t\t\t**********************************ERROR sum av lengden til reflinkseksjoner er ikke lik lengden p� veireferansen, diff utgj�r:"+((int)Math.round(m)-mtr.getI()));
    		}
    		
    		Veireferanse vr = new Veireferanse();
    		vr.setKommunenummer(p.getKommunenummer().intValue());
    		vr.setFylkenummer(p.getFylkesnummer().intValue());
    		vr.setVegkategori(p.getVeikategori().charAt(0));
    		vr.setVegstatus(p.getVeistatus().charAt(0));
    		vr.setVegstatusoverordnet('V');
    		vr.setVegnummer(p.getVeinummer().intValue());
    		vr.setFrahp(p.getHp().intValue());
    		vr.setTilhp(p.getHp().intValue());
    		
    		
    		if (p.getTilKm()<p.getFraKm()){
    			vr.setFraMeter((int)(Math.round(p.getTilKm()*1000)));
        		vr.setTilMeter((int)(Math.round(p.getFraKm()*1000)));
        		System.out.println("byttet retning...");
    		}else{
	    		vr.setFraMeter((int)(Math.round(p.getFraKm()*1000)));
	    		vr.setTilMeter((int)(Math.round(p.getTilKm()*1000)));
    		}
    		String vrr = vr.getFylkenummer()+"/"+vr.getKommunenummer()+" "+((char)vr.getVegkategori())+""+((char)vr.getVegstatus())+""+vr.getVegnummer()+"("+vr.getFrahp()+"-"+vr.getTilhp()+": "+vr.getFraMeter()+"-"+vr.getTilMeter()+") ";
    		System.out.println("Resultat fra getReflinksectionWithinRoadRef() - "+vrr);
    		
    		int m2 = 0;
    		List<ReflinkSeksjon> resultat = service.getReflinkSectionsWithinRoadref(vr).getReflinkSeksjon();
    		System.out.println("Antall reflinkseksjoner fra WS:"+resultat.size());
    		for (ReflinkSeksjon rs:resultat){
    			List<Veireferanse> veireferanseList = new ArrayList<Veireferanse>();
    			String meter = getMeter(rs, service, new Int(), veireferanseList);
    			System.out.println("\t"+rs.getReflinkId()+": "+rs.getFra()+"-"+rs.getTil()+" "+meter);
    			
    			for (Veireferanse vrl:veireferanseList){
    				String rr = vrl.getFylkenummer()+"/"+vrl.getKommunenummer()+" "+((char)vrl.getVegkategori())+""+((char)vrl.getVegstatus())+""+vrl.getVegnummer()+"("+vrl.getFrahp()+"-"+vrl.getTilhp()+": "+vrl.getFraMeter()+"-"+vrl.getTilMeter()+") ";
    				String warning = "";
    				int fraM = vrl.getFraMeter();
    				int tilM = vrl.getTilMeter();
    				int dist = 0;
    				if (fraM<tilM){
    					dist = tilM-fraM;
    				}else
    					dist = fraM-tilM;
    				m2+=dist;
    				if (vrl.getFrahp()!=vrl.getTilhp())
    					warning="WARN: fra hp er ikke lik til hp";
    				System.out.println("\t\t"+rr+" "+warning);
    				
    			}
    		}
    		double res = Math.round(m)-m2;
    		if (res!=0){
    			System.out.println("ERROR2 - sum (tilkm-frakm) beregnet fra prodreflinksection er ulik tilkm-frakm fra prodstrekning diff="+res);
    		}
    		System.out.println("----------------------------------------------------------------------");
    		System.out.println();
    	}
    	log.debug("Slutter test av prodreflinkseksjon");
    }
	private String getMeter(ReflinkSeksjon rs, GisServicesSoap service, Int m, List<Veireferanse> veireferanseList){
    	ArrayOfReflinkSeksjon ar = new ArrayOfReflinkSeksjon();
    	ar.getReflinkSeksjon().add(rs);
    	Holder<Boolean> historicFlag = new Holder<Boolean>();
		Holder<ArrayOfVeireferanse> result = new Holder<ArrayOfVeireferanse>();
		service.getRoadrefsWithinReflinkSections(ar, result, historicFlag);
		String meter = "(";
		if (result.value!=null){
			List<Veireferanse> vr = result.value.getVeireferanse();
			veireferanseList.addAll(vr);
			for (Veireferanse v:vr){
				int fraM = v.getFraMeter();
				int tilM = v.getTilMeter();
				int dist = 0;
				if (fraM<tilM){
					dist = tilM-fraM;
				}else
					dist = fraM-tilM;
				m.addI(dist);
				meter += " "+dist+"m ";
			}
		}else
			meter +="...";
		meter +=")";
		return meter;
    }
    
    class Int{
    	int i=0;
    	void addI(int i){
    		this.i +=i;
    	}
    	int getI(){
    		return i;
    	}
    }
    
    @SuppressWarnings("unchecked")
	public long testKonverterKontraktveinett(Long id){
    	
    	Driftkontrakt d = em.find(Driftkontrakt.class, id);
    	log.debug("Henter veinett for kontrakt:"+id+" "+d.getKontraktnavn());
    	long start = System.currentTimeMillis();
    	List<Veinettreflinkseksjon> reflinkSeksjoner = em.createNamedQuery(Veinettreflinkseksjon.QUERY_FIND)
    	.setParameter("veinettId", d.getVeinettId())
    	.getResultList();
    	for (Veinettreflinkseksjon v:reflinkSeksjoner){
    		v.getId();
    	}
    	
    	log.debug("Done fetching data");
    	log.debug("Veinett hentet på "+(System.currentTimeMillis()-start)+"ms");
    	start = System.currentTimeMillis();
    	service.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(reflinkSeksjoner);
    	long time = System.currentTimeMillis()-start;
    	log.debug("konverterte "+reflinkSeksjoner.size()+" reflinkseksjoner til geometri på "+time+"ms");
    	return time;
    }
}
