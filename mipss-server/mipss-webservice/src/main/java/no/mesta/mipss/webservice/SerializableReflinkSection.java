package no.mesta.mipss.webservice;

import no.mesta.mipss.webservice.service.Directions;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class SerializableReflinkSection implements Serializable {

    private final int id;
    private final double from;
    private final double to;
    private final Direction direction;

    public SerializableReflinkSection(int id, double from, double to, Direction direction) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public double getFrom() {
        return from;
    }

    public double getTo() {
        return to;
    }

    public Direction getDirection() {
        return direction;
    }

    public static List<SerializableReflinkSection> createSerializableSections(List<ReflinkSeksjon> seksjoner) {
        List<SerializableReflinkSection> sections = new LinkedList<>();

        for(ReflinkSeksjon section : seksjoner) {
            int id = section.getReflinkId();
            double from = section.getFra();
            double to = section.getTil();
            Directions dir = section.getDirection();

            sections.add(new SerializableReflinkSection(id, from, to, dir!=null?Direction.getDirection(dir.toString()):null));
        }
        return sections;
    }

    public static List<ReflinkSeksjon> createWebserviceSections(List<SerializableReflinkSection> sections) {
        List<ReflinkSeksjon> webServiceSections = new LinkedList<>();

        for(SerializableReflinkSection section : sections) {
            webServiceSections.add(section.createWebserviceObject());
        }

        return webServiceSections;
    }

    public ReflinkSeksjon createWebserviceObject() {
        ReflinkSeksjon rls = new ReflinkSeksjon();
        rls.setReflinkId(getId());
        rls.setFra(getFrom());
        rls.setTil(getTo());
        rls.setDirection(getDirection()!=null?Directions.valueOf(getDirection().toString().toUpperCase()):null);

        return rls;
    }

    public enum Direction {
        UNKNOWN,WITH,TOWARDS;

        public static Direction getDirection(String value) {
            if(value==null) return UNKNOWN;
            return Direction.valueOf(value.toUpperCase());
        }
    }
}
