package no.mesta.mipss.webservice;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.persistence.kvernetf1.Prodreflinkseksjon;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.ProdstrekningSok;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.service.GeometryVector;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.ReflinkSeksjonGeometryVectors;
import no.mesta.mipss.webservice.service.Veireferanse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Denne klassen konverterer mellom objektene som benyttes til webservicen og de 
 * vi bruker i JPA mot databasen
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kul�</a>
 *
 */
public class NvdbTranslator {

	private static Logger log = LoggerFactory.getLogger(NvdbTranslator.class);
	
	public static Veireferanse translateFromVeinettveireferanseToVeireferanse(Veinettveireferanse veireferanse){
		Veireferanse v= new Veireferanse();
    	v.setFraMeter((int)(veireferanse.getFraKm()*1000));
    	v.setFylkenummer(veireferanse.getFylkesnummer().intValue());
    	v.setFrahp(veireferanse.getHp().intValue());
    	v.setTilhp(veireferanse.getHp().intValue());
    	v.setKommunenummer(veireferanse.getKommunenummer().intValue());
    	v.setTilMeter((int)(veireferanse.getTilKm()*1000));
    	char kat = veireferanse.getVeikategori()!=null?veireferanse.getVeikategori().charAt(0):null;
    	v.setVegkategori(kat);
    	v.setVegnummer(veireferanse.getVeinummer().intValue());
    	char stat = veireferanse.getVeistatus()!=null?veireferanse.getVeistatus().charAt(0):null;
    	v.setVegstatus(stat);
    	v.setVegstatusoverordnet(stat);
    	return v;
	}
	
	public static List<Veireferanse> translateFromVeinettveireferanseToVeireferanseList(List<Veinettveireferanse> list){
		List<Veireferanse> veiref = new ArrayList<Veireferanse>();
		for (Veinettveireferanse v:list){
			veiref.add(translateFromVeinettveireferanseToVeireferanse(v));
		}
		return veiref;
	}

	public static Veireferanse translateFromProdstrekningToVeireferanse(Prodstrekning p){
		Veireferanse v= new Veireferanse();
    	v.setFraMeter((int)(p.getFraKm()*1000));
    	v.setFylkenummer(p.getFylkesnummer().intValue());
    	v.setFrahp(p.getHp().intValue());
    	v.setTilhp(p.getHp().intValue());
    	v.setKommunenummer(p.getKommunenummer().intValue());
    	v.setTilMeter((int)(p.getTilKm()*1000));
    	v.setVegkategori(p.getVeikategori().charAt(0));
    	v.setVegnummer(p.getVeinummer().intValue());
    	v.setVegstatus(p.getVeistatus().charAt(0));
    	v.setVegstatusoverordnet(p.getVeistatus().charAt(0));
    	return v;
	}

	public static Veireferanse translateFromProdstrekningToVeireferanse(ProdstrekningSok p){
		Veireferanse v= new Veireferanse();
    	v.setFraMeter((int)(p.getFraKm()*1000));
    	v.setFylkenummer(p.getFylkesnummer());
    	v.setFrahp(p.getHp());
    	v.setTilhp(p.getHp());
    	v.setKommunenummer(p.getKommunenummer());
    	v.setTilMeter((int)(p.getTilKm()*1000));
    	v.setVegkategori(p.getVeikategori().charAt(0));
    	v.setVegnummer(p.getVeinummer());
    	v.setVegstatus(p.getVeistatus().charAt(0));
    	v.setVegstatusoverordnet(p.getVeistatus().charAt(0));
    	return v;
	}

	public static List<Veireferanse> translateFromProdstrekningListToVeireferanseList(List<Prodstrekning> list){
		List<Veireferanse> refList = new ArrayList<Veireferanse>();
		for (Prodstrekning p:list){
			refList.add(translateFromProdstrekningToVeireferanse(p));
		}
		return refList;
	}

	/**
	 * Konverterer fra en Veireferanse (brukes av webservice) til en Veinettveireferanse
	 * @param veiref
	 * @return
	 */
	public static Veinettveireferanse translateFromVeireferanseToVeinettveireferanse(Veireferanse veiref){
		Veinettveireferanse v = new Veinettveireferanse();
		v.setFraKm((double)veiref.getFraMeter()/1000);
		v.setFylkesnummer(new Long(veiref.getFylkenummer()));
		v.setHp(new Long(veiref.getFrahp()));
		v.setKommunenummer(new Long(veiref.getKommunenummer()));
		v.setTilKm((double)veiref.getTilMeter()/1000);
		v.setVeikategori((char)veiref.getVegkategori()+"");
		v.setVeinummer(new Long(veiref.getVegnummer()));
		v.setVeistatus((char)veiref.getVegstatus()+"");
		return v;
	}
	
	/**
	 * Konverterer en listen med Veireferanse til en liste med Veinettveireferanse 
	 * @param veireferanse
	 * @return
	 */
	public static List<Veinettveireferanse> translateFromVeireferanseToVeinettveireferanseList(List<Veireferanse> veireferanse) {
		List<Veinettveireferanse> list = new ArrayList<Veinettveireferanse>();
		for (Veireferanse v:veireferanse){
			list.add(translateFromVeireferanseToVeinettveireferanse(v));
		}
		return list;
	}
	
	/**
	 * Konverterer en liste med Veinettreflinkseksjon til en liste med ReflinkSeksjon(som brukes av webservice)
	 * @param list
	 * @return
	 */
	public static List<ReflinkSeksjon> translateFromVeinettreflinkseksjonToReflinkSeksjonList(List<Veinettreflinkseksjon> list){
		List<ReflinkSeksjon> reflinkseksjoner = new ArrayList<ReflinkSeksjon>();
        for (Veinettreflinkseksjon p:list){
            reflinkseksjoner.add(translateFromVeinettreflinkseksjonToReflinkSeksjon(p));
        }
        return reflinkseksjoner;
	}
	
	/**
	 * Konverterer fra en Veinettreflinkseksjon til en ReflinkSeksjon (som brukes av webservice)
	 * @param reflink
	 * @return
	 */
	public static ReflinkSeksjon translateFromVeinettreflinkseksjonToReflinkSeksjon(Veinettreflinkseksjon reflink){
		ReflinkSeksjon ref = new ReflinkSeksjon();
        ref.setFra(reflink.getFra());
        ref.setTil(reflink.getTil());
        ref.setReflinkId(reflink.getReflinkIdent().intValue());
        return ref;
	}
	
	/**
	 * Konverterer en liste med Prodreflinkseksjon til en liste med ReflinkSeksjon(som brukes av webservice)
	 * @param list
	 * @return
	 */
	public static List<ReflinkSeksjon> translateFromProdreflinkSeksjonToReflinkseksjon(List<Prodreflinkseksjon> list){
		List<ReflinkSeksjon> reflinkseksjoner = new ArrayList<ReflinkSeksjon>();
        for (Prodreflinkseksjon p:list){
            reflinkseksjoner.add(translateFromProdreflinkseksjonToReflinkSeksjon(p));
        }
        return reflinkseksjoner;
	}
	
	/**
	 * Konverterer fra en Prodreflinkseksjon til en ReflinkSeksjon (som brues av webservice) 
	 * @param reflink
	 * @return
	 */
	public static ReflinkSeksjon translateFromProdreflinkseksjonToReflinkSeksjon(Prodreflinkseksjon reflink){
		ReflinkSeksjon ref = new ReflinkSeksjon();
        ref.setFra(reflink.getFra());
        ref.setTil(reflink.getTil());
        ref.setReflinkId(reflink.getReflinkIdent());
        return ref;
	}
	
	public static List<Prodreflinkseksjon> translateFromVeinettreflinkseksjonToProdreflinkseksjon(List<Veinettreflinkseksjon> list){
		List<Prodreflinkseksjon> prodseksjoner = new ArrayList<Prodreflinkseksjon>();
		for (Veinettreflinkseksjon rs:list){
			prodseksjoner.add(translateFromReflinkseksjonToProdreflinkseksjon(rs));
		}
		return prodseksjoner;
	}
	
	public static Prodreflinkseksjon translateFromReflinkseksjonToProdreflinkseksjon(Veinettreflinkseksjon rs){
		Prodreflinkseksjon pr = new Prodreflinkseksjon();
		pr.setFra(rs.getFra());
		pr.setTil(rs.getTil());
		pr.setReflinkIdent(rs.getReflinkIdent().intValue());
		return pr;
	}
	
	/**
	 * Konverterer fra en ReflinkSeksjon(brukes av webservice) til en Veinettreflinkseksjon
	 * @param reflink
	 * @return
	 */
	public static Veinettreflinkseksjon translateFromReflinkSeksjonToVeinettreflinkseksjon(ReflinkSeksjon reflink){
		Veinettreflinkseksjon v = new Veinettreflinkseksjon ();
		v.setFra(reflink.getFra());
		v.setTil(reflink.getTil());
		v.setReflinkIdent(new Long(reflink.getReflinkId()));
		return v;
	}
	
	/**
	 * Konverterer en liste av ReflinkSeksjon til en liste med Veinettreflinkseksjon 
	 * @param reflinkSeksjon
	 * @return
	 */
	public static List<Veinettreflinkseksjon> translateFromReflinkSeksjonToVeinettreflinkseksjonList(List<ReflinkSeksjon> reflinkSeksjon) {
		log.debug("Translates "+reflinkSeksjon.size()+" reflinkseksjoner til veinettreflinkseksjoner");
		List<Veinettreflinkseksjon> list = new ArrayList<Veinettreflinkseksjon>();
		for (ReflinkSeksjon r:reflinkSeksjon){
			list.add(translateFromReflinkSeksjonToVeinettreflinkseksjon(r));
		}
		log.debug(list.size()+" translated");
		return list;
	}
	
	/**
	 * Konverterer en liste med Point objekter til en Liste med GeometryVector (som benyttes av webservice)
	 * @param list
	 * @return
	 */
	public static List<GeometryVector> translateFromPointToGeometryVector(List<Point> list){
		List<GeometryVector> geometryList = new ArrayList<GeometryVector>();
		for (Point p:list){
			GeometryVector geometryVector = new GeometryVector();
			geometryVector.setEast(p.getX());
			geometryVector.setNorth(p.getY());
			geometryList.add(geometryVector);
		}
		return geometryList;
	}

	public static List<ReflinkStrekning> translateFromGeometryVectorToPoint(List<ReflinkSeksjonGeometryVectors> geometryVectors) {
		long start = System.currentTimeMillis();
		List<ReflinkStrekning> reflink = new ArrayList<ReflinkStrekning>();
		for (ReflinkSeksjonGeometryVectors vec: geometryVectors){
			List<GeometryVector> geometryVector = vec.getGeometryVector().getGeometryVector();
			Point[] vector = translateFromVectorsToPoints(geometryVector);

			ReflinkStrekning rs = new ReflinkStrekning();
			rs.setVectors(vector);
			rs.setFra(vec.getReflinkSeksjon().getFra());
			rs.setTil(vec.getReflinkSeksjon().getTil());
			rs.setReflinkId(vec.getReflinkSeksjon().getReflinkId());

			reflink.add(rs);
		}
		log.debug("Antall GeometryVectors inn til translator:"+geometryVectors.size()+" antall ut:"+reflink.size()+" brukte:"+(System.currentTimeMillis()-start)+"ms");
		return reflink;
	}
	
	private static Point[] translateFromVectorsToPoints(List<GeometryVector> geometryVectors){
		Point[] points = new Point[geometryVectors.size()];
		for (int i=0;i<geometryVectors.size();i++){
			points[i] = translateFromVectorToPoint(geometryVectors.get(i));
		}
		return points;
	}

	private static Point translateFromVectorToPoint(GeometryVector v){
		Point p = new Point();
		p.setLocation(v.getEast(), v.getNorth());
		return p;
	}

}