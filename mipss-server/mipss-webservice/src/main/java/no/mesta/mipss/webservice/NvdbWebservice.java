package no.mesta.mipss.webservice;

import java.awt.Point;
import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.persistence.kvernetf1.Prodreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.webservice.service.GetFeaturesWithinReflinkSectionsResult;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.Veireferanse;

@Remote
public interface NvdbWebservice {
	public static final String BEAN_NAME="NvdbWebserviceBean";
	
	/**
	 * Funksjonen benytter webservicen til å hente informasjon om vegobjekter (features) 
	 * av en gitt vegobjekttype (featuretype) innenfor ønsket veinett med prodreflinkseksjoner 
	 * som parameter
	 * 
	 * @param featureTypeId
	 * @param attributeTypeIds
	 * @param reflinkSeksjon
	 */
	void getFeaturesWithinReflinkSectionsFromProdreflinkseksjon(int featureTypeId, List<Integer> attributeTypeIds,List<Prodreflinkseksjon> reflinkSeksjon);

	/**
	 * Funksjonen benytter webservicen til å hente informasjon om vegobjekter (features) 
	 * av en gitt vegobjekttype (featuretype) innenfor ønsket veinett med veinettreflinkseksjoner 
	 * som parameter
	 * 
	 * @param featureTypeId
	 * @param attributeTypeIds
	 * @param reflinkSeksjon
	 */
	List<GetFeaturesWithinReflinkSectionsResult> getFeaturesWithinReflinkSectionsFromVeinettreflinkseksjon(int featureTypeId, List<Integer> attributeTypeIds,List<Veinettreflinkseksjon> reflinkSeksjon);

	/**
	 * Funksjonen benyttes for å hente geometrien til vegnettet når reflinkseksjonene er kjent
	 * med prodreflinkseksjoner som parameter
	 * @param reflinkSeksjon
	 */
	List<ReflinkStrekning> getGeometryWithinReflinkSectionsFromProdreflinkseksjon(List<Prodreflinkseksjon> reflinkSeksjon);

	/**
	 * Funksjonen benyttes for å hente geometrien til vegnettet når reflinkseksjonene er kjent
	 * med veinettreflinkseksjoner som parameter
	 * @param reflinkSeksjon
	 */
	List<ReflinkStrekning> getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(List<Veinettreflinkseksjon> reflinkSeksjon);

	/**
	 * Funksjonen benyttes for å hente geometrien til vegnettet når reflinkseksjonene er kjent
	 * @param reflinkSeksjoner
	 * @return
	 * TODO Listen med Point objekter skaper slepelinjer i kartet. 
	 */
	List<ReflinkStrekning> getGeometryWithinReflinkSections(List<SerializableReflinkSection> reflinkSeksjoner);

	/**
	 * Konverterer strekningen mellom listen med xy vektorer til reflinkseksjoner
	 * @param vector
	 */
	List<Veinettreflinkseksjon> getReflinkSectionsFromGeometry(List<Point> vector);

	/**
	 * Henter en liste med Veinettreflinkseksjon innenfor gitte parametere
	 * 
	 * @param reflinkId reflinken som er utgangspunktet for � finne reflinkseksjoene
	 * @param reflinkPos punktet på reflinken som skal benyttes til å hente data fra/til/inkludert
	 * @param direction flagg som bestemmer hvilke reflinkseksjoner som skal returneres
	 *  	 0: gir alle reflinkseksjonene fra HP'ens startposisjon til posisjonen p� reflinken
	 *  	 1: gir alle reflinkseksjonene fra HP'ens slutt til posisjonen p� reflinken
	 * 		 2: gir alle reflinkseksjonene innenfor HP'en som tilh�rer reflinkId + reflinkPos
	 * @return
	 */
	List<Veinettreflinkseksjon> getReflinkSectionsWithinHp(int reflinkId, double reflinkPos, int direction);

	/**
	 * Henter de reflinkseksjonene som ligger innenfor veireferansen 
	 * @param veinettreferanse
	 * @return
	 */
	List<Veinettreflinkseksjon> getReflinkSectionsWithinRoadref(Veinettveireferanse veinettreferanse);

	/**
	 * Henter veireferansene som ligger innenfor listen med reflinkseksjoner
	 * @param reflinkSeksjon Prodreflinkseksjon
	 * @return
	 */
	List<Veinettveireferanse> getRoadrefsWithinReflinkSectionsFromProdreflinkseksjon(List<Prodreflinkseksjon> reflinkSeksjon);

	/**
	 * Henter veireferansene som ligger innenfor listen med reflinkseksjoner 
	 * @param reflinkSeksjon Veinettreflinkseksjon
	 * @return
	 */
	List<Veinettveireferanse> getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(List<Veinettreflinkseksjon> reflinkSeksjon);

	/**
	 * Henter veireferansene som ligger innenfor listen med reflinkseksjoner 
	 * @param list ReflinkSeksjon
	 * @return
	 */
	List<Veinettveireferanse> getRoadrefsWithinReflinkSecations(List<SerializableReflinkSection> list);
	/**
	 * Henter reflinkseksjoene som ligger innenfor veireferansen 
	 * @param veiref WebService object
	 * @return WebService object
	 */
	List<SerializableReflinkSection> getReflinkSectionsWithinRoadref(SerializableRoadRef veiref);
	
	/**
	 * Henter nærmeste reflinkseksjon fra en koordinat
	 * @param point
	 * @return
	 */
	Veinettreflinkseksjon getReflinkSectionFromGeometry(Point point);
	
	/**
	 * Gj�r et kall mot nvdb for å sjekke om transformasjoner av veireferanser fungerer
	 * @return
	 */
	boolean isNvdbLoaded();
}