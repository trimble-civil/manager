
package no.mesta.mipss.sam.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the no.mesta.mipss.sam.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FileImport_QNAME = new QName("http://oslo.kommune.no/Smart/FileImport", "FileImport");
    private final static QName _SendDataResponse_QNAME = new QName("http://sammock.mipss.mesta.no/", "sendDataResponse");
    private final static QName _SendData_QNAME = new QName("http://sammock.mipss.mesta.no/", "sendData");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: no.mesta.mipss.sam.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendData }
     * 
     */
    public SendData createSendData() {
        return new SendData();
    }

    /**
     * Create an instance of {@link Content }
     * 
     */
    public Content createContent() {
        return new Content();
    }

    /**
     * Create an instance of {@link SendDataResponse }
     * 
     */
    public SendDataResponse createSendDataResponse() {
        return new SendDataResponse();
    }

    /**
     * Create an instance of {@link FileImport }
     * 
     */
    public FileImport createFileImport() {
        return new FileImport();
    }

    /**
     * Create an instance of {@link Sender }
     * 
     */
    public Sender createSender() {
        return new Sender();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileImport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://oslo.kommune.no/Smart/FileImport", name = "FileImport")
    public JAXBElement<FileImport> createFileImport(FileImport value) {
        return new JAXBElement<FileImport>(_FileImport_QNAME, FileImport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sammock.mipss.mesta.no/", name = "sendDataResponse")
    public JAXBElement<SendDataResponse> createSendDataResponse(SendDataResponse value) {
        return new JAXBElement<SendDataResponse>(_SendDataResponse_QNAME, SendDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sammock.mipss.mesta.no/", name = "sendData")
    public JAXBElement<SendData> createSendData(SendData value) {
        return new JAXBElement<SendData>(_SendData_QNAME, SendData.class, null, value);
    }

}
