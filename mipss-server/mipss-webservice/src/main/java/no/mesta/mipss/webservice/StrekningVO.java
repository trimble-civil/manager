package no.mesta.mipss.webservice;

import java.awt.Point;

import java.io.Serializable;

import java.util.List;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;

@SuppressWarnings("serial")
public class StrekningVO implements Serializable{
    private List<Point[]> pointList;
    private Prodstrekning prodstrekning;
    
    public StrekningVO(List<Point[]> pointList, Prodstrekning prodstrekning) {
        this.pointList = pointList;
        this.prodstrekning = prodstrekning;
    }

    public List<Point[]> getPointList() {
        return pointList;
    }

    public Prodstrekning getProdstrekning() {
        return prodstrekning;
    }
}
