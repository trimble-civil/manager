package no.mesta.mipss.webservice;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.webservice.service.ArrayOfReflinkSeksjon;
import no.mesta.mipss.webservice.service.GeometryVector;
import no.mesta.mipss.webservice.service.GisServices;
import no.mesta.mipss.webservice.service.GisServicesSoap;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import no.mesta.mipss.webservice.service.ReflinkSeksjonGeometryVectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WebserviceFacade {
    private Logger log = LoggerFactory.getLogger(WebserviceFacade.class);
    
    private GisServicesSoap gisService;
    
    public WebserviceFacade() {
    	
    }
    
    /**
     * Henter en Liste med geometri i UTM33 fra webservicen som konverteres p� bakgrunn av ReflinkSeksjonene i listen
     * @param reflinkSeksjoner listen med reflinkseksjoner
     * @return geometri (x, y) 
     */
    public List<ReflinkStrekning> getGeometryWithinReflinkSections(List<ReflinkSeksjon> reflinkSeksjoner){
        ArrayOfReflinkSeksjon s = new ArrayOfReflinkSeksjon();
        s.getReflinkSeksjon().addAll(reflinkSeksjoner);
        List<ReflinkSeksjonGeometryVectors> geometryVectors = getGisService().getGeometryWithinReflinkSections(s).getReflinkSeksjonGeometryVectors();
        
        return NvdbTranslator.translateFromGeometryVectorToPoint(geometryVectors);//convertGeometryVectorToPointList(vectorList);
    }
    
    /**
     * Henter en Liste med geometri i UTM33 fra webservicen som konverteres p� bakgrunn av Veinettreflinkseksjon i listen
     * @param reflinkSeksjoner listen med Veinettreflinkseksjon
     * @return geometri (x, y) 
     */
    public List<ReflinkStrekning> getGeometryWithinReflinkSectionsFromVeinett(List<Veinettreflinkseksjon> reflinkSeksjoner){
    	List<ReflinkSeksjon> refList = convertVeinettreflinkseksjonToReflinkSeksjon(reflinkSeksjoner);
        return getGeometryWithinReflinkSections(refList);
    }
    /**
     * Konverterer Veinettreflinkseksjon som benyttes mot databasen til Reflinkseksjon som benyttes av webservice
     * @param list
     * @return
     */
    private List<ReflinkSeksjon> convertVeinettreflinkseksjonToReflinkSeksjon(List<Veinettreflinkseksjon> list){
        List<ReflinkSeksjon> reflinkseksjoner = new ArrayList<ReflinkSeksjon>();
        for (Veinettreflinkseksjon p:list){
            ReflinkSeksjon ref = new ReflinkSeksjon();
            ref.setFra(p.getFra());
            ref.setTil(p.getTil());
            ref.setReflinkId(p.getReflinkIdent().intValue());
            
            reflinkseksjoner.add(ref);
        }
        return reflinkseksjoner;
    }
    
    /**
     * Konverterer en liste med GeometryVector objekter til en liste med Point objekter
     * @param vectorList listen med GeometryVector objekter
     * @return liste med Point objekter
     */
    @SuppressWarnings("unused")
	private List<Point> convertGeometryVectorToPointList(List<GeometryVector> vectorList){
    	log.debug("convertGeometryVectorToPointList("+vectorList.size()+")");
        List<Point> pointList = new ArrayList<Point>();
        for (GeometryVector g:vectorList){
            Point p = new Point();
            p.setLocation(g.getEast(), g.getNorth());
            pointList.add(p);
        }
        log.debug("Returning ("+pointList.size()+")");
        return pointList;
    }
    
    
    
    private GisServicesSoap getGisService(){
    	
    	if (gisService==null){
    		gisService = new GisServices(null, new QName("http://www.mesta.no/GisEngineWebServices/", "GisServices")).getGisServicesSoap();
    	}
    	
    	return gisService;
    }
}
