package no.mesta.mipss.klimadata.wsklima;

import java.io.Serializable;

@SuppressWarnings("serial")
public class StasjonVO implements Serializable {
	private String fylke;
	private String navn;
	private int stnr;
	private int utmX;
	private int utmY;

	public StasjonVO() {

	}

	/**
	 * @return the fylke
	 */
	public String getFylke() {
		return fylke;
	}

	public String getNavn() {
		return navn;
	}

	/**
	 * @return the stnr stnr er stasjonsnummeret til målestasjonen
	 */
	public int getStnr() {
		return stnr;
	}

	public int getUtmX() {
		return utmX;
	}

	public int getUtmY() {
		return utmY;
	}

	/**
	 * Fylke
	 * 
	 * @param fylke
	 *            the fylke to set
	 */
	public void setFylke(String fylke) {
		this.fylke = fylke;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * @param stnr
	 *            the stnr to set
	 */
	public void setStnr(int stnr) {
		this.stnr = stnr;
	}

	public void setUtmX(int utmX) {
		this.utmX = utmX;
	}

	public void setUtmY(int utmY) {
		this.utmY = utmY;
	}

}
