package no.mesta.mipss.klimadata.wsklima;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class KlimadataVO implements Serializable{
	private String id;
	private Integer quality;
	private String value;
	private ElementVO element;
	private Date dateFrom;
	private Date dateTo;
	
	public KlimadataVO(){
		
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the quality
	 */
	public Integer getQuality() {
		return quality;
	}
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Integer quality) {
		this.quality = quality;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the element
	 */
	public ElementVO getElement() {
		return element;
	}
	/**
	 * @param element the element to set
	 */
	public void setElement(ElementVO element) {
		this.element = element;
	}
	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the dateTo
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo the dateTo to set
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
}
