package no.mesta.mipss.klimadata.wsklima;

import no.mesta.mipss.util.ProxyUtil;
import no.met.metdata.proxy.MetDataServiceProxy;
import no.met.metdata.proxy.no_met_metdata_ElementProperties;

public class Test {
	public Test(){
		
		
		MetDataServiceProxy p = new MetDataServiceProxy();
		try {
			
			no_met_metdata_ElementProperties[] e = p.getElementsFromTimeserieType("0");
			for (no_met_metdata_ElementProperties ee:e){
				System.out.println(ee.getElemCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		new Test();
	}

}
