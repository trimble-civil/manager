package no.mesta.mipss.klimadata.wsklima;

import java.util.List;

import javax.ejb.Remote;

import no.mesta.mipss.klimadata.yr.WeatherData;

@Remote
public interface Klimadata {
	public static final String BEAN_NAME = "KlimadataBean";

	/**
	 * Test metode for å se om ws fungerer på serveren.
	 */
	List<StasjonVO> getStations();

	/**
	 * Henter data fra met for angitt stasjon
	 * 
	 * @param stnr
	 */
	List<KlimadataVO> getDataFromStation(Integer stnr);

	/**
	 * Henter værdata fra api.yr.no webservice
	 * 
	 * @param east
	 * @param north
	 * @return
	 */
	List<WeatherData> getWeatherDataFromYr(double east, double north);
	/**
	 * Henter værdata fra api.yr.no webservice
	 * 
	 * @param east
	 * @param north
	 * @return
	 */
	List<WeatherData> getWeatherDataFromYrLatLong(double lat, double lon);
}
