package no.mesta.mipss.klimadata.wsklima;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import no.mesta.mipss.klimadata.yr.WeatherData;
import no.mesta.mipss.klimadata.yr.WeatherService;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.util.ProxyUtil;
import no.met.metdata.proxy.MetDataServiceProxy;
import no.met.metdata.proxy.no_met_metdata_ElementProperties;
import no.met.metdata.proxy.no_met_metdata_Location;
import no.met.metdata.proxy.no_met_metdata_Metdata;
import no.met.metdata.proxy.no_met_metdata_StationProperties;
import no.met.metdata.proxy.no_met_metdata_TimeStamp;
import no.met.metdata.proxy.no_met_metdata_WeatherElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(name = Klimadata.BEAN_NAME, mappedName="ejb/"+Klimadata.BEAN_NAME)
public class KlimadataBean implements Klimadata, KlimadataLocal {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@EJB
	private KonfigParam konfig;

	/** {@inheritDoc} */
	
	public List<StasjonVO> getStations() {

		List<StasjonVO> stasjoner = new ArrayList<StasjonVO>();

		MetDataServiceProxy p = new MetDataServiceProxy();
		try {
			no_met_metdata_StationProperties[] stations = p.getStationsProperties("", "");
			for (no_met_metdata_StationProperties s : stations) {
				StasjonVO stasjon = new StasjonVO();
				stasjon.setUtmX(s.getUtm_e());
				stasjon.setUtmY(s.getUtm_n());
				stasjon.setNavn(s.getName());
				stasjon.setStnr(s.getStnr());
				stasjon.setFylke(s.getDepartment());
				stasjoner.add(stasjon);
				// log.debug(s.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return stasjoner;

	}
	
	public List<KlimadataVO> getDataFromStation(Integer stnr) {
		
		
		List<ElementVO> elements = getElementsForStation(stnr, "0");
		String elementString = elements.toString();
		MetDataServiceProxy p = new MetDataServiceProxy();
		log.debug("Henter data fra stasjon:" + stnr + " elementer:" + elementString);
		List<KlimadataVO> klimadataList = new ArrayList<KlimadataVO>();

		try {

			no_met_metdata_Metdata metData = p.getMetData("0", "", "", "", stnr.toString(), elementString, "", "", "");
			for (no_met_metdata_TimeStamp ts : metData.getTimeStamp()) {
				for (no_met_metdata_Location l : ts.getLocation()) {
					for (no_met_metdata_WeatherElement e : l.getWeatherElement()) {
						String id = e.getId();
						Integer quality = e.getQuality();
						String value = e.getValue();
						KlimadataVO k = new KlimadataVO();
						k.setId(id);
						k.setQuality(quality);
						k.setValue(value);
						if (value.equals("-99999"))
							continue;
						k.setElement(getElementType(elements, id));
						k.setDateFrom(ts.getFrom());
						k.setDateTo(ts.getTo());
						klimadataList.add(k);
					}
				}
			}
			log.debug(metData.getType());

		} catch (Exception e) {
			e.printStackTrace();
		}

		
		return klimadataList;
	}

	public List<WeatherData> getWeatherDataFromYr(double north, double east) {
		// TODO hack for å unngå logging fra Wire.java med HttpClient
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error");

		WeatherService s = new WeatherService(konfig);
		return s.getWeatherData(north, east);
	}
	public List<WeatherData> getWeatherDataFromYrLatLong(double lat, double lon) {
		// TODO hack for å unngå logging fra Wire.java med HttpClient
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error");

		WeatherService s = new WeatherService(konfig);
		
		return s.getWeatherDataLatLon(lat, lon);
	}

	private ElementVO getElementType(List<ElementVO> elements, String id) {
		ElementVO c = new ElementVO(id);
		int index = elements.indexOf(c);
		if (index != -1) {
			return elements.get(index);
		}
		return c;
	}

	private List<ElementVO> getElementsForStation(Integer stnr, String timeserietypeId) {
		MetDataServiceProxy p = new MetDataServiceProxy();
		List<ElementVO> elementsForStation = new ArrayList<ElementVO>();
		try {
			no_met_metdata_ElementProperties[] elements = p.getElementsFromTimeserieTypeStation(timeserietypeId, stnr);
			for (no_met_metdata_ElementProperties e : elements) {
				ElementVO element = new ElementVO(e.getElemCode(), e.getName());
				elementsForStation.add(element);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return elementsForStation;
	}

}
