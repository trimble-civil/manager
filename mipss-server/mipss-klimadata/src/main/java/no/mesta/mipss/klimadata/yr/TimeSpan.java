package no.mesta.mipss.klimadata.yr;

public enum TimeSpan {
	HOUR, 
	HOUR1,
	HOUR2,
	HOUR3,
	HOUR6
}
