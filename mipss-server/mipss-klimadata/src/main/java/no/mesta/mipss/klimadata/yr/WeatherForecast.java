package no.mesta.mipss.klimadata.yr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
/**
 * Represents weatherforcasts pr hour. Each WeatherData element in the list has forecasts with identical starting hours, but with
 * different time spans(0, 1, 2, 3 and 6)
 *  
 * @author harkul
 *
 */
public class WeatherForecast {
	private List<WeatherData> weatherData = new ArrayList<WeatherData>();
	private Date firstDate;

	public void addWeatherData(WeatherData data){
		weatherData.add(data);
		if (firstDate==null||data.getDateFrom().before(firstDate))
			firstDate = data.getDateFrom();
	}
	
	public List<WeatherData> getWeatherData(){
		return weatherData;
	}
	
	public Date getFirstDate(){
		return firstDate;
	}
	public void sortData(){
		Collections.sort(weatherData, c);
	}
	
	public WeatherData getWeatherDataHour(){
		WeatherData w = getWeatherDataOfSpan(TimeSpan.HOUR);
		WeatherData w2= getWeatherDataOfSpan(TimeSpan.HOUR1);
		if (w2!=null){
			w.setPrecipitation(w2.getPrecipitation());
			w.setWeatherId(w2.getWeatherId());
			w.setSymbol(w2.getSymbol());
		}
		return w;
		
	}
	public WeatherData getWeatherDataOfSpan(TimeSpan span){
		for (WeatherData w:weatherData){
			if (w.getTimeSpan().equals(span))
				return w;
		}
		return null;
	}
	
	private Comparator<WeatherData> c = new Comparator<WeatherData>() {
		public int compare(WeatherData o1, WeatherData o2) {
			if (o1.getDateTo().before(o2.getDateTo()))
				return -1;
			if (o1.getDateTo().after(o2.getDateTo()))
				return 1;
			return 0;
		}
	};
}
