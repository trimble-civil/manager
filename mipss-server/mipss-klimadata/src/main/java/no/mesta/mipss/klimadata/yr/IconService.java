package no.mesta.mipss.klimadata.yr;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import no.mesta.mipss.util.ProxyUtil;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;

public class IconService {

	private static HashMap<Integer, Image> symbolCache = new HashMap<Integer, Image>();
	
	private static IconService instance;
	
	private IconService(){
		
	}
	
	public static IconService getInstance(){
		if (instance==null){
			instance = new IconService();
		}
		return instance;
	}
	private DefaultHttpClient getClient(){
		return new DefaultHttpClient();
	}
	
	public Image getIcon(int symbolId){
		Image image = symbolCache.get(symbolId);
		if (image!=null){
			return image;
		}
		DefaultHttpClient client = getClient();
		String url ="http://api.yr.no/weatherapi/weathericon/1.1/?symbol="+symbolId+";content_type=image/png";

		HttpGet method = new HttpGet(url);
		try {
			HttpResponse response = client.execute((HttpUriRequest)method);
			image = ImageIO.read(response.getEntity().getContent());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        if (image!=null)
        	symbolCache.put(symbolId, image);
        return image;
	}
	
	public Image getBeaufortImage(int beaufort, double deg){
		
		return BeaufortScale.getImage(beaufort, degToRadians(deg));
	}
	 private static double degToRadians (double deg){
	        return (deg / 180.0 * Math.PI);
	 }
	
	static class BeaufortScale{
		public static Image getImage(int beaufort, double radians){
			return Beaufort.getImage(beaufort, radians);
		}
	}
	
	static class Beaufort{
		static int b = 0;
		public static Image getImage(int beaufort, double radians){
			//beaufort = b++;
			BufferedImage image = new BufferedImage(32,32, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = (Graphics2D)image.getGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(Color.black);
			g.rotate(radians, 15, 15);
			g.drawLine(15, 0, 15, 27);
			
			Polygon p = new Polygon();
            p.addPoint(15, 30);
            p.addPoint(11, 25);
            p.addPoint(19, 25);
            g.fillPolygon(p);
            
            if (beaufort==2){
            	g.drawLine(16, 5, 18, 5);
            }
            if (beaufort==3){
            	g.drawLine(16, 0, 22, 0);
            }
            if (beaufort==4){
            	g.drawLine(16, 0, 22, 0);
            	g.drawLine(16, 5, 18, 5);
            }
            if (beaufort==5){
            	g.drawLine(16, 0, 22, 0);
            	g.drawLine(16, 5, 22, 5);
            }
            if (beaufort==6){
            	g.drawLine(16, 0, 22, 0);
            	g.drawLine(16, 5, 22, 5);
            	g.drawLine(16, 10, 18, 10);
            }
            if (beaufort==7){
            	g.drawLine(16, 0, 22, 0);
            	g.drawLine(16, 5, 22, 5);
            	g.drawLine(16, 10, 22, 10);
            }
            if (beaufort==8){
            	g.drawLine(16, 0, 22, 0);
            	g.drawLine(16, 5, 22, 5);
            	g.drawLine(16, 10, 22, 10);
            	g.drawLine(16, 15, 18, 15);
            }
			
			return image;
		}
	}
	
}
