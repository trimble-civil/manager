package no.mesta.mipss.klimadata.wsklima;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ElementVO implements Serializable{
	private String code;
	private String name;
	
	
	public ElementVO(String code){
		this.code = code;
	}
	public ElementVO(String code, String name){
		this.code = code;
		this.name = name;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return code;
	}
	public int hashCode(){
		return code.hashCode();
	}
	public boolean equals(Object o){
		if (o==this) return true;
		if (!(o instanceof ElementVO)) return false;
		
		ElementVO other = (ElementVO)o;		
		return (other.getCode().equals(getCode()));
	}
}
