package no.mesta.mipss.klimadata.yr.export;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Forecast {
	private SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
	private String date;
	private List<HourForecast> hourForecast = new ArrayList<HourForecast>();;
	private String dayOfWeek;
	
	public String getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = f.format(date);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dow = c.get(Calendar.DAY_OF_WEEK);
		dayOfWeek=String.valueOf(dow-1);
		
	}
	public String getDayOfWeek(){
		return dayOfWeek;
	}
	public List<HourForecast> getHourForecast() {
		return hourForecast;
	}
	public void setHourForecast(List<HourForecast> hourForecast) {
		this.hourForecast = hourForecast;
	}
	
	
}
