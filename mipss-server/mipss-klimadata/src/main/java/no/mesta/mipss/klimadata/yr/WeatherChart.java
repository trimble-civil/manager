package no.mesta.mipss.klimadata.yr;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class WeatherChart extends JPanel{
	private int max;
	private int min;
	private int height;
	private int width;
	
	private Date start;
	private Date stopp;
	
	private Rectangle chart;
	
	private int marginLeft = 25;
	private int marginRight = 25;
	
	private int marginTop = 60;
	
	private int headerHeight = 40;
	private int footerHeight = 40;
	
	private int zeroDegreePixel;
	private int pxPrDegree = 1;
	private double degreeSpan;
	private static double MIN_TEMPERATUR_SPAN = 20;
	
	private int roundEdge = 10;
	
	private List<WeatherData> weatherList;
	
	private JDialog frame;
	private Color gridColor = Color.lightGray;
	private Color darkGridColor = Color.lightGray;
	private Color tickMarkerColor = Color.gray;
	private Color hourTextColor = Color.gray;
	private Color zerogridColor = Color.gray;
	private Color backgroundColor = Color.white;
	private Color borderColor = Color.black;
	private Color dateInfoColor = Color.black;
	private Color daySeparatorColor = Color.gray;
	private Color positiveDegreesColor = Color.red;
	private Color negativeDegreesColor = Color.blue;
	private Color dewPointColor = new Color(76, 165, 227);
	private Color precipitationColor = new Color(0,0.5f,1,0.7f);
	private Color precipitationValueColor = Color.black;
	private Color alternatingBackgroundColor = new Color(200,200,200,90);
	private int marginBottom;
	
	private boolean alternateColors = false;
	
	public WeatherChart(JDialog frame, List<WeatherData> weatherList){
		this.frame = frame;
		setWeatherList(weatherList);
	}
	
	
	public void setWeatherList(List<WeatherData> weatherList){
		this.weatherList = weatherList;
		start = weatherList.get(0).getDateFrom();
		stopp = weatherList.get(weatherList.size()-1).getDateFrom();
		double mx = weatherList.get(0).getDegree();
		double mn = weatherList.get(0).getDegree();
		for (WeatherData w:weatherList){
			double deg = w.getDegree();
			if (deg>mx){
				mx = deg;
			}
			if (deg<mn){
				mn = deg;
			}
		}
		min = (int)Math.floor(mn);
		max = (int)Math.ceil(mx);
		degreeSpan = 0;
		if (min>0){
			degreeSpan = max-min;
		}else
			degreeSpan = max+Math.abs(min);
		
		//use a minimum of MIN_TEMPERATUR_SPAN degrees in the chart
		if (degreeSpan<MIN_TEMPERATUR_SPAN){
			//append to the max to place the lines in the middle of the chart
			int addMax = (int)(Math.ceil((MIN_TEMPERATUR_SPAN-degreeSpan)/2));
			degreeSpan=MIN_TEMPERATUR_SPAN;
			max+=addMax;
		}
		pxPrDegree = (int)Math.ceil(degreeSpan/getHbarCount());
	}
	public void setMax(int max){
		this.max = max;
		frame.repaint();
	}
	public void setMin(int min){
		this.min = min;
		frame.repaint();
	}
	public double getMax(){
		return max;
	}
	public double getMin(){
		return min;
	}
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getStopp() {
		return stopp;
	}

	public void setStopp(Date stopp) {
		this.stopp = stopp;
	}
	
	public void paint(Graphics g){
		AffineTransform transform = ((Graphics2D)g).getTransform();
		calcChart();
		chart =  new Rectangle(marginLeft,marginTop,width, height);
		g.setColor(backgroundColor);
		g.fillRoundRect(chart.x, chart.y-headerHeight, chart.width, chart.height+headerHeight+footerHeight, roundEdge, roundEdge);
		g.setColor(Color.black);
		drawChart(g);
		
		((Graphics2D)g).setTransform(transform);
		g.setColor(borderColor);
		g.drawRoundRect(chart.x, chart.y-headerHeight, chart.width, chart.height+headerHeight+footerHeight+1, roundEdge, roundEdge);
	}
	
	/**
	 * Tegn grafen p� Grafikk-konteksten
	 * @param g
	 */
	private void drawChart(Graphics g){
		drawGrid(g);
		drawHeader(g);
		drawLinesAndSymbols(g);
		drawDewPoint(g);
		drawFooter(g);
		
	}
	
	/**
	 * Tegn bakgrunnen til grafen
	 * @param g
	 */
	private void drawGrid(Graphics g){
		g.translate(chart.x, chart.y);
		
//		Color lightGray = new Color(220,220,220);
		
		int vBarCount = getVbarCount();
		int vBarWidth = chart.width/vBarCount;
		g.setColor(gridColor);
		for (int i=0;i<=vBarCount;i++){
			int x = i*vBarWidth;
			if (i!=0&&i%2==0)
				g.drawLine(x, 0, x, chart.height);
		}
		
		int hBarCount = getHbarCount();
		int hBarHeight = chart.height/hBarCount;
		boolean zeroFound = false;
		for (int i=0;i<=hBarCount;i++){
			int y = i*hBarHeight;
			g.setColor(gridColor);
			//draw dark line for 0 degrees
			if (pxPrDegree==1){
				if (max-i==0){
					g.setColor(zerogridColor);
					zeroDegreePixel = y;
				}
			}
			else{
				if (!zeroFound){
					if ((max-i*pxPrDegree)==1){
						g.setColor(zerogridColor);
						int nullMeridianY = y+(hBarHeight/2);
						zeroDegreePixel = nullMeridianY;
						g.drawLine(0, nullMeridianY, chart.width, nullMeridianY);
						g.setColor(gridColor);
						zeroFound = true;
					}
					if ((max-i*pxPrDegree)==0){
						g.setColor(zerogridColor);
						zeroDegreePixel = y;
						zeroFound = true;
					}
				}
			}
			
			if (alternateColors&&i!=0&&i%2!=0){
				g.setColor(alternatingBackgroundColor);
				g.fillRect(0, y, chart.width, hBarHeight);
			}else{
				g.drawLine(0, y, chart.width, y);
			}
			
			g.setColor(tickMarkerColor);
			g.drawLine(-2, y, 2, y);
			g.drawLine(chart.width-2, y, chart.width+2, y);
			if (i%2==0)
				drawHtext(g, i, y);
		}
	}
	
	/**
	 * Tegn overdelen av grafen
	 * @param g
	 */
	private void drawHeader(Graphics g){
		g.translate(0, -headerHeight);
		g.setColor(backgroundColor);
		g.fillRoundRect(0, 0, chart.width, headerHeight, roundEdge, roundEdge);
		
		int vCount = getVbarCount();
		int vWidth = chart.width/vCount;
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		DateFormat format = new SimpleDateFormat("EEEE, dd.MMMM", new Locale("no", "no"));
		String font = "Tahoma";
		Font bold = new Font(font, Font.BOLD, 12);
		Font plain = new Font(font, Font.PLAIN, 11);
		for (int i=0;i<=vCount;i++){
			if (c.get(Calendar.HOUR_OF_DAY)==0){
				int x = i*vWidth;
				String formatted = format.format(c.getTime());
				String d = formatted.substring(0, formatted.indexOf(','));
				String day = d.substring(0,1).toUpperCase()+d.substring(1);
				String time = formatted.substring(formatted.indexOf(' ')+1, formatted.length());
				g.setColor(dateInfoColor);
				g.setFont(bold);
				g.drawString(day, x+5, 14);
				int xPad = (int)g.getFontMetrics(bold).getStringBounds(day, g).getWidth();
				g.setFont(plain);
				g.drawString(time, x+10+xPad, 14);
				g.setColor(daySeparatorColor);
				g.drawLine(x, 2, x, chart.height+headerHeight);
			}
			c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY)+1);
		}
		g.translate(0, headerHeight);
	}
	
	private void drawDewPoint(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Point prev = null;
		BasicStroke str = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10f);
		g2.setStroke(str);
		g2.setColor(dewPointColor);
		int i =-1;
//		GeneralPath path = new GeneralPath();
		for (WeatherData w:weatherList){
			Point p = valueToPixel(w.getDateFrom(), w.getDewPoint());
			if (prev!=null){
//				path.curveTo(arg0, arg1, arg2, arg3, p.x, p.y)
				Line2D line = new Line2D.Double(prev.x, prev.y, p.x, p.y);
				g2.draw(line);
			}else{
//				path.moveTo(p.x, p.y);
			}
			i++;
			prev = p;
		}
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		g2.setStroke(new BasicStroke());
	}
	/**
	 * Tegn linjer og symboler til grafen
	 * @param g
	 */
	private void drawLinesAndSymbols(Graphics g){
		
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Point prev = null;
		BasicStroke str = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10f);
		g2.setStroke(str);
		Line2D zero = new Line2D.Double(0, zeroDegreePixel, chart.width, zeroDegreePixel);
		Font smallFont = new Font("Tahoma", Font.PLAIN, 7);
		int i =-1;
		for (WeatherData w:weatherList){
			Point p = valueToPixel(w.getDateFrom(), w.getDegree());
			if (prev!=null){
				Line2D line = new Line2D.Double(prev.x, prev.y, p.x, p.y);
				boolean draw = true;
				if (max>0&&min>0){
					g2.setColor(positiveDegreesColor);
					g2.draw(line);
					draw=false;
				}
				if (max<0&&min<0){
					g2.setColor(negativeDegreesColor);
					g2.draw(line);
					draw=false;
				}
				if (draw){
					//both over zero
					if (line.getY1()<=zeroDegreePixel&&line.getY2()<=zeroDegreePixel){
						g2.setColor(positiveDegreesColor);
						g2.draw(line);
					}
					//both under zero
					if (line.getY1()>zeroDegreePixel&&line.getY2()>zeroDegreePixel){
						g2.setColor(negativeDegreesColor);
						g2.draw(line);
					}
					
					//first point over zero
					if (line.getY1()<=zeroDegreePixel&&line.getY2()>zeroDegreePixel){
						Point inter = findIntersection(line.getP1(), line.getP2(), zero.getP1(), zero.getP2());
						int intersectX = inter.x;
						
						//split line
						Line2D under = new Line2D.Double(line.getX1(), line.getY1(), intersectX, zeroDegreePixel);
						g2.setColor(positiveDegreesColor);
						g2.draw(under);
						Line2D over = new Line2D.Double(intersectX, zeroDegreePixel, line.getX2(), line.getY2());
						g2.setColor(negativeDegreesColor);
						g2.draw(over);
					}
					//second point over zero
					if (line.getY1()>zeroDegreePixel&&line.getY2()<=zeroDegreePixel){
						int intersectX = findIntersection(line.getP1(), line.getP2(), zero.getP1(), zero.getP2()).x;
						Line2D over = new Line2D.Double(line.getX1(), line.getY1(), intersectX, zeroDegreePixel);
						g2.setColor(negativeDegreesColor);
						g2.draw(over);
						Line2D under = new Line2D.Double(intersectX, zeroDegreePixel, line.getX2(), line.getY2());
						g2.setColor(positiveDegreesColor);
						g2.draw(under);
					}
				}
			}
			if (i%2==0 && i!=weatherList.size()-1){
				Image sym = w.getSymbol();
				if(sym!=null)
					g.drawImage(sym, p.x-(sym.getWidth(this)/2), p.y-sym.getHeight(this)-10, this);
				
			}
			if (w.getPrecipitation()!=0){
				int precipPixelHeight = (int)(w.getPrecipitation()*(getHeight()/50));
				int vBarCount = getVbarCount();
				int vBarWidth = chart.width/vBarCount;
				int x = i*vBarWidth;
				//flytt 0,0 til stedet der nedbørstolpen skal tegnes
				g.translate(chart.x+x, chart.height-precipPixelHeight);
				g.setColor(precipitationColor);
				
				g.fillRect(-7, 0, 16, precipPixelHeight);
				
				g.setFont(smallFont);
				g.setColor(precipitationValueColor);
				g.drawString(w.getPrecipitation()+"", -4, -5);
				//flytt 0,0 tilbake
				g.translate(-((chart.x+x)), -(chart.height-precipPixelHeight));
			}
			i++;
			prev = p;
		}
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		g2.setStroke(new BasicStroke());
		
	}
	
	/**
	 * Tegn bunnen av grafen. 
	 * @param g
	 */
	private void drawFooter(Graphics g){
		Font plain = new Font("Tahoma", Font.PLAIN, 11);
		g.setFont(plain);
		
		g.translate(-marginLeft, 0);
		
		g.setColor(backgroundColor);
		g.fillRoundRect(chart.x, chart.height+1, chart.width, footerHeight, roundEdge, roundEdge);
		g.translate(chart.x, chart.height+1);
		int vCount = getVbarCount();
		int vWidth = chart.width/vCount;
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		int index = 0;
		for (WeatherData w: weatherList){
			int x = index*vWidth;
			if (index==0||index%2==0&&index!=weatherList.size()-1){
				g.setColor(darkGridColor);
				if (index!=0)
					g.drawLine(x, 0, x, footerHeight);
				//paint image..
				g.drawImage(w.getWindIcon(),x+3, 4, this);
			}
			g.setColor(tickMarkerColor);
			g.drawLine(x, -2, x, 2);
			if (index!=0)
				g.drawLine(x, footerHeight-2, x, footerHeight+2);
			//paint the hour of day
			if (index!=0&&index%2!=0){
				int time = c.get(Calendar.HOUR_OF_DAY);
				int center = (time<10)?3:6;
				g.setColor(hourTextColor);
				g.drawString(""+time, x-center, footerHeight+15);
			}
			
			index++;
			c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY)+1);
		}
		g.drawLine(roundEdge/2, footerHeight, chart.width-(roundEdge/2), footerHeight);
		
	}
	
	/**
	 * Antall horisontale streker, en strek representerer en grad (+, 0 eller -)
	 * @return
	 */
	private int getHbarCount(){
		return (int)degreeSpan;
		
	}
	/**
	 * Antall vertikale streker, en strek representerer en time
	 * @return
	 */
	private int getVbarCount(){
		long end = stopp.getTime();
		long st = start.getTime();
		long span = end-st;
		
		return (int)(span/1000/60/60)+1;
	}
	
	/**
	 * Tegner gradeteksten
	 * @param g
	 * @param i
	 * @param y
	 */
	private void drawHtext(Graphics g, int i, int y){
		g.setColor(Color.black);
		g.drawString((max-(i*pxPrDegree))+"\u00B0", -marginLeft+2, y+5);
	}
	
	private Point valueToPixel(Date date, double degrees){
		long st = start.getTime();
		int span = (int)(stopp.getTime()-st);
		long cur = date.getTime();
		int w = chart.width;
		int msPrPx =  span/w;
		int pos = (int)(cur-st);
		int x = pos/msPrPx;
		Point p = new Point();
		p.x = x;
		
		if (degrees<0){
			double app = max;
			int hbarCount = getHbarCount(); 
			double deg = Math.abs(degrees)+app;
			int h = chart.height;
			double degPrPx = h/hbarCount;
			int y = (int)(deg*degPrPx);
			p.y = y;
		}else{
			double deg = max-degrees;
			int h= chart.height;
			int hBarCount = getHbarCount();
			double degPrPx =h/hBarCount;
			int y =(int)(deg*degPrPx);
			p.y = y;
		}
		p.y = p.y/pxPrDegree;
		return p;
	}
	
	private void calcChart(){
		calcDimensions();
	}
	private void calcDimensions(){
		int h = (int)((getHeight()-(marginTop+headerHeight+marginBottom+15))/getHbarCount());
		height = getHbarCount()*h;
		width = getWidth()-(marginLeft+marginRight);
	}
	
	 /**
	  * kalkuler krysningen mellom to linjer, sjekker etter paralelle linjer
	  * sjekker også at krysningspunktet faktisk er på linjesegmentet p1-p2
	  */
	 private Point findIntersection(Point2D p1,Point2D p2,  
	   Point2D p3,Point2D p4) {  
	   double xD1,yD1,xD2,yD2,xD3,yD3;  
	   double dot,deg,len1,len2;  
	   double ua,div;  
	   
	   // kalkuler forskjellene
	   xD1=p2.getX()-p1.getX();  
	   xD2=p4.getX()-p3.getX();  
	   yD1=p2.getY()-p1.getY();  
	   yD2=p4.getY()-p3.getY();  
	   xD3=p1.getX()-p3.getX();  
	   yD3=p1.getY()-p3.getY();    
	   
	   // kalkuler lengden til de to linjene  
	   len1=Math.sqrt(xD1*xD1+yD1*yD1);  
	   len2=Math.sqrt(xD2*xD2+yD2*yD2);  
	   
	   // calculate angle between the two lines.  
	   dot=xD1*xD2+yD1*yD2; // dot product  
	   deg=dot/(len1*len2);  
	   
	   // if abs(angle)==1 then the lines are parallell,  
	   // so no intersection is possible  
	   if(Math.abs(deg)==1) return null;  
	   
	   // find intersection Pt between two lines  
	   Point pt=new Point(0,0);  
	   div=yD2*xD1-xD2*yD1;  
	   ua=(xD2*yD3-yD2*xD3)/div;  
	   pt.x=(int)(p1.getX()+ua*xD1);  
	   pt.y=(int)(p1.getY()+ua*yD1);  
	   return pt;  
	 }

	public void setRoundEdge(int roundEdge) {
		this.roundEdge = roundEdge;
	}


	public void setGridColor(Color gridColor) {
		this.gridColor = gridColor;
	}


	public void setDarkGridColor(Color darkGridColor) {
		this.darkGridColor = darkGridColor;
	}


	public void setTickMarkerColor(Color tickMarkerColor) {
		this.tickMarkerColor = tickMarkerColor;
	}


	public void setHourTextColor(Color hourTextColor) {
		this.hourTextColor = hourTextColor;
	}


	public void setZerogridColor(Color zerogridColor) {
		this.zerogridColor = zerogridColor;
	}


	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}


	public void setBorderColor(Color borderColor) {
		this.borderColor = borderColor;
	}


	public void setDateInfoColor(Color dateInfoColor) {
		this.dateInfoColor = dateInfoColor;
	}


	public void setDaySeparatorColor(Color daySeparatorColor) {
		this.daySeparatorColor = daySeparatorColor;
	}


	public void setPositiveDegreesColor(Color positiveDegreesColor) {
		this.positiveDegreesColor = positiveDegreesColor;
	}


	public void setNegativeDegreesColor(Color negativeDegreesColor) {
		this.negativeDegreesColor = negativeDegreesColor;
	}


	public void setPrecipitationColor(Color precipitationColor) {
		this.precipitationColor = precipitationColor;
	}


	public void setPrecipitationValueColor(Color precipitationValueColor) {
		this.precipitationValueColor = precipitationValueColor;
	}
	
	public void setMarginLeft(int marginLeft) {
		this.marginLeft = marginLeft+24;
	}
	public void setMarginRight(int marginRight) {
		this.marginRight = marginRight+3;
	}

	public void setMarginTop(int marginTop) {
		this.marginTop = marginTop+headerHeight;
	}
	public void setMarginBottom(int marginBottom){
		this.marginBottom = marginBottom;
	}
	public void setMargins(int left, int right, int top, int bottom){
		setMarginLeft(left);
		setMarginRight(right);
		setMarginTop(top);
		setMarginBottom(bottom);
	}
	public void setMargins(int margin){
		setMargins(margin, margin, margin, margin);
	}
	
	public void setAlternateColor(boolean alternateColors){
		this.alternateColors = alternateColors;
	}
	 
//	public static Image arrowImage = Toolkit.getDefaultToolkit().getImage("arrow2.png");
//	public static Image image = Toolkit.getDefaultToolkit().getImage("symbol.png");
//	static Calendar cal = Calendar.getInstance();
//	
//	public static Weather w(){
//		Random r = new Random();
//		r.nextInt(10);
//		Weather w = new Weather();
//		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//		w.setDate(cal.getTime());
//		w.setSymbol(image);
//		w.setWind(arrowImage);
//		w.setDegree(r.nextDouble());
//		return w;
//	}
//	public static List<Weather> getList(){
//		List<Weather> list = new ArrayList<Weather>();
//		for (int i=0;i<48;i++){
//			
//			if (i==30){
//				cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//				list.add(c(cal.getTime(), 2, image));
//			}
//			else if (i==20){
//				cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//				list.add(c(cal.getTime(), -2, image));
//			}
//			else{
//				list.add(w());
//			}
//		}
//		return list;
//	}
//	public static Weather c(Date date, double degree, Image image){
//		Weather w = new Weather();
//		w.setDate(date);
//		w.setDegree(degree);
//		w.setSymbol(image);
//		w.setWind(arrowImage);
//		return w;
//	}
}


//Image symbol = Toolkit.getDefaultToolkit().getImage("symbol.png");
//
//Calendar cal = Calendar.getInstance();
//wList.add(c(cal.getTime(), 6, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 7, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 7.5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 3, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 2, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 7, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 7, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 7, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), -2, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 3, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 6, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 4, symbol));
//
//wList.add(c(cal.getTime(), 4, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 6, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 7, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 3, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), -1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), -1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), -1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 2, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 2, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 3, symbol));
//
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 5, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 3, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), -1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//System.out.println(cal.getTime());
//wList.add(c(cal.getTime(), -1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 8, symbol));
//
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), -1, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 2, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 2, symbol));
//cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)+1);
//wList.add(c(cal.getTime(), 3, symbol));
