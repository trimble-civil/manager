package no.mesta.mipss.klimadata.wsklima;

import no.mesta.mipss.klimadata.yr.WeatherData;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface KlimadataLocal {
	/**
	 * Henter værdata fra api.yr.no webservice
	 * 
	 * @param east
	 * @param north
	 * @return
	 */
	List<WeatherData> getWeatherDataFromYr(double east, double north);
	List<WeatherData> getWeatherDataFromYrLatLong(double lat, double lon);
}
