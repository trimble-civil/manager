package no.mesta.mipss.klimadata.yr;


import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Representerer værdata som er hentet fra api.yr.no
 * 
 * @author @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class WeatherData implements Serializable{

	private Date dateFrom;
	private Date dateTo;
	private double degree;
	private byte[] symbol;
	private double precipitation;
	private double windDirection;
	private double windSpeed;
	private float humidity;
	private int beaufort;
	private byte[] windIcon;
	private int weatherId;
	private TimeSpan timeSpan;
	private double min;
	private double max;
	private double dewPoint;
	
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date date) {
		this.dateFrom = date;
	}
	public void setDateTo(Date date){
		this.dateTo = date;
	}
	public Date getDateTo(){
		return this.dateTo;
	}
	public double getDegree() {
		return degree;
	}
	public void setDegree(double degree) {
		this.degree = degree;
	}
	public void setDewPoint(double dewPoint) {
		this.dewPoint = dewPoint;
	}
	public Image getSymbol() {
		if (symbol!=null){
			try {
				BufferedImage im = ImageIO.read(new ByteArrayInputStream(symbol));
				return im;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
		
	}
	public void setSymbol(Image symbol) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try	{
			ImageIO.write((RenderedImage) symbol, "png", baos);
		}
		catch (IOException e){
			throw new IllegalStateException(e.toString());
		}
		byte[] b = baos.toByteArray();
		
		this.symbol = b;
	}
	
	public double getPrecipitation(){
		return precipitation;
	}
	public void setPrecipitation(double precipitation){
		this.precipitation = precipitation;
	}
	public float getHumidity(){
		return humidity;
	}
	public void setHumidity(float humidity){
		this.humidity = humidity;
	}
	public void setMinMax(double min, double max){
		this.min = min;
		this.max = max;
	}


	/**
	 * @return the windDirection
	 */
	public double getWindDirection() {
		return windDirection;
	}
	/**
	 * @param windDirection the windDirection to set
	 */
	public void setWindDirection(double windDirection) {
		this.windDirection = windDirection;
	}
	/**
	 * @return the beaufort
	 */
	public int getBeaufort() {
		return beaufort;
	}
	/**
	 * @param beaufort the beaufort to set
	 */
	public void setBeaufort(int beaufort) {
		this.beaufort = beaufort;
	}
	
	public Image getWindIcon() {
		if (windIcon!=null){
			try {
				BufferedImage im = ImageIO.read(new ByteArrayInputStream(windIcon));
				return im;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
		
	}
	public void setWindIcon(Image symbol) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try	{
			ImageIO.write((RenderedImage) symbol, "png", baos);
		}
		catch (IOException e){
			throw new IllegalStateException(e.toString());
		}
		byte[] b = baos.toByteArray();
		this.windIcon = b;
	}
	
	public void setWeatherId(int weatherId) {
		this.weatherId = weatherId;
	}
	public int getWeatherId() {
		return weatherId;
	}
	public void setTimeSpan(TimeSpan timeSpan) {
		this.timeSpan = timeSpan;
	}
	public TimeSpan getTimeSpan() {
		return timeSpan;
	}
	public double getDewPoint(){
		double laplace = 6.112;
		double T = degree;
		double RH = humidity;
		double es = laplace*Math.exp(17.76*T/(T+243.5));
        double ex = (RH*es)/100;
        double dewPoint = (243.5*Math.log(ex/laplace))/(17.67-Math.log(ex/laplace));
        
        double result = Math.round(dewPoint*100);
        result = result / 100;
		return result;
	}
	public boolean equals(Object other){
		if (!(other instanceof WeatherData))
			return false;
		WeatherData o = (WeatherData)other;
		return new EqualsBuilder()
				.append(getDateFrom(), o.getDateFrom())
				.append(getDateTo(), o.getDateTo())
				.isEquals();
		
	}
	
	public String toString(){
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
		return (getDateFrom()!=null?f.format(getDateFrom()):null)+"\t"+(dateTo!=null?f.format(dateTo):null)+"\tt="+getDegree()+"\tp="+getPrecipitation()+"\tb="+getBeaufort()+"\tw="+getWindDirection()+"\t"+getTimeSpan()+"\t"+weatherId+"\t"+min+","+max;
	}
	public void setWindSpeed(double windSpeed) {
		this.windSpeed = windSpeed;
	}
	public double getWindSpeed() {
		return windSpeed;
	}
}
