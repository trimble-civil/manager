package no.mesta.mipss.klimadata.yr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Klasse for å konvertere time_type_attr_value til en date.
 * 
 * <xs:pattern value="\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}" />
 * 
 * @author harkul
 *
 */
public class TimeTypeMarshaller {
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ssZ";
	
	public static Date unmarshal(String date){
		date = date.replaceAll("T", " ");
		SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT);
		try {
			return f.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String marshal(Date date) {
		// TODO Auto-generated method stub
		return null;
	}
}
