package no.mesta.mipss.klimadata.yr;

import java.awt.Image;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.klimadata.LatLongUtils;
import no.mesta.mipss.klimadata.yr.vo.LocationType;
import no.mesta.mipss.klimadata.yr.vo.Precipitation;
import no.mesta.mipss.klimadata.yr.vo.ProductType;
import no.mesta.mipss.klimadata.yr.vo.Temperature;
import no.mesta.mipss.klimadata.yr.vo.TimeType;
import no.mesta.mipss.klimadata.yr.vo.UnitValue;
import no.mesta.mipss.klimadata.yr.vo.Weatherdata;
import no.mesta.mipss.klimadata.yr.vo.Windspeed;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.persistence.Clock;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Tjenesteklasse for å hente værdata fra YR.
 *
 * Trenger en refactor!!.
 *
 * @author harkul
 *
 */
public class WeatherService {

    private final KonfigParam konfig;

    public WeatherService(KonfigParam konfig) {
        this.konfig = konfig;
    }

    public List<WeatherData> getWeatherDataLatLon(double lat, double lon) {
        return getWeatherData("" + lat, "" + lon);
    }

    public List<WeatherData> getWeatherData(double north, double east) {
        Point2D.Double point = LatLongUtils.utmToLat(east, north);
        String lat = point.x + "";
        String lon = point.y + "";

        return getWeatherData(lat, lon);
    }

    /**
     * rekursiv fjerning av alle noder med angitt nodeklasse
     *
     * @param node
     * @param nodeClass
     */
    public void removeAll(Node node, String nodeClass) {
        if (node.getClass().getName().equals(nodeClass)) {
            node.getParentNode().removeChild(node);
        } else {
            NodeList list = node.getChildNodes();
            for ( int i = 0; i < list.getLength(); i++ ) {
                removeAll(list.item(i), nodeClass);
            }
        }
    }

    public void printAll(Node node, String tab) {
        System.out.println(tab + node.getNodeName() + " " + node.getNodeValue() + " " + node.getClass());
        NodeList list = node.getChildNodes();
        for ( int i = 0; i < list.getLength(); i++ ) {
            printAll(list.item(i), tab);
        }
    }

    public String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ( ( line = reader.readLine() ) != null ) {
                sb.append(line).append("\n");
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public List<WeatherText> getTextForecast(String lat, String lon) {
        return new WeatherServiceTextforecast().getTextForecast(this, lat, lon);
    }

    public List<WeatherData> getWeatherData(String lat, String lon) {
        DefaultHttpClient client = getClient();

        try {
            String service = konfig.hentEnForApp("studio.mipss.mapplugin", "url_locationforecast").getVerdi();
            String url = service + "lat=" + lat + "&lon=" + lon;
            System.out.println("Vær-url: " + url);
            HttpGet method = new HttpGet(url);
            HttpResponse response = client.execute((HttpUriRequest) method);

            InputStream rstream = response.getEntity().getContent();
            String s = convertStreamToString(rstream);
            JAXBContext jc = JAXBContext.newInstance("no.mesta.mipss.klimadata.yr.vo");
            Unmarshaller u = jc.createUnmarshaller();
            Weatherdata d = (Weatherdata) u.unmarshal(new StreamSource(new StringReader(s)));

            Map<Date, WeatherData> weatherList = new HashMap<Date, WeatherData>();
            for ( ProductType p : d.getProduct() ) {
                for ( TimeType t : p.getTime() ) {
                    if (t.getFrom() == null) {
                        continue;
                    }
                    Calendar c = convertCalendar(t.getFrom());
                    Date from = c.getTime();
                    WeatherData weather = weatherList.get(from);
                    if (weather == null) {
                        weather = new WeatherData();
                        weather.setDateFrom(from);
                        weatherList.put(from, weather);
                    }

                    extractDataFromTimeType(t, weather);
                }
            }
            List<WeatherData> tempWeatherList = new ArrayList<WeatherData>();
            Iterator<WeatherData> it = weatherList.values().iterator();
            while ( it.hasNext() ) {
                WeatherData w = it.next();
                tempWeatherList.add(w);
            }
            Comparator<WeatherData> c = new Comparator<WeatherData>() {
                @Override
                public int compare(WeatherData o1, WeatherData o2) {
                    if (o1.getDateFrom().before(o2.getDateFrom())) {
                        return -1;
                    }
                    if (o1.getDateFrom().after(o2.getDateFrom())) {
                        return 1;
                    }
                    return 0;
                }
            };
            Collections.sort(tempWeatherList, c);

            List<WeatherData> finalList = new ArrayList<WeatherData>();
            for ( int i = 0; i < tempWeatherList.size(); i++ ) {
                if (i < 48) {
                    finalList.add(tempWeatherList.get(i));
                } else {
                    break;
                }
            }
            System.out.println(url);
            return finalList;

        } catch ( JAXBException e ) {
            e.printStackTrace();
            throw new RuntimeException("JAXB failed", e);
        } catch ( ClientProtocolException e ) {
            e.printStackTrace();
            throw new RuntimeException("Html failed", e);
        } catch ( IOException e ) {
            e.printStackTrace();
            throw new RuntimeException("IOException", e);
        } catch ( IllegalStateException e ) {
            e.printStackTrace();
            throw new RuntimeException("Illegal state", e);
        } catch ( Throwable t ) {
            t.printStackTrace();
            throw new RuntimeException(t);
        }
    }

    DefaultHttpClient getClient() {
        return new DefaultHttpClient();
    }

    /**
     * Refactor til å returnere et objekt
     *
     * @param lat
     * @param lon
     * @return
     */
    public List<WeatherForecast> getWeatherDataNewVersion(String lat, String lon) {
        DefaultHttpClient client = getClient();
        try {

            String service = konfig.hentEnForApp("studio.mipss.mapplugin", "url_locationforecast").getVerdi();
            String url = service + "lat=" + lat + "&lon=" + lon;
            HttpGet method = new HttpGet(url);
            HttpResponse response = client.execute(method);

            InputStream rstream = response.getEntity().getContent();
            String s = convertStreamToString(rstream);
            JAXBContext jc = JAXBContext.newInstance("no.mesta.mipss.klimadata.yr.vo");
            Unmarshaller u = jc.createUnmarshaller();
            Weatherdata d = (Weatherdata) u.unmarshal(new StreamSource(new StringReader(s)));

            Map<Date, WeatherForecast> weatherMap = new HashMap<Date, WeatherForecast>();
            for ( ProductType p : d.getProduct() ) {
                Comparator<TimeType> comp = new Comparator<TimeType>() {
                    @Override
                    public int compare(TimeType o1, TimeType o2) {
                        int val = o1.getFrom().compare(o2.getFrom());
                        if (val == 0) {
                            return o1.getTo().compare(o2.getTo());
                        }
                        return val;
                    }
                };
                Collections.sort(p.getTime(), comp);
                extractDataFromProductType(weatherMap, p);
            }

            //sorter listen på dato
            List<WeatherForecast> tempList = new ArrayList<WeatherForecast>();
            Set<Date> keySet = weatherMap.keySet();
            TreeSet<Date> sortedKeys = new TreeSet<Date>(keySet);
            for ( Date date : sortedKeys ) {
                WeatherForecast listWd = weatherMap.get(date);
                listWd.sortData();
                tempList.add(listWd);
            }
            return tempList;
        } catch ( JAXBException e ) {
            e.printStackTrace();
            throw new RuntimeException("JAXB failed", e);
        } catch ( ClientProtocolException e ) {
            e.printStackTrace();
            throw new RuntimeException("Html failed", e);
        } catch ( IOException e ) {
            e.printStackTrace();
            throw new RuntimeException("IOException", e);
        } catch ( IllegalStateException e ) {
            e.printStackTrace();
            throw new RuntimeException("Illegal state", e);
        } catch ( Throwable t ) {
            t.printStackTrace();
            throw new RuntimeException(t);
        }
    }

    private void extractDataFromProductType(
            Map<Date, WeatherForecast> weatherMap, ProductType p) {
        for ( TimeType t : p.getTime() ) {
            if (t.getFrom() == null) {
                continue;
            }
            Calendar c = convertCalendar(t.getFrom());
            Date from = c.getTime();
            //ignorer historiske data
            if (from.before(Clock.now())) {
                continue;
            }
            Date to = convertCalendar(t.getTo()).getTime();
            WeatherForecast dataList = weatherMap.get(from);
            if (dataList == null) {
                dataList = new WeatherForecast();
                weatherMap.put(from, dataList);
            }

            WeatherData weather = new WeatherData();
            weather.setDateFrom(from);
            weather.setDateTo(to);
            dataList.addWeatherData(weather);
            weather.setTimeSpan(getTimeSpan(from, to));

            extractDataFromTimeType(t, weather);
        }
    }

    private void extractDataFromTimeType(TimeType t, WeatherData weather) {
        for ( LocationType lt : t.getLocation() ) {
            List<JAXBElement<? extends Serializable>> e = lt.getGroundCoverAndPressureAndMaximumPrecipitation();
            for ( JAXBElement<? extends Serializable> el : e ) {
                Object value = el.getValue();

                // henter ut temperatur.
                if (value instanceof Temperature) {
                    Temperature temp = (Temperature) value;
                    if (( (Temperature) value ).getId().equals("TTT")) { //Vanlig temperatur						
                        weather.setDegree(temp.getValue().doubleValue());
                    } else if (( (Temperature) value ).getId().equals("TD")) {
                        weather.setDewPoint(temp.getValue().doubleValue());
                    }
                } // henter ut nedbør
                else if (value instanceof Precipitation) {
                    Precipitation precip = (Precipitation) value;
                    weather.setPrecipitation(precip.getValue().doubleValue());
                    weather.setMinMax(precip.getMinvalue() != null ? precip.getMinvalue().doubleValue() : 0, precip.getMaxvalue() != null ? precip.getMaxvalue().doubleValue() : 0);
                } // Henter ut symbol.
                else if (value instanceof LocationType.Symbol) {
                    LocationType.Symbol symbol = (LocationType.Symbol) value;
                    Image image = IconService.getInstance().getIcon(symbol.getNumber().intValue());
                    weather.setSymbol(image);
                    weather.setWeatherId(symbol.getNumber().intValue());
                } else if (value instanceof LocationType.WindDirection) {
                    LocationType.WindDirection wind = (LocationType.WindDirection) value;
                    weather.setWindDirection(wind.getDeg().doubleValue());
                } else if (value instanceof Windspeed) {
                    Windspeed wind = (Windspeed) value;
                    weather.setBeaufort(wind.getBeaufort().intValue());
                    if (wind.getMps() != null) {
                        weather.setWindSpeed(wind.getMps().doubleValue());
                    }
                    weather.setWindIcon(IconService.getInstance().getBeaufortImage(weather.getBeaufort(), weather
                                                                                   .getWindDirection()));// TODO implement
                    // better logic!
                    // :D
                } else if ("humidity".equals(el.getName().getLocalPart())) {
                    UnitValue uv = (UnitValue) value;
                    float humidity = uv.getValue();
                    weather.setHumidity(humidity);
                }
            }
        }
    }

    private TimeSpan getTimeSpan(Date from, Date to) {
        int hoursBetween = (int) ( ( to.getTime() - from.getTime() ) / ( 1000 * 60 * 60 ) );
        switch ( hoursBetween ) {
            case 0:
                return TimeSpan.HOUR;
            case 1:
                return TimeSpan.HOUR1;
            case 2:
                return TimeSpan.HOUR2;
            case 3:
                return TimeSpan.HOUR3;
            case 6:
                return TimeSpan.HOUR6;
        }
        return null;
    }

    /**
     * Konverter fra en XMLGregorianCalendar til en Calendar.
     *
     * @param c
     * @return
     */
    Calendar convertCalendar(XMLGregorianCalendar c) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, c.getYear());
        cal.set(Calendar.MONTH, c.getMonth() - 1);
        cal.set(Calendar.DAY_OF_MONTH, c.getDay());
        cal.set(Calendar.HOUR_OF_DAY, c.getHour());
        cal.set(Calendar.MINUTE, c.getMinute());
        cal.set(Calendar.SECOND, c.getSecond());
        cal.setTime(convertFromUTCtoCET(cal.getTime()));
        return cal;
    }

    /**
     * Konverterer en dato fra UTC til CET
     *
     * @param utcDate
     * @return
     */
    private Date convertFromUTCtoCET(Date utcDate) {
        Calendar cc = new GregorianCalendar();
        cc.setTime(utcDate);
        Calendar utcCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        utcCal.set(Calendar.YEAR, cc.get(Calendar.YEAR));
        utcCal.set(Calendar.MONTH, cc.get(Calendar.MONTH));
        utcCal.set(Calendar.DAY_OF_MONTH, cc.get(Calendar.DAY_OF_MONTH));
        utcCal.set(Calendar.HOUR_OF_DAY, cc.get(Calendar.HOUR_OF_DAY));

        Calendar c = new GregorianCalendar(TimeZone.getTimeZone("CET"));
        c.setTime(utcCal.getTime());
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
