package no.mesta.mipss.klimadata.yr.export;

public class HourForecast {
	private int fromHour;
	private int toHour;
	private int weatherCode;
	private double temperature;
	private double precipitation;
	private double beaufort;
	private float humidity;
	private double windSpeed;
	private double windDirection;
	private double dewpoint;
	
	public int getFromHour() {
		return fromHour;
	}
	public void setFromHour(int fromHour) {
		this.fromHour = fromHour;
	}
	public int getToHour() {
		return toHour;
	}
	public void setToHour(int toHour) {
		this.toHour = toHour;
	}
	public int getWeatherCode() {
		return weatherCode;
	}
	public void setWeatherCode(int weatherCode) {
		this.weatherCode = weatherCode;
	}
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	public double getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(double precipitation) {
		this.precipitation = precipitation;
	}
	public void setBeaufort(double beaufort) {
		this.beaufort = beaufort;
	}
	public double getBeaufort() {
		return beaufort;
	}
	
	public void setHumidity(float humidity){
		this.humidity = humidity;
	}
	public float getHumidity(){
		return humidity;
	}
	public void setDewPoint(double dewPoint){
		this.dewpoint = dewPoint;
	}
	public double getDewPoint(){
		return dewpoint;
	}
	public void setWindSpeed(double windSpeed) {
		this.windSpeed = windSpeed;
	}
	public double getWindSpeed(){
		return windSpeed;
	}
	public void setWindDirection(double windDirection) {
		this.windDirection = windDirection;
	}
	public double getWindDirection(){
		return windDirection;
	}
	
	
	
}
