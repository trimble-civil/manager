package no.mesta.mipss.klimadata.yr.export;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Weather {
	private SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
	private String from;
	private String to;
	private List<Forecast> forecasts = new ArrayList<Forecast>();
	private List<TextForecast> textForecasts = new ArrayList<TextForecast>();
	private String meteogram;
	
	public String getFrom() {
		return from;
	}
	public void setFrom(Date from) {
		this.from = f.format(from);
	}
	public String getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = f.format(to);
	}
	public List<Forecast> getForecasts() {
		return forecasts;
	}
	public void setForecasts(List<Forecast> forecasts) {
		this.forecasts = forecasts;
	}
	public List<TextForecast> getTextForecasts() {
		return textForecasts;
	}
	public void setTextForecasts(List<TextForecast> textForecasts) {
		this.textForecasts = textForecasts;
	}
	public void setMeteogram(String meteogram){
		this.meteogram = meteogram;
	}
	public String getMeteogram(){
		return meteogram;
	}
	
	
}
