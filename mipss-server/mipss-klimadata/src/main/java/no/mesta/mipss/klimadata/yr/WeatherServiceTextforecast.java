package no.mesta.mipss.klimadata.yr;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import no.mesta.mipss.klimadata.yr.vo.textforecast.LocationType;
import no.mesta.mipss.klimadata.yr.vo.textforecast.TimeType;
import no.mesta.mipss.klimadata.yr.vo.textforecast.Weather;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

class WeatherServiceTextforecast {

	public List<WeatherText> getTextForecast(WeatherService ws, String lat, String lon){
		DefaultHttpClient client =ws.getClient();
		try {
			String url = "http://api.yr.no/weatherapi/textlocation/1.0/?latitude="+lat+";longitude="+lon+";language=nb";
			HttpGet method = new HttpGet(url);
			HttpResponse response = client.execute((HttpUriRequest) method);
			
			InputStream rstream = response.getEntity().getContent();
			String s = ws.convertStreamToString(rstream);
			JAXBContext jc = JAXBContext.newInstance("no.mesta.mipss.klimadata.yr.vo.textforecast");
			Unmarshaller u = jc.createUnmarshaller();
			Weather w = (Weather)u.unmarshal(new StreamSource( new StringReader(s) ) );
			List<TimeType> time = w.getTime();
			List<WeatherText> textList = new ArrayList<WeatherText>();
			for (TimeType t:time){
				Date from = t.getFrom();
				Date to = t.getTo();
				
				List<LocationType> location = t.getLocation();
				for (LocationType l:location){
					String name = l.getName();
					String forecast = l.getForecast();
					WeatherText wt = new WeatherText();
					wt.setDateFrom(from);
					wt.setDateTo(to);
					wt.setForecast(forecast);
					wt.setLocation(name);
					textList.add(wt);
				}
			}
			return textList;
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException("JAXB failed", e);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new RuntimeException("Html failed", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("IOException", e);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RuntimeException("Illegal state", e);
		}catch (Throwable t){
			t.printStackTrace();
			throw new RuntimeException(t);
		}
	}
}
