package no.mesta.mipss.kjoretoy.component.stroing;

import no.mesta.mipss.persistence.stroing.Stroperiode;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GritPeriodUtilTest {

    private List<Stroperiode> existingPeriods;
    private Stroperiode newPeriod;

    @Before
    public void setUp() {
        newPeriod = new Stroperiode();

        Stroperiode sp1 = new Stroperiode();
        sp1.setFraDato(getDate(2016, 1, 1));
        sp1.setTilDato(getDate(2016, 1, 10));

        Stroperiode sp2 = new Stroperiode();
        sp2.setFraDato(getDate(2016, 1, 11));
        sp2.setTilDato(getDate(2016, 1, 20));

        Stroperiode sp3 = new Stroperiode();
        sp3.setFraDato(getDate(2016, 1, 21));
        sp3.setTilDato(null);

        existingPeriods = new ArrayList<>();
        existingPeriods.add(sp1);
        existingPeriods.add(sp2);
        existingPeriods.add(sp3);
    }

    @Test
    public void periodCollidesWithExistingPeriods_noCollision_expectsFalse() {
        newPeriod.setFraDato(getDate(2016, 1, 21));
        newPeriod.setTilDato(getDate(2016, 1, 30));

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, true);

        assertFalse(collision);
    }

    @Test
    public void periodCollidesWithExistingPeriods_allPeriods_expectsTrue() {
        newPeriod.setFraDato(getDate(2016, 1, 1));
        newPeriod.setTilDato(getDate(2016, 2, 1));

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, true);

        assertTrue(collision);
    }

    @Test
    public void periodCollidesWithExistingPeriods_singlePeriodFromDateInside_expectsFalse() {
        newPeriod.setFraDato(getDate(2016, 1, 26));
        newPeriod.setTilDato(getDate(2016, 2, 5));

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, true);

        assertFalse(collision);
    }

    @Test
    public void periodCollidesWithExistingPeriods_singlePeriodToDateInside_expectsTrue() {
        newPeriod.setFraDato(getDate(2015, 12, 25));
        newPeriod.setTilDato(getDate(2016, 1, 1));

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, true);

        assertTrue(collision);
    }

    @Test
    public void periodCollidesWithExistingPeriods_fromDateInsideOnePeriodeToDateInsideAnotherPeriod_expectsTrue() {
        newPeriod.setFraDato(getDate(2016, 1, 5));
        newPeriod.setTilDato(getDate(2016, 1, 15));

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, true);

        assertTrue(collision);
    }

    @Test
    public void periodCollidesWithExistingPeriods_toDateNull_expectsFalse() {
        newPeriod.setFraDato(getDate(2016, 2, 11));
        newPeriod.setTilDato(null);

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, true);

        assertFalse(collision);
    }

    @Test
    public void periodCollidesWithExistingPeriods_sameActivePeriod_expectsTrue() {
        newPeriod.setFraDato(getDate(2016, 1, 21));
        newPeriod.setTilDato(null);

        boolean collision = GritPeriodUtil.periodCollidesWithExistingPeriods(newPeriod, existingPeriods, false);

        assertTrue(collision);
    }

    @Test
    public void getActiveGritPeriod_periodWithNullToDate_expectsActiveGritPeriod() {
        Stroperiode sp3 = new Stroperiode();
        sp3.setFraDato(getDate(2016, 1, 21));
        sp3.setTilDato(null);
        existingPeriods.add(sp3);

        Stroperiode activePeriod = GritPeriodUtil.getActiveGritPeriod(existingPeriods);

        assertEquals(sp3, activePeriod);
    }

    @Test
    public void getActiveGritPeriod_periodWithToDateLatest_expectsActiveGritPeriod() {
        Stroperiode sp3 = new Stroperiode();
        sp3.setFraDato(getDate(2016, 1, 21));
        sp3.setTilDato(getDate(2016, 1, 30));
        existingPeriods.add(sp3);

        Stroperiode activePeriod = GritPeriodUtil.getActiveGritPeriod(existingPeriods);

        assertEquals(sp3, activePeriod);
    }

    private Date getDate(int year, int month, int dayOfMonth) {
        return new GregorianCalendar(year, month - 1, dayOfMonth).getTime();
    }

}