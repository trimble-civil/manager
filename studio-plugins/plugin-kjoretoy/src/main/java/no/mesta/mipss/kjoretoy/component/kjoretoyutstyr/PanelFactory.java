package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Container;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import no.mesta.mipss.kjoretoy.component.CacheHolder;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.component.installasjon.Dfu_StatusListPanel;
import no.mesta.mipss.kjoretoy.component.installasjon.KjoretoyDfuListViewPanel;
import no.mesta.mipss.kjoretoy.component.installasjon.KjoretoyInstallasjonPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyBildeViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyCompleteViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyDokViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyInfoViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyKontakttypeViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyLevKontraktViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyOrdreViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyeierViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyordreListView;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.search.SearchView;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

/**
*  Verktøysklasse fro opprettelse av paneler.
*/
public class PanelFactory {
	private static final CacheHolder cacheHolder = new CacheHolder(KjoretoyUtils.getKjoretoyBean());
	
    private PanelFactory() {}
    
    public static CacheHolder getCacheHolder() {
    	return cacheHolder;
    }
    
    public static Dfu_StatusListPanel createDfu_StatusListPanel(Dfuindivid entity) {
    	Dfu_StatusListPanel panel = new Dfu_StatusListPanel(entity, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle());
    	return panel;
    }
    
    public static KjoretoyDfuListViewPanel createKjoretoyDfuListViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, Container parent, CompositePanel compositePanelParent, MipssPlugin mipssPlugin) {
    	KjoretoyDfuListViewPanel panel = new KjoretoyDfuListViewPanel(controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), compositePanelParent, mipssPlugin);
    	return panel;
    }
    
    public static KjoretoyInstallasjonPanel createKjoretoyInstallasjonPanel(Installasjon installasjon, MipssPlugin mipssPlugin) {
    	KjoretoyInstallasjonPanel panel = new KjoretoyInstallasjonPanel(installasjon, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), mipssPlugin);
    	return panel;
    }
    
    public static KjoretoyBildeViewPanel createKjoretoyBildeViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyInfoViewPanel parent) {
    	KjoretoyBildeViewPanel panel = new KjoretoyBildeViewPanel(controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle());
    	return panel;
    }
    
    public static KjoretoyCompleteViewPanel createKjoretoyCompleteViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, boolean hasUpdateAccess, Window parentContainer, MipssPlugin mipssPlugin) {
    	KjoretoyCompleteViewPanel panel = new KjoretoyCompleteViewPanel(controller, kjoretoy, hasUpdateAccess, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), parentContainer, mipssPlugin);
    	return panel;
    }
    
    public static KjoretoyLevKontraktViewPanel createKjoretoyLevKontraktViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, JFrame parent, boolean begrensetTilgangEndring) {
    	return new KjoretoyLevKontraktViewPanel(controller, kjoretoy, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), parent, null, begrensetTilgangEndring);
    }
    
    public static KjoretoyOrdreViewPanel createKjoretoyOrdreViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, Container parent, MipssPlugin mipssPlugin) {
    	return new KjoretoyOrdreViewPanel(controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), mipssPlugin);
    }
    
    public static KjoretoyDokViewPanel createKjoretoyDokViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyInfoViewPanel parent) {
    	KjoretoyDokViewPanel panel = new KjoretoyDokViewPanel(controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle());
    	return panel;
    }
    
    public static KjoretoyeierViewPanel createKjoretoyeierViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyLevKontraktViewPanel parent, Container parentContainer) {
    	KjoretoyeierViewPanel panel = new KjoretoyeierViewPanel(controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), parentContainer);
    	return panel;
    }
    
    public static KjoretoyInfoViewPanel createKjoretoyInfoViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, boolean hasUpdateAccess, MipssPlugin parentPlugin, boolean iWizard) {
    	KjoretoyInfoViewPanel panel = new KjoretoyInfoViewPanel(controller, kjoretoy, hasUpdateAccess, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), parentPlugin, iWizard);
    	return panel;
    }
    
    public static KjoretoyKontakttypeViewPanel createKjoretoyKontakttypeViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, Container parentContainer, CompositePanel parent) {
    	KjoretoyKontakttypeViewPanel panel = new KjoretoyKontakttypeViewPanel(controller, kjoretoy, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), parentContainer, parent);
    	return panel;
    }
    
    public static KjoretoyViewPanel createKjoretoyViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyCompleteViewPanel parent, MipssPlugin parentPlugin) {
    	KjoretoyViewPanel panel = new KjoretoyViewPanel(controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), parentPlugin);
    	return panel;
    }
    
    public static KjoretoyUtstyrIoPanel createKjoretoyUtstyrIoPanel(KjoretoyUtstyr ku, MipssPlugin parentPlugin){
    	KjoretoyUtstyrIoPanel panel = new KjoretoyUtstyrIoPanel(ku, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), parentPlugin);
    	return panel;
    }
    
    public static KjoretoyUtstyrListPanel createKjoretoyUtstyrListPanel(InstrumentationController controller, Kjoretoy kjoretoy, Window parent, CompositePanel compositePanelParent, boolean enkelVisning, boolean visEndreKnapp, boolean begrensetTilgangEndring, MipssPlugin parentPlugin) {
    	KjoretoyUtstyrListPanel panel = new KjoretoyUtstyrListPanel(parentPlugin, controller, kjoretoy, parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), compositePanelParent, enkelVisning, visEndreKnapp, begrensetTilgangEndring);
    	return panel;
    }
    
    public static KjoretoyUtstyrPanel createKjoretoyUtstyrPanel(KjoretoyUtstyr ku, boolean enkelVisning, MipssPlugin parentPlugin) {
    	KjoretoyUtstyrPanel panel = new KjoretoyUtstyrPanel(ku, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), enkelVisning, parentPlugin);
    	return panel;
    }
    
    public static NewKjoretoyUtstyrIoPanel createNewKjoretoyUtstyrIoPanel(KjoretoyUtstyrIo kuio, boolean update) {
    	NewKjoretoyUtstyrIoPanel panel = new NewKjoretoyUtstyrIoPanel(kuio, update, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle());
    	return panel;
    }
    
    /**
     * Oppretter en ny {@code KjoretoyUtstyr} instans, kobler den til det leverte {@code Kjoretoy}
     * og oppretter en ny panel basert på den.
     * @param kjoretoy
     * @return en ny {@code KjoretoyUtstyr} instans.
     */
    public static NewKjoretoyUtstyrPanel createNewKjoretoyUtstyrPanel(MipssPlugin plugin, Kjoretoy kjoretoy, boolean enkelVisning) {
        // opprett et nytt stykke utstyr for det gjeldende kjøretøyet
        KjoretoyUtstyr ku = new KjoretoyUtstyr();
        ku.setKjoretoy(kjoretoy);
        
        // koble kjøretøy og utstyr sammen
        if(kjoretoy.getKjoretoyUtstyrList() == null) {
        	kjoretoy.setKjoretoyUtstyrList(new ArrayList<KjoretoyUtstyr>());
        }
//        kjoretoy.addKjoretoyUtstyr(ku);
        
        // opprett dialogvinduet
        NewKjoretoyUtstyrPanel panel = new NewKjoretoyUtstyrPanel(plugin, ku, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), enkelVisning);
        return panel;
    }
    
    public static SearchView createSearchView(List<KjoretoyListV> hits, InstrumentationController controller) {
    	SearchView panel = new SearchView(hits, controller, KjoretoyUtils.getBundle());
    	return panel;
    }
    
    public static KjoretoyordreListView createKjoretoyordreListView(Driftkontrakt contract) {
    	KjoretoyordreListView panel = new KjoretoyordreListView(contract);
    	//TODO: harmoniser initialisringen når KjoretoyordreListView arver fra AbstractMipssEntityPanel
    	return panel;
    }
}
