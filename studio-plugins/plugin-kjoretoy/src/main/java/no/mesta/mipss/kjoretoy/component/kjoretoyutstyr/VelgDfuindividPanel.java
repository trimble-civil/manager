package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.kjoretoy.DfuindividQueryFilter;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.SokPanel;
import no.mesta.mipss.util.KjoretoyUtils;

public class VelgDfuindividPanel extends AbstractRelasjonPanel<Dfuindivid> {
	private SokPanel<Dfuindivid> sokPanel;
	
	public VelgDfuindividPanel(MipssKjoretoy mipssKjoretoy) {
		sokPanel = new SokPanel<Dfuindivid>(Dfuindivid.class,
				  new DfuindividQueryFilter(),
				  mipssKjoretoy,
				  "getDfuindividList",
				  MipssKjoretoy.class,
				  false,
				  "serienummer",
				  "tlfnummer",
				  "modellNavn");
		
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void updateComplete() {
		setComplete(Boolean.valueOf(sokPanel.getSelectedEntity() != null));
	}
	
	private void initGui() {
		this.setLayout(new GridBagLayout());
		this.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));

	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(500, 500);
	}

	@Override
	public Dfuindivid getNyRelasjon() {
		return sokPanel.getSelectedEntity();
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getPropertyString("dfu.velgIndivid.dialogTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
