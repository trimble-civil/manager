package no.mesta.mipss.instrumentering.wizard;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.util.KjoretoyUtils;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;


/**
 * Viser panelene som utgjør de forskjellige trinn i wizard for instrumentering.
 * Er controller klassen for wizard.
 * 
 * @author jorge
 *
 */
@SuppressWarnings("serial")
public class WizardPanel extends JDialog {
	private MipssPlugin mipssPlugin;
	private Window parent;
	private ResourceBundle bundle = null;
	private boolean simpleWizard = false;
	private boolean visEndreUtstyrKnapp = false;
	
	private WizardLine wizardLine;
	private Kjoretoy kjoretoy;
	private Driftkontrakt initiellKontrakt;
	private Wizard wizard;
	private Kjoretoy initiellKjoretoy;
	private boolean begrensetTilgangEndring = false;
	
	public WizardPanel(JFrame parent, MipssPlugin mipssPlugin, boolean simpleWizard, Driftkontrakt initiellKontrakt, Kjoretoy initiellKjoretoy, boolean visEndreUtstyrKnapp, boolean begrensetTilgangEndring) {
		super(parent, true);
		this.mipssPlugin = mipssPlugin;
		this.parent = parent;
		this.simpleWizard = simpleWizard;
		this.initiellKontrakt = initiellKontrakt;
		this.initiellKjoretoy = initiellKjoretoy;
		this.visEndreUtstyrKnapp = visEndreUtstyrKnapp;
		this.begrensetTilgangEndring = begrensetTilgangEndring;
		
		bundle = Utils.getBundle();
		initPanel();
		initWizard();
	}
	
	public WizardPanel(JDialog parent, MipssPlugin mipssPlugin, boolean simpleWizard, Driftkontrakt initiellKontrakt, Kjoretoy initiellKjoretoy) {
		super(parent, true);
		this.mipssPlugin = mipssPlugin;
		this.parent = parent;
		this.simpleWizard = simpleWizard;
		this.initiellKontrakt = initiellKontrakt;
		this.initiellKjoretoy = initiellKjoretoy;
		bundle = Utils.getBundle();
		initPanel();
		initWizard();
	}
	
	/**
	 * 
	 * @return
	 */
	public Kjoretoy startWizard() {
		
		GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0);
		
	    //Map gatheredSettings = (Map) WizardDisplayer.showWizard(wiz);
		WizardDisplayer.installInContainer(this, gbc, wizard, null, null, new WizardResultReceiver(){
	
			@Override
			public void cancelled(Map arg0) {
				kjoretoy = null;
				dispose();
			}

			@Override
			public void finished(Object arg0) {
				kjoretoy = wizardLine.getKjoretoy();
				dispose();
			}			
		});
		
		setSize(new Dimension(800, 600));
		setLocationRelativeTo(null);
		setVisible(true);
		return kjoretoy;
	}
	
	private void initPanel() {
		Dimension d = new Dimension(800, 450);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		this.setMinimumSize(d);
	}
	
	private void initWizard() {
		UIManager.put("wizard.sidebar.image", ImageUtil.convertImageToBufferedImage(IconResources.WIZARD_ICON.getImage()));

		wizardLine = new WizardLine(mipssPlugin.getLoader().getLoggedOnUserSign());
		
		if(initiellKjoretoy != null) {
			wizardLine.setKjoretoy(initiellKjoretoy);
		} else {
			Kjoretoy tmpKjoretoy = new Kjoretoy();
			tmpKjoretoy.setSlettetFlagg(Long.valueOf(0));
			List<Selskap> list = KjoretoyUtils.getKjoretoyBean().getSelskapeList();
			if(list.size() >= 1) {
				tmpKjoretoy.addSelskap(list.get(0));
			}
			wizardLine.setKjoretoy(tmpKjoretoy);
		}

		if(initiellKontrakt != null) {
			wizardLine.setSelectedKontrakt(initiellKontrakt);
		}
		
		if(simpleWizard) {
			RegistrerKjoretoyIdentifikasjonPage registrerKjoretoyIdentifikasjonPage = new RegistrerKjoretoyIdentifikasjonPage(wizardLine, bundle.getString("wizard.kjoretoyIdentifikasjon"), bundle.getString("wizard.kjoretoyIdentifikasjon"), begrensetTilgangEndring);
			RegistrerUtstyrPage registrerUtstyrPage = new RegistrerUtstyrPage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerUtstyr.title"), bundle.getString("wizard.registrerUtstyr.title"), true, simpleWizard, visEndreUtstyrKnapp, begrensetTilgangEndring);
			RegistrerKontraktOgLeverandorPage registrerKontraktOgLeverandorPage = new RegistrerKontraktOgLeverandorPage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerLevKontrakt.title"), bundle.getString("wizard.registrerLevKontrakt.title"), true, simpleWizard, initiellKontrakt, begrensetTilgangEndring);
			RegistrerOrdrePage registrerOrdrePage = new RegistrerOrdrePage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerOrdre.title"), bundle.getString("wizard.registrerOrdre.title"), true, simpleWizard, begrensetTilgangEndring);
			LeggTilStroperioderPage leggTilStroperioderPage = new LeggTilStroperioderPage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerOrdre.title"), bundle.getString("wizard.leggTilStroperioder.title"), true, simpleWizard);
			LeggTilKlippeperioderPage leggTilKlippeperioderPage = new LeggTilKlippeperioderPage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerOrdre.title"), bundle.getString("wizard.leggTilKlippeperioder.title"), true, simpleWizard);
			DokBildeLoggPage dokBildeLoggPage = new DokBildeLoggPage(mipssPlugin, wizardLine, bundle.getString("wizard.dokBildeLogg.title"), bundle.getString("wizard.dokBildeLogg.title"), true, simpleWizard);
			RodetilknytningPage rodetilknytningPage = new RodetilknytningPage(mipssPlugin,wizardLine, parent,bundle.getString("wizard.rodeTilknytning.title"), bundle.getString("wizard.rodeTilknytning.title"), true, simpleWizard);

			WizardPage[] pages = {registrerKjoretoyIdentifikasjonPage, dokBildeLoggPage, registrerUtstyrPage, leggTilStroperioderPage, leggTilKlippeperioderPage, registrerKontraktOgLeverandorPage, registrerOrdrePage,rodetilknytningPage};
			wizard = WizardPage.createWizard(pages);
		} else {
			RegistrerKjoretoyIdentifikasjonPage registrerKjoretoyIdentifikasjonPage = new RegistrerKjoretoyIdentifikasjonPage(wizardLine, bundle.getString("wizard.kjoretoyIdentifikasjon"), bundle.getString("wizard.kjoretoyIdentifikasjon"), begrensetTilgangEndring);
			RegistrerUtstyrPage registrerUtstyrPage = new RegistrerUtstyrPage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerUtstyr.title"), bundle.getString("wizard.registrerUtstyr.title"), true, simpleWizard, visEndreUtstyrKnapp, begrensetTilgangEndring);
			RegistrerKontraktOgLeverandorPage registrerKontraktOgLeverandorPage = new RegistrerKontraktOgLeverandorPage(mipssPlugin, wizardLine, parent, bundle.getString("wizard.registrerLevKontrakt.title"), bundle.getString("wizard.registrerLevKontrakt.title"), true, simpleWizard, initiellKontrakt, begrensetTilgangEndring);
			RegistrerDFUPage registrerDFUPage = new RegistrerDFUPage(wizardLine, parent, bundle.getString("wizard.registrerDFU.title"), bundle.getString("wizard.registrerDFU.title"), true, mipssPlugin);

			WizardPage[] pages = {registrerKjoretoyIdentifikasjonPage, registrerDFUPage, registrerUtstyrPage, registrerKontraktOgLeverandorPage};
			wizard = WizardPage.createWizard(pages);
		}
		
		this.setLayout(new GridBagLayout());
	}
	
	public Wizard getWizard() {
		return wizard;
	}
}
