package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.utils.BildeFilter;
import no.mesta.mipss.kjoretoy.utils.FileUtils;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * panel for visning av bilder som tilhører et kjøretøy.
 * @author jorge
 */
public class KjoretoyBildeViewPanel extends AbstractKjoretoyPanel {
	private static final long serialVersionUID = 8162156470500892883L;
	private Logger logger = LoggerFactory.getLogger(KjoretoyBildeViewPanel.class);
    private List<Bilde> list ;
    private KjoretoyInfoViewPanel parent ;
    private MipssKjoretoy bean ;
    private JFileChooser bildeChooser ;
    private boolean loaded = false;
    private Bilde currentBilde ;
    // Variables declaration - do not modify
    private JLabel bildeNavnLabel;
    private JLabel bildePositionLabel;
    private JSlider bilderPostionSlider;
    private JPanel pnlMain;
    private JPanel pnlBilde;
    private JLabel kjoretoyBildeArea;
    private JLabel kjoretoyBildeTitleLabel;
    private JButton leggtilBilde;
    private JButton slettBilde;
    // End of variables declaration

    public KjoretoyBildeViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyInfoViewPanel parent, MipssKjoretoy bean, ResourceBundle bundle) {
        super(kjoretoy, controller);
        this.bean = bean;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        initActions();
        
        this.parent = parent;
        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
            this.repaint();
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	public void enableUpdating(boolean canUpdate) {
    }
    
    public void saveChanges() {
    }

    public boolean saveChangesAndContinue() {
        return true;
    }
    
    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
        
    protected void loadFromEntity() {
    	loaded = false;
        try {
            list = bean.getBildeListForKjoretoy(mipssEntity.getId());
            if (list == null) {
                list = new LinkedList<Bilde>();
            }
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, getBundle().getString("kjoretoyBilde.ingenListe"));
            list = new ArrayList<Bilde>();
        }

        loadSlider(1);
        
        if (list.size() > 0) {
            // vis det første bildet.
            Bilde first = list.get(0);
            showResizedBilde(first);
            updateLabels(list.size(), first, 1);
        }
        else {
            showResizedBilde(null);
            updateLabels(0, null, 0);
        }
        loaded = true;
    }
    
    private void initActions() {
    	bildeChooser = new JFileChooser();
    	bildeChooser.addChoosableFileFilter(new BildeFilter());
    	
    	leggtilBilde.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = bildeChooser.showOpenDialog(KjoretoyBildeViewPanel.this);

		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		            File file = bildeChooser.getSelectedFile();
		            
		            // fileChooser kan returnere null
		            if(file == null) {
		            	return;
		            }
		            logger.debug("Åpner: " + file.getName());

		            // les inn bildet og init det
		            Bilde bilde = FileUtils.loadBilde(file);
		            if(bilde == null) {
		            	KjoretoyUtils.showException(getBundle().getString("gen.kanIkkeLeseBilde"));
		            	return;
		            }
		            bilde.setGuid(UUID.randomUUID().toString());

		            // legg til kjøretøy for lagring
		            mipssEntity.addBilde(bilde);
		            parent.enableSaveButton(true);
		            
		            // legg til den ineterne listen for visning
		            list.add(bilde);

		            loadSlider(list.size() - 1);
		            showResizedBilde(bilde);
		            loadSlider(list.size());
		        } else {
		            logger.debug("Open command cancelled by user.");
		        }
			}
    		
    	});
    	
    	slettBilde.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Finner valgt bilde og fjerner dette fra kjøretøyet
				if(currentBilde != null) {
					mipssEntity.removeKjoretoyBilde(currentBilde);
					list.remove(currentBilde);
					parent.enableSaveButton(true);
					int currentPos = bilderPostionSlider.getValue();
					Bilde b = null;
					if(currentPos > 0 && currentPos <= list.size()) {
						b = list.get(currentPos - 1);
					} else if(list.size() > 0) {
						currentPos = list.size();
						b = list.get(currentPos - 1);
					}
					loadSlider(currentPos-1);
		            showResizedBilde(b);
		            loadSlider(currentPos);
				}
			}
    	});
    	
        bilderPostionSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
            	if(! loaded) {
            		return;
            	}
                JSlider source = (JSlider)e.getSource();
                if (! source.getValueIsAdjusting()) {
                	int pos = source.getValue();
                	
                	// ignorer hvis listen er tom
                	if(pos > 0) {
	                    if(list.size() > 0) {
	                    	showResizedBilde(list.get(pos - 1));
	                    	updateLabels(list.size(), list.get(pos - 1), pos);
	                    } else {
	                    	showResizedBilde(null);
	                    	updateLabels(list.size(), null, pos);
	                    }
                	}
                }
            }
        });
        
        kjoretoyBildeArea.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e){
	        	if (e.getClickCount() >= 2 && currentBilde != null){
	        		String extension = "." + FileUtils.getFileNameExtension(currentBilde.getFilnavn());
	        		File temp = FileUtils.saveTempFile(extension, currentBilde.getBildeLob());
	        		DesktopHelper.openFile(temp);
	            }
	            e.consume();
        	}
        	@Override
			public void mouseEntered(MouseEvent e) {
        		kjoretoyBildeArea.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				kjoretoyBildeArea.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
        });
    }
    
    private void loadSlider(int position) {
    	logger.debug("init slider med max " + list.size());
        bilderPostionSlider.setMinimum(1);
        bilderPostionSlider.setMaximum(list.size());
        bilderPostionSlider.setValue(position);
        bilderPostionSlider.setPaintTicks(false);
    }
    
    /**
     * Tilpasser størrelsen på bilde til den JLabel som inneholder det.
     * Henter den riktige størrelsen fra en JPanel med EtchedBorder som i sin
     * tur inneholder JPanel og trekker fire pixels i hver retning for å 
     * legge igjen plass til Border.
     * @param b Bilde som skal tilpasses istørrelsen og vises.
     */
    private void showResizedBilde(Bilde b) {
        if (b == null) {
            kjoretoyBildeArea.setIcon(null);
            return;
        }
        
        ImageIcon icon = new ImageIcon(b.getBildeLob());
        Image image = icon.getImage();
        Image resized = null;

        Dimension panelD = pnlBilde.getPreferredSize();

        int contWidth = panelD.width - 4;
        int contHeight = panelD.height - 4;
        int iWidth = image.getWidth(null);
        int iHeight = image.getHeight(null);
        
        if (iWidth > iHeight) {
            resized = image.getScaledInstance(contWidth, -1, Image.SCALE_SMOOTH);
        }
        else if (iHeight > iWidth){
            resized = image.getScaledInstance(-1, contHeight, Image.SCALE_SMOOTH);
        }
        else {
            resized = image;
        }
        // tving komplett resizing og lagre den nye Image i Icon
        icon.setImage(new ImageIcon(resized).getImage());
        
        kjoretoyBildeArea.removeAll();
        kjoretoyBildeArea.setIcon(icon);
        currentBilde = b;
    }
    
    /**
     * Oppdaterer de to Labels. En med bildets navn og den andre med bildets
     * posisjon i listen.
     * @param b <code>Bilde<(code> som det skal oppdateres for.
     * @param listSize. Listestørrelsen bestemmer initialiseringen.
     */
    private void updateLabels(int listSize, Bilde b, int position) {
        if (listSize == 0) {
            kjoretoyBildeArea.setText(getBundle().getString("kjoretoyBilde.IngenBilder"));
            bildeNavnLabel.setText(getBundle().getString("kjoretoyBilde.IngenBilder"));
            bildePositionLabel.setText("");
        }
        else {
            bildeNavnLabel.setText(b.getTextForGUI());
            kjoretoyBildeArea.setText("");
            try {
            	bildePositionLabel.setText(String.format(getBundle().getString("kjoretoyBilde.position"), position, listSize));
            }
            catch (Exception e) {
            	// i tilfelle noen har endret formatdefinisjonen i properties filen
            	bildePositionLabel.setText("");
			}
        }
    }
        
    protected void initComponentsPresentation() {
        initComponents();
        KjoretoyUtils.setBold(kjoretoyBildeTitleLabel);
        pnlBilde.setBackground(Color.white);
    }
    

    protected boolean isSaved() {
    	return true;
    }

    private void initComponents() {

        pnlMain = new JPanel();
        kjoretoyBildeTitleLabel = new JLabel();
        leggtilBilde = new JButton();
        slettBilde = new JButton();
        pnlBilde = new JPanel();
        kjoretoyBildeArea = new JLabel();
        bildeNavnLabel = new JLabel();
        bilderPostionSlider = new JSlider();
        bildePositionLabel = new JLabel();

        setName("Form"); // NOI18N

        pnlMain.setBorder(BorderFactory.createTitledBorder(getBundle().getString("kjoretoyBilde.Title")));
        pnlMain.setName("jPanel1"); // NOI18N

        kjoretoyBildeTitleLabel.setText(getBundle().getString("kjoretoyBilde.Title")); // NOI18N

        leggtilBilde.setText(getBundle().getString("kjoretoyBilde.LeggTilBildeButton")); // NOI18N
        slettBilde.setText(getBundle().getString("kjoretoyBilde.SlettBildeButton")); // NOI18N
        pnlBilde.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        pnlBilde.setName("jPanel2"); // NOI18N
        pnlBilde.setVerifyInputWhenFocusTarget(false);

        kjoretoyBildeArea.setHorizontalAlignment(SwingConstants.CENTER);
        kjoretoyBildeArea.setText(getBundle().getString("kjoretoyBilde.IngenBilder")); // NOI18N
        kjoretoyBildeArea.setBorder(BorderFactory.createEtchedBorder());
        kjoretoyBildeArea.setName("kjoretoyBildeArea"); // NOI18N
        kjoretoyBildeArea.setOpaque(true);

        bildeNavnLabel.setText(getBundle().getString("kjoretoyBilde.IngenBilder")); // NOI18N
        bildeNavnLabel.setName("bildeNavnLabel"); // NOI18N

        bilderPostionSlider.setName("bilderPostionSlider"); // NOI18N

        bildePositionLabel.setText(getBundle().getString("kjoretoyBilde.position")); // NOI18N
        bildePositionLabel.setName("bildePositionLabel"); // NOI18N
        
        /*GroupLayout jPanel2Layout = new GroupLayout(pnlBilde);
        pnlBilde.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(bildeNavnLabel, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)
                    .addComponent(kjoretoyBildeArea, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kjoretoyBildeArea, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bildeNavnLabel)
                .addContainerGap(12, Short.MAX_VALUE))
        );*/
        
        kjoretoyBildeArea.setMinimumSize(new Dimension(289, 230));
        kjoretoyBildeArea.setMaximumSize(new Dimension(289, 230));
        kjoretoyBildeArea.setPreferredSize(new Dimension(289, 230));
        
        pnlBilde.setLayout(new GridBagLayout());
        pnlBilde.add(kjoretoyBildeArea, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 5, 11), 0, 0));
        pnlBilde.add(bildeNavnLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 11, 11), 0, 0));
        
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(pnlBilde, new GridBagConstraints(0, 0, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(bilderPostionSlider, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(bildePositionLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(leggtilBilde, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnlMain.add(slettBilde, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

	@Override
	public Dimension getDialogSize() {
		return null;
	}




}
