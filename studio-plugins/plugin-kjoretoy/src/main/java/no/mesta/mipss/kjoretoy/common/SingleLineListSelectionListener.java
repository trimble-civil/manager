package no.mesta.mipss.kjoretoy.common;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Adapter for ListSelectionListener hendelsesbetjener. Implementerer boiler-plate kode
 * for enkelt bruk når man kan velge kun én linje i tabellen av gangen. 
 * @author jorge
 */
public abstract class SingleLineListSelectionListener implements ListSelectionListener {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Betjener hendelsen. Ignorerer uferdige hendelser og velget kun den første linjen
     * hvis flere ble valgt. Logger et varsel hvis flere enn én linje veges.
     * @param e
     */
    public void valueChanged(ListSelectionEvent e) {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (e.getValueIsAdjusting() || lsm.isSelectionEmpty()) {
            logger.trace("Ignorerer hendelse");
            return;
        }

        if (lsm.getMinSelectionIndex() != lsm.getMaxSelectionIndex()) {
            logger.warn("RowSelectionMode er ikke satt til SINGLE_SELECTION. Godtar kun første linje");
        }
        selectionServiceMethod(lsm.getMinSelectionIndex());
    }

    /**
     * Eksekverer betjenningslogikken for hendelsen.
     * @param index er indeksen i listen som tabellen holder
     */
    public abstract void selectionServiceMethod(int index);
}
