package no.mesta.mipss.kjoretoy.component.installasjon.graf;

import java.util.Date;

public class UtstyrGrafPeriode {
	private Date startDato;
	private Date sluttDato;
	private Boolean aktiv;
	private String utstyrNavn;
	
	public UtstyrGrafPeriode(Date startDato, Date sluttDato, Boolean aktiv, String utstyrNavn) {
		this.startDato = startDato;
		this.sluttDato = sluttDato;
		this.aktiv = aktiv;
		this.utstyrNavn = utstyrNavn;
	}

	public Date getStartDato() {
		return startDato;
	}

	public void setStartDato(Date startDato) {
		this.startDato = startDato;
	}

	public Date getSluttDato() {
		return sluttDato;
	}

	public void setSluttDato(Date sluttDato) {
		this.sluttDato = sluttDato;
	}

	public Boolean getAktiv() {
		return aktiv;
	}

	public void setAktiv(Boolean aktiv) {
		this.aktiv = aktiv;
	}

	public String getUtstyrNavn() {
		return utstyrNavn;
	}

	public void setUtstyrNavn(String utstyrNavn) {
		this.utstyrNavn = utstyrNavn;
	}
}
