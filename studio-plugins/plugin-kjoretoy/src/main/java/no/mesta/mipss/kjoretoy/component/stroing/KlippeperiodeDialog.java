package no.mesta.mipss.kjoretoy.component.stroing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.IkkeoverlappendePeriodeValidator;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.persistence.klipping.Klippeperiode;
import no.mesta.mipss.persistence.stroing.Stroperiode;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

public class KlippeperiodeDialog extends JDialog {
	private JPanel pnlMain = new JPanel();
	private JLabel lblFraDato = new JLabel();
	private JMipssDatePicker datePckrFraDato = new JMipssDatePicker();
	private JLabel lblTilDato = new JLabel();
	private JMipssDatePicker datePckrTilDato = new JMipssDatePicker();
	private JLabel lblKlippebreddeCm = new JLabel();
	private JFormattedTextField txtKlippebreddeCm = new JFormattedTextField();
	private JLabel lblAvgrens = new JLabel();
	private JCheckBox chkAvgrens = new JCheckBox();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	
	private Klippeperiode klippeperiode;
	private Klippeperiode oldKlippeperiode;
	
	private BindingGroup bindingGroup = new BindingGroup();
	private boolean nyPeriode;
	private List<Klippeperiode> eksisterendePerioder;
	private IkkeoverlappendePeriodeValidator periodeValidator = new IkkeoverlappendePeriodeValidator();
	
	private boolean klippeperiodeOk;
	
	public KlippeperiodeDialog(JFrame parentFrame, Klippeperiode klippeperiode, List<Klippeperiode> eksisterendePerioder, boolean nyPeriode) {
		super(parentFrame, KjoretoyUtils.getPropertyString("klipping.dialog"), true);
		this.klippeperiode = klippeperiode;
		this.eksisterendePerioder = eksisterendePerioder;
		this.nyPeriode = nyPeriode;
		
		init();
	}
	
	public KlippeperiodeDialog(JDialog parentDialog, Klippeperiode klippeperiode, List<Klippeperiode> eksisterendePerioder, boolean nyPeriode) {
		super(parentDialog, KjoretoyUtils.getPropertyString("klipping.dialog"), true);
		this.klippeperiode = klippeperiode;
		this.eksisterendePerioder = eksisterendePerioder;
		this.nyPeriode = nyPeriode;
		
		init();
	}
	
	public boolean isKlippeperiodeOk(){
		return klippeperiodeOk;
	}
	private void init() {
		bindingGroup.unbind();
		lagKlippeperiodeKopi();
		initLabels();
		initButtons();
		initTxtFields();
		initDatePickers();
		initGui();
		
		bindingGroup.bind();
	}
	
	private void lagKlippeperiodeKopi() {
		oldKlippeperiode = klippeperiode;
		klippeperiode = new Klippeperiode();
		klippeperiode.setKjoretoy(oldKlippeperiode.getKjoretoy());
		klippeperiode.setFraDato(oldKlippeperiode.getFraDato());
		klippeperiode.merge(oldKlippeperiode);
	}
	
	public Klippeperiode showDialog() {
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		bindingGroup.unbind();
		
		if (klippeperiode!=null){
			klippeperiode.setTilDato(CalendarUtil.round(klippeperiode.getTilDato(), false));
			klippeperiode.setFraDato(CalendarUtil.round(klippeperiode.getFraDato(), true));
		}
		
		return klippeperiode;
	}
	
	private void initLabels() {
		lblKlippebreddeCm.setText(KjoretoyUtils.getPropertyString("klipping.lblKlippebreddeCm"));
		lblFraDato.setText(KjoretoyUtils.getPropertyString("stroing.lblFraDato"));
		lblTilDato.setText(KjoretoyUtils.getPropertyString("stroing.lblTilDato"));
	}
	
	private void initButtons() {
		btnOk.setText(KjoretoyUtils.getPropertyString("button.ok"));
		btnAvbryt.setText(KjoretoyUtils.getPropertyString("button.avbrytt"));
		
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validateInput()) {
					if(!periodeValidator.sjekkDatoerIPeriode(klippeperiode)) {
						JOptionPane.showMessageDialog(KlippeperiodeDialog.this, KjoretoyUtils.getPropertyString("stroing.messageFeilDato"), KjoretoyUtils.getPropertyString("stroing.messageFeilDatoTitle"), JOptionPane.ERROR_MESSAGE);					
					} //else if(!periodeValidator.sjekkPeriodeMotEksisterendePerioder(true, eksisterendePerioder, stroperiode)) {
					else if (!sjekkOverlapp()){
						JOptionPane.showMessageDialog(KlippeperiodeDialog.this, KjoretoyUtils.getPropertyString("klipping.messageOverlapp"), KjoretoyUtils.getPropertyString("klipping.messageOverlappTitle"), JOptionPane.ERROR_MESSAGE);
					} else {
						klippeperiodeOk = true;
						KlippeperiodeDialog.this.dispose();
					}
				}
			}
		});
		
		btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				klippeperiode = null;
				KlippeperiodeDialog.this.dispose();
			}
		});
		
		bindingGroup.addBinding(BindingHelper.createbinding(klippeperiode, "${fraDato != null}", btnOk, "enabled", BindingHelper.READ));
	}
	
	private boolean sjekkOverlapp(){
		Date nyFraDato = CalendarUtil.round(klippeperiode.getFraDato(), true);
		Date nyTilDato = CalendarUtil.round(klippeperiode.getTilDato(), false);
		
		for(Klippeperiode eksPeriode : eksisterendePerioder) {
			if (eksPeriode.getKjoretoyId().equals(klippeperiode.getKjoretoyId())&&eksPeriode.getFraDato().equals(klippeperiode.getFraDato())){
				continue;
			}
			if (CalendarUtil.isOverlappende(nyFraDato, nyTilDato, eksPeriode.getFraDato(), eksPeriode.getTilDato(), false)){
				return false;
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private void initTxtFields() {
		txtKlippebreddeCm.setDocument(new DocumentFilter(3));
		Binding klippebreddeBind = BindingHelper.createbinding(klippeperiode, "klippebreddeCm", txtKlippebreddeCm, "text", BindingHelper.READ_WRITE);
		klippebreddeBind.setConverter(new LongStringConverter());
		bindingGroup.addBinding(klippebreddeBind);
		txtKlippebreddeCm.setHorizontalAlignment(JTextField.RIGHT);
	}
	
	private void initDatePickers() {
		bindingGroup.addBinding(BindingHelper.createbinding(klippeperiode, "fraDato", datePckrFraDato, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(klippeperiode, "tilDato", datePckrTilDato, "date", BindingHelper.READ_WRITE));
		
		if(!nyPeriode) {
			datePckrFraDato.setEditable(false);
		}
		
		bindingGroup.addBinding(BindingHelper.createbinding(klippeperiode, "${tilDato != null}", chkAvgrens, "selected", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(chkAvgrens, "selected", datePckrTilDato, "editable", BindingHelper.READ));
		
		lblAvgrens.setText(KjoretoyUtils.getPropertyString("stroing.lblAvgrens"));
		chkAvgrens.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!chkAvgrens.isSelected()) {
					klippeperiode.setTilDato(null);
				} else if(klippeperiode.getTilDato() == null) {
					klippeperiode.setTilDato(klippeperiode.getFraDato());
				}
			}
		});
	}
	
	private boolean validateInput() {
		if(klippeperiode.getKlippebreddeCm() == null) {
			JOptionPane.showMessageDialog(KlippeperiodeDialog.this, KjoretoyUtils.getPropertyString("klipping.messageManglerInfo"), KjoretoyUtils.getPropertyString("stroing.messageManglerInfoTitle"), JOptionPane.ERROR_MESSAGE);
			return false;
		} else {
			return true;
		}
	}
	
	private void initGui() {
		Dimension dim = new Dimension(100, 19);
		Dimension dimTxt = new Dimension(100, 19);
		txtKlippebreddeCm.setPreferredSize(dimTxt);
		txtKlippebreddeCm.setMinimumSize(dimTxt);
		datePckrFraDato.setPreferredSize(dimTxt);
		datePckrFraDato.setMinimumSize(dimTxt);
		datePckrTilDato.setPreferredSize(dimTxt);
		datePckrTilDato.setMinimumSize(dimTxt);
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblKlippebreddeCm, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(txtKlippebreddeCm, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblFraDato, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(datePckrFraDato, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblAvgrens, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(chkAvgrens, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblTilDato, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(datePckrTilDato, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}
}
