/**
 * 
 */
package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import no.mesta.mipss.util.KjoretoyUtils;

/**
 * Eget vindu for registrering av fritekst.
 * @author jorge
 *
 */
public class KjoretoyloggPanel extends JPanel {
	private ResourceBundle bundle = null;
	private String fritekst = null;
	private boolean valid = false;
	private JDialog dialog = null;
	
	public KjoretoyloggPanel() {
		bundle = KjoretoyUtils.getBundle();
		
		initComponents();
		initActions();
	}
	
	public String registerInOwnDialog() {
        dialog = new JDialog();

        dialog.setTitle(bundle.getString("kjoretoy.registerFritekst"));
        dialog.setModal(true);
        dialog.setLocationByPlatform(true);
        dialog.setResizable(true);
        dialog.getContentPane().add(this);
        
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        this.repaint();
        
		return(valid? fritekst : null);
	}
	
	private void initActions() {
		lagreButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fritekst = notatTextArea.getText();
				valid = true;
				dialog.dispose();
			}
		});
		avbrytButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fritekst = null;
				valid = false;
				dialog.dispose();
			}
			
		});
	}

    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        notatTextArea = new javax.swing.JTextArea();
        avbrytButton = new javax.swing.JButton();
        lagreButton = new javax.swing.JButton();

        setName("Form"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        notatTextArea.setColumns(20);
        notatTextArea.setRows(5);
        notatTextArea.setName("notatTextArea"); // NOI18N
        notatTextArea.setLineWrap(true);
        notatTextArea.setWrapStyleWord(true);
        jScrollPane1.setViewportView(notatTextArea);

        avbrytButton.setText(bundle.getString("button.avbrytt")); // NOI18N
        avbrytButton.setName("avbrytButton"); // NOI18N

        lagreButton.setText(bundle.getString("button.lagre")); // NOI18N
        lagreButton.setName("lagreButton"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lagreButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(avbrytButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(avbrytButton)
                    .addComponent(lagreButton))
                .addContainerGap())
        );
    }


    // Variables declaration - do not modify
    private javax.swing.JButton avbrytButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton lagreButton;
    private javax.swing.JTextArea notatTextArea;
    // End of variables declaration

}
