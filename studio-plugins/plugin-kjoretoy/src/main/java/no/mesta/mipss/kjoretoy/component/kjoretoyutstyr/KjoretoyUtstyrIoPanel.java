package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EditableMippsTableModel.MipssTableColumn;
import no.mesta.mipss.util.KjoretoyUtils;

/**
 * Viser informasjon om KjoretoyUtstyrIo sin portkonfigurasjon og Utstyrperioder
 * for det KjoretoyUtstyr som eier den. Til dette brukes det to tabeller ved siden
 * av hverandre.
 * @author jorge
 *
 */
public class KjoretoyUtstyrIoPanel extends AbstractMipssEntityPanel<KjoretoyUtstyr>  {
	private static final long serialVersionUID = 4037055685488486560L;
	@SuppressWarnings("unused")
	private boolean canUpdate = false;
	private MipssKjoretoy bean = null;
	
    private JTable aktivePerioderTable;
    private JLabel lblPortkonfigurering;
    private JPanel pnlMain;
    private JScrollPane scrlPaneKonfigureringTable;
    private JLabel konfigureringTitle;
    private JButton nyPortkonfigureringButton;
    private JButton btnEndrePortkonfig;
    private JTable portkonfigureringTable;
	private final MipssPlugin parentPlugin;
	
	public KjoretoyUtstyrIoPanel(KjoretoyUtstyr mipssEnt, MipssKjoretoy bean, ResourceBundle bundle, MipssPlugin parentPlugin) {
		super(mipssEnt);
		this.bean = bean;
		this.parentPlugin = parentPlugin;
		super.setBundle(bundle);
        
		try {
			initComponentsPresentation();
		}
		catch (Exception e) {
			KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyrio.ingenvisning"));
			return;
		}
		if (mipssEnt != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyrio.ingenvisning")); 
            }
        }
	}
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	@Override
	public void enableUpdating(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}


	@Override
	protected void initComponentsPresentation() {
		initComponents();
		
		KjoretoyUtils.setBold(konfigureringTitle);
        
		initListeners();
	}


	@Override
	protected boolean isSaved() {
		// peridoer og posrtkonfigureringer lagres internt i denne klassen
		return false;
	}

    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
    
	@Override
	protected void loadFromEntity() {
		initPortkonfigureringTable();
		if(mipssEntity == null) {
			nyPortkonfigureringButton.setEnabled(false);
			btnEndrePortkonfig.setEnabled(false);
		}
	}

	@Override
	public void saveChanges() {
		// peridoer og posrtkonfigureringer lagres internt i denne klassen
	}

	@Override
	public boolean saveChangesAndContinue() {
		// peridoer og posrtkonfigureringer lagres internt i denne klassen
		return false;
	}
    
    private void initPortkonfigureringTable() {
    	List<KjoretoyUtstyrIo> ios = null;
    	if(mipssEntity != null) {
    		try {
    			ios = bean.getKjoretoyUtstyrIoListFor(mipssEntity.getId());
    		}
    		catch (Exception e) {
    			KjoretoyUtils.showException(e, getBundle().getString("gen.listeIngenVisning"));
			}
    	}
    	
        // definer kolonner
        List<MipssTableColumn> columns = new ArrayList<MipssTableColumn>();
        columns.add(new MipssTableColumn(getBundle().getString("utstyr.aktivFra"), "fraDato", false));
        columns.add(new MipssTableColumn(getBundle().getString("utstyr.aktivTil"), "tilDato", false));
        columns.add(new MipssTableColumn(getBundle().getString("utstyr.portId"),   "bitNr", false));
        columns.add(new MipssTableColumn(getBundle().getString("utstyr.sensor"),   "sensortypeNavn", false));
        
        // init tabellen med kolonner og rader
        EditableMippsTableModel<KjoretoyUtstyrIo> model = new EditableMippsTableModel<KjoretoyUtstyrIo>(KjoretoyUtstyrIo.class, ios, columns);
        portkonfigureringTable.setModel(model);
                
        KjoretoyUtils.initDefaultTableSettings(portkonfigureringTable);
        
        // disable hvis tom
        portkonfigureringTable.setEnabled(!(ios == null || ios.size() == 0));
        
    }
    
    /**
     * bruker lyttere til bindingen, ikke BeansBinding.
     */
    private void initListeners() {
    	nyPortkonfigureringButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				KjoretoyUtstyrIo io = new KjoretoyUtstyrIo();
				io.getOwnedMipssEntity().setOpprettetAv(parentPlugin.getLoader().getLoggedOnUserSign());
				io.getOwnedMipssEntity().setOpprettetDato(Clock.now());
				io.setKjoretoyUtstyr(mipssEntity);
				updateKjoretoyUtstyrIo(io, false);
				
			}
    	});
    	
    	btnEndrePortkonfig.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				endrePortKonfig(portkonfigureringTable.getSelectedRow());
			}
    	});
    	portkonfigureringTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e){
                if (e.getClickCount() >= 2){
                    int index = ((JTable)e.getSource()).getSelectedRow();
                    endrePortKonfig(index);
                }
                e.consume();
            }
        });
    }
    
    @SuppressWarnings("unchecked")
	private void endrePortKonfig(int index) {
    	if(index >= 0 && index < portkonfigureringTable.getRowCount()) {
	        List<KjoretoyUtstyrIo>ios = ((EditableMippsTableModel<KjoretoyUtstyrIo>) portkonfigureringTable.getModel()).getEntities();
	        if(ios != null && ios.size() > 0 && index >= 0) {
	            KjoretoyUtstyrIo io = ios.get(index);
	            updateKjoretoyUtstyrIo(io, true);
	        }
    	}
    }
    
    /**
     * Åpner et dialogvindu for oppdatering av det leverte {@code KjoretoyUtstyrIo}.
     * @param io
     * @param editableFraDato TRUE for å åpne for redigering av fraDato-feltet.
     */
    @SuppressWarnings("unchecked")
	private void updateKjoretoyUtstyrIo(KjoretoyUtstyrIo io, boolean update) {
    	NewKjoretoyUtstyrIoPanel ioPanel = PanelFactory.createNewKjoretoyUtstyrIoPanel(io, update);

		io = ioPanel.newEntityDialogBox();
		EditableMippsTableModel<KjoretoyUtstyrIo> model = (EditableMippsTableModel<KjoretoyUtstyrIo>) portkonfigureringTable.getModel();
		try {
			List<KjoretoyUtstyrIo> kjoretoyUtstyrIoList = bean.getKjoretoyUtstyrIoListFor(mipssEntity.getId());
			model.setEntities(kjoretoyUtstyrIoList);
		}
		catch (Exception e) {
			KjoretoyUtils.showException(e, getBundle().getString("gen.ikkeLagret"));
		}
		portkonfigureringTable.repaint();
    }
    
	private void initComponents() {

        pnlMain = new JPanel();
        konfigureringTitle = new JLabel();
        aktivePerioderTable = new JTable();
        scrlPaneKonfigureringTable = new JScrollPane();
        portkonfigureringTable = new JTable();
        lblPortkonfigurering = new JLabel();
        nyPortkonfigureringButton = new JButton();
        btnEndrePortkonfig = new JButton();

        pnlMain.setBorder(BorderFactory.createEtchedBorder());
        konfigureringTitle.setText(getBundle().getString("utstyr.konfigurering.title"));
        
        scrlPaneKonfigureringTable.setViewportView(portkonfigureringTable);

        btnEndrePortkonfig.setText(getBundle().getString("utstyr.konfigurering.endre"));
        
        lblPortkonfigurering.setText(getBundle().getString("utstyr.konfigurering.portkonfigurering"));
        nyPortkonfigureringButton.setText(getBundle().getString("utstyr.konfigurering.nyPort"));
        
        pnlMain.setBorder(BorderFactory.createTitledBorder(getBundle().getString("utstyr.konfigurering.title")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(lblPortkonfigurering, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(btnEndrePortkonfig, new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(nyPortkonfigureringButton, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(scrlPaneKonfigureringTable, new GridBagConstraints(3, 1, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

	@Override
	protected Dimension getDialogSize() {
		return new Dimension(300, 300);
	}

}
