package no.mesta.mipss.kjoretoy.component.installasjon.ioliste;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;

public class IOListeTableObject implements IRenderableMipssEntity {
	private Ioliste ioliste;
	private KjoretoyUtstyr kjoretoyUtstyr;
	
	public IOListeTableObject(Ioliste ioliste) {
		this.ioliste = ioliste;
	}

    public Ioliste getIoliste() {
		return ioliste;
	}

	public Long getBitNr() {
        return ioliste.getBitNr();
    }
	
    public String getFritekst() {
        return ioliste.getFritekst();
    }
    
    public String getNavn1() {
        return ioliste.getNavn1();
    }
    
    public String getNavn2() {
        return ioliste.getNavn2();
    }
    
    public String getIoteknologiNavn() {
        return ioliste.getIoteknologiNavn();
    }
    
	public KjoretoyUtstyr getKjoretoyUtstyr() {
		return kjoretoyUtstyr;
	}

	public void setKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
		this.kjoretoyUtstyr = kjoretoyUtstyr;
	}

	public String getKjoretoyUtstyrNavn() {
		return kjoretoyUtstyr != null ? kjoretoyUtstyr.getTextForGUI() : "";
	}
	
	@Override
	public String getTextForGUI() {
		return null;
	}
	
	public boolean canHaveUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
		return this.kjoretoyUtstyr == null;
	}
}
