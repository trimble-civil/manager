package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;
import java.awt.Window;

import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.KjoretoyUtstyrListPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.plugin.MipssPlugin;

import org.netbeans.spi.wizard.WizardPage;

@SuppressWarnings("serial")
public class RegistrerUtstyrPage extends WizardPage  {
	private Window parent;
	private KjoretoyUtstyrListPanel kjoretoyUtstyrListPanel;
	
	private boolean enkelWizard;
	private WizardLine wizardLine;
	private boolean visEndreKnapp;
	private boolean begrensetTilgangEndring;
	private final MipssPlugin plugin;
	
	public RegistrerUtstyrPage(MipssPlugin plugin, WizardLine wizardLine, Window parent, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard, boolean visEndreKnapp, boolean begrensetTilgangEndring) {
		super(stepId, stepDescription, autoListen);
		this.plugin = plugin;
		this.parent = parent;
		this.wizardLine = wizardLine;
		this.enkelWizard = enkelWizard;
		this.visEndreKnapp = visEndreKnapp;
		this.begrensetTilgangEndring = begrensetTilgangEndring;
		
		initGui();
	}
	
	private void initGui() {
		this.setLayout(new BorderLayout());
	}
	
	@Override
	protected void renderingPage() {
		if(kjoretoyUtstyrListPanel == null) {
			kjoretoyUtstyrListPanel = PanelFactory.createKjoretoyUtstyrListPanel(wizardLine, wizardLine.getKjoretoy(), parent, null, enkelWizard, visEndreKnapp, begrensetTilgangEndring, plugin);
			this.add(kjoretoyUtstyrListPanel, BorderLayout.CENTER);
		}
	}
}
