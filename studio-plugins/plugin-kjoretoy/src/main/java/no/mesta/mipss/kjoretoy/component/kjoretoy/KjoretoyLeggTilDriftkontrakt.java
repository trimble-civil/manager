package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.kjoretoy.common.table.DriftkontraktTableObject;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

public class KjoretoyLeggTilDriftkontrakt extends AbstractRelasjonPanel<List<KontraktKjoretoy>> {
	private JPanel pnlDriftkontraktList = new JPanel();
	private JScrollPane scrlPaneDriftkontraktList = new JScrollPane();
	private JXTable tblDriftkontrakt = new JXTable();
	private MipssBeanTableModel<DriftkontraktTableObject> tblModel;
	private List<KontraktKjoretoy> oldKontraktKjoretoyList;
	private List<Driftkontrakt> alleDriftkontrakter;
	private Kjoretoy kjoretoy;
	
	public KjoretoyLeggTilDriftkontrakt(List<KontraktKjoretoy> oldKontraktKjoretoyList, List<Driftkontrakt> alleDriftkontrakter, Kjoretoy kjoretoy) {
		this.oldKontraktKjoretoyList = oldKontraktKjoretoyList;
		this.alleDriftkontrakter = alleDriftkontrakter;
		this.kjoretoy = kjoretoy;
		
		initTable();
		initGui();
	}
	
	private void initTable() {
		tblModel = new MipssBeanTableModel<DriftkontraktTableObject>(DriftkontraktTableObject.class, new int[] {0}, "valgt", "driftkontrakt");
		tblDriftkontrakt.setModel(tblModel);
		tblDriftkontrakt.setDefaultRenderer(Driftkontrakt.class, new RenderableMipssEntityTableCellRenderer<Driftkontrakt>());
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(DriftkontraktTableObject.class, tblModel.getColumns());
		tblDriftkontrakt.setColumnModel(colModel);
		
		List<DriftkontraktTableObject> modelList = new ArrayList<DriftkontraktTableObject>();
		for(Driftkontrakt driftkontrakt: alleDriftkontrakter) {
			DriftkontraktTableObject driftkontraktTableObject = new DriftkontraktTableObject(driftkontrakt);
			modelList.add(driftkontraktTableObject);
		}
		tblModel.setEntities(modelList);
		
		tblDriftkontrakt.getColumn(0).setMaxWidth(50);
		
		tblModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				updateComplete();
			}
    	});
	}
	
	private void initGui() {
		scrlPaneDriftkontraktList.setViewportView(tblDriftkontrakt);
		pnlDriftkontraktList.setLayout(new GridBagLayout());
		pnlDriftkontraktList.add(scrlPaneDriftkontraktList, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.setLayout(new GridBagLayout());
		this.add(pnlDriftkontraktList, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	private void updateComplete() {
		setComplete(getSelectedDriftkontraktList().size() > 0);
	}
	
	private List<Driftkontrakt> getSelectedDriftkontraktList() {
		List<DriftkontraktTableObject> kontraktList = tblModel.getEntities();
		List<Driftkontrakt> driftkontraktList = new ArrayList<Driftkontrakt>();
		
		for(DriftkontraktTableObject driftkontraktTableObject: kontraktList) {
			if(driftkontraktTableObject.getValgt()) {
				driftkontraktList.add(driftkontraktTableObject.getDriftkontrakt());
			}
		}
		
		return driftkontraktList;
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(400, 600);
	}

	@Override
	public List<KontraktKjoretoy> getNyRelasjon() {
		List<Driftkontrakt> driftkontraktList = getSelectedDriftkontraktList();
		List<KontraktKjoretoy> kontraktKjoretoyList = new ArrayList<KontraktKjoretoy>();
		
		HashSet<Long> oldDriftkontraktHashSet = new HashSet<Long>();
		for(KontraktKjoretoy kontraktKjoretoy : oldKontraktKjoretoyList) {
			oldDriftkontraktHashSet.add(kontraktKjoretoy.getKontraktId());
		}
		
		//Fjerner kontrakter som er duplikater
		for(Driftkontrakt driftkontrakt : driftkontraktList) {
			if(!oldDriftkontraktHashSet.contains(driftkontrakt.getId())) {
				KontraktKjoretoy kontraktKjoretoy = new KontraktKjoretoy();
				kontraktKjoretoy.setDriftkontrakt(driftkontrakt);
				kontraktKjoretoy.setKjoretoy(kjoretoy);
				kontraktKjoretoy.setAnsvarligFlagg((oldKontraktKjoretoyList.size() == 0 && kontraktKjoretoyList.size() == 0) ? Long.valueOf(1) : Long.valueOf(0));
				kontraktKjoretoyList.add(kontraktKjoretoy);
			}
		}
		
		return kontraktKjoretoyList;
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getBundle().getString("driftkontrakt.leggTilDialogTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
