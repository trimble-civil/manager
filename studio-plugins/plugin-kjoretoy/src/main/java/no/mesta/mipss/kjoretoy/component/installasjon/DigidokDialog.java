package no.mesta.mipss.kjoretoy.component.installasjon;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import no.mesta.mipss.persistence.Digidok;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.util.KjoretoyUtils;

@SuppressWarnings("serial")
public class DigidokDialog extends JDialog{

	private DigidokPanel pnlIgnorering;
	private JButton btnOk;
	private JButton btnCancel;
	private boolean isCancelled = true;
	private final List<Digidok> porter;
	private final String ignorering;
	
	public DigidokDialog(JDialog owner, List<Digidok> porter, String ignorering){
		super(owner, true);
		this.porter = porter;
		this.ignorering = ignorering;
		
		initComponents();
		initGui();
	}

	public boolean isCancelled(){
		return isCancelled;
	}
	private void initComponents() {
		pnlIgnorering = new DigidokPanel(porter, ignorering);
		
		btnOk = new JButton(getOkAction());
		btnCancel = new JButton(getCancelAction());
	}
	
	private Action getOkAction() {
		return new AbstractAction(KjoretoyUtils.getPropertyString("button.ok"), IconResources.OK_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				isCancelled=false;
				closeWindow();
			}
		};
	}
	
	private Action getCancelAction() {
		return new AbstractAction(KjoretoyUtils.getPropertyString("button.avbrytt"), IconResources.CANCEL_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				closeWindow();
			}
		};
	}
	
	private void closeWindow(){
		pnlIgnorering.dispose();
		this.dispose();
	}
	public String getIgnoreMaske(){
		return pnlIgnorering.getIgnorering();
	}
	private void initGui() {
		JPanel pnlButton = new JPanel(new GridBagLayout());
		pnlButton.add(btnOk, 	 new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,5,5),0,0));
		pnlButton.add(btnCancel, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,5,5),0,0));
		
		setLayout(new BorderLayout());
		add(pnlIgnorering, BorderLayout.CENTER);
		add(pnlButton, BorderLayout.SOUTH);
	}
}
