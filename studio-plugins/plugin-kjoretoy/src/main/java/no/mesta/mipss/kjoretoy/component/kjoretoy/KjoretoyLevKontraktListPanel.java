package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.instrumentering.wizard.WizardLine;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoylogg;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.KjoretoyVelgLeverandorPanel;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;

public class KjoretoyLevKontraktListPanel extends AbstractKjoretoyPanel implements CompositePanel {
	private MipssKjoretoy bean;
	private MaintenanceContract maintenanceContractBean;
	
	private MipssBeanTableModel<KontraktKjoretoy> model;
	
	private KjoretoyLevKontraktViewPanel parentPanel;
	
	private Container parent;
    private boolean loaded = false;
	private BindingGroup bindingGroup;
    
	private JPanel pnlMain = new JPanel();
	private JPanel pnlLeverandor = new JPanel();
	private JPanel pnlKontrakter = new JPanel();
	private JScrollPane scrlPaneKontrakter = new JScrollPane();
	private JMipssBeanTable<KontraktKjoretoy> tblKontrakter;
	private JLabel lblLeverandorNavn = new JLabel();
	private JTextField txtLeverandorNavn = new JTextField();
	private JLabel lblLeverandorInfo = new JLabel();
	private JButton btnVelgLeverandor = new JButton();
	private JButton btnLeggTilKontrakt = new JButton();
	private JButton btnSlettKontrakt = new JButton();
	private JButton btnSettAnsvarligKontrakt = new JButton();
	private boolean kanEndreAnsvarligKontrakt;
	
	public KjoretoyLevKontraktListPanel(InstrumentationController controller, KjoretoyLevKontraktViewPanel parentPanel, Kjoretoy kjoretoy, MipssKjoretoy bean, MaintenanceContract maintenanceContractBean, Container parent, boolean begrensetTilgangEndring, Driftkontrakt initiellDriftkontrakt) {
		super(kjoretoy, controller);

		this.parentPanel = parentPanel;
		this.bean = bean;
		this.maintenanceContractBean = maintenanceContractBean;
		this.parent = parent;

		bindingGroup = new BindingGroup();
		
		initTable();
		initButtons();
		initGui();
		initTextField();
		initLeverandorInfo();
		
		bindingGroup.bind();
		
		kanEndreAnsvarligKontrakt = true;
		if(begrensetTilgangEndring) {
			btnVelgLeverandor.setEnabled(true);
			if(initiellDriftkontrakt != null) {
				for(KontraktKjoretoy kontraktKjoretoy : model.getEntities()) {
					if(kontraktKjoretoy.getAnsvarligFlaggBoolean()) {
						if(!kontraktKjoretoy.getKontraktId().equals(initiellDriftkontrakt.getId())) {
							kanEndreAnsvarligKontrakt = false;
						}
					}
				}
			}
		}
	}
	
	private void initButtons() {
		btnVelgLeverandor.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.velg")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<LevStedView> leggTilLeverandorDialog;
				if(parent instanceof JFrame) {
					leggTilLeverandorDialog = new LeggTilRelasjonDialog<LevStedView>((JFrame)parent ,new KjoretoyVelgLeverandorPanel(maintenanceContractBean));
				} else {
					leggTilLeverandorDialog = new LeggTilRelasjonDialog<LevStedView>((JDialog)parent ,new KjoretoyVelgLeverandorPanel(maintenanceContractBean));
				}
				LevStedView levStedView = leggTilLeverandorDialog.showDialog();
				if(levStedView != null) {
					Leverandor leverandor = maintenanceContractBean.opprettLeverandor(levStedView.getLevNr());
					if(leverandor != null) {
						mipssEntity.setLeverandor(leverandor);
						enableSaveButton(true);
					}
				}
			}
		});
		
		btnLeggTilKontrakt.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.leggTil"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<List<KontraktKjoretoy>> leggTilRelasjonDialog;
				List<Driftkontrakt> alleDriftkontrakter = maintenanceContractBean.getDriftkontraktList();
				Collections.sort(alleDriftkontrakter);
				if(parent instanceof JFrame) {
					leggTilRelasjonDialog = new LeggTilRelasjonDialog<List<KontraktKjoretoy>>((JFrame)parent, new KjoretoyLeggTilDriftkontrakt(model.getEntities(), alleDriftkontrakter, mipssEntity));
				} else {
					leggTilRelasjonDialog = new LeggTilRelasjonDialog<List<KontraktKjoretoy>>((JDialog)parent, new KjoretoyLeggTilDriftkontrakt(model.getEntities(), alleDriftkontrakter, mipssEntity));
				}
				List<KontraktKjoretoy> kontraktKjoretoyList = leggTilRelasjonDialog.showDialog();
				if(kontraktKjoretoyList != null && kontraktKjoretoyList.size() > 0) {
					for(final KontraktKjoretoy kontraktKjoretoy : kontraktKjoretoyList) {
						mipssEntity.getKontraktKjoretoyList().add(kontraktKjoretoy);
						
						Driftkontrakt d = kontraktKjoretoy.getDriftkontrakt();
						Long ansvarlig = kontraktKjoretoy.getAnsvarligFlagg();
						Kjoretoylogg logg = new Kjoretoylogg();
						logg.setOpprettetAv(getInstrumentationController().getLoggedOnUser());
						logg.setOpprettetDato(Clock.now());
						logg.setTekst("Koblet til kontrakt:"+d.getKontraktnummer()+" "+d.getKontraktnavn()+" ansvarlig="+(ansvarlig==1?"Ja":"Nei"));
						mipssEntity.addKjoretoylogg(logg);
					}
					model.fireTableModelEvent(0, tblKontrakter.getRowCount()-1, TableModelEvent.UPDATE);
					enableSaveButton(true);
				}
			}
		});
		
		btnSettAnsvarligKontrakt.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.settSomAnsvarlig"), IconResources.OK2_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!kanEndreAnsvarligKontrakt) {
					JOptionPane.showMessageDialog(KjoretoyLevKontraktListPanel.this, KjoretoyUtils.getPropertyString("leverandor.kunAnsvarligKontraktKanBytteAnsvarligKontrakt"));
					return;
				}
				
				int selectedRow = tblKontrakter.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblKontrakter.getRowCount()) {
					KontraktKjoretoy valgtKontraktKjoretoy = model.get(tblKontrakter.convertRowIndexToModel(selectedRow));
					for(KontraktKjoretoy kontraktKjoretoy : model.getEntities()) {
						if(kontraktKjoretoy.equals(valgtKontraktKjoretoy)) {
							if(!kontraktKjoretoy.getAnsvarligFlaggBoolean()) {
								Driftkontrakt d = kontraktKjoretoy.getDriftkontrakt();
								Long ansvarlig = kontraktKjoretoy.getAnsvarligFlagg();
								Kjoretoylogg logg = new Kjoretoylogg();
								logg.setOpprettetAv(getInstrumentationController().getLoggedOnUser());
								logg.setOpprettetDato(Clock.now());
								logg.setTekst("Ansvarlig kontrakt satt til:"+d.getKontraktnummer()+" "+d.getKontraktnavn());
								mipssEntity.addKjoretoylogg(logg);
								kontraktKjoretoy.setAnsvarligFlagg(Long.valueOf(1));
							}
						} else {
							kontraktKjoretoy.setAnsvarligFlagg(Long.valueOf(0));
						}
					}
					enableSaveButton(true);
					model.fireTableModelEvent(0, tblKontrakter.getRowCount()-1, TableModelEvent.UPDATE);
				}
			}
		});
		
		btnSlettKontrakt.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.slett"), IconResources.REMOVE_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = tblKontrakter.getSelectedRows();
				List<KontraktKjoretoy> toBeremoved = new ArrayList<KontraktKjoretoy>();
				for(int i=0; i<selectedRows.length; i++) {
					if(selectedRows[i] >= 0 && selectedRows[i] < tblKontrakter.getRowCount()) {
						KontraktKjoretoy valgtKontraktKjoretoy = model.get(tblKontrakter.convertRowIndexToModel(selectedRows[i]));
						toBeremoved.add(valgtKontraktKjoretoy);
					}
				}
				if(toBeremoved.size() > 0) {
					for (KontraktKjoretoy k:toBeremoved){
						if (k.getAnsvarligFlaggBoolean()){
							List<Installasjon> inst = k.getKjoretoy().getInstallasjonList();
							if (inst.isEmpty()){
								doSlett(k);
								continue;
							}
							Date aktivTilDato = null;
							for (Installasjon i:inst){
								if ((aktivTilDato == null && i.getAktivTilDato() != null) || 
										(i.getAktivTilDato() != null && aktivTilDato != null && i.getAktivTilDato().after(aktivTilDato))){
									aktivTilDato = i.getAktivTilDato();
								} else{
									aktivTilDato = null;
								}
							}
							
							if (aktivTilDato==null||aktivTilDato.after(Clock.now())){
								String regnr = k.getKjoretoy().getRegnr();
								PropertyResourceBundleUtil bundle = PropertyResourceBundleUtil.getPropertyBundle("Instrumentation");
								String msg = bundle.getGuiString("message.kanIkkeFjerneKjoretoy", new Object[]{regnr});
								String title= Utils.getProperty("message.kanIkkeFjerneKjoretoy.title");
								JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.ERROR_MESSAGE);
							}else{
								doSlett(k);
							}
						} else {
							doSlett(k);
						}
					}
				}
			}

			private void doSlett(KontraktKjoretoy k) {
				Driftkontrakt d = k.getDriftkontrakt();
				Kjoretoylogg logg = new Kjoretoylogg();
				logg.setKjoretoy(mipssEntity);
				logg.setOpprettetAv(getInstrumentationController().getLoggedOnUser());
				logg.setOpprettetDato(Clock.now());
				logg.setTekst("Koblet fra kontrakt:"+d.getKontraktnummer()+" "+d.getKontraktnavn()+" ansvarlig="+(k.getAnsvarligFlagg()==1?"Ja":"Nei"));
				mipssEntity.addKjoretoylogg(logg);
				
				mipssEntity.getKontraktKjoretoyList().remove(k);
				model.fireTableModelEvent(0, tblKontrakter.getRowCount()-1, TableModelEvent.UPDATE);
				enableSaveButton(true);
			}
		});
		
		bindingGroup.addBinding(BindingHelper.createbinding(tblKontrakter, "${noOfSelectedRows > 0}", btnSlettKontrakt, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(tblKontrakter, "${noOfSelectedRows == 1}", btnSettAnsvarligKontrakt, "enabled"));
	}

	private void initTextField() {
		txtLeverandorNavn.setEditable(false);
		BindingHelper.createbinding(this, "${mipssEntity.leverandor != null ? mipssEntity.leverandor.navn : null}", txtLeverandorNavn, "text").bind();
	}
	
	private void initLeverandorInfo(){
		BindingHelper.createbinding(this, "${mipssEntity.leverandor != null ? mipssEntity.leverandor.valgbarFlagg : null}", this, "leverandorValgbar").bind();
	}
	public void setLeverandorValgbar(Boolean valgbar){
		if (valgbar==null){
			lblLeverandorInfo.setText("");
		}
		else if (!valgbar){
			lblLeverandorInfo.setForeground(Color.red);
			lblLeverandorInfo.setText(KjoretoyUtils.getPropertyString("leverandor.leverandorIkkeValgbar"));
		}else{
			lblLeverandorInfo.setText("");
		}
	}
	
	private void initTable() {
		model = new MipssBeanTableModel<KontraktKjoretoy>("kontraktKjoretoyList", KontraktKjoretoy.class, "driftkontrakt", "ansvarligFlaggBoolean");
		BindingHelper.createbinding(this, "mipssEntity", model, "sourceObject", BindingHelper.READ).bind();
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(KontraktKjoretoy.class, model.getColumns());
		
		tblKontrakter = new JMipssBeanTable<KontraktKjoretoy>(model, columnModel);
		tblKontrakter.setFillsViewportWidth(true);
		tblKontrakter.setDoWidthHacks(false);
		tblKontrakter.setDefaultRenderer(Driftkontrakt.class, new RenderableMipssEntityTableCellRenderer<Driftkontrakt>());
		tblKontrakter.getColumn(1).setMaxWidth(150);
		tblKontrakter.getColumn(1).setMinWidth(150);
	}
	
	private void initGui() {
		lblLeverandorNavn.setText(KjoretoyUtils.getPropertyString("leverandor.leverandorNavn"));
		txtLeverandorNavn.setMinimumSize(new Dimension(150, 21));
		txtLeverandorNavn.setPreferredSize(new Dimension(150, 21));
		
		scrlPaneKontrakter.setViewportView(tblKontrakter);
		
		pnlLeverandor.setLayout(new GridBagLayout());
		pnlLeverandor.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("leverandor.border")));
		pnlLeverandor.add(lblLeverandorNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeverandor.add(txtLeverandorNavn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeverandor.add(btnVelgLeverandor, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeverandor.add(lblLeverandorInfo, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlKontrakter.setLayout(new GridBagLayout());
		pnlKontrakter.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("kontrakter.border")));
		pnlKontrakter.add(scrlPaneKontrakter, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlKontrakter.add(btnLeggTilKontrakt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontrakter.add(btnSettAnsvarligKontrakt, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontrakter.add(btnSlettKontrakt, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(pnlLeverandor, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(pnlKontrakter, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	@Override
	public void enableUpdating(boolean canUpdate) {
	}

	@Override
	protected void initComponentsPresentation() {
		
	}

	@Override
	protected void loadFromEntity() {
		loaded = false;
		List<KontraktKjoretoy> kontraktList = bean.getKontraktKjoretoyList(mipssEntity.getId());
		mipssEntity.setKontraktKjoretoyList(kontraktList);
		model.setEntities(kontraktList);
		
		enableSaveButton(false);
        loaded = true;
	}

	@Override
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy)bean;
	}

	@Override
	public void enableSaveButton(boolean enable) {
		if(parentPanel != null) {
			parentPanel.enableSaveButton(enable);
		}
		
		if(enable) {
			kjoretoyChanged();
		}
	}
}
