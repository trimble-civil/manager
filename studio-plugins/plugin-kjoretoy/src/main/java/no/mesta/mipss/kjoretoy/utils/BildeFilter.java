/**
 * 
 */
package no.mesta.mipss.kjoretoy.utils;

import java.io.File;
import javax.swing.filechooser.FileFilter;


/**
 * Filtrering som tillater kun bildefiler og kataloger
 * @author jorge
 */
public class BildeFilter extends FileFilter {
    private final static String[] types = {
    	"jpeg", "jpg", "gif", "tif", "tiff", "png"
    };
    
    /**
     * Godkjenn alle kataloger og alle bilder
     * @see java.io.FileFilter#accept(java.io.File)
     */
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = FileUtils.getFileNameExtension(f.getName());
        if (extension != null) {
        	for (String t : types) {
        		if(extension.equals(t)) {
        			return true;
        		}
        	}
        }
        return false;
    }

    /**
     * @return en kort beskrivelse
     */
    public String getDescription() {
        return "Kun bilder";
    }

}
