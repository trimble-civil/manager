package no.mesta.mipss.kjoretoy.component.rodetilknytning;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.instrumentering.wizard.WizardLine;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetilknytningstype;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.ui.JDateTimePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.KjoretoyUtils;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class RodetilknytningDialog extends JDialog{

    private JPanel pnlMain = new JPanel();
    private JLabel lblRode = new JLabel();
    private JComboBox<Rode> cbxRode = new JComboBox<>();
    private JLabel lblTilknytningspunkt = new JLabel();
    private JComboBox<Rodetilknytningstype> cbxTilknytningspunkt = new JComboBox<>();
    private JLabel lblFraDato = new JLabel();
    private JDateTimePicker datePckrFraDato = new JDateTimePicker();
    private JLabel lblTilDato = new JLabel();
    private JDateTimePicker datePckrTilDato = new JDateTimePicker();
    private JPanel pnlButtons = new JPanel();
    private JButton btnOk = new JButton();
    private JButton btnAvbryt = new JButton();

    private WizardLine wizardLine;
    private MipssPlugin plugin;

    private KjoretoyRode kjoretoyRode;
    private KjoretoyRode oldKjoretoyRode;

    private int indexRode = 0;

    private List<KjoretoyRode> eksisterendeRoder;
    private boolean nyRodetilknytning;
    private BindingGroup bindingGroup = new BindingGroup();
    private MipssComboBoxModel<Rode> cbxRodeModel;
    private MipssComboBoxModel<Rodetilknytningstype> cbxRodetilknytningsTypeModel;


    private boolean kjoretoyRodeOk;

    public RodetilknytningDialog(JFrame parentFrame, KjoretoyRode kjoretoyRode, List<KjoretoyRode> eksisterendeRoder, boolean nyRodetilknytning, WizardLine wizardLine, MipssPlugin plugin) {
        super(parentFrame, KjoretoyUtils.getPropertyString("rode.dialog"), true);
        this.kjoretoyRode = kjoretoyRode;
        this.eksisterendeRoder = eksisterendeRoder;
        this.nyRodetilknytning = nyRodetilknytning;
        this.wizardLine = wizardLine;
        this.plugin = plugin;
        init();
    }

    public RodetilknytningDialog(JDialog parentDialog, KjoretoyRode kjoretoyRode, List<KjoretoyRode> eksisterendeRoder, boolean nyRodetilknytning,WizardLine wizardLine, MipssPlugin plugin) {
        super(parentDialog, KjoretoyUtils.getPropertyString("rode.dialog"), true);
        this.kjoretoyRode = kjoretoyRode;
        this.eksisterendeRoder = eksisterendeRoder;
        this.nyRodetilknytning = nyRodetilknytning;
        this.wizardLine = wizardLine;
        this.plugin = plugin;
        init();
    }

    private void init() {
        bindingGroup.unbind();
        initRodeTilknytningKopi();
        initLabels();
        initCombobox();
        initDatePickers();
        initButtons();
        initGui();
        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyRode, "kontrakt", wizardLine.getSelectedKontrakt().getId(), "text", BindingHelper.READ_WRITE));

        bindingGroup.bind();

        if(!nyRodetilknytning) {
            datePckrFraDato.setEnabled(false);
            datePckrFraDato.setDate(kjoretoyRode.getFraTidspunkt());
            datePckrFraDato.setEditable(false);
        }
    }

    public KjoretoyRode showDialog() {
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
        bindingGroup.unbind();

        if (kjoretoyRode!=null){
            kjoretoyRode.setTilTidspunkt(datePckrTilDato.getDate());
            kjoretoyRode.setFraTidspunkt(datePckrFraDato.getDate());
        }

        return kjoretoyRode;
    }

    private void initCombobox() {
        if (nyRodetilknytning) {
            Rodegenerator rodeBean = BeanUtil.lookup("RodegeneratorBean", Rodegenerator.class);
            List<Rode> roderList = rodeBean.getRodeForKontrakt(wizardLine.getSelectedKontrakt().getId(), 15L, false, true);
            cbxRodeModel = new MipssComboBoxModel<>(roderList);

            for (int i = 0; i < roderList.size(); i++) {
                if (roderList.get(i).getId().equals(kjoretoyRode.getRodeId())) {
                    this.indexRode = i;
                    break;
                }
            }
        } else {
            cbxRodeModel = new MipssComboBoxModel<>(Collections.singletonList(kjoretoyRode.getRode()));
        }

        cbxRode.setModel(cbxRodeModel);
        cbxRode.setRenderer(new MipssListCellRenderer<Rode>());

        List<Rodetilknytningstype> rodetilknytningstypeList = KjoretoyUtils.getKjoretoyBean().getRodetilknytningstypeValgbareList();
        cbxRodetilknytningsTypeModel = new MipssComboBoxModel<>(rodetilknytningstypeList);

        cbxTilknytningspunkt.setModel(cbxRodetilknytningsTypeModel);
        cbxTilknytningspunkt.setRenderer(new MipssListCellRenderer<Rodetilknytningstype>());

        if (!nyRodetilknytning) {
            cbxRode.setEditable(false);
            cbxRode.setEnabled(false);
            cbxRode.setSelectedIndex(this.indexRode);
            cbxTilknytningspunkt.setEditable(false);
            cbxTilknytningspunkt.setEnabled(false);
        }

        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyRode, "rode", cbxRodeModel, "selectedItem", BindingHelper.READ_WRITE));
        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyRode, "rodetilknytningstype", cbxRodetilknytningsTypeModel, "selectedItem", BindingHelper.READ_WRITE));
    }

    private void initRodeTilknytningKopi() {
        oldKjoretoyRode = kjoretoyRode;
        kjoretoyRode = new KjoretoyRode();
        kjoretoyRode.setKjoretoy(oldKjoretoyRode.getKjoretoy());
        kjoretoyRode.setFraTidspunkt(oldKjoretoyRode.getFraTidspunkt());
        kjoretoyRode.setRodeId(oldKjoretoyRode.getRodeId());
        kjoretoyRode.merge(oldKjoretoyRode);
    }

    private void initDatePickers() {
        if (nyRodetilknytning) {
            datePckrTilDato = new JDateTimePicker();
            datePckrTilDato.setDate(Clock.now());
            datePckrFraDato.setDate(null);
        }

        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyRode, "fraTidspunkt", datePckrFraDato, "date", BindingHelper.READ_WRITE));
        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyRode, "tilTidspunkt", datePckrTilDato, "date", BindingHelper.READ_WRITE));
    }

    private void initButtons() {
        btnOk.setText(KjoretoyUtils.getPropertyString("button.ok"));
        btnAvbryt.setText(KjoretoyUtils.getPropertyString("button.avbrytt"));

        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(nyRodetilknytning) {
                    if(validateInput()) {
                        if(kjoretoyRode.getTilTidspunkt() != null) {
                            if(kjoretoyRode.getFraTidspunkt().after(kjoretoyRode.getTilTidspunkt())) {
                                JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageDate"), KjoretoyUtils.getPropertyString("rode.messageDateTitle"), JOptionPane.ERROR_MESSAGE);
                            }
                            else if(!sjekkOverlapp()) {
                                JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageOverlapp"), KjoretoyUtils.getPropertyString("rode.messageOverlappTitle"), JOptionPane.ERROR_MESSAGE);
                            }
                            else if(kjoretoyRode.getTilTidspunkt().after(plugin.getLoader().getSelectedDriftkontrakt().getGyldigTilDato())) {
                                JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageUgyldigPeriode"), KjoretoyUtils.getPropertyString("rode.messageUgyldigPeriodeTitle"), JOptionPane.ERROR_MESSAGE);
                            }
                            else {
                                kjoretoyRodeOk = true;
                                RodetilknytningDialog.this.dispose();
                            }
                        }
                        else if(!sjekkOverlapp()) {
                            JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageOverlapp"), KjoretoyUtils.getPropertyString("rode.messageOverlappTitle"), JOptionPane.ERROR_MESSAGE);
                        }
                        else {
                            kjoretoyRodeOk = true;
                            RodetilknytningDialog.this.dispose();
                        }
                    }
                    else{
                        JOptionPane.showMessageDialog(RodetilknytningDialog.this,KjoretoyUtils.getPropertyString("rode.messageManglerInfo"),KjoretoyUtils.getPropertyString("rode.messageManglerInfoTitle"),JOptionPane.ERROR_MESSAGE);
                    }
                }
                else {
                    if(kjoretoyRode.getTilTidspunkt() != null) {
                        if(kjoretoyRode.getFraTidspunkt().after(kjoretoyRode.getTilTidspunkt())) {
                            JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageDate"), KjoretoyUtils.getPropertyString("rode.messageDateTitle"), JOptionPane.ERROR_MESSAGE);
                        }
                        else if(kjoretoyRode.getTilTidspunkt().after(plugin.getLoader().getSelectedDriftkontrakt().getGyldigTilDato())) {
                            JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageUgyldigPeriode"), KjoretoyUtils.getPropertyString("rode.messageUgyldigPeriodeTitle"), JOptionPane.ERROR_MESSAGE);
                        }
                        else if(!sjekkOverlappEdit()) {
                            JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.messageOverlapp"), KjoretoyUtils.getPropertyString("rode.messageOverlappTitle"), JOptionPane.ERROR_MESSAGE);
                        }
                        else if(!sjekkTilDato()) {
                            JOptionPane.showMessageDialog(RodetilknytningDialog.this, KjoretoyUtils.getPropertyString("rode.tilDatoOverlapp"), KjoretoyUtils.getPropertyString("rode.tilDatoOverlappTitle"), JOptionPane.ERROR_MESSAGE);
                        }
                        else {
                            kjoretoyRodeOk = true;
                            RodetilknytningDialog.this.dispose();
                        }
                    }
                    else {
                        kjoretoyRodeOk = true;
                        RodetilknytningDialog.this.dispose();
                    }
                }
            }
        });

        btnAvbryt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kjoretoyRode = null;
                RodetilknytningDialog.this.dispose();
            }
        });

        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyRode, "${kjoretoyRode != null}", btnOk, "enabled", BindingHelper.READ));

    }

    private boolean validateInput() {
        if(kjoretoyRode.getRode() == null || kjoretoyRode.getRodetilknytningstype() == null) {
            return false;
        }
        else {
            return true;
        }
    }

    private boolean sjekkTilDato() {
        Date tilTidsPunkt = datePckrTilDato.getDate();

        if(tilTidsPunkt != null && (tilTidsPunkt.before(Clock.now()))) {
            return false;
        }
        return true;
    }

    private boolean sjekkOverlappEdit() {
        Date tilTidspunkt = datePckrTilDato.getDate();

        for(KjoretoyRode eksRode: eksisterendeRoder) {
            long rode1 = eksRode.getRodeId();
            long rode2 = kjoretoyRode.getRodeId();

            long type1 = eksRode.getRodetilknytningstypeId();
            long type2 = kjoretoyRode.getRodetilknytningstypeId();

            if(rode1==rode2 && type1==type2) {
                if(eksRode.getFraTidspunkt().equals(kjoretoyRode.getFraTidspunkt())) {
                    // Do not check overlap with itself
                    continue;
                }

                if ((eksRode.getTilTidspunkt()==null || eksRode.getTilTidspunkt().after(kjoretoyRode.getFraTidspunkt()))
                        && tilTidspunkt.after(eksRode.getFraTidspunkt())) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean sjekkOverlapp() {
        Date fraTidspunkt = datePckrFraDato.getDate();
        Date tilTidspunkt = datePckrTilDato.getDate();

        for(KjoretoyRode eksRode: eksisterendeRoder) {
            long rode1 = eksRode.getRodeId();
            long rode2 = kjoretoyRode.getRodeId();

            long type1 = eksRode.getRodetilknytningstypeId();
            long type2 = kjoretoyRode.getRodetilknytningstypeId();
            if(rode1==rode2 && type1==type2) {

                if (CalendarUtil.isOverlappende(fraTidspunkt, tilTidspunkt, eksRode.getFraTidspunkt(), eksRode.getTilTidspunkt(), false)) {
                    return false;
                }
            }
        }

        return true;
    }

    private void initLabels() {
        lblRode.setText(KjoretoyUtils.getPropertyString("rode.lblRode"));
        lblTilknytningspunkt.setText(KjoretoyUtils.getPropertyString("rode.lblTilknytningstype"));
        lblFraDato.setText(KjoretoyUtils.getPropertyString("rode.lblFraDato"));
        lblTilDato.setText(KjoretoyUtils.getPropertyString("rode.lblTilDato"));
    }

    private void initGui() {

        Dimension dim = new Dimension(170, 23);
        cbxRode.setPreferredSize(dim);
        cbxRode.setMinimumSize(dim);
        cbxTilknytningspunkt.setPreferredSize(dim);
        cbxTilknytningspunkt.setMinimumSize(dim);
        lblFraDato.setPreferredSize(dim);
        lblFraDato.setMinimumSize(dim);
        lblTilDato.setPreferredSize(dim);
        lblTilDato.setMinimumSize(dim);
        datePckrFraDato.setPreferredSize(dim);
        datePckrFraDato.setMinimumSize(dim);
        datePckrTilDato.setPreferredSize(dim);
        datePckrTilDato.setMinimumSize(dim);

        pnlButtons.setLayout(new GridBagLayout());
        pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(lblRode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(cbxRode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 0), 0, 0));

        pnlMain.add(lblTilknytningspunkt, new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 0), 0, 0));
        pnlMain.add(cbxTilknytningspunkt, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 0), 0, 0));

        pnlMain.add(lblFraDato, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(datePckrFraDato, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlMain.add(lblTilDato, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 5), 0, 0));
        pnlMain.add(datePckrTilDato, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));


        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));

    }

    public boolean isKjoretoyRodeOk(){
        return kjoretoyRodeOk;
    }

}
