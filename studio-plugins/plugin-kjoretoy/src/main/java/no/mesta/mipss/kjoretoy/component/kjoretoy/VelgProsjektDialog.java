package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.driftkontrakt.bean.ProsjektQueryFilter;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.SokPanel;
import no.mesta.mipss.util.KjoretoyUtils;

public class VelgProsjektDialog extends AbstractRelasjonPanel<ProsjektView> {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
	
	private SokPanel<ProsjektView> sokPanel;
	
	public VelgProsjektDialog(MaintenanceContract maintenanceContract) {
		sokPanel = new SokPanel<ProsjektView>(ProsjektView.class, 
				new ProsjektQueryFilter(), 
				maintenanceContract, 
				"getProsjektViewList", 
				MaintenanceContract.class, 
				false,
				"prosjektnr", 
				"prosjektnavn", 
				"ansvar");
		initGui();
			
		sokPanel.addListSelectionListener(new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			updateComplete();
			}
		});
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlMain.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(400,500);
	}

	@Override
	public ProsjektView getNyRelasjon() {
		return sokPanel.getSelectedEntity();
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getPropertyString("kjoretoyordre.velgProsjektDialogTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
	
	private void updateComplete() {
		setComplete(Boolean.valueOf(sokPanel.getSelectedEntity() != null));
	}
}
