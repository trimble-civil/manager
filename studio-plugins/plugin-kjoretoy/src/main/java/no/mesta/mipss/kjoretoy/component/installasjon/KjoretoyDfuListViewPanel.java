package no.mesta.mipss.kjoretoy.component.installasjon;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.instrumentering.wizard.WizardLine;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyCompleteViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.tablebrowser.BrowserUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInstallasjonV;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.table.DateTableCellRenderer;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;

/**
 * Panel for visning av listen med alle DFU som var/er i bruk på et kjøretøy. 
 * @author jorge
 *
 */
public class KjoretoyDfuListViewPanel extends AbstractKjoretoyPanel {
	private static final long serialVersionUID = 1L;
	private List<DfuInstallasjonV> dfus = null;
    private Container parent = null;
    private MipssKjoretoy bean = null;
    
    private JXTable dfuTable;
    private JLabel dfuTitle;
    private JPanel pnlMain;
    private JScrollPane scrlPaneDfuTable;
    private JButton btnNyInstallasjon;
    private JButton btnEndreInstallasjon;
    private JButton btnSlettInstallasjon;
    private CompositePanel compositePanelParent;
    private MipssPlugin mipssPlugin;
    
    public KjoretoyDfuListViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, Container parent, MipssKjoretoy bean, ResourceBundle bundle, CompositePanel compositePanelParent, MipssPlugin mipssPlugin) {
        super(kjoretoy, controller);
        this.bean = bean;
        this.compositePanelParent = compositePanelParent;
        this.mipssPlugin = mipssPlugin;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        this.parent = parent;

        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    public void enableUpdating(boolean canUpdate) {
        btnNyInstallasjon.setEnabled(canUpdate);
        btnEndreInstallasjon.setEnabled(canUpdate);
        btnSlettInstallasjon.setEnabled(canUpdate);
        //TODO: slås av kun hvis dialogboxen brukes til å redigere.
        dfuTable.setEnabled(canUpdate);
    }
    
    protected void loadFromEntity() {
        // hent DFUer fra DB
        try {
            dfus = bean.getDfuInstallasjonVList(mipssEntity.getId());
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, getBundle().getString("dfuList.ingenListe")); 
            dfus = new ArrayList<DfuInstallasjonV>();
        }
        loadTable(dfus);
    }
    
    @SuppressWarnings("unchecked")
	private void loadTable(List<DfuInstallasjonV> list) {
        ((EditableMippsTableModel<DfuInstallasjonV>)dfuTable.getModel()).setEntities(dfus);
        this.repaint();
    }
    
    /**
     * Setter opp listeners til alle aktive komponenter.
     */
    private void initListeners() {
        dfuTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					int index = dfuTable.rowAtPoint(e.getPoint());
					endreInstallasjon(index);
				}
			}
        });

        btnNyInstallasjon.addActionListener(new ActionListener() {
                    /**
                     * Åpner et tomt dialogvindu for registrering av ny installasjon.
                     */
                    @SuppressWarnings("unchecked")
					public void actionPerformed(ActionEvent e) {
                    	Installasjon installasjon = new Installasjon();
                    	installasjon.setOpprettetAv(mipssPlugin.getLoader().getLoggedOnUserSign());
                    	installasjon.setOpprettetDato(Clock.now());
                    	
                    	installasjon.setKjoretoy(mipssEntity);
                    	
                        KjoretoyInstallasjonPanel inst = PanelFactory.createKjoretoyInstallasjonPanel(installasjon, mipssPlugin);
                        inst.setDialogBoxTitle(getBundle().getString("dfu.installasjon.nyRegistrering"));
                        installasjon = inst.newEntityDialogBox();
                        mipssEntity.getInstallasjonList().add(installasjon);
                        
                        if (getInstrumentationController() instanceof WizardLine){
                        	//NAZTY hack for å vite om vi er i wizard mode..
                        	dfus.add(new DfuInstallasjonV(installasjon));
                        	loadTable(dfus);
                        	return;
                        }
                        if(installasjon != null) {
                        	bean.updateInstallasjon(installasjon, mipssPlugin.getLoader().getLoggedOnUserSign());
                        	loadFromEntity();
                        }
                        
                    }
                });
        
        btnEndreInstallasjon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = dfuTable.getSelectedRow();
				endreInstallasjon(index);
			}
        });
        
        btnSlettInstallasjon.addActionListener(new ActionListener(){
        	@Override
        	public void actionPerformed(ActionEvent e){
        		int index = dfuTable.getSelectedRow();
        		slettInstallasjon(index);
        	}
        });
    }
    
    private void slettInstallasjon(int viewIndex){
    	int idx = dfuTable.convertRowIndexToModel(viewIndex);
    	if(idx >= 0 && idx < dfuTable.getRowCount()) {
    		int v = JOptionPane.showConfirmDialog(this, getBundle().getString("message.slettInst.message"), getBundle().getString("message.slettInst.title"), JOptionPane.YES_NO_OPTION);
    		if (v==JOptionPane.YES_OPTION){
	    		DfuInstallasjonV installasjonv = dfus.get(idx);
	    		List<Installasjon> toRemove = new ArrayList<Installasjon>();
	    		if (installasjonv.getId()!=null){
		    		for (Installasjon i:mipssEntity.getInstallasjonList()){
		    			if (i.getId() != null && i.getId().equals(installasjonv.getId())){
		    				toRemove.add(i);
		    			}
		    		}
		    		mipssEntity.getInstallasjonList().removeAll(toRemove);
	    		}
	    		if (getInstrumentationController() instanceof WizardLine){
	    			dfus.remove(idx);
	    			mipssEntity.getInstallasjonList().remove(idx);
	    			loadTable(dfus);
	    		}else{
		    		bean.removeInstallasjon(installasjonv.getId());
		    		loadFromEntity();
	    		}
    		}
    	}
    }
    /**
     * @param viewIndex Valgt index i tabell
     */
    private void endreInstallasjon(int viewIndex) {
    	int modelIndex = dfuTable.convertRowIndexToModel(viewIndex);
		if(modelIndex >= 0 && modelIndex < dfuTable.getRowCount()) {
            DfuInstallasjonV dfuInstallasjonV = dfus.get(modelIndex);
            Installasjon installasjon = bean.getInstallasjon(dfuInstallasjonV.getId());
            KjoretoyInstallasjonPanel inst = PanelFactory.createKjoretoyInstallasjonPanel(installasjon, mipssPlugin);
            inst.setDialogBoxTitle(getBundle().getString("dfu.installasjon.nyRegistrering"));
            installasjon = inst.newEntityDialogBox();
            if (installasjon==null)
            	return;
            bean.updateInstallasjon(installasjon, mipssPlugin.getLoader().getLoggedOnUserSign());
            dfuTable.getSelectionModel().clearSelection();
            loadFromEntity();
		}
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        // init tittelen
        KjoretoyUtils.setBold(dfuTitle);
        dfuTable.setDefaultRenderer(Date.class, new DateTableCellRenderer(MipssDateFormatter.DATE_TIME_FORMAT));
        // init tabellkolonner og standard visning
        BrowserUtils.initKjoretoyInstallasjonTable(dfuTable);
//        KjoretoyUtils.initDefaultTableSettings(dfuTable);
        
        initListeners();
    }

    private void initComponents() {
        pnlMain = new JPanel();
        dfuTitle = new JLabel();
        scrlPaneDfuTable = new JScrollPane();
        
        dfuTable = new JXTable();
        dfuTable.setHighlighters(new Highlighter[] { 
        		 new ColorHighlighter(new HighlightPredicate(){
					@Override
					public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
						int modelIndex = dfuTable.convertRowIndexToModel(adapter.row);
						DfuInstallasjonV dfuInstallasjonV = dfus.get(modelIndex);
						Date atd = dfuInstallasjonV.getAktivTilDato();
						if (atd!=null&&atd.before(Clock.now())){
							return true;
						}
						return false;
					}
        		 }, Color.lightGray ,UIManager.getColor("Table.foreground"),UIManager.getColor("Table.selectionBackground"),UIManager.getColor("Table.selectionForeground"))});
        btnNyInstallasjon = new JButton();
        btnEndreInstallasjon = new JButton();
        btnSlettInstallasjon = new JButton();

        dfuTitle.setText(getBundle().getString("dfuList.title"));

        scrlPaneDfuTable.setViewportView(dfuTable);
        btnNyInstallasjon.setText(getBundle().getString("dfuList.nyInstallasjon"));
        btnEndreInstallasjon.setText(getBundle().getString("dfuList.endreInstallasjon"));
        btnSlettInstallasjon.setText(getBundle().getString("dfuList.slettInstallasjon"));
        
        btnNyInstallasjon.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnNyInstallasjon.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnEndreInstallasjon.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnEndreInstallasjon.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnSlettInstallasjon.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnSlettInstallasjon.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        pnlMain.setBorder(BorderFactory.createTitledBorder(getBundle().getString("dfuList.title")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(scrlPaneDfuTable, 		new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(btnNyInstallasjon, 		new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(btnEndreInstallasjon,	new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(btnSlettInstallasjon, 	new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));

        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
