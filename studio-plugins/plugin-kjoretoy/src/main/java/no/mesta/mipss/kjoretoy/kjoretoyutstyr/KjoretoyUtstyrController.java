package no.mesta.mipss.kjoretoy.kjoretoyutstyr;

import java.awt.Color;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.ListCellRenderer;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class KjoretoyUtstyrController implements PropertyChangeSource {
	private PropertyChangeSupport props;
	private BindingGroup bindingGroup;
	private List<KjoretoyUtstyr> eksisterendeUtstyr;
	private KjoretoyUtstyr kjoretoyUtstyr;
	private boolean nyttUtstyr;
	private MipssBeanTableModel<KjoretoyUtstyrIo> tblModelIOPeriode;
	private MipssRenderableEntityTableColumnModel colModelIOPeriode;
	private MipssComboBoxModel<Utstyrgruppe> cbxModelGruppe;
	private MipssComboBoxModel<Utstyrsubgruppe> cbxModelSubgruppe;
	private MipssComboBoxModel<Utstyrmodell> cbxModelModell;
	
	public KjoretoyUtstyrController(List<KjoretoyUtstyr> eksisterendeUtstyr, KjoretoyUtstyr kjoretoyUtstyr, boolean nyttUtstyr) {
		if(kjoretoyUtstyr.getId() != null) {
			kjoretoyUtstyr = KjoretoyUtils.getKjoretoyBean().getKjoretoyUtstyr(kjoretoyUtstyr.getId());
		}
		
		this.eksisterendeUtstyr = eksisterendeUtstyr;
		this.kjoretoyUtstyr = kjoretoyUtstyr;
		this.nyttUtstyr = nyttUtstyr;
		
		//Kode som fikser opp i uheldigheter fra forrige versjon
		if(kjoretoyUtstyr.getUtstyrsubgruppe() == null && kjoretoyUtstyr.getUtstyrmodell() != null) {
			kjoretoyUtstyr.setUtstyrsubgruppe(kjoretoyUtstyr.getUtstyrmodell().getUtstyrsubgruppe());
		}
		if(kjoretoyUtstyr.getUtstyrgruppe() == null && kjoretoyUtstyr.getUtstyrsubgruppe() != null) {
			kjoretoyUtstyr.setUtstyrgruppe(kjoretoyUtstyr.getUtstyrsubgruppe().getUtstyrgruppe());
		}
		
		tblModelIOPeriode = new MipssBeanTableModel<KjoretoyUtstyrIo>(KjoretoyUtstyrIo.class, "fraDato", "tilDato", "iolisteNavn", "bitNr", "sensortypeNavn", "dfuModellNavn");
		colModelIOPeriode = new MipssRenderableEntityTableColumnModel(KjoretoyUtstyrIo.class, tblModelIOPeriode.getColumns());
		tblModelIOPeriode.setEntities(kjoretoyUtstyr.getKjoretoyUtstyrIoList());
		
		List<Utstyrgruppe> utstyrgruppeList = KjoretoyUtils.getKjoretoyBean().getUtstyrgruppeList();
		Collections.sort(utstyrgruppeList, new Comparator<Utstyrgruppe>() {
			@Override
			public int compare(Utstyrgruppe o1, Utstyrgruppe o2) {
				return o1.getNavn().compareTo(o2.getNavn());
			}
		});
		cbxModelGruppe = new MipssComboBoxModel<Utstyrgruppe>(utstyrgruppeList, true);
		cbxModelSubgruppe = new MipssComboBoxModel<Utstyrsubgruppe>(new ArrayList<Utstyrsubgruppe>(), true);
		cbxModelModell = new MipssComboBoxModel<Utstyrmodell>(new ArrayList<Utstyrmodell>(), true);
		
		cbxModelGruppe.addPropertyChangeListener("selectedItem", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				cbxGruppeUpdated();
			}
		});
		cbxModelSubgruppe.addPropertyChangeListener("selectedItem", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				cbxSubgruppeUpdated();
			}
		});
		cbxModelModell.addPropertyChangeListener("selectedItem", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				cbxModellUpdated();
			}
		});
		
		getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyr, "utstyrgruppe", cbxModelGruppe, "selectedItem", BindingHelper.READ_WRITE));
		getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyr, "utstyrsubgruppe", cbxModelSubgruppe, "selectedItem", BindingHelper.READ_WRITE));
		getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyr, "utstyrmodell", cbxModelModell, "selectedItem", BindingHelper.READ_WRITE));
	}
	
	public BindingGroup getBindingGroup() {
		if(bindingGroup == null) {
			bindingGroup = new BindingGroup();
		}
		
		return bindingGroup;
	}

	public KjoretoyUtstyr getKjoretoyUtstyr() {
		return kjoretoyUtstyr;
	}

	public Action getLagreAction(final Window window) {
		return new AbstractAction(KjoretoyUtils.getPropertyString("utstyrsdialog.btnLagre")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.dispose();
				bindingGroup.unbind();
			}
		};
	}
	
	public Action getAvbrytAction(final Window window) {
		return new AbstractAction(KjoretoyUtils.getPropertyString("utstyrsdialog.btnAvbryt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.dispose();
				bindingGroup.unbind();
			}
		};
	}
	
	public Action getNyIOPeriodeAction(final Window window) {
		return new AbstractAction(KjoretoyUtils.getPropertyString("utstyrsdialog.btnNyIOPeriode")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				KjoretoyUtstyrIoDialog ioPeriodeDialog = new KjoretoyUtstyrIoDialog(KjoretoyUtstyrController.this, window, new KjoretoyUtstyrIo(), true);
				KjoretoyUtstyrIo kjoretoyUtstyrIo = ioPeriodeDialog.showDialog();
				if(kjoretoyUtstyrIo != null) {
					tblModelIOPeriode.getEntities().add(kjoretoyUtstyrIo);
					tblModelIOPeriode.fireTableModelEvent(0, tblModelIOPeriode.getSize(), TableModelEvent.UPDATE);
				}
			}
		};
	}
	
	public Action getEndreIOPeriodeAction(final Window window, final JXTable tblKjoretoyUtstyrIo) {
		return new AbstractAction(KjoretoyUtils.getPropertyString("utstyrsdialog.btnEndreIOPeriode")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = tblKjoretoyUtstyrIo.getSelectedRow();
				if(index >= 0 && index < tblKjoretoyUtstyrIo.getRowCount()) {
					index = tblKjoretoyUtstyrIo.convertRowIndexToModel(index);
					KjoretoyUtstyrIo kjoretoyUtstyrIo = tblModelIOPeriode.getEntities().get(index);
					KjoretoyUtstyrIoDialog kjoretoyUtstyrIoDialog = new KjoretoyUtstyrIoDialog(KjoretoyUtstyrController.this, window, kjoretoyUtstyrIo, false);
					KjoretoyUtstyrIo kjoretoyUtstyrIo2 = kjoretoyUtstyrIoDialog.showDialog();
					if(kjoretoyUtstyrIo2 != null) {
						kjoretoyUtstyrIo.setTilDato(kjoretoyUtstyrIo2.getTilDato());
						kjoretoyUtstyrIo.setFraDato(kjoretoyUtstyrIo2.getFraDato());
						kjoretoyUtstyrIo.setTilDato(kjoretoyUtstyrIo2.getTilDato());
						kjoretoyUtstyrIo.setSensortype(kjoretoyUtstyrIo2.getSensortype());
						kjoretoyUtstyrIo.setKjoretoyUtstyr(kjoretoyUtstyrIo2.getKjoretoyUtstyr());
						kjoretoyUtstyrIo.setIoliste(kjoretoyUtstyrIo2.getIoliste());
						kjoretoyUtstyrIo.setBitNr(kjoretoyUtstyrIo2.getBitNr());
						kjoretoyUtstyrIo.setSensortypeNavn(kjoretoyUtstyrIo2.getSensortypeNavn());
						tblModelIOPeriode.fireTableModelEvent(index, index, TableModelEvent.UPDATE);
					}
				}
			}
		};
	}
	
	public Action getSlettIOPeriodeAction(final JXTable tblKjoretoyUtstyrIo) {
		return new AbstractAction(KjoretoyUtils.getPropertyString("utstyrsdialog.btnSlettIOPeriode")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] indecies = tblKjoretoyUtstyrIo.getSelectedRows();
				List<KjoretoyUtstyrIo> remList = new ArrayList<KjoretoyUtstyrIo>(indecies.length);
				for(int i = 0; i < indecies.length; i++) {
					int index = indecies[i];
					if(index >= 0 && index < tblKjoretoyUtstyrIo.getRowCount()) {
						index = tblKjoretoyUtstyrIo.convertRowIndexToModel(index);
						remList.add(tblModelIOPeriode.getEntities().get(index));
					}
				}
				tblModelIOPeriode.getEntities().removeAll(remList);
				tblModelIOPeriode.fireTableModelEvent(0, tblKjoretoyUtstyrIo.getRowCount(), TableModelEvent.UPDATE);
			}
		};
	}
	
	public TableModel getIOPeriodeTableModel() {
		return tblModelIOPeriode;
	}
	
	public TableColumnModel getIOPeriodeTableColumnModel() {
		return colModelIOPeriode;
	}
	
	public ComboBoxModel getGruppeCbxModel() {
		return cbxModelGruppe;
	}
	
	public ComboBoxModel getSubgruppeCbxModel() {
		return cbxModelSubgruppe;
	}
	
	public ComboBoxModel getModellCbxModel() {
		return cbxModelModell;
	}
	
	public ListCellRenderer getGruppeRenderer() {
		return new MipssListCellRenderer<Utstyrgruppe>();
	}
	
	public ListCellRenderer getSubgruppeRenderer() {
		return new MipssListCellRenderer<Utstyrsubgruppe>();
	}
	
	public ListCellRenderer getModellRenderer() {
		return new MipssListCellRenderer<Utstyrmodell>();
	}
	
	public Boolean getGruppeEditable() {
		return cbxModelGruppe != null && cbxModelGruppe.getEntities() != null && cbxModelGruppe.getEntities().size() > 0;
	}
	
	public Boolean getSubgruppeEditable() {
		return cbxModelSubgruppe != null && cbxModelSubgruppe.getEntities() != null && cbxModelSubgruppe.getEntities().size() > 0;
	}
	
	public Boolean getModellEditable() {
		return cbxModelModell != null && cbxModelModell.getEntities() != null && cbxModelModell.getEntities().size() > 0;
	}
	
	public Color getGruppeForeground() {
		return getGruppeEditable() && cbxModelGruppe.getSelectedItem() == null ? Color.RED : Color.BLACK;
	}
	
	public Color getSubgruppeForeground() {
		return getSubgruppeEditable() && cbxModelSubgruppe.getSelectedItem() == null && cbxModelGruppe.getSelectedItem().getKreverSubgrFlagg() ? Color.RED : Color.BLACK;
	}
	
	public Color getModellForeground() {
		return getModellEditable() && cbxModelModell.getSelectedItem() == null && cbxModelSubgruppe.getSelectedItem().getKreverModellFlagg() ? Color.RED : Color.BLACK;
	}
	
	private void cbxGruppeUpdated() {
		Boolean oldSubgruppeEditable = getSubgruppeEditable();
		
		Utstyrgruppe utstyrgruppe = cbxModelGruppe.getSelectedItem();
		if(utstyrgruppe != null) {
			List<Utstyrsubgruppe> utstyrsubgruppeList = KjoretoyUtils.getKjoretoyBean().getUtstyrsubgruppeListFor(utstyrgruppe.getId());
			Collections.sort(utstyrsubgruppeList, new Comparator<Utstyrsubgruppe>() {
				@Override
				public int compare(Utstyrsubgruppe o1, Utstyrsubgruppe o2) {
					return o1.getNavn().compareTo(o2.getNavn());
				}
			});
			cbxModelSubgruppe.setEntities(utstyrsubgruppeList);
		} else {
			cbxModelSubgruppe.setEntities(new ArrayList<Utstyrsubgruppe>());
		}
		
		getProps().firePropertyChange("subgruppeEditable", oldSubgruppeEditable, getSubgruppeEditable());
		getProps().firePropertyChange("gruppeForeground", Color.WHITE, getGruppeForeground());
	}
	
	private void cbxSubgruppeUpdated() {
		Boolean oldModellEditable = getModellEditable();
		
		Utstyrsubgruppe utstyrsubgruppe = cbxModelSubgruppe.getSelectedItem();
		if(utstyrsubgruppe != null) {
			List<Utstyrmodell> utstyrmodellList = KjoretoyUtils.getKjoretoyBean().getUtstyrmodellListFor(utstyrsubgruppe.getId());
			Collections.sort(utstyrmodellList, new Comparator<Utstyrmodell>() {
				@Override
				public int compare(Utstyrmodell o1, Utstyrmodell o2) {
					return o1.getNavn().compareTo(o2.getNavn());
				}
			});
			cbxModelModell.setEntities(utstyrmodellList);
		} else {
			cbxModelModell.setEntities(new ArrayList<Utstyrmodell>());
		}
		
		getProps().firePropertyChange("modellEditable", oldModellEditable, getModellEditable());
		getProps().firePropertyChange("subgruppeForeground", Color.WHITE, getSubgruppeForeground());
	}
	
	private void cbxModellUpdated() {
		getProps().firePropertyChange("modellForeground", Color.WHITE, getModellForeground());
	}

	private PropertyChangeSupport getProps() {
		if(props == null) {
			props = new PropertyChangeSupport(this);
		}
		
		return props;
	}
	
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		getProps().addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		getProps().removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		getProps().removePropertyChangeListener(propName, l);
	}
	
	private class AktivIOPeriode {
		private String kjoretoyutstyrNavn;
		private Date fraDato;
		private Date tilDato;
		private Ioliste ioliste;
	}

	public boolean verifiserKjoretyrUtstyrIo(KjoretoyUtstyrIoDialog kjoretoyUtstyrIoDialog, KjoretoyUtstyrIo kjoretoyUtstyrIo) {
		return true;
	}
}
