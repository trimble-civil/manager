/**
 * 
 */
package no.mesta.mipss.kjoretoy.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.dokarkiv.Dokformat;
import no.mesta.mipss.persistence.dokarkiv.Doktype;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.util.EpostUtils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Verkt�ysklasse med statiske metoder for lesing av {@code Bilde} og {@code Dokument}
 * fra filer som ligger utenfor classpath
 * 
 * @author jorge
 *
 */
public class FileUtils {
	private static Logger logger = LoggerFactory.getLogger(FileUtils.class);
	
	/**
	 * Leser en bildefil og leverer dens innhold i et Bildeobjekt.
	 * Bildeobjektet er initialisert med filens navn (ikke absolute path) og st�rrelse.
	 * Bildets GUID blir ikke satt.
	 * @param file 
	 * @return et nytt Bilde eller NULL hvis exceptions kastet.
	 */
    public static Bilde loadBilde(File file) {
    	ByteArrayOutputStream stream = readFile(file);
    	
    	if(stream == null) {
    		return null;
    	}
    	byte[] bytes = stream.toByteArray();
    	
    	Bilde bilde = new Bilde();
    	bilde.setFilnavn(file.getName());
    	bilde.setBildeLob(bytes);

        return bilde;
    }

    /**
     * Leser en fil og leverer dens innhols i et {@code Dokument}. 
     * Dokumentet er initialisert med filens sti, navn og st�rrelse.
     * @param file
     * @return
     */
    public static Dokument loadDokument(File file, MipssKjoretoy bean) {
    	ByteArrayOutputStream stream = readFile(file);
    	
    	if(stream == null) {
    		return null;
    	}
    	byte[] bytes = stream.toByteArray();
    	
    	Dokument dok = new Dokument();
    	dok.setFilsti(file.getPath());
    	dok.setLob(bytes);
    	dok.setNavn(file.getName());
    	
    	Dokformat format = bean.getDokformat(getFileNameExtension(file.getName()));
    	if (format == null){
    		JOptionPane.showMessageDialog(null, Resources.getResource("message.feilformat.message", EpostUtils.getSupportEpostAdresse()), Resources.getResource("message.feilformat.title"), JOptionPane.ERROR_MESSAGE);
    		return null;
    	}
    	Doktype type = bean.getDoktype("Kjoretoy");
    	dok.setDokformat(format);
    	dok.setDoktype(type);
    	
        return dok;
    }
    
    /**
     * Leser innhodlet i en fil inn i en ByteArrayOutputStream.
     * @param file
     * @return stream'en eller NULL hvis problemer.
     */
    private static ByteArrayOutputStream readFile(File file) {
    	FileInputStream inFile = null;
    	
    	try {
    		inFile = new FileInputStream(file);
    	} 
    	catch (FileNotFoundException e) {
    		logger.warn("Finner ikke " + file.getAbsolutePath());
    		return null;
    	}

    	ByteArrayOutputStream stream = new ByteArrayOutputStream();
    	try {
    		IOUtils.copy(inFile, stream);
    	}
    	catch (Exception e) {
			logger.warn("kan ikke lese " + file.getAbsolutePath() + ": " + e.getMessage());
			return null;
		}
    	try {
			inFile.close();
		} catch (IOException e) {
			logger.warn("ignorerer at " + file.getAbsolutePath() + " ikke kan stenges: " + e.getMessage());
		}
    	return stream;
    }
    
    /**
     * Lagrer det leverte bufferet i en temporarfil med den leverte extension.
     * @param ext file extension som skal typifisere filen.
     * @param buffer data som skal lagres
     * @return den lagrede filen
     */
    public static File saveTempFile(String ext, byte[] buffer) {
    	File outFile;
    	FileOutputStream fileStream;
    	try {
    		outFile = File.createTempFile("temp_fil", ext);
    		fileStream = new FileOutputStream(outFile);
    		ByteArrayInputStream byteStream = new ByteArrayInputStream(buffer);
    		IOUtils.copy(byteStream, fileStream);
    		fileStream.close();
    	}
    	catch (FileNotFoundException e) {
			logger.warn("Finner ikke filen å lagre: " + e.getMessage());
			return null;
		}
    	catch (IOException e) {
    		logger.warn("Lagringsfeil for temposrsr fil: " + e.getMessage());
    		return null;
		}
    	return outFile;
    }

	/**
	 * Ekstraherer alle tegn bak punktum (.) i den leverte strengen og setter
	 * dem i low case.
	 * 
	 * @param fileName
	 * @return tegnene eller en tom streng hvis filenavnet manglet extension.
	 */
	public static String getFileNameExtension(String fileName) {
	    String ext = null;
	    int i = fileName.lastIndexOf('.');
	
	    if (i > 0 &&  i < fileName.length() - 1) {
	        ext = fileName.substring(i+1).toLowerCase();
	    }
	    
	    if(ext == null) {
	    	ext = "";
	    }
	    return ext;
	}

}
