package no.mesta.mipss.kjoretoy.component.stroing;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.persistence.stroing.Stroperiode;

import java.util.Date;
import java.util.List;

class GritPeriodUtil {

    static boolean periodCollidesWithExistingPeriods(Stroperiode newPeriod, List<Stroperiode> periods, boolean ignoreActivePeriod) {
        Stroperiode activePeriod = getActiveGritPeriod(periods);

        for (Stroperiode existingPeriod : periods) {
            if (ignoreActivePeriod && existingPeriod.equals(activePeriod)) {
                continue;
            } else {
                if (periodContainsExistingPeriod(newPeriod, existingPeriod)) {
                    return true;
                } else if (CalendarUtil.isOverlappende(newPeriod.getFraDato(), newPeriod.getTilDato(), existingPeriod.getFraDato(), existingPeriod.getTilDato(), true)) {
                    return true;
                }
            }
        }

        return false;
    }

    static boolean periodBecomesNewActivePeriod(Stroperiode newActivePeriod, Stroperiode currentlyActivePeriod) {
        return currentlyActivePeriod.getFraDato().before(newActivePeriod.getFraDato()) && (currentlyActivePeriod.getTilDato() == null ||
                currentlyActivePeriod.getTilDato().getTime() >= newActivePeriod.getFraDato().getTime());
    }

    static boolean periodContainsExistingPeriod(Stroperiode newPeriod, Stroperiode existingPeriod) {
        if (newPeriod.getTilDato() == null && existingPeriod.getTilDato() == null) {
            return newPeriod.getFraDato().before(existingPeriod.getFraDato());
        } else if (newPeriod.getTilDato() == null) {
            return newPeriod.getFraDato().before(existingPeriod.getTilDato());
        } else if (existingPeriod.getTilDato() == null) {
            return newPeriod.getTilDato().after(existingPeriod.getFraDato());
        } else {
            return existingPeriod.getFraDato().after(newPeriod.getFraDato()) && existingPeriod.getTilDato().before(newPeriod.getTilDato());
        }
    }

    static Stroperiode getActiveGritPeriod(List<Stroperiode> periods) {
        Stroperiode activePeriod = null;
        Date latestDate = null;

        for (Stroperiode existingPeriod : periods) {
            if (existingPeriod.getTilDato() == null) {
                return existingPeriod;
            } else if (latestDate == null || existingPeriod.getTilDato().after(latestDate)) {
                latestDate = existingPeriod.getTilDato();
                activePeriod = existingPeriod;
            }
        }

        return activePeriod;
    }

}