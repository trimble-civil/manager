package no.mesta.mipss.kjoretoy.tablebrowser;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import no.mesta.mipss.kjoretoy.common.SingleLineListSelectionListener;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.KjoretoyUtstyrPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.NewKjoretoyUtstyrPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrView;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.util.KjoretoyUtils;
import no.mesta.mipss.util.KjoretoyUtils.DialogElection;

import org.jdesktop.swingx.JXTable;


/**
 * Navigerer i en tabell med kjøretøyUtstyr og viser tabellraden somer valgt.
 * Støtter alle mulige oppdateringer vha. en nedre rad med knapper.
 * @author jorge
 */
public class KjoretoyUtstyrBrowser extends BrowserPanel<KjoretoyUtstyrPanel, KjoretoyUtstyrView, KjoretoyUtstyr> {
	private static final long serialVersionUID = -720342501332902118L;
	
	private KjoretoyUtstyrPanel utstyrPanel = null;
    private EditableMippsTableModel<KjoretoyUtstyrView> model = null;
    private Kjoretoy ownerEntity = null;
    private int tableIndex = 0;
    private JButton nyButton = null;
    private JButton slettButton = null;
    
    private boolean enkelVisning;

	private MipssPlugin plugin;
    
    /**
     * Oppretter en instans KjoretoyUtstyrBrowser som er ferdig for å vises.
     * @param ownerEntity Man henter id (kaller getId()) på denne entiteten for å vise den
     * windows tittel.
     * @param entities listen en entiteter som vises i tabellen.
     * @param index hvilken rad i tabellen skal velges fra starten av.
     */
    public KjoretoyUtstyrBrowser(Kjoretoy ownerEntity, List<KjoretoyUtstyrView> entities, int index, Container parent, boolean enkelVisning, MipssPlugin plugin) {
        super(parent);
        
        this.ownerEntity = ownerEntity;
        this.tableIndex = index;
        this.enkelVisning = enkelVisning;
        tableEntities = entities;
		this.plugin = plugin;
                
        utstyrPanel = PanelFactory.createKjoretoyUtstyrPanel(null, enkelVisning, plugin);
        setUnitPanel(utstyrPanel);
        initButtons();
        
        setTitle(String.format(bundle.getString("utstyr.browserTitle"), ownerEntity.getTextForGUI()));
        
        initTable(getBrowseTable(), index);
    }
    
    /**
     * Oppdaterer tabellen fra databasen.
     */
    @SuppressWarnings("unchecked")
	public void reloadTable(KjoretoyUtstyr selectEntity) {
        try {
            tableEntities = bean.getKjoretoyUtstyrViewList(ownerEntity.getId());
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, bundle.getString("gen.ingenOppdatering"));
            return;
        }
        JTable table = super.getBrowseTable();
        
        // hvis man ikke frisker opp modellen, kan tabellvisningen feile
        BrowserUtils.initKjoretoyUtstyrTable(table, enkelVisning);

        model = (EditableMippsTableModel<KjoretoyUtstyrView>)table.getModel();
        model.setEntities(tableEntities);
        
        // slå av slettknappen hvis listen er tom
        if(tableEntities.size() == 0) {
        	slettButton.setEnabled(false);
        }
        else {
        	slettButton.setEnabled(true);
        }
        
        // hvis det er en enhet som ble oppdatert eller lagt til, velg den
        int selection = 0;
        if (selectEntity != null) {   
            for (KjoretoyUtstyrView l : tableEntities) {
            	long viewId = l.getId();
            	long entId = selectEntity.getId();
                if (viewId == entId){
                    tableIndex = table.convertRowIndexToView(selection);
                    table.changeSelection(tableIndex, 0, false, false);
                    break;
                }
                selection++;
            }
        }
        // hvis ingen enhet eller ikke funnet i tabellen
        if(selectEntity == null || selection == tableEntities.size()) {
            tableIndex = 0;
            table.changeSelection(tableIndex, 0, false, false);
        }
        // tøm visningen av et item i listen
        utstyrPanel.setMipssEntity(selectEntity);
        table.repaint();
    }
    
    @SuppressWarnings("unchecked")
	private void initTable(final JXTable table, int index) {
        // sett opp titler og modellen
        BrowserUtils.initKjoretoyUtstyrTable(table, enkelVisning);
        
        // koble entitetene til modellen
        model = (EditableMippsTableModel<KjoretoyUtstyrView>)table.getModel();
        model.setEntities(tableEntities);
        
        // sett opp betjeneningen av radvalg i tabellen.
        table.getSelectionModel().addListSelectionListener(new SingleLineListSelectionListener() {
                    public void selectionServiceMethod(int index) {
                        tableIndex = index;
                        int modelIndex = table.convertRowIndexToModel(index);
                        
                        // hent nøkkelen for entiteten som skal vises
                        if (tableEntities == null || tableEntities.size() == 0) {
                            return;
                        }
                        Long id = tableEntities.get(modelIndex).getId();
                        
                        if(id == null) {
                        	JOptionPane.showMessageDialog(utstyrPanel, bundle.getString("gen.ikkeViseUtstyrSomIkkeErLagret"));
                        	utstyrPanel.setMipssEntity(null);
                        	//Nasty woraround for å ikke åpne vindu.. 
//                        	SwingUtilities.windowForComponent(KjoretoyUtstyrBrowser.this).dispose();
                        } else {
                        	// vis entiteten
                        	utstyrPanel.setMipssEntity(bean.getKjoretoyUtstyr(id));
                        	if (utstyrPanel.getMipssEntity().getId().equals(id)) {
                        		tableIndex = index;
                        		table.changeSelection(tableIndex, 0, false, false);
							}
                        }
                    }
                });
        
        // velg raden som kalleren har bestemt
        table.changeSelection(index, 0, false, false);
    }
    
    /**
     * initialiserer knappenen slik at:
     * <ul>
     * <li>den første knappen er for opprettelse
     *  av nye KjoretoyUtstyr og derfor kontrolleres kun i denne browseren.</li>
     * 
     * <li>Den andre knappen er for fjerning av eksisterende KjoretoyUtstyr og kontrolleres kun
     * herfra også.</li>
     * 
     * <li>De to siste kontrolleres fra enhetsvinsningspanelen. De brukes for å lagre/forkaste
     * endringene i en enkel enhet.</li>
     * </ul>
     */
    private void initButtons() {
        // oppretter knapper vha. sine navn
        String[] labels = new String[] {
            bundle.getString("button.ny"),
            bundle.getString("button.slett"),
            bundle.getString("button.lagre"),
            bundle.getString("button.avbrytt")};
        
        getButtonPanel().initButtons(labels);
        
        // init knappen for nye forekomster
        nyButton = getButtonPanel().getButton(0);        
        nyButton.setEnabled(true);
        nyButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        launchNewKjoretoyUtstyrPanel();
                    }
                });
        
        // init knappen for fjerning av forkomster
        slettButton = getButtonPanel().getButton(1);                
        slettButton.setEnabled(true);
        slettButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        removeKjoretoyUtstyr();
                    }
                });
        
        // lever referanser til knappene
        utstyrPanel.setLagreButton(getButtonPanel().getButton(2));
        utstyrPanel.setAvbrytButton(getButtonPanel().getButton(3));
    }

        
    /**
     * åpner et dialogvindu for å registrere nytt utstyr
     */
    private void launchNewKjoretoyUtstyrPanel() {
//    	JOptionPane.showMessageDialog(this, "Kommer");
    	
    	// opprett dialogvinduet
        NewKjoretoyUtstyrPanel newKjoretoyUtstyr = PanelFactory.createNewKjoretoyUtstyrPanel(plugin, ownerEntity, enkelVisning);
        newKjoretoyUtstyr.setParent(this);
        
        // vis dialogvinduet og vent på svar
        KjoretoyUtstyr newUtstyr = newKjoretoyUtstyr.newEntityDialogBox();
        if (newUtstyr != null) {
        	newUtstyr = bean.addKjoretoyUtstyr(newUtstyr);
            reloadTable(newUtstyr);
        }
    }
    
    /**
     * Fjerner et {@code KjoretoyUtstyr} fra listen.
     */
    private void removeKjoretoyUtstyr() {
    	if (tableEntities == null || tableEntities.size() == 0) {
    		return;
    	}
        // spør brukeren om sletting
    	int modelIndex = super.getBrowseTable().convertRowIndexToModel(tableIndex);
        KjoretoyUtstyrView current = tableEntities.get(modelIndex);
         
        DialogElection election = KjoretoyUtils.showSlettForkastDialog(this, getInfo(current), bundle.getString("utstyr.sletting"));
        if (election == DialogElection.JA) {
            try {
                bean.removeKjoretoyUtstyrWithId(current.getId());
                ownerEntity = bean.getWholeKjoretoy(ownerEntity.getId());
            }
            catch (Exception e) {
                KjoretoyUtils.showException(e, bundle.getString("gen.ingenSletting"));
                return;
            }
            logger.debug("Slettet kjøretøyUtstyr " + current.getId());
            reloadTable(null);
        }
    }
    
    private String getInfo(KjoretoyUtstyrView current) {
        StringBuffer b = new StringBuffer("Fjerne montering av utstyr på kjøretøy ");
        b.append(ownerEntity.getTextForGUI()).
        append('\n');
        
        if(current.getUtstyrgruppeNavn() != null) {
            b.append("\n\tmed utstyrgruppe ").
            append(current.getUtstyrgruppeNavn());
        }
        if(current.getUtstyrsubgruppeNavn() != null) {
            b.append("\n\tmed utstyrsubgruppe ").
            append(current.getUtstyrsubgruppeNavn());
        }
        if(current.getUtstyrmodellNavn() != null) {
            b.append("\n\tmed utstyrmodell ").
            append(current.getUtstyrmodellNavn());
        }

        b.append('?');
            
        return b.toString();
    }

	@Override
	protected Dimension getDialogSize() {
		return new Dimension(400, 300);
	}
}
