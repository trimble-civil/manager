package no.mesta.mipss.kjoretoy.component.stroing;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.utils.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.klipping.Klippeperiode;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class KlippeperioderListPanel extends AbstractKjoretoyPanel implements CompositePanel {
	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneOrdreList = new JScrollPane();
	private JMipssBeanTable<Klippeperiode> tblKlippeperiode;
	private MipssBeanTableModel<Klippeperiode> mdlKlippeperiode;
	private JButton btnEndre = new JButton();
	private JButton btnNy = new JButton();
	private JButton btnSlett = new JButton();
	
    private Container parent;
	private MipssKjoretoy mipssKjoretoy;
	private MaintenanceContract maintenanceContract;
	private MipssPlugin parentPlugin;
    private boolean loaded = false;
    private KlippeperioderPanel parentPanel;
    private BindingGroup bindingGroup;
	
	public KlippeperioderListPanel(InstrumentationController controller, KlippeperioderPanel parentPanel, Kjoretoy kjoretoy, Container parent, MipssKjoretoy mipssKjoretoy, MaintenanceContract maintenanceContract, MipssPlugin parentPlugin) {
		super(kjoretoy, controller);
		
		this.parentPanel = parentPanel;
		this.parent = parent;
		this.mipssKjoretoy = mipssKjoretoy;
		this.maintenanceContract = maintenanceContract;
		this.parentPlugin = parentPlugin;
		
		bindingGroup = new BindingGroup();
		
		initTable();
		initButtons();
		initGui();
		
		bindingGroup.bind();
	}

	private void initButtons() {
		btnNy.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.leggTil"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Klippeperiode klippeperiode = new Klippeperiode();
				klippeperiode.setKjoretoy(mipssEntity);
				klippeperiode.setFraDato(Clock.now());
				klippeperiode.setOpprettetAv(parentPlugin.getLoader().getLoggedOnUserSign());
				klippeperiode.setOpprettetDato(Clock.now());
				//TODO lag klippeperiodedialog
				KlippeperiodeDialog klippeperiodeDialog = null; 
				if(parent instanceof JFrame) {
					klippeperiodeDialog = new KlippeperiodeDialog((JFrame)parent, klippeperiode, mipssEntity.getKlippeperiodeList(), true);
				} else {
					klippeperiodeDialog = new KlippeperiodeDialog((JDialog)parent, klippeperiode, mipssEntity.getKlippeperiodeList(), true);
				}
				
				Klippeperiode klippeperiodeNy = klippeperiodeDialog.showDialog();
				if(klippeperiodeDialog.isKlippeperiodeOk()) {
					klippeperiode.setFraDato(klippeperiodeNy.getFraDato());
					klippeperiode.merge(klippeperiodeNy);
					List<Klippeperiode> list = mipssEntity.getKlippeperiodeList();
					list.add(klippeperiode);
					mdlKlippeperiode.fireTableModelEvent(0, list.size(), TableModelEvent.INSERT);
					enableSaveButton(true);
				}
			}
		});
		
		btnEndre.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.endre"), IconResources.DETALJER_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKlippeperiode.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblKlippeperiode.getRowCount()) {
					Klippeperiode klippeperiode = mdlKlippeperiode.get(tblKlippeperiode.convertRowIndexToModel(selectedRow));
					endreKlippeperiode(klippeperiode);
					mdlKlippeperiode.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
				}
			}
		});
		if (UserUtils.isAdmin(parentPlugin.getLoader().getLoggedOnUser(true))){
			btnSlett.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.slett"), IconResources.REMOVE_ITEM_ICON) {
				@Override
				public void actionPerformed(ActionEvent e) {
					int r = JOptionPane.showConfirmDialog(KlippeperioderListPanel.this, KjoretoyUtils.getPropertyString("slettKlippeperioder.warning"), KjoretoyUtils.getPropertyString("slettKlippeperioder.warning.title"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
						if (r==JOptionPane.YES_OPTION){
						int selectedRows[] = tblKlippeperiode.getSelectedRows();
						List<Klippeperiode> removed = new ArrayList<Klippeperiode>();
						for(int i=0; i<selectedRows.length; i++) {
							int selectedRow = selectedRows[i];
							if(selectedRow >= 0 && selectedRow < tblKlippeperiode.getRowCount()) {
								Klippeperiode klippeperiode = mdlKlippeperiode.get(tblKlippeperiode.convertRowIndexToModel(selectedRow));
								removed.add(klippeperiode);
							}
						}
						
						if(removed.size() > 0) {
							mipssEntity.getKlippeperiodeList().removeAll(removed);
							mdlKlippeperiode.fireTableModelEvent(0, tblKlippeperiode.getRowCount(), TableModelEvent.UPDATE);
							enableSaveButton(true);
						}
					}
				}
			});
			bindingGroup.addBinding(BindingHelper.createbinding(tblKlippeperiode, "${noOfSelectedRows > 0}", btnSlett, "enabled"));
		}
		//ikke admin, bruker kan ikke slette strøperioder. 
		else{
			btnSlett.setText(KjoretoyUtils.getPropertyString("button.slett"));
			btnSlett.setIcon(IconResources.REMOVE_ITEM_ICON);
			btnSlett.setEnabled(false);
			btnSlett.setToolTipText(Resources.getResource("button.slett.tooltip", EpostUtils.getSupportEpostAdresse()));
		}
		
		
		bindingGroup.addBinding(BindingHelper.createbinding(tblKlippeperiode, "${noOfSelectedRows == 1}", btnEndre, "enabled"));
	}
	
	private void initTable() {
		mdlKlippeperiode = new MipssBeanTableModel<Klippeperiode>("klippeperiodeList", Klippeperiode.class, "klippebreddeCm", "fraDato", "tilDato");
		BindingHelper.createbinding(this, "mipssEntity", mdlKlippeperiode, "sourceObject", BindingHelper.READ).bind();
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Klippeperiode.class, mdlKlippeperiode.getColumns());
		tblKlippeperiode = new JMipssBeanTable<Klippeperiode>(mdlKlippeperiode, colModel);
		tblKlippeperiode.setFillsViewportWidth(true);
		tblKlippeperiode.setDoWidthHacks(false);
		tblKlippeperiode.setDefaultRenderer(Date.class, new DateTimeRenderer());
		tblKlippeperiode.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					int selectedRow = tblKlippeperiode.getSelectedRow();
					if(selectedRow >= 0 && selectedRow < tblKlippeperiode.getRowCount()) {
						Klippeperiode klippeperiode = mdlKlippeperiode.get(tblKlippeperiode.convertRowIndexToModel(selectedRow));
						endreKlippeperiode(klippeperiode);
						mdlKlippeperiode.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
					}
				}
			}
		});
	}
	
	private void initGui() {
		scrlPaneOrdreList.setViewportView(tblKlippeperiode);
		
		pnlMain.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("klipping.border")));
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneOrdreList, new GridBagConstraints(0, 0, 1, 4, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlMain.add(btnNy, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(btnEndre, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(btnSlett, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	private void endreKlippeperiode(Klippeperiode klippeperiode) {
		if(klippeperiode != null) {
			//TODO lage Klippeperiodedialog
			KlippeperiodeDialog klippeperiodeDialog = null; 
			if(parent instanceof JFrame) {
				klippeperiodeDialog = new KlippeperiodeDialog((JFrame)parent, klippeperiode, mipssEntity.getKlippeperiodeList(), false);
			} else {
				klippeperiodeDialog = new KlippeperiodeDialog((JDialog)parent, klippeperiode, mipssEntity.getKlippeperiodeList(), false);
			}
			
			Klippeperiode klippeperiodeNy = klippeperiodeDialog.showDialog();
			if(klippeperiodeDialog.isKlippeperiodeOk()) {
				klippeperiode.setKlippebreddeCm(klippeperiodeNy.getKlippebreddeCm());
				klippeperiode.setFraDato(klippeperiodeNy.getFraDato());
				klippeperiode.setTilDato(klippeperiodeNy.getTilDato());
				
				klippeperiode.setEndretAv(parentPlugin.getLoader().getLoggedOnUserSign());
				klippeperiode.setEndretDato(Clock.now());
				enableSaveButton(true);
			}
		}
	}
	
	@Override
	public void enableUpdating(boolean canUpdate) {
	}

	@Override
	protected void initComponentsPresentation() {
	}

	@Override
	protected void loadFromEntity() {
		if(mipssEntity != null) {
			List<Klippeperiode> ordreList = mipssEntity.getKlippeperiodeList();
			mdlKlippeperiode.setEntities(ordreList);
		} else {
			mdlKlippeperiode.setEntities(new ArrayList<Klippeperiode>());
		}
		enableSaveButton(false);
        loaded = true;
	}

	@Override
	public void setSessionBean(Object bean) {
		this.mipssKjoretoy = (MipssKjoretoy)bean;
	}

	@Override
	public void enableSaveButton(boolean enable) {
		if(parentPanel != null) {
			parentPanel.enableSaveButton(enable);
		}
		
		if(enable) {
			kjoretoyChanged();
		}
	}
}
