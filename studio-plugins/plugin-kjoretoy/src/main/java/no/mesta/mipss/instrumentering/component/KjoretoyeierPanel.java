/**
 * 
 */
package no.mesta.mipss.instrumentering.component;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;

import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

/**
 * @author jorge
 */
public class KjoretoyeierPanel extends JPanel {
	
	private ResourceBundle bundle = null;
	// Variables declaration - do not modify
    private JLabel lblEierNavn;
    private JLabel kjoretoyEierTitle1;
    private JTextField kjoretoyeierNavn1;
    private JButton velgKjoretoyeierButton;
    // End of variables declaration
	public KjoretoyeierPanel(Kjoretoy kjoretoy) {
		super();
		if(kjoretoy == null) {
			throw new IllegalStateException("har fått NULL kjoretoy");
		}
		
		bundle = Utils.getBundle();
		
		initComponents();
		Utils.setBold(kjoretoyEierTitle1);
	}
	
	private void initButton() {
		velgKjoretoyeierButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
    private void initComponents() {

        kjoretoyEierTitle1 = new JLabel();
        lblEierNavn = new JLabel();
        kjoretoyeierNavn1 = new JTextField();
        velgKjoretoyeierButton = new JButton();

        setName("Form"); // NOI18N

        kjoretoyEierTitle1.setText(bundle.getString("kjoretoyeier.nyttKjoretoy.title")); // NOI18N
        kjoretoyEierTitle1.setToolTipText(bundle.getString("kjoretoyeier.nyttKjoretoy.title.tooltip")); // NOI18N
        kjoretoyEierTitle1.setName("kjoretoyEierTitle1"); // NOI18N

        lblEierNavn.setText(bundle.getString("kjoretoyeier.navn")); // NOI18N
        lblEierNavn.setName("jLabel4"); // NOI18N

        kjoretoyeierNavn1.setEditable(false);
        kjoretoyeierNavn1.setToolTipText(bundle.getString("kjoretoyeier.nyttKjoretoy.title.tooltip")); // NOI18N
        kjoretoyeierNavn1.setName("kjoretoyeierNavn1"); // NOI18N

        velgKjoretoyeierButton.setText(bundle.getString("kjoretoyeier.velgButton")); // NOI18N
        velgKjoretoyeierButton.setName("velgKjoretoyeierButton1"); // NOI18N

        kjoretoyeierNavn1.setMinimumSize(new Dimension(150, 19));
        kjoretoyeierNavn1.setPreferredSize(new Dimension(150, 19));
        
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoyeier.nyttKjoretoy.title")));
        this.add(lblEierNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        this.add(kjoretoyeierNavn1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        this.add(velgKjoretoyeierButton, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        /*GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(kjoretoyEierTitle1, GroupLayout.PREFERRED_SIZE, 418, GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblEierNavn)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kjoretoyeierNavn1, GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(velgKjoretoyeierButton)
                        .addGap(77, 77, 77)))
                .addGap(95, 95, 95))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(kjoretoyEierTitle1)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEierNavn)
                    .addComponent(velgKjoretoyeierButton)
                    .addComponent(kjoretoyeierNavn1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );*/
    }
}
