package no.mesta.mipss.instrumentering.utils;

import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;


public class Utils {
	private static String bundleName = "Instrumentation";
	private static MaintenanceContract maintenanceContract= null;
	private static MipssKjoretoy mipssKjoretoy = null;
	private static SimpleDateFormat simpleDateFormat = null;
	
	public static ResourceBundle getBundle() {
		return ResourceBundle.getBundle(bundleName);
	}
	
	public static String getProperty(String key) {
		return ResourceBundle.getBundle(bundleName).getString(key);
	}
		
    /**
     * Instantierer sesjonsbønnen MaintenanceContract kun en gang og returnerer den.
     * @return b�nnen.
     */
	public static MaintenanceContract getMaintenanceContract() {
		if(maintenanceContract != null) {
			return maintenanceContract;
		}
		maintenanceContract = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
        return maintenanceContract;
	}
	
    /**
     * Instantierer sesjonsbønnen MipssKjoretoy kun en gang og returnerer den.
     * @return b�nnen.
     */
	public static MipssKjoretoy getKjoretoyBean() {
		if(mipssKjoretoy != null) {
			return mipssKjoretoy;
		}
		mipssKjoretoy = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
		// create the JPA session bean
        return mipssKjoretoy;
	}

	public static void showException(String mssg) {
		JOptionPane.showMessageDialog(null, mssg, "Uventet hendelse", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Formatterer date til en streng med format dd.MM.yyyy.
	 * @param date
	 * @return den formaterte dato eller "ikke satt" hvis date var null.
	 */
	public static String formatDate(Date date) {
		if(date == null) {
			return getBundle().getString("gen.ikkeSatt");
		}
		if(simpleDateFormat == null) {
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
		}
		return simpleDateFormat.format(date);
	}
	
	/**
	 * Kontrollerer om string ikke er null eller tom. 
	 * @param string
	 * @return string eller "ingen".
	 */
	public static String checkString(String string) {
		if(string == null || string.length() == 0) {
			return getBundle().getString("gen.ingen");
		}
		return string;
	}
	
    /**
     * Endrer tekstfremvisningen av den leverte JLabel til Bold.
     * @param label
     */
    public static void setBold(JComponent comp) {
        comp.setFont(new Font(comp.getFont().getFamily(), Font.BOLD, comp.getFont().getSize()));
    }
    

}
