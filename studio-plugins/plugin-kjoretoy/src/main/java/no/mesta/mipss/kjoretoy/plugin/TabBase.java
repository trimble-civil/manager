package no.mesta.mipss.kjoretoy.plugin;

import java.awt.Component;
import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.GroupLayout;
import javax.swing.JPanel;

import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Grunnlaget som hoved-TabbedPane bruker under seg.
 * Det er et tomt panel som tillater enkel endring av etiketten og
 * sentralisert visning.
 * Instanser av dette panelet får lagt til innholdet på to måter:
 * <ul>
 *   <li>Man kan bruk den vanlige add() metoden. Da kan man legge til innhold som ikke viser ett 
 *   enkelt kjøretøy. i dette tilfelle vil kall til <code>getKjoretoyView()</code> returnere null.</li>
 *   
 *   <li><Man kan bruke <code>setKjoretoyView()</code> for å sette dedikerte paneler som viser
 *   ett enkelt kjøretøy eller deler av det</li>
 * </ul>
 * @author jorge
 * @version
 */
public class TabBase extends JPanel {
	private static final long serialVersionUID = -559546457807647157L;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    // index i tabbedPane initialisert til "ikke funnet"
    private int myIndex = -1;
    private String label = null;
    private KjoretoyTabbedPane tabsContainer = null;
    private AbstractMipssEntityPanel<?> kjoretoyView = null;
//    private JScrollPane scroller = null;
    private GroupLayout layout = null;
    
    public TabBase(KjoretoyTabbedPane parent, String label) {
        this();
        this.tabsContainer = parent;
        this.label = label;
    }

    public TabBase() {
    	super();
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showContent() {
        selectMe();
    }
    
    private void selectMe () {
        if (tabsContainer != null) {
            tabsContainer.setSelectedComponent(this);
            tabsContainer.repaint();
        }
    }

    public KjoretoyTabbedPane getTabsContainer() {
        return tabsContainer;
    }
    
    /**
     * Setter indeksen til denne fanen i TabbedPane container.
     * Må kalles etter at fanen er lagt til container.
     */
    public void fetchTabIndex() {
        myIndex = tabsContainer.indexOfComponent(this);
    }
    
    /**
     * Setter fanens ettikett til den teksten i parameteret.
     * @param label er den kortte informative teksten som vises som tab'ens etikett.
     */
    public void setLabel(String label) {
        this.label = label;
        tabsContainer.setTitleAt(myIndex, label);
    }

    public String getLabel() {
        return label;
    }

    private void jbInit() throws Exception {
    	this.setLayout(new GridLayout(1, 1));
//    	scroller = new javax.swing.JScrollPane();
//    	scroller.setBorder(null);
//    	
//        layout = new javax.swing.GroupLayout(this);
//        this.setLayout(layout);
//        layout.setHorizontalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addComponent(scroller, javax.swing.GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE)
//        );
//        layout.setVerticalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addComponent(scroller, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
//        );
    }

    /** 
     * Setter et dedikert panel for visning av ett kjøretøy som eneste innhold i denne Tab'en.
     * 
     */
    public void setKjoretoyView(AbstractMipssEntityPanel<?> kjoretoyView) {
        add(kjoretoyView);
        this.kjoretoyView = kjoretoyView;
        kjoretoyView.addPropertyChangeListener("mipssEntity", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				boolean enableTabs = TabBase.this.kjoretoyView.getMipssEntity() != null;
				for(int i = 1; i < tabsContainer.getTabCount(); i++) {
					tabsContainer.setEnabledAt(i, enableTabs);
				}
			}
        });
    }

    /**
     * Henter panelet som er hovedinnholdet i en Tab, hvis Tab'en
     * inneholder et AbstractKjoretoyView. Dette er ikke tilfelle for Tab'en
     * for trefflisten og evt. andre.
     * @return AbstractKjoretoyView'et som denne Tab'en inneholder eller null.
     */
    public AbstractMipssEntityPanel<?> getKjoretoyView() {
        return kjoretoyView;
    }
    
    /**
     * Ignorerer kall hvis denne JPanel allerede har er aktivt visningsobkekt. 
     */
    @Override
    public Component add (Component c) {
        if (kjoretoyView != null) {
            logger.error("Kan ikke sette nytt innhold til en Tab som allered har et kjoretoyView. Ignoreres.");
            return null;
        }
//        else if(c instanceof JScrollPane) {
        	return super.add(c);
//        }
//        else {
//        	scroller.setViewportView(c);
//            return c;
//        }
    }

}
