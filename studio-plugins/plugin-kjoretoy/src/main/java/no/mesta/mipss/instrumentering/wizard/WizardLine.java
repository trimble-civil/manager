/**
 * 
 */
package no.mesta.mipss.instrumentering.wizard;

import javax.swing.Action;

import no.mesta.mipss.instrumentering.component.OrdretypePanel;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.plugin.TabBase;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;

/**
 * Implementerer en produksjonslinje som binder de enkelte trinn i et samlebånd
 * slik at hvert trinn får tilgang til alle valg foretatt på de forrige.
 * 
 * @author jorge
 */
public class WizardLine implements InstrumentationController {
	// felles panel for alle trinn i wizard.
	private static OrdretypePanel ordretypePanel;
	
	private Kjoretoy kjoretoy;
	private Driftkontrakt selectedKontrakt;
	private KontraktProsjekt selectedProsjektKontrakt;
	private Kjoretoyordretype selectedOrdretype;

	private final String loggedOnUserSign;
	
	public WizardLine(final String loggedOnUserSign) {
		this.loggedOnUserSign = loggedOnUserSign;
		ordretypePanel = new OrdretypePanel(this);
	}
	
	/**
	 * Returnerer fellesinstansen for ordretypepanelet.
	 * @return
	 */
	public OrdretypePanel getOrdretypePanel() {
		return ordretypePanel;
	}

	/**
	 * Returerer opprettet kjørerøy
	 * @return
	 */
	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}

	/**
	 * 
	 * @param kjoretoy
	 */
	public void setKjoretoy(Kjoretoy kjoretoy) {
		this.kjoretoy = kjoretoy;
	}

	/**
	 * @return selectedKontrakt
	 */
	public Driftkontrakt getSelectedKontrakt() {
		return selectedKontrakt;
	}

	/**
	 * @param selectedKontrakt the selectedKontrakt to set
	 */
	public void setSelectedKontrakt(Driftkontrakt selectedKontrakt) {
		this.selectedKontrakt = selectedKontrakt;
	}

	/**
	 * @return selectedProsjekt
	 */
	public KontraktProsjekt getSelectedProsjektKontrakt() {
		return selectedProsjektKontrakt;
	}

	/**
	 * @param selectedProsjektKontrakt the selectedProsjekt to set
	 */
	public void setSelectedProsjekt(KontraktProsjekt selectedProsjektKontrakt) {
		this.selectedProsjektKontrakt = selectedProsjektKontrakt;
	}

	/**
	 * @return selectedOrdretype
	 */
	public Kjoretoyordretype getSelectedOrdretype() {
		return selectedOrdretype;
	}

	/**
	 * @param selectedOrdretype the selectedOrdretype to set
	 */
	public void setSelectedOrdretype(Kjoretoyordretype selectedOrdretype) {
		this.selectedOrdretype = selectedOrdretype;
	}

	@Override
	public void handleSelectedPanel(TabBase selectedTab) {
		
	}

	@Override
	public Object openDialogWindow(Class baseClass, long id,
			AbstractMipssEntityPanel dialogPanel,
			AbstractMipssEntityPanel parent) {
		return null;
	}

	@Override
	public void registerKjoretoyPanel(
			AbstractKjoretoyPanel abstractKjoretoyPanel) {
		
	}

	@Override
	public void setSelectedItem(Long itemId, boolean useInfoPanel, int index) {
		
	}

	@Override
	public Action getAvbrytKjoretoyAction() {
		return null;
	}

	@Override
	public Action getLagreKjoretoyAction() {
		return null;
	}

	@Override
	public void kjoretoyChanged() {
		
	}

	@Override
	public String getLoggedOnUser() {
		return loggedOnUserSign;
	}
}
