package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

public class KjoretoyLevKontraktViewPanel extends AbstractKjoretoyPanel implements CompositePanel {
	private MipssKjoretoy bean;
	private MaintenanceContract maintenanceContractBean;
	
	private MipssBeanTableModel<KontraktKjoretoy> model;
	
	private KjoretoyLevKontraktListPanel kjoretoyLevKontraktListPanel;
    private KjoretoyeierViewPanel kjoretoyeierViewPanel1;
    private KjoretoyKontakttypeViewPanel kjoretoyKontakttypeViewPanel1;

	private Window parent;
    private boolean loaded = false;
	
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JButton btnLagre = new JButton();
	private JButton btnAvbryt = new JButton();
	
	public KjoretoyLevKontraktViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, MipssKjoretoy bean, MaintenanceContract maintenanceContractBean, Window parent, Driftkontrakt initiellKontrakt, boolean begrensetTilgangEndring) {
		super(kjoretoy, controller);
		this.bean = bean;
		this.maintenanceContractBean = maintenanceContractBean;
		this.parent = parent;
		
		kjoretoyLevKontraktListPanel = new KjoretoyLevKontraktListPanel(controller, this, kjoretoy, bean, maintenanceContractBean, parent, begrensetTilgangEndring, initiellKontrakt);
        kjoretoyeierViewPanel1 = PanelFactory.createKjoretoyeierViewPanel(getInstrumentationController(), null, this, parent);
        kjoretoyKontakttypeViewPanel1 = PanelFactory.createKjoretoyKontakttypeViewPanel(getInstrumentationController(), null, parent, this);

		initButtons();
		initGui();
	}
	
	private void initButtons() {
		btnLagre.setAction(getInstrumentationController().getLagreKjoretoyAction());
		btnAvbryt.setAction(getInstrumentationController().getAvbrytKjoretoyAction());
	}
	
	private void initGui() {
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(kjoretoyLevKontraktListPanel, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(kjoretoyeierViewPanel1, 		new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(kjoretoyKontakttypeViewPanel1, 	new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(pnlButtons, 					new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
	@Override
	public void enableUpdating(boolean canUpdate) {
	}

	@Override
	protected void initComponentsPresentation() {
		
	}

	@Override
	protected void loadFromEntity() {
		loaded = false;
		
		kjoretoyLevKontraktListPanel.setMipssEntity(getMipssEntity());
		kjoretoyLevKontraktListPanel.loadFromEntity();
		
		enableSaveButton(false);
        loaded = true;
	}


	@Override
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy)bean;
	}

	@Override
	public void enableSaveButton(boolean enable) {
		btnLagre.setEnabled(enable);
		btnAvbryt.setEnabled(enable);
		
		if(enable) {
			kjoretoyChanged();
		}
	}

}
