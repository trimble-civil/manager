package no.mesta.mipss.instrumentering.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.instrumentering.wizard.WizardLine;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;


public class OrdretypePanel extends JPanel implements ActionListener {
	private Logger logger = LoggerFactory.getLogger(OrdretypePanel.class);
	private ButtonGroup buttons = null;
	private WizardLine wizardLine = null;
	
	public OrdretypePanel(WizardLine wizardLine) {
		super();
		this.wizardLine = wizardLine;
		
		List<Kjoretoyordretype> typer = initOrdretyper();
		initComponents(typer);
	}
	
	/**
	 * Henter leisten med ordretyper
	 * @return listen
	 */
	private List<Kjoretoyordretype> initOrdretyper() {
		List<Kjoretoyordretype> typer;
		try {
			typer = Utils.getMaintenanceContract().getKjoretoyordretypeValidList();
		}
		catch (Exception e) {
			logger.error("Kan ikke hente liste med kontrakter: " + e);
			Utils.showException("Instrumenteringsveileder kan ikke starte");
			e.printStackTrace();
			return null;
		}
		
		return typer;
	}

	private void initComponents(List<Kjoretoyordretype> typer) {
		if(typer == null) {
			return;
		}
		setLayout(new java.awt.GridLayout());
		buttons = new ButtonGroup();
		
		for(Kjoretoyordretype t : typer) {
			OrderetypeButton button = new OrderetypeButton(t);
			add(button);
			buttons.add(button);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		OrderetypeButton button = (OrderetypeButton) e.getSource();
		wizardLine.setSelectedOrdretype(button.getOrdretype());
	}
	
	/**
	 * En JRadioButton med referanse til sin Kjoretoyordretype.
	 * @author jorge
	 *
	 */
	private class OrderetypeButton extends JRadioButton {
		private Kjoretoyordretype ordretype = null;
		
		private OrderetypeButton(Kjoretoyordretype ordretype) {
			super();
			this.ordretype = ordretype;
			super.setText(ordretype.getNavn());
			super.setName(ordretype.getNavn());
		}
		
		private Kjoretoyordretype getOrdretype() {
			return ordretype;
		}
	}
}
