package no.mesta.mipss.kjoretoy.control;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyordreListView;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.plugin.MainInstrumentationPanel;
import no.mesta.mipss.kjoretoy.plugin.TabBase;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller klasse for driftskontrakter.
 * @author jorge
 */
public class DriftkontraktListControl {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    private JComboBox comboBox;
    private TabBase workingTab;
    private String label;
    
    public DriftkontraktListControl(TabBase workingTab) {
        this.workingTab = workingTab;
        label = "Velg kontrakt:";
    }
    
    public JComboBox getDriftkontraktComboBox () {
        List kontrakter = null;
        MipssComboBoxModel<Driftkontrakt> comboBoxModel;
        if (comboBox == null) {
            // Hent listen fra DB
            MipssKjoretoy bean = KjoretoyUtils.getKjoretoyBean();
            try {
                kontrakter = MainInstrumentationPanel.getMaintenanceContractBean().getDriftkontraktList();
            }
            catch (Exception e) {
                logger.error("Kan ikke hente kontraktlisten.", e);
                //TODO: vis et dialogvindu med feilmeldingen 
            }
            
            // sett opp comboboxen
            /* ikke ferdig ennå
            MipssComboBox<Driftkontrakt> mipsCombo = new MipssComboBox<Driftkontrakt>(kontrakter, null);
            comboBox = mipsCombo.initComboBox(new JComboBox());
            */
            comboBox.setPreferredSize(new Dimension(200, 21));
            comboBox.setAutoscrolls(true);
            
            comboBox.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JComboBox source = (JComboBox)e.getSource();
                        Driftkontrakt selection = (Driftkontrakt) source.getSelectedItem();
                        
                        // opprett visningspanel
                        KjoretoyordreListView view = PanelFactory.createKjoretoyordreListView(selection);
                        
                        // sett fanen i TabbedPane.
                        workingTab.removeAll();
                        //workingTab.add(view);
                        workingTab.showContent();
                        
                        // endre etiketten
                        //workingTab.setLabel(selection.getRelevantNavn());
                    }
                });
        }
        return comboBox;
    }

    public String getLabel() {
        return label;
    }
}
