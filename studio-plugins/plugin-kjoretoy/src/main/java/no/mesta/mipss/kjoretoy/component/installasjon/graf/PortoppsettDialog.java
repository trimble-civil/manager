package no.mesta.mipss.kjoretoy.component.installasjon.graf;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

@SuppressWarnings("serial")
public class PortoppsettDialog extends JDialog {
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JButton btnLukk = new JButton();
	private JFreeChart portoppsettChart;
	
	public PortoppsettDialog(JDialog parentDialog, JFreeChart portoppsettChart, Kjoretoy kjoretoy, Installasjon installasjon) {
		super(parentDialog, KjoretoyUtils.getPropertyString("portoppsettGraf.tittel") + kjoretoy.getTextForGUI(), false);
		this.portoppsettChart = portoppsettChart;
		
		initGui();
	}
	
	private void initGui() {
		btnLukk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnLukk.setText(KjoretoyUtils.getPropertyString("portoppsettGraf.lukkKnapp"));
		
		ChartPanel chartPanel = new ChartPanel(portoppsettChart);
        chartPanel.setPreferredSize(new Dimension(600, 400));
        
        chartPanel.setBorder(BorderFactory.createEtchedBorder());
        
        pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnLukk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		pnlMain.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
		
		setContentPane(pnlMain);
		this.pack();
	}
	
	public void visGraf() {
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
