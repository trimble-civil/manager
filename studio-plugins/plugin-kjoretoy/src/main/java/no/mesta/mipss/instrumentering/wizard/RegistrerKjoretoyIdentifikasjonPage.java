package no.mesta.mipss.instrumentering.wizard;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.instrumentering.component.KjoretoyIdentificationPanel;
import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.kjoretoy.KjoretoyQueryFilter;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

public class RegistrerKjoretoyIdentifikasjonPage extends WizardPage {
    private KjoretoyIdentificationPanel kjoretoyIdentificationPanel;
	
	private JPanel pnlMain = new JPanel();
	private JPanel pnlIdent = new JPanel();
	private JLabel lblMaskinnummer = new JLabel();
	private JFormattedTextField txtMaskinnummer = new JFormattedTextField();
	private JLabel lblRegnr = new JLabel();
	private JFormattedTextField txtRegnr = new JFormattedTextField();
	private JLabel lblKjoretoyId = new JLabel();
	private JFormattedTextField txtKjoretoyId = new JFormattedTextField();
	private JPanel pnlKosenavn = new JPanel();
	private JLabel lblGuiNavn = new JLabel();
	private JFormattedTextField txtGuiNavn = new JFormattedTextField();
	private JRadioButton rbtnMaskinNrEllerRegNr = new JRadioButton();
	private JRadioButton rbtnKjoretoyId = new JRadioButton();
	private ButtonGroup buttonGroup = new ButtonGroup();
	private JPanel pnlBeskrivelse = new JPanel();
	private JScrollPane scrlPaneBeskrivelse = new JScrollPane();
	private JTextArea txtAreaBeskrivelse = new JTextArea();
	
	private BindingGroup bindingGroup;
	private ResourceBundle bundle;
	private WizardLine wizardLine;
	private Kjoretoy eksisterendeKjoretoy;
	private boolean begrensetTilgangEndring;
	
	public RegistrerKjoretoyIdentifikasjonPage(WizardLine wizardLine, String stepId, String stepDescription, boolean begrensetTilgangEndring) {
		super(stepId, stepDescription, false);
		this.wizardLine = wizardLine;
		this.begrensetTilgangEndring = begrensetTilgangEndring;
		
		bindingGroup = new BindingGroup();
		
		bundle = Utils.getBundle();
		
		buttonGroup.add(rbtnMaskinNrEllerRegNr);
		buttonGroup.add(rbtnKjoretoyId);
		
    	kjoretoyIdentificationPanel = new KjoretoyIdentificationPanel(wizardLine.getKjoretoy(), begrensetTilgangEndring);
		
		initButtons();
		initTextFields();
		initGui();
		
		initFromEntity(wizardLine.getKjoretoy());
		updateTxtVisibility();
		
		if(begrensetTilgangEndring) {
			String eksterntNavn = wizardLine.getKjoretoy().getEksterntNavn();
			if (eksterntNavn!=null&&eksterntNavn.startsWith("NY_BIL")){
				txtMaskinnummer.setEditable(true);
				txtRegnr.setEditable(true);
				txtKjoretoyId.setEditable(true);
				rbtnMaskinNrEllerRegNr.setEnabled(true);
				rbtnKjoretoyId.setEnabled(true);
			}else{
				txtMaskinnummer.setEditable(false);
				txtRegnr.setEditable(false);
				txtKjoretoyId.setEditable(false);
				rbtnMaskinNrEllerRegNr.setEnabled(false);
				rbtnKjoretoyId.setEnabled(false);
			}
		}
	}
	
	private void initFromEntity(Kjoretoy kjoretoy) {
		if(kjoretoy != null) {
			if(kjoretoy.getMaskinnummer() != null || (kjoretoy.getRegnr() != null && kjoretoy.getRegnr().length() > 0)) {
				rbtnMaskinNrEllerRegNr.setSelected(true);
			} else if(kjoretoy.getEksterntNavn() != null && kjoretoy.getEksterntNavn().length() > 0) {
				rbtnKjoretoyId.setSelected(true);
			}
		}
	}

	@Override
	protected void renderingPage() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				updateNextStatus();
			}
		});
	}
	
	private void updateNextStatus() {
		if(!wizardLine.getKjoretoy().isComplete()) {
			setProblem(bundle.getString("wizard.manglerIdentifikasjon"));
		} else {
			setProblem(null);
			setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
		}
    }
	
	@Override
	public WizardPanelNavResult allowNext(String stepName, Map settings, Wizard wizard) {
		//Sjekker om denne bilen finnes fra før
		KjoretoyQueryFilter kjoretoyQueryFilter = new KjoretoyQueryFilter();
		kjoretoyQueryFilter.setEksterntNavn(wizardLine.getKjoretoy().getEksterntNavn());
		kjoretoyQueryFilter.setRegNr(wizardLine.getKjoretoy().getRegnr());
		List<Kjoretoy> list = KjoretoyUtils.getKjoretoyBean().getKjoretoyList(kjoretoyQueryFilter);
		
		if(list.size() > 0) {
			if(wizardLine.getSelectedKontrakt() != null) {
				//Legger inn knytning mot kontrakten
				Kjoretoy kjoretoy = list.get(0);
				boolean ikkeTilknyttetFraFoer = true;
				for(KontraktKjoretoy kontraktKjoretoy : kjoretoy.getKontraktKjoretoyList()) {
					if(kontraktKjoretoy.getKontraktId().equals(wizardLine.getSelectedKontrakt().getId())) {
						ikkeTilknyttetFraFoer = false;
					}
				}
				
				if(ikkeTilknyttetFraFoer) {
					KontraktKjoretoy kk = new KontraktKjoretoy();
					kk.setDriftkontrakt(wizardLine.getSelectedKontrakt());
					kk.setKjoretoy(kjoretoy);
					kk.setAnsvarligFlagg(Long.valueOf(0));
					kjoretoy.getKontraktKjoretoyList().add(kk);
					
					//Viser melding om at eksisterende kjøretøy er funnet
					JOptionPane.showMessageDialog(this, bundle.getString("wizard.fantEksisterendeKjoretoy"));
					
					//Legger det eksisterende kjøretøyet inn i wizardLine
					eksisterendeKjoretoy = kjoretoy;
					
					//Enabler finish
					setForwardNavigationMode(WizardController.MODE_CAN_FINISH);
				} else {
					//Viser melding om at eksisterende kjøretøy er funnet
					JOptionPane.showMessageDialog(this, bundle.getString("wizard.fantEksisterendeKjoretoyPaaKontrakt"));
				}
			} else {
				//Eksisterende kjøretøy er funnet
				JOptionPane.showMessageDialog(this, bundle.getString("wizard.fantEksisterendeKjoretoyGlobal"));
			}
			
			//Returnerer 
			return WizardPanelNavResult.REMAIN_ON_PAGE;
		}
		
		return super.allowNext(stepName, settings, wizard);
	}
	
	@Override
	public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
		wizardLine.setKjoretoy(eksisterendeKjoretoy);
		return super.allowFinish(stepName, settings, wizard);
	}
	
	@SuppressWarnings("unchecked")
	private void initTextFields() {
		txtMaskinnummer.setDocument(new DocumentFilter(8));
		txtRegnr.setDocument(new DocumentFilter(10, null, true));
		txtKjoretoyId.setDocument(new DocumentFilter(20, null, false));
		txtGuiNavn.setDocument(new DocumentFilter(30, null, false));
		txtAreaBeskrivelse.setDocument(new DocumentFilter(4000, null, false));
		
		bindingGroup.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, wizardLine.getKjoretoy(), BeanProperty.create("regnrValid"), txtRegnr, BeanProperty.create("text"), null));
		bindingGroup.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, wizardLine.getKjoretoy(), BeanProperty.create("nummerEllerNavn"), txtKjoretoyId, BeanProperty.create("text")));
		bindingGroup.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, wizardLine.getKjoretoy(), BeanProperty.create("guitekst"), txtGuiNavn, BeanProperty.create("text")));
		bindingGroup.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, wizardLine.getKjoretoy(), BeanProperty.create("fritekst"), txtAreaBeskrivelse, BeanProperty.create("text")));
		
		Binding binding = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, wizardLine.getKjoretoy(), BeanProperty.create("maskinnummerValid"), txtMaskinnummer, BeanProperty.create("text"), null);
		binding.setConverter(new LongStringConverter());
		
		bindingGroup.addBinding(binding);
		
		bindingGroup.bind();
		
		KeyListener kl = new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						updateNextStatus();
					}
				});
			}
		};
		
		txtMaskinnummer.addKeyListener(kl);
		txtRegnr.addKeyListener(kl);
		txtKjoretoyId.addKeyListener(kl);
	}
	
	private void initButtons() {
		rbtnMaskinNrEllerRegNr.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateTxtVisibility();
			}
		});
		rbtnKjoretoyId.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateTxtVisibility();
			}
		});
	}
	
	private void updateTxtVisibility() {
		final boolean mnrOrRegnr = rbtnMaskinNrEllerRegNr.isSelected();
		final boolean kId = rbtnKjoretoyId.isSelected();
		
		txtMaskinnummer.setEditable(mnrOrRegnr);
		txtMaskinnummer.setFocusable(mnrOrRegnr);
		
		txtRegnr.setEditable(mnrOrRegnr);
		txtRegnr.setFocusable(mnrOrRegnr);
		
		txtKjoretoyId.setEditable(kId);
		txtKjoretoyId.setFocusable(kId);
	}
	
	private void initGui() {
		lblMaskinnummer.setText(bundle.getString("kjoretoy.maskinnummer.maicy"));
		lblRegnr.setText(bundle.getString("kjoretoy.regnum.skilt"));
		lblKjoretoyId.setText(bundle.getString("kjoretoy.id"));
		lblGuiNavn.setText(bundle.getString("kjoretoy.kortnavn"));
		
		rbtnMaskinNrEllerRegNr.setText(bundle.getString("kjoretoy.radioButtonMaskinEllerReg"));
		rbtnKjoretoyId.setText(bundle.getString("kjoretoy.radioButtonKjoretoyId"));
		
		txtMaskinnummer.setPreferredSize(new Dimension(100, 19));
		txtMaskinnummer.setMinimumSize(new Dimension(100, 19));
		txtRegnr.setPreferredSize(new Dimension(100, 19));
		txtRegnr.setMinimumSize(new Dimension(100, 19));
		txtKjoretoyId.setPreferredSize(new Dimension(100, 19));
		txtKjoretoyId.setMinimumSize(new Dimension(100, 19));
		txtGuiNavn.setPreferredSize(new Dimension(150, 19));
		txtGuiNavn.setMinimumSize(new Dimension(150, 19));
		
		pnlIdent.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoy.identBorder")));
		pnlKosenavn.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoy.kosenavnBorder")));
		pnlBeskrivelse.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoy.beskrivelseBorder")));
		
		scrlPaneBeskrivelse.setViewportView(txtAreaBeskrivelse);
		txtAreaBeskrivelse.setLineWrap(true);
		txtAreaBeskrivelse.setWrapStyleWord(true);
		
		pnlIdent.setLayout(new GridBagLayout());
		pnlIdent.add(rbtnMaskinNrEllerRegNr, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlIdent.add(lblMaskinnummer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 30, 5, 5), 0, 0));
		pnlIdent.add(txtMaskinnummer, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlIdent.add(lblRegnr, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 30, 5, 5), 0, 0));
		pnlIdent.add(txtRegnr, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 5), 0, 0));
		pnlIdent.add(rbtnKjoretoyId, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlIdent.add(lblKjoretoyId, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 30, 5, 5), 0, 0));
		pnlIdent.add(txtKjoretoyId, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlKosenavn.setLayout(new GridBagLayout());
		pnlKosenavn.add(lblGuiNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKosenavn.add(txtGuiNavn, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlBeskrivelse.setLayout(new GridBagLayout());
		pnlBeskrivelse.add(scrlPaneBeskrivelse, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(pnlIdent, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(pnlKosenavn, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(pnlBeskrivelse, new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(kjoretoyIdentificationPanel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
}
