package no.mesta.mipss.kjoretoy.tablebrowser;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JTable;

import no.mesta.mipss.persistence.datafangsutstyr.DfuInstallasjonV;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrView;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EditableMippsTableModel.MipssTableColumn;
import no.mesta.mipss.util.KjoretoyUtils;


public class BrowserUtils {
    private BrowserUtils() {
    }
    
    public static JTable initKjoretoyInstallasjonTable(JTable table) {
    	ResourceBundle bundle = KjoretoyUtils.getBundle();
        // init tabellkolonner
        List<MipssTableColumn> list = new ArrayList<MipssTableColumn>();
        list.add(new MipssTableColumn(bundle.getString("dfuList.kategori"),    "kategori", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.modell"),      "modell", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.serail"),      "serienummer", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.status"),      "currentStatus", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.simNummer"),   "simkortnummer", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.aktivFra"),    "aktivFraDato", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.aktivTil"),    "aktivTilDato", false));
        list.add(new MipssTableColumn(bundle.getString("dfuList.sisteSignal"), "sisteSignalDato", false));
        
        // sett opp tabellen
        EditableMippsTableModel<DfuInstallasjonV> model = new EditableMippsTableModel<DfuInstallasjonV>(DfuInstallasjonV.class, null, list);
        table.setModel(model);
        KjoretoyUtils.initDefaultTableSettings(table);
        return table;
    }
    
    /**
     * Setter opp kolonnenavn og modellen i den leverte tabellen.
     * Modellen er typedefinert for objekter av {@code KjoretoyUtstyr}.
     * @param table
     * @param bundle
     * @return den oppsatte tabellen
     */
    public static JTable initKjoretoyUtstyrTable(JTable table, boolean enkelVisning) {
    	ResourceBundle bundle = KjoretoyUtils.getBundle();
        // init tabellkolonner
        List<MipssTableColumn> list = new ArrayList<MipssTableColumn>();
        list.add(new MipssTableColumn(bundle.getString("utstyrList.gruppe"),    "utstyrgruppeNavn", false));
        list.add(new MipssTableColumn(bundle.getString("utstyrList.subgruppe"), "utstyrsubgruppeNavn", false));
        list.add(new MipssTableColumn(bundle.getString("utstyrList.modell"),    "utstyrmodellNavn", false));
        list.add(new MipssTableColumn(bundle.getString("utstyrList.serienr"),   "individSerienummer", false));
        if(!enkelVisning) {
        	list.add(new MipssTableColumn(bundle.getString("utstyrList.aktivPort"), "aktivPort", false));
        }
        list.add(new MipssTableColumn(bundle.getString("utstyrList.notat"),     "fritekst", false));
        
        EditableMippsTableModel<KjoretoyUtstyrView> model = new EditableMippsTableModel<KjoretoyUtstyrView>(KjoretoyUtstyrView.class, null, list);
        table.setModel(model);
        KjoretoyUtils.initDefaultTableSettings(table);
        
        return table;
    }
}
