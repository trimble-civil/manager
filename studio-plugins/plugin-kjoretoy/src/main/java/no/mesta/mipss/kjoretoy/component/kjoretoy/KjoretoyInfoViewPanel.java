package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.PlainDocument;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoylogg;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;

/**
 * Samlepanel for visning av tillegsinfo til et kjøretøy.
 * @author jorge
 */
public class KjoretoyInfoViewPanel extends AbstractKjoretoyPanel implements CompositePanel {
	private static final long serialVersionUID = 5246905440282971382L;
	private Logger logger = LoggerFactory.getLogger(KjoretoyInfoViewPanel.class);
	public final static int ALIGNED_BUTTONS_WIDTH = 150;
	public static final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy '-' HH:mm:ss");
	
	//private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private boolean hasUpdateAccess = false;
    private MipssKjoretoy bean;
    private BindingGroup binding;
    private ResourceBundle bundle;
    private boolean loaded = false;
	// Variables declaration - do not modify
    private JButton avbrytButton;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel pnlNotat;
    private JPanel pnlLogg;
    private JPanel pnlButtons;
    private JScrollPane scrlPaneNotat;
    private JScrollPane scrlPaneLogg;
    private KjoretoyBildeViewPanel kjoretoyBildeViewPanel;
    private KjoretoyDokViewPanel kjoretoyDokViewPanel1;
    private JButton lagreButton;
    private JLabel loggLabel;
    private JTextArea loggTextArea;
    private JLabel notatLabel;
    private JTextArea notatTextArea;
    private JButton nyLoggButton;
    
    private MipssPlugin parentPlugin;
	private final boolean iWizard;
    
    // End of variables declaration
    public KjoretoyInfoViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, boolean hasUpdateAccess, MipssKjoretoy bean, ResourceBundle bundle, MipssPlugin parentPlugin, boolean iWizard) {
        super(kjoretoy, controller);
        this.bean = bean;
		this.iWizard = iWizard;
        super.setBundle(bundle);
        this.bundle = bundle;
        this.parentPlugin = parentPlugin;
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        this.hasUpdateAccess = hasUpdateAccess;
        
        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void enableUpdating(boolean canUpdate) {
    }

    private void initBindings() {
    	if(binding != null) {
    		binding.unbind();
    	}
    	binding = new BindingGroup();
    	binding.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mipssEntity, BeanProperty.create("fritekst"), notatTextArea, BeanProperty.create("text"), null));
       	binding.bind();
    }
    
    private void initActions() {
    	lagreButton.setAction(getInstrumentationController().getLagreKjoretoyAction());
    	avbrytButton.setAction(getInstrumentationController().getAvbrytKjoretoyAction());
    	
    	nyLoggButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				KjoretoyloggPanel notat = new KjoretoyloggPanel();
				String ft = notat.registerInOwnDialog();
				
				if(ft != null) {
					Kjoretoylogg logg = new Kjoretoylogg();
					logg.setKjoretoy(mipssEntity);
					logg.setTekst(ft);
					logg.setOpprettetAv(parentPlugin.getLoader().getLoggedOnUserSign());
					logg.setOpprettetDato(Clock.now());
					mipssEntity.addKjoretoylogg(logg);
					enableSaveButton(true);
					showLogg();
				}
			}
    	});
    	notatTextArea.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {
				enableSaveButton(true);				
			}
    	});
    }
    
    protected void loadFromEntity() {
    	loaded = false;
        showLogg();
        
        initBindings();
        
        // vis bilder og dokumenter i egne kontrollere
        kjoretoyBildeViewPanel.setMipssEntity(mipssEntity);
        kjoretoyDokViewPanel1.setMipssEntity(mipssEntity);
        
        enableSaveButton(false);
        loaded = true;
    }

    private void showLogg() {
    	loggTextArea.setText("");
    	loggTextArea.append(concatLogg(mipssEntity.getKjoretoyloggList()));
    }
    
    private String concatLogg(List<Kjoretoylogg> loggs) {
        StringBuffer b = new StringBuffer();
        Collections.sort(loggs);
        
        for (Kjoretoylogg l : loggs) {
        	b.append(dateFormat.format(l.getOpprettetDato()));
        	if(l.getOpprettetAv() != null) {
        		b.append(". Skrevet av ").
        		append(l.getOpprettetAv());
        	}
        	b.append('\n').
            append(l.getTekst()).
            append('\n').
            append('\n');
        	
        }
        return b.toString();
    }
    
	@Override
	public void enableSaveButton(boolean enable) {
		lagreButton.setEnabled(enable);
		avbrytButton.setEnabled(enable);
		
		if(enable) {
			kjoretoyChanged();
		}
	}

    protected void initComponentsPresentation() {
        initComponents();
        KjoretoyUtils.setBold(notatLabel);
        KjoretoyUtils.setBold(loggLabel);
                
        loggTextArea.setEditable(false);
        loggTextArea.setBackground(Color.WHITE);
        
        notatTextArea.setDocument(new PlainDocument());
        notatTextArea.setFont(this.getFont());
        notatTextArea.setLineWrap(true);
        notatTextArea.setWrapStyleWord(true);
        
        // vis loggen
        loggTextArea.setDocument(new PlainDocument());
        loggTextArea.setFont(this.getFont());
        loggTextArea.setLineWrap(true);
        loggTextArea.setWrapStyleWord(true);
        
        lagreButton.setEnabled(false);
        initActions();
    }

    /**
     * Initialiserer controllere fro tom visning.
     * Limt inn fra NetBeans. Husk å initialisere MIPSS controllere med "this".
     */
    private void initComponents() {
    	JPanel pnlMain = new JPanel();
    	pnlButtons = new JPanel();
        pnlNotat = new JPanel();
        jLabel1 = new JLabel();
        scrlPaneNotat = new JScrollPane();
        notatTextArea = new JTextArea();
        notatLabel = new JLabel();
        pnlLogg = new JPanel();
        jLabel2 = new JLabel();
        loggLabel = new JLabel();
        nyLoggButton = new JButton();
        scrlPaneLogg = new JScrollPane();
        loggTextArea = new JTextArea();
        kjoretoyBildeViewPanel = PanelFactory.createKjoretoyBildeViewPanel(getInstrumentationController(), mipssEntity, this);
        kjoretoyDokViewPanel1 = PanelFactory.createKjoretoyDokViewPanel(getInstrumentationController(), mipssEntity, this);
        avbrytButton = new JButton();
        lagreButton = new JButton();

        notatTextArea.setColumns(20);
        notatTextArea.setRows(5);
        scrlPaneNotat.setViewportView(notatTextArea);

        notatLabel.setText(bundle.getString("kjoreotyInfo.notatLabel")); // NOI18N
        loggLabel.setText(bundle.getString("kjoretoyInfo.loggLabel")); // NOI18N

        nyLoggButton.setText(bundle.getString("kjoretoyInfo.nyloggButon")); // NOI18N

        nyLoggButton.setMinimumSize(new Dimension(ALIGNED_BUTTONS_WIDTH, 21));
        nyLoggButton.setPreferredSize(new Dimension(ALIGNED_BUTTONS_WIDTH, 21));
        
        loggTextArea.setColumns(20);
        loggTextArea.setRows(5);
        scrlPaneLogg.setViewportView(loggTextArea);
        
        avbrytButton.setText(bundle.getString("button.avbrytt")); // NOI18N
        lagreButton.setText(bundle.getString("button.lagre")); // NOI18N
        
        pnlNotat.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoreotyInfo.notatLabel")));
        pnlNotat.setLayout(new GridBagLayout());
        pnlNotat.add(scrlPaneNotat, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        
        pnlLogg.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoyInfo.loggLabel")));
        pnlLogg.setLayout(new GridBagLayout());
        pnlLogg.add(scrlPaneLogg, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlLogg.add(nyLoggButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        pnlButtons.setLayout(new GridBagLayout());
        pnlButtons.add(lagreButton, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButtons.add(avbrytButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        
        pnlMain.setLayout(new GridBagLayout());
        if(iWizard) {
        	pnlMain.add(pnlLogg, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(9, 0, 5, 11), 0, 0));
        	pnlMain.add(kjoretoyBildeViewPanel, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 11, 0, 5), 0, 0));
            pnlMain.add(kjoretoyDokViewPanel1, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 11), 0, 0));
        } else {
        	pnlMain.add(pnlNotat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(9, 11, 5, 5), 0, 0));
        	pnlMain.add(pnlLogg, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(9, 0, 5, 11), 0, 0));
        	pnlMain.add(kjoretoyBildeViewPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 11, 0, 5), 0, 0));
            pnlMain.add(kjoretoyDokViewPanel1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 11), 0, 0));
            pnlMain.add(pnlButtons, new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 11, 11), 0, 0));
        }
        if(!iWizard) {
        	
        }
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
