package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;
import java.awt.Container;

import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyOrdreListPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.netbeans.spi.wizard.WizardPage;

public class RegistrerOrdrePage extends WizardPage {
	private MipssPlugin mipssPlugin;
	private WizardLine wizardLine;
	private Container parent;
	private boolean enkelWizard;
	private boolean begrensetTilgangEndring;
	
	private KjoretoyOrdreListPanel kjoretoyOrdreViewPanel;
	
	public RegistrerOrdrePage(MipssPlugin mipssPlugin, WizardLine wizardLine, Container parent, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard, boolean begrensetTilgangEndring) {
		super(stepId, stepDescription, autoListen);
		this.mipssPlugin = mipssPlugin;
		this.wizardLine = wizardLine;
		this.parent = parent;
		this.enkelWizard = enkelWizard;
		this.begrensetTilgangEndring = begrensetTilgangEndring;
		
		initGui();
	}
	
	private void initGui() {
		this.setLayout(new BorderLayout());
	}
	
	@Override
	protected void renderingPage() {
		if(kjoretoyOrdreViewPanel == null) {
			kjoretoyOrdreViewPanel = new KjoretoyOrdreListPanel(wizardLine, null, wizardLine.getKjoretoy(), parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), mipssPlugin, begrensetTilgangEndring);
			this.add(kjoretoyOrdreViewPanel, BorderLayout.CENTER);
		}
	}
}
