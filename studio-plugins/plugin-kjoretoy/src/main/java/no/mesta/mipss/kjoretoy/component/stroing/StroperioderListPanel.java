package no.mesta.mipss.kjoretoy.component.stroing;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.utils.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.stroing.Stroperiode;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class StroperioderListPanel extends AbstractKjoretoyPanel implements CompositePanel {

	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneOrdreList = new JScrollPane();
	private JMipssBeanTable<Stroperiode> tblStroperiode;
	private MipssBeanTableModel<Stroperiode> tblModelStroperiode;
	private JButton btnEndre = new JButton();
	private JButton btnNy = new JButton();
	private JButton btnClose = new JButton();
	private JButton btnSlett = new JButton();
	
    private Container parent;
	private MipssKjoretoy mipssKjoretoy;
	private MaintenanceContract maintenanceContract;
	private MipssPlugin parentPlugin;
    private boolean loaded = false;
    private StroperioderPanel parentPanel;
    private BindingGroup bindingGroup;
	
	public StroperioderListPanel(InstrumentationController controller, StroperioderPanel parentPanel, Kjoretoy kjoretoy, Container parent, MipssKjoretoy mipssKjoretoy, MaintenanceContract maintenanceContract, MipssPlugin parentPlugin) {
		super(kjoretoy, controller);
		
		this.parentPanel = parentPanel;
		this.parent = parent;
		this.mipssKjoretoy = mipssKjoretoy;
		this.maintenanceContract = maintenanceContract;
		this.parentPlugin = parentPlugin;
		
		bindingGroup = new BindingGroup();
		
		initTable();
		initButtons();
		initGui();
		
		bindingGroup.bind();
	}

	private void initButtons() {
		btnNy.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.leggTil"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Stroperiode stroperiode = new Stroperiode();
				stroperiode.setKjoretoy(mipssEntity);
				stroperiode.setFraDato(CalendarUtil.getTomorrow(true));
				stroperiode.setOpprettetAv(parentPlugin.getLoader().getLoggedOnUserSign());
				stroperiode.setOpprettetDato(Clock.now());

				StroperiodeDialog stroperiodeDialog;
				if (parent instanceof JFrame) {
					stroperiodeDialog = new StroperiodeDialog((JFrame)parent, stroperiode, mipssEntity.getStroperiodeList(), true, parentPlugin);
				} else {
					stroperiodeDialog = new StroperiodeDialog((JDialog)parent, stroperiode, mipssEntity.getStroperiodeList(), true, parentPlugin);
				}

				Stroperiode stroperiodeNy = stroperiodeDialog.showDialog();
				if (stroperiodeDialog.isStroperiodeOk()) {
					stroperiode.setFraDato(stroperiodeNy.getFraDato());
					stroperiode.merge(stroperiodeNy);
					List<Stroperiode> list = mipssEntity.getStroperiodeList();
					list.add(stroperiode);
					tblModelStroperiode.fireTableModelEvent(0, list.size(), TableModelEvent.INSERT);
					enableSaveButton(true);
				}
			}
		});
		
		btnEndre.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.endre"), IconResources.DETALJER_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (UserUtils.isAdmin(parentPlugin.getLoader().getLoggedOnUser(true)) ||
						UserUtils.isBrukerInRolle(parentPlugin.getLoader().getLoggedOnUser(true), "Superbruker")) {
					int selectedRow = tblStroperiode.getSelectedRow();

					if (selectedRow >= 0 && selectedRow < tblStroperiode.getRowCount()) {
						Stroperiode stroperiode = tblModelStroperiode.get(tblStroperiode.convertRowIndexToModel(selectedRow));
						endreStroperiode(stroperiode);
						tblModelStroperiode.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
					}
				} else {
					JOptionPane.showMessageDialog(
							StroperioderListPanel.this,
							KjoretoyUtils.getPropertyString("changeGritPeriod.warning"),
							KjoretoyUtils.getPropertyString("changeGritPeriod.warning.title"),
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		btnClose.setAction(getCloseGritPeriodAction());
		bindingGroup.addBinding(BindingHelper.createbinding(tblStroperiode, "${noOfSelectedRows == 1}", btnClose, "enabled"));

		if (UserUtils.isAdmin(parentPlugin.getLoader().getLoggedOnUser(true))){
			btnSlett.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.slett"), IconResources.REMOVE_ITEM_ICON) {
				@Override
				public void actionPerformed(ActionEvent e) {
					int r = JOptionPane.showConfirmDialog(
							StroperioderListPanel.this,
							KjoretoyUtils.getPropertyString("slettStroperioder.warning"),
							KjoretoyUtils.getPropertyString("slettStroperioder.warning.title"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);

					if (r == JOptionPane.YES_OPTION) {
						int selectedRows[] = tblStroperiode.getSelectedRows();
						List<Stroperiode> removed = new ArrayList<>();

						for (int i=0; i<selectedRows.length; i++) {
							int selectedRow = selectedRows[i];

							if(selectedRow >= 0 && selectedRow < tblStroperiode.getRowCount()) {
								Stroperiode stroperiode = tblModelStroperiode.get(tblStroperiode.convertRowIndexToModel(selectedRow));
								removed.add(stroperiode);
							}
						}
						
						if (removed.size() > 0) {
							mipssEntity.getStroperiodeList().removeAll(removed);
							tblModelStroperiode.fireTableModelEvent(0, tblStroperiode.getRowCount(), TableModelEvent.UPDATE);
							enableSaveButton(true);
						}
					}
				}
			});
			bindingGroup.addBinding(BindingHelper.createbinding(tblStroperiode, "${noOfSelectedRows > 0}", btnSlett, "enabled"));
		}
		//ikke admin, bruker kan ikke slette strøperioder. 
		else {
			btnSlett.setText(KjoretoyUtils.getPropertyString("button.slett"));
			btnSlett.setIcon(IconResources.REMOVE_ITEM_ICON);
			btnSlett.setEnabled(false);
			btnSlett.setToolTipText(Resources.getResource("button.slett.tooltip", EpostUtils.getSupportEpostAdresse()));
		}
		
		bindingGroup.addBinding(BindingHelper.createbinding(tblStroperiode, "${noOfSelectedRows == 1}", btnEndre, "enabled"));
	}

	private void initTable() {
		tblModelStroperiode = new MipssBeanTableModel<>("stroperiodeList", Stroperiode.class, "stroprodukt", "strobreddeCm", "gramPrKvm", "fraDato", "tilDato");
		BindingHelper.createbinding(this, "mipssEntity", tblModelStroperiode, "sourceObject", BindingHelper.READ).bind();
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Stroperiode.class, tblModelStroperiode.getColumns());
		tblStroperiode = new JMipssBeanTable<>(tblModelStroperiode, colModel);
		tblStroperiode.setFillsViewportWidth(true);
		tblStroperiode.setDoWidthHacks(false);
		tblStroperiode.setDefaultRenderer(Stroprodukt.class, new RenderableMipssEntityTableCellRenderer<Stroprodukt>());
		tblStroperiode.setDefaultRenderer(Date.class, new DateTimeRenderer());
		tblStroperiode.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					int selectedRow = tblStroperiode.getSelectedRow();
					if(selectedRow >= 0 && selectedRow < tblStroperiode.getRowCount()) {
						Stroperiode stroperiode = tblModelStroperiode.get(tblStroperiode.convertRowIndexToModel(selectedRow));
						endreStroperiode(stroperiode);
						tblModelStroperiode.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
					}
				}
			}
		});
	}
	
	private void initGui() {
		scrlPaneOrdreList.setViewportView(tblStroperiode);
		
		pnlMain.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("stroing.border")));
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneOrdreList, new GridBagConstraints(0, 0, 1, 4, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlMain.add(btnNy, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(btnEndre, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(btnClose, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(btnSlett, new GridBagConstraints(1, 3, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	private void endreStroperiode(Stroperiode stroperiode) {
		if(stroperiode != null) {
			StroperiodeDialog stroperiodeDialog = null; 
			if(parent instanceof JFrame) {
				stroperiodeDialog = new StroperiodeDialog((JFrame)parent, stroperiode, mipssEntity.getStroperiodeList(), false, parentPlugin);
			} else {
				stroperiodeDialog = new StroperiodeDialog((JDialog)parent, stroperiode, mipssEntity.getStroperiodeList(), false, parentPlugin);
			}
			
			Stroperiode stroperiodeNy = stroperiodeDialog.showDialog();
			if(stroperiodeDialog.isStroperiodeOk()) {
				stroperiode.setGramPrKvm(stroperiodeNy.getGramPrKvm());
				stroperiode.setStrobreddeCm(stroperiodeNy.getStrobreddeCm());
				stroperiode.setStroprodukt(stroperiodeNy.getStroprodukt());
				stroperiode.setFraDato(stroperiodeNy.getFraDato());
				stroperiode.setTilDato(stroperiodeNy.getTilDato());
				
				stroperiode.setEndretAv(parentPlugin.getLoader().getLoggedOnUserSign());
				stroperiode.setEndretDato(Clock.now());
				enableSaveButton(true);
			}
		}
	}
	
	@Override
	public void enableUpdating(boolean canUpdate) {
	}

	@Override
	protected void initComponentsPresentation() {
	}

	@Override
	protected void loadFromEntity() {
		if(mipssEntity != null) {
			List<Stroperiode> ordreList = mipssEntity.getStroperiodeList();
			tblModelStroperiode.setEntities(ordreList);
		} else {
			tblModelStroperiode.setEntities(new ArrayList<>());
		}
		enableSaveButton(false);
        loaded = true;
	}

	@Override
	public void setSessionBean(Object bean) {
		this.mipssKjoretoy = (MipssKjoretoy)bean;
	}

	@Override
	public void enableSaveButton(boolean enable) {
		if(parentPanel != null) {
			parentPanel.enableSaveButton(enable);
		}
		
		if(enable) {
			kjoretoyChanged();
		}
	}

	private Action getCloseGritPeriodAction() {
		return new AbstractAction(KjoretoyUtils.getPropertyString("button.close"), IconResources.LOCK_ICON_16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblStroperiode.getSelectedRow();

				if (selectedRow >= 0 && selectedRow < tblStroperiode.getRowCount()) {
					Stroperiode gritPeriod = tblModelStroperiode.get(tblStroperiode.convertRowIndexToModel(selectedRow));

					if (gritPeriod.getTilDato() == null) {
						int closePeriod = JOptionPane.showConfirmDialog(
								StroperioderListPanel.this,
								KjoretoyUtils.getPropertyString("closeGritPeriod.confirmation"),
								KjoretoyUtils.getPropertyString("closeGritPeriod.title"),
								JOptionPane.YES_NO_OPTION);

						if (closePeriod == JOptionPane.YES_OPTION) {
                            gritPeriod.setTilDato(Date.from(ZonedDateTime.now().plusMinutes(1).toInstant()));
                            gritPeriod.setEndretAv(parentPlugin.getLoader().getLoggedOnUserSign());
                            gritPeriod.setEndretDato(Clock.now());

                            tblModelStroperiode.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
                            enableSaveButton(true);
						}
					} else {
						JOptionPane.showMessageDialog(
								StroperioderListPanel.this,
								KjoretoyUtils.getPropertyString("closeGritPeriod.alreadyClosed"),
                                KjoretoyUtils.getPropertyString("closeGritPeriod.title"),
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
		};
	}

}