package no.mesta.mipss.kjoretoy.component.installasjon.graf;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.util.KjoretoyUtils;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedRangeCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.GanttRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;

public class PortoppsettGrafFactory {
	public static JFreeChart createPortoppsettChart(Installasjon installasjon, List<KjoretoyUtstyr> kjoretoyutstyrList) {
		DateAxis dateAxis = new DateAxis();
		CategoryItemRenderer renderer = new GanttRenderer();
		CombinedRangeCategoryPlot cPlot = new CombinedRangeCategoryPlot(dateAxis);
		cPlot.setOrientation(PlotOrientation.HORIZONTAL);
		cPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		
		Date infDate = getInfinityDate(kjoretoyutstyrList);
		Date maxDate = Clock.now();
		Date minDate = Clock.now();
		
		if(installasjon.getDfumodell() != null) {
			for(Ioliste ioliste : installasjon.getDfumodell().getIolisteList()) {
				List<UtstyrGrafPeriode> periodeList = buildPeriodeList(ioliste, kjoretoyutstyrList);
				if(periodeList.size() > 0) {
					maxDate = getMaxDate(periodeList, maxDate, infDate);
					minDate = getMinDate(periodeList, minDate);
					
					CategoryPlot categoryPlot = getPlotForPort(ioliste, periodeList, dateAxis, renderer, infDate);
					if(categoryPlot != null) {
						categoryPlot.setOrientation(PlotOrientation.HORIZONTAL);
						cPlot.add(categoryPlot, getUtstyrCount(periodeList));
					}
				}
			}
		}
		
		minDate = new Date(minDate.getTime() - ((maxDate.getTime() - minDate.getTime()) * 5L) / 100L);
		
		dateAxis.setMinimumDate(minDate);
		dateAxis.setMaximumDate(maxDate);
		
		renderer.setSeriesPaint(0, Color.GREEN);
		renderer.setSeriesPaint(1, Color.RED);
		
		JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, cPlot, false);
		
		if(cPlot.getSubplots().size() > 0) {
			LegendTitle legendTitle = new LegendTitle((LegendItemSource)cPlot.getSubplots().get(0));
			BlockContainer container = legendTitle.getItemContainer();
	        container.setFrame(new BlockBorder(1.0D, 1.0D, 1.0D, 1.0D));
	        container.setPadding(2D, 10D, 5D, 2D);
	        legendTitle.setWrapper(container);
	        legendTitle.setPosition(RectangleEdge.BOTTOM);
	        legendTitle.setHorizontalAlignment(HorizontalAlignment.CENTER);
	        legendTitle.setBackgroundPaint(Color.WHITE);
			chart.addLegend(legendTitle);
		}

		return chart;
	}
	
	private static Date getInfinityDate(List<KjoretoyUtstyr> kjoretoyutstyrList) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(Clock.now());
		cal.add(Calendar.YEAR, -100);
		Date date = cal.getTime();
		
		for(KjoretoyUtstyr kjoretoyUtstyr : kjoretoyutstyrList) {
			for(KjoretoyUtstyrIo kjoretoyUtstyrIo : kjoretoyUtstyr.getKjoretoyUtstyrIoList()) {
				if(kjoretoyUtstyrIo.getTilDato() != null && kjoretoyUtstyrIo.getTilDato().after(date)) {
					date = kjoretoyUtstyrIo.getTilDato();
				} else if(kjoretoyUtstyrIo.getFraDato().after(date)) {
					date = kjoretoyUtstyrIo.getFraDato();
				}
			}
		}
		
		cal.setTime(date);
		cal.add(Calendar.YEAR, 1);
		
		return cal.getTime();
	}
	
	private static Date getMaxDate(List<UtstyrGrafPeriode> periodeList, Date oldMax, Date infDate) {
		for(UtstyrGrafPeriode utstyrGrafPeriode : periodeList) {
			if(utstyrGrafPeriode.getSluttDato() == null) {
				return infDate;
			} else if(utstyrGrafPeriode.getSluttDato().after(oldMax)) {
				oldMax = utstyrGrafPeriode.getSluttDato();
			}
		}
		
		return oldMax;
	}
	
	private static Date getMinDate(List<UtstyrGrafPeriode> periodeList, Date oldMin) {
		for(UtstyrGrafPeriode utstyrGrafPeriode : periodeList) {
			if(utstyrGrafPeriode.getStartDato().before(oldMin)) {
				oldMin = utstyrGrafPeriode.getStartDato();
			}
		}
		
		return oldMin;
	}
	
	private static int getUtstyrCount(List<UtstyrGrafPeriode> periodeList) {
		Set<String> set = new HashSet<String>();
		
		for(UtstyrGrafPeriode utstyrGrafPeriode : periodeList) {
			set.add(utstyrGrafPeriode.getUtstyrNavn());
		}
		
		return set.size();
	}
	
	private static CategoryPlot getPlotForPort(Ioliste ioliste, List<UtstyrGrafPeriode> periodeList, DateAxis valueAxis, CategoryItemRenderer categoryItemRenderer, Date infinityDate) {
		IntervalCategoryDataset dataset = createDataset(periodeList, infinityDate);
		CategoryPlot categoryPlot = new CategoryPlot(dataset, new CategoryAxis(ioliste.getNavn1()), valueAxis, categoryItemRenderer);
		
		return categoryPlot;
	}
	
	private static IntervalCategoryDataset createDataset(List<UtstyrGrafPeriode> periodeList, Date infinityDate) {
		TaskSeries taskSeriesAktiv = new TaskSeries(KjoretoyUtils.getPropertyString("portoppsettGraf.aktivSerie"));
		TaskSeries taskSeriesInnaktiv = new TaskSeries(KjoretoyUtils.getPropertyString("portoppsettGraf.innaktivSerie"));
		
		Map<Integer, List<UtstyrGrafPeriode>> map = new HashMap<Integer, List<UtstyrGrafPeriode>>();
		for(UtstyrGrafPeriode utstyrGrafPeriode : periodeList) {
			Integer hash = new HashCodeBuilder().append(utstyrGrafPeriode.getUtstyrNavn()).append(utstyrGrafPeriode.getAktiv()).toHashCode();
			if(map.containsKey(hash)) {
				map.get(hash).add(utstyrGrafPeriode);
			} else {
				List<UtstyrGrafPeriode> list = new ArrayList<UtstyrGrafPeriode>();
				list.add(utstyrGrafPeriode);
				map.put(hash, list);
			}
		}
		
		for(List<UtstyrGrafPeriode> list : map.values()) {
			if(list.size() == 1) {
				UtstyrGrafPeriode utstyrGrafPeriode = list.get(0);
				Date startDate = utstyrGrafPeriode.getStartDato();
				Date sluttDate = utstyrGrafPeriode.getSluttDato() != null ? utstyrGrafPeriode.getSluttDato() : infinityDate;
				Task task = new Task(utstyrGrafPeriode.getUtstyrNavn(), new SimpleTimePeriod(startDate, sluttDate));
	
				if(utstyrGrafPeriode.getAktiv()) {
					taskSeriesAktiv.add(task);
				} else {
					taskSeriesInnaktiv.add(task);
				}
			} else {
				UtstyrGrafPeriode utstyrGrafPeriodeTmp = list.get(0);
				Date superStartDate = null;
				Date superSluttDate = null;
				Task superTask = new Task(utstyrGrafPeriodeTmp.getUtstyrNavn(), new SimpleTimePeriod(new Date(), new Date()));
				for(UtstyrGrafPeriode utstyrGrafPeriode : list) {
					Date startDate = utstyrGrafPeriode.getStartDato();
					Date sluttDate = utstyrGrafPeriode.getSluttDato() != null ? utstyrGrafPeriode.getSluttDato() : infinityDate;
					Task task = new Task(utstyrGrafPeriode.getUtstyrNavn(), new SimpleTimePeriod(startDate, sluttDate));
					superTask.addSubtask(task);
					if(superStartDate == null || superStartDate.after(startDate)) {
						superStartDate = startDate;
					}
					if(superSluttDate == null || superSluttDate.after(sluttDate)) {
						superSluttDate = sluttDate;
					}
				}
				superTask.setDuration(new SimpleTimePeriod(superStartDate, superSluttDate));
				if(utstyrGrafPeriodeTmp.getAktiv()) {
					taskSeriesAktiv.add(superTask);
				} else {
					taskSeriesInnaktiv.add(superTask);
				}
			}
		}
		
		TaskSeriesCollection taskSeriesCollection = new TaskSeriesCollection();
		taskSeriesCollection.add(taskSeriesAktiv);
		taskSeriesCollection.add(taskSeriesInnaktiv);

        return taskSeriesCollection;
	}
	
	private static List<UtstyrGrafPeriode> buildPeriodeList(Ioliste ioliste, List<KjoretoyUtstyr> kjoretoyutstyrList) {
		List<UtstyrGrafPeriode> retList = new ArrayList<UtstyrGrafPeriode>();
		
		//Loop gjennom alle kjøretøyutstyr
		for(KjoretoyUtstyr kjoretoyUtstyr : kjoretoyutstyrList) {
			List<KjoretoyUtstyrIo> ioPeriodeList = kjoretoyUtstyr.getKjoretoyUtstyrIoList();
			
			//Sorterer utstyr-io-liste og periode-liste etter startdato på periodene
			Collections.sort(ioPeriodeList, new Comparator<KjoretoyUtstyrIo>() {
				@Override
				public int compare(KjoretoyUtstyrIo o1, KjoretoyUtstyrIo o2) {
					return o1.getFraDato().compareTo(o2.getFraDato());
				}
			});
			
			//Loop gjennom alle kjoretoyutstyrio
			for(KjoretoyUtstyrIo kjoretoyUtstyrIo : kjoretoyUtstyr.getKjoretoyUtstyrIoList()) {
				//Sjekker om rett port
				if(kjoretoyUtstyrIo.getIoliste().getId().equals(ioliste.getId())) {
					Date currentDate = kjoretoyUtstyrIo.getFraDato();

					if(currentDate != null) {
						UtstyrGrafPeriode utstyrGrafPeriode = new UtstyrGrafPeriode(currentDate, kjoretoyUtstyrIo.getTilDato(), Boolean.FALSE, kjoretoyUtstyr.getTextForGUI());
						retList.add(utstyrGrafPeriode);
					}
				}
			}
		}
		
		return retList;
	}
	
	private static boolean before(Date first, Date second) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(first);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		first = cal.getTime();
		cal.setTime(second);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		second = cal.getTime();
		return first.before(second) || first.equals(second);
	}
}
