package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;

import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyInfoViewPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.netbeans.spi.wizard.WizardPage;

public class DokBildeLoggPage extends WizardPage {
	private MipssPlugin mipssPlugin;
	private WizardLine wizardLine;
	
	private KjoretoyInfoViewPanel kjoretoyInfoViewPanel;

	public DokBildeLoggPage(MipssPlugin mipssPlugin, WizardLine wizardLine, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard) {
		super(stepId, stepDescription, autoListen);
		this.mipssPlugin = mipssPlugin;
		this.wizardLine = wizardLine;
		
		initGui();
	}

	private void initGui() {
		this.setLayout(new BorderLayout());
	}
	
	@Override
	protected void renderingPage() {
		if(kjoretoyInfoViewPanel == null) {
			kjoretoyInfoViewPanel = new KjoretoyInfoViewPanel(wizardLine, wizardLine.getKjoretoy(), true, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getBundle(), mipssPlugin, true);
			this.add(kjoretoyInfoViewPanel, BorderLayout.CENTER);
		}
	}
}
