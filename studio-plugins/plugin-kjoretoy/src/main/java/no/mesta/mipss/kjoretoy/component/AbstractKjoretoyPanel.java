package no.mesta.mipss.kjoretoy.component;

import java.awt.Dimension;

import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

public abstract class AbstractKjoretoyPanel extends AbstractMipssEntityPanel<Kjoretoy> {
	private InstrumentationController controller;
	
	public AbstractKjoretoyPanel(Kjoretoy mipssEnt, InstrumentationController controller) {
		super(mipssEnt);
		this.controller = controller;
		
		controller.registerKjoretoyPanel(this);
	}
	
	protected InstrumentationController getInstrumentationController() {
		return controller;
	}

	/**
	 * Kalles dersom panelet har endret kjøretøyet
	 */
	protected void kjoretoyChanged() {
		controller.kjoretoyChanged();
	}

	@Override
	protected boolean isSaved() {
		return true;
	}

	@Override
	public void saveChanges() {}

	@Override
	public boolean saveChangesAndContinue() {
		return true;
	}

	@Override
	public void unloadEntity() {}
	
	@Override
	public Dimension getDialogSize() {
		return null;
	}
}
