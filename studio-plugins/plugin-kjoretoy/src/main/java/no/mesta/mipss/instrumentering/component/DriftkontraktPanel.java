/**
 * 
 */
package no.mesta.mipss.instrumentering.component;

import java.util.ResourceBundle;

import javax.swing.JPanel;

import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Kunde;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;

/**
 * @author jorsor
 */
public class DriftkontraktPanel extends JPanel {
	private static final String EMPTY_STRING = "";
	// Variables declaration - do not modify
    private javax.swing.JTextField ansvarsnummer;
	private javax.swing.JTextField ansvarsted;
    private ResourceBundle bundle;
    private javax.swing.JTextField distrikt;
    private javax.swing.JLabel driftkontraktLederTitle;
    private javax.swing.JLabel driftkontraktTitle;
    private javax.swing.JTextField faks;
    private javax.swing.JTextArea fritekst;
    private javax.swing.JTextField gyldigFraDate;
    private javax.swing.JTextField gyldigTilDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField driftsleder;
    private javax.swing.JTextField kontraktnavn;
    private javax.swing.JTextField kontraktstype;
    private javax.swing.JTextField kunde;
    private javax.swing.JTextField prosjektnummer;
    private javax.swing.JLabel prosjektnummerLable;
    private javax.swing.JTextField telefon1;
    private javax.swing.JTextField telefon2;
    // End of variables declaration
	public DriftkontraktPanel () {
		super();
		bundle = Utils.getBundle();
		initComponents();
		Utils.setBold(driftkontraktTitle);
		Utils.setBold(driftkontraktLederTitle);
		
		
	}
	
	private void clearComponents() {
		kontraktnavn.setText(EMPTY_STRING);
		prosjektnummer.setText(EMPTY_STRING);
		ansvarsnummer.setText(EMPTY_STRING);
		ansvarsted.setText(EMPTY_STRING);
		kontraktstype.setText(EMPTY_STRING);
		kunde.setText(EMPTY_STRING);
		distrikt.setText(EMPTY_STRING);
		gyldigFraDate.setText(EMPTY_STRING);
		gyldigTilDate.setText(EMPTY_STRING);
		
		fritekst.setText(EMPTY_STRING);
		
		driftsleder.setText(EMPTY_STRING);
		telefon1.setText(EMPTY_STRING);
		telefon2.setText(EMPTY_STRING);
		faks.setText(EMPTY_STRING);
	}
	
	private void initComponents() {

        driftkontraktTitle = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        kontraktnavn = new javax.swing.JTextField();
        prosjektnummerLable = new javax.swing.JLabel();
        prosjektnummer = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        ansvarsnummer = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        ansvarsted = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        fritekst = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        gyldigFraDate = new javax.swing.JTextField();
        gyldigTilDate = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        driftsleder = new javax.swing.JTextField();
        driftkontraktLederTitle = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        telefon1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        telefon2 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        faks = new javax.swing.JTextField();
        kontraktstype = new javax.swing.JTextField();
        kunde = new javax.swing.JTextField();
        distrikt = new javax.swing.JTextField();

        setName("Form"); // NOI18N
        driftkontraktTitle.setText(bundle.getString("driftkontrakt.title")); // NOI18N
        driftkontraktTitle.setName("driftkontraktTitle"); // NOI18N

        jLabel2.setText(bundle.getString("driftkontrakt.navn")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        kontraktnavn.setEditable(false);
        kontraktnavn.setName("kontraktnavn"); // NOI18N

        prosjektnummerLable.setText(bundle.getString("driftkontrakt.prosjektnummer")); // NOI18N
        prosjektnummerLable.setName("prosjektnummerLable"); // NOI18N

        prosjektnummer.setEditable(false);
        prosjektnummer.setName("prosjektnummer"); // NOI18N

        jLabel1.setText(bundle.getString("driftkontrakt.ansvarnummer")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        ansvarsnummer.setEditable(false);
        ansvarsnummer.setName("ansvarsnummer"); // NOI18N

        jLabel4.setText(bundle.getString("driftkontrakt.kunde")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        jLabel5.setText(bundle.getString("driftkontrakt.type")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jLabel6.setText(bundle.getString("driftkontrakt.distrikt")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N

        jLabel7.setText(bundle.getString("driftkontrakt.ansvarsted")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        ansvarsted.setEditable(false);
        ansvarsted.setName("ansvarsted"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        fritekst.setColumns(20);
        fritekst.setEditable(false);
        fritekst.setRows(5);
        fritekst.setName("fritekst"); // NOI18N
        jScrollPane1.setViewportView(fritekst);

        jLabel8.setText(bundle.getString("driftkontrakt.gyldigFra")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N

        jLabel9.setText(bundle.getString("driftkontrakt.gyldigTil")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        gyldigFraDate.setEditable(false);
        gyldigFraDate.setName("gyldigFraDate"); // NOI18N

        gyldigTilDate.setEditable(false);
        gyldigTilDate.setName("gyldigTilDate"); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel3.setText(bundle.getString("driftkontrakt.leder.navn")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        driftsleder.setEditable(false);
        driftsleder.setName("driftsleder"); // NOI18N

        driftkontraktLederTitle.setText(bundle.getString("driftkontrakt.leder.title")); // NOI18N
        driftkontraktLederTitle.setName("driftkontraktLederTitle"); // NOI18N

        jLabel10.setText(bundle.getString("driftkontrakt.leder.tlf1")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N

        telefon1.setEditable(false);
        telefon1.setName("telefon1"); // NOI18N

        jLabel11.setText(bundle.getString("driftkontrakt.leder.tlf2")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N

        telefon2.setEditable(false);
        telefon2.setName("telefon2"); // NOI18N

        jLabel12.setText(bundle.getString("driftkontrakt.leder.faks")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N

        faks.setEditable(false);
        faks.setName("faks"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(driftkontraktLederTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                            .addComponent(driftsleder, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(telefon1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(telefon2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(faks, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(194, 194, 194))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(driftkontraktLederTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telefon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(telefon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(driftsleder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        kontraktstype.setEditable(false);
        kontraktstype.setName("kontraktstype"); // NOI18N

        kunde.setEditable(false);
        kunde.setName("kunde"); // NOI18N

        distrikt.setEditable(false);
        distrikt.setName("distrikt"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(kontraktnavn, javax.swing.GroupLayout.PREFERRED_SIZE, 399, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(driftkontraktTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel2)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(prosjektnummerLable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                                                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(distrikt)
                                            .addComponent(kunde)
                                            .addComponent(gyldigFraDate, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                            .addComponent(gyldigTilDate, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                            .addComponent(kontraktstype)))
                                    .addComponent(jLabel9)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(72, 72, 72)
                                        .addComponent(ansvarsted, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(72, 72, 72)
                                        .addComponent(ansvarsnummer, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(72, 72, 72)
                                        .addComponent(prosjektnummer, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)))
                        .addGap(0, 0, 0))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(driftkontraktTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(kontraktnavn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(prosjektnummerLable)
                            .addComponent(prosjektnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ansvarsnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ansvarsted, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(kontraktstype, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(kunde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(distrikt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(gyldigFraDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(gyldigTilDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }
	
	private void setComponents(Driftkontrakt entity) {
		kontraktnavn.setText(entity.getTextForGUI());
		kontraktstype.setText(entity.getTypeNavn());
		
		Kunde k = null;//entity.getKunde();
		if(k == null) {
			kunde.setText(bundle.getString("gen.ingen"));
		}
		else {
			kunde.setText(k.getNavn());
		}
		
		Driftdistrikt dd = entity.getDriftdistrikt();
		if(dd == null) {
			distrikt.setText(bundle.getString("gen.ingen"));
		}
		else {
			distrikt.setText(dd.getNavn());
		}
		
		gyldigFraDate.setText(Utils.formatDate(entity.getGyldigFraDato()));
		gyldigTilDate.setText(Utils.formatDate(entity.getGyldigTilDato()));
		
		fritekst.setText(Utils.checkString(entity.getBeskrivelse()));
		
		driftsleder.setText(Utils.checkString("Fjernet ifm db-endring"));
		telefon1.setText(Utils.checkString("Fjernet ifm db-endring"));
		telefon2.setText(Utils.checkString("Fjernet ifm db-endring"));
		faks.setText(Utils.checkString("Fjernet ifm db-endring"));
		
		/*kontraktleder.setText(Utils.checkString(entity.getKundekontaktnavn()));
		telefon1.setText(Utils.checkString(entity.getKundekontakttelefon1()));
		telefon2.setText(Utils.checkString(entity.getKundekontakttelefon2()));
		faks.setText(Utils.checkString(entity.getKundekontaktfaks()));*/
	}
	
	public void setDriftkontrakt(Driftkontrakt entity) {
		if(entity == null) {
			clearComponents();
		}
		else {
			setComponents(entity);
		}
	}
	
    /**
	 * Setter det prosjektet som er valgt. 
	 * @param prosjektKontrakt
	 */
	public void setProsjektKontrakt(KontraktProsjekt prosjektKontrakt) {
		if(prosjektKontrakt == null) {
			prosjektnummer.setText(EMPTY_STRING);
			ansvarsnummer.setText(EMPTY_STRING);
			ansvarsted.setText(EMPTY_STRING);
		}
		else {
			prosjektnummer.setText(String.valueOf(prosjektKontrakt.getProsjektNr()));
			ansvarsnummer.setText("Må tas ifm dbendring");
			ansvarsted.setText("Må tas ifm dbendring");
			/*Ansvarssted ansvar = prosjektKontrakt.getAnsvarssted();
			if(ansvar == null) {
				ansvarsnummer.setText(EMPTY_STRING);
				ansvarsted.setText(EMPTY_STRING);
			}
			else {
				ansvarsnummer.setText(String.valueOf(ansvar.getId()));
				ansvarsted.setText(ansvar.getNavn());
			}*/
		}
	}


    

}
