package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.ui.combobox.MipssComboBoxWrapper.PseudoEntity;

import org.jdesktop.beansbinding.Converter;


public class ObjectUtstyrmodellConverter extends Converter<Object, Utstyrmodell> {

	@Override
	public Utstyrmodell convertForward(Object val) {
		if(val == null) {
			return null;
		}
		else if(val instanceof PseudoEntity) {
			return null;
		}
		else {
			return (Utstyrmodell) val;
		}
	}

	@Override
	public Object convertReverse(Utstyrmodell val) {
		if(val == null) {
			return null;
		}
		else {
			return (Object)val;
		}
	}

}
