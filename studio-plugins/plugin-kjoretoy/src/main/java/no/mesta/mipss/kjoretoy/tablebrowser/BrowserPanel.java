package no.mesta.mipss.kjoretoy.tablebrowser;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.common.ButtonPanel;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXTable;

/**
 * Skjelett for visning av en entitetstabell og en panel for enkeltentieter valgt i tabellen.
 * JPanel for denne klassen er delt i tre:
 * <ul>
 *  <li>Den øvre delen viser en tabell av entiteter.</li>
 *  <li>Den nedre delen viser en presentasjonspanel for hver entity man velger i tabellen.</li>
 *  <li>Den nedre delen inneholder flere knapper som kontrollerer oppdateringer, m.m.</li>
 * </ul>
 * 
 * Denne klassen baserer seg på et forhold mellom entitetene i databasen definert som
 * typeparameter U, og listenheter definert ved typeparameter L som følger:
 * <ul>
 * <li>U-entiteter kommer fra tabeller i databasen.</li>
 * <li>L-objekter er treffresultater i søk i databaseview, e.l.</li>
 * <li>Det er viktig at både L-objekter identifiseres med samme ID-property som U-entiteter.
 *      da kan man hente en U-entietet vis nøkkelen til sinL-objekt.</li>
 * <li>Samme ID-forhold brukes for å legge til og fjerne entiteter.</li>
 * </ul>
 * @author jorge
 */
public abstract class BrowserPanel<P extends AbstractMipssEntityPanel<?> & Browsable, L extends IRenderableMipssEntity, U extends IRenderableMipssEntity> extends JPanel implements WindowListener {
	private static final long serialVersionUID = -7319966216777794505L;

	protected Logger logger = LoggerFactory.getLogger(BrowserPanel.class);
    
    private P unitPanel = null;
    protected static ResourceBundle bundle = null;
    protected static MipssKjoretoy bean = null;
    protected List<L> tableEntities = null;
    
    private String title = "Enhetsliste";
    private ButtonPanel buttonPanel = null;
    private JXTable browseTable = null;
    private JDialog dialog = null;
    private Component parent = null;
    
    private JPanel entityPanel;
    private JScrollPane scrlPaneBrowseTable;
    
    /**
     * Oppretter en tom panel.
     */
    public BrowserPanel(Component parent) {
        super();
        this.parent = parent;
        bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
        bean = KjoretoyUtils.getKjoretoyBean();
        try {
            initComponents();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, bundle.getString("errormessage.IngenListVisning"));
            return;
        }
    }
    
    /** 
    /**
     * Oppretter en instans som peker til sin panel for enhetsvisning.
     * @param unitPanel er panelet som viser hver enhet som velges i tabellen.
     */
    public BrowserPanel(Component parent, P unitPanel) {
        this(parent);
        setUnitPanel(unitPanel);        
    }

	/**
     * Laster innholdet i tabellen fra databasen når brukeren har endret noe som
     * må vises i tabellen.
     * @param selectEntity brukes til å velge riktig rad i browsertabellen. Hvis null
     * blir første rad vist.
     */
    public abstract void reloadTable(U selectEntity);

    protected abstract Dimension getDialogSize();
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    
    public JDialog getDialog() {
		return dialog;
	}
    
    /**
     * Åpner et nytt modal dialogvindu med denne browser som innhold. 
     */
    public List<L> createAndShowDialogBox() {
        dialog = new JDialog((Frame)AbstractMipssEntityPanel.getContainerWindow(parent));
        dialog.setTitle(title);
        dialog.setModal(true);
        dialog.setResizable(true);
        dialog.setSize(getDialogSize());
        dialog.getContentPane().add((Component) this);
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        dialog.addWindowListener((WindowListener)this);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        
        return tableEntities;
    }
    
    /**
     * Stenger og dreper dialogvinduet med hele innholdet.
     */
    public void closeDialogBox() {
        if(dialog != null) {
            dialog.dispose();
        }
    }

    /**
     * leverer panelen som viser den valgteenheten.
     * @return
     */
    public P getEntityPanel() {
        return unitPanel;
    }
    
    /**
     * henter ut JPanel med alle knapper som er satt opp.
     * Knappene er allerede innlemmet i komponentstrukturen i panelet.
     * @return
     */
    public ButtonPanel getButtonPanel() {
        return buttonPanel;
    }
    
    /**
     * Henter ut tabellen. Tabellen er allerede innlemmet i komponentstrukturen i panelet.
     * @return
     */
    public JXTable getBrowseTable() {
        return browseTable;
    }
    
    /**
     * Setter panelen for enhetsvisningen.
     */
    public void setUnitPanel(P unitPanel) {
        this.unitPanel = unitPanel;
        unitPanel.setBrowserParent(this);
        entityPanel.removeAll();
        entityPanel.add(unitPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    /**
     * Henter panelen for enhetsvisningen.
     */
    public P getUnitPanel() {
        return unitPanel;
    }
    
    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        if (unitPanel.saveChangesAndContinue()) {
            dialog.pack();
            dialog.setVisible(true);
        }
        else {
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        }
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    private void initComponents() {
        scrlPaneBrowseTable = new JScrollPane();
        browseTable = new JXTable();
        entityPanel = new JPanel();
        entityPanel.setLayout(new GridBagLayout());
        buttonPanel = new ButtonPanel();

        scrlPaneBrowseTable.setViewportView(browseTable);
        scrlPaneBrowseTable.setMinimumSize(new Dimension(200, 100));
        scrlPaneBrowseTable.setPreferredSize(new Dimension(200, 100));
        
        this.setLayout(new GridBagLayout());
//        this.add(scrlPaneBrowseTable, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 0, 11), 0, 0));
        this.add(entityPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(buttonPanel, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
}
