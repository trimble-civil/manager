package no.mesta.mipss.kjoretoy.component;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ResourceBundle;

import javax.ejb.EJBException;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Hovedklasse for paneler som viser deler av informasjonen om en MIPSS entitet.
 * 
 * Implementasjoner av denne klassen kan vises i dialogboxer med egne knapper for lagring,stenging, m.m. eller
 * kan åpne et enkelt dialogvindu med en OK-knapp.
 * 
 * @author jorge
 */
public abstract class AbstractMipssEntityPanel<T extends IRenderableMipssEntity> extends JPanel implements WindowListener, PropertyChangeSource {
	private PropertyChangeSupport props = new PropertyChangeSupport(this); 
	private Logger logger = LoggerFactory.getLogger(AbstractMipssEntityPanel.class);
	private String dialogBoxTitle = null;
    
    protected T mipssEntity;
    private ResourceBundle bundle;
    
    private JPanel parent = null;
    protected JDialog dialog = null;
    
    /**
     * Constructor som initialiserer visningen av tomme komponenter og setter kjøretøy objektet.
     * @param mipssEntity kan være null.
     */
    public AbstractMipssEntityPanel(T mipssEnt) {
        this.mipssEntity = mipssEnt;
    }
    
    /**
     * Setter MIPSS entiteten som skal vises.
     * Hvis man allerede har en Kjoretoy instans som er lik den leverte blir
     * kallet ignorert.
     * @param mipsEntity
     */
    public void setMipssEntity(T mipsEntity) {
//        if (this.mipssEntity != null && this.mipssEntity.equals(mipsEntity)) {
//            return;
//        }
        // varsle om en nærstående uploading
        unloadEntity();
        
        // Lagre eventuelle endringer før man viser neste entitet
        saveChangesAndContinue();
        
        Object oldEntity = this.mipssEntity; 
        this.mipssEntity = mipsEntity;
        try {
            loadFromEntity();
        } catch (Exception e) {
            logger.error("kunne ikke vise: " + mipssEntity);
            e.printStackTrace();
        }
        
        props.firePropertyChange("mipssEntity", oldEntity, mipssEntity);
    }

    public T getMipssEntity() {
    	return mipssEntity;
    }
    
    /**
     * Denne metoden initialiserer tomme komponenter for visningen. Vanligvis
     * kaller denne metoden den dedikerte GIO-oppsett metoden fra JDeveloper eller
     * Netbeans.
     * Bruk denne metoden for å sette listeners. Husk at denne metoden kalles fra
     * contructor'en i denne abstrakte klassen, slik at interne variabler i konkrete
     * klasser som arver fra denne ikke er blitt initialisert når denne
     * metoden utføres.
     */
    protected abstract void initComponentsPresentation();
    
    /**
     * Definerer egen logikk for å initialisere de komponentene designet i NetBeans og 
     * eventuelt andre.
     * Avhenger av at komponenetene er initialøisert først.
     * Denne metoden burde først frigjøre den eventuelle tidligere entiteten før
     * man laster fra den nye. Dette er særlig viktig der man bruker BeansBinding.
     */
    protected abstract void loadFromEntity();
    
    /**
     * Slår av/på alle oppdateringsrettighetene fro alle controllere hvis innhold
     * kan endres på.
     * @param canUpdate flagg som slår oppdateringsrettighetene av eller på.
     */
    public abstract void enableUpdating(boolean canUpdate);
    
    /**
     * Utfører lagringen av alle foretatte endringene.
     */
    public abstract void saveChanges();
    
    /**
     * Kontrollerer om brukeren har gjort noen endringer. HVis det er tilfelle
     * spørres brukeren om endringene skal lagres.
     * @return {@code true} hvis brukeren velger å fortsette.
     */
    public abstract boolean saveChangesAndContinue();
    
    /**
     * Sjekker om endringene er lagret.
     * @return TRUE hvis lagret
     */
    protected abstract boolean isSaved();
    
    /**
     * Setter den EJB session bean som brukes for å lese/oppdatere databasen.
     * Kun én slik bean per panel.
     * @param bean
     */
    public abstract void setSessionBean(Object bean);
    
    /**
     * Signaliserer at en ny entitet er på vei og at panelet må forberede seg.
     */
    public abstract void unloadEntity();

    /**
     * Setter størrelsen på dialogen om panelet vises i en dialog
     * @return
     */
    protected abstract Dimension getDialogSize();
    
    /**
     * Setter panelet som omfatter dette panelet.
     * @param parent
     */
    public void setParent (JPanel parent) {
        this.parent = parent;
    }
    
    /**
     * Åpner et dialogvindu som viser panelet. Bruker titelen man har satt opp
     * i dialogBoxTitle på forhånd.
     */
    public void createAndShowDialogBox() {
        JOptionPane.showMessageDialog(null, (Object)this, getDialogBoxTitle(), JOptionPane.PLAIN_MESSAGE);
    }
    
    /**
     * Åpne et modalt dialogvindu for registrering av nytt utstyr.
     * @return Objektet til det nye utstyret eller null hvis registreringen avvist.
     */
    public T newEntityDialogBox() {
        if (parent == null) {
            dialog = new JDialog();
        }
        else {
            dialog = new JDialog(getContainerWindow(parent)); 
        }
        dialog.setTitle(getDialogBoxTitle());
        dialog.setModal(true);
        dialog.setResizable(true);
        dialog.setSize(getDialogSize());
        dialog.getContentPane().add(this);
        
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        dialog.addWindowListener((WindowListener)this);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        
        return (isSaved())? mipssEntity : null;
    }
    
    /**
     * betjerner vindusstenging fra vindusrammen.
     */
    protected void closeDialog() {
    	SwingUtilities.windowForComponent(this).dispose();
        if (dialog != null) {
            dialog.dispose();
        }
        else {
        	logger.debug("Mangler dialogobjektet å stenge");
        }
    }

    /**
     * Stenger dialogvinduet hvis brukeren velger så.
     */
    public void windowClosing(WindowEvent e) {
        if (saveChangesAndContinue()) {
            dialog.pack();
            dialog.setVisible(true);
        }
        else {
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        }
    }
    
    public void windowOpened(WindowEvent e) {}

    public void windowClosed(WindowEvent e) {}

    public void windowIconified(WindowEvent e) {}

    public void windowDeiconified(WindowEvent e) {}

    public void windowActivated(WindowEvent e) {}

    public void windowDeactivated(WindowEvent e) {}

    public void setDialogBoxTitle(String dialogBoxTitle) {
        this.dialogBoxTitle = dialogBoxTitle;
    }

    public String getDialogBoxTitle() {
        if (dialogBoxTitle == null) {
            return "";
        }
        return dialogBoxTitle;
    }

	/**
	 * @return the bundle
	 * @throws IllegalStateException hvis budle ikke var allerede injisert.
	 */
	public ResourceBundle getBundle() {
		if(bundle == null) {
			throw new IllegalStateException("Finner ikke resource bundle");
		}
		return bundle;
	}

	/**
	 * @param bundle the bundle to set
	 */
	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}
	
    /**
     * Går bakover i parent hierarkiet inntil den finner en parent som er {@code Window}.
     * @param containee
     * @return
     */
    public static Window getContainerWindow(Component containee) {
        Component parent = containee;

        while (!(parent instanceof Window)){
            if (parent.getParent() == null) {
                break;
            }
            parent = parent.getParent();
        }
        
        return (Window) parent;
    }
    
    /**
     * Viser et dialogvindu med feilmeldingen.
     * @param e
     * @param infoMessage
     */
    private void showException(Exception e, String infoMessage) {
	    if (e instanceof EJBException) {
	    	String m = infoMessage + '\n' + bundle.getString("errormessage.noDbConnection");
	        JOptionPane.showMessageDialog(null, m, "Uventet hendelse", JOptionPane.ERROR_MESSAGE);
	    }
	    else {
	    	String m = "Legg til håndtering av: \n" + infoMessage + '\n' + e.getClass();
	    	JOptionPane.showMessageDialog(null, m, "Uventet hendelse", JOptionPane.ERROR_MESSAGE);
	        e.printStackTrace();
	    }
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
    	props.addPropertyChangeListener(l);
    }
    
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
    	props.addPropertyChangeListener(propName, l);
    }
    
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
    	props.removePropertyChangeListener(l);
    }
    
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
    	props.removePropertyChangeListener(propName, l);
    }
}
