package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.instrumentering.wizard.WizardLine;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyCompleteViewPanel;
import no.mesta.mipss.kjoretoy.kjoretoyutstyr.KjoretoyUtstyrController;
import no.mesta.mipss.kjoretoy.kjoretoyutstyr.KjoretoyUtstyrDialog;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.tablebrowser.BrowserUtils;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrView;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;


/**
 * Viser kun utstyrlisten for kjøretøyet sendt inn i constructor.
 */
public class KjoretoyUtstyrListPanel extends AbstractKjoretoyPanel {
	private final static boolean NY_UTSTYRDIALOG = false;
	private Window parent = null;
    private List<KjoretoyUtstyrView> utstyr = null;
    private MipssKjoretoy bean = null;
    private JPanel pnlMain;
    private JScrollPane scrlPaneUtstyr;
    private JLabel kjoretoyUtstyrLabel;
    private JButton nyttUtstyrButton;
    private JButton btnEndreUtstyr;
    private JButton btnSlettUtstyr;
    private JXTable utstyrTable;
    private CompositePanel compositePanelParent;
    private boolean enkelVisning;
    private boolean visEndreKnapp;
    private boolean begrensetTilgangEndring;
	private final MipssPlugin plugin;
    
    public KjoretoyUtstyrListPanel(MipssPlugin plugin, InstrumentationController controller, Kjoretoy kjoretoy, Window parent, MipssKjoretoy bean, ResourceBundle bundle, CompositePanel compositePanelParent, boolean enkelVisning, boolean visEndreKnapp, boolean begrensetTilgangEndring) {    
        super(kjoretoy, controller);
		this.plugin = plugin;
        this.bean = bean;
        this.compositePanelParent = compositePanelParent;
        this.enkelVisning = enkelVisning;
        this.visEndreKnapp = visEndreKnapp;
        this.begrensetTilgangEndring = begrensetTilgangEndring;
        
        super.setBundle(bundle); 
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning"));
            return;
        }
        this.parent = parent;
        
        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning"));
            }
        }
        
        if(begrensetTilgangEndring) {
        	nyttUtstyrButton.setEnabled(false);
        	btnEndreUtstyr.setEnabled(false);
        	btnSlettUtstyr.setEnabled(false);
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    public void enableUpdating(boolean canUpdate) {
        utstyrTable.setEnabled(canUpdate);
        nyttUtstyrButton.setEnabled(canUpdate && !begrensetTilgangEndring);
        btnEndreUtstyr.setEnabled(canUpdate && !begrensetTilgangEndring);
        btnSlettUtstyr.setEnabled(canUpdate && !begrensetTilgangEndring);
    }
        
    private void initListeners () {
    	if(visEndreKnapp && !begrensetTilgangEndring) {
	    	utstyrTable.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(e.getClickCount() == 2) {
						if(NY_UTSTYRDIALOG) {
							int index = utstyrTable.rowAtPoint(e.getPoint());
							endreUtstyr(index);
						} else {
							int index = utstyrTable.rowAtPoint(e.getPoint());
							int realIndex = utstyrTable.convertRowIndexToModel(index);
							List<KjoretoyUtstyrView> liste = ((EditableMippsTableModel<KjoretoyUtstyrView>)utstyrTable.getModel()).getEntities();
			            	if(liste != null && liste.size() > 0) {
			            		KjoretoyUtstyrView kuv = liste.get(realIndex);
			            		if (kuv.getId()==null){
			            			JOptionPane.showMessageDialog(KjoretoyUtstyrListPanel.this, KjoretoyUtils.getBundle().getString("gen.ikkeViseUtstyrSomIkkeErLagret"));
			            		}else{
				            		KjoretoyUtstyr ku = bean.getKjoretoyUtstyr(kuv.getId());
				            		KjoretoyUtstyrPanel kjoretoyUtstyrPanel = PanelFactory.createKjoretoyUtstyrPanel(ku, enkelVisning, plugin);
				            		kjoretoyUtstyrPanel.newEntityDialogBox();
				                   	loadFromEntity();
			            		}
			            	}
						}
					}
				}
	        });
	    	btnSlettUtstyr.addActionListener(new ActionListener(){
	    		public void actionPerformed(ActionEvent e){
	    			slettUtstyr(utstyrTable.getSelectedRow());
	    		}
	    	});
	    	btnEndreUtstyr.addActionListener(new ActionListener() {
	            @SuppressWarnings("unchecked")
				public void actionPerformed(ActionEvent e){
	            	if(NY_UTSTYRDIALOG) {
	            		int index = utstyrTable.getSelectedRow();
		                endreUtstyr(index);
	            	} else {
		            	int index = utstyrTable.getSelectedRow();
		                int realIndex = utstyrTable.convertRowIndexToModel(index);
		                List<KjoretoyUtstyrView> liste = ((EditableMippsTableModel<KjoretoyUtstyrView>)utstyrTable.getModel()).getEntities();
		            	if(liste != null && liste.size() > 0) {
		            		KjoretoyUtstyrView kuv = liste.get(realIndex);
		            		if (kuv.getId()==null){
		            			JOptionPane.showMessageDialog(KjoretoyUtstyrListPanel.this, KjoretoyUtils.getBundle().getString("gen.ikkeViseUtstyrSomIkkeErLagret"));
		            		}else{
			            		KjoretoyUtstyr ku = bean.getKjoretoyUtstyr(kuv.getId());
			            		KjoretoyUtstyrPanel kjoretoyUtstyrPanel = PanelFactory.createKjoretoyUtstyrPanel(ku, enkelVisning, plugin);
			            		kjoretoyUtstyrPanel.newEntityDialogBox();
			                   	loadFromEntity();
		            		}
		            	}
	            	}
	            }
	        });
    	}
    	if(!begrensetTilgangEndring) {
	        nyttUtstyrButton.addActionListener(new ActionListener() {
	                @SuppressWarnings("unchecked")
					public void actionPerformed(ActionEvent e) {
	                	if(NY_UTSTYRDIALOG) {
		                	KjoretoyUtstyrDialog kjoretoyUtstyrDialog = new KjoretoyUtstyrDialog(parent, new KjoretoyUtstyrController(mipssEntity.getKjoretoyUtstyrList(), new KjoretoyUtstyr(), true));
		                	kjoretoyUtstyrDialog.showDialog();
	                	} else {
		                    // opprett dialogvinduet
		                    NewKjoretoyUtstyrPanel newKjoretoyUtstyr = PanelFactory.createNewKjoretoyUtstyrPanel(plugin, mipssEntity, enkelVisning);
		                    newKjoretoyUtstyr.setParent(null);
		                    
		                    // vis dialogvinduet og vent på svar
		                    KjoretoyUtstyr newUtstyr = newKjoretoyUtstyr.newEntityDialogBox();
		                    if (newUtstyr != null) {
		                    	if (!(getInstrumentationController() instanceof WizardLine)){
		                    		newUtstyr = bean.addKjoretoyUtstyr(newUtstyr);
		                    	}
		                    	mipssEntity.getKjoretoyUtstyrList().add(newUtstyr);
		                    	utstyr.add(new KjoretoyUtstyrView(newUtstyr));
		                    	((EditableMippsTableModel<KjoretoyUtstyrView>)utstyrTable.getModel()).fireTableDataChanged();
		                    	
//		                    	if(compositePanelParent != null) {
//		                    		compositePanelParent.enableSaveButton(true);
//		                    	}
		                    	//loadFromEntity();
		                    }
	                	}
	                }
	            });
    	}
    }
    
    private void slettUtstyr(int viewIndex){
    	int idx = utstyrTable.convertRowIndexToModel(viewIndex);
    	if(idx >= 0 && idx < utstyrTable.getRowCount()) {
    		int v = JOptionPane.showConfirmDialog(this, getBundle().getString("message.slettUtstyr.message"), getBundle().getString("message.slettUtstyr.title"), JOptionPane.YES_NO_OPTION);
    		if (v==JOptionPane.YES_OPTION){
	    		KjoretoyUtstyrView kjoretoyUtstyr = utstyr.get(idx);
	    		List<KjoretoyUtstyr> toRemove = new ArrayList<KjoretoyUtstyr>();
	    		if (kjoretoyUtstyr.getId()!=null){
		    		for (KjoretoyUtstyr i:mipssEntity.getKjoretoyUtstyrList()){
		    			if (i.getId().equals(kjoretoyUtstyr.getId())){
		    				toRemove.add(i);
		    			}
		    		}
		    		mipssEntity.getKjoretoyUtstyrList().removeAll(toRemove);
	    		}
	    		for (KjoretoyUtstyr k:toRemove){
	    			bean.removeKjoretoyUtstyr(k);
	    		}
	    		loadFromEntity();
    		}
    	}
    }
    
    private void endreUtstyr(int viewIndex) {
        int realIndex = utstyrTable.convertRowIndexToModel(viewIndex);
        List<KjoretoyUtstyrView> liste = ((EditableMippsTableModel<KjoretoyUtstyrView>)utstyrTable.getModel()).getEntities();
    	if(realIndex >= 0 && liste != null && liste.size() > 0) {
    		KjoretoyUtstyrView kjoretoyUtstyrView = liste.get(realIndex);
    		List<KjoretoyUtstyr> list = mipssEntity.getKjoretoyUtstyrList();
    		KjoretoyUtstyr kjoretoyUtstyr = null;
    		for(KjoretoyUtstyr tmpKjoretoyUtstyr : list) {
    			if(tmpKjoretoyUtstyr.getId().equals(kjoretoyUtstyrView.getId())) {
    				kjoretoyUtstyr = tmpKjoretoyUtstyr;
    				break;
    			}
    		}
        	KjoretoyUtstyrDialog kjoretoyUtstyrDialog = new KjoretoyUtstyrDialog(parent, new KjoretoyUtstyrController(mipssEntity.getKjoretoyUtstyrList(), kjoretoyUtstyr, false));
        	kjoretoyUtstyrDialog.showDialog();

        	loadTable(utstyr);
    	}
    }
    
    /**
     * Laster opp den leverte listen til tabellen og tegner tabellen på nytt.
     * @param utstyrList
     */
    @SuppressWarnings("unchecked")
	private void loadTable(List<KjoretoyUtstyrView> utstyrList) {
        this.utstyr = utstyrList;
        
        // frisk opp modellen 
        BrowserUtils.initKjoretoyUtstyrTable(utstyrTable, enkelVisning);
        
        // sett inn listen for visning
        ((EditableMippsTableModel<KjoretoyUtstyrView>)utstyrTable.getModel()).setEntities(utstyrList);
        utstyrTable.repaint();
        utstyrTable.clearSelection();
    }
    
    @Override
    protected void loadFromEntity() {
        //hent utstyrlisten
         List<KjoretoyUtstyrView> list = null;
         try {
             list = bean.getKjoretoyUtstyrViewList(mipssEntity.getId());
         }
         catch (Exception e) {
             KjoretoyUtils.showException(e, getBundle().getString("utstyrList.ingenVisning")); 
             list = new ArrayList<KjoretoyUtstyrView>();
         }
         loadTable(list);
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        KjoretoyUtils.setBold(kjoretoyUtstyrLabel);
        
        // init tabellen
        initListeners();
        KjoretoyUtils.initDefaultTableSettings(utstyrTable);
    }

    private void initComponents() {
        pnlMain = new JPanel();
        kjoretoyUtstyrLabel = new JLabel();
        nyttUtstyrButton = new JButton();
        btnEndreUtstyr = new JButton();
        btnSlettUtstyr = new JButton();
        scrlPaneUtstyr = new JScrollPane();
        utstyrTable = new JXTable();

        kjoretoyUtstyrLabel.setText(getBundle().getString("utstyrList.title")); // NOI18N
        nyttUtstyrButton.setText(getBundle().getString("utstyrList.nyttButton")); // NOI18N
        btnEndreUtstyr.setText(getBundle().getString("utstyrList.endreButton"));
        btnSlettUtstyr.setText(getBundle().getString("utstyrList.slettButton"));
        
        scrlPaneUtstyr.setViewportView(utstyrTable);

        nyttUtstyrButton.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        nyttUtstyrButton.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        btnEndreUtstyr.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnEndreUtstyr.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        btnSlettUtstyr.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        btnSlettUtstyr.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        pnlMain.setBorder(BorderFactory.createTitledBorder(getBundle().getString("utstyrList.title")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(scrlPaneUtstyr, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(nyttUtstyrButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        if(visEndreKnapp) {
        	pnlMain.add(btnEndreUtstyr, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        	pnlMain.add(btnSlettUtstyr, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        }

        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
