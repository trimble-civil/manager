package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.component.installasjon.KjoretoyDfuListViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.KjoretoyUtstyrListPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * GUI-objektet for en kjøretøy entitet.
 * Består av flere komponenter som hver viser deler av kjøretøyet.
 * @author jorge
 */
public class KjoretoyCompleteViewPanel extends AbstractKjoretoyPanel implements CompositePanel {
	private static final long serialVersionUID = 644438454365518648L;
	public static final int ALIGNED_BUTTONS_WIDTH = 130;
	private Logger logger = LoggerFactory.getLogger(KjoretoyCompleteViewPanel.class);
	
	private Window parentContainer;
	private List<AbstractMipssEntityPanel<?>>subComponents = new ArrayList<AbstractMipssEntityPanel<?>>();
    private boolean hasUpdateAccess = false;
    private MipssKjoretoy bean;
    private boolean loaded = false;
    // Variables declaration - do not modify
    private JButton cancelButton;
    private JPanel pnlMain;
    private JPanel pnlButtons;
    private KjoretoyDfuListViewPanel kjoretoyDfuListViewPanel1;
    private KjoretoyUtstyrListPanel kjoretoyUtstyrViewPanel1;
    private KjoretoyViewPanel kjoretoyViewPanel1;
    private JButton saveButton;
    private JLabel nbText;

    private MipssPlugin mipssPlugin;
    
    public KjoretoyCompleteViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, boolean hasUpdateAccess, MipssKjoretoy bean, ResourceBundle bundle, Window parentContainer, MipssPlugin mipssPlugin) {
        super(kjoretoy, controller);
        this.bean = bean;
        this.parentContainer = parentContainer;
        this.mipssPlugin = mipssPlugin;
        
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }

        this.hasUpdateAccess = hasUpdateAccess;

        subComponents.add(kjoretoyViewPanel1);
        subComponents.add(kjoretoyDfuListViewPanel1);
        subComponents.add(kjoretoyUtstyrViewPanel1);

        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
                return;
            }
        }
    }
    
    /** {@inheritDoc} */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	/** {@inheritDoc} */
	public void enableUpdating(boolean canUpdate) {
		for (AbstractMipssEntityPanel panel : subComponents) {
            panel.enableUpdating(canUpdate);
        }
    }

    protected void loadFromEntity() {
    	loaded = false;
        // sett opp listen som brukes for å utføre SAVE-kommandoer
    	
        for (AbstractMipssEntityPanel panel : subComponents) {
            panel.setMipssEntity(mipssEntity);
        }
        enableUpdating(hasUpdateAccess);
        enableSaveButton(false);
        this.repaint();
        loaded = true; 
        
        List<Kjoretoyordre> ordrelist = mipssEntity.getKjoretoyordreList();
        ordreloop:
        for (Kjoretoyordre kjoretoyordre : ordrelist) {
			if (!kjoretoyordre.getSisteKjoretoyordrestatus().getIdent().equals("40")) { //Produksjonssatt
				nbText.setVisible(true);
				break ordreloop;
			} else{
				nbText.setVisible(false);
			}
		}
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        initActions();
        enableSaveButton(false);
        cancelButton.setEnabled(true);
    }

    /**
     * Oppretter og registrerer listeners for begge knappene.
     */
    private void initActions () {
    	saveButton.setAction(getInstrumentationController().getLagreKjoretoyAction());
    	cancelButton.setAction(getInstrumentationController().getAvbrytKjoretoyAction());
    }

	@Override
	public void enableSaveButton(boolean enable) {
		saveButton.setEnabled(enable);
		cancelButton.setEnabled(enable);
		
		if(enable) {
			kjoretoyChanged();
		}
	}
    
     private void initComponents() {

         pnlMain = new JPanel();
         kjoretoyViewPanel1 = PanelFactory.createKjoretoyViewPanel(getInstrumentationController(), null, this, mipssPlugin);
         kjoretoyDfuListViewPanel1 = PanelFactory.createKjoretoyDfuListViewPanel(getInstrumentationController(), null, this, this, mipssPlugin);
         kjoretoyUtstyrViewPanel1 = PanelFactory.createKjoretoyUtstyrListPanel(getInstrumentationController(), null, parentContainer, this, false, true, false, mipssPlugin);
         
         pnlButtons = new JPanel();
         saveButton = new JButton();
         cancelButton = new JButton();

         saveButton.setText(getBundle().getString("button.lagre")); // NOI18N
         cancelButton.setText(getBundle().getString("button.avbrytt")); // NOI18N

         pnlButtons.setLayout(new GridBagLayout());
         pnlButtons.add(saveButton, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
         pnlButtons.add(cancelButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
         
         nbText = new JLabel(getBundle().getString("label.iOrdreLista"));
         nbText.setFont(new Font("SansSerif", Font.BOLD, 14));
         nbText.setForeground(Color.red);
         nbText.setVisible(false);
         
         pnlMain.setLayout(new GridBagLayout());
         pnlMain.add(nbText, 						new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 11, 5, 9), 0, 0));
         pnlMain.add(kjoretoyViewPanel1, 			new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(9, 11, 5, 9), 0, 0));
         pnlMain.add(kjoretoyDfuListViewPanel1, 	new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 5, 11), 0, 0));
         pnlMain.add(kjoretoyUtstyrViewPanel1, 		new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 5, 11), 0, 0));
         pnlMain.add(pnlButtons, 					new GridBagConstraints(0, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
         
         this.setLayout(new GridBagLayout());
         this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
     }
}
