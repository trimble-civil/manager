package no.mesta.mipss.kjoretoy.common;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Enkel panel med en vannrett rekke JButtons.
 * Panelet kan utvides kun horisontalt. Knappene legger seg til høyre.
 * @author jorge
 */
public class ButtonPanel extends JPanel {
	private static final long serialVersionUID = -4590582137266733871L;
	private JButton[] buttons = null;
    
    /**
     * Oppretter kun en tom panel uten noen begrensninger.
     * Brukes for å instantiere instanser i NetBeans designer.
     */
    public ButtonPanel() {
        super();
    }
    
    /**
     * Oppretter en initialisert instans med alle sine knapper.
     * @param buttonLabels
     */
    public ButtonPanel(String[] buttonLabels) {
        super();
        initButtons(buttonLabels);
    }
    
    /**
     * Initialiserer knappene og setter JFrames som holder dem til
     * riktig størrelse m.m.
     * Kalles kun én gang. alle følgende kall blir ignorert.
     * @param buttonLabels
     */
    public void initButtons(String[] buttonLabels) {
        if (buttons != null) {
            return;
        }
        if (buttonLabels == null || buttonLabels.length == 0) {
            throw new IllegalArgumentException("Mangler knapper");
        }

        initComponents(buttonLabels);
    }
    
    /**
     * Setter opp en loddrett serie av JButtons med de navn som ble levert til constructor.
     * Serien legger seg til høyre.
     */
    private void initComponents(String[] buttonLabels) {        
        // opprett hoved layout
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        
        // opprett vannrett gruppering
        GroupLayout.SequentialGroup hSequence = layout.createSequentialGroup();
        hSequence.addContainerGap(120, Short.MAX_VALUE);
        
        // opprett loddrett gruppering
        GroupLayout.ParallelGroup vSequence = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE);
        
        // legg til hver button
        buttons = new JButton[buttonLabels.length];
        for (int i = 0; i < buttonLabels.length; i++) {
            String label = buttonLabels[i];
            JButton button = new JButton(label);
            buttons[i] = button;
            
            // legg til vannrett gruppering med spesiell sluttavstand
            hSequence.addComponent(button, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE);
            if (i >= buttonLabels.length - 1) {
                hSequence.addGap(6, 6, 6);
            }
            else {
                hSequence.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED);
            }
            
            // legg til loddrett gruppering 
            vSequence.addComponent(button);
        }
        
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(hSequence)
        );
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(vSequence)
                .addContainerGap())
        );
        
        this.setLayout(layout);
    }
    
    /**
     * Slår av/på alle knappene avhengig av det leverte parameteret.
     * @param enable neste av/på tilstand for alle knappene.
     */
    public void setEnableAll(boolean enable) {
        if (buttons == null) {
            throw new IllegalStateException("Knappene er ikke initialisert");
        }
        for (JButton b : buttons) {
            b.setEnabled(enable);
        }
    }
    
    /**
     * Henter en button med levert indeks.
     * @param index
     * @return
     */
    public JButton getButton(int index) {
        try {
            return buttons[index];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Button indeks er feil: " + index);
        }
        catch(NullPointerException npe) {
            throw new NullPointerException("Button listen ikke initialisert");
        }
    }
}
