package no.mesta.mipss.kjoretoy.plugin;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import no.mesta.mipss.kjoretoy.control.SearchControl;
import no.mesta.mipss.plugin.MipssPlugin;

/**
 * Øvre delen i plugins GUI. Inneholder alle controllere i plugin. Disse
 * controllere koordinerer visning ihht. brukerens valg. På denne måten er 
 * denne JPanel hoved-controller for hele plugin: den kobler sammen controllere
 * med sine paneler.
 * @author jorge
 */
public class TopPanel extends JPanel {
    private FlowLayout  topPanelLayout = new FlowLayout();
    
    private SearchControl searchControl;
    
    private KjoretoyTabbedPane workingTabs;
    private StatusPanel statusPanel;
    private MipssPlugin mipssPlugin;

    public TopPanel(KjoretoyTabbedPane workingTabs, StatusPanel statusPanel, MipssPlugin mipssPlugin, SearchControl searchControl) {
        super();
        this.workingTabs = workingTabs;
        this.statusPanel = statusPanel;
        this.mipssPlugin = mipssPlugin;
        this.searchControl = searchControl;
        
        // sett opp controllere først
        initControllers();
    }

    private void initControllers() {
        // init visningsmodus i panelet
        topPanelLayout.setAlignment(FlowLayout.LEFT);
        setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        this.setLayout(topPanelLayout);

        this.add(searchControl);
    }
}
