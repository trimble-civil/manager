package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.common.IolisteButtonGroup;
import no.mesta.mipss.kjoretoy.common.IolisteRadioButton;
import no.mesta.mipss.kjoretoy.common.SingleLineListSelectionListener;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.JDateTimePicker;
import no.mesta.mipss.ui.combobox.MasterComboListener;
import no.mesta.mipss.ui.combobox.MasterSlaveComboListener;
import no.mesta.mipss.ui.combobox.MipssComboBoxWrapper;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EditableMippsTableModel.MipssTableColumn;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dedikert panel for registrering av nye KjoretoyUtstyr.
 * Lagringen tillates (lagreknappen slås på) kun når hele det påkrevde hierarkiet av
 * gruppe/subgruppe/modell/individ er valgt. Det reageres ikke på andre endringer.
 * @author jorge
 */
public class NewKjoretoyUtstyrPanel extends AbstractMipssEntityPanel<KjoretoyUtstyr>  {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(NewKjoretoyUtstyrPanel.class);
    
    private FieldToolkit utstyrBinding;
//    private FieldToolkit periodeBinding;
    
    private MipssComboBoxWrapper<Utstyrgruppe> gruppeComboWrapper;
    private MipssComboBoxWrapper<Utstyrsubgruppe> subgruppeComboWrapper;
    private MipssComboBoxWrapper<Utstyrmodell> modellComboWrapper;
//    private MipssComboBoxWrapper<Utstyrindivid> individIdComboWrapper;
    private MipssComboBoxWrapper<Sensortype> sensortypeComboWrapper;
    private Sensortype selectedSensor;
    private List<Ioliste> iolisteList;
    
    private boolean saved = false;
    private IolisteButtonGroup ioButtonGroup;
    private MipssKjoretoy bean;
    // Variables declaration - do not modify
    private JLabel beskrivelseTitle;
    private JLabel datafangstTitle;
    private JTextField dfuEnhet;
    private JButton forkastButton;
//    private JTextArea fritekstArea;
    private JComboBox gruppeComboBox;
    private JLabel gruppeMessage;
    private JLabel gruppeTitle;
//    private JComboBox individComboBox;
//    private JLabel individMessage;
    private JLabel individTitle;
    private JTable ioTable;
    private JLabel ioTableLabel;
    private JDateTimePicker iolisteFraDato;
    private JDateTimePicker iolisteTilDato;
    private JCheckBox iolisteTilDatoCheckBox;
    private JLabel iolisteTilDatoLabel;
    private JLabel lblDfuEnhet;
//    private JLabel lblAktivFra;
    private JLabel lblDfuAktivFra;
    private JLabel lblSensorType;
    private JPanel pnlVelgGruppeOgModell;
//    private JPanel pnlVelgIndivid;
//    private JPanel pnlBeskrivelseOgPeriode;
    private JPanel pnlDatafangstEnhet;
//    private JScrollPane scrlPaneFritekst;
    private JScrollPane scrlPaneIoTable;
//    private JTextField maskinnummerField;
    private JComboBox modellComboBox;
    private JButton opprettButton;
    private JComboBox sensortypeComboBox;
    private JLabel sensortypeMessage;
    private JComboBox subgruppeComboBox;
//    private JDateTimePicker utstyrFraDato;
    private JLabel utstyrPeriodeMessage;
//    private JDateTimePicker utstyrTilDato;
//    private JCheckBox utstyrTilDatoCheckBox;
//    private JLabel utstyrTilDatoLabel;
    private JPanel pnlButtons;
    
    private boolean enkelVisning;

	private final MipssPlugin plugin;
    
    // End of variables declaration
    public NewKjoretoyUtstyrPanel(MipssPlugin plugin, KjoretoyUtstyr kjoretoyUtstyr, MipssKjoretoy bean, ResourceBundle bundle, boolean enkelVisning) {
        super(kjoretoyUtstyr);
		this.plugin = plugin;
        this.bean = bean;
        this.enkelVisning = enkelVisning;
        super.setBundle(bundle);
        
        if (kjoretoyUtstyr.getKjoretoy() == null) {
            throw new IllegalArgumentException("Mangler referanse til kj�ret�y for � legge til nytt utstyr");
        }
        
        super.setDialogBoxTitle(KjoretoyUtils.formatString(KjoretoyUtils.getPropertyString("utstyr.ny.Title"), mipssEntity.getKjoretoy().getTextForGUI()));
        
        checkIolisteList();
        
        utstyrBinding = FieldToolkit.createToolkit();
//        periodeBinding = FieldToolkit.createToolkit();
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyr.ingenVisning")); 
            return;
        };
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    /**
     * Forsyner KjoretoyUtstyr med en tom liste av Ioliste hvis den manglet.
     */
    private void checkIolisteList() {
    	if(mipssEntity.getKjoretoyUtstyrIoList() == null) {
    		mipssEntity.setKjoretoyUtstyrIoList(new Vector<KjoretoyUtstyrIo>());
    	}
    }

    /**
     * viser brukerveiledning omvalg av nestee subnivå i hierarkiet ut av
     * det siste vlget og av at det valgte objektets krav på subnivå. 
     */
    private void updateWarningMessages() {
        boolean shown = true;
        
        if (gruppeComboWrapper.getMipssSelection() == null) {
            gruppeMessage.setText(getBundle().getString("utstyr.melding.gruppeKreves"));
        }
        else if ((gruppeComboWrapper.getMipssSelection() != null) && 
            (((Utstyrgruppe)gruppeComboWrapper.getMipssSelection()).getKreverSubgrFlagg() == true) &&
            (subgruppeComboWrapper.getMipssSelection() == null)) {
            gruppeMessage.setText(getBundle().getString("utstyr.melding.subgruppeKreves"));
        }
        else if((subgruppeComboWrapper.getMipssSelection() != null) &&
            (((Utstyrsubgruppe)subgruppeComboWrapper.getMipssSelection()).getKreverModellFlagg() == true) &&
            (modellComboWrapper.getMipssSelection() == null)) {
            gruppeMessage.setText(getBundle().getString("utstyr.melding.modellKreves"));
        }
        else {
            gruppeMessage.setText("");
            shown = false;
        }
        
        if((! shown && 
            (modellComboWrapper.getMipssSelection() != null) &&
            ((Utstyrmodell)modellComboWrapper.getMipssSelection()).getKreverIndividFlagg() == true) 
//            && individIdComboWrapper.getMipssSelection() == null
            ) {
//            individMessage.setText(getBundle().getString("utstyr.melding.individKreves"));
        }
        else {
//            individMessage.setText("");
            shown = false;
        }

        sensortypeMessage.setVisible(!shown && selectedSensor == null && (iolisteList.size() > 0));
    }

    /**
     * setter opp alle bindingene. 
     */
    private void initBindings() {
        //Setter tildatoer til null dersom initiell verdi er null
        if(!iolisteTilDatoCheckBox.isSelected()) {
        	iolisteTilDato.setDate(null);
//        	iolisteTilDato.setEditable(false);
        }
//        if(!utstyrTilDatoCheckBox.isSelected()) {
//        	utstyrTilDato.setDate(null);
//        	utstyrTilDato.setEditable(false);
//        }
    	
        // bind så funksjonalitet for *TilDato og *FraDato. Dette slår begge checkboxer av, slik det er ånskelig
//        periodeBinding.addBinding(Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, 
//        		utstyrTilDatoCheckBox, BeanProperty.create("selected"),
//        		utstyrTilDato, BeanProperty.create("enabled")));
//        periodeBinding.addBinding(Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, 
//        		utstyrTilDatoCheckBox, BeanProperty.create("selected"),
//        		utstyrTilDatoLabel, BeanProperty.create("visible")));
//
//        periodeBinding.addBinding(Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, 
//        		iolisteTilDatoCheckBox, BeanProperty.create("selected"),
//        		iolisteTilDato, BeanProperty.create("enabled")));
//        periodeBinding.addBinding(Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, 
//        		iolisteTilDatoCheckBox, BeanProperty.create("selected"),
//        		iolisteTilDatoLabel, BeanProperty.create("visible")));
//
//        periodeBinding.bindReactiveButton(opprettButton);
//        periodeBinding.bind();
//
//        // bind valg av utstyrdato til av/på slåing av radiobuttons. Beansbindign verker ikke her!
//        utstyrFraDato.addPropertyChangeListener("date", new PropertyChangeListener() {
//			@Override
//			public void propertyChange(PropertyChangeEvent evt) {
//				ioButtonGroup.setPeriodeFraDato((Date)evt.getNewValue());
//				ioTable.repaint();
//			}
//        });
//        utstyrTilDato.addPropertyChangeListener("date", new PropertyChangeListener() {
//			@Override
//			public void propertyChange(PropertyChangeEvent evt) {
//				ioButtonGroup.setPeriodeTilDato((Date)evt.getNewValue());
//				ioTable.repaint();
//			}
//        });
        iolisteFraDato.addPropertyChangeListener("date", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				ioButtonGroup.setIolisteFraDato((Date)evt.getNewValue());
				ioTable.repaint();
			}
        });
        iolisteTilDato.addPropertyChangeListener("date", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				ioButtonGroup.setIolisteTilDato((Date)evt.getNewValue());
				ioTable.repaint();
			}
        });

        // registrer endringer i forholdene til grupper/subgrupper m.m: Ikke bruk BeansBinding her
        utstyrBinding.registerCangeListener(mipssEntity.getClass(), mipssEntity, "completelyDefined");
        utstyrBinding.bindReactiveButton(opprettButton);
        
        // ikke bind fritext fordi den slår på lagreknappen uavhengig av valg av gruppe osv.
        //utstyrBinding.bindTextInputArea(fritekstArea, mipssEntity.getClass(), mipssEntity, "fritekst");

        utstyrBinding.bind();
    }
    
    /**
     * Setter opp kaskadekobling for tre combobokser og for sensortyper. 
     */
    private void initComboBoxes() {
        // instantier en kjede av master-slave comboboxer 
//        individIdComboWrapper = new MipssComboBoxWrapper<Utstyrindivid>(null, individComboBox);
        modellComboWrapper = new MipssComboBoxWrapper<Utstyrmodell>(null, modellComboBox);//,individIdComboWrapper
        subgruppeComboWrapper = new MipssComboBoxWrapper<Utstyrsubgruppe>(null, subgruppeComboBox, modellComboWrapper);
        gruppeComboWrapper = new MipssComboBoxWrapper<Utstyrgruppe>(PanelFactory.getCacheHolder().getUtstyrgruppeList(), gruppeComboBox, subgruppeComboWrapper);
        sensortypeComboWrapper = new MipssComboBoxWrapper<Sensortype>(PanelFactory.getCacheHolder().getSensortypeList(), sensortypeComboBox);
        
        // action for valg i utstyrgruppe listen
        gruppeComboBox.addActionListener(new MasterSlaveComboListener<Utstyrgruppe, Utstyrsubgruppe>(gruppeComboWrapper, subgruppeComboWrapper, Utstyrgruppe.class) {
                    public void reinitSlave(MipssComboBoxWrapper<Utstyrsubgruppe> slave, Long id, int index) {
                        logger.trace("reinitialisering av gruppe kalt med id " + id);
                        
                        Utstyrgruppe valgtGruppe = gruppeComboWrapper.getList().get(index);
                        
                        // oppdater kjoretoyUtstyr med den valgte gruppen
                        mipssEntity.setUtstyrgruppe(valgtGruppe);
                        
                        // hent alle subgrupper i den valgte gruppen
                        slave.setList(bean.getUtstyrsubgruppeListFor(id));
                        
                        // kontrollerer tilgjengelighet for alle radiobuttons
                        ioButtonGroup.setUtstyrgruppe(valgtGruppe);
                        updateWarningMessages();
                    }

                    public void clearSlave(MipssComboBoxWrapper<Utstyrsubgruppe> slave) {
                        // fjern gruppen fra kjoretoyUtstyr
                        mipssEntity.setUtstyrgruppe(null);
                        
                        // slår av alle radiobuttons
                        ioButtonGroup.setUtstyrgruppe(null);
                        updateWarningMessages();
                    }
                });

        // action for valg i subgruppelisten
        subgruppeComboBox.addActionListener(new MasterSlaveComboListener<Utstyrsubgruppe, Utstyrmodell>(subgruppeComboWrapper, modellComboWrapper, Utstyrsubgruppe.class) {
                    public void reinitSlave(MipssComboBoxWrapper<Utstyrmodell> slave, Long id, int index) {
                        logger.trace("reinitialisering av subgruppe kalt med id " + id);
                        
                        Utstyrsubgruppe valgt = subgruppeComboWrapper.getList().get(index);
                        
                        // oppdater entiteten med den nye subgruppen
                        mipssEntity.setUtstyrsubgruppe(valgt);
                        
                        // hent alle modeller som er i den valgte subgruppen
                        slave.setList(bean.getUtstyrmodellListFor(id));
                        
                        // velg i den nye modelllisten basert på den eksisterende modellen i entiteten
                     // trengs ikke for nye forekomster: slave.setMipssSelection(mipssEntity.getUtstyrmodellForGui());
                        updateWarningMessages();
                    }

                    public void clearSlave(MipssComboBoxWrapper<Utstyrmodell> slave) {
                        // fjern subgruppen fra entiteten
                        mipssEntity.setUtstyrsubgruppe(null);
                        updateWarningMessages();
                    }
                });
        modellComboBox.addActionListener(new MasterComboListener<Utstyrmodell>(modellComboWrapper) {
		      /**
		       * Utløser bindingene som blanker ut felt for individet
		       */
        	protected void serveNoselection() {
        		logger.trace("serveNoselection for Utstyrindivid kalt");
        		mipssEntity.setUtstyrmodell(null);
//        		maskinnummerField.setText("");
        		updateWarningMessages();
        	}
        	/**
        	 * Utlåser bindingene som viser felt fra det valgte individet
        	 */
        	protected void serveSelection(Utstyrmodell m) {
        		logger.trace("serveSelection for Utstyrindivid kalt med individ " + m.getTextForGUI());
        		mipssEntity.setUtstyrmodell(m);
//        		maskinnummerField.setText(m.getMaskinnummerString());
        		updateWarningMessages();
        	}
        });
        // action for valg i modellisten
//        modellComboBox.addActionListener(new MasterSlaveComboListener<Utstyrmodell, Utstyrindivid>(modellComboWrapper, individIdComboWrapper, Utstyrmodell.class) {
//                    /**
//                     * Setter ny individliste i sin combobox og blanker ut alle individfelt
//                     */
//                    public void reinitSlave(MipssComboBoxWrapper<Utstyrindivid> slave, Long id, int index) {
//                        logger.trace("reinitialisering av modell kalt med id " + id);
//
//                        Utstyrmodell valgt = modellComboWrapper.getList().get(index);
//                        
//                        // oppdater entiteten med den nye modellen
//                        mipssEntity.setUtstyrmodell(valgt);
//                        
//                        // hent alle individer i den valgte modellen
//                        slave.setList(bean.getUtstyrindividListFor(id));
//                        
//                        // velg i den nye listen basert på det eksisterende individet
//                        // trengs ikke for nye forekomster: slave.setMipssSelection(mipssEntity.getUtstyrindividForGui());
//                        updateWarningMessages();
//                    }
//
//                    public void clearSlave(MipssComboBoxWrapper<Utstyrindivid> slave) {
//                        // fjern modellenfra entiteten
//                        mipssEntity.setUtstyrmodell(null);
//                        updateWarningMessages();
//                    }
//                });
//
//        // action for valg i individlisten
//        individComboBox.addActionListener(new MasterComboListener<Utstyrindivid>(individIdComboWrapper) {
//                    /**
//                     * Utløser bindingene som blanker ut felt for individet
//                     */
//                    protected void serveNoselection() {
//                        logger.trace("serveNoselection for Utstyrindivid kalt");
//                        mipssEntity.setUtstyrindivid(null);
//                        maskinnummerField.setText("");
//                        updateWarningMessages();
//                    }
//                    /**
//                     * Utlåser bindingene som viser felt fra det valgte individet
//                     */
//                    protected void serveSelection(Utstyrindivid m) {
//                        logger.trace("serveSelection for Utstyrindivid kalt med individ " + m.getTextForGUI());
//                        mipssEntity.setUtstyrindivid(m);
//                        maskinnummerField.setText(m.getMaskinnummerString());
//                        updateWarningMessages();
//                    }
//                });
                
        // action for valg av sensortype
         sensortypeComboBox.addActionListener(new MasterComboListener<Sensortype>(sensortypeComboWrapper) {
                    protected void serveNoselection() {
                        selectedSensor = null;
                        ioButtonGroup.clearSelection();
                        
                        // utlåser generell disabling
                        ioButtonGroup.setSensortype(selectedSensor);
                        
                        ioTable.clearSelection();
                        ioTable.repaint();
                        updateWarningMessages();
                    }

                    protected void serveSelection(Sensortype m) {
                        selectedSensor = m;
                        
                        // utlåser at radiobuttons revurderer sin tilgjengelighet
                        ioButtonGroup.setSensortype(selectedSensor);
                        
                        updateWarningMessages();
                    }
                });
    }
    
    private Dfuindivid getAktivDfuFromKjoretoy(Kjoretoy kjoretoy){
    	List<Installasjon> installasjonList = kjoretoy.getInstallasjonList();
    	
    	Date now = Clock.now();
    	for (Installasjon i:installasjonList){
    		//noe feil med installasjonen..
    		if (i.getAktivTilDato()==null&&i.getAktivFraDato()==null){
    			continue;
    		}
    		//installasjonen har ingen til dato (=en gang i fremtiden)
    		if (i.getAktivTilDato()==null&&i.getAktivFraDato().before(now)){
    			return i.getDfuindivid();
    		}
    		if (i.getAktivFraDato().before(now)&&i.getAktivTilDato().after(now)){
    			return i.getDfuindivid();
    		}
    	}
    	return null;
    }
    /**
     * initialiserer iolisten for å vises i tabllen, samt DFU serienummer
     */
    private void initIolisteList() {
        // hent iolisten fra databasen
    	iolisteList = bean.getIolisteListForKjoretoy(mipssEntity.getKjoretoy().getId());
    	
    	// hent DFU for å vise den id.
        Dfuindivid dfu = null;
        try {
            dfu  = bean.getDfuindividForKjoretoy(mipssEntity.getKjoretoy().getId());
            
            //
            if (dfu==null&&!enkelVisning){//enkeltVisning==false indikerer wizard fra admin..
            	dfu = getAktivDfuFromKjoretoy(mipssEntity.getKjoretoy());
            	if (dfu!=null){
            		iolisteList = dfu.getDfumodell().getIolisteList();
            	}
            }
        }
        catch (Exception e) {
        	logger.warn("Kunne ikke hente DFU: " + e);
        	dfuEnhet.setText("");
        }
        finally {
	        if (dfu != null) {
	            dfuEnhet.setText(dfu.getTextForGUI());
	        }
	        if (iolisteList == null || iolisteList.size() == 0) {
	        	if(iolisteList == null) {
	        		iolisteList = new Vector<Ioliste>();
	        	}
	            ioTableLabel.setText(getBundle().getString("utstyr.ny.ingenPortListe"));
	            datafangstTitle.setText(getBundle().getString("utstyr.ny.ikkeKnyttTilDfu"));
	            sensortypeComboBox.setEnabled(false);
	            iolisteFraDato.setEnabled(false);
	            iolisteTilDatoCheckBox.setEnabled(false);
	            iolisteTilDato.setEnabled(false);
	        }
        }
    }
    
    /**
     * Initialiserer tabellen med Ioliste og relaterte componenter: comboboxen for Sensortype,
     * samt datoer for Iolisten.
     * 
     * Prøver å hente IOListen for kjøretøyet på to måter:
     * Først ved å hente Dfuindividet med sin modell som i sin tur har sin IOliste. Hvis individet
     * ikke er gistrert ellr har ikek en definert modell, pørver man å hente IOlisten direkte fra 
     * setter opp IO tabellen og fyller den med IOlisten som tilsvarer DFU installert op kjøretøyet.
     */
    private void initIoTable() {
        // definer kolonner
        List<MipssTableColumn> list = new ArrayList<MipssTableColumn>();
        list.add(new MipssTableColumn("", new IolisteRadioButton(), true));
        list.add(new MipssTableColumn(getBundle().getString("utstyr.ny.ioliste.kategori"),  "${dfumodell.dfukategori.navn}", false));
        list.add(new MipssTableColumn(getBundle().getString("utstyr.ny.ioliste.dfuModell"), "${dfumodell.navn}", false));
        list.add(new MipssTableColumn(getBundle().getString("utstyr.ny.ioliste.ioNavn1"),   "navn1", false));
        list.add(new MipssTableColumn(getBundle().getString("utstyr.ny.ioliste.ioNavn2"),   "navn2", false));
        list.add(new MipssTableColumn(getBundle().getString("utstyr.ny.ioliste.teknologi"), "${ioteknologi.navn}", false));
        list.add(new MipssTableColumn(getBundle().getString("utstyr.ny.ioliste.bitNr"),     "bitNr", false));
        
        // init tabellen med kolonner og rader
        EditableMippsTableModel<Ioliste> model = new EditableMippsTableModel<Ioliste>(Ioliste.class, iolisteList, list);
        ioTable.setModel(model);
        
        // den fårste kolonnen er spesiell
        TableColumn column0 = ioTable.getColumnModel().getColumn(0);

        column0.setCellEditor(new JRadioButtonCellEditor());
        column0.setCellRenderer(new JRadioButtonRenderer());
        column0.setPreferredWidth(26);
        column0.setResizable(false);
        column0.setMaxWidth(26);
        
        // sørger for at den valgte linjen slår på sin RadioButton
        ioTable.getSelectionModel().addListSelectionListener(new SingleLineListSelectionListener() {
			@Override
			public void selectionServiceMethod(int index) {
				Enumeration<AbstractButton> e = ioButtonGroup.getElements();
				logger.debug("Indeks for valgt ioliste: " + index);

				for(int i = 0; e.hasMoreElements(); i++) {
					JRadioButton r = (JRadioButton) e.nextElement();

					if (index == i) {
						if(r.isEnabled()) {
							r.setSelected(true);
						}
						else {
							ioTable.clearSelection();
						}
						ioTable.repaint();
						return;
					}
				}
			}        	
        });
        
        KjoretoyUtils.initDefaultTableSettings(ioTable);
        
        // tabellen er utilgjengelig inntil den får sine rader
        ioTable.setEnabled(false);
    }
    
    /**
     * kontrollerer valget av en RadioButton i Ioliste tabellen
     */
    public class JRadioButtonCellEditor extends AbstractCellEditor implements TableCellEditor {
		private static final long serialVersionUID = 1L;
		private JRadioButton current = null;

        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            if(row >= ioButtonGroup.getButtonCount()) {
            	IolisteRadioButton b = (IolisteRadioButton)value;
                
                // hver radioButton vet hvilken Ioliste entitet den hårer til
            	b.setKjoretoyUtstyr(mipssEntity);
                b.setIoliste(iolisteList.get(row));
                
                // koble gruppen med sin ny radiobutton, inkluderte callbackreferanse
                ioButtonGroup.add(b);
                b.setButtonGroup(ioButtonGroup);
//                b.checkAvailability(selectedSensor, mipssEntity.getUtstyrgruppeForGui(), utstyrFraDato.getDate(), utstyrTilDato.getDate(), iolisteFraDato.getDate(), iolisteTilDato.getDate());
            }
            current = (JRadioButton)value;
            return (Component)value;
        }

        public Object getCellEditorValue() {
            return current;
        }
    }

    /**
     * Kontrollerer visningen av RadioButtons i Ioliste tabellen og
     * reagerer på valg i Ioliste tabellen og legger den valgte Iolisten til 
     * KjoretoyUtstyr.
     */
    private class JRadioButtonRenderer implements  TableCellRenderer /*, ActionListener, ChangeListener*/ {
    	@Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if(row >= ioButtonGroup.getButtonCount()) {
                IolisteRadioButton b = (IolisteRadioButton)value;
                if (b==null)
                	return null;
                b.setBackground(Color.WHITE);
                
                // hver radioButton settes opp med sin ioliste og en ref til kjoretoyUtstyr
                b.setKjoretoyUtstyr(mipssEntity);
                b.setIoliste(iolisteList.get(row));

                // koble gruppen med sin ny radiobutton, inkluderte callbackreferanse
                ioButtonGroup.add(b);
                b.setButtonGroup(ioButtonGroup);
//                b.checkAvailability(selectedSensor, mipssEntity.getUtstyrgruppeForGui(), utstyrFraDato.getDate(), utstyrTilDato.getDate(), iolisteFraDato.getDate(), iolisteTilDato.getDate());
            }
            return (Component)value;
        }
    }

    /**
     * initialiserer de to knapper for lagring/avvisning av nytt kjåretåyutstyr
     */
    private void initButtons() {
        opprettButton.setEnabled(false);
        opprettButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveChanges();
                closeDialog();
            }
        });
        
        forkastButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (! saveChangesAndContinue()) {
                    closeDialog();
                }
            }
        });
    }

    protected void initComponentsPresentation() {
        initComponents();
        gruppeMessage.setForeground(Color.RED);
//        individMessage.setForeground(Color.RED);
        sensortypeMessage.setForeground(Color.RED);
        sensortypeMessage.setVisible(false);
        
//        KjoretoyUtils.setReadOnly(maskinnummerField);
        KjoretoyUtils.setReadOnly(dfuEnhet);
        KjoretoyUtils.setBold(gruppeTitle);
        KjoretoyUtils.setBold(individTitle);
        KjoretoyUtils.setBold(beskrivelseTitle);
        KjoretoyUtils.setBold(datafangstTitle);
        
        iolisteTilDatoCheckBox.setSelected(false);
//        utstyrTilDatoCheckBox.setSelected(false);

        initComboBoxes();
        initButtons();
        initIolisteList();
        initIoTable();
        ioButtonGroup = new IolisteButtonGroup(ioTable);
        initBindings();
        
        // disse fire sendes over her fordi lyttere ennå ikke er tatt i bruk
//        ioButtonGroup.setPeriodeFraDato(utstyrFraDato.getDate());
//        ioButtonGroup.setPeriodeTilDato(utstyrTilDato.getDate());
        ioButtonGroup.setIolisteFraDato(iolisteFraDato.getDate());
        ioButtonGroup.setIolisteTilDato(iolisteTilDato.getDate());
    }

    /**
     * ikke i bruk idenne klassen fordi vi ikke bytter entiteter.
     */
    protected void loadFromEntity() {
    }

    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
    
    public void enableUpdating(boolean canUpdate) {
    }

    /**
     * Lagrer i databasen og oppdaterer den interne mipssEntity.
     */
    public void saveChanges() {
        try {
        	// fritekst er ikke bundet
//        	mipssEntity.setFritekst(fritekstArea.getText());

            //mipssEntity = bean.addKjoretoyUtstyr(mipssEntity);
            saved = true;
        }
        catch(IllegalStateException ise) {
        	KjoretoyUtils.showCorectionInfo(getBundle().getString("utstyr.ny.ingenBitNr"));
        	saved = false;
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, getBundle().getString("utstyr.ingenNyLagring"));
            saved = false;
        }
    }
    
    protected void discardChanges() {
    	Kjoretoy k = mipssEntity.getKjoretoy();
    	k.setKjoretoyUtstyrList(bean.getKjoretoyUtstyrListForKjoretoy(k.getId()));
    }
    
    protected boolean isSaved() {
    	return saved;
    }

    public boolean saveChangesAndContinue() {
    	logger.debug("Endringene. Utstyr: " + utstyrBinding.isFieldsChanged()/* + ". Periode: " + periodeBinding.isFieldsChanged()*/);
        if(utstyrBinding.isFieldsChanged() /*|| periodeBinding.isFieldsChanged()*/) {
            Container browser = this.getParent();
            
            // vis et dialogvindu for å spårre brukeren
            KjoretoyUtils.DialogElection elected = KjoretoyUtils.showLagreForkastDialog(this, 
                getBundle().getString("dialog.info.lagring"), 
                getBundle().getString("dialog.title.lagring"));
                
            switch (elected) {
                case LAGRE:
                    saveChanges();
                    return false;
                case FORKAST:
                	discardChanges();
                    return false;
                case FORTSETT:
                    return true;
                default:
                    return false;
            }
        }
        else {
            return false;
        }
    }
    
    private void initComponents() {
        pnlVelgGruppeOgModell = new JPanel();
        gruppeTitle = new JLabel();
        gruppeMessage = new JLabel();
        gruppeComboBox = new JComboBox();
        subgruppeComboBox = new JComboBox();
        modellComboBox = new JComboBox();
//        pnlVelgIndivid = new JPanel();
        individTitle = new JLabel();
//        individComboBox = new JComboBox();
//        maskinnummerField = new JTextField();
//        individMessage = new JLabel();
//        pnlBeskrivelseOgPeriode = new JPanel();
        beskrivelseTitle = new JLabel();
//        scrlPaneFritekst = new JScrollPane();
//        fritekstArea = new JTextArea();
//        lblAktivFra = new JLabel();
//        utstyrFraDato = new JDateTimePicker();
//        utstyrTilDatoLabel = new JLabel();
//        utstyrTilDato = new JDateTimePicker();
        utstyrPeriodeMessage = new JLabel();
//        utstyrTilDatoCheckBox = new JCheckBox();
        pnlDatafangstEnhet = new JPanel();
        datafangstTitle = new JLabel();
        lblSensorType = new JLabel();
        sensortypeComboBox = new JComboBox();
        scrlPaneIoTable = new JScrollPane();
        ioTable = new JTable();
        ioTableLabel = new JLabel();
        lblDfuEnhet = new JLabel();
        dfuEnhet = new JTextField();
        sensortypeMessage = new JLabel();
        iolisteTilDato = new JDateTimePicker(null);
        lblDfuAktivFra = new JLabel();
        iolisteTilDatoLabel = new JLabel();
        iolisteFraDato = new JDateTimePicker();
        iolisteTilDatoCheckBox = new JCheckBox();
        opprettButton = new JButton();
        forkastButton = new JButton();
        pnlButtons = new JPanel();

        pnlVelgGruppeOgModell.setBorder(BorderFactory.createEtchedBorder());
        gruppeTitle.setText(getBundle().getString("utstyr.ny.gruppeTitle")); // NOI18N
        gruppeMessage.setHorizontalAlignment(SwingConstants.TRAILING);
        gruppeComboBox.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        subgruppeComboBox.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        modellComboBox.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
//        pnlVelgIndivid.setBorder(BorderFactory.createEtchedBorder());
        individTitle.setText(getBundle().getString("utstyr.ny.velgEnhetTitle")); // NOI18N
//        individComboBox.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
//        individMessage.setHorizontalAlignment(SwingConstants.TRAILING);
//        pnlBeskrivelseOgPeriode.setBorder(BorderFactory.createEtchedBorder());
        beskrivelseTitle.setText(getBundle().getString("utstyr.ny.fritekstTitle")); // NOI18N
//        fritekstArea.setColumns(20);
//        fritekstArea.setRows(5);
//        scrlPaneFritekst.setViewportView(fritekstArea);
//        scrlPaneFritekst.setMinimumSize(new Dimension(200, 100));
//        scrlPaneFritekst.setPreferredSize(new Dimension(200, 100));
//        fritekstArea.setDocument(new DocumentFilter(4000, null, false));
//
//        lblAktivFra.setText(getBundle().getString("utstyr.ny.aktivFra")); // NOI18N
//        utstyrTilDatoLabel.setHorizontalAlignment(SwingConstants.TRAILING);
//        utstyrTilDatoLabel.setText(getBundle().getString("utstyr.ny.aktivTil")); // NOI18N
        utstyrPeriodeMessage.setHorizontalAlignment(SwingConstants.TRAILING);
//        utstyrTilDatoCheckBox.setText(getBundle().getString("gen.enableDatoTil")); // NOI18N
        pnlDatafangstEnhet.setBorder(BorderFactory.createEtchedBorder());
        datafangstTitle.setText(getBundle().getString("utstyr.ny.knyttTilDfu")); // NOI18N
        lblSensorType.setHorizontalAlignment(SwingConstants.TRAILING);
        lblSensorType.setText(getBundle().getString("utstyr.ny.sensortype")); // NOI18N
        scrlPaneIoTable.setViewportView(ioTable);
        ioTableLabel.setText(getBundle().getString("utstyr.ny.velgIO")); // NOI18N
        lblDfuEnhet.setText(getBundle().getString("utstyr.ny.dfuEnhet")); // NOI18N
        sensortypeMessage.setHorizontalAlignment(SwingConstants.TRAILING);
        sensortypeMessage.setText(getBundle().getString("utstyr.melding.sensorKreves")); // NOI18N
        lblDfuAktivFra.setText(getBundle().getString("utstyr.ny.aktivFra")); // NOI18N
        iolisteTilDatoLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        iolisteTilDatoLabel.setText(getBundle().getString("utstyr.ny.aktivTil")); // NOI18N
        iolisteTilDatoCheckBox.setText(getBundle().getString("gen.enableDatoTil")); // NOI18N
        opprettButton.setText(getBundle().getString("utstyr.ny.okButton")); // NOI18N
        forkastButton.setText(getBundle().getString("utstyr.ny.avbrytButton")); // NOI18N
        pnlVelgGruppeOgModell.setBorder(BorderFactory.createTitledBorder(getBundle().getString("utstyr.ny.gruppeTitle")));
        pnlVelgGruppeOgModell.setLayout(new GridBagLayout());
        pnlVelgGruppeOgModell.add(gruppeMessage, new GridBagConstraints(0, 0, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlVelgGruppeOgModell.add(gruppeComboBox, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnlVelgGruppeOgModell.add(subgruppeComboBox, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlVelgGruppeOgModell.add(modellComboBox, new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
//        pnlVelgIndivid.setBorder(BorderFactory.createTitledBorder(getBundle().getString("utstyr.ny.velgEnhetTitle")));
//        pnlVelgIndivid.setLayout(new GridBagLayout());
//        pnlVelgIndivid.add(individMessage, new GridBagConstraints(0, 0, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
//        pnlVelgIndivid.add(individComboBox, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
//        pnlVelgIndivid.add(maskinnummerField, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
//        pnlBeskrivelseOgPeriode.setBorder(BorderFactory.createTitledBorder(getBundle().getString("utstyr.ny.fritekstTitle")));
//        pnlBeskrivelseOgPeriode.setLayout(new GridBagLayout());
//        pnlBeskrivelseOgPeriode.add(scrlPaneFritekst, new GridBagConstraints(0, 0, 5, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
//        pnlBeskrivelseOgPeriode.add(lblAktivFra, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
//        pnlBeskrivelseOgPeriode.add(utstyrFraDato, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
//        
//        if(!enkelVisning) {
//        	pnlBeskrivelseOgPeriode.add(utstyrTilDatoCheckBox, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
//        	pnlBeskrivelseOgPeriode.add(utstyrTilDatoLabel, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
//        	pnlBeskrivelseOgPeriode.add(utstyrTilDato, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
//        }
        
        pnlDatafangstEnhet.setBorder(BorderFactory.createTitledBorder(getBundle().getString("utstyr.ny.knyttTilDfu")));
        pnlDatafangstEnhet.setLayout(new GridBagLayout());
        pnlDatafangstEnhet.add(sensortypeMessage, new GridBagConstraints(0, 0, 5, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(lblDfuEnhet, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(dfuEnhet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(lblSensorType, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(sensortypeComboBox, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(ioTableLabel, new GridBagConstraints(0, 2, 5, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(scrlPaneIoTable, new GridBagConstraints(0, 3, 5, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(lblDfuAktivFra, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(iolisteFraDato, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(iolisteTilDatoCheckBox, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(iolisteTilDatoLabel, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlDatafangstEnhet.add(iolisteTilDato, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        pnlButtons.setLayout(new GridBagLayout());
        pnlButtons.add(opprettButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButtons.add(forkastButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.add(pnlVelgGruppeOgModell, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 5, 11), 0, 0));
//        this.add(pnlVelgIndivid, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 5, 11), 0, 0));        
        
        if(enkelVisning) {
//        	this.add(pnlBeskrivelseOgPeriode, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 5, 11), 0, 0));
        } else {
//    	 	this.add(pnlBeskrivelseOgPeriode, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 5, 11), 0, 0));
        	this.add(pnlDatafangstEnhet, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 11, 11), 0, 0));
        }
        
        this.add(pnlButtons, new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
    }

	@Override
	protected Dimension getDialogSize() {
		return enkelVisning ? new Dimension(600, 200) : new Dimension(600, 400);
	}
}
