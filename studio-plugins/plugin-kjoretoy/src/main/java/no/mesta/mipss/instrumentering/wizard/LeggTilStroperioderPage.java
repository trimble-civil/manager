package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;
import java.awt.Container;

import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyOrdreListPanel;
import no.mesta.mipss.kjoretoy.component.stroing.StroperioderListPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.netbeans.spi.wizard.WizardPage;

@SuppressWarnings("serial")
public class LeggTilStroperioderPage extends WizardPage {
	private MipssPlugin mipssPlugin;
	private WizardLine wizardLine;
	private Container parent;
	private boolean enkelWizard;
	
	private StroperioderListPanel stroperioderListPanel;
	
	public LeggTilStroperioderPage(MipssPlugin mipssPlugin, WizardLine wizardLine, Container parent, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard) {
		super(stepId, stepDescription, autoListen);
		this.mipssPlugin = mipssPlugin;
		this.wizardLine = wizardLine;
		this.parent = parent;
		this.enkelWizard = enkelWizard;
		
		initGui();
	}
	
	private void initGui() {
		this.setLayout(new BorderLayout());
	}
	
	@Override
	protected void renderingPage() {
		if(stroperioderListPanel == null) {
			stroperioderListPanel = new StroperioderListPanel(wizardLine, null, wizardLine.getKjoretoy(), parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), mipssPlugin);
			this.add(stroperioderListPanel, BorderLayout.CENTER);
		}
	}
}
