package no.mesta.mipss.kjoretoy.component.installasjon;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.Digidok;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXHeader;

@SuppressWarnings("serial")
public class DigidokPanel extends JPanel{

	private List<DigidokCheck> porter;
	private String ignorering;
	private BindingGroup bg = new BindingGroup();
	private JLabel lblDigi;
	private JTextField txtDigi;
	private JXHeader pnlHeader;
	
	public DigidokPanel(List<Digidok> porter, String ignorering){
		
		this.ignorering = ignorering;
		
		Collections.sort(porter, new Comparator<Digidok>() {
			@Override
			public int compare(Digidok o1, Digidok o2) {
				return o1.getBitNr().compareTo(o2.getBitNr());
			}
		});
		
		this.porter = new ArrayList<DigidokCheck>();
		for (Digidok p:porter){
			DigidokCheck c = new DigidokCheck(p);
			StringBuilder sb = new StringBuilder(String.valueOf(p.getBitNr()));
			sb.append(", ");
			sb.append(p.getFritekst());
			c.setText(sb.toString());
			c.setToolTipText(p.getFritekst());
			this.porter.add(c);
			bg.addBinding(BindingHelper.createbinding(c, "selected", this, "checkUpdate"));
		}
		initComponents();
		initGui();
		if (ignorering!=null){
			setSelections();
		}
		bg.bind();
	}
	/**
	 * Konverterer ignoreringsmasken til valg av checkbokser.
	 */
	private void setSelections(){
		Integer hexIgnore = Integer.valueOf(ignorering, 16);
		String binaryString = Integer.toBinaryString(hexIgnore);
		int co =0;
		for (int i=binaryString.length()-1; i >= 0;--i){
			if (binaryString.charAt(i)=='0'){
				getDigiAtBitPos(co).setSelected(true);
			}
			co++;
		}
	}
	private DigidokCheck getDigiAtBitPos(int pos){
		for (DigidokCheck d:porter){
			if (d.getPort().getBitNr()==pos){
				return d;
			}
		}
		return null;
	}
	/**
	 * For bindings, når en checkboks oppdateres skal masken regenereres
	 * @param checked
	 */
	public void setCheckUpdate(boolean checked){
		calc();
	}
	/**
	 * Beregner ny ignoreringsmaske
	 */
	private void calc(){
		int value = 0;
		for (DigidokCheck d:porter){
			value += !d.isSelected()?(1<<d.getPort().getBitNr()):0;
		}
		ignorering = Integer.toHexString(value).toUpperCase();
		txtDigi.setText(ignorering);
	}
	
	public void dispose(){
		bg.unbind();
	}
	
	private void initGui() {
		double size = porter.size();
		double cols = 2;
		double rows = Math.ceil(size/cols);
		
		JPanel pnlPorter = new JPanel();
		pnlPorter.setLayout(new GridLayout((int)rows, (int)cols));
		for (DigidokCheck d:porter){
			pnlPorter.add(d);
		}
		
		setLayout(new GridBagLayout());
		JPanel pnlIgnorering = new JPanel(new GridBagLayout());
		pnlIgnorering.add(lblDigi, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5),0,0));
		pnlIgnorering.add(txtDigi, 		new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,5),0,0));
		
		add(pnlHeader, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,0),0,0));
		add(pnlIgnorering, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0),0,0));
		add(pnlPorter, 		new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, 	  new Insets(5,5,5,5),0,0));
	}
	
	private void initComponents(){
		lblDigi = new JLabel(KjoretoyUtils.getPropertyString("label.genignore"));
		txtDigi = new JTextField();
		txtDigi.setEditable(false);
		
		pnlHeader = new JXHeader(KjoretoyUtils.getPropertyString("label.ignoreHeader.title"), KjoretoyUtils.getPropertyString("label.ignoreHeader.desc"));
	}
	
	public String getIgnorering(){
		return ignorering;
	}
	
	/**
	 * Klasse for å holde styr på hvilken digidok som tilhører hvilken checkbox
	 * @author harkul
	 *
	 */
	private class DigidokCheck extends JCheckBox{
		private Digidok port;
		public DigidokCheck(Digidok port){
			this.port = port;
		}
		public Digidok getPort(){
			return port;
		}
		
	}
}
