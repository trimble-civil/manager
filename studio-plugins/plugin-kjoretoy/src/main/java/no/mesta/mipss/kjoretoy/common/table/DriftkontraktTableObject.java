package no.mesta.mipss.kjoretoy.common.table;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

public class DriftkontraktTableObject implements IRenderableMipssEntity {
	private Driftkontrakt driftkontrakt;
	private Boolean valgt;
	
	public DriftkontraktTableObject(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
		valgt = Boolean.FALSE;
	}
	
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}
	
	public Boolean getValgt() {
		return valgt;
	}

	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}

	@Override
	public String getTextForGUI() {
		return driftkontrakt.getTextForGUI();
	}

}
