/**
 * 
 */
package no.mesta.mipss.kjoretoy.common;

import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.JRadioButton;

import no.mesta.mipss.kjoretoy.util.IOPortValidator;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.PeriodeIoMonitor;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Spesialisert {@code JRadioButton} som vet hvilken {@code Ioliste} den representerer
 * på skjermen.
 * Har logikk for å legge til og fjerne sin {@code Ioliste} til/fra den {@code KjoretoyUtstyr} 
 * som er satt.
 * 
 * Denne klassen avhenger av at KjoretoyUtstyr den har en referanse til, er alltid tiden 
 * ajourført med referanser til den valgte gruppen og fra/til datoene.
 * 
 * @author jorge
 */
public class IolisteRadioButton extends JRadioButton implements Cloneable {
	private static final long serialVersionUID = 7438946058020638231L;
	private Logger logger = LoggerFactory.getLogger(IolisteRadioButton.class);
	
	private Ioliste myIoliste = null;
	private KjoretoyUtstyr kjoretoyUtstyr = null;
	private IolisteButtonGroup buttonGroup = null;
	
	// instans opprettet her og lagt til listen
	private KjoretoyUtstyrIo kjoretoyUtstyrIo = null;
	private String loggedOnUser;
	
	/**
	 * Oppretter ny instans uten videre.
	 */
	public IolisteRadioButton() {
		super();
	}
	public void setLoggedOnUser(String loggedOnUser){
		this.loggedOnUser = loggedOnUser;
	}
	
	@Override
	public void setSelected(boolean selected) {
		if(buttonGroup == null) {
			throw new IllegalStateException("Mangler ButtonGroup for å velge (bort) en radioButton");
		}

		if(selected) {
			buttonGroup.clearSelection();
			addToList(buttonGroup.getSensortype(), buttonGroup.getUtstyrgruppe(), buttonGroup.getIolisteFraDato(), buttonGroup.getIolisteTilDato());
		}
		else {
			removeFromList();
		}
		super.setSelected(selected);
	}
	
	/**
	 * Fjerner {@code Ioliste}n denne knappen besitter fra listen.
	 * Kallet ignoreres hvis denne knappens {@code Ioliste} eller listen er null.
	 */
	public void removeFromList() {
		if(kjoretoyUtstyr == null || myIoliste == null) {
			logger.warn("Mangelfull initialisering");
			return;
		}
		if(kjoretoyUtstyr.getKjoretoyUtstyrIoList() == null) {
			logger.warn("Mangelfull initialisering: savner listen med Iolister");
			return;
		}
		if(kjoretoyUtstyrIo != null) {
			logger.debug("fjerner ioliste: " + myIoliste);
			kjoretoyUtstyr.getKjoretoyUtstyrIoList().remove(kjoretoyUtstyrIo);
			kjoretoyUtstyrIo = null;
		}
		
	}
	
	@Override
	public IolisteRadioButton clone() {
		return new IolisteRadioButton();
	}
	
	/**
	 * Legger til den {@code Ioliste}n denne knappen besitter til listen.
	 * Kallet ignoreres hvis denne knappens {@code Ioliste} eller listen er null.
	 * @param sensor som er valgt 
	 * @param gruppe som er valgt 
	 * @param fraDato satt av brukeren
	 * @param tilDato satt av brukeren
	 * @throws IllegalStateException hvis BIT_NR ikke kan settes.
	 */
	public void addToList(Sensortype sensor, Utstyrgruppe gruppe, Date fraDato, Date tilDato) {
		
		logger.debug("legger til Ioliste: " + myIoliste);
		if(kjoretoyUtstyr == null || myIoliste == null) {
			logger.warn("Mangelfull initialisering");
			return;
		}
		if(kjoretoyUtstyr.getKjoretoyUtstyrIoList() == null) {
			logger.warn("Mangelfull initialisering: savner listen med Iolister");
			return;
		}
		// opprett full struktur ifølge modellen
		kjoretoyUtstyrIo = new KjoretoyUtstyrIo();
		kjoretoyUtstyrIo.setSensortype(sensor);
		kjoretoyUtstyrIo.setIoliste(myIoliste);
		kjoretoyUtstyrIo.setKjoretoyUtstyr(kjoretoyUtstyr);
		kjoretoyUtstyrIo.setFraDato(fraDato);
		kjoretoyUtstyrIo.setTilDato(tilDato);
		//TODO ...
//		kjoretoyUtstyrIo.getOwnedMipssEntity().setOpprettetAv(loggedOnUser);
//		kjoretoyUtstyrIo.getOwnedMipssEntity().setOpprettetDato(Clock.now());
        
        if (! setBitNummer(kjoretoyUtstyrIo, gruppe)) {
        	throw new IllegalStateException("Finner ikke BIT_NUMMER for KjoretoyUtstyrIo");
        }

        // legg min ioliste til kjøretøyutstyrs liste
        kjoretoyUtstyr.getKjoretoyUtstyrIoList().add(kjoretoyUtstyrIo);
        
        // valider overlappinger
        PeriodeIoMonitor mon = new PeriodeIoMonitor();
        try {
        	mon.monitorPeriodeIo(kjoretoyUtstyr);
        }
        catch(Exception e) {
        	logger.debug("Fjerner overlapping: " + e);
        	removeFromList();
        }
	}
	
	/**
	 * Velger hvilke bitNr som skal settes i {@code Ioliste}n.
	 * BitNr fra Ioliste}n kommer først. Hvis den er null,
	 * velger man {@code SerieportBitNr} fra {@code Utstyrgruppe}n.
	 * Hvis den siset er null også, har man truffet en alvorlig feil i
	 * datagrunnlaget.
	 * @param kuio
	 * @param io
	 * @param gruppe
	 * @return True hvis velget OK, ellers false.
	 */
	private boolean setBitNummer(KjoretoyUtstyrIo kuio, Utstyrgruppe gruppe) {
    	boolean ok = true;
    	if(myIoliste.getBitNr() != null) {
    		kuio.setBitNr(myIoliste.getBitNr());
    	}
    	else if(gruppe.getSerieportBitNr() != null) {
    		kuio.setBitNr(gruppe.getSerieportBitNr());
    	}
    	else {
    		logger.error("Finner ikke BIT_NUMMER for KjoretoyUtstyrIo. Iolistes ID: " + myIoliste.getId() + ". Utstyrgruppens ID: " + gruppe.getId());
    		ok = false;
    	}
    	return ok;
    }
	
	/**
	 * Kontrollerer om fra/til datoene i denne knappen overlapper med andre. Hvis det
	 * er tilfelle, blir knappen utilgjengelig. 
	 * @param sensor
	 * @param gruppe
	 * @param from
	 * @param to
	 * @returnn TUE hvis knappen tilgjengelig
	 */
	public boolean checkAvailability(Sensortype sensor, Utstyrgruppe gruppe, Date utstyrperiodeFrom, Date utstyrperiodeTo, Date ioPeriodeFrom, Date ioPeriodeTo) {
		logger.debug("Sjekker tilgjengelighet for radiobutton");
		if(kjoretoyUtstyr == null || myIoliste == null) {
			super.setEnabled(false);
			return false;
		}

		if(sensor == null || gruppe == null) {
			super.setEnabled(false);
			return false;
		}
		
		// Har vi en BitNr å bruke?
        if(myIoliste.getBitNr() == null && gruppe.getSerieportBitNr() == null) {
        	super.setEnabled(false);
			return false;
        }
		// Sjekker periodeoverlappinger for hele kjøretøyet
        return checkOverlaps(utstyrperiodeFrom, utstyrperiodeTo, ioPeriodeFrom, ioPeriodeTo);
	}
	
	/**
	 * Validerer fra/til datoene for overlappinger for hele kjøretøyet.
	 * Gjøre denne knappen utilghengelig hvis overlappinger inntreffer.
	 * @param utstyrperiodeFrom
	 * @param utstyrperiodeTo
	 * @return
	 */
	private boolean checkOverlaps(Date utstyrperiodeFrom, Date utstyrperiodeTo, Date ioPeriodeFrom, Date ioPeriodeTo) {
		logger.debug("Sjekker overlapping for radiobutton START");
		if(kjoretoyUtstyr == null || myIoliste == null) {
			super.setEnabled(false);
			logger.debug("Sjekker overlapping for radiobutton disabled STOP: utstyr eller ioliste er null");
			return false;
		}
        
//		PeriodeIoValidator monitor = new PeriodeIoValidator(kjoretoyUtstyr, kjoretoyUtstyrIo);
		IOPortValidator ioPortValidator = new IOPortValidator(kjoretoyUtstyr.getKjoretoy(), myIoliste.getBitNr(), utstyrperiodeFrom, utstyrperiodeTo, ioPeriodeFrom, ioPeriodeTo);
//		if(! monitor.isOverlappingFree(myIoliste.getBitNr(), from, to)) {
		if(!ioPortValidator.isValid()) {
			if(super.isSelected()) {
				setSelected(false);
				buttonGroup.clearSelection();
				super.setEnabled(false);
				ResourceBundle bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
				KjoretoyUtils.showException(bundle.getString("utstyr.ny.overlappingMedNyDato"));
			}
			else {
				super.setEnabled(false);
			}
        	
        	
    		logger.debug("Sjekker overlapping for radiobutton disabled STOP: overlapping funnet");
			return false;
        }

        super.setEnabled(true);
        logger.debug("Sjekker overlapping for radiobutton enabled STOP");
		return true;
	}

	/**
	 * Setter fraDato i den interne kjoretoyUtstyrIo, hvis sen siste ikke er null
	 * @param ioFraDato
	 */
	public void setIolisteFraDato(Date ioFraDato, Date oldIoTilDato, Date oldPeriodeFraDato, Date oldPeriodeTilDato) {
		if(kjoretoyUtstyrIo != null) {
			kjoretoyUtstyrIo.setFraDato(ioFraDato);
		}
		checkOverlaps(oldPeriodeFraDato, oldPeriodeTilDato, ioFraDato, oldIoTilDato);
	}
	
	/**
	 * Setter tilDato i den interne kjoretoyUtstyrIo, hvis sen siste ikke er null
	 * @param ioTilDato
	 */
	public void setIolisteTilDato(Date ioTilDato, Date oldIoFraDato, Date oldPeriodeFraDato, Date oldPeriodeTilDato) {
		if(kjoretoyUtstyrIo != null) {
			kjoretoyUtstyrIo.setTilDato(ioTilDato);
		}
		checkOverlaps(oldPeriodeFraDato, oldPeriodeTilDato, oldIoFraDato, ioTilDato);
	}

	/**
	 * Gjør denne knappen (u)tilgjengelig avhengig av periodeoverlappinger
	 * @param periodeFraDato
	 */
	public void setPeriodeFraDato(Date periodeFraDato, Date oldPeriodeTilDato, Date oldIoFraDato, Date oldIoTilDato) {
		checkOverlaps(periodeFraDato, oldPeriodeTilDato, oldIoFraDato, oldIoTilDato);
	}
	
	/**
	 * Gjør denne knappen (u)tilgjengelig avhengig av periodeoverlappinger
	 * @param periodeTilDato
	 */
	public void setPeriodeTilDato(Date periodeTilDato, Date oldPeriodeFraDato, Date oldIoFraDato, Date oldIoTilDato) {
		checkOverlaps(oldPeriodeFraDato, periodeTilDato, oldIoFraDato, oldIoTilDato);
	}

	/**
	 * @return the myIoliste
	 */
	public Ioliste getIoliste() {
		return myIoliste;
	}

	/**
	 * @param myIoliste the myIoliste to set
	 */
	public void setIoliste(Ioliste myIoliste) {
		this.myIoliste = myIoliste;
	}

	/**
	 * @return the kjoretoyUtstyr
	 */
	public KjoretoyUtstyr getKjoretoyUtstyr() {
		return kjoretoyUtstyr;
	}

	/**
	 * @param kjoretoyUtstyr the kjoretoyUtstyr to set
	 */
	public void setKjoretoyUtstyr(KjoretoyUtstyr kjoretoyUtstyr) {
		this.kjoretoyUtstyr = kjoretoyUtstyr;
	}

	/**
	 * @return the buttonGroup
	 */
	public IolisteButtonGroup getButtonGroup() {
		return buttonGroup;
	}

	/**
	 * @param buttonGroup the buttonGroup to set
	 */
	public void setButtonGroup(IolisteButtonGroup buttonGroup) {
		this.buttonGroup = buttonGroup;
	}
}
