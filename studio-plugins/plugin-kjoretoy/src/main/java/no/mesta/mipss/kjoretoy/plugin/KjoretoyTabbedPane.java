package no.mesta.mipss.kjoretoy.plugin;

import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.component.stroing.StroperioderPanel;
import no.mesta.mipss.kjoretoy.search.SearchView;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Setter opp og kontrollerer visningen av arbeidsfaner for instrumenteringen.
 * Oppsettet av fanene er hardkodet i første omgang.
 * Den første fanen brukes alltid til søkeresultat.
 * Den andre fanen brukes alltid til hovedarbeidsflaten.
 * Den tredje fanen brukes alltid til tilleggsinfo om et kjøretøy.
 * De påfølgfende tabber brukes til ymse andre ting.
 * @author jorge
 */
public class KjoretoyTabbedPane extends JTabbedPane {
	private static final long serialVersionUID = 8279472157893109686L;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private TabBase kjoretoySearch;
    private TabBase kjoretoyMainInfo;
    private TabBase kjoretoyExtraInfo;
    private TabBase kjoretoyMipssLogg;
    private TabBase kjoretoyOrdre;
    private TabBase kjoretoyKontrakt;
    private TabBase kjoretoyStroperioder;
    
    private ResourceBundle bundle;
    private InstrumentationController controller;
    private boolean hasUpdateAccess = false;
    private MipssPlugin mipssPlugin;
    
    public KjoretoyTabbedPane(InstrumentationController controller, boolean hasUpdateAccess, MipssPlugin mipssPlugin) {
        super();
        this.hasUpdateAccess = hasUpdateAccess;
        this.mipssPlugin = mipssPlugin;
        this.controller = controller;
        
        bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
        initTabs();
        
        // fang opp visning av hver fane.
        super.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (KjoretoyTabbedPane.this.controller == null) {
                    logger.error("Controller objektet ikke satt i KjoretoyTabbedPane");
                    throw new IllegalStateException("Controller objektet ikke satt i KjoretoyTabbedPane");
                }
                int tabIndex = getSelectedIndex();
                String title = getTitleAt(tabIndex);
                KjoretoyTabbedPane.this.controller.handleSelectedPanel((TabBase)getSelectedComponent());
            }
        });
    }

    /**
     * Setter opp fanene i endelig rekkefølge.
     */
    private void initTabs() {
    	try {
	        kjoretoySearch = initTab(new TabBase(this, bundle.getString("mainTab.Sokeresultat")), IconResources.SEARCH_ICON);
	        kjoretoySearch.add(new SearchView(null));
	        
	        kjoretoyMainInfo = initTab(new TabBase(this, bundle.getString("mainTab.Identifikasjon")), IconResources.BIL_SMALL_ICON);
	        kjoretoyMainInfo.setKjoretoyView(PanelFactory.createKjoretoyCompleteViewPanel(controller, null, hasUpdateAccess, mipssPlugin.getLoader().getApplicationFrame(), mipssPlugin));
	        
	        kjoretoyExtraInfo = initTab(new TabBase(this, bundle.getString("mainTab.ExtraInfo")), IconResources.DETALJER_SMALL_ICON);
	        kjoretoyExtraInfo.setKjoretoyView(PanelFactory.createKjoretoyInfoViewPanel(controller, null, hasUpdateAccess, mipssPlugin, false));
	        
	        kjoretoyOrdre = initTab(new TabBase(this, bundle.getString("mainTab.Ordre")), IconResources.EDIT_ICON);
	        kjoretoyOrdre.setKjoretoyView(PanelFactory.createKjoretoyOrdreViewPanel(controller, null, mipssPlugin.getLoader().getApplicationFrame(), mipssPlugin));
	        
	        kjoretoyKontrakt = initTab(new TabBase(this, bundle.getString("mainTab.Kontrakt")), IconResources.HISTORY_ICON);
	        kjoretoyKontrakt.setKjoretoyView(PanelFactory.createKjoretoyLevKontraktViewPanel(controller, null, mipssPlugin.getLoader().getApplicationFrame(), false));
	        
	        kjoretoyStroperioder = initTab(new TabBase(this, bundle.getString("mainTab.Stroperioder")), IconResources.SPREDER_ICON);
	        kjoretoyStroperioder.setKjoretoyView(new StroperioderPanel(controller, null, mipssPlugin.getLoader().getApplicationFrame(), KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), mipssPlugin));
	        
			for(int i = 1; i < getTabCount(); i++) {
				setEnabledAt(i, false);
			}
    	}
    	catch (Exception e) {
			KjoretoyUtils.showException(e, KjoretoyUtils.getBundle().getString("gen.ingenPlugin"));
		}
    }
    
    private TabBase initTab(TabBase tab, ImageIcon icon) {
        this.addTab(tab.getLabel(), icon, tab);
        tab.fetchTabIndex();
        return tab;
    }

    public InstrumentationController getController() {
        return controller;
    }

    public TabBase getKjoretoySearch() {
        return kjoretoySearch;
    }

    public TabBase getKjoretoyMainInfo() {
        return kjoretoyMainInfo;
    }

    public TabBase getKjoretoyExtraInfo() {
        return kjoretoyExtraInfo;
    }

    public TabBase getKjoretoyOrdre() {
        return kjoretoyOrdre;
    }

    public TabBase getKjoretoyKontrakt() {
        return kjoretoyKontrakt;
    }
}
