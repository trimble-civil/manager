package no.mesta.mipss.kjoretoy.component.installasjon;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.persistence.datafangsutstyr.Dfu_Status;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EditableMippsTableModel.MipssTableColumn;
import no.mesta.mipss.util.KjoretoyUtils;

/**
 * Panel for visningen av statushistorikken for et DFU
 * @author jorge
 */
public class Dfu_StatusListPanel  extends AbstractMipssEntityPanel<Dfuindivid> {
	private static final long serialVersionUID = -8693308677885255380L;
	private MipssKjoretoy bean = null;
	
	public Dfu_StatusListPanel(Dfuindivid dfuindivid, MipssKjoretoy bean, ResourceBundle bundle) {
        super(dfuindivid);
        this.bean = bean;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("gen.ingenStatusVisning"));
            return;
        }
        
        if (dfuindivid != null) {
            try {
               loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("gen.ingenStatusVisning")); 
            }
        }
    }

	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    protected void initComponentsPresentation() {
        initComponents();
        statusTitle.setText(getBundle().getString("gen.statusList.title")); // NOI18N
        
        // init tabellkolonner
        List<MipssTableColumn> list = new ArrayList<MipssTableColumn>();
        list.add(new MipssTableColumn(getBundle().getString("dfu.enhet.status"),     "statusNavn", false));
        list.add(new MipssTableColumn(getBundle().getString("dfu.enhet.opprettet"),  "opprettetDato", false));
        list.add(new MipssTableColumn(getBundle().getString("dfu.enhet.opprettetAv"),  "opprettetAv", false));
        list.add(new MipssTableColumn(getBundle().getString("dfu.enhet.levbestilling"),"levbestId", false));

        // sett opp tabellen
        EditableMippsTableModel<Dfu_Status> model = new EditableMippsTableModel<Dfu_Status>(Dfu_Status.class, null, list);
        statusTable.setModel(model);
        KjoretoyUtils.initDefaultTableSettings(statusTable);
        this.repaint();    

    }

    @SuppressWarnings("unchecked")
	protected void loadFromEntity() {
        List<Dfu_Status> stats = mipssEntity.getDfu_StatusList();
        ((EditableMippsTableModel<Dfu_Status>)statusTable.getModel()).setEntities(stats);
    }

    public void enableUpdating(boolean canUpdate) {
    }

    public void saveChanges() {
    }

    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
    
    public boolean saveChangesAndContinue() {
        return true;
    }

    protected boolean isSaved() {
    	// TODO: implementer update/create
    	return true;
    }

    private void initComponents() {

        statusTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        statusTable = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jScrollPane1.setViewportView(statusTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(statusTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(statusTitle)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }


    // Variables declaration - do not modify
    private javax.swing.JTable statusTable;
    private javax.swing.JLabel statusTitle;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration

	@Override
	protected Dimension getDialogSize() {
		return null;
	}

}
