package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.ui.KjoretoyLeggTilEierPanel;
import no.mesta.mipss.ui.KjoretoyNyEierDialog;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.util.KjoretoyUtils;

/**
 * Panel for visning av et kjøretøys eier.
 * @author jorge
 */
public class KjoretoyeierViewPanel extends AbstractKjoretoyPanel {
	private static final long serialVersionUID = -6596480588912549464L;
	private KjoretoyLevKontraktViewPanel parent;
    private MipssKjoretoy bean;
    private Container parentContainer;
    
    // Variables declaration - do not modify
    private JTextArea adresse;
    private JButton byttEierButton;
    private JButton endreEierButton;
    private JTextField epost;
    private JLabel lblEierNavn;
    private JLabel lblEierAdresse;
    private JLabel lblEierTlf1;
    private JLabel lblEierTlf2;
    private JLabel lblEpost;
    private JPanel pnlMain;
    private JScrollPane scrlPaneAdresse;
    private JLabel kjoretoyeierTitle;
    private JTextField navn;
    private JTextField telefonnummer1;
    private JTextField telefonnummer2;
    // End of variables declaration
    public KjoretoyeierViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyLevKontraktViewPanel parent, MipssKjoretoy bean, ResourceBundle bundle, Container parentContainer) {
        super(kjoretoy, controller);
        this.bean = bean;
        this.parentContainer = parentContainer;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        this.parent = parent;
        
        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
        }
    }
        
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	public void enableUpdating(boolean canUpdate) {
        navn.setEnabled(canUpdate);
        adresse.setEnabled(canUpdate);
        telefonnummer1.setEnabled(canUpdate);
        telefonnummer2.setEnabled(canUpdate);
        epost.setEnabled(canUpdate);
        byttEierButton.setEnabled(canUpdate);
        endreEierButton.setEnabled(canUpdate);
    }

    private void initListeners () {
        byttEierButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                    	LeggTilRelasjonDialog<Kjoretoyeier> leggTilRelasjonDialog;
                    	if(parentContainer instanceof JFrame) {
                    		leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyeier>((JFrame)parentContainer, new KjoretoyLeggTilEierPanel(mipssEntity, bean));
                    	} else {
                    		leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyeier>((JDialog)parentContainer, new KjoretoyLeggTilEierPanel(mipssEntity, bean));
                    	}
                    	
                    	Kjoretoyeier kjoretoyeier = leggTilRelasjonDialog.showDialog();
                    	
                    	if(kjoretoyeier != null) {
                    		mipssEntity.setKjoretoyeier(kjoretoyeier);
                    		initFromBean(kjoretoyeier);
                    		if(parent != null) {
                    			parent.enableSaveButton(true);
                    		}
                    	}
                    }
                });
        
        endreEierButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Kjoretoyeier original = mipssEntity.getKjoretoyeier();
				if(original != null) {
					Kjoretoyeier kopi = new Kjoretoyeier(); 
					kopi.setId(original.getId());
					kopi.merge(original);
					
					KjoretoyNyEierDialog kjoretoyNyEierDialog;
					if(parentContainer instanceof JFrame) {
						kjoretoyNyEierDialog = new KjoretoyNyEierDialog((JFrame)parentContainer, kopi);
					} else {
						kjoretoyNyEierDialog = new KjoretoyNyEierDialog((JDialog)parentContainer, kopi);
	            	}
					
					Kjoretoyeier kjoretoyeier = kjoretoyNyEierDialog.showDialog();
					
					if(kjoretoyeier != null) {
						original.merge(kjoretoyeier);
						initFromBean(kjoretoyeier);
						if(parent != null) {
							parent.enableSaveButton(true);
						}
					}
				}
			}
        });
    }
    
    private void initFromBean(Kjoretoyeier eier) {
        // vis modellen som kjøretøyet bruker
        
        if (eier != null) {
            navn.setText(KjoretoyUtils.stringContentOf(eier.getNavn()));
            adresse.setText(KjoretoyUtils.stringContentOf(eier.getWholeAdresse()));
            telefonnummer1.setText(KjoretoyUtils.stringContentOf(eier.getTlfnummer1()));
            telefonnummer2.setText(KjoretoyUtils.stringContentOf(eier.getTlfnummer2()));
            epost.setText(KjoretoyUtils.stringContentOf(eier.getEpostAdresse()));
        }
        else {
            navn.setText("");
            adresse.setText("");
            navn.setText("");
            telefonnummer1.setText("");
            telefonnummer2.setText("");
            epost.setText("");
        }
        pnlMain.repaint();
    }
    
    protected void loadFromEntity() {
        Kjoretoyeier eier = mipssEntity.getKjoretoyeier();
        initFromBean(eier);
        
        if (eier != null) {
            byttEierButton.setText(getBundle().getString("kjoretoyeier.byttEierButton"));
        }
        else {
            byttEierButton.setText(getBundle().getString("kjoretoyeier.nyEierButton"));
        }
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        KjoretoyUtils.setBold(kjoretoyeierTitle);
        initListeners();
    }

    private void initComponents() {

        pnlMain = new JPanel();
        lblEierNavn = new JLabel();
        lblEierAdresse = new JLabel();
        navn = new JTextField();
        telefonnummer1 = new JTextField();
        lblEierTlf1 = new JLabel();
        telefonnummer2 = new JTextField();
        lblEierTlf2 = new JLabel();
        epost = new JTextField();
        lblEpost = new JLabel();
        byttEierButton = new JButton();
        endreEierButton = new JButton();
        kjoretoyeierTitle = new JLabel();
        scrlPaneAdresse = new JScrollPane();
        adresse = new JTextArea();

        lblEierNavn.setHorizontalAlignment(SwingConstants.TRAILING);
        lblEierNavn.setText(getBundle().getString("kjoretoyeier.navn")); // NOI18N

        lblEierAdresse.setHorizontalAlignment(SwingConstants.TRAILING);
        lblEierAdresse.setText(getBundle().getString("kjoretoyeier.adr")); // NOI18N
        
        navn.setEditable(false);

        telefonnummer1.setEditable(false);

        lblEierTlf1.setHorizontalAlignment(SwingConstants.TRAILING);
        lblEierTlf1.setText(getBundle().getString("kjoretoyeier.telefonnummer1")); // NOI18N

        telefonnummer2.setEditable(false);
        telefonnummer2.setName("telefonnummer2"); // NOI18N

        lblEierTlf2.setHorizontalAlignment(SwingConstants.TRAILING);
        lblEierTlf2.setText(getBundle().getString("kjoretoyeier.telefonnummer2")); // NOI18N

        epost.setEditable(false);

        lblEpost.setHorizontalAlignment(SwingConstants.TRAILING);
        lblEpost.setText(getBundle().getString("kjoretoyeier.ePost")); // NOI18N

        byttEierButton.setText(getBundle().getString("kjoretoyeier.byttEierButton")); // NOI18N
        
        endreEierButton.setText(getBundle().getString("kjoretoyeier.endreEierButton"));

        kjoretoyeierTitle.setText(getBundle().getString("kjoretoyeier.title")); // NOI18N

        adresse.setLineWrap(true);
        adresse.setWrapStyleWord(true);
        adresse.setName("adresse"); // NOI18N
        adresse.setEditable(false);
        scrlPaneAdresse.setViewportView(adresse);

        navn.setPreferredSize(new Dimension(140, 21));
        navn.setMinimumSize(new Dimension(140, 21));
        epost.setPreferredSize(new Dimension(140, 21));
        epost.setMinimumSize(new Dimension(140, 21));
        scrlPaneAdresse.setPreferredSize(new Dimension(120, 21));
        scrlPaneAdresse.setMinimumSize(new Dimension(120, 21));
        telefonnummer1.setPreferredSize(new Dimension(100, 21));
        telefonnummer1.setMinimumSize(new Dimension(100, 21));
        telefonnummer2.setPreferredSize(new Dimension(100, 21));
        telefonnummer2.setMinimumSize(new Dimension(100, 21));
        
        byttEierButton.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        byttEierButton.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        endreEierButton.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        endreEierButton.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        pnlMain.setBorder(BorderFactory.createTitledBorder(getBundle().getString("kjoretoyeier.title")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(lblEierNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(navn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        pnlMain.add(lblEpost, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(epost, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        pnlMain.add(lblEierTlf1, new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));        
        pnlMain.add(telefonnummer1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        pnlMain.add(lblEierTlf2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(telefonnummer2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        pnlMain.add(lblEierAdresse, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(scrlPaneAdresse, new GridBagConstraints(5, 0, 1, 2, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 5, 11), 0, 0));
        pnlMain.add(byttEierButton, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(endreEierButton, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
