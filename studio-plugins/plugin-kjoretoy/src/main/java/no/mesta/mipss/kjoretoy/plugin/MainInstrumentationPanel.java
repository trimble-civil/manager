package no.mesta.mipss.kjoretoy.plugin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.kjoretoy.control.SearchControl;
import no.mesta.mipss.messages.AapneKjoretoyMessage;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Inngangspunktet for denne plugin.
 * @author jorge
 */
public class MainInstrumentationPanel extends MipssPlugin {
	private static final long serialVersionUID = -5592773352850228144L;
	private static Logger logger = LoggerFactory.getLogger(MainInstrumentationPanel.class);
    private static ResourceBundle bundle;
    private JPanel guiContainer;
    private BorderLayout layoutMain = new BorderLayout();
    private StatusPanel statusPanel;
    private KjoretoyTabbedPane kjoretoyTabbedPane;
    private TopPanel topPanel;
	private SearchControl searchControl;

    private static MaintenanceContract maintenanceContractBean;
    
    // så lenge vi ikke har brukerrettigheter bruker vi denne for å simulere skriverettighetene
    private static Bruker bruker = null;
    private boolean hasUpdateAccess = true;
    
    public MainInstrumentationPanel() {
//    	super(false);
    	
        logger.debug("Plugin created");
        
        // mens vi venter på rammeverket
        bruker = new Bruker();
        bruker.setSignatur("TESTBRUK");
        bruker.setNavn("Test Bruker");
    }

    protected void initPluginGUI() {
        bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
        // create the JPA session beans

        guiContainer = new JPanel();

        guiContainer.setLayout(layoutMain);
        //guiContainer.setSize(new Dimension(400, 300));
        
        statusPanel = new StatusPanel();
        searchControl = new SearchControl(statusPanel, this);
        kjoretoyTabbedPane = new KjoretoyTabbedPane(searchControl, hasUpdateAccess, this);
        searchControl.initTabBases(kjoretoyTabbedPane.getKjoretoySearch(), kjoretoyTabbedPane.getKjoretoyMainInfo());
        topPanel = new TopPanel(kjoretoyTabbedPane, statusPanel, this, searchControl);
        
        // place the three components
        guiContainer.add(topPanel, BorderLayout.NORTH);
        //this.add(workTabbedPane, BorderLayout.CENTER);
        guiContainer.add(kjoretoyTabbedPane, BorderLayout.CENTER);
        guiContainer.add(statusPanel, BorderLayout.SOUTH);
        guiContainer.setPreferredSize(new Dimension(800, 600));
    }

    public static MaintenanceContract getMaintenanceContractBean() {
        if (maintenanceContractBean == null) {
            KjoretoyUtils.showException(bundle.getString("errormessage.appserverConnection"));
            throw new NullPointerException("Serverforbindelsen ikke opprettet");
        }
        return maintenanceContractBean;
    }

    public void shutdown() {
         loader = null;
    }

    public JPanel getModuleGUI() {
        return guiContainer;
    }

    /**
     * midlertidlig løsning mens vi venter på rammeverket
     * @return
     */
    public static Bruker getBruker() {
        return bruker;
    }
    
    public static String getBrukerSignatur() {
        return bruker.getSignatur();
    }
    
    /**{@inheritDoc}*/
    @Override
	public void receiveMessage(PluginMessage<?, ?> message) {
    	if(message instanceof AapneKjoretoyMessage) {
    		AapneKjoretoyMessage aapneKjoretoyMessage = (AapneKjoretoyMessage)message;
    		if(aapneKjoretoyMessage.getKjoretoyId() != null) {
    			loader.activatePlugin(getCacheKey());
    			searchControl.fetchOneHit(aapneKjoretoyMessage.getKjoretoyId());
    		}
    		message.processMessage();
    	}
	}
}
