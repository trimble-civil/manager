package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre_Status;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.KjoretoyUtils;

public class KjoretoyordreNyStatusDialog extends AbstractRelasjonPanel<Kjoretoyordre_Status> {
	private JPanel pnlMain = new JPanel();
	private JLabel lblStatus = new JLabel();
	private JComboBox cbxStatus = new JComboBox();
	private JLabel lblKommentar = new JLabel();
	private JTextArea txtAreaKommentar = new JTextArea();
	private JScrollPane scrlPaneKommentar = new JScrollPane();
	
	private MaintenanceContract maintenanceContract;
	private Kjoretoyordre kjoretoyordre;
	private MipssComboBoxModel<Kjoretoyordrestatus> cbxModelStatus;
	
	public KjoretoyordreNyStatusDialog(MaintenanceContract maintenanceContract, Kjoretoyordre kjoretoyordre) {
		this.maintenanceContract = maintenanceContract;
		this.kjoretoyordre = kjoretoyordre;
		
		initComboBoxes();
		initGui();
		updateComplete();
	}
	
	private void initComboBoxes() {
		List<Kjoretoyordrestatus> kjoretoyordrestatusList = maintenanceContract.getKjoretoyordrestatusList();
		
		//Luker ut statuser som har vært satt før
		for(Iterator<Kjoretoyordrestatus> it = kjoretoyordrestatusList.iterator(); it.hasNext();) {
			Kjoretoyordrestatus kjoretoyordrestatus = it.next();
			for(Kjoretoyordre_Status kjoretoyStatus : kjoretoyordre.getKjoretoyordre_StatusList()) {
				if(kjoretoyStatus.getKjoretoyordrestatus().getNavn().equals(kjoretoyordrestatus.getNavn())) {
					it.remove();
					break;
				}
			}
		}
		
		cbxModelStatus = new MipssComboBoxModel<Kjoretoyordrestatus>(kjoretoyordrestatusList);
		cbxStatus.setModel(cbxModelStatus);
		cbxStatus.setRenderer(new MipssListCellRenderer<Kjoretoyordrestatus>());
		cbxStatus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void initGui() {
		lblStatus.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.statusLabel"));
		lblKommentar.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.labelKommentar"));
		
		scrlPaneKommentar.setViewportView(txtAreaKommentar);
		
		cbxStatus.setMinimumSize(new Dimension(150, 21));
		cbxStatus.setPreferredSize(new Dimension(150, 21));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblStatus, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxStatus, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblKommentar, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(scrlPaneKommentar, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
	private void updateComplete() {
		setComplete(cbxModelStatus.getSelectedItem() != null);
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(300, 200);
	}
	
	@Override
	public Kjoretoyordre_Status getNyRelasjon() {
		Kjoretoyordre_Status kjoretoyordre_Status = new Kjoretoyordre_Status();
		kjoretoyordre_Status.setKjoretoyordre(kjoretoyordre);
		kjoretoyordre_Status.setKjoretoyordrestatus(cbxModelStatus.getSelectedItem());
		kjoretoyordre_Status.setFritekst(txtAreaKommentar.getText());
		kjoretoyordre_Status.getOwnedMipssEntity().setOpprettetDato(new Date());
		return kjoretoyordre_Status;
	}
	
	@Override
	public String getTitle() {
		return KjoretoyUtils.getBundle().getString("kjoretoyordre.statusTitle");
	}
	
	@Override
	public boolean isLegal() {
		return true;
	}
}
