package no.mesta.mipss.kjoretoy.tablebrowser;

import javax.swing.JButton;

/**
 * Definerer metoder for å sette referanser til kontrollknapper.
 * Bruken av disse knappene er vagfri.
 * Klasser som implementerer dette grensesnittet må ta høyde for at disse knappene
 * ikke blir satt.
 * Disse knappene kan deretter fritt bindes.
 * @author jorge
 */
public interface Browsable {
  /**
     * Setter knappen som beordrer gogkjenning.
     * @param button
     */
    public void setOkButton(JButton button);
    
    /**
     * Setter knappen som beordrer avbrudd.
     * @param button
     */
    public void setAvbrytButton(JButton button);

    /**
     * Setter knappen som beordrer lagring av endringene.
     * @param button
     */
    public void setLagreButton(JButton button);
    
    /**
     * implementerer en callback metode for å endre navigatoren når brukeren endrer entiteten. 
     * @param browserParent
     */
    public void setBrowserParent(BrowserPanel<?, ?, ?> browserParent);
}
