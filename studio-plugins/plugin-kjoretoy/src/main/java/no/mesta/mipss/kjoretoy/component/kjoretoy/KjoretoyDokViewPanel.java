package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.utils.DokumentFilter;
import no.mesta.mipss.kjoretoy.utils.FileUtils;
import no.mesta.mipss.persistence.dokarkiv.Dokument;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EditableMippsTableModel.MipssTableColumn;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for visning av alle dokumenter et kjøretøy er knyttet til.
 * @author jorge
 */

public class KjoretoyDokViewPanel extends AbstractKjoretoyPanel {
	private static final long serialVersionUID = 3961956816504439619L;
	private Logger logger = LoggerFactory.getLogger(KjoretoyDokViewPanel.class);
	
	private KjoretoyInfoViewPanel parent;
	private MipssKjoretoy bean;
	private JFileChooser dokChooser;
	private EditableMippsTableModel<Dokument> model;
    // Variables declaration - do not modify
    private JLabel KjoretoyDokTitleLabel;
    private JButton leggTilDokButton;
    private JButton aapneDokButton;
    private JButton slettDokButton;
    private JTable docTable;
    private JScrollPane scrlPaneTable;
    // End of variables declaration
    public KjoretoyDokViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyInfoViewPanel parent, MipssKjoretoy bean, ResourceBundle bundle) {
        super(kjoretoy, controller);
        this.bean = bean;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        initActions();
        this.parent = parent;

        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
            repaint();
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    public void enableUpdating(boolean canUpdate) {
    }
    
    protected void loadFromEntity() {
        // hent dokumentene
        List<Dokument> doks = null;
        try {
            doks = bean.getDokumentListForKjoretoy(mipssEntity.getId());
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, getBundle().getString("dokList.ingenListe"));
            doks = new ArrayList<Dokument>();
        }
        mipssEntity.setDokumentList(doks);
        
        // sett opp kolonnene
        List<MipssTableColumn> list = new ArrayList<MipssTableColumn>();
        list.add(new MipssTableColumn(getBundle().getString("dokList.navn"),    "navn", false));
        list.add(new MipssTableColumn(getBundle().getString("dokList.type"),    "${doktype.navn}", false));
        list.add(new MipssTableColumn(getBundle().getString("dokList.format"),  "${dokformat.navn}", false));
        list.add(new MipssTableColumn(getBundle().getString("dokList.lagtInn"), "${ownedMipssEntity.opprettetDato}", false));
        
        // sett opp tabellen
        model = new EditableMippsTableModel<Dokument>(Dokument.class, doks, list);
        docTable.setModel(model);
        KjoretoyUtils.initDefaultTableSettings(docTable);
        this.repaint();
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        KjoretoyUtils.setBold(KjoretoyDokTitleLabel);
    }
    
    private void initActions() {
    	// opprett filåpner
    	dokChooser = new JFileChooser();
    	dokChooser.addChoosableFileFilter(new DokumentFilter());
    	
    	leggTilDokButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = dokChooser.showOpenDialog(KjoretoyDokViewPanel.this);

		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		            File file = dokChooser.getSelectedFile();
		            
		            // fileChooser kan returnere null
		            if(file == null) {
		            	return;
		            }
		            
		            Dokument dokument = FileUtils.loadDokument(file, bean);
		            if(dokument == null) {
		            	//KjoretoyUtils.showException(getBundle().getString("gen.kanIkkeLeseFil"));
		            	return;
		            }
		            
		            // legg til kjøretøy for lagring
		            mipssEntity.addDokument(dokument);
		            parent.enableSaveButton(true);
		            // vis dokumentet i tabellen
		            model.setEntities(mipssEntity.getDokumentList());
		            docTable.getSelectionModel().clearSelection();
		            docTable.repaint();
		        } else {
		            logger.debug("Open command cancelled by user.");
		        }
			}
    	});
    	
    	aapneDokButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = docTable.getSelectedRows();
				for(int i=0; i<selectedRows.length; i++) {
					if(selectedRows[i] >= 0 && selectedRows[i] < docTable.getRowCount()) {
						Dokument dok = mipssEntity.getDokumentList().get(selectedRows[i]);
						String extension = "." + FileUtils.getFileNameExtension(dok.getNavn());
						File temp = FileUtils.saveTempFile(extension, dok.getLob());
						DesktopHelper.openFile(temp);
					}
				}
			}
    	});
    	
    	slettDokButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = docTable.getSelectedRows();
				for(int i=0; i<selectedRows.length; i++) {
					if(selectedRows[i] >= 0 && selectedRows[i] < docTable.getRowCount()) {
						Dokument doc = model.getEntities().get(selectedRows[i]);
						
						//Sjekker om denne typen kan slettes
						if(doc.getDoktype() != null && doc.getDoktype().getBegrensetTilgangFlagg() == 1) {
							JOptionPane.showMessageDialog(getParent(), getBundle().getString("gen.kanIkkeSletteFilMedBegrensetTilgang"));
						} else {
							mipssEntity.removeDokument(doc);
				            parent.enableSaveButton(true);
				            model.setEntities(mipssEntity.getDokumentList());
				            docTable.getSelectionModel().clearSelection();
				            docTable.repaint();
						}
					}
				}
			}
    	});
    }

    private void initComponents() {

        KjoretoyDokTitleLabel = new JLabel();
        leggTilDokButton = new JButton();
        aapneDokButton = new JButton();
        slettDokButton = new JButton();
        scrlPaneTable = new JScrollPane();
        docTable = new JTable();

        setBorder(BorderFactory.createTitledBorder(getBundle().getString("dokList.title")));
        setName("Form"); // NOI18N

        KjoretoyDokTitleLabel.setText(getBundle().getString("dokList.title")); // NOI18N
        KjoretoyDokTitleLabel.setName("KjoretoyDokTitleLabel"); // NOI18N

        leggTilDokButton.setText(getBundle().getString("dokList.leggTilDok")); // NOI18N
        aapneDokButton.setText(getBundle().getString("dokList.aapneDok")); // NOI18N
        slettDokButton.setText(getBundle().getString("dokList.slettDok")); // NOI18N

        leggTilDokButton.setMinimumSize(new Dimension(KjoretoyInfoViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        leggTilDokButton.setPreferredSize(new Dimension(KjoretoyInfoViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        scrlPaneTable.setName("jScrollPane1"); // NOI18N

        docTable.setName("docTable"); // NOI18N
        scrlPaneTable.setViewportView(docTable);

        this.setLayout(new GridBagLayout());
        this.add(scrlPaneTable, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        this.add(leggTilDokButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        this.add(aapneDokButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        this.add(slettDokButton, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
    }
}
