package no.mesta.mipss.instrumentering.component;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.ui.combobox.MasterComboListener;
import no.mesta.mipss.ui.combobox.MipssComboBoxWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Panel for registrering av nytt kjoretoy. Inneholder de viktigste felt fro indentifisering
 * av et kjoøretøy.
 * TODO: Gjøre om årslisten til å avhenge av valgt modell.
 * @author jorge
 *
 */
public class KjoretoyIdentificationPanel extends JPanel {
	private Logger logger = LoggerFactory.getLogger(KjoretoyIdentificationPanel.class);
	
	private Kjoretoy kjoretoy;
	
	private MipssComboBoxWrapper<Kjoretoytype> typeComboWrapper;
    private MipssComboBoxWrapper<Kjoretoymodell> modellComboWrapper;
	private ResourceBundle bundle;
    private JLabel lblType;
    private JLabel lblModell;
    private JComboBox modellComboBox;
    private JComboBox typeComboBox;
    
	/**
	 * 
	 */
	public KjoretoyIdentificationPanel(Kjoretoy kjoretoy, boolean begrensetTilgangEndring) {
		super();
		if(kjoretoy == null) {
			throw new IllegalArgumentException("Fått et NULL kjøretøy.");
		}
		
		this.kjoretoy = kjoretoy;
		bundle = Utils.getBundle();
		
		initComponents();
		
		initTypeCombo();
		initModellCombo();
		
		if(begrensetTilgangEndring) {
			typeComboBox.setEnabled(false);
			modellComboBox.setEnabled(false);
		}
	}
	
	private void initTypeCombo() {
		// hent kjoretoytypene
		List<Kjoretoytype> kjoretoytyper;
		try {
			kjoretoytyper = Utils.getKjoretoyBean().getKjoretoytypeList();
		}
		catch (Exception e) {
			throw new IllegalStateException("Kan ikke hente kjoretoytyper", e);
		}
        typeComboWrapper = new MipssComboBoxWrapper<Kjoretoytype>(kjoretoytyper, typeComboBox, bundle.getString("kjoretoytype.velgType"), bundle.getString("kjoretoytype.ingenLinjer"));
        typeComboBox.addActionListener(new MasterComboListener<Kjoretoytype>(typeComboWrapper) {
			@Override
			protected void serveNoselection() {
				kjoretoy.setKjoretoytype(null);
			}
			@Override
			protected void serveSelection(Kjoretoytype m) {
				logger.debug("setter type: " + m);
				kjoretoy.setKjoretoytype(m);
			}
        });
        
        if(kjoretoy.getKjoretoytype() != null) {
        	typeComboWrapper.setMipssSelection(kjoretoy.getKjoretoytype());
        }
	}
	
	private void initModellCombo() {
        // sett opp combon med modeller
        List<Kjoretoymodell> modeller;
        try {
        	modeller = Utils.getKjoretoyBean().getKjoretoymodellList();
        }
        catch (Exception e) {
        	throw new IllegalStateException("Kan ikke hente kjoretoymodeller", e);
		}
        modellComboWrapper = new MipssComboBoxWrapper<Kjoretoymodell>(modeller, modellComboBox, bundle.getString("kjoretoymodell.velgModell"), bundle.getString("kjoretoymodell.ingenModeller"));
        modellComboBox.addActionListener(new MasterComboListener<Kjoretoymodell>(modellComboWrapper) {
			@Override
			protected void serveNoselection() {
				kjoretoy.setKjoretoymodell(null);
			}
			@Override
			protected void serveSelection(Kjoretoymodell m) {
				logger.debug("setter modell: " + m);
				// TODO: dette må endres når årslisten hentes fra modellen
				kjoretoy.setKjoretoymodell(m);
			}
        });
        
        if(kjoretoy.getKjoretoymodell() != null) {
        	modellComboWrapper.setMipssSelection(kjoretoy.getKjoretoymodell());
        }
	}
	

    private void initComponents() {                          
    	JPanel pnlKlassifisering = new JPanel();
    	pnlKlassifisering.setLayout(new GridBagLayout());
        
        lblType = new JLabel();
        typeComboBox = new JComboBox();
        lblModell = new JLabel();
        modellComboBox = new JComboBox();
        
        lblType.setText(bundle.getString("kjoretoy.type"));
        typeComboBox.setName("typeComboBox");
        lblModell.setText(bundle.getString("kjoretoy.modell"));
        modellComboBox.setName("modellComboBox");
        
        typeComboBox.setMinimumSize(new Dimension(80, 19));
        typeComboBox.setPreferredSize(new Dimension(80, 19));
        
        pnlKlassifisering.add(lblType, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlKlassifisering.add(typeComboBox, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 11), 0, 0));
        pnlKlassifisering.add(lblModell, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlKlassifisering.add(modellComboBox, new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoy.ny.id.title")));
        this.add(pnlKlassifisering, new GridBagConstraints(0, 4, 4, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
    }    
}
