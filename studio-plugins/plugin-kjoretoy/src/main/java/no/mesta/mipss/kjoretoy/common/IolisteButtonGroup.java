package no.mesta.mipss.kjoretoy.common;

import java.util.Date;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JTable;

import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reagerer på endringer i ethvert felt med å sjekke tilgjengeløighet for alle buttons.
 * @author jorge
 */
public class IolisteButtonGroup extends ButtonGroup {
	private static final long serialVersionUID = 6692654211399278831L;
	private Logger logger = LoggerFactory.getLogger(IolisteButtonGroup.class);
	private Utstyrgruppe utstyrgruppe = null;
	private Sensortype sensortype = null;
	private Date periodeFraDato = null;
	private Date periodeTilDato = null;
	private Date iolisteFraDato = null;
	private Date iolisteTilDato = null;
	private JTable ioTable = null;
	
	public IolisteButtonGroup(JTable ioTable) {
		super();
		this.ioTable = ioTable;
	}
	
	@Override
	public void clearSelection() {
		super.clearSelection();
		Enumeration<AbstractButton> e = super.getElements();
		while(e.hasMoreElements()) {
			IolisteRadioButton iob = (IolisteRadioButton)e.nextElement();
			iob.setSelected(false);
		}
	}
	
	/**
	 * Slår hver knap av eller på avhengig av innhold i feltene i denne klassen, samt
	 * andre KjoretoyUtstyrIo i Kjoretoyet.
	 */
	public void checkAvailability() {
		boolean tableAvailable = false;
		Enumeration<AbstractButton> e = super.getElements();

    	// hver button slår seg selv av eller på
    	while(e.hasMoreElements()) {
    		IolisteRadioButton rb = (IolisteRadioButton)e.nextElement();
    		if(rb.checkAvailability(sensortype, utstyrgruppe, periodeFraDato, periodeTilDato, iolisteFraDato, iolisteTilDato)) {
    			tableAvailable = true;
    		}
    	}
    	ioTable.setEnabled(tableAvailable);
	}

	/**
	 * @return the utstyrgruppe
	 */
	public Utstyrgruppe getUtstyrgruppe() {
		return utstyrgruppe;
	}

	/**
	 * @param utstyrgruppe the utstyrgruppe to set
	 */
	public void setUtstyrgruppe(Utstyrgruppe utstyrgruppe) {
		this.utstyrgruppe = utstyrgruppe;
		checkAvailability();
	}

	/**
	 * @return the sensortype
	 */
	public Sensortype getSensortype() {
		return sensortype;
	}

	/**
	 * @param sensortype the sensortype to set
	 */
	public void setSensortype(Sensortype sensortype) {
		this.sensortype = sensortype;
		checkAvailability();
	}

	/**
	 * @return the periodeFraDato
	 */
	public Date getPeriodeFraDato() {
		return periodeFraDato;
	}

	/**
	 * @param periodeFraDato the periodeFraDato to set
	 */
	public void setPeriodeFraDato(Date periodeFraDato) {
		logger.debug("setter PeriodeFraDato til: " + periodeFraDato);
		this.periodeFraDato = periodeFraDato;
		
		// send videre til knappene
		Enumeration<AbstractButton> e = super.getElements();
    	while(e.hasMoreElements()) {
    		IolisteRadioButton rb = (IolisteRadioButton)e.nextElement();
    		rb.setPeriodeFraDato(periodeFraDato, periodeTilDato, iolisteFraDato, iolisteTilDato);
    	}
    	checkAvailability();
	}

	/**
	 * @return the periodeTilDato
	 */
	public Date getPeriodeTilDato() {
		return periodeTilDato;
	}

	/**
	 * @param periodeTilDato the periodeTilDato to set
	 */
	public void setPeriodeTilDato(Date periodeTilDato) {
		logger.debug("setter PeriodeTilDato til: " + periodeTilDato);
		this.periodeTilDato = periodeTilDato;
		
		// send videre til knappene
		Enumeration<AbstractButton> e = super.getElements();
    	while(e.hasMoreElements()) {
    		IolisteRadioButton rb = (IolisteRadioButton)e.nextElement();
    		rb.setPeriodeTilDato(periodeTilDato, periodeFraDato, iolisteFraDato, iolisteTilDato);
    	}
    	checkAvailability();
	}

	/**
	 * @return the iolisteFraDato
	 */
	public Date getIolisteFraDato() {
		return iolisteFraDato;
	}

	/**
	 * @param iolisteFraDato the iolisteFraDato to set
	 */
	public void setIolisteFraDato(Date iolisteFraDato) {
		logger.debug("setter IolisteFraDato til: " + iolisteFraDato);
		this.iolisteFraDato = iolisteFraDato;
		
		// send videre til knappene
		Enumeration<AbstractButton> e = super.getElements();
    	while(e.hasMoreElements()) {
    		IolisteRadioButton rb = (IolisteRadioButton)e.nextElement();
    		rb.setIolisteFraDato(iolisteFraDato, iolisteTilDato, periodeFraDato, periodeTilDato);
    	}
    	checkAvailability();
	}

	/**
	 * @return the iolisteTilDato
	 */
	public Date getIolisteTilDato() {
		return iolisteTilDato;
	}

	/**
	 * @param iolisteTilDato the iolisteTilDato to set
	 */
	public void setIolisteTilDato(Date iolisteTilDato) {
		logger.debug("setter IolisteTilDato til: " + iolisteTilDato);
		this.iolisteTilDato = iolisteTilDato;
		
		// send videre til knappene
		Enumeration<AbstractButton> e = super.getElements();
    	while(e.hasMoreElements()) {
    		IolisteRadioButton rb = (IolisteRadioButton)e.nextElement();
    		rb.setIolisteTilDato(iolisteTilDato, iolisteFraDato, periodeFraDato, periodeTilDato);
    	}
    	checkAvailability();
	}


}
