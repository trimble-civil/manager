/**
 * 
 */
package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.exceptions.IolisteOverlappingException;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.ui.combobox.MasterComboListener;
import no.mesta.mipss.ui.combobox.MipssComboBoxWrapper;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXDatePicker;


/**
 * Panel for registrering av ny, eller endring av ekssisterende KjoretoyUtstyrIO.
 * Liten panel med skjema for tidsperioden, sensortype og ioliste.
 * @author jorge
 *
 */
public class NewKjoretoyUtstyrIoPanel extends AbstractMipssEntityPanel<KjoretoyUtstyrIo> {
    private JXDatePicker datePckrAktivFra;
    private JXDatePicker datePckrAktivTil;
    private JCheckBox chkAktivTil;
    private JLabel lblAktivTil;
    private JButton btnForkast;
    private JLabel lblAktivFra;
    private JLabel lblTilkobling;
    private JLabel lblSensorType;
    private KjoretoyUtstyrIoLagreButton btnLagre;
    private JComboBox cbxSensorType;
    private JComboBox cbxTilkobling;
    private JPanel pnlMain;
    private JPanel pnlButtons;
    
	private boolean saved = false;
	private MipssComboBoxWrapper<Sensortype> sensorComboWrapper = null;
	private MipssComboBoxWrapper<Ioliste> tilkoblingComboWrapper = null;
	private boolean updateIo = false;
	private MipssKjoretoy bean = null;
	
	public NewKjoretoyUtstyrIoPanel(KjoretoyUtstyrIo mipssEnt, boolean update, MipssKjoretoy bean, ResourceBundle bundle) {
		super(mipssEnt);
		this.bean = bean;
		super.setBundle(bundle);
        
		try {
			initComponentsPresentation();
		}
		catch (Exception e) {
			KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyrio.ingenvisning"));
			return;
		}
		
		updateIo = update;
		
		if(mipssEnt != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyrio.ingenvisning")); 
            }
        }
		
		if(update) {
    		setDialogBoxTitle(KjoretoyUtils.getPropertyString("ioliste.edit.title"));
    	}
    	else {
    		setDialogBoxTitle(KjoretoyUtils.getPropertyString("ioliste.ny.title"));
    	}
	}

	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
	@Override
	public void enableUpdating(boolean canUpdate) {
		// Kallet ignoreres: alltid på
	}

	@Override
	protected void initComponentsPresentation() {
		initComponents();
		initComboBoxes();
	}

	@Override
	protected boolean isSaved() {
		return saved;
	}

    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
    
	@Override
	protected void loadFromEntity() {
		if(mipssEntity.getKjoretoyUtstyr() == null) {
			throw new IllegalStateException("Mangler referanse til KjoretoyUtstyr");
		}
		
		if(updateIo) {
			// hent fraDato for visning
			datePckrAktivFra.setDate(mipssEntity.getFraDato());
		} else {
			// sett fraDato fordi UI viser den som satt
			mipssEntity.setFraDato(datePckrAktivFra.getDate());
			datePckrAktivFra.setEditable(true);
			aktiverTilDato(false);
		}

		if(mipssEntity.getTilDato() != null) {
			aktiverTilDato(true);
			datePckrAktivTil.setDate(mipssEntity.getTilDato());
		} else {
			aktiverTilDato(false);
		}
		
		btnLagre.setKjoretoyUtstyrIo(mipssEntity);
		initBindings();
		btnLagre.setEnabled(false);
	}

	/**
	 * {@inheritDoc}
	 * Hvis lagringen finner en overlapping, blir brukeren bedt om å velge
	 * annerledes.
	 * TODO Teste ferdig logikken for å utelatte allrede brukte Iolister fra listen
	 * i comboboxen
	 */
	@Override
	public void saveChanges() {
		try {
			List<KjoretoyUtstyrIo> list = bean.getKjoretoyUtstyrIoListFor(mipssEntity.getKjoretoyUtstyr().getId());
			if(checkOverlappingPeriods(list)) {
				KjoretoyUtils.showCorectionInfo(getBundle().getString("gen.overlappendePerioder"));
			} else if(mipssEntity.getTilDato() != null && mipssEntity.getFraDato() != null && mipssEntity.getFraDato().after(mipssEntity.getTilDato())) {
				KjoretoyUtils.showCorectionInfo(getBundle().getString("gen.negativPeriode"));
			} else {
				if(updateIo) {
					int c = JOptionPane.showConfirmDialog(this, getBundle().getString("message.portkonfig.endre.desc"), getBundle().getString("message.portkonfig.endre.title"), JOptionPane.YES_NO_OPTION);
					if (c==JOptionPane.YES_OPTION){
						bean.updateKjoretoyUtstyrIo(mipssEntity);
					}
				} else {
					bean.addKjoretoyUtstyrIo(mipssEntity);
				}

				saved = true;
			}
		} catch (Exception e) {
			Throwable cause = e.getCause();
			if(cause != null && cause instanceof RemoteException) {
				RemoteException o = (RemoteException)cause;
				if(o.detail instanceof IolisteOverlappingException || (o.detail != null && o.detail.getCause() instanceof IolisteOverlappingException)) {
					KjoretoyUtils.showException(getBundle().getString("gen.overlappendeIOPorter"));
					saved = false;
					return;
				}
			}
			
			KjoretoyUtils.showException(e, getBundle().getString("gen.ikkeLagret"));
		}
	}

	private boolean checkOverlappingPeriods(List<KjoretoyUtstyrIo> list) {
		boolean overlapper = false;
		
		for(KjoretoyUtstyrIo ioPeriode : list) {
			if(updateIo && ioPeriode.getId().equals(mipssEntity.getId())) {
				continue; //Skipper sjekk mot samme entitet 
			}
						
			if(mipssEntity.getTilDato() == null && ioPeriode.getTilDato() == null) {
				overlapper = true; //To åpne perioder går ikke
				break;
			} else if(mipssEntity.getTilDato() == null) {
				if(mipssEntity.getFraDato().before(ioPeriode.getTilDato())) {
					overlapper = true;
					break;
				}
			} else if(ioPeriode.getTilDato() == null) {
				if(ioPeriode.getFraDato().before(mipssEntity.getTilDato())) {
					overlapper = true;
					break;
				}
			} else {
				if(mipssEntity.getFraDato().before(ioPeriode.getFraDato())) {
					if(mipssEntity.getTilDato().after(ioPeriode.getFraDato())) {
						overlapper = true;
						break;
					}
				} else {
					if(ioPeriode.getTilDato().after(mipssEntity.getFraDato())) {
						overlapper = true;
						break;
					}
				}
			}
		}
		
		return overlapper;
	}
	
	@Override
	public boolean saveChangesAndContinue() {
		if(! btnLagre.isEnabled()) {
			return false;
		}

        Container browser = this.getParent();
        KjoretoyUtils.DialogElection elected;

    	// vis et dialogvindu for å spørre brukeren om forkasting
        elected = KjoretoyUtils.showForkastFortsettDialog(this, 
                getBundle().getString("dialog.info.forkast"), 
                getBundle().getString("dialog.title.forkast"));

        switch (elected) {
            case LAGRE:
                saveChanges();
                return !saved;
            case FORKAST:
                return false;
            case FORTSETT:
                return true;
            default:
                return false;
        }
	}
	
	private void initBindings() {
		datePckrAktivFra.addPropertyChangeListener("date", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				mipssEntity.setFraDato(datePckrAktivFra.getDate());
				btnLagre.setEnabled(true);
			}
		});
		datePckrAktivTil.addPropertyChangeListener("date", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				mipssEntity.setTilDato(datePckrAktivTil.getDate());
				btnLagre.setEnabled(true);
			}
		});
		btnLagre.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveChanges();
				if(isSaved()) closeDialog();
			}
		});
		btnForkast.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (! saveChangesAndContinue()) {
                    closeDialog();
                }
			}
		});
		
		chkAktivTil.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				aktiverTilDato(chkAktivTil.isSelected());
			}
		});
	}
	
	private void aktiverTilDato(boolean aktiver) {
		chkAktivTil.setSelected(aktiver);
		datePckrAktivTil.setEditable(aktiver);
	}
	
	private void initComboBoxes() {
		// opprett comboboxen og velg linje
		sensorComboWrapper = new MipssComboBoxWrapper<Sensortype>(PanelFactory.getCacheHolder().getSensortypeList(), cbxSensorType);
		sensorComboWrapper.setMipssSelection(mipssEntity.getSensortype());
		
		cbxSensorType.addActionListener(new MasterComboListener<Sensortype>(sensorComboWrapper){
			@Override
			protected void serveNoselection() {
				mipssEntity.setSensortype(null);
				btnLagre.setEnabled(false);
			}
			@Override
			protected void serveSelection(Sensortype m) {
				mipssEntity.setSensortype(m);
				btnLagre.setEnabled(true);
			}
		});
		
		// hent liste med validerte iolister
		List<Ioliste> iolisteList = bean.getValidatedIolisteListForUtstyr(mipssEntity.getKjoretoyUtstyr());
		
		// fjern Iolister som ellerede er i bruk
		iolisteList = filterBusyIolister(iolisteList);
		
		// opprett boxen og velg linje
		tilkoblingComboWrapper = new MipssComboBoxWrapper<Ioliste>(iolisteList, cbxTilkobling);
		tilkoblingComboWrapper.setMipssSelection(mipssEntity.getIoliste());
		
		cbxTilkobling.addActionListener(new MasterComboListener<Ioliste>(tilkoblingComboWrapper){
			@Override
			protected void serveNoselection() {
				mipssEntity.setIoliste(null);
				btnLagre.setEnabled(false);
			}
			@Override
			protected void serveSelection(Ioliste m) {
				mipssEntity.setIoliste(m);
				if(m != null 
						&& m.getBitNr() == null 
						&& mipssEntity.getKjoretoyUtstyr() != null 
						&& mipssEntity.getKjoretoyUtstyr().getUtstyrgruppe() != null
						&& mipssEntity.getKjoretoyUtstyr().getUtstyrgruppe().getSerieportBitNr() != null) {
					mipssEntity.setBitNr(mipssEntity.getKjoretoyUtstyr().getUtstyrgruppe().getSerieportBitNr());
				}
				btnLagre.setEnabled(true);
			}
		});
	}
	
	/**
	 * Fjerner iolister som er i bruk for den gjeldenede perioden fra den leverte listen.
	 * @param original
	 * @return den filtrerte listen.
	 */
	private List<Ioliste> filterBusyIolister(List<Ioliste> original) {
		return original;
	}
	
	/**
	 * Generert i NetBeans og limt inn. Hust å initialisere  aktivTil med de tekstene som skal vises.
	 */
    private void initComponents() {
    	pnlButtons = new JPanel();
        pnlMain = new JPanel();
        datePckrAktivFra = new JXDatePicker();
        datePckrAktivTil = new JXDatePicker();
        lblAktivFra = new JLabel();
        lblTilkobling = new JLabel();
        cbxTilkobling = new JComboBox();
        lblSensorType = new JLabel();
        cbxSensorType = new JComboBox();
        btnLagre = new KjoretoyUtstyrIoLagreButton();
        btnForkast = new JButton();
        lblAktivTil = new JLabel();
		chkAktivTil = new JCheckBox();
        
        lblAktivFra.setText(getBundle().getString("utstyr.ny.aktivFra"));

        lblTilkobling.setText(getBundle().getString("portkonfigurasjon.label"));

        lblSensorType.setText(getBundle().getString("utstyr.ny.sensortype"));
        
        btnLagre.setText(getBundle().getString("button.lagre"));

        btnForkast.setText(getBundle().getString("button.avbrytt"));

        lblAktivTil.setText(getBundle().getString("utstyr.ny.aktivTil"));
        chkAktivTil.setText(getBundle().getString("gen.enableDatoTil"));
        
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(lblAktivFra, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 0), 0, 0));
        pnlMain.add(datePckrAktivFra, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 11, 0), 0, 0));
        pnlMain.add(chkAktivTil, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlMain.add(lblAktivTil, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 0), 0, 0));
        pnlMain.add(datePckrAktivTil, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 11, 0), 0, 0));
        pnlMain.add(lblTilkobling, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlMain.add(cbxTilkobling, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        pnlMain.add(lblSensorType, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlMain.add(cbxSensorType, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        
        pnlButtons.setLayout(new GridBagLayout());
        pnlButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlButtons.add(btnForkast, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
    }                        
               
	@Override
	protected Dimension getDialogSize() {
		return new Dimension(240, 240);
	}
}
