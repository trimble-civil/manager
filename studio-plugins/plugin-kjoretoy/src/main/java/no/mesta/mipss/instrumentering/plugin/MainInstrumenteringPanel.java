/**
 * 
 */
package no.mesta.mipss.instrumentering.plugin;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.plugin.MipssPlugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Grensesnittet mellom MIPS Studio rammeverk og denne plugin.
 * @author jorge
 */
public class MainInstrumenteringPanel extends MipssPlugin{
	private static Logger logger = LoggerFactory.getLogger(MainInstrumenteringPanel.class);
	private JPanel guiContainer = null;
    private IMipssStudioLoader loader = null;
    private InstrumenteringTabbedPane tabs = null;
    
	@Override
	public JPanel getModuleGUI() {
		return guiContainer;
	}

	@Override
	protected void initPluginGUI() {

        tabs = new InstrumenteringTabbedPane(this, getLoader().getApplicationFrame());
        guiContainer = new JPanel();
        GroupLayout layout = new javax.swing.GroupLayout(guiContainer);
    	guiContainer.setLayout(layout);
    	
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabs, javax.swing.GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabs, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
        );

        guiContainer.repaint();
	}

	@Override
	public void shutdown() {
		guiContainer = null;
		
	}

    /**
     * Testmetode for å vise denne plugin for seg.
     * @param args
     */
    public static void main(String[] args){
        try {
             UIManager.setLookAndFeel(new net.java.plaf.windows.WindowsLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            logger.error("");
        }
        JFrame frame = new JFrame();
        frame.setSize(810,690);
        frame.setLocationRelativeTo(null);
        MainInstrumenteringPanel inst = new MainInstrumenteringPanel();
        inst.initPluginGUI();
        frame.add(inst.getModuleGUI());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
