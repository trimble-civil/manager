package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre_Status;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrelogg;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.MipssTextAreaInputDialog;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

public class KjoretoyordreDetaljPanel extends AbstractRelasjonPanel<Kjoretoyordre> {
	private Kjoretoy kjoretoy;
	private MipssKjoretoy mipssKjoretoy;
	private MaintenanceContract maintenanceContract;
	private Kjoretoyordre kjoretoyordre;
	private String brukerSignatur;
	private List<KontraktProsjekt> prosjektPlukkliste;
	
	private MipssBeanTableModel<Kjoretoyordre_Status> tblModelStatus;
	
	private JPanel pnlTop = new JPanel();
	private JPanel pnlLogg = new JPanel();
	private JScrollPane scrlPaneLogg = new JScrollPane();
	private JTextArea txtAreaLogg = new JTextArea();
	private JButton btnNyLogg = new JButton();
	private JLabel lblOrdreId = new JLabel();
	private JFormattedTextField txtOrdreId = new JFormattedTextField();
	private JLabel lblEksterntNavn = new JLabel();
	private JFormattedTextField txtEksterntNavn = new JFormattedTextField();
	private JLabel lblType = new JLabel();
	private JComboBox cbxType = new JComboBox();
	private JLabel lblProsjekt = new JLabel();
	private JFormattedTextField txtProsjekt = new JFormattedTextField();
	private JButton btnVelgProsjekt = new JButton();
	private JLabel lblFritekst = new JLabel();
	private JScrollPane scrlPaneFritekst = new JScrollPane();
	private JTextArea txtAreaFritekst = new JTextArea();
	private JPanel pnlStatus = new JPanel();
	private JXTable tblStatus = new JXTable();
	private JScrollPane scrlPaneStatusTable = new JScrollPane();
	private JButton btnEndreStatus = new JButton();
	private JCheckBox chkVerstedAdminFlagg = new JCheckBox();
	
	/**
	 *	Konstruktør. prosjektnummerlisten kan være null om man ønsker søk etter pnr i OA 
	 * 
	 * @param kjoretoy
	 * @param mipssKjoretoy
	 * @param maintenanceContract
	 * @param kjoretoyordre
	 * @param brukerSignatur
	 * @param prosjektPlukkliste kan være null
	 */
	public KjoretoyordreDetaljPanel(Kjoretoy kjoretoy, MipssKjoretoy mipssKjoretoy, MaintenanceContract maintenanceContract, Kjoretoyordre kjoretoyordre, String brukerSignatur, List<KontraktProsjekt> prosjektPlukkliste) {
		this.kjoretoy = kjoretoy;
		this.mipssKjoretoy = mipssKjoretoy;
		this.maintenanceContract = maintenanceContract;
		this.kjoretoyordre = kjoretoyordre;
		this.brukerSignatur = brukerSignatur;
		this.prosjektPlukkliste = prosjektPlukkliste;
		
		initButtons();
		initComboBoxes();
		initTextField();
		initTable();
		initCheckBoxes();
		initGui();
		updateLoggList();
		updateComplete();
	}
	
	private void initGui() {
		txtOrdreId.setMinimumSize(new Dimension(150, 21));
		txtOrdreId.setPreferredSize(new Dimension(150, 21));
		txtEksterntNavn.setMinimumSize(new Dimension(150, 21));
		txtEksterntNavn.setPreferredSize(new Dimension(150, 21));
		txtProsjekt.setMinimumSize(new Dimension(150, 21));
		txtProsjekt.setPreferredSize(new Dimension(150, 21));
		scrlPaneFritekst.setMinimumSize(new Dimension(150, 100));
		scrlPaneFritekst.setPreferredSize(new Dimension(150, 100));
		
		lblOrdreId.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.ordreIdLabel"));
		lblEksterntNavn.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.kjoretoyNavn"));
		lblType.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.typeLabel"));
		lblProsjekt.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.prosjektLabel"));
		lblFritekst.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.fritekstLabel"));
		
		txtAreaFritekst.setLineWrap(true);
		txtAreaFritekst.setWrapStyleWord(true);
		scrlPaneFritekst.setViewportView(txtAreaFritekst);
		
		pnlTop.setLayout(new GridBagLayout());
		pnlTop.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getBundle().getString("kjoretoyordre.ordreBorder")));
		
		pnlTop.add(lblOrdreId, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTop.add(txtOrdreId, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlTop.add(lblEksterntNavn, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTop.add(txtEksterntNavn, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlTop.add(lblType, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTop.add(cbxType, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlTop.add(lblProsjekt, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTop.add(txtProsjekt, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlTop.add(btnVelgProsjekt, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlTop.add(lblFritekst, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTop.add(scrlPaneFritekst, new GridBagConstraints(1, 4, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
		pnlTop.add(chkVerstedAdminFlagg, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		scrlPaneStatusTable.setViewportView(tblStatus);
		pnlStatus.setLayout(new GridBagLayout());
		pnlStatus.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getBundle().getString("kjoretoyordre.statusBorder")));
		pnlStatus.add(scrlPaneStatusTable, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlStatus.add(btnEndreStatus, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlLogg.setLayout(new GridBagLayout());
		pnlLogg.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getBundle().getString("kjoretoyordre.loggBorder")));
		txtAreaLogg.setLineWrap(true);
		txtAreaLogg.setWrapStyleWord(true);
		txtAreaLogg.setEditable(false);
		scrlPaneLogg.setViewportView(txtAreaLogg);
		pnlLogg.add(scrlPaneLogg, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlLogg.add(btnNyLogg, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlTop, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(11, 11, 0, 11), 0, 0));
		this.add(pnlStatus, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 11, 0, 11), 0, 0));
		this.add(pnlLogg, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 11, 11, 11), 0, 0));
	}
	
	private void initTextField() {
		txtOrdreId.setEditable(false);
		BindingHelper.createbinding(kjoretoyordre, "id", txtOrdreId, "text", BindingHelper.READ).bind();
		
		txtEksterntNavn.setEditable(false);
		BindingHelper.createbinding(kjoretoyordre, "kjoretoy.eksterntNavn", txtEksterntNavn, "text", BindingHelper.READ).bind();
		
		txtProsjekt.setEditable(false);
		BindingHelper.createbinding(kjoretoyordre, "prosjektNr", txtProsjekt, "text", BindingHelper.READ).bind();
		
		BindingHelper.createbinding(kjoretoyordre, "fritekst", txtAreaFritekst, "text", BindingHelper.READ_WRITE).bind();
	}
	
	private void initCheckBoxes() {
		chkVerstedAdminFlagg.setText(KjoretoyUtils.getBundle().getString("kjoretoyordre.checkBoxVerkstedAdmin"));
		BindingHelper.createbinding(kjoretoyordre, "verkstedAdminFlagg", chkVerstedAdminFlagg, "selected", BindingHelper.READ_WRITE).bind();
	}
	
	private void initButtons() {
		btnVelgProsjekt.setAction(new AbstractAction(KjoretoyUtils.getBundle().getString("kjoretoyordre.prosjektButton")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<ProsjektView> leggTilRelasjonDialog = null;
				if(prosjektPlukkliste != null) {
					leggTilRelasjonDialog = new LeggTilRelasjonDialog<ProsjektView>(getParentDialog(), new VelgProsjektnummerPanel(prosjektPlukkliste, maintenanceContract));
				} else {
					leggTilRelasjonDialog = new LeggTilRelasjonDialog<ProsjektView>(getParentDialog(), new VelgProsjektDialog(maintenanceContract));
				}
				
				ProsjektView kontraktProsjekt = leggTilRelasjonDialog.showDialog();
				if(kontraktProsjekt != null) {
					kjoretoyordre.setSelskapskode(kontraktProsjekt.getSelskapskode());
					kjoretoyordre.setProsjektNr(kontraktProsjekt.getProsjektnr());
					updateComplete();
				}
			}
		});
		btnNyLogg.setAction(new AbstractAction(KjoretoyUtils.getBundle().getString("kjoretoyordre.nyLoggButton")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				MipssTextAreaInputDialog mipssTextAreaInputDialog = new MipssTextAreaInputDialog(getParentDialog(), KjoretoyUtils.getBundle().getString("kjoretoyordre.nyLoggTitle"));
				String nyLoggmelding = mipssTextAreaInputDialog.getInput();
				if(nyLoggmelding != null) {
					if(nyLoggmelding.length() > 255) {
						nyLoggmelding = nyLoggmelding.substring(0, 255);
					}
					
					Kjoretoyordrelogg kjoretoyordrelogg = new Kjoretoyordrelogg();
					kjoretoyordrelogg.setKjoretoyordre(kjoretoyordre);
					kjoretoyordrelogg.setTekst(nyLoggmelding);
					kjoretoyordrelogg.setOpprettetDato(new Date());
					kjoretoyordrelogg.setOpprettetAv(brukerSignatur);
					kjoretoyordre.getKjoretoyordreloggList().add(kjoretoyordrelogg);
					updateLoggList();
				}
			}
		});
		btnEndreStatus.setAction(new AbstractAction(KjoretoyUtils.getBundle().getString("kjoretoyordre.statusButton")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<Kjoretoyordre_Status> leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyordre_Status>(getParentDialog(), new KjoretoyordreNyStatusDialog(maintenanceContract, kjoretoyordre));
				Kjoretoyordre_Status kjoretoyordre_Status = leggTilRelasjonDialog.showDialog();
				if(kjoretoyordre_Status != null) {
					if(kjoretoyordre_Status.getFritekst() != null && kjoretoyordre_Status.getFritekst().length() > 4000) {
						kjoretoyordre_Status.setFritekst(kjoretoyordre_Status.getFritekst().substring(0, 4000));
					}
					kjoretoyordre_Status.getOwnedMipssEntity().setOpprettetAv(brukerSignatur);
					kjoretoyordre.getKjoretoyordre_StatusList().add(kjoretoyordre_Status);
					tblModelStatus.fireTableModelEvent(0, tblStatus.getRowCount()-1, TableModelEvent.INSERT);
					updateComplete();
				}
			}
		});
	}
	
	private void initComboBoxes() {
		List<Kjoretoyordretype> kjoretoyordretypeList = maintenanceContract.getKjoretoyordretypeValidList();
		MipssComboBoxModel<Kjoretoyordretype> cbxModel = new MipssComboBoxModel<Kjoretoyordretype>(kjoretoyordretypeList);
		cbxType.setModel(cbxModel);
		cbxType.setRenderer(new MipssListCellRenderer<Kjoretoyordretype>());
		BindingHelper.createbinding(kjoretoyordre, "kjoretoyordretype", cbxType, "selectedItem", BindingHelper.READ_WRITE).bind();
		cbxType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void initTable() {
		tblModelStatus = new MipssBeanTableModel<Kjoretoyordre_Status>(Kjoretoyordre_Status.class, "opprettetDato", "kjoretoyordrestatus", "fritekst");
		tblModelStatus.setEntities(kjoretoyordre.getKjoretoyordre_StatusList());
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Kjoretoyordre_Status.class, tblModelStatus.getColumns());
		tblStatus.setModel(tblModelStatus);
		tblStatus.setColumnModel(colModel);
		tblStatus.setDefaultRenderer(Kjoretoyordrestatus.class, new RenderableMipssEntityTableCellRenderer<Kjoretoyordrestatus>());
		tblStatus.setPreferredScrollableViewportSize(new Dimension(0, 0));
	}
	
	private void updateLoggList() {
		String s = "";
		for(Kjoretoyordrelogg kjoretoyordrelogg : kjoretoyordre.getKjoretoyordreloggList()) {
			s += MipssDateFormatter.formatDate(kjoretoyordrelogg.getOpprettetDato(), true) + "\n" + kjoretoyordrelogg.getOpprettetAv() + "\n" + kjoretoyordrelogg.getTekst() + "\n\n";
		}
		txtAreaLogg.setText(s);
	}
	
	private void updateComplete() {
		setComplete(kjoretoyordre.getKjoretoyordretype() != null && kjoretoyordre.getSisteKjoretoyordrestatus() != null && kjoretoyordre.getProsjektNr() != null);
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(600, 600);
	}

	@Override
	public Kjoretoyordre getNyRelasjon() {
		if(kjoretoyordre.getFritekst() != null && kjoretoyordre.getFritekst().length() > 4000) {
			kjoretoyordre.setFritekst(kjoretoyordre.getFritekst().substring(0, 4000));
		}
		return kjoretoyordre;
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getBundle().getString("kjoretoyordre.detaljTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
