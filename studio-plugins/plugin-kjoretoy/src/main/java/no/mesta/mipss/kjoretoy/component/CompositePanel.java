/**
 * 
 */
package no.mesta.mipss.kjoretoy.component;

/**
 * For paneler som er bygget opp av mindre paneler og som trenger å få sin tilstand oppdatert
 * av de panelene de består av.
 * @author jorge
 *
 */
public interface CompositePanel {

	/**
	 * Setter om lagreknappen skal slås på eller av.
	 * @param enable
	 */
	public abstract void enableSaveButton(boolean enable);
}
