package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.driftkontrakt.bean.ProsjektQueryFilter;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

public class VelgProsjektnummerPanel extends AbstractRelasjonPanel<ProsjektView> {
	private List<KontraktProsjekt> prosjektnummerList;
	
	private JPanel pnlProsjektList = new JPanel();
	private JScrollPane scrlPaneProsjektList = new JScrollPane();
	private JXTable tblProsjektList = new JXTable();
	private MipssBeanTableModel<ProsjektView> tblModelProsjekt;
	private MaintenanceContract maintenanceContract;
	
	public VelgProsjektnummerPanel(List<KontraktProsjekt> prosjektnummerList, MaintenanceContract maintenanceContract) {
		this.prosjektnummerList = prosjektnummerList;
		this.maintenanceContract = maintenanceContract;
		
		initTable();
		initGui();
	}
	
	private void initTable() {
		tblModelProsjekt = new MipssBeanTableModel<ProsjektView>(ProsjektView.class, "prosjektnr", "prosjektnavn");
		tblModelProsjekt.setEntities(buildProsjektList(prosjektnummerList));
		tblProsjektList.setModel(tblModelProsjekt);
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(ProsjektView.class, tblModelProsjekt.getColumns());
		tblProsjektList.setColumnModel(colModel);
		
		tblProsjektList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tblProsjektList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
    	});
	}
	
	private List<ProsjektView> buildProsjektList(List<KontraktProsjekt> prosjektnummerList) {
		List<ProsjektView> retList = new ArrayList<ProsjektView>();
		
		for(KontraktProsjekt kontraktProsjekt : prosjektnummerList) {
			ProsjektQueryFilter pqf = new ProsjektQueryFilter();
			pqf.setProsjektnr(kontraktProsjekt.getProsjektNr());
			List<ProsjektView> tmpList = maintenanceContract.getProsjektViewList(pqf);
			if(tmpList != null) {
				retList.addAll(tmpList);
			}
		}
		
		return retList;
	}
	
	private void initGui() {
		scrlPaneProsjektList.setViewportView(tblProsjektList);
        this.setLayout(new GridBagLayout());
        pnlProsjektList.setLayout(new GridBagLayout());
        pnlProsjektList.add(scrlPaneProsjektList, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlProsjektList, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));

	}
	
	private void updateComplete() {
		setComplete(getSelectedProsjekt() != null);
	}
	
	private ProsjektView getSelectedProsjekt() {
		List<ProsjektView> kontraktList = tblModelProsjekt.getEntities();
		ProsjektView selectedProsjekt = null;
		int selectedIndex = tblProsjektList.getSelectedRow();
		if(selectedIndex >= 0 && selectedIndex < kontraktList.size()) {
			int realIndex = tblProsjektList.convertRowIndexToModel(selectedIndex);
			selectedProsjekt = kontraktList.get(realIndex);
		}
		
		return selectedProsjekt;
	}

	@Override
	public Dimension getDialogSize() {
		return new Dimension(300,400);
	}

	@Override
	public ProsjektView getNyRelasjon() {
		return getSelectedProsjekt();
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getPropertyString("kjoretoyordre.velgProsjektFraListeDialogTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
