package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import javax.swing.JButton;

import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;

/**
 * kontrollerer at knappen blir tilgjengelig kun når alle obligatoriske felt i 
 * {@code KjoretoyUtstyrIo} er satt.
 * @author jorge
 *
 */
public class KjoretoyUtstyrIoLagreButton extends JButton {
	private static final long serialVersionUID = 2856516745427962203L;
	private KjoretoyUtstyrIo io = null;
	
	public KjoretoyUtstyrIoLagreButton() {
		super();
	}
	
	public KjoretoyUtstyrIoLagreButton(KjoretoyUtstyrIo io) {
		super();
		this.io = io;
	}
	
	/**
	 * Tester om alle obligatoriske felt er satt før enabling.
	 * Vi hopper over bitNr fordi den settes av JPA.
	 */
	@Override
	public void setEnabled(boolean enable) {
		if(enable) {
			if(io.getFraDato() != null && io.getSensortype() != null && io.getIoliste() != null) {
				super.setEnabled(enable);
			}
		}
		else {
			super.setEnabled(enable);
		}
	}

	/**
	 * @return the io
	 */
	public KjoretoyUtstyrIo getKjoretoyUtstyrIo() {
		return io;
	}

	/**
	 * @param io the io to set
	 */
	public void setKjoretoyUtstyrIo(KjoretoyUtstyrIo io) {
		this.io = io;
	}
}
