package no.mesta.mipss.instrumentering.plugin;

import java.awt.Container;
import java.util.ResourceBundle;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.instrumentering.search.SearchPanel;
import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.instrumentering.wizard.WizardPanel;
import no.mesta.mipss.plugin.MipssPlugin;


public class InstrumenteringTabbedPane extends JTabbedPane {
	private MipssPlugin mipssPlugin;
	private Container parent;
	private SearchPanel search = null;
	private JPanel detail = null;
	private ResourceBundle bundle = null;
	
	InstrumenteringTabbedPane(MipssPlugin mipssPlugin, Container parent) {
		super();
		this.mipssPlugin = mipssPlugin;
		this.parent = parent;
		bundle = Utils.getBundle();
		initTabs();
	}
	
	private void initTabs() {
		search = new SearchPanel();
		super.addTab(bundle.getString("main.tab.search"), search);
		detail = new JPanel();
		super.addTab(bundle.getString("main.tab.details"), detail);
    }
}
