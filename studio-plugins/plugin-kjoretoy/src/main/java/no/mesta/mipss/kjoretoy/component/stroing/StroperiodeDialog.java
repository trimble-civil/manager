package no.mesta.mipss.kjoretoy.component.stroing;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.*;
import no.mesta.mipss.persistence.stroing.Stroperiode;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class StroperiodeDialog extends JDialog {

	private JPanel pnlMain = new JPanel();
	private JLabel lblFraDato = new JLabel();
	private JMipssDatePicker datePckrFraDato = new JMipssDatePicker();
	private JLabel lblTilDato = new JLabel();
	private JMipssDatePicker datePckrTilDato = new JMipssDatePicker();
	private JLabel lblStroprodukt = new JLabel();
	private JComboBox cbxStroprodukt = new JComboBox();
	private JLabel lblGramPrKvm = new JLabel();
	private JFormattedTextField txtGramPrKvm = new JFormattedTextField();
	private JLabel lblStrobreddeCm = new JLabel();
	private JFormattedTextField txtStrobreddeCm = new JFormattedTextField();
	private JLabel lblStroproduktBeskrivelse = new JLabel();
	private JLabel lblAvgrens = new JLabel();
	private JCheckBox chkAvgrens = new JCheckBox();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();

	private MipssComboBoxModel<Stroprodukt> cbxStroproduktModel;

	private Stroperiode stroperiode;
	private Stroperiode oldStroperiode;

	private BindingGroup bindingGroup = new BindingGroup();
	private boolean nyPeriode;
	private MipssPlugin parentPlugin;
	private List<Stroperiode> eksisterendePerioder;
	private IkkeoverlappendePeriodeValidator periodeValidator = new IkkeoverlappendePeriodeValidator();
	private boolean stroperiodeOk;
	private Date tomorrow;

	public StroperiodeDialog(JFrame parentFrame, Stroperiode stroperiode, List<Stroperiode> eksisterendePerioder, boolean nyPeriode, MipssPlugin parentPlugin) {
		super(parentFrame, KjoretoyUtils.getPropertyString("stroing.dialog"), true);
		this.stroperiode = stroperiode;
		this.eksisterendePerioder = eksisterendePerioder;
		this.nyPeriode = nyPeriode;
		this.parentPlugin = parentPlugin;

		init();
	}

	public StroperiodeDialog(JDialog parentDialog, Stroperiode stroperiode, List<Stroperiode> eksisterendePerioder, boolean nyPeriode, MipssPlugin parentPlugin) {
		super(parentDialog, KjoretoyUtils.getPropertyString("stroing.dialog"), true);
		this.stroperiode = stroperiode;
		this.eksisterendePerioder = eksisterendePerioder;
		this.nyPeriode = nyPeriode;
		this.parentPlugin = parentPlugin;

		init();
	}

	public StroperiodeDialog() {
	}

	public boolean isStroperiodeOk(){
		return stroperiodeOk;
	}

	private void init() {
		tomorrow = CalendarUtil.getTomorrow(true);

		bindingGroup.unbind();
		lagStroperiodeKopi();
		initLabels();
		initButtons();
		initCombobox();
		initTxtFields();
		initDatePickers();
		initGui();

		if (!nyPeriode) {
			stroperiode.addPropertyChangeListener("stroprodukt", evt -> {
                if (stroperiode.getProduktId() != null && !stroperiode.getProduktId().equals(oldStroperiode.getProduktId())) {
                    JOptionPane.showMessageDialog(StroperiodeDialog.this, KjoretoyUtils.getPropertyString("stroing.messageEndreProdukt"), KjoretoyUtils.getPropertyString("stroing.messageEndreProduktTitle"), JOptionPane.WARNING_MESSAGE);
                }
            });
		}

		bindingGroup.bind();
	}

	boolean isAdmin() {
		return UserUtils.isAdmin(parentPlugin.getLoader().getLoggedOnUser(true));
	}

	private void lagStroperiodeKopi() {
		oldStroperiode = stroperiode;
		stroperiode = new Stroperiode();
		stroperiode.setKjoretoy(oldStroperiode.getKjoretoy());
		stroperiode.setFraDato(oldStroperiode.getFraDato());
		stroperiode.merge(oldStroperiode);
	}

	public Stroperiode showDialog() {
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		bindingGroup.unbind();

		if (stroperiode != null) {
			stroperiode.setTilDato(CalendarUtil.round(stroperiode.getTilDato(), false));
			stroperiode.setFraDato(CalendarUtil.round(stroperiode.getFraDato(), true));
		}

		return stroperiode;
	}

	private void initLabels() {
		lblStroprodukt.setText(KjoretoyUtils.getPropertyString("stroing.lblStroprodukt"));
		lblStrobreddeCm.setText(KjoretoyUtils.getPropertyString("stroing.lblStrobreddeCm"));
		lblGramPrKvm.setText(KjoretoyUtils.getPropertyString("stroing.lblGramPrKvm"));
		lblFraDato.setText(KjoretoyUtils.getPropertyString("stroing.lblFraDato"));
		lblTilDato.setText(KjoretoyUtils.getPropertyString("stroing.lblTilDato"));
	}

	private void initButtons() {
		btnOk.setText(KjoretoyUtils.getPropertyString("button.ok"));
		btnAvbryt.setText(KjoretoyUtils.getPropertyString("button.avbrytt"));

		btnOk.addActionListener(e -> {
            if (validateInput()) {
				if (!periodeValidator.sjekkDatoerIPeriode(stroperiode)) {
					JOptionPane.showMessageDialog(
							StroperiodeDialog.this,
							KjoretoyUtils.getPropertyString("stroing.messageFeilDato"),
							KjoretoyUtils.getPropertyString("stroing.messageFeilDatoTitle"),
							JOptionPane.ERROR_MESSAGE);
				} else {
					Stroperiode active = GritPeriodUtil.getActiveGritPeriod(eksisterendePerioder);

					boolean newPeriodCollidesWithExistingPeriods = GritPeriodUtil.periodCollidesWithExistingPeriods(stroperiode, eksisterendePerioder, true);

					if (newPeriodCollidesWithExistingPeriods) {
						showOverlappingErrorMessage();
					} else {
						if (active != null && active.getFraDato().before(stroperiode.getFraDato()) &&
								(active.getTilDato() == null || active.getTilDato().getTime() >= stroperiode.getFraDato().getTime())) {
							Date newToDateExistingPeriod = CalendarUtil.round(CalendarUtil.getDayBefore(stroperiode.getFraDato()), false);
							int r = confirmPeriodToDateChange(active, newToDateExistingPeriod);

							if (r == JOptionPane.YES_OPTION) {
								active.setTilDato(newToDateExistingPeriod);
								validGritPeriod();
							}
						} else if (!nyPeriode && stroperiode.equals(active)) {
							validGritPeriod();
						} else if (GritPeriodUtil.periodCollidesWithExistingPeriods(stroperiode, eksisterendePerioder, false)) {
							showOverlappingErrorMessage();
						} else {
							validGritPeriod();
						}
					}
				}
            }
        });

		btnAvbryt.addActionListener(e -> {
            stroperiode = null;
            StroperiodeDialog.this.dispose();
        });

		bindingGroup.addBinding(BindingHelper.createbinding(stroperiode, "${stroprodukt != null && fraDato != null}", btnOk, "enabled", BindingHelper.READ));
	}

	private void validGritPeriod() {
		stroperiodeOk = true;
		StroperiodeDialog.this.dispose();
	}

	private void showOverlappingErrorMessage() {
		JOptionPane.showMessageDialog(
				StroperiodeDialog.this,
				KjoretoyUtils.getPropertyString("stroing.messageOverlapp"),
				KjoretoyUtils.getPropertyString("stroing.messageOverlappTitle"),
				JOptionPane.ERROR_MESSAGE);
	}

	private int confirmPeriodToDateChange(Stroperiode currentlyActivePeriod, Date newToDate) {
		PropertyResourceBundleUtil bundle = PropertyResourceBundleUtil.getPropertyBundle(Constants.BUNDLE_NAME);

		return JOptionPane.showConfirmDialog(
				StroperiodeDialog.this,
				bundle.getGuiString(
						"newGritPeriod.changePreviousPeriodWarning",
						MipssDateFormatter.formatDate(currentlyActivePeriod.getFraDato(), MipssDateFormatter.DATE_FORMAT),
						MipssDateFormatter.formatDate(newToDate, MipssDateFormatter.DATE_TIME_FORMAT),
						MipssDateFormatter.formatDate(stroperiode.getFraDato(), MipssDateFormatter.DATE_TIME_FORMAT)),
				KjoretoyUtils.getPropertyString("newGritPeriod.changePreviousPeriodWarning.title"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);
	}

	private void initCombobox() {
		List<Stroprodukt> stroproduktList = KjoretoyUtils.getKjoretoyBean().getStroproduktValgbareList();
		cbxStroproduktModel = new MipssComboBoxModel<>(stroproduktList);

		cbxStroprodukt.setModel(cbxStroproduktModel);
		cbxStroprodukt.setRenderer(new MipssListCellRenderer<Stroprodukt>());

		bindingGroup.addBinding(BindingHelper.createbinding(stroperiode, "stroprodukt", cbxStroproduktModel, "selectedItem", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(stroperiode, "${stroprodukt.beskrivelse}", lblStroproduktBeskrivelse, "text", BindingHelper.READ));
	}

	@SuppressWarnings("unchecked")
	private void initTxtFields() {
		txtStrobreddeCm.setDocument(new DocumentFilter(3));
		Binding strobreddeBind = BindingHelper.createbinding(stroperiode, "strobreddeCm", txtStrobreddeCm, "text", BindingHelper.READ_WRITE);
		strobreddeBind.setConverter(new LongStringConverter());
		bindingGroup.addBinding(strobreddeBind);
		txtStrobreddeCm.setHorizontalAlignment(JTextField.RIGHT);

		txtGramPrKvm.setDocument(new DocumentFilter(3));
		Binding gramBind = BindingHelper.createbinding(stroperiode, "gramPrKvm", txtGramPrKvm, "text", BindingHelper.READ_WRITE);
		gramBind.setConverter(new LongStringConverter());
		bindingGroup.addBinding(gramBind);
		txtGramPrKvm.setHorizontalAlignment(JTextField.RIGHT);
	}

	/**
	 * Registers a listener that prevents the user (unless he's an administrator) from picking a date before tomorrow.
	 */
	private void configureFromDatePicker() {
		datePckrFraDato.addPropertyChangeListener("date", evt -> {
			Date newDate = (Date) evt.getNewValue();

			if (!isAdmin()) {
				if (newDate.before(tomorrow)) {
					JOptionPane.showMessageDialog(
							StroperiodeDialog.this,
							KjoretoyUtils.getPropertyString("newGritPeriod.dateWarning"),
							KjoretoyUtils.getPropertyString("newGritPeriod.dateWarning.title"),
							JOptionPane.WARNING_MESSAGE);
					datePckrFraDato.setDate(tomorrow);
				}
			}
		});
	}

	private void initDatePickers() {
		bindingGroup.addBinding(BindingHelper.createbinding(stroperiode, "fraDato", datePckrFraDato, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(stroperiode, "tilDato", datePckrTilDato, "date", BindingHelper.READ_WRITE));

		if (!nyPeriode) {
			datePckrFraDato.setEditable(false);
		}

		bindingGroup.addBinding(BindingHelper.createbinding(stroperiode, "${tilDato != null}", chkAvgrens, "selected", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(chkAvgrens, "selected", datePckrTilDato, "editable", BindingHelper.READ));

		lblAvgrens.setText(KjoretoyUtils.getPropertyString("stroing.lblAvgrens"));
		chkAvgrens.addActionListener(e -> {
            if (!chkAvgrens.isSelected()) {
                stroperiode.setTilDato(null);
            } else if (stroperiode.getTilDato() == null) {
                stroperiode.setTilDato(stroperiode.getFraDato());
            }
        });

		configureFromDatePicker();
	}

	private boolean validateInput() {
		if (stroperiode.getGramPrKvm() == null || stroperiode.getStrobreddeCm() == null) {
			showInvalidInputMessage();
			return false;
		} else {
			if (isAdmin()) {
				return true;
			} else {
				if (stroperiode.getFraDato().before(tomorrow)) {
					showInvalidInputMessage();
					return false;
				} else {
					return true;
				}
			}
		}
	}

	private void showInvalidInputMessage() {
		JOptionPane.showMessageDialog(
				StroperiodeDialog.this,
				KjoretoyUtils.getPropertyString("stroing.messageManglerInfo"),
				KjoretoyUtils.getPropertyString("stroing.messageManglerInfoTitle"),
				JOptionPane.ERROR_MESSAGE);
	}

	private void initGui() {
		Dimension dim = new Dimension(100, 19);
		Dimension dimTxt = new Dimension(100, 19);
		cbxStroprodukt.setPreferredSize(dim);
		cbxStroprodukt.setMinimumSize(dim);
		lblStroproduktBeskrivelse.setPreferredSize(dim);
		lblStroproduktBeskrivelse.setMinimumSize(dim);
		txtStrobreddeCm.setPreferredSize(dimTxt);
		txtStrobreddeCm.setMinimumSize(dimTxt);
		txtGramPrKvm.setPreferredSize(dimTxt);
		txtGramPrKvm.setMinimumSize(dimTxt);
		datePckrFraDato.setPreferredSize(dimTxt);
		datePckrFraDato.setMinimumSize(dimTxt);
		datePckrTilDato.setPreferredSize(dimTxt);
		datePckrTilDato.setMinimumSize(dimTxt);

		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblStroprodukt, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxStroprodukt, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblStroproduktBeskrivelse, new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 0), 0, 0));
		pnlMain.add(lblStrobreddeCm, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(txtStrobreddeCm, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblGramPrKvm, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 5), 0, 0));
		pnlMain.add(txtGramPrKvm, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 0), 0, 0));
		pnlMain.add(lblFraDato, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(datePckrFraDato, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblAvgrens, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(chkAvgrens, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblTilDato, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(datePckrTilDato, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}

}