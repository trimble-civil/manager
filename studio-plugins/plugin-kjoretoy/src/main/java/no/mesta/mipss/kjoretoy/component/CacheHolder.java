package no.mesta.mipss.kjoretoy.component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;


/**
 * Cacher lister av basisentiteter som ikke kan endres under kjøringen.
 * Stoler på at det kun finnes én GUI tråd.
 * @author jorge
 */
public class CacheHolder {
	private static MipssKjoretoy bean = null;
	
    private static List<Dfumodell> dfumodellList = null;
    private static List<Dfuindivid> dfuindividList = null;
    private static List<Dfustatus> dfustatusList = null;
    private static List<Utstyrgruppe> utstyrgruppeList = null;
    private static List<Sensortype> sensortypeList = null;
    private static List<Kjoretoymodell>modellList = null;
    
    public CacheHolder(MipssKjoretoy bean) {
    	CacheHolder.bean = bean;
    }
    	
    	
    public List<Dfumodell> getDfumodellList() {
        if (dfumodellList == null) {
            dfumodellList = bean.getDfumodellList();
        }
        return dfumodellList;
    }
    
    public List<Dfuindivid> getDfuindividList() {
        if (dfuindividList == null) {
            dfuindividList = bean.getDfuindividList();
        }
        return dfuindividList;
    }
    
    public List<Dfustatus> getDfustatusList() {
        if (dfustatusList == null) {
            dfustatusList = bean.getDfustatusList();
        }
        return dfustatusList;
    }
    
    public List<Utstyrgruppe> getUtstyrgruppeList() {
        if (utstyrgruppeList == null) {
            utstyrgruppeList = bean.getUtstyrgruppeList();
            Collections.sort(utstyrgruppeList, new Comparator<Utstyrgruppe>() {
				@Override
				public int compare(Utstyrgruppe o1, Utstyrgruppe o2) {
					if(o1.getNavn() == null && o2.getNavn() == null) {
						return 0;
					} else if(o1.getNavn() == null) {
						return -1;
					} else if(o2.getNavn() == null) {
						return 1;
					} else {
						return o1.getNavn().toLowerCase().compareTo(o2.getNavn().toLowerCase());
					}
				}
            });
        }
        return utstyrgruppeList;
    }
    
    public List<Sensortype> getSensortypeList() {
        if(sensortypeList == null) {
            sensortypeList = bean.getSensortypeList();
        }
        return sensortypeList;
    }
}
