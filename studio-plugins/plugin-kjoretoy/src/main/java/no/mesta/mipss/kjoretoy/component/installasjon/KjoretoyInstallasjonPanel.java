package no.mesta.mipss.kjoretoy.component.installasjon;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.kjoretoy.component.installasjon.graf.PortoppsettDialog;
import no.mesta.mipss.kjoretoy.component.installasjon.graf.PortoppsettGrafFactory;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.VelgDfuindividPanel;
import no.mesta.mipss.kjoretoy.tablebrowser.Browsable;
import no.mesta.mipss.kjoretoy.tablebrowser.BrowserPanel;
import no.mesta.mipss.persistence.Digidok;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInstallasjonV;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.JDateTimePicker;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;
import org.jfree.chart.JFreeChart;

/**
 * Viser hele spekter av informasjon rundt Installasjon, DFU modell, DFU individ mm.
 * @author jorge
 */
public class KjoretoyInstallasjonPanel extends AbstractMipssEntityPanel<Installasjon> implements Browsable {
	private static final long serialVersionUID = 6200148186610317154L;
    
    private MipssKjoretoy bean;
    
    private JButton avbrytButton;
    
    private boolean canUpdate;
    // Variables declaration - do not modify
    private JTextField txtDfumodell;
    private JTextArea txtAreaEnhetNotat;
    private JTextField txtEnhetNyDato;
    private JTextField txtEnhetSerial;
    private JTextField txtEnhetSim;
    private JTextField txtEnhetSisteSignal;
    private JFormattedTextField txtEnhetStatus;
    private JTextField txtEnhetTelefon;
    private JTextField txtEnhetVersjon;
    private JDateTimePicker datePckrInstallasjonAktivFra;
    private JTextField txtInstallasjonIgnoreMask;
    private JTextField txtInstallasjonOpprettetAv;
    private JTextField txtInstallasjonSistendretAv;
    private JTextField txtEnhetKategori;
    private JTextField txtEnhetLeverandor;
    private JDateTimePicker datePckrInstallasjonAktivTil;
    private JLabel lblInstallasjonAktivFra;
    private JLabel lblEnhetLeverandor;
    private JLabel lblEnhetSerial;
    private JLabel lblEnhetVersjon;
    private JLabel lblEnhetNyDato;
    private JLabel lblEnhetSisteSignal;
    private JLabel lblEnhetTelefon;
    private JLabel lblEnhetSim;
    private JLabel lblEnhetStatus;
    private JLabel lblEnhetNotat;
    private JLabel lblInstallasjonAktivTil;
    private JLabel lblInstallasjonOpprettet;
    private JLabel lblInstallasjonEndret;
    private JLabel lblInstallasjonIgnoreMask;
    private JLabel lblEnhetModell;
    private JLabel lblEnhetKategori;
    private JPanel pnlEnhet;
    private JPanel pnlInstallasjon;
    
    private JScrollPane scrEnhetNotat;
    
    private JPanel pnlMain;
    private JPanel pnlButtons;
    private JButton btnOk;
    private JButton btnAvbryt;
    private JButton btnVelgDfu;
    private JButton btnEnhetStatus;
    private JButton btnIgnorering;
    private boolean isSaved;
    private BindingGroup bindingGroup;
    private BrowserPanel browserParent;
    private JButton btnVisPortOppsett;
    
    private JLabel lblDndPortoppsett = new JLabel();
    private JPanel pnlDndPortoppsettIkkeTilgjengelig = new JPanel();
    private JLabel lblDndPortoppsettIkkeTilgjengelig = new JLabel();
    
    private boolean initReady = true;
    private MipssPlugin parentPlugin;
    
    public KjoretoyInstallasjonPanel(Installasjon installasjon, MipssKjoretoy bean, ResourceBundle bundle, MipssPlugin parentPlugin) {
        super(installasjon);
        this.bean = bean;
        this.parentPlugin = parentPlugin;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("dfu.ingenVisning"));
            return;
        }
        
        if (installasjon != null) {
            try {
               loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("dfu.ingenVisning")); 
            }
        }
        
        isSaved = true;
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    public void setOkButton(JButton button) {
    }

    public void setAvbrytButton(JButton button) {
        avbrytButton = button;
        avbrytButton.setName("avbrytButton");
        avbrytButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (! saveChangesAndContinue()) {
                    Container container = ((Container)e.getSource()).getParent();
                    BrowserPanel bp = (BrowserPanel)container.getParent();
                    bp.closeDialogBox();
                }
            }
        });
    }

    public void setLagreButton(JButton button) {
    	button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setMipssEntity(bean.updateInstallasjon(mipssEntity, parentPlugin.getLoader().getLoggedOnUserSign()));
				Container container = ((Container)e.getSource()).getParent();
                BrowserPanel bp = (BrowserPanel)container.getParent();
                bp.closeDialogBox();
			}
    	});
    }

    public void setBrowserParent(BrowserPanel browserParent) {
    	this.browserParent = browserParent;
    }

    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        initInstallationPresentation();
        initIndividPresentation();
    }
    
    private void initInstallationPresentation() {
            
        // init åpning av statuslisten for individ
        btnEnhetStatus.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    // Hvis ikke initialisert fra entity, vis tom liste for å få uniform UI.
                    if (mipssEntity == null) {
                        Dfu_StatusListPanel statusPanel = PanelFactory.createDfu_StatusListPanel(null);
                        statusPanel.createAndShowDialogBox();
                    } else {
                        Dfuindivid ind = ((Installasjon) mipssEntity).getDfuindivid();
                        Dfu_StatusListPanel statusPanel = PanelFactory.createDfu_StatusListPanel(ind);
                        statusPanel.createAndShowDialogBox();
                    }
                }
            });
        
        btnVisPortOppsett.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JDialog parentContainer = null;
		        if(browserParent != null) {
		        	parentContainer = browserParent.getDialog();
		        } else {
		        	parentContainer = dialog;
		        }
		        
		        JFreeChart chart = PortoppsettGrafFactory.createPortoppsettChart(mipssEntity, mipssEntity.getKjoretoy().getKjoretoyUtstyrList());
		        PortoppsettDialog portoppsettDialog = new PortoppsettDialog(parentContainer, chart, mipssEntity.getKjoretoy(), mipssEntity);
		        portoppsettDialog.visGraf();
			}
        });
        btnIgnorering.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog parentContainer = null;
		        if(browserParent != null) {
		        	parentContainer = browserParent.getDialog();
		        } else {
		        	parentContainer = dialog;
		        }
		        List<Digidok> porter = bean.getDigiDok();
		        DigidokDialog d = new DigidokDialog(parentContainer, porter, mipssEntity.getIgnorerMaske());
		        d.setSize(500, 350);
		        d.setLocationRelativeTo(parentContainer);
		        d.setTitle(KjoretoyUtils.getPropertyString("ignorering.title"));
		        d.setVisible(true);
		        if (!d.isCancelled()){
		        	mipssEntity.setIgnorerMaske(d.getIgnoreMaske());
		        }
			}
        });
    }
    
    private void initIndividPresentation() {
        btnVelgDfu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<Dfuindivid> leggTilRelasjonDialog;
				
		        JDialog parentContainer = null;
		        if(browserParent != null) {
		        	parentContainer = browserParent.getDialog();
		        } else {
		        	parentContainer = dialog;
		        }
				
           		leggTilRelasjonDialog = new LeggTilRelasjonDialog<Dfuindivid>(parentContainer, new VelgDfuindividPanel(bean));            	
           		Dfuindivid dfuindivid = leggTilRelasjonDialog.showDialog();
            	
            	if(dfuindivid != null) {
            		dfuindivid = bean.getDfuindivid(dfuindivid.getId());//eagerfetch installasjoner.
            		mipssEntity.setDfuindivid(dfuindivid);
            	}
			}
        });
    }

    protected void loadFromEntity() {
    	initReady = false;
    	initBindings();
        
        if(mipssEntity.getDfuindivid() != null) {
        	btnVelgDfu.setEnabled(false);
        } else {
        	btnVelgDfu.setEnabled(true);
        }
        
        initReady = true;
    }
    
    private void initBindings() {
    	if(bindingGroup != null) {
    		bindingGroup.unbind();
    	}
    	
		bindingGroup = new BindingGroup();
		
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "aktivFraDato", datePckrInstallasjonAktivFra, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "aktivTilDato", datePckrInstallasjonAktivTil, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${opprettetDatoString} ${opprettetAv}", txtInstallasjonOpprettetAv, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${endretDatoString} ${endretAv}", txtInstallasjonSistendretAv, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "ignorerMaske", txtInstallasjonIgnoreMask, "text", BindingHelper.READ));
		
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfumodell == null ? null : dfumodell.dfukategori.navn}", txtEnhetKategori, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfumodell == null ? null : dfumodell.leverandorNavn}", txtEnhetLeverandor, "text", BindingHelper.READ));
		
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.versjon}", txtEnhetVersjon, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.anskaffetDatoString}", txtEnhetNyDato, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.dfuinfo.sisteLivstegnDatoString}", txtEnhetSisteSignal, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.tlfnummerString}", txtEnhetTelefon, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.simkortnummerString}", txtEnhetSim, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.fritekst}", txtAreaEnhetNotat, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.serienummer}", txtEnhetSerial, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.currentStatus.navn}", txtEnhetStatus, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(mipssEntity, "${dfuindivid == null ? null : dfuindivid.modellNavn}", txtDfumodell, "text", BindingHelper.READ));

		bindingGroup.bind();
    }
    
    
    public void enableUpdating(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    public void saveChanges() {
    	isSaved = true;
    	//TODO dette kan ikke gjøres i wizard...
//    	setMipssEntity(bean.updateInstallasjon(mipssEntity, parentPlugin.getLoader().getLoggedOnUserSign()));
    }

    
    public boolean saveChangesAndContinue() {
        return false;
    }
    

    protected boolean isSaved() {
    	return isSaved;
    }

    private void initComponents() {
    	Dimension stdDim = new Dimension(100, 19);
    	Dimension halfDim = new Dimension(50, 19);
    	
    	pnlMain = new JPanel();
        pnlInstallasjon = new JPanel();
        lblInstallasjonAktivFra = new JLabel();
        datePckrInstallasjonAktivFra = new JDateTimePicker(false, true);
        lblInstallasjonAktivTil = new JLabel();
        datePckrInstallasjonAktivTil = new JDateTimePicker(true, false);
        lblInstallasjonOpprettet = new JLabel();
        txtInstallasjonOpprettetAv = new JTextField();
        lblInstallasjonIgnoreMask = new JLabel();
        txtInstallasjonIgnoreMask = new JTextField();
        lblInstallasjonEndret = new JLabel();
        txtInstallasjonSistendretAv = new JTextField();
        lblEnhetModell = new JLabel();
        lblEnhetKategori = new JLabel();
        txtEnhetKategori = new JTextField();
        lblEnhetLeverandor = new JLabel();
        txtEnhetLeverandor = new JTextField();
        txtDfumodell = new JTextField();
        pnlEnhet = new JPanel();
        lblEnhetSerial = new JLabel();
        lblEnhetVersjon = new JLabel();
        lblEnhetNyDato = new JLabel();
        lblEnhetSisteSignal = new JLabel();
        lblEnhetTelefon = new JLabel();
        txtEnhetTelefon = new JTextField();
        lblEnhetSim = new JLabel();
        txtEnhetSim = new JTextField();
        lblEnhetStatus = new JLabel();
        lblEnhetNotat = new JLabel();
        txtEnhetStatus = new JFormattedTextField();
        scrEnhetNotat = new JScrollPane();
        txtAreaEnhetNotat = new JTextArea();
        txtEnhetSerial = new JTextField();
        txtEnhetVersjon = new JTextField();
        txtEnhetNyDato = new JTextField();
        txtEnhetSisteSignal = new JTextField();
        btnEnhetStatus = new JButton();
        btnVelgDfu = new JButton();
        btnVisPortOppsett = new JButton();
        btnIgnorering = new JButton();

        pnlInstallasjon.setBorder(BorderFactory.createTitledBorder(getBundle().getString("dfu.installasjon.title")));

        lblInstallasjonAktivFra.setText(getBundle().getString("dfu.installasjon.fra"));
        lblInstallasjonAktivTil.setText(getBundle().getString("dfu.installasjon.til"));
        lblInstallasjonOpprettet.setText(getBundle().getString("dfu.installasjon.opprettet"));
        lblInstallasjonIgnoreMask.setText(getBundle().getString("dfu.installasjon.ignoreMask"));
        lblInstallasjonEndret.setText(getBundle().getString("dfu.installasjon.sistEndret"));
        btnIgnorering.setText("...");
        
        txtInstallasjonIgnoreMask.setPreferredSize(halfDim);
        txtInstallasjonIgnoreMask.setMinimumSize(halfDim);
        
        txtInstallasjonOpprettetAv.setPreferredSize(stdDim);
        txtInstallasjonOpprettetAv.setMinimumSize(stdDim);
        txtInstallasjonSistendretAv.setPreferredSize(stdDim);
        txtInstallasjonSistendretAv.setMinimumSize(stdDim);
       
        txtInstallasjonIgnoreMask.setEditable(false);
        txtInstallasjonOpprettetAv.setEditable(false);
        txtInstallasjonSistendretAv.setEditable(false);
        
        pnlInstallasjon.setLayout(new GridBagLayout());
        pnlInstallasjon.add(lblInstallasjonAktivFra, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlInstallasjon.add(datePckrInstallasjonAktivFra, 	new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlInstallasjon.add(lblInstallasjonAktivTil, 		new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlInstallasjon.add(datePckrInstallasjonAktivTil, 	new GridBagConstraints(4, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        
        pnlInstallasjon.add(lblInstallasjonIgnoreMask, 		new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlInstallasjon.add(txtInstallasjonIgnoreMask, 		new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlInstallasjon.add(btnIgnorering, 					new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlInstallasjon.add(lblInstallasjonOpprettet, 		new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlInstallasjon.add(txtInstallasjonOpprettetAv, 	new GridBagConstraints(4, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlInstallasjon.add(lblInstallasjonEndret, 			new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlInstallasjon.add(txtInstallasjonSistendretAv, 	new GridBagConstraints(6, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        lblDndPortoppsett.setText(getBundle().getString("dfu.modell.dndPortoppsett"));

        txtEnhetKategori.setPreferredSize(stdDim);
        txtEnhetLeverandor.setPreferredSize(stdDim);
        
        txtEnhetKategori.setEditable(false);
        txtEnhetLeverandor.setEditable(false);
        
        pnlEnhet.setBorder(BorderFactory.createTitledBorder(getBundle().getString("dfu.enhet.title")));
        lblEnhetSerial.setText(getBundle().getString("dfu.enhet.serial"));
        lblEnhetVersjon.setText(getBundle().getString("dfu.enhet.versjon"));
        lblEnhetNyDato.setText(getBundle().getString("dfu.enhet.nyDato"));
        lblEnhetSisteSignal.setText(getBundle().getString("dfu.enhet.sisteSignal"));
        lblEnhetTelefon.setText(getBundle().getString("dfu.enhet.telefon"));
        lblEnhetSim.setText(getBundle().getString("dfu.enhet.sim"));
        lblEnhetStatus.setText(getBundle().getString("dfu.enhet.status"));
        lblEnhetNotat.setText(getBundle().getString("dfu.enhet.notat"));
        scrEnhetNotat.setViewportView(txtAreaEnhetNotat);
        btnEnhetStatus.setText("...");
        btnVelgDfu.setText(getBundle().getString("dfu.enhet.knappVelg"));
        btnVisPortOppsett.setText(getBundle().getString("dfu.enhet.visPortOppsett"));
        lblEnhetModell.setText(getBundle().getString("dfu.modell.modell"));
        lblEnhetKategori.setText(getBundle().getString("dfu.modell.kategori"));
        lblEnhetLeverandor.setText(getBundle().getString("dfu.modell.leverandor"));

        txtEnhetSerial.setPreferredSize(stdDim);
        txtEnhetVersjon.setPreferredSize(stdDim);
        txtEnhetNyDato.setPreferredSize(stdDim);
        txtEnhetSisteSignal.setPreferredSize(stdDim);
        txtEnhetTelefon.setPreferredSize(stdDim);
        txtEnhetSim.setPreferredSize(stdDim);
        txtEnhetStatus.setPreferredSize(stdDim);
        txtDfumodell.setPreferredSize(stdDim);
        
        scrEnhetNotat.setPreferredSize(new Dimension(100, 80));

        txtEnhetSerial.setEditable(false);
        txtEnhetVersjon.setEditable(false);
        txtEnhetNyDato.setEditable(false);
        txtEnhetSisteSignal.setEditable(false);
        txtEnhetTelefon.setEditable(false);
        txtEnhetSim.setEditable(false);
        txtAreaEnhetNotat.setEditable(false);
        txtEnhetStatus.setEditable(false);
        txtDfumodell.setEditable(false);
        
        pnlEnhet.setLayout(new GridBagLayout());
        pnlEnhet.add(lblEnhetSerial, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetSerial, 		new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlEnhet.add(lblEnhetVersjon, 		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetVersjon, 		new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlEnhet.add(btnVelgDfu, 			new GridBagConstraints(4, 0, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlEnhet.add(btnVisPortOppsett, 	new GridBagConstraints(4, 1, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));

        pnlEnhet.add(lblEnhetNyDato, 		new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetNyDato, 		new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlEnhet.add(lblEnhetSisteSignal, 	new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetSisteSignal, 	new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlEnhet.add(lblEnhetTelefon, 		new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetTelefon, 		new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlEnhet.add(lblEnhetSim, 			new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetSim, 			new GridBagConstraints(3, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
        pnlEnhet.add(lblEnhetStatus, 		new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlEnhet.add(txtEnhetStatus, 		new GridBagConstraints(5, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlEnhet.add(btnEnhetStatus, 		new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        pnlEnhet.add(lblEnhetModell, 		new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 11, 5), 0, 0));
        pnlEnhet.add(txtDfumodell, 			new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 11), 0, 0));
        pnlEnhet.add(lblEnhetKategori, 	new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 5), 0, 0));
        pnlEnhet.add(txtEnhetKategori, 	new GridBagConstraints(3, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 11), 0, 0));
        pnlEnhet.add(lblEnhetLeverandor, 	new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 5), 0, 0));
        pnlEnhet.add(txtEnhetLeverandor, 	new GridBagConstraints(5, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 5), 0, 0));

        pnlEnhet.add(lblEnhetNotat, 		new GridBagConstraints(0, 4, 7, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlEnhet.add(scrEnhetNotat, 	new GridBagConstraints(0, 5, 7, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));

        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(pnlInstallasjon, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 5, 11), 0, 0));
        pnlMain.add(pnlEnhet, 			new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 11, 11), 0, 0));
    
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
        lblDndPortoppsettIkkeTilgjengelig.setText(getBundle().getString("dfu.enhet.dndPortoppsettIkkeTilgjengelig"));
        pnlDndPortoppsettIkkeTilgjengelig.setLayout(new GridBagLayout());
        pnlDndPortoppsettIkkeTilgjengelig.add(lblDndPortoppsettIkkeTilgjengelig, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

	@Override
	public Dimension getDialogSize() {
		return new Dimension(680, 450);
	}
	
	@Override
	public Installasjon newEntityDialogBox() {
		isSaved = false;
		
		//Legger til en knapperad
		pnlButtons = new JPanel();
    	btnOk = new JButton();
    	btnAvbryt = new JButton();
    	
    	btnOk.setText(getBundle().getString("dfu.installasjon.ok"));
    	btnAvbryt.setText(getBundle().getString("dfu.installasjon.avbryt"));
    	
    	btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(mipssEntity.getAktivFraDato() == null) {
					JOptionPane.showMessageDialog(dialog, getBundle().getString("installasjon.manglerFraDatomelding"));
				} else {
					if (validateDates()){
						saveChanges();
						closeDialog();
					}
				}
			}
    	});
    	
    	btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeDialog();
			}
    	});
    	
    	pnlButtons.setLayout(new GridBagLayout());
    	pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
    	pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
    	this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
    	
		return super.newEntityDialogBox();
	}
	private boolean validateDates(){
		StringBuilder message = new StringBuilder("Installasjonen overlapper med andre installasjoner:\n");
		List<DfuInstallasjonV> kjoretoyInstallasjoner = bean.getDfuInstallasjonVList(mipssEntity.getKjoretoy().getId());
		Date aktivFraDato = mipssEntity.getAktivFraDato();
		Date aktivTilDato = mipssEntity.getAktivTilDato();
		boolean installasjonOk = true;
		for (DfuInstallasjonV i:kjoretoyInstallasjoner){
			if (i.getId()!=null && !i.getId().equals(mipssEntity.getId())){
				if (CalendarUtil.isOverlappende(aktivFraDato, aktivTilDato, i.getAktivFraDato(), i.getAktivTilDato(), false)){
					message.append("Installasjon ("+i.getId()+"):"+MipssDateFormatter.formatDate(i.getAktivFraDato())+"-"+MipssDateFormatter.formatDate(i.getAktivTilDato())+"\n");
					installasjonOk= false;
				}
			}
		}
		Dfuindivid dfuindivid = bean.getDfuindivid(mipssEntity.getDfuindivid().getId());
		if (dfuindivid!=null){
			List<Installasjon> dfuInstallasjoner = dfuindivid.getInstallasjonList();
			for (Installasjon i:dfuInstallasjoner){
				if (i.getId()!=null && !i.getId().equals(mipssEntity.getId())){
					if (CalendarUtil.isOverlappende(aktivFraDato, aktivTilDato, i.getAktivFraDato(), i.getAktivTilDato(), false)){
						message.append("Dfuindivid ("+i.getDfuId()+"):"+MipssDateFormatter.formatDate(i.getAktivFraDato())+"-"+MipssDateFormatter.formatDate(i.getAktivTilDato())+"\n");
						installasjonOk= false;
					}
				}
			}
		}
		if (!installasjonOk){
			message.append("\nKan ikke legge til installasjonen, fjern de overlappende periodene\n");
			JOptionPane.showMessageDialog(this, message, "Overlappende installasjoner", JOptionPane.WARNING_MESSAGE);
		}
		return installasjonOk;
	}
}
