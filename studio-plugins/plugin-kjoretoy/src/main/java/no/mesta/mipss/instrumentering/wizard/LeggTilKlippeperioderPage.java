package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;
import java.awt.Container;

import no.mesta.mipss.kjoretoy.component.stroing.KlippeperioderListPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.netbeans.spi.wizard.WizardPage;

public class LeggTilKlippeperioderPage extends WizardPage {
	private MipssPlugin mipssPlugin;
	private WizardLine wizardLine;
	private Container parent;
	private boolean enkelWizard;
	
	private KlippeperioderListPanel klippeperioderListPanel;
	
	public LeggTilKlippeperioderPage(MipssPlugin mipssPlugin, WizardLine wizardLine, Container parent, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard) {
		super(stepId, stepDescription, autoListen);
		this.mipssPlugin = mipssPlugin;
		this.wizardLine = wizardLine;
		this.parent = parent;
		this.enkelWizard = enkelWizard;
		
		initGui();
	}
	
	private void initGui() {
		this.setLayout(new BorderLayout());
	}
	
	@Override
	protected void renderingPage() {
		if(klippeperioderListPanel == null) {
			klippeperioderListPanel = new KlippeperioderListPanel(wizardLine, null, wizardLine.getKjoretoy(), parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), mipssPlugin);
			this.add(klippeperioderListPanel, BorderLayout.CENTER);
		}
	}
}
