package no.mesta.mipss.kjoretoy.component.kjoretoyutstyr;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JPanel;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.kjoretoy.plugin.MainInstrumentationPanel;
import no.mesta.mipss.kjoretoy.tablebrowser.Browsable;
import no.mesta.mipss.kjoretoy.tablebrowser.BrowserPanel;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrView;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Viser utstyret på kjøretøyet pa egen panel.
 * Visningen av gruppe, subgruppe og modell bestemmes av KjoretoyUtstyr entiteten:
 * <ul>
 * <li>Valget av subgruppe avhenger av den valgte gruppen.</li>
 * <li>Valget av modell avhenger av den valgte subgruppen.</li>
 * <li>Valget av individ avhenger av den valgte modellen</li>
 * <li>Det valgte individet vises til slutt</li>
 * </ul>
 * 
 * Oppdateringene lagres i to helt uavhengige bolker: de som gjelder selvet {@code KjoretoyUtstyr} og
 * de som gjelder den {@code Utstyrindivid}.
 * 
 * BeansBinding brukes for å koble sammen enkeltfelt i {@code KjoretoyUtstyr} og i {@code Utstyrindivid}.
 * 
 * Kaskadekoblingen ev Comboboxer forregår utenfor BeansBinding.
 * 
 * Properties i {@code KjoretoyUtstyr} som endres av ComboBoxene er kun registrert i {@code FiledToolkit}, selve
 * oppdateringen av disse properties foregår i Comboboxenes egne Listeners.
 * 
 * @author jorge
 */
public class KjoretoyUtstyrPanel  extends AbstractMipssEntityPanel<KjoretoyUtstyr> implements Browsable {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
    private boolean canUpdate = true;
    
    private FieldToolkit installasjonBinding;
    private FieldToolkit individBinding;

    private JButton avbrytButton;
//    private JButton lagreButton;

    private BrowserPanel<KjoretoyUtstyrPanel, KjoretoyUtstyrView, KjoretoyUtstyr> browserParent;
    private MipssKjoretoy bean;
    private JPanel pnlMain;
    private KjoretoyUtstyrIoPanel kjoretoyUtstyrIoPanel;
    private boolean enkelVisning;
	private final MipssPlugin plugin;
    
    // End of variables declaration
    public KjoretoyUtstyrPanel(KjoretoyUtstyr utstyr, MipssKjoretoy bean, ResourceBundle bundle, boolean enkelVisning, MipssPlugin plugin) {
        super(utstyr);
        this.bean = bean;
		this.plugin = plugin;
        super.setBundle(bundle);
        super.setDialogBoxTitle(getBundle().getString("utstyr.title"));
        
        this.enkelVisning = enkelVisning;
        
        // opprett beansbinding sentralt
        installasjonBinding = FieldToolkit.createToolkit();
        individBinding = FieldToolkit.createToolkit();
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyr.ingenVisning")); 
            return;
        }
        if (utstyr != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("utstyr.ingenVisning")); 
            }
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    /**
     * @inheritDoc
     * @param browserParent
     */
    @Override
    public void setBrowserParent(BrowserPanel browserParent) {
        this.browserParent = browserParent;
    }

    /**
     * Binder alle entitetsfelt i installasjonsdelen som skal vises til sine GUI komponenter. 
     * Registrer felt med forhold til gruppe/subgruppe m.m.
     * Bind lagreknappen.
     */
    private void initEntityBindings() {
        // fjern alle koblinger fra forrige entitet.
        installasjonBinding.unbindAndRemove();
        
        // bind knappene
//        if (lagreButton != null) {
//            installasjonBinding.bindReactiveButton(lagreButton);
//        }
        
        installasjonBinding.bind();
    }

    protected boolean isSaved() {
    	// TODO: implementer update/create
    	return true;
    }

    /**
     * Bestemmer utseendet og tilgang til utvalgte gui komponenter og initialiserer Listemers til conboBoxer.
     */
    protected void initComponentsPresentation() {
        initComponents();

    }
    
    /**
     * Bruker bindingene for å koble installasjon og individ til sine entiteter, men
     * vanlig gui komponentoppdatering for grupper/subgrupper/++.
     */
    protected void loadFromEntity() {
        // korriger GUI fra eventuelle endringer for forrige entitet
//        lagreButton.setEnabled(false);
        
        if(mipssEntity == null) {
        	installasjonBinding.unbindAndRemove();
        	clearForm();
        }
        else {
	        // bind den nye entiteten
	        try {
	            initEntityBindings();
	        }
	        catch (Exception e) {
	            logger.error("Kan ikke binde pga: " + e);
	            e.printStackTrace();
	        }

	        // init den nedre delen av skjemaet med IOporter og perioder
	        kjoretoyUtstyrIoPanel.setMipssEntity(mipssEntity);
        }
    }

    /**
     * Fjerner al informasjon fra UI komponentene.
     */
    private void clearForm() {
    	kjoretoyUtstyrIoPanel.setMipssEntity(null);
    }
    
    public void enableUpdating(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    /**
     * {@inheritDoc}
     */
    public void saveChanges() {
        if (installasjonBinding.isFieldsChanged()) {
        	logger.debug("installasjonBinding har endret seg");
            // identifiser brukeren for å lagre endringene med dens navn
        	if(MainInstrumentationPanel.getBruker() != null) {
        		mipssEntity.getOwnedMipssEntity().setUserSignatur(MainInstrumentationPanel.getBruker().getSignatur());
        	}
            try {
            	KjoretoyUtstyr kjoretoyUtstyr = bean.updateKjoretoyUtstyr(mipssEntity);
            	installasjonBinding.clearFieldChanges();
                super.setMipssEntity(kjoretoyUtstyr);
            }
            catch (Exception e) {
                KjoretoyUtils.showException(e, getBundle().getString("utstyr.ingenEndringslagring"));
                return;
            }
            installasjonBinding.clearFieldChanges();
        }
        
        if (browserParent != null) {
            browserParent.reloadTable(mipssEntity);
        }
//        lagreButton.setEnabled(false);
    }

    /**
     * {@inheritDoc}
     */
    public boolean saveChangesAndContinue() {
    	logger.debug("installasjonBinding endret: " + installasjonBinding.isFieldsChanged() + ". individBinding endret: " + individBinding.isFieldsChanged());
        if(! installasjonBinding.isFieldsChanged() && ! individBinding.isFieldsChanged()) {
            return false;
        }
        
        Container browser = this.getParent();
        
        // vis et dialogvindu for å spørre brukeren
        KjoretoyUtils.DialogElection elected = KjoretoyUtils.showLagreForkastDialog(this, 
            getBundle().getString("dialog.info.lagring"), 
            getBundle().getString("dialog.title.lagring"));
            
        switch (elected) {
            case LAGRE:
                saveChanges();
                return false;
            case FORKAST:
                return false;
            case FORTSETT:
                return true;
            default:
                return false;
        }
    }

    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }
    
    /**
     * Vi har ingen OK knappe
     * @param button
     */
    public void setOkButton(JButton button) {
    }

    public void setAvbrytButton(JButton button) {
        avbrytButton = button;
        avbrytButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        logger.debug("avbrytButton.actionListener.actionPerformed kalt");
                        if (! saveChangesAndContinue()) {
                            Container container = ((Container)e.getSource()).getParent();
                            BrowserPanel bp = (BrowserPanel)container.getParent();
                            bp.closeDialogBox();
                        }
                    }
                });
    }

    public void setLagreButton(JButton button) {
//        lagreButton = button;
//        lagreButton.addActionListener(new ActionListener() {
//
//                    public void actionPerformed(ActionEvent e) {
//                        logger.debug("lagreButton.actionListener.actionPerformed kalt");
//                        saveChanges();
//                    }
//                });
    }
    

    private void initComponents() {
        pnlMain = new JPanel();
        kjoretoyUtstyrIoPanel = PanelFactory.createKjoretoyUtstyrIoPanel(null, plugin);
        pnlMain.setLayout(new GridBagLayout());
        if(enkelVisning) {
        } else {
        	pnlMain.add(kjoretoyUtstyrIoPanel, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
    }

	@Override
	protected Dimension getDialogSize() {
		return new Dimension(400, 300);
	}
}
