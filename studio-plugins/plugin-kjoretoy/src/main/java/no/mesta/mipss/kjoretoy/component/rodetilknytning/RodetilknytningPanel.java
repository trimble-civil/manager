package no.mesta.mipss.kjoretoy.component.rodetilknytning;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.plugin.MipssPlugin;

import javax.swing.*;
import java.awt.*;

public class RodetilknytningPanel extends AbstractKjoretoyPanel implements CompositePanel {

    private MipssKjoretoy mipssKjoretoy;
    private JButton btnLagre = new JButton();
    private JButton btnAvbryt = new JButton();
    private JPanel pnlButtons = new JPanel();
    private RodetilknytningListPanel pnlRodetilknytning;
    private boolean loaded = false;

    private Container parent;
    private MaintenanceContract maintenanceContract;
    private MipssPlugin parentPlugin;

    public RodetilknytningPanel(InstrumentationController controller, Kjoretoy kjoretoy, Container parent, MipssKjoretoy mipssKjoretoy, MaintenanceContract maintenanceContract, MipssPlugin parentPlugin) {
        super(kjoretoy, controller);

        this.parent = parent;
        this.mipssKjoretoy = mipssKjoretoy;
        this.maintenanceContract = maintenanceContract;
        this.parentPlugin = parentPlugin;

        pnlRodetilknytning = new RodetilknytningListPanel(controller, this, kjoretoy, parent, mipssKjoretoy, maintenanceContract, parentPlugin);

        initButtons();
        initGui();

    }

    private void initButtons() {
        btnLagre.setAction(getInstrumentationController().getLagreKjoretoyAction());
        btnAvbryt.setAction(getInstrumentationController().getAvbrytKjoretoyAction());
    }

    private void initGui() {
        pnlButtons.setLayout(new GridBagLayout());
        pnlButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        this.setLayout(new GridBagLayout());
        this.add(pnlRodetilknytning, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 0, 11), 0, 0));
        this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 11, 11), 0, 0));
    }

    @Override
    public void enableUpdating(boolean canUpdate) {
    }

    @Override
    protected void initComponentsPresentation() {
    }

    @Override
    protected void loadFromEntity() {
        pnlRodetilknytning.setMipssEntity(getMipssEntity());
        enableSaveButton(false);
        loaded = true;
    }

    @Override
    public void setSessionBean(Object bean) {
        this.mipssKjoretoy = (MipssKjoretoy)bean;
    }

    @Override
    public void enableSaveButton(boolean enable) {
        btnLagre.setEnabled(enable);
        btnAvbryt.setEnabled(enable);

        if(enable) {
            kjoretoyChanged();
        }
    }

}
