package no.mesta.mipss.kjoretoy.plugin;

import javax.swing.JPanel;

/**
 * Kontrollerer og viser statusrapportering. Har hovedsakelig to tilstander:
 * <ul>
 * <li>Cleared: teksten viser "Ferdig" og progressbar er deaktivert.</li>
 * <li>Active: teksten viser relevant tekst og proressbar er aktive</li>
 * </ul>
 * @author jorge
 * @version
 */
@SuppressWarnings("serial")
public class StatusPanel extends JPanel {
    private static final String FERDIG = "Ferdig";
    private String statusMessage;

    public StatusPanel() {
        try {
            initComponents();
        } catch (Exception e) {
            e.printStackTrace();
        }
        clearStatusMessage();
    }

    /**
     * Viser statusMessage og starter probressBar'en til å
     * gåfrem og tilbake.
     * @param statusMessage er meldingen som skal vises.
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        statusMessageView.setText(statusMessage);
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(false);
    }
    
    /**
     * Stopper ventemodus, hvis den var på, og viser feilmeldingen. 
     * Stopper progressBar'en også.
     * @param errorMsg
     */
    public void setErrorMessage(String errorMsg) {
        statusMessageView.setText(errorMsg);
        progressBar.setIndeterminate(false);
    }

    /**
     * Viser den predefinerte "Ferdig" melding og stopper progressBar'en.
     */
    public void clearStatusMessage() {
        setStatusMessage(FERDIG);
        progressBar.setIndeterminate(false);
    }

    public String getStatusMessage() {
        return statusMessage;
    }
    
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        progressBar = new javax.swing.JProgressBar();
        statusMessageView = new javax.swing.JLabel();

        setName("Form"); // NOI18N

        jPanel1.setName("jPanel1"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        statusMessageView.setName("statusMessageView"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(643, Short.MAX_VALUE)
                .addComponent(statusMessageView)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                .addComponent(statusMessageView)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>


    // Variables declaration - do not modify
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusMessageView;
    // End of variables declaration

}
