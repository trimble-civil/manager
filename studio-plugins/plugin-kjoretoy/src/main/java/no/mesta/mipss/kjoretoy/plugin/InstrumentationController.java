package no.mesta.mipss.kjoretoy.plugin;

import javax.swing.Action;

import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;


public interface InstrumentationController {

 /**
     * Call back metoden for å nøkklene til en MIPSS entitet 
     * som er blitt valgt av brukeren.
     * @param itemId er nøkkelen til item i databasen.
     * @param useInfoPanel er true for å overkjøre panelet som brukeren ser på
     * og åpne hovedpanelet.
     * @param index av den leverte item i trefflisten.
     */
    public void setSelectedItem (Long itemId, boolean useInfoPanel, int index);
    
    /**
     * Call back metode som brukes for å melde at en JPanel er blitt valgt av brukeren.
     * Brukes b.a. fra JTabbedPane når brukeren velger en Tab.
     * @param selectedTab er den valgte JPanel.
     */
    public void handleSelectedPanel(TabBase selectedTab);
    
    /**
     * Instantierer et objekt av type baseClass fra databasen og viser den i det leverte panelet.
     * @param baseClass
     * @param id
     * @param dialogPanel
     * @param parent
     * @return
     */
    public Object openDialogWindow(Class<?> baseClass, long id, AbstractMipssEntityPanel<?> dialogPanel, AbstractMipssEntityPanel<?> parent);
    
    /**
     * Registrerer view for kjøretøy 
     * @param abstractKjoretoyPanel
     */
    public void registerKjoretoyPanel(AbstractKjoretoyPanel abstractKjoretoyPanel);
    
    public Action getLagreKjoretoyAction();
    
    public Action getAvbrytKjoretoyAction();
    
    /**
     * Brukes for å melde fra om endringer i kjøretøyet
     */
    public void kjoretoyChanged();
    
    public String getLoggedOnUser();
}
