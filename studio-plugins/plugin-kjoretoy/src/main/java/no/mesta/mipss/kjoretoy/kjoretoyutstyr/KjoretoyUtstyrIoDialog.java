package no.mesta.mipss.kjoretoy.kjoretoyutstyr;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXDatePicker;

@SuppressWarnings("serial")
public class KjoretoyUtstyrIoDialog extends JDialog {
	private JPanel pnlMain = new JPanel();
	private JLabel lblSensortype = new JLabel();
	private JComboBox cbxSensortype = new JComboBox();
	private JLabel lblDfuModell = new JLabel();
	private JComboBox cbxDfuModell = new JComboBox();
	private JLabel lblIoliste = new JLabel();
	private JComboBox cbxIoListe = new JComboBox();
	private JLabel lblPortNr = new JLabel();
	private JFormattedTextField txtPortNr = new JFormattedTextField();
	private JLabel lblFraDato = new JLabel();
	private JLabel lblTilDato = new JLabel();
	private JXDatePicker datePckrFraDato = new JXDatePicker();
	private JXDatePicker datePckrTilDato = new JXDatePicker();
	private JLabel lblAvgrens = new JLabel();
	private JCheckBox chkAvgrens = new JCheckBox();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	
	private KjoretoyUtstyrController kjoretoyUtstyrController;
	private KjoretoyUtstyrIo kjoretoyUtstyrIo;
	private BindingGroup bindingGroup;
	private boolean nyPeriode;
	
	public KjoretoyUtstyrIoDialog(KjoretoyUtstyrController kjoretoyUtstyrController, Window window, KjoretoyUtstyrIo kjoretoyUtstyrIo, boolean nyPeriode) {
		super(window, KjoretoyUtils.getPropertyString("ioperiodedialog.title"), ModalityType.APPLICATION_MODAL);
		this.kjoretoyUtstyrController = kjoretoyUtstyrController;
		
		//Lager oss en kopi av originalen. Letter avbryt-implementasjon
		this.kjoretoyUtstyrIo = new KjoretoyUtstyrIo();
		this.kjoretoyUtstyrIo.setId(kjoretoyUtstyrIo.getId());
		this.kjoretoyUtstyrIo.setFraDato(kjoretoyUtstyrIo.getFraDato());
		this.kjoretoyUtstyrIo.setTilDato(kjoretoyUtstyrIo.getTilDato());
		this.kjoretoyUtstyrIo.setSensortype(kjoretoyUtstyrIo.getSensortype());
		this.kjoretoyUtstyrIo.setKjoretoyUtstyr(kjoretoyUtstyrIo.getKjoretoyUtstyr());
		this.kjoretoyUtstyrIo.setIoliste(kjoretoyUtstyrIo.getIoliste());
		this.kjoretoyUtstyrIo.setBitNr(kjoretoyUtstyrIo.getBitNr());
		this.kjoretoyUtstyrIo.setSensortypeNavn(kjoretoyUtstyrIo.getSensortypeNavn());
		this.kjoretoyUtstyrIo.getOwnedMipssEntity().setOpprettetAv(kjoretoyUtstyrIo.getOwnedMipssEntity().getOpprettetAv());
		this.kjoretoyUtstyrIo.getOwnedMipssEntity().setOpprettetDato(kjoretoyUtstyrIo.getOwnedMipssEntity().getOpprettetDato());
		this.nyPeriode = nyPeriode;
		
		bindingGroup = new BindingGroup();
		initLabels();
		initComboboxes();
		initTextFields();
		initDatePickers();
		initButtons();
		initGui();
	}

	public KjoretoyUtstyrIo showDialog() {
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		
		bindingGroup.bind();
		setVisible(true);
		bindingGroup.unbind();
		
		return kjoretoyUtstyrIo;
	}
	
	private void initLabels() {
		lblSensortype.setText(KjoretoyUtils.getPropertyString("ioperiodedialog.lblSensortype"));
		lblDfuModell.setText(KjoretoyUtils.getPropertyString("ioperiodedialog.lblDfuModell"));
		lblIoliste.setText(KjoretoyUtils.getPropertyString("ioperiodedialog.lblIoliste"));
		lblPortNr.setText(KjoretoyUtils.getPropertyString("ioperiodedialog.lblPortNr"));
		lblFraDato.setText(KjoretoyUtils.getPropertyString("utstyrperiodedialog.lblFraDato"));
		lblTilDato.setText(KjoretoyUtils.getPropertyString("utstyrperiodedialog.lblTilDato"));
		lblAvgrens.setText(KjoretoyUtils.getPropertyString("utstyrperiodedialog.lblAvgrens"));
	}

	private void initComboboxes() {
		List<Sensortype> sensortypeList = KjoretoyUtils.getKjoretoyBean().getSensortypeList();
		MipssComboBoxModel<Sensortype> cbxModelSensortype = new MipssComboBoxModel<Sensortype>(sensortypeList);
		cbxSensortype.setModel(cbxModelSensortype);
		cbxSensortype.setRenderer(new MipssListCellRenderer<Sensortype>());
		
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyUtstyrIo, "sensortype", cbxModelSensortype, "selectedItem", BindingHelper.READ_WRITE));
		
		List<Dfumodell> dfuModellList = KjoretoyUtils.getKjoretoyBean().getDfumodellList();
		//Luker ut modeller som ikke har Ioliste
		for(Iterator<Dfumodell> it = dfuModellList.iterator(); it.hasNext();) {
			Dfumodell dfumodell = it.next();
			if(dfumodell.getIolisteList() == null || dfumodell.getIolisteList().isEmpty()) {
				it.remove();
			}
		}
		MipssComboBoxModel<Dfumodell> cbxModelDfuModell = new MipssComboBoxModel<Dfumodell>(dfuModellList);
		cbxDfuModell.setModel(cbxModelDfuModell);
		cbxDfuModell.setRenderer(new MipssListCellRenderer<Dfumodell>());
		
		MipssComboBoxModel<Ioliste> cbxModelIoliste = new MipssComboBoxModel<Ioliste>(new ArrayList<Ioliste>());
		cbxIoListe.setModel(cbxModelIoliste);
		cbxIoListe.setRenderer(new MipssListCellRenderer<Ioliste>());
		
		if(!nyPeriode && kjoretoyUtstyrIo.getIoliste() != null) {
			cbxModelDfuModell.setSelectedItem(kjoretoyUtstyrIo.getIoliste().getDfumodell());
		}

		bindingGroup.addBinding(BindingHelper.createbinding(cbxModelDfuModell, "${selectedItem.iolisteList}", cbxModelIoliste, "entities", BindingHelper.READ));		
		
		cbxDfuModell.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyUtstyrIo, "ioliste", cbxModelIoliste, "selectedItem", BindingHelper.READ_WRITE));
	}
	
	private void initTextFields() {
		txtPortNr.setEditable(false);
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyUtstyrIo, "${ioliste == null ? null : ioliste.bitNr}", txtPortNr, "text", BindingHelper.READ));
	}
	
	private void initDatePickers() {
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyUtstyrIo, "fraDato", datePckrFraDato, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyUtstyrIo, "tilDato", datePckrTilDato, "date", BindingHelper.READ_WRITE));
	
		if(!nyPeriode) {
			datePckrFraDato.setEditable(false);
		}
		
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyUtstyrIo, "${tilDato != null}", chkAvgrens, "selected", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(chkAvgrens, "selected", datePckrTilDato, "editable", BindingHelper.READ));
		
		lblAvgrens.setText(KjoretoyUtils.getPropertyString("stroing.lblAvgrens"));
		chkAvgrens.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!chkAvgrens.isSelected()) {
					kjoretoyUtstyrIo.setTilDato(null);
				} else if(kjoretoyUtstyrIo.getTilDato() == null) {
					kjoretoyUtstyrIo.setTilDato(kjoretoyUtstyrIo.getFraDato());
				}
			}
		});
	}

	private void initButtons() {
		btnOk.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("utstyrperiodedialog.btnOk")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(kjoretoyUtstyrController.verifiserKjoretyrUtstyrIo(KjoretoyUtstyrIoDialog.this, kjoretoyUtstyrIo)) {
					dispose();
				}
			}
		});
		
		btnAvbryt.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("utstyrperiodedialog.btnAvbryt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				kjoretoyUtstyrIo = null;
				dispose();
			}
		});
	}
	
	private void initGui() {
		Dimension dim = new Dimension(120, 19);
		cbxSensortype.setPreferredSize(dim);
		cbxDfuModell.setPreferredSize(dim);
		cbxIoListe.setPreferredSize(dim);
		txtPortNr.setPreferredSize(dim);
		datePckrFraDato.setPreferredSize(dim);
		datePckrTilDato.setPreferredSize(dim);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblSensortype, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxSensortype, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblDfuModell, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxDfuModell, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblIoliste, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxIoListe, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblPortNr, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(txtPortNr, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblFraDato, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(datePckrFraDato, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblAvgrens, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(chkAvgrens, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblTilDato, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(datePckrTilDato, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 0, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 11, 11), 0, 0));
	}
}
