/**
 * 
 */
package no.mesta.mipss.kjoretoy.utils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * @author jorge
 *
 */
public class DokumentFilter extends FileFilter {
	private final static String[] types = {"doc", "docx",  
										  "xls", "xlsx", 
										  "ppt", "pptx",  
										  "pdf", "txt"};

	/**
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
            return true;
        }

        String extension = FileUtils.getFileNameExtension(f.getName());
        if (extension != null) {
        	for (String t : types) {
        		if(extension.equals(t)) {
        			return true;
        		}
        	}
        }
        return false;
	}

	/**
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Kun relevante dokumenter";
	}

}
