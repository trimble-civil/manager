package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.bestilling.BestillingSkjema;
import no.mesta.mipss.bestilling.BestillingSkjemaType;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

public class KjoretoyOrdreListPanel extends AbstractKjoretoyPanel implements CompositePanel  {
	private JPanel pnlOrdreList = new JPanel();
	private JScrollPane scrlPaneOrdreList = new JScrollPane();
	private JXTable tblOrdreList = new JXTable();
	private MipssBeanTableModel<Kjoretoyordre> tblModelOrdre;
	private JButton btnEndre = new JButton();
	private JButton btnNy = new JButton();
	private JButton btnSlett = new JButton();
	private JButton btnTilPDF = new JButton();

    private Container parent;
	private MipssKjoretoy mipssKjoretoy;
	private MaintenanceContract maintenanceContract;
	private MipssPlugin parentPlugin;
    private boolean loaded = false;
    private KjoretoyOrdreViewPanel parentPanel;
    private boolean begrensetTilgangEndring;
	
	public KjoretoyOrdreListPanel(InstrumentationController controller, KjoretoyOrdreViewPanel parentPanel, Kjoretoy kjoretoy, Container parent, MipssKjoretoy mipssKjoretoy, MaintenanceContract maintenanceContract, MipssPlugin parentPlugin, boolean begrensetTilgangEndring) {
		super(kjoretoy, controller);
		
		this.parentPanel = parentPanel;
		this.parent = parent;
		this.mipssKjoretoy = mipssKjoretoy;
		this.maintenanceContract = maintenanceContract;
		this.parentPlugin = parentPlugin;
		this.begrensetTilgangEndring = begrensetTilgangEndring;
		
		initButtons();
		initTable();
		initGui();
	}
	
	private void initButtons() {
		btnNy.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.leggTil"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Kjoretoyordre kjoretoyordre = new Kjoretoyordre();
				kjoretoyordre.setKjoretoy(mipssEntity);
				kjoretoyordre.getOwnedMipssEntity().setOpprettetAv(parentPlugin.getLoader().getLoggedOnUserSign());
				kjoretoyordre.setSignatur(parentPlugin.getLoader().getLoggedOnUserSign());
				kjoretoyordre.setVerkstedAdminFlagg(Boolean.FALSE);
				LeggTilRelasjonDialog<Kjoretoyordre> leggTilRelasjonDialog = null; 
				if(parent instanceof JFrame) {
					leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyordre>((JFrame)parent, new KjoretoyordreDetaljPanel(mipssEntity, mipssKjoretoy, maintenanceContract, kjoretoyordre, parentPlugin.getLoader().getLoggedOnUserSign(), null));
				} else {
					leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyordre>((JDialog)parent, new KjoretoyordreDetaljPanel(mipssEntity, mipssKjoretoy, maintenanceContract, kjoretoyordre, parentPlugin.getLoader().getLoggedOnUserSign(), null));
				}
				kjoretoyordre = leggTilRelasjonDialog.showDialog();
				
				if(kjoretoyordre != null) {
					Kjoretoy k = getMipssEntity();
					List<Kjoretoyordre> lko = k.getKjoretoyordreList();
					lko.add(kjoretoyordre);
					tblModelOrdre.fireTableModelEvent(0, lko.size(), TableModelEvent.INSERT);
					enableSaveButton(true);
				}
			}
		});
		
		btnEndre.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.endre"), IconResources.DETALJER_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblOrdreList.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblOrdreList.getRowCount()) {
					Kjoretoyordre kjoretoyordre = tblModelOrdre.get(tblOrdreList.convertRowIndexToModel(selectedRow));
					endreOrdre(kjoretoyordre);
					tblModelOrdre.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
				}
			}
		});
		
		btnSlett.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.slett"), IconResources.REMOVE_ITEM_ICON){
			public void actionPerformed(ActionEvent e){
				int selectedRow = tblOrdreList.getSelectedRow();
				slettOrdre(selectedRow);
			}
		});
		
		btnTilPDF.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.visBestillingsskjema"), IconResources.ADOBE_16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblOrdreList.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblOrdreList.getRowCount()) {
					Kjoretoyordre kjoretoyordre = tblModelOrdre.get(tblOrdreList.convertRowIndexToModel(selectedRow));
					if(kjoretoyordre != null) {
						createAndOpenPdf(kjoretoyordre);
					}
				}
			}
		});

        if(begrensetTilgangEndring) {
        	btnNy.setEnabled(false);
        	btnEndre.setEnabled(false);
        	btnSlett.setEnabled(false);
        }
	}
	private void slettOrdre(int selectedRow) {
		if(selectedRow >= 0 && selectedRow < tblOrdreList.getRowCount()) {
			int v = JOptionPane.showConfirmDialog(this, KjoretoyUtils.getPropertyString("message.slettOrdre.desc"), KjoretoyUtils.getPropertyString("message.slettOrdre.title"), JOptionPane.YES_NO_OPTION);
			if (v==JOptionPane.YES_OPTION){
				Kjoretoyordre kjoretoyordre = tblModelOrdre.get(tblOrdreList.convertRowIndexToModel(selectedRow));
				Kjoretoy k = getMipssEntity();
				List<Kjoretoyordre> lko = k.getKjoretoyordreList();
				lko.remove(kjoretoyordre);
				
				mipssKjoretoy.removeOrdre(kjoretoyordre);
				
				tblModelOrdre.fireTableModelEvent(0, lko.size(), TableModelEvent.DELETE);
			}
		}
		
	}
	private void initTable() {
		tblModelOrdre = new MipssBeanTableModel<Kjoretoyordre>("kjoretoyordreList", Kjoretoyordre.class, "id", "kjoretoyordretype", "sisteKjoretoyordrestatus", "prosjektNr", "signatur");
		BindingHelper.createbinding(this, "mipssEntity", tblModelOrdre, "sourceObject", BindingHelper.READ).bind();
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Kjoretoyordre.class, tblModelOrdre.getColumns());
		tblOrdreList.setModel(tblModelOrdre);
		tblOrdreList.setColumnModel(colModel);
		tblOrdreList.setDefaultRenderer(Kjoretoyordretype.class, new RenderableMipssEntityTableCellRenderer<Kjoretoyordretype>());
		tblOrdreList.setDefaultRenderer(Kjoretoyordrestatus.class, new RenderableMipssEntityTableCellRenderer<Kjoretoyordrestatus>());
		
		tblOrdreList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					int selectedRow = tblOrdreList.getSelectedRow();
					if(selectedRow >= 0 && selectedRow < tblOrdreList.getRowCount()) {
						Kjoretoyordre kjoretoyordre = tblModelOrdre.get(tblOrdreList.convertRowIndexToModel(selectedRow));
						createAndOpenPdf(kjoretoyordre);
					}
				}
			}
		});
	}
	
	private void initGui() {
		scrlPaneOrdreList.setViewportView(tblOrdreList);
		
		pnlOrdreList.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("kjoretoysordre.border")));
		pnlOrdreList.setLayout(new GridBagLayout());
		pnlOrdreList.add(scrlPaneOrdreList, new GridBagConstraints(0, 0, 1, 4, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlOrdreList.add(btnNy, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlOrdreList.add(btnSlett, 	new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlOrdreList.add(btnEndre, 	new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlOrdreList.add(btnTilPDF, new GridBagConstraints(1, 3, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlOrdreList, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	private void endreOrdre(Kjoretoyordre kjoretoyordre) {
		if(kjoretoyordre != null) {
			Kjoretoyordre kjoretoyordreKopi = KjoretoyUtils.lagKopiAvOrdre(kjoretoyordre);
			LeggTilRelasjonDialog<Kjoretoyordre> leggTilRelasjonDialog = null;
			if(parent instanceof JFrame) {
				leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyordre>((JFrame)parent, new KjoretoyordreDetaljPanel(mipssEntity, mipssKjoretoy, maintenanceContract, kjoretoyordreKopi, parentPlugin.getLoader().getLoggedOnUserSign(), null));
			} else {
				leggTilRelasjonDialog = new LeggTilRelasjonDialog<Kjoretoyordre>((JDialog)parent, new KjoretoyordreDetaljPanel(mipssEntity, mipssKjoretoy, maintenanceContract, kjoretoyordreKopi, parentPlugin.getLoader().getLoggedOnUserSign(), null));
			}
			kjoretoyordreKopi = leggTilRelasjonDialog.showDialog();
			if(kjoretoyordreKopi != null) {
				kjoretoyordre.merge(kjoretoyordreKopi);
				enableSaveButton(true);
			}
		}
	}
	
    private void createAndOpenPdf(Kjoretoyordre ordre){
    	tblOrdreList.setCursor(new Cursor(Cursor.WAIT_CURSOR));
		final BestillingSkjema bestillingSkjema = 
			new BestillingSkjema(BestillingSkjemaType.RAPPORTUTTAK, 
					null,
					ordre.getId(),
					getInstrumentationController().getLoggedOnUser());
		bestillingSkjema.genererRapport();
		tblOrdreList.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
	
	@Override
	public void enableUpdating(boolean canUpdate) {
	}

	@Override
	protected void initComponentsPresentation() {
	}

	@Override
	protected void loadFromEntity() {
		if(mipssEntity != null) {
			List<Kjoretoyordre> ordreList = mipssEntity.getKjoretoyordreList();
			tblModelOrdre.setEntities(ordreList);
		} else {
			tblModelOrdre.setEntities(new ArrayList<Kjoretoyordre>());
		}
		enableSaveButton(false);
        loaded = true;
	}

	@Override
	public void setSessionBean(Object bean) {
		this.mipssKjoretoy = (MipssKjoretoy)bean;
	}

	@Override
	public void enableSaveButton(boolean enable) {
		if(parentPanel != null) {
			parentPanel.enableSaveButton(enable);
		}
		
		if(enable) {
			kjoretoyChanged();
		}
	}
}
