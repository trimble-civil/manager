package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;
import java.awt.Container;

import no.mesta.mipss.kjoretoy.component.installasjon.KjoretoyDfuListViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.plugin.MipssPlugin;

import org.netbeans.spi.wizard.WizardPage;

public class RegistrerDFUPage extends WizardPage {
	private WizardLine wizardLine;
	private Container parent;
	private KjoretoyDfuListViewPanel kjoretoyDfuListViewPanel;
	private MipssPlugin mipssPlugin;
	
	public RegistrerDFUPage(WizardLine wizardLine, Container parent, String stepId, String stepDescription, boolean autoListen, MipssPlugin mipssPlugin) {
		super(stepId, stepDescription, autoListen);
		this.wizardLine = wizardLine;
		this.parent = parent;
		this.mipssPlugin = mipssPlugin;
		
		initGui();
	}
	
	private void initGui() {
		this.setLayout(new BorderLayout());
	}
	
	@Override
	protected void renderingPage() {
		if(kjoretoyDfuListViewPanel == null) {
			kjoretoyDfuListViewPanel = PanelFactory.createKjoretoyDfuListViewPanel(wizardLine, wizardLine.getKjoretoy(), parent, null, mipssPlugin);
			this.add(kjoretoyDfuListViewPanel, BorderLayout.CENTER);
		}
	}
}
