package no.mesta.mipss.kjoretoy.search;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.TableHelper;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;

/**
 * Viser trefflisten med kjøretøy i egen tabell.
 * @author jorge
 */
public class SearchView extends AbstractMipssEntityPanel<KjoretoyListV> {
	private static final long serialVersionUID = -9195104809157595677L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
    List<KjoretoyListV> hits;
    InstrumentationController controller;
    private JPanel pnlMain;
    private JScrollPane scrlPaneTable;
    private PositionLabel kjoretoyLineLabel;
    private JMipssBeanTable<KjoretoyListV> kjoretoyListTable;
    private MipssBeanTableModel<KjoretoyListV> model;
    private JButton toExcelButton;
    private JButton btnAapneKjoretoy;
    //TODO bind enabled til valgte rad, dersom slettet skal denne være disabled..
    private JButton btnSlettKjoretoy;
    
    public SearchView(KjoretoyListV ignored) {
        super(null);
    }
    
    public SearchView(List<KjoretoyListV> hits, InstrumentationController controller, ResourceBundle bundle) {
        this(null);
        super.setBundle(bundle);
        logger.trace("SearchView oppretttet med " + hits.size() + " kjøretøy");
        try {
            initComponents();
        } 
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoyList.ingenListe"));
        }
        setController(controller);
        setHitList(hits);
        
    }
    
	/**
	 * Dette panelet trenger igen session bean.
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
	}
	
    public void setController (InstrumentationController controller) {
        this.controller = controller;
    }
    
    public void setHitList (List<KjoretoyListV> hits) {
        this.hits = hits;
        model.setEntities(hits);
        kjoretoyLineLabel.setPosition(0);
    }
    
    /**
     * Viser indeks til linjen i tabellen som brukeren har valgt. 
     */
    private class KjoretoySelectionListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    
            if (e.getValueIsAdjusting() || lsm.isSelectionEmpty()) {
                return;
            }
            if (lsm.getMinSelectionIndex() != lsm.getMaxSelectionIndex()) {
                logger.warn("RowSelectionMode er ikke satt ril SINGLE_SELECTION. Godtar kun første linje");
            }
            kjoretoyLineLabel.setPosition(lsm.getMinSelectionIndex());
        }
    }
    
    /**
     * En JLabel som viser hvilken linje brukeren har valgt.
     * Avhenger av riktig format i properties-filen som må inneholde to <code>%d</code> elementer:
     * én for den linjen brukeren har valgt og én for linjeantallet. 
     */
    private class PositionLabel extends JLabel {
        private final String format = getBundle().getString("kjoretoyList.LineCounter");
        public void setPosition(int position) {
            setText(KjoretoyUtils.formatString(format, position + 1, hits.size()));
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {
    	kjoretoyLineLabel = new PositionLabel();
    	
    	model = new MipssBeanTableModel<KjoretoyListV>(KjoretoyListV.class, "eksterntNavn", "regnr", "maskinnummerString", "endretDato", "modell", "typeNavn", "eierNavn", "dfuSerienummer", "dfuTlfnummer", "dfuStatus", "sisteLivstegnDato", "ansKontrakt");
    	MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(KjoretoyListV.class, model.getColumns());
      
        kjoretoyListTable = new JMipssBeanTable<KjoretoyListV>(model, columnModel);
        kjoretoyListTable.setHighlighters(new Highlighter[] { 
				new ColorHighlighter(new HighlightPredicate(){
					@Override
					public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
						KjoretoyListV kjoretoyListV = model.get(kjoretoyListTable.convertRowIndexToModel(adapter.row));
						if (kjoretoyListV==null||kjoretoyListV.getSlettet()==null){
							return false;
						}
						return kjoretoyListV.getSlettet().longValue()==1;
					}
				},Color.pink,
				TableHelper.getCellForeground(),
				TableHelper.getSelectedBackground(),
				TableHelper.getSelectedForeground())
		});
        kjoretoyListTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					int index = kjoretoyListTable.rowAtPoint(e.getPoint());
					aapneKjoretoy(index);
				}
			}
        });
        
        // definer egenskapene for hele tabellen
        kjoretoyListTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        kjoretoyListTable.setFillsViewportHeight(true);
        kjoretoyListTable.setAutoCreateRowSorter(true);
        kjoretoyListTable.setFillsViewportWidth(true);
        kjoretoyListTable.setDoWidthHacks(false);
        
        // oppdatarer linjetellere og posisjonen
        kjoretoyListTable.setRowSelectionAllowed(true);
        kjoretoyListTable.setColumnSelectionAllowed(false);
        kjoretoyListTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        kjoretoyListTable.getSelectionModel().addListSelectionListener(new KjoretoySelectionListener());
      
        pnlMain = new JPanel();
        scrlPaneTable = new JScrollPane();
        
        toExcelButton = new JButton();
        btnAapneKjoretoy = new JButton();
        
        toExcelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				File excelFile = kjoretoyListTable.exportToExcel(JMipssBeanTable.EXPORT_TYPE.ALL_ROWS);
				DesktopHelper.openFile(excelFile);
			}
        });
        
        btnAapneKjoretoy.addActionListener(new ActionListener() {
        	@Override
			public void actionPerformed(ActionEvent e) {
				int index = kjoretoyListTable.getSelectedRow();
				aapneKjoretoy(index);
			}
        });
        btnSlettKjoretoy = new JButton(getSlettKjoretoyAction());
        pnlMain.setBorder(null);
        scrlPaneTable.setViewportView(kjoretoyListTable);
        toExcelButton.setIcon(IconResources.EXCEL_ICON16);
        btnAapneKjoretoy.setIcon(IconResources.DETALJER_ICON_16);
        toExcelButton.setText(getBundle().getString("kjoretoyList.ToExcelButton"));
        btnAapneKjoretoy.setText(getBundle().getString("kjoretoyList.AapneButton"));

        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(scrlPaneTable, 		new GridBagConstraints(0, 0, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        pnlMain.add(btnAapneKjoretoy, 	new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(11, 0, 0, 0), 0, 0));
        pnlMain.add(toExcelButton, 		new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(11, 0, 0, 5), 0, 0));
        pnlMain.add(btnSlettKjoretoy, 	new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(12, 0, 0, 5), 0, 0));
        pnlMain.add(kjoretoyLineLabel, 	new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 0, 0, 0), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
    }

    @SuppressWarnings("serial")
	private Action getSlettKjoretoyAction(){
    	return new AbstractAction(getBundle().getString("kjoretoyList.slettKjoretoy"), IconResources.BUTTON_CANCEL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int viewIndex = kjoretoyListTable.getSelectedRow();
				if(viewIndex >= 0 && viewIndex < kjoretoyListTable.getRowCount()) {
		    		int index = kjoretoyListTable.convertRowIndexToModel(viewIndex);
		    		KjoretoyListV kjoretoyListV = model.get(index);
		    		slettKjoretoy(kjoretoyListV);
		    	}
			}
		};
    }
    private void slettKjoretoy(KjoretoyListV kjoretoy){
    	if (kjoretoy.getSlettet()!=null&&kjoretoy.getSlettet().longValue()==1)
    		return;
    	int confirm = JOptionPane.showConfirmDialog(this, getBundle().getString("message.deletekjoretoy")+kjoretoy.getEksterntNavn()+"\n"+getBundle().getString("message.deletekjoretoy2"), getBundle().getString("message.deletekjoretoy.title"), JOptionPane.YES_NO_OPTION);
    	if (confirm==JOptionPane.YES_OPTION){
    		MipssKjoretoy bean = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
    		Kjoretoy deleteKjoretoy = bean.deleteKjoretoy(kjoretoy, controller.getLoggedOnUser());
    		kjoretoy.setSlettet(deleteKjoretoy.getSlettetFlagg());
    		kjoretoy.setEndretDato(deleteKjoretoy.getOwnedMipssEntity().getEndretDato());
    		kjoretoy.setEndretAv(deleteKjoretoy.getOwnedMipssEntity().getEndretAv());
    	}
    }
    private void aapneKjoretoy(int viewIndex) {
    	if(viewIndex >= 0 && viewIndex < kjoretoyListTable.getRowCount()) {
    		int index = kjoretoyListTable.convertRowIndexToModel(viewIndex);
    		KjoretoyListV kjoretoyListV = model.get(index);
    		controller.setSelectedItem(kjoretoyListV.getId(), true, index);
    	}
    }
    
	@Override
	public void enableUpdating(boolean canUpdate) {
		// ignorert
	}

	@Override
	protected void initComponentsPresentation() {
		// ignorert
	}

	@Override
	protected boolean isSaved() {
		// ignorert
		return false;
	}

	@Override
	protected void loadFromEntity() {
		// ignorert
	}
	
    /** {@inheritDoc} */
    public void unloadEntity() {
    	//ignorert
    }

	@Override
	public void saveChanges() {
		// ignorert
	}

	@Override
	public boolean saveChangesAndContinue() {
		// ignorert
		return false;
	}

	@Override
	protected Dimension getDialogSize() {
		return new Dimension(300, 100);
	}
}
