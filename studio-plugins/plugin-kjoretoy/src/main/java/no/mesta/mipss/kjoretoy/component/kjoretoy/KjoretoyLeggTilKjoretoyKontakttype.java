package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyKontakttype;
import no.mesta.mipss.persistence.kontrakt.Kjoretoykontakttype;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;


public class KjoretoyLeggTilKjoretoyKontakttype extends AbstractRelasjonPanel<KjoretoyKontakttype> implements PropertyChangeListener {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlKontakttype = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JComboBox cbxKontaktType = new JComboBox();
    private JLabel lblKontaktType = new JLabel();
    private JPanel pnlKontaktinformasjon = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblKontaktNavn = new JLabel();
    private JTextField txtKontaktNavn = new JTextField();
    private JLabel lblTelefon1 = new JLabel();
    private JFormattedTextField txtTelefon1 = new JFormattedTextField();
    private JLabel lblTelefon2 = new JLabel();
    private JLabel lblEpost = new JLabel();
    private JFormattedTextField txtTelefon2 = new JFormattedTextField();
    private JTextField txtEpost = new JTextField();
    private JLabel lblBeskrivelse = new JLabel();
    private JTextField txtBeskrivelse = new JTextField();
    
	private BindingGroup bindingGroup;
	private KjoretoyKontakttype kjoretoyKontaktype;
	private List<Kjoretoykontakttype> kontakttyper;
	private Kjoretoy kjoretoy;
	
	private KjoretoyKontakttype originalKjoretoyKontaktype;
	private boolean endreKontakt;

	public KjoretoyLeggTilKjoretoyKontakttype(Kjoretoy kjoretoy, List<Kjoretoykontakttype> kontakttyper) {
		this.kjoretoy = kjoretoy;
		this.kontakttyper = kontakttyper;
		
		kjoretoyKontaktype = new KjoretoyKontakttype();
		kjoretoyKontaktype.addPropertyChangeListener(this);
		
		kjoretoyKontaktype.setKjoretoy(kjoretoy);
		kjoretoyKontaktype.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		
		endreKontakt = false;
		
		init();
	}
	
	public KjoretoyLeggTilKjoretoyKontakttype(Kjoretoy kjoretoy, KjoretoyKontakttype kjoretoyKontaktype, List<Kjoretoykontakttype> kontakttyper) {
		this.kjoretoy = kjoretoy;
		this.kontakttyper = kontakttyper;
		
		this.kjoretoyKontaktype = new KjoretoyKontakttype();
		this.kjoretoyKontaktype.setKjoretoy(kjoretoyKontaktype.getKjoretoy());
		this.kjoretoyKontaktype.setKontakttype(kjoretoyKontaktype.getKontakttype());
		this.kjoretoyKontaktype.mergeAll(kjoretoyKontaktype);
		
		this.kjoretoyKontaktype.addPropertyChangeListener(this);
		originalKjoretoyKontaktype = kjoretoyKontaktype;
		
		endreKontakt = true;
		
		init();
	}
	
	private void init() {
		bindingGroup = new BindingGroup();
		
		//Fjerner brukte typer fra cbx for typer
		for(Iterator<Kjoretoykontakttype> it = kontakttyper.iterator(); it.hasNext();) {
			Kjoretoykontakttype kontakttype = it.next();
			if(kontakttype.getKunEnKontaktFlagg()) {
				for(KjoretoyKontakttype kjoretoyKontaktype : kjoretoy.getKjoretoyKontaktypeList()) {
					if(kjoretoyKontaktype.getKontakttypeNavn().equals(kontakttype.getNavn()) && !kjoretoyKontaktype.equals(this.kjoretoyKontaktype)) {
						it.remove();
					}
				}
			}
		}
		
		initComboBox();
		initGui();
		initTextFields();
		
		bindingGroup.bind();
	}
	
	@SuppressWarnings("unchecked")
	private void initTextFields() {
		LongStringConverter longStringConverter = new LongStringConverter();
		
		Binding tlf1Binding = BindingHelper.createbinding(kjoretoyKontaktype, "tlfnummer1", txtTelefon1, "text", BindingHelper.READ_WRITE);
		Binding tlf2Binding = BindingHelper.createbinding(kjoretoyKontaktype, "tlfnummer2", txtTelefon2, "text", BindingHelper.READ_WRITE);
		
		tlf1Binding.setConverter(longStringConverter);
		tlf2Binding.setConverter(longStringConverter);
		
		bindingGroup.addBinding(tlf1Binding);
		bindingGroup.addBinding(tlf2Binding);
		
		txtTelefon1.setDocument(new DocumentFilter(8, "0123456789", false));
		txtTelefon2.setDocument(new DocumentFilter(8, "0123456789", false));
		
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyKontaktype, "kontaktnavn", txtKontaktNavn, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyKontaktype, "epostAdresse", txtEpost, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyKontaktype, "beskrivelse", txtBeskrivelse, "text", BindingHelper.READ_WRITE));
	}
	
	private void initComboBox() {
        MipssComboBoxModel<Kjoretoykontakttype> cbxModelKontakttype = new MipssComboBoxModel<Kjoretoykontakttype>(kontakttyper, false);
        cbxKontaktType.setModel(cbxModelKontakttype);
        cbxKontaktType.setRenderer(new MipssListCellRenderer<Kjoretoykontakttype>());
        
        if(endreKontakt) {
        	cbxModelKontakttype.setSelectedItem(kjoretoyKontaktype.getKontakttype());
        	cbxKontaktType.setEnabled(false);
        	cbxKontaktType.setFocusable(false);
        } else {
	        if(kontakttyper.size() > 0) {
	        	kjoretoyKontaktype.setKontakttype(kontakttyper.get(0));
	        }
	        
	        bindingGroup.addBinding(BindingHelper.createbinding(kjoretoyKontaktype, "kontakttype", cbxKontaktType, "selectedItem", BindingHelper.READ_WRITE));
        }
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlKontakttype.setLayout(gridBagLayout2);
        pnlKontakttype.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getBundle().getString("border.type")));
        cbxKontaktType.setPreferredSize(new Dimension(120, 21));
        lblKontaktType.setText(KjoretoyUtils.getBundle().getString("label.kontakttype"));
        pnlKontaktinformasjon.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getBundle().getString("border.kontaktinformasjon")));
        pnlKontaktinformasjon.setLayout(gridBagLayout3);
        lblKontaktNavn.setText(KjoretoyUtils.getBundle().getString("label.kontaktnavn"));
        txtKontaktNavn.setPreferredSize(new Dimension(150, 21));
        txtKontaktNavn.setSize(new Dimension(150, 21));
        lblTelefon1.setText(KjoretoyUtils.getBundle().getString("label.telefon1"));
        txtTelefon1.setPreferredSize(new Dimension(150, 21));
        lblTelefon2.setText(KjoretoyUtils.getBundle().getString("label.telefon2"));
        lblEpost.setText(KjoretoyUtils.getBundle().getString("label.epost"));
        txtTelefon2.setPreferredSize(new Dimension(150, 21));
        txtEpost.setPreferredSize(new Dimension(150, 21));
        lblBeskrivelse.setText(KjoretoyUtils.getBundle().getString("label.merknad"));
        txtBeskrivelse.setPreferredSize(new Dimension(150, 21));
        pnlKontakttype.add(cbxKontaktType, 
                    new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontakttype.add(lblKontaktType, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 0), 0, 0));
        this.add(pnlKontakttype, 
                 new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                        new Insets(11, 11, 5, 11), 0, 0));
        pnlKontaktinformasjon.add(lblKontaktNavn, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtKontaktNavn, 
                    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblTelefon1, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtTelefon1, 
                    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblTelefon2, 
                    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblEpost, 
                    new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtTelefon2, 
                    new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtEpost, 
                    new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblBeskrivelse, 
                    new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtBeskrivelse, 
                    new GridBagConstraints(1, 5, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        this.add(pnlKontaktinformasjon, 
                 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(0, 11, 11, 11), 0, 0));
	}
	
	private void updateComplete() {
		String navn = kjoretoyKontaktype.getKontaktnavn();
		String kontraktkontakttypeNavn = kjoretoyKontaktype.getKontakttypeNavn();
		setComplete(Boolean.valueOf(navn != null && navn.length() > 0 && kontraktkontakttypeNavn != null && kontraktkontakttypeNavn.length() > 0));
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(275, 325);
	}

	@Override
	public KjoretoyKontakttype getNyRelasjon() {
		if(endreKontakt) {
			originalKjoretoyKontaktype.merge(kjoretoyKontaktype);
			return originalKjoretoyKontaktype;
		} else {
			return kjoretoyKontaktype;
		}
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getBundle().getString("dialogTitle.leggTilKontakt");
	}

	@Override
	public boolean isLegal() {
		return true;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateComplete();
	}
}
