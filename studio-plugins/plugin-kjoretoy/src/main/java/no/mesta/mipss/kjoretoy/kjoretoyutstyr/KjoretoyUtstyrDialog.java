package no.mesta.mipss.kjoretoy.kjoretoyutstyr;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class KjoretoyUtstyrDialog extends JDialog {
	private JPanel pnlGruppeInfo = new JPanel();
	private JPanel pnlBeskrivelse = new JPanel();
	private JPanel pnlUtstyrsperioder = new JPanel();
	private JPanel pnlIOPerioder = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JLabel lblGruppe = new JLabel();
	private JComboBox cbxGruppe = new JComboBox();
	private JLabel lblSubGruppe = new JLabel();
	private JComboBox cbxSubGruppe = new JComboBox();
	private JLabel lblModell = new JLabel();
	private JComboBox cbxModell = new JComboBox();
	private JScrollPane scrlPaneBeskrivelse = new JScrollPane();
	private JTextArea txtAreaBeskrivelse = new JTextArea();
	private JScrollPane scrlPaneUtstyrsperioder = new JScrollPane();
	private JScrollPane scrlPaneIOPerioder = new JScrollPane();
	private JXTable tblUtstyrsperioder = new JXTable();
	private JXTable tblIOPerioder = new JXTable();
	private JButton btnAvbryt = new JButton();
	private JButton btnLagre = new JButton();
	private JButton btnNyUtstyrperiode = new JButton();
	private JButton btnEndreUtstyrperiode = new JButton();
	private JButton btnSlettUtstyrperiode = new JButton();
	private JButton btnNyIOperiode = new JButton();
	private JButton btnEndreIOperiode = new JButton();
	private JButton btnSlettIOperiode = new JButton();
	
	private KjoretoyUtstyrController kjoretoyUtstyrController;
	
	public KjoretoyUtstyrDialog(Window parentWindow, KjoretoyUtstyrController kjoretoyUtstyrController) {
		super(parentWindow, KjoretoyUtils.getPropertyString("utstyrsdialog.title"), ModalityType.APPLICATION_MODAL);
		this.kjoretoyUtstyrController = kjoretoyUtstyrController;

		init();
	}
	
	public KjoretoyUtstyr showDialog() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
		return null;
	}
	
	private void init() {
		initLabels();
		initTextFields();
		initTables();
		initComboboxes();
		initButtons();
		initGui();
		
		kjoretoyUtstyrController.getBindingGroup().bind();
	}

	private void initTables() {
		tblUtstyrsperioder.setPreferredScrollableViewportSize(new Dimension(0, 0));
		tblIOPerioder.setPreferredScrollableViewportSize(new Dimension(0, 0));
		tblIOPerioder.setModel(kjoretoyUtstyrController.getIOPeriodeTableModel());
		tblIOPerioder.setColumnModel(kjoretoyUtstyrController.getIOPeriodeTableColumnModel());
		tblIOPerioder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					kjoretoyUtstyrController.getEndreIOPeriodeAction(KjoretoyUtstyrDialog.this, tblIOPerioder).actionPerformed(null);
				}
			}
		});		
	}
	
	private void initLabels() {
		lblGruppe.setText(KjoretoyUtils.getPropertyString("utstyrsdialog.lblGruppe"));
		lblSubGruppe.setText(KjoretoyUtils.getPropertyString("utstyrsdialog.lblSubgruppe"));
		lblModell.setText(KjoretoyUtils.getPropertyString("utstyrsdialog.lblModell"));
		
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "gruppeForeground", lblGruppe, "foreground"));
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "subgruppeForeground", lblSubGruppe, "foreground"));
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "modellForeground", lblModell, "foreground"));
	}
	
	private void initComboboxes() {
		cbxGruppe.setModel(kjoretoyUtstyrController.getGruppeCbxModel());
		cbxSubGruppe.setModel(kjoretoyUtstyrController.getSubgruppeCbxModel());
		cbxModell.setModel(kjoretoyUtstyrController.getModellCbxModel());
		
		cbxGruppe.setRenderer(kjoretoyUtstyrController.getGruppeRenderer());
		cbxSubGruppe.setRenderer(kjoretoyUtstyrController.getSubgruppeRenderer());
		cbxModell.setRenderer(kjoretoyUtstyrController.getModellRenderer());
		
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "gruppeEditable", cbxGruppe, "enabled"));
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "subgruppeEditable", cbxSubGruppe, "enabled"));
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "modellEditable", cbxModell, "enabled"));
	}
	
	private void initButtons() {
		btnAvbryt.setAction(kjoretoyUtstyrController.getAvbrytAction(this));
		btnLagre.setAction(kjoretoyUtstyrController.getLagreAction(this));
		
		btnNyIOperiode.setAction(kjoretoyUtstyrController.getNyIOPeriodeAction(this));
		btnEndreIOperiode.setAction(kjoretoyUtstyrController.getEndreIOPeriodeAction(this, tblIOPerioder));
		btnSlettIOperiode.setAction(kjoretoyUtstyrController.getSlettIOPeriodeAction(tblIOPerioder));
	}
	
	private void initTextFields() {
		txtAreaBeskrivelse.setWrapStyleWord(true);
		txtAreaBeskrivelse.setLineWrap(true);
		
		kjoretoyUtstyrController.getBindingGroup().addBinding(BindingHelper.createbinding(kjoretoyUtstyrController, "${kjoretoyUtstyr.fritekst}", txtAreaBeskrivelse, "text"));
	}
	
	private void initGui() {
		Dimension dim = new Dimension(150, 19);
		
		cbxGruppe.setPreferredSize(dim);
		cbxSubGruppe.setPreferredSize(dim);
		cbxModell.setPreferredSize(dim);
		
		pnlGruppeInfo.setLayout(new GridBagLayout());		
		pnlGruppeInfo.add(lblGruppe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlGruppeInfo.add(cbxGruppe, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		pnlGruppeInfo.add(lblSubGruppe, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlGruppeInfo.add(cbxSubGruppe, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		pnlGruppeInfo.add(lblModell, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlGruppeInfo.add(cbxModell, new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		
		scrlPaneBeskrivelse.setViewportView(txtAreaBeskrivelse);
		scrlPaneBeskrivelse.setPreferredSize(new Dimension(100, 100));
		scrlPaneBeskrivelse.setMinimumSize(new Dimension(100, 100));
		pnlBeskrivelse.setLayout(new GridBagLayout());		
		pnlBeskrivelse.add(scrlPaneBeskrivelse, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));

		scrlPaneUtstyrsperioder.setViewportView(tblUtstyrsperioder);
		scrlPaneUtstyrsperioder.setPreferredSize(new Dimension(100, 100));
		pnlUtstyrsperioder.setLayout(new GridBagLayout());		
		pnlUtstyrsperioder.add(scrlPaneUtstyrsperioder, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlUtstyrsperioder.add(btnNyUtstyrperiode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlUtstyrsperioder.add(btnEndreUtstyrperiode, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlUtstyrsperioder.add(btnSlettUtstyrperiode, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));

		scrlPaneIOPerioder.setViewportView(tblIOPerioder);
		scrlPaneIOPerioder.setPreferredSize(new Dimension(100, 100));
		pnlIOPerioder.setLayout(new GridBagLayout());		
		pnlIOPerioder.add(scrlPaneIOPerioder, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlIOPerioder.add(btnNyIOperiode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlIOPerioder.add(btnEndreIOperiode, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlIOPerioder.add(btnSlettIOperiode, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));

		pnlButtons.setLayout(new GridBagLayout());		
		pnlButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlGruppeInfo.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("utstyrsdialog.border.gruppe")));
		pnlBeskrivelse.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("utstyrsdialog.border.beskrivelse")));
		pnlUtstyrsperioder.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("utstyrsdialog.border.utstyrsperioder")));
		pnlIOPerioder.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("utstyrsdialog.border.ioperioder")));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlGruppeInfo, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 5, 11), 0, 0));
		this.add(pnlBeskrivelse, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 5, 11), 0, 0));
		this.add(pnlUtstyrsperioder, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 5, 11), 0, 0));
		this.add(pnlIOPerioder, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}
}
