package no.mesta.mipss.kjoretoy.control;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import no.mesta.mipss.instrumentering.wizard.WizardPanel;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.AbstractMipssEntityPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.plugin.StatusPanel;
import no.mesta.mipss.kjoretoy.plugin.TabBase;
import no.mesta.mipss.kjoretoy.search.SearchView;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kontrollerer søkekommandoer ved å vise GUI-elementer som brukes for å søke
 * og ved å utføre søk samt vise treff. Dessuten kontrollerer visningen
 * av enkeltkjøretøy i tre eller fire forskjellige faner.
 * 
 * Beholder tilstanden i brukerens valg: Beholder kjøretøyet som vises og den
 * fanen som ble vist sist.
 * 
 * @author jorge
 * @version
 */
public class SearchControl extends JPanel implements InstrumentationController, PropertyChangeListener {
	private static final long serialVersionUID = -5894711565143018779L;
	private enum navigation {FIRST, PREVIOUS, NEXT, LAST};
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private ResourceBundle bundle;
    private StatusPanel statusPanel;
    private TabBase hitListTab;
    private TabBase kjoretoyTab;
    private MipssKjoretoy bean = null;
    
    private JButton btnNyttKjoretoy;
    private JTextField navigationTextField;
    private JButton searchButton;
    private JTextField searchTextField;
    private JLabel searchTitle;
    private boolean changed;
    
    private MipssPlugin mipssPlugin;
    
    
    private List<AbstractKjoretoyPanel> abstractKjoretoyPanelList = new ArrayList<AbstractKjoretoyPanel>();
    
    // variabler som holder tilstanden
    private Kjoretoy currentKjoretoy = null;
    private TabBase lastUsedTab = null;
    private List<KjoretoyListV> currentHitList = null;
    private int currentIndex = -1;
    
    /**
     * Instansier med koblinger tilbake til statuspanellet og til søkefanen.
     * @param hitListTab peker til søkefanen hvor man viser trefflisten.
     * @param kjoretoyTab peker til hovedfanen for visning av ett kjøretøy.
     * @param statusPanel peker til statuspanellet for visning av ventestatus m.m.
     */
    public SearchControl(StatusPanel statusPanel, MipssPlugin mipssPlugin) {
        bean = KjoretoyUtils.getKjoretoyBean();
        bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
        this.statusPanel = statusPanel;
        this.mipssPlugin = mipssPlugin;
        changed = false;
        
        try {
            initComponents();
            initComponentsOwn();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, bundle.getString("search.ingenControl"));
        }
    }
    
    public void initTabBases(TabBase hitListTab, TabBase kjoretoyTab) {
    	this.hitListTab = hitListTab;
    	this.kjoretoyTab = kjoretoyTab;
    }
    
    
    private void initComponentsOwn() {
    	btnNyttKjoretoy.addActionListener(new ActionListener() {
    		@Override
			public void actionPerformed(ActionEvent e) {
				WizardPanel wizardPanel = new WizardPanel(mipssPlugin.getLoader().getApplicationFrame(), mipssPlugin, false, null, null, false, false);
				Kjoretoy kjoretoy = wizardPanel.startWizard();
				if(kjoretoy != null) {
					
					//Fix som løser problemet med persist/merge problematikk med kjøretøyeier
					Kjoretoyeier eier = kjoretoy.getKjoretoyeier();
					Kjoretoyeier eksisterendeEier = bean.getKjoretoyeierById(kjoretoy.getKjoretoyeier().getId());					
					if (eksisterendeEier != null) {
						kjoretoy.setKjoretoyeier(null);
						kjoretoy = bean.addKjoretoy(kjoretoy);
						kjoretoy.setKjoretoyeier(eier);
						kjoretoy = bean.updateKjoretoy(kjoretoy);
					} else{
						kjoretoy = bean.addKjoretoy(kjoretoy);
					}
					fetchOneHit(kjoretoy.getId());
				}
			}
    	});
        searchButton.addActionListener(new SearchActionListener());
        searchTextField.addKeyListener(new SearchKeyListener());
        
        // Betjener klickingen på navigasjonsknappene.
        ActionListener navigationListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               JButton b = (JButton) e.getSource();
               if (navigation.NEXT.toString().equals(b.getName())) {
                   logger.debug("NEXT");
                   navigateTo(currentIndex + 1);
               }
               else if (navigation.PREVIOUS.toString().equals(b.getName())) {
                   logger.debug("PREVIOUS");
                   navigateTo(currentIndex - 1);
               }
               else if (navigation.FIRST.toString().equals(b.getName())) {
                   logger.debug("FIRST");
                   navigateTo(0);
               }
               else {
                   logger.debug("LAST");
                   navigateTo(currentHitList.size()-1);
               }
            }
        };

       // bold på på posisjonsinfo
       KjoretoyUtils.setBold(navigationTextField);
       
	}
	
    /**
     * Kontrollerer navigasjonen i trefflisten.
     * @param index
     */
    private void navigateTo(int index) {
        if (index < 0) {
            index = 0;
        }
        else if (index > currentHitList.size()-1) {
            index = currentHitList.size()-1;
        }
        currentIndex = index;
        fetchOneHit(currentHitList.get(index).getId());
    }
    
    /**
     * Setter søketråden til å utføre et søk i bakgrunnen.
     * Starter visningen av ventingen i statuspanelet.
     * @param searchCriteria søkebegrepet å lete etter.
     */
    private void executeSearch(String searchCriteria) {
        SearchWorker worker = new SearchWorker(searchCriteria);
        
        worker.addPropertyChangeListener(new PropertyChangeListener(){
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("state".equals(evt.getPropertyName())) {
                            SearchWorker w = (SearchControl.SearchWorker) evt.getSource();
                            if (w.getState() == SwingWorker.StateValue.DONE) {
                                try {
                                    showHitList(w.get());
                                } catch (ExecutionException ie) {
                                     logger.debug("showHitList failed", ie);
                                } catch (InterruptedException ee) {
                                     logger.debug("showHitList failed", ee);
                                }
                            }
                        }
                    }
                });
                
        statusPanel.setStatusMessage(bundle.getString("search.Waiting"));
        worker.execute();
    }
    
    /**
     * Viser den leverte trefflisten på hovedpanelet.
     * Kallet fra PropertyChangeListener til SwingWorker tråden.
     * @param hits trefflisten hentet fra databasen.
     */
    private void showHitList(List<KjoretoyListV> hits) {
        // husk listen for senere navigering fra denne klassen.
        currentHitList = hits;
        
        // informer brukeren om ferdig henting
        statusPanel.clearStatusMessage();

        // vis listen
        SearchView view = PanelFactory.createSearchView(hits, this);
        hitListTab.removeAll();
        hitListTab.add(view);
        hitListTab.setLabel(bundle.getString("mainTab.Sokeresultat") + " (" + hits.size() + ")");
        hitListTab.showContent();
    }

    /**
     * Viser ett kjøretøy i hovedfane for kjøretøy.
     * Dette kallet betyr at et nytt item er blitt valgt i trefflisten, så vi
     * oppdaterer vår current kjøretøy.
     * @param itemId
     */
    public void fetchOneHit(Long itemId) {
        OneKjoretoyWorker worker = new OneKjoretoyWorker(itemId);
        
        worker.addPropertyChangeListener(new PropertyChangeListener(){
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("state".equals(evt.getPropertyName())) {
                            OneKjoretoyWorker w = (SearchControl.OneKjoretoyWorker) evt.getSource();
                            if (w.getState() == SwingWorker.StateValue.DONE) {
                                try {
                                    showOneHit(w.get());
                                } catch (ExecutionException ie) {
                                     logger.debug("showOneHit failed", ie);
                                } catch (InterruptedException ee) {
                                     logger.debug("showOneHit failed", ee);
                                }
                            }
                        }
                    }
                });
                
        statusPanel.setStatusMessage(bundle.getString("search.Fetching"));
        worker.execute();             
    }
    
    /**
     * Viser det leverte kjøretøyet i hovedpanelet for kjøretøyvisning.
     * Kalles fra PropertyChangeListener for henting av enkeltkjøretøy.
     * Slår navigeringsknappene på eller av avhengig av indexen i listen.
     * Oppdaterer navigeringsinfo.
     * @param kjoretoy som er nylig hentet fra databasen.
     */
    private void showOneHit(Kjoretoy kjoretoy) {
		boolean kanBytte = true;
    	boolean lagreForBytte = false;
    	
    	if(currentKjoretoy != null) {
    		if(changed) {
    			int svar = JOptionPane.showConfirmDialog(mipssPlugin.getLoader().getApplicationFrame(), bundle.getString("message.lagreEndringer"), bundle.getString("message.lagreEndringer.title"), JOptionPane.YES_NO_CANCEL_OPTION);
    			kanBytte = svar == JOptionPane.YES_OPTION || svar == JOptionPane.NO_OPTION;
    			lagreForBytte = svar == JOptionPane.YES_OPTION;
    		} else {
    			kanBytte = true;
    		}
    	}
    	
    	if(lagreForBytte) {
    		currentKjoretoy = KjoretoyUtils.getKjoretoyBean().updateKjoretoy(currentKjoretoy);
    		if(kjoretoy != null && currentKjoretoy.getId().equals(kjoretoy.getId())) {
    			kjoretoy = bean.getWholeKjoretoy(currentKjoretoy.getId());;
    		}
    	}
    	
        // stopp progress bar og informer brukeren om endt henting
        statusPanel.clearStatusMessage();
    	
    	if(kanBytte) {
    		changed = false;
    	} else {
    		return;
    	}
    	
        // husk kjøretøyet
        currentKjoretoy = kjoretoy;
        
    	refreshKjoretoyInPanels();
        
        // vis hovedtab'en for kjøretøy ved starten og nar man velger i trefflisten
        if (lastUsedTab == null || lastUsedTab == hitListTab) {
            handleSelectedPanel(kjoretoyTab);
        }
        // ellers bruk den nåværende tab
        else {
            handleSelectedPanel(lastUsedTab);
        }
        
        if(currentHitList != null) {
	        updateNavigationInfo(kjoretoy);
        }
    }
    
    private void updateNavigationInfo(Kjoretoy kjoretoy) {
    	StringBuffer b = new StringBuffer(KjoretoyUtils.getBundle().getString("kjoretoy.kjoretoy") + " ");
        b.append(kjoretoy.getRelevantNavn()).
        append(' ').
        append('(').
        append(currentIndex + 1).
        append(" av ").
        append(currentHitList.size()).
        append(')');
        navigationTextField.setText(b.toString());
    }
    
    /**
     * Henter nytt kjøretøy fra DB og viser den. Visningen kommer på hvovedpanelen hvis
     * useInfoPanel er true.
     * Callback metoden brukt når brukeren velger et kjøretøy i trefflisten.
     * @param itemId nøkkel til kjøretøyet som skal hentes fra DB.
     * @param useInfoPanel true for å omdirigere visningen til hovedpanelen.
     * @param index is the index of the selected item in the hit list.
     */
    public void setSelectedItem(Long itemId, boolean useInfoPanel, int index) {
        currentIndex = index;
        logger.debug("setSelectedItem kallt med: " + itemId);
        fetchOneHit(itemId);
    }

    /**
     * Viser nåværende kjøretøy i en ny Panel hvis den leverte panelen er 
     * forskjellig fra den nåværende. Hvis den leverte og de nåværende panelene
     * er like, blir panelen kun åpnet.
     * Henter aldri nytt kjøretøy.
     * @param selectedTab
     */
    public void handleSelectedPanel(TabBase selectedTab) {
        AbstractMipssEntityPanel viewer = selectedTab.getKjoretoyView();

        if (viewer != null && currentKjoretoy != null) {
            lastUsedTab = selectedTab;
            selectedTab.showContent();
        }
        else if(viewer == null){
        	logger.debug("Finner ikke panel for visning");
        }
        // det er ikke farlig at selectedTab er null, hvis viewer eksisterer
    }

    public Object openDialogWindow(Class baseClass, long id, 
                                   AbstractMipssEntityPanel dialogPanel, 
                                   AbstractMipssEntityPanel parent) {
        // TODO: evaluer om denne sentraliserte løsningen for dialogvinduer
        // er bedre enn at hver komponent åpner sine dialogvinduer for seg.
        return null;
    }

    /**
     * Betjener hendelser som stammer fra søkeknappen.
     * @author jorge
     */
    private class SearchActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            executeSearch(searchTextField.getText());
        }
    }
    
    /**
     * Betjener hendelser fra ENTER-tasten på søketekstfeltet.
     * @author jorge
     */
    private class SearchKeyListener implements KeyListener {
        public void keyTyped(KeyEvent e) {}
        public void keyPressed(KeyEvent e) {}

        public void keyReleased(KeyEvent e) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                executeSearch(searchTextField.getText());
            }
        }
    }

    /**
     * Henter trefflisten fra DB i egen tråd.
     * @author jorge
     */
    public class SearchWorker extends SwingWorker<List<KjoretoyListV>, Void> implements PropertyChangeListener {
        private String searchChriteria = null;
        
        public SearchWorker(String searchChriteria) {
            this.searchChriteria = searchChriteria;
        }
        
        protected List<KjoretoyListV> doInBackground() {
            List<KjoretoyListV> hits = null;
            try {
                hits = bean.getKjoretoyListVList(searchChriteria);
            }
            catch (Exception e) {
                KjoretoyUtils.showException(e, bundle.getString("search.ingenTreffliste"));
                this.done();
                hits = new ArrayList<KjoretoyListV>();
            }
            return hits;
        }

        public void propertyChange(PropertyChangeEvent evt) {
            if ("state".equals(evt.getPropertyName())) {
                SearchWorker w = (SearchControl.SearchWorker) evt.getSource();
                if (w.getState() == SwingWorker.StateValue.DONE) {
                    try {
                        showHitList(w.get());
                    } catch (ExecutionException ie) {
                         logger.debug("showHitList failed", ie);
                    } catch (InterruptedException ee) {
                         logger.debug("showHitList failed", ee);
                    }
                }
            }
        }
    }
    
    private class OneKjoretoyWorker extends SwingWorker<Kjoretoy, Void> {
        private Long id = null;
        public OneKjoretoyWorker(Long id) {
            this.id = id;
        }
        protected Kjoretoy doInBackground() {
            Kjoretoy k = null;
            try {
                k = bean.getWholeKjoretoy(id);
            }
            catch (Exception e) {
                KjoretoyUtils.showException(e, bundle.getString("search.ingenKjoretoyvisning"));
                k = new Kjoretoy();
            }
            return k;
        }
    }
    
    private void initComponents() {
    	btnNyttKjoretoy = new JButton();
        searchTitle = new JLabel();
        searchButton = new JButton();
        searchTextField = new JTextField();
        navigationTextField = new JTextField();

        btnNyttKjoretoy.setText(bundle.getString("search.nyttKjoretoy"));
        searchTitle.setText(bundle.getString("search.KjoretoyTitle"));
        searchButton.setText(bundle.getString("search.sesarchButton"));

        searchTextField.setName("searchTextField");
        navigationTextField.setEditable(false);
        navigationTextField.setAutoscrolls(false);
        navigationTextField.setBorder(null);
        navigationTextField.setName("navigationTextField");
        navigationTextField.setOpaque(false);
        searchTextField.setMinimumSize(new Dimension(120, 21));
        searchTextField.setPreferredSize(new Dimension(120, 21));

        this.setLayout(new GridBagLayout());
        this.add(btnNyttKjoretoy, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 11), 0, 0));
        this.add(searchTitle, 			new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
        this.add(searchTextField, 		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 11), 0, 0));
        this.add(searchButton, 			new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
        this.add(navigationTextField,	new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
    }

	@Override
	public void registerKjoretoyPanel(AbstractKjoretoyPanel abstractKjoretoyPanel) {
		abstractKjoretoyPanelList.add(abstractKjoretoyPanel);
		abstractKjoretoyPanel.addPropertyChangeListener("mipssEntity", this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
//		System.out.println("oppdatert kjøretøy: " + evt.getSource().getClass().getName());
//		if(evt.getNewValue() != currentKjoretoy) {
//			System.out.println(" nytt kjøretøy");
//		}
	}

	@Override
	public Action getAvbrytKjoretoyAction() {
		return new AbstractAction(KjoretoyUtils.getPropertyString("button.avbrytt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Kjoretoy kjoretoy = bean.getWholeKjoretoy(currentKjoretoy.getId());
				showOneHit(kjoretoy);
//                if(saveChangesAndContinue()) {
//                	unloadEntity();
//                	setMipssEntity(bean.getWholeKjoretoy(currentKjoretoy.getId()));
//                	loadFromEntity();
//                }
			}
		};
	}

	@Override
	public Action getLagreKjoretoyAction() {
		return new AbstractAction(KjoretoyUtils.getPropertyString("button.lagre")) {
			@Override
			public void actionPerformed(ActionEvent e) {
		    	try {
		    		String endretAv = mipssPlugin.getLoader().getLoggedOnUserSign();
		    		currentKjoretoy.getOwnedMipssEntity().setEndretAv(endretAv);
		    		currentKjoretoy.getOwnedMipssEntity().setEndretDato(Clock.now());
		    		
		    		KjoretoyUtils.getKjoretoyBean().updateKjoretoy(currentKjoretoy);
		    		Kjoretoy kjoretoy = bean.getWholeKjoretoy(currentKjoretoy.getId());
		    		changed = false;
		    		showOneHit(kjoretoy);
//		    		enableSaveButton(false);
		    	}
		    	catch (Throwable ex) {
					KjoretoyUtils.showException(ex, bundle.getString("gen.ikkeLagret"));
				}
			}
		};
	}
	
	private void refreshKjoretoyInPanels() {
		for(AbstractKjoretoyPanel abstractKjoretoyPanel : abstractKjoretoyPanelList) {
			abstractKjoretoyPanel.setMipssEntity(currentKjoretoy);
		}
	}

	@Override
	public void kjoretoyChanged() {
		this.changed = true;
	}

	@Override
	public String getLoggedOnUser() {
		return mipssPlugin.getLoader().getLoggedOnUserSign();
	}
}
