package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyKontakttype;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EpostadresseTableHelper;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

/**
 * Panel som viser kontaktene til et kjøretøy i en tabell.
 * @author jorge
 */
public class KjoretoyKontakttypeViewPanel extends AbstractKjoretoyPanel {
	private static final long serialVersionUID = 1052395600745898234L;
	private MipssKjoretoy bean = null;
    private JPanel pnlMain;
    private JScrollPane scrlpaneKontakter;
    private JXTable kjoretoyKontakttypeTable;
    private JLabel kontakterTitle;
    private JButton leggTilKontaktButton;
    private JButton endreKontaktButton;
    private JButton slettKontaktButton;
    private Container parentContainer;
    private CompositePanel parent;
	
    public KjoretoyKontakttypeViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, MipssKjoretoy bean, ResourceBundle bundle, Container parentContainer, CompositePanel parent) {
        super(kjoretoy, controller);
        this.bean = bean;
        this.parentContainer = parentContainer;
        this.parent = parent;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        
        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
        }
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    public void enableUpdating(boolean canUpdate) {
        kjoretoyKontakttypeTable.setEnabled(canUpdate);
        leggTilKontaktButton.setEnabled(canUpdate);
        endreKontaktButton.setEnabled(canUpdate);
        slettKontaktButton.setEnabled(canUpdate);
    }
    
    private void initFromBean() {
        List<KjoretoyKontakttype> list = mipssEntity.getKjoretoyKontaktypeList();
        List<EditableMippsTableModel.MipssTableColumn> attribs = new LinkedList<EditableMippsTableModel.MipssTableColumn>();
        attribs.add(new EditableMippsTableModel.MipssTableColumn(getBundle().getString("kontakttype.typeTitle"), "kontakttypeNavn", false));
        attribs.add(new EditableMippsTableModel.MipssTableColumn(getBundle().getString("kontakttype.navnTitle"), "kontaktnavn", false));
        attribs.add(new EditableMippsTableModel.MipssTableColumn(getBundle().getString("kontakttype.tlf1Title"), "tlfnummer1", false));
        attribs.add(new EditableMippsTableModel.MipssTableColumn(getBundle().getString("kontakttype.tlfnr2"), "tlfnummer2", false));
        attribs.add(new EditableMippsTableModel.MipssTableColumn(getBundle().getString("kontakttype.epostTitle"), "epost", false));
        attribs.add(new EditableMippsTableModel.MipssTableColumn(getBundle().getString("kontakttype.beskrivelseTitle"), "beskrivelse", false));
        
        EditableMippsTableModel <KjoretoyKontakttype> model;
        model = new EditableMippsTableModel <KjoretoyKontakttype>(KjoretoyKontakttype.class, list, attribs);
        kjoretoyKontakttypeTable.setModel(model);
        
        // definer egenskapene for denne tabellen
        kjoretoyKontakttypeTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        kjoretoyKontakttypeTable.setFillsViewportHeight(true);
        kjoretoyKontakttypeTable.setAutoCreateRowSorter(true);
        
        kjoretoyKontakttypeTable.setDefaultRenderer(Long.class, new DefaultTableCellRenderer());
        
        this.repaint();
    }
    
    protected void loadFromEntity() {
        initFromBean();

        kjoretoyKontakttypeTable.getTableHeader().setReorderingAllowed(true);
    }
    private void initListeners () {
        leggTilKontaktButton.addActionListener(new ActionListener() {
                    @SuppressWarnings("unchecked")
					public void actionPerformed(ActionEvent e) {
                    	LeggTilRelasjonDialog<KjoretoyKontakttype> leggTilRelasjonDialog;
                        if(parentContainer instanceof JFrame) {
                        	leggTilRelasjonDialog = new LeggTilRelasjonDialog<KjoretoyKontakttype>((JFrame)parentContainer, new KjoretoyLeggTilKjoretoyKontakttype(mipssEntity, bean.getKontakttypeList()));
                        } else {
                        	leggTilRelasjonDialog = new LeggTilRelasjonDialog<KjoretoyKontakttype>((JDialog)parentContainer, new KjoretoyLeggTilKjoretoyKontakttype(mipssEntity, bean.getKontakttypeList()));
                        }
                        
                        KjoretoyKontakttype kjoretoyKontaktype = leggTilRelasjonDialog.showDialog();
                        
                        if(kjoretoyKontaktype != null) {
                        	mipssEntity.getKjoretoyKontaktypeList().add(kjoretoyKontaktype);
                        	((EditableMippsTableModel<KjoretoyKontakttype>)kjoretoyKontakttypeTable.getModel()).fireTableDataChanged();
                        	if(parent != null) {
                        		parent.enableSaveButton(true);
                        	}
                        }
                    }
                });
        endreKontaktButton.addActionListener(new ActionListener() {
            @SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
            	int selectedIndex = kjoretoyKontakttypeTable.getSelectedRow();
            	selectedIndex = kjoretoyKontakttypeTable.convertRowIndexToModel(selectedIndex);
            	if(selectedIndex >= 0 && selectedIndex < kjoretoyKontakttypeTable.getRowCount()) {
            		EditableMippsTableModel<KjoretoyKontakttype> model = (EditableMippsTableModel<KjoretoyKontakttype>)kjoretoyKontakttypeTable.getModel();
            		KjoretoyKontakttype kjoretoyKontaktype = model.getEntities().get(selectedIndex);
	            	LeggTilRelasjonDialog<KjoretoyKontakttype> leggTilRelasjonDialog;
	                if(parentContainer instanceof JFrame) {
	                	leggTilRelasjonDialog = new LeggTilRelasjonDialog<KjoretoyKontakttype>((JFrame)parentContainer, new KjoretoyLeggTilKjoretoyKontakttype(mipssEntity, kjoretoyKontaktype, bean.getKontakttypeList()));
	                } else {
	                	leggTilRelasjonDialog = new LeggTilRelasjonDialog<KjoretoyKontakttype>((JDialog)parentContainer, new KjoretoyLeggTilKjoretoyKontakttype(mipssEntity, kjoretoyKontaktype, bean.getKontakttypeList()));
	                }
	                
	                kjoretoyKontaktype = leggTilRelasjonDialog.showDialog();
	                	                
	                model.fireTableDataChanged();
                	if(parent != null && kjoretoyKontaktype != null) {
                		parent.enableSaveButton(true);
                	}
            	}
            }
        });
        
        kjoretoyKontakttypeTable.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("unchecked")
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					int index = kjoretoyKontakttypeTable.rowAtPoint(e.getPoint());
					int realIndex = kjoretoyKontakttypeTable.convertRowIndexToModel(index);
					if(realIndex >= 0 && realIndex < kjoretoyKontakttypeTable.getRowCount()) {
						EditableMippsTableModel<KjoretoyKontakttype> model = (EditableMippsTableModel<KjoretoyKontakttype>)kjoretoyKontakttypeTable.getModel();
	            		KjoretoyKontakttype kjoretoyKontaktype = model.getEntities().get(realIndex);
		            	LeggTilRelasjonDialog<KjoretoyKontakttype> leggTilRelasjonDialog;
		                if(parentContainer instanceof JFrame) {
		                	leggTilRelasjonDialog = new LeggTilRelasjonDialog<KjoretoyKontakttype>((JFrame)parentContainer, new KjoretoyLeggTilKjoretoyKontakttype(mipssEntity, kjoretoyKontaktype, bean.getKontakttypeList()));
		                } else {
		                	leggTilRelasjonDialog = new LeggTilRelasjonDialog<KjoretoyKontakttype>((JDialog)parentContainer, new KjoretoyLeggTilKjoretoyKontakttype(mipssEntity, kjoretoyKontaktype, bean.getKontakttypeList()));
		                }
		                
		                kjoretoyKontaktype = leggTilRelasjonDialog.showDialog();
		                	                
		                model.fireTableDataChanged();
	                	if(parent != null && kjoretoyKontaktype != null) {
	                		parent.enableSaveButton(true);
	                	}
					}
				}
			}
        });
        slettKontaktButton.addActionListener(new ActionListener() {
            @SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
            	int[] selectedRows = kjoretoyKontakttypeTable.getSelectedRows();
            	if(selectedRows.length > 0) {
            		EditableMippsTableModel<KjoretoyKontakttype> model = (EditableMippsTableModel<KjoretoyKontakttype>)kjoretoyKontakttypeTable.getModel();
            		List<KjoretoyKontakttype> remList = new ArrayList<KjoretoyKontakttype>();
            		for(int i=0; i<selectedRows.length; i++) {
            			int index = kjoretoyKontakttypeTable.convertRowIndexToModel(selectedRows[i]);
            			if(index >= 0 && index < kjoretoyKontakttypeTable.getRowCount()) {            				
                    		KjoretoyKontakttype kjoretoyKontaktype = model.getEntities().get(index);
                    		remList.add(kjoretoyKontaktype);
            			}
            		}
            		
            		if(remList.size() > 0) {
            			model.getEntities().removeAll(remList);
            			model.fireTableDataChanged();
                    	if(parent != null) {
                    		parent.enableSaveButton(true);
                    	}
            		}
            	}
            }
        });
    }
    

    protected void initComponentsPresentation() {
        initComponents();
        KjoretoyUtils.setBold(kontakterTitle);
        initListeners();
    }

    private void initComponents() {

        pnlMain = new JPanel();
        kontakterTitle = new JLabel();
        leggTilKontaktButton = new JButton();
        endreKontaktButton = new JButton();
        slettKontaktButton = new JButton();
        scrlpaneKontakter = new JScrollPane();
        kjoretoyKontakttypeTable = new JXTable();
        EpostadresseTableHelper.prepareTable(kjoretoyKontakttypeTable);

        setName("Form"); // NOI18N

        pnlMain.setBorder(BorderFactory.createEtchedBorder());
        pnlMain.setName("jPanel1"); // NOI18N

        kontakterTitle.setText(getBundle().getString("kontakttype.title")); // NOI18N
        kontakterTitle.setName("kontakterTitle"); // NOI18N

        leggTilKontaktButton.setText(getBundle().getString("kontakttype..LeggTilKontaktButton")); // NOI18N
        leggTilKontaktButton.setName("leggTilKontaktButton"); // NOI18N

        endreKontaktButton.setText(getBundle().getString("kontakttype..EndreKontaktButton")); // NOI18N
        slettKontaktButton.setText(getBundle().getString("kontakttype..SlettKontaktButton")); // NOI18N
        
        scrlpaneKontakter.setName("jScrollPane1"); // NOI18N

        kjoretoyKontakttypeTable.setName("kjoretoyKontakttypeTable"); // NOI18N
        kjoretoyKontakttypeTable.getTableHeader().setReorderingAllowed(false);
        scrlpaneKontakter.setViewportView(kjoretoyKontakttypeTable);

        leggTilKontaktButton.setMinimumSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        leggTilKontaktButton.setPreferredSize(new Dimension(KjoretoyCompleteViewPanel.ALIGNED_BUTTONS_WIDTH, 21));
        
        pnlMain.setBorder(BorderFactory.createTitledBorder(getBundle().getString("kontakttype.title")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(scrlpaneKontakter, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(leggTilKontaktButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(endreKontaktButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(slettKontaktButton, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));

        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
