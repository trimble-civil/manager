package no.mesta.mipss.kjoretoy.component.rodetilknytning;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.instrumentering.wizard.WizardLine;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.component.CompositePanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.kjoretoy.utils.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetilknytningstype;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.util.KjoretoyUtils;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RodetilknytningListPanel extends AbstractKjoretoyPanel implements CompositePanel {

    private JPanel pnlMain = new JPanel();
    private JScrollPane scrlPaneOrdreList = new JScrollPane();
    private JMipssBeanTable<KjoretoyRode> tblRodeTilknytning;
    private MipssBeanTableModel<KjoretoyRode> mdlRodeTilknytning;

    private JButton btnEndre = new JButton();
    private JButton btnNy = new JButton();
    private JButton btnSlett = new JButton();

    private WizardLine wizardLine;
    private Container parent;
    private MipssPlugin parentPlugin;
    private MaintenanceContract maintenanceContract;
    private RodetilknytningPanel parentPanel;
    private MipssKjoretoy mipssKjoretoy;
    private BindingGroup bindingGroup;
    private boolean loaded = false;

    public RodetilknytningListPanel(InstrumentationController controller, RodetilknytningPanel parentPanel, Kjoretoy kjoretoy, Container parent, MipssKjoretoy mipssKjoretoy, MaintenanceContract maintenanceContract, MipssPlugin parentPlugin) {
        super(kjoretoy, controller);

        this.wizardLine = (WizardLine) controller;
        this.parentPanel = parentPanel;
        this.parent = parent;
        this.mipssKjoretoy = mipssKjoretoy;
        this.maintenanceContract = maintenanceContract;
        this.parentPlugin = parentPlugin;

        bindingGroup = new BindingGroup();

        initTable();
        initButtons();
        initGui();

        bindingGroup.bind();
    }

    private void initTable() {
        mdlRodeTilknytning = new MipssBeanTableModel<KjoretoyRode>("kjoretoyRodeList", KjoretoyRode.class,"rode", "rodetilknytningstype", "kontrakt", "fraTidspunkt", "tilTidspunkt");

        BindingHelper.createbinding(this, "mipssEntity", mdlRodeTilknytning, "sourceObject", BindingHelper.READ).bind();

        MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(KjoretoyRode.class, mdlRodeTilknytning.getColumns());
        tblRodeTilknytning = new JMipssBeanTable<KjoretoyRode>(mdlRodeTilknytning, colModel);

        tblRodeTilknytning.setFillsViewportWidth(true);
        tblRodeTilknytning.setDoWidthHacks(false);

        tblRodeTilknytning.setDefaultRenderer(Rode.class, new RenderableMipssEntityTableCellRenderer<Rode>());
        tblRodeTilknytning.setDefaultRenderer(Rodetilknytningstype.class, new RenderableMipssEntityTableCellRenderer<Rodetilknytningstype>());
        tblRodeTilknytning.setDefaultRenderer(Driftkontrakt.class, new RenderableMipssEntityTableCellRenderer<Driftkontrakt>());
        tblRodeTilknytning.setDefaultRenderer(Date.class, new DateTimeRenderer());

        tblRodeTilknytning.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2) {
                    int selectedRow = tblRodeTilknytning.getSelectedRow();
                    if(selectedRow >= 0 && selectedRow < tblRodeTilknytning.getRowCount()) {
                        KjoretoyRode kjoretoyRode = mdlRodeTilknytning.get(tblRodeTilknytning.convertRowIndexToModel(selectedRow));
                        endreKjoretoyRode(kjoretoyRode);
                        mdlRodeTilknytning.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
                    }
                }
            }
        });
    }

    private void initButtons() {
        btnNy.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.leggTil"), IconResources.ADD_ITEM_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {

                KjoretoyRode kjoretoyRode = new KjoretoyRode();
                kjoretoyRode.setKjoretoy(mipssEntity);
                kjoretoyRode.setKontrakt(wizardLine.getSelectedKontrakt());
                kjoretoyRode.setFraTidspunkt(Clock.now());
                kjoretoyRode.setOpprettetAv(parentPlugin.getLoader().getLoggedOnUserSign());
                kjoretoyRode.setOpprettetDato(Clock.now());

                RodetilknytningDialog rodetilknytningDialog = null;

                if(parent instanceof JFrame) {
                    rodetilknytningDialog = new RodetilknytningDialog((JFrame)parent, kjoretoyRode, mipssEntity.getKjoretoyRodeList(), true, wizardLine, parentPlugin);
                }
                else {
                    rodetilknytningDialog = new RodetilknytningDialog((JFrame)parent, kjoretoyRode, mipssEntity.getKjoretoyRodeList(), true, wizardLine, parentPlugin);
                }

                KjoretoyRode kjoretoyRodeNy = rodetilknytningDialog.showDialog();
                if(rodetilknytningDialog.isKjoretoyRodeOk()) {
                    kjoretoyRode.setFraTidspunkt(kjoretoyRodeNy.getFraTidspunkt());
                    kjoretoyRode.setTilTidspunkt(kjoretoyRodeNy.getTilTidspunkt());
                    kjoretoyRode.setRode(kjoretoyRodeNy.getRode());
                    kjoretoyRode.merge(kjoretoyRodeNy);
                    List<KjoretoyRode> list = mipssEntity.getKjoretoyRodeList();
                    list.add(kjoretoyRode);
                    mdlRodeTilknytning.fireTableModelEvent(0, list.size(), TableModelEvent.INSERT);
                    enableSaveButton(true);
                }
            }
        });
        btnEndre.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.endre"), IconResources.DETALJER_SMALL_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = tblRodeTilknytning.getSelectedRow();
                if(selectedRow >= 0 && selectedRow < tblRodeTilknytning.getRowCount()) {
                    KjoretoyRode kjoretoyRode = mdlRodeTilknytning.get(tblRodeTilknytning.convertRowIndexToModel(selectedRow));
                    endreKjoretoyRode(kjoretoyRode);
                    mdlRodeTilknytning.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);

                }
            }
        });

        if (UserUtils.isAdmin(parentPlugin.getLoader().getLoggedOnUser(true)) || UserUtils.isBrukerInRolle(parentPlugin.getLoader().getLoggedOnUser(true), "Superbruker")){
            btnSlett.setAction(new AbstractAction(KjoretoyUtils.getPropertyString("button.slett"), IconResources.REMOVE_ITEM_ICON) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(RodetilknytningListPanel.this, KjoretoyUtils.getPropertyString("rode.messageDelete"), KjoretoyUtils.getPropertyString("rode.messageDeleteTitle"), JOptionPane.WARNING_MESSAGE);
                }
            });
            bindingGroup.addBinding(BindingHelper.createbinding(tblRodeTilknytning, "${noOfSelectedRows > 0}", btnSlett, "enabled"));
        }
        //ikke admin eller superbruker, vanlig bruker kan ikke slette rodetilknytninger.
        else{
            btnSlett.setText(KjoretoyUtils.getPropertyString("button.slett"));
            btnSlett.setIcon(IconResources.REMOVE_ITEM_ICON);
            btnSlett.setEnabled(false);
            btnSlett.setToolTipText(Resources.getResource("button.slett.tooltip", EpostUtils.getSupportEpostAdresse()));
        }

        bindingGroup.addBinding(BindingHelper.createbinding(tblRodeTilknytning, "${noOfSelectedRows == 1}", btnEndre, "enabled"));
    }

    private void endreKjoretoyRode(KjoretoyRode kjoretoyRode) {
        if(kjoretoyRode != null) {
            RodetilknytningDialog rodetilknytningDialog = null;
            if(parent instanceof JFrame) {
                rodetilknytningDialog = new RodetilknytningDialog((JFrame)parent, kjoretoyRode, mipssEntity.getKjoretoyRodeList(), false, wizardLine,parentPlugin);
            } else {
                rodetilknytningDialog = new RodetilknytningDialog((JDialog) parent, kjoretoyRode, mipssEntity.getKjoretoyRodeList(), false, wizardLine,parentPlugin);
            }

            KjoretoyRode kjoretoyRodeNy = rodetilknytningDialog.showDialog();
            if(rodetilknytningDialog.isKjoretoyRodeOk()) {
                kjoretoyRode.setKontrakt(kjoretoyRodeNy.getKontrakt());
                kjoretoyRode.setRodeId(kjoretoyRodeNy.getRodeId());
                kjoretoyRode.setRode(kjoretoyRodeNy.getRode());
                kjoretoyRode.setRodetilknytningstype(kjoretoyRodeNy.getRodetilknytningstype());
                kjoretoyRode.setRodetilknytningstypeId(kjoretoyRodeNy.getRodetilknytningstypeId());
                kjoretoyRode.setFraTidspunkt(kjoretoyRodeNy.getFraTidspunkt());
                kjoretoyRode.setTilTidspunkt(kjoretoyRodeNy.getTilTidspunkt());

                kjoretoyRode.setEndretAv(parentPlugin.getLoader().getLoggedOnUserSign());
                kjoretoyRode.setEndretDato(Clock.now());
                enableSaveButton(true);
            }
        }
    }


    private void initGui() {
        scrlPaneOrdreList.setViewportView(tblRodeTilknytning);

        pnlMain.setBorder(BorderFactory.createTitledBorder(KjoretoyUtils.getPropertyString("rodeTilknytning.border")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(scrlPaneOrdreList, new GridBagConstraints(0, 0, 1, 4, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(btnNy, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(btnEndre, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(btnSlett, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));

        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    @Override
    public void enableUpdating(boolean canUpdate) {
    }

    @Override
    protected void initComponentsPresentation() {
    }

    @Override
    public void setSessionBean(Object bean) {
        this.mipssKjoretoy = (MipssKjoretoy)bean;
    }

    @Override
    protected void loadFromEntity() {
        if(mipssEntity != null) {
            List<KjoretoyRode> ordreList = mipssEntity.getKjoretoyRodeList();
            mdlRodeTilknytning.setEntities(ordreList);
        } else {
            mdlRodeTilknytning.setEntities(new ArrayList<KjoretoyRode>());
        }
        enableSaveButton(false);
        loaded = true;
    }

    @Override
    public void enableSaveButton(boolean enable) {
        if(parentPanel != null) {
            parentPanel.enableSaveButton(enable);
        }

        if(enable) {
            kjoretoyChanged();
        }
    }
}
