package no.mesta.mipss.kjoretoy.tablebrowser;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;

import no.mesta.mipss.kjoretoy.common.SingleLineListSelectionListener;
import no.mesta.mipss.kjoretoy.component.installasjon.KjoretoyInstallasjonPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInstallasjonV;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.util.Constants;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.swingx.JXTable;

/**
 * presenterer en panel med en tabell av [{@code Installasjon}er øverst og en hoveddel
 * som viser de installasjonene man valger i tabellen.
 * @author jorge
 */
public class KjoretoyInstallasjonBrowser extends BrowserPanel<KjoretoyInstallasjonPanel, DfuInstallasjonV, Installasjon> {
	private static final long serialVersionUID = 3081949303283821398L;

	private ResourceBundle bundle = null;

    private KjoretoyInstallasjonPanel installasjonPanel; // = PanelFactory.createKjoretoyInstallasjonPanel(null);
    private EditableMippsTableModel<DfuInstallasjonV> model = null;
    
    public KjoretoyInstallasjonBrowser(Kjoretoy ownerEntity, List<DfuInstallasjonV> entities, int index, Container parent, MipssPlugin mipssPlugin) {
        super(parent);
        installasjonPanel = PanelFactory.createKjoretoyInstallasjonPanel(null, mipssPlugin);
        setUnitPanel(installasjonPanel);
        tableEntities = entities;
        bundle = java.util.ResourceBundle.getBundle(Constants.BUNDLE_NAME);
        
        setTitle(String.format(bundle.getString("dfu.installasjon.browserTitle"), ownerEntity.getTextForGUI()));
        
        initTable(getBrowseTable(), index);
        initButtons();
    }
    
    public void reloadTable(Installasjon selectEntity) {
        //TODO: må utvikles
    }
    
    @SuppressWarnings("unchecked")
	private void initTable(final JXTable table, int index) {
        // sett opp titler og modellen
        BrowserUtils.initKjoretoyInstallasjonTable(table);

        // koble entitene til modellen
        model = (EditableMippsTableModel<DfuInstallasjonV>)table.getModel();
        model.setEntities(tableEntities);
        
        // sett opp betjeneningen av radvalg i tabellen.
        table.getSelectionModel().addListSelectionListener(new SingleLineListSelectionListener() {
                    public void selectionServiceMethod(int index) {
                        int realIndex = table.convertRowIndexToModel(index);
                    	DfuInstallasjonV dfuInstallasjonV = tableEntities.get(realIndex);
                        
                        installasjonPanel.setMipssEntity(bean.getInstallasjon(dfuInstallasjonV.getId()));
                    }
                });
        
        // velg linjen som kalleren har valgt
        table.setRowSelectionInterval(index, index);
        
        table.setBackground(Color.WHITE);
    }
    
    private void initButtons() {
        // oppretter knapper vha. sine navn
        String[] labels = new String[] {
            bundle.getString("button.lagre"),
            bundle.getString("button.avbrytt")};
        
        getButtonPanel().initButtons(labels);
        
        // lever referanser til knappene
        installasjonPanel.setLagreButton(getButtonPanel().getButton(0));
        installasjonPanel.setAvbrytButton(getButtonPanel().getButton(1));
    }

	@Override
	protected Dimension getDialogSize() {
		return installasjonPanel.getDialogSize();
	}
}
