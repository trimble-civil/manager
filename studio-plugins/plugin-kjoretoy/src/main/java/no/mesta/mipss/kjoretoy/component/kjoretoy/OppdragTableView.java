package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;

import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

/**
 * tidlig versjon av panel for visning av instrumenteringsoppdrag. 
 * Ikke i bruk
 * @author jorge
 *
 */
public class OppdragTableView extends JPanel{
    private String name;
    private Object[][] values;
    private String[] titles = 
    { "Oppdragsgiver", "Kjøretøy", "Tidspunkt", "Prioritet", "Detaljer" };

    private JLabel oppdragTableTitle = new JLabel();
    private JTable oppdragTable = new JTable();
    private JLabel tableEmpty = new JLabel();
    
    public OppdragTableView(String name, List list) {
        this.name = name;
        this.values = toArray(list);
        insertButtons();
        try {
            jbInit();
        } catch (Exception e) { 
            e.printStackTrace();
        }
    }
    
    private Object[][] toArray(List list) {
        if (list == Collections.EMPTY_LIST) {
            return null;
        }
        // TODO: transformer dette
        return null;
    }
    
    /**
     * Setter inn en JButton i den siste cellen i hver rad. Knappen
     * bruker oppdragsnøkklen som var i cellen som ActionCommand.
     */
    private void insertButtons() {
        if (values == null || values.length == 0) {
            return;
        }
        for (Object[] row : values) {
            JButton button = new JButton("Åpne");
            button.setActionCommand((String) row[row.length-1]);
            row[row.length-1] = button;
        }
    }

    private void jbInit() throws Exception {
        this.setPreferredSize(new Dimension(400, 30));
        this.setMinimumSize(new Dimension(400, 300));
        oppdragTableTitle.setText(name);
        oppdragTableTitle.setPreferredSize(new Dimension(200, 0));
        oppdragTableTitle.setMinimumSize(new Dimension(200, 0));
        oppdragTable.setMinimumSize(new Dimension(400, 450));
        oppdragTable.setMaximumSize(new Dimension(400, 450));
        oppdragTable.setPreferredSize(new Dimension(400, 450));
        oppdragTable.setPreferredScrollableViewportSize(new Dimension(400, 
                                                                      450));oppdragTable.setSize(new Dimension(400, 450));
        oppdragTable.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.add(oppdragTableTitle, null);
        
        // Sett opp enten en tabell eller en forklaring på at ingen oppdarg funnet.
        /*
        if (values == null || values.length == 0) {
            tableEmpty.setText("Ingen oppdrag funnet");
            this.add(tableEmpty);
        }
        else {
        */
            oppdragTable = new JTable(new OppdragTableModel());
            this.add(oppdragTable, null);
        //}
    }

    /**
     * Styrer visningen av tabellen.
     * @author jorge
     * @version
     */
    private class OppdragTableModel extends AbstractTableModel {
        public int getColumnCount() {
            return titles.length;
        }

        public int getRowCount() {
            return (values == null)? 0 : values.length;
        }

        public String getColumnName(int col) {
            return titles[col];
        }

        public Object getValueAt(int row, int col) {
            return values[row][col];
        }

        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /**
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            return false;
        }
    }
}
