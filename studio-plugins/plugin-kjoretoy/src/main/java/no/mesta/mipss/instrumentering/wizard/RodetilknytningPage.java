package no.mesta.mipss.instrumentering.wizard;

import no.mesta.mipss.kjoretoy.component.rodetilknytning.RodetilknytningListPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;
import org.netbeans.spi.wizard.WizardPage;

import java.awt.*;

public class RodetilknytningPage extends WizardPage {
    private MipssPlugin mipssPlugin;
    private WizardLine wizardLine;
    private Container parent;
    private boolean enkelWizard;

    private RodetilknytningListPanel rodeTilknyningListPanel;

    public RodetilknytningPage(MipssPlugin mipssPlugin, WizardLine wizardLine, Container parent, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard) {
            super(stepId,stepDescription,autoListen);
            this.mipssPlugin = mipssPlugin;
            this.wizardLine = wizardLine;
            this.parent = parent;
            this.enkelWizard = enkelWizard;

             initGui();
    }

    private void initGui() {
        this.setLayout(new BorderLayout());
    }
    @Override
    protected void renderingPage() {
        if(rodeTilknyningListPanel == null) {
            rodeTilknyningListPanel = new RodetilknytningListPanel(wizardLine, null, wizardLine.getKjoretoy(), parent, KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), mipssPlugin);
            this.add(rodeTilknyningListPanel, BorderLayout.CENTER);
        }
    }
}
