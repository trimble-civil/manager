/**
 * 
 */
package no.mesta.mipss.instrumentering.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JPanel;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.instrumentering.utils.Utils;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyKontakttype;
import no.mesta.mipss.persistence.kontrakt.Kjoretoykontakttype;
import no.mesta.mipss.ui.combobox.MasterComboListener;
import no.mesta.mipss.ui.combobox.MipssComboBoxWrapper;
import no.mesta.mipss.ui.table.EditableMippsTableModel;
import no.mesta.mipss.ui.table.EditableMippsTableModel.MipssTableColumn;


/**
 * Panel for registrering av kontaktpersonerl fro et nytt kjøretøy.
 * Inkluderer et skjema for registrering av informasjonen om en kontakt og
 * en tabell med alle allerede registrerte kontakter.
 *  
 * @author jorge
 */
public class KjoretoyKontaktPanel extends JPanel {
	
	private Kjoretoy kjoretoy;
	private ResourceBundle bundle;
	private KjoretoyKontakttype kontaktType;
	private MipssComboBoxWrapper<Kjoretoykontakttype> typeComboBoxWrapper;
	private FieldToolkit binding;
	private EditableMippsTableModel<KjoretoyKontakttype> konkatterModell;

    // Variables declaration - do not modify
    private javax.swing.JTextField epost;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable kontakterTable;
    private javax.swing.JLabel kontakttypeTitle;
    private javax.swing.JButton leggTilButton;
    private javax.swing.JTextField navn;
    private javax.swing.JTextArea notat;
    private javax.swing.JTextField telefon1;
    private javax.swing.JTextField telefon2;
    private javax.swing.JComboBox typeComboBox;
    // End of variables declaration
	public KjoretoyKontaktPanel(Kjoretoy kjoretoy) {
		super();
		if(kjoretoy == null) {
			throw new IllegalStateException("Har fått NULL kjøretøy");
		}
		
		this.kjoretoy = kjoretoy;
		
		bundle = Utils.getBundle();
		kontaktType = new KjoretoyKontakttype();
		initComponents();
		Utils.setBold(kontakttypeTitle);
		
		loadTyper();
		initButton();
		initBindings();
		initKontakterTable();
	}
	
	private void loadTyper() {
		List<Kjoretoykontakttype> typer;
		try {
			typer = Utils.getKjoretoyBean().getKontakttypeList();
		}
		catch (Exception e) {
			Utils.showException("finner ikke kontakttyper");
			throw new IllegalStateException(e);
		}
		
		typeComboBoxWrapper = new MipssComboBoxWrapper<Kjoretoykontakttype>(typer, typeComboBox, "Velg kontakttype", "Ingen kontakttyper");
		typeComboBox.addActionListener(new MasterComboListener<Kjoretoykontakttype>(typeComboBoxWrapper) {
			@Override
			protected void serveNoselection() {
//				kontaktType.setKontakttype(null);
			}
			@Override
			protected void serveSelection(Kjoretoykontakttype m) {
//				kontaktType.setKontakttype(m);
			}
		});
	}
	
	private void initButton() {
		leggTilButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				try {
					insertKontakt(kontaktType.clone());
				}
				catch (CloneNotSupportedException cnse) {
					// KjoretoyKontaktype er cloneable, so denne exception ignoreres
				}
				*/
			}
		});
	}
	
	private void initBindings() {
		binding = FieldToolkit.createToolkit();
		
        binding.bindTextInputField(navn, kontaktType.getClass(), kontaktType, "kontaktnavn", null);
        binding.bindTextInputField(telefon1, kontaktType.getClass(), kontaktType, "tlfnummer1", null);
        binding.bindTextInputField(telefon2, kontaktType.getClass(), kontaktType, "tlfnummer2", null);
        binding.bindTextInputField(epost, kontaktType.getClass(), kontaktType, "epostAdresse", null);
        binding.bindTextInputArea(notat, kontaktType.getClass(), kontaktType, "beskrivelse");
        binding.bindReactiveButton(leggTilButton);
        binding.bind();

	}
	private void initKontakterTable() {
		List<MipssTableColumn> kolonner = new ArrayList<MipssTableColumn>();
		kolonner.add(new MipssTableColumn(bundle.getString("kontakttype.typeTitle"),  "kontaktnavn", false));
		kolonner.add(new MipssTableColumn(bundle.getString("kontakttype.navnTitle"),  "kontaktnavn", false));
		kolonner.add(new MipssTableColumn(bundle.getString("kontakttype.tlf1Title"),  "kontaktnavn", false));
		kolonner.add(new MipssTableColumn(bundle.getString("kontakttype.tlfnr2"),  "kontaktnavn", false));
		kolonner.add(new MipssTableColumn(bundle.getString("kontakttype.epostTitle"),  "kontaktnavn", false));

		konkatterModell = new EditableMippsTableModel<KjoretoyKontakttype>(KjoretoyKontakttype.class, null, kolonner);
		kontakterTable.setModel(konkatterModell);
	}
	
	/**
	 * Setter den leverte kontakten inn i kontakttabellen.
	 */
	private void insertKontakt(KjoretoyKontakttype ny) {
		kjoretoy.addKjoretoyKontaktype(ny);
		konkatterModell.getEntities().add(ny);
		kontakterTable.repaint();
	}
	
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        navn = new javax.swing.JTextField();
        telefon1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        telefon2 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        epost = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        notat = new javax.swing.JTextArea();
        typeComboBox = new javax.swing.JComboBox();
        leggTilButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        kontakterTable = new javax.swing.JTable();
        kontakttypeTitle = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setName("Form"); // NOI18N

        jLabel1.setText(bundle.getString("kontakttype.typeTitle")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel2.setText(bundle.getString("kontakttype.navnTitle")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        jLabel3.setText(bundle.getString("kontakttype.tlf1Title")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        navn.setName("navn"); // NOI18N

        telefon1.setName("telefon1"); // NOI18N

        jLabel4.setText(bundle.getString("kontakttype.tlfnr2")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        telefon2.setName("telefon2"); // NOI18N

        jLabel5.setText(bundle.getString("kontakttype.epostTitle")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        epost.setName("epost"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        notat.setColumns(20);
        notat.setRows(5);
        notat.setName("notat"); // NOI18N
        jScrollPane1.setViewportView(notat);

        typeComboBox.setName("typeComboBox"); // NOI18N

        leggTilButton.setText(bundle.getString("gen.leggTil")); // NOI18N
        leggTilButton.setName("leggTilButton"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        kontakterTable.setName("kontakterTable"); // NOI18N
        jScrollPane2.setViewportView(kontakterTable);

        kontakttypeTitle.setText(bundle.getString("kontakttype.title")); // NOI18N
        kontakttypeTitle.setName("kontakttypeTitle"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(navn, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(typeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(telefon1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(telefon2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(epost, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))
                    .addComponent(leggTilButton, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(kontakttypeTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(kontakttypeTitle)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(typeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(navn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(telefon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(telefon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(epost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(leggTilButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }



}
