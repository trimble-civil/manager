package no.mesta.mipss.kjoretoy.component.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.component.AbstractKjoretoyPanel;
import no.mesta.mipss.kjoretoy.plugin.InstrumentationController;
import no.mesta.mipss.mipsslogg.MipssLoggModule;
import no.mesta.mipss.mipsslogg.messages.OpenMipssloggMessage;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.KjoretoymodellNavn;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.combobox.MasterComboListener;
import no.mesta.mipss.ui.combobox.MipssComboBoxWrapper;
import no.mesta.mipss.util.KjoretoyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;

/**
 * Panel for visning av hovedinformasjonen om et kjøretøy.
 * @author jorge
 *
 */
public class KjoretoyViewPanel extends AbstractKjoretoyPanel {
	private static final long serialVersionUID = 4770831382125659756L;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
    private KjoretoyCompleteViewPanel parent;
    
    private List<Kjoretoytype> kjoretoytyper;
    private List<KjoretoymodellNavn> kjoretoymodeller;
    private MipssComboBoxWrapper<Kjoretoytype> typeComboWrapper;
    private MipssComboBoxWrapper<KjoretoymodellNavn> modellComboWrapper;
    
    private boolean initReady = false;
    private MipssKjoretoy bean;
    private BindingGroup binding;
    private ResourceBundle bundle;
    // Variables declaration - do not modify
    private JFormattedTextField guiTekstTextField;
    private JFormattedTextField eksterntNavnTextField;
    private JLabel lblMaskinnummer;
    private JLabel lblRegistreringsnummer;
    private JLabel lblGuiTekst;
    private JLabel lblEksterntNavn;
    private JLabel lblType;
    private JLabel lblModell;
    private JPanel pnlMain;
    private JLabel kjoretoyMainTitle;
    private JComboBox kjoretoytypeComboBox;
    private JFormattedTextField maskinnummerTextField;
    private JComboBox modellComboBox;
    private JFormattedTextField regnrTextField;
    private JButton btnVisRaadata;

    private MipssPlugin parentPlugin;

    public KjoretoyViewPanel(InstrumentationController controller, Kjoretoy kjoretoy, KjoretoyCompleteViewPanel parent, MipssKjoretoy bean, ResourceBundle bundle, MipssPlugin parentPlugin) {
        super(kjoretoy, controller);
        this.bean = bean;
        this.bundle = bundle;
        this.parentPlugin = parentPlugin;
        super.setBundle(bundle);
        
        try {
            initComponentsPresentation();
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            return;
        }
        this.parent = parent;
        
        if (kjoretoy != null) {
            try {
                loadFromEntity();
            } catch (Exception e) {
                KjoretoyUtils.showException(e, KjoretoyUtils.getPropertyString("kjoretoy.ingenVisning")); 
            }
        }
    }
    
    private void initBindings() {
		if(binding != null) {
			binding.unbind();
		}
    	
    	maskinnummerTextField.setDocument(new DocumentFilter(8));
		regnrTextField.setDocument(new DocumentFilter(10, null, true));
		eksterntNavnTextField.setDocument(new DocumentFilter(20, null, false));
		guiTekstTextField.setDocument(new DocumentFilter(30, null, false));
		
    	binding = new BindingGroup();
    	binding.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mipssEntity, BeanProperty.create("nummerEllerNavn"), eksterntNavnTextField, BeanProperty.create("text"), null));
    	binding.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mipssEntity, BeanProperty.create("regnrValid"), regnrTextField, BeanProperty.create("text"), null));
    	binding.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mipssEntity, BeanProperty.create("guitekst"), guiTekstTextField, BeanProperty.create("text"), null));
    	
		Binding bind = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, mipssEntity, BeanProperty.create("maskinnummerValid"), maskinnummerTextField, BeanProperty.create("text"), null);
		bind.setConverter(new LongStringConverter());
		binding.addBinding(bind);
    	
    	binding.bind();
    }
        
	/**
	 * {@inheritDoc}
	 */
	public void setSessionBean(Object bean) {
		this.bean = (MipssKjoretoy) bean;
	}
	
    public void enableUpdating(boolean canUpdate) {
        // combobox'er kan ikke skrives på
        kjoretoytypeComboBox.setEditable(false);
        modellComboBox.setEditable(false);
        
        // de andre endres 
        guiTekstTextField.setEditable(canUpdate);
        maskinnummerTextField.setEditable(canUpdate);
        regnrTextField.setEditable(canUpdate);
        
        kjoretoytypeComboBox.setEnabled(canUpdate);
        modellComboBox.setEnabled(canUpdate);
    }
    
    /**
     * Henter tre statiske lister fra DB kun én gang.
     * Noter at denne metoden ikke er trådsikker, men denne 
     * klassen er en del av GUI til en kliwentapp: så det er ingen fare
     * for at flere tråder går i ben på hverandre.
     */
    private void initLists() {
        if (kjoretoytyper == null) {
            kjoretoytyper = bean.getKjoretoytypeList();
            typeComboWrapper = new MipssComboBoxWrapper<Kjoretoytype>(kjoretoytyper, kjoretoytypeComboBox, bundle.getString("kjoretoytype.ikkeValgt"), bundle.getString("kjoretoytype.ingenTyper"));
            kjoretoytypeComboBox.addActionListener(new MasterComboListener<Kjoretoytype>(typeComboWrapper) {
				@Override
				protected void serveNoselection() {
					mipssEntity.setKjoretoytype(null);
					if(initReady) {
						parent.enableSaveButton(true);
					}
				}
				@Override
				protected void serveSelection(Kjoretoytype m) {
					mipssEntity.setKjoretoytype(m);
					if(initReady) {
						parent.enableSaveButton(true);
					}
				}
            	
            });
        }
        if (kjoretoymodeller == null) {
            kjoretoymodeller = bean.getKjoretoymodellNavnList();
            modellComboWrapper = new MipssComboBoxWrapper<KjoretoymodellNavn>(kjoretoymodeller, modellComboBox, bundle.getString("kjoretoymodell.ikkeValgt"), bundle.getString("kjoretoymodell.ingenModell"));
            modellComboBox.addActionListener(new MasterComboListener<KjoretoymodellNavn>(modellComboWrapper) {
				@Override
				protected void serveNoselection() {
					mipssEntity.setKjoretoymodell(null);
					if(initReady) {
						parent.enableSaveButton(true);
					}
//					aarComboWrapper.setList(null);
				}
				@Override
				protected void serveSelection(KjoretoymodellNavn m) {
					//aarComboWrapper.setList(m.getAarList());
					if(m.hasOneYearOnly()) {
						// ingen år å velge i mellom: sett den eneste modellen
						Kjoretoymodell modell = m.getAarList().get(0).getKjoretoymodell();
						mipssEntity.setKjoretoymodell(modell);
						if(initReady) {
							parent.enableSaveButton(true);
						}
					}
				}
            });
            
        }
    }
    
    protected void loadFromEntity() {
        // sett opp enkle felt
    	initBindings();
        
        // comboboxer skal ikke utføre sine actions under oppsetting
        initReady = false;
        // sett opp combobox'er
        try {
            initLists();
            typeComboWrapper.setMipssSelection(mipssEntity.getKjoretoytype());
            
            if(mipssEntity.getKjoretoymodell() != null) {
	            modellComboWrapper.setMipssSelection(mipssEntity.getKjoretoymodell().getKjoretoymodellNavn());
//	            aarComboWrapper.setList(mipssEntity.getKjoretoymodell().getKjoretoymodellNavn().getAarList());
//	            aarComboWrapper.setMipssSelection(mipssEntity.getKjoretoymodell().getKjoretoymodellAar());
            }
            
//            maskinComboWrapper.setMipssSelection(mipssEntity.getMaskinsenter());
        }
        catch (Exception e) {
            KjoretoyUtils.showException(e, getBundle().getString("kjoretoy.ingenVisning"));
            return;
        }
        initReady = true;
    }

    /**
     * Setter opp alle aksjoner som betjener brukeres valg og endringer.
     */
    private void initActions() {
        // reager på endringer i combobox'er
         ActionListener comboboxListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (initReady) {
                        if (parent == null) {
                            throw new IllegalStateException("MIPSS controller feil initialsert: " + this);
                        }
                    }
                }
            };
            
        kjoretoytypeComboBox.addActionListener(comboboxListener);
        modellComboBox.addActionListener(comboboxListener);
        
        guiTekstTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {
				parent.enableSaveButton(true);				
			}
    	});
        
        maskinnummerTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {
				parent.enableSaveButton(true);		
			}
    	});
        
        regnrTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {
				parent.enableSaveButton(true);			
			}
    	});
        
        eksterntNavnTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {
				parent.enableSaveButton(true);			
			}
    	});
        
        btnVisRaadata.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Date now = Clock.now();
				Calendar cal = Calendar.getInstance();
				cal.setTime(now);
				cal.add(Calendar.DATE, -1);
				Date from = cal.getTime();
				
				IMipssStudioLoader loader = parentPlugin.getLoader();
				MipssPlugin plugin = loader.getPlugin(MipssLoggModule.class);
				OpenMipssloggMessage m = new OpenMipssloggMessage(null, mipssEntity.getRegnr(), null, from, now);
				m.setTargetPlugin(plugin);
				loader.sendMessage(m);
			}
		});
    }
    
    protected void initComponentsPresentation() {
        initComponents();
        kjoretoyMainTitle.setText(getBundle().getString("kjoretoy.kjoretoy"));
        KjoretoyUtils.setBold(kjoretoyMainTitle);
        initActions();
    }

    private void initComponents() {
        pnlMain = new JPanel();
        lblRegistreringsnummer = new JLabel();
        lblGuiTekst = new JLabel();
        lblMaskinnummer = new JLabel();
        lblEksterntNavn = new JLabel();
        eksterntNavnTextField = new JFormattedTextField();
        kjoretoyMainTitle = new JLabel();
        regnrTextField = new JFormattedTextField();
        maskinnummerTextField = new JFormattedTextField();
        guiTekstTextField = new JFormattedTextField();
        kjoretoytypeComboBox = new JComboBox();
        modellComboBox = new JComboBox();
        btnVisRaadata = new JButton();
        lblType = new JLabel();
        lblModell = new JLabel();
       
        lblType.setText(bundle.getString("kjoretoy.typeLabel"));
        lblModell.setText(bundle.getString("kjoretoy.modellLabel"));
       
        lblRegistreringsnummer.setText(bundle.getString("kjoretoy.RegistreringsnummerLabel")); // NOI18N

        lblGuiTekst.setText(bundle.getString("kjoretoy.guiTekstLabel")); // NOI18N

        lblMaskinnummer.setText(bundle.getString("kjoretoy.MaskinnummerLabel")); // NOI18N

        lblEksterntNavn.setText(bundle.getString("kjoretoy.eksterntNavn")); // NOI18N

        kjoretoyMainTitle.setText(bundle.getString("kjoretoy.kjoretoy")); // NOI18N
        
        btnVisRaadata.setText(bundle.getString("kjoretoy.visRaadata"));
        btnVisRaadata.setIcon(IconResources.MIPSS_LOGG);

        kjoretoytypeComboBox.setPreferredSize(new Dimension(120, 21));
        kjoretoytypeComboBox.setMinimumSize(new Dimension(120, 21));
        modellComboBox.setPreferredSize(new Dimension(120, 21));
        modellComboBox.setMinimumSize(new Dimension(120, 21));
        eksterntNavnTextField.setPreferredSize(new Dimension(120, 21));
        eksterntNavnTextField.setMinimumSize(new Dimension(120, 21));
        regnrTextField.setPreferredSize(new Dimension(120, 21));
        regnrTextField.setMinimumSize(new Dimension(120, 21));
        maskinnummerTextField.setPreferredSize(new Dimension(120, 21));
        maskinnummerTextField.setMinimumSize(new Dimension(120, 21));
        guiTekstTextField.setPreferredSize(new Dimension(120, 21));
        guiTekstTextField.setMinimumSize(new Dimension(120, 21));
       
        pnlMain.setBorder(BorderFactory.createTitledBorder(bundle.getString("kjoretoy.kjoretoy")));
        pnlMain.setLayout(new GridBagLayout());
        pnlMain.add(lblType, 				new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(kjoretoytypeComboBox, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));

        pnlMain.add(lblModell, 				new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(modellComboBox, 		new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        
        pnlMain.add(lblEksterntNavn, 		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(eksterntNavnTextField, 	new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        
        pnlMain.add(lblRegistreringsnummer, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(regnrTextField, 		new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
       
        pnlMain.add(lblMaskinnummer, 		new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlMain.add(maskinnummerTextField, 	new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));      
        
        pnlMain.add(lblGuiTekst, 		new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        pnlMain.add(guiTekstTextField, 	new GridBagConstraints(5, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
        
        pnlMain.add(btnVisRaadata, 		new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
