package no.mesta.mipss.instrumentering.wizard;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyKontakttypeViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyLevKontraktListPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoy.KjoretoyeierViewPanel;
import no.mesta.mipss.kjoretoy.component.kjoretoyutstyr.PanelFactory;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.util.KjoretoyUtils;

import org.netbeans.spi.wizard.WizardPage;

public class RegistrerKontraktOgLeverandorPage extends WizardPage  {
	private KjoretoyKontakttypeViewPanel kjoretoyKontakPanel;
    private KjoretoyeierViewPanel kjoretoyeierPanel;
	private MipssPlugin mipssPlugin;
	private WizardLine wizardLine;
	private Container parent;
	private boolean enkelWizard;
	private Driftkontrakt initiellKontrakt;

	private KjoretoyLevKontraktListPanel kjoretoyLevKontraktListPanel;
	private boolean begrensetTilgangEndring;
	
	public RegistrerKontraktOgLeverandorPage(MipssPlugin mipssPlugin, WizardLine wizardLine, Container parent, String stepId, String stepDescription, boolean autoListen, boolean enkelWizard, Driftkontrakt initiellKontrakt, boolean begrensetTilgangEndring) {
		super(stepId, stepDescription, autoListen);
		this.mipssPlugin = mipssPlugin;
		this.wizardLine = wizardLine;
		this.parent = parent;
		this.enkelWizard = enkelWizard;
		this.initiellKontrakt = initiellKontrakt;
		this.begrensetTilgangEndring = begrensetTilgangEndring;
		
		initGui();
	}
	
	private void initGui() {
		this.setLayout(new GridLayout(2, 1));
	}
	
	@Override
	protected void renderingPage() {
		if(kjoretoyLevKontraktListPanel == null) {
			if(wizardLine.getKjoretoy() != null && initiellKontrakt != null) {
				boolean lagKontraktknytning = true;
				Kjoretoy kjoretoy = wizardLine.getKjoretoy();
				for(KontraktKjoretoy kontraktKjoretoy : kjoretoy.getKontraktKjoretoyList()) {
					if(kontraktKjoretoy.getKontraktId().equals(initiellKontrakt.getId())) {
						lagKontraktknytning = false;
					}
				}
				
				if(lagKontraktknytning) {
					KontraktKjoretoy kk = new KontraktKjoretoy();
					kk.setDriftkontrakt(initiellKontrakt);
					kk.setKjoretoy(wizardLine.getKjoretoy());
					kk.setAnsvarligFlagg(Long.valueOf(1));
					kjoretoy.getKontraktKjoretoyList().add(kk);
				}
			}
			
			kjoretoyeierPanel = PanelFactory.createKjoretoyeierViewPanel(wizardLine, wizardLine.getKjoretoy(), null, parent);
	    	kjoretoyKontakPanel = PanelFactory.createKjoretoyKontakttypeViewPanel(wizardLine, wizardLine.getKjoretoy(), parent, null);
			
	    	final JPanel pnlEierOgKontakter = new JPanel();
	    	pnlEierOgKontakter.setLayout(new GridBagLayout());
	    	pnlEierOgKontakter.add(kjoretoyeierPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
//	    	pnlEierOgKontakter.add(kjoretoyKontakPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	    	
			kjoretoyLevKontraktListPanel = new KjoretoyLevKontraktListPanel(wizardLine, null, wizardLine.getKjoretoy(), KjoretoyUtils.getKjoretoyBean(), KjoretoyUtils.getMaintenanceContractBean(), parent, begrensetTilgangEndring, initiellKontrakt);
			this.add(kjoretoyLevKontraktListPanel);
			this.add(pnlEierOgKontakter);
		}
	}
}
