package no.mesta.mipss.kontraktdashboard.gui.komponenter;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;

import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.resources.images.IconResources;


/**
 *  Klasse som viser ikoner på samme måte som FargetStatusInfo men som ikke har noen labler eller tekster.
 *  Det er av og til behov for å vise et hjelpikon i Dashboard på steder som ikke har verdi, da kan denne
 *  klassen benyttes for å vise samme type ikoner som ellers i dashboard, uten at man trenger å knytte verdier til denne.
 *  
 * @author olehel
 */
public class StandAloneInfoHelpIkon {
	private String hjelpeFil;

	private final Icon HELP_ICON = IconResources.HELP_16_ICON;
	private final Icon WARNING_ICON = IconResources.DASHBOARD_WARNING_ICON;
	
	private JButton btnHelp;
	
	StandAloneInfoHelpIkon thisElement;
	
	public StandAloneInfoHelpIkon(){
		this("default");
	}
	
	public StandAloneInfoHelpIkon(String hjelpefil){
		this.hjelpeFil = hjelpefil;
		thisElement = this;
		generateButton();
	}
	
	private JButton generateButton() {
		btnHelp = new JButton();
		btnHelp.setIcon(HELP_ICON);	
		btnHelp.setEnabled(true);
		btnHelp.setToolTipText(Resources.getResource("tooltip.helpButton"));
		btnHelp.setMargin(new Insets(0, 0, 0, 0));
		btnHelp.setBorder(null);
		btnHelp.setContentAreaFilled(false);
		
		btnHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new HjelpDialog(thisElement);
			}
		});
		return btnHelp;
	}
	
	public JButton getHelpButton() {
		return btnHelp;
	}
	
	public void setHelpButton(JButton button) {
		this.btnHelp = button;
	}
	
	public void setHelpButton(){
		btnHelp.setIcon(HELP_ICON);	
		btnHelp.setEnabled(true);
	}
	
	public void setWarningButton(){
		btnHelp.setIcon(WARNING_ICON);
		btnHelp.setEnabled(true);	
	}
	
	public void setButtonTooltipText(String tooltip){
		btnHelp.setToolTipText(tooltip); 
	}
	
	public String getHjelpeFil() {
		return hjelpeFil;
	}	
}

