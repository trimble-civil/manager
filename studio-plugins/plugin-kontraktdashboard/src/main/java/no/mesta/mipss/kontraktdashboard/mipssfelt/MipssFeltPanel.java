package no.mesta.mipss.kontraktdashboard.mipssfelt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JSeparator;

import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.dropdown.DropdownPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.AdvarselEllerOkPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.DashboardBoxTitlePanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;

import org.jdesktop.swingx.JXBusyLabel;

/**
 * Panel for å vise frem viktig informasjon fra MIPSS Felt.
 * @author larnym
 */
@SuppressWarnings("serial")
public class MipssFeltPanel extends DashboardBoxTitlePanel{
	
	private KontraktdashboardController controller;	
	private JPanel containerPanel;
	private JXBusyLabel wait;
	private JDashboardPanel pnlMain;
	private AdvarselEllerOkPanel pnlStatusAdvarsel;
	private DropdownPanel pnlOk;
	private JPanel pnlAdvarsler;
	private MipssFeltController mipssFeltController;	
	private MipssFeltPanel parentPanel;
	
	public MipssFeltPanel(KontraktdashboardController controller){
		super(Resources.getResource("title.mipssfelt"), Resources.getResource("info.mipssFelt"));
		parentPanel = this;
		this.controller = controller;
		containerPanel = new JDashboardPanel(new GridBagLayout());
		this.add(containerPanel);
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("spinner.henterData"));
		wait.setVisible(true);
		wait.setBusy(true);
		containerPanel.add(wait, new GridBagConstraints(0,0, 1,1, 0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20,0,20,0), 0, 0));			

		initGUI();
		
		hentMipssFeltData();
	}


	/**
	 * Henter MipssFelt data fra backend via controlleren
	 */
	private void hentMipssFeltData() {
		new Thread(){
			public void run(){
				try {
					mipssFeltController.hentDataOgOppdaterUnderpaneler(parentPanel);
					wait.setVisible(false);
					containerPanel.add(pnlMain, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));			
					oppdaterStatusInfo();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Setter opp GUI
	 */
	private void initGUI() {
		pnlMain = new JDashboardPanel(new GridBagLayout());	
		mipssFeltController = new MipssFeltController(controller);
		
		pnlStatusAdvarsel = new AdvarselEllerOkPanel();
				
		pnlOk = new DropdownPanel(mipssFeltController.getOKPanel());
		pnlAdvarsler = mipssFeltController.getKunFeilogAdvarslerPanel();
		
		pnlMain.add(pnlStatusAdvarsel,	new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,0,10,0), 0, 0));
		pnlMain.add(new JSeparator(),	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlAdvarsler,		new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,10,0), 0, 0));
		pnlMain.add(new JSeparator(),	new GridBagConstraints(0,4, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlOk,				new GridBagConstraints(0,5, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));				
	}
	
	/**
	 * Oppdaterer informasjonen som viser varsel-trekant eller OK-ikon i toppen av panelet.
	 */
	public void oppdaterStatusInfo() {
		int antallFeil = mipssFeltController.getTotaltAntallFeil();
		pnlStatusAdvarsel.isOk(antallFeil);
	}

	/**
	 * Viser eller skjuler spinneren i toppen på panelet
	 */
	public void showSpinner(Boolean show) {
		pnlStatusAdvarsel.showSpinner(show);
	}

	public void oppfrisk() {
		hentMipssFeltData();		
	}
}
