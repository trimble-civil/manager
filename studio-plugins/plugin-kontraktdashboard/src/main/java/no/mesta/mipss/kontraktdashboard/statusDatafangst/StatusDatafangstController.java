package no.mesta.mipss.kontraktdashboard.statusDatafangst;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.FargetStatusInfo;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;
import no.mesta.mipss.kontraktdashboardservice.KontraktdashboardService;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;

/**
 * Controller som oppretter og populerer paneler med data
 * @author larnym
 *
 */
public class StatusDatafangstController{

	private MaintenanceContract contractBean;
	private KontraktdashboardService dashboardBean;
	private KontraktdashboardController kontraktController;

	private ArrayList<FargetStatusInfo> fargetStatusInfoList = new ArrayList<FargetStatusInfo>();
	
	//Data
	private FargetStatusInfo mistetLivstegnSisteDogn;
	private FargetStatusInfo alleUtenLivstegn;
	private FargetStatusInfo mistetGpsSisteDogn;
	private FargetStatusInfo alleUtenGPS;
	private FargetStatusInfo ugyldigStroperiode;
	private FargetStatusInfo ukjentStrometode;
	private FargetStatusInfo ukjentProduksjon;
	private FargetStatusInfo ikkeProdsatte;
	private FargetStatusInfo utenUtstyr;
	
	private JDashboardPanel pnlFeilOgAdvarsler;
	private JDashboardPanel pnlOk;
	private int totaltAntallFeil;
	
	public StatusDatafangstController(KontraktdashboardController kontraktController) {
		this.kontraktController = kontraktController;
		
		contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		dashboardBean = BeanUtil.lookup(KontraktdashboardService.BEAN_NAME, KontraktdashboardService.class);
		
		pnlFeilOgAdvarsler = new JDashboardPanel(new GridBagLayout());
		pnlOk = new JDashboardPanel(new GridBagLayout());
		
		createFargetStatusInfoElementer();		
	}

	/**
	 * Oppretter alle nødvendige FargetStatusInfo-elementer
	 */
	private void createFargetStatusInfoElementer() {
		
		//LIVSTEGN
		mistetLivstegnSisteDogn = new FargetStatusInfo(Resources.getResource("datafangst.info.label.mistetLivstegnSisteDogn"), "stk", "st_mistet_livstegn");
		alleUtenLivstegn = new FargetStatusInfo(Resources.getResource("datafangst.info.label.alleUtenLivstegn"), "stk", "st_mistet_livstegn");		

		//GPS
		
		mistetGpsSisteDogn = new FargetStatusInfo(Resources.getResource("datafangst.info.label.mistetGpsSisteDogn"), "stk", "st_mistet_gps");
		alleUtenGPS = new FargetStatusInfo(Resources.getResource("datafangst.info.label.alleUtenGPS"), "stk", "st_mistet_gps");		
		
		//SPREDERDATA
		ugyldigStroperiode = new FargetStatusInfo(Resources.getResource("datafangst.info.label.ugyldigStroperiode"), "stk", "st_ugyldig_stroperiode");		
		ukjentStrometode = new FargetStatusInfo(Resources.getResource("datafangst.info.label.ukjentStrometode"), "stk", "st_ukjent_strometode");		
		ukjentProduksjon = new FargetStatusInfo(Resources.getResource("datafangst.info.label.ukjentProduksjon"), "stk", "st_ukjent_produksjon");

		//BESTILLINGER		
		ikkeProdsatte = new FargetStatusInfo(Resources.getResource("datafangst.info.label.ikkeProdsatte"), "stk", "st_ikke_prodsatte_bestillinger");		
		utenUtstyr = new FargetStatusInfo(Resources.getResource("datafangst.info.label.utenUtstyr"), "stk", "st_uten_utstyr");
	
		fargetStatusInfoList.add(alleUtenLivstegn);
		fargetStatusInfoList.add(alleUtenGPS);
		fargetStatusInfoList.add(mistetLivstegnSisteDogn);		
		fargetStatusInfoList.add(mistetGpsSisteDogn);		
		fargetStatusInfoList.add(ugyldigStroperiode);
		fargetStatusInfoList.add(ukjentStrometode);
		fargetStatusInfoList.add(ukjentProduksjon);
		fargetStatusInfoList.add(ikkeProdsatte);
		fargetStatusInfoList.add(utenUtstyr);
	}

	/**
	 * Metode som kaller backend for uthenting av data. 
	 * Når dataene er hentet oppdateres FargetStatusInfo-elementene og underpanalene, 
	 * slik at elementer som inneholder feil blir lagt til i feil-panelet, 
	 * mens elementer som er ok blir lagt til i ok-panelet.
	 */
	public void hentDataOgOppdaterUnderpaneler(){
		nullstillData();
		hentDataFraBackend();
		oppdaterKnapperOgBakgrunn();
		oppdaterUnderpaneler();
	}
	
	/**
	 * Henter ut informasjon om kjøretøy tilknyttet kontrakten.
	 * Disse datene benyttes for å telle opp antall kjøretøy uten siste livstegn osv.
	 */
	private void hentDataFraBackend(){
				
		 	Date treDagerSiden = 	LocalDate.now().minus(Period.days(3)).toDate();
		 	Date tolvTimerSiden = LocalDate.now().minus(Period.hours(12)).toDate();
		 	Date trettiSeksTimerSiden = LocalDate.now().minus(Period.hours(36)).toDate();

		    List<KontraktKjoretoyInfo> kjoretoyList = getAlleKjoretoy();
			
		    for (KontraktKjoretoyInfo kjoretoy : kjoretoyList) {

		    	//LIVSTEGN
		    	if (kjoretoy.getDfuSisteLivtegnDato() != null && kjoretoy.getDfuSisteLivtegnDato().after(trettiSeksTimerSiden) && kjoretoy.getDfuSisteLivtegnDato().before(tolvTimerSiden)) {
		    		mistetLivstegnSisteDogn.setVerdi(mistetLivstegnSisteDogn.getVerdi() + 1);
		    	}
		    	else if (kjoretoy.getDfuSisteLivtegnDato() != null && kjoretoy.getDfuSisteLivtegnDato().before(treDagerSiden)){
		    		alleUtenLivstegn.setVerdi(alleUtenLivstegn.getVerdi() + 1);
		    	}

		    	//GPS
		    	if (kjoretoy.getDfuSistePosisjonDato() != null && kjoretoy.getDfuSistePosisjonDato().after(trettiSeksTimerSiden) && kjoretoy.getDfuSistePosisjonDato().before(tolvTimerSiden)) {
		    		mistetGpsSisteDogn.setVerdi(mistetGpsSisteDogn.getVerdi() + 1);
		    	}
		    	else if (kjoretoy.getDfuSistePosisjonDato() != null && kjoretoy.getDfuSistePosisjonDato().before(treDagerSiden)){
		    		alleUtenGPS.setVerdi(alleUtenGPS.getVerdi() + 1);
		    	}

		    	//UGYLDIG STRØPERIODE
		    	//Implementert samme logikk som benyttes i kjøretøysoversikten
		    	boolean harSprederUtenDatafangst = kjoretoy.getSprUtenDatafangstFlagg() != null && kjoretoy.getSprUtenDatafangstFlagg() == 1L;
		    	boolean harStroperioder = kjoretoy.getAktuellStroperiodeKlartekst() != null && kjoretoy.getAktuellStroperiodeKlartekst().length() > 0;
		    	Date fra = kjoretoy.getAktuellStroperiodeFraDato();
		    	Date til = kjoretoy.getAktuellStroperiodeTilDato();
		    	boolean stroperiodeOk = isPeriodValid(fra, til);

		    	if ((harSprederUtenDatafangst && !harStroperioder) || (harSprederUtenDatafangst && !stroperiodeOk)) {
		    		ugyldigStroperiode.setVerdi(ugyldigStroperiode.getVerdi() + 1);
		    	}	

		    	//UTEN UTSTYR
		    	//Ikke den beste måten å sjekke om kjøretøyet er satt opp med utstyr, men ble enig med Marius om at vi bruker denne løsningen inntil videre. 
		    	if (kjoretoy.getBestillingStatus() == null && (kjoretoy.getAktuellKonfigKlartekst() == null || kjoretoy.getAktuellKonfigKlartekst().toString().equals(""))) {
		    		utenUtstyr.setVerdi(utenUtstyr.getVerdi() + 1);
		    	}		
		    }  
		    
		    //IKKE PRODUKSJONSSATTE BESTILLINGER
			ikkeProdsatte.setVerdi(dashboardBean.getAntallIkkeProdsatteBestillinger(kontraktController.getDriftskontraktId()));
		    
		    //UKJENT STRØMETODE
		    ukjentStrometode.setVerdi(dashboardBean.getKjoretoyMedUkjentStrometode(kontraktController.getDriftskontraktId()));
		    
		    //UKJENT PRODUKSJON
		    ukjentProduksjon.setVerdi(dashboardBean.getKjoretoyMedUkjentProdukjson(kontraktController.getDriftskontraktId()));
		    
		    //TEST TEST TEST
		    //NULLER UT ALLE VERDIER. BRUKES FOR Å SE HVORDAN DET SER UT NÅR ALT ER OK
//		    for (FargetStatusInfo info : dataList) {
//				info.setVerdi(0.0);
//			}
	}
	
	/**
	 * Dataene må nullstilles i forbindelse med en "oppfriskning" av skjermbildet.
	 */
	private void nullstillData() {
		// TODO Auto-generated method stub
		mistetLivstegnSisteDogn.setVerdi(0.0);
		alleUtenLivstegn.setVerdi(0.0);
		mistetGpsSisteDogn.setVerdi(0.0);
		alleUtenGPS.setVerdi(0.0);
		ukjentStrometode.setVerdi(0.0);
		ukjentProduksjon.setVerdi(0.0);
		ugyldigStroperiode.setVerdi(0.0);
		utenUtstyr.setVerdi(0.0);
		ikkeProdsatte.setVerdi(0.0);
	}

	/**
	 * Labels og knapper som oppdateres basert på dataene som ble funnet.
	 */
	private void oppdaterKnapperOgBakgrunn() {
		
		for (FargetStatusInfo element : fargetStatusInfoList) {
			if (element.getVerdi() != 0) {				
				if(element == alleUtenLivstegn || element == alleUtenGPS){ //De to eneste som skal vise gul bakgrunn.
					element.setLabelWarningBackground();
				} else{
					element.setLabelFailureBackground();
				}				
			} else{
				element.setLabelNOBackground();
			}
		}
	}

	
	public JPanel getKunFeilogAdvarslerPanel(){
		return pnlFeilOgAdvarsler;
	}
	
	public JPanel getOKPanel(){
		return pnlOk;
	}
	
	public int getTotaltAntallFeil(){
		return totaltAntallFeil;
	}
	
	/**
	 * Legger til FargetStatusInfo-elementer til enten feil-panelet eller ok-panelet, basert på verdiene.
	 */
	private void oppdaterUnderpaneler() {
		int radFeil = 0;
		int radOk = 0;
		
		for (FargetStatusInfo element : fargetStatusInfoList) {
			if (element.getVerdi() != 0 && (element != alleUtenLivstegn && element != alleUtenGPS)) {
				
				pnlFeilOgAdvarsler.add(element.getLblElementTekst(),	new GridBagConstraints(0,radFeil, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));
				pnlFeilOgAdvarsler.add(element.getLblVerdiOgBenevnelse(),	new GridBagConstraints(1,radFeil, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,10), 0, 0));
				pnlFeilOgAdvarsler.add(element.getHelpButton(),		new GridBagConstraints(2,radFeil, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,10), 0, 0));
				radFeil++;
				
			} else{
				pnlOk.add(element.getLblElementTekst(),	new GridBagConstraints(0,radOk, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));
				pnlOk.add(element.getLblVerdiOgBenevnelse(),	new GridBagConstraints(1,radOk, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,10), 0, 0));
				pnlOk.add(element.getHelpButton(),		new GridBagConstraints(2,radOk, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,10), 0, 0));
				radOk++;				
			}
		}
		
		totaltAntallFeil = radFeil;
	}	

	
	/**
	 * Henter ut alle kjøretøy tilknyttet kontrakten
	 * @return
	 */
	private List<KontraktKjoretoyInfo> getAlleKjoretoy(){
		Date iGaar = new LocalDateTime().minusDays(1).toDate();
		return contractBean.getKontraktKjoretoyInfo(kontraktController.getDriftskontraktId(), iGaar, null);
	}
	
	/**
	 * Sjekker om perioden er ok.
	 * Benyttes i forbindelse med sjekk på godkjent stroperiode
	 */
	private boolean isPeriodValid(Date fra, Date til){
		if (fra==null&&til==null)
			return true;
		
		Date today = Clock.now();
		if (fra!=null){
			if (fra.after(today))
				return false;
		}
		if (til!=null)
			if (til.before(today))
				return false;
		
		return true;
	}
}
