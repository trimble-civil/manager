package no.mesta.mipss.kontraktdashboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class NoKontraktPanel extends JPanel {

	private Image pil = new ImageIcon(getClass().getResource("/images/kontraktpil.png")).getImage();
	private Image background = new ImageIcon(getClass().getResource("/images/summer_nature_background.jpg")).getImage();

	public NoKontraktPanel() {
		this.setLayout(new GridBagLayout());
		
		JPanel infoPanel = new JPanel();
		infoPanel.setBackground(new Color(255,255,255,150));
		infoPanel.setBorder(new EmptyBorder(10,10,10,10));
		
		JLabel info = new JLabel(Resources.getResource("ikkeValgtKontrakt.label"));
		info.setFont(new Font("Sans-Serif", Font.PLAIN, 24));
		info.setBackground(Color.LIGHT_GRAY);
		
		infoPanel.add(info);
		
		this.add(new JLabel(new ImageIcon(pil)), new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0, 0));
		this.add(infoPanel, 					 new GridBagConstraints(0,1, 1,1, 0.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,50,0,10), 0, 0));
	}
	
	
	public void paintComponent(Graphics g) {
		g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
	}	
}
