package no.mesta.mipss.kontraktdashboard.gui.dropdown;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

import no.mesta.mipss.ui.taskpane.TaskTitleBar;

import org.jdesktop.swingx.JXCollapsiblePane;

@SuppressWarnings("serial")
public class DropdownPane extends JPanel{

	private JXCollapsiblePane contentPane;
	private DropdownTitleBar titleBar;
	private JComponent content;
	
	public DropdownPane(){
		initComponents();
		initGui();
	}
	private void initGui() {
		setLayout(new BorderLayout());
		JPanel c = new JPanel(new BorderLayout());
		c.add(contentPane, BorderLayout.NORTH);
		JPanel m = new JPanel(new BorderLayout());
		m.add(titleBar, BorderLayout.NORTH);
		m.add(c, BorderLayout.CENTER);
		add(m, BorderLayout.CENTER);
		
		setOpaque(false);
		m.setOpaque(false);
		c.setOpaque(false);
	}
	private void initComponents(){
		contentPane = new JXCollapsiblePane();
		contentPane.setContentPane(createDefaultContentPane());
		titleBar = new DropdownTitleBar(contentPane);		
	}
	
	public void setTitleRightComponent(JComponent comp){
		titleBar.setRightComponent(comp);
	}
	
	public void setContent(JComponent content){
		this.content = content;
		contentPane.getContentPane().add(content);
	}
	
	public void setCollapsed(boolean collapsed){
		contentPane.setCollapsed(collapsed);
	}
	public boolean isCollapsed(){
		return contentPane.isCollapsed();
	}
	private JPanel createDefaultContentPane(){
		JPanel pnl = new JPanel(new BorderLayout());
		pnl.setBackground(Color.white);
//		pnl.setBorder(BorderFactory.createMatteBorder(1,1,1,1,Color.gray));
//		pnl.setPreferredSize(new Dimension(300, 100));
		pnl.setOpaque(false);
		return pnl;
	}
	
	
}
