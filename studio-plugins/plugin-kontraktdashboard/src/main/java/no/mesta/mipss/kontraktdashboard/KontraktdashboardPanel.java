package no.mesta.mipss.kontraktdashboard;


import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import no.mesta.mipss.kontraktdashboard.driftslogg.DriftsloggPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;
import no.mesta.mipss.kontraktdashboard.mipssfelt.MipssFeltPanel;
import no.mesta.mipss.kontraktdashboard.produksjon.ProduksjonPanel;
import no.mesta.mipss.kontraktdashboard.statusDatafangst.RodedekningPanel;
import no.mesta.mipss.kontraktdashboard.statusDatafangst.StatusDatafangstPanel;
import no.mesta.mipss.resources.images.IconResources;

/**
 * Hovedpanelet til dashboardet som oppretter og inneholder alle underpaneler.
 * @author larnym
 */
@SuppressWarnings("serial")
public class KontraktdashboardPanel extends JPanel{

	private ProduksjonPanel produksjonPanel;
	private DriftsloggPanel driftsloggPanel;
	private MipssFeltPanel mipssFeltPanel;
	private StatusDatafangstPanel statusPanel;
	private RodedekningPanel rodePanel;
	private JPopupMenu menu;

	private Image background = new ImageIcon(getClass().getResource("/images/summer_nature_background.jpg")).getImage();
	private final KontraktdashboardController controller;
	//	private Image background = new ImageIcon(getClass().getResource("/images/background_dashboard.jpeg")).getImage();
	//	private Image background = new ImageIcon(getClass().getResource("/images/blue.jpg")).getImage();

	public KontraktdashboardPanel(KontraktdashboardController controller) {
		this.controller = controller;
		this.setLayout(new BorderLayout());
		
		addContextMenu();
		
		try{
			produksjonPanel = new ProduksjonPanel(controller);
			driftsloggPanel = new DriftsloggPanel(controller);
			mipssFeltPanel = new MipssFeltPanel(controller);
			statusPanel = new StatusDatafangstPanel(controller);
			rodePanel = new RodedekningPanel(controller);

			initGUI();
		} catch (Exception e) {
			JPanel pnlFailure = new JPanel(new BorderLayout());
			JLabel lblVelg = new JLabel("Velg en kontrakt for å se dashboardet");
			lblVelg.setHorizontalAlignment(JLabel.CENTER);
			pnlFailure.add(lblVelg, BorderLayout.CENTER);
			this.add(pnlFailure, BorderLayout.CENTER);
		}

		
	}

	private void addContextMenu() {
		menu = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem(Resources.getResource("menu.oppfrisk"));
		menuItem.setIcon(IconResources.REFRESH_VIEW_ICON);
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.oppfrisk();
				
			}
		});
		menu.add(menuItem);
		
		this.setComponentPopupMenu(menu);
		
	}

	private void initGUI() {

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setOpaque(false);
		scrollPane.getViewport().setOpaque(false);	

		//Produksjon
		JDashboardPanel pnlProdusksjonContainer = new JDashboardPanel(new GridBagLayout());
		pnlProdusksjonContainer.add(produksjonPanel, 	new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,10), 0, 0));		
		pnlProdusksjonContainer.add(addEmptyPanel(), 	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,100), 0, 0));			

		//Driftslogg + Innstillinger
		JDashboardPanel pnlDLInnstillinger = new JDashboardPanel(new GridBagLayout());
		pnlDLInnstillinger.add(driftsloggPanel, 	new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,10), 0, 0));		
		pnlDLInnstillinger.add(addEmptyPanel(), 	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10,0,0,10), 0, 0));	
//		pnlDLInnstillinger.add(innstillingerPanel, 	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10,0,0,10), 0, 0));	

		
		JDashboardPanel pnlProdDrift = new JDashboardPanel(new GridBagLayout());
		pnlProdDrift.setComponentPopupMenu(menu);
		pnlProdDrift.add(pnlProdusksjonContainer, 	new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,20,0), 0, 0));		
		pnlProdDrift.add(pnlDLInnstillinger, 		new GridBagConstraints(1,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));	

		// Mipss felt
		JDashboardPanel pnlLeftColumn = new JDashboardPanel(new GridBagLayout());
		pnlLeftColumn.add(statusPanel, 			new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));		
		pnlLeftColumn.add(addEmptyPanel(), 	new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,10,10,0), 0, 0));	
		
		//Status / Datafangst
		JDashboardPanel pnlCenterColumn = new JDashboardPanel(new GridBagLayout());
		pnlCenterColumn.add(rodePanel, 			new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));		
		pnlCenterColumn.add(addEmptyPanel(), 	new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,10,0), 0, 0));	

		//Rodedekning
		JDashboardPanel pnlRightColumn = new JDashboardPanel(new GridBagLayout());
		pnlRightColumn.add(mipssFeltPanel, 			new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,10), 0, 0));		
		pnlRightColumn.add(addEmptyPanel(), 	new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,10,10), 0, 0));	

		JDashboardPanel pnlFeltStatusRode = new JDashboardPanel(new GridLayout(1,3, 10, 0));
		pnlFeltStatusRode.add(pnlLeftColumn);		
		pnlFeltStatusRode.add(pnlCenterColumn);
		pnlFeltStatusRode.add(pnlRightColumn);

		scrollPane.setViewportView(pnlProdDrift);

		this.add(pnlFeltStatusRode, BorderLayout.NORTH);
		this.add(scrollPane, BorderLayout.CENTER);

	}

	public void paintComponent(Graphics g) {
		g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
	}

	/**
	 * Oppretter og returnerer et tomt panel. 
	 * Denne metoden benyttes der hvor det er behov for å legge til et blankt vindu som skal "ta opp" ekstra plass, slik som f.eks i Gridbaglayouts.
	 * @return
	 */
	private JDashboardPanel addEmptyPanel(){
		JDashboardPanel emptyDash = new JDashboardPanel();
		return emptyDash;
	}

	public void oppfrisk() {

		produksjonPanel.oppfrisk();
		driftsloggPanel.oppfrisk();
		mipssFeltPanel.oppfrisk();
		statusPanel.oppfrisk();
		rodePanel.oppfrisk();
	}
}
