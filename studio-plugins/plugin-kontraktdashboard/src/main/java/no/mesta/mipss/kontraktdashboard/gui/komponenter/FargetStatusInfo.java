package no.mesta.mipss.kontraktdashboard.gui.komponenter;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;

import no.mesta.mipss.kontraktdashboard.DashboardUtils;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.resources.images.IconResources;

/**
 * Klasse som holder på en JLabel for hovedteksten til elementet, en JLabel for verdien og en JButton, samt andre data.
 * Denne klassen benyttes der hvor man skal vise en verdi med en gitt bakgrunnsfarge,
 * samt en hjelpeknapp. 
 * @author larnym
 */
public class FargetStatusInfo {
	private JLabel lblVerdiOgBenevnelse; //sammenslått av verdi og benevnelse
	private JLabel lblElementTekst; //Første tekst på linja
	private JButton btnHelp;
	private Double verdi;      //Tallet som skal vises
	private String benevnelse; //Benevnelse/Enhet til tallet som vises
	
	private String hjelpeFil;
	
	private DecimalFormat df = new DecimalFormat("#.##");
	
	private final Icon HELP_ICON = IconResources.HELP_16_ICON;
	private final Icon WARNING_ICON = IconResources.DASHBOARD_WARNING_ICON;
	
	private Font stdFont = new Font("Verdana", Font.PLAIN, 11);
	
	private FargetStatusInfo thisElement;
	
	public FargetStatusInfo(){
		this("");
	}
	
	public FargetStatusInfo(String benevnelse) {
		this("", benevnelse, "default");
	}
	
	
	public FargetStatusInfo(String tekst, String benevnelse, String hjelpeFil) {
		this(tekst, benevnelse);
		this.hjelpeFil = hjelpeFil;
	}
	
	public FargetStatusInfo(String tekst, String benevnelse) {
		lblVerdiOgBenevnelse = new JLabel();
		lblVerdiOgBenevnelse.setOpaque(true);
		lblVerdiOgBenevnelse.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		lblElementTekst = new JLabel(tekst);
		lblElementTekst.setFont(stdFont);
		lblVerdiOgBenevnelse.setFont(stdFont);
		setBenevnelse(benevnelse);
		setVerdi(0.0);		
		generateButton();	
		
		this.benevnelse = benevnelse;
		this.hjelpeFil = "";
		thisElement = this;
	}
	
	private JButton generateButton() {
		btnHelp = new JButton();
		btnHelp.setIcon(HELP_ICON);	
		btnHelp.setEnabled(true);
		btnHelp.setToolTipText(Resources.getResource("tooltip.helpButton"));
		btnHelp.setMargin(new Insets(0, 0, 0, 0));
		btnHelp.setBorder(null);
		btnHelp.setContentAreaFilled(false);
		
		btnHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new HjelpDialog(thisElement);
			}
		});
		return btnHelp;
	}
	
	public void setHelpButton(){
		btnHelp.setIcon(HELP_ICON);	
		btnHelp.setEnabled(true);
	}
	
	public void setWarningButton(){
		btnHelp.setIcon(WARNING_ICON);
		btnHelp.setEnabled(true);	
	}
	
	public void setButtonTooltipText(String tooltip){
		btnHelp.setToolTipText(tooltip); 
	}
	
	public JLabel getLblVerdiOgBenevnelse() {
		return lblVerdiOgBenevnelse;
	}
	
	public JLabel getLblElementTekst() {
		return lblElementTekst;
	}

	public JButton getHelpButton() {
		return btnHelp;
	}
	
	public void setHelpButton(JButton button) {
		this.btnHelp = button;
	}
	
	public Double getVerdi() {
		return verdi;
	}

	public void setVerdi(Double verdi) {
		this.verdi = verdi;
		lblVerdiOgBenevnelse.setText(df.format(verdi) + " " + getBenevnelse());		
	}
	
	public void setVerdiOgbenevnelseTekst(String tekst) {
		lblVerdiOgBenevnelse.setText(tekst);		
	}

	public String getBenevnelse() {
		return benevnelse;
	}
	public void setBenevnelse(String benevnelse) {
		this.benevnelse = benevnelse;
	}
	
	public void setLabelWarningBackground(){
		lblVerdiOgBenevnelse.setBackground(DashboardUtils.WARNING_COLOR);	
		lblVerdiOgBenevnelse.setForeground(Color.BLACK);
	}
	
	public void setLabelFailureBackground(){
		lblVerdiOgBenevnelse.setBackground(DashboardUtils.FAILURE_COLOR);
		lblVerdiOgBenevnelse.setForeground(Color.WHITE);
	}
	
	public void setLabelOKBackground(){
		lblVerdiOgBenevnelse.setBackground(DashboardUtils.OK_COLOR);
		lblVerdiOgBenevnelse.setForeground(Color.WHITE);
	}
	
	public void setLabelNOBackground(){
		lblVerdiOgBenevnelse.setBackground(null);
	}
	
	public String getHjelpeFil() {
		return hjelpeFil;
	}
	
	public void setTooltip(String tooltip) {
		lblElementTekst.setToolTipText(tooltip);
		lblVerdiOgBenevnelse.setToolTipText(tooltip);
	}
}
