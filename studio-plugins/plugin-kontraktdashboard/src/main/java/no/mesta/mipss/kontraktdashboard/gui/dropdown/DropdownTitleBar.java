package no.mesta.mipss.kontraktdashboard.gui.dropdown;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

import no.mesta.mipss.ui.taskpane.TaskToggleButton;

import org.jdesktop.swingx.JXCollapsiblePane;

@SuppressWarnings("serial")
public class DropdownTitleBar extends JPanel{
	
	public DropdownTitleBar(JXCollapsiblePane contentPane){
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(100, 20));
		setPreferredSize(new Dimension(100, 20));
		setOpaque(false);
		
	}

	@Override
	public int getHeight(){
		return 20;
	}
	
	public void setContent(JComponent contents){
		contents.setOpaque(false);
		add(contents, BorderLayout.CENTER);
	}
	
	
	public void setRightComponent(JComponent component){
		JPanel pnl = new JPanel();
		pnl.setLayout(new BoxLayout(pnl, BoxLayout.LINE_AXIS));
		pnl.add(Box.createHorizontalStrut(5));
		pnl.add(component);
		pnl.setOpaque(false);
		add(pnl, BorderLayout.EAST);
		component.setOpaque(false);
	}
}
