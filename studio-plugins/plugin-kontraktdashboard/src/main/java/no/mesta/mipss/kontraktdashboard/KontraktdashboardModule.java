package no.mesta.mipss.kontraktdashboard;

import javax.swing.JComponent;

import no.mesta.mipss.plugin.MipssPlugin;

/**
 * Modul for å vise viktig informasjon om en valgt kontrakt.
 * @author larnym
 */
@SuppressWarnings("serial")
public class KontraktdashboardModule extends MipssPlugin{
	private KontraktdashboardController controller;
	private KontraktdashboardPanel kontraktdashboardPanel;
	
	private NoKontraktPanel noKontraktPanel;
	
	@Override
	public JComponent getModuleGUI() {
		if (controller.getDriftskontrakt() != null) {
			return kontraktdashboardPanel;
		} else{
			return noKontraktPanel;
		}
	}

	@Override
	protected void initPluginGUI() {
		controller = new KontraktdashboardController(this);
		if (controller.getDriftskontrakt() != null) {
			this.setDisplayName("<html>Kontraktdashboard for <b>" + controller.getDriftskontrakt().getKontraktnummer() + " - " + controller.getDriftskontrakt().getKontraktnavn() + "</b></html>");
			kontraktdashboardPanel = new KontraktdashboardPanel(getController());
			controller.setHovedPanel(kontraktdashboardPanel);
		} else{
			this.setDisplayName(Resources.getResource("ikkeValgtKontrakt.displayName"));
			noKontraktPanel = new NoKontraktPanel();
		}		
	}

	@Override
	public void shutdown() {
		controller = null;
		kontraktdashboardPanel = null;	
	}

	public KontraktdashboardController getController() {
		return controller;
	}
}
