package no.mesta.mipss.kontraktdashboard;

import java.awt.Color;
import java.util.Calendar;
import java.util.Date;

public class DashboardUtils {
	
	public static final Color WARNING_COLOR = new Color(255, 216, 0);
	public static final Color FAILURE_COLOR = new Color(255, 0, 0);
	public static final Color OK_COLOR = new Color(45, 187, 40);
	public static final Color MESTA_COLOR= new Color(1, 116, 183);
	
	public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
}
