package no.mesta.mipss.kontraktdashboard.gui.dropdown;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JLabel;


@SuppressWarnings("serial")
public class LinkLabel extends JLabel{

	private Action action;
	
	public LinkLabel(String title, final Action action){
		this.action = action;
		setText(title);
		setForeground(Color.blue);
		addMouseListener(new LinkMouseAdapter());
	}
	
	public LinkLabel(String title){
		setText(title);
		setForeground(Color.blue);
		addMouseListener(new LinkMouseAdapter());
	}
	
	public void setAction(Action action){
		this.action = action;
	}
	class LinkMouseAdapter extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent e) {
			ActionEvent ev = new ActionEvent(LinkLabel.this, 0, "clicked");
			action.actionPerformed(ev);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
			fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
			Font underline = new Font("Tahoma",Font.PLAIN, 11).deriveFont(fontAttributes);
			setFont(underline);
		}
		@Override
		public void mouseExited(MouseEvent e) {
			Font plain = new Font("Tahoma",Font.PLAIN, 11);
			setFont(plain);
		}
	}
}
