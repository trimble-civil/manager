package no.mesta.mipss.kontraktdashboard;

import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.DetaljertProduksjon;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.common.StroMetodeMedMengde;
import no.mesta.mipss.driftslogg.DriftsloggBean;
import no.mesta.mipss.driftslogg.DriftsloggService;
import no.mesta.mipss.driftslogg.PeriodeKontrakt;
import no.mesta.mipss.driftslogg.util.AgressoUtil;
import no.mesta.mipss.driftslogg.util.CalendarHelper;
import no.mesta.mipss.kontraktdashboardservice.KontraktdashboardService;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.produksjonsrapport.AdminrapportService;
import no.mesta.mipss.rodeservice.Rodegenerator;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * Kontroller for kontraktdashboardet.
 * @author larnym
 */
public class KontraktdashboardController {
	
	public static PropertyResourceBundleUtil guiProps;
	private KontraktdashboardModule plugin;
	private KontraktdashboardPanel hovedPanel;
	private Rodegenerator rodeBean;
	private KontraktdashboardService dashboardBean;
	private MaintenanceContract contractBean;
	private DriftsloggService driftsloggBean;
	private MaintenanceContract maintenanceContractBean;
	private AdminrapportService adminRapportBean;

	public KontraktdashboardController(KontraktdashboardModule plugin){
		this.plugin = plugin;
	}
	

	public KontraktdashboardModule getPlugin() {
		return plugin;
	}

	/**
	 * Henter ut valgt driftskontrakt-id
	 * @return
	 */
	public Long getDriftskontraktId() {		
		return plugin.getLoader().getSelectedDriftkontrakt().getId();
	}

	/**
	 * Henter ut valgt driftskontrakt
	 * @return
	 */
	public Driftkontrakt getDriftskontrakt() {		
		return plugin.getLoader().getSelectedDriftkontrakt();
	}
	
	/**
	 * Henter ut oppstartsdatoen til kontrakten
	 */
	public Date getDriftskontraktOppstartDato(){
		return plugin.getLoader().getSelectedDriftkontrakt().getOpprettetDato();
	}

	public Rodegenerator getRodeBean() {
		if (rodeBean == null) {
			rodeBean = BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class);
		}
		
		return rodeBean;
	}
	
	public MaintenanceContract getMaintenanceContractbean(){
		if(maintenanceContractBean==null){
			maintenanceContractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		}
		return maintenanceContractBean;
		
	}
	
	public AdminrapportService getAdminRapportBean(){
		if(adminRapportBean==null) {
			adminRapportBean = BeanUtil.lookup(AdminrapportService.BEAN_NAME, AdminrapportService.class);
		}
		return adminRapportBean;
		
	}
	
	public KontraktdashboardService getDashboardBean() {
		if (dashboardBean == null) {
			dashboardBean = BeanUtil.lookup(KontraktdashboardService.BEAN_NAME, KontraktdashboardService.class);
		}
		
		return dashboardBean;
	}
	
	public MaintenanceContract getContractBean(){
		if(contractBean == null){
			contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		}
		
		return contractBean;
	}
	
	public DriftsloggService getDriftsloggBean(){
		if(driftsloggBean == null){
			driftsloggBean = BeanUtil.lookup(DriftsloggService.BEAN_NAME, DriftsloggService.class);
		}
		
		return driftsloggBean;
	}


	/**
	 * Hente ut neste overføringsdato til agresso
	 * @return
	 */
	public Date getOverforingTilAgressoDato() {
		
		return AgressoUtil.getAgressoOverforingDato();
	}
	
	/**
	 * Hente ut neste overføringsdato til agresso og returnerer dette som en string
	 * @return
	 */
	public String getOverforingTilAgressoDatoString() {
		
		Date agressoOverforingDato = AgressoUtil.getAgressoOverforingDato();
		SimpleDateFormat f = new SimpleDateFormat("EEEE d. MMM 'kl.' HH:mm");
		return f.format(agressoOverforingDato);
	}
	
	/**
	 * Gets the number of trips that have been registered but not yet submitted to Manager.
	 */
	public int getNumberOfTripsRegisteredNotSubmitted() {
		return getDriftsloggBean().getNumberOfTripsRegisteredNotSubmitted(getDriftskontraktId());
	}

	/**
	 * Gets the number of trips that have been submitted but not yet processed in Manager.
	 */
	public int getNumberOfTripsSubmittedNotProcessed() {
		return getDriftsloggBean().getNumberOfTripsSubmittedNotProcessed(getDriftskontraktId());
	}
	
	/**
	 * Henter ut antall leverandørkontrakter som utløper de neste 7 dagene
	 * @return
	 */
	public int getAntallLeverandorerSomUtloper(){
		List<KontraktLeverandor> kontraktLeverandorList = new ArrayList<KontraktLeverandor>();
		int antallUtlopendeLeverandorer = 0;
		LocalDate nowPlusSevenDays= (new LocalDate()).plusDays(7);

		kontraktLeverandorList = getDriftsloggBean().getKontraktLeverandorer(getDriftskontraktId());
		for (KontraktLeverandor kontraktLeverandor : kontraktLeverandorList) {
			Date agressoInactiveDate = getContractBean().getInaktivDateForLeverandor(kontraktLeverandor.getLeverandor());
			//sjekker datoer for utløp i forhold til dagens dato, sjekker også agresso-utløpsdato på samme måte.
			if ((kontraktLeverandor.getTilDato().before(nowPlusSevenDays.toDate()) && kontraktLeverandor.getTilDato().after(Clock.now())) || 
									(agressoInactiveDate!=null && agressoInactiveDate.before(nowPlusSevenDays.toDate()) && agressoInactiveDate.after(Clock.now())))  {
				antallUtlopendeLeverandorer+=1;
			}
		}			
		return antallUtlopendeLeverandorer;
	}

	/**
	 * Henter ut antall kjøretøy med produksjon > tomkjøring.
	 * @param kontraktId
	 * @param dagSelector
	 * @return
	 */
	public Long getAntallKjoretoyMedProduksjon(Long kontraktId, DagSelector dagSelector){
		Date tilDato = new java.util.Date();
		Date fraDato= tolkDagSelectorForFraTildato(dagSelector);
		return maintenanceContractBean.getKontraktKjoretoyProduksjonAntallOverPeriod(kontraktId, fraDato, tilDato);
	}

	/** 
	 * Slår opp det totale antallet kjøretøy for en kontrakt.
	 * @param kontraktId
	 * @return
	 */
	public long getTotaltAntallKjoretoyForKontrakt(Long kontraktId){
		return getMaintenanceContractbean().getKontraktKjoretoyTotaltAntall(kontraktId);
	}

	/**
	 * Henter ut det totale antallet brøytekilometer for kontrakten de siste 24 timer eller siden kl 15 i går, avhengig av dagSelector.
	 * @param dagSelector
	 * @return
	 */
	public Long getAntallBroytekilometerForKontrakt(DagSelector dagSelector){
		Date tilDato = new java.util.Date();
		Date fraDato= tolkDagSelectorForFraTildato(dagSelector);
		return maintenanceContractBean.getAntallBroytekilometerForKontrakt(getDriftskontraktId(), fraDato, tilDato);
	}
	
	/**
	 * Henter ut data om strømetoder som er benyttet i perioden dagSelector angir
	 * @param dagSelector
	 * @return liste av StroMedodeMedMengde-objekter
	 */
	public List<StroMetodeMedMengde> getMengdePrStrometode(DagSelector dagSelector){
		Date tilDato = new java.util.Date();
		Date fraDato= tolkDagSelectorForFraTildato(dagSelector);
		return getDashboardBean().getMengdePrStrometode(this.getDriftskontraktId(), fraDato, tilDato);	 
	}

	/**
	 * Henter ut detaljert data om produksjon som har foregått for den gitte kontrakten i perioden som dagSelector angir
	 * @param dagSelector
	 * @return
     */
	public List<DetaljertProduksjon> getDetaljertProduksjon(DagSelector dagSelector) {
		Date tilDato = new java.util.Date();
		Date fraDato= tolkDagSelectorForFraTildato(dagSelector);
		return getDashboardBean().getDetaljertProduksjon(this.getDriftskontraktId(), fraDato, tilDato);
	}

	public void oppfrisk() {
		hovedPanel.oppfrisk();
	}


	public void setHovedPanel(KontraktdashboardPanel kontraktdashboardPanel) {
		this.hovedPanel = kontraktdashboardPanel;		
	}


	public Font getFont() {
		return new Font("Verdana", Font.PLAIN, 12);
	}

	/**
	 * Henter ut antall retardasjonsmålinger og antall kilometer kontinuerlige friksjonsmålinger.
	 * @param driftskontraktId
	 * @param dagSelector
	 * @return
	 */
	public NokkeltallFelt hentAntRetKmKontForKontrakt(Long driftskontraktId, DagSelector dagSelector) {
		Date tilDato = new java.util.Date();
		Date fraDato= tolkDagSelectorForFraTildato(dagSelector);
		return getAdminRapportBean().hentAntRetKmKontForKontrakt(this.getDriftskontraktId(), fraDato, tilDato);
	}
			
	/**
	 * 
	 * Justerer fra og tildato i forhold til hva som er indikert i dagselector
	 * @param dagSelector
	 */
	public Date tolkDagSelectorForFraTildato(DagSelector dagSelector){
		if(dagSelector.getIndikator().equals(DagSelector.PeriodeIndikator.TREDAGENFOR)){
			return (new DateTime().toDateMidnight().toDateTime().minusDays(1).plusHours(15)).toDate();
		}
		if(dagSelector.getIndikator().equals(DagSelector.PeriodeIndikator.TREDOGN)){
			return (new DateTime().toDateMidnight().toDateTime().minusDays(3).plusHours(15)).toDate();
		}

		return null;
	}

}
