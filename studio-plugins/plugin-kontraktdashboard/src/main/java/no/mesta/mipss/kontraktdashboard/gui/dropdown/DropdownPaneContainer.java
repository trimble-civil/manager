package no.mesta.mipss.kontraktdashboard.gui.dropdown;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.jdesktop.swingx.VerticalLayout;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

public class DropdownPaneContainer extends JPanel{

	private List<DropdownPane> taskPanes = new ArrayList<DropdownPane>();
	public DropdownPaneContainer(){
		setLayout(new VerticalLayout(10));
		setOpaque(false);
	}
	
	public void addTaskPane(DropdownPane taskPane){
		taskPanes.add(taskPane);
		add(taskPane);
	}
	public void removeAllTaskPanes(){
		taskPanes.clear();
		removeAll();
	}
	public void collapseAll(boolean collapsed){
		for (DropdownPane p:taskPanes){
			p.setCollapsed(collapsed);
		}
	}
}
