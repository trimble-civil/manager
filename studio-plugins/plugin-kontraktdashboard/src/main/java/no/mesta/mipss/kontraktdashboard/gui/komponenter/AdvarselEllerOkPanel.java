package no.mesta.mipss.kontraktdashboard.gui.komponenter;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.JLabel;

import org.jdesktop.swingx.JXBusyLabel;

import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class AdvarselEllerOkPanel extends JDashboardPanel{
	
	private final Icon OK_ICON = IconResources.OK_48;
	private final Icon OK_INFO_ICON = IconResources.OK_INFO_48;
	private final Icon WARNING_ICON = IconResources.WARNING_48;
	
	private JLabel lblTekst;
	private JLabel lblIcon;
	private JXBusyLabel wait;
	
	private Font font = new Font("verdana", Font.BOLD, 14);
				
	public AdvarselEllerOkPanel(){

		this.setLayout(new GridBagLayout());
		lblTekst = new JLabel();
		lblIcon = new JLabel(WARNING_ICON);
		lblTekst.setFont(font);
		
		wait = new JXBusyLabel();
		wait.setVisible(false);
		wait.setBusy(false);
		
		this.add(lblIcon, 	new GridBagConstraints(0,0, 1,1, 0.0,1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,20,0,0), 0, 0));
		this.add(lblTekst, 	new GridBagConstraints(1,0, 1,1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,30,0,10), 0, 0));	
		this.add(wait,		new GridBagConstraints(2,0, 1,1, 0.0,1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0,10,0,10), 0, 0));	
	}
	
	public void isOk(int antallFeil){
		if (antallFeil == 0) {
			lblTekst.setText(Resources.getResource("infotekst.ok"));
			lblIcon.setIcon(OK_ICON);
		} else{
			lblTekst.setText(Resources.getResource("infotekst.feilFunnet"));
			lblIcon.setIcon(WARNING_ICON);
		}
	}
	
	public void isMuligFeil(String tekst){
		lblIcon.setIcon(OK_INFO_ICON);
		lblTekst.setText(tekst);
	}
	
	public void setTekst(String tekst){
		lblTekst.setText(tekst);
	}

	public void showSpinner(Boolean show) {
		if (show) {
			wait.setBusy(true);
			wait.setVisible(true);
		} else{
			wait.setBusy(false);
			wait.setVisible(false);
		}		
	}
}
