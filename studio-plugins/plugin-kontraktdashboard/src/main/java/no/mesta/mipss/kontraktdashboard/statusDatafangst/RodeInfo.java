package no.mesta.mipss.kontraktdashboard.statusDatafangst;

import no.mesta.mipss.kontraktdashboard.gui.komponenter.FargetStatusInfo;

import org.jfree.chart.ChartPanel;

/**
 * Klasse som holder på informasjon om R12 eller strø- og brøtreroder
 * @author larnym
 */
public class RodeInfo {
	
	private String navn;
	private Double dekning;
	private ChartPanel graf;
	private Boolean erR12Rode;
	private FargetStatusInfo data;	
	
	public RodeInfo(String hjelpeFil){
		data = new FargetStatusInfo("", "%", hjelpeFil);
		setErR12Rode(false);
	}
	
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		this.navn = navn;
	}
	
	public Double getDekning() {
		return dekning;
	}
	
	public void setDekning(Double dekning) {
		this.dekning = dekning;
		data.setVerdi(dekning);
		if (dekning < 99.95) {
			data.setLabelFailureBackground();
		}
	}
	
	public ChartPanel getGraf() {
		return graf;
	}
	
	public void setGraf(ChartPanel graf) {
		this.graf = graf;
	}
	
	public Boolean getErR12Rode() {
		return erR12Rode;
	}
	
	public void setErR12Rode(Boolean erR12Rode) {
		this.erR12Rode = erR12Rode;
		if (erR12Rode && dekning < 99.95) {
			data.setLabelWarningBackground();
		}		
	}

	public FargetStatusInfo getData() {
		return data;
	}

	public void setData(FargetStatusInfo data) {
		this.data = data;
	}
}
