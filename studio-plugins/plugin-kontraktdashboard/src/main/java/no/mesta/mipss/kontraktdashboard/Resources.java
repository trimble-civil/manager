package no.mesta.mipss.kontraktdashboard;


import no.mesta.mipss.common.PropertyResourceBundleUtil;

/**
 * Ressurser som benyttes i forbindelse med GUI-tekst osv.
 * @author larnym
 */
public class Resources {
	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "kontraktdashboardText";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
	public static String getResource(String key, String... values){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key, values);
	}
	

}
