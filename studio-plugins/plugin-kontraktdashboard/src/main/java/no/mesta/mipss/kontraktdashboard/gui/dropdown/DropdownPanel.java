package no.mesta.mipss.kontraktdashboard.gui.dropdown;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.ui.taskpane.LinkLabel;

@SuppressWarnings("serial")
public class DropdownPanel extends DropdownPaneContainer{
	private DropdownPane paneSearch;
	private JPanel pnlInnerPanel;
	
	public DropdownPanel(JPanel pnlInnerPanel){
		this.pnlInnerPanel = pnlInnerPanel;
		initComponents();
	}
	private void initComponents() {
		paneSearch = new DropdownPane();
		paneSearch.setContent(pnlInnerPanel);
		
		paneSearch.setTitleRightComponent(createSearchPaneTitle());
		
		addTaskPane(paneSearch);
		paneSearch.setCollapsed(true);
		
	}
	
	private JPanel createSearchPaneTitle(){
		
		final LinkLabel label = new LinkLabel(Resources.getResource("dropdown.vis"));
		Action labelAction = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (paneSearch.isCollapsed()){
					paneSearch.setCollapsed(false);
					label.setText(Resources.getResource("dropdown.skjul"));
				}else{
					paneSearch.setCollapsed(true);
					label.setText(Resources.getResource("dropdown.vis"));
				}
			}
		};
		label.setAction(labelAction);
		JPanel titleContent = new JPanel();
		titleContent.setLayout(new BoxLayout(titleContent, BoxLayout.PAGE_AXIS));
		titleContent.add(Box.createHorizontalStrut(0));
		titleContent.add(label);
		return titleContent;
	}
}
