package no.mesta.mipss.kontraktdashboard.statusDatafangst;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JSeparator;

import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.dropdown.DropdownPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.AdvarselEllerOkPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.DashboardBoxTitlePanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class StatusDatafangstPanel extends DashboardBoxTitlePanel{

	private JPanel containerPanel;
	private JXBusyLabel wait;
	private JDashboardPanel pnlMain;
	private DropdownPanel pnlOk;
	private StatusDatafangstController statusController;
	private KontraktdashboardController controller;
	
	private JPanel pnlAdvarsler;
	private AdvarselEllerOkPanel pnlStatusAdvarsel;
	
	public StatusDatafangstPanel(KontraktdashboardController controller){
		super(Resources.getResource("title.status"), Resources.getResource("info.status"));
		this.controller = controller;
				
		containerPanel = new JDashboardPanel(new GridBagLayout());
		this.add(containerPanel);
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("spinner.henterData"));
		wait.setBusy(true);
		wait.setVisible(true);
		containerPanel.add(wait, new GridBagConstraints(0,0, 1,1, 0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20,0,20,0), 0, 0));			
		
		initGUI();
		
		oppdaterGUIMedData();		
	}

	
	private void oppdaterGUIMedData() {
		new Thread(){
			public void run(){
				try {
					statusController.hentDataOgOppdaterUnderpaneler();
					pnlStatusAdvarsel.isOk(statusController.getTotaltAntallFeil());
					showSpinner(false);
					wait.setVisible(false);
					containerPanel.add(pnlMain, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));			

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();	
	}

	private void initGUI() {
		pnlMain = new JDashboardPanel(new GridBagLayout());
		statusController = new StatusDatafangstController(controller);
		
		pnlStatusAdvarsel = new AdvarselEllerOkPanel();
				
		pnlOk = new DropdownPanel(statusController.getOKPanel());
		pnlAdvarsler = statusController.getKunFeilogAdvarslerPanel();
		
		pnlMain.add(pnlStatusAdvarsel,	new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,0,10,0), 0, 0));
		pnlMain.add(new JSeparator(),	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlAdvarsler,		new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,10,0), 0, 0));
		pnlMain.add(new JSeparator(),	new GridBagConstraints(0,3, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlOk,				new GridBagConstraints(0,4, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		
	}
	
	/**
	 * Viser eller skjuler spinneren i toppen på panelet
	 */
	public void showSpinner(Boolean show) {
		pnlStatusAdvarsel.showSpinner(show);		
	}

	@Override
	public void oppfrisk() {
		showSpinner(true);
		oppdaterGUIMedData();
		
	}
}
