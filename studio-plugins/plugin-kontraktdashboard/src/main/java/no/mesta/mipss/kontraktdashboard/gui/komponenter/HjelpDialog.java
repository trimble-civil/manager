package no.mesta.mipss.kontraktdashboard.gui.komponenter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import no.mesta.mipss.common.ResourceFetcher;

/**
 * Dialog som viser hjelpeinformasjon. Benyttes ved trykk på hjelpknappene i kontraktdashboard.
 * Åpner default.html dersom FargetStatusInfo-element ikke har satt en hjelpeFil-link
 * 
 * NB!
 * Hjelpefil-linken i FargetStatusInfo må være lik som navnet på html-filen for at denne klassen skal vise riktig fil.
 * 
 * @author larnym
 */

@SuppressWarnings("serial")
public class HjelpDialog extends JDialog {
	
	private URL url;
	private String urlString = "";
	private JEditorPane pane;
	private JScrollPane scrollPane;
	private final String DEFAULT_STRING = "default";
	
	public HjelpDialog(FargetStatusInfo infoElement){
		
		super(null, "Hjelp", ModalityType.MODELESS);
		
		pane = new JEditorPane();
		scrollPane = new JScrollPane();
		urlString = infoElement.getHjelpeFil();
		try {
			if (infoElement.getHjelpeFil().isEmpty()) {
				urlString = DEFAULT_STRING;
			}
			url = ResourceFetcher.getResourceAsURL("/hjelp/" + urlString + ".html");
			
			pane.setPage(url);
			scrollPane.setViewportView(pane);
		} catch (MalformedURLException e) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			try {
				urlString = DEFAULT_STRING;
				url = ResourceFetcher.getResourceAsURL("/hjelp/" + urlString + ".html");
				pane.setPage(url);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
		
		this.add(scrollPane);
		setSize(600, 760);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}

	public HjelpDialog(StandAloneInfoHelpIkon infoElement){
		
		super(null, "Hjelp", ModalityType.MODELESS);
		
		pane = new JEditorPane();
		scrollPane = new JScrollPane();
		urlString = infoElement.getHjelpeFil();
		try {
			if (infoElement.getHjelpeFil().isEmpty()) {
				urlString = DEFAULT_STRING;
			}
			url = ResourceFetcher.getResourceAsURL("/hjelp/" + urlString + ".html");
			
			pane.setPage(url);
			scrollPane.setViewportView(pane);
		} catch (MalformedURLException e) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			try {
				urlString = DEFAULT_STRING;
				url = ResourceFetcher.getResourceAsURL("/hjelp/" + urlString + ".html");
				pane.setPage(url);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
			
		this.add(scrollPane);
		setSize(600, 760);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}

	
	
}
