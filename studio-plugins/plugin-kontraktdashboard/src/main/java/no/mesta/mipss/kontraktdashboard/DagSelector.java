package no.mesta.mipss.kontraktdashboard;

/**
 * Dette er klassen som fungerer som visning i dropdownboks i produksjonspanelet i Dashboard.
 * I forbindelse med valg i denne dropdownboksen, har man behov for enkelt å indikere hva slags datagrunnlag man ønsker å vise.
 * 
 * indikator er en enum som forenkler tolkningen av valgt verdi i comboboksen.
 * visningsTekst er teksten brukeren ser som mulig valg i dropdownboksen
 * .
 * @author olehel
 *
 */
public class DagSelector{

	private String visningsTekst;
	private PeriodeIndikator indikator;
	
	public enum PeriodeIndikator{TREDAGENFOR, ETTDOGN, TREDOGN;}

	public DagSelector(String visningsTekst, PeriodeIndikator indikator){
		this.visningsTekst = visningsTekst;
		this.setIndikator(indikator);
		
	}
	
	public String getVisningsTekst() {
		return visningsTekst;
	}
	public void setVisningsTekst(String visningsTekst) {
		this.visningsTekst = visningsTekst;
	}

	

	public String toString(){
		return visningsTekst;
	}

	public void setIndikator(PeriodeIndikator indikator) {
		this.indikator = indikator;
	}

	public PeriodeIndikator getIndikator() {
		return indikator;
	}
	
}
