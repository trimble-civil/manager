package no.mesta.mipss.kontraktdashboard.driftslogg;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.DashboardBoxTitlePanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.FargetStatusInfo;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;

import org.jdesktop.swingx.JXBusyLabel;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 * Panel for å vise frem viktig informasjon fra driftsloggen.
 * @author olehel
 */
@SuppressWarnings("serial")
public class DriftsloggPanel extends DashboardBoxTitlePanel{
	
	private KontraktdashboardController controller;
	private JPanel containerPanel;
	private JXBusyLabel wait;
	private JDashboardPanel pnlMain;
	
	private FargetStatusInfo overforAgressoDato;
	private FargetStatusInfo tripsRegisteredNotSubmitted;
	private FargetStatusInfo tripsSubmittedNotProcessed;
	private FargetStatusInfo antallLeverandorerSomUtloper;

	public DriftsloggPanel(KontraktdashboardController controller){
		super(Resources.getResource("title.driftslogg", MipssDateFormatter.formatDate(new Date(), "MM yyyy")),
				Resources.getResource("info.driftslogg"));
		this.controller = controller;
		containerPanel = new JDashboardPanel(new GridBagLayout());
		this.add(containerPanel);
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("spinner.henterData"));
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("spinner.henterData"));

		tripsSubmittedNotProcessed = new FargetStatusInfo(Resources.getResource("driftslogg.info.label.tripsSubmittedNotProcessed"), "stk", "dl_trips_not_processed");
		tripsRegisteredNotSubmitted = new FargetStatusInfo(Resources.getResource("driftslogg.info.label.tripsRegisteredNotSubmitted"), "stk", "dl_trips_not_submitted");
		antallLeverandorerSomUtloper = new FargetStatusInfo(Resources.getResource("driftslogg.info.label.antallLeverandorerSomUtloper"), "stk", "dl_utlopende_leverandorer");

		hentDriftsloggData();

		initGUI();
	}

	/**
	 * Henter ned data og oppdaterer felter
	 */
	private void hentDriftsloggData() {
		String driftsloggMelding = KonfigparamPreferences.getInstance().hentEnForApp(
				"studio.mipss.driftslogg", "operationLogTransferMessage").getVerdi();
		overforAgressoDato = new FargetStatusInfo(driftsloggMelding, "");
		overforAgressoDato.setVerdiOgbenevnelseTekst(controller.getOverforingTilAgressoDatoString());

		Date dato = controller.getOverforingTilAgressoDato();
		LocalDate today = new LocalDate();
		LocalDate overforingAgresso = new LocalDate(dato);

		overforingAgresso = overforingAgresso.plusDays(1);  //Overføring til agresso skal gjelde hele den dagen overføringen skjer.
		int numberOfDays = Days.daysBetween(today, overforingAgresso).getDays();

		tripsSubmittedNotProcessed.setVerdi((double) controller.getNumberOfTripsSubmittedNotProcessed());
		tripsRegisteredNotSubmitted.setVerdi((double) controller.getNumberOfTripsRegisteredNotSubmitted());
		antallLeverandorerSomUtloper.setVerdi((double) controller.getAntallLeverandorerSomUtloper());
		
		if (numberOfDays > 7) {
			overforAgressoDato.setLabelOKBackground();
			if(tripsRegisteredNotSubmitted.getVerdi().intValue()>0){
				tripsRegisteredNotSubmitted.setHelpButton();
			}
			if(tripsSubmittedNotProcessed.getVerdi().intValue()>0){
				tripsSubmittedNotProcessed.setHelpButton();
			}
			if(antallLeverandorerSomUtloper.getVerdi().intValue()>0){
				antallLeverandorerSomUtloper.setWarningButton();
				antallLeverandorerSomUtloper.setLabelWarningBackground();
			}
		} else if (numberOfDays > 3){
			overforAgressoDato.setLabelWarningBackground();
			if(tripsRegisteredNotSubmitted.getVerdi().intValue()>0){
				tripsRegisteredNotSubmitted.setWarningButton();
				tripsRegisteredNotSubmitted.setLabelFailureBackground();
			}
			if(tripsSubmittedNotProcessed.getVerdi().intValue()>0){
				tripsSubmittedNotProcessed.setWarningButton();
				tripsSubmittedNotProcessed.setLabelFailureBackground();
			}
			if(antallLeverandorerSomUtloper.getVerdi().intValue()>0){
				antallLeverandorerSomUtloper.setWarningButton();
				antallLeverandorerSomUtloper.setLabelFailureBackground();
			}
		} else{
			overforAgressoDato.setLabelFailureBackground();
		}
	}

	/**
	 * Setter opp GUI
	 */
	private void initGUI() {
		pnlMain = new JDashboardPanel(new GridBagLayout());

		overforAgressoDato.getLblElementTekst().setHorizontalAlignment(JLabel.CENTER);
		overforAgressoDato.getLblVerdiOgBenevnelse().setHorizontalAlignment(JLabel.CENTER);

		pnlMain.add(tripsSubmittedNotProcessed.getLblElementTekst(),			    new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
		pnlMain.add(tripsSubmittedNotProcessed.getLblVerdiOgBenevnelse(),			new GridBagConstraints(1,0, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,0,10), 0, 0));
		pnlMain.add(tripsSubmittedNotProcessed.getHelpButton(),				new GridBagConstraints(2,0, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,0), 0, 0));

		pnlMain.add(tripsRegisteredNotSubmitted.getLblElementTekst(),		    new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
		pnlMain.add(tripsRegisteredNotSubmitted.getLblVerdiOgBenevnelse(),	        new GridBagConstraints(1,1, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,0,10), 0, 0));
		pnlMain.add(tripsRegisteredNotSubmitted.getHelpButton(),		        new GridBagConstraints(2,1, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,0), 0, 0));

		if(antallLeverandorerSomUtloper.getVerdi().intValue()>0){
			pnlMain.add(antallLeverandorerSomUtloper.getLblElementTekst(),		    	new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
			pnlMain.add(antallLeverandorerSomUtloper.getLblVerdiOgBenevnelse(),			new GridBagConstraints(1,2, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,0,10), 0, 0));
			pnlMain.add(antallLeverandorerSomUtloper.getHelpButton(),				new GridBagConstraints(2,2, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,0), 0, 0));
		}

		pnlMain.add(overforAgressoDato.getLblElementTekst(),				new GridBagConstraints(0,3, 2,1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20,0,0,0), 0, 0));
		pnlMain.add(overforAgressoDato.getLblVerdiOgBenevnelse(),				new GridBagConstraints(0,4, 3,1, 1.0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
	
		containerPanel.add(pnlMain, 								new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0, 0));			
	}


	public void oppfrisk() {
		hentDriftsloggData();
	}

}