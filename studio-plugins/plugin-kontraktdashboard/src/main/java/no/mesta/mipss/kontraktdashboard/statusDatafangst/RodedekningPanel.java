package no.mesta.mipss.kontraktdashboard.statusDatafangst;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import no.mesta.mipss.kontraktdashboard.DashboardUtils;
import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.dropdown.DropdownPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.AdvarselEllerOkPanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.DashboardBoxTitlePanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;
import no.mesta.mipss.persistence.kontrakt.Rodetype;

import org.jdesktop.swingx.JXBusyLabel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * Panel som viser rodedekning for "Strø- og brøyteroder" og "R12-roder".
 * @author larnym
 */
@SuppressWarnings("serial")
public class RodedekningPanel extends DashboardBoxTitlePanel{
	
	private KontraktdashboardController controller;
	private JPanel containerPanel;
	private JDashboardPanel pnlMain;
	private JXBusyLabel wait;
	private final int R12RODE = 96;
	private final int STRORODE = 15;
	private final Double MAX_PROSENT = 99.95;
	
	private AdvarselEllerOkPanel pnlStatusAdvarsel;
	
	private JPanel pnlOk;
	private JPanel pnlAdvarsler;
	private DropdownPanel pnlAltOk;
	
	private RodeInfo stroRode;
	private RodeInfo r12Rode;
	
	private StringBuilder advarselTekst;
	
	public RodedekningPanel(KontraktdashboardController controller) {
		super(Resources.getResource("title.rodedekning"), Resources.getResource("info.rodedekning"));
		
		pnlMain = new JDashboardPanel(new GridBagLayout());
		this.controller = controller;
		containerPanel = new JDashboardPanel(new GridBagLayout());
		
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("spinner.henterData"));
		wait.setBusy(true);
		wait.setVisible(true);
		
		containerPanel.add(wait, new GridBagConstraints(0,0, 1,1, 0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20,0,20,0), 0, 0));			

		initGUI();
		
		oppdaterGUIMedData();
				
	}

	private void initGUI() {
		pnlMain = new JDashboardPanel(new GridBagLayout());
		
		pnlStatusAdvarsel = new AdvarselEllerOkPanel();
		
		pnlAdvarsler = new JDashboardPanel(new GridBagLayout());
		
		pnlOk = new JDashboardPanel(new GridBagLayout());
		pnlAltOk = new DropdownPanel(pnlOk);
		
		pnlMain.add(pnlStatusAdvarsel,	new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,0,10,0), 0, 0));
		pnlMain.add(new JSeparator(),	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlAdvarsler,		new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,10,0), 0, 0));
		pnlMain.add(new JSeparator(),	new GridBagConstraints(0,3, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlAltOk,			new GridBagConstraints(0,4, 1,1, 0.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		
		this.add(containerPanel);
	}


	/**
	 * Setter igang en ny tråd som genererer grafene og legger de til i GUI'et.
	 */
	private void oppdaterGUIMedData() {
		
		new Thread(){
			public void run(){
				try {
					hentDataOgLagGrafer();
					leggGraferTilIGUI();
					wait.setVisible(false);
					showSpinner(false);
					containerPanel.add(pnlMain, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));							

				} catch (Exception e) {
					e.printStackTrace();
				}
			}	
		}.start();	

	}
	
	private void hentDataOgLagGrafer() {
		List<Rodetype> rodetyper = controller.getRodeBean().getRodetypeForKontrakt(controller.getDriftskontraktId());
		
		for (Rodetype rodetype : rodetyper) {

			Double dekning = controller.getRodeBean().getDekningsgradRodetypeKontraktveinett(controller.getDriftskontraktId(), rodetype.getId(), true);	

			//TEST TEST TEST
			//dekning = 100.0;
			
			if(dekning != null){

				if (isStroRode(rodetype.getId())) {
					stroRode = new RodeInfo("ro_rodedekning_stro");
					stroRode.setDekning(dekning);
					stroRode.setNavn(rodetype.getNavn());							
					stroRode.setErR12Rode(false);	
					stroRode.setGraf(lagRodeGrafPanel(stroRode.getDekning(), false));
					
				} else if(isR12Rode(rodetype.getId())){
					r12Rode = new RodeInfo("ro_rodedekning_R12");
					r12Rode.setDekning(dekning);
					r12Rode.setNavn(rodetype.getNavn());							
					r12Rode.setErR12Rode(true);	
					r12Rode.setGraf(lagRodeGrafPanel(r12Rode.getDekning(), true));
				}
			}
		}	
	}

	private void leggGraferTilIGUI() {
		
		if(stroRode != null && r12Rode != null){
			
			JPanel pnlStroRode = new JDashboardPanel(new GridBagLayout());
			pnlStroRode.add(new JLabel(stroRode.getNavn()),		new GridBagConstraints(0,0, 1,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));
			pnlStroRode.add(stroRode.getGraf(),					new GridBagConstraints(0,1, 1,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
			pnlStroRode.add(stroRode.getData().getLblVerdiOgBenevnelse(),	new GridBagConstraints(1,1, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0, 0));
			pnlStroRode.add(stroRode.getData().getHelpButton(), 		new GridBagConstraints(2,1, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0, 0));			
		
			JPanel pnlR12Rode = new JDashboardPanel(new GridBagLayout());
			pnlR12Rode.add(new JLabel(r12Rode.getNavn()),		new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));
			pnlR12Rode.add(r12Rode.getGraf(),					new GridBagConstraints(0,3, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
			pnlR12Rode.add(r12Rode.getData().getLblVerdiOgBenevnelse(),	new GridBagConstraints(1,3, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0, 0));
			pnlR12Rode.add(r12Rode.getData().getHelpButton(), 		new GridBagConstraints(2,3, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,10), 0, 0));			

			advarselTekst = new StringBuilder();
			advarselTekst.append("<html>");		
					
			if (r12Rode.getDekning() < MAX_PROSENT) {
				pnlAdvarsler.add(pnlR12Rode, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
				advarselTekst.append(Resources.getResource("rodedekning.label.muligFeilR12"));
				pnlStatusAdvarsel.isMuligFeil("");
			} else{
				pnlOk.add(pnlR12Rode, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
			}
			
			if (stroRode.getDekning() < MAX_PROSENT) {
				pnlAdvarsler.add(pnlStroRode, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
				advarselTekst.append(Resources.getResource("rodedekning.label.muligFeilStro"));
				pnlStatusAdvarsel.isOk(1);
			} else{
				pnlOk.add(pnlStroRode, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
			}
			
			if (stroRode.getDekning() >= MAX_PROSENT && r12Rode.getDekning() >= MAX_PROSENT) {
				advarselTekst.append(Resources.getResource("infotekst.ok"));
				pnlStatusAdvarsel.isOk(0);
			}
			
			advarselTekst.append("</html>");
			
			pnlStatusAdvarsel.setTekst(advarselTekst.toString());
			
		}else {
			pnlStatusAdvarsel.isMuligFeil(Resources.getResource("rodedekning.label.ikkeGyldigDekning"));
		}

	}
	
	private boolean isStroRode(Long id){		
		return id == STRORODE ? true : false;
	}
	
	private boolean isR12Rode(Long id){		
		return id == R12RODE ? true : false;
	}

	/**
	 * Oppretter et horisontalt bar-chart basert på dekningsgraden.
	 * @param dekning
	 * @return
	 */
	private ChartPanel lagRodeGrafPanel(Double dekning, boolean erR12rode) {
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(dekning, "", "");
		
		JFreeChart chart = ChartFactory.createBarChart(null, null, null, dataset, PlotOrientation.HORIZONTAL, false, true, false);
		chart.setBackgroundPaint(Color.white);
		chart.getPlot().setOutlinePaint(null);
		
		CategoryPlot plot = chart.getCategoryPlot();
		ValueAxis rangeAxis = plot.getRangeAxis();
		if (dekning > MAX_PROSENT) {
			rangeAxis.setRange(0, dekning);
		} else{
			rangeAxis.setRange(0, dekning + 10);
		}
		
		rangeAxis.setVisible(false);
		rangeAxis.setUpperMargin(10);
		
		CategoryAxis axis = plot.getDomainAxis();
		axis.setVisible(false);
		
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		if (dekning < MAX_PROSENT) {
			if (erR12rode){
				renderer.setSeriesPaint(0, DashboardUtils.WARNING_COLOR);
			}else{
				renderer.setSeriesPaint(0, DashboardUtils.FAILURE_COLOR);
			}
			
		} else {
			renderer.setSeriesPaint(0, new Color(0, 102, 0));
		}

		ChartPanel chartPanel = new ChartPanel(chart);
		
		Dimension d = new Dimension(this.getWidth() - 100 ,30);
		chartPanel.setPreferredSize(d);
		return chartPanel;
	}
	
	/**
	 * Viser eller skjuler spinneren i toppen på panelet
	 */
	public void showSpinner(Boolean show) {
		pnlStatusAdvarsel.showSpinner(show);		
	}

	public void oppfrisk() {
		showSpinner(true);
		oppdaterGUIMedData();		
	}
}
