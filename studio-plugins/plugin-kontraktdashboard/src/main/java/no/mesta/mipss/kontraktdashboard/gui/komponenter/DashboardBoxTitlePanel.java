package no.mesta.mipss.kontraktdashboard.gui.komponenter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.kontraktdashboard.DashboardUtils;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.painter.RectanglePainter;

/**
 * JXTitledPanel hvor bakgrunnsfarge og gjennomsiktighet settes.
 * @author larnym
 */
@SuppressWarnings("serial")
public class DashboardBoxTitlePanel extends JXTitledPanel {
	
	private String infoTekst;
	private JButton refreshButton;
		
	public DashboardBoxTitlePanel(){
		this("", "");
	}
	
	public DashboardBoxTitlePanel(LayoutManager mgr){
		this.setLayout(mgr);
	}
	
	/**
	 * 
	 * @param title - Tittelen på panelet
	 * @param infoTekst - Teksten som vises ved trykk på info-knappen øverst til høyre
	 */
	public DashboardBoxTitlePanel(String title, String infoTekst) {
		this.infoTekst = infoTekst;
		this.setTitle(title);
		Font titleFont = new Font("Verdana", Font.BOLD, 14);
		this.setTitleFont(titleFont);	
		this.setBackground(Color.WHITE);
		this.setOpaque(true);
		
		this.setTitlePainter(new RectanglePainter(DashboardUtils.MESTA_COLOR, DashboardUtils.MESTA_COLOR));
		this.setTitleForeground(Color.WHITE);
		
		addInfoAndRefreshButton();		
	}

	private void addInfoAndRefreshButton() {
		
		//Info
		JButton infoButton = new JButton();
		infoButton.setIcon(IconResources.INFO_16_DASHBOARD);
		infoButton.setToolTipText(Resources.getResource("info.infobutton.tooltip"));
		infoButton.setMargin(new Insets(0, 0, 0, 0));
		infoButton.setBorder(null);
		infoButton.setContentAreaFilled(false);		
		
		//Refresh
		refreshButton = new JButton();
		refreshButton.setIcon(IconResources.REFRESH_16_DASHBOARD);
		refreshButton.setToolTipText(Resources.getResource("info.refreshbutton.tooltip"));
		refreshButton.setMargin(new Insets(0, 0, 0, 30));
		refreshButton.setBorder(null);
		refreshButton.setContentAreaFilled(false);		
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		pnlButtons.add(refreshButton,	new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(2,0,2,5), 0, 0));
		pnlButtons.add(infoButton,		new GridBagConstraints(1,0, 1,1, 1.0,1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(2,0,2,2), 0, 0));
		pnlButtons.setOpaque(false);
		
		
		this.setRightDecoration(pnlButtons);
		
		infoButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, infoTekst, Resources.getResource("info.title"), JOptionPane.INFORMATION_MESSAGE);
				
			}
		});	
		
		refreshButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				oppfrisk();
				
			}
		});
	}
	
	/**
	 * Oppfrisker panelet
	 */
	public void oppfrisk(){
		
	}
}
