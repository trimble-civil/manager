package no.mesta.mipss.kontraktdashboard.produksjon;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.*;

import no.mesta.mipss.common.DetaljertProduksjon;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.StroMetodeMedMengde;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.kontraktdashboard.DagSelector;
import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.DashboardBoxTitlePanel;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.FargetStatusInfo;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;

import org.jdesktop.swingx.JXBusyLabel;
import org.jfree.util.Log;
import org.joda.time.DateTime;

/**
 * Panel som viser en oversikt over produksjonen som har skjedd en valgt periode.
 * @author olehel
 */
@SuppressWarnings("serial")
public class ProduksjonPanel extends DashboardBoxTitlePanel implements PropertyChangeSource {

	private KontraktdashboardController controller;
	private JPanel containerPanel;
	private DagSelector [] dager = {new DagSelector(Resources.getResource("produksjon.dager.dropdown.yesterdaytime"),DagSelector.PeriodeIndikator.TREDAGENFOR), 
									new DagSelector(Resources.getResource("produksjon.dager.dropdown.treedays") + " (" + getWeekdayNameThreeDaysAgo() + " kl 15:00)",DagSelector.PeriodeIndikator.TREDOGN)};
	private JXBusyLabel wait;
		
	private JDashboardPanel pnlMain;
	private JDashboardPanel pnlDatoVelger;
	private JDashboardPanel pnlProdData;
	private JDashboardPanel pnlProdDataDetaljert;
	private JDashboardPanel pnlStrometode;
	
	private FargetStatusInfo antallProdusert;
	private FargetStatusInfo antallRetardasjon;

	private JLabel lblProdStatus;
	private JLabel lblProdtype;
	private JLabel lblEnhet;
	private JLabel lblMengdeProd;
	private JLabel lblTimerTotalt;
	private JLabel lblKapasitetTime;
	private JLabel lblKmProd;
	private JLabel lblKmProdTime;
	
	JComboBox dagPicker;

	private DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(new Locale("no", "NO")));
		
	public ProduksjonPanel(KontraktdashboardController controller) {
		super(Resources.getResource("title.produksjon"), Resources.getResource("info.produksjon"));
		
		this.controller = controller;	
		containerPanel = new JDashboardPanel(new GridBagLayout());
		this.add(containerPanel);
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("spinner.henterData"));
		dagPicker = new JComboBox(dager);		
		initMainPanel();
		initDatoVelger();		
		initData();	
		initGui();
	}
	
	/**
	 * Returnerer korrekt ukedagsnavn på norsk, 3 dager tilbake i tid
	 * @return
	 */
	private String getWeekdayNameThreeDaysAgo() {
		return CalendarUtil.getDisplayNameForWeek((new DateTime().toDateMidnight().toDateTime().minusDays(3).plusHours(15)).toDate());
	}

	private void initMainPanel(){
		pnlMain = new JDashboardPanel(new GridBagLayout());		
	}
	
	private void initGui() {
		pnlMain.add(pnlDatoVelger,		  new GridBagConstraints(0,0, 1,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlProdData,		  new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		pnlMain.add(pnlProdDataDetaljert, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		containerPanel.add(pnlMain, 	  new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0, 0));
	}

	private void initDatoVelger() {
		pnlDatoVelger = new JDashboardPanel(new GridBagLayout());
		pnlDatoVelger.add(dagPicker, 								new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0) );
		pnlDatoVelger.add(new JSeparator(JSeparator.HORIZONTAL), 	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0) );
		
		dagPicker.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				hentProduksjonsdata();
			}
		});
	}
	
	/**
	 * Henter ut antall retadasjonsmålinger og antall kilometer kontinuerlige målinger for en gitt kontrakt
	 */
	protected void hentAntallRetardasjonsMaalinger() {
		NokkeltallFelt tilstand = new NokkeltallFelt();

		tilstand = controller.hentAntRetKmKontForKontrakt(controller.getDriftskontraktId(), (DagSelector)dagPicker.getSelectedItem());
		
		//Kan være null dersom kontrakten ikke har startet
		if (tilstand != null) {
			antallRetardasjon.setVerdi(tilstand.getAntRetardasjon().doubleValue()); //retardasjonsmålinger
		} else{
			antallRetardasjon.setVerdi(0.0); //retardasjonsmålinger
		}
	}

	private void initData() {
		pnlProdData = new JDashboardPanel(new GridBagLayout());
		antallProdusert = new FargetStatusInfo(Resources.getResource("produksjon.info.label.antallProdusert"), "av xx stk");
		antallRetardasjon = new FargetStatusInfo(Resources.getResource("produksjon.info.title.lblFriksjonsmalingerRetardasjon"), "stk");

		//Total mengde pr strometode
		pnlStrometode = new JDashboardPanel(new GridBagLayout());
		
		pnlProdData.add(antallProdusert.getLblElementTekst(), new GridBagConstraints(0,0, 1,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
		pnlProdData.add(antallProdusert.getLblVerdiOgBenevnelse(), new GridBagConstraints(1,0, 1,1, 1.0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,10,0,0), 0, 0));
		
		pnlProdData.add(antallRetardasjon.getLblElementTekst(), new GridBagConstraints(2,0, 1,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,30,0,0), 0, 0));
		pnlProdData.add(antallRetardasjon.getLblVerdiOgBenevnelse(), new GridBagConstraints(3,0, 1,1, 1.0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,0), 0, 0));

		pnlProdData.add(pnlStrometode, new GridBagConstraints(0,12, 4,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));
		lblProdStatus = new JLabel();

		pnlProdDataDetaljert = new JDashboardPanel(new GridBagLayout());
		lblProdtype = new JLabel(Resources.getResource("produksjon.detaljert.label.produksjonstype"));
		lblEnhet = new JLabel(Resources.getResource("produksjon.detaljert.label.enhet"));
		lblMengdeProd = new JLabel(Resources.getResource("produksjon.detaljert.label.mengdeProdusert"));
		lblTimerTotalt = new JLabel(Resources.getResource("produksjon.detaljert.label.timerTotalt"));
		lblKapasitetTime = new JLabel(Resources.getResource("produksjon.detaljert.label.kapasitetTime"));
		lblKmProd = new JLabel(Resources.getResource("produksjon.detaljert.label.kmProd"));
		lblKmProdTime = new JLabel(Resources.getResource("produksjon.detaljert.label.kmProdTime"));

		hentProduksjonsdata();
	}
	
	private void hentProduksjonsdata(){
		wait.setBusy(true);
		wait.setVisible(true);
		containerPanel.add(wait, new GridBagConstraints(0,0, 1,1, 0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,new Insets(20,200,20,200), 0, 0));
		
		new Thread(){
			public void run(){
				try {
					pnlMain.setVisible(false);
					hentAntallKjoretoyMedProduksjon();
					hentAntallRetardasjonsMaalinger();
					leggTilMengdePrStrometode();
					hentDetaljertProduksjonsdata();
					pnlMain.setVisible(true);
					wait.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	private void hentAntallKjoretoyMedProduksjon(){
		Long totAntKjForKontrakt = new Long(0);
		Long antKjtMedProd = new Long(0);

		totAntKjForKontrakt = controller.getTotaltAntallKjoretoyForKontrakt(controller.getDriftskontraktId());
		antKjtMedProd = controller.getAntallKjoretoyMedProduksjon(controller.getDriftskontraktId(), (DagSelector)dagPicker.getSelectedItem());
		antallProdusert.setVerdiOgbenevnelseTekst(antKjtMedProd + " av " + totAntKjForKontrakt + " stk.");
	}
	
	private void leggTilMengdePrStrometode(){
		//Loope gjennom og legg til alle som er brukt er i denne perioden.
		int linje = 1; 
		String mengdeVerdiTonnLabel = " tonn";
		Double mengdeVerdiTonnVal = 0.0;
		
		String mengdeVerdiKubikkLabel = " m3";
		Double mengdeVerdiKubikkVal = 0.0;
		
		List<StroMetodeMedMengde> stroMetodeMedMengdeList = new ArrayList<StroMetodeMedMengde>();
		JLabel lblTotalStrometodeTitle = new JLabel(Resources.getResource("produksjon.info.title.lblTotalStrometodeTitle"));
		JLabel lblTotalStrometodeKubikkTitle = new JLabel(Resources.getResource("produksjon.info.title.lblTotalStrometodeKubikkTitle"));
		JLabel lblTotalStrometodeTonnTitle = new JLabel(Resources.getResource("produksjon.info.title.lblTotalStrometodeTonnTitle"));
		
		FargetStatusInfo infoKubikkOnly = new FargetStatusInfo("",  mengdeVerdiKubikkLabel);
		
		stroMetodeMedMengdeList = controller.getMengdePrStrometode((DagSelector)dagPicker.getSelectedItem());
		pnlStrometode.removeAll(); //renser bildet.
		if(!stroMetodeMedMengdeList.isEmpty()){
			pnlStrometode.add(lblTotalStrometodeTitle, new GridBagConstraints(0,0, 1,1, 0.5,0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
			pnlStrometode.add(lblTotalStrometodeKubikkTitle, new GridBagConstraints(1,0, 1,1, 1.0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,30,0,0), 0, 0));
			pnlStrometode.add(lblTotalStrometodeTonnTitle, new GridBagConstraints(2,0, 1,1, 0.0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,100,0,0), 0, 0));
		} else{
			lblProdStatus.setText(Resources.getResource("produksjon.info.label.ingenStroing"));
			pnlStrometode.add(lblProdStatus, new GridBagConstraints(0,0, 1,1, 0.5,0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		}
		for (StroMetodeMedMengde stroMetodeMedMengde : stroMetodeMedMengdeList) {
			if(stroMetodeMedMengde.getM3_losning()!=null){
				mengdeVerdiKubikkVal = stroMetodeMedMengde.getM3_losning();
				infoKubikkOnly = new FargetStatusInfo("", mengdeVerdiKubikkLabel);
				infoKubikkOnly.setVerdi(mengdeVerdiKubikkVal);
			}
			if(stroMetodeMedMengde.getTonn_fast()!=null){
				mengdeVerdiTonnVal = stroMetodeMedMengde.getTonn_fast();
			}
			FargetStatusInfo infoTonn = new FargetStatusInfo(stroMetodeMedMengde.getStroprodukt_navn() + ":", mengdeVerdiTonnLabel);
			
			infoTonn.setVerdi(mengdeVerdiTonnVal);
			
			pnlStrometode.add(infoTonn.getLblElementTekst(), 	new GridBagConstraints(0,linje , 1,1, 1.0,0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5,0,0,0), 0, 0));
			if(stroMetodeMedMengde.getM3_losning()!=null){
				pnlStrometode.add(infoKubikkOnly.getLblVerdiOgBenevnelse(), 	new GridBagConstraints(1,linje , 1,1, 1.0,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5,0,0,0), 0, 0));
			}
			pnlStrometode.add(infoTonn.getLblVerdiOgBenevnelse(), 	new GridBagConstraints(2,linje , 1,1, 1.0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,0,0), 0, 0));
			pnlStrometode.add(infoTonn.getLblVerdiOgBenevnelse(), 	new GridBagConstraints(2,linje , 1,1, 1.0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,0,0), 0, 0));
			linje++;
		}
		pnlStrometode.updateUI();
	}

	private void hentDetaljertProduksjonsdata() {
		pnlProdDataDetaljert.removeAll();

		List<DetaljertProduksjon> detaljertProd = controller.getDetaljertProduksjon(
				(DagSelector) dagPicker.getSelectedItem());

		if (!detaljertProd.isEmpty()) {
			// kolonneoverskrifter
			leggTilDetlajertProduksjonRad(0, 0, lblProdtype, SwingConstants.CENTER, true, true);
			leggTilDetlajertProduksjonRad(1, 0, lblEnhet, SwingConstants.CENTER, true, true);
			leggTilDetlajertProduksjonRad(2, 0, lblMengdeProd, SwingConstants.CENTER, true, true);
			leggTilDetlajertProduksjonRad(3, 0, lblTimerTotalt, SwingConstants.CENTER, true, true);
			leggTilDetlajertProduksjonRad(4, 0, lblKapasitetTime, SwingConstants.CENTER, true, true);
			leggTilDetlajertProduksjonRad(5, 0, lblKmProd, SwingConstants.CENTER, true, true);
			leggTilDetlajertProduksjonRad(6, 0, lblKmProdTime, SwingConstants.CENTER, true, true);

			int rad = 1;
			for (DetaljertProduksjon prod : detaljertProd) {
				leggTilDetlajertProduksjonRad(0, rad,
						new JLabel(prod.getProdtype()), SwingConstants.LEFT, false, false);
				leggTilDetlajertProduksjonRad(1, rad,
						new JLabel(prod.getEnhet()), SwingConstants.CENTER, false, false);
				leggTilDetlajertProduksjonRad(2, rad,
						new JLabel(df.format(prod.getMengde_prod())), SwingConstants.RIGHT, false, false);
				leggTilDetlajertProduksjonRad(3, rad,
						new JLabel(df.format(prod.getTimer_totalt())), SwingConstants.RIGHT, false, false);
				leggTilDetlajertProduksjonRad(4, rad,
						new JLabel(df.format(prod.getKapasitet())), SwingConstants.RIGHT, false, false);
				leggTilDetlajertProduksjonRad(5, rad,
						new JLabel(df.format(prod.getKm_prod())), SwingConstants.RIGHT, false, false);
				leggTilDetlajertProduksjonRad(6, rad,
						new JLabel(df.format(prod.getKm_prod_time())), SwingConstants.RIGHT, false, false);

				rad++;
			}
		} else {
			lblProdStatus.setText(Resources.getResource("produksjon.detaljert.label.ingenProduksjon"));
		}

		pnlProdDataDetaljert.updateUI();
	}

	private void leggTilDetlajertProduksjonRad(int kolonne, int rad, JLabel tekst,
   			int justering, boolean fet, boolean luftOver) {
		// Lar oss alternere hvilke rader i tabellen som skal være grå.
		if (rad % 2 == 0) {
			tekst.setOpaque(true);
		}

		// Lar oss sette fet skrift teksten (brukes foreløpig bare til kolonneoverskriftene).
		if (fet) {
			Font f = new Font(tekst.getFont().getName(), Font.BOLD, tekst.getFont().getSize());
			tekst.setFont(f);
		}

		Insets luft = new Insets(0, 0, 0, 0);
		// Lar oss legge luft over kolonneoverskriftene.
		if (luftOver) {
			luft = new Insets(10, 0, 0, 0);
		}

		tekst.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		tekst.setHorizontalAlignment(justering);

		pnlProdDataDetaljert.add(
				tekst,
				new GridBagConstraints(
						kolonne, rad,
						1, 1, 1.0, 0,
						GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
						luft,
						0, 0
				)
		);
	}

	public void oppfrisk() {
		hentProduksjonsdata();
	}

}