package no.mesta.mipss.kontraktdashboard.gui.komponenter;

import java.awt.LayoutManager;

import javax.swing.JPanel;

/**
 * Panel som benyttes på kontraktdashboard. Dette panelet sørger for at panelet er transparant, 
 * slik at bakgrunnsfargen på boksene enkelt kan byttes via controlleren.
 * Dette brukes gjerne på underpanelene innenfor hvert hovedpanel.
 * @author larnym
 */

@SuppressWarnings("serial")
public class JDashboardPanel extends JPanel{
	
	public JDashboardPanel() {
		super();
		this.setOpaque(false);
	}
	
	public JDashboardPanel(LayoutManager mgr){
		super(mgr);
		this.setOpaque(false);
	}
}
