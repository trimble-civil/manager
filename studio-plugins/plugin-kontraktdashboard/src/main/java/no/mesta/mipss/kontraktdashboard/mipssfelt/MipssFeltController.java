package no.mesta.mipss.kontraktdashboard.mipssfelt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.kontraktdashboard.KontraktdashboardController;
import no.mesta.mipss.kontraktdashboard.Resources;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.FargetStatusInfo;
import no.mesta.mipss.kontraktdashboard.gui.komponenter.JDashboardPanel;
import no.mesta.mipss.kontraktdashboardservice.KontraktdashboardService;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import no.mesta.mipss.mipssfelt.AvvikQueryParams.RapportType;
import no.mesta.mipss.mipssfelt.AvvikService;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.produksjonsrapport.AdminrapportService;

import org.joda.time.LocalDate;

/**
 * Controller som oppretter og populerer paneler med data.
 * @author larnym
 */
public class MipssFeltController{

	private KontraktdashboardController kontraktController;
	
	private MipssFeltPanel parentPanel;
	
	private AdminrapportService mipssFeltBean;
	private AvvikService avvikBean;
	private KontraktdashboardService dashboardbean;

	private ArrayList<FargetStatusInfo> fargetStatusInfoList = new ArrayList<FargetStatusInfo>();
	
	private FargetStatusInfo antallAvvik;
	private FargetStatusInfo antallKommendeMangler;
	private FargetStatusInfo antallManglerSisteSyv;
	private FargetStatusInfo tilleggsarbeidSisteSyv;
	private FargetStatusInfo rSkjemaEnDag;
	private FargetStatusInfo rSkjemaEnMaaned;
	private FargetStatusInfo dekningRV;
	private FargetStatusInfo dekningFV;	
	private FargetStatusInfo dekningEV;	
	
	private JDashboardPanel pnlFeilOgAdvarsler;
	private JDashboardPanel pnlOk;
	private int totaltAntallFeil;
	
	public MipssFeltController(KontraktdashboardController kontraktController) {
		this.kontraktController = kontraktController;
		mipssFeltBean = BeanUtil.lookup(AdminrapportService.BEAN_NAME, AdminrapportService.class);
		avvikBean = BeanUtil.lookup(AvvikService.BEAN_NAME, AvvikService.class);
		dashboardbean = BeanUtil.lookup(KontraktdashboardService.BEAN_NAME, KontraktdashboardService.class);

		pnlFeilOgAdvarsler = new JDashboardPanel(new GridBagLayout());
		pnlOk = new JDashboardPanel(new GridBagLayout());
		
		createFargetStatusInfoElementer();		
	}

	/**
	 * Oppretter alle nødvendige FargetStatusInfo-elementer
	 */
	private void createFargetStatusInfoElementer() {
		
		//AVVIK / MANGLER / TILLEGGSARBEID
		antallAvvik = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.antallAvvik"), "stk", "mf_nytt_avvik");
		antallKommendeMangler = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.antallKommendeMangler"), "stk", "mf_kommende_mangler");
		antallManglerSisteSyv = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.antallManglerSisteSyv"), "stk", "mf_mangler_siste_syv");
		tilleggsarbeidSisteSyv = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.tilleggsarbeidSisteSyv"), "stk", "mf_tilleggsarbeid_siste_syv");
		
		//R-SKJEMA
		rSkjemaEnDag = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.rSkjemaEnDag"), "stk", "mf_usendte_rskjema_ett_dogn");
		rSkjemaEnMaaned = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.rSkjemaEnMaaned"), "stk", "mf_usendte_rskjema_en_maaned");

		//DEKNINGSGRAD
		dekningRV = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.dekningRV"), "%", "mf_dekningsgrad_RV");
		dekningFV = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.dekningFV"), "%", "mf_dekningsgrad_FV");
		dekningEV = new FargetStatusInfo(Resources.getResource("mipssfelt.info.label.dekningEV"), "%", "mf_dekningsgrad_EV");

		fargetStatusInfoList.add(rSkjemaEnDag);
		fargetStatusInfoList.add(rSkjemaEnMaaned);
		fargetStatusInfoList.add(antallKommendeMangler);	
		fargetStatusInfoList.add(antallManglerSisteSyv);
		fargetStatusInfoList.add(dekningRV);
		fargetStatusInfoList.add(dekningEV);
		fargetStatusInfoList.add(dekningFV);
		fargetStatusInfoList.add(antallAvvik);
		fargetStatusInfoList.add(tilleggsarbeidSisteSyv);		
	}

	/**
	 * Metode som kaller backend for uthenting av data. 
	 * Når dataene er hentet oppdateres FargetStatusInfo-elementene og underpanalene, 
	 * slik at elementer som inneholder feil blir lagt til i feil-panelet, 
	 * mens elementer som er ok blir lagt til i ok-panelet.
	 * @param parentPanel 
	 */
	public void hentDataOgOppdaterUnderpaneler(MipssFeltPanel parentPanel){
		this.parentPanel = parentPanel;
		hentDataFraBackend();
		oppdaterKnapperOgBakgrunn();
		oppdaterUnderpaneler();
	}
	
	/**
	 * Henter ut informasjon om kjøretøy tilknyttet kontrakten.
	 */
	private void hentDataFraBackend(){
		 
	    //Setter startverdier på det som tar lang tid å hente.
		dekningRV.setVerdi(100.0);
		dekningRV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.lasterData"));
		dekningFV.setVerdi(100.0);
		dekningFV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.lasterData"));
		dekningEV.setVerdi(100.0);
		dekningEV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.lasterData"));
		antallManglerSisteSyv.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.lasterData"));
		antallAvvik.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.lasterData"));
	    
		// Nye mangler siste syv
		new Thread(){
	    	public void run(){
	    		
	    		Double antallNyeManglerSisteSyvDagerResult = getAntallNyeManglerSisteSyvDager();
	    		
	    		if (antallNyeManglerSisteSyvDagerResult != null && antallNyeManglerSisteSyvDagerResult >= 0) {			    	
			    	antallManglerSisteSyv.setVerdi(antallNyeManglerSisteSyvDagerResult);			    	
				} else{
					antallManglerSisteSyv.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
				}

	    		oppdaterKnapperOgBakgrunn();
	    		oppdaterUnderpaneler();

	    		parentPanel.oppdaterStatusInfo();

	    	}	

	    }.start();
	    
	    //Dekningsgrad
	    parentPanel.showSpinner(true);
	    
	    // Starter en egen tråd for å hente dekningsgrad for riksvei og europavei (7 dager). 
	    new Thread(){
	    	public void run(){
	    		
	    		Double dekningsgradRVresult = getDekningsGradRv();
	    		Double dekningsgradEVresult = getDekningsgradEV();
	    		
	    		if (dekningsgradRVresult != null && dekningsgradRVresult >= 0) {			    	
			    	dekningRV.setVerdi(dekningsgradRVresult);			    	
				} else if (dekningsgradRVresult == null) {
					dekningRV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.ingenData"));
				} else {
					dekningRV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
				}
	    		
	    		if (dekningsgradEVresult != null && dekningsgradEVresult >= 0) {			    	
			    	dekningEV.setVerdi(dekningsgradEVresult);			    	
				} else if (dekningsgradEVresult == null) {
					dekningEV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.ingenData"));
				} else {
					dekningEV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
				}

	    		oppdaterKnapperOgBakgrunn();
	    		oppdaterUnderpaneler();

	    		parentPanel.oppdaterStatusInfo();

	    	}	

	    }.start();
						
		
		// Starter en egen tråd for å hente dekningsgrad for fylkesvei (14 dager)
		// Oppdaterer GUI etter at dataene er hentet.
		new Thread(){
			public void run(){
				
				Double dekningsgradFVResult = getDekningsGradFV();
				   			    
			    if (dekningsgradFVResult != null && dekningsgradFVResult >= 0) {			    	
			    	dekningFV.setVerdi(dekningsgradFVResult);			    	
				} else{
					dekningFV.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
				}
			    oppdaterKnapperOgBakgrunn();
	    		oppdaterUnderpaneler();

	    		parentPanel.oppdaterStatusInfo();

			}
		}.start();
	   

	    // STATUS 'NYTT AVVIK'	
	    Double antallAvvikResult = dashboardbean.getAntallAvvikMedStatusNyttavvik(kontraktController.getDriftskontraktId());
	    if (antallAvvikResult >= 0) {
	    	antallAvvik.setVerdi(antallAvvikResult);
		} else{
			antallAvvik.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
		}	
	    
	    
	    //R-skjema eldre enn en dag
	    Double antallEldreEnnEnDagResult = getAntallEldreEnnEnDag();
	   
	    
	    if (antallEldreEnnEnDagResult >= 0) {
	    	rSkjemaEnDag.setVerdi(antallEldreEnnEnDagResult);
		} else{
			rSkjemaEnDag.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
		}
	    
	    //R-skjema eldre enn en maaned	    
	    Double antallEldreEnnEnMaanedResult = getAntallEldreEnnEnMaaned();
	    
	    if (antallEldreEnnEnMaanedResult >= 0) {
	    	rSkjemaEnMaaned.setVerdi(antallEldreEnnEnMaanedResult);
		} else{
			rSkjemaEnMaaned.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
		}
		
		//Registreringer som tilleggsarbeid
	    Double antallTillegsarbeidResult = getAntallTillegsarbeid();
	    
	    if (antallTillegsarbeidResult >= 0) {
	    	tilleggsarbeidSisteSyv.setVerdi(antallTillegsarbeidResult);
		} else{
			tilleggsarbeidSisteSyv.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
		}     
	    
	    // Antall mulig kommende mangler
	    Double antallMuligeManglerResult = getAntallKommendeMangler();

	    if (antallMuligeManglerResult >= 0) {
	    	antallKommendeMangler.setVerdi(antallMuligeManglerResult);
		} else{
			antallKommendeMangler.setVerdiOgbenevnelseTekst(Resources.getResource("mipssfelt.feilVedHentingAvData"));
		} 
	    
	    parentPanel.showSpinner(false);
	}
	
	
	/**
	 * Henter dekningsgrad på RV, samt antall mangler for de siste syv dagene. 
	 * @return
	 */
	private Double getDekningsGradRv() {
		try {
			
			NokkeltallFelt dekningsgrad = getDekningsgradRVogEV();
		   			
			if (dekningsgrad.getProsentInspisertRv() != null) {
				return dekningsgrad.getProsentInspisertRv();
			} else{
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Double getDekningsgradEV(){
		
		try {			
			NokkeltallFelt dekningsgrad = getDekningsgradRVogEV();
		   			
			if (dekningsgrad.getProsentInspisertEv() != null) {
				return dekningsgrad.getProsentInspisertEv();
			} else{
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	private NokkeltallFelt getDekningsgradRVogEV(){
		 	Date idag = LocalDate.now().toDate();
		    Date enUkeSiden = LocalDate.now().minusDays(7).toDate();

			NokkeltallFelt nokkeltallDekningsgrad = mipssFeltBean.hentDekningsgradTilDashboard(kontraktController.getDriftskontraktId(), enUkeSiden, idag);
			return nokkeltallDekningsgrad;
	}
	
	private Double getAntallNyeManglerSisteSyvDager(){
		Date idag = LocalDate.now().toDate();
	    Date enUkeSiden = LocalDate.now().minusDays(7).toDate();
	    
	    AvvikQueryParams params = new AvvikQueryParams();
		params.setValgtKontrakt(kontraktController.getDriftskontrakt());
		params.setDateFra(enUkeSiden);
		params.setDateTil(idag);
		params.setNyeMangler(true);
		List<AvvikSokResult> nyeManglerResultList;
		try {
			nyeManglerResultList = avvikBean.sokMangler(params);
			if (nyeManglerResultList != null) {
				return (double)nyeManglerResultList.size();
			} else{
				return 0.0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;			
		}	
	}
	
	/**
	 * Henter dekningsgrad for Fylkesvei de siste 14 dagene.
	 * @return
	 */
	private Double getDekningsGradFV() {
		try {
			
		    Date idag = new Date();
		    Calendar cal = Calendar.getInstance();
		    cal.add(Calendar.DATE, -14); //Trekker fra 2 uker
		    Date toUkerSiden = cal.getTime();					
			
			NokkeltallFelt nokkeltall = mipssFeltBean.hentDekningsgradTilDashboard(kontraktController.getDriftskontraktId(), toUkerSiden, idag);
			
			return nokkeltall.getProsentInspisertFv();

		} catch (Exception e) {
			e.printStackTrace();
			return -1.0;
		}
	}
	
	/**
	 * Henter antall R-skjemaer som er eldre enn en måned
	 * @return
	 */
	private Double getAntallEldreEnnEnMaaned() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
	    Date enMaaned = cal.getTime();
	    
	    try {
		    return (double) dashboardbean.getAntallRSkjema(kontraktController.getDriftskontraktId(), kontraktController.getDriftskontraktOppstartDato(), enMaaned);
		} catch (Exception e) {
			return -1.0;
		}
	}

	/**
	 * Henter antall R-skjemar som eldre enn en dag.
	 * @return
	 */
	private Double getAntallEldreEnnEnDag() {
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1); //Trekker fra en dag
		Date iGaar = cal.getTime();
		cal.add(Calendar.MONTH, -1);
	    Date enMaaned = cal.getTime();
	    
	    return (double) dashboardbean.getAntallRSkjema(kontraktController.getDriftskontraktId(), enMaaned, iGaar);
	}

	/**
	 * Henter antall registreringer som tillegsarbeid de siste syv dager.
	 * @return
	 */
	private Double getAntallTillegsarbeid() {

		Date idag = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -7); //Trekker fra syv dager
	    Date enUkeSiden = cal.getTime();
		
	    AvvikQueryParams avvikparams = new AvvikQueryParams();
	    avvikparams.setDateFra(enUkeSiden);
	    avvikparams.setDateTil(idag);
	    avvikparams.setTilleggsarbeid(true);
	    avvikparams.setRapportType(RapportType.OPPRETTET_I_PERIODEN);
	    avvikparams.setValgtKontrakt(kontraktController.getDriftskontrakt());

	    try {
	    	List<AvvikSokResult> avvikList = avvikBean.sokAvvik(avvikparams);
	    	return (double) avvikList.size();
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return -1.0;    	
	    }
	}

	/**
	 * Henter antall mulig kommende mangler
	 * @return
	 */
	private Double getAntallKommendeMangler() {
		AvvikQueryParams avvikParams = new AvvikQueryParams();		
		avvikParams.setAapentAvvik(false);
		avvikParams.setAlleMangler(false);
		avvikParams.setDateEnabled(false);
		avvikParams.setEtterslep(false);
		avvikParams.setFristdatoOverskredet(false);
		avvikParams.setInkluderKmIVeiListe(false);
		avvikParams.setKommendeMangler(true);
		avvikParams.setKunSlettede(false);
		avvikParams.setMangler(false);
		avvikParams.setNyeMangler(false);
		avvikParams.setTilleggsarbeid(false);
		avvikParams.setRapportType(null);
		avvikParams.setTrafikksikkerhet(false);
		avvikParams.setValgtKontrakt(kontraktController.getDriftskontrakt());
	
		try {
			List<AvvikSokResult> avvikList = avvikBean.sokMangler(avvikParams);
			double teller = 0;
			Date threeDays = LocalDate.now().plusDays(3).toDateMidnight().toDate();
			for (AvvikSokResult avvikSokResult : avvikList) {
				Date frist = avvikSokResult.getTiltaksDato();
				if (frist.before(threeDays)) {
					teller++;
				}
			}
			return teller;
		} catch (Exception e) {
			e.printStackTrace();
			return -1.0;
		}
	}

	/**
	 * Labels og knapper som oppdateres basert på dataene som ble funnet.
	 */
	private void oppdaterKnapperOgBakgrunn() {
		
		for (FargetStatusInfo element : fargetStatusInfoList) {
			
			element.setLabelNOBackground();
			element.getHelpButton().setEnabled(true);
			
			if(element == antallKommendeMangler && element.getVerdi() != 0){				
				element.setLabelFailureBackground();					
			}
			
			if((element == dekningFV || element == dekningRV || element == dekningEV) && element.getVerdi() < 99.9 && element.getVerdi() > 97.0){
				element.setLabelWarningBackground();
			} else if((element == dekningFV || element == dekningRV || element == dekningEV) && element.getVerdi() < 97.0){
				element.setLabelFailureBackground();
			}
			
			if((element == rSkjemaEnDag || element == rSkjemaEnMaaned) && element.getVerdi() > 0){ //De to eneste som skal vise gul bakgrunn.
				element.setLabelWarningBackground();
			}			
		}
	}

	
	public JPanel getKunFeilogAdvarslerPanel(){
		return pnlFeilOgAdvarsler;
	}
	
	public JPanel getOKPanel(){
		return pnlOk;
	}
	
	public int getTotaltAntallFeil(){
		return totaltAntallFeil;
	}
	
	/**
	 * Legger til FargetStatusInfo-elementer til enten feil-panelet eller ok-panelet, basert på verdiene.
	 */
	private void oppdaterUnderpaneler() {
		int radFeil = 0;
		int radOk = 0;
		
		for (FargetStatusInfo element : fargetStatusInfoList) {
			
			if (	(element == antallAvvik || element == tilleggsarbeidSisteSyv) ||
					((element == dekningFV || element == dekningRV || element == dekningEV) && element.getVerdi() >= 0 && element.getVerdi() < 99.9) ||
					(element == antallKommendeMangler && element.getVerdi() > 0) ) {
				
				pnlFeilOgAdvarsler.add(element.getLblElementTekst(),		new GridBagConstraints(0,radFeil, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));
				pnlFeilOgAdvarsler.add(element.getLblVerdiOgBenevnelse(),	new GridBagConstraints(1,radFeil, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,0,10), 0, 0));
				pnlFeilOgAdvarsler.add(element.getHelpButton(),				new GridBagConstraints(2,radFeil, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,10), 0, 0));
				radFeil ++;
				
			} else {
				
				pnlOk.add(element.getLblElementTekst(),			new GridBagConstraints(0,radOk, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10,10,0,0), 0, 0));
				pnlOk.add(element.getLblVerdiOgBenevnelse(),	new GridBagConstraints(1,radOk, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,0,10), 0, 0));
				pnlOk.add(element.getHelpButton(),				new GridBagConstraints(2,radOk, 1,1, 0,0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,0,10), 0, 0));
				radOk ++;
			}
			
		}
		
		totaltAntallFeil = radFeil - 2; //Trekker fra de to som alltid vises (nye avvik og tillegsarbeid)
	}	
	
}
