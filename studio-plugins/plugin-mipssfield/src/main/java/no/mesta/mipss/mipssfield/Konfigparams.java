package no.mesta.mipss.mipssfield;

/**
 * Konfigureringsnøkler denne modulen benytter seg av.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class Konfigparams {
    public static final String MONTHS_AGO = "maanederTilbake";
    public static final String YEAR_BACK_RULE = "aarTilbakeRegel";
}
