package no.mesta.mipss.mipssfield.incident;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.ISaveStateObserver;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.incident.r11.R11Panel;
import no.mesta.mipss.mipssfield.incident.r5.R5;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.r.r11.VaerFraVaerstasjon;
import no.mesta.mipss.reports.HendelseDS;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.MipssFieldDS;
import no.mesta.mipss.reports.R11DS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Handlinger man kan gjøre på en hendelse
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JIncidentDetailActions extends JPanel {
	private final JButton closeWindow;
	private final JIncidentDetailPanel parent;
	private final JButton saveButton;
	private final JButton r5Button;
	private final JButton r11Button;
	
	private JMenuItem r5FormItem;
	private JMenuItem r11FormItem;
	private JPopupListMenu reportsPopup;
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public JIncidentDetailActions(final JIncidentDetailPanel parent) {
		this.parent = parent;

		closeWindow = new JButton(new CloseWindowAction(GUITexts.getText(GUITexts.CLOSE_WINDOW),
				IconResources.CANCEL_ICON));
		saveButton = new JButton(parent.getSaveAction());
		r5Button = new JButton(new R5Action(GUITexts.getText(GUITexts.OPPRETT_R5), IconResources.ATTACHMENT));
		r11Button = new JButton(new R11Action(GUITexts.getText(GUITexts.OPPRETT_R11), IconResources.ATTACHMENT)); 
		
		List<JMenuItem> items = new ArrayList<JMenuItem>();
		
		JMenuItem r2FormItem = new JMenuItem(new R2FormAction(GUITexts.getText(GUITexts.R2_FORM), IconResources.ADOBE_16));
		r5FormItem = new JMenuItem(new R5FormAction("R5", IconResources.ADOBE_16));
		r11FormItem = new JMenuItem(new R11FormAction(GUITexts.getText(GUITexts.R11_FORM), IconResources.ADOBE_16));
		items.add(r2FormItem);
		items.add(r5FormItem);
		items.add(r11FormItem);
		
		reportsPopup = new JPopupListMenu(GUITexts.getText(GUITexts.REPORTS), IconResources.PRINT_ICON, items);
		
		closeWindow.setEnabled(false);
		saveButton.setEnabled(false);
		r5Button.setEnabled(false);
		r11Button.setEnabled(false);
		reportsPopup.setEnabled(false);

		setLayout(new GridBagLayout());
		JPanel leftPanel = new JPanel(new GridBagLayout());
		JPanel rightPanel = new JPanel(new GridBagLayout());
		
		
		leftPanel.add(saveButton, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		leftPanel.add(r5Button, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		leftPanel.add(r11Button, new GridBagConstraints(2,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		rightPanel.add(reportsPopup, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		rightPanel.add(closeWindow, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		
		add(leftPanel, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(rightPanel, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
	}

	public void ready() {
		closeWindow.setEnabled(true);
		saveButton.setEnabled(true);
		reportsPopup.setEnabled(true);
		r5Button.setEnabled(true);
		r11Button.setEnabled(true);
		
		ISaveStateObserver<Hendelse> observer = parent.getSaveStateObserver();
		Binding<FieldToolkit, Boolean, ISaveStateObserver<Hendelse>, Boolean> observeChange = BindingHelper.createbinding(parent.getFieldToolkit(), "fieldsChanged", observer, "changed");
		Binding<ISaveStateObserver<Hendelse>, Boolean, JButton, Boolean> enableSave = BindingHelper.createbinding(observer, "enabled", saveButton, "enabled");
		parent.setSaveStateObserver(observer);
		
		parent.getFieldToolkit().addBinding(observeChange);
		parent.getFieldToolkit().addBinding(enableSave);
		//TODO utkommentert pga modellendringer
//		if (parent.getDetailObject().getErForsikringssak()){
//			r5FormItem.setEnabled(true);
//			r5Button.setText(GUITexts.getText(GUITexts.VIS_R5));
//		}else{
//			r5FormItem.setEnabled(false);
//			r5Button.setText(GUITexts.getText(GUITexts.OPPRETT_R5));
//		}
//		
//		if (parent.getDetailObject().getErSkred()){
//			r11FormItem.setEnabled(true);
//			r11Button.setText(GUITexts.getText(GUITexts.VIS_R11));
//		}else{
//			r11FormItem.setEnabled(false);
//			r11Button.setText(GUITexts.getText(GUITexts.OPPRETT_R11));
//		}
	}
	
	/**
	 * Lukker vinduet
	 */
	class CloseWindowAction extends AbstractAction {

		public CloseWindowAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			parent.getOwner().closeWindow();
		}
	}

	/**
	 * Action klasse for å åpne funn
	 */
	class R2FormAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public R2FormAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			boolean cancel = parent.saveOrCancel();
			if(cancel) {
				return;
			}
			try {
//				Hendelse hendelse = parent.getDetailObject();
//				HendelseDS dataSource = new HendelseDS(Collections.singletonList(hendelse.getGuid()));
//				dataSource.setFieldBean(parent.getParentPlugin().getMipssField());
//				JReportBuilder builder = new JReportBuilder(Report.R2_FORM, Report.R2_FORM.getDefaultParameters(),
//						dataSource, ReportHelper.EXPORT_PDF, parent.getOwner());
//				File report = builder.getGeneratedReportFile();
//				if(report != null) {
//					DesktopHelper.openFile(report);
//				}
			} catch (Throwable t) {
				parent.getParentPlugin().getLoader().handleException(t, parent.getParentPlugin(),
						GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
			}
		}
	}
	
	/**
	 * Action klasse for å åpne hendelse i R5 skjema
	 * 
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 */
	class R5FormAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public R5FormAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			boolean cancel = parent.saveOrCancel();
			if(cancel) {
				return;
			}
			try {
				//TODO utkommentert pga modellendringer
//				Hendelse hendelse = parent.getDetailObject();
//				MipssFieldDS<Forsikringsskade> dataSource = new MipssFieldDS<Forsikringsskade>(Forsikringsskade.class, Collections.singletonList(hendelse.getForsikringsskade().getGuid()));
//				dataSource.setFieldBean(parent.getParentPlugin().getMipssField());
//				JReportBuilder builder = new JReportBuilder(Report.R5_FORM, Report.R5_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, parent.getOwner());
//				File report = builder.getGeneratedReportFile();
//				if(report != null) {
//					DesktopHelper.openFile(report);
//				}
			} catch (Throwable t) {
				parent.getParentPlugin().getLoader().handleException(t, parent.getParentPlugin(),GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
			}
		}
	}
	
	class R11FormAction extends AbstractAction{
		public R11FormAction(String name, Icon icon){
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			boolean cancel = parent.saveOrCancel();
			if(cancel) {
				return;
			}
			try {
				//TODO utkommentert pga modellendringer
//				Hendelse hendelse = parent.getDetailObject();
//				R11DS dataSource = new R11DS(Skred.class, Collections.singletonList(hendelse.getSkred().getGuid()), parent.getParentPlugin().getMipssField());
//				JReportBuilder builder = new JReportBuilder(Report.R11_FORM, Report.R11_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, parent.getOwner());
//				File report = builder.getGeneratedReportFile();
//				if (report != null) {
//					DesktopHelper.openFile(report);
//				}
			} catch (Throwable t) {
				parent.getParentPlugin().getLoader().handleException(t, parent.getParentPlugin(),GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
			}
		}
	}
	class R5Action extends AbstractAction {

		public R5Action(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			MipssField bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
			Hendelse hendelse = parent.getDetailObject();
			R5 r = new R5(hendelse);
			//TODO utkommentert pga modellendringer
//			if (hendelse.getErForsikringssak()){
//				JDialog f = new JDialog(parent.getOwner(), true);
//				JPanel p = new JPanel();
//				p.setLayout(new BorderLayout());
//				p.add(r);
//				f.add(p);
//				f.setSize(new Dimension(775, 550));
//				f.pack();
//				f.setLocationRelativeTo(null);
//				f.setVisible(true);
//				
//				Forsikringsskade fs = r.getResults();
//				if (!r.isCancelled()){
//					for (SkadeKontakt sk: fs.getSkadeKontakter()){
//						if (sk.getGuid()==null){
//							sk.setGuid(ServerUtils.getInstance().fetchGuid());
//							sk.setSkade(fs);
//						}
//					}
//					if (fs.getGuid()==null)
//						fs.setGuid(ServerUtils.getInstance().fetchGuid());
//					
//					log.debug("lagre hendelsen..");
//					fs = bean.oppdaterForsikringsskade(fs, parent.getParentPlugin().getLoader().getLoggedOnUserSign());
//					hendelse.setForsikringsskade(fs);
//					log.debug("lagret");
//				}else{
//					Forsikringsskade refreshed = bean.hentForsikringsskade(fs.getGuid());
//					hendelse.setForsikringsskade(refreshed);
//				}
//			}else{
//				r.runWizard();
//				if (!r.isCancelled()){
//					Forsikringsskade f = r.getResults();
//					for (SkadeKontakt sk :f.getSkadeKontakter()){
//						sk.setGuid(ServerUtils.getInstance().fetchGuid());
//						sk.setSkade(f);
//					}
//					String guid = ServerUtils.getInstance().fetchGuid();
//					f.setGuid(guid);
//					try {
//						f = bean.addForsikringsskade(hendelse, f, parent.getParentPlugin().getLoader().getLoggedOnUserSign());
//					} catch (Throwable t){
//						JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(parent), "Beklager, en ukjent feil oppstod ved lagring av R5", "Feil", JOptionPane.ERROR_MESSAGE);
//					}
//					hendelse.setForsikringsskade(f);
//					
//					if (parent.getDetailObject().getErForsikringssak()){
//						r5FormItem.setEnabled(true);
//						r5Button.setText(GUITexts.getText(GUITexts.VIS_R5));
//						parent.getViewObject().setR5(new Integer(1));
//						parent.getParentPlugin().getIncidentTable().repaint();
//					}else{
//						r5FormItem.setEnabled(false);
//						r5Button.setText(GUITexts.getText(GUITexts.OPPRETT_R5));
//					}
//				}
//			}
		}
	}
	class R11Action extends AbstractAction {

		public R11Action(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Hendelse hendelse = parent.getDetailObject();
			//TODO utkommentert pga modellendringer
//			if (hendelse.getErSkred()){
//				JDialog d = new JDialog(parent.getOwner(), true);
//				R11Panel r = new R11Panel(hendelse);
//				d.add(r);
//				d.pack();
//				d.setLocationRelativeTo(null);
//				d.setVisible(true);
//				if (!r.isCancelled()){
//					persistHendelseSkred(hendelse, r);
//				}
//				
//			}else{
//				R11Panel r = new R11Panel(hendelse);
//				r.runWizard();
//				if (!r.isCancelled()){
//					hendelse.setSkred(r.getSkred());
//					hendelse.setVeiref(hendelse.getStedfestingList().get(0).getVeiref());
//					hendelse.setStedfestingList(hendelse.getStedfestingList());
//					parent.redrawMap();
//
//					if (parent.getDetailObject().getErSkred()){
//						r11FormItem.setEnabled(true);
//						r11Button.setText(GUITexts.getText(GUITexts.VIS_R11));
//						parent.getViewObject().setR11(new Integer(1));
//						parent.getParentPlugin().getIncidentTable().repaint();
//					}else{
//						r11FormItem.setEnabled(false);
//						r11Button.setText(GUITexts.getText(GUITexts.OPPRETT_R11));
//					}
//				}else{
//					hendelse.setSkred(r.getOriginalSkred());
//				}
//			}
		}

		private void persistHendelseSkred(Hendelse hendelse, R11Panel r) {
			//TODO utkommentert pga modellendringer
//			hendelse.getSkred().merge(r.getSkred());
//			
//			parent.getParentPlugin().getMipssField().oppdaterHendelseSkred(hendelse, parent.getParentPlugin().getLoader().getLoggedOnUserSign());
//			parent.getViewObject().setR11(new Integer(1));
//			parent.getParentPlugin().getIncidentTable().repaint();
		}
	}
	
}
