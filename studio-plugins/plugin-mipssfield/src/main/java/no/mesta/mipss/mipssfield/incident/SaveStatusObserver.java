package no.mesta.mipss.mipssfield.incident;

import no.mesta.mipss.mipssfield.SaveStateAdapter;
import no.mesta.mipss.persistence.mipssfield.Hendelse;

/**
 * Avgjør om lagreknappen skal være på eller av.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SaveStatusObserver extends SaveStateAdapter<Hendelse>  {
	
	public boolean isEnabled() {
		return isChanged() &&!isErrors();
	}

	public void setDetail(Hendelse hendelse) {
		detail = hendelse;
	}
}