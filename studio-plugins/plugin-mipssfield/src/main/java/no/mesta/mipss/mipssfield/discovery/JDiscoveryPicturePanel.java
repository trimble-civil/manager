package no.mesta.mipss.mipssfield.discovery;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.Column;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.MaxLengthEnforcer;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.ImageNameFilter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXImageView;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.CheckerboardPainter;

public class JDiscoveryPicturePanel extends JXPanel implements MipssBeanLoaderListener, ComponentListener {
	public static final Dimension BUTTON_SIZE = new Dimension(45, 22);
	private static final Logger logger = LoggerFactory.getLogger(JDiscoveryPicturePanel.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 116);
	public static final Dimension PREF_SIZE = new Dimension(450, 232);
	private static final Integer STRUT = 2;
	private AvvikBilde activeImage;
	private JLabel antallBilder;
	private final List<Binding<?, ?, ?, ?>> bindings = new ArrayList<Binding<?, ?, ?, ?>>();
	private boolean busy = true;
	private final JComponent buttons;
	private final List<JButton> buttonsList = new ArrayList<JButton>();
	private Avvik funn;
	private final MipssPunktregPictureModel model;
	private final JDiscoveryDetailPanel parent;
	private final JViewport picturesPane = new JViewport();

	public JDiscoveryPicturePanel(final JDiscoveryDetailPanel parent, final MipssPunktregPictureModel model) {
		setBackgroundPainter(new CheckerboardPainter<JDiscoveryPicturePanel>(Color.WHITE, new Color(250, 250, 250), 50));
		picturesPane.setView(createBusyPanel());
		picturesPane.setOpaque(false);
		model.addMipssBeanLoaderListener(this);
		this.model = model;
		this.parent = parent;
		antallBilder = new JLabel();
		antallBilder.setBackground(new Color(0f, 0f, 0f, 0.75f));
		antallBilder.setOpaque(true);
		antallBilder.setForeground(Color.WHITE);
		bindings.add(BindingHelper.createbinding(model, "noOfBildeString", antallBilder, "text"));
		
		
		addComponentListener(this);
		setLayout(null);

		add(picturesPane);

		buttons = createButtonsPanel();
		add(buttons);

		setComponentZOrder(picturesPane, 1);
		setComponentZOrder(buttons, 0);

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	private void addPicture() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setLocale(new Locale("no", "NO"));
		fileChooser.setFileFilter(new ImageNameFilter());
		int what = fileChooser.showOpenDialog(this);
		if (what == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			Bilde b = createBilde(file);
			AvvikBilde pb = createPunktregBilde(b);
			if (pb == null) {
				return;
			}

			funn.addBilde(pb);
			model.addPunktregBilde(pb);

			activeImage = null;
			picturesPane.setView(createImagesBox());
			blankBorders();
			draw();
		}
	}

	protected void blankBorders() {
		Component[] components = ((Box) picturesPane.getView()).getComponents();
		for (Component c : components) {
			if (c instanceof JXImageView) {
				JXImageView imageView = (JXImageView) c;
				imageView.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
			}
		}
		repaint();
	}

	private double calculateScale(Image image, Dimension size) {
		double scale = 1;

		if (image == null) {
			return scale;
		}

		double imageWidth = image.getWidth(null);
		double imageHeight = image.getHeight(null);
		double viewWidth = size.getWidth();
		double viewHeight = size.getHeight();

		if (viewWidth > viewHeight) {
			if (imageWidth < imageHeight) {
				scale = viewWidth / imageWidth;
			} else {
				scale = viewHeight / imageHeight;
			}
		} else {
			if (imageWidth > imageHeight) {
				scale = viewHeight / imageHeight;
			} else {
				scale = viewWidth / imageWidth;
			}
		}

		return scale;
	}

	private Dimension calculateViewSize(Image image) {
		double viewHeight = picturesPane.getHeight(), viewWidth = 0;
		if (image == null) {
			return new Dimension(0, (int) viewHeight);
		}

		double imageWidth = image.getWidth(null);
		double imageHeight = image.getHeight(null);

		if (imageHeight > viewHeight) {
			viewWidth = imageWidth / (imageHeight / viewHeight);
		} else {
			viewWidth = (viewHeight / imageHeight) * imageWidth;
		}

		Dimension d = new Dimension((int) viewWidth, (int) viewHeight);

		return d;
	}

	/** {@inheritDoc} */
	@Override
	public void componentHidden(ComponentEvent e) {
		draw();
	}

	/** {@inheritDoc} */
	@Override
	public void componentMoved(ComponentEvent e) {
		draw();
	}

	/** {@inheritDoc} */
	@Override
	public void componentResized(ComponentEvent e) {
		draw();
	}

	/** {@inheritDoc} */
	@Override
	public void componentShown(ComponentEvent e) {
		draw();
	}

	private Bilde createBilde(File file) {
		Bilde b = new Bilde();
		b.setGuid(ServerUtils.getInstance().fetchGuid());
		b.setNyDato(Clock.now());
		b.setOppretter(parent.getParentPlugin().getLoader().getLoggedOnUserSign());
		b.setFilnavn(file.getName());
		try {
			byte[] imageLob = IOUtils.toByteArray(new FileInputStream(file));
			b.setBildeLob(imageLob);
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

		b.setSmaabildeLob(ImageUtil.getScaledInstance(new ByteArrayInputStream(b.getBildeLob()), 320, 240, 0.5f));
		b.setBredde(b.getImage().getWidth());
		b.setHoyde(b.getImage().getHeight());

		return b;
	}

	private JComponent createBusyPanel() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.setOpaque(false);
		JXBusyLabel busy = new JXBusyLabel();
		busy.setBusy(true);
		busy.setOpaque(false);
		panel.add(busy);
		return panel;
	}

	private JComponent createButtonsPanel() {
		Box buttonPanel = Box.createHorizontalBox();

		JButton edit = new JButton(GUITexts.getText(GUITexts.EDIT));
		edit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getActiveImage() != null) {
					editPicture();
				}
			}
		});
		decorateButton(edit);
		Binding<JDiscoveryPicturePanel, Boolean, JButton, Boolean> editBinding = BindingHelper.createbinding(this,
				"${activeImage != null}", edit, "enabled");
		bindings.add(editBinding);

		JButton delete = new JButton(GUITexts.getText(GUITexts.DELETE));
		delete.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getActiveImage() != null) {
					removeImage();
					picturesPane.revalidate();
					repaint();
				}
			}
		});
		decorateButton(delete);
		Binding<JDiscoveryPicturePanel, Boolean, JButton, Boolean> deleteBinding = BindingHelper.createbinding(this,
				"${activeImage != null}", delete, "enabled");
		bindings.add(deleteBinding);
		JButton add = new JButton(GUITexts.getText(GUITexts.ADD));
		add.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				addPicture();
			}
		});
		decorateButton(add);
		JButton open = new JButton(GUITexts.getText(GUITexts.OPEN));
		decorateButton(open);
		Binding<JDiscoveryPicturePanel, Boolean, JButton, Boolean> openBinding = BindingHelper.createbinding(this,
				"${activeImage != null}", open, "enabled");
		bindings.add(openBinding);
		open.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getActiveImage() != null) {
					openImage();
				}
			}
		});

		JButton previous = new JButton(IconResources.GO_PREVIOUS_ICON);
		previous.setBorder(null);
		previous.setMargin(new Insets(0, 0, 0, 0));
		previous.setFocusable(false);
		previous.setOpaque(false);
		previous.setEnabled(false);
		buttonsList.add(previous);
		previous.addMouseListener(new MouseAdapter() {
			Scroller scroller = null;

			/** {@inheritDoc} */
			@Override
			public void mousePressed(MouseEvent e) {
				Point currentPos = picturesPane.getViewPosition();
				scroller = new Scroller(currentPos, -3);
				scroller.execute();
			}

			/** {@inheritDoc} */
			@Override
			public void mouseReleased(MouseEvent e) {
				scroller.cancelled = true;
			}
		});

		JButton next = new JButton(IconResources.GO_NEXT_ICON);
		next.setBorder(null);
		next.setMargin(new Insets(0, 0, 0, 0));
		next.setFocusable(false);
		next.setOpaque(false);
		next.setEnabled(false);
		buttonsList.add(next);
		next.addMouseListener(new MouseAdapter() {
			Scroller scroller = null;

			/** {@inheritDoc} */
			@Override
			public void mousePressed(MouseEvent e) {
				Point currentPos = picturesPane.getViewPosition();
				scroller = new Scroller(currentPos, 3);
				scroller.execute();
			}

			/** {@inheritDoc} */
			@Override
			public void mouseReleased(MouseEvent e) {
				scroller.cancelled = true;
			}
		});

		buttonPanel.add(antallBilder);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(previous);
		buttonPanel.add(BoxUtil.createHorizontalStrut(STRUT * 2));
		buttonPanel.add(next);
		buttonPanel.add(Box.createHorizontalGlue());
		if (parent.getParentPlugin().hasWriteAccess()) {
			buttonPanel.add(delete);
			buttonPanel.add(BoxUtil.createHorizontalStrut(STRUT));
			buttonPanel.add(edit);
			buttonPanel.add(BoxUtil.createHorizontalStrut(STRUT));
			buttonPanel.add(add);
			buttonPanel.add(BoxUtil.createHorizontalStrut(STRUT));
		}
		buttonPanel.add(open);

		return buttonPanel;
	}

	public JComponent createImageInfo(AvvikBilde pb, Color color) {
		Box box = Box.createVerticalBox();
		JLabel dateLabel = new JLabel(MipssDateFormatter.formatDate(pb.getBilde().getNyDato(),
				MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
		dateLabel.setForeground(color);
		dateLabel.setBackground(new Color(0f, 0f, 0f, 0.75f));
		dateLabel.setOpaque(true);
		JLabel statusLabel = new JLabel(pb.getStatusType().getNavn());
		statusLabel.setForeground(color);
		statusLabel.setBackground(new Color(0f, 0f, 0f, 0.75f));
		statusLabel.setOpaque(true);

		box.add(dateLabel);
		box.add(BoxUtil.createVerticalStrut(1));
		box.add(statusLabel);

		return box;
	}

	private JComponent createImagesBox() {
		Box bildeBox = Box.createHorizontalBox();
		List<AvvikBilde> bilder = model.getPunktregBilder();
		Collections.sort(bilder);
//		antallBilder.setText("Bilder:" + bilder.size());
		for (AvvikBilde pb : bilder) {
			final JXImageView view = new JXImageView();
			final AvvikBilde bilde = pb;
			Image image = pb.getBilde().getSmaaImage();
			if (image == null) {
				image = IconResources.MISSING_ICON.getImage();
			}
			if (pb.getBilde().getBeskrivelse() != null) {
				view.setToolTipText("<html><pre>" + pb.getBilde().getBeskrivelse() + "</pre></html>");
			}
			view.setImage(image);
			view.setDragEnabled(false);
			view.setEditable(false);
			view.addMouseListener(new MouseAdapter() {
				/** {@inheritDoc} */
				@Override
				public void mouseClicked(MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						setActiveImage(bilde);
						if (e.getClickCount() == 1) {
							blankBorders();
							view.setBorder(BorderFactory.createLineBorder(Color.GREEN, 3));
						} else if (e.getClickCount() == 2) {
							openImage();
						}
					}
				}

			});
			view.setLayout(new BorderLayout());
			JComponent info = createImageInfo(pb, Color.WHITE);
			view.add(info, BorderLayout.NORTH);

			bildeBox.add(view);
		}

		return bildeBox;
	}

	private AvvikBilde createPunktregBilde(final Bilde b) {
		final AvvikBilde pb = new AvvikBilde();
		pb.setBilde(b);
		pb.setAvvik(funn);

		final JDialog dialog = new JDialog(parent.getOwner(), GUITexts.getText(GUITexts.ADD_PICTURE), true);

		List<AvvikstatusType> statuser = setToList(funn.getStatusTyper());
		MipssComboBoxModel<AvvikstatusType> model = new MipssComboBoxModel<AvvikstatusType>(statuser);
		final JComboBox combo = new JComboBox(model);
		combo.setRenderer(new MipssListCellRenderer<AvvikstatusType>());
		ComponentSizeResources.setComponentSize(combo, new Dimension(160, 22));

		final JTextArea text = new JTextArea();
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setViewportView(text);
		ComponentSizeResources.setComponentSize(pane, new Dimension(160, 120));

		Integer maxLength = (Integer) b.getAnnotationValue("getBeskrivelse", Column.class, "length");
		MaxLengthEnforcer enforcer = new MaxLengthEnforcer(b, text, "beskrivelse", maxLength);
		enforcer.start();

		JXImageView view = new JXImageView();
		ComponentSizeResources.setComponentSize(view, new Dimension(160, 140));
		view.setSize(new Dimension(180, 160));
		view.setImage(b.getImage());
		view.setDragEnabled(false);
		view.setEditable(false);
		view.setScale(calculateScale(b.getImage(), view.getSize()));

		JButton ok = new JButton(GUITexts.getText(GUITexts.ADD));
		ok.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(ok, BUTTON_SIZE);
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (combo.getSelectedItem() != null) {
					pb.setStatusType((AvvikstatusType) combo.getSelectedItem());
					b.setBeskrivelse(text.getText());
					dialog.setVisible(false);
				}
			}
		});

		JButton cancel = new JButton(GUITexts.getText(GUITexts.CANCEL));
		cancel.setMargin(new Insets(1, 1, 1, 1));
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pb.setBilde(null);
				dialog.setVisible(false);
			}
		});

		Box buttons = BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(), cancel, BoxUtil
				.createHorizontalStrut(2), ok);

		Box gui = BoxUtil.createVerticalBox(2, BoxUtil.createHorizontalBox(2, BoxUtil.createVerticalBox(2, view),
				BoxUtil.createHorizontalStrut(2), BoxUtil.createVerticalBox(2, combo, pane)), buttons);

		dialog.add(gui);

		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		dialog.pack();
		dialog.setLocationRelativeTo(parent.getParentPlugin().getModuleGUI());
		dialog.setVisible(true);

		if (pb.getBilde() != null) {
			return pb;
		} else {
			return null;
		}
	}

	private void decorateButton(JButton button) {
		button.setMargin(new Insets(1, 1, 1, 1));
		button.setEnabled(false);
		buttonsList.add(button);
		ComponentSizeResources.setComponentSize(button, BUTTON_SIZE);
	}

	private synchronized void draw() {
		layoutComponents();
		if (!busy) {
			scaleImages();
		}
		repaint();
	}

	private void editPicture() {
		final JDialog dialog = new JDialog(parent.getOwner(), GUITexts.getText(GUITexts.EDIT), true);
		final AvvikstatusType originalStatus = getActiveImage().getStatusType();
		List<AvvikstatusType> statuser = setToList(funn.getStatusTyper());
		MipssComboBoxModel<AvvikstatusType> model = new MipssComboBoxModel<AvvikstatusType>(statuser);
		final JComboBox combo = new JComboBox(model);
		combo.setRenderer(new MipssListCellRenderer<AvvikstatusType>());
		ComponentSizeResources.setComponentSize(combo, new Dimension(160, 22));
		combo.setSelectedItem(originalStatus);

		final JTextArea text = new JTextArea();
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		text.setText(getActiveImage().getBilde().getBeskrivelse());
		JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setViewportView(text);
		ComponentSizeResources.setComponentSize(pane, new Dimension(160, 120));

		Integer maxLength = (Integer) getActiveImage().getBilde().getAnnotationValue("getBeskrivelse", Column.class,
				"length");
		MaxLengthEnforcer enforcer = new MaxLengthEnforcer(getActiveImage().getBilde(), text, "beskrivelse", maxLength);
		enforcer.start();

		JButton ok = new JButton(GUITexts.getText(GUITexts.EDIT));
		ok.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(ok, BUTTON_SIZE);
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (combo.getSelectedItem() != null) {
					getActiveImage().setStatusType((AvvikstatusType) combo.getSelectedItem());
					getActiveImage().getBilde().setBeskrivelse(text.getText());
					dialog.setVisible(false);
				}
			}
		});

		JButton cancel = new JButton(GUITexts.getText(GUITexts.CANCEL));
		cancel.setMargin(new Insets(1, 1, 1, 1));
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
			}
		});

		Box buttons = BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(), cancel, BoxUtil
				.createHorizontalStrut(2), ok);

		Box gui = BoxUtil.createVerticalBox(2, BoxUtil
				.createHorizontalBox(2, BoxUtil.createVerticalBox(2, combo, pane)), buttons);

		dialog.add(gui);

		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		dialog.pack();
		dialog.setLocationRelativeTo(parent.getParentPlugin().getModuleGUI());
		dialog.setVisible(true);

		if (!getActiveImage().getStatusType().equals(originalStatus)) {
			Component[] components = ((Box) picturesPane.getView()).getComponents();
			for (Component c : components) {
				if (c instanceof JXImageView) {
					JXImageView imageView = (JXImageView) c;
					Image image = imageView.getImage();
					if (image == getActiveImage().getBilde().getSmaaImage()) {
						imageView.removeAll();
						JComponent info = createImageInfo(activeImage, Color.WHITE);
						imageView.setLayout(new BorderLayout());
						imageView.add(info, BorderLayout.NORTH);
						break;
					}
				}
			}
			picturesPane.revalidate();
		}
	}

	public AvvikBilde getActiveImage() {
		return activeImage;
	}

	private void layoutComponents() {
		Insets insets = getInsets();
		int x = insets.left, y = insets.top;
		int width = getWidth() - insets.left - insets.right, height = getHeight() - insets.top - insets.bottom;

		buttons.setBounds(new Rectangle(x + 2, height - 20, width - 4, 20));
		ComponentSizeResources.setComponentSize(picturesPane, new Dimension(width, height));
		picturesPane.setBounds(new Rectangle(x, y, width, height));
	}

	public void reload(){
		picturesPane.setView(null);
		JComponent images = createImagesBox();
		picturesPane.setView(images);
		blankBorders();
		busy = false;
		draw();
	}
	/** {@inheritDoc} */
	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		logger.debug("loadingFinished()");

		picturesPane.setView(null);
		JComponent images = createImagesBox();
		picturesPane.setView(images);
		blankBorders();
		busy = false;
		draw();
	}

	/** {@inheritDoc} */
	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		logger.debug("loadingStarted()");
		busy = true;
		picturesPane.setView(createBusyPanel());
	}

	protected void openImage() {
		int imageIndex = model.indexOf(getActiveImage());
		JPictureDialogue picture = new JPictureDialogue(parent.getOwner(), model);
		picture.showBilde(imageIndex);
		picture.setVisible(true);
	}

	private void removeImage() {
		/*Component[] components = ((Box) picturesPane.getView()).getComponents();
		for (Component c : components) {
			if (c instanceof JXImageView) {
				JXImageView imageView = (JXImageView) c;
				Image image = imageView.getImage();
				if (image == getActiveImage().getBilde().getSmaaImage()) {
					((Box) picturesPane.getView()).remove(imageView);
					funn.removeBilde(getActiveImage());
					break;
				}
			}
		}*/
		funn.removeBilde(getActiveImage());
		model.removePunktregBilde(activeImage);
		setActiveImage(null);
	}

	private void scaleImages() {
		Component[] components = ((Box) picturesPane.getView()).getComponents();
		for (Component c : components) {
			if (c instanceof JXImageView) {
				JXImageView imageView = (JXImageView) c;
				Image image = imageView.getImage();
				Dimension size = calculateViewSize(image);
				imageView.setScale(calculateScale(image, size));
				ComponentSizeResources.setComponentSize(imageView, size);
				imageView.setSize(size);
			}
		}
	}

	public void setActiveImage(AvvikBilde image) {
		AvvikBilde b = activeImage;
		activeImage = image;
		firePropertyChange("activeImage", b, image);
	}

	public void setPunktreg(Avvik funn) {
		this.funn = funn;
		for (JButton b : buttonsList) {
			b.setEnabled(true);
		}

		for (Binding<?, ?, ?, ?> b : bindings) {
			parent.getFieldToolkit().addBinding(b);
		}
	}

	private List<AvvikstatusType> setToList(Set<AvvikstatusType> set) {
		List<AvvikstatusType> list = new ArrayList<AvvikstatusType>();
		Iterator<AvvikstatusType> it = set.iterator();
		while (it.hasNext()) {
			list.add((AvvikstatusType) it.next());
		}
		Collections.sort(list);
		return list;
	}

	class Scroller extends SwingWorker<Void, Void> {
		boolean cancelled;
		Point p;
		int step = 0;

		public Scroller(Point p, int step) {
			this.p = p;
			this.step = step;
		}

		@Override
		protected Void doInBackground() throws Exception {
			while (!cancelled) {
				int maxX = picturesPane.getView().getWidth() - picturesPane.getWidth();
				p = new Point(p.x + step, p.y);
				if (p.x < 0) {
					p = new Point(0, p.y);
					cancelled = true;
				} else if (p.x > maxX) {
					p = new Point(maxX, p.y);
					cancelled = true;
				}
				picturesPane.setViewPosition(p);
				Thread.sleep(6);
			}

			return null;
		}

	}
}
