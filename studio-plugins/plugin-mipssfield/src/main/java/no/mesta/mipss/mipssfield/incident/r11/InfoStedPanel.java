package no.mesta.mipss.mipssfield.incident.r11;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.service.veinett.VeirefVO;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

import org.apache.commons.lang.builder.EqualsBuilder;

@SuppressWarnings("serial")
public class InfoStedPanel extends JPanel{
	private Font bold;
	private final R11Controller controller;
	private JTextField koordinatFraField;
	private MipssComboBoxModel<HpWrapper> fraHpModel;
	private MipssComboBoxModel<HpWrapper> tilHpModel;
	private JComboBox veiCombo;
	private JComboBox fraHpCombo;
	private JComboBox tilHpCombo;
	private JTextField mFraField;
	private JTextField mTilField;
	private MipssComboBoxModel<VeirefVO> veiModel;
	private JTextField koordinatTilField;
	

	public InfoStedPanel(R11Controller controller){
		this.controller = controller;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(createHeaderPanel(), new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
//		add(createEmnePanel(), 	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
		add(createStedPanel(), 	new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
		
	}
	
	private JPanel createHeaderPanel(){
		
		Skred skred = controller.getSkred();
		
		JPanel panel = new JPanel(new GridBagLayout());
		
		JLabel fkLabel = new JLabel(GUITexts.getText(GUITexts.R11_FK));
		JLabel navnLabel = new JLabel(GUITexts.getText(GUITexts.R11_NAVN));
		JLabel regAvLabel = new JLabel(GUITexts.getText(GUITexts.R11_REG_AV));
		JLabel skredDatoLabel = new JLabel(GUITexts.getText(GUITexts.R11_SKREDDATO));
		
		bold = fkLabel.getFont();
		bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
		fkLabel.setFont(bold);
		navnLabel.setFont(bold);
		regAvLabel.setFont(bold);
		skredDatoLabel.setFont(bold);
		
		Driftkontrakt kontrakt = skred.getSak().getKontrakt();
		String nr = kontrakt.getKontraktnummer();
		String navn = kontrakt.getKontraktnavn();
		String fraDato = MipssDateFormatter.formatDate(kontrakt.getGyldigFraDato(), "yyyy");
		String tilDato = MipssDateFormatter.formatDate(kontrakt.getGyldigTilDato(), "yyyy");
		String regnavn = skred.getOpprettetAv();
		String dato = MipssDateFormatter.formatDate(skred.getSkredDato(), "dd.MM.yyyy HH:mm");
		JLabel fkInfoLabel = new JLabel("FK"+nr);
		JLabel navnInfoLabel = new JLabel(navn+" "+fraDato+"-"+tilDato);
		JLabel regAvInfoLabel =new JLabel(regnavn);
		JLabel skredDatoInfoLabel = new JLabel(dato);
		
		panel.add(fkLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		panel.add(fkInfoLabel, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		
		panel.add(navnLabel, new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		panel.add(navnInfoLabel, new GridBagConstraints(3,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		
		panel.add(regAvLabel, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		panel.add(regAvInfoLabel, new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		
		panel.add(skredDatoLabel, new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		panel.add(skredDatoInfoLabel, new GridBagConstraints(1,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		
		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_HEADER_TITLE)));
		return panel;
	}
	
	private JPanel createEmnePanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		
		JLabel emneLabel = new JLabel(GUITexts.getText(GUITexts.R11_EMNE));
		JTextField emneField = new JTextField();
		
		panel.add(emneLabel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		panel.add(emneField, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,5), 0,0));
		
		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_EMNE_TITLE)));
		
		BindingHelper.createbinding(controller.getSkred(), "emne", emneField, "text", BindingHelper.READ_WRITE).bind();
		return panel;
	}
	
	private JPanel createStedPanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		JLabel stedLabel = new JLabel(GUITexts.getText(GUITexts.R11_STEDSNAVN));
		JLabel veiLabel = new JLabel(GUITexts.getText(GUITexts.R11_VEI));
		JLabel fraHpLabel = new JLabel(GUITexts.getText(GUITexts.R11_FRAHP));
		JLabel tilHpLabel = new JLabel(GUITexts.getText(GUITexts.R11_TILHP));
		JLabel m1Label = new JLabel(GUITexts.getText(GUITexts.R11_M));
		JLabel m2Label = new JLabel(GUITexts.getText(GUITexts.R11_M));
		JLabel feltLabel = new JLabel(GUITexts.getText(GUITexts.R11_FELT));
		JLabel koordinatFraLabel = new JLabel(GUITexts.getText(GUITexts.R11_KOORDINAT_FRA));
		JLabel koordinatTilLabel = new JLabel(GUITexts.getText(GUITexts.R11_KOORDINAT_TIL));

		JTextField stedField = new JTextField();
		veiCombo = createVeiCombo();
		fraHpCombo = createFraHpCombo();
		tilHpCombo = createTilHpCombo();
		JComboBox feltCombo = new JComboBox();
		JCheckBox stengtCheck = new JCheckBox(GUITexts.getText(GUITexts.R11_STENGTRAS));
		koordinatFraField = new JTextField();
		koordinatFraField.setEditable(false);
		koordinatTilField = new JTextField();
		koordinatTilField.setEditable(false);
		mFraField = new JTextField();
		mTilField = new JTextField();
		
		//koble gui komponenter mot datamodeller
		Dimension size = fraHpCombo.getPreferredSize();
		size.width=50;
		fraHpCombo.setPreferredSize(size);
		tilHpCombo.setPreferredSize(size);
		
		fraHpModel = new MipssComboBoxModel<HpWrapper>(new ArrayList<HpWrapper>());
		tilHpModel = new MipssComboBoxModel<HpWrapper>(new ArrayList<HpWrapper>());
		
		veiCombo.setModel(createVeiModel());
		fraHpCombo.setModel(fraHpModel);
		tilHpCombo.setModel(tilHpModel);
		
		Egenskap felt = controller.findEgenskap(R11Panel.FELT_ID);
		EgenskapItemListener listener = new EgenskapItemListener(felt, controller.getSkred());
		feltCombo.addItemListener(listener);
		
		MipssComboBoxModel<Egenskapverdi> feltModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(felt));
		feltCombo.setModel(feltModel);
		
		feltCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(felt));
		
		if (controller.getOriginalSkred()==null){
			feltCombo.setSelectedIndex(0);
		}
		//bind guivalg mot modellobjekter
		BindingHelper.createbinding(mFraField, "text", this, "koordinatFra", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(mTilField, "text", this, "koordinatTil", BindingHelper.READ_WRITE).bind();
		
		BindingHelper.createbinding(controller.getSkred().getSak(), "skadested", stedField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(fraHpModel, "${selectedItem!=null}", mFraField, "enabled").bind();
		BindingHelper.createbinding(tilHpModel, "${selectedItem!=null}", mTilField, "enabled").bind();
		BindingHelper.createbinding(controller.getSkred(), "stengtPgaSkredfare", stengtCheck, "selected", BindingHelper.READ_WRITE).bind();
		setVeiValues();
		
		//plasser ut gui
		panel.add(stedLabel, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(stedField, 	new GridBagConstraints(1,0, 4,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		
		panel.add(veiLabel, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(veiCombo, 	new GridBagConstraints(1,1, 2,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		panel.add(fraHpLabel, 	new GridBagConstraints(3,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(fraHpCombo, 	new GridBagConstraints(4,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		panel.add(m1Label,		new GridBagConstraints(5,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(mFraField, 	new GridBagConstraints(6,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		panel.add(tilHpLabel, 	new GridBagConstraints(7,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(tilHpCombo, 	new GridBagConstraints(8,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		panel.add(m2Label, 		new GridBagConstraints(9,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(mTilField, 	new GridBagConstraints(10,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		
		panel.add(feltLabel, 	new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(feltCombo, 	new GridBagConstraints(1,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(stengtCheck, 	new GridBagConstraints(3,2, 5,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		
		panel.add(koordinatFraLabel, new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(koordinatFraField, new GridBagConstraints(1,3, 10,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		
		panel.add(koordinatTilLabel, new GridBagConstraints(0,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(koordinatTilField, new GridBagConstraints(1,4, 10,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		
		
		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_STED_TITLE)));
		return panel;
	}
	
	public void setKoordinatFra(String txtIgnoreThis){
		new SwingWorker<Void, Void>() {
			private String value;
			public Void doInBackground(){
				
				Veinettveireferanse c1=null;
				if (!mFraField.getText().equals("")){
					HpWrapper fraHp = fraHpModel.getSelectedItem();
					Integer fra = Integer.valueOf(mFraField.getText());
					c1 = new Veinettveireferanse();
					c1.setFraKm(Double.valueOf((double)fra/1000));
					c1.setTilKm(Double.valueOf((double)fra/1000));
					c1.setFylkesnummer(fraHp.getVeiref().getFylkesnummer());
					c1.setKommunenummer(fraHp.getVeiref().getKommunenummer());
					c1.setHp(fraHp.getVeiref().getHp());
					c1.setVeikategori(fraHp.getVeiref().getVeitype().charAt(0)+"");
					c1.setVeistatus(fraHp.getVeiref().getVeitype().charAt(1)+"");
					c1.setVeinummer(fraHp.getVeiref().getVeinummer());
				}
				value = controller.getKoordinater(c1, controller.getFraHp().getPunktstedfest(), controller.getFraHp());
				return null;
			}
			@Override
			public void done(){
				koordinatFraField.setText(value);
			}
		}.execute();
	}
	
	public void setKoordinatTil(String txtIgnoreThis){
		new SwingWorker<Void, Void>() {
			private String value;
			public Void doInBackground(){
				HpWrapper tilHp = tilHpModel.getSelectedItem();
				
				Veinettveireferanse c2=null;
				if (!mTilField.getText().equals("")){
					Integer til = Integer.valueOf(mTilField.getText());
					c2 = new Veinettveireferanse();
					c2.setFraKm(Double.valueOf((double)til/1000));
					c2.setTilKm(Double.valueOf((double)til/1000));
					c2.setFylkesnummer(tilHp.getVeiref().getFylkesnummer());
					c2.setKommunenummer(tilHp.getVeiref().getKommunenummer());
					c2.setHp(tilHp.getVeiref().getHp());
					c2.setVeikategori(tilHp.getVeiref().getVeitype().charAt(0)+"");
					c2.setVeistatus(tilHp.getVeiref().getVeitype().charAt(1)+"");
					c2.setVeinummer(tilHp.getVeiref().getVeinummer());
				}
				value = controller.getKoordinater(c2, controller.getTilHp().getPunktstedfest(), controller.getTilHp());
				return null;
			}
			@Override
			public void done(){
				koordinatTilField.setText(value);
			}
		}.execute();
	}
	private void setVeiValues(){
		
		VeirefVO selectedFraVei= null;
		Punktveiref fraRef = controller.getFraHp();
		Punktveiref tilRef = controller.getTilHp();
		
		for (VeirefVO veiref:veiModel.getEntities()){
			if (isVeirefEqual(veiref, fraRef)){
				selectedFraVei= veiref;
			}
		}
		veiCombo.setSelectedItem(selectedFraVei);

		HpWrapper fraHp = null;
		HpWrapper tilHp = null;
		for (HpWrapper w:fraHpModel.getEntities()){
			if (w.getVeiref().getHp().intValue()==fraRef.getHp()){
				fraHp = w;
			}
		}
		for (HpWrapper w:tilHpModel.getEntities()){
			if (w.getVeiref().getHp().intValue()==tilRef.getHp()){
				tilHp = w;
			}
		}
		
		fraHpCombo.setSelectedItem(fraHp);
		tilHpCombo.setSelectedItem(tilHp);
		
		BindingHelper.createbinding(fraHpModel, "selectedItem", this, "fraHp", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(tilHpModel, "selectedItem", this, "tilHp", BindingHelper.READ_WRITE).bind();
		//disse feltene kan ikke bindes ettersom den faktisk meterverdien kun kan settes etter at 
		//vi har hentet reflinkseksjoner fra nvdb i controlleren.
		mFraField.setText(controller.getFraHp().getMeter()+"");
		mTilField.setText(controller.getTilHp().getMeter()+"");
	}
	/**
	 * custom bindingsmetode for å sette verdien på punktveirefobjektet når bruker velger i comboboxen
	 * @param hp
	 */
	public void setFraHp(HpWrapper hp){
		if (hp!=null)
			controller.getFraHp().setHp(hp.getVeiref().getHp().intValue());
	}
	/**
	 * custom bindingsmetode for å sette verdien på punktveirefobjektet når bruker velger i comboboxen
	 * @param hp
	 */
	public void setTilHp(HpWrapper hp){
		if (hp!=null)
			controller.getTilHp().setHp(hp.getVeiref().getHp().intValue());
	}
	
	private boolean isVeirefEqual(VeirefVO ref, Punktveiref vei){
		return new EqualsBuilder()
			.append(ref.getFylkesnummer().intValue(), vei.getFylkesnummer())
			.append(ref.getKommunenummer().intValue(), vei.getKommunenummer())
			.append(ref.getVeitype(), vei.getVeikategori()+vei.getVeistatus())
			.append(ref.getVeinummer().intValue(), vei.getVeinummer())
			//.append(ref.getHp().intValue(), vei.getHp())
			.isEquals();
	}
	private MipssComboBoxModel<VeirefVO> createVeiModel(){
		List<VeirefVO> veirefList = new ArrayList<VeirefVO>();
		veiModel = new MipssComboBoxModel<VeirefVO>(veirefList);
		for (VeirefVO v:controller.getVeirefList()){
			if (!veirefList.contains(v)){
				veirefList.add(v);
			}
		}
		return veiModel;
	}
	private JComboBox createVeiCombo(){
		JComboBox veiCombo = new JComboBox();
		veiCombo.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getItem() instanceof VeirefVO){
					VeirefVO v = (VeirefVO)e.getItem();
					
					List<HpWrapper> entities = new ArrayList<HpWrapper>();
					for (VeirefVO vo:controller.getVeirefList()){
						if (vo.equals(v)){
							entities.add(new HpWrapper(vo));
						}
					}
					fraHpModel.setEntities(entities);
					tilHpModel.setEntities(entities);
				}else{
					fraHpModel.setEntities(new ArrayList<HpWrapper>());
					tilHpModel.setEntities(new ArrayList<HpWrapper>());
				}
				setKoordinatFra("");
				setKoordinatTil("");
			}
		});
		return veiCombo;
	}
	
	private JComboBox createFraHpCombo(){
		JComboBox fraHpCombo = new JComboBox();
		fraHpCombo.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				setKoordinatFra("");
				setKoordinatTil("");
			}
			
		});
		return fraHpCombo;
	}
	private JComboBox createTilHpCombo(){
		JComboBox tilHpCombo = new JComboBox();
		tilHpCombo.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				setKoordinatFra("");
				setKoordinatTil("");
			}
		});
		return tilHpCombo;
	}
	class HpWrapper implements IRenderableMipssEntity{
		private VeirefVO veiref;
		public HpWrapper(VeirefVO veiref){
			this.veiref = veiref;
		}
		public VeirefVO getVeiref(){
			return veiref;
		}
		@Override
		public String toString(){
			return String.valueOf(veiref.getHp());
		}
		@Override
		public String getTextForGUI() {
			return String.valueOf(veiref.getHp());
		}
	}
}
