package no.mesta.mipss.mipssfield.incident;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractDetailPanel;
import no.mesta.mipss.mipssfield.JDetailFrame;
import no.mesta.mipss.mipssfield.JMapPanel;
import no.mesta.mipss.mipssfield.JPunktregPanel;
import no.mesta.mipss.mipssfield.JTopLine;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.discovery.JDiscoveryDetailPanel;
import no.mesta.mipss.mipssfield.discovery.JPunktregLocatorDialog;
import no.mesta.mipss.mipssfield.vo.HendelseResult;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JLoadingPanel;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.picturepanel.MipssDefaultPictureModel;
import no.mesta.mipss.ui.picturepanel.MipssPictureModel;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Detalj panel for hendelser
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JIncidentDetailPanel extends JAbstractDetailPanel<HendelseSokView, Hendelse> implements
		MipssBeanLoaderListener {
	private static final Logger logger = LoggerFactory.getLogger(JIncidentDetailPanel.class);
	private static final int STRUT = 2;
	private JIncidentDetailActions actionsPanel;
	private Hendelse hendelse;
	private final HendelseSokView hendelseSok;

	private JMapPanel mapPanel;
	private boolean newHendelse;
	private final MipssFieldModule parentPlugin;
	private MipssPictureModel model;
	private JIncidentPicturePanel picturePanel;
	
	public JIncidentDetailPanel(final MipssFieldModule parentPlugin, final JDetailFrame owner, final HendelseSokView hendelseSok, final AbstractAction saveAction) {
		super(hendelseSok, owner, parentPlugin, saveAction);
		this.parentPlugin = parentPlugin;
		setSaveStateObserver(new SaveStatusObserver());

		if (hendelseSok == null) {
			hendelse = parentPlugin.newHendelseInstance();
			this.hendelseSok = HendelseSokView.newInstance(hendelse);
			newHendelse = true;
		} else {
			this.hendelseSok = hendelseSok;
		}

		initGui(parentPlugin, hendelseSok);
	}

	public void reloadPicturePanel(){
		for (Bilde b: hendelse.getBilder()){
			model.addBilde(b);
		}
		picturePanel.reload();
	}
	private void initGui(final MipssFieldModule parentPlugin, final HendelseSokView hendelseSok) {
		model = new MipssDefaultPictureModel();
		
		JLoadingPanel timeAndPlacePanel = createTimeAndPlacePanel();
		JLoadingPanel assessmentPanel = createAssessmentPanel();
		JLoadingPanel commentPanel = createCommentPanel();
		picturePanel = createPicturePanel();
		mapPanel = createMapPanel();
		JLoadingPanel tiltakPanel = createTiltakPanel();
		JLoadingPanel funnPanel = createPunktregPanel();
		actionsPanel = new JIncidentDetailActions(this);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		add(new JTopLine(parentPlugin.getIncidentColor()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, timeAndPlacePanel, BoxUtil.createHorizontalStrut(STRUT), assessmentPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, picturePanel, BoxUtil.createHorizontalStrut(STRUT), mapPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, commentPanel, BoxUtil.createHorizontalStrut(STRUT), tiltakPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, funnPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(actionsPanel);
		add(BoxUtil.createVerticalStrut(STRUT));

		SwingWorker<Void, Void> loadHendelse = new SwingWorker<Void, Void>() {
			/** {@inheritDoc} */
			@Override
			protected Void doInBackground() throws Exception {
				try {
					if (!newHendelse) {
						final Hendelse hendelse = parentPlugin.getMipssField().hentHendelse(hendelseSok.getGuid());
						setHendelse(hendelse);
					} else {
						setHendelse(hendelse);
					}
				} catch (Throwable t) {
					t.printStackTrace();
					logger.error("Could not fetch incident", t);
				}
				return null;
			}
		};
		loadHendelse.execute();
	}

	private JLoadingPanel createAssessmentPanel() {
		JLoadingPanel panel = new JLoadingPanel(JAssessmentPanel.class, new Class<?>[] { JIncidentDetailPanel.class,
				Hendelse.class });
		panel.setArgument(JIncidentDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JAssessmentPanel.MIN_SIZE, JAssessmentPanel.PREF_SIZE,
				JAssessmentPanel.MAX_SIZE);
		loadingPanels.add(panel);
		
		return panel;
	}

	private JLoadingPanel createCommentPanel() {
		JLoadingPanel panel = new JLoadingPanel(JCommentPanel.class, new Class<?>[] { JIncidentDetailPanel.class,
				Hendelse.class });
		panel.setArgument(JIncidentDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JCommentPanel.MIN_SIZE, JCommentPanel.PREF_SIZE,
				JCommentPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.COMMENT)));
		return panel;
	}

	/** {@inheritDoc} */
	@Override
	public AbstractAction createDefaultSaveAction() {
		return new SaveAction(GUITexts.getText(GUITexts.SAVE), IconResources.SAVE_ICON);
	}

	public JMapPanel createMapPanel() {
		JMapPanel panel = new JMapPanel(parentPlugin, this);
		panel.setBorder(BorderResources.createComponentBorder());
		ComponentSizeResources.setComponentSizes(panel, JMapPanel.MIN_SIZE, JMapPanel.PREF_SIZE, JMapPanel.MAX_SIZE);
		return panel;
	}
	private JIncidentPicturePanel createPicturePanel() {
		JIncidentPicturePanel picturePanel = new JIncidentPicturePanel(this, model);
		picturePanel.setBorder(BorderResources.createComponentBorder());
		
//		JLoadingPanel panel = new JLoadingPanel(JIncidentPicturePanel.class, new Class<?>[] {
//				JIncidentDetailPanel.class, Hendelse.class });
//		panel.setArgument(JIncidentDetailPanel.class, this);
//		panel.setBorder(BorderResources.createComponentBorder());
//		ComponentSizeResources.setComponentSizes(panel, JIncidentPicturePanel.MIN_SIZE,
//				JIncidentPicturePanel.PREF_SIZE, JIncidentPicturePanel.MAX_SIZE);
//		loadingPanels.add(panel);
		
		return picturePanel;
	}

	private JLoadingPanel createPunktregPanel() {
		JLoadingPanel panel = new JLoadingPanel(JDiscoveryPanel.class, new Class<?>[] { JIncidentDetailPanel.class, Hendelse.class });
		panel.setArgument(JIncidentDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JPunktregPanel.MIN_SIZE, JPunktregPanel.PREF_SIZE, JPunktregPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.DISCOVERIES)));
		return panel;
	}

	private JLoadingPanel createTiltakPanel() {
		JLoadingPanel panel = new JLoadingPanel(JTiltakPanel.class, new Class<?>[] { JIncidentDetailPanel.class,
				Hendelse.class });
		panel.setArgument(JIncidentDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JTiltakPanel.MIN_SIZE, JTiltakPanel.PREF_SIZE,
				JTiltakPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.ACTIONS)));
		return panel;
	}

	private JLoadingPanel createTimeAndPlacePanel() {
		JLoadingPanel panel = new JLoadingPanel(JTimeAndPlace.class, new Class<?>[] { JIncidentDetailPanel.class, Hendelse.class });
		panel.setArgument(JIncidentDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JTimeAndPlace.MIN_SIZE, JTimeAndPlace.PREF_SIZE, JTimeAndPlace.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.TIME_AND_PLACE)));
		return panel;
	}

	/** {@inheritDoc} */
	public void dispose() {
		super.dispose();

		hendelse = null;
		loadingPanels.clear();
		loadingPanels = null;
	}

	/** {@inheritDoc} */
	@Override
	public Hendelse getDetailObject() {
		return hendelse;
	}

	/** {@inheritDoc} */
	@Override
	public String getTabTitle() {
		return hendelseSok.getTextForGUI();
	}

	/** {@inheritDoc} */
	@Override
	public Integer getType() {
		return INCIDENT_TYPE;
	}

	/** {@inerhitDoc} */
	@Override
	public HendelseSokView getViewObject() {
		return hendelseSok;
	}

	/** {@inheritDoc} */
	public String getWindowTitle() {
		return hendelseSok.getTextForGUI();
	}

	public boolean isNewHendelse() {
		return newHendelse;
	}

	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
	}

	public void redrawMap() {
		mapPanel.setHendelse(hendelse);
	}

	/**
	 * Resetter GUI
	 * 
	 */
	public void reset() {
		getFieldToolkit().reset();
	}

	public void setHendelse(Hendelse hendelse) {
		getSaveStateObserver().setDetail(hendelse);
		this.hendelse = hendelse;
		mapPanel.setHendelse(hendelse);
		
		for (JLoadingPanel panel : loadingPanels) {
			panel.setArgument(Hendelse.class, hendelse);
		}
		picturePanel.setHendelse(hendelse);
		reloadPicturePanel();
		
		actionsPanel.ready();
		waitAndBindWhenReady();
		getFieldToolkit().registerCangeListener(Hendelse.class, hendelse, "bilder");
	}
	
	
	private static class AttatchAction extends AbstractAction {
		private final JIncidentDetailPanel parent;

		public AttatchAction(final JIncidentDetailPanel parent, final String name, final Icon icon) {
			super(name, icon);
			this.parent = parent;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			parent.getParentPlugin().enforceWriteAccess();
			boolean cancel = parent.saveOrCancel();
			if (cancel) {
				return;
			}

			double x, y;
			Punktstedfest sted = parent.getDetailObject().getSak().getStedfesting().get(0);
			if (sted==null){
				JOptionPane.showMessageDialog(parent.getParent(), GUITexts.getText(GUITexts.KAN_IKKE_KOBLE_HENDELSE), GUITexts.getText(GUITexts.KAN_IKKE_KOBLE_HENDELSE_TITLE), JOptionPane.WARNING_MESSAGE);
				return;
			}
			if (sted.getSnappetX() != null && sted.getSnappetY() != null) {
				x = sted.getSnappetX();
				y = sted.getSnappetY();
			} else if (sted.getX() != null && sted.getY() != null) {
				x = sted.getX();
				y = sted.getY();
			} else {
				return;
			}

			Driftkontrakt kontrakt = parent.getParentPlugin().getCommonFilterPanel().getValgtKontrakt();
			JPunktregLocatorDialog dialog = new JPunktregLocatorDialog(parent.getDetailObject(), parent.getOwner(),
					kontrakt, x, y);
			Avvik punktreg = dialog.getPunktreg();
			//TODO utkommentert pga modellendring
//			if (punktreg != null && !parent.getDetailObject().getPunktregistreringer().contains(punktreg)) {
//				punktreg.setHendelse(parent.getDetailObject());
//				parent.getDetailObject().addPunktreg(punktreg);
//				parent.getMipssField().setHendelse(punktreg, parent.getDetailObject(),
//						parent.getParentPlugin().getLoader().getLoggedOnUser(false));
//				parent.getViewObject().setAntallFunn(parent.getDetailObject().getAntallFunn());
//
//				JDiscoveryDetailPanel panel = parent.getOwner().getDetailPanel(punktreg);
//				if (panel != null) {
//					panel.getDetailObject().setHendelse(parent.getDetailObject());
//					panel.getFieldToolkit().resetValue(Avvik.class, panel.getDetailObject(), "hendelse");
//				}
//
//				MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.INFO_ICON, GUITexts
//						.getText(GUITexts.INFORMATION), GUITexts.getText(GUITexts.ADDED_AND_SAVED),
//						MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
//				dialogue.askUser();
//			} else if (punktreg != null) {
//				MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.INFO_ICON, GUITexts
//						.getText(GUITexts.INFORMATION), GUITexts.getText(GUITexts.ALREADY_EXISTS),
//						MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
//				dialogue.askUser();
//			}
		}
	}

	public static class JDiscoveryPanel extends JPunktregPanel implements PropertyChangeListener {
		private static final Dimension BUTTON_SIZE = new Dimension(50, 22);
		private static final String PUNKTREGISTRERINGER = "punktregistreringer";
		private final Box buttons = Box.createHorizontalBox();
		private final JIncidentDetailPanel parent;

		public JDiscoveryPanel(final JIncidentDetailPanel parent, final Hendelse hendelse) {
			super(parent, hendelse);
			this.parent = parent;

			initButtons();
			parent.getDetailObject().addPropertyChangeListener(PUNKTREGISTRERINGER, this);
			setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.NO_OF_DISCOVERIES_TITLE,
					getTableModel().getSize())));

			if (parent.getParentPlugin().hasWriteAccess()) {
				add(BoxUtil.createHorizontalBox(STRUT, buttons));
				add(BoxUtil.createVerticalStrut(STRUT));
			}
		}

		private void initButtons() {
			JButton remove = new JButton(GUITexts.getText(GUITexts.UNATTACTH));
			remove.setMargin(new Insets(1, 1, 1, 1));
			ComponentSizeResources.setComponentSize(remove, BUTTON_SIZE);
			remove.addActionListener(new ActionListener() {
				/** {@inheritDoc} */
				@Override
				public void actionPerformed(ActionEvent e) {
					parent.getParentPlugin().enforceWriteAccess();
					boolean cancel = parent.saveOrCancel();
					if (cancel) {
						return;
					}
					Avvik p = getTable().getSelectedEntities().get(0);
					getTable().clearSelection();

					parent.getMipssField().clearHendelse(p, parent.getParentPlugin().getLoader().getLoggedOnUser(false));
					//TODO utkommentert pga modellendring
//					parent.getDetailObject().removePunktreg(p);
//					parent.getViewObject().setAntallFunn(parent.getDetailObject().getAntallFunn());

					JDiscoveryDetailPanel panel = parent.getOwner().getDetailPanel(p);
					if (panel != null) {
						//TODO utkommentert pga modellendring
//						panel.getDetailObject().setHendelse(null);
						panel.getFieldToolkit().resetValue(Avvik.class, panel.getDetailObject(), "hendelse");
					}

					MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.INFO_ICON, GUITexts
							.getText(GUITexts.INFORMATION), GUITexts.getText(GUITexts.REMOVED_AND_SAVED),
							MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
					dialogue.askUser();
				}
			});
			Binding<JMipssBeanTable<Avvik>, Boolean, JButton, Boolean> enableRemove = BindingHelper.createbinding(
					getTable(), "${noOfSelectedRows == 1}", remove, "enabled");
			enableRemove.bind();

			List<JMenuItem> items = new ArrayList<JMenuItem>();
			items.add(new JMenuItem(new NewPunktregAction(parent, GUITexts.getText(GUITexts.NEW_DISCOVERY), null)));
			items.add(new JMenuItem(new AttatchAction(parent, GUITexts.getText(GUITexts.ATTATCH), null)));
			JPopupListMenu add = new JPopupListMenu(GUITexts.getText(GUITexts.ATTATCH), null, items);
			add.setMargin(new Insets(1, 1, 1, 1));
			ComponentSizeResources.setComponentSize(add, BUTTON_SIZE);

			buttons.add(Box.createHorizontalGlue());
			buttons.add(remove);
			buttons.add(BoxUtil.createHorizontalStrut(STRUT));
			buttons.add(add);
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getSource() == parent.getDetailObject()
					&& StringUtils.equals(PUNKTREGISTRERINGER, evt.getPropertyName())) {
				//TODO utkommentert pga modellendring
//				setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.NO_OF_DISCOVERIES_TITLE,
//						parent.getDetailObject().getPunktregistreringer().size())));
			}
		}
	}

	private static class NewPunktregAction extends AbstractAction {
		private final JIncidentDetailPanel parent;

		public NewPunktregAction(final JIncidentDetailPanel parent, final String name, final Icon icon) {
			super(name, icon);
			this.parent = parent;
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent e) {
			parent.getParentPlugin().enforceWriteAccess();
			boolean cancel = parent.saveOrCancel();
			if (cancel) {
				return;
			}

			final AbstractAction saveAction = new SaveNewPunktregAction(parent);
			parent.getOwner().addDetail((PunktregSok) null, saveAction);
			SwingWorker<Void, Void> copyWorker = new SwingWorker<Void, Void>() {
				private JDiscoveryDetailPanel panel;
				/** {@inheritDoc} */
				@Override
				protected Void doInBackground() throws Exception {
					
					panel = parent.getOwner().getDetailPanelBySaveAction(saveAction);
					while (panel.getDetailObject() == null) {
						Thread.yield();
					}
					
					Punktstedfest sted = new Punktstedfest(parent.getDetailObject().getSak().getStedfesting().get(0));
					sted.setGuid(ServerUtils.getInstance().fetchGuid());
					Punktveiref vei = new Punktveiref(parent.getDetailObject().getSak().getVeiref());
					sted.addPunktveiref(vei);
					panel.getDetailObject().setStedfesting(sted);
					panel.getDetailObject().setOpprettetDato(parent.getDetailObject().getDato());
					panel.getDetailObject().getStatus().setOpprettetDato(parent.getDetailObject().getDato());
					//TODO utkommentert pga modellendring
//					panel.getDetailObject().setHendelse(parent.getDetailObject());
					
					//TODO
					int r = JOptionPane.showConfirmDialog(parent.getParent(), GUITexts.getText(GUITexts.COPY_IMAGES_HENDELSE), GUITexts.getText(GUITexts.COPY_IMAGES_HENDELSE_TITLE), JOptionPane.YES_NO_OPTION);
					if (r==JOptionPane.YES_OPTION){
						Hendelse hendelse = parent.getDetailObject();
						for (Bilde b:hendelse.getBilder()){
							
							Bilde bilde = new Bilde();
							bilde.setGuid(ServerUtils.getInstance().fetchGuid());
							bilde.mergeValues(b);
							bilde.setKjoretoyList(null);
							
							AvvikBilde pb = new AvvikBilde();
							pb.setAvvik(panel.getDetailObject());
							pb.setStatusType(panel.getDetailObject().getStatus().getAvvikstatusType());
							pb.setBilde(bilde);
							
							panel.getDetailObject().addBilde(pb);
						}
					}
					panel.reloadPicturePanel();
					panel.redrawMap();

					return null;
				}
				@Override
				public void done(){
					
				}
			};
			
			copyWorker.execute();
		}
	}

	/**
	 * Lagrer endringer
	 * 
	 */
	private class SaveAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public SaveAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			try {
				getParentPlugin().enforceWriteAccess();
				Hendelse hendelse = getDetailObject();
				//TODO utkommentert pga modellendringer
//				if (hendelse.getErSkred()){
//					
//					hendelse = getParentPlugin().getMipssField().oppdaterHendelseSkred(hendelse, getParentPlugin().getLoader().getLoggedOnUserSign());
//				}
				HendelseResult result = getParentPlugin().getMipssField().oppdaterHendelse(hendelse, getParentPlugin().getLoader().getLoggedOnUserSign());
				
				hendelse.merge(result.getHendelse());
				getViewObject().merge(result.getHendelseSokView());
				reset();

				if (isNewHendelse() && !getParentPlugin().getIncidentTableModel().contains(hendelseSok)) {
					getParentPlugin().getIncidentTable().clearSelection();
					getParentPlugin().getIncidentTableModel().addItem(result.getHendelseSokView());
				}
			} catch (Throwable t) {
				getParentPlugin().getLoader().handleException(t, getParentPlugin(),
						GUITexts.getText(GUITexts.SAVING_FAILED));
			}
		}
	}

	/**
	 * Brukes når man skal lagre et nytt funn ved tilkobling til hendelse
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</A>
	 */
	private static class SaveNewPunktregAction extends AbstractAction {
		private final JIncidentDetailPanel parent;

		public SaveNewPunktregAction(final JIncidentDetailPanel parent) {
			super(GUITexts.getText(GUITexts.SAVE), IconResources.SAVE_ICON);
			this.parent = parent;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			parent.getParentPlugin().enforceWriteAccess();
			JDiscoveryDetailPanel panel = parent.getOwner().getDetailPanelBySaveAction(this);
			AbstractAction save = panel.createDefaultSaveAction();
			save.actionPerformed(e);
			//TODO utkommentert pga modellendring
//			if (!parent.getDetailObject().getPunktregistreringer().contains(panel.getDetailObject())) {
//				parent.getDetailObject().addPunktreg(panel.getDetailObject());
//				parent.getViewObject().setAntallFunn(parent.getDetailObject().getAntallFunn());
//			}
		}
	}
}
