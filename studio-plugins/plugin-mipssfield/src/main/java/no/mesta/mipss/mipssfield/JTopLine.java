package no.mesta.mipss.mipssfield;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

import no.mesta.mipss.ui.ComponentSizeResources;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JTopLine extends JPanel {

	public JTopLine(Color bg) {
		Dimension min = new Dimension(0,4);
		Dimension max = new Dimension(Short.MAX_VALUE, 4);
		
		setBackground(bg);
		
		ComponentSizeResources.setComponentSizes(this, min, min, max);
	}
}
