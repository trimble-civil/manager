package no.mesta.mipss.mipssfield.elrapp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.elrapp.ElrappService;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.HendelseQueryFilter;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.incident.JIncidentActions;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.MipssProgressDialog;
import no.mesta.mipss.ui.Sphere;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTableCellRenderer;
import no.mesta.mipss.ui.table.ElrappStatusTableCellRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.valueobjects.ElrappStatus;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.error.ErrorInfo;

@SuppressWarnings("serial")
public class ElrappPanel extends JPanel implements MipssBeanLoaderListener{

	private final MipssFieldModule parentPlugin;

	private JMipssBeanTable<HendelseSokView> elrappTable;
	private MipssBeanTableModel<HendelseSokView> elrappTableModel;
	private JScrollPane elrappScroll;
	private JButton sendElrappButton;
	private JButton sokButton;
	private HendelseQueryFilter filter;
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private JXPanel busyPanel;
	
	public ElrappPanel(MipssFieldModule parentPlugin){
		this.parentPlugin = parentPlugin;
		filter = parentPlugin.getIncidentFilter();
		
		parentPlugin.addElrappSendtListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				elrappTableModel.loadData();
			}
		});
		initGui();
		
		
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		createTable();
		
		busyLabel.setText(GUITexts.getText(GUITexts.HENTER_DATA));
		busyPanel = new JXPanel(new BorderLayout());
		busyPanel.add(busyLabel);
		elrappScroll = new JScrollPane(elrappTable);
		
		sendElrappButton = new JButton(getSendElrappAction());
		sokButton = new JButton(getSokAction());
		JPanel legendPanel = createLegend();
		

		BindingHelper.createbinding(elrappTable, "${noOfSelectedRows > 0}", sendElrappButton, "enabled").bind();
		BindingHelper.createbinding(filter, "${kontraktId != null}", sokButton, "enabled").bind();
		
		add(sokButton, 		  new GridBagConstraints(0,0, 2, 1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,10,10), 0,0));
		add(elrappScroll, 	  new GridBagConstraints(0,1, 2, 1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(legendPanel, 	  new GridBagConstraints(0,2, 1, 1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10,0,10,10), 0,0));
		add(sendElrappButton, new GridBagConstraints(1,2, 1, 1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,10,10), 0,0));
	}
	
	
	private JPanel createLegend(){
		Sphere red = new Sphere(Color.red, 20);
		Sphere yellow = new Sphere(Color.yellow, 20);
		Sphere green = new Sphere(Color.green, 20);
		
		JLabel ikkeSendt = new JLabel("Ikke sendt", red.getIcon(), JLabel.LEFT);
		ikkeSendt.setToolTipText(GUITexts.getText(GUITexts.ELRAPP_IKKESENDT));
		JLabel endret = new JLabel("Endret", yellow.getIcon(), JLabel.LEFT);
		endret.setToolTipText(GUITexts.getText(GUITexts.ELRAPP_ENDRET));
		JLabel rappOk = new JLabel("Rapportert Ok", green.getIcon(), JLabel.LEFT);
		rappOk.setToolTipText(GUITexts.getText(GUITexts.ELRAPP_OK));
		
		JPanel panel = new JPanel();
		panel.add(ikkeSendt);
		panel.add(endret);
		panel.add(rappOk);
		return panel;
	}
	private Action getSokAction() {
		return new AbstractAction(GUITexts.getText(GUITexts.SEARCH), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				elrappTableModel.setBeanMethodArgValues(new Object[] { filter });
				elrappTableModel.loadData();
			}
		};
	}

	private Action getSendElrappAction() {
		return new AbstractAction(GUITexts.getText(GUITexts.SEND_ELRAPP), IconResources.ELRAPP_ICON_16) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				sendElrappButton.setEnabled(false);
				final HendelseSokView h = elrappTable.getSelectedEntities().get(0);
				String d = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.field", "elrappRapporteringsGrenseDato").getVerdi();
				SimpleDateFormat f = new SimpleDateFormat(MipssDateFormatter.DATE_TIME_FORMAT);
				Date notBeforeDate=null;
				try {
					notBeforeDate = f.parse(d);
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				
				if (h.getOpprettetDato().before(notBeforeDate)){
					JOptionPane.showMessageDialog(parentPlugin.getLoader().getApplicationFrame(), GUITexts.getText(GUITexts.ELRAPP_GRENSE_DATO_WARNING));
					sendElrappButton.setEnabled(true);
					return;
				}
				
				final Bruker bruker = parentPlugin.getLoader().getLoggedOnUser(false);
				String msg = JIncidentActions.checkContractReadyForElrapp(parentPlugin.getCommonFilterPanel().getValgtKontrakt(), bruker);
				if (msg!=null){
					JOptionPane.showMessageDialog(parentPlugin.getLoader().getApplicationFrame(), msg, "Kan ikke sende data til Elrapp", JOptionPane.ERROR_MESSAGE);
					sendElrappButton.setEnabled(true);
					return;
				}
				final MipssProgressDialog progress = new MipssProgressDialog(GUITexts.getText(GUITexts.SEND_ELRAPP_MESSAGE), parentPlugin.getLoader().getApplicationFrame());
				progress.setVisible(true);
//				new SwingWorker(){
//					@Override
//					public Void doInBackground(){
//						
//						try {
//							ElrappService b = BeanUtil.lookup(ElrappService.BEAN_NAME, ElrappService.class);
//							
//							ElrappResponse res = b.sendRSchema(h, bruker);
//							progress.dispose();
//							if (res!=null){
//								Long elrappDokumentId = res.getElrappDokumentId();
//								Long elrappVersjon = res.getElrappVersjon();
//								String message = GUITexts.getText(GUITexts.SEND_ELRAPP_MESSAGE_OK, new Object[]{res.getRtype(), elrappDokumentId, elrappVersjon});
//								String title = GUITexts.getText(GUITexts.SEND_ELRAPP_MESSAGE_OK_TITLE, new Object[]{res.getRtype()});
//								JOptionPane.showMessageDialog(parentPlugin.getLoader().getApplicationFrame(), message, title, JOptionPane.INFORMATION_MESSAGE);
//							}
//						} catch (Throwable t){
//							progress.dispose();
//							ErrorInfo info = new ErrorInfo("Feil ved sending til elrapp", "Skjema ble IKKE sendt\n"+t.getMessage(), null, "Feil", t, Level.SEVERE, null);
//							t.printStackTrace();
//						    JXErrorPane.showDialog(null, info);
//						} finally{
//							sendElrappButton.setEnabled(true);
//						}
//						return null;
//					}
//					@Override
//					public void done(){
//						parentPlugin.fireElrappSendtEvent();
//					}
//				}.execute();

			}
		};
	}

	private void createTable(){
		elrappTableModel = new MipssBeanTableModel<HendelseSokView>(HendelseSokView.class, 
				"elrappStatus", "refnummer", "dato", "hpGuiText", "elrappDokumentIdent", "elrappVersjon", "rString", "rapportertDato");
		
		elrappTableModel.setBeanInstance(parentPlugin.getMipssField());
		elrappTableModel.setBeanMethod("sokHendelser");
		elrappTableModel.setBeanInterface(MipssField.class);
		elrappTableModel.setBeanMethodArguments(new Class<?>[] { HendelseQueryFilter.class });
		elrappTableModel.addTableLoaderListener(this);
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(HendelseSokView.class, elrappTableModel.getColumns());
		elrappTable = new JMipssBeanTable<HendelseSokView>(elrappTableModel, colModel);
		elrappTable.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		elrappTable.setDefaultRenderer(ElrappStatus.class, new ElrappStatusTableCellRenderer());
		elrappTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		elrappTable.setFillsViewportWidth(true);
		elrappTable.setDoWidthHacks(false);
		elrappTable.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount()==2){
					List<HendelseSokView> entities = elrappTable.getSelectedEntities();
					parentPlugin.openIncident(entities);
				}
			}
		});
		elrappTable.getColumn(0).setPreferredWidth(5);
		elrappTable.getColumn(1).setPreferredWidth(10);

	}

	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		busyLabel.setBusy(false);
		elrappScroll.setViewportView(elrappTable);
	}

	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		busyLabel.setBusy(true);
		elrappScroll.setViewportView(busyLabel);
	}
	

}
