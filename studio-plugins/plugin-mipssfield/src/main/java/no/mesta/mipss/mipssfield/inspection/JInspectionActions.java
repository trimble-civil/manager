package no.mesta.mipss.mipssfield.inspection;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolTip;
import javax.swing.ToolTipManager;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

/**
 * Handlingsknapper for inspeksjoner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JInspectionActions extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(JInspectionActions.class);
	private MipssFieldModule parentPlugin;
	private BindingGroup group = new BindingGroup();
	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	@SuppressWarnings("unchecked")
	public JInspectionActions(MipssFieldModule parentPlugin) {
		logger.debug("JInspectionActions() start");
		this.parentPlugin = parentPlugin;
		List<JMenuItem> items = new ArrayList<JMenuItem>();

		JButton openButton = new JButton(new OpenAction(GUITexts.getText(GUITexts.DETAILS), null));
		JButton mapButton = new JButton(new MapAction(GUITexts.getText(GUITexts.MAP), IconResources.NORGE_ICON));
		mapButton.setEnabled(false);
		JMenuItem overviewButton = new JMenuItem(new OverviewXLSAction(GUITexts.getText(GUITexts.INSPECTION_OVERVIEW_REPORT), IconResources.EXCEL_ICON16));
		JButton overviewPDFButton = new JButton(new OverviewPDFAction(GUITexts.getText(GUITexts.INSPECTION_REPORT), IconResources.ADOBE_16));
//		JMenuItem overviewPDFButton = new JMenuItem(new OverviewPDFAction(GUITexts.getText(GUITexts.INSPECTION_REPORT), IconResources.ADOBE_16)){
//			public JToolTip createToolTip() {
//				ToolTipManager.sharedInstance().setDismissDelay(20000);
//				return super.createToolTip();
//			}
//		};
		overviewPDFButton.setToolTipText(GUITexts.getText(GUITexts.TOOLTIP_OVERVIEWPDF));
		
		JMenuItem hpReport = new JMenuItem(new HPReportAction(GUITexts.getText(GUITexts.HP_REPORT), IconResources.EXCEL_ICON16));
		JButton excelButton = new JButton(new ExcelAction("Excel", IconResources.EXCEL_ICON16));
		JButton slettButton = new JButton(new DeleteAction("Slett", IconResources.TRASH_ICON));
		JButton gjenopprettButton = new JButton(new RestoreAction("Gjenopprett", IconResources.UNTRASH_ICON));
//		items.add(overviewPDFButton);
		items.add(overviewButton);
		items.add(hpReport);
		JPopupListMenu reportsPopup = new JPopupListMenu("Rapporter", IconResources.PRINT_ICON, items);

		
		Binding detailsBinding = BindingHelper.createbinding(parentPlugin.getInspectionTable(),
				"${noOfSelectedRows == 1}", openButton, "enabled");
		Binding mapBinding = BindingHelper.createbinding(parentPlugin.getInspectionTable(), "${noOfSelectedRows > 0}", mapButton, "enabled");
		Binding hpBinding = BindingHelper.createbinding(parentPlugin.getInspectionTableModel(), "${size > 0}",
				hpReport, "enabled");
		Binding overviewBinding = BindingHelper.createbinding(parentPlugin.getInspectionTableModel(), "${size > 0}",
				overviewButton, "enabled");
		Binding overviewPDFBinding = BindingHelper.createbinding(parentPlugin.getCommonFilterPanel(), "${valgtKontrakt != null}",overviewPDFButton, "enabled");
		Binding excelBinding = BindingHelper.createbinding(parentPlugin.getInspectionTableModel(), "${size > 0}",
				excelButton, "enabled");
		Binding popupBinding = BindingHelper.createbinding(parentPlugin.getInspectionTableModel(), "${size > 0}",
				reportsPopup, "enabled");
		Binding slettBinding = BindingHelper.createbinding(parentPlugin.getInspectionFilter(),
				"${kunSlettede != true}", slettButton, "visible");
		Binding slettEnable = BindingHelper.createbinding(parentPlugin.getInspectionTable(), "${noOfSelectedRows > 0}",
				slettButton, "enabled");
		Binding gjenopprettBinding = BindingHelper.createbinding(parentPlugin.getInspectionFilter(),
				"${kunSlettede == true}", gjenopprettButton, "visible");
		Binding gjenopprettEnable = BindingHelper.createbinding(parentPlugin.getInspectionTable(),
				"${noOfSelectedRows > 0}", gjenopprettButton, "enabled");

		group.addBinding(detailsBinding);
		group.addBinding(mapBinding);
		group.addBinding(hpBinding);
		group.addBinding(overviewBinding);
		group.addBinding(overviewPDFBinding);
		group.addBinding(excelBinding);
		group.addBinding(popupBinding);
		group.addBinding(slettBinding);
		group.addBinding(slettEnable);
		group.addBinding(gjenopprettBinding);
		group.addBinding(gjenopprettEnable);
		logger.debug("JInspectionActions() bind start");
		group.bind();
		logger.debug("JInspectionActions() bind end");

		BoxLayout layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
		setLayout(layout);
		add(Box.createHorizontalStrut(5));
		if (parentPlugin.hasWriteAccess()) {
			add(slettButton);
			add(gjenopprettButton);
		}
		add(Box.createHorizontalGlue());
		add(overviewPDFButton);
		add(Box.createHorizontalStrut(10));
		add(excelButton);
		add(Box.createHorizontalStrut(10));
		add(reportsPopup);
		add(Box.createHorizontalStrut(10));
		add(mapButton);
		add(Box.createHorizontalStrut(10));
		add(openButton);

		Dimension min = new Dimension(200, 24);
		Dimension pref = new Dimension(939, 24);
		Dimension max = new Dimension(Short.MAX_VALUE, 24);
		ComponentSizeResources.setComponentSizes(this, min, pref, max);
		logger.debug("JInspectionActions() end");
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		group.unbind();
		group = null;
		parentPlugin = null;
	}
	class DeleteAction extends AbstractAction {

		public DeleteAction(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<InspeksjonSokView> selection = parentPlugin.getInspectionTable().getSelectedEntities();

			MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
					IconResources.QUESTION_ICON, GUITexts.getText(GUITexts.DELETE_INSPECTIONS_TITLE), GUITexts.getText(
							GUITexts.DELETE_INSPECTIONS_TXT, selection.size()), MipssDialogue.Type.QUESTION,
					new MipssDialogue.Button[] { MipssDialogue.Button.YES, MipssDialogue.Button.NO,
							MipssDialogue.Button.CANCEL });
			dialogue.askUser();

			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL
					|| dialogue.getPressedButton() == MipssDialogue.Button.NOTHING) {
				return;
			}

			boolean deleteOtherData = dialogue.getPressedButton() == MipssDialogue.Button.YES;

			parentPlugin.deleteInspections(selection, deleteOtherData);
		}
	}

	class ExcelAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public ExcelAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			JMipssBeanTable<InspeksjonSokView> table = parentPlugin.getInspectionTable();
			try {
				parentPlugin.openExcel(table);
			} catch (Throwable t) {
				parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
			}
		}
	}

	/**
	 * Action klasse for å åpne inspeksjoner
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class HPReportAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public HPReportAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<InspeksjonSokView> selection = parentPlugin.getInspectionTable().getSelectedEntities();
			List<InspeksjonSokView> inspections = null;
			if (selection != null && !selection.isEmpty()) {
				inspections = selection;
			} else {
				MipssBeanTableModel<InspeksjonSokView> model = parentPlugin.getInspectionTableModel();
				inspections = model.getEntities();
			}
			if (inspections != null && !inspections.isEmpty()) {
				try {
					parentPlugin.viewInspectedHPs(inspections);
				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}

	/**
	 * Action klasse for å åpne inspeksjoner i kart
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class MapAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public MapAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<InspeksjonSokView> selection = parentPlugin.getInspectionTable().getSelectedEntities();
			if (selection != null) {
				parentPlugin.showInspectionsInMap(selection);
			}
		}
	}

	/**
	 * Action klasse for å åpne inspeksjon
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class OpenAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public OpenAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<InspeksjonSokView> selection = parentPlugin.getInspectionTable().getSelectedEntities();

			if (selection != null) {
				parentPlugin.openInspections(selection);
			}
		}
	}
	/**
	 * Action klasse for å åpne inspeksjoner
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class OverviewPDFAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public OverviewPDFAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			try {
				parentPlugin.viewInspectionReportPDF();
			} catch (Throwable t) {
				parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
			}
		}
	}
	/**
	 * Action klasse for å åpne inspeksjoner
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class OverviewXLSAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public OverviewXLSAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<InspeksjonSokView> selection = parentPlugin.getInspectionTable().getSelectedEntities();
			List<InspeksjonSokView> inspections = null;
			if (selection != null && !selection.isEmpty()) {
				inspections = selection;
			} else {
				MipssBeanTableModel<InspeksjonSokView> model = parentPlugin.getInspectionTableModel();
				inspections = model.getEntities();
			}
			if (inspections != null && !inspections.isEmpty()) {
				try {
					parentPlugin.viewInspectionOverviewXLS(inspections);
				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}

	class RestoreAction extends AbstractAction {

		public RestoreAction(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<InspeksjonSokView> selection = parentPlugin.getInspectionTable().getSelectedEntities();

			MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
					IconResources.INFO_ICON, GUITexts.getText(GUITexts.RESTORE_INSPECTIONS_TITLE), GUITexts.getText(
							GUITexts.RESTORE_INSPECTIONS_TXT, selection.size()), MipssDialogue.Type.INFO,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();

			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL
					|| dialogue.getPressedButton() == MipssDialogue.Button.NOTHING) {
				return;
			}

			parentPlugin.restoreInspections(selection);
		}
	}
}
