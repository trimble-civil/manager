package no.mesta.mipss.mipssfield;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;

import no.mesta.mipss.core.IBlankFieldValidator;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JNoBlankLabel;

/**
 * Lager en line med to komponenter
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class LineBox<T> extends Box {
	private static int STRUT = 2;
	private JComponent component;
	private JLabel label;
	private String labelTxt;

	public LineBox(String labelTxt, JComponent component, IBlankFieldValidator<T> validator) {
		super(BoxLayout.X_AXIS);
		if (validator == null) {
			label = new JLabel(labelTxt);
		} else {
			label = new JNoBlankLabel<T>(labelTxt, validator);
		}

		this.labelTxt = labelTxt;
		this.component = component;
		layoutComponents();
	}

	@SuppressWarnings("unchecked")
	public IBlankFieldValidator<T> getValidator() {
		if(label instanceof JNoBlankLabel) {
			return ((JNoBlankLabel<T>) label).getValidator();
		} else {
			return null;
		}
	}

	private void layoutComponents() {
		ComponentSizeResources.setComponentSizes(component);
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setHorizontalTextPosition(JLabel.RIGHT);
		label.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
		ComponentSizeResources.setComponentSize(label, new Dimension(66, 20));

		add(label);
		add(BoxUtil.createHorizontalStrut(STRUT));
		add(component);

		Dimension min = new Dimension(59 + STRUT + component.getMinimumSize().width, component.getMinimumSize().height);
		int prefWidth = Math.max(Short.MAX_VALUE, 59 + STRUT + component.getPreferredSize().width);
		Dimension pref = new Dimension(prefWidth, component.getPreferredSize().height);
		int maxWidth = Math.max(Short.MAX_VALUE, 59 + STRUT + component.getMaximumSize().width);
		Dimension max = new Dimension(maxWidth, component.getMaximumSize().height);
		ComponentSizeResources.setComponentSizes(this, min, pref, max);
	}
	
	public void setValidator(IBlankFieldValidator<T> validator) {
		label = new JNoBlankLabel<T>(labelTxt, validator);
		removeAll();
		layoutComponents();
	}
}