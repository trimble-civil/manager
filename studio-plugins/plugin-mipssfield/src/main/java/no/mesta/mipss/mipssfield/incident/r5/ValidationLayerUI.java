package no.mesta.mipss.mipssfield.incident.r5;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RadialGradientPaint;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JComponent;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;

public class ValidationLayerUI extends AbstractLayerUI<JComponent>{
	
	private String validationMethod;
	private final Object target;
	private final Class<?> targetClass;
	public ValidationLayerUI(String validationMethod, Object target){
		this.validationMethod = validationMethod;
		this.target = target;
		this.targetClass = target.getClass();
	}
	
	@Override  
    protected void paintLayer(Graphics2D g2, JXLayer<JComponent> l) {
        super.paintLayer(g2, l);
        if (!isContentValid()){
        	g2.setPaint(new RadialGradientPaint(l.getWidth()/2,l.getHeight()/2,l.getWidth()/2,new float[]{0,1}, new Color[]{new Color(1f,1f,1f,0f), new Color(1f, 0f, 0f, 0.5f)}));
        	g2.fillRect(0, 0, l.getWidth(), l.getHeight());
        }
    }
	private boolean isContentValid(){
		
		boolean valid = true;
		try {
			Method m = targetClass.getMethod(validationMethod, (Class[])null);
			m.setAccessible(true);
			Boolean result = (Boolean)m.invoke(target, (Object[])null);
			valid = result;
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return valid;
	}
}
	
	
