package no.mesta.mipss.mipssfield;

import java.io.File;
import java.io.FilenameFilter;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.lang.StringUtils;

/**
 * filnavn filter for filvelgere
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 *
 */
public class ImageNameFilter extends FileFilter implements FilenameFilter {
	
	@Override
	public boolean accept(File dir, String name) {
		if (StringUtils.endsWithIgnoreCase(name, ".png") || StringUtils.endsWithIgnoreCase(name, ".jpe")
				|| StringUtils.endsWithIgnoreCase(name, ".jpg") || StringUtils.endsWithIgnoreCase(name, ".bmp")
				|| StringUtils.endsWithIgnoreCase(name, ".gif") || StringUtils.endsWithIgnoreCase(name, ".jpeg")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean accept(File f) {
		if(f != null && f.isFile()) {
			return accept(f.getParentFile(), f.getName());
		} else {
			return true;
		}
	}

	@Override
	public String getDescription() {
		return "Bilder";
	}

}
