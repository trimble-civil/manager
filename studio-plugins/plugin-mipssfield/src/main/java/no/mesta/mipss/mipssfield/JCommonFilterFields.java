package no.mesta.mipss.mipssfield;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.JPDAPanel;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.roadpicker.JRoadSelectDialog;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Converter;

import com.lowagie.text.Font;

/**
 * Kontrakt og dato
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JCommonFilterFields extends JPanel implements MipssBeanLoaderListener, PropertyChangeListener, ItemListener {
	private static final String FIELD_NAME_DATEFIELD = "date";
	private static final String FIELD_NAME_FROMDATE = "fromDate";
	private static final String FIELD_NAME_KONTRAKT = "valgtKontrakt";
	private static final String FIELD_NAME_TODATE = "toDate";
	private static final String FIELD_NAME_VEIBUTTONCOLOR = "veiButtonColor";
	private static final String FIELD_NAME_VEIBUTTONTEXT = "veiButtonText";
	private static final String FIELD_NAME_VEIINFO = "veiInfo";
	private static final Logger logger = LoggerFactory.getLogger(JCommonFilterFields.class);
	private static final int STRUT = 5;
	private JMipssContractPicker contractPanel;
	private boolean enabled;
	private Date fromDate;
	private JMipssDatePicker fromDatePicker;
	private BindingGroup group = new BindingGroup();
	private JCheckBox openFromDate;
	private MipssFieldModule parentPlugin;
	private JButton pdaButton;
	private JPDAPanel pdaPanel;
	private Date toDate;
	private JMipssDatePicker toDatePicker;
	private JButton veiButton;
	private VeiInfo veiInfo;

	public JCommonFilterFields(final MipssFieldModule parentPlugin) {
		logger.debug("JCommonFilterFields() start");
		this.parentPlugin = parentPlugin;
		initComponents();
		initGUI();
		bindComponents();

		// Hack
		setDates();
		logger.debug("JCommonFilterFields() end");
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		parentPlugin = null;
		group.unbind();
		group = null;
		for (PropertyChangeListener l:contractPanel.getPropertyChangeListeners()){
			contractPanel.removePropertyChangeListener(l);
		}
		contractPanel.removePropertyChangeListener(this);
	}
	private void bindComponents() {
		logger.debug("bindComponents() start");
		final String enabledField = "enabled";
		Binding<JMipssContractPicker, Boolean, JCommonFilterFields, Boolean> enableThis = BindingHelper.createbinding(
				contractPanel, "${valgtKontrakt != null}", this, enabledField);
		group.addBinding(enableThis);

		Binding<JCheckBox, Boolean, JMipssDatePicker, Boolean> enableFromDate = BindingHelper.createbinding(
				openFromDate, "selected", fromDatePicker, enabledField);
		group.addBinding(enableFromDate);

		Binding<JCommonFilterFields, Date, MipssFieldModule, Date> filterDiscovertyToDate = BindingHelper
				.createbinding(this, FIELD_NAME_TODATE, parentPlugin, "${discoveryFilter.tilDato}");
		Binding<JCommonFilterFields, Date, MipssFieldModule, Date> filterInspectionToDate = BindingHelper
				.createbinding(this, FIELD_NAME_TODATE, parentPlugin, "${inspectionFilter.tilDato}");
		Binding<JCommonFilterFields, Date, MipssFieldModule, Date> filterIncidentToDate = BindingHelper
				.createbinding(this, FIELD_NAME_TODATE, parentPlugin, "${incidentFilter.tilDato}");
		group.addBinding(filterDiscovertyToDate);
		group.addBinding(filterInspectionToDate);
		group.addBinding(filterIncidentToDate);

		Binding<JCommonFilterFields, Date, MipssFieldModule, Date> filterDiscovertyFromDate = BindingHelper
				.createbinding(this, FIELD_NAME_FROMDATE, parentPlugin, "${discoveryFilter.fraDato}");
		Binding<JCommonFilterFields, Date, MipssFieldModule, Date> filterInspectionFromDate = BindingHelper
				.createbinding(this, FIELD_NAME_FROMDATE, parentPlugin, "${inspectionFilter.fraDato}");
		Binding<JCommonFilterFields, Date, MipssFieldModule, Date> filterIncidentFromDate = BindingHelper
				.createbinding(this, FIELD_NAME_FROMDATE, parentPlugin, "${incidentFilter.fraDato}");
		group.addBinding(filterDiscovertyFromDate);
		group.addBinding(filterInspectionFromDate);
		group.addBinding(filterIncidentFromDate);

		final String valgtKontrakt = "${valgtKontrakt != null ? valgtKontrakt.id : null}";
		Binding<JCommonFilterFields, Long, MipssFieldModule, Long> filterDiscovertyContract = BindingHelper.createbinding(this, valgtKontrakt, parentPlugin, "${discoveryFilter.kontraktId}");
		Binding<JCommonFilterFields, Long, MipssFieldModule, Long> filterInspectionContract = BindingHelper
				.createbinding(this, valgtKontrakt, parentPlugin, "${inspectionFilter.kontraktId}");
		Binding<JCommonFilterFields, Long, MipssFieldModule, Long> filterIncidentContract = BindingHelper
				.createbinding(this, valgtKontrakt, parentPlugin, "${incidentFilter.kontraktId}");
		group.addBinding(filterDiscovertyContract);
		group.addBinding(filterInspectionContract);
		group.addBinding(filterIncidentContract);

		Binding<JCommonFilterFields, String, JButton, String> buttonTextBinding = BindingHelper.createbinding(this,
				FIELD_NAME_VEIBUTTONTEXT, veiButton, "text");
		group.addBinding(buttonTextBinding);

		Binding<JCommonFilterFields, Color, JButton, Color> buttonColor = BindingHelper.createbinding(this,
				FIELD_NAME_VEIBUTTONCOLOR, veiButton, "foreground");
		group.addBinding(buttonColor);

		Binding<JMipssContractPicker, Driftkontrakt, JPDAPanel, Driftkontrakt> pdaKontrakt = BindingHelper
				.createbinding(contractPanel, "valgtKontrakt", pdaPanel, "kontrakt");
		group.addBinding(pdaKontrakt);

		final String alleValgte = "alleValgte";
		Binding<JPDAPanel, Long[], MipssFieldModule, Long[]> filterDiscovertyPDA = BindingHelper.createbinding(pdaPanel,
				alleValgte, parentPlugin, "${discoveryFilter.dfuer}", BindingHelper.READ_WRITE);
		Binding<JPDAPanel, Long[], MipssFieldModule, Long[]> filterIncidentPDA = BindingHelper.createbinding(pdaPanel,
				alleValgte, parentPlugin, "${inspectionFilter.dfuer}", BindingHelper.READ_WRITE);
		Binding<JPDAPanel, Long[], MipssFieldModule, Long[]> filterInspectionPDA = BindingHelper.createbinding(
				pdaPanel, alleValgte, parentPlugin, "${incidentFilter.dfuer}", BindingHelper.READ_WRITE);
		group.addBinding(filterDiscovertyPDA);
		group.addBinding(filterIncidentPDA);
		group.addBinding(filterInspectionPDA);

		Binding<JPDAPanel, Boolean, JButton, Color> pdaButtonColor = BindingHelper.createbinding(pdaPanel, "set",
				pdaButton, "foreground");
		Converter<Boolean, Color> converter = new Converter<Boolean, Color>() {
			@Override
			public Color convertForward(Boolean b) {
				return b != null && b == true ? Color.RED : Color.BLACK;
			}

			@Override
			public Boolean convertReverse(Color c) {
				return c == Color.RED ? true : false;
			}
		};
		pdaButtonColor.setConverter(converter);

		group.addBinding(pdaButtonColor);

		group.bind();
		openFromDate.setSelected(true);
		fromDatePicker.setEnabled(false);
		
		logger.debug("bindComponents() end");
	}

	private void bindVeiGroup(VeiInfo vei) {
		if (vei != null) {
			vei.addPropertyChangeListener(this);
		}
	}

	private void clearDfuFilter() {
		pdaPanel.setAlleValgte(null);
		pdaPanel.setIngenValgt(false);
		pdaPanel.setAlleValgt(false);
	}

//	/** {@inheritDoc} */
//	@Override
//	protected void finalize() throws Throwable {
////		Throwable err = null;
////		try {
////			group.unbind();
////		} catch (Throwable t) {
////			t = null;
////		}
////		try {
//			super.finalize();
////		} catch (Throwable t) {
////			err = t;
////		}
//
//
////		if (err != null) {
////			throw err;
////		}
//	}

	public boolean getEnabled() {
		return enabled;
	}

	public Date getFromDate() {
		if (openFromDate.isSelected()) {
			logger.debug("getFromDate() return " + fromDate);
			return fromDate;
		} else {
			logger.debug("getFromDate() return null");
			return null;
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public Driftkontrakt getValgtKontrakt() {
		return contractPanel.getValgtKontrakt();
	}

	public Color getVeiButtonColor() {
		Color color = null;
		if (veiInfo != null && !StringUtils.isBlank(StringUtils.trimToEmpty(veiInfo.toString()))) {
			color = Color.RED;
		} else {
			color = Color.BLACK;
		}

		return color;
	}

	public String getVeiButtonText() {
		String stdTxt = GUITexts.getText(GUITexts.SEARCH_ROAD);
		if (veiInfo != null) {
			String veiTxt = veiInfo.toString();
			veiTxt = veiTxt.substring(0, veiTxt.indexOf("m0"));
			return StringUtils.isBlank(StringUtils.trimToEmpty(veiTxt)) ? stdTxt : veiTxt;
		} else {
			return stdTxt;
		}
	}

	public VeiInfo getVeiInfo() {
		return veiInfo;
	}

	private void initComponents() {
		logger.debug("initComponents() start");
		contractPanel = new JMipssContractPicker(parentPlugin, false);
		contractPanel.addPropertyChangeListener(FIELD_NAME_KONTRAKT, this);
		pdaPanel = new JPDAPanel();
		pdaButton = new JButton("PDA", IconResources.PDA_ICON);
		pdaButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Dialog dialog = new Dialog(parentPlugin.getLoader().getApplicationFrame(), pdaPanel);
				dialog.setLocationRelativeTo(JCommonFilterFields.this);
				dialog.pack();
				dialog.setVisible(true);
			}

		});
		pdaButton.addPropertyChangeListener("foreground", this);
		Date noSearchBefore = parentPlugin.getNoSearchBefore();

		fromDatePicker = new JMipssDatePicker();
		fromDatePicker.setDate(parentPlugin.getDefaultFromDate());
		fromDatePicker.setNotBeforeDate(noSearchBefore);
		toDatePicker = new JMipssDatePicker(Clock.now());
		toDatePicker.setNotBeforeSource(fromDatePicker, FIELD_NAME_DATEFIELD);
		fromDatePicker.addPropertyChangeListener(FIELD_NAME_DATEFIELD, this);
		toDatePicker.addPropertyChangeListener(FIELD_NAME_DATEFIELD, this);
		
		fromDatePicker.pairWith(toDatePicker);
		openFromDate = new JCheckBox();
		openFromDate.addItemListener(this);
		
		veiButton = new JButton(getVeiButtonText(), IconResources.VEI_ICON);
		veiButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				logger.debug("actionPerformed(" + e + ") start");
				
				JRoadSelectDialog dialog = new JRoadSelectDialog(parentPlugin.getLoader().getApplicationFrame(), false,
						false);
				
				BindingHelper.createbinding(contractPanel, "valgtKontrakt", dialog, "driftkontrakt").bind();
				dialog.setVeiSok(new VeiInfo(veiInfo));
				dialog.setVisible(true);
				
				if (dialog.isSet()) {
					VeiInfo old = veiInfo;
					String oldVeiText = veiButton.getText();
					if (dialog.getVeiSok().isBlank()){
						unbindVeiGroup(veiInfo);
						veiInfo = null;
					}else{
						
						
						veiInfo = dialog.getVeiSok();
						unbindVeiGroup(old);
						bindVeiGroup(veiInfo);
					}
					firePropertyChange(FIELD_NAME_VEIINFO, old, veiInfo);
					firePropertyChange(FIELD_NAME_VEIBUTTONTEXT, oldVeiText, getVeiButtonText());
					firePropertyChange(FIELD_NAME_VEIBUTTONCOLOR, null, getVeiButtonColor());
				
				}
			}
		});
		veiButton.addPropertyChangeListener("foreground", this);
		logger.debug("initComponents() end");
	}

	private void initGUI() {
		logger.debug("initGUI() start");
		Box fromDateBox = BoxUtil.createHorizontalBox(0, openFromDate, new JLabel(GUITexts
				.getText(GUITexts.FROM)), BoxUtil.createHorizontalStrut(5), fromDatePicker);
		Box toDateBox = BoxUtil.createHorizontalBox(0, new JLabel(GUITexts.getText(GUITexts.TO)),
				BoxUtil.createHorizontalStrut(5), toDatePicker);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, contractPanel, BoxUtil.createHorizontalStrut(STRUT),veiButton, pdaButton, Box.createHorizontalGlue(),
				fromDateBox, toDateBox));
		add(BoxUtil.createVerticalStrut(STRUT));
		logger.debug("initGUI() end");
	}

	public boolean isIngenDfuerValgt() {
		return pdaPanel.isIngenValgt();
	}

	/** {@inheritDoc} */
	@Override
	public void itemStateChanged(ItemEvent e) {
		Object src = e.getItemSelectable();
		if (src == openFromDate) {
			logger.debug("enable/disable fromDate");
			boolean on = e.getStateChange() == ItemEvent.SELECTED;
			firePropertyChange(FIELD_NAME_FROMDATE, (on ? null : fromDate), (on ? fromDate : null));
		}
	}

	/** {@inheritDoc} */
	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		setEnabled(true);
	}

	/** {@inheritDoc} */
	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		setEnabled(false);
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		logger.debug("propertychangeEvent(" + evt + ")");
		String property = evt.getPropertyName();

		if (StringUtils.equals(FIELD_NAME_KONTRAKT, property) && evt.getSource() == contractPanel) {
			Driftkontrakt old = (Driftkontrakt) evt.getOldValue();
			Driftkontrakt now = (Driftkontrakt) evt.getNewValue();
			logger.debug("fireing new contract value : " + old + "," + now);
			clearDfuFilter();
			firePropertyChange(FIELD_NAME_KONTRAKT, old, now);
		} else if (StringUtils.equals(FIELD_NAME_DATEFIELD, property) && evt.getSource() == fromDatePicker) {
			Date old = (Date) evt.getOldValue();
			Date now = (Date) evt.getNewValue();
			fromDate = now;
			logger.debug("fireing new fromDate value : " + old + "," + now);
			firePropertyChange(FIELD_NAME_FROMDATE, old, now);
		} else if (StringUtils.equals(FIELD_NAME_DATEFIELD, property) && evt.getSource() == toDatePicker) {
			Date old = (Date) evt.getOldValue();
			Date now = (Date) evt.getNewValue();
			toDate = now;
			logger.debug("fireing new toDate value : " + old + "," + now);
			firePropertyChange(FIELD_NAME_TODATE, old, now);
		} else if (evt.getSource() == veiInfo) {
			String newText = getVeiButtonText();
			newText = StringUtils.isBlank(newText) ? GUITexts.getText(GUITexts.SEARCH_ROAD) : newText;
			firePropertyChange(FIELD_NAME_VEIBUTTONCOLOR, null, getVeiButtonColor());
			firePropertyChange(FIELD_NAME_VEIBUTTONTEXT, null, newText);
		} else if (StringUtils.equals("foreground", property)) {
			Color color = (Color) evt.getNewValue();
			JButton button = null;
			if (evt.getSource() == pdaButton || evt.getSource() == veiButton) {
				button = (JButton) evt.getSource();
			}

			if (button != null) {
				if (color == Color.RED) {
					button.setFont(button.getFont().deriveFont(Font.BOLD));
				} else {
					button.setFont(button.getFont().deriveFont(Font.NORMAL));
				}
			}
		} 
	}

	private void setDates() {
		fromDate = fromDatePicker.getDate();
		parentPlugin.getDiscoveryFilter().setFraDato(getFromDate());
		parentPlugin.getIncidentFilter().setFraDato(getFromDate());
		parentPlugin.getInspectionFilter().setFraDato(getFromDate());

		parentPlugin.getDiscoveryFilter().setTilDato(getToDate());
		parentPlugin.getIncidentFilter().setTilDato(getToDate());
		parentPlugin.getInspectionFilter().setTilDato(getToDate());
	}

	public void setEnabled(boolean f) {
		veiButton.setEnabled(f);
		toDatePicker.setEnabled(f);
		openFromDate.setEnabled(f);
		pdaButton.setEnabled(f);
		fromDatePicker.setEnabled(openFromDate.isEnabled() && openFromDate.isSelected());
	}

	private void unbindVeiGroup(VeiInfo vei) {
		if (vei != null) {
			vei.removePropertyChangeListener(this);
		}
	}

	class Dialog extends JDialog {
		private boolean set;

		public Dialog(final JFrame frame, final Component component) {
			super(frame, GUITexts.getText(GUITexts.SEARCH_PDA), true);
			add(BoxUtil.createVerticalBox(0, component, new PDAButtonPanel(this)));
		}

		public boolean isSet() {
			return set;
		}

		public void setSet(boolean b) {
			set = b;
		}
	}

	class PDAButtonPanel extends JPanel {

		public PDAButtonPanel(final Dialog owner) {
			JButton ok = new JButton(GUITexts.getText(GUITexts.OK), IconResources.OK_ICON);
			ok.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					owner.setSet(true);
					owner.setVisible(false);
				}
			});

			JButton reset = new JButton(GUITexts.getText(GUITexts.RESET), IconResources.RESET_ICON);
			reset.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					pdaPanel.reset();
				}
			});

			setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			add(BoxUtil.createVerticalStrut(2));
			add(BoxUtil
					.createHorizontalBox(0, Box.createHorizontalGlue(), reset, BoxUtil.createHorizontalStrut(10), ok));
			add(BoxUtil.createVerticalStrut(2));
		}
	}
}
