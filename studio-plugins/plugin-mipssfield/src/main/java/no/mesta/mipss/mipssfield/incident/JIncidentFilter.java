package no.mesta.mipss.mipssfield.incident;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.HendelseQueryFilter;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.inspection.JInspectionFilter;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JAarsakPanel;
import no.mesta.mipss.ui.JTrafikktiltakPanel;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Panel for å sette filter på tabell for oversikt over hendelser
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JIncidentFilter extends JPanel implements MipssBeanLoaderListener {
	private static final int HSTRUT = 5;
	private static final Logger logger = LoggerFactory.getLogger(JInspectionFilter.class);
	private static final int VSTRUT = 5;
	private final HendelseQueryFilter filter;
	private JButton filterButton;
	private MipssFieldModule parentPlugin;
	private JAarsakPanel aarsakPanel;
	private JTrafikktiltakPanel tiltakPanel;
	private JLabel noRowsHolder = new JLabel("0");
	private JButton hentMedIdButton;

	/**
	 * Konstruktør
	 * 
	 */
	@SuppressWarnings("unchecked")
	public JIncidentFilter(final MipssFieldModule parentPlugin) {
		this.parentPlugin = parentPlugin;
		parentPlugin.getIncidentTableModel().addTableLoaderListener(this);
		filter = parentPlugin.getIncidentFilter();

		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), GUITexts
				.getText(GUITexts.FILTER_TITLE)));

		filterButton = new JButton(GUITexts.getText(GUITexts.SEARCH), IconResources.SEARCH_ICON);
		filterButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filterButton.setEnabled(false);
				updateFilter();
				JIncidentFilter.this.parentPlugin.getIncidentTableModel().setBeanMethodArgValues(
						new Object[] { filter });
				JIncidentFilter.this.parentPlugin.getIncidentTableModel().loadData();
			}
		});
		
		hentMedIdButton = new JButton(GUITexts.getText(GUITexts.GET_INCIDENT_W_ID), IconResources.DETALJER_SMALL_ICON);
		hentMedIdButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				parentPlugin.openWithDialog(MipssFieldModule.IHF_TYPE.HENDELSE);
			}
		});
		Binding<HendelseQueryFilter, Boolean, JButton, Boolean> enableFilterButton = BindingHelper.createbinding(
				filter, "${kontraktId != null}", filterButton, "enabled");
		enableFilterButton.bind();

		JCheckBox forsikringssak = new JCheckBox(GUITexts.getText(GUITexts.INSURANCE_CLAIM), false);
		Binding fb = BindingHelper.createbinding(forsikringssak, "selected", filter, "forsikringssak");
		fb.bind();
		
		JCheckBox skred = new JCheckBox(GUITexts.getText(GUITexts.SKRED));
		Binding sk = BindingHelper.createbinding(skred, "selected", filter, "skred");
		sk.bind();
		
		JCheckBox kunslettede = new JCheckBox(GUITexts.getText(GUITexts.ONLY_DELETED), false);
		Binding sb = BindingHelper.createbinding(kunslettede, "selected", filter, "kunSlettede");
		sb.bind();

		
		aarsakPanel = new JAarsakPanel();
		tiltakPanel = new JTrafikktiltakPanel();

		ComponentSizeResources.setComponentSize(aarsakPanel, new Dimension(200, 200));
		ComponentSizeResources.setComponentSize(tiltakPanel, new Dimension(200, 200));
		
		Box rader = BoxUtil.createHorizontalBox(0, new JLabel("Rader: "), noRowsHolder);

		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(rader, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,5,2,0), 0,0));
		buttonPanel.add(filterButton, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,5,2,0), 0,0));
		buttonPanel.add(hentMedIdButton,new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,10,2,5), 0,0));
		
		setLayout(new GridBagLayout());
		add(aarsakPanel, new GridBagConstraints(0,0, 1,3, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(tiltakPanel, new GridBagConstraints(1,0, 1,3, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		add(forsikringssak, new GridBagConstraints(2,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15,0,0,0), 0,0));
		add(skred, 			new GridBagConstraints(2,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(kunslettede,   	new GridBagConstraints(2,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		add(buttonPanel, 	new GridBagConstraints(0,3, 4,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));

	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		parentPlugin = null;
	}

	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		logger.debug("loadingFinished(" + event + ")");
		filterButton.setEnabled(true);
		int rows = parentPlugin.getIncidentTableModel().getRowCount();
		noRowsHolder.setText(String.valueOf(rows));
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		logger.debug("loadingStarted(" + event + ")");
	}

	private void updateFilter() {
		filter.setAarsaker(aarsakPanel.getAlleValgte());
		filter.setTrafikktiltak(tiltakPanel.getAlleValgte());
		filter.setIngenAarsaker(aarsakPanel.isIngenValgt());
		filter.setIngenTrafikktiltak(tiltakPanel.isIngenValgt());
		filter.setIngenDfuer(parentPlugin.getCommonFilterPanel().isIngenDfuerValgt());
		filter.setVeiInfo(parentPlugin.getCommonFilterPanel().getVeiInfo());
	}
}
