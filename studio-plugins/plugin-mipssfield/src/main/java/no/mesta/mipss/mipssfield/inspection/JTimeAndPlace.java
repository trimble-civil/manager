package no.mesta.mipss.mipssfield.inspection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Formatter;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for å vise tid og sted for en inspeksjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JTimeAndPlace extends JAbstractInfoPanel {
	private static final Dimension FIELD_MAX_SIZE = new Dimension(Short.MAX_VALUE, 22);
	private static final Dimension FIELD_MIN_SIZE = new Dimension(220, 22);
	private static final Dimension FIELD_PREF_SIZE = new Dimension(450, 22);
	private static final String FORMAT = "%1$,1.3f";
	private static final Logger logger = LoggerFactory.getLogger(JTimeAndPlace.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 120);
	public static final Dimension MIN_SIZE = new Dimension(100, 120);
	public static final Dimension PREF_SIZE = new Dimension(450, 120);
	private static final Integer STRUT = 2;

	public JTimeAndPlace(final JInspectionDetailPanel parent, final Inspeksjon inspeksjon) {
		logger.debug("created()");
		setFieldToolkit(parent.getFieldToolkit());
		long varighet = 0;
		JTextField varighetField = null;
		if (inspeksjon.getTilTidspunkt()!=null){
			varighet = inspeksjon.getTilTidspunkt().getTime() - inspeksjon.getFraTidspunkt().getTime();
			Time time = new Time(varighet + 30000); // Legger til 30sekunder for å avrunde opp til hele minutt
			varighetField = new JTextField(time.toString());
			varighetField.setEditable(false);
			varighetField.setEnabled(false);
			getFieldToolkit().controlEnabledField(varighetField);
		}else{
			varighetField = new JTextField("Ikke avsluttet!");
			varighetField.setForeground(Color.red);
			varighetField.setEditable(false);
		}
		
		
		ComponentSizeResources.setComponentSizes(varighetField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		InspeksjonSokView view = parent.getViewObject();
		Double totalInsp = view.getEvInspisert() + view.getRvInspisert() + view.getFvInspisert()
				+ view.getKvInspisert();
		String evTxt = format(view.getEvInspisert());
		String rvTxt = format(view.getRvInspisert());
		String fvTxt = format(view.getFvInspisert());
		String kvTxt = format(view.getKvInspisert());
		String totTxt = format(totalInsp);

		String inspisert;
		if (totalInsp != null && totalInsp > 0) {
			inspisert = GUITexts.getText(GUITexts.TOTAL) + ":" + totTxt + GUITexts.getText(GUITexts.KM_ON);
			if (view.getEvInspisert() != null && view.getEvInspisert() > 0) {
				inspisert += " EV:" + evTxt;
			}

			if (view.getRvInspisert() != null && view.getRvInspisert() > 0) {
				inspisert += " RV:" + rvTxt;
			}

			if (view.getFvInspisert() != null && view.getFvInspisert() > 0) {
				inspisert += " FV:" + fvTxt;
			}

			if (view.getKvInspisert() != null && view.getKvInspisert() > 0) {
				inspisert += " KV:" + kvTxt;
			}
		} else {
			inspisert = GUITexts.getText(GUITexts.TOTAL) + ":" + totTxt;
		}

		JTextField inspisertField = new JTextField(inspisert);
		inspisertField.setEditable(false);
		inspisertField.setEnabled(false);
		getFieldToolkit().controlEnabledField(inspisertField);
		ComponentSizeResources.setComponentSizes(inspisertField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JTextField kontraktField = getFieldToolkit().createTextInputField(Inspeksjon.class, inspeksjon,
				"${kontrakt.textForGUI}", null);
		kontraktField.setEditable(false);
		kontraktField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(kontraktField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JTextField refnummerField = toolkit.createTextInputField(Inspeksjon.class, inspeksjon, "refnummer", null);
		ComponentSizeResources.setComponentSizes(refnummerField, new Dimension(100,22), new Dimension(100, 22), new Dimension(100, 22));
		refnummerField.setEditable(false);
		refnummerField.setEnabled(false);
		
		JTextField opprettetField = getFieldToolkit().createTextInputField(Inspeksjon.class, inspeksjon, "textForGUI",
				null);
		opprettetField.setEditable(false);
		opprettetField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(opprettetField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JLabel idLabel = new JLabel(GUITexts.getText(GUITexts.ID));
		ComponentSizeResources.setComponentSize(idLabel, new Dimension(22, 22));
		idLabel.setHorizontalAlignment(JLabel.RIGHT);
		idLabel.setHorizontalTextPosition(JLabel.RIGHT);
		idLabel.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Driftkontrakt>(GUITexts.getText(GUITexts.CONTRACT), kontraktField, null), 
				idLabel, refnummerField, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.EXECUTED), opprettetField,
				null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.DURATION), varighetField,
				null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.INSPECTED),
				inspisertField, null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	private String format(Double d) {
		Formatter formatter = new Formatter();
		formatter = formatter.format(FORMAT, (Double) d);
		String txt = formatter.toString();
		formatter.close();
		return txt;
	}

	private class Time {
		private static final String BLANK = " ";
		private static final int SECONDS_DIVIDER = 1000;
		private static final int MINUTES_DIVIDER = SECONDS_DIVIDER * 60;
		private static final int HOURS_DIVIDER = MINUTES_DIVIDER * 60;
		private static final int DAYS_DIVIDER = HOURS_DIVIDER * 24;
		private static final String EMPTY = "";
		private final long time;

		public Time(long time) {
			this.time = time;
		}

		private long days() {
			long days = getDays() * DAYS_DIVIDER;
			return days;
		}

		public int getDays() {
			return (int) time / DAYS_DIVIDER;
		}

		public int getHours() {
			return (int) (time - days()) / HOURS_DIVIDER;
		}

		public int getMinutes() {
			return (int) (time - days() - hours()) / MINUTES_DIVIDER;
		}

		public int getSeconds() {
			return (int) (time - days() - hours() - minutes()) / SECONDS_DIVIDER;
		}

		private long hours() {
			return getHours() * HOURS_DIVIDER;
		}

		private long minutes() {
			return getMinutes() * MINUTES_DIVIDER;
		}

		public String toString() {
			String days = getDays() != 0 ? getDays() + "d" + BLANK : EMPTY;
			String hours = getHours() + "t" + BLANK;
			String minutes = getMinutes() + "min";

			return days + hours + minutes;
		}
	}
}
