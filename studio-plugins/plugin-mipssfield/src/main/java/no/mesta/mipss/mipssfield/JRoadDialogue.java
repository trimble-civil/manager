package no.mesta.mipss.mipssfield;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.VetoableChangeSource;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.BlankStringValidator;
import no.mesta.mipss.core.IBlankFieldValidator;
import no.mesta.mipss.core.IntegerStringConverter;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MaxSizeEnforcer;
import no.mesta.mipss.core.NotZeroIntegerValidator;
import no.mesta.mipss.core.NullIntegerValidator;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JNoBlankLabel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

/**
 * Dialogvindu for å redigere stedfesting
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JRoadDialogue extends JDialog implements PropertyChangeSource, PropertyChangeListener, VetoableChangeSource {
	private static final String FELT = "felt";
	private static final String FYLKE = "fylke";
	private static final String HP = "hp";
	private static final String KOMMUNE = "kommune";
	private static final Logger logger = LoggerFactory.getLogger(JRoadDialogue.class);
	private static final String METER = "meter";
	private static final String PARAM_APP = "studio.mipss.field";
	private static final String PARAM_VALUE = "veityper";
	private static String[] roadTypes;
	private static final int STRUT = 2;
	private static final String TEXT = "text";
	private static final String VEI_TYPE = "veiType";
	private static final String VEIKAT = "veikat";
	private static final String VEINR = "veinr";
	private static final String VEISTAT = "veistat";
	private final MipssField bean;
	private BindingGroup bindingGroup;
	private boolean cancelled;
	private String felt = " ";
	private int fylke;
	private int hp;
	private final Box illegalRoad;
	private int kommune;
	private final Map<String, JNoBlankLabel<?>> labels = new HashMap<String, JNoBlankLabel<?>>();
	private int meter;
	private boolean needsValidation;
	private String veikat;
	private int veinr;
	private String veistat;
	private String veiType;
	private final VetoableChangeSupport vetos = new VetoableChangeSupport(this);

	/**
	 * Konstruktør
	 * 
	 * @param owner
	 *            vinduet dialogen tilhører
	 * @param button
	 *            knappen eller komponentet den skal vises over
	 * @param stedfesting
	 *            veien som skal vises/eredigeres
	 */
	public JRoadDialogue(JFrame owner, JComponent center, Punktveiref stedfesting, MipssField bean) {
		super(owner, true);
		this.bean = bean;
		initRoadTypes();
		setTitle(GUITexts.getText(GUITexts.ROAD_DIALOGUE_TITLE));

		if (!StringUtils.isBlank(stedfesting.getVeikategori()) && !StringUtils.isBlank(stedfesting.getVeistatus())) {
			veikat = stedfesting.getVeikategori();
			veistat = stedfesting.getVeistatus();
			veiType = veikat + veistat;
		} else {
			veiType = null;
		}
		
		fylke = stedfesting.getFylkesnummer();
		kommune = stedfesting.getKommunenummer();
		veinr = stedfesting.getVeinummer();
		hp = stedfesting.getHp();
		meter = stedfesting.getMeter();
		felt = stedfesting.getKjorefelt();

		IntegerStringConverter intConverter = new IntegerStringConverter();
		bindingGroup = new BindingGroup();

		JTextField fylkeField = new JTextField(null);
		Binding<JRoadDialogue, Integer, JTextField, String> fylkeBinding = BindingHelper.createbinding(this, FYLKE,
				fylkeField, TEXT, BindingHelper.READ_WRITE);
		fylkeBinding.setConverter(intConverter);
		bindingGroup.addBinding(fylkeBinding);
		fylkeField.setHorizontalAlignment(JTextField.RIGHT);
		Box fylke = createBox(GUITexts.getText(GUITexts.FYLKE), fylkeField, new NotZeroIntegerValidator<JRoadDialogue>(
				this, FYLKE), FYLKE);
		new MaxSizeEnforcer<Integer>(this, fylkeField, FYLKE, 20).start();

		JTextField kommuneField = new JTextField(null);
		Binding<JRoadDialogue, Integer, JTextField, String> kommuneBinding = BindingHelper.createbinding(this, KOMMUNE,
				kommuneField, TEXT, BindingHelper.READ_WRITE);
		kommuneBinding.setConverter(intConverter);
		bindingGroup.addBinding(kommuneBinding);
		kommuneField.setHorizontalAlignment(JTextField.RIGHT);
		Box kommune = createBox(GUITexts.getText(GUITexts.KOMMUNE), kommuneField,
				new NullIntegerValidator<JRoadDialogue>(this, KOMMUNE), KOMMUNE);
		new MaxSizeEnforcer<Integer>(this, kommuneField, KOMMUNE, 99).start();

		JComboBox veiTypeCombo = new JComboBox(roadTypes);
		Binding<JRoadDialogue, String, JComboBox, String> veiTypeBinding = BindingHelper.createbinding(this, VEI_TYPE,
				veiTypeCombo, "selectedItem", BindingHelper.READ_WRITE);
		bindingGroup.addBinding(veiTypeBinding);
		Box veikat = createBox(GUITexts.getText(GUITexts.ROAD_TYPE), veiTypeCombo,
				new BlankStringValidator<JRoadDialogue>(this, VEI_TYPE), VEI_TYPE);

		JTextField veinrField = new JTextField(null);
		Binding<JRoadDialogue, Integer, JTextField, String> veinrBinding = BindingHelper.createbinding(this, VEINR,
				veinrField, TEXT, BindingHelper.READ_WRITE);
		veinrBinding.setConverter(intConverter);
		bindingGroup.addBinding(veinrBinding);
		veinrField.setHorizontalAlignment(JTextField.RIGHT);
		Box veinr = createBox(GUITexts.getText(GUITexts.ROAD_NO), veinrField, new NotZeroIntegerValidator<JRoadDialogue>(
				this, VEINR), VEINR);
		new MaxSizeEnforcer<Integer>(this, veinrField, VEINR, 99999).start();

		JTextField hpField = new JTextField(null);
		Binding<JRoadDialogue, Integer, JTextField, String> hpBinding = BindingHelper.createbinding(this, HP, hpField,
				TEXT, BindingHelper.READ_WRITE);
		hpBinding.setConverter(intConverter);
		bindingGroup.addBinding(hpBinding);
		hpField.setHorizontalAlignment(JTextField.RIGHT);
		Box hp = createBox(GUITexts.getText(GUITexts.HP), hpField, new NotZeroIntegerValidator<JRoadDialogue>(this, HP),
				HP);
		new MaxSizeEnforcer<Integer>(this, hpField, HP, 999).start();

		JTextField meterField = new JTextField(null);
		Binding<JRoadDialogue, Integer, JTextField, String> meterBinding = BindingHelper.createbinding(this, METER,
				meterField, TEXT, BindingHelper.READ_WRITE);
		meterBinding.setConverter(intConverter);
		bindingGroup.addBinding(meterBinding);
		meterField.setHorizontalAlignment(JTextField.RIGHT);
		Box meter = createBox(GUITexts.getText(GUITexts.METER), meterField, new NullIntegerValidator<JRoadDialogue>(
				this, METER), METER);
		new MaxSizeEnforcer<Integer>(this, meterField, METER, 99999).start();

		JTextField feltField = new JTextField(null);
		Binding<JRoadDialogue, String, JTextField, String> feltBinding = BindingHelper.createbinding(this, FELT,
				feltField, TEXT, BindingHelper.READ_WRITE);
		bindingGroup.addBinding(feltBinding);
		feltField.setHorizontalAlignment(JTextField.RIGHT);
		Box felt = createBox(GUITexts.getText(GUITexts.FIELD), feltField, null, null);

		JButton cancel = new JButton(GUITexts.getText(GUITexts.CANCEL));
		cancel.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(cancel, new Dimension(60, 22));
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancel();
			}
		});
		JButton ok = new JButton(GUITexts.getText(GUITexts.OK));
		ok.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(ok, new Dimension(60, 22));
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				finished();
			}
		});

		illegalRoad = Box.createHorizontalBox();
		illegalRoad.add(new JLabel(IconResources.WARNING_ICON));
		illegalRoad.add(BoxUtil.createHorizontalStrut(STRUT));
		illegalRoad.add(new JLabel(GUITexts.getText(GUITexts.ILLEGAL_ROAD)));
		illegalRoad.add(Box.createHorizontalGlue());
		illegalRoad.setVisible(false);

		Box buttons = Box.createHorizontalBox();
		buttons.add(Box.createHorizontalGlue());
		buttons.add(cancel);
		buttons.add(BoxUtil.createHorizontalStrut(STRUT));
		buttons.add(ok);

		Box fields = Box.createHorizontalBox();
		fields.add(fylke);
		fields.add(BoxUtil.createHorizontalStrut(STRUT));
		fields.add(kommune);
		fields.add(BoxUtil.createHorizontalStrut(STRUT));
		fields.add(veikat);
		fields.add(BoxUtil.createHorizontalStrut(STRUT));
		fields.add(veinr);
		fields.add(BoxUtil.createHorizontalStrut(STRUT));
		fields.add(hp);
		fields.add(BoxUtil.createHorizontalStrut(STRUT));
		fields.add(meter);
		fields.add(BoxUtil.createHorizontalStrut(STRUT));
		fields.add(felt);

		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		add(illegalRoad);
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, fields));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, buttons));
		add(BoxUtil.createVerticalStrut(STRUT));

		bindingGroup.bind();

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			/** {@inheritDoc} */
			@Override
			public void windowClosing(WindowEvent e) {
				cancel();
			}
		});

		pack();
		setResizable(false);
		setLocationRelativeTo(center);
		setVisible(true);
	}

	/** {@inheritDoc} */
	@Override
	public void addVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		vetos.addVetoableChangeListener(propertyName, listener);
	}

	/** {@inheritDoc} */
	@Override
	public void addVetoableChangeListener(VetoableChangeListener listener) {
		vetos.addVetoableChangeListener(listener);
	}

	/**
	 * Når brukeren avbryter
	 * 
	 */
	private void cancel() {
		cancelled = true;
		dispose();
	}

	/**
	 * Lager en horisontalbox med label og felt
	 * 
	 * @param label
	 * @param field
	 * @return
	 */
	private <T> Box createBox(String label, JComponent field, IBlankFieldValidator<T> validator, String fieldName) {

		Box box = Box.createVerticalBox();
		if (validator != null) {
			JNoBlankLabel<T> jLabel = new JNoBlankLabel<T>(label, validator);
			box.add(BoxUtil.createHorizontalBox(0, jLabel, Box.createHorizontalGlue()));
			labels.put(fieldName, jLabel);
			addPropertyChangeListener(fieldName, this);
		} else {
			box.add(BoxUtil.createHorizontalBox(0, new JLabel(label), Box.createHorizontalGlue()));
		}
		box.add(Box.createHorizontalStrut(STRUT));
		box.add(field);

		Dimension d = new Dimension(60, 20);
		ComponentSizeResources.setComponentSize(field, d);

		return box;
	}

	/**
	 * Når brukeren trykker ok
	 * 
	 */
	private void finished() {
		if (validateRoad()) {
			dispose();
		}
	}

	public String getFelt() {
		return felt;
	}

	public int getFylke() {
		return fylke;
	}

	public int getHp() {
		return hp;
	}

	public int getKommune() {
		return kommune;
	}

	public int getMeter() {
		return meter;
	}

	public String getVeikat() {
		return veikat;
	}

	public int getVeinr() {
		return veinr;
	}

	public String getVeistat() {
		return veistat;
	}

	public String getVeiType() {
		return veiType;
	}

	private void initRoadTypes() {
		if (roadTypes == null) {
			Konfigparam konfig = KonfigparamPreferences.getInstance().hentEnForApp(PARAM_APP, PARAM_VALUE);
			if (konfig == null || StringUtils.isBlank(konfig.getVerdi())) {
				throw new IllegalStateException("No MIPSS_FELT RoadType configured in Konfigparam");
			}

			roadTypes = konfig.getVerdi().split(",");
			for (int i = 0; i < roadTypes.length; i++) {
				roadTypes[i] = roadTypes[i].trim();
			}
		}
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public boolean isValidated() {
		return needsValidation;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String fieldName = evt.getPropertyName();
		JNoBlankLabel<?> label = labels.get(fieldName);
		if (label != null && label.isWarning()) {
			label.ok();
		}

		illegalRoad.setVisible(false);
		pack();
	}

	/** {@inheritDoc} */
	@Override
	public void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
		vetos.removeVetoableChangeListener(propertyName, listener);
	}

	/** {@inheritDoc} */
	@Override
	public void removeVetoableChangeListener(VetoableChangeListener listener) {
		vetos.removeVetoableChangeListener(listener);
	}

	public void setFelt(String felt) {
		String old = this.felt;
		this.felt = felt;
		firePropertyChange(FELT, old, felt);
	}

	public void setFylke(int fylke) {
		int old = this.fylke;
		this.fylke = fylke;

		try {
			vetos.fireVetoableChange(FYLKE, old, fylke);
		} catch (PropertyVetoException e) {
			this.fylke = old;
		}

		needsValidation = true;
		firePropertyChange(FYLKE, old, fylke);
	}

	public void setHp(int hp) {
		int old = this.hp;
		this.hp = hp;

		try {
			vetos.fireVetoableChange(HP, old, hp);
		} catch (PropertyVetoException e) {
			this.hp = old;
		}
		
		needsValidation = true;
		firePropertyChange(HP, old, hp);
	}

	public void setKommune(int kommune) {
		int old = this.kommune;
		this.kommune = kommune;

		try {
			vetos.fireVetoableChange(KOMMUNE, old, kommune);
		} catch (PropertyVetoException e) {
			this.kommune = old;
		}

		needsValidation = true;
		firePropertyChange(KOMMUNE, old, kommune);
	}
	
	public void setMeter(int meter) {
		int old = this.meter;
		this.meter = meter;

		try {
			vetos.fireVetoableChange(METER, old, meter);
		} catch (PropertyVetoException e) {
			this.meter = old;
		}

		needsValidation = true;
		firePropertyChange(METER, old, meter);
	}

	public void setVeikat(String veikat) {
		logger.debug("setVeikat(" + veikat + ") start");
		String old = this.veikat;
		String oldVeiType = this.veiType;
		this.veikat = veikat;

		if (StringUtils.isBlank(veikat)) {
			this.veiType = null;
		} else {
			if (StringUtils.isBlank(veistat)) {
				setVeistat("V");
			}

			this.veiType = String.valueOf(veikat) + String.valueOf(veistat);
		}

		needsValidation = true;
		firePropertyChange(VEIKAT, old, veikat);
		firePropertyChange(VEI_TYPE, oldVeiType, this.veiType);
	}

	public void setVeinr(int veinr) {
		int old = this.veinr;
		this.veinr = veinr;

		try {
			vetos.fireVetoableChange(VEINR, old, veinr);
		} catch (PropertyVetoException e) {
			this.veinr = old;
		}

		needsValidation = true;
		firePropertyChange(VEINR, old, veinr);
	}

	public void setVeistat(String veistat) {
		logger.debug("setVeistat(" + veistat + ") start");
		String old = this.veistat;
		String oldType = this.veiType;
		this.veistat = veistat;

		if (StringUtils.isBlank(veistat)) {
			this.veiType = null;
		} else {
			this.veiType = String.valueOf(veikat) + String.valueOf(veistat);
		}

		needsValidation = true;
		firePropertyChange(VEISTAT, old, veistat);
		firePropertyChange(VEI_TYPE, oldType, this.veiType);
	}

	public void setVeiType(String veiType) {
		logger.debug("setVeiType(" + veiType + ") start");
		String oldVeiType = this.veiType;
		String oldKat = this.veikat;
		String oldstat = this.veistat;

		this.veiType = veiType;
		if (StringUtils.isBlank(veiType)) {
			this.veikat = null;
			this.veistat = null;
		} else {
			this.veikat = veiType.substring(0, 1);
			this.veistat = veiType.substring(1, 2);
		}

		needsValidation = true;
		firePropertyChange(VEI_TYPE, oldVeiType, veiType);
		firePropertyChange(VEISTAT, oldstat, this.veistat);
		firePropertyChange(VEIKAT, oldKat, this.veikat);
	}

	private boolean validateRoad() {
		boolean valid = true;
		
		if(!needsValidation) {
			return true;
		}

		if (fylke == 0 || fylke == 13) {
			valid = valid && false;
			labels.get(FYLKE).warn();
		} else {
			labels.get(FYLKE).ok();
		}

		if (veinr == 0) {
			valid = valid && false;
			labels.get(VEINR).warn();
		} else {
			labels.get(VEINR).ok();
		}
		
		if(hp == 0) {
			valid = valid && false;
			labels.get(HP).warn();
		} else {
			labels.get(HP).ok();
		}

		if (StringUtils.isBlank(veiType)) {
			valid = valid && false;
			labels.get(VEI_TYPE).warn();
		} else {
			labels.get(VEI_TYPE).ok();
		}

		if (StringUtils.equals("KV", veiType)) {
			if (kommune == 0) {
				valid = valid && false;
				labels.get(KOMMUNE).warn();
			} else {
				labels.get(KOMMUNE).ok();
			}
		} else {
			if (kommune > 0) {
				valid = valid && false;
				labels.get(KOMMUNE).warn();
			} else {
				labels.get(KOMMUNE).ok();
			}
		}

		Punktveiref veiref = new Punktveiref();
		veiref.setFylkesnummer(getFylke());
		veiref.setHp(getHp());
		veiref.setKommunenummer(getKommune());
		veiref.setMeter(getMeter());
		veiref.setVeikategori(getVeikat());
		veiref.setVeinummer(getVeinr());
		veiref.setVeistatus(getVeistat());

		if (valid) {
			boolean legal = bean.validatePunktveiref(veiref);
			valid = valid && legal;
			illegalRoad.setVisible(true);
			pack();
		}

		return valid;
	}
}
