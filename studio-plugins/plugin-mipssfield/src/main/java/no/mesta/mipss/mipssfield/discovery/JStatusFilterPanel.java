package no.mesta.mipss.mipssfield.discovery;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import no.mesta.mipss.mipssfield.PunktregStatusFilter;

/**
 * Liste av checkmarks for hvilke filtere som skal være på
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JStatusFilterPanel extends JPanel {
	private List<JOtherFilter> valg = new ArrayList<JOtherFilter>();

	/**
	 * Konstruktør
	 * 
	 * @param filtere
	 */
	public JStatusFilterPanel(PunktregStatusFilter[] filtere) {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		for (PunktregStatusFilter f : filtere) {
			JOtherFilter box = new JOtherFilter(f);
			valg.add(box);
			add(box);
		}
	}

	/**
	 * Valgte filtere
	 * 
	 * @return
	 */
	public PunktregStatusFilter[] getSelection() {
		List<PunktregStatusFilter> resultat = new ArrayList<PunktregStatusFilter>();
		for (JOtherFilter b : valg) {
			if (b.isSelected()) {
				resultat.add(b.getFilter());
			}
		}

		PunktregStatusFilter[] array = new PunktregStatusFilter[resultat.size()];

		return resultat.toArray(array);
	}

	/** {@inheritDoc} */
	class JOtherFilter extends JCheckBox {
		private PunktregStatusFilter f;

		/**
		 * Konstruktør
		 * 
		 * @param f
		 */
		public JOtherFilter(PunktregStatusFilter f) {
			super(f.getTextForGUI(), false);
			this.f = f;
		}

		/**
		 * Filter som boxen er basert på
		 * 
		 * @return
		 */
		public PunktregStatusFilter getFilter() {
			return f;
		}
	}
}
