package no.mesta.mipss.mipssfield.incident.r5;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.UIUtils;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.jxlayer.JXLayer;

public class SkadevolderPanel extends JPanel{
	private JFormattedTextField regnrField;
	private JFormattedTextField hengerField;
	private JFormattedTextField merkeField;
	private JFormattedTextField forsikringsseskField;
	private JFormattedTextField eiernavnField;
	private JFormattedTextField tlf1Field;
	private JFormattedTextField tlf2Field;

	private JRadioButton snobroytJa;
	private JRadioButton snobroytNei;
	private JFormattedTextField navnField;
	private JFormattedTextField adresseField;
	private final Forsikringsskade forsikringsskade;
	private final GridBagConstraints c;
	
	private JCheckBox ukjentSkadevolderCheck;
	private BindingGroup bg = new BindingGroup();
	private final R5 mainForm;


	public SkadevolderPanel(Forsikringsskade forsikringsskade, GridBagConstraints c, R5 mainForm) {
		this.forsikringsskade = forsikringsskade;
		this.c = c;
		this.mainForm = mainForm;
		initGui();
	}

	private void initGui(){
		setLayout(new GridBagLayout());
		regnrField = new JFormattedTextField(); 
		hengerField = new JFormattedTextField(); 
		merkeField = new JFormattedTextField();
		
		forsikringsseskField = new JFormattedTextField(); 
		
		eiernavnField = new JFormattedTextField();
		tlf1Field = new JFormattedTextField();
		tlf1Field.setDocument(new DocumentFilter(20));
		
		adresseField = new JFormattedTextField(); 
		navnField = new JFormattedTextField(); 
		
		tlf2Field = new JFormattedTextField(); 
		tlf2Field.setDocument(new DocumentFilter(20));
		
		snobroytJa = new JRadioButton(GUITexts.getText(GUITexts.YES));
		snobroytNei = new JRadioButton(GUITexts.getText(GUITexts.NO));
		snobroytJa.setEnabled(false);
		snobroytNei.setEnabled(false);
		
		ukjentSkadevolderCheck = new JCheckBox("Ukjent skadevolder");
		ukjentSkadevolderCheck.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean enabled = true;
				if (ukjentSkadevolderCheck.isSelected()){
					enabled = false;
					if (!isSkadevolderNotPresent()){
						int a = JOptionPane.showConfirmDialog(SkadevolderPanel.this, GUITexts.getText(GUITexts.R5_SKADEVOLDERWARNING), GUITexts.getText(GUITexts.R5_SKADEVOLDERWARNING_TITLE), JOptionPane.YES_NO_OPTION);
						if (a==JOptionPane.YES_OPTION){
							nukeSkadevolder();
						}else{
							ukjentSkadevolderCheck.setSelected(false);
							enabled=true;
						}
					}
					
				}
				enableSkadevolderField(enabled);
			}
		});
		
		ButtonGroup rGroup = new ButtonGroup();
		rGroup.add(snobroytNei);
		rGroup.add(snobroytJa);
		bind();
		createSkadevolderPanel();
		
		if (isSkadevolderNotPresent()){
			ukjentSkadevolderCheck.setSelected(true);
			enableSkadevolderField(false);
		}
		
	}
	private void nukeSkadevolder(){
		List<SkadeKontakt> forsikringsselskap = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORSIKRINGSELSKAP);
		List<SkadeKontakt> eier = mainForm.getKontakter(forsikringsskade, SkadeKontakt.EIER_AV_KJORETOY);
		List<SkadeKontakt> forer = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORER_AV_KJORETOY);
		forsikringsskade.getSkadeKontakter().removeAll(forsikringsselskap);
		forsikringsskade.getSkadeKontakter().removeAll(eier);
		forsikringsskade.getSkadeKontakter().removeAll(forer);
		forsikringsskade.setSkadevolderBilmerke(null);
		forsikringsskade.setSkadevolderHenger(null);
		forsikringsskade.setSkadevolderRegnr(null);
		forsikringsseskField.setText(null);
		eiernavnField.setText(null);
		tlf1Field.setText(null);
		adresseField.setText(null);
		navnField.setText(null);
		tlf2Field.setText(null);
	}
	private boolean isSkadevolderNotPresent(){
		List<SkadeKontakt> forsikringsselskap = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORSIKRINGSELSKAP);
		List<SkadeKontakt> eier = mainForm.getKontakter(forsikringsskade, SkadeKontakt.EIER_AV_KJORETOY);
		List<SkadeKontakt> forer = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORER_AV_KJORETOY);
		String bilmerke = forsikringsskade.getSkadevolderBilmerke();
		String henger = forsikringsskade.getSkadevolderHenger();
		String regnr = forsikringsskade.getSkadevolderRegnr();
		return (forsikringsselskap.size()==0 &&
				eier.size()==0 &&
				forer.size()==0 &&
				bilmerke==null &&
				henger==null &&
				regnr==null);
	}
	private void enableSkadevolderField(boolean enabled){
		regnrField.setEnabled(enabled);
		hengerField.setEnabled(enabled);
		merkeField.setEnabled(enabled);
		forsikringsseskField.setEnabled(enabled);
		eiernavnField.setEnabled(enabled);
		tlf1Field.setEnabled(enabled);
		adresseField.setEnabled(enabled);
		navnField.setEnabled(enabled);
		tlf2Field.setEnabled(enabled);
	}
	private void bind(){
		bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadevolderRegnr",regnrField, "text",  BindingHelper.READ_WRITE));
		bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadevolderHenger",hengerField, "text",  BindingHelper.READ_WRITE));
		bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadevolderBilmerke",merkeField, "text",  BindingHelper.READ_WRITE));
		
		bg.addBinding(BindingHelper.createbinding(forsikringsskade, "broyteskade",snobroytJa, "selected"));
		bg.addBinding(BindingHelper.createbinding(forsikringsskade, "${broyteskade==false}",snobroytNei, "selected"));
		bg.bind();
	}
	private void createSkadevolderPanel(){
		JLabel skadevolderLabel = new JLabel(GUITexts.getText(GUITexts.SKADEVOLDER));
		JLabel regnrLabel = new JLabel(GUITexts.getText(GUITexts.REGNR));
		JLabel hengerLabel = new JLabel(GUITexts.getText(GUITexts.HENGER));
		JLabel merkeLabel = new JLabel(GUITexts.getText(GUITexts.MERKE)+"                 ");
		JLabel forsikringsseskLabel = new JLabel(GUITexts.getText(GUITexts.FORSIKRINGSELSK));
		JLabel eiernavnLabel = new JLabel(GUITexts.getText(GUITexts.NAVN_EIER));
		JLabel tlf1Label = new JLabel(GUITexts.getText(GUITexts.TELEFONNR));
		JLabel adresseLabel = new JLabel(GUITexts.getText(GUITexts.ADRESSE_EIER));
		JLabel navnLabel = new JLabel(GUITexts.getText(GUITexts.NAVN_FORER));
		JLabel tlf2Label = new JLabel(GUITexts.getText(GUITexts.TELEFONNR));
		JLabel snobroytLabel = new JLabel(GUITexts.getText(GUITexts.SNOBROYT));
		
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		Font f = skadevolderLabel.getFont();
		skadevolderLabel.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
		add(skadevolderLabel, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		add(ukjentSkadevolderCheck, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=1;
		add(UIUtils.getTwoComponentPanelVert(regnrLabel, regnrField, false), c);
		c.gridx+=1;
		c.gridwidth=2;
		add(UIUtils.getTwoComponentPanelVert(hengerLabel, hengerField, false), c);
		c.gridx+=2;
		c.gridwidth=2;
		add(UIUtils.getTwoComponentPanelVert(merkeLabel, merkeField, false), c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		add(UIUtils.getTwoComponentPanelVert(forsikringsseskLabel, decorateField(forsikringsseskField, "validateForsikringsselskap"), false), c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=3;
		add(UIUtils.getTwoComponentPanelVert(eiernavnLabel, decorateField(eiernavnField, "validateEier"), false), c);
		c.gridx+=3;
		c.gridwidth=2;
		add(UIUtils.getTwoComponentPanelVert(tlf1Label, tlf1Field, false), c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		add(UIUtils.getTwoComponentPanelVert(adresseLabel, adresseField, false), c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=3;
		add(UIUtils.getTwoComponentPanelVert(navnLabel, decorateField(navnField, "validateForer"), false), c);
		c.gridx+=3;
		c.gridwidth=2;
		add(UIUtils.getTwoComponentPanelVert(tlf2Label, tlf2Field, false), c);
		
		JPanel chkPanel = UIUtils.getHorizPanel();
		chkPanel.add(snobroytLabel);
		chkPanel.add(UIUtils.getHStrut(5));
		chkPanel.add(snobroytJa);
		chkPanel.add(snobroytNei);
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		add(chkPanel, c);
		
		c.gridx=0;
		c.gridy++;
		c.fill=GridBagConstraints.BOTH;
		c.weightx=1;
		c.weighty=1;
		add(new JPanel(), c);
		c.fill=GridBagConstraints.HORIZONTAL;
		c.weightx=0.5;
		c.weighty=0.0;
	}
	
	public boolean validateEier(){
		String eier = eiernavnField.getText();
		String eierTlf = tlf1Field.getText();
		String eierAdresse = adresseField.getText();
		if (eier.equals("")&&eierTlf.equals("")&&eierAdresse.equals("")){
			return true;
		}
		if (eier.equals("")) //eiernavn kan ikke være "" når andre felter er utfylt
			return false;
		
		return true;
		
	}
	
	public boolean validateForer(){
		String forerNavn = navnField.getText();//fører
		String forerTlf = tlf2Field.getText();
		
		if (forerNavn.equals("")&&forerTlf.equals("")){
			return true;
		}
		if (forerNavn.equals(""))//fører navn kan ikke være "" når tlf er utfylt
			return false;
		return true;
	}
	
	public boolean isValidFields(){
		boolean valid =true;
		if (!validateEier()){
			valid=false;
		}
		if (!validateForer()){
			valid=false;
		}
		if (!validateForsikringsselskap()){
			valid=false;
		}
		return valid;
	}
	public boolean validateForsikringsselskap(){
		return true;
	}
	private JXLayer<JComponent> decorateField(JComponent c, String validationMethod){
        JXLayer<JComponent> layer = new JXLayer<JComponent>(c);
        layer.setUI(new ValidationLayerUI(validationMethod, this));
        return layer;
	}
	
	public String getEierNavn(){
		return eiernavnField.getText();
	}
	public String getTlf1(){
		return tlf1Field.getText();
	}
	public String getTlf2(){
		return tlf2Field.getText();
	}
	public String getAdresse(){
		return adresseField.getText();
	}
	public String getForerNavn(){
		return navnField.getText();
	}
	public String getForsikringsselskap(){
		return forsikringsseskField.getText();
	}
	public void setForsikringsselskap(String navn){
		forsikringsseskField.setText(navn);	
	}
	public void setEierNavn(String navn){
		eiernavnField.setText(navn);
	}
	public void setTlf1(String tlf1){
		tlf1Field.setText(tlf1);		
	}
	public void setAdresse(String adresse){
		adresseField.setText(adresse);
	}
	public void setForerNavn(String navn){
		navnField.setText(navn);
	}
	public void setTlf2(String tlf2){
		tlf2Field.setText(tlf2);
	}
}
