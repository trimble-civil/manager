package no.mesta.mipss.mipssfield.incident.r11;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.r.r11.SkredEgenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.VaerFraVaerstasjon;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.service.veinett.VeirefVO;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class R11Controller {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private List<Egenskap> egenskapList;
	private List<VeirefVO> veirefList;
	
//	private final SkredService skredBean;
	private Skred originalSkred;
	private Skred skred;
	
	private VeinettService veinettService;
	private NvdbWebservice nvdb;
	
	public R11Controller(Hendelse hendelse){
		//TODO utkommentert pga modellendringer
//		this.originalSkred = hendelse.getSkred();
		skred = new Skred();
		
		if (originalSkred!=null)
			skred.setGuid(originalSkred.getGuid());
		
		//klon skredobjektet slik at vi jobber på et objekt som ikke endrer tilstanden til 
		//det originale skredobjektet, for å ha mulighet til å sette verdien tilbake til 
		//den originale dersom brukeren avbryter
		try{
			skred.merge(originalSkred);
		} catch (IllegalArgumentException e){
			//TODO hvorfor i alle verdens dager kaster denne en illegalargumentexception
		}
		
		//skal opprette nytt skred og trenger noen defaultverdier
		if (originalSkred==null){
			opprettNyttSkred(hendelse);
		}
		//TODO utkommentert pga modellendringer
//		if (hendelse.getStedfestingList().size()<2){
//			opprettTilPunktstedfest(hendelse);
//		}
		
		
		cloneSkredEgenskapverdiListe();
		
//		skredBean = BeanUtil.lookup(SkredService.BEAN_NAME, SkredService.class);
		veinettService = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
//		egenskapList = skredBean.getAlleEgenskaper();
		veirefList = veinettService.getVeirefListForDriftkontrakt(hendelse.getSak().getKontraktId());
		
	}
	/**
	 * Kloner skredets SkredEgenskapList og legger relasjonene til det klonede Skredobjektet
	 */
	private void cloneSkredEgenskapverdiListe() {
		List<SkredEgenskapverdi> egenskaper = new ArrayList<SkredEgenskapverdi>();
		if (originalSkred!=null){
			for (SkredEgenskapverdi v:originalSkred.getSkredEgenskapList()){
				SkredEgenskapverdi n = new SkredEgenskapverdi();
				n.setEgenskapverdi(v.getEgenskapverdi());
				n.setOpprettetDato(v.getOpprettetDato());
				n.setSkred(v.getSkred());
				egenskaper.add(n);
			}
		}
		skred.setSkredEgenskapList(egenskaper);
	}
	/**
	 * Oppretter et tilStedfest objekt dersom det ikke finnes fra før. Verdiene til 
	 * det nye Punktstedfest vil være det samme som Hendelse sin Punktstedfest
	 * @param hendelse
	 */
	private void opprettTilPunktstedfest(Hendelse hendelse) {
		//TODO utkommentert pga modellendringer
//		List<Punktstedfest> stedfestingList = hendelse.getStedfestingList();
//		//opprett en stedfesting til som representerer til_hp
//		if (stedfestingList.size()<2){
//			Punktstedfest nyttTilPunkt = new Punktstedfest();
//			nyttTilPunkt.setGuid(ServerUtils.getInstance().fetchGuid());
//			
//			Punktstedfest origPunkt = stedfestingList.get(0);
//			
//			nyttTilPunkt.setReflinkIdent(origPunkt.getReflinkIdent());
//			nyttTilPunkt.setReflinkPosisjon(origPunkt.getReflinkPosisjon());
//			nyttTilPunkt.setSnappetH(origPunkt.getSnappetH());
//			nyttTilPunkt.setSnappetX(origPunkt.getSnappetX());
//			nyttTilPunkt.setSnappetY(origPunkt.getSnappetY());
//			nyttTilPunkt.setX(origPunkt.getX());
//			nyttTilPunkt.setY(origPunkt.getY());
//			//TODO utkommentert pga modellendring
////			nyttTilPunkt.setHendelse(hendelse);
//			
//			Punktveiref veiref = origPunkt.getVeiref().clone();
//			veiref.setFraDato(Clock.now());
//			nyttTilPunkt.addPunktveiref(veiref);
//			stedfestingList.add(nyttTilPunkt);
//		}
	}
	
	/**
	 * Oppretter et nytt Skred tilknyttet Hendelsen
	 * @param hendelse
	 */
	private void opprettNyttSkred(Hendelse hendelse) {
		skred.setGuid(ServerUtils.getInstance().fetchGuid());
		//TODO utkommentert pga modellendring
//		skred.setHendelse(hendelse);
		skred.setStengtPgaSkredfare(Boolean.FALSE);
		skred.setIngenNedbor(Boolean.FALSE);
		skred.setGjelderGS(Boolean.FALSE);
		skred.setSkredDato(hendelse.getDato());
		VaerFraVaerstasjon vaer = new VaerFraVaerstasjon();
		vaer.setSkred(skred);
		skred.setVaerFraVaerstasjon(vaer);
	}
	
	/**
	 * Returnerer den originale forekomsten av skred, 
	 * ingen endringer vil bli gjort på dette objektet eller dets referanser 
	 * @return
	 */
	public Skred getOriginalSkred(){
		return originalSkred;
	}
	
	/**
	 * Returnerer den editerte versjonen av skred
	 * @return
	 */
	public Skred getSkred(){
		return skred;
	}
	/**
	 * Lager en streng med koordinater for gitt veinettveireferanse og oppdaterer
	 * samtidig faktiske reflinkseksjoner og veireferanser for stedfestingen.
	 * @param c1
	 * @param punktstedfest 
	 * @return
	 */
	public String getKoordinater(Veinettveireferanse c1, Punktstedfest punktstedfest, Punktveiref veiref){
		String east = GUITexts.getText(GUITexts.R11_EAST);
		String s1 = "N:..."+east+":...";
		if (c1!=null){
			List<Veinettreflinkseksjon> v1 = nvdb.getReflinkSectionsWithinRoadref(c1);
			List<ReflinkStrekning> geo1 = nvdb.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(v1);
			if (geo1.size()>0){
				ReflinkStrekning rs = geo1.get(0);
				Point p1 = rs.getVectors()[0];
				//TODO mister presisjon her...
				//reflinkposisjon er et flyttall
				punktstedfest.setReflinkPosisjon(rs.getFra());
				punktstedfest.setReflinkIdent(Long.valueOf(rs.getReflinkId()));
				punktstedfest.setX(p1.getX());
				punktstedfest.setY(p1.getY());
				punktstedfest.setSnappetX(p1.getX());
				punktstedfest.setSnappetY(p1.getY());
				
				Veinettreflinkseksjon vr = new Veinettreflinkseksjon();
				vr.setReflinkIdent(Long.valueOf(rs.getReflinkId()));
				vr.setFra(rs.getFra());
				vr.setTil(rs.getTil());
				List<Veinettreflinkseksjon> l = new ArrayList<Veinettreflinkseksjon>();
				l.add(vr);
				List<Veinettveireferanse> nyveirefList = nvdb.getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(l);
				if (nyveirefList.size()>0){
					Veinettveireferanse nyveiref = nyveirefList.get(0);
					veiref.setFylkesnummer(nyveiref.getFylkesnummer().intValue());
					veiref.setKommunenummer(nyveiref.getKommunenummer().intValue());
					veiref.setHp(nyveiref.getHp().intValue());
					veiref.setMeter((int)(nyveiref.getFraKm()*1000));
					veiref.setVeikategori(nyveiref.getVeikategori());
					veiref.setVeinummer(nyveiref.getVeinummer().intValue());
					veiref.setVeistatus(nyveiref.getVeistatus());
				}
				
				s1 ="N:"+p1.getY()+" "+east+":"+p1.getX();
			}
		}
		
		return s1;
	}
	/**
	 * Returnerer Punktveirefobjektet som tilhører Hendelsen og som representerer fra_hp
	 * @return
	 */
	public Punktveiref getFraHp(){
		List<Punktstedfest> stedfestingList = getSkred().getSak().getStedfesting();
		Punktstedfest fraPunkt = stedfestingList.get(0);
		Punktveiref fraRef = fraPunkt.getVeiref();
		return fraRef;
	}
	/**
	 * Returnerer Punktveirefobjektet som tilhører Hendelsen og som representerer til_hp
	 * @return
	 */
	public Punktveiref getTilHp(){
		List<Punktstedfest> stedfestingList = getSkred().getSak().getStedfesting();
		Punktstedfest tilPunkt = stedfestingList.get(1);
		Punktveiref tilRef = tilPunkt.getVeiref();
		return tilRef;
	}
	
	/**
	 * Returnerer listen med VeirefVO som er akutelle for den valgte kontrakten
	 * @return
	 */
	public List<VeirefVO> getVeirefList(){
		return veirefList;
	}
	
	/**
	 * Hent alle egenskapverdier fra server
	 * @param e
	 * @return
	 */
	public List<Egenskapverdi> getEgenskapverdi(Egenskap e){
//		List<Egenskapverdi> egenskapverdier = skredBean.getEgenskapverdier(e);
//		Egenskapverdi velg = new Egenskapverdi();
//		velg.setVerdi("<velg verdi>");
//		egenskapverdier.add(0, velg);
//		return egenskapverdier;
		return null;
	}
	
	/**
	 * Fjerner en egenskapverdi fra skredet. 
	 * @param verdi
	 */
	public void slettEgenskapverdi(Egenskapverdi verdi){
		SkredEgenskapverdi toRemove = null;
		for (SkredEgenskapverdi s:skred.getSkredEgenskapList()){
			if (s.getEgenskapverdi().equals(verdi)){
				toRemove = s;
				break;
			}
		}
		if (toRemove!=null){
			getSkred().getSkredEgenskapList().remove(toRemove);
		}
	}
	
	/**
	 * Legger til en ny egenskapverdi på skredet, sjekker ikke om den finnes fra før.
	 * @param verdi
	 */
	public void addEgenskapverdi(Egenskapverdi verdi){
		SkredEgenskapverdi ny = new SkredEgenskapverdi();
		ny.setEgenskapverdi(verdi);
		ny.setSkred(getSkred());
		ny.setOpprettetDato(Clock.now());
		getSkred().getSkredEgenskapList().add(ny);
	}
	/**
	 * Henter Egenskapverdi med gitt id tilknyttet skredet
	 * @param id
	 * @return
	 */
	public Egenskapverdi getEgenskapveriSkredSingle(Egenskap egenskap){
		for (SkredEgenskapverdi s:skred.getSkredEgenskapList()){
			if (s.getEgenskapverdi().getEgenskap().equals(egenskap)){
				return s.getEgenskapverdi();
			}
		}
		return null;
	}
	
	/**
	 * Sjekker om en gitt egenskapverdi allerede er tilknyttet skredet
	 * @param verdi
	 * @return
	 */
	public boolean isEgenskapVerdiPrensent(Egenskapverdi verdi){
		for (SkredEgenskapverdi s:skred.getSkredEgenskapList()){
			if (s.getEgenskapverdi().equals(verdi))
				return true;
		}
		return false;
	}
	/**
	 * Finner egenskapen med angitt id
	 * @param id
	 * @return
	 */
	public Egenskap findEgenskap(Long id){
		for (Egenskap e:egenskapList){
			if (e.getId().equals(id))
					return e;
		}
		return null;
	}
	
	@SuppressWarnings("serial")
	/**
	 * Wrapperklasse for en Egenskapverdi tilknyttet en JCheckBox
	 */
	public static class EgenskapCheck extends JCheckBox{
		private Egenskapverdi verdi;
		public EgenskapCheck(Egenskapverdi verdi){
			super(verdi.getVerdi());
			this.verdi = verdi;
		}
		public Egenskapverdi getVerdi(){
			return verdi;
		}
	}
}
