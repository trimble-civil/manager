package no.mesta.mipss.mipssfield.inspection;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import no.mesta.mipss.mipssfield.SaveStateAdapter;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

/**
 * Avgjør om lagreknappen skal være på eller av.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SaveStatusObserver extends SaveStateAdapter<Inspeksjon> implements PropertyChangeListener {

	private boolean calculateEnabled() {
		return isChanged() && !isErrors();
	}

	public boolean isEnabled() {
		return calculateEnabled();
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
	}

	public void setDetail(Inspeksjon inspeksjon) {
		detail = inspeksjon;
	}
}