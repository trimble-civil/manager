package no.mesta.mipss.mipssfield;

import java.awt.Component;
import java.util.Collections;
import java.util.Map;

import no.mesta.mipss.mipssfield.discovery.OpenDiscoveryMessage;
import no.mesta.mipss.mipssfield.elrapp.OpenElrappMessage;
import no.mesta.mipss.mipssfield.incident.OpenIncidentMessage;
import no.mesta.mipss.mipssfield.inspection.OpenInspectionMessage;
import no.mesta.mipss.plugin.PluginMessage;

/**
 * Benyttes til å sette opp menypunkter i MIPSS Studio. Bytter faneark.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class OpenTabMessage extends PluginMessage<String,String> {
    public OpenTabMessage() {
    }

    /** {@inheritDoc} */
    public void processMessage() {
        if(isConsumed()) {
            return;
        }
        
        String mode = null;
        if(this instanceof OpenDiscoveryMessage) {
            mode = MipssFieldModule.MODE_DISCOVERIES;
        } else if(this instanceof OpenInspectionMessage) {
            mode = MipssFieldModule.MODE_INSPECTION;
        } else if(this instanceof OpenIncidentMessage) {
            mode = MipssFieldModule.MODE_INCIDENT;
        } else if (this instanceof OpenElrappMessage){
        	mode = MipssFieldModule.MODE_ELRAPP;
        }
        else {
            throw new IllegalStateException("Unkown message type");
        }
        
        MipssFieldModule plugin = (MipssFieldModule) getTargetPlugin();
        plugin.setMode(mode);
        
        setConsumed(true);
    }

    /**
     * Ikke i bruk
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public Map<String, String> getResult() {
        return Collections.EMPTY_MAP;
    }

    /**
     * Ikke i bruk
     * 
     * @return
     */
    public Component getMessageGUI() {
        return null;
    }
}
