package no.mesta.mipss.mipssfield.incident.r11;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import net.sf.jasperreports.engine.JRException;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.MipssFieldDS;
import no.mesta.mipss.reports.R11RapportObjekt;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

/**
 * Panel for å legge inn/redigere R11 skjema
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class R11Panel extends JPanel{

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			return;
		}
		MipssField field = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		HendelseSokView hsv = field.hentHendelseSok(Long.valueOf(701));
		Hendelse h = field.hentHendelseSkred(hsv.getGuid());
//		h.setSkred(null);
		JFrame f = new JFrame();
		f.add(new R11Panel(h));
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
//		
//		new R11Panel(h).runWizard();
	}
	public static final Long SKREDTYPE_ID = 	Long.valueOf(1);
	public static final Long LOSNEOMRADE_ID = 	Long.valueOf(2);
	public static final Long VOLUM_ID = 		Long.valueOf(3);
	public static final Long HOYDE_ID = 		Long.valueOf(4);
	public static final Long SKADE_ID = 		Long.valueOf(5);
	public static final Long STENGING_ID = 		Long.valueOf(6);
	public static final Long BLOKKERTVEI_ID = 	Long.valueOf(7);
	public static final Long VIND_ID = 			Long.valueOf(8);
	public static final Long SNO_ID = 			Long.valueOf(9);
	public static final Long REGN_ID = 			Long.valueOf(10);
	public static final Long FELT_ID = 			Long.valueOf(11);
	private boolean cancelled = true;
	
	private R11Controller controller;

	
	public R11Panel(Hendelse hendelse){
		controller = new R11Controller(hendelse);
		initGui();
	}
	public Skred getSkred(){
		return controller.getSkred();
	}
	public Skred getOriginalSkred(){
		return controller.getOriginalSkred();
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		JLabel tittelLabel = new JLabel(GUITexts.getText(GUITexts.R11_TITTEL));
		Font f = tittelLabel.getFont();
		tittelLabel.setFont(new Font(f.getName(), Font.BOLD, 12));
				
		JButton previewButton = new JButton(getPreviewAction());
		JButton lagreButton = new JButton(getLagreAction());
		JButton avbrytButton = new JButton(getAvbrytAction());
		
		JTabbedPane tab = new JTabbedPane();
		tab.addTab("Info/Sted", new InfoStedPanel(controller));
		tab.addTab("Skred og skade", new SkredSkadePanel(controller));
		tab.addTab(GUITexts.getText(GUITexts.R11_BLOKKERING_TITLE), new BlokkeringVaerPanel(controller));
		
		add(tittelLabel,	new GridBagConstraints(0,0, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10,5,10,5), 0,0));
		add(tab,			new GridBagConstraints(0,1, 3,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,5,0,5), 0,0));
		add(previewButton,	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,15,15), 0,0));
		add(lagreButton, 	new GridBagConstraints(1,2, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,15,5), 0,0));
		add(avbrytButton, 	new GridBagConstraints(2,2, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,0,15,5), 0,0));
	}
	
	public boolean isCancelled(){
		return cancelled;
	}
	public void runWizard(){
		InfoStedPanel inf = new InfoStedPanel(controller);
		SkredSkadePanel skred = new SkredSkadePanel(controller);
		BlokkeringVaerPanel blok = new BlokkeringVaerPanel(controller);
		
		PageInfo i = new PageInfo(inf);
		PageSkred s = new PageSkred(skred);
		PageBlokkering b = new PageBlokkering(blok);
		
		WizardPage[] pages = {i,s,b};
		Wizard wizard = WizardPage.createWizard(pages);
		UIManager.put("wizard.sidebar.image", ImageUtil.convertImageToBufferedImage(IconResources.WIZARD_ICON.getImage()));
		new WizardDialog(null, wizard).startWizard();
	}
	
	private AbstractAction getPreviewAction(){
		return new AbstractAction(GUITexts.getText(GUITexts.R11_PREVIEW_RAPPORT), IconResources.DETALJER_SMALL_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PreviewDS dataSource = new PreviewDS(controller.getSkred());
				dataSource.setFieldBean(BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class));
				JReportBuilder builder = new JReportBuilder(Report.R11_FORM, Report.R11_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, null);
				File report = builder.getGeneratedReportFile();
				if (report != null) {
					DesktopHelper.openFile(report);
				}
			}
		};
	}
	class PreviewDS extends MipssFieldDS<Skred>{

		int c = 0;
		public PreviewDS(Skred skred) {
			super(Skred.class, new ArrayList<String>());
			setFieldBean(BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class));
			setEntity(new R11RapportObjekt(skred));
		}
		protected void loadPageData(){
		}
		public boolean next() throws JRException{
			if (c==0){
				c++;
				return true;	
			}else
			return false;
		}
	}
	
	private AbstractAction getLagreAction(){
		return new AbstractAction("Lagre", IconResources.SAVE_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=false;
				SwingUtilities.windowForComponent(R11Panel.this).dispose();
				
				/*Skred skred = controller.getSkred();
				for (SkredEgenskapverdi v:skred.getSkredEgenskapList()){
					System.out.println(v);
				}*/
			}
		};
	}
	
	private AbstractAction getAvbrytAction(){
		return new AbstractAction("Avbryt", IconResources.CANCEL_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = true;
				SwingUtilities.windowForComponent(R11Panel.this).dispose();
			}
		};
	}
	
	static class PageInfo extends WizardPage{
		public static String getDescription(){
			return "Info/Sted";
		}
		public PageInfo(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	static class PageSkred extends WizardPage{
		public static String getDescription(){
			return "Skred og skade";
		}
		public PageSkred(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	static class PageBlokkering extends WizardPage{
		public static String getDescription(){
			return "Blokkering og vær";
		}
		public PageBlokkering(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	
	private class WizardDialog extends JDialog{
		private final Wizard wizard;
		public WizardDialog(JDialog parent, Wizard wizard){
			super(parent, true);
			this.wizard = wizard;
		}
		public void startWizard(){
			GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0);
			this.setLayout(new GridBagLayout());
			WizardDisplayer.installInContainer(WizardDialog.this, gbc, wizard, null, null, new WizardResultReceiver(){
				@SuppressWarnings("unchecked")
				@Override
				public void cancelled(Map arg0) {
					cancelled = true;
					dispose();
				}
				@Override
				public void finished(Object arg0) {
//					if (validateForm()){
//						cancelled=false;
					cancelled=false;
						dispose();
//					}else{
//						showError(GUITexts.getText(GUITexts.MISSING_FIELD));
//					}
				}			
			});
			setSize(new Dimension(825, 500));
			setLocationRelativeTo(null);
			setVisible(true);
		}
	}
}
