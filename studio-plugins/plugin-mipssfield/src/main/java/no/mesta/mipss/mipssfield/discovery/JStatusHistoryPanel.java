/**
 * 
 */
package no.mesta.mipss.mipssfield.discovery;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.core.MaxLengthEnforcer;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssEntityRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.decorator.SortOrder;

/**
 * Panel med statushistorikk for funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JStatusHistoryPanel extends JPanel implements PropertyChangeListener {
	private static final Logger logger = LoggerFactory.getLogger(JStatusHistoryPanel.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 100);
	public static final Dimension PREF_SIZE = new Dimension(200, 200);
	private static final int STRUT = 2;
	private final Avvik funn;
	private final JDiscoveryDetailPanel parent;
	private JButton slett;
	private JMipssBeanTable<AvvikStatus> table;
	private JScrollPane tableScrollPane;
	private final CloseStatusObserver closeObserver = new CloseStatusObserver();

	/**
	 * Lager panelet med referanse til gitt plugin
	 * 
	 * @param parentPlugin
	 * @param funn
	 */
	public JStatusHistoryPanel(final JDiscoveryDetailPanel parent, final Avvik funn) {
		this.funn = funn;
		this.parent = parent;

		initGUI();
		parent.getFieldToolkit().registerCangeListener(AvvikStatus[].class, funn, "statuser");
		closeObserver.addPropertyChangeListener("enabled", this);
		closeObserver.addPropertyChangeListener("changed", this);
	}

	private JComponent createButtonsPanel() {
		Box buttons = Box.createHorizontalBox();

		JButton ny = new JButton(GUITexts.getText(GUITexts.ADD));
		ny.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				JDiscoveryStatusDialogue dialog = new JDiscoveryStatusDialogue(parent, funn);
				dialog.showRelativeTo(parent);
			}
		});
		ny.setEnabled(false);
//		parent.getFieldToolkit().controlEnabledField(ny);
		decorateButton(ny);
		slett = new JButton(GUITexts.getText(GUITexts.DELETE));
		slett.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				table.clearSelection();
				AvvikStatus ps = funn.getStatus();
				funn.removeStatus(ps);
				if(StringUtils.equalsIgnoreCase(ps.getAvvikstatusType().getStatus(), AvvikstatusType.LUKKET_STATUS)) {
					funn.setLukketDato(null);
				}
			}
		});
		slett.setEnabled(false);
		decorateButton(slett);
		table.addPropertyChangeListener("selectedEntities", this);

		Binding<FieldToolkit, Boolean, CloseStatusObserver, Boolean> closeChange = BindingHelper.createbinding(parent.getFieldToolkit(), "fieldsChanged", closeObserver, "changed");
		Binding<Avvik, String, CloseStatusObserver, String> closeStatus = BindingHelper.createbinding(funn,"status", closeObserver, "status");
		Binding<Avvik, Object, JButton, Object> nyEnabledBind = BindingHelper.createbinding(funn, "${lukketDato==null}", ny, "enabled");
		
		parent.getFieldToolkit().addBinding(closeChange);
		parent.getFieldToolkit().addBinding(closeStatus);
		parent.getFieldToolkit().addBinding(nyEnabledBind);

		buttons.add(Box.createHorizontalGlue());
		buttons.add(slett);
		buttons.add(BoxUtil.createHorizontalStrut(STRUT));
		buttons.add(ny);

		return buttons;
	}

	private void decorateButton(JButton b) {
		ComponentSizeResources.setComponentSize(b, new Dimension(50, 22));
		b.setMargin(new Insets(1, 1, 1, 1));
	}

	protected void initGUI() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		initTableScrollPane();

		add(Box.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, Box.createHorizontalStrut(STRUT), tableScrollPane, Box
				.createHorizontalStrut(STRUT)));
		add(Box.createVerticalStrut(STRUT));
		if (parent.getParentPlugin().hasWriteAccess()) {
			add(BoxUtil.createHorizontalBox(0, Box.createHorizontalStrut(STRUT), createButtonsPanel(), Box
					.createHorizontalStrut(STRUT)));
		}
		add(Box.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	protected void initTableScrollPane() {
		MipssBeanTableModel<AvvikStatus> tableModel = new MipssBeanTableModel<AvvikStatus>("statuser",
				AvvikStatus.class, "opprettetDato", "punktregstatusType", "fritekst", "opprettetAv");
		tableModel.setSourceObject(funn);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				AvvikStatus.class, tableModel.getColumns());
		table = new JMipssBeanTable<AvvikStatus>(tableModel, columnModel);
		table.addMouseListener(new MouseAdapter() {
			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				logger.debug("mouseClicked(" + e + ")");
				if (SwingUtilities.isLeftMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
					if (e.getClickCount() == 2) {
						List<AvvikStatus> statuses = table.getSelectedEntities();
						AvvikStatus status = (statuses != null && statuses.size() > 0 ? statuses.get(0) : null);
						if (status != null) {
							CommentsDialogue dialogue = new CommentsDialogue(status);
							dialogue.show(table);
						}
					}
				}

			}
		});
		table.setFillsViewportWidth(true);
		table.setFillsViewportHeight(true);
		table.setDefaultRenderer(AvvikstatusType.class, new MipssEntityRenderer());
		table.setDefaultRenderer(Date.class, new DateTimeRenderer());
		table.setSortOrder(0, SortOrder.ASCENDING);
		table.setEnabled(false);
		parent.getFieldToolkit().controlEnabledField(table);
		tableScrollPane = new JScrollPane(table);
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == table && StringUtils.equals("selectedEntities", evt.getPropertyName())) {
			List<AvvikStatus> statuser = table.getSelectedEntities();
			if (statuser.size() == 1 && statuser.get(0).equals(funn.getStatus()) && funn.getStatuser().size() > 1) {
				slett.setEnabled(true);
			} else {
				slett.setEnabled(false);
			}
			
		} else if (evt.getSource() == closeObserver) {
			String property = evt.getPropertyName();
			if (funn.getStatus().getAvvikstatusType().getStatus().equals(AvvikstatusType.LUKKET_STATUS)){
				AvvikStatus status = funn.getStatus();
				funn.setLukketDato(status.getOpprettetDato());
			}
			if (StringUtils.equals("enabled", property)) {
				logger.debug("enabled of closeObserver changed");
				parent.getFieldToolkit().setEnableFields(closeObserver.isEnabled());
			} else if (StringUtils.equals("changed", property)) {
				logger.debug("changed of closeObserver changed");
			}
		}
	}

	/**
	 * Lar brukeren
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class CommentsDialogue extends JDialog {
		private final AvvikStatus status;

		public CommentsDialogue(final AvvikStatus status) {
			super(JStatusHistoryPanel.this.parent.getOwner(), true);
			setIconImage(IconResources.HISTORY_ICON.getImage());
			this.status = status;

			Container pane = getContentPane();
			pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

			pane.add(BoxUtil.createVerticalStrut(STRUT));
			pane.add(BoxUtil.createHorizontalBox(0, BoxUtil.createHorizontalStrut(STRUT), createStatusField(), Box
					.createHorizontalGlue(), BoxUtil.createHorizontalStrut(STRUT)));
			pane.add(BoxUtil.createVerticalStrut(STRUT));
			pane.add(BoxUtil.createHorizontalBox(0, BoxUtil.createHorizontalStrut(STRUT), createCommentsPanel(), Box
					.createHorizontalStrut(STRUT)));
			pane.add(BoxUtil.createVerticalStrut(STRUT));
			pane.add(BoxUtil.createHorizontalBox(0, BoxUtil.createHorizontalStrut(STRUT), Box.createHorizontalGlue(),
					createButtons(), BoxUtil.createHorizontalStrut(STRUT)));
			pane.add(BoxUtil.createVerticalStrut(STRUT));
		}

		protected JComponent createButtons() {
			JButton cancelButton = new JButton(IconResources.CANCEL_ICON);
			ComponentSizeResources.setComponentSize(cancelButton, new Dimension(20, 20));
			cancelButton.addActionListener(new ActionListener() {

				/** {@inheritDoc} */
				@Override
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
				}

			});

			JButton saveButton = new JButton(IconResources.SAVE_ICON);
			saveButton.addActionListener(new ActionListener() {

				/** {@inheritDoc} */
				@Override
				public void actionPerformed(ActionEvent e) {
					AvvikStatus fromDb = parent.getMipssField().oppdaterPunktStatus(status);
					status.setFritekst(fromDb.getFritekst());
					setVisible(false);
				}

			});
			ComponentSizeResources.setComponentSize(saveButton, new Dimension(20, 20));

			Box box = BoxUtil.createHorizontalBox(0, cancelButton, BoxUtil.createHorizontalStrut(10), saveButton);
			ComponentSizeResources.setComponentSize(box, new Dimension(50, 20));
			return box;
		}

		protected JComponent createCommentsPanel() {
			JTextArea area = new JTextArea();
			area.setLineWrap(true);
			area.setWrapStyleWord(true);
			Binding<AvvikStatus, String, JTextArea, String> binding = BindingHelper.createbinding(status,
					"fritekst", area, "text", BindingHelper.READ_WRITE);
			binding.bind();

			Integer maxLength = (Integer) status.getAnnotationValue("getFritekst", Column.class, "length");
			MaxLengthEnforcer enforcer = new MaxLengthEnforcer(status, area, "fritekst", maxLength);
			enforcer.start();

			JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			pane.setViewportView(area);
			pane.setPreferredSize(new Dimension(300, 60));
			Box box = BoxUtil.createHorizontalBox(0, pane);
			Dimension d = pane.getPreferredSize();
			Dimension max = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
			ComponentSizeResources.setComponentSizes(box, d, d, max);
			return box;
		}

		protected JComponent createStatusField() {
			JLabel statusField = new JLabel(status.getAvvikstatusType().getTextForGUI() + " ("
					+ MipssDateFormatter.formatDate(status.getOpprettetDato(), true) + ")");
			ComponentSizeResources.setComponentSize(statusField, new Dimension(220, 20));

			Box box = BoxUtil.createHorizontalBox(0, statusField);
			ComponentSizeResources.setComponentSize(box, new Dimension(220, 20));
			return box;
		}

		public void show(JComponent center) {
			pack();
			setLocationRelativeTo(center);
			setVisible(true);
		}
	}
}
