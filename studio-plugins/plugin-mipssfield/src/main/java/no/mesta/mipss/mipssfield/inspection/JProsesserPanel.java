package no.mesta.mipss.mipssfield.inspection;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.vo.InspisertProsess;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.decorator.SortOrder;

public class JProsesserPanel extends JPanel {
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 100);
	public static final Dimension PREF_SIZE = new Dimension(450, 200);
	private static final int STRUT = 2;
	private final Inspeksjon source;
	private JMipssBeanTable<InspisertProsess> table;
	private final JInspectionDetailPanel parent;

	public JProsesserPanel(final JInspectionDetailPanel parent, final Inspeksjon source) {
		this.source = source;
		this.parent = parent;

		initTable();

		JScrollPane tableScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setViewportView(table);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, tableScrollPane));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	public void initTable() {
		MipssBeanTableModel<InspisertProsess> tableModel = new MipssBeanTableModel<InspisertProsess>(
				"inspiserteProsesser", InspisertProsess.class, "adHoc", "kode", "navn", "antallFunn");
		tableModel.setSourceObject(source);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				InspisertProsess.class, tableModel.getColumns());
		table = new JMipssBeanTable<InspisertProsess>(tableModel, columnModel);
		table.setDefaultRenderer(Double.class, new DoubleRenderer());
		table.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		table.setSortable(true);
		table.setSortOrder(1, SortOrder.ASCENDING);
		table.setHorizontalScrollEnabled(true);
		table.setEnabled(false);
		parent.getFieldToolkit().controlEnabledField(table);

		setColumnWidth(table.getColumn(0), 50);
		setColumnWidth(table.getColumn(1), 50);
		setColumnWidth(table.getColumn(2), 150);
		setColumnWidth(table.getColumn(3), 50);
	}

	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}

	private class BooleanRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

		public BooleanRenderer() {
		}

		/** {@inheritDoc} */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			JPanel panel = new JPanel(new BorderLayout());
			panel.setBackground(component.getBackground());
			panel.setForeground(component.getForeground());

			if (value != null && value instanceof Boolean) {
				Boolean b = (Boolean) value;
				JLabel label;
				if (!b) {
					label = new JLabel(GUITexts.getText(GUITexts.YES));
					panel.add(label, BorderLayout.CENTER);
				} else {
					label = new JLabel(GUITexts.getText(GUITexts.NO));
					panel.add(label, BorderLayout.CENTER);
				}
				label.setBackground(component.getBackground());
				label.setForeground(component.getForeground());
			}

			return panel;
		}
	}
}
