package no.mesta.mipss.mipssfield;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.datatransfer.Clipboard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.TransferHandler;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapsgen.Bbox;
import no.mesta.mipss.mapsgen.LineItem;
import no.mesta.mipss.mapsgen.MapItem;
import no.mesta.mipss.mapsgen.MapsGen;
import no.mesta.mipss.mapsgen.PointImage;
import no.mesta.mipss.mapsgen.PointItem;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JMipssImageView;
import no.mesta.mipss.ui.picturepanel.ClipboardImageSupport;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXBusyLabel;

public class JMapPanel extends JAbstractInfoPanel {
	private static PointImage hendelseImage;
	private static final Logger logger = LoggerFactory.getLogger(JMapPanel.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 116);
	public static final Dimension PREF_SIZE = new Dimension(450, 232);
	private static PointImage punktregImage;
	private static final int STRUT = 2;
	private final Clipboard clipboard = getToolkit().getSystemClipboard();
	private final JButton copybutton;
	private final JButton mapbutton = new JButton(GUITexts.getText(GUITexts.MAP));
	private final JMipssImageView mapView = new JMipssImageView();
	private final Box messages = Box.createHorizontalBox();
	private final JButton savebutton;
	private Hendelse hendelse;
	private Inspeksjon inspeksjon;
	private Avvik punktreg;

	public JMapPanel(final MipssFieldModule plugin, final JAbstractDetailPanel<?, ?> parentPanel) {
		// mapView.setAutoFit(true);
		mapView.setLayout(new BorderLayout());
		mapView.setExportFormat("png");
		mapbutton.setMargin(new Insets(1, 1, 1, 1));
		
		mapbutton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				IRenderableMipssEntity entity = parentPanel.getViewObject();
				if (entity instanceof MipssFieldDetailItem){
					List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
					list.add((MipssFieldDetailItem) entity);
					plugin.openItemsInMap(list);
				}
			}
		});
		ComponentSizeResources.setComponentSize(mapbutton, new Dimension(50, 22));

		mapView.add(messages, BorderLayout.NORTH);

		copybutton = new JButton(GUITexts.getText(GUITexts.COPY));
		copybutton.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(copybutton, new Dimension(50, 22));
		copybutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TransferHandler handler = mapView.getTransferHandler();
				handler.exportToClipboard(mapView, clipboard, TransferHandler.COPY);
			}
		});

		savebutton = new JButton();
		savebutton.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(savebutton, new Dimension(50, 22));
		savebutton.setAction(getSaveAction());
		savebutton.setText(GUITexts.getText(GUITexts.SAVE));

		mapView.add(BoxUtil.createVerticalBox(STRUT, BoxUtil.createHorizontalBox(STRUT, Box.createHorizontalGlue(),
				savebutton, copybutton, mapbutton)), BorderLayout.SOUTH);
		mapView.setTransferHandler(new ClipboardImageSupport());

		setFieldToolkit(toolkit);
		setLayout(new BorderLayout());
		add(mapView, BorderLayout.CENTER);

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
		disableButtons();
	}

	private void clearMessages() {
		messages.removeAll();
		mapView.revalidate();
	}

	public void disableButtons() {
		savebutton.setEnabled(false);
		copybutton.setEnabled(false);
		mapbutton.setEnabled(false);
	}

	public void enableButtons() {
		savebutton.setEnabled(true);
		copybutton.setEnabled(true);
		mapbutton.setEnabled(true);
	}

	private ProduksjonService getProdBean() {
		ProduksjonService bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		return bean;
	}

	private List<ReflinkStrekning> getReflinkStrekninger(List<Prodstrekning> strekninger) {
		List<ReflinkStrekning> reflinkStrekninger = getProdBean().getReflinkStrekningFromProdstrekning(strekninger, true);
		return reflinkStrekninger;
	}

	public Action getSaveAction() {
		Action action = new AbstractAction() {
			public void actionPerformed(ActionEvent evt) {
				Image img = mapView.getImage();
				BufferedImage dst = new BufferedImage(img.getWidth(null), img.getHeight(null),
						BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = (Graphics2D) dst.getGraphics();
				// smooth scaling
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
				g.drawImage(img, 0, 0, null);
				g.dispose();
				FileDialog fd = new FileDialog((Frame) SwingUtilities.windowForComponent(JMapPanel.this));
				fd.setMode(FileDialog.SAVE);
				String date = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT);
				date = date.replace(":", "");
				date = date.replace(" ", "_");
				date = date.replace(".", "");
				fd.setFile("kart" + date + ".png");
				fd.setFilenameFilter(new PNGNameFilter());
				fd.setVisible(true);
				if (fd.getFile() != null) {
					String fileName = fd.getFile();
					if (!StringUtils.endsWithIgnoreCase(fileName, ".png")) {
						fileName += ".png";
					}
					try {
						ImageIO.write(dst, "png", new File(fd.getDirectory(), fileName));
					} catch (IOException ex) {
						logger.error("Error writing file", ex);
					}
				}
			}
		};

		action.putValue(Action.NAME, GUITexts.getText(GUITexts.SAVE));
		return action;
	}

	private void setBusyMessage(String msgTxt) {
		JXBusyLabel busy = new JXBusyLabel();
		messages.removeAll();
		messages.add(busy);
		messages.add(BoxUtil.createHorizontalStrut(2));
		messages.add(new JLabel(msgTxt));
		messages.add(Box.createHorizontalGlue());
		busy.setBusy(true);
		mapView.revalidate();
	}

	public void setHendelse(final Hendelse hendelse) {
		this.hendelse = hendelse;
		mapView.setExportName("hendelseKart.png");
		MapPointWorker worker = new MapPointWorker(hendelse.getGuid(), hendelse.getSak().getStedfesting().get(0), getHendelseImage(),
				IconResources.MAP_PIN_RIGHTY_SM);
		worker.execute();
	}

	public void setInspeksjon(Inspeksjon inspeksjon) {
		this.inspeksjon = inspeksjon;
		mapView.setExportName("inspeksjonKart.png");
		MapLinesWorker worker = new MapLinesWorker(inspeksjon.getStrekninger());
		worker.execute();
	}

	private void setMessage(final String msg, final ImageIcon msgIcon) {
		messages.removeAll();
		messages.add(new JLabel(msg, msgIcon, JLabel.LEFT));
		messages.add(Box.createHorizontalGlue());
	}

	public void setPunktreg(final Avvik punktreg) {
		this.punktreg = punktreg;
		mapView.setExportName("funnKart.png");
		MapPointWorker worker = new MapPointWorker(punktreg.getGuid(), punktreg.getStedfesting(), getPunktregImage(),
				IconResources.MAP_PIN_LEFT_SM);
		worker.execute();
	}

	private static PointImage getHendelseImage() {
		if (hendelseImage == null) {
			hendelseImage = new PointImage();
			hendelseImage.setHotspot(new Point(2, 40));
			hendelseImage.setImageData(ImageUtil.imageToBytes(ImageUtil
					.convertImageToBufferedImage(IconResources.MAP_PIN_RIGHTY.getImage())));
		}
		return hendelseImage;
	}

	private static PointImage getPunktregImage() {
		if (punktregImage == null) {
			punktregImage = new PointImage();
			punktregImage.setHotspot(new Point(22, 40));
			punktregImage.setImageData(ImageUtil.imageToBytes(ImageUtil
					.convertImageToBufferedImage(IconResources.MAP_PIN_LEFT.getImage())));
		}
		return punktregImage;
	}

	private class MapLinesWorker extends SwingWorker<Void, Void> {
		private final List<Prodstrekning> strekninger;

		public MapLinesWorker(final List<Prodstrekning> strekninger) {
			this.strekninger = strekninger != null ? strekninger : new ArrayList<Prodstrekning>();
		}

		/** {@inheritDoc} */
		@Override
		protected Void doInBackground() throws Exception {
			mapView.setImage(IconResources.MAP_NORGE_DARK.getImage());
			disableButtons();
			setBusyMessage("Laster kart");
			if (strekninger.size() == 0) {
				clearMessages();
				mapView.setImage(IconResources.MAP_NORGE.getImage());
				return null;
			}

			List<MapItem> items = new ArrayList<MapItem>();
			List<ReflinkStrekning> reflinkStrekninger = getReflinkStrekninger(strekninger);
			for (ReflinkStrekning rs : reflinkStrekninger) {
				LineItem li = new LineItem();
				li.setColor(Color.BLUE);
				li.setCoords(rs.getVectors());
				items.add(li);
			}

			Bbox bbox = new Bbox();
			for (MapItem li : items) {
				bbox.addBounds(li.getBbox());
			}
			double width = bbox.getWidth();
			double height = bbox.getHeight();
			double screenWidth = mapView.getWidth() - 10;
			double screenHeight = mapView.getHeight() - 10;
			double pixelsNeeded = Math.max((width / screenWidth), (height / screenHeight));
			logger.debug("Scale set to: " + pixelsNeeded);
			if (items.size() == 0) {
				clearMessages();
				mapView.setImage(IconResources.MAP_NORGE.getImage());
				return null;
			}
			try {
				MapsGen mapsGen = BeanUtil.lookup(MapsGen.BEAN_NAME, MapsGen.class);
				byte[] mapImagebytes = mapsGen.generateMap(items, new ArrayList<PointImage>(), new Dimension(
						(int) (mapView.getWidth() * 1.5), (int) (mapView.getWidth() * 1.5)), pixelsNeeded);
				final Image mapImage = ImageUtil.bytesToImage(mapImagebytes);
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						mapView.setImage(mapImage);
						clearMessages();
						// mapView.autoFit();
						enableButtons();
					}
				});
			} catch (Throwable t) {
				setMessage(GUITexts.getText(GUITexts.LOADING_FAILED), IconResources.WARNING_ICON);
				logger.warn("doInBackground", t);
			}
			return null;
		}

		/** {@inheritDoc} */
		@Override
		protected void done() {
			// revalidate();
		}
	}

	private class MapPointWorker extends SwingWorker<Void, Void> {
		private final String guid;
		private final PointImage image;
		private final ImageIcon msgIcon;
		private final Punktstedfest sted;

		public MapPointWorker(final String guid, final Punktstedfest stedfesting, final PointImage image,
				final ImageIcon msgIcon) {
			this.guid = guid;
			this.sted = stedfesting;
			this.image = image;
			this.msgIcon = msgIcon;
		}

		/** {@inheritDoc} */
		@Override
		protected Void doInBackground() {
			logger.debug("loading map for " + guid);
			mapView.setImage(IconResources.MAP_NORGE_DARK.getImage());
			disableButtons();
			setBusyMessage("Laster kart");
			if (sted != null) {
				Double bredde = sted.getSnappetY() != null ? sted.getSnappetY() : sted.getY();
				Double lengde = sted.getSnappetX() != null ? sted.getSnappetX() : sted.getX();

				if (bredde != null && lengde != null && bredde != 0 && lengde != 0) {
					logger.debug("Position exists for this discovery:" + bredde + "/" + lengde);
					PointItem pos = new PointItem();
					Point point = new Point();
					point.setLocation(lengde, bredde);
					pos.setPos(point);
					pos.setPointImageIndex(0);

					MapsGen mapsGen = BeanUtil.lookup(MapsGen.BEAN_NAME, MapsGen.class);
					try {
						byte[] mapImagebytes = mapsGen.generateMap(Collections.singletonList((MapItem) pos),
								Collections.singletonList(image), new Dimension(900, 768), 4);
						final Image mapImage = ImageUtil.bytesToImage(mapImagebytes);
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								mapView.setImage(mapImage);
								clearMessages();
								enableButtons();
							}
						});
					} catch (Throwable t) {
						setMessage(GUITexts.getText(GUITexts.LOADING_FAILED), IconResources.WARNING_ICON);
						mapView.setImage(IconResources.MAP_NORGE.getImage());
						logger.warn(guid + ":Loading failed", t);
					}
					clearMessages();
					enableButtons();
				} else {
					setMessage(GUITexts.getText(GUITexts.MISSING_POSITION), msgIcon);
					mapView.setImage(IconResources.MAP_NORGE.getImage());
					logger.warn(guid + ":No position for this item");
				}
			} else {
				setMessage(GUITexts.getText(GUITexts.MISSING_LOCATION), IconResources.WARNING_ICON);
				mapView.setImage(IconResources.MAP_NORGE.getImage());
				logger.warn(guid + ":No location for this item");
			}

			return null;
		}

		/** {@inheritDoc} */
		@Override
		protected void done() {
			logger.debug("done loading map for " + guid);
		}
	}

	private class PNGNameFilter implements FilenameFilter {

		@Override
		public boolean accept(File dir, String name) {
			if (StringUtils.endsWithIgnoreCase(name, ".png")) {
				return true;
			} else {
				return false;
			}
		}

	}
}
