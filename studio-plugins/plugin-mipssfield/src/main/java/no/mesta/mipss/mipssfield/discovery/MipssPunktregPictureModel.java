package no.mesta.mipss.mipssfield.discovery;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.picturepanel.MipssPictureModel;
import no.mesta.mipss.ui.picturepanel.MipssPictureModelEvent;
import no.mesta.mipss.ui.picturepanel.MipssPictureModelListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MipssPunktregPictureModel implements MipssPictureModel, PropertyChangeSource {
	private static final Logger logger = LoggerFactory.getLogger(MipssPunktregPictureModel.class);
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private Object beanInstance;
	private Class<?> beanInterface;
	private String beanMethod;
	private Class<?>[] beanMethodArguments;
	private Object[] beanMethodArgValues;
	private List<AvvikBilde> bilder = new ArrayList<AvvikBilde>();
	private boolean errors;
	private boolean loadingData;
	private List<MipssBeanLoaderListener> loadListeners = new ArrayList<MipssBeanLoaderListener>();
	private Method method;
	private List<MipssPictureModelListener> modelListeners = new ArrayList<MipssPictureModelListener>();
	
	/** {@inheritDoc} */
	@Override
	public void addBilde(Bilde b) {
		throw new IllegalStateException(new OperationNotSupportedException());
	}

	/** {@inheritDoc} */
	@Override
	public void addMipssBeanLoaderListener(MipssBeanLoaderListener l) {
		loadListeners.add(l);
	}

	/** {@inheritDoc} */
	@Override
	public void addMipssPictureModelListener(MipssPictureModelListener l) {
		modelListeners.add(l);
	}

	/**
	 * Konstruerer et metodeobjekt som kan brukes til å laste data
	 * 
	 * @return Metode instansen for gitt <code>beanInteface</code>(interface),
	 *         <code>beanMethod</code>(navn) og <code>beanMethodArguments</code>
	 * @throws java.lang.IllegalStateException
	 *             hvis interface eller metode ikke er satt
	 * @throws java.lang.IllegalArgumentException
	 *             hvis interface ikke har metoden
	 * @see #setBeanMethodArguments(Class[])
	 * @see #setBeanMethod(String)
	 * @see #setBeanInterface(Class)
	 */
	private Method constructMethod() {
		if (beanInterface == null) {
			throw new IllegalStateException("No beanInterface set!");
		}

		if (beanMethod == null) {
			throw new IllegalStateException("No beanMethod set!");
		}

		try {
			method = beanInterface.getMethod(beanMethod, beanMethodArguments);
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException("No such method!", e);
		}

		return method;
	}

	/**
	 * Notifiserer alle lyttere om at loading er ferdig
	 * 
	 * @param e
	 */
	private void fireLoadingFinishedEvent(MipssBeanLoaderEvent e) {
		for (MipssBeanLoaderListener l : loadListeners) {
			l.loadingFinished(e);
		}
	}

	/**
	 * Notifiserer alle lyttere om at loading har startet
	 * 
	 * @param e
	 */
	private void fireLoadingStartedEvent(MipssBeanLoaderEvent e) {
		for (MipssBeanLoaderListener l : loadListeners) {
			l.loadingStarted(e);
		}
	}

	/**
	 * Notifisere om at modellen er endret
	 * 
	 * @param e
	 */
	private void fireModelChanged(MipssPictureModelEvent e) {
		for (MipssPictureModelListener l : modelListeners) {
			l.modelChanged(e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public Bilde getBilde(int index) {
		return bilder.get(index).getBilde();
	}

	/** {@inheritDoc} */
	@Override
	public int getBildeIndex(Bilde b) {
		int index = -1;

		if (b == null) {
			throw new IllegalArgumentException(new NullPointerException("b == null"));
		}

		for (AvvikBilde pb : bilder) {
			if (b.equals(pb.getBilde())) {
				index = bilder.indexOf(pb);
			}
		}
		return index;
	}

	/** {@inheritDoc} */
	@Override
	public List<Bilde> getBilder() {
		List<Bilde> b = new ArrayList<Bilde>();

		for (AvvikBilde pb : bilder) {
			b.add(pb.getBilde());
		}

		return b;
	}

	public AvvikBilde getPunktregBilde(int index) {
		return bilder.get(index);
	}

	public List<AvvikBilde> getPunktregBilder() {
		return bilder;
	}

	public boolean hasErrors() {
		return errors;
	}

	public int indexOf(AvvikBilde pb) {
		return bilder.indexOf(pb);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isLoadingData() {
		return loadingData;
	}

	/**
	 * Laster bilder til modellen. Kan også kaste exceptions kastet av
	 * <code>constructMethod()</code>
	 * 
	 * @throws java.lang.IllegalStateException
	 *             hvis beanInstance ikke er satt
	 * @see java.lang.reflect.InvocationTargetException
	 * @see java.lang.IllegalAccessException
	 * @see #constructMethod()
	 * @see #setBeanInstance(Object)
	 */
	public void loadBilder() {
		if (method == null) {
			method = constructMethod();
		}

		if (beanInstance == null) {
			throw new IllegalStateException("No beanInstance set!");
		}

		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

			/** {@inheritDoc} */
			@SuppressWarnings("unchecked")
			@Override
			protected Void doInBackground() {
				errors = false;
				List<AvvikBilde> bilder;
				try {
					bilder = (List<AvvikBilde>) method.invoke(beanInstance, beanMethodArgValues);
				} catch (IllegalAccessException e) {
					errors = true;
					throw new IllegalArgumentException("No access to method!", e);
				} catch (InvocationTargetException e) {
					errors = true;
					throw new IllegalArgumentException("Method threw exception!", e);
				} catch (Throwable t) {
					errors = true;
					throw new IllegalStateException("Exception occured", t);
				}

				logger.debug("Found " + (bilder == null ? "null" : String.valueOf(bilder.size())) + " pictures");
				setPunktregBilder(bilder);
				return null;
			}

			/** {@inheritDoc} */
			@Override
			protected void done() {
				loadingDone();
			}
		};

		fireLoadingStartedEvent(new MipssBeanLoaderEvent(this, false));
		loadingData = true;
		worker.execute();
	}

	/**
	 * Kalles av tråden som laster data når den er ferdig
	 * 
	 */
	protected void loadingDone() {
		loadingData = false;
		fireLoadingFinishedEvent(new MipssBeanLoaderEvent(this, false));
	}

	/** {@inheritDoc} */
	@Override
	public void removeBilde(Bilde b) {
		throw new IllegalStateException(new OperationNotSupportedException());
	}

	/** {@inheritDoc} */
	@Override
	public void removeMipssBeanLoaderListener(MipssBeanLoaderListener l) {
		loadListeners.remove(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removeMipssPictureModelListener(MipssPictureModelListener l) {
		modelListeners.remove(l);
	}

	public void removePunktregBilde(AvvikBilde pb) {
		String oldValue = getNoOfBildeString();
		bilder.remove(pb);
		props.firePropertyChange("noOfBildeString", oldValue, getNoOfBildeString());
	}

	/**
	 * Setter bønne instansen som har metoden som laster dataene
	 * 
	 * @param beanInstance
	 */
	public void setBeanInstance(Object beanInstance) {
		this.beanInstance = beanInstance;
	}

	/**
	 * Interface for bønna som laster data
	 * 
	 * @param beanInteface
	 */
	public void setBeanInterface(Class<?> beanInteface) {
		this.beanInterface = beanInteface;
	}

	/**
	 * Navnet på bønne metoden
	 * 
	 * @param beanMethod
	 */
	public void setBeanMethod(String beanMethod) {
		this.beanMethod = beanMethod;
	}

	/**
	 * Setter argumenttypene for metode som laster data
	 * 
	 * @param beanMethodArguments
	 */
	public void setBeanMethodArguments(Class<?>[] beanMethodArguments) {
		this.beanMethodArguments = beanMethodArguments;
	}

	/**
	 * Setter metode argument verdiene
	 * 
	 * @param beanMethodArgValues
	 */
	public void setBeanMethodArgValues(Object[] beanMethodArgValues) {
		this.beanMethodArgValues = beanMethodArgValues;
	}

	/** {@inheritDoc} */
	@Override
	public void setBilder(List<Bilde> bilder) {
		throw new IllegalStateException(new OperationNotSupportedException());
	}

	public void setPunktregBilder(List<AvvikBilde> bilder) {
		synchronized (this.bilder) {
			this.bilder = bilder;
			fireModelChanged(new MipssPictureModelEvent(this));
		}
	}

	/** {@inheritDoc} */
	@Override
	public int size() {
		return bilder.size();
	}

	public void addPunktregBilde(AvvikBilde pb) {
		String oldValue = getNoOfBildeString();
		bilder.add(pb);
		props.firePropertyChange("noOfBildeString", oldValue, getNoOfBildeString());
	}

	public String getNoOfBildeString() {
		return "Antall bilder: " + (bilder != null ? bilder.size() : "0");
	}
	
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
}
