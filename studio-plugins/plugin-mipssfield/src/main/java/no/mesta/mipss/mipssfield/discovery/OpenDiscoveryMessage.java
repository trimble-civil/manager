package no.mesta.mipss.mipssfield.discovery;

import no.mesta.mipss.mipssfield.OpenTabMessage;

/**
 * Melding for å åpne funn fanearket.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class OpenDiscoveryMessage extends OpenTabMessage {
    public OpenDiscoveryMessage() {
    }
}
