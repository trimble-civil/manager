package no.mesta.mipss.mipssfield.inspection;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Temperatur;
import no.mesta.mipss.persistence.mipssfield.Vaer;
import no.mesta.mipss.persistence.mipssfield.Vind;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for å vise tid og sted for en inspeksjon
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JAssessmentPanel extends JAbstractInfoPanel implements VetoableChangeListener {
	private static final String ETTERSLEPSREG_FLAGG = "etterslepsregFlagg";
	private static final Logger logger = LoggerFactory.getLogger(JAssessmentPanel.class);
	private static final Integer STRUT = 2;
	public static final Dimension MIN_SIZE = new Dimension(100, 120);
	public static final Dimension PREF_SIZE = new Dimension(450, 120);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 120);
	private static final Dimension FIELD_MIN_SIZE = new Dimension(220, 22);
	private static final Dimension FIELD_PREF_SIZE = new Dimension(450, 22);
	private static final Dimension FIELD_MAX_SIZE = new Dimension(Short.MAX_VALUE, 22);
	private final JCheckBox etterslepBox;
	private final Inspeksjon inspeksjon;
	private final Long originalEtterslep;

	public JAssessmentPanel(final JInspectionDetailPanel parent, final Inspeksjon inspeksjon) {
		logger.debug("created()");
		this.inspeksjon = inspeksjon;
		originalEtterslep = inspeksjon.getEtterslepsregFlagg();
		setFieldToolkit(parent.getFieldToolkit());

		inspeksjon.addVetoableChangeListener(ETTERSLEPSREG_FLAGG, this);
		
		JTextField vaerField = getFieldToolkit().createTextInputField(Inspeksjon.class, inspeksjon, "${vaer.textForGUI}",
				null);
		vaerField.setEditable(false);
		vaerField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(vaerField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JTextField vindField = getFieldToolkit().createTextInputField(Inspeksjon.class, inspeksjon, "${vind.textForGUI}",
				null);
		vindField.setEditable(false);
		vindField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(vindField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JTextField temperaturField = getFieldToolkit().createTextInputField(Inspeksjon.class, inspeksjon, "${temperatur.textForGUI}",
				null);
		temperaturField.setEditable(false);
		temperaturField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(temperaturField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);	

		etterslepBox = getFieldToolkit().createCheckBox(Inspeksjon.class, inspeksjon, "etterslep", null, GUITexts.getText(GUITexts.BACKLOG));
		etterslepBox.setEnabled(false);
		etterslepBox.setEnabled(false);
		etterslepBox.addVetoableChangeListener(this);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Vaer>(GUITexts.getText(GUITexts.WEATHER), vaerField, null), Box
				.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Vind>(GUITexts.getText(GUITexts.WIND), vindField, null), Box
				.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Temperatur>(GUITexts.getText(GUITexts.TEMPERATURE), temperaturField, null), Box
				.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, BoxUtil.createHorizontalStrut(61), etterslepBox, Box
				.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	/** {@inheritDoc} */
	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		if(evt.getSource() == inspeksjon && StringUtils.equals(ETTERSLEPSREG_FLAGG, evt.getPropertyName())) {
			if(!originalEtterslep.equals((Long) evt.getNewValue())) {
				logger.debug("etterslepsregFlagg is read only");
				etterslepBox.setSelected(originalEtterslep == 1L ? true : false);
				throw new PropertyVetoException("Read only!", evt);
			}
		} else if (evt.getSource() == etterslepBox) {
			if(StringUtils.equals("editable", evt.getPropertyName())) {
				throw new PropertyVetoException("Read only!", evt);
			}
		}
	}
}
