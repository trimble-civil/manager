package no.mesta.mipss.mipssfield;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mipssfield.MipssFieldModule.IHF_TYPE;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.DocumentFilter;

public class OpenIHFDialog extends JDialog{
	
	public enum VALUE{
		OK, 
		CANCEL
	}
	private VALUE value;
	private final IHF_TYPE type;
	private JTextField numberField;
	private JButton okButton;
	private JButton cancelButton;
	private final MipssFieldModule plugin;

	
	public OpenIHFDialog(IHF_TYPE type, Window parent, MipssFieldModule plugin){
		super(parent);
		this.plugin = plugin;
		setModal(true);
		setTitle(GUITexts.getText(GUITexts.OPEN)+" "+type.toString()+" med id");
		this.type = type;
		initGui();
	}
	public VALUE getValue(){
		return value;
	}
	private void initGui() {
		setLayout(new BorderLayout());
		JPanel panel = new JPanel(new GridBagLayout());	
		okButton = new JButton(getOkAction());
		cancelButton = new JButton(getCancelAction());
		
		JLabel idLabel = new JLabel("ID:");
		numberField = new JTextField();
		numberField.setDocument(new DocumentFilter(10));
		numberField.setAction(getOkAction());
		panel.add(idLabel, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10,5,5,5), 0,0));
		panel.add(numberField, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10,5,5,5), 0,0));
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(okButton, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		buttonPanel.add(cancelButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0,0));
		
		panel.add(buttonPanel, new GridBagConstraints(0,1, 2,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10,5,10,5), 0,0));
		
		add(panel);
	}
	@SuppressWarnings("serial")
	private AbstractAction getOkAction(){
		return new AbstractAction(GUITexts.getText(GUITexts.OPEN), IconResources.OK2_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Long id = Long.valueOf(numberField.getText());
				MipssField bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
				Long driftkontraktId = plugin.getCommonFilterPanel().getValgtKontrakt().getId();
				
				boolean fail = false;
				Long dkid = null;
				OpenIHFDialog.this.dispose();
				switch (type) {
					case FUNN:
						dkid = bean.getDriftkontraktIdForPunktreg(id);
						if (isCorrectKontrakt(dkid, driftkontraktId))
							plugin.openDiscovery(id);
						else
							fail=true;
						break;
					case HENDELSE:
						dkid = bean.getDriftkontraktIdForHendelse(id);
						if (isCorrectKontrakt(dkid, driftkontraktId))
							plugin.openIncident(id);
						else
							fail=true;
						break;
					case INSPEKSJON:
						dkid = bean.getDriftkontraktIdForInspeksjon(id);
						if (isCorrectKontrakt(dkid, driftkontraktId))
							plugin.openInspection(id);
						else
							fail=true;
						break;
					default:
						break;
				}
				if (fail){
					JOptionPane.showMessageDialog(plugin.getLoader().getApplicationFrame(), "Ugyldig id for valgt kontrakt", "Ugyldig id", JOptionPane.WARNING_MESSAGE);
				}
				
			}
		};
	}
	private boolean isCorrectKontrakt(Long dkid, Long driftkontraktId){
		if (dkid==null)
			return true;
		return dkid.equals(driftkontraktId);
	}
	@SuppressWarnings("serial")
	private AbstractAction getCancelAction(){
		return new AbstractAction("Avbryt", IconResources.NO_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				OpenIHFDialog.this.dispose();
			}
		};
	}
}
