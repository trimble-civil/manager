package no.mesta.mipss.mipssfield.discovery;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Viser en status dialog for funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDiscoveryStatusDialogue extends JDialog {
	private static final Logger logger = LoggerFactory.getLogger(JDiscoveryStatusDialogue.class);
	private static final int STRUT = 5;
	private final Avvik funn;
	private JPunktregStatusPanel panel;
	private final JDiscoveryDetailPanel parent;
	private JButton closeDiscovery;
	private JButton cancel;
	private JButton add;

	public JDiscoveryStatusDialogue(final JDiscoveryDetailPanel parent, final Avvik funn) {
		super(parent.getOwner(), GUITexts.getText(GUITexts.STATUS), true);
		setIconImage(IconResources.EDIT_ICON.getImage());
		this.parent = parent;
		this.funn = funn;
		
		initComponents();
		initGui();
		bind();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(closeDiscovery, new GridBagConstraints(0,0,1,1,0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		buttonPanel.add(cancel, 		new GridBagConstraints(1,0,1,1,1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		buttonPanel.add(add, 			new GridBagConstraints(2,0,1,1,0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0,0));
		
		add(panel, 		 new GridBagConstraints(0,0,1,1,1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,5,5,5), 0,0));
		add(buttonPanel, new GridBagConstraints(0,1,1,1,1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
	}

	private void initComponents() {
		panel = new JPunktregStatusPanel(parent.getParentPlugin(), funn);
		closeDiscovery = new JButton(new CloseAction(IconResources.YELLOW_HELMET_ICON, GUITexts.getText(GUITexts.CLOSE_DISCOVERY)));
		cancel = new JButton(new CancelAction(IconResources.CANCEL_ICON, GUITexts.getText(GUITexts.CANCEL)));
		add = new JButton(new AddAction(IconResources.ADD_ITEM_ICON, GUITexts.getText(GUITexts.ADD)));
	}
	
	private void bind(){
		BindingHelper.createbinding(panel, "${status.punktregstatusType != null}", add, "enabled").bind();
	}


	/**
	 * Viser dialogen
	 * 
	 * @param center
	 */
	public void showRelativeTo(Component center) {
		pack();
		setLocationRelativeTo(center);
		setVisible(true);
	}

	/**
	 * Legger til statusen på funnet
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	private class AddAction extends AbstractAction {

		public AddAction(Icon icon, String text) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent e) {
			logger.debug("actionPerformed() status: " + panel.getStatus());
			funn.addStatus(panel.getStatus());
			setVisible(false);
		}
	}

	/**
	 * Avbryter dialogen
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	private class CancelAction extends AbstractAction {

		public CancelAction(Icon icon, String text) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent e) {
			setVisible(false);
		}
	}

	/**
	 * Lukker et funn
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 * 
	 */
	private class CloseAction extends AbstractAction {

		public CloseAction(Icon icon, String text) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			AvvikStatus status = panel.getStatus();
			ClosePunktregProcess process = new ClosePunktregProcess(parent);
			process.execute(status.getFritekst(), status.getOpprettetDato());
			setVisible(false);
		}
	}
}
