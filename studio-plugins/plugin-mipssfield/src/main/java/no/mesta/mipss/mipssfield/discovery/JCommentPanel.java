package no.mesta.mipss.mipssfield.discovery;

import java.awt.Dimension;

import javax.persistence.Column;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.core.MaxLengthEnforcer;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.ComponentSizeResources;


/**
 * Generell informasjon om funn, dette.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JCommentPanel extends JAbstractInfoPanel {
    private static final String BESKRIVELSE = "beskrivelse";
	@SuppressWarnings("unused")
	private MipssFieldModule parentPlugin;
	public static final Dimension MIN_SIZE = new Dimension(120, 120);
	public static final Dimension PREF_SIZE = new Dimension(200, 120);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 120);
    
    /**
     * Konstruktør
     * 
     * @param parentPlugin
     * @param toolkit
     * @param punktreg
     */
    public JCommentPanel(MipssFieldModule parentPlugin, FieldToolkit toolkit, 
                         Avvik punktreg) {
        this.parentPlugin = parentPlugin;
        setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.COMMENT)));
        setFieldToolkit(toolkit);
        
        JTextArea field = getFieldToolkit().createTextInputArea(Avvik.class, punktreg, BESKRIVELSE);
		field.setLineWrap(true);
		field.setWrapStyleWord(true);
		field.setEnabled(false);

        JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.setViewportView(field);
        JComponent panel = createPanel((JComponent) null , pane);

        Dimension max = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Dimension preferred = new Dimension(400, 140);
        Dimension min = new Dimension(200, 80);
        ComponentSizeResources.setComponentSizes(panel, min, preferred, max);
        ComponentSizeResources.setComponentSizes(pane, min, preferred, max);

        Integer maxLength = (Integer) punktreg.getAnnotationValue("getBeskrivelse", Column.class, "length");
        MaxLengthEnforcer enforcer = new MaxLengthEnforcer(punktreg, field, BESKRIVELSE, maxLength);
        enforcer.start();
        
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(panel);
        add(Box.createVerticalGlue());

        setMinimumSize(MIN_SIZE);
        setPreferredSize(PREF_SIZE);
        setMaximumSize(MAX_SIZE);
    }
}
