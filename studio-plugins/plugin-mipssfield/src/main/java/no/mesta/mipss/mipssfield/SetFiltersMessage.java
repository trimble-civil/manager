package no.mesta.mipss.mipssfield;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.plugin.PluginMessage;

/**
 * Benyttes til å sette søke filter.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SetFiltersMessage extends PluginMessage<String,String> {
	private List<FilterBase> filters;
	
    public SetFiltersMessage() {
    	this(new ArrayList<FilterBase>());
    }

    public SetFiltersMessage(ArrayList<FilterBase> filters) {
		this.filters = filters;
	}

	/** {@inheritDoc} */
    public void processMessage() {
        if(isConsumed()) {
            return;
        }
                
        MipssFieldModule plugin = (MipssFieldModule) getTargetPlugin();
        plugin.setFilters(filters);
        
        setConsumed(true);
    }

    /**
     * Ikke i bruk
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public Map<String, String> getResult() {
        return Collections.EMPTY_MAP;
    }

    /**
     * Ikke i bruk
     * 
     * @return
     */
    public Component getMessageGUI() {
        return null;
    }
}
