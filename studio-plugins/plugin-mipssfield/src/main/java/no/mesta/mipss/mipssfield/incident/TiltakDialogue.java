package no.mesta.mipss.mipssfield.incident;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseTrafikktiltak;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.editablelist.IItemDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

/**
 * Dialog for å legge til estimat. Ikke gjenbrukbar
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class TiltakDialogue extends JDialog implements IItemDialogue<HendelseTrafikktiltak> {
	private String beskrivelse;
	private JTextArea beskrivelseField;
	private boolean cancelled = true;
	private JComboBox comboBox;
	private Trafikktiltak type;
	private BindingGroup group = new BindingGroup();
	private final Logger logger = LoggerFactory.getLogger(TiltakDialogue.class);
	private boolean missingType;
	private final Hendelse hendelse;
	private JLabel validation;

	public TiltakDialogue(final JFrame owner, final Hendelse hendelse, final List<Trafikktiltak> typer) {
		super(owner, true);

		this.hendelse = hendelse;

		setTitle(GUITexts.getText(GUITexts.ACTIONS));

		comboBox = new JComboBox();
		JComboBoxBinding<Trafikktiltak, List<Trafikktiltak>, JComboBox> cb = SwingBindings
				.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ_WRITE, typer, comboBox);
		cb.bind();

		validation = new JLabel((String) null) {
			/** {@inheritDoc} */
			@Override
			public void setText(String txt) {
				if (txt == null) {
					setIcon(null);
				} else {
					setIcon(IconResources.ERROR_ICON_SMALL);
				}
				super.setText(txt);
			}
		};
		validation.setForeground(Color.RED);
		Box validationBox = Box.createHorizontalBox();
		validationBox.add(validation);
		validationBox.add(Box.createHorizontalGlue());

		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				Trafikktiltak t = (Trafikktiltak) cb.getSelectedItem();
				if (t != null && missingType) {
					validation.setText(null);
					pack();
					missingType = false;
				}
			}
		});

		beskrivelseField = new JTextArea();
		beskrivelseField.setLineWrap(true);
		beskrivelseField.setWrapStyleWord(true);
		JScrollPane beskrivelseScroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		beskrivelseScroll.setViewportView(beskrivelseField);
		ComponentSizeResources.setComponentSize(comboBox, new Dimension(280, 22));
		ComponentSizeResources.setComponentSize(beskrivelseScroll, new Dimension(218, 86));

		Box inputBox = Box.createVerticalBox();
		inputBox.add(BoxUtil.createVerticalStrut(2));
		inputBox.add(new LineBox<String>(GUITexts.getText(GUITexts.COMMENT), beskrivelseScroll, null));

		JButton cancel = new JButton(new AbstractAction(GUITexts.getText(GUITexts.CANCEL)) {
			public void actionPerformed(ActionEvent e) {
				close(true);
			}
		});
		cancel.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(cancel, new Dimension(45, 22));

		JButton add = new JButton(new AbstractAction(GUITexts.getText(GUITexts.ADD)) {
			public void actionPerformed(ActionEvent e) {
				Trafikktiltak t = getTrafikktiltakType();

				boolean errors = false;
				if (t == null) {
					missingType = true;
					validation.setText(GUITexts.getText(GUITexts.NO_ESTIMATE_TYPE));
					errors = true;
				}
				
				if (errors) {
					// Feilmelding er skrevet ut, resize vindu slik at alt
					// vises
					pack();
				} else {
					close(false);
				}
			}
		});
		add.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(add, new Dimension(45, 22));

		Box buttons = Box.createHorizontalBox();
		buttons.add(Box.createHorizontalGlue());
		buttons.add(cancel);
		buttons.add(Box.createHorizontalStrut(2));
		buttons.add(add);

		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(2));
		add(BoxUtil.createHorizontalBox(2, comboBox));
		add(Box.createVerticalStrut(2));
		add(validationBox);
		add(Box.createVerticalStrut(2));
		add(BoxUtil.createHorizontalBox(2, inputBox));
		add(Box.createVerticalStrut(2));
		add(BoxUtil.createHorizontalBox(2, buttons));
		add(Box.createVerticalStrut(2));

		ComponentSizeResources.setComponentSize(this, new Dimension(290, 170));
		setResizable(false);
		bindGroup();
	}

	/**
	 * Lukker dialogen
	 * 
	 * @param f
	 */
	private void close(boolean f) {
		cancelled = f;
		setVisible(false);
	}


	/** {@inheritDoc} */
	public void doDialogue(JComponent center) {
		missingType = false;
		cancelled = true;

		pack();
		setLocationRelativeTo((Component) center);
		setVisible(true);
	}


	public String getBeskrivelse() {
		return beskrivelse;
	}

	public Trafikktiltak getTrafikktiltakType() {
		return type;
	}

	/** {@inheritDoc} */
	public HendelseTrafikktiltak getNewValue() {
		HendelseTrafikktiltak tt = new HendelseTrafikktiltak();
		tt.setHendelse(hendelse);
		tt.setTrafikktiltak(getTrafikktiltakType());
		tt.setBeskrivelse(getBeskrivelse());

		unbindGroup();
		return tt;
	}

	public Hendelse getHendelse() {
		return hendelse;
	}

	/**
	 * Lager newEstimate objektet og setter det opp
	 * 
	 */
	private void bindGroup() {
		logger.debug("bindGroup() start");

		Binding<TiltakDialogue, String, JTextArea, String> commentBinding = BindingHelper.createbinding(this,
				"beskrivelse", beskrivelseField, "text", BindingHelper.READ_WRITE);

		// knytter type dropdown til objektet
		Binding<TiltakDialogue, Trafikktiltak, JComboBox, Trafikktiltak> typeBinding = BindingHelper
				.createbinding(this, "type", comboBox, "selectedItem", BindingHelper.READ_WRITE);

		group.addBinding(typeBinding);
		group.addBinding(commentBinding);
		group.bind();
		logger.debug("bindGroup() done");
	}

	/** {@inheritDoc} */
	public boolean isCancelled() {
		return cancelled;
	}


	public void setBeskrivelse(String beskrivelse) {
		String old = this.beskrivelse;
		this.beskrivelse = beskrivelse;
		
		if(beskrivelse != null && beskrivelse.length() > 255) {
			this.beskrivelse = old;
			beskrivelseField.setText(old);
		}
		
		firePropertyChange("beskrivelse", old, beskrivelse);
	}

	public void setType(Trafikktiltak type) {
		Trafikktiltak old = this.type;
		this.type = type;
		firePropertyChange("type", old, type);
	}

	/**
     * 
     */
	private void unbindGroup() {
		logger.debug("unbindGroup()");
		group.unbind();
		for (Binding<?, ?, ?, ?> b : group.getBindings()) {
			group.removeBinding(b);
		}
	}
}
