package no.mesta.mipss.mipssfield.incident;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseTrafikktiltak;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssEntityRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.decorator.SortOrder;

public class JTiltakPanel extends JPanel {
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 100);
	public static final Dimension PREF_SIZE = new Dimension(450, 200);
	private final JIncidentDetailPanel parent;
	private final Hendelse hendelse;
	private JMipssBeanTable<HendelseTrafikktiltak> table;
	private static final int STRUT = 2;

	public JTiltakPanel(final JIncidentDetailPanel parent, final Hendelse hendelse) {
		this.parent = parent;
		this.hendelse = hendelse;

		parent.getFieldToolkit().registerCangeListener(Hendelse.class, this.hendelse, "tiltak");

		initTable();

		JScrollPane tableScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setViewportView(table);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, tableScrollPane));
		add(BoxUtil.createVerticalStrut(STRUT));
		if (parent.getParentPlugin().hasWriteAccess()) {
			add(BoxUtil.createHorizontalBox(STRUT, createButtonsPanel()));
			add(BoxUtil.createVerticalStrut(STRUT));
		}

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	private JComponent createButtonsPanel() {
		Box buttons = Box.createHorizontalBox();

		JButton add = new JButton(GUITexts.getText(GUITexts.ADD));
		add.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				TiltakDialogue dialog = new TiltakDialogue(parent.getOwner(), hendelse, parent.getParentPlugin()
						.getTrafikktiltaksTyper());
				dialog.doDialogue(table);
				if (!dialog.isCancelled()) {
					HendelseTrafikktiltak nyttTiltak = dialog.getNewValue();
					if (hendelse.getTiltak().contains(nyttTiltak)) {
						List<HendelseTrafikktiltak> tiltak = hendelse.getTiltak();
						HendelseTrafikktiltak eksisterendeTiltak = tiltak.get(tiltak.indexOf(nyttTiltak));
						eksisterendeTiltak.setBeskrivelse(nyttTiltak.getBeskrivelse());
						table.repaint();
					} else {
						table.clearSelection();
						hendelse.addTrafikktiltak(nyttTiltak);
					}
				}
			}
		});
		decorateButton(add);
		JButton fjern = new JButton(GUITexts.getText(GUITexts.DELETE));
		fjern.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				List<HendelseTrafikktiltak> selection = table.getSelectedEntities();
				if (selection != null && selection.size() > 0) {
					table.clearSelection();
					for (HendelseTrafikktiltak t : selection) {
						hendelse.removeTrafikktiltak(t);
					}
				}
			}
		});
		decorateButton(fjern);
		Binding<JMipssBeanTable<HendelseTrafikktiltak>, Boolean, JButton, Boolean> fjernBinding = BindingHelper.createbinding(
				table, "${noOfSelectedRows > 0}", fjern, "enabled");
		parent.getFieldToolkit().addBinding(fjernBinding);

		buttons.add(Box.createHorizontalGlue());
		buttons.add(fjern);
		buttons.add(BoxUtil.createHorizontalStrut(STRUT));
		buttons.add(add);

		return buttons;
	}

	private void decorateButton(JButton b) {
		ComponentSizeResources.setComponentSize(b, new Dimension(50, 22));
		b.setMargin(new Insets(1, 1, 1, 1));
	}

	private void initTable() {
		MipssBeanTableModel<HendelseTrafikktiltak> tableModel = new MipssBeanTableModel<HendelseTrafikktiltak>("tiltak",
				HendelseTrafikktiltak.class, "type", "beskrivelse");
		tableModel.setSourceObject(hendelse);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				HendelseTrafikktiltak.class, tableModel.getColumns());
		table = new JMipssBeanTable<HendelseTrafikktiltak>(tableModel, columnModel);
		table.setDefaultRenderer(Trafikktiltak.class, new MipssEntityRenderer());
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		table.setSortable(true);
		table.setSortOrder(0, SortOrder.ASCENDING);
		table.setHorizontalScrollEnabled(true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					table.selectRow(e);
					HendelseTrafikktiltak tiltak = table.getSelectedEntities().get(0);
					TiltakDialogue dialog = new TiltakDialogue(parent.getOwner(), hendelse, parent.getParentPlugin()
							.getTrafikktiltaksTyper());
					dialog.setType(tiltak.getTrafikktiltak());
					dialog.setBeskrivelse(tiltak.getBeskrivelse());
					dialog.doDialogue(table);
					HendelseTrafikktiltak editTiltak = dialog.getNewValue();
					if (!dialog.isCancelled()) {
						table.clearSelection();
						hendelse.removeTrafikktiltak(tiltak);
						hendelse.addTrafikktiltak(editTiltak);
					}
				}
			}
		});
	}
}
