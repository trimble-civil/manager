package no.mesta.mipss.mipssfield;

import java.awt.Dimension;
import java.util.List;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JDateTimePicker;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Converter;
import org.jdesktop.swingx.JXDatePicker;

/**
 * Panel klasse for å bygge felter og knytte de sammen
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public abstract class JAbstractInfoPanel extends JPanel{
    protected FieldToolkit toolkit;

    /**
     * Lager en ledetekst
     * 
     * @param labelTxt
     * @return
     */
    private JComponent createLabel(String labelTxt) {
        return new JLabel(labelTxt);
    }
    
    /**
     * Lager et panel for label og input
     * 
     * @param label
     * @param input
     * @return
     */
    protected JComponent createPanel(JComponent label, JComponent input) {
        Box box = Box.createHorizontalBox();
        if(label != null) {
            box.add(label);
            box.add(Box.createHorizontalStrut(10));
        }
        
        box.add(Box.createHorizontalGlue());
        box.add(input);
        
        return box;
    }
    
    protected JComponent createInputTextFieldPanel(String labelTxt, Class<?> clazz, Object instance, String instanceField, Converter<?,?> converter) {
        JComponent label = null;
        if(labelTxt != null) {
            label = createLabel(labelTxt);
        }
        
        if(getValue(instance, instanceField) == null) {
            setValue(instance, instanceField, "");
        }
        
        JTextField field = getFieldToolkit().createTextInputField(clazz, instance, instanceField, converter);

        setComponentSizes(field);
        
        return createPanel(label, field);
    }
    
    protected JComponent createInputTextAreaPanel(String labelTxt, Class<?> clazz, Object instance, String instanceField) {
        JComponent label = null;
        if(labelTxt != null) {
            label = createLabel(labelTxt);
        }

        if(getValue(instance, instanceField) == null) {
            setValue(instance, instanceField, "");
        }
        
        JTextArea field = getFieldToolkit().createTextInputArea(clazz, instance, instanceField);
		field.setLineWrap(true);
		field.setWrapStyleWord(true);

        JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.setViewportView(field);
        JComponent panel = createPanel(label, pane);

        Dimension max = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Dimension preferred = new Dimension(400, 140);
        Dimension min = new Dimension(200, 80);
        ComponentSizeResources.setComponentSizes(panel, min, preferred, max);
        ComponentSizeResources.setComponentSizes(pane, min, preferred, max);
        
        return panel;
    }
    
    protected JComponent createDatePickerPanel(String labelTxt, Class<?> clazz, Object instance, String instanceField, String dateFormat) {
        JComponent label = null;
        if(labelTxt != null) {
            label = createLabel(labelTxt);
        }

        JXDatePicker field = getFieldToolkit().createDatePicker(clazz, instance, instanceField, dateFormat);

        JComponent panel = createPanel(label, field);
        
        setComponentSizes(panel);
        
        return panel;
    }

    protected JComponent createDateTimePickerPanel(String labelTxt, Class<?> clazz, Object instance, String instanceField, String dateFormat) {
        JComponent label = null;
        if(labelTxt != null) {
            label = createLabel(labelTxt);
        }

        JDateTimePicker field = getFieldToolkit().createDateTimePicker(clazz, instance, instanceField, dateFormat);

        JComponent panel = createPanel(label, field);
        
        setComponentSizes(panel);
        
        return panel;
    }

    protected<T extends IRenderableMipssEntity> JComponent createDropdownPanel(String labelTxt, Class<?> clazz, Object instance, String instanceField, Converter<?,?> converter, Class<T> listClazz, List<T> list) {
        JComponent label = null;
        if(labelTxt != null) {
            label = createLabel(labelTxt);
        }

        JComboBox field = getFieldToolkit().createDropDown(clazz, instance, instanceField, converter, listClazz, list);

        JComponent panel = createPanel(label, field);
        
        setComponentSizes(panel);
        
        return panel;
    }
    
    protected JComponent createCheckBoxPanel(String labelTxt, Class<?> clazz, Object instance, String instanceField, Converter<?,?> converter) {
        JComponent label = null;
        if(labelTxt != null) {
            label = createLabel(labelTxt);
        }

        JCheckBox field = getFieldToolkit().createCheckBox(clazz, instance, instanceField, converter);
        
        JComponent panel = createPanel(label, field);
        
        setComponentSizes(panel);
        
        return panel;
    }
    
    protected void bind() {
        toolkit.bind();
    }

    public void setFieldToolkit(FieldToolkit toolkit) {
        this.toolkit = toolkit;
    }

    public FieldToolkit getFieldToolkit() {
        return toolkit;
    }
    
    @SuppressWarnings("unchecked")
	private Object getValue(Object instance, String field) {
        BeanProperty property = BeanProperty.create(field);
        
        return property.getValue(instance);
    }
    
    @SuppressWarnings("unchecked")
	private void setValue(Object instance, String field, Object value) {
        BeanProperty property = BeanProperty.create(field);
        
        property.setValue(instance, value);
    }
    
    private void setComponentSizes(JComponent component) {
        Dimension max = new Dimension(800, 20);
        Dimension preferred = new Dimension(400, 20);
        Dimension min = new Dimension(50, 20);
        
        ComponentSizeResources.setComponentSizes(component, min, preferred, max);
    }
}
