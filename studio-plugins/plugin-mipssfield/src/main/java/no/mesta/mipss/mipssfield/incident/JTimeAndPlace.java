package no.mesta.mipss.mipssfield.incident;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.NullObjectValidator;
import no.mesta.mipss.core.PunktveirefValidator;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.JRoadDialogue;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.mipssutils.MipssServerUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JDateTimePicker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Panel for å vise tid og sted for et funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JTimeAndPlace extends JAbstractInfoPanel {
	private static final String DATO = "dato";
	private static final Logger logger = LoggerFactory.getLogger(JTimeAndPlace.class);
	private static final Integer STRUT = 2;
	public static final Dimension MIN_SIZE = new Dimension(100, 189);
	public static final Dimension PREF_SIZE = new Dimension(450, 189);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 189);
	private static final Dimension FIELD_MIN_SIZE = new Dimension(220, 22);
	private static final Dimension FIELD_PREF_SIZE = new Dimension(450, 22);
	private static final Dimension FIELD_BIG_PREF_SIZE = new Dimension(450 + 37, 22);
	private static final Dimension FIELD_BIGGER_PREF_SIZE = new Dimension(450 + (37 * 2), 22);
	private static final Dimension FIELD_MAX_SIZE = new Dimension(Short.MAX_VALUE, 22);
	private static final Dimension BUTTON_SIZE = new Dimension(37, 22);
	private final JIncidentDetailPanel parent;
	private final Hendelse hendelse;
	private final JButton veiEdit = new JButton();
	private final LineBox<Punktveiref> veiBox;

	public JTimeAndPlace(final JIncidentDetailPanel parent, final Hendelse hendelse) {
		logger.debug("setting up kontraktField");
		this.parent = parent;
		this.hendelse = hendelse;

		setFieldToolkit(parent.getFieldToolkit());

		JTextField kontraktField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse,
				"${kontrakt.textForGUI}", null);
		kontraktField.setEditable(false);
		ComponentSizeResources.setComponentSizes(kontraktField, FIELD_MIN_SIZE, FIELD_BIGGER_PREF_SIZE, FIELD_MAX_SIZE);

		JTextField refnummerField = toolkit.createTextInputField(Hendelse.class, hendelse, "refnummer", null);
		ComponentSizeResources.setComponentSizes(refnummerField, new Dimension(100,22), new Dimension(100, 22), new Dimension(100, 22));
		refnummerField.setEditable(false);
		refnummerField.setEnabled(false);
		
		JDateTimePicker datoField = getFieldToolkit().createDateTimePicker(Hendelse.class, hendelse, DATO,
				MipssDateFormatter.SHORT_DATE_FORMAT);

		JTextField opprettetDatoField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse,
				"${ownedMipssEntity.opprettetDatoString}", null);
		opprettetDatoField.setEditable(false);
		ComponentSizeResources.setComponentSizes(opprettetDatoField, FIELD_MIN_SIZE, FIELD_BIGGER_PREF_SIZE, FIELD_MAX_SIZE);
		
		logger.debug("setting up veiField");
		JTextField veiField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse, "${veiref.textForGUI}", null);
		veiField.setEditable(false);
		ComponentSizeResources.setComponentSizes(veiField, FIELD_MIN_SIZE, FIELD_BIG_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up posisjon");
		JTextField stedfestField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse,
				"${stedfesting.textForGUI}", null);
		stedfestField.setEditable(false);
		ComponentSizeResources.setComponentSizes(stedfestField, FIELD_MIN_SIZE, FIELD_BIG_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up kommune");
		JTextField kommuneField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse, "kommune", null);
		ComponentSizeResources.setComponentSizes(kommuneField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up skadested");
		JTextField skadestedField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse, "skadested", null);
		ComponentSizeResources.setComponentSizes(skadestedField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		veiEdit.setAction(new PlaceIncidentAction());
		veiEdit.setText(GUITexts.getText(GUITexts.EDIT));
		veiEdit.setMargin(new Insets(1, 1, 1, 1));
		ComponentSizeResources.setComponentSize(veiEdit, BUTTON_SIZE);
		if(!parent.getParentPlugin().hasWriteAccess()) {
			veiEdit.setVisible(false);
		}

		logger.debug("setting up inspeksjonField");
		JTextField inspeksjonField = toolkit.createTextInputField(Hendelse.class, hendelse, "${inspeksjon.textForGUI}",
				null);
		inspeksjonField.setEditable(false);
		inspeksjonField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(inspeksjonField, FIELD_MIN_SIZE, FIELD_BIG_PREF_SIZE, FIELD_MAX_SIZE);
		JButton inspeksjonLink = new JButton(GUITexts.getText(GUITexts.OPEN));
		inspeksjonLink.setMargin(new Insets(1, 1, 1, 1));
		inspeksjonLink.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO utkommentert pga modellendring
//				if (hendelse.getInspeksjonGuid() != null) {
//					if (!parent.getOwner().navigateToDetailTab(hendelse.getInspeksjon())) {
//						InspeksjonSokView s = parent.getParentPlugin().getMipssField().hentInspeksjonSok(
//								hendelse.getInspeksjonGuid());
//						if (s != null) {
//							parent.getOwner().addDetail(s);
//						}
//					}
//				}

			}
		});
		final String enabledField = "enabled";
		Binding<Hendelse, Boolean, JButton, Boolean> enableInspeksjonLink = BindingHelper.createbinding(hendelse,
				"${inspeksjonGuid != null}", inspeksjonLink, enabledField);
		getFieldToolkit().addBinding(enableInspeksjonLink);
		ComponentSizeResources.setComponentSize(inspeksjonLink, BUTTON_SIZE);

		PunktveirefValidator<Hendelse> veirefValidator = null;
		if (parent.isNewHendelse()) {
			veirefValidator = new PunktveirefValidator<Hendelse>(hendelse, "veiref");
			parent.getSaveStateObserver().addValidator(veirefValidator);
		}
		veiBox = new LineBox<Punktveiref>(GUITexts.getText(GUITexts.ROAD), veiField, veirefValidator);

		JLabel idLabel = new JLabel(GUITexts.getText(GUITexts.ID));
		ComponentSizeResources.setComponentSize(idLabel, new Dimension(22, 22));
		idLabel.setHorizontalAlignment(JLabel.RIGHT);
		idLabel.setHorizontalTextPosition(JLabel.RIGHT);
		idLabel.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
		
		setFieldToolkit(toolkit);
		bindRoad();

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Driftkontrakt>(GUITexts.getText(GUITexts.CONTRACT), kontraktField, null), 
				idLabel, refnummerField, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Date>(GUITexts.getText(GUITexts.INCIDENT_DATE), datoField,
				new NullObjectValidator<Hendelse, Date>(hendelse, DATO)), BoxUtil.createHorizontalStrut(1), 
				new LineBox<Driftkontrakt>(GUITexts.getText(GUITexts.REGISTRERT), opprettetDatoField, null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, veiBox, BoxUtil.createHorizontalStrut(1), veiEdit, Box
				.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Punktstedfest>(GUITexts.getText(GUITexts.POSITION),
				stedfestField, null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.INCIDENT_PLACE),
				skadestedField, null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.COUNTY), kommuneField,
				null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Inspeksjon>(GUITexts.getText(GUITexts.INSPECTION),
				inspeksjonField, null), BoxUtil.createHorizontalStrut(1), inspeksjonLink, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	private void bindRoad() {
		String[] fields = new String[] { "stedfesting", "veiref", "${veiref.fylkesnummer}", "${veiref.kommunenummer}",
				"${veiref.veikategori}", "${veiref.veistatus}", "${veiref.veinummer}", "${veiref.hp}",
				"${veiref.meter}", "${veiref.kjorefelt}" };

		for (String field : fields) {
			getFieldToolkit().registerCangeListener(Hendelse.class, hendelse, field);
		}
	}

	/**
	 * Kjører opp en dialog hvor man stedfester funnet langs veien
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class PlaceIncidentAction extends AbstractAction {
		private Punktveiref vei = null;

		/**
		 * Konstruktør
		 * 
		 * @param icon
		 * @param vei
		 */
		public PlaceIncidentAction() {
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {		
			vei = parent.getDetailObject().getSak().getVeiref();
			if (vei == null) {
				vei = new Punktveiref();
				vei.setFraDato(Clock.now());
				//TODO utkommentert pga modellendringer
//				if (hendelse.getSak().getStedfesting() != null) {
//					hendelse.getSak().setVeiref(vei);
//				} else {
//					//det finnes ingen stedfesting på hendelsen, opprett en...
//					Punktstedfest ps = new Punktstedfest();
//					String guid = ServerUtils.getInstance().fetchGuid();
//					ps.setGuid(guid);
//					//TODO utkommentert pga modellendring
////					ps.setHendelse(hendelse);
//					
//					vei.setPunktstedfest(ps);
//					List<Punktstedfest> plist = new ArrayList<Punktstedfest>();
//					plist.add(ps);
//					hendelse.setStedfestingList(plist);
//					hendelse.setVeiref(vei);
//				}
			}

			JRoadDialogue dialogue = new JRoadDialogue(parent.getOwner(), null, vei, parent.getParentPlugin().getMipssField());
			if (!dialogue.isCancelled()) {

				vei.setFylkesnummer(dialogue.getFylke());
				vei.setKommunenummer(dialogue.getKommune());
				vei.setVeikategori(dialogue.getVeikat());
				vei.setVeistatus(dialogue.getVeistat());
				vei.setVeinummer(dialogue.getVeinr());
				vei.setHp(dialogue.getHp());
				vei.setMeter(dialogue.getMeter());
				vei.setKjorefelt(dialogue.getFelt());

				Punktstedfest sted = parent.getParentPlugin().getMipssField().calculatePunktstedfest(vei);
				vei.getPunktstedfest().setX(sted.getX());
				vei.getPunktstedfest().setY(sted.getY());
				vei.getPunktstedfest().setSnappetX(sted.getSnappetX());
				vei.getPunktstedfest().setSnappetY(sted.getSnappetY());
				vei.getPunktstedfest().setSnappetH(sted.getSnappetH());
				vei.getPunktstedfest().setReflinkPosisjon(sted.getReflinkPosisjon());
				vei.getPunktstedfest().setReflinkIdent(sted.getReflinkIdent());

				if(dialogue.isValidated()) {
					parent.redrawMap();
				}
			}
		}
	}
}
