package no.mesta.mipss.mipssfield.inspection;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractDetailPanel;
import no.mesta.mipss.mipssfield.JPunktregPanel;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.BooleanRenderer;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssEntityRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.decorator.SortOrder;

public class JObservationsPanel extends JPanel {
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(600, 100);
	public static final Dimension PREF_SIZE = new Dimension(900, 200);
	private static final int STRUT = 2;
	private final JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> parent;
	private final Object source;
	private JMipssBeanTable<Hendelse> hendelseTable;
	private MipssBeanTableModel<Hendelse> tableModel;

	public JObservationsPanel(final JInspectionDetailPanel parent, final Inspeksjon source) {
		this.parent = parent;
		this.source = source;

		initHendelseTable();

		JPunktregPanel punktregPanel = new JPunktregPanel(parent, source);

		JTabbedPane pane = new JTabbedPane();
		pane.add(GUITexts.getText(GUITexts.NO_OF_DISCOVERIES_TITLE, punktregPanel.getTableModel().getSize()),
				punktregPanel);

		JScrollPane tableScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setViewportView(hendelseTable);
		Box hendelseBox = Box.createVerticalBox();
		hendelseBox.add(BoxUtil.createVerticalStrut(STRUT));
		hendelseBox.add(BoxUtil.createHorizontalBox(STRUT, tableScrollPane));
		hendelseBox.add(BoxUtil.createVerticalStrut(STRUT));

		pane.add(GUITexts.getText(GUITexts.NO_OF_INCIDENTS_TITLE, tableModel.getSize()), hendelseBox);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, pane));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	private void initHendelseTable() {
		tableModel = new MipssBeanTableModel<Hendelse>("hendelser", Hendelse.class, "dato", "ownedMipssEntity", "type",
				"aarsak", "tiltakstekster", "veiref", "antallFunn", "antallBilder", "harInspeksjon",
				"erForsikringssak", "henvendelseFra", "henvendelseMottattAv");
		tableModel.setSourceObject(source);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Hendelse.class,
				tableModel.getColumns());
		MipssEntityRenderer entityRendrer = new MipssEntityRenderer();
		hendelseTable = new JMipssBeanTable<Hendelse>(tableModel, columnModel);
		hendelseTable.setDefaultRenderer(Punktveiref.class, entityRendrer);
		hendelseTable.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		hendelseTable.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
		hendelseTable.setDefaultRenderer(Hendelseaarsak.class, entityRendrer);
		hendelseTable.setDefaultRenderer(OwnedMipssEntity.class, new OpprettetRenderer());
		hendelseTable.setFillsViewportHeight(true);
		hendelseTable.setFillsViewportWidth(true);
		hendelseTable.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		hendelseTable.setSortable(true);
		hendelseTable.setSortOrder(0, SortOrder.ASCENDING);
		hendelseTable.setHorizontalScrollEnabled(true);
		hendelseTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					hendelseTable.selectMouseSelection(e);
					Hendelse hendelse = hendelseTable.getSelectedEntities().get(0);
					if (!parent.getOwner().navigateToDetailTab(hendelse)) {
						parent.getOwner().addDetail(HendelseSokView.newInstance(hendelse));
					}
				}
			}
		});
		hendelseTable.setEnabled(false);
		parent.getFieldToolkit().controlEnabledField(hendelseTable);

		setColumnWidth(hendelseTable.getColumn(0), 80); // dato
		setColumnWidth(hendelseTable.getColumn(1), 80); // opprettet av
		setColumnWidth(hendelseTable.getColumn(2), 90); // type
		setColumnWidth(hendelseTable.getColumn(3), 70); // årsak
		setColumnWidth(hendelseTable.getColumn(5), 120); // hp
		setColumnWidth(hendelseTable.getColumn(6), 45); // funn
		setColumnWidth(hendelseTable.getColumn(7), 45); // bilder
		setColumnWidth(hendelseTable.getColumn(8), 30); // insp
		setColumnWidth(hendelseTable.getColumn(9), 30); // r5
		setColumnWidth(hendelseTable.getColumn(10), 80); // fra
		setColumnWidth(hendelseTable.getColumn(11), 80); // av
	}

	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}

	private class OpprettetRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

		public OpprettetRenderer() {
		}

		/** {@inheritDoc} */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			JPanel panel = new JPanel(new BorderLayout());
			panel.setBackground(component.getBackground());
			panel.setForeground(component.getForeground());

			if (value != null && value instanceof OwnedMipssEntity) {
				OwnedMipssEntity o = (OwnedMipssEntity) value;
				if (o != null) {
					JLabel label = new JLabel(o.getOpprettetAv());
					label.setBackground(component.getBackground());
					label.setForeground(component.getForeground());
					panel.add(label, BorderLayout.CENTER);
				}
			}

			return panel;
		}
	}
}
