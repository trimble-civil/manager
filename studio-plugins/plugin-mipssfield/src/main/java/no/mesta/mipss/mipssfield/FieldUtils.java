package no.mesta.mipss.mipssfield;

import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.VeiInfoUtil;


/**
 * Hjelpeklasse for å lage div strenger og kalkulasjoner.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class FieldUtils {	
    /**
     * Kalkulerer skaleringen for et bilde
     * 
     * @param itemWidth
     * @param itemHeight
     * @param targetWidth
     * @param targetHeight
     * @return
     */
    public static double calculateImageScale(int itemWidth, int itemHeight, int targetWidth, int targetHeight) {
        double scale = 1;
        if(itemWidth > itemHeight) {
            scale = (double)targetWidth/ (double)itemWidth;
        } else {
            scale = (double)targetHeight/(double)itemHeight;
        }
        
        return scale;
    }
    
    /**
     * Bygger opp en string som kan benyttes i GUI for å vise en veistrekning
     * 
     * @param vei
     * @return
     */
    public static String buildRoadInfo(Punktveiref vei) {
        return VeiInfoUtil.getVeiStedfest(vei);
    }
}
