package no.mesta.mipss.mipssfield.incident;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JTopLine;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.inspection.JInspectionActions;
import no.mesta.mipss.mipssfield.inspection.JInspectionFilter;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Oversiktspanel for hendelser
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JIncidentPanel extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(JIncidentPanel.class);
	private JMipssBeanTable<HendelseSokView> table;
	private MipssFieldModule parentPlugin;
	private List<HendelseSokView> selection;
	private JPopupMenu menu;
	private JIncidentFilter incidentFilter;
	private JIncidentActions incidentActions;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	public JIncidentPanel(MipssFieldModule parentPlugin) {
		logger.debug("JIncidentPanel() start");
		this.parentPlugin = parentPlugin;

		table = parentPlugin.getIncidentTable();
		table.setFillsViewportHeight(true);
		table.addMouseListener(new MouseAdapter() {
			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
					if (e.getClickCount() == 2) {
						checkTableSelection(e);
						openIncidents();
					}
				}
			}

			/** {@inheritDoc} */
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
					checkTableSelection(e);

					// Vis høyreknapp menyen
					initAndShow(e.getLocationOnScreen());
				}
			}

			/**
			 * Viser høyre menyen på oppgitt posisjon
			 * 
			 * @param pos
			 */
			private void initAndShow(Point pos) {
				if (menu == null) {
					menu = new JPopupMenu();
					menu.add(createMenuItem(new OpenAction(GUITexts.getText(GUITexts.DETAILS), null)));
					menu.add(createMenuItem(new MapAction(GUITexts.getText(GUITexts.MAP), null)));

					menu.addMouseListener(new MouseAdapter() {
						/** {@inheritDoc} */
						public void mouseExited(MouseEvent e) {
							menu.setVisible(false);
						}
					});
				}

				pos.translate(-15, -15);
				menu.setLocation(pos);
				menu.setVisible(true);
			}

			/**
			 * Lager et menu item for menyen
			 * 
			 */
			private JMenuItem createMenuItem(Action a) {
				JMenuItem item = new JMenuItem(a);
				item.addMouseListener(new ItemHighlighter(item));
				return item;
			}
		});

		JMipssBeanScrollPane scroll = new JMipssBeanScrollPane(table, parentPlugin.getIncidentTableModel());
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		setLayout(new GridBagLayout());
		incidentFilter = new JIncidentFilter(parentPlugin);
		incidentActions = new JIncidentActions(parentPlugin);
		add(incidentFilter, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(scroll, new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(incidentActions, new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		
//		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

//		add(new JTopLine(parentPlugin.getIncidentColor()));
//		incidentFilter = new JIncidentFilter(parentPlugin);
//		add(incidentFilter);
//		add(scroll);
//		incidentActions = new JIncidentActions(parentPlugin);
//		add(incidentActions);
		logger.debug("JIncidentPanel() end");
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		incidentFilter.dispose();
		incidentActions.dispose();
		incidentFilter = null;
		incidentActions = null;
		parentPlugin = null;
	}
	/**
	 * Logikk for høyreklikk valg i tabellen Sørger for at valgte rader i
	 * tabellen oppfører seg logisk:
	 * 
	 * <ol>
	 * <li>Hvis ingen rader er valg, velg raden under musen i det høyremusknapp
	 * trykkes</li>
	 * <li>Hvis en eller flere rader er valgt, og raden under musen er med i
	 * utvalget: Da skal utvalget beholdes</li>
	 * <li>Hvis en eller flere rader er valgt, og raden under musen ikke er med
	 * i utvalget: Velg da kun raden under musen</li>
	 * </ol>
	 * 
	 * Metoden lagrer utvalget i selection variabelen.
	 */
	private void checkTableSelection(MouseEvent e) {
		int[] rows = table.getSelectedRows();

		// Sjekk om noe er valgt i tabellen
		if (rows.length == 0) {
			// Hvis ikke -> velg rad under muspeker
			selectRow(e);
			selection = parentPlugin.getIncidentTable().getSelectedEntities();
		} else {
			// Hvis --> sjekk at raden under musa er med i utvalget
			selection = parentPlugin.getIncidentTable().getSelectedEntities();
			int row = table.rowAtPoint(e.getPoint());
			if (row != -1) {
				HendelseSokView underMouse = (HendelseSokView) parentPlugin.getIncidentTableModel().get(row);
				if (!selection.contains(underMouse)) {
					// Hvis ikke -> velg rad under muspeker
					selectRow(e);
					selection = parentPlugin.getIncidentTable().getSelectedEntities();
				}
			}
		}
	}

	/**
	 * Åpner hendelser vha parentPlugin
	 * 
	 */
	private void openIncidents() {
		// Hent ut de valgte radene til selection
		selection = parentPlugin.getIncidentTable().getSelectedEntities();

		if (selection != null) {
			parentPlugin.openIncident(selection);
		}
	}

	/**
	 * Setter valgt rad i tabellen til den som er under musepekeren
	 * 
	 */
	private void selectRow(MouseEvent e) {
		// Sørg for at raden der musa er klikkes på med venstre...
		int index = table.rowAtPoint(e.getPoint());
		if (index != -1) {
			table.setRowSelectionInterval(index, index);
		}
	}

	/**
	 * Klasse for å åpne et funn
	 */
	class OpenAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param text
		 * @param icon
		 */
		public OpenAction(String text, Icon icon) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			menu.setVisible(false);
			openIncidents();
		}
	}

	/**
	 * Klasse for å vise et funn i kart
	 */
	class MapAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param text
		 * @param icon
		 */
		public MapAction(String text, Icon icon) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			menu.setVisible(false);
			// Hent ut de valgte radene til selection
			selection = parentPlugin.getIncidentTable().getSelectedEntities();

			if (selection != null) {
				parentPlugin.showIncidentsInMap(selection);
			}
		}
	}

	/**
	 * Sørger for at JMenuItem reagerer på mouseEntered og mouseExited
	 */
	class ItemHighlighter implements MouseListener {
		private JMenuItem item;

		/**
		 * Konstruktør
		 * 
		 */
		public ItemHighlighter(JMenuItem item) {
			this.item = item;
		}

		/** {@inheritDoc} */
		public void mouseClicked(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.background"));
			item.setForeground(UIManager.getColor("MenuItem.foreground"));
		}

		/** {@inheritDoc} */
		public void mousePressed(MouseEvent e) {
			// Brukes ikke
		}

		/** {@inheritDoc} */
		public void mouseReleased(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.background"));
			item.setForeground(UIManager.getColor("MenuItem.foreground"));
		}

		/** {@inheritDoc} */
		public void mouseEntered(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.selectionBackground"));
			item.setForeground(UIManager.getColor("MenuItem.selectionForeground"));
		}

		/** {@inheritDoc} */
		public void mouseExited(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.background"));
			item.setForeground(UIManager.getColor("MenuItem.foreground"));
		}
	}
}
