package no.mesta.mipss.mipssfield;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.IBlankFieldValidator;
import no.mesta.mipss.core.IDisposable;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 * @param <D>
 */
public abstract class SaveStateAdapter<D extends IRenderableMipssEntity> implements ISaveStateObserver<D>,
		PropertyChangeSource, IDisposable, PropertyChangeListener {
	private boolean changed;
	protected D detail;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private boolean oldErrors;
	protected PropertyChangeSupport props = new PropertyChangeSupport(this);
	protected List<IBlankFieldValidator<?>> validators = new ArrayList<IBlankFieldValidator<?>>();

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}
	
	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}
	
	/** {@inheritDoc} */
	@Override
	public void addValidator(IBlankFieldValidator<?> validator) {
		validators.add(validator);
		validator.addPropertyChangeListener("blank", this);
	}
	
	/** {@inheritDoc} */
	@Override
	public void dispose() {
		validators.clear();
		validators = null;
		
		if(props != null && props.getPropertyChangeListeners() != null) {
			for(PropertyChangeListener l:props.getPropertyChangeListeners()) {
				props.removePropertyChangeListener(l);
			}
			props = null;
		}
		detail = null;
		changed = false;
	}
	
	public D getDetail() {
		return detail;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isChanged() {
		return changed;
	}

	/** {@inheritDoc} */
	@Override
	public abstract boolean isEnabled();

	/** {@inheritDoc} */
	@Override
	public boolean isErrors() {
		boolean errors = false;
		
		for(IBlankFieldValidator<?> validator:validators) {
			errors = errors || validator.isBlank();
		}
		
		oldErrors = errors;
		
		return errors;
	}

	/** {@inheritDoc} */
	public void propertyChange(PropertyChangeEvent evt) {
		if(validators.contains(evt.getSource()) && StringUtils.equals("blank", evt.getPropertyName())) {
			boolean oldHasErrors = this.oldErrors;
			props.firePropertyChange("errors", oldHasErrors, isErrors());
			props.firePropertyChange("enabled", changed && !oldHasErrors, isEnabled());
		}
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/**
	 * Delegate
	 * 
	 * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 * @param l
	 */
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	public void setChanged(boolean f) {
		boolean old = this.changed;
		boolean oldEnabled = isEnabled();
		changed = f;
		props.firePropertyChange("changed", old, f);
		props.firePropertyChange("enabled", oldEnabled, isEnabled());
	}
	
	public abstract void setDetail(D detail);
}
