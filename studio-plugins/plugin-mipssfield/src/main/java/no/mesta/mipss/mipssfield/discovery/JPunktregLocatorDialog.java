package no.mesta.mipss.mipssfield.discovery;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Transient;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.PunktregQueryFilter;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.BooleanRenderer;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SortController;
import org.jdesktop.swingx.decorator.SortKey;
import org.jdesktop.swingx.decorator.SortOrder;
import org.jdesktop.swingx.table.TableColumnExt;

public class JPunktregLocatorDialog extends JDialog {
	private static final String APP = "MIPSS_FELT";
	private static final String APP_VALUE = "Funn_radius";
	private static final Dimension BUTTON_SIZE = new Dimension(45, 22);
	private static final Logger logger = LoggerFactory.getLogger(JPunktregLocatorDialog.class);
	private static final int STRUT = 2;
	private final MipssField BEAN;
	private Box buttons;
	private boolean cancelled;
	private Point center;
	private List<PunktregLocate> funn = new ArrayList<PunktregLocate>();
	private final Hendelse hendelse;
	private PunktregLocate punktregLocate;
	private JMipssBeanTable<PunktregLocate> table;

	public JPunktregLocatorDialog(final Hendelse hendelse, final JFrame owner, final Driftkontrakt kontrakt, double x,
			double y) {
		super(owner, GUITexts.getText(GUITexts.SELECT_PUNKTREG), true);
		center = new Point();
		center.setLocation(x, y);
		this.hendelse = hendelse;

		Konfigparam param = KonfigparamPreferences.getInstance().hentEnForApp(APP, APP_VALUE);
		double boxReach = 100;
		try {
			boxReach = Double.valueOf(param.getVerdi());
		} catch (Throwable t) {
			logger.warn("Parsing " + APP + "," + APP_VALUE + " gave Exception", t);
			t = null;
		}

		BEAN = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		PunktregQueryFilter filter = new PunktregQueryFilter(null);
		filter.setKontraktId(kontrakt.getId());
		filter.setOrigo(center);
		filter.setBoxReach(boxReach);
		List<PunktregSok> viewPunktreg = BEAN.sokFunn(filter);

		for (PunktregSok vp : viewPunktreg) {
			Boolean first = hendelse.getGuid().equals(vp.getHendelseGuid());
			if (vp.getX() != null && vp.getY() != null) {
				funn.add(new PunktregLocate(vp, center, first));
			}
		}
		//TODO utkommentert pga modellendring
//		for(Avvik p:hendelse.getPunktregistreringer()) {
//			Boolean first = hendelse.getGuid().equals(p.getHendelseGuid());
//			PunktregLocate pl = PunktregLocate.newInstance(p, center, first);
//			if(!funn.contains(pl)) {
//				funn.add(pl);
//			}
//		}

		initWindow();
		initTable();
		initButtons();

		JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pane.setViewportView(table);

		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, pane));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, buttons));
		add(BoxUtil.createVerticalStrut(STRUT));

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public Color getCellBackground() {
		return UIManager.getColor("Table.background");
	}

	public Color getCellForeground() {
		return UIManager.getColor("Table.foreground");
	}

	public Avvik getPunktreg() {
		if (!cancelled && punktregLocate != null) {
			Avvik punktreg = BEAN.hentPunktreg(punktregLocate.getPunktregGuid());
			return punktreg;
		} else {
			return null;
		}
	}

	public Color getSelectedBackground() {
		return UIManager.getColor("Table.selectionBackground");
	}

	public Color getSelectedForeground() {
		Color c = new Color(200, 0, 0);
		return c;
	}

	private void initButtons() {
		buttons = Box.createHorizontalBox();

		JButton add = new JButton(GUITexts.getText(GUITexts.ATTATCH));
		ComponentSizeResources.setComponentSize(add, BUTTON_SIZE);
		add.setMargin(new Insets(1, 1, 1, 1));
		add.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				punktregLocate = table.getSelectedEntities().get(0);
				setVisible(false);
			}
		});
		Binding<JMipssBeanTable<PunktregLocate>, Boolean, JButton, Boolean> enableAdd = BindingHelper.createbinding(
				table, "${noOfSelectedRows == 1}", add, "enabled");
		enableAdd.bind();

		JButton cancel = new JButton(GUITexts.getText(GUITexts.CANCEL));
		ComponentSizeResources.setComponentSize(cancel, BUTTON_SIZE);
		cancel.setMargin(new Insets(1, 1, 1, 1));
		cancel.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = true;
				setVisible(false);
			}
		});

		buttons.add(Box.createHorizontalGlue());
		buttons.add(cancel);
		buttons.add(BoxUtil.createHorizontalStrut(STRUT));
		buttons.add(add);
	}

	private void initTable() {
		MipssBeanTableModel<PunktregLocate> model = new MipssBeanTableModel<PunktregLocate>(PunktregLocate.class,
				"opprettetDato", "opprettetAv", "distance", "fylkesnummer",
				"kommunenummer", "vei", "veinummer", 
				"hp", "meter", "harHendelse", 
				"harInspeksjon", "etterslep", "antallBilder", 
				"sisteStatusDato", "sisteStatus", "tiltaksDato", 
				"prosessTxt", "tilstandtypeNavn", "aarsakNavn", 
				"pdaDfuNavn", "beskrivelse", "hvalue");
		model.setEntities(funn);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				PunktregLocate.class, model.getColumns());
		table = new JMipssBeanTable<PunktregLocate>(model, columnModel);
		table.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		table.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
		table.setDefaultRenderer(Double.class, new DoubleRenderer());
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setSortOrder(21, SortOrder.ASCENDING);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					table.selectRow(e);
					punktregLocate = table.getSelectedEntities().get(0);
					setVisible(false);
				} else {
					punktregLocate = table.getSelectedEntities().get(0);
				}
			}
		});
		table.setHighlighters(new Highlighter[] { new ColorHighlighter(new FunnHighlightPredicate(),
				Color.YELLOW, getCellForeground(), getSelectedBackground(), getSelectedForeground()) });
		table.getColumnExt(21).setVisible(false);
		setColumnWidth(table.getColumn(0), 90); // registrertDato
		setColumnWidth(table.getColumn(1), 90); // registrertAv
		setColumnWidth(table.getColumn(2), 55); // distance
		setColumnWidth(table.getColumn(3), 30); // fylkesnummer
		setColumnWidth(table.getColumn(4), 30); // kommunenummer
		setColumnWidth(table.getColumn(5), 30); // vei
		setColumnWidth(table.getColumn(6), 40); // veinummer
		setColumnWidth(table.getColumn(7), 30); // hp
		setColumnWidth(table.getColumn(8), 40); // meter
		setColumnWidth(table.getColumn(9), 30); // harHendelse
		setColumnWidth(table.getColumn(10), 30); // harInspeksjon
		setColumnWidth(table.getColumn(11), 30); // etterslep
		setColumnWidth(table.getColumn(12), 45); // antallBilder
		setColumnWidth(table.getColumn(13), 90); // sisteStatusDato
		setColumnWidth(table.getColumn(14), 90); // tiltaksDato
		setColumnWidth(table.getColumn(15), 130); // prosessTxt
		setColumnWidth(table.getColumn(16), 110); // tilstandtypeNavn
	}
	
	private void initWindow() {
		setDefaultCloseOperation(JPunktregLocatorDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			/** {@inheritDoc} */
			@Override
			public void windowClosing(WindowEvent e) {
				cancelled = true;
				dispose();
			}
		});
	}

	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}

	private class FunnHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@SuppressWarnings("unchecked")
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;

			MipssRenderableEntityTableModel<PunktregLocate> tableModel = (MipssRenderableEntityTableModel<PunktregLocate>) table.getModel();
			int rowModel = table.convertRowIndexToModel(row);
			PunktregLocate funn = tableModel.get(rowModel);
			
			boolean highlight = false;
			if (funn != null) {
				highlight = hendelse.getGuid().equals(funn.getHendelseGuid());
			}
			
			return highlight;
		}
	}

	public static class PunktregLocate extends PunktregSok {
		private final Point origo;
		private final Point position;
		private Integer hvalue;
		private Boolean first;

		public PunktregLocate(PunktregSok view, Point center, Boolean first) {
			origo = center;
			this.first = first;
			if (view != null) {
				mergeValues(view);
			}
			setPunktregGuid(view.getPunktregGuid());

			position = new Point();
			position.setLocation(getX(), getY());
		}

		@Transient
		public Integer getDistance() {
			return (int) origo.distance(position);
		}
		
		public Integer getHvalue(){
			if (first)
				return -1;
			return (int) origo.distance(position);
		}

		public static PunktregLocate newInstance(Avvik p, Point center, Boolean first) {
			return new PunktregLocate(PunktregSok.newInstance(p), center, first);
		}
	}
}
