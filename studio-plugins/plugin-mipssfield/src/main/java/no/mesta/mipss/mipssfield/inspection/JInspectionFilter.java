package no.mesta.mipss.mipssfield.inspection;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.InspeksjonQueryFilter;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.process.JProcessPanel;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

/**
 * Panel for å sette filter på tabell for oversikt over inspeksjoner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JInspectionFilter extends JPanel implements MipssBeanLoaderListener, PropertyChangeListener {
	private static final Logger logger = LoggerFactory.getLogger(JInspectionFilter.class);
	private InspeksjonQueryFilter filter;
	private JButton filterButton;
	private MipssFieldModule parentPlugin;
	private JProcessPanel prosessPanel;
	private JLabel noRowsHolder = new JLabel("0");
	private BindingGroup bind;
	private JButton hentMedIdButton;

	/**
	 * Konstruktør
	 * 
	 */
	public JInspectionFilter(final MipssFieldModule parentPlugin) {
		this.parentPlugin = parentPlugin;
		filter = parentPlugin.getInspectionFilter();
		parentPlugin.getInspectionTableModel().addTableLoaderListener(this);

		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), GUITexts.getText(GUITexts.FILTER_TITLE)));

		filterButton = new JButton(GUITexts.getText(GUITexts.SEARCH), IconResources.SEARCH_ICON);
		filterButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filterButton.setEnabled(false);
				updateFilter();
				JInspectionFilter.this.parentPlugin.getInspectionTableModel().setBeanMethodArgValues(new Object[] { filter });
				JInspectionFilter.this.parentPlugin.getInspectionTableModel().loadData();
			}
		});
		
		hentMedIdButton = new JButton(GUITexts.getText(GUITexts.GET_INSPECTION_W_ID), IconResources.DETALJER_SMALL_ICON);
		hentMedIdButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				parentPlugin.openWithDialog(MipssFieldModule.IHF_TYPE.INSPEKSJON);
			}
		});
		bind = new BindingGroup();
		Binding<InspeksjonQueryFilter, Boolean, JButton, Boolean> enableFilterButton = BindingHelper.createbinding(
				filter, "${kontraktId != null}", filterButton, "enabled");
//		enableFilterButton.bind();
		bind.addBinding(enableFilterButton);

		JCheckBox kunEtterslep = new JCheckBox(GUITexts.getText(GUITexts.BACKLOG), false);
		Binding<JCheckBox, Boolean, InspeksjonQueryFilter, Boolean> eb = BindingHelper.createbinding(kunEtterslep,
				"selected", filter, "kunEtterslep");
		bind.addBinding(eb);

		JCheckBox kunslettede = new JCheckBox(GUITexts.getText(GUITexts.ONLY_DELETED), false);
		Binding<JCheckBox, Boolean, InspeksjonQueryFilter, Boolean> sb = BindingHelper.createbinding(kunslettede,
				"selected", filter, "kunSlettede");
		bind.addBinding(sb);
		kunslettede.addPropertyChangeListener("selected", this);

		prosessPanel = new JProcessPanel(filter, "kontraktId", false);
		prosessPanel.setPreferredSize(new Dimension(200,200));
		prosessPanel.setMinimumSize(new Dimension(200,200));
		
		Box rader = BoxUtil.createHorizontalBox(0, new JLabel("Rader: "), noRowsHolder);
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(rader, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,5,2,0), 0,0));
		buttonPanel.add(filterButton, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,5,2,0), 0,0));
		buttonPanel.add(hentMedIdButton,new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,10,2,5), 0,0));
		
		setLayout(new GridBagLayout());
		add(prosessPanel,   new GridBagConstraints(0,0, 1,2, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(kunEtterslep, 	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15,0,0,0), 0,0));
		add(kunslettede,   	new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		add(buttonPanel, new GridBagConstraints(0,3, 2,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));

		bind.bind();
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		bind.unbind();
		filterButton = null;
		prosessPanel=null;
		parentPlugin.getInspectionTableModel().setBeanMethodArgValues(null);
		parentPlugin.getInspectionTableModel().removeTableLoaderListener(this);
		parentPlugin = null;
	}
	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		logger.debug("loadingFinished(" + event + ")");
		filterButton.setEnabled(true);
		int rows = parentPlugin.getInspectionTableModel().getRowCount();
		noRowsHolder.setText(String.valueOf(rows));
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		logger.debug("loadingStarted(" + event + ")");
	}

	private void updateFilter() {
		Long[] prosesser = prosessPanel.getAlleValgteProsesser();
		Long[] underprosesser = prosessPanel.getAlleValgteUnderprosesser();
		
		Long[] valgteProsesser = (Long[])ArrayUtils.addAll(prosesser, underprosesser);
		filter.setProsessId(valgteProsesser);
		//filter.setProsessId(prosessPanel.getAlleValgteProsesser());
		//filter.setUnderprosessId(prosessPanel.getAlleValgteUnderprosesser());
		filter.setIngenProsesser(prosessPanel.isIngenValgt());
		filter.setVeiInfo(parentPlugin.getCommonFilterPanel().getVeiInfo());
		filter.setIngenDfuer(parentPlugin.getCommonFilterPanel().isIngenDfuerValgt());
	}


	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();
		if (StringUtils.equals("selected", property)) {
			Boolean b = (Boolean) evt.getNewValue();
			if (b != null && b) {
				logger.debug("ok");
			}
		}
	}
}
