package no.mesta.mipss.mipssfield;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.driftkontrakt.bean.ProsessHelper;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.messages.MipssMapOpenMipssFieldItem;
import no.mesta.mipss.mipssfield.vo.PunktregResult;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.Temperatur;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.persistence.mipssfield.Vaer;
import no.mesta.mipss.persistence.mipssfield.Vind;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.reports.HPReportDS;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.PunktregDS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.table.BooleanRenderer;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SortOrder;

/**
 * Veimesterpakke plugin
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssFieldModule extends MipssPlugin {
	public enum IHF_TYPE{
		INSPEKSJON,
		HENDELSE, 
		FUNN
	};
	
	public static final String APPLICATION_NAME = "studio.mipss.field";
	private static MipssField bean;
	private static MaintenanceContract contractBean;
	private static Date defaultFromDate;
	private static List<Hendelseaarsak> hendelsesaarsaker;
	private static final Logger logger = LoggerFactory.getLogger(MipssFieldModule.class);
	public static final String MODE_DISCOVERIES = "FUNN";
	public static final String MODE_INCIDENT = "HENDELSE";
	public static final String MODE_INSPECTION = "INSPEKSJON";
	public static final String MODE_ELRAPP = "ELRAPP";
	private static List<Prosess> prosesser;
	private static List<AvvikstatusType> statusTyper;
	private static List<Temperatur> temperaturListe;
	private static List<Avviktilstand> tilstandTyper;
	private static List<Trafikktiltak> trafikktiltaksTyper;
	private static List<Vaer> vaerListe;
	private static List<Vind> vindListe;
	private JCommonFilterFields commonFilterPanel;
	private DateTimeRenderer dateTimeRenderer = new DateTimeRenderer();
	private MipssBeanTableModel<PunktregSok> dicoveryTableModel;
	private PunktregQueryFilter discoveryFilter;
	private JMipssBeanTable<PunktregSok> discoveryTable;
	private DoubleRenderer doubleRenderer = new DoubleRenderer();
	private HendelseQueryFilter incidentFilter;
	private JMipssBeanTable<HendelseSokView> incidentTable;
	private MipssBeanTableModel<HendelseSokView> incidentTableModel;
	private InspeksjonQueryFilter inspectionFilter;
	private TableColumn inspectionSlettetAvColumn;
	private TableColumn inspectionSlettetDatoColumn;
	private JMipssBeanTable<InspeksjonSokView> inspectionTable;
	private MipssBeanTableModel<InspeksjonSokView> inspectionTableModel;
	private String mode;
	private Date noSearchBefore;
	private JMipssFieldPanel pluginGUI;
	private static Long pdaDfuIdent;

	private List<ActionListener> elrappSendtListeners = new ArrayList<ActionListener>();
	/**
	 * Ikke gjør noe i konstruktøren
	 * 
	 */
	public MipssFieldModule() {
		mode = MODE_DISCOVERIES;
	}

	public AvvikStatus createNewStatus(AvvikstatusType type) {
		AvvikStatus status = new AvvikStatus();
		status.setAvvikstatusType(type);
		status.setOpprettetAv(getLoader().getLoggedOnUserSign());
		status.setOpprettetDato(Clock.now());

		return status;
	}

	public void deleteDiscoveries(List<PunktregSok> selection) {
		enforceWriteAccess();

		List<String> guider = new ArrayList<String>();
		Map<String, PunktregSok> hendelser = new HashMap<String, PunktregSok>();

		for (PunktregSok funn : selection) {
			guider.add(funn.getPunktregGuid());
			hendelser.put(funn.getPunktregGuid(), funn);
		}

		List<PunktregSok> slettede = getMipssField().slettFunn(guider, getLoader().getLoggedOnUserSign());
		for (PunktregSok slettet : slettede) {
			PunktregSok hendelse = hendelser.get(slettet.getPunktregGuid());
			hendelse.merge(slettet);
		}

		getDiscoveryTable().clearSelection();
	}

	public void deleteIncidents(List<HendelseSokView> selection, boolean otherDataToo) {
		enforceWriteAccess();

		List<String> guider = new ArrayList<String>();
		Map<String, HendelseSokView> hendelser = new HashMap<String, HendelseSokView>();

		for (HendelseSokView hendelse : selection) {
			guider.add(hendelse.getGuid());
			hendelser.put(hendelse.getGuid(), hendelse);
		}

		List<HendelseSokView> slettede = getMipssField().slettHendelser(guider, otherDataToo,
				getLoader().getLoggedOnUserSign());
		for (HendelseSokView slettet : slettede) {
			HendelseSokView hendelse = hendelser.get(slettet.getGuid());
			hendelse.merge(slettet);
		}

		getIncidentTable().clearSelection();
	}

	public void deleteInspections(List<InspeksjonSokView> selection, boolean otherDataToo) {
		enforceWriteAccess();

		List<String> guider = new ArrayList<String>();
		Map<String, InspeksjonSokView> inspeksjoner = new HashMap<String, InspeksjonSokView>();

		for (InspeksjonSokView inspeksjon : selection) {
			guider.add(inspeksjon.getGuid());
			inspeksjoner.put(inspeksjon.getGuid(), inspeksjon);
		}

		List<InspeksjonSokView> slettede = getMipssField().slettInspeksjoner(guider, otherDataToo,
				getLoader().getLoggedOnUserSign());
		for (InspeksjonSokView slettet : slettede) {
			InspeksjonSokView inspeksjon = inspeksjoner.get(slettet.getGuid());
			inspeksjon.merge(slettet);
		}

		getInspectionTable().clearSelection();
	}

	public Color getCellBackground() {
		return UIManager.getColor("Table.background");
	}

	public JCommonFilterFields getCommonFilterPanel() {
		return commonFilterPanel;
	}

	/**
	 * Angir default verdi for fra dato søket
	 * 
	 * @return
	 */
	public Date getDefaultFromDate() {
		if (defaultFromDate == null) {
			Konfigparam konfig = KonfigparamPreferences.getInstance().hentEnForApp(APPLICATION_NAME,
					Konfigparams.MONTHS_AGO);

			if (konfig == null) {
				Date oneMonthAgo = CalendarUtil.getMonthsAgo(1);
				return oneMonthAgo;
			} else {
				int months = 1;
				try {
					months = Integer.valueOf(konfig.getVerdi());
				} catch (Throwable t) {
					t = null;
				}

				defaultFromDate = CalendarUtil.getMonthsAgo(months);
			}
		}

		return defaultFromDate;
	}

	private static Long getDefaultPdaDfuIdent() {
		if(pdaDfuIdent == null) {
			try {
				Konfigparam param = KonfigparamPreferences.getInstance().hentEnForApp(APPLICATION_NAME, "defaultPda");
				pdaDfuIdent = Long.decode(param.getVerdi());
			} catch (Throwable t) {
				logger.error("no defaultPda", t);
				pdaDfuIdent = -1L;
			}
		}
		
		return pdaDfuIdent;
	}

	/**
	 * Modellen for funn som vises i plugin
	 * 
	 * @return
	 */
	public MipssBeanTableModel<PunktregSok> getDicoveryTableModel() {
		return dicoveryTableModel;
	}

	public Color getDiscoveryColor() {
		return MipssColors.getDiscovery();
	}

	public PunktregQueryFilter getDiscoveryFilter() {
		return discoveryFilter;
	}

	/**
	 * Tabellen for funn som vises i plugin
	 * 
	 * @return
	 */
	public JMipssBeanTable<PunktregSok> getDiscoveryTable() {
		return discoveryTable;
	}

	public List<Hendelseaarsak> getHendelsesaarsaker() {
		if (hendelsesaarsaker == null) {
			hendelsesaarsaker = getMipssField().hentAlleHendelsesAarsaker();
			Collections.sort(hendelsesaarsaker);
		}

		return hendelsesaarsaker;
	}

	public Color getIncidentColor() {
		return MipssColors.getIncident();
	}

	public HendelseQueryFilter getIncidentFilter() {
		return incidentFilter;
	}

	/**
	 * Tabellen for hendelser som vises i plugin
	 * 
	 * @return
	 */
	public JMipssBeanTable<HendelseSokView> getIncidentTable() {
		return incidentTable;
	}

	/**
	 * Modellen for hendelser som vises i plugin
	 * 
	 * @return
	 */
	public MipssBeanTableModel<HendelseSokView> getIncidentTableModel() {
		return incidentTableModel;
	}

	public Color getInspectionColor() {
		return MipssColors.getInspection();
	}

	public InspeksjonQueryFilter getInspectionFilter() {
		return inspectionFilter;
	}

	public TableColumn getInspectionSlettetAvColumn() {
		return inspectionSlettetAvColumn;
	}

	public TableColumn getInspectionSlettetDatoColumn() {
		return inspectionSlettetDatoColumn;
	}

	/**
	 * Tabellen for inspeksjoner som vises i plugin
	 * 
	 * @return
	 */
	public JMipssBeanTable<InspeksjonSokView> getInspectionTable() {
		return inspectionTable;
	}

	/**
	 * Modellen for inspeksjoner som vises i plugin
	 * 
	 * @return
	 */
	public MipssBeanTableModel<InspeksjonSokView> getInspectionTableModel() {
		return inspectionTableModel;
	}

	public MaintenanceContract getMaintenanceContract() {
		if (contractBean == null) {
			try {
				contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
			} catch (Exception e) {
				loader.logException(e, this, "Could not get MaintenanceContract");
			}
		}

		return contractBean;
	}

	public MipssField getMipssField() {
		if (bean == null) {
			bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		}

		return bean;
	}

	/** {@inheritDoc} */
	@Override
	public JPanel getModuleGUI() {
		return pluginGUI;
	}

	/**
	 * Angir tidligste dato som brukeren kan søke fra og med
	 * 
	 * @return
	 */
	public Date getNoSearchBefore() {
		if (noSearchBefore == null) {
			Konfigparam konfig = KonfigparamPreferences.getInstance().hentEnForApp(APPLICATION_NAME,
					Konfigparams.YEAR_BACK_RULE);

			Date oneYearAgo = CalendarUtil.getMonthsAgo(12);
			if (konfig == null) {
				noSearchBefore = oneYearAgo;
			} else {
				boolean ruleEnabled = false;
				try {
					ruleEnabled = Boolean.valueOf(konfig.getVerdi());
				} catch (Throwable t) {
					t = null;
				}

				if (!ruleEnabled) {
					noSearchBefore = null;
				} else {
					noSearchBefore = oneYearAgo;
				}
			}
		}

		return noSearchBefore;
	}

	/**
	 * Alle hovedprosessene
	 * 
	 * @return
	 */
	public List<Prosess> getProsesser() {
		if (prosesser == null) {
			prosesser = getMipssField().hentAlleProsesser();
			Collections.sort(prosesser);
		}

		return prosesser;
	}
	/**
	 * Henter alle prosessene tilknyttet en kontrakt
	 * @param kontraktId
	 * @return
	 */
	public List<Prosess> getProsessForKontrakt(Long kontraktId){
		List<Prosess> prosessForKontrakt = getMaintenanceContract().getProsessForKontrakt(kontraktId, false);
		return new ProsessHelper().createTree(prosessForKontrakt);
	}

	public List<Avviktilstand> getPunktregtilstandtyper() {
		if (tilstandTyper == null) {
			tilstandTyper = getMipssField().hentAllePunktregtilstandtype();
			Collections.sort(tilstandTyper);
		}

		return tilstandTyper;
	}

	public Color getSelectedBackground() {
		return UIManager.getColor("Table.selectionBackground");
	}

	public Color getSelectedForeground() {
		Color c = new Color(200, 0, 0);
		return c;
	}

	public AvvikstatusType getStatus(String status) {
		AvvikstatusType type = null;
		for (AvvikstatusType sok : getStatusTyper()) {
			if (StringUtils.equals(sok.getStatus(), status)) {
				type = sok;
				break;
			}
		}

		return type;
	}

	public List<AvvikstatusType> getStatusTyper() {
		if (statusTyper == null) {
			statusTyper = getMipssField().hentAlleStatusTyper();
			Collections.sort(statusTyper);
		}

		return statusTyper;
	}

	public List<Temperatur> getTemperaturListe() {
		if (temperaturListe == null) {
			temperaturListe = getMipssField().hentAlleTemperatur();
			Collections.sort(temperaturListe);
		}
		return temperaturListe;
	}

	public List<Trafikktiltak> getTrafikktiltaksTyper() {
		if (trafikktiltaksTyper == null) {
			trafikktiltaksTyper = getMipssField().hentAlleTrafikktiltaktyper();
			Collections.sort(trafikktiltaksTyper);
		}

		return trafikktiltaksTyper;
	}

	public List<Vaer> getVaerListe() {
		if (vaerListe == null) {
			vaerListe = getMipssField().hentAlleVaer();
			Collections.sort(vaerListe);
		}
		return vaerListe;
	}

	public List<Vind> getVindListe() {
		if (vindListe == null) {
			vindListe = getMipssField().hentAlleVind();
			Collections.sort(vindListe);
		}
		return vindListe;
	}

	private void initDiscoveryTable() {
		logger.debug("initDiscoveryTable() start");

		dicoveryTableModel = new MipssBeanTableModel<PunktregSok>(PunktregSok.class, "refnummer", "sisteStatusDato", "sisteStatus",
				"tiltaksDato", "prosessTxt", "tilstandtypeNavn", "aarsakNavn", "harHendelse", "harInspeksjon",
				"etterslep", "antallBilder", "fylkesnummer", "kommunenummer", "vei", "veinummer", "hp", "meter",
				"opprettetDato", "opprettetAv", "pdaDfuNavn", "beskrivelse", "historikk", "estimat");

		MipssRenderableEntityTableColumnModel dicoveryColumnModel = new MipssRenderableEntityTableColumnModel(
				PunktregSok.class, dicoveryTableModel.getColumns());

		// Dette gjør tabellen i stand til å laste data direkte fra bønna
		dicoveryTableModel.setBeanInstance(getMipssField());
		dicoveryTableModel.setBeanMethod("sokFunn");
		dicoveryTableModel.setBeanInterface(MipssField.class);
		dicoveryTableModel.setBeanMethodArguments(new Class<?>[] { PunktregQueryFilter.class });
		discoveryTable = new JMipssBeanTable<PunktregSok>(dicoveryTableModel, dicoveryColumnModel);
		discoveryTable.setDefaultRenderer(Date.class, dateTimeRenderer);
		discoveryTable.setDefaultRenderer(Double.class, doubleRenderer);
		discoveryTable.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		discoveryTable.setFillsViewportHeight(true);
		discoveryTable.setFillsViewportWidth(true);
		discoveryTable.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		discoveryTable.setSortable(true);
		discoveryTable.setSortOrder(0, SortOrder.DESCENDING);
		discoveryTable.setColumnControlVisible(true);
		discoveryTable.addColumnStateSupport("discoveryTable", this);

		setColumnWidth(discoveryTable.getColumn(0), 60); // sisteStatusDato
		setColumnWidth(discoveryTable.getColumn(1), 90); // sisteStatusDato
		setColumnWidth(discoveryTable.getColumn(3), 90); // tiltaksDato
		setColumnWidth(discoveryTable.getColumn(4), 130); // prosessTxt
		setColumnWidth(discoveryTable.getColumn(5), 110); // tilstandtypeNavn
		setColumnWidth(discoveryTable.getColumn(7), 30); // harHendelse
		setColumnWidth(discoveryTable.getColumn(8), 30); // harInspeksjon
		setColumnWidth(discoveryTable.getColumn(9), 30); // etterslep
		setColumnWidth(discoveryTable.getColumn(10), 45); // antallBilder
		setColumnWidth(discoveryTable.getColumn(11), 30); // fylkesnummer
		setColumnWidth(discoveryTable.getColumn(12), 30); // kommunenummer
		setColumnWidth(discoveryTable.getColumn(13), 30); // vei
		setColumnWidth(discoveryTable.getColumn(14), 40); // veinummer
		setColumnWidth(discoveryTable.getColumn(15), 30); // hp
		setColumnWidth(discoveryTable.getColumn(16), 40); // meter
		setColumnWidth(discoveryTable.getColumn(17), 90); // registrertAv

		discoveryTable.setHighlighters(new Highlighter[] { new ColorHighlighter(new FunnHighlightPredicate(),
				getCellBackground(), Color.RED, getSelectedBackground(), getSelectedForeground()) });

		discoveryTable.getColumnExt(12).setComparator(new VeiComparator()); // vei

		logger.debug("initDiscoveryTable() end");
		discoveryTable.loadUserSettings();
	}

	private void initIncidentTable() {
		logger.debug("initIncidentTable() start");
		incidentTableModel = new MipssBeanTableModel<HendelseSokView>(HendelseSokView.class, "refnummer", "opprettetDato", "dato", "typeNavn",
				"aarsakNavn", "tiltakTekster", "hpGuiText", "kommune", "skadested", "antallFunn", "antallBilder",
				"funnetUnderInspeksjon", "r5Sak", "r11Sak", "opprettetAv", "pdaDfuNavn", "beskrivelse");
		MipssRenderableEntityTableColumnModel incidentColumnModel = new MipssRenderableEntityTableColumnModel(
				HendelseSokView.class, incidentTableModel.getColumns());
		// Dette gjør tabellen i stand til å laste data direkte fra bønna
		incidentTableModel.setBeanInstance(getMipssField());
		incidentTableModel.setBeanMethod("sokHendelser");
		incidentTableModel.setBeanInterface(MipssField.class);
		incidentTableModel.setBeanMethodArguments(new Class<?>[] { HendelseQueryFilter.class });
		incidentTable = new JMipssBeanTable<HendelseSokView>(incidentTableModel, incidentColumnModel);
		incidentTable.setDefaultRenderer(Date.class, dateTimeRenderer);
		incidentTable.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		incidentTable.setDefaultRenderer(Double.class, doubleRenderer);
		incidentTable.setFillsViewportHeight(true);
		incidentTable.setFillsViewportWidth(true);
		incidentTable.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		incidentTable.setColumnControlVisible(true);
		incidentTable.setSortable(true);
		incidentTable.setSortOrder(0, SortOrder.DESCENDING);
		incidentTable.addColumnStateSupport("incidentTable", this);

		setColumnWidth(incidentTable.getColumn(0), 60);
		setColumnWidth(incidentTable.getColumn(1), 90);
		setColumnWidth(incidentTable.getColumn(2), 90);
		incidentTable.setHighlighters(new Highlighter[] { new ColorHighlighter(new HendelseHighlightPredicate(),
				getCellBackground(), Color.RED, getSelectedBackground(), getSelectedForeground()) });
		incidentTable.loadUserSettings();
		logger.debug("initIncidentTable() end");
	}

	private void initInspectionTable() {
		logger.debug("initInspectionTable() start");
		inspectionTableModel = new MipssBeanTableModel<InspeksjonSokView>(InspeksjonSokView.class, "refnummer", "fra", "til",
				"etterslep", "funnHendelserTekst", "hpTekst", "prosesserTekst", "vaer", "vind", "temperatur",
				"opprettetAv", "evInspisert", "rvInspisert", "fvInspisert", "kvInspisert", "pdaDfuNavn", 
				//"slettetDato","slettetAv", 
				"beskrivelse");
		MipssRenderableEntityTableColumnModel inspectionColumnModel = new MipssRenderableEntityTableColumnModel(
				InspeksjonSokView.class, inspectionTableModel.getColumns());

//		inspectionSlettetDatoColumn = inspectionColumnModel.getColumn(16);
//		inspectionSlettetAvColumn = inspectionColumnModel.getColumn(17);
//		inspectionColumnModel.removeColumn(inspectionSlettetDatoColumn);
//		inspectionColumnModel.removeColumn(inspectionSlettetAvColumn);

		// Dette gjør tabellen i stand til å laste data direkte fra bønna
		inspectionTableModel.setBeanInstance(getMipssField());
		inspectionTableModel.setBeanMethod("sokInspeksjoner");
		inspectionTableModel.setBeanInterface(MipssField.class);
		inspectionTableModel.setBeanMethodArguments(new Class<?>[] { InspeksjonQueryFilter.class });
		inspectionTable = new JMipssBeanTable<InspeksjonSokView>(inspectionTableModel, inspectionColumnModel);
		inspectionTable.setDefaultRenderer(Date.class, dateTimeRenderer);
		inspectionTable.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		inspectionTable.setDefaultRenderer(Double.class, doubleRenderer);
		inspectionTable.setFillsViewportHeight(true);
		inspectionTable.setFillsViewportWidth(true);
		inspectionTable.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		inspectionTable.setColumnControlVisible(true);
		inspectionTable.addColumnStateSupport("inspectionTable", this);
		setColumnWidth(inspectionColumnModel.getColumn(0), 60);
		setColumnWidth(inspectionColumnModel.getColumn(1), 90);
		setColumnWidth(inspectionColumnModel.getColumn(2), 90);
		setColumnWidth(inspectionColumnModel.getColumn(3), 30);
		setColumnWidth(inspectionColumnModel.getColumn(4), 30);
		setColumnWidth(inspectionColumnModel.getColumn(5), 117);

		setColumnWidth(inspectionColumnModel.getColumn(7), 65);
		setColumnWidth(inspectionColumnModel.getColumn(8), 65);
		setColumnWidth(inspectionColumnModel.getColumn(9), 65);

		setColumnWidth(inspectionColumnModel.getColumn(11), 45);
		setColumnWidth(inspectionColumnModel.getColumn(12), 45);
		setColumnWidth(inspectionColumnModel.getColumn(13), 45);
		setColumnWidth(inspectionColumnModel.getColumn(14), 45);

		inspectionTable.setHighlighters(new Highlighter[] { new ColorHighlighter(new InspectionHighlightPredicate(),
				getCellBackground(), Color.RED, getSelectedBackground(), getSelectedForeground()) });
		inspectionTable.loadUserSettings();
		logger.debug("initInspectionTable() end");
	}

	/** {@inheritDoc} */
	@Override
	protected void initPluginGUI() {
		logger.debug("initPluginGUI() start");

		discoveryFilter = new PunktregQueryFilter(getNoSearchBefore());
		inspectionFilter = new InspeksjonQueryFilter(getNoSearchBefore());
		incidentFilter = new HendelseQueryFilter(getNoSearchBefore());

		initDiscoveryTable();
		initInspectionTable();
		initIncidentTable();
		pluginGUI = new JMipssFieldPanel(this);
		pluginGUI.setTabPane(mode);
//		pluginGUI.repaint();
		logger.debug("initPluginGUI() end");
	}

	/**
	 * Lukker et funn
	 */
	public PunktregResult lukkFunn(Avvik punkt, String tekst, Date lukketDato) {
		enforceWriteAccess();

		String signatur = getLoader().getLoggedOnUserSign();
		return getMipssField().lukkFunn(punkt.getGuid(), tekst, signatur, lukketDato);
	}

	/**
	 * Viser funn i en pdf
	 * 
	 * @param discoveries
	 */
	public File makeDiscoveriesPDF(List<PunktregSok> discoveries, Map<String, Object> params) {
		enforceReadAccess();

		List<String> guids = new ArrayList<String>();
		for (PunktregSok po : discoveries) {
			guids.add(po.getPunktregGuid());
		}

		PunktregDS dataSource = new PunktregDS(guids);
//		dataSource.setFieldBean(getMipssField());
		File report = ReportHelper.buildReport("Funn.jasper", dataSource, ReportHelper.EXPORT_PDF, params);
		return report;
	}

	public Hendelse newHendelseInstance() {
		Hendelse hendelse = new Hendelse();
		hendelse.setGuid(ServerUtils.getInstance().fetchGuid());
		hendelse.setDato(Clock.now());
		hendelse.getOwnedMipssEntity().setOpprettetAv(getLoader().getLoggedOnUserSign());
		hendelse.getOwnedMipssEntity().setOpprettetDato(hendelse.getDato());
		//TODO utkommentert pga modellendring
//		hendelse.setKontrakt(getCommonFilterPanel().getValgtKontrakt());
//		hendelse.setPdaDfuIdent(getDefaultPdaDfuIdent());
//		List<Punktstedfest> stedfestList = newPunktstedfestListInstance();
//		for (Punktstedfest st:stedfestList){
//			st.setHendelse(hendelse);
//		}
//		hendelse.setStedfestingList(stedfestList);
		return hendelse;
	}

	public Avvik newPunktregInstance() {
		Avvik punktreg = new Avvik();
		punktreg.setGuid(ServerUtils.getInstance().fetchGuid());
		punktreg.setOpprettetAv(getLoader().getLoggedOnUserSign());
		punktreg.setOpprettetDato(Clock.now());
		punktreg.addStatus(createNewStatus(getStatus(AvvikstatusType.NY_STATUS)));
		punktreg.setEtterslepFlagg(Boolean.FALSE);
		punktreg.setStedfesting(newPunktstedfestInstance());

		return punktreg;
	}

	public List<Punktstedfest> newPunktstedfestListInstance() {
		Punktstedfest stedfest = new Punktstedfest();
		stedfest.setGuid(ServerUtils.getInstance().fetchGuid());
		
		Punktveiref veiref = new Punktveiref();
		veiref.setFraDato(Clock.now());
		stedfest.addPunktveiref(veiref);
		
		List<Punktstedfest> stedfestList = new ArrayList<Punktstedfest>();
		stedfestList.add(stedfest);
		return stedfestList;
	}
	public Punktstedfest newPunktstedfestInstance() {
		Punktstedfest stedfest = new Punktstedfest();
		stedfest.setGuid(ServerUtils.getInstance().fetchGuid());
		Punktveiref veiref = new Punktveiref();
		veiref.setFraDato(Clock.now());
		stedfest.addPunktveiref(veiref);
		return stedfest;
	}
	
	private JDialog showWaitDialog(String text){
		JDialog d = new JDialog();
		JPanel labelPanel = new JPanel(new FlowLayout());
		JXBusyLabel busyLabel = new JXBusyLabel();
		busyLabel.setBusy(true);
		labelPanel.add(busyLabel);
		labelPanel.add(new JLabel(text));
		d.add(labelPanel);
		d.pack();
		d.setLocationRelativeTo(getLoader().getApplicationFrame());
		d.setVisible(true);
		return d;
	}
	public void openWithDialog(IHF_TYPE type){
		OpenIHFDialog d = new OpenIHFDialog(type, SwingUtilities.getWindowAncestor(getModuleGUI()), this);
		d.pack();
		d.setLocationRelativeTo(null);
		d.setVisible(true);
	}
	
	public void openDiscovery(final Long id){
		final JDialog wait = showWaitDialog("Henter funn");
		new Thread(){
			public void run(){
				PunktregSok punktreg = getMipssField().hentPunktregSok(id);
				if (punktreg!=null){
					List<PunktregSok> d = new ArrayList<PunktregSok>();
					d.add(punktreg);
					openDiscoveries(d);
					wait.dispose();
				}else{
					wait.dispose();
					JOptionPane.showMessageDialog(null, "Fant ingen funn med id:"+String.valueOf(id), "Fant ikke funn", JOptionPane.WARNING_MESSAGE);
				}
			}
		}.start();
	}
	public void openIncident(final Long id){
		final JDialog wait = showWaitDialog("Henter hendelse");
		new Thread(){
			public void run(){
				HendelseSokView hendelse = getMipssField().hentHendelseSok(id);
				if (hendelse!=null){
					List<HendelseSokView> i = new ArrayList<HendelseSokView>();
					i.add(hendelse);
					openIncident(i);
					wait.dispose();
				}else{
					wait.dispose();
					JOptionPane.showMessageDialog(null, "Fant ingen hendelser med id:"+String.valueOf(id), "Fant ikke hendelse", JOptionPane.WARNING_MESSAGE);
				}
			}
		}.start();
	}
	public void openInspection(final Long id){
		final JDialog wait = showWaitDialog("Henter hendelse");
		new Thread(){
			public void run(){
				InspeksjonSokView inspeksjon = getMipssField().hentInspeksjonSok(id);
				if (inspeksjon!=null){
					List<InspeksjonSokView> i = new ArrayList<InspeksjonSokView>();
					i.add(inspeksjon);
					openInspections(i);
					wait.dispose();
				}else{
					wait.dispose();
					JOptionPane.showMessageDialog(null, "Fant ingen inspeksjon med id:"+String.valueOf(id), "Fant ikke inspeksjon", JOptionPane.WARNING_MESSAGE);
				}
			}
		}.start();
	}
	/**
	 * Åpner detaljvinduene for funn
	 * 
	 */
	public void openDiscoveries(List<PunktregSok> discoveries) {
		enforceReadAccess();

		if (discoveries.size() > 4) {
			MipssDialogue dialogue = new MipssDialogue(getLoader().getApplicationFrame(), IconResources.WARNING_ICON,
					GUITexts.getText(GUITexts.WARNING), GUITexts.getText(GUITexts.OPEN_MANY_REPORT_DISCOVERIES,
							discoveries.size()), MipssDialogue.Type.WARNING, new MipssDialogue.Button[] {
							MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
				return;
			}
		}

		JDetailFrame frame = new JDetailFrame(this);
		for (PunktregSok d : discoveries) {
			try {
				frame.addDetail(d);
			} catch (Throwable t) {
				frame = null;
				getLoader().handleException(t, this, GUITexts.getText(GUITexts.FAILED_OPEN_DISCOVERY));
			}
		}

		if (frame != null) {
			frame.setVisible(true);
		}
	}

	public <T extends IRenderableMipssEntity> void openExcel(JMipssBeanTable<T> table) {
		enforceReadAccess();
		JMipssBeanTable.EXPORT_TYPE exportType = JMipssBeanTable.EXPORT_TYPE.SELECTED_ROWS_ONLY;
		if (table.getSelectedRowCount()==0){
			exportType = JMipssBeanTable.EXPORT_TYPE.ALL_ROWS;
		}
		File excelFile = table.exportToExcel(exportType);
		DesktopHelper.openFile(excelFile);
	}

	/**
	 * Åpner detaljvinduene for hendelse
	 * 
	 */
	public void openIncident(List<HendelseSokView> incidents) {
		enforceReadAccess();

		if (incidents.size() > 4) {
			MipssDialogue dialogue = new MipssDialogue(getLoader().getApplicationFrame(), IconResources.WARNING_ICON,
					GUITexts.getText(GUITexts.WARNING), GUITexts
							.getText(GUITexts.OPEN_MANY_INCIDENTS, incidents.size()), MipssDialogue.Type.WARNING,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
				return;
			}
		}

		JDetailFrame frame = new JDetailFrame(this);
		for (HendelseSokView hv : incidents) {
			try {
				frame.addDetail(hv);
			} catch (Throwable t) {
				frame = null;
				getLoader().handleException(t, this, GUITexts.getText(GUITexts.FAILED_OPEN_INCIDENT));
			}
		}
		if (frame != null) {
			frame.setVisible(true);
		}
	}

	/**
	 * Åpner detaljvinduene for inspeksjon
	 * 
	 */
	public void openInspections(List<InspeksjonSokView> inspections) {
		enforceReadAccess();

		if (inspections.size() > 4) {
			MipssDialogue dialogue = new MipssDialogue(getLoader().getApplicationFrame(), IconResources.WARNING_ICON,
					GUITexts.getText(GUITexts.WARNING), GUITexts.getText(GUITexts.OPEN_MANY_INSPECTIONS, inspections
							.size()), MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK,
							MipssDialogue.Button.CANCEL });
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
				return;
			}
		}

		JDetailFrame frame = new JDetailFrame(this);
		for (InspeksjonSokView i : inspections) {
			try {
				frame.addDetail(i);
			} catch (Throwable t) {
				frame = null;
				getLoader().handleException(t, this, GUITexts.getText(GUITexts.FAILED_OPEN_INSPECTION));
			}
		}
		if (frame != null) {
			frame.setVisible(true);
		}
	}

	public void openItems(final List<MipssFieldDetailItem> items) {
		enforceReadAccess();

		SwingWorker<Void, Void> waitForRunning = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				while (getStatus() != MipssPlugin.Status.RUNNING) {
					Thread.yield();
				}

				JDetailFrame frame = new JDetailFrame(MipssFieldModule.this);
				for (MipssFieldDetailItem item : items) {
					try {
						frame.addDetail(item);
					} catch (Throwable t) {
						frame = null;
						getLoader().handleException(t, MipssFieldModule.this,
								GUITexts.getText(GUITexts.FAILED_OPEN_ITEM));
					}
				}
				if (frame != null) {
					frame.setVisible(true);
				}
				return null;
			}
		};
		waitForRunning.execute();
	}

	/** {@inheritDoc} */
	@Override
	public void receiveMessage(PluginMessage<?, ?> message) {
		if (!(message instanceof OpenTabMessage || message instanceof OpenDetailMessage)) {
			throw new IllegalArgumentException("Unsupported message type");
		}

		if (this != message.getTargetPlugin()) {
			throw new IllegalStateException("Invalid plugin receiver for target");
		}

		message.processMessage();
	}

	public void restoreDiscoveries(List<PunktregSok> selection) {
		enforceWriteAccess();

		List<String> guider = new ArrayList<String>();
		Map<String, PunktregSok> hendelser = new HashMap<String, PunktregSok>();

		for (PunktregSok funn : selection) {
			guider.add(funn.getPunktregGuid());
			hendelser.put(funn.getPunktregGuid(), funn);
		}

		List<PunktregSok> slettede = getMipssField().gjennopprettFunn(guider);
		for (PunktregSok slettet : slettede) {
			PunktregSok hendelse = hendelser.get(slettet.getPunktregGuid());
			hendelse.merge(slettet);
		}

		getDiscoveryTable().clearSelection();
	}

	public void restoreIncidents(List<HendelseSokView> selection) {
		enforceWriteAccess();

		List<String> guider = new ArrayList<String>();
		Map<String, HendelseSokView> hendelser = new HashMap<String, HendelseSokView>();

		for (HendelseSokView hendelse : selection) {
			guider.add(hendelse.getGuid());
			hendelser.put(hendelse.getGuid(), hendelse);
		}

		List<HendelseSokView> gjenopprettede = getMipssField().gjenopprettHendelser(guider);
		for (HendelseSokView gjenopprettet : gjenopprettede) {
			HendelseSokView hendelse = hendelser.get(gjenopprettet.getGuid());
			hendelse.merge(gjenopprettet);
		}

		getIncidentTable().clearSelection();
	}

	public void restoreInspections(List<InspeksjonSokView> selection) {
		enforceWriteAccess();

		List<String> guider = new ArrayList<String>();
		Map<String, InspeksjonSokView> inspeksjoner = new HashMap<String, InspeksjonSokView>();

		for (InspeksjonSokView inspeksjon : selection) {
			guider.add(inspeksjon.getGuid());
			inspeksjoner.put(inspeksjon.getGuid(), inspeksjon);
		}

		List<InspeksjonSokView> gjenopprettede = getMipssField().gjenopprettInspeksjoner(guider);
		for (InspeksjonSokView gjenopprett : gjenopprettede) {
			InspeksjonSokView inspeksjon = inspeksjoner.get(gjenopprett.getGuid());
			inspeksjon.merge(gjenopprett);
		}

		getInspectionTable().clearSelection();
	}

	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}

	public void setCommonFilterPanel(JCommonFilterFields commonFilterPanel) {
		this.commonFilterPanel = commonFilterPanel;
	}

	public void setDiscoveryFilter(PunktregQueryFilter filter) {
		PunktregQueryFilter old = this.discoveryFilter;
		this.discoveryFilter = filter;
		props.firePropertyChange("discoveryFilter", old, filter);
	}

	public void setFilters(List<FilterBase> filters) {
		for (FilterBase filter : filters) {
			if (filter instanceof PunktregQueryFilter) {
				setDiscoveryFilter((PunktregQueryFilter) filter);
			} else if (filter instanceof InspeksjonQueryFilter) {
				setInspectionFilter((InspeksjonQueryFilter) filter);
			} else if (filter instanceof HendelseQueryFilter) {
				setIncidentFilter((HendelseQueryFilter) filter);
			} else {
				throw new IllegalArgumentException("unknown filter type " + filter);
			}
		}
	}

	public void setIncidentFilter(HendelseQueryFilter filter) {
		HendelseQueryFilter old = this.incidentFilter;
		this.incidentFilter = filter;
		props.firePropertyChange("incidentFilter", old, filter);
	}

	public void setInspectionFilter(InspeksjonQueryFilter filter) {
		InspeksjonQueryFilter old = this.inspectionFilter;
		this.inspectionFilter = filter;
		props.firePropertyChange("inspectionFilter", old, filter);
	}

	public void setMode(String mode) {
		String old = this.mode;
		if (getStatus().equals(Status.RUNNING)) {
			pluginGUI.setTabPane(mode);
		}

		this.mode = mode;

		props.firePropertyChange("mode", old, mode);
	}

	/**
	 * Viser funn i kart
	 * 
	 */
	public void showDiscoveriesInMap(List<PunktregSok> discoveries) {
		openItemsInMap(discoveries);
	}

	/**
	 * Viser hendelser i kart
	 * 
	 */
	public void showIncidentsInMap(List<HendelseSokView> incidents) {
		openItemsInMap(incidents);
	}

	/**
	 * Viser inspeksjoner i kart
	 * 
	 */
	public void showInspectionsInMap(List<InspeksjonSokView> inspections) {
		openItemsInMap(inspections);
	}
	
	/**
	 * Sender en melding til rammeverket om å åpne entitetene i kartet
	 * @param items
	 */
	@SuppressWarnings("unchecked")
	public void openItemsInMap(List<? extends MipssFieldDetailItem> items){
		MipssPlugin target;
		try {
			target = getLoader().getPlugin((Class<? extends MipssPlugin>)Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));
			MipssMapOpenMipssFieldItem message = new MipssMapOpenMipssFieldItem(items);
			message.setTargetPlugin(target);
			getLoader().sendMessage(message);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public void shutdown() {
		pluginGUI.dispose();
		pluginGUI = null;
		inspectionTable= null;
		inspectionTableModel = null;
		
		for (PropertyChangeListener l:props.getPropertyChangeListeners()){
			props.removePropertyChangeListener(l);
		}
		commonFilterPanel.dispose();
		commonFilterPanel = null;
	}

	/**
	 * Viser funn i en pdf
	 * 
	 * @param discoveries
	 */
	public void viewDiscoveryOverviewPDF(List<PunktregSok> discoveries) {
		enforceReadAccess();

		try {
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(discoveries);
			JReportBuilder builder = new JReportBuilder(Report.DISCOVERY_OVERVIEW, Report.DISCOVERY_OVERVIEW
					.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, getLoader().getApplicationFrame());
			File report = builder.getGeneratedReportFile();

			if (report != null) {
				DesktopHelper.openFile(report);
			}
		} catch (Throwable t) {
			getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
		}
	}

	/**
	 * Lister ut inspiserte strekninger
	 * 
	 * @param inspections
	 */
	public void viewInspectedHPs(List<InspeksjonSokView> inspections) {
		enforceReadAccess();

		List<String> guids = new ArrayList<String>();

		for (InspeksjonSokView i : inspections) {
			if (i.getHpTekst() != null) {
				guids.add(i.getGuid());
			}
		}

		if (guids.size() < 1) {
			MipssDialogue dialogue = new MipssDialogue(getLoader().getApplicationFrame(), IconResources.INFO_ICON,
					GUITexts.getText(GUITexts.INFORMATION), GUITexts.getText(GUITexts.EMPTY_REPORT, GUITexts
							.getText(GUITexts.NO_HP)), MipssDialogue.Type.INFO,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK });
			dialogue.askUser();
			return;
		}

		logger.debug("viewInspectedHPs(" + guids + ") db call");
		try {
			HPReportDS dataSource = new HPReportDS(guids);
			dataSource.setFieldBean(getMipssField());
			JReportBuilder builder = new JReportBuilder(Report.HP_REPORT, Report.HP_REPORT.getDefaultParameters(),
					dataSource, ReportHelper.EXPORT_XLS, getLoader().getApplicationFrame());
			File report = builder.getGeneratedReportFile();

			if (report != null) {
				DesktopHelper.openFile(report);
			}
		} catch (Throwable t) {
			getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
		}
	}
	
	
	public void viewInspectionReportPDF(){
		enforceReadAccess();
		
//		final JDialog d = new JDialog(getLoader().getApplicationFrame());
//		d.setModal(true);
//		
		InspeksjonrapportHandler panel = new InspeksjonrapportHandler(this);
//		d.setSize(new Dimension(270,175));
//		d.add(panel);
//		d.setLocationRelativeTo(getLoader().getApplicationFrame());
//		d.setVisible(true);
//		d.remove(panel);
	}

	
	/**
	 * Viser inspeksjoner i et excel ark
	 * 
	 * @param inspections
	 */
	public void viewInspectionOverviewXLS(List<InspeksjonSokView> inspections) {
		try {
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inspections);
			JReportBuilder builder = new JReportBuilder(Report.INSPECTION_OVERVIEW, Report.INSPECTION_OVERVIEW
					.getDefaultParameters(), dataSource, ReportHelper.EXPORT_XLS, getLoader().getApplicationFrame());
			File report = builder.getGeneratedReportFile();

			if (report != null) {
				DesktopHelper.openFile(report);
			}
		} catch (Throwable t) {
			getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
		}
	}
	
	public void addElrappSendtListener(ActionListener elrappSendtListener){
		this.elrappSendtListeners.add(elrappSendtListener);
	}
	public void removeElrappSendtListener(ActionListener elrappSendtListener){
		this.elrappSendtListeners.remove(elrappSendtListener);
	}
	/**
	 * Skal fyres når et skjema er sendt til elrapp
	 */
	public void fireElrappSendtEvent(){
		for (ActionListener e:elrappSendtListeners){
			e.actionPerformed(null);
		}
	}
	
	
	
	
	private class FunnHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;

			MipssRenderableEntityTableModel<PunktregSok> tableModel = getDicoveryTableModel();
			JMipssBeanTable<PunktregSok> table = getDiscoveryTable();
			int rowModel = table.convertRowIndexToModel(row);

			PunktregSok funn = tableModel.get(rowModel);

			boolean highlight = false;
			if (funn != null) {
				highlight = funn.getSlettetDato() != null;
			}
			return highlight;
		}
	}

	private class HendelseHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;

			MipssRenderableEntityTableModel<HendelseSokView> tableModel = getIncidentTableModel();
			JMipssBeanTable<HendelseSokView> table = getIncidentTable();
			int rowModel = table.convertRowIndexToModel(row);

			HendelseSokView hendelse = tableModel.get(rowModel);

			boolean highlight = false;
			if (hendelse != null) {
				highlight = hendelse.getSlettetDato() != null;
			}
			return highlight;
		}
	}

	private class InspectionHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;

			MipssRenderableEntityTableModel<InspeksjonSokView> tableModel = getInspectionTableModel();
			JMipssBeanTable<InspeksjonSokView> table = getInspectionTable();
			int rowModel = table.convertRowIndexToModel(row);

			InspeksjonSokView inspeksjon = tableModel.get(rowModel);

			boolean highlight = false;
			if (inspeksjon != null) {
				highlight = inspeksjon.getSlettetDato() != null;
			}
			return highlight;
		}
	}

	private class LongPair implements Serializable {
		private final Long l1;
		private final Long l2;

		public LongPair(Long l1, Long l2) {
			this.l1 = l1;
			this.l2 = l2;
		}

		/** {@inheritDoc} */
		public boolean equals(Object o) {
			if (o == this) {
				return true;
			}

			if (o == null) {
				return false;
			}

			if (!(o instanceof LongPair)) {
				return false;
			}

			LongPair other = (LongPair) o;

			return new EqualsBuilder().append(l1, other.l1).append(l2, other.l2).isEquals();
		}

		/** {@inheritDoc} */
		public int hashCode() {
			return new HashCodeBuilder().append(l1).append(l2).toHashCode();
		}
	}

	private class VeiComparator implements Comparator<String> {

		/** {@inheritDoc} */
		@Override
		public int compare(String vei1, String vei2) {
			if (StringUtils.equals(vei1, vei2)) {
				return 0;
			} else if (StringUtils.isBlank(vei1) && !StringUtils.isBlank(vei2)) {
				return -1;
			} else if (!StringUtils.isBlank(vei1) && StringUtils.isBlank(vei2)) {
				return 1;
			}

			String vk1 = vei1.substring(0, 1);
			String vk2 = vei2.substring(0, 1);
			Integer k1 = veiToScale(vk1);
			Integer k2 = veiToScale(vk2);

			return k1.compareTo(k2);
		}

		private int veiToScale(String vk) {
			if (StringUtils.equalsIgnoreCase("K", vk)) {
				return 0;
			} else if (StringUtils.equalsIgnoreCase("F", vk)) {
				return 1;
			} else if (StringUtils.equalsIgnoreCase("R", vk)) {
				return 2;
			} else if (StringUtils.equalsIgnoreCase("E", vk)) {
				return 3;
			} else {
				throw new IllegalArgumentException("Unknown road category! [" + vk + "]");
			}
		}
	}
}
