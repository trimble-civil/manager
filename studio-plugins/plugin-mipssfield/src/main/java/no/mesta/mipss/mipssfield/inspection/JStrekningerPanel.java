package no.mesta.mipss.mipssfield.inspection;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.decorator.SortOrder;

public class JStrekningerPanel extends JPanel{
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 100);
	public static final Dimension PREF_SIZE = new Dimension(450, 200);
	private static final int STRUT = 2;
	private final Inspeksjon source;
	private JMipssBeanTable<Prodstrekning> table;
	private final JInspectionDetailPanel parent;

	public JStrekningerPanel(final JInspectionDetailPanel parent, final Inspeksjon source) {
		this.source = source;
		this.parent = parent;

		initTable();

		JScrollPane tableScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setViewportView(table);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, tableScrollPane));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	public void initTable() {
		MipssBeanTableModel<Prodstrekning> tableModel = new MipssBeanTableModel<Prodstrekning>("strekninger",
				Prodstrekning.class, "fylkesnummer", "kommunenummer", "vei", "hp", "fraKm", "tilKm");
		tableModel.setSourceObject(source);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Prodstrekning.class,
				tableModel.getColumns());
		table = new JMipssBeanTable<Prodstrekning>(tableModel, columnModel);
		table.setDefaultRenderer(Double.class, new DoubleRenderer());
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		table.setSortable(true);
		table.setSortOrder(0, SortOrder.DESCENDING);
		table.setHorizontalScrollEnabled(true);
		table.setEnabled(false);
		parent.getFieldToolkit().controlEnabledField(table);
		
		setColumnWidth(table.getColumn(0), 50);
		setColumnWidth(table.getColumn(1), 50);
		setColumnWidth(table.getColumn(2), 50);
		setColumnWidth(table.getColumn(3), 50);
	}
	
	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}
}
