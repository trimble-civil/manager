package no.mesta.mipss.mipssfield;

import javax.swing.JDialog;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.process.JProcessPanel;
import no.mesta.mipss.ui.roadpicker.KontraktVeinettPanel;


@SuppressWarnings("serial")
public class InspeksjonrapportHandler{
	private final MipssFieldModule plugin;
	private JDialog utvalgsDialog ;
	private JProcessPanel prosessPanel;
	private KontraktVeinettPanel veinettPanel;
	private JMipssDatePicker fromDatePicker;
	private JMipssDatePicker toDatePicker;
	private Driftkontrakt kontrakt;

	public InspeksjonrapportHandler(MipssFieldModule plugin){
		this.plugin = plugin;
		utvalgsDialog = new JDialog(plugin.getLoader().getApplicationFrame(), "Inspeksjonrapport", true);
		utvalgsDialog.setSize(560,550);
//		initGui();
	}

//	private void initGui() {
//		kontrakt = plugin.getCommonFilterPanel().getValgtKontrakt();
//		
//		prosessPanel = new JProcessPanel(plugin.getInspectionFilter(), "kontraktId", false);
//		
//		veinettPanel = new KontraktVeinettPanel(kontrakt, false, true, true, true, false);
//		veinettPanel.setBorder(BorderFactory.createTitledBorder("Velg veier"));
//		
//		JButton okButton = new JButton(new OkAction("Lag rapport", IconResources.OK2_ICON));
//		JButton cancelButton = new JButton(new CancelAction("Avbryt", IconResources.CANCEL_ICON));
//
//		JPanel datePanelVert = new JPanel();
//		datePanelVert.setLayout(new BoxLayout(datePanelVert, BoxLayout.PAGE_AXIS));
//		
//		JPanel datePanel = new JPanel();
//		datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.LINE_AXIS));
//		JLabel fra = new JLabel("fra:");
//		JLabel til = new JLabel("til:");
//		
//		fromDatePicker = new JMipssDatePicker();
//		fromDatePicker.setDate(plugin.getCommonFilterPanel().getFromDate());
//		toDatePicker = new JMipssDatePicker();
//		toDatePicker.setDate(plugin.getCommonFilterPanel().getToDate()!=null?plugin.getCommonFilterPanel().getToDate():Clock.now());
//		
//		datePanel.add(Box.createHorizontalStrut(5));
//		datePanel.add(fra);
//		datePanel.add(Box.createHorizontalStrut(5));
//		datePanel.add(fromDatePicker);
//		datePanel.add(Box.createHorizontalStrut(10));
//		datePanel.add(til);
//		datePanel.add(Box.createHorizontalStrut(5));
//		datePanel.add(toDatePicker);
//		datePanel.add(Box.createHorizontalGlue());
//		datePanelVert.setBorder(BorderFactory.createTitledBorder("Tidsperiode:"));
//		
//		datePanelVert.add(datePanel);
//		datePanelVert.add(Box.createVerticalStrut(5));
//		
//		JPanel buttonPanel = new JPanel(new GridBagLayout());
//		JPanel utvalgsPanel = new JPanel(new GridBagLayout());
//		
//		buttonPanel.add(okButton, 	  new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,10), 0,0));
//		buttonPanel.add(cancelButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0,0));
//		
//		utvalgsPanel.add(datePanelVert,    new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,5,0), 0,0));
//		utvalgsPanel.add(prosessPanel, new GridBagConstraints(0,2, 1,1, 1.0,0.3, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,5,0), 0,0));
//		utvalgsPanel.add(veinettPanel, new GridBagConstraints(0,3, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,5,0), 0,0));
//		utvalgsPanel.add(buttonPanel,  new GridBagConstraints(0,4, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
//		utvalgsDialog.add(utvalgsPanel);
//		utvalgsDialog.setLocationRelativeTo(plugin.getModuleGUI());
//		utvalgsDialog.setVisible(true);
//    	
//		
//		
//    	
//	}
//	private List<InspeksjonsRapport> filtrerProsess(List<InspeksjonsRapport> rapportData, Set<Prosess> prosesser){
//		if (prosesser.size()==0)
//			return rapportData;
//		Set<String> prosessString = new HashSet<String>();
//		for (Prosess p:prosesser){
//			prosessString.add(p.getProsessKode());
//		}
//		List<InspeksjonsRapport> filtrert = new ArrayList<InspeksjonsRapport>();
//		for (InspeksjonsRapport i:rapportData){
//			String[] p = i.getProsessKontrollert().split(",");
//			for (String pk:p){
//				if (prosessString.contains(pk)){
//					filtrert.add(i);
//					break;
//				}
//			}
//		}
//		
//		return filtrert;
//	}
//	
//	private void lagFalskeStrekningerAvAvvik(List<InspeksjonsRapport> rapportdata) {
//		for (InspeksjonsRapport r:rapportdata){
//			for (Avvik a:r.getAvvik()){
//				StrekningAvvik t = new StrekningAvvik();
//				Strekning s = new Strekning();
//				s.setFraKm(a.getKm());
//				s.setTilKm(a.getKm());
//				s.setFylkesnummer(a.getFylkesnummer());
//				s.setKommunenummer(a.getKommunenummer());
//				s.setVeikategori(a.getVeikategori());
//				s.setVeistatus(a.getVeistatus());
//				s.setVeinummer(a.getVeinummer());
//				s.setHp(a.getHp());
//				s.setFraTidspunkt(a.getOpprettetDato());
//				s.setTilTidspunkt(a.getOpprettetDato());
//				List<Avvik> sl = new ArrayList<Avvik>();
//				sl.add(a);
//				t.setStrekning(s);
//				t.setAvvikList(sl);
//				if (r.getStrekningAvvik()==null){
//					r.setStrekningAvvik(new ArrayList<StrekningAvvik>());
//				}
//				r.getStrekningAvvik().add(t);
//			}
//		}
//	}
//	
//	class OkAction extends AbstractAction{
//
//		public OkAction(String text, Icon icon){
//			super(text, icon);
//		}
//		@Override
//		public void actionPerformed(ActionEvent e) {
//			utvalgsDialog.dispose();
//			if (veinettPanel.getValgte().size()>0){
//	    		
//	    		Date fraDato = fromDatePicker.getDate();
//	    		Date tilDato = toDatePicker.getDate();
//	    		InspeksjonsrapportParams params = new InspeksjonsrapportParams();
//	    		params.setVeireferanser(veinettPanel.getValgte());
//	    		params.setFraTidspunkt(fraDato);
//	    		params.setTilTidspunkt(tilDato);
//	    		params.setDriftkontraktId(kontrakt.getId());
//	    		params.setInkluderKm(Boolean.FALSE);
//	    		
//	    		InspeksjonsrapportService bean = BeanUtil.lookup(InspeksjonsrapportService.BEAN_NAME, InspeksjonsrapportService.class);
//	    		MipssLogger l = new MipssLogger(plugin.getLoader().getLoggedOnUserSign());
//	    		l.start();
//	    		List<InspeksjonsRapport> rapportdata=null;
//				try {
//					rapportdata = bean.hentRapportdata(params);
//					lagFalskeStrekningerAvAvvik(rapportdata);
//					rapportdata = filtrerProsess(rapportdata, prosessPanel.getValgteProsesser());
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				} 
//	    		InspectionReport inspectionReport = new InspectionReport();
//	    		inspectionReport.setInspeksjonList(rapportdata);
//	    		inspectionReport.setDriftkontrakt(kontrakt);
//	    		inspectionReport.setPeriodeFra(fraDato);
//	    		inspectionReport.setPeriodeTil(tilDato);
//	    		
//	    		try {
//					BeanDataSource<InspectionReport> ds = new BeanDataSource<InspectionReport>(inspectionReport);
//					JReportBuilder builder = new JReportBuilder(Report.INSPECTION_REPORT, Report.INSPECTION_REPORT
//							.getDefaultParameters(), ds, ReportHelper.EXPORT_PDF, plugin.getLoader().getApplicationFrame(), true);
//					File report = builder.getGeneratedReportFile();
//					if (report != null) {
//						DesktopHelper.openFile(report);
//					}
//					l.logg(LoggType.HENT_RAPPORT, "INSPEKSJONSRAPPORT");
//				} catch (Throwable t) {
//					plugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
//					l.logg(LoggType.HENT_RAPPORT_FEIL, t.getMessage());
//				}finally{
//					utvalgsDialog.setVisible(false);
//				}
//				
//	    	}
//		}
//	}
//
//	class CancelAction extends AbstractAction{
//
//		public CancelAction(String text, Icon icon){
//			super(text, icon);
//		}
//		@Override
//		public void actionPerformed(ActionEvent e) {
//			utvalgsDialog.dispose();
//		}
//		
//	}
	
}
