package no.mesta.mipss.mipssfield.inspection;

import java.awt.Dimension;

import javax.persistence.Column;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.MaxLengthEnforcer;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.ComponentSizeResources;


/**
 * Generell kommentar om hendelse.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JCommentPanel extends JAbstractInfoPanel {
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(120, 116);
	public static final Dimension PREF_SIZE = new Dimension(450, 232);
	private static final String BESKRIVELSE = "beskrivelse";
    
    /**
     * Konstruktør
     * 
     * @param parentPlugin
     * @param toolkit
     * @param punktreg
     */
    public JCommentPanel(final JInspectionDetailPanel owner, 
                         final Inspeksjon inspeksjon) {
        setFieldToolkit(owner.getFieldToolkit());
        setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.COMMENT)));

        JTextArea field = getFieldToolkit().createTextInputArea(Avvik.class, inspeksjon, BESKRIVELSE);
		field.setLineWrap(true);
		field.setWrapStyleWord(true);
		field.setEnabled(false);
		getFieldToolkit().addBinding(BindingHelper.createbinding(inspeksjon, "${tilTidspunkt!=null}", field, "editable"));

        JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.setViewportView(field);
        JComponent panel = createPanel((JComponent) null , pane);

        Dimension max = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Dimension preferred = new Dimension(400, 140);
        Dimension min = new Dimension(200, 80);
        ComponentSizeResources.setComponentSizes(panel, min, preferred, max);
        ComponentSizeResources.setComponentSizes(pane, min, preferred, max);

        Integer maxLength = (Integer) inspeksjon.getAnnotationValue("getBeskrivelse", Column.class, "length");
        MaxLengthEnforcer enforcer = new MaxLengthEnforcer(inspeksjon, field, BESKRIVELSE, maxLength);
        enforcer.start();

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(panel);
        add(Box.createVerticalGlue());

        setMinimumSize(MIN_SIZE);
        setPreferredSize(PREF_SIZE);
        setMaximumSize(MAX_SIZE);
    }
}
