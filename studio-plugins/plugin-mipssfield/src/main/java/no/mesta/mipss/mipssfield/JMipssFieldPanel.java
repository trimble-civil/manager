package no.mesta.mipss.mipssfield;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.mipssfield.discovery.JDiscoveryPanel;
import no.mesta.mipss.mipssfield.elrapp.ElrappPanel;
import no.mesta.mipss.mipssfield.incident.JIncidentPanel;
import no.mesta.mipss.mipssfield.inspection.JInspectionPanel;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hoved gui panel for plugin
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JMipssFieldPanel extends JPanel {
	private static final ImageIcon[] icons = new ImageIcon[] { IconResources.BIL_SMALL_ICON,
			IconResources.MAP_PIN_RIGHTY_SM, IconResources.MAP_PIN_LEFT_SM, IconResources.ELRAPP_ICON_16 };
	private static final Logger logger = LoggerFactory.getLogger(JMipssFieldPanel.class);
	private MipssFieldModule parentPlugin;
	private JTabbedPane tabPanel = new JTabbedPane();
	private JInspectionPanel inspectionPanel;
	private JCommonFilterFields commonFilterFields;
	private JIncidentPanel incidentPanel;
	private JDiscoveryPanel discoveryPanel;
	private ElrappPanel elrappPanel;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	public JMipssFieldPanel(MipssFieldModule parentPlugin) {
		this.parentPlugin = parentPlugin;

		initGui();
		setPreferredSize(new Dimension(939, 600));
	}

	/**
	 * Init gui for plugin
	 * 
	 */
	private void initGui() {
//		logger.debug("initGui() start");
		setLayout(new BorderLayout());
		commonFilterFields = new JCommonFilterFields(parentPlugin);
		parentPlugin.setCommonFilterPanel(commonFilterFields);
//
		inspectionPanel = new JInspectionPanel(parentPlugin);
		tabPanel.addTab(GUITexts.getText(GUITexts.INSPECTIONS), inspectionPanel);
		
		tabPanel.setTabComponentAt(0, new JMipssFieldTabComponent(GUITexts.getText(GUITexts.INSPECTIONS), 0,
				parentPlugin.getInspectionColor(), inspectionPanel, tabPanel));
//
		incidentPanel = new JIncidentPanel(parentPlugin);
		tabPanel.addTab(GUITexts.getText(GUITexts.INCIDENTS), incidentPanel);
		tabPanel.setTabComponentAt(1, new JMipssFieldTabComponent(GUITexts.getText(GUITexts.INCIDENTS), 1, parentPlugin
				.getIncidentColor(), incidentPanel, tabPanel));
//
		discoveryPanel = new JDiscoveryPanel(parentPlugin);
		tabPanel.addTab(GUITexts.getText(GUITexts.DISCOVERIES), discoveryPanel);
		tabPanel.setTabComponentAt(2, new JMipssFieldTabComponent(GUITexts.getText(GUITexts.DISCOVERIES), 2, parentPlugin.getDiscoveryColor(), discoveryPanel, tabPanel));
//
		elrappPanel = new ElrappPanel(parentPlugin);
		tabPanel.addTab(GUITexts.getText(GUITexts.ELRAPP), elrappPanel);
		tabPanel.setTabComponentAt(3, new JMipssFieldTabComponent(GUITexts.getText(GUITexts.ELRAPP), 3, null, elrappPanel, tabPanel));
//		
		add(parentPlugin.getCommonFilterPanel(), BorderLayout.NORTH);
		add(tabPanel, BorderLayout.CENTER);
		logger.debug("initGui() end");
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		inspectionPanel.dispose();
		inspectionPanel = null;
		incidentPanel.dispose();
		incidentPanel = null;
		discoveryPanel.dispose();
		discoveryPanel=null;
		
		parentPlugin.setCommonFilterPanel(null);
		commonFilterFields.dispose();
		commonFilterFields = null;
		parentPlugin = null;
	}
	/**
	 * Setter aktivt faneark
	 * 
	 * @param mode
	 */
	public void setTabPane(String mode) {
		if (StringUtils.equals(MipssFieldModule.MODE_DISCOVERIES, mode)) {
			tabPanel.setSelectedIndex(2);
		} else if (StringUtils.equals(MipssFieldModule.MODE_INCIDENT, mode)) {
			tabPanel.setSelectedIndex(1);
		} else if (StringUtils.equals(MipssFieldModule.MODE_INSPECTION, mode)) {
			tabPanel.setSelectedIndex(0);
		} else if (StringUtils.equals(MipssFieldModule.MODE_ELRAPP, mode)){
			tabPanel.setSelectedIndex(3);
		}else {
			throw new IllegalArgumentException("Unknown mode {" + mode + "}");
		}
	}

	/**
	 * Pynter på faneark tabfeltene
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class JMipssFieldTabComponent extends JComponent {
		public JMipssFieldTabComponent(String name, final int index, final Color background, final Component owner,
				final JTabbedPane tabbedPane) {
//			double FACTOR = 1.1d;
//			setBackground(background);
//			final Color light = new Color(Math.min((int) (background.getRed() * FACTOR), 255), Math.min(
//					(int) (background.getGreen() * FACTOR), 255), Math.min((int) (background.getBlue() * FACTOR), 255));
//			setLayout(new FlowLayout());
//			setBackground(light);
//			setOpaque(false);
//
//			final ChangeListener changeListener = new ChangeListener() {
//				public void stateChanged(ChangeEvent e) {
//					JTabbedPane source = (JTabbedPane) e.getSource();
//					JPanel tab = (JPanel) source.getSelectedComponent();
//
//					if (tab != owner) {
//						setBackground(background);
//						source.setBackgroundAt(index, background);
//					} else {
//						setBackground(light);
//						source.setBackgroundAt(index, light);
//					}
//				}
//			};
//			addMouseListener(new MouseAdapter() {
//				/** {@inheritDoc} */
//				public void mouseClicked(MouseEvent e) {
//					tabbedPane.setSelectedComponent(owner);
//					tabbedPane.setBackgroundAt(index, light);
//					setBackground(light);
//				}
//
//				/** {@inheritDoc} */
//				public void mouseEntered(MouseEvent e) {
//					setBackground(light);
//					tabbedPane.setBackgroundAt(index, light);
//				}
//
//				/** {@inheritDoc} */
//				public void mouseExited(MouseEvent e) {
//					setBackground(background);
//
//					JPanel tab = (JPanel) tabbedPane.getSelectedComponent();
//
//					if (tab != owner) {
//						tabbedPane.setBackgroundAt(index, background);
//						setBackground(background);
//					} else {
//						tabbedPane.setBackgroundAt(index, light);
//						setBackground(light);
//					}
//				}
//			});

			JLabel label = new JLabel(name, icons[index], JLabel.LEFT);
//			label.setOpaque(false);
//			label.setFocusable(false);
//			label.setBorder(null);
			
			ComponentSizeResources.setComponentSize(label, new Dimension(85, 19));
			setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
			add(Box.createHorizontalStrut(2));
			add(label);
			ComponentSizeResources.setComponentSize(this, new Dimension(85, 20));

//			setBorder(null);
//			tabbedPane.addChangeListener(changeListener);
		}
	}
}
