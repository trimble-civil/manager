package no.mesta.mipss.mipssfield;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.plugin.PluginMessage;

/**
 * Benyttes til å sette opp detalj om funn, inspeksjon eller hendelse i MIPSS Studio. Bytter faneark.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class OpenDetailMessage extends PluginMessage<String,String> {
	private List<MipssFieldDetailItem> items;
	
    public OpenDetailMessage() {
    	this(new ArrayList<MipssFieldDetailItem>());
    }

    public OpenDetailMessage(final List<MipssFieldDetailItem> items) {
    	this.items = items;
    }
    
    /**
     * Ikke i bruk
     * 
     * @return
     */
    public Component getMessageGUI() {
        return null;
    }

    /**
     * Ikke i bruk
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
	public Map<String, String> getResult() {
        return Collections.EMPTY_MAP;
    }

    /** {@inheritDoc} */
    public void processMessage() {
        if(isConsumed()) {
            return;
        }
           
        MipssFieldModule plugin = (MipssFieldModule) getTargetPlugin();
        plugin.openItems(items);
        
        setConsumed(true);
    }

    public void setItems(final List<MipssFieldDetailItem> items) {
    	this.items = items != null ? items : new ArrayList<MipssFieldDetailItem>();
    }
}
