package no.mesta.mipss.mipssfield.discovery;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.LongBooleanConverter;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.JMipssDatePicker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for å vise vurdering av et funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JAssessmentPanel extends JAbstractInfoPanel implements PropertyChangeListener {
	private static final Logger logger = LoggerFactory.getLogger(JAssessmentPanel.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 120);
	public static final Dimension MIN_SIZE = new Dimension(120, 120);
	public static final Dimension PREF_SIZE = new Dimension(200, 120);
	private static final Integer STRUT = 2;
	private final JDiscoveryDetailPanel owner;
//	private final JComboBox prosessCombo;
	private final Avvik punktreg;
//	private final JComboBox underprosessCombo;
//	private final MipssObservableList<Prosess> underprosesser;
//	private MipssComboBoxModel<Prosess> prosessModel;

	public JAssessmentPanel(final JDiscoveryDetailPanel owner, Avvik punktreg) {
		setFieldToolkit(owner.getFieldToolkit());

		this.punktreg = punktreg;
		this.owner = owner;

		Long kontraktId = punktreg.getSak().getKontraktId();
		Long prosessId = punktreg.getProsessId();
//		List<Underprosess> underprosesser = owner.getParentPlugin().hentUnderprosess(kontraktId, prosessId);
//		this.underprosesser = new MipssObservableList<Prosess>();//(underprosesser);

//		prosessCombo = getFieldToolkit().createDropDown(Punktreg.class, punktreg, "prosess", null, Prosess.class,
//				owner.getParentPlugin().getProsessForKontrakt(kontraktId));
//		prosessCombo = new JComboBox();
//		MipssListCellRenderer<Prosess> prosessRenderer = new MipssListCellRenderer<Prosess>("<ikke satt>");
//		prosessCombo.setRenderer(prosessRenderer);
//		prosessModel = new MipssComboBoxModel<Prosess>(owner.getParentPlugin().getProsessForKontrakt(kontraktId), true);
//		prosessCombo.setModel(prosessModel);
////		prosessCombo.addItemListener(new ItemListener(){
//			@Override
//			public void itemStateChanged(ItemEvent e) {
//				itemChanged(e);
//			}
//		});
		
//		punktreg.addPropertyChangeListener("prosess", this);

//		MipssListCellRenderer<Prosess> renderer = new MipssListCellRenderer<Prosess>("<ikke satt>");
//		underprosessCombo = getFieldToolkit().createDropDown(Punktreg.class, punktreg, "prosess", null, Prosess.class, this.underprosesser, renderer);
//		underprosessCombo.setEnabled(false);

		JComboBox tilstandCombo = getFieldToolkit().createDropDown(Avvik.class, punktreg, "tilstand", null, Avviktilstand.class, owner.getParentPlugin().getPunktregtilstandtyper());
		tilstandCombo.setEnabled(false);

		JCheckBox etterslepBox = getFieldToolkit().createCheckBox(Avvik.class, punktreg, "etterslepFlagg", new LongBooleanConverter(), GUITexts.getText(GUITexts.BACKLOG));
		etterslepBox.setEnabled(false);
		JCheckBox trafikksikkerhetBox = getFieldToolkit().createCheckBox(Avvik.class, punktreg,
				"trafikksikkerhetFlagg", new LongBooleanConverter(), GUITexts.getText(GUITexts.TRAFFICSAFETY));
		trafikksikkerhetBox.setEnabled(false);
		JCheckBox tilleggsarbeidBox = getFieldToolkit().createCheckBox(Avvik.class, punktreg,
				"avvikUtenforKontraktFlagg", new LongBooleanConverter(), GUITexts.getText(GUITexts.EXTRAWORK));
		tilleggsarbeidBox.setEnabled(false);

		JMipssDatePicker picker = toolkit.createDatePicker(Avvik.class, punktreg, "tiltaksDato",
				MipssDateFormatter.SHORT_DATE_FORMAT);
		picker.setEnabled(false);
//		picker.setNotBeforeDate(punktreg.getOpprettetDato());

//		NullEntityValidator<Punktreg, Prosess> prosessValidator = new NullEntityValidator<Punktreg, Prosess>(punktreg, "prosess");
//		NullEntityValidator<Punktreg, Underprosess> underProsessValidator = new NullEntityValidator<Punktreg, Underprosess>(punktreg, "underprosess");
//		owner.getSaveStateObserver().addValidator(prosessValidator);
//		owner.getSaveStateObserver().addValidator(underProsessValidator);
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

//		add(BoxUtil.createVerticalStrut(STRUT));
//		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Prosess>(GUITexts.getText(GUITexts.PROCESS), prosessCombo, null)));
		add(BoxUtil.createVerticalStrut(STRUT));
//		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Prosess>(GUITexts.getText(GUITexts.UNDER_PROCESS), underprosessCombo, prosessValidator)));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Avviktilstand>(GUITexts.getText(GUITexts.CONDITION), tilstandCombo, null)));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, BoxUtil.createHorizontalStrut(61), etterslepBox, BoxUtil
				.createHorizontalStrut(STRUT), trafikksikkerhetBox, BoxUtil.createHorizontalStrut(STRUT),
				tilleggsarbeidBox, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Date>(GUITexts.getText(GUITexts.DEADLINE), picker, null), Box
				.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
		
//		if (punktreg.getProsess()!=null)
//			velgProsess(punktreg.getProsess());
//		velgUnderprosess(punktreg.getProsess());
	}

	private Prosess getRoot(Prosess a){
		if (a==null)
			return null;
		Prosess root = a.getEierprosess();
		if (root==null){
			return a;
		}else{
			return getRoot(root);
		}
	}
//	private void velgProsess(Prosess prosess){
//		Prosess eier = getRoot(prosess);
//		
//		
//		boolean found = false;
//		for (Prosess p : prosessModel.getEntities()){
//			if (p.equals(eier)){
//				found = true;
//			}
//		}
//		prosessCombo.setSelectedItem(null);
//		if (found){
//			prosessCombo.setSelectedItem(eier);
//		}else{
//			List<Prosess> entities = prosessModel.getEntities();
//			if (eier==null){
//				eier = prosess;
//			}
//			entities.add(eier);
//			prosessModel.setEntities(entities);
//			prosessCombo.setSelectedItem(eier);
//		}
//	
//		System.out.println(eier);
//	}
	
//	private void velgUnderprosess(Prosess prosess){
//		boolean found = false;
//		for (int i=0;i<prosessCombo.getItemCount();i++){
//			Prosess p = (Prosess)prosessCombo.getItemAt(i);
//			if (p.getId().equals(prosess.getId())){
//				found = true;
//				break;
//			}
//		}
//		System.out.println("underprosess:"+prosess);
//		underprosessCombo.setSelectedItem(prosess);
//	}
	public void itemChanged(ItemEvent evt) {
		logger.debug("propertyChange()");
		Prosess p = (Prosess) evt.getItem();
		if (p != null) {
			Long kontraktId = punktreg.getSak().getKontraktId();
			Long prosessId = p.getId();
//			List<Underprosess> underprosesserNew = owner.getParentPlugin().hentUnderprosess(kontraktId, prosessId);
			List<Prosess> underprosesserNew = new ArrayList<Prosess>();
			for (Prosess pp:p.getProsesser()){
				createListeAvUnderprosesser(underprosesserNew, pp);
			}
			
//			underprosesser.clear();
//			underprosesser.addAll(underprosesserNew);
//
//			if (underprosesser.size() == 1) {
//				punktreg.setProsess(underprosesser.get(0));
//			}
		} 
	}
	private void createListeAvUnderprosesser(List<Prosess> list, Prosess p){
		list.add(p);
		for (Prosess pp:p.getProsesser()){
			createListeAvUnderprosesser(list, pp);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println("PROPERTY CHANGED: JAssessmentPanel... ");
		
	}
}
