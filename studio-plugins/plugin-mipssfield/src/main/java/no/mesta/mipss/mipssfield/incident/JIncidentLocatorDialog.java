package no.mesta.mipss.mipssfield.incident;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Transient;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.HendelseQueryFilter;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.discovery.JPunktregLocatorDialog.PunktregLocate;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.BooleanRenderer;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SortOrder;

public class JIncidentLocatorDialog extends JDialog {
	private static final String APP = "MIPSS_FELT";
	private static final String APP_VALUE = "Funn_radius";
	private static final Logger logger = LoggerFactory.getLogger(JIncidentLocatorDialog.class);
	private static final int STRUT = 2;
	private final MipssField BEAN;
	private Box buttons;
	private boolean cancelled;
	private Point center;
	private HendelseLocate hendelseLocate;
	private List<HendelseLocate> hendelser = new ArrayList<HendelseLocate>();
	private JMipssBeanTable<HendelseLocate> table;
	private static final Dimension BUTTON_SIZE = new Dimension(45, 22);
	private final Avvik punktreg;
	
	public JIncidentLocatorDialog(final Avvik punktreg, final JFrame owner, final Driftkontrakt kontrakt, double x, double y) {
		super(owner, GUITexts.getText(GUITexts.SELECT_INCIDENT), true);
		this.punktreg = punktreg;
		
		center = new Point();
		center.setLocation(x, y);

		Konfigparam param = KonfigparamPreferences.getInstance().hentEnForApp(APP, APP_VALUE);
		double boxReach = 100;
		try {
			boxReach = Double.valueOf(param.getVerdi());
		} catch (Throwable t) {
			logger.warn("Parsing " + APP + "," + APP_VALUE + " gave Exception", t);
			t = null;
		}

		BEAN = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		HendelseQueryFilter filter = new HendelseQueryFilter(null);
		filter.setKontraktId(kontrakt.getId());
		filter.setOrigo(center);
		filter.setBoxReach(boxReach);
		List<HendelseSokView> viewHendelser = BEAN.sokHendelser(filter);

		for (HendelseSokView vh : viewHendelser) {
			if (vh.getFraX() != null && vh.getFraY() != null) {
				//TODO utkommentert pga modellendring
//				Boolean first = vh.getGuid().equals(punktreg.getHendelseGuid());
//				hendelser.add(new HendelseLocate(vh, center, first));
			}
		}

		initWindow();
		initTable();
		initButtons();

		JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pane.setViewportView(table);

		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, pane));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, buttons));
		add(BoxUtil.createVerticalStrut(STRUT));

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public Hendelse getHendelse() {
		if (!cancelled && hendelseLocate != null) {
			Hendelse hendelse = BEAN.hentHendelse(hendelseLocate.getGuid());
			return hendelse;
		} else {
			return null;
		}
	}

	private void initButtons() {
		buttons = Box.createHorizontalBox();

		JButton add = new JButton(GUITexts.getText(GUITexts.ATTATCH));
		ComponentSizeResources.setComponentSize(add, BUTTON_SIZE);
		add.setMargin(new Insets(1, 1, 1, 1));
		add.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				hendelseLocate = table.getSelectedEntities().get(0);
				setVisible(false);
			}
		});
		Binding<JMipssBeanTable<HendelseLocate>, Boolean, JButton, Boolean> enableAdd = BindingHelper.createbinding(
				table, "${noOfSelectedRows == 1}", add, "enabled");
		enableAdd.bind();

		JButton cancel = new JButton(GUITexts.getText(GUITexts.CANCEL));
		ComponentSizeResources.setComponentSize(cancel, BUTTON_SIZE);
		cancel.setMargin(new Insets(1, 1, 1, 1));
		cancel.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = true;
				setVisible(false);
			}
		});

		buttons.add(Box.createHorizontalGlue());
		buttons.add(cancel);
		buttons.add(BoxUtil.createHorizontalStrut(STRUT));
		buttons.add(add);
	}

	private void initTable() {
		MipssBeanTableModel<HendelseLocate> model = new MipssBeanTableModel<HendelseLocate>(HendelseLocate.class,
				"dato", "distance", "hpGuiText", "antallFunn", "antallBilder", "typeNavn", "aarsakNavn",
				"tiltakTekster", "kommune", "skadested", "funnetUnderInspeksjon", "r5Sak", "opprettetAv", "pdaDfuNavn",
				"beskrivelse", "hvalue");
		model.setEntities(hendelser);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
				HendelseLocate.class, model.getColumns());
		table = new JMipssBeanTable<HendelseLocate>(model, columnModel);
		table.setDefaultRenderer(Boolean.class, new BooleanRenderer());
		table.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setSortOrder(15, SortOrder.ASCENDING);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					table.selectRow(e);
					hendelseLocate = table.getSelectedEntities().get(0);
					setVisible(false);
				} else {
					hendelseLocate = table.getSelectedEntities().get(0);
				}
			}
		});
		table.setHighlighters(new Highlighter[] { new ColorHighlighter(new HendelseHighlightPredicate(),
				Color.YELLOW, getCellForeground(), getSelectedBackground(), getSelectedForeground()) });
		table.getColumnExt(15).setVisible(false);
		setColumnWidth(table.getColumn(0), 80);
		setColumnWidth(table.getColumn(1), 55); // distance
		setColumnWidth(table.getColumn(2), 135); // hpGuiText
		setColumnWidth(table.getColumn(3), 45); // antallFunn
		setColumnWidth(table.getColumn(4), 45); // antallBilder
		setColumnWidth(table.getColumn(10), 30); // funnetUnderInspeksjon
		setColumnWidth(table.getColumn(11), 30); // r5Sak
	}
	
	public Color getCellForeground() {
		return UIManager.getColor("Table.foreground");
	}
	public Color getSelectedBackground() {
		return UIManager.getColor("Table.selectionBackground");
	}

	public Color getSelectedForeground() {
		Color c = new Color(200, 0, 0);
		return c;
	}
	private class HendelseHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@SuppressWarnings("unchecked")
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;

			MipssRenderableEntityTableModel<HendelseLocate> tableModel = (MipssRenderableEntityTableModel<HendelseLocate>) table.getModel();
			int rowModel = table.convertRowIndexToModel(row);
			HendelseLocate hendelse = tableModel.get(rowModel);
			
			boolean highlight = false;
			if (hendelse != null) {
				//TODO utkommentert pga modellendring
//				highlight = hendelse.getGuid().equals(punktreg.getHendelseGuid());

			}
			
			return highlight;
		}
	}
	
	private void initWindow() {
		setDefaultCloseOperation(JIncidentLocatorDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			/** {@inheritDoc} */
			@Override
			public void windowClosing(WindowEvent e) {
				cancelled = true;
				dispose();
			}
		});
	}

	private void setColumnWidth(TableColumn column, int width) {
		column.setPreferredWidth(width);
		column.setMinWidth(width);
	}

	public static class HendelseLocate extends HendelseSokView {
		private final Point origo;
		private final Point position;
		private Boolean first;

		public HendelseLocate(HendelseSokView view, Point center, Boolean first) {
			origo = center;
			this.first = first;
			if (view != null) {
				mergeValues(view);
			}
			setGuid(view.getGuid());

			position = new Point();
			position.setLocation(getFraX(), getFraY());
		}
		
		public Integer getHvalue(){
			if (first)
				return -1;
			return (int) origo.distance(position);
		}
		
		@Transient
		public Integer getDistance() {
			return (int) origo.distance(position);
		}
	}
}
