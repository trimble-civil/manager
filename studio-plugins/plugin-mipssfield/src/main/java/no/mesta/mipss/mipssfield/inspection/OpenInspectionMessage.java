package no.mesta.mipss.mipssfield.inspection;

import no.mesta.mipss.mipssfield.OpenTabMessage;

/**
 * Melding for å åpne inspeksjon faneark.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class OpenInspectionMessage extends OpenTabMessage {
    public OpenInspectionMessage() {
    }
}
