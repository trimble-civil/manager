/**
 * 
 */
package no.mesta.mipss.mipssfield.discovery;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.vo.PunktregResult;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sletter siste status for et punkt
 * 
 * Kalles fra GUI
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SlettSisteStatusProcess {
	private static final Logger logger = LoggerFactory.getLogger(SlettSisteStatusProcess.class);
	private final JDiscoveryDetailPanel parent;

	public SlettSisteStatusProcess(final JDiscoveryDetailPanel parent) {
		this.parent = parent;
	}

	public void execute() {
		boolean slettStatus = true;
		if (parent.getFieldToolkit().isFieldsChanged()) {
			MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.WARNING_ICON, GUITexts
					.getText(GUITexts.DISCOVERY_CLOSEWARNING_TITLE), GUITexts
					.getText(GUITexts.DISCOVERY_CLOSEWARNING_TEXT), MipssDialogue.Type.WARNING,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
				slettStatus = false;
			}
		}

		Avvik punkt = parent.getDetailObject();
		PunktregSok view = parent.getViewObject();
		
		if (slettStatus) {
			PunktregResult result = parent.getMipssField().slettSisteStatus(punkt.getGuid());
			view.merge(result.getView());
			punkt.merge(result.getPunktreg());
			parent.reset();
			logger.debug("Toolkit changed fields status: " + parent.getFieldToolkit().isFieldsChanged());
			logger.debug("Punktreg status is: " + punkt.getStatus());
		}
	}
}
