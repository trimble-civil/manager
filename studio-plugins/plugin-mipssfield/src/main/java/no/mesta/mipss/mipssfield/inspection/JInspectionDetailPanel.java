package no.mesta.mipss.mipssfield.inspection;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractDetailPanel;
import no.mesta.mipss.mipssfield.JDetailFrame;
import no.mesta.mipss.mipssfield.JMapPanel;
import no.mesta.mipss.mipssfield.JTopLine;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.vo.InspeksjonResult;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JLoadingPanel;

/**
 * Detalj panel for inspeksjoner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JInspectionDetailPanel extends JAbstractDetailPanel<InspeksjonSokView, Inspeksjon> {
	private static final int STRUT = 2;
    private final JInspectionDetailActions actionPanel;
    private Inspeksjon inspeksjon;
	private final InspeksjonSokView inspeksjonSok;
	private final JMapPanel mapPanel;
	private final MipssFieldModule parentPlugin;

	public JInspectionDetailPanel(final MipssFieldModule parentPlugin,final JDetailFrame owner,  final InspeksjonSokView inspeksjonSok) {
        super(inspeksjonSok, owner, parentPlugin, null);
        this.parentPlugin = parentPlugin;
        this.inspeksjonSok = inspeksjonSok;
        setSaveStateObserver(new SaveStatusObserver());
         
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        JLoadingPanel timeAndPlace = createTimeAndPlacePanel();
        JLoadingPanel assessmentPanel = createAssessmentPanel();
        JLoadingPanel commentPanel = createCommentPanel();
        mapPanel = createMapPanel();
        JLoadingPanel punktregPanel = createObservationPanel();
        JLoadingPanel strekningPanel = createStrekningPanel();
        JLoadingPanel prosesserPanel = createProsesserPanel();
        actionPanel = new JInspectionDetailActions(this);
        
		add(new JTopLine(parentPlugin.getInspectionColor()));
        add(BoxUtil.createVerticalStrut(STRUT));
        add(BoxUtil.createHorizontalBox(0, timeAndPlace, BoxUtil.createHorizontalStrut(STRUT), assessmentPanel));
        add(BoxUtil.createVerticalStrut(STRUT));
        add(BoxUtil.createHorizontalBox(0, commentPanel, BoxUtil.createHorizontalStrut(STRUT), mapPanel));
        add(BoxUtil.createVerticalStrut(STRUT));
        add(BoxUtil.createHorizontalBox(0, prosesserPanel, BoxUtil.createHorizontalStrut(STRUT), strekningPanel));
        add(BoxUtil.createVerticalStrut(STRUT));
        add(BoxUtil.createHorizontalBox(0, punktregPanel));
        add(BoxUtil.createVerticalStrut(STRUT));
        add(actionPanel);

		SwingWorker<Void, Void> loadInspeksjon = new SwingWorker<Void, Void>() {
			/** {@inheritDoc} */
			@Override
			protected Void doInBackground() throws Exception {
				//TODO utkommentert pga modellendringer
//				Inspeksjon inspeksjon = parentPlugin.getMipssField().hentInspeksjon(inspeksjonSok.getGuid());
//				List<Avvik> punktregs = new ArrayList<Avvik>();
//				for (Avvik p:inspeksjon.getPunktregistreringer()){
//					if (p.getSlettetDato()==null){
//						punktregs.add(p);
//					}
//				}
//				inspeksjon.setPunktregistreringer(punktregs);
//				setInspeksjon(inspeksjon);
				return null;
			}
		};
		loadInspeksjon.execute();
    }

	private JLoadingPanel createAssessmentPanel() {
		JLoadingPanel panel = new JLoadingPanel(JAssessmentPanel.class, new Class<?>[] { JInspectionDetailPanel.class,
			Inspeksjon.class });
		panel.setArgument(JInspectionDetailPanel.class,this);
		ComponentSizeResources.setComponentSizes(panel, JAssessmentPanel.MIN_SIZE, JAssessmentPanel.PREF_SIZE,
				JAssessmentPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.ASSESSMENT)));
		return panel;
	}

	private JLoadingPanel createCommentPanel() {
		JLoadingPanel panel = new JLoadingPanel(JCommentPanel.class, new Class<?>[] { JInspectionDetailPanel.class,
			Inspeksjon.class });
		panel.setArgument(JInspectionDetailPanel.class,this);
		ComponentSizeResources.setComponentSizes(panel, JCommentPanel.MIN_SIZE, JCommentPanel.PREF_SIZE,
				JCommentPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.COMMENT)));
		return panel;
	}

	private JMapPanel createMapPanel() {
		JMapPanel panel = new JMapPanel(parentPlugin, this);
		panel.setBorder(BorderResources.createComponentBorder());
		ComponentSizeResources.setComponentSizes(panel, JMapPanel.MIN_SIZE, JMapPanel.PREF_SIZE, JMapPanel.MAX_SIZE);
		return panel;
	}

	private JLoadingPanel createObservationPanel() {
		JLoadingPanel panel = new JLoadingPanel(JObservationsPanel.class, new Class<?>[] { JInspectionDetailPanel.class,
			Inspeksjon.class });
		panel.setArgument(JInspectionDetailPanel.class,this);
		ComponentSizeResources.setComponentSizes(panel, JObservationsPanel.MIN_SIZE, JObservationsPanel.PREF_SIZE,
				JObservationsPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentBorder());
		return panel;
	}

	private JLoadingPanel createProsesserPanel() {
		JLoadingPanel panel = new JLoadingPanel(JProsesserPanel.class, new Class<?>[] { JInspectionDetailPanel.class,
			Inspeksjon.class });
		panel.setArgument(JInspectionDetailPanel.class,this);
		ComponentSizeResources.setComponentSizes(panel, JProsesserPanel.MIN_SIZE, JProsesserPanel.PREF_SIZE,
				JProsesserPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.INSPECTED_PROCESSES)));
		return panel;
	}

	private JLoadingPanel createStrekningPanel() {
		JLoadingPanel panel = new JLoadingPanel(JStrekningerPanel.class, new Class<?>[] { JInspectionDetailPanel.class,
			Inspeksjon.class });
		panel.setArgument(JInspectionDetailPanel.class,this);
		ComponentSizeResources.setComponentSizes(panel, JStrekningerPanel.MIN_SIZE, JStrekningerPanel.PREF_SIZE,
				JStrekningerPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.STREATCHES)));
		return panel;
	}

	private JLoadingPanel createTimeAndPlacePanel() {
		JLoadingPanel panel = new JLoadingPanel(JTimeAndPlace.class, new Class<?>[] { JInspectionDetailPanel.class,
			Inspeksjon.class });
		panel.setArgument(JInspectionDetailPanel.class,this);
		ComponentSizeResources.setComponentSizes(panel, JTimeAndPlace.MIN_SIZE, JTimeAndPlace.PREF_SIZE,
				JTimeAndPlace.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.TIME_AND_PLACE)));
		return panel;
	}

    /** {@inheritDoc} */
    public void dispose() {
    }
           
    /** {@inheritDoc} */
    @Override
    public Inspeksjon getDetailObject() {
        return inspeksjon;
    }

    public Inspeksjon getInspeksjon() {
		return inspeksjon;
	}
    
    public MipssFieldModule getParentPlugin() {
		return parentPlugin;
	}

    /** {@inheritDoc} */
    @Override
    public String getTabTitle() {
        return inspeksjonSok.getTextForGUI();
    }

    /** {@inheritDoc} */
    @Override
    public Integer getType() {
        return INSPECTION_TYPE;
    }

    /** {@inerhitDoc} */
	@Override
	public InspeksjonSokView getViewObject() {
		return inspeksjonSok;
	}

    /** {@inheritDoc} */
    public String getWindowTitle() {
        return inspeksjonSok.getTextForGUI();
    }

    /**
	 * Resetter GUI
	 * 
	 */
	public void reset() {
		getFieldToolkit().reset();
	}

	private void setInspeksjon(Inspeksjon inspeksjon) {
		getSaveStateObserver().setDetail(inspeksjon);
		this.inspeksjon = inspeksjon;
		mapPanel.setInspeksjon(inspeksjon);
		for (JLoadingPanel panel : loadingPanels) {
			panel.setArgument(Inspeksjon.class, inspeksjon);
		}
		actionPanel.ready();
		waitAndBindWhenReady();
	}

	/** {@inheritDoc} */
	@Override
	public AbstractAction createDefaultSaveAction() {
		return new SaveAction(GUITexts.getText(GUITexts.SAVE), IconResources.SAVE_ICON);
	}
	
	/**
	 * Lagrer endringer
	 * 
	 */
	private class SaveAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public SaveAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			try {
				Inspeksjon inspeksjon = getDetailObject();
				if (inspeksjon.getTilTidspunkt()==null){
					JOptionPane.showMessageDialog(JInspectionDetailPanel.this, GUITexts.getText(GUITexts.WARNING_SAVE_INSPECTION), "Feil ved lagring av inspeksjon", JOptionPane.ERROR_MESSAGE);
					return;
				}
				InspeksjonResult result = getParentPlugin().getMipssField().oppdaterInspeksjon(inspeksjon,
						getParentPlugin().getLoader().getLoggedOnUserSign());
				inspeksjon.merge(result.getInspeksjon());
				getViewObject().merge(result.getInspeksjonSokView());
				reset();
			} catch (Throwable t) {
				getParentPlugin().getLoader().handleException(t, getParentPlugin(), GUITexts.getText(GUITexts.SAVING_FAILED));
			}
		}
	}
}
