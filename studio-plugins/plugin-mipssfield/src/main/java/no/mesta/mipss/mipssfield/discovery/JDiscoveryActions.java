package no.mesta.mipss.mipssfield.discovery;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.PunktregDS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.BindingGroup;

/**
 * Handlingsknapper for oversiktsbilde for funn.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JDiscoveryActions extends JPanel {
	private static final String ENABLED = "enabled";
	private static final Logger logger = LoggerFactory.getLogger(JDiscoveryActions.class);
	private MipssFieldModule parentPlugin;
	private BindingGroup group;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	@SuppressWarnings("unchecked")
	public JDiscoveryActions(MipssFieldModule parentPlugin) {
		logger.debug("JDiscoveryActions() start");
		this.parentPlugin = parentPlugin;
		List<JMenuItem> items = new ArrayList<JMenuItem>();

//		JButton newButton = new JButton(new NewPunktreg(GUITexts.getText(GUITexts.NEW), null));
//		JButton openButton = new JButton(new OpenAction(GUITexts.getText(GUITexts.DETAILS), null));
//		JButton mapButton = new JButton(new MapAction(GUITexts.getText(GUITexts.MAP), IconResources.NORGE_ICON));
//		mapButton.setEnabled(false);
//		
//		JMenuItem reportsButton = new JMenuItem(new PDFAction(GUITexts.getText(GUITexts.DISCOVERY_DETAIL_REPORT),IconResources.ADOBE_16));
//		JMenuItem overviewButton = new JMenuItem(new OverviewPDFAction(GUITexts.getText(GUITexts.DISCOVERY_OVERVIEW_REPORT), IconResources.ADOBE_16));
//		JButton excelButton = new JButton(new ExcelAction(GUITexts.getText(GUITexts.EXCEL),IconResources.EXCEL_ICON16));
//		JButton slettButton = new JButton(new DeleteAction(GUITexts.getText(GUITexts.DELETE), IconResources.TRASH_ICON));
//		JButton gjenopprettButton = new JButton(new RestoreAction(GUITexts.getText(GUITexts.RESTORE),IconResources.UNTRASH_ICON));
//
//		items.add(reportsButton);
//		items.add(overviewButton);
//		JPopupListMenu reportsPopup = new JPopupListMenu(GUITexts.getText(GUITexts.REPORTS), IconResources.PRINT_ICON,
//				items);
//
//		group = new BindingGroup();
//		Binding detailsBinding = BindingHelper.createbinding(parentPlugin.getDiscoveryTable(), "${noOfSelectedRows == 1}", openButton, ENABLED);
//		Binding mapBinding = BindingHelper.createbinding(parentPlugin.getDiscoveryTable(),"${noOfSelectedRows > 0}", mapButton, ENABLED);
//		
//		
//		Binding printBinding = BindingHelper.createbinding(parentPlugin.getDiscoveryTable(), "${noOfSelectedRows > 0}",
//				reportsButton, ENABLED);
//		Binding overviewBinding = BindingHelper.createbinding(parentPlugin.getDicoveryTableModel(), "${size > 0}",
//				overviewButton, ENABLED);
//		Binding excelBinding = BindingHelper.createbinding(parentPlugin.getDicoveryTableModel(), "${size > 0}",
//				excelButton, ENABLED);
//		Binding popupBinding = BindingHelper.createbinding(parentPlugin.getDicoveryTableModel(), "${size > 0}",
//				reportsPopup, ENABLED);
//		Binding slettBinding = BindingHelper.createbinding(parentPlugin.getDiscoveryFilter(), "${kunSlettede != true}",
//				slettButton, "visible");
//		Binding slettEnable = BindingHelper.createbinding(parentPlugin.getDiscoveryTable(), "${noOfSelectedRows > 0}",
//				slettButton, ENABLED);
//		Binding gjenopprettBinding = BindingHelper.createbinding(parentPlugin.getDiscoveryFilter(),
//				"${kunSlettede == true}", gjenopprettButton, "visible");
//		Binding gjenopprettEnable = BindingHelper.createbinding(parentPlugin.getDiscoveryTable(),
//				"${noOfSelectedRows > 0}", gjenopprettButton, ENABLED);
//		Binding nyBinding = BindingHelper.createbinding(parentPlugin.getCommonFilterPanel(),
//				"${valgtKontrakt != null}", newButton, ENABLED);
//
//		group.addBinding(detailsBinding);
//		group.addBinding(mapBinding);
//		group.addBinding(printBinding);
//		group.addBinding(overviewBinding);
//		group.addBinding(excelBinding);
//		group.addBinding(popupBinding);
//		group.addBinding(slettBinding);
//		group.addBinding(slettEnable);
//		group.addBinding(gjenopprettBinding);
//		group.addBinding(gjenopprettEnable);
//		group.addBinding(nyBinding);
//		logger.debug("JDiscoveryActions() bind start");
//		group.bind();
//		logger.debug("JDiscoveryActions() bind end");
//
//		BoxLayout layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
//		setLayout(layout);
//		if (parentPlugin.hasWriteAccess()) {
//			add(slettButton);
//			add(gjenopprettButton);
//			add(BoxUtil.createHorizontalStrut(10));
//			add(newButton);
//		}
//		add(Box.createHorizontalGlue());
//		add(excelButton);
//		add(BoxUtil.createHorizontalStrut(10));
//		add(reportsPopup);
//		add(BoxUtil.createHorizontalStrut(10));
//		add(mapButton);
//		add(BoxUtil.createHorizontalStrut(10));
//		add(openButton);
//		logger.debug("JDiscoveryActions() end");
//
//		Dimension min = new Dimension(200, 24);
//		Dimension pref = new Dimension(200, 24);
//		Dimension max = new Dimension(Short.MAX_VALUE, 24);
//		ComponentSizeResources.setComponentSizes(this, min, pref, max);
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		group.unbind();
		group = null;
		parentPlugin = null;
	}

	private List<String> getIds(List<PunktregSok> selection) {
		List<String> list = new ArrayList<String>();

		for (PunktregSok p : selection) {
			list.add(p.getPunktregGuid());
		}

		return list;
	}

	class DeleteAction extends AbstractAction {

		public DeleteAction(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parentPlugin.getDiscoveryTable().getSelectedEntities();

			MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
					IconResources.QUESTION_ICON, GUITexts.getText(GUITexts.DELETE_DISCOVERY_TITLE), GUITexts.getText(
							GUITexts.DELETE_DISCOVERY_TXT, selection.size()), MipssDialogue.Type.QUESTION,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();

			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL
					|| dialogue.getPressedButton() == MipssDialogue.Button.NOTHING) {
				return;
			}

			parentPlugin.deleteDiscoveries(selection);
		}
	}

	class ExcelAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public ExcelAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			JMipssBeanTable<PunktregSok> table = parentPlugin.getDiscoveryTable();
			
////			ExcelPOIHelper excel = new ExcelPOIHelper();
//			excel.setDateFormat(MipssDateFormatter.DATE_FORMAT);
//			
//			try {
//				File tempFile = File.createTempFile("tabellExport", ".xls");
//				HSSFWorkbook workbook = excel.createWorkbook(tempFile, excel.getExcelTemplate("funn_excel.xls"));
//				HSSFSheet sheet = excel.getSheet(0, workbook);
//				List<PunktregSok> entities = table.getSelectedEntities();
//				if (entities.size()==0){
//					entities = ((MipssBeanTableModel)table.getModel()).getEntities();
//				}
//				int rowCount = 1;
//				for (PunktregSok sok:entities){
//					HSSFRow row = excel.createRow(sheet, rowCount);
//					
//					int c = 0;
//					excel.writeData(row, c++, sok.getRefnummer());
//					excel.setDateFormat(MipssDateFormatter.DATE_TIME_FORMAT);
//					excel.writeData(row, c++, sok.getSisteStatusDato());
//					excel.writeData(row, c++, sok.getSisteStatus());
//					excel.setDateFormat(MipssDateFormatter.DATE_FORMAT);
//					excel.writeData(row, c++, sok.getTiltaksDato());
//					if (sok.getTiltaksDato()!=null && sok.getTiltaksDato().before(sok.getSisteStatusDato()))
//						excel.writeData(row, c++, "X");
//					else{
//						c++;
//					}
//					excel.writeData(row, c++, sok.getProsessTxt(), true);
//					excel.writeData(row, c++, sok.getTilstandtypeNavn());
//					excel.writeData(row, c++, sok.getAarsakNavn());
//					excel.writeData(row, c++, sok.getHarHendelse());
//					excel.writeData(row, c++, sok.getHarInspeksjon());
//					excel.writeData(row, c++, sok.getEtterslep());
//					excel.writeData(row, c++, sok.getAntallBilder());
//					excel.writeData(row, c++, sok.getFylkesnummer());
//					excel.writeData(row, c++, sok.getKommunenummer());
//					excel.writeData(row, c++, sok.getVei());
//					excel.writeData(row, c++, sok.getVeinummer());
//					excel.writeData(row, c++, sok.getHp());
//					excel.writeData(row, c++, sok.getMeter());
//					excel.setDateFormat(MipssDateFormatter.DATE_TIME_FORMAT);
//					excel.writeData(row, c++, sok.getOpprettetDato());
//					excel.writeData(row, c++, sok.getOpprettetAv());
//					excel.writeData(row, c++, sok.getPdaDfuNavn(), true);
//					excel.writeData(row, c++, sok.getBeskrivelse(), true);
//					excel.writeData(row, c++, sok.getHistorikk(), true);
//					excel.writeData(row, c, sok.getEstimat());
//					
//					rowCount++;
//					excel.setDefaultBorders(row, (short)c);
//				}
//				
//				excel.closeWorkbook(workbook);
//				DesktopHelper.openFile(excel.getExcelFile());
//			} catch (Throwable t) {
//				parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
//			} 
		}
	}

	
	/**
	 * Action klasse for å åpne funn
	 */
	class MapAction extends AbstractAction {
		public MapAction(String name, Icon icon) {
			super(name, icon);
		}
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parentPlugin.getDiscoveryTable().getSelectedEntities();
			if (selection != null) {
				parentPlugin.showDiscoveriesInMap(selection);
			}
		}
	}

	class OpenAction extends AbstractAction {
		public OpenAction(String name, Icon icon) {
			super(name, icon);
		}
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parentPlugin
				.getDiscoveryTable()
				.getSelectedEntities();

			if (selection != null) {
				parentPlugin.openDiscoveries(selection);
			}
		}
	}

	/**
	 * Action klasse for å åpne funn
	 */
	class OverviewPDFAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public OverviewPDFAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parentPlugin.getDiscoveryTable().getSelectedEntities();
			List<PunktregSok> discoveries = null;
			if (selection != null && !selection.isEmpty()) {
				discoveries = selection;
			} else {
				MipssBeanTableModel<PunktregSok> model = parentPlugin.getDicoveryTableModel();
				discoveries = model.getEntities();
			}

			if (discoveries != null && !discoveries.isEmpty()) {
				try {
					parentPlugin.viewDiscoveryOverviewPDF(discoveries);
				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}

	/**
	 * Action klasse for å åpne funn
	 */
	class PDFAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public PDFAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parentPlugin.getDiscoveryTable().getSelectedEntities();
			if (selection != null) {
				try {
					if (selection.size() > 4) {
						MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
								IconResources.WARNING_ICON, GUITexts.getText(GUITexts.WARNING), GUITexts.getText(
										GUITexts.OPEN_MANY_REPORT_DISCOVERIES, selection.size()),
								MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK,
										MipssDialogue.Button.CANCEL });
						dialogue.askUser();
						if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
							return;
						}
					}

					List<String> ids = getIds(selection);

					PunktregDS dataSource = new PunktregDS(ids);
//					dataSource.setFieldBean(parentPlugin.getMipssField());
					JReportBuilder builder = new JReportBuilder(Report.DISCOVERY_DETAIL_PRINT,
							Report.DISCOVERY_DETAIL_PRINT.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF,
							parentPlugin.getLoader().getApplicationFrame());
					File report = builder.getGeneratedReportFile();

					if (report != null) {
						DesktopHelper.openFile(report);
					}
				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, parentPlugin,
							GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}

	class RestoreAction extends AbstractAction {

		public RestoreAction(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parentPlugin.getDiscoveryTable().getSelectedEntities();

			MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
					IconResources.INFO_ICON, GUITexts.getText(GUITexts.RESTORE_DISCOVERY_TITLE), GUITexts.getText(
							GUITexts.RESTORE_DISCOVERY_TXT, selection.size()), MipssDialogue.Type.INFO,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();

			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL
					|| dialogue.getPressedButton() == MipssDialogue.Button.NOTHING) {
				return;
			}

			parentPlugin.restoreDiscoveries(selection);
		}
	}

	class NewPunktreg extends AbstractAction {

		public NewPunktreg(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			parentPlugin.enforceWriteAccess();

			// Kanskje lage egen skjerm for nytt funn?
			parentPlugin.openDiscoveries(Collections.singletonList((PunktregSok) null));
		}
	}
}
