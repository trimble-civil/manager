package no.mesta.mipss.mipssfield.incident;

import java.awt.Dimension;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Hendelseaarsak;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JDateTimePicker;

/**
 * Panel for å vise vurdering av et funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JAssessmentPanel extends JAbstractInfoPanel{
	private static final String TYPE = "type";
	private static final Integer STRUT = 2;
	public static final Dimension MIN_SIZE = new Dimension(100, 189);
	public static final Dimension PREF_SIZE = new Dimension(450, 189);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 189);
	private static final Dimension FIELD_MIN_SIZE = new Dimension(220, 22);
	private static final Dimension FIELD_PREF_SIZE = new Dimension(384, 22);
	private static final Dimension FIELD_MAX_SIZE = new Dimension(384, 22);
	
	public JAssessmentPanel(final JIncidentDetailPanel owner, final Hendelse hendelse) {
        setFieldToolkit(owner.getFieldToolkit());
        
		JComboBox causeCombo = getFieldToolkit().createDropDown(Hendelse.class, hendelse, "aarsak", null, Hendelseaarsak.class, owner.getParentPlugin().getHendelsesaarsaker());
		ComponentSizeResources.setComponentSizes(causeCombo, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);
		JDateTimePicker mottatDatoField = getFieldToolkit().createDateTimePicker(Hendelse.class, hendelse, "henvendelseDato", MipssDateFormatter.SHORT_DATE_FORMAT);
		if(owner.isNewHendelse()) {
			mottatDatoField.setDate(null);
		}

		JTextField emneField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse, "skjemaEmne", null);
		ComponentSizeResources.setComponentSizes(emneField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);
		
		JTextField meldingFraField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse, "henvendelseFra", null);
		ComponentSizeResources
				.setComponentSizes(meldingFraField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JTextField mottatAvField = getFieldToolkit().createTextInputField(Hendelse.class, hendelse, "henvendelseMottattAv", null);
		ComponentSizeResources
				.setComponentSizes(mottatAvField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);
        
        
		JPanel emnePanel = new JPanel();
		emnePanel.setLayout(new BoxLayout(emnePanel, BoxLayout.PAGE_AXIS));
		emnePanel.add(BoxUtil.createVerticalStrut(5));
		emnePanel.add(BoxUtil.createVerticalStrut(5));
		emnePanel.setBorder(BorderResources.createComponentBorder());
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        
        mainPanel.add(BoxUtil.createVerticalStrut(STRUT));
        
        mainPanel.add(BoxUtil.createVerticalStrut(STRUT));
        mainPanel.add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Hendelseaarsak>(GUITexts.getText(GUITexts.CAUSE), causeCombo, null), Box
				.createHorizontalGlue()));
        mainPanel.add(BoxUtil.createVerticalStrut(STRUT));
        mainPanel.add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.MESSAGE_FROM), meldingFraField, null), Box
				.createHorizontalGlue()));
        mainPanel.add(BoxUtil.createVerticalStrut(STRUT));
        mainPanel.add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.RECEIVED_BY), mottatAvField, null), Box
				.createHorizontalGlue()));
        mainPanel.add(BoxUtil.createVerticalStrut(STRUT));
        mainPanel.add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Date>(GUITexts.getText(GUITexts.RECEIVED_DATE), mottatDatoField, null), Box
				.createHorizontalGlue()));
        mainPanel.add(BoxUtil.createVerticalStrut(STRUT));

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(BoxUtil.createVerticalBox(STRUT, emnePanel, mainPanel));
        
        setMinimumSize(MIN_SIZE);
        setPreferredSize(PREF_SIZE);
        setMaximumSize(MAX_SIZE);
        mainPanel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.ASSESSMENT)));
	}
}
