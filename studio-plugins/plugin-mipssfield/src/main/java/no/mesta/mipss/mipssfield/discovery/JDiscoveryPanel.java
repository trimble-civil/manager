package no.mesta.mipss.mipssfield.discovery;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * GUI for funn skjermbildet
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDiscoveryPanel extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(JDiscoveryPanel.class);
	private JPopupMenu menu;
	private MipssFieldModule parentPlugin;
	private List<PunktregSok> selection;
	private JMipssBeanTable<PunktregSok> table;
	private JDiscoveryFilter filter;
	private JDiscoveryActions actions;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	public JDiscoveryPanel(MipssFieldModule parentPlugin) {
		logger.debug("JDiscoveryPanel() start");
		this.parentPlugin = parentPlugin;

		initGui();
		logger.debug("JDiscoveryPanel() end");
	}

	/**
	 * Logikk for høyreklikk valg i tabellen Sørger for at valgte rader i
	 * tabellen oppfører seg logisk:
	 * 
	 * <ol>
	 * <li>Hvis ingen rader er valg, velg raden under musen i det høyremusknapp
	 * trykkes</li>
	 * <li>Hvis en eller flere rader er valgt, og raden under musen er med i
	 * utvalget: Da skal utvalget beholdes</li>
	 * <li>Hvis en eller flere rader er valgt, og raden under musen ikke er med
	 * i utvalget: Velg da kun raden under musen</li>
	 * </ol>
	 * 
	 * Metoden lagrer utvalget i selection variabelen.
	 */
	private void checkTableSelection(MouseEvent e) {
		int[] rows = table.getSelectedRows();

		// Sjekk om noe er valgt i tabellen
		if (rows.length == 0) {
			// Hvis ikke -> velg rad under muspeker
			selectRow(e);
			selection = parentPlugin.getDiscoveryTable().getSelectedEntities();
		} else {
			// Hvis --> sjekk at raden under musa er med i utvalget
			selection = parentPlugin.getDiscoveryTable().getSelectedEntities();
			int row = table.rowAtPoint(e.getPoint());
			if (row != -1) {
				PunktregSok underMouse = (PunktregSok) parentPlugin.getDicoveryTableModel().get(row);
				if (!selection.contains(underMouse)) {
					// Hvis ikke -> velg rad under muspeker
					selectRow(e);
					selection = parentPlugin.getDiscoveryTable().getSelectedEntities();
				}
			}
		}
	}

	/**
	 * Setter opp gui for funn
	 * 
	 */
	private void initGui() {
		logger.debug("initGui() start");
		table = parentPlugin.getDiscoveryTable();
		table.setFillsViewportHeight(true);
		table.addMouseListener(new MouseAdapter() {
			/**
			 * Lager et menu item for menyen
			 * 
			 */
			private JMenuItem createMenuItem(Action a) {
				JMenuItem item = new JMenuItem(a);
				item.addMouseListener(new ItemHighlighter(item));
				return item;
			}

			/**
			 * Viser høyre menyen på oppgitt posisjon
			 * 
			 * @param pos
			 */
			private void initAndShow(Point pos) {
				if (menu == null) {
					menu = new JPopupMenu();
					menu.add(createMenuItem(new OpenAction(GUITexts.getText(GUITexts.OPEN), null)));
					menu.add(createMenuItem(new MapAction("Vis i kart", null)));
					menu.add(createMenuItem(new PDFAction("Utskrift", null)));

					menu.addMouseListener(new MouseAdapter() {
						/** {@inheritDoc} */
						public void mouseExited(MouseEvent e) {
							menu.setVisible(false);
						}
					});
				}

				pos.translate(-15, -15);
				menu.setLocation(pos);
				menu.setVisible(true);
			}

			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
					if (e.getClickCount() == 2) {
						checkTableSelection(e);
						openDiscoveries();
					}
				}
			}

			/** {@inheritDoc} */
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
					checkTableSelection(e);

					// Vis høyreknapp menyen
					initAndShow(e.getLocationOnScreen());
				}
			}
		});

		filter = new JDiscoveryFilter(parentPlugin);
		JMipssBeanScrollPane scroll = new JMipssBeanScrollPane(table, parentPlugin.getDicoveryTableModel());
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		actions = new JDiscoveryActions(parentPlugin);
		setLayout(new GridBagLayout());
		add(filter, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		add(scroll, new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(actions, new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		logger.debug("initGui() end");
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		filter.dispose();
		actions.dispose();
		filter = null;
		actions = null;
		parentPlugin = null;
	}
	/**
	 * Åpner valgte funn
	 * 
	 */
	private void openDiscoveries() {
		// Hent ut de valgte radene til selection
		selection = parentPlugin.getDiscoveryTable().getSelectedEntities();

		if (selection != null) {
			parentPlugin.openDiscoveries(selection);
		}
	}

	/**
	 * Setter valgt rad i tabellen til den som er under musepekeren
	 * 
	 */
	private void selectRow(MouseEvent e) {
		// Sørg for at raden der musa er klikkes på med venstre...
		int index = table.rowAtPoint(e.getPoint());
		if (index != -1) {
			table.setRowSelectionInterval(index, index);
		}
	}

	/**
	 * Sørger for at JMenuItem reagerer på mouseEntered og mouseExited
	 */
	class ItemHighlighter implements MouseListener {
		private JMenuItem item;

		/**
		 * Konstruktør
		 * 
		 */
		public ItemHighlighter(JMenuItem item) {
			this.item = item;
		}

		/** {@inheritDoc} */
		public void mouseClicked(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.background"));
			item.setForeground(UIManager.getColor("MenuItem.foreground"));
		}

		/** {@inheritDoc} */
		public void mouseEntered(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.selectionBackground"));
			item.setForeground(UIManager.getColor("MenuItem.selectionForeground"));
		}

		/** {@inheritDoc} */
		public void mouseExited(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.background"));
			item.setForeground(UIManager.getColor("MenuItem.foreground"));
		}

		/** {@inheritDoc} */
		public void mousePressed(MouseEvent e) {
			// Brukes ikke
		}

		/** {@inheritDoc} */
		public void mouseReleased(MouseEvent e) {
			item.setBackground(UIManager.getColor("MenuItem.background"));
			item.setForeground(UIManager.getColor("MenuItem.foreground"));
		}
	}

	/**
	 * Klasse for å vise et funn i kart
	 */
	class MapAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param text
		 * @param icon
		 */
		public MapAction(String text, Icon icon) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			menu.setVisible(false);
			// Hent ut de valgte radene til selection
			selection = parentPlugin.getDiscoveryTable().getSelectedEntities();

			if (selection != null) {
				parentPlugin.showDiscoveriesInMap(selection);
			}
		}
	}

	/**
	 * Klasse for å åpne et funn
	 */
	class OpenAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param text
		 * @param icon
		 */
		public OpenAction(String text, Icon icon) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			menu.setVisible(false);
			openDiscoveries();
		}
	}

	/**
	 * Klasse for å slette et funn
	 * 
	 */
	class PDFAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param text
		 * @param icon
		 */
		public PDFAction(String text, Icon icon) {
			super(text, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			menu.setVisible(false);

			// Hent ut de valgte radene til selection
			selection = parentPlugin.getDiscoveryTable().getSelectedEntities();

			if (selection != null) {
				File report = parentPlugin.makeDiscoveriesPDF(selection, new HashMap<String, Object>());
				DesktopHelper.openFile(report);
			}
		}
	}
}
