package no.mesta.mipss.mipssfield.discovery;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Avgjør om lagreknappen skal være på eller av.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class CloseStatusObserver {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(CloseStatusObserver.class);
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private AvvikStatus status;
	private boolean funnChanged;
	
	public void setStatus(AvvikStatus status) {
		boolean oldEnabled = isEnabled();
		AvvikStatus old = this.status;
		this.status = status;
		props.firePropertyChange("status", old, status);
		props.firePropertyChange("enabled", oldEnabled, isEnabled());
	}
	
	public AvvikStatus getStatus() {
		return status;
	}
	
	public void setChanged(boolean f) {
		boolean old = this.funnChanged;
		boolean oldEnabled = isEnabled();
		funnChanged = f;
		props.firePropertyChange("changed", old, f);
		props.firePropertyChange("enabled", oldEnabled, isEnabled());
	}
	
	public boolean isChanged() {
		return funnChanged;
	}
	
	public boolean isEnabled() {
		if(status != null) {
			if(!StringUtils.equalsIgnoreCase(status.getAvvikstatusType().getStatus(), AvvikstatusType.LUKKET_STATUS)) {
				return true;
			}
		}

		return false;
	}

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
