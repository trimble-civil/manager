package no.mesta.mipss.mipssfield;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import no.mesta.mipss.mipssfield.discovery.JDiscoveryDetailPanel;
import no.mesta.mipss.mipssfield.incident.JIncidentDetailPanel;
import no.mesta.mipss.mipssfield.inspection.JInspectionDetailPanel;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Detaljert vindu for funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDetailFrame extends JFrame {
	private static final Logger logger = LoggerFactory.getLogger(JDetailFrame.class);
	protected static Dimension maxSize = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	protected static Dimension minSize = new Dimension(600, 550);
	protected static Dimension prefSize = new Dimension(900, 768);
	private final MipssFieldModule parentPlugin;
	private final JDetailTabs tabs;

	protected JDetailFrame(MipssFieldModule parentPlugin) {
		this.parentPlugin = parentPlugin;
		setLayout(new BorderLayout());
		setIconImage(IconResources.MIPSS_FIELD_ICON.getImage());
		tabs = new JDetailTabs(this);
		add(tabs, BorderLayout.CENTER);
		ComponentSizeResources.setComponentSizes(this, minSize, prefSize, maxSize);
		setDefaultCloseOperation(JDetailFrame.DO_NOTHING_ON_CLOSE);

		addWindowListener(new WindowAdapter() {
			/** {@inheritDoc} */
			@Override
			public void windowClosing(WindowEvent e) {
				boolean close = true;
				for (JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> panel : tabs
						.getPanels()) {
					close = close && panel.closeDetailPanel();

					if (!close) {
						break;
					}
				}
				if (close) {
					dispose();
				}
			}
		});
	}

	public void addDetail(HendelseSokView hendelseSok) {
		addDetail(hendelseSok, null);
	}

	public void addDetail(HendelseSokView hendelseSok, AbstractAction saveAction) {
		JIncidentDetailPanel panel = new JIncidentDetailPanel(parentPlugin, this, hendelseSok, saveAction);
		openPanel(panel);
	}

	public void addDetail(InspeksjonSokView inspeksjonSok) {
		JInspectionDetailPanel panel = new JInspectionDetailPanel(parentPlugin, this, inspeksjonSok);
		openPanel(panel);
	}

	public void addDetail(MipssFieldDetailItem item) {
		addDetail(item, null);
	}

	public void addDetail(MipssFieldDetailItem item, AbstractAction saveAction) {
		if (item instanceof PunktregSok) {
			addDetail((PunktregSok) item, saveAction);
		} else if (item instanceof InspeksjonSokView) {
			addDetail((InspeksjonSokView) item);
		} else if (item instanceof HendelseSokView) {
			addDetail((HendelseSokView) item, saveAction);
		} else {
			throw new IllegalArgumentException("Unknown detail object" + item);
		}
	}

	public void addDetail(PunktregSok funnSok) {
		addDetail(funnSok, null);
	}

	public void addDetail(PunktregSok funnSok, AbstractAction saveAction) {
		JDiscoveryDetailPanel panel = new JDiscoveryDetailPanel(parentPlugin, this, funnSok, saveAction);
		openPanel(panel);
	}

	/**
	 * Lukker fanen for panelet, hvis det var siste fane åpent lukkes også
	 * vinduet
	 * 
	 * @param panel
	 */
	public void closePanel(
			JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> panel) {
		logger.info("closePanel(" + panel.getViewObject() + ")");
		if (!panel.closeDetailPanel()) {
			return;
		}

		panel.dispose();
		tabs.remove(panel);
		int tabCount = tabs.getTabCount();
		if (tabCount == 0) {
			tabs.dispose();
			dispose();
		}
	}
	
	public void closeWindow() {
		WindowEvent event = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
		processEvent(event);

	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		logger.debug("dispose() start");
		tabs.dispose();
		super.dispose();
		logger.debug("dispose() end");
	}

	@SuppressWarnings("unchecked")
	public<T extends JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> T getDetailPanel(Object detailObject) {
		T tab = (T) tabs.getTab(detailObject); 
		return tab;
	}

	@SuppressWarnings("unchecked")
	public<T extends JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> T getDetailPanelBySaveAction(AbstractAction saveAction) {
		T tab = (T) tabs.getTabBySaveAction(saveAction);
		return tab;
	}

	/**
	 * Plugin instans som eier vinduet
	 * 
	 * @return
	 */
	public MipssFieldModule getParentPlugin() {
		return parentPlugin;
	}

	public JDetailTabs getTabs() {
		return tabs;
	}
	
	/**
	 * Åpner detalj fanen for gitt objekt
	 * 
	 * @param detailObject
	 * @return negativt hvis ingen åpen fane for objektet finnes
	 */
	public boolean navigateToDetailTab(Object detailObject) {
		boolean b = tabs.navigateToDetailTab(detailObject);
		pack();
		return b;
	}

	/**
	 * Åpner en fane for det gitte panelet
	 * 
	 * @param panel
	 */
	public void openPanel(JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> panel) {
		logger.info("openPanel(" + panel.getViewObject() + ")");
		tabs.addTab(panel.getTabTitle(), panel);
		setTitle(panel.getWindowTitle());
		pack();
		prefSize = getSize();
		setLocationRelativeTo(parentPlugin.getLoader().getApplicationFrame());
	}
}
