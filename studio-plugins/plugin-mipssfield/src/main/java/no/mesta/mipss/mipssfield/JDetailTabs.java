package no.mesta.mipss.mipssfield;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.core.IDisposable;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel for faner.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDetailTabs extends JTabbedPane implements IDisposable {
	private static final Logger logger = LoggerFactory.getLogger(JDetailTabs.class);
	private JDetailFrame owner;

	/**
	 * Konstruktør
	 * 
	 * @param owner
	 *            vinduet panelet med faner ligger i
	 */
	public JDetailTabs(JDetailFrame owner) {
		super();
		this.owner = owner;
	}

	public void addTab(String title,
			JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> component) {
		insertTab(title, component);
	}

	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose() {
		logger.debug("dispose() start");
		int tabCount = getTabCount();
		for (ChangeListener cl : getChangeListeners()) {
			removeChangeListener(cl);
		}

		if (tabCount == 0) {
			return;
		}

		for (int i = 0; i < tabCount; i++) {
			JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> tab = getDetailPanel(i);
			tab.dispose();
		}
		removeAll();

		logger.debug("dispose() end");
	}

	@SuppressWarnings("unchecked")
	public JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> getDetailPanel(
			int index) {
		return (JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>) getComponentAt(index);
	}

	public List<JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> getPanels() {
		List<JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> panels = new ArrayList<JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>>();

		int tabs = getTabCount();
		if (tabs > 0) {
			for (int i = 0; i < tabs; i++) {
				JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> tab = getDetailPanel(i);
				panels.add(tab);
			}
		}

		return panels;
	}

	@SuppressWarnings("unchecked")
	public<T extends JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> T getTab(Object detailObject) {
		int tabs = getTabCount();
		if (tabs == 0) {
			return null;
		}
		
		for (int i = 0; i < tabs; i++) {
			T tab = (T) getDetailPanel(i);
			if (tab.getDetailObject().equals(detailObject)) {
				return tab;
			}
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public<T extends JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> T getTabBySaveAction(AbstractAction saveAction) {
		int tabs = getTabCount();
		if (tabs == 0) {
			return null;
		}
		
		for (int i = 0; i < tabs; i++) {
			T tab = (T) getDetailPanel(i);
			if (tab.getSaveAction().equals(saveAction)) {
				return tab;
			}
		}
		
		return null;
	}

	/**
	 * Legger inn panelet som fane
	 * 
	 * @param title
	 * @param newTab
	 */
	private void insertTab(String title,
			JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> newTab) {
		logger.debug("insertTab(" + title + "," + newTab + ")");
		int tabs = getTabCount();
		boolean added = false;
		int index = 0;
		if (tabs > 0) {
			for (int i = 0; i < tabs; i++) {
				JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> tab = getDetailPanel(i);
				int compare = tab.compareTo(newTab);
				logger.debug("insertTab at index " + i + " gives: " + tab + " and compares: " + compare);
				if (compare >= 0) {
					logger.debug("insertTab adding tab at index " + i);
					insertTab(title, null, newTab, null, i);
					added = true;
					index = i;
					break;
				}
			}

			if (!added) {
				super.addTab(title, newTab);
				index = tabs;
			}
		} else {
			super.addTab(title, newTab);
		}

		setSelectedComponent(newTab);
		Component tabComponent = newTab.getTabComponent(owner, this);
		setTabComponentAt(index, tabComponent);
		setBackgroundAt(index, tabComponent.getBackground());
	}

	/**
	 * Åpner fanen som holder detaljene for objektet. Evt returnerer false hvis
	 * ikke noe slikt objekt finnes
	 * 
	 * @param detailObject
	 * @return
	 */
	public boolean navigateToDetailTab(Object detailObject) {
		int tabs = getTabCount();
		if (tabs == 0) {
			return false;
		}

		for (int i = 0; i < tabs; i++) {
			JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> tab = getDetailPanel(i);
			if (tab.getDetailObject().equals(detailObject)) {
				setSelectedComponent(tab);
				return true;
			}
		}

		return false;
	}
}
