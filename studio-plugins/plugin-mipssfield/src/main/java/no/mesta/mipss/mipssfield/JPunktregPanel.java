package no.mesta.mipss.mipssfield;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mipssfield.incident.JIncidentDetailPanel;
import no.mesta.mipss.mipssfield.inspection.JInspectionDetailPanel;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.persistence.mipssfield.Avviktilstand;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssEntityRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.decorator.SortOrder;

public class JPunktregPanel extends JPanel {
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	public static final Dimension MIN_SIZE = new Dimension(600, 100);
	public static final Dimension PREF_SIZE = new Dimension(900, 200);
	private static final int STRUT = 2;
	private final JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> parent;
	private final Object source;
	private JMipssBeanTable<Avvik> table;
	private MipssBeanTableModel<Avvik> tableModel;

	public JPunktregPanel(final JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> parent, final Object source) {
		this.parent = parent;
		this.source = source;

	

		initTable();

		JScrollPane tableScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setViewportView(table);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, tableScrollPane));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	public JPunktregPanel(final JIncidentDetailPanel parent, final Hendelse hendelse) {
		this(parent, (Object) hendelse);
	}

	public JPunktregPanel(final JInspectionDetailPanel parent, final Inspeksjon inspeksjon) {
		this(parent, (Object) inspeksjon);
	}

	public JMipssBeanTable<Avvik> getTable() {
		return table;
	}
	
	public MipssBeanTableModel<Avvik> getTableModel() {
		return tableModel;
	}
	
	private void initTable() {
		tableModel = new MipssBeanTableModel<Avvik>("punktregistreringer",
				Avvik.class, "refnummer", "sisteStatusDato", "sisteStatusType", "tiltaksDato", "prosess", "tilstand",
				"hendelsestype", "veiref");
		tableModel.setSourceObject(source);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Avvik.class, tableModel.getColumns());
		MipssEntityRenderer entityRendrer = new MipssEntityRenderer();
		table = new JMipssBeanTable<Avvik>(tableModel, columnModel);
		table.setDefaultRenderer(Punktveiref.class, entityRendrer);
		table.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
		table.setDefaultRenderer(Prosess.class, entityRendrer);
		table.setDefaultRenderer(Avviktilstand.class, entityRendrer);
		table.setDefaultRenderer(AvvikstatusType.class, entityRendrer);
		table.setFillsViewportHeight(true);
		table.setFillsViewportWidth(true);
		table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		table.setSortable(true);
		table.setSortOrder(0, SortOrder.ASCENDING);
		table.setHorizontalScrollEnabled(true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
					table.selectMouseSelection(e);
					Avvik punkt = table.getSelectedEntities().get(0);
					if (!parent.getOwner().navigateToDetailTab(punkt)) {
						parent.getOwner().addDetail(PunktregSok.newInstance(punkt));
					}
				}
			}
		});
		table.setEnabled(false);
		parent.getFieldToolkit().controlEnabledField(table);
	}
}
