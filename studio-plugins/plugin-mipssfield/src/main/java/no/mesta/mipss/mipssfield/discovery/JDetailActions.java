package no.mesta.mipss.mipssfield.discovery;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.ISaveStateObserver;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.PunktregDS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Panel med handlingsknapper for detaljbilde for funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDetailActions extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(JDetailActions.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 24);
	public static final Dimension MIN_SIZE = new Dimension(600, 24);
	public static final Dimension PREF_SIZE = new Dimension(900, 24);
	private static final int STRUT = 2;
	private final JButton closeWindow;
	private final JDiscoveryDetailPanel parent;
	private JButton printButton;
	private JButton saveButton;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	public JDetailActions(final JDiscoveryDetailPanel parent) {
		logger.trace("JDetailActions() created");
		this.parent = parent;

		printButton = new JButton(new PrintAction(GUITexts.getText(GUITexts.DETAIL_REPORT), IconResources.PRINT_ICON));
		saveButton = new JButton(parent.getSaveAction());
		closeWindow = new JButton(new CloseWindowAction(GUITexts.getText(GUITexts.CLOSE_WINDOW),
				IconResources.CANCEL_ICON));

		printButton.setEnabled(false);
		saveButton.setEnabled(false);
		closeWindow.setEnabled(false);

		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		if (parent.getParentPlugin().hasWriteAccess()) {
			add(saveButton);
		}
		add(Box.createHorizontalGlue());
		add(printButton);
		add(BoxUtil.createHorizontalStrut(STRUT));
		add(closeWindow);

		ComponentSizeResources.setComponentSizes(this, MIN_SIZE, PREF_SIZE, MAX_SIZE);
	}

	@SuppressWarnings("unchecked")
	public void ready() {
		ISaveStateObserver observer = parent.getSaveStateObserver();

		Binding observeChange = BindingHelper.createbinding(parent.getFieldToolkit(), "fieldsChanged", observer,
				"changed");
		Binding enableSave = BindingHelper.createbinding(observer, "enabled", saveButton, "enabled");
		parent.getFieldToolkit().addBinding(observeChange);
		parent.getFieldToolkit().addBinding(enableSave);

		printButton.setEnabled(true);
		saveButton.setEnabled(false);
		closeWindow.setEnabled(true);
	}

	/**
	 * Lukker vinduet
	 */
	class CloseWindowAction extends AbstractAction {

		public CloseWindowAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			parent.getOwner().closeWindow();
		}
	}

	/**
	 * Action klasse for å åpne funn
	 */
	class MapAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public MapAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<PunktregSok> selection = parent.getParentPlugin().getDiscoveryTable().getSelectedEntities();
			if (selection != null) {
				parent.getParentPlugin().showDiscoveriesInMap(selection);
			}
		}
	}

	/**
	 * Action klasse for å exportere funn til PDF
	 */
	class PrintAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public PrintAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			try {
				boolean cancel = parent.saveOrCancel();
				if (cancel) {
					return;
				}

				List<String> ids = Collections.singletonList(parent.getDetailObject().getGuid());

				PunktregDS dataSource = new PunktregDS(ids);
//				dataSource.setFieldBean(parent.getParentPlugin().getMipssField());
				JReportBuilder builder = new JReportBuilder(Report.DISCOVERY_DETAIL_PRINT,
						Report.DISCOVERY_DETAIL_PRINT.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF,
						parent.getOwner());
				File report = builder.getGeneratedReportFile();

				if (report != null) {
					DesktopHelper.openFile(report);
				}
			} catch (Exception ex) {
				parent.getParentPlugin().getLoader().handleException(ex, parent.getParentPlugin(),
						GUITexts.getText(GUITexts.PRINT_FAILED));
			}
		}
	}
}
