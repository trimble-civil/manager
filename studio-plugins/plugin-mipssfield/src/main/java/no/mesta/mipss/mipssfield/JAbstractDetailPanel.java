package no.mesta.mipss.mipssfield;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JAbstractPanel;
import no.mesta.mipss.ui.JLoadingPanel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstrakt klasse for detaljbilder som skal legges i faner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public abstract class JAbstractDetailPanel<V extends IRenderableMipssEntity, D extends IRenderableMipssEntity> extends
		JAbstractPanel implements
		Comparable<JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity>> {
	private static final Color[] colors = new Color[] { MipssColors.getInspection(), MipssColors.getIncident(),
			MipssColors.getDiscovery() };
	/** Funn detaljbilde */
	public static final Integer DISCOVERY_TYPE = 3;
	private static final ImageIcon[] icons = new ImageIcon[] { IconResources.BIL_SMALL_ICON,
			IconResources.MAP_PIN_RIGHTY_SM, IconResources.MAP_PIN_LEFT_SM };
	/** Hendelse detaljbilde */
	public static final Integer INCIDENT_TYPE = 2;
	/** Inspeksjon detaljbilde */
	public static final Integer INSPECTION_TYPE = 1;
	private static final Logger logger = LoggerFactory.getLogger(JAbstractDetailPanel.class);
	protected List<JLoadingPanel> loadingPanels = new ArrayList<JLoadingPanel>();
	private ChangeListener changeListener;
	private ISaveStateObserver<D> observer;
	private final JDetailFrame owner;
	private final MipssFieldModule parentPlugin;
	private final AbstractAction saveAction;
	private final FieldToolkit toolkit = FieldToolkit.createToolkit();
	private final V view;

	public JAbstractDetailPanel(final V v, final JDetailFrame owner, final MipssFieldModule parentPlugin,
			final AbstractAction saveAction) {
		this.owner = owner;
		this.view = v;
		this.parentPlugin = parentPlugin;
		this.saveAction = saveAction;

		getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "closeAction");
		getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_W, KeyEvent.CTRL_MASK),
				"closeAction");
		getActionMap().put("closeAction", new AbstractAction() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				owner.getTabs().removeChangeListener(changeListener);
				owner.closePanel(JAbstractDetailPanel.this);
			}
		});
	}

	public boolean closeDetailPanel() {
		if (observer.isChanged()) {
			getOwner().navigateToDetailTab(getDetailObject());
			MipssDialogue dialogue = new MipssDialogue(getOwner(), IconResources.WARNING_ICON, GUITexts
					.getText(GUITexts.UNSAVED_CHANGES_TITLE), GUITexts.getText(GUITexts.UNSAVED_CHANGES_TXT),
					MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK,
							MipssDialogue.Button.CANCEL });
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
				return false;
			}
		}

		return true;
	}

	/** {@inheritDoc} */
	public int compareTo(JAbstractDetailPanel<? extends IRenderableMipssEntity, ? extends IRenderableMipssEntity> o) {
		if (o == null) {
			return 1;
		}

		if (getType().equals(o.getType())) {
			return new CompareToBuilder().append(getType(), o.getType()).append(view, o.view).toComparison();
		} else {
			return new CompareToBuilder().append(getType(), o.getType()).toComparison();
		}
	}

	public abstract AbstractAction createDefaultSaveAction();

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		logger.info("disposing: " + view);
		if (toolkit != null) {
			try {
				toolkit.dispose();
			} catch (Throwable t) {
				logger.warn("Dispose ",t);
			}
		}

		if (observer != null) {
			observer.dispose();
			observer = null;
		}
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (!(o instanceof JAbstractDetailPanel)) {
			return false;
		}

		JAbstractDetailPanel<IRenderableMipssEntity, IRenderableMipssEntity> other = (JAbstractDetailPanel<IRenderableMipssEntity, IRenderableMipssEntity>) o;

		return new EqualsBuilder().append(getDetailObject(), other.getDetailObject()).isEquals();
	}

	/**
	 * Objektet som detaljvinduet gjelder for
	 * 
	 * @return
	 */
	public abstract D getDetailObject();

	/**
	 * Toolkitet som benyttes i panelet
	 * 
	 * @return
	 */
	public FieldToolkit getFieldToolkit() {
		return toolkit;
	}

	public MipssField getMipssField() {
		return parentPlugin.getMipssField();
	}

	/**
	 * Vinduet som holder panelet
	 * 
	 * @return
	 */
	public JDetailFrame getOwner() {
		return owner;
	}

	public MipssFieldModule getParentPlugin() {
		return parentPlugin;
	}

	public AbstractAction getSaveAction() {
		if (saveAction != null) {
			return saveAction;
		} else {
			return createDefaultSaveAction();
		}
	}

	public ISaveStateObserver<D> getSaveStateObserver() {
		return observer;
	}

	/**
	 * Bakgrunnsfargen for denne typen panel
	 * 
	 * @return
	 */
	public Color getTabBackground() {
		return colors[getType() - 1];
	}

	/**
	 * Lager komponentet som vises i toppfliken for fanen
	 * 
	 * @param owner
	 *            vinduet som holder panelet
	 * @param tabs
	 *            fanearket som holder panelet
	 * @return Komponentet som skal vises i fliken for fanen som holder dette
	 *         panelet
	 */
	public Component getTabComponent(final JDetailFrame owner, final JDetailTabs tabs) {
		double FACTOR = 1.1d;
		Color o = getTabBackground();
		final Color light = new Color(Math.min((int) (o.getRed() * FACTOR), 255), Math.min(
				(int) (o.getGreen() * FACTOR), 255), Math.min((int) (o.getBlue() * FACTOR), 255));
		final Box box = Box.createHorizontalBox();
		box.setBackground(light);
		box.setOpaque(false);

		changeListener = new ChangeListener() {
			@SuppressWarnings("unchecked")
			public void stateChanged(ChangeEvent e) {
				JDetailTabs source = (JDetailTabs) e.getSource();
				JAbstractDetailPanel<IRenderableMipssEntity, IRenderableMipssEntity> tab = (JAbstractDetailPanel<IRenderableMipssEntity, IRenderableMipssEntity>) source
						.getSelectedComponent();

				int index = source.indexOfComponent(JAbstractDetailPanel.this);
				if (tab != JAbstractDetailPanel.this) {
					logger.debug("stateChanged: {" + getTabTitle() + "} is not active");
					box.setBackground(getTabBackground());
					source.setBackgroundAt(index, getTabBackground());
				} else {
					logger.debug("stateChanged: {" + getTabTitle() + "} is active");
					box.setBackground(light);
					source.setBackgroundAt(index, light);
				}
			}
		};

		JLabel label = new JLabel(getTabTitle(), icons[getType() - 1], JLabel.LEFT);
		label.setOpaque(false);
		label.setFocusable(false);
		ComponentSizeResources.setComponentSizes(label, new Dimension(126, 16), new Dimension(156, 16), new Dimension(
				186, 16));

		JButton button = new JButton(IconResources.CLOSE_TAB_ICON);
		button.setBorder(null);
		button.setFocusable(false);
		button.setOpaque(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tabs.removeChangeListener(changeListener);
				owner.closePanel(JAbstractDetailPanel.this);
			}
		});

		box.add(label);
		box.add(button);

		// Lytter på musa og slår opp i fanene for å styre fager
		box.addMouseListener(new MouseAdapter() {
			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				tabs.setSelectedComponent(JAbstractDetailPanel.this);

				int index = tabs.indexOfComponent(JAbstractDetailPanel.this);
				tabs.setBackgroundAt(index, light);
				box.setBackground(light);
			}

			/** {@inheritDoc} */
			public void mouseEntered(MouseEvent e) {
				box.setBackground(light);

				int index = tabs.indexOfComponent(JAbstractDetailPanel.this);
				tabs.setBackgroundAt(index, light);
			}

			/** {@inheritDoc} */
			@SuppressWarnings("unchecked")
			public void mouseExited(MouseEvent e) {
				box.setBackground(getTabBackground());
				int index = tabs.indexOfComponent(JAbstractDetailPanel.this);

				JAbstractDetailPanel<IRenderableMipssEntity, IRenderableMipssEntity> tab = (JAbstractDetailPanel<IRenderableMipssEntity, IRenderableMipssEntity>) tabs
						.getSelectedComponent();

				if (tab != JAbstractDetailPanel.this) {
					tabs.setBackgroundAt(index, getTabBackground());
					box.setBackground(getTabBackground());
				} else {
					tabs.setBackgroundAt(index, light);
					box.setBackground(light);
				}
			}
		});

		// Lytter på fanene for å styre farger
		tabs.addChangeListener(changeListener);

		ComponentSizeResources.setComponentSizes(box, new Dimension(126, 17), new Dimension(156, 17), new Dimension(
				186, 17));

		return box;
	}

	/**
	 * Tittelen til fanen panelet ligger i
	 * 
	 * @return
	 */
	public abstract String getTabTitle();

	/**
	 * Returnerer typen detaljvindu
	 * 
	 * @return
	 */
	public abstract Integer getType();

	public abstract V getViewObject();

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getDetailObject()).toHashCode();
	}

	public boolean saveOrCancel() {
		if (!observer.isEnabled() && observer.isErrors()) {
			// Kan ikke lagre pga feil
			MipssDialogue dialogue = new MipssDialogue(getOwner(), IconResources.WARNING_ICON, GUITexts
					.getText(GUITexts.WARNING), GUITexts.getText(GUITexts.CANNOT_CONTINUE_DUE_TO_ERRORS),
					MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
			dialogue.askUser();

			return true;
		} else if (observer.isEnabled() && observer.isErrors()) {
			// lagreknappen er på, men objektet inneholder feil.
			MipssDialogue dialogue = new MipssDialogue(getOwner(), IconResources.WARNING_ICON, GUITexts
					.getText(GUITexts.WARNING), GUITexts.getText(GUITexts.CANNOT_CONTINUE_DUE_TO_ERRORS),
					MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
			dialogue.askUser();

			return false;
		} else if (observer.isEnabled() && !observer.isErrors()) {
			// lagreknappen er på, og der finnes ikke feil
			AbstractAction cancelAction = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// Ignorer
				}
			};
			cancelAction.putValue(AbstractAction.NAME, GUITexts.getText(GUITexts.CANCEL));
			MipssDialogue.ActionButton cancel = new MipssDialogue.ActionButton(cancelAction,
					MipssDialogue.Button.CANCEL);
			MipssDialogue.ActionButton save = new MipssDialogue.ActionButton(getSaveAction(), MipssDialogue.Button.SAVE);

			MipssDialogue dialogue = new MipssDialogue(getOwner(), IconResources.WARNING_ICON, GUITexts
					.getText(GUITexts.WARNING), GUITexts.getText(GUITexts.SAVE_OR_ABORT_BEFORE_CONTINUE),
					MipssDialogue.Type.WARNING, cancel, save);
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.SAVE) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	protected void waitAndBindWhenReady(){
		new SwingWorker<Void, Void>(){
			@Override
			public Void doInBackground(){
				if (!isAllPanelsLoaded()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
		        return null;
			}
			@Override
			public void done(){
				doneLoading();
			}
		}.execute();
	}
	
	protected void doneLoading(){
		getFieldToolkit().bind();
        if(!parentPlugin.hasWriteAccess()) {
        	getFieldToolkit().setEnableFields(false);
        }
	}
	private boolean isAllPanelsLoaded(){
		int loaded = 0;
		for (JLoadingPanel p:loadingPanels){
			if (p.isLoaded())
				loaded++;
		}
		return (loaded == loadingPanels.size());
	}
	public void setSaveStateObserver(ISaveStateObserver<D> observer) {
		this.observer = observer;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "{title=" + getWindowTitle() + ",toString():" + super.toString() + "}";
	}
}
