package no.mesta.mipss.mipssfield.inspection;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.ISaveStateObserver;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;

import org.jdesktop.beansbinding.Binding;

/**
 * Handlinger man kan utføre på et funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JInspectionDetailActions extends JPanel {
	private static final int STRUT = 2;
	private final JButton closeWindow;
	private final JInspectionDetailPanel parent;
	private final JButton saveButton;

	public JInspectionDetailActions(final JInspectionDetailPanel parent) {
		this.parent = parent;

		closeWindow = new JButton(new CloseWindowAction(GUITexts.getText(GUITexts.CLOSE_WINDOW),
				IconResources.CANCEL_ICON));
		closeWindow.setEnabled(false);
		saveButton = new JButton(parent.getSaveAction());
		saveButton.setEnabled(false);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		if(!parent.getParentPlugin().hasWriteAccess()) {
			saveButton.setVisible(false);
		}
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, BoxUtil.createHorizontalStrut(STRUT), saveButton,
				Box.createHorizontalGlue(), closeWindow, BoxUtil.createHorizontalStrut(STRUT)));
		add(BoxUtil.createVerticalStrut(STRUT));
	}

	public JInspectionDetailPanel getParent() {
		return parent;
	}

	public void ready() {
		closeWindow.setEnabled(true);
		saveButton.setEnabled(true);

		ISaveStateObserver<Inspeksjon> observer = parent.getSaveStateObserver();
		Binding<FieldToolkit, Boolean, ISaveStateObserver<Inspeksjon>, Boolean> observeChange = BindingHelper.createbinding(parent
				.getFieldToolkit(), "fieldsChanged", observer, "changed");
		Binding<ISaveStateObserver<Inspeksjon>, Boolean, JButton, Boolean> enableSave = BindingHelper.createbinding(observer,
				"enabled", saveButton, "enabled");
		parent.setSaveStateObserver(observer);

		parent.getFieldToolkit().addBinding(observeChange);
		parent.getFieldToolkit().addBinding(enableSave);
	}

	/**
	 * Lukker vinduet
	 */
	class CloseWindowAction extends AbstractAction {

		public CloseWindowAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			parent.getOwner().closeWindow();
		}
	}
}
