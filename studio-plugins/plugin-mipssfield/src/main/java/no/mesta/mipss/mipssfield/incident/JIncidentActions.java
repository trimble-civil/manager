package no.mesta.mipss.mipssfield.incident;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.reports.HendelseDS;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.MipssFieldDS;
import no.mesta.mipss.reports.R11DS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.MipssProgressDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import no.mesta.mipss.util.EpostUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

/**
 * Handlingsknapper for inspeksjoner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JIncidentActions extends JPanel {
	private static final String ENABLED = "enabled";
	private static final Logger logger = LoggerFactory.getLogger(JIncidentActions.class);
	private MipssFieldModule parentPlugin;
	private BindingGroup group;
	private JButton sendElrappButton;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 */
	@SuppressWarnings("unchecked")
	public JIncidentActions(MipssFieldModule parentPlugin) {
		logger.debug("JIncidentActions() start");
		this.parentPlugin = parentPlugin;
		List<JMenuItem> items = new ArrayList<JMenuItem>();

		JButton newButton = new JButton(new NewHendelse(GUITexts.getText(GUITexts.NEW), IconResources.ADD_ITEM_ICON));
		JButton openButton = new JButton(new OpenAction(GUITexts.getText(GUITexts.DETAILS), IconResources.DOCUMENT_OPEN));
		JButton mapButton = new JButton(new MapAction(GUITexts.getText(GUITexts.MAP), IconResources.NORGE_ICON));
		
		
		mapButton.setEnabled(false);
		JMenuItem r2FormItem = new JMenuItem(new R2FormAction(GUITexts.getText(GUITexts.R2_FORM), IconResources.ADOBE_16));
		final JMenuItem r5FormItem = new JMenuItem(new R5FormAction(GUITexts.getText(GUITexts.R5_FORM), IconResources.ADOBE_16));
		final JMenuItem r11FormItem = new JMenuItem(new R11FormAction(GUITexts.getText(GUITexts.R11_FORM), IconResources.ADOBE_16));
		
		JButton excelButton = new JButton(new ExcelAction(GUITexts.getText(GUITexts.EXCEL),
				IconResources.EXCEL_ICON16));
		JButton slettButton = new JButton(new DeleteAction(GUITexts.getText(GUITexts.DELETE), IconResources.TRASH_ICON));
		JButton gjenopprettButton = new JButton(new RestoreAction(GUITexts.getText(GUITexts.RESTORE),
				IconResources.UNTRASH_ICON));
		sendElrappButton = new JButton(new SendElrappAction(GUITexts.getText(GUITexts.SEND_ELRAPP), IconResources.ELRAPP_ICON_16));
		
		items.add(r2FormItem);
		items.add(r5FormItem);
		items.add(r11FormItem);
		JPopupListMenu reportsPopup = new JPopupListMenu(GUITexts.getText(GUITexts.REPORTS), IconResources.PRINT_ICON,
				items);

		group = new BindingGroup();
		Binding detailsBinding = BindingHelper.createbinding(parentPlugin.getIncidentTable(),
				"${noOfSelectedRows == 1}", openButton, ENABLED);
		Binding mapBinding = BindingHelper.createbinding(parentPlugin.getIncidentTable(), "${noOfSelectedRows > 0}", mapButton, ENABLED);
//		Binding r5Binding = BindingHelper.createbinding(parentPlugin.getIncidentTable(),"${noOfSelectedRows == 1}", r5Button, ENABLED);
		
		Binding r11FormBinding = BindingHelper.createbinding(parentPlugin.getIncidentTable(), "${noOfSelectedRows > 0}", r11FormItem, ENABLED);
		Binding r5FormBinding = BindingHelper.createbinding(parentPlugin.getIncidentTable(), "${noOfSelectedRows > 0}", r5FormItem, ENABLED);
		Binding r2FormBinding = BindingHelper.createbinding(parentPlugin.getIncidentTable(), "${noOfSelectedRows > 0}", r2FormItem, ENABLED);
		Binding excelBinding = BindingHelper.createbinding(parentPlugin.getIncidentTableModel(), "${size > 0}",
				excelButton, ENABLED);
		Binding popupBinding = BindingHelper.createbinding(parentPlugin.getIncidentTableModel(), "${size > 0}",
				reportsPopup, ENABLED);
		Binding slettBinding = BindingHelper.createbinding(parentPlugin.getIncidentFilter(), "${kunSlettede != true}",
				slettButton, "visible");
		Binding slettEnable = BindingHelper.createbinding(parentPlugin.getIncidentTable(), "${noOfSelectedRows > 0}",
				slettButton, ENABLED);
		Binding gjenopprettBinding = BindingHelper.createbinding(parentPlugin.getIncidentFilter(),
				"${kunSlettede == true}", gjenopprettButton, "visible");
		Binding gjenopprettEnable = BindingHelper.createbinding(parentPlugin.getIncidentTable(),
				"${noOfSelectedRows > 0}", gjenopprettButton, ENABLED);
		Binding nyBinding = BindingHelper.createbinding(parentPlugin.getCommonFilterPanel(),
				"${valgtKontrakt != null}", newButton, ENABLED);
		
		Binding elrappBinding = BindingHelper.createbinding(parentPlugin.getIncidentTable(), "${noOfSelectedRows > 0}", sendElrappButton, ENABLED);
		
//		group.addBinding(r5Binding);	
		group.addBinding(nyBinding);
		group.addBinding(detailsBinding);
		group.addBinding(mapBinding);
		group.addBinding(r2FormBinding);
		group.addBinding(r5FormBinding);
		group.addBinding(r11FormBinding);
		group.addBinding(excelBinding);
		group.addBinding(popupBinding);
		group.addBinding(slettBinding);
		group.addBinding(slettEnable);
		group.addBinding(gjenopprettBinding);
		group.addBinding(gjenopprettEnable);
		group.addBinding(elrappBinding);
		
		logger.debug("JIncidentActions() bind start");
		group.bind();
		logger.debug("JIncidentActions() bind end");

		parentPlugin.getIncidentTable().addMouseListener(new MouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (JIncidentActions.this.parentPlugin.getIncidentTable().getNoOfSelectedRows()==1){
					if (JIncidentActions.this.parentPlugin.getIncidentTable().getSelectedEntities().get(0).getR5().intValue()==0){
						r5FormItem.setEnabled(false);
					}else{
						r5FormItem.setEnabled(true);
					}
					
					if (JIncidentActions.this.parentPlugin.getIncidentTable().getSelectedEntities().get(0).getR11().intValue()==0){
						r11FormItem.setEnabled(false);
					}else{
						r11FormItem.setEnabled(true);
					}
				}
			}
		});
		
		
		BoxLayout layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
		setLayout(layout);
		if (parentPlugin.hasWriteAccess()) {
			add(slettButton);
			add(gjenopprettButton);
			add(BoxUtil.createHorizontalStrut(10));
			add(newButton);
//			add(BoxUtil.createHorizontalStrut(10));
//			add(r5Button);
		}
		add(Box.createHorizontalGlue());
		add(sendElrappButton);
		add(BoxUtil.createHorizontalStrut(10));
		
		add(excelButton);
		add(BoxUtil.createHorizontalStrut(10));
		add(reportsPopup);
		add(BoxUtil.createHorizontalStrut(10));
		add(mapButton);
		add(BoxUtil.createHorizontalStrut(10));
		add(openButton);
		logger.debug("JIncidentActions() end");

		Dimension min = new Dimension(200, 24);
		Dimension pref = new Dimension(200, 24);
		Dimension max = new Dimension(Short.MAX_VALUE, 24);
		ComponentSizeResources.setComponentSizes(this, min, pref, max);
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		group.unbind();
		group = null;
		parentPlugin = null;
	}
	private List<String> getIds(List<HendelseSokView> selection) {
		List<String> list = new ArrayList<String>();

		for (HendelseSokView h : selection) {
			list.add(h.getGuid());
		}

		return list;
	}
	
	/**
	 * Returnerer en liste med forsikringsskade GUID'er basert på et utvalg
	 * av hendelsesokview'er. Dersom hendelsene ikke har tilknyttet noen 
	 * forsikringsskader vil listen av id'er være tom, kun de hendelsene
	 * med r5==1 vil bli evaluert.
	 * 
	 * @param selection
	 * @return
	 */
	private List<String> getForsikringsskadeIds(List<HendelseSokView> selection){
		MipssField bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		List<String> list = new ArrayList<String>();
		
		for (HendelseSokView h:selection){
			if (h.getR5().intValue()==1){
				list.add(h.getGuid());
			}
		}
		List<String> forsikringsskadeGuids=bean.hentForsikringsskadeGuid(list);
		return forsikringsskadeGuids;
	}
	
	private List<String> getSkredGuids(List<HendelseSokView> selection){
		MipssField bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
		List<String> list = new ArrayList<String>();
		
		for (HendelseSokView h:selection){
			if (h.getR11().intValue()==1){
				list.add(h.getGuid());
			}
		}
		List<String> skredGuids=bean.hentSkredGuid(list);
		return skredGuids;
	}
	/**
	 * TODO dette er en hack for å få forsikringsskaderapporten til å fungere og SKAL
	 * FJERNES NÅR VI DEPLOYER NY SERVER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * @param selection
	 * @return
	 */
	@Deprecated
	private List<String> getForsikringsSkadeGUIDHack(List<String> selection){
		String ids = "";
		for (int i=0;i<selection.size();i++){
			ids+="'"+selection.get(i)+"'";
			if (i<selection.size()-1)
				ids+=",";
		}
		List<String> guids = new ArrayList<String>();
		String qry = "select fs.guid from hendelse h join forsikringsskade fs on fs.hendelse_guid=h.guid where h.guid in("+ids+")";
		try (Connection c = parentPlugin.getLoader().getConnection();
			PreparedStatement rows = c.prepareStatement(qry);
			ResultSet r = rows.executeQuery()) {
			if (r.next()){
				guids.add(r.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return guids;
	}
	class DeleteAction extends AbstractAction {

		public DeleteAction(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();

			MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
					IconResources.QUESTION_ICON, GUITexts.getText(GUITexts.DELETE_INCIDENTS_TITLE), GUITexts.getText(
							GUITexts.DELETE_INCIDENTS_TXT, selection.size()), MipssDialogue.Type.QUESTION,
					new MipssDialogue.Button[] { MipssDialogue.Button.YES, MipssDialogue.Button.NO, MipssDialogue.Button.CANCEL });
			dialogue.askUser();

			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL
					|| dialogue.getPressedButton() == MipssDialogue.Button.NOTHING) {
				return;
			}

			boolean deleteOtherData = dialogue.getPressedButton() == MipssDialogue.Button.YES;

			parentPlugin.deleteIncidents(selection, deleteOtherData);
		}
	}

	class ExcelAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public ExcelAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			JMipssBeanTable<HendelseSokView> table = parentPlugin.getIncidentTable();
			try {
				parentPlugin.openExcel(table);
			} catch (Throwable t) {
				parentPlugin.getLoader().handleException(t, this, GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
			}
		}
	}

	/**
	 * Action klasse for å åpne hendelse i kart
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class MapAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public MapAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();
			if (selection != null) {
				parentPlugin.showIncidentsInMap(selection);
			}
		}
	}

	/**
	 * Action klasse for å åpne hendelse
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class OpenAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public OpenAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();

			if (selection != null) {
				parentPlugin.openIncident(selection);
			}
		}
	}

	/**
	 * Action klasse for å åpne hendelse i R2 skjema
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	class R2FormAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public R2FormAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();
			if (selection != null) {
				try {
					if (selection.size() > 4) {
						MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
								IconResources.WARNING_ICON, GUITexts.getText(GUITexts.WARNING), GUITexts.getText(
										GUITexts.OPEN_MANY_REPORT_INCIDENTS, selection.size()),
								MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK,
										MipssDialogue.Button.CANCEL });
						dialogue.askUser();
						if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
							return;
						}
					}

					List<String> ids = getIds(selection);
					//List<Hendelse> hendelser = parentPlugin.getMipssField().hentHendelser(ids);
					//JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(hendelser);
					
					HendelseDS dataSource = new HendelseDS(ids);
//					MipssFieldDS<Hendelse> dataSource = new MipssFieldDS<Hendelse>(Hendelse.class, ids);
//					dataSource.setFieldBean(parentPlugin.getMipssField());
//					
					JReportBuilder builder = new JReportBuilder(Report.R2_FORM, Report.R2_FORM.getDefaultParameters(),
							dataSource, ReportHelper.EXPORT_PDF, parentPlugin.getLoader().getApplicationFrame());
					File report = builder.getGeneratedReportFile();

					if (report != null) {
						DesktopHelper.openFile(report);
					}
				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, parentPlugin,
							GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}
	
	/**
	 * Action klasse for å åpne hendelse i R5 skjema
	 * 
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 */
	class R5FormAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public R5FormAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();
			if (selection != null) {
				try {
					if (selection.size() > 4) {
						MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
								IconResources.WARNING_ICON, GUITexts.getText(GUITexts.WARNING), GUITexts.getText(
										GUITexts.OPEN_MANY_REPORT_INCIDENTS, selection.size()),
								MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK,
										MipssDialogue.Button.CANCEL });
						dialogue.askUser();
						if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
							return;
						}
					}
					List<String> ids = getForsikringsskadeIds(selection);
					if (ids.isEmpty()){
						return;
					}
					MipssFieldDS<Forsikringsskade> dataSource = new MipssFieldDS<Forsikringsskade>(Forsikringsskade.class, ids);
					dataSource.setFieldBean(parentPlugin.getMipssField());
					JReportBuilder builder = new JReportBuilder(Report.R5_FORM, Report.R5_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, parentPlugin.getLoader().getApplicationFrame());
					File report = builder.getGeneratedReportFile();

					if (report != null) {
						DesktopHelper.openFile(report);
					}
				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, parentPlugin,GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}
	/**
	 * Action klasse for å åpne hendelse i R11 skjema
	 * 
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 */
	class R11FormAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public R11FormAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();
			if (selection != null) {
				try {
					if (selection.size() > 4) {
						MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
								IconResources.WARNING_ICON, GUITexts.getText(GUITexts.WARNING), GUITexts.getText(
										GUITexts.OPEN_MANY_REPORT_INCIDENTS, selection.size()),
								MipssDialogue.Type.WARNING, new MipssDialogue.Button[] { MipssDialogue.Button.OK,
										MipssDialogue.Button.CANCEL });
						dialogue.askUser();
						if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
							return;
						}
					}
					List<String> ids = getSkredGuids(selection);
					if (ids.isEmpty()){
						return;
					}
					R11DS dataSource = new R11DS(ids);
//					dataSource.setFieldBean(parentPlugin.getMipssField());
					JReportBuilder builder = new JReportBuilder(Report.R11_FORM, Report.R11_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, parentPlugin.getLoader().getApplicationFrame());
					File report = builder.getGeneratedReportFile();
					if (report != null) {
						DesktopHelper.openFile(report);
					}

				} catch (Throwable t) {
					parentPlugin.getLoader().handleException(t, parentPlugin,GUITexts.getText(GUITexts.REPORT_FAILED_TXT));
				}
			}
		}
	}
	class RestoreAction extends AbstractAction {

		public RestoreAction(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<HendelseSokView> selection = parentPlugin.getIncidentTable().getSelectedEntities();

			MipssDialogue dialogue = new MipssDialogue(parentPlugin.getLoader().getApplicationFrame(),
					IconResources.INFO_ICON, GUITexts.getText(GUITexts.RESTORE_INCIDENTS_TITLE), GUITexts.getText(
							GUITexts.RESTORE_INCIDENTS_TXT, selection.size()), MipssDialogue.Type.INFO,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();

			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL
					|| dialogue.getPressedButton() == MipssDialogue.Button.NOTHING) {
				return;
			}

			parentPlugin.restoreIncidents(selection);
		}
	}

	
	class NewHendelse extends AbstractAction {

		public NewHendelse(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			parentPlugin.enforceWriteAccess();

			// Kanskje lage egen skjerm for nytt funn?
			parentPlugin.openIncident(Collections.singletonList((HendelseSokView) null));
		}
	}
	public static String checkContractReadyForElrapp(Driftkontrakt d, Bruker bruker){
		List<String> str = new ArrayList<String>();
		if (bruker.getElrappIdent()==null){
			str.add("Brukeren:"+bruker.getSignatur()+" mangler elrappbruker, be driftsleder sette opp en bruker under stab og prosjekt i kontraktsdatamodulen.\n");
		}
		
		List<String> kontraktErr = new ArrayList<String>();
		kontraktErr.add("Kontrakten mangler elrappkonfigurasjon:");
		if (d.getElrappKontraktIdent()==null){
			kontraktErr.add("-Elrapp kontraktident");
		}
		if (d.getElrappSluttDato()==null){
			kontraktErr.add("-Elrapp sluttdato");
		}
		if (d.getElrappStartDato()==null){
			kontraktErr.add("-Elrapp startdato");
		}
		if (kontraktErr.size()>1){
			kontraktErr.add("Send en e-post til " + EpostUtils.getSupportEpostAdresse() + "for hjelp");
			String errmsg =StringUtils.join(kontraktErr.toArray(), "\n");
			str.add(errmsg);
		}
		if (str.size()>0){
			return StringUtils.join(str.toArray(), "\n");
		}
		return null;
	}
	class SendElrappAction extends AbstractAction {

		public SendElrappAction(String name, Icon icon) {
			super(name, icon);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			sendElrappButton.setEnabled(false);
			
			final HendelseSokView h = JIncidentActions.this.parentPlugin.getIncidentTable().getSelectedEntities().get(0);
			String d = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.field", "elrappRapporteringsGrenseDato").getVerdi();
			SimpleDateFormat f = new SimpleDateFormat(MipssDateFormatter.DATE_TIME_FORMAT);
			Date notBeforeDate=null;
			try {
				notBeforeDate = f.parse(d);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			
			if (h.getOpprettetDato().before(notBeforeDate)){
				JOptionPane.showMessageDialog(parentPlugin.getLoader().getApplicationFrame(), GUITexts.getText(GUITexts.ELRAPP_GRENSE_DATO_WARNING), GUITexts.getText(GUITexts.ELRAPP_GRENSE_DATO_WARNING_TITLE), JOptionPane.INFORMATION_MESSAGE);
				sendElrappButton.setEnabled(true);
				return;
			}
			
			final Bruker bruker = parentPlugin.getLoader().getLoggedOnUser(true);
			Driftkontrakt dk = parentPlugin.getCommonFilterPanel().getValgtKontrakt();
			String msg = checkContractReadyForElrapp(dk, bruker);
			if (msg!=null){
				JOptionPane.showMessageDialog(parentPlugin.getLoader().getApplicationFrame(), msg, "Kan ikke sende data til Elrapp", JOptionPane.ERROR_MESSAGE);
				sendElrappButton.setEnabled(true);
				return;
			}
			
			final MipssProgressDialog progress = new MipssProgressDialog(GUITexts.getText(GUITexts.SEND_ELRAPP_MESSAGE), parentPlugin.getLoader().getApplicationFrame());
			progress.setVisible(true);
//			new SwingWorker(){
//				@Override
//				public Void doInBackground(){
//					
//					try {
//						ElrappService b = BeanUtil.lookup(ElrappService.BEAN_NAME, ElrappService.class);
//						HendelseSokView h = JIncidentActions.this.parentPlugin.getIncidentTable().getSelectedEntities().get(0);
//						ElrappResponse res = b.sendRSchema(h, bruker);
//						
//						progress.dispose();
//						if (res!=null){
//							Long elrappDokumentId = res.getElrappDokumentId();
//							Long elrappVersjon = res.getElrappVersjon();
//							String message = GUITexts.getText(GUITexts.SEND_ELRAPP_MESSAGE_OK, new Object[]{res.getRtype(), elrappDokumentId, elrappVersjon});
//							String title = GUITexts.getText(GUITexts.SEND_ELRAPP_MESSAGE_OK_TITLE, new Object[]{res.getRtype()});
//							JOptionPane.showMessageDialog(parentPlugin.getLoader().getApplicationFrame(), message, title, JOptionPane.INFORMATION_MESSAGE);
//						}
//					} catch (Throwable t){
//						progress.dispose();
//						ErrorInfo info = new ErrorInfo("Feil ved sending til elrapp", "Skjema ble IKKE sendt\n"+t.getMessage(), null, "Feil", t, Level.SEVERE, null);
//						t.printStackTrace();
//					    JXErrorPane.showDialog(null, info);
//					} finally{
//						sendElrappButton.setEnabled(true);
//					}
//					return null;
//				}
//				public void done(){
//					parentPlugin.fireElrappSendtEvent();
//				}
//			}.execute();
		}
	}
	class R5Action extends AbstractAction {

		public R5Action(String name, Icon icon) {
			super(name, icon);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			MipssField bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
			
			HendelseSokView h = JIncidentActions.this.parentPlugin.getIncidentTable().getSelectedEntities().get(0);
			if (h.getR5().intValue()==0){
				//Ingen r5 registrert opprett ny 
				
			}else{
				//r5 eksisterer rediger
				
			}
			
			parentPlugin.enforceWriteAccess();
			
		}
	}
}
