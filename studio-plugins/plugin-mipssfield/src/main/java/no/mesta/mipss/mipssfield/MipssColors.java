package no.mesta.mipss.mipssfield;

import java.awt.Color;

import org.apache.commons.lang.StringUtils;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;

/**
 * Fargene til inspeksjoner
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 * 
 */
public class MipssColors {
	private static final String APP = "studio.mipss.field";
	private static final String APP_VALUE_DISC = "colorDiscovery";
	private static final String APP_VALUE_INCI = "colorIncident";
	private static final String APP_VALUE_INSP = "colorInspection";
	private static final Color DISC;
	private static final Color INCI;
	private static final Color INSP;

	static {
		DISC = setupColor(APP_VALUE_DISC);
		INCI = setupColor(APP_VALUE_INCI);
		INSP = setupColor(APP_VALUE_INSP);
	}

	public static Color getDiscovery() {
		return new Color(DISC.getRGB());
	}

	public static Color getIncident() {
		return new Color(INCI.getRGB());
	}

	public static Color getInspection() {
		return new Color(INSP.getRGB());
	}

	private static Color setupColor(String appValueDisc) {
		Konfigparam param = KonfigparamPreferences.getInstance().hentEnForApp(APP, appValueDisc);

		try {
			int rgb = Integer.decode(param.getVerdi());

			return new Color(rgb);
		} catch (Throwable t) {
			t.printStackTrace();
			if (param == null && StringUtils.equals(appValueDisc, APP_VALUE_DISC)) {
				return new Color(0xebdcb7);
			} else if (param == null && StringUtils.equals(appValueDisc, APP_VALUE_INCI)) {
				return new Color(0xebb7e9);
			} else if (param == null && StringUtils.equals(appValueDisc, APP_VALUE_INSP)) {
				return new Color(0xb7d2eb);
			} else {
				return null;
			}
		}
	}
}
