package no.mesta.mipss.mipssfield;

import no.mesta.mipss.core.IBlankFieldValidator;
import no.mesta.mipss.core.IDisposable;

/**
 * Avgjør om lagreknappen skal være på eller av.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public interface ISaveStateObserver<D> extends IDisposable{

	/**
	 * Legger til en validator til denne observatøren
	 * 
	 * @param validator
	 */
	public void addValidator(IBlankFieldValidator<?> validator);
	
	/**
	 * Beregner om objektet er endret eller ikke
	 * 
	 * @return
	 */
	public boolean isChanged();
	
	/**
	 * Avgjør om en lagreknapp er på eller av
	 * 
	 * @return
	 */
	public boolean isEnabled();
	
	/**
	 * Forteller om det er feil på objektet som overvåkes
	 * 
	 * @return
	 */
	public boolean isErrors();
	
	/**
	 * Objektet som overvåkes
	 * 
	 * @param detail
	 */
	public void setDetail(D detail);
}
