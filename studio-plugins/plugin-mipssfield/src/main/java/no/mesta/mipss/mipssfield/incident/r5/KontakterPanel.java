package no.mesta.mipss.mipssfield.incident.r5;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.UIUtils;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.jxlayer.JXLayer;

public class KontakterPanel extends JPanel{
	private BindingGroup bg = new BindingGroup();

	private JCheckBox vitnerJa;
	private JFormattedTextField vitnerHvemField;
	private JMipssDatePicker vitnerDato;
	private JFormattedTextField politiField;
	private JMipssDatePicker politiDato;
	private JFormattedTextField politiResultatField;
	private JFormattedTextField bergingNavnField;
	private JFormattedTextField bergingTlfField;
	private JFormattedTextField bergingKontakt1NavnField;
	private JFormattedTextField bergingKontakt1TlfField;
	private JFormattedTextField bergingKontakt1ResultatField;
	private JFormattedTextField berging2NavnField;
	private JFormattedTextField berging2TlfField;
	private JFormattedTextField bergingKontakt2NavnField;
	private JFormattedTextField bergingKontakt2ResultatField;
	private JFormattedTextField bergingKontakt2TlfField;
	private JRadioButton politiJa;
	private JRadioButton politiNei;
	private JFormattedTextField tilleggField;

	private final Forsikringsskade forsikringsskade;
	private final GridBagConstraints c;

	private final R5 r5;

	public KontakterPanel(Forsikringsskade forsikringsskade, GridBagConstraints c, R5 r5){
		this.forsikringsskade = forsikringsskade;
		this.c = c;
		this.r5 = r5;
		initGui();
	}
	
	private void initGui(){
		setLayout(new GridBagLayout());
		vitnerJa = new JCheckBox(GUITexts.getText(GUITexts.YES));
		vitnerHvemField = new JFormattedTextField();
		vitnerDato = new JMipssDatePicker();
		vitnerDato.setDate(null);
		
		politiField = new JFormattedTextField();
		politiDato = new JMipssDatePicker();
		politiDato.setDate(null);
		
		politiJa = new JRadioButton(GUITexts.getText(GUITexts.YES));
		politiNei = new JRadioButton(GUITexts.getText(GUITexts.NO));
		politiNei.setSelected(true);
		ButtonGroup pGroup = new ButtonGroup();
		pGroup.add(politiJa);
		pGroup.add(politiNei);
		
		politiResultatField = new JFormattedTextField(); 
		bergingNavnField = new JFormattedTextField();
		bergingTlfField = new JFormattedTextField();
		bergingTlfField.setDocument(new DocumentFilter(20));
		berging2TlfField = new JFormattedTextField();
		berging2TlfField.setDocument(new DocumentFilter(20));
		
		bergingKontakt1NavnField = new JFormattedTextField();
		bergingKontakt1TlfField = new JFormattedTextField();
		bergingKontakt1TlfField.setDocument(new DocumentFilter(20));
		bergingKontakt1ResultatField = new JFormattedTextField();
		
		berging2NavnField = new JFormattedTextField();
		bergingKontakt2NavnField = new JFormattedTextField();
		bergingKontakt2TlfField = new JFormattedTextField();
		bergingKontakt2TlfField.setDocument(new DocumentFilter(20));
		bergingKontakt2ResultatField = new JFormattedTextField();
		
		tilleggField = new JFormattedTextField();
		bind();
		createKontakterPanel();
	}
	private void bind(){
		Binding vitnerDatoEnable = BindingHelper.createbinding(vitnerJa, "selected", vitnerDato, "enabled");
		Binding vitnerEnable = BindingHelper.createbinding(vitnerJa, "selected", vitnerHvemField, "enabled");

		bg.addBinding(vitnerDatoEnable);
		bg.addBinding(vitnerEnable);
		
		bg.addBinding(BindingHelper.createbinding(forsikringsskade, "tilleggsopplysninger", tilleggField, "text", BindingHelper.READ_WRITE));
		
		bg.bind();
	}
	
	private void createKontakterPanel() {
		JLabel skadevolderKjentLabel = new JLabel(GUITexts.getText(GUITexts.SKADEVOLDER_KJENT));
		JLabel vitnerLabel = new JLabel(GUITexts.getText(GUITexts.VITNER));
		JLabel vitnerHvemLabel = new JLabel(GUITexts.getText(GUITexts.VITNER_HVEM));
		JLabel vitnerDatoLabel = new JLabel(GUITexts.getText(GUITexts.DATE));
		JLabel politiLabel = new JLabel(GUITexts.getText(GUITexts.POLITI));
		JLabel politiDatoLabel= new JLabel(GUITexts.getText(GUITexts.DATE));
		JLabel politiResultatLabel = new JLabel(GUITexts.getText(GUITexts.POLITI_RESULTAT));
		JLabel bergingselskLabel = new JLabel(GUITexts.getText(GUITexts.BERGINGSELSK));
		JLabel navnBergingLabel = new JLabel(GUITexts.getText(GUITexts.NAVN_BERGING));
		JLabel kontaktpersonLabel = new JLabel(GUITexts.getText(GUITexts.KONTAKTPERSON));
		JLabel kontaktpersonTlfLabel = new JLabel(GUITexts.getText(GUITexts.KONTAKTPERSON_TLF));
		JLabel fire1Label = new JLabel(GUITexts.getText(GUITexts.FIRE1));
		JLabel fire2Label = new JLabel(GUITexts.getText(GUITexts.FIRE2));
		JLabel politiAnmeldLabel = new JLabel(GUITexts.getText(GUITexts.ANMELD_POLITI));
		JLabel kopianmeldLabel = new JLabel(GUITexts.getText(GUITexts.KOPI_ANMELD));
		JLabel tilleggLabel = new JLabel(GUITexts.getText(GUITexts.TILLEGG));
		
		Font f = skadevolderKjentLabel.getFont();
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		skadevolderKjentLabel.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
		add(skadevolderKjentLabel, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		JPanel vitnerPanel = UIUtils.getHorizPanel();
		vitnerPanel.add(vitnerLabel);
		vitnerPanel.add(UIUtils.getHStrut(5));
		vitnerPanel.add(vitnerJa);
		c.anchor=GridBagConstraints.SOUTH;
		add(vitnerPanel, c);
		c.anchor=GridBagConstraints.CENTER;
		c.gridx+=2;
		add(UIUtils.getTwoComponentPanelVert(vitnerHvemLabel, decorateField(vitnerHvemField, "validateVitne"), false), c);
		c.gridx+=2;
		c.gridwidth=1;
		add(UIUtils.getTwoComponentPanelVert(vitnerDatoLabel, vitnerDato, true), c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=4;
		add(UIUtils.getTwoComponentPanelVert(politiLabel, decorateField(politiField, "validatePoliti"), false), c);
		
		c.gridx+=4;
		c.gridwidth=1;
		add(UIUtils.getTwoComponentPanelVert(politiDatoLabel, politiDato, true), c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		add(UIUtils.getTwoComponentPanelVert(politiResultatLabel, politiResultatField, false), c);
		
		Insets i = c.insets;
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		c.insets=new Insets(10,0,10,20);
		add(bergingselskLabel, c);
		
		c.insets = i;
		c.gridx=0;
		c.gridy++;
		c.gridwidth=3;
		add(navnBergingLabel, c);
		
		c.gridx+=3;
		c.gridwidth=1;
		add(kontaktpersonLabel, c);
		c.gridx++;
		add(kontaktpersonTlfLabel, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=3;
		JPanel fire1Pnl = UIUtils.getHorizPanel();
		fire1Pnl.add(fire1Label);
		fire1Pnl.add(UIUtils.getHStrut(5));
		fire1Pnl.add(decorateField(bergingNavnField, "validateBergingsselskap1"));
		fire1Pnl.add(bergingTlfField);
		Dimension bsize = new Dimension(75, 20);
		bergingTlfField.setPreferredSize(bsize);
		bergingTlfField.setSize(bsize);
		bergingTlfField.setMaximumSize(bsize);
		c.anchor=GridBagConstraints.NORTH;
		add(fire1Pnl, c);
		c.gridx+=3;
		
		c.gridwidth=1;
		add(bergingKontakt1NavnField, c);
		c.gridx++;
		add(bergingKontakt1TlfField, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		JPanel resultat1Panel = UIUtils.getHorizPanel();
		resultat1Panel.add(new JLabel("Resultat:"));
		resultat1Panel.add(bergingKontakt1ResultatField);
		add(resultat1Panel, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=3;
		JPanel fire2Pnl = UIUtils.getHorizPanel();
		fire2Pnl.add(fire2Label);
		fire2Pnl.add(UIUtils.getHStrut(5));
		fire2Pnl.add(decorateField(berging2NavnField, "validateBergingsselskap2"));
		fire2Pnl.add(berging2TlfField);
		berging2TlfField.setPreferredSize(bsize);
		berging2TlfField.setSize(bsize);
		berging2TlfField.setMaximumSize(bsize);
		
		add(fire2Pnl, c);
		c.gridx+=3;
		c.gridwidth=1;
		add(bergingKontakt2NavnField, c);
		c.gridx++;
		add(bergingKontakt2TlfField, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		JPanel resultat2Panel = UIUtils.getHorizPanel();
		resultat2Panel.add(new JLabel("Resultat:"));
		resultat2Panel.add(bergingKontakt2ResultatField);
		add(resultat2Panel, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		JPanel politiPanel = UIUtils.getHorizPanel();
		politiPanel.add(politiAnmeldLabel);
		politiPanel.add(UIUtils.getHStrut(5));
		politiPanel.add(politiJa);
		politiPanel.add(politiNei);
		politiPanel.add(UIUtils.getHStrut(5));
		politiPanel.add(kopianmeldLabel);
		add(politiPanel, c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=5;
		add(UIUtils.getTwoComponentPanelVert(tilleggLabel, tilleggField, false), c);
		
		c.gridwidth=1;
		c.gridx=4;
		c.gridy++;
		
		add(new JButton(r5.getPreviewAction(GUITexts.getText(GUITexts.PREVIEW), null)), c);
		
		c.gridx=0;
		c.gridy++;
		c.fill=GridBagConstraints.BOTH;
		c.weightx=1;
		c.weighty=1;
		add(new JPanel(), c);
	}
	private JXLayer<JComponent> decorateField(JComponent c, String validationMethod){
        JXLayer<JComponent> layer = new JXLayer<JComponent>(c);
        layer.setUI(new ValidationLayerUI(validationMethod, this));
        return layer;
	}
	public String getVitneNavn(){
		return vitnerHvemField.getText();
	}
	public Date getVitneDato(){
		return vitnerDato.getDate();
	}
	public boolean isVitnerJaSelected(){
		return vitnerJa.isSelected();
	}
	public boolean isPolitiJaSelected(){
		return politiJa.isSelected();
	}
	public String getPoliti(){
		return politiField.getText();
	}
	public String getPolitiResultat(){
		return politiResultatField.getText();
	}
	public Date getPolitiDato(){
		return politiDato.getDate();
	}
	public String getBerging2Navn(){
		return berging2NavnField.getText();
	}
	public String getBergingKontakt2Navn(){
		return bergingKontakt2NavnField.getText();
	}
	public String getBergingKontakt2Tlf(){
		return bergingKontakt2TlfField.getText();
	}
	public String getBerging2Tlf(){
		return berging2TlfField.getText();
	}
	public String getBergingkontakt2Resultat(){
		return bergingKontakt2ResultatField.getText();
	}
	
	public String getBergingNavn(){
		return bergingNavnField.getText();
	}
	public String getBergingKontaktNavn(){
		return bergingKontakt1NavnField.getText();
	}
	public String getBergingKontaktTlf(){
		return bergingKontakt1TlfField.getText();
	}
	public String getBergingTlf(){
		return bergingTlfField.getText();
	}
	public String getBergingkontaktResultat(){
		return bergingKontakt1ResultatField.getText();
	}
	
	public void setVitneNavn(String navn){
		vitnerHvemField.setText(navn);
	}
	public void setVitneDato(Date dato){
		vitnerDato.setDate(dato);
	}
	public void setVitne(boolean value){
		vitnerJa.setSelected(value);
	}
	public void setPoliti(String politi){
		politiField.setText(politi);
	}
	public void setPolitiResultat(String politiResultat){
		politiResultatField.setText(politiResultat);
	}
	public void setPolitiDato(Date dato){
		politiDato.setDate(dato);
	}
	public void setBergingNavn(String navn){
		bergingNavnField.setText(navn);
	}
	public void setBergingKontaktNavn(String navn){
		bergingKontakt1NavnField.setText(navn);
	}
	public void setBergingKontaktTlf(String tlf){
		bergingKontakt1TlfField.setText(tlf);
	}
	public void setBergingKontaktResultat(String resultat){
		bergingKontakt1ResultatField.setText(resultat);
	}
	public void setBergingTlf(String tlf){
		bergingTlfField.setText(tlf);
	}
	
	
	public void setBerging2Navn(String navn){
		berging2NavnField.setText(navn);
	}
	public void setBergingKontakt2Navn(String navn){
		bergingKontakt2NavnField.setText(navn);
	}
	public void setBergingKontakt2Tlf(String tlf){
		bergingKontakt2TlfField.setText(tlf);
	}
	public void setBergingKontakt2Resultat(String resultat){
		bergingKontakt2ResultatField.setText(resultat);
	}
	public void setBerging2Tlf(String tlf){
		berging2TlfField.setText(tlf);
	}
	public void setPolitiJa(boolean value){
		politiJa.setSelected(value);
	}
	public void setPolitiNei(boolean value){
		politiNei.setSelected(value);
	}
	public boolean isValidFields(){
		boolean valid = true;
		if (!validateVitne()){
			valid=false;
		}
		if (!validatePoliti()){
			valid=false;
		}
		if (!validateBergingsselskap1()){
			valid=false;
		}
		if (!validateBergingsselskap2()){
			valid=false;
		}
		return valid;
	}
	public boolean validateBergingsselskap1(){
		String navn = bergingNavnField.getText();
		String kontakt = bergingKontakt1NavnField.getText();
		String tlfBerg = bergingTlfField.getText();
		String tlf = bergingKontakt1TlfField.getText();
		String resultat = bergingKontakt1ResultatField.getText();
		if (navn.equals("")&&kontakt.equals("")&&tlf.equals("")&&resultat.equals("")&&tlfBerg.equals("")){
			return true;
		}
		if (navn.equals("")){//navn kan ikke være "" når vi andre andre data på beringsselskapene
			return false;
		}
		return true;
	}

	
	public boolean validateBergingsselskap2(){
		String navn = berging2NavnField.getText();
		String kontakt = bergingKontakt2NavnField.getText();
		String tlfBerg = berging2TlfField.getText();
		String tlf = bergingKontakt2TlfField.getText();
		String resultat = bergingKontakt2ResultatField.getText();
		if (navn.equals("")&&kontakt.equals("")&&tlf.equals("")&&resultat.equals("")&&tlfBerg.equals("")){
			return true;
		}
		if (navn.equals("")){//navn kan ikke være "" når vi andre andre data på beringsselskapene
			return false;
		}
		return true;
	}
	public boolean validatePoliti(){
		String politi = politiField.getText();
		String resultat = politiResultatField.getText();
		if (StringUtils.isBlank(politi) && StringUtils.isBlank(resultat) && politiDato.getDate()==null){
			return true;
		}
		if (StringUtils.isBlank(politi)){
			return false;//politinavn kan ikke være "" når andre felter på politi er satt
		}
		return true;
	}
	public boolean validateVitne(){
		if (!vitnerJa.isSelected()){
			return true;
		}
		
		String vitneNavn = vitnerHvemField.getText();
		if (vitneNavn.equals("")){//navn kan ikke være "" når vi har en dato
			return false;
		}
		return true;
	}
	
}
