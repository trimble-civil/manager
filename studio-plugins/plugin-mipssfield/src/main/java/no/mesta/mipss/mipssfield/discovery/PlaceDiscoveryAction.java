package no.mesta.mipss.mipssfield.discovery;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import no.mesta.mipss.mipssfield.JRoadDialogue;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kjører opp en dialog hvor man stedfester funnet langs veien
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class PlaceDiscoveryAction extends AbstractAction {
	private static final Logger logger = LoggerFactory.getLogger(PlaceDiscoveryAction.class);
	private final JDiscoveryDetailPanel parent;
	private Punktveiref vei;

	public PlaceDiscoveryAction(final JDiscoveryDetailPanel parent) {
		this.parent = parent;
	}

	/** {@inheritDoc} */
	public void actionPerformed(ActionEvent e) {		
		vei = parent.getDetailObject().getVeiref();
		if (vei == null) {
			vei = new Punktveiref();
			vei.setFraDato(Clock.now());
			if (parent.getDetailObject().getStedfesting() != null) {
				parent.getDetailObject().setVeiref(vei);
			} else {
				logger.error(parent.getDetailObject().getGuid() + " : Cannot add punktveiref");
				return;
			}
		}

		JRoadDialogue dialogue = new JRoadDialogue(parent.getOwner(), null, vei, parent.getMipssField());
		if (!dialogue.isCancelled()) {
			
			vei.setFylkesnummer(dialogue.getFylke());
			vei.setKommunenummer(dialogue.getKommune());
			vei.setVeikategori(dialogue.getVeikat());
			vei.setVeistatus(dialogue.getVeistat());
			vei.setVeinummer(dialogue.getVeinr());
			vei.setHp(dialogue.getHp());
			vei.setMeter(dialogue.getMeter());
			vei.setKjorefelt(dialogue.getFelt());
			
			Punktstedfest sted = parent.getParentPlugin().getMipssField().calculatePunktstedfest(vei);
			vei.getPunktstedfest().setX(sted.getX());
			vei.getPunktstedfest().setY(sted.getY());
			vei.getPunktstedfest().setSnappetX(sted.getSnappetX());
			vei.getPunktstedfest().setSnappetY(sted.getSnappetY());
			vei.getPunktstedfest().setSnappetH(sted.getSnappetH());
			vei.getPunktstedfest().setReflinkPosisjon(sted.getReflinkPosisjon());
			vei.getPunktstedfest().setReflinkIdent(sted.getReflinkIdent());
			
			if(dialogue.isValidated()) {
				parent.redrawMap();
			}
		}
	}
}
