package no.mesta.mipss.mipssfield.incident.r5;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import net.sf.jasperreports.engine.JRException;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.persistence.mipssfield.Skadekontakttype;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.MipssFieldDS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.UIUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
@SuppressWarnings("serial")
public class R5 extends JPanel{
	
	
	private boolean cancelled = true;
	private Forsikringsskade forsikringsskade;
	private boolean isNew=true;
	private List<Skadekontakttype> kontaktList = BeanUtil.lookup(MipssEntityService.BEAN_NAME, MipssEntityService.class).getSkadekontakttypeList();
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private OverordnetPanel overordnetPanel;
	private SkadevolderPanel skadevolderPanel;
	private KontakterPanel kontakterPanel;
	
		
	public R5(Hendelse hendelse){
		//TODO utkommentert pga modellendringer
//		if (hendelse.getForsikringsskade()==null){
//			this.forsikringsskade = new Forsikringsskade();
//			//TODO utkommentert pga modellendring
////			forsikringsskade.setHendelse(hendelse);
//			log.debug("R5 new forsikringsskade:"+forsikringsskade);
//		}else{
//			this.forsikringsskade = hendelse.getForsikringsskade();
//			isNew = false;
//			log.debug("R5 old forsikringsskade:"+forsikringsskade);
//		}
		initGui();
		if (!isNew)
			setForsikringsskade(this.forsikringsskade);
	}
	private SkadeKontakt getBergingsselskap1(){
		String navn = kontakterPanel.getBergingNavn();
		String kontakt = kontakterPanel.getBergingKontaktNavn();
		String tlfBerg = kontakterPanel.getBergingTlf();
		String tlf = kontakterPanel.getBergingKontaktTlf();
		String resultat = kontakterPanel.getBergingkontaktResultat();
		if (navn.equals("")&&kontakt.equals("")&&tlf.equals("")&&resultat.equals("")&&tlfBerg.equals("")){
			if (kontaktBergingsselskap1==null){
				return null;
			} else {
				navn = " ";	
			}
		}
		SkadeKontakt kont = null;
		if (kontaktBergingsselskap1==null){
			Skadekontakttype type = getType(SkadeKontakt.BERGINSSELSKAP);
			kont = new SkadeKontakt();
			kont.setType(type);
		}else
			kont = kontaktBergingsselskap1;
		kont.setNavn(navn);
		kont.setKontaktNavn(kontakt);
		if (!tlf.equals(""))
			kont.setKontaktTlfnummer(Long.valueOf(tlf));
		else{
			kont.setKontaktTlfnummer(null);
		}
		if (!tlfBerg.equals("")){
			kont.setTlfnummer(Long.valueOf(tlfBerg));
		}else{
			kont.setTlfnummer(null);
		}
		kont.setResultat(resultat);
		return kont;
	}
	private SkadeKontakt getBergingsselskap2(){
		String navn = kontakterPanel.getBerging2Navn();
		String kontakt = kontakterPanel.getBergingKontakt2Navn();
		String tlf = kontakterPanel.getBergingKontakt2Tlf();
		String tlfBerg = kontakterPanel.getBerging2Tlf();
		String resultat = kontakterPanel.getBergingkontakt2Resultat();
		if (navn.equals("")&&kontakt.equals("")&&tlf.equals("")){
			if (kontaktBergingsselskap2==null){
				return null;
			} else {
				navn = " ";
			}
		}
		
		SkadeKontakt kont = null;
		if (kontaktBergingsselskap2==null){
			Skadekontakttype type = getType(SkadeKontakt.BERGINSSELSKAP);
			kont = new SkadeKontakt();
			kont.setType(type);
		}else
			kont = kontaktBergingsselskap2;
		kont.setNavn(navn);
		kont.setKontaktNavn(kontakt);
		if (!tlf.equals(""))
			kont.setKontaktTlfnummer(Long.valueOf(tlf));
		else
			kont.setKontaktTlfnummer(null);
		if (!tlfBerg.equals("")){
			kont.setTlfnummer(Long.valueOf(tlfBerg));
		}else{
			kont.setTlfnummer(null);
		}
		kont.setResultat(resultat);
		return kont;
	}
	private SkadeKontakt getEier(){
		String eiernavn = skadevolderPanel.getEierNavn();
		String eierTlf = skadevolderPanel.getTlf1();
		String eierAdresse = skadevolderPanel.getAdresse();
		if (eiernavn.equals("")&&eierTlf.equals("")&&eierAdresse.equals("")){
			if(kontaktEier == null) {
				return null;
			} else {
				eiernavn = " ";
			}
		}
		SkadeKontakt eierKont = null;
		if (kontaktEier==null){
			Skadekontakttype eietype = getType(SkadeKontakt.EIER_AV_KJORETOY);
			eierKont = new SkadeKontakt();
			eierKont.setType(eietype);
		}else
			eierKont = kontaktEier;
		
		eierKont.setAdresse(eierAdresse);
		if (!eierTlf.equals(""))
			eierKont.setTlfnummer(Long.valueOf(eierTlf));
		eierKont.setNavn(eiernavn);
		
		return eierKont;
	}
	private SkadeKontakt getForer(){
		String forerNavn = skadevolderPanel.getForerNavn();
		String forerTlf = skadevolderPanel.getTlf2();
		
		if (forerNavn.equals("")&&forerTlf.equals("")){
			if(kontaktForer==null) {
				return null;
			} else {
				forerNavn = " ";
			}
		}
		SkadeKontakt forerKont= null;
		if (kontaktForer==null){
			Skadekontakttype forertype = getType(SkadeKontakt.FORER_AV_KJORETOY);
			forerKont = new SkadeKontakt();
			forerKont.setType(forertype);
		}else{
			forerKont = kontaktForer;
		}
		
		forerKont.setNavn(forerNavn);
		if (!forerTlf.equals(""))
			forerKont.setTlfnummer(Long.valueOf(forerTlf));
		
		return forerKont;
	}
	private SkadeKontakt getForsikringsselskap(){
		String fselsk= skadevolderPanel.getForsikringsselskap();
		if (fselsk.equals("")){
			if(kontaktForsikringsselskap == null) {
				return null;
			} else {
				fselsk = " ";
			}
		}
		SkadeKontakt felskKont = null;
		if (kontaktForsikringsselskap==null){
			Skadekontakttype fselsktype = getType(SkadeKontakt.FORSIKRINGSELSKAP);
			felskKont = new SkadeKontakt();
			felskKont.setType(fselsktype);
		}else
			felskKont = kontaktForsikringsselskap;
		felskKont.setNavn(fselsk);
		
		return felskKont;
	}
	public List<SkadeKontakt> getKontakter(Forsikringsskade skade, String type){
		List<SkadeKontakt> kontakter = new ArrayList<SkadeKontakt>();
		for (SkadeKontakt k:skade.getSkadeKontakter()){
			if (k.getType().getNavn().equals(type)){
				kontakter.add(k);
			}
		}
		return kontakter;
	}
	private SkadeKontakt getPoliti(){
		String politi = kontakterPanel.getPoliti();
		String resultat = kontakterPanel.getPolitiResultat();
		Date date = kontakterPanel.getPolitiDato();
		if (politi.equals("")&&resultat.equals("")&&date==null){
			if (kontaktPoliti==null){
				return null;
			} else {
				politi = " ";
			}
		}
		SkadeKontakt politikontakt = null;
		if (kontaktPoliti==null){
			Skadekontakttype polititype = getType(SkadeKontakt.POLITI);
			politikontakt = new SkadeKontakt();
			politikontakt.setType(polititype);
		}else
			politikontakt = kontaktPoliti;
		politikontakt.setNavn(politi);
		politikontakt.setDato(date);
		politikontakt.setResultat(resultat);
		return politikontakt;
	}
	
	public Forsikringsskade getResults(){
//		if (overordnetPanel.isAntattCheckSelected()){
//			forsikringsskade.setSkadeAntattDato(forsikringsskade.getSkadeDato());
//			forsikringsskade.setSkadeDato(null);
//			System.out.println("antatt checked"+forsikringsskade.getSkadeAntattDato());
//		}else{
//			if (forsikringsskade.getSkadeAntattDato()!=null){
//				forsikringsskade.setSkadeDato(forsikringsskade.getSkadeAntattDato());
//				System.out.println("antatt not:"+forsikringsskade.getSkadeDato());
//				forsikringsskade.setSkadeAntattDato(null);
//			}
//			
//		}
		
//		if (kontaktEier==null){
//			SkadeKontakt eier = getEier();
//			if(eier != null) {
//				forsikringsskade.getSkadeKontakter().add(eier);
//			}
//		}
		SkadeKontakt eier = getEier();
		if(eier != null && kontaktEier==null) {
			forsikringsskade.getSkadeKontakter().add(eier);
		}
		
//		if (kontaktForsikringsselskap==null){
//			SkadeKontakt forsikringsselskap = getForsikringsselskap();
//			if(forsikringsselskap != null) {
//				forsikringsskade.getSkadeKontakter().add(forsikringsselskap);
//			}
//		}
		SkadeKontakt forsikringsselskap = getForsikringsselskap();
		if(forsikringsselskap != null && kontaktForsikringsselskap == null) {
			forsikringsskade.getSkadeKontakter().add(forsikringsselskap);
		}
		
//		if (kontaktForer==null){
//			SkadeKontakt forer = getForer();
//			if(forer != null) {
//				forsikringsskade.getSkadeKontakter().add(forer);
//			}
//		}
		SkadeKontakt forer = getForer();
		if(forer != null && kontaktForer==null) {
			forsikringsskade.getSkadeKontakter().add(forer);
		}
		
//		if (kontaktPoliti==null){
//			SkadeKontakt politi = getPoliti();
//			if(politi != null) {
//				forsikringsskade.getSkadeKontakter().add(politi);
//			}
//		}
		SkadeKontakt politi = getPoliti();
		if(politi != null && kontaktPoliti==null) {
			forsikringsskade.getSkadeKontakter().add(politi);
		}
		
//		if (kontaktBergingsselskap1==null){
//			SkadeKontakt berging1 = getBergingsselskap1();
//			if(berging1 != null) {
//				forsikringsskade.getSkadeKontakter().add(berging1);
//			}
//		}
		SkadeKontakt berging1 = getBergingsselskap1();
		if(berging1 != null && kontaktBergingsselskap1==null) {
			forsikringsskade.getSkadeKontakter().add(berging1);
		}
		
//		if (kontaktBergingsselskap2==null){
//			SkadeKontakt berging2 = getBergingsselskap2();
//			if(berging2 != null) {
//				forsikringsskade.getSkadeKontakter().add(berging2);
//			}
//		}
		SkadeKontakt berging2 = getBergingsselskap2();
		if(berging2 != null && kontaktBergingsselskap2==null) {
			forsikringsskade.getSkadeKontakter().add(berging2);
		}
		
		if (kontakterPanel.isVitnerJaSelected()){
//			if (kontaktVitne==null){
//				SkadeKontakt vitne = getVitne();
//				if(vitne != null) {
//					forsikringsskade.getSkadeKontakter().add(vitne);
//				}
//			}
			SkadeKontakt vitne = getVitne();
			if(vitne != null && kontaktVitne==null) {
				forsikringsskade.getSkadeKontakter().add(vitne);
			}
		}else{
			if (kontaktVitne!=null){
				forsikringsskade.getSkadeKontakter().remove(kontaktVitne);
			}
		}
		
		if (kontakterPanel.isPolitiJaSelected())
			forsikringsskade.setAnmeldt(Boolean.TRUE);
		else
			forsikringsskade.setAnmeldt(Boolean.FALSE);
		return forsikringsskade;
	}
	
	
	public Forsikringsskade getTempResults(){
		Forsikringsskade f = new Forsikringsskade();
		//TODO utkommentert pga modellendring
//		f.setHendelse(forsikringsskade.getHendelse());
//		
//		if (overordnetPanel.isAntattCheckSelected()){
//			f.setSkadeAntattDato(forsikringsskade.getSkadeDato());
//		}else{
//			f.setSkadeDato(forsikringsskade.getSkadeDato());
//		}
		SkadeKontakt eier = getEier();
		SkadeKontakt forsikringsselskap = getForsikringsselskap();
		SkadeKontakt forer = getForer();
		SkadeKontakt politi = getPoliti();
		SkadeKontakt berging1 = getBergingsselskap1();
		SkadeKontakt berging2 = getBergingsselskap2();
		if (eier!=null)
			f.getSkadeKontakter().add(eier);
		if (forsikringsselskap!=null)
			f.getSkadeKontakter().add(forsikringsselskap);
		if (forer!=null)
			f.getSkadeKontakter().add(forer);
		if (kontakterPanel.isVitnerJaSelected()){
			f.getSkadeKontakter().add(getVitne());
		}
		if (politi!=null)
			f.getSkadeKontakter().add(politi);
		if (berging1!=null)
			f.getSkadeKontakter().add(berging1);
		if (berging2!=null)
			f.getSkadeKontakter().add(berging2);
		
		if (kontakterPanel.isPolitiJaSelected()){
			f.setAnmeldt(Boolean.TRUE);
		}else{
			f.setAnmeldt(Boolean.FALSE);
		}
		
		f.setMeldtAv(forsikringsskade.getMeldtAv());
		f.setEntrepenorNavn(forsikringsskade.getEntrepenorNavn());
		f.setFritekst(forsikringsskade.getFritekst());
		
		f.setEntrepenorDato(forsikringsskade.getEntrepenorDato());
		f.setMeldtDato(forsikringsskade.getMeldtDato());
		f.setReparertDato(forsikringsskade.getReparertDato());				
		
		f.setEstimatKostnad(forsikringsskade.getEstimatKostnad());		
		
		f.setUtsattRepKommentar(forsikringsskade.getUtsattRepKommentar());
		f.setSkadevolderBilmerke(forsikringsskade.getSkadevolderBilmerke());
		f.setSkadevolderHenger(forsikringsskade.getSkadevolderHenger());
		f.setSkadevolderRegnr(forsikringsskade.getSkadevolderRegnr());
		f.setTilleggsopplysninger(forsikringsskade.getTilleggsopplysninger());
		return f;
	}
	
	private Skadekontakttype getType(String typeNavn){
		for (Skadekontakttype sk:kontaktList){
			if (sk.getNavn().equals(typeNavn)){
				return sk;
			}
		}
		return null;
	}
	
	private SkadeKontakt getVitne(){
		String vitneNavn = kontakterPanel.getVitneNavn();
		Date date = kontakterPanel.getVitneDato();
		
		SkadeKontakt v=null;
		if (kontaktVitne==null){
			Skadekontakttype vitnetype = getType(SkadeKontakt.VITNE);
			v = new SkadeKontakt();
			v.setType(vitnetype);
		}else
			v = kontaktVitne;
		v.setNavn(vitneNavn);
		v.setDato(date);
		
		return v;
	}
	private void initGui() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		GridBagConstraints c = new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,20), 0,0);
		overordnetPanel = new OverordnetPanel(forsikringsskade, c);
		skadevolderPanel = new SkadevolderPanel(forsikringsskade, c, this);
		kontakterPanel = new KontakterPanel(forsikringsskade, c, this);
		
		JPanel actionButtons = UIUtils.getHorizPanel();
		actionButtons.add(Box.createHorizontalGlue());
		actionButtons.add(new JButton(getPreviewAction(GUITexts.getText(GUITexts.PREVIEW), null)));
		actionButtons.add(UIUtils.getHStrut(15));
		actionButtons.add(new JButton(new OkAction("Lagre", null)));
		actionButtons.add(UIUtils.getHStrut(5));
		actionButtons.add(new JButton(new AvbrytAction("Avbryt", null)));
		
		JTabbedPane tab = new JTabbedPane();
		tab.addTab("Overordnet", overordnetPanel);
		tab.addTab("Skadevolder", skadevolderPanel);
		tab.addTab("Kontakter og resultater", kontakterPanel);
		
		overordnetPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 20, 0));
		skadevolderPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 20, 0));
		kontakterPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 20, 0));
		
		JLabel tittelLabel = new JLabel(GUITexts.getText(GUITexts.R5_TITTEL));
		Font f = tittelLabel.getFont();
		tittelLabel.setFont(new Font(f.getName(), Font.BOLD, 11));
		
		JPanel tittelPanel = UIUtils.getHorizPanel();
		tittelPanel.add(UIUtils.getHStrut(5));
		tittelPanel.add(tittelLabel);
		tittelPanel.add(Box.createHorizontalGlue());
		
		add(UIUtils.getVStrut(10));
		add(tittelPanel);
		add(UIUtils.getVStrut(10));
		add(tab);
		add(UIUtils.getVStrut(5));
		add(actionButtons);
	}
	
	
	
	
	
	public boolean isCancelled(){
		return cancelled;
	}
	
	public void runWizard(){
		PageOverordnet o = new PageOverordnet(overordnetPanel);
		PageSkadevolder s = new PageSkadevolder(skadevolderPanel);
		PageKontakter k = new PageKontakter(kontakterPanel);
		
		WizardPage[] pages = {o,s,k};
		Wizard wizard = WizardPage.createWizard(pages);
		UIManager.put("wizard.sidebar.image", ImageUtil.convertImageToBufferedImage(IconResources.WIZARD_ICON.getImage()));
		new WizardDialog(null, wizard).startWizard();
	}
	
	
	private SkadeKontakt kontaktForsikringsselskap;
	private SkadeKontakt kontaktEier;
	private SkadeKontakt kontaktForer;
	private SkadeKontakt kontaktVitne;
	private SkadeKontakt kontaktPoliti;
	@SuppressWarnings("unused")
	private SkadeKontakt kontaktLensmann;
	private SkadeKontakt kontaktBergingsselskap1;
	private SkadeKontakt kontaktBergingsselskap2;
	
	/**
 	 * Setter de feltene som ikke lar seg binde. 
	 *
 	 */
	private void setForsikringsskade(Forsikringsskade skade){
		List<SkadeKontakt> fs = getKontakter(skade, SkadeKontakt.FORSIKRINGSELSKAP);
		if (!fs.isEmpty()){
			skadevolderPanel.setForsikringsselskap(fs.get(0).getNavn());
			kontaktForsikringsselskap = fs.get(0);//initKontakter.add(fs.get(0));
		}
//		if (skade.getSkadeAntattDato()!=null){
//			forsikringsskade.setSkadeDato(skade.getSkadeAntattDato());
//			System.out.println("setter skadedato:"+forsikringsskade.getSkadeDato());
//			forsikringsskade.setSkadeAntattDato(null);
//			overordnetPanel.setAntattCheck(true);
//		}
		fs = getKontakter(skade, SkadeKontakt.EIER_AV_KJORETOY);
		if (!fs.isEmpty()){
			skadevolderPanel.setEierNavn(fs.get(0).getNavn());
			skadevolderPanel.setTlf1(""+fs.get(0).getTlfnummer());
			skadevolderPanel.setAdresse(fs.get(0).getAdresse());
			kontaktEier = fs.get(0);//initKontakter.add(fs.get(0));
		}
		fs = getKontakter(skade, SkadeKontakt.FORER_AV_KJORETOY);
		if (!fs.isEmpty()){
			skadevolderPanel.setForerNavn(fs.get(0).getNavn());
			skadevolderPanel.setTlf2(fs.get(0).getTlfnummer()+"");
			kontaktForer = fs.get(0);//initKontakter.add(fs.get(0));
		}
		fs = getKontakter(skade, SkadeKontakt.VITNE);
		if (!fs.isEmpty()){
			kontakterPanel.setVitneNavn(fs.get(0).getNavn());
			kontakterPanel.setVitneDato(fs.get(0).getDato());
			kontakterPanel.setVitne(true);
			kontaktVitne = fs.get(0);//initKontakter.add(fs.get(0));
		}
		fs = getKontakter(skade, SkadeKontakt.POLITI);
		if (!fs.isEmpty()){
			String politiText = fs.get(0).getNavn();
			if (fs.get(0).getKontaktNavn()!=null)
				politiText+= " "+fs.get(0).getKontaktNavn();
			if (fs.get(0).getTlfnummer()!=null)
				politiText+= " "+fs.get(0).getTlfnummer();
			
			kontakterPanel.setPoliti(politiText);
			kontakterPanel.setPolitiResultat(fs.get(0).getResultat());
			kontakterPanel.setPolitiDato(fs.get(0).getDato());
			kontaktPoliti=fs.get(0);//initKontakter.add(fs.get(0));
		}else{
			fs = getKontakter(skade, SkadeKontakt.LENSMANN);
			if (!fs.isEmpty()){
				String politiText = fs.get(0).getNavn();
				if (fs.get(0).getKontaktNavn()!=null)
					politiText+= " "+fs.get(0).getKontaktNavn();
				if (fs.get(0).getTlfnummer()!=null)
					politiText+= " "+fs.get(0).getTlfnummer();
				
				kontakterPanel.setPoliti(politiText);
				kontakterPanel.setPolitiResultat(fs.get(0).getResultat());
				kontakterPanel.setPolitiDato(fs.get(0).getDato());
				kontaktLensmann=fs.get(0);//initKontakter.add(fs.get(0));
			}
		}
		
		fs = getKontakter(skade, SkadeKontakt.BERGINSSELSKAP);
		if (!fs.isEmpty()){
			kontakterPanel.setBergingNavn(fs.get(0).getNavn());
			kontakterPanel.setBergingKontaktNavn(fs.get(0).getKontaktNavn());
			kontakterPanel.setBergingKontaktTlf(fs.get(0).getKontaktTlfnummer()+"");
			kontakterPanel.setBergingKontaktResultat(fs.get(0).getResultat());
			kontakterPanel.setBergingTlf(fs.get(0).getTlfnummer()+"");
			kontaktBergingsselskap1 = fs.get(0);//initKontakter.add(fs.get(0));
			if (fs.size()>1){
				kontakterPanel.setBerging2Navn(fs.get(1).getNavn());
				kontakterPanel.setBergingKontakt2Navn(fs.get(1).getKontaktNavn());
				kontakterPanel.setBergingKontakt2Tlf(fs.get(1).getKontaktTlfnummer()+"");
				kontakterPanel.setBerging2Tlf(fs.get(1).getTlfnummer()+"");
				kontakterPanel.setBergingKontakt2Resultat(fs.get(1).getResultat());
				kontaktBergingsselskap2 = fs.get(1);//initKontakter.add(fs.get(1));
			}
		}
		if (skade.getAnmeldt())
			kontakterPanel.setPolitiJa(true);
		else
			kontakterPanel.setPolitiNei(true);
	}
	
	
	
	
	
	
	/**
	 * Validerer at alle feltene er ihht r5 modellen
	 */
	public boolean validateForm(){
		boolean valid = true;
		if (!skadevolderPanel.isValidFields())
			valid = false;
		if (!kontakterPanel.isValidFields())
			valid = false;
		if (!overordnetPanel.isValidFields()){
			valid = false;
		}
		return valid;
	}
	
	
	
	private void showError(String error){
		repaint();
		JOptionPane.showMessageDialog(getParent(), error, "Feil i skjema", JOptionPane.ERROR_MESSAGE);
	}

	class AvbrytAction extends AbstractAction{

		public AvbrytAction(String txt, ImageIcon icon){
			super(txt, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			cancelled = true;
			SwingUtilities.windowForComponent(R5.this).dispose();
		}
	}
	class OkAction extends AbstractAction{

		public OkAction(String txt, ImageIcon icon){
			super(txt, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			cancelled = false;
			if (validateForm()){
				SwingUtilities.windowForComponent(R5.this).dispose();
			}else
				showError(GUITexts.getText(GUITexts.MISSING_FIELD));
		}
	}
	
	static class PageKontakter extends WizardPage{
		public static String getDescription(){
			return "Kontakter";
		}
		public PageKontakter(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	static class PageOverordnet extends WizardPage{
		public static String getDescription(){
			return "Overordnet";
		}
		public PageOverordnet(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	static class PageSkadevolder extends WizardPage{
		public static String getDescription(){
			return "Skadevolder";
		}
		public PageSkadevolder(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	static class PageVisning extends WizardPage{
		public static String getDescription(){
			return "Forhåndsvisning";
		}
		public PageVisning(JPanel content) {
			setLayout(new BorderLayout());
			add(content);
		}
	}
	public AbstractAction getPreviewAction(String txt, ImageIcon icon){
		return new AbstractAction(txt, icon){
			@Override
			public void actionPerformed(ActionEvent e){
				PreviewDS dataSource = new PreviewDS(getTempResults());
				JReportBuilder builder = new JReportBuilder(Report.R5_FORM, Report.R5_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF,new JFrame());
				File report = builder.getGeneratedReportFile();
				if (report != null) {
					DesktopHelper.openFile(report);
				}
			}
		};
	}
	class PreviewDS extends MipssFieldDS<Forsikringsskade>{

		int c = 0;
		public PreviewDS(Forsikringsskade f) {
			super(Forsikringsskade.class, new ArrayList<String>());
			setFieldBean(BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class));
			setEntity(f);
		}
		protected void loadPageData(){
		}
		public boolean next() throws JRException{
			if (c==0){
				c++;
				return true;	
			}else
			return false;
		
		
		}
	}
	private class WizardDialog extends JDialog{
		private final Wizard wizard;
		public WizardDialog(JDialog parent, Wizard wizard){
			super(parent, true);
			this.wizard = wizard;
		}
		public void startWizard(){
			GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0);
			this.setLayout(new GridBagLayout());
		    //Map gatheredSettings = (Map) WizardDisplayer.showWizard(wiz);
			WizardDisplayer.installInContainer(WizardDialog.this, gbc, wizard, null, null, new WizardResultReceiver(){
				@SuppressWarnings("unchecked")
				@Override
				public void cancelled(Map arg0) {
					cancelled = true;
					dispose();
				}
				@Override
				public void finished(Object arg0) {
					if (validateForm()){
						cancelled=false;
						dispose();
					}else{
						showError(GUITexts.getText(GUITexts.MISSING_FIELD));
					}
				}			
			});
			setSize(new Dimension(775, 550));
			setLocationRelativeTo(null);
			setVisible(true);
		}
	}
	
}
