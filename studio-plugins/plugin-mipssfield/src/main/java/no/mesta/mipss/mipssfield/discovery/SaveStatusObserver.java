package no.mesta.mipss.mipssfield.discovery;

import no.mesta.mipss.mipssfield.SaveStateAdapter;
import no.mesta.mipss.persistence.mipssfield.Avvik;

/**
 * Avgjør om lagreknappen skal være på eller av.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class SaveStatusObserver extends SaveStateAdapter<Avvik>  {

	public SaveStatusObserver() {
	}
	

	public boolean isEnabled() {
		return isChanged() && !isErrors();
	}


	public void setDetail(Avvik punktreg) {
		detail = punktreg;		
	}
}