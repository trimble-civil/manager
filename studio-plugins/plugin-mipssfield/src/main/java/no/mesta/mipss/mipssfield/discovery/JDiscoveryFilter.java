package no.mesta.mipss.mipssfield.discovery;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.PunktregQueryFilter;
import no.mesta.mipss.mipssfield.PunktregStatusFilter;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JPunktregstatusTypePanel;
import no.mesta.mipss.ui.JTilstandPanel;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.process.JProcessPanel;

import org.jdesktop.beansbinding.Binding;

/**
 * Søkefilter panelet for funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDiscoveryFilter extends JPanel implements MipssBeanLoaderListener {
	private static final int HSTRUT = 5;
	private static final int VSTRUT = 5;
	private final PunktregQueryFilter filter;
	private JButton filterButton;
	private JButton hentMedIdButton;
	private JStatusFilterPanel otherFiltersList;
	private MipssFieldModule parentPlugin;
	private JProcessPanel prosessPanel;
	private JTilstandPanel tilstandPanel;
	private JPunktregstatusTypePanel statusPanel;
	private JLabel noRowsHolder = new JLabel("0");

	/**
	 * Konstruktør
	 * 
	 */
	public JDiscoveryFilter(final MipssFieldModule parentPlugin) {
		super();
		this.parentPlugin = parentPlugin;
		filter = parentPlugin.getDiscoveryFilter();
		parentPlugin.getDicoveryTableModel().addTableLoaderListener(this);

		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), GUITexts
				.getText(GUITexts.FILTER_TITLE)));

		filterButton = new JButton(GUITexts.getText(GUITexts.SEARCH), IconResources.SEARCH_ICON);
		filterButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filterButton.setEnabled(false);
				updateFilter();
				
				
				JDiscoveryFilter.this.parentPlugin.getDicoveryTableModel().setBeanMethodArgValues(
						new Object[] { filter });
				JDiscoveryFilter.this.parentPlugin.getDicoveryTableModel().loadData();
			}
		});
		
		hentMedIdButton = new JButton(GUITexts.getText(GUITexts.GET_DISCORVERY_W_ID), IconResources.DETALJER_SMALL_ICON);
		hentMedIdButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				parentPlugin.openWithDialog(MipssFieldModule.IHF_TYPE.FUNN);
			}
		});
		Binding<PunktregQueryFilter, Boolean, JButton, Boolean> enableFilterButton = BindingHelper.createbinding(
				filter, "${kontraktId != null}", filterButton, "enabled");
		enableFilterButton.bind();
		JCheckBox kunslettede = new JCheckBox(GUITexts.getText(GUITexts.ONLY_DELETED), false);
		Binding<JCheckBox, Boolean, PunktregQueryFilter, Boolean> sb = BindingHelper.createbinding(kunslettede,
				"selected", filter, "kunSlettede");
		sb.bind();
		
		JCheckBox fristOverskredet = new JCheckBox(GUITexts.getText(GUITexts.DUE_DATE_PASSED), false);
		sb = BindingHelper.createbinding(fristOverskredet, "selected", filter, "fristdatoOverskredet");
		sb.bind();
		
		prosessPanel = new JProcessPanel(filter, "kontraktId", true);
		tilstandPanel = new JTilstandPanel();
		
		statusPanel = new JPunktregstatusTypePanel();
		ComponentSizeResources.setComponentSize(tilstandPanel, new Dimension(200, 200));
		
		ComponentSizeResources.setComponentSize(statusPanel, new Dimension(200, 200));
		
		Binding<JPunktregstatusTypePanel, Integer, PunktregQueryFilter, Integer> hb = BindingHelper.createbinding(
				statusPanel, "${!historicSearch}", filter, "sisteStatus");
		hb.bind();
		otherFiltersList = new JStatusFilterPanel(PunktregStatusFilter.values());

		
		Box rader = BoxUtil.createHorizontalBox(0, new JLabel("Rader: "), noRowsHolder);
	
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(rader, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,5,2,0), 0,0));
		buttonPanel.add(filterButton, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,5,2,0), 0,0));
		buttonPanel.add(hentMedIdButton,new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,10,2,5), 0,0));
		
		setLayout(new GridBagLayout());
		add(prosessPanel,  new GridBagConstraints(0,0, 1,3, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(tilstandPanel, new GridBagConstraints(1,0, 1,3, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0,0,0,0), 0,0));
		
		add(statusPanel,   new GridBagConstraints(2,0, 1,3, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0,0,0,0), 0,0));
		
		add(otherFiltersList,   new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15,0,0,0), 0,0));
		add(kunslettede,   		new GridBagConstraints(3,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(fristOverskredet,   new GridBagConstraints(3,2, 1,1, 0.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
		add(buttonPanel,   		new GridBagConstraints(0,3, 5,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		parentPlugin = null;
	}
	/** {@inheritDoc} */
	public void loadingFinished(MipssBeanLoaderEvent event) {
		filterButton.setEnabled(true);
		int rows = parentPlugin.getDicoveryTableModel().getRowCount();
		noRowsHolder.setText(String.valueOf(rows));
	}

	/** {@inheritDoc} */
	public void loadingStarted(MipssBeanLoaderEvent event) {
		// knappen disables i actionlytteren
	}

	private void updateFilter() {
		filter.setProsessId(prosessPanel.getAlleValgteProsesser());
		filter.setUnderprosessId(prosessPanel.getAlleValgteUnderprosesser());
		filter.setTilstander(tilstandPanel.getAlleValgte());
		filter.setStatuser(statusPanel.getAlleValgte());
		filter.setAndreFiltre(otherFiltersList.getSelection());
		filter.setIngenProsesser(prosessPanel.isIngenValgt());
		filter.setIngenTilstander(tilstandPanel.isIngenValgt());
		filter.setVeiInfo(parentPlugin.getCommonFilterPanel().getVeiInfo());
		filter.setIngenDfuer(parentPlugin.getCommonFilterPanel().isIngenDfuerValgt());
	}
}