/**
 * 
 */
package no.mesta.mipss.mipssfield.discovery;

import java.util.Date;

import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.vo.PunktregResult;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Lukker et punkt
 * 
 * Kalles fra GUI
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ClosePunktregProcess {
	private static final Logger logger = LoggerFactory.getLogger(ClosePunktregProcess.class);
	private final JDiscoveryDetailPanel parent;

	public ClosePunktregProcess(final JDiscoveryDetailPanel parent) {
		this.parent = parent;
	}

	public void execute(String text, Date date) {
		boolean closePunkt = true;
		if (parent.getFieldToolkit().isFieldsChanged()) {
			MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.WARNING_ICON, GUITexts
					.getText(GUITexts.DISCOVERY_CLOSEWARNING_TITLE), GUITexts
					.getText(GUITexts.DISCOVERY_CLOSEWARNING_TEXT), MipssDialogue.Type.WARNING,
					new MipssDialogue.Button[] { MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL });
			dialogue.askUser();
			if (dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
				closePunkt = false;
			}
		}

		Avvik punkt = parent.getDetailObject();
		PunktregSok view = parent.getViewObject();
		
		if (closePunkt) {
			PunktregResult result = parent.getParentPlugin().lukkFunn(punkt, text, date);
			view.merge(result.getView());
			punkt.merge(result.getPunktreg());
			parent.reset();
			parent.getFieldToolkit().setEnableFields(false);
			logger.debug("Toolkit changed fields status: " + parent.getFieldToolkit().isFieldsChanged());
			logger.debug("Punktreg status is: " + punkt.getStatus());
		}
	}
}
