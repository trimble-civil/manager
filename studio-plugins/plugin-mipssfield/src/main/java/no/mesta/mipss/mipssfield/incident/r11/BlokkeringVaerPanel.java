package no.mesta.mipss.mipssfield.incident.r11;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.VaerFraVaerstasjon;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

@SuppressWarnings("serial")
public class BlokkeringVaerPanel extends JPanel {
	private Font bold;
	private R11Controller controller;
	private JMipssDatePicker stengtPicker;
	private JSpinner stengtSpinner;
	private JMipssDatePicker apnetPicker;
	private JSpinner apnetSpinner;
	
	public BlokkeringVaerPanel(R11Controller controller){
		this.controller = controller;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(createBlokkeringPanel(), 	new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
		add(createBildekommentarPanel(), new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
	}
	private JPanel createBlokkeringPanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		
		JLabel blokkertLabel = new JLabel(GUITexts.getText(GUITexts.R11_BLOKKERING));
		JLabel stengingLabel = new JLabel(GUITexts.getText(GUITexts.R11_STENGING));
		JLabel vaerLabel = new JLabel(GUITexts.getText(GUITexts.R11_VAER));

		JLabel stengtLabel = new JLabel(GUITexts.getText(GUITexts.R11_STENGT));
		JLabel apnetLabel = new JLabel(GUITexts.getText(GUITexts.R11_APNET));
		JLabel kl1 = new JLabel(GUITexts.getText(GUITexts.R11_KL));
		JLabel kl2 = new JLabel(GUITexts.getText(GUITexts.R11_KL));
		JLabel gjelderVeiLabel = new JLabel(GUITexts.getText(GUITexts.R11_GJELDERVEI));
		
		JLabel regnLabel = new JLabel(GUITexts.getText(GUITexts.R11_REGN));
		JLabel vatsnoLabel = new JLabel(GUITexts.getText(GUITexts.R11_VATSNO));
		JLabel snoLabel = new JLabel(GUITexts.getText(GUITexts.R11_SNO));
		JLabel vindretningLabel = new JLabel(GUITexts.getText(GUITexts.R11_VINDRETNING));
		JLabel vindstyrkeLabel = new JLabel(GUITexts.getText(GUITexts.R11_VINDSTYRKE));
		JLabel temperaturLabel = new JLabel(GUITexts.getText(GUITexts.R11_TEMPERATUR));
		JLabel nedborSisteLabel = new JLabel(GUITexts.getText(GUITexts.R11_NEDBORSISTE3));
		
		JLabel regnMmDg = new JLabel(GUITexts.getText(GUITexts.R11_MMDG));
		JLabel vatsnoMmDg = new JLabel(GUITexts.getText(GUITexts.R11_MMDG));
		JLabel snoMmDg = new JLabel(GUITexts.getText(GUITexts.R11_MMDG));
		JLabel grader = new JLabel(GUITexts.getText(GUITexts.R11_GRADER));
		JLabel vindMs = new JLabel(GUITexts.getText(GUITexts.R11_MS));
		JLabel nedbSiste = new JLabel(GUITexts.getText(GUITexts.R11_MM3DG));
		
		JCheckBox gsCheck = new JCheckBox(GUITexts.getText(GUITexts.R11_GS));
		
		JComboBox veiCombo = new JComboBox();
		JComboBox stengingCombo = new JComboBox();
		JComboBox vindretningCombo = createVindretningCombo();
		
		JTextField regnField = new JTextField();
		JTextField vatsnoField = new JTextField();
		JTextField snoField = new JTextField();
		JTextField vindstyrkeField = new JTextField();
		JTextField temperaturField = new JTextField();
		JTextField nedborSiste3Field = new JTextField();		
		
		createStengtDato();
		createApnetDato(stengtPicker);
		
        
        bold = blokkertLabel.getFont();
		bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
        blokkertLabel.setFont(bold);
		stengingLabel.setFont(bold);
		vaerLabel.setFont(bold);
		gjelderVeiLabel.setFont(bold);
        Dimension size = stengtLabel.getSize();
        size.width=37;
        size.height=20;
        stengtLabel.setPreferredSize(size);
        apnetLabel.setPreferredSize(size);
        
        Egenskap vei = controller.findEgenskap(R11Panel.BLOKKERTVEI_ID);
		Egenskap stenging = controller.findEgenskap(R11Panel.STENGING_ID);
		
		
		MipssComboBoxModel<Egenskapverdi> veiModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(vei));
		MipssComboBoxModel<Egenskapverdi> stengingModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(stenging));

		veiCombo.setModel(veiModel);
		stengingCombo.setModel(stengingModel);
		
		EgenskapItemListener veiListener = new EgenskapItemListener(vei, controller.getSkred());
		EgenskapItemListener stengingListener = new EgenskapItemListener(stenging, controller.getSkred());
		
		veiCombo.addItemListener(veiListener);
		stengingCombo.addItemListener(stengingListener);
		
		veiCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(vei));
		stengingCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(stenging));
		
		if (controller.getOriginalSkred()==null){
			stengingCombo.setSelectedIndex(0);
			veiCombo.setSelectedIndex(0);
		}
		
        VaerFraVaerstasjon vaerFraVaerstasjon = controller.getSkred().getVaerFraVaerstasjon();
		BindingHelper.createbinding(controller.getSkred(), "gjelderGS", gsCheck, "selected", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "regnMmPrDag", regnField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "sluddMmPrDag", vatsnoField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "snoMmPrDag", snoField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "vindstyrkeMPrSek", vindstyrkeField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "vindretning", vindretningCombo, "selectedItem", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "temperatur", temperaturField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(vaerFraVaerstasjon, "mmNedborSiste3Dogn", nedborSiste3Field, "text", BindingHelper.READ_WRITE).bind();

		
        JPanel stengtPanel = new JPanel(new GridBagLayout());
        stengtPanel.add(stengtLabel, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        stengtPanel.add(stengtPicker, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        stengtPanel.add(kl1, 			new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        stengtPanel.add(stengtSpinner, 	new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        
        JPanel apnetPanel = new JPanel(new GridBagLayout());
        apnetPanel.add(apnetLabel, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        apnetPanel.add(apnetPicker, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        apnetPanel.add(kl2, 		new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        apnetPanel.add(apnetSpinner, 	new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
        
		panel.add(blokkertLabel, 	new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(stengingLabel, 	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vaerLabel, 		new GridBagConstraints(2,0, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(gsCheck, 			new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		panel.add(gjelderVeiLabel, 	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(veiCombo, 		new GridBagConstraints(0,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		panel.add(stengingCombo, 	new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		panel.add(stengtPanel, 		new GridBagConstraints(1,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		panel.add(apnetPanel, 		new GridBagConstraints(1,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		panel.add(regnLabel, 		new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(vatsnoLabel, 		new GridBagConstraints(2,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(snoLabel, 		new GridBagConstraints(2,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(vindretningLabel, new GridBagConstraints(2,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(vindstyrkeLabel, 	new GridBagConstraints(2,5, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(temperaturLabel, 	new GridBagConstraints(2,6, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(nedborSisteLabel, new GridBagConstraints(2,7, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(regnField, 		new GridBagConstraints(3,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vatsnoField, 		new GridBagConstraints(3,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(snoField, 		new GridBagConstraints(3,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vindretningCombo, new GridBagConstraints(3,4, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vindstyrkeField, 	new GridBagConstraints(3,5, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(temperaturField, 	new GridBagConstraints(3,6, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(nedborSiste3Field,new GridBagConstraints(3,7, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(regnMmDg, 		new GridBagConstraints(4,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vatsnoMmDg, 		new GridBagConstraints(4,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(snoMmDg, 			new GridBagConstraints(4,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vindMs, 			new GridBagConstraints(4,5, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(grader, 			new GridBagConstraints(4,6, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(nedbSiste,		new GridBagConstraints(4,7, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));

		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_BLOKKERING_TITLE)));
		return panel;
	}
	private void createStengtDato(){
		stengtPicker = new JMipssDatePicker();
		
		Date midnight = stengtPicker.getDate();
		Calendar c = Calendar.getInstance();
		c.setTime(midnight);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		Dimension spinnerSize = new Dimension(55, 22);
		stengtSpinner = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        stengtSpinner.setPreferredSize(spinnerSize);
        stengtSpinner.setMinimumSize(spinnerSize);
        stengtSpinner.setMaximumSize(spinnerSize);
        stengtSpinner.setEditor(new JSpinner.DateEditor(stengtSpinner, "HH:mm"));
		
        MipssDateTimePickerHolder stengtDato = new MipssDateTimePickerHolder(stengtPicker, stengtSpinner);
        BindingHelper.createbinding(controller.getSkred(), "stengtDato", stengtDato, "date", BindingHelper.READ_WRITE).bind();
	}
	
	private void createApnetDato(JMipssDatePicker stengtPicker){
		apnetPicker = new JMipssDatePicker();
		stengtPicker.pairWith(apnetPicker);
		
		Calendar c = Calendar.getInstance();
		c.setTime(Clock.now());
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		if (stengtPicker.getDate()!=null){
			c.setTime(stengtPicker.getDate());
		}
		
		Dimension spinnerSize = new Dimension(55, 22);
		apnetSpinner = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        apnetSpinner.setPreferredSize(spinnerSize);
        apnetSpinner.setMinimumSize(spinnerSize);
        apnetSpinner.setMaximumSize(spinnerSize);
        apnetSpinner.setEditor(new JSpinner.DateEditor(apnetSpinner, "HH:mm"));
        MipssDateTimePickerHolder apnetDato = new MipssDateTimePickerHolder(apnetPicker, apnetSpinner);
        BindingHelper.createbinding(controller.getSkred(), "aapnetDato", apnetDato, "date", BindingHelper.READ_WRITE).bind();
	}
	
	private JPanel createBildekommentarPanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		JLabel kommentarLabel = new JLabel(GUITexts.getText(GUITexts.R11_KOMMENTARER));
		kommentarLabel.setFont(bold);
		JCheckBox bilderCheck = new JCheckBox(GUITexts.getText(GUITexts.R11_LEGGVEDBILDER));
		JTextArea kommentarArea = new JTextArea();
		kommentarArea.setWrapStyleWord(true);
		kommentarArea.setLineWrap(true);
		Dimension size = kommentarArea.getPreferredSize();
		size.height=50;
		kommentarArea.setPreferredSize(size);
		JScrollPane kommentarScroll = new JScrollPane(kommentarArea);
		
		BindingHelper.createbinding(controller.getSkred(), "kommentar", kommentarArea, "text", BindingHelper.READ_WRITE).bind();

		panel.add(bilderCheck, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(kommentarLabel, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0,0));
		panel.add(kommentarScroll, 	new GridBagConstraints(0,2, 3,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,5), 0,0));

		
		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_BILDEKOMMENTAR_TITLE)));
		return panel;
	}
	
	private JComboBox createVindretningCombo(){
		JComboBox vindretningCombo = new JComboBox();
		String east = GUITexts.getText(GUITexts.R11_EAST);
		vindretningCombo.addItem("N");
		vindretningCombo.addItem("N"+east);
		vindretningCombo.addItem(east);
		vindretningCombo.addItem("S"+east);
		vindretningCombo.addItem("S");
		vindretningCombo.addItem("SV");
		vindretningCombo.addItem("V");
		vindretningCombo.addItem("NV");
		return vindretningCombo;
	}
}
