package no.mesta.mipss.mipssfield.incident;

import no.mesta.mipss.mipssfield.OpenTabMessage;

/**
 * Melding for å åpne hendelse faneark.
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class OpenIncidentMessage extends OpenTabMessage {
    public OpenIncidentMessage() {
    }
}
