package no.mesta.mipss.mipssfield;

import java.util.List;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;

public class InspeksjonrapportParams {

	private boolean ev;
	private boolean fv;
	private boolean rv;
	private boolean kv;
	
	private boolean kunMedFunn;
	
	private List<Veinettveireferanse> veiList;
	private Driftkontrakt driftkontrakt;
	
	public boolean isEv() {
		return ev;
	}
	public void setEv(boolean ev) {
		this.ev = ev;
	}
	public boolean isFv() {
		return fv;
	}
	public void setFv(boolean fv) {
		this.fv = fv;
	}
	public boolean isRv() {
		return rv;
	}
	public void setRv(boolean rv) {
		this.rv = rv;
	}
	public boolean isKv() {
		return kv;
	}
	public void setKv(boolean kv) {
		this.kv = kv;
	}
	public boolean isKunMedFunn() {
		return kunMedFunn;
	}
	public void setKunMedFunn(boolean kunMedFunn) {
		this.kunMedFunn = kunMedFunn;
	}
	public List<Veinettveireferanse> getVeiList() {
		return veiList;
	}
	public void setVeiList(List<Veinettveireferanse> veiList) {
		this.veiList = veiList;
	}
	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}
	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		this.driftkontrakt = driftkontrakt;
	}
	
	
}
