package no.mesta.mipss.mipssfield;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

/**
 * Gui tekster for MipssField plugin
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class GUITexts {
	public static final String ACTIONS = "actions";
    public static final String ADD = "add";
    public static final String ADDED_AND_SAVED = "addedAndSaved";
    public static final String ADD_PICTURE = "addPicture";
    public static final String ALREADY_EXISTS = "alreadyExists";
    public static final String ASSESSMENT = "assessment";
    public static final String ATTATCH = "attatch";
    public static final String BACKLOG = "backlog";
    public static final String CANCEL = "cancel";
    public static final String CANNOT_ADD_DUE_TO_ERRORS = "cannotAddDueToErrors";
    public static final String CANNOT_CONTINUE_DUE_TO_ERRORS = "cannotContinueDueToErrors";
    public static final String CAUSE = "cause";
    public static final String CLOSE_DISCOVERY = "closeDiscovery";
    public static final String CLOSE_WINDOW = "closeWindow";
    public static final String COMMENT = "comment";
    public static final String CONDITION = "condition";
    public static final String CONTRACT = "contract";
    public static final String COPY = "copy";
    public static final String COUNTY = "county";
    public static final String CREATED = "created";
    public static final String DATE = "date";
    public static final String DEADLINE = "deadline";
    public static final String DELETE = "delete";
    public static final String DELETE_DISCOVERY_TITLE = "deleteDiscoveryTitle";
    public static final String DELETE_DISCOVERY_TXT = "deleteDiscoveryTxt";
    public static final String DELETE_INCIDENTS_TITLE = "deleteIncidentsTitle";
    public static final String DELETE_INCIDENTS_TXT = "deleteIncidentsTxt";
    public static final String DELETE_INSPECTIONS_TITLE = "deleteInspectionsTitle";
    public static final String DELETE_INSPECTIONS_TXT = "deleteInspectionsTxt";
    public static final String DETAILS = "details";
    public static final String DETAIL_REPORT = "detailReport";
    public static final String DISCOVERIES = "discoveries";
    public static final String DISCOVERY_CLOSEWARNING_TEXT = "discovery.closewarning.text";
    public static final String DISCOVERY_CLOSEWARNING_TITLE = "discovery.closewarning.title";
    public static final String DISCOVERY_DETAIL_REPORT = "discoveryDetailReport";
    public static final String DISCOVERY_OVERVIEW_REPORT = "discoveryOverviewReport";
    public static final String DURATION = "duration";
    public static final String EDIT = "edit";
    public static final String EMPTY_REPORT = "emptyReport";
    public static final String ESTIMATE = "estimate";
    public static final String ESTIMATE_TITLE = "estimateTitle";
    public static final String ESTIMATES = "estimates";
    public static final String EXCEL = "excel";
    public static final String EXECUTED = "executed";
    public static final String EXTRAWORK = "extraWork";
    public static final String FAILED_OPEN_DISCOVERY = "failedOpenDiscovery";
    public static final String FAILED_OPEN_INCIDENT = "failedOpenIncident";
    public static final String FAILED_OPEN_INSPECTION = "failedOpenInspection";
    public static final String FAILED_OPEN_ITEM = "failedOpenItem";
    public static final String FIELD = "field";
    public static final String FILTER_TITLE = "filterTitle";
    public static final String FROM = "from";
    public static final String FYLKE = "fylke";
    public static final String HISTORY = "history";
    public static final String HP = "hp";
    public static final String ILLEGAL_ROAD = "illegalRoad";
    public static final String HP_REPORT = "hpReport";
    public static final String INCIDENT = "incident";
    public static final String INCIDENT_DATE = "incidentDate";
    public static final String INCIDENT_PLACE = "incidentPlace";
    public static final String INCIDENTS = "incidents";
    public static final String INFORMATION = "information";
    public static final String INSPECTED = "inspected";
    public static final String INSPECTED_PROCESSES = "inspectedProcesses";
    public static final String INSPECTION = "inspection";
    public static final String INSPECTION_OVERVIEW_REPORT = "inspectionOverviewReport";
    public static final String INSPECTION_REPORT = "inspectionReport";
    public static final String INSPECTION_REPORT_FETCHINGDATA = "inspectionReportFethingData";
    public static final String INSPECTION_ROAD_CATEGORIES= "inspectionRoadCategories";
    public static final String INSPECTIONS = "inspections";
    public static final String INSURANCE_CLAIM = "insuranceClaim";
    public static final String SKRED = "skred";
    public static final String ID = "id";
    public static final String KM_ON = "kmOn";
    public static final String KOMMUNE = "kommune";
    public static final String LOADING_FAILED = "loadingFailed";
    public static final String MAP = "map";
    public static final String MESSAGE_FROM = "messageFrom";
    public static final String METER = "meter";
    public static final String MISSING_LOCATION = "missing.location";
    public static final String MISSING_POSITION = "missing.position";
    public static final String NEW = "new";
    public static final String NEW_DISCOVERY = "newDiscovery";
    public static final String NO = "no";
    public static final String NO_ESTIMATE_TYPE = "noEstimateType";
    public static final String NO_ESTIMATE_VALUE = "noEstimateValue";
    public static final String NO_HP = "noHp";
    public static final String NO_OF_DISCOVERIES_TITLE = "noOfDiscoveriesTitle";
    public static final String NO_OF_INCIDENTS_TITLE = "noOfIcidentsTitle";
    public static final String NOT_IMPLEMENTED = "notImplemented";
    public static final String OK = "ok";
    public static final String ONLY_DELETED = "onlyDeleted";
    public static final String DUE_DATE_PASSED = "dueDatePassed";
    public static final String OPEN = "open";
	public static final String OPEN_MANY_INCIDENTS = "openManyIncidents";
	public static final String OPEN_MANY_INSPECTIONS = "openManyInspections";
	public static final String OPEN_MANY_REPORT_DISCOVERIES = "openManyReportDiscoveries";
	public static final String OPEN_MANY_REPORT_INCIDENTS = "openManyReportIncidents";
	public static final String OPEN_MANY_REPORT_INSPECTIONS = "openManyReportInspections";
	public static final String OVERVIEW = "overview";
	public static final String POSITION = "position";
	public static final String PRINT = "print";
	public static final String PRINT_FAILED = "printFailed";
	public static final String PROCESS = "process";
	public static final String PROCESSES = "processes";
	private static PropertyResourceBundleUtil props;
	public static final String MISSING_FIELD="missingField";
	public static final String PUNKTREGSTATUS_NEW = "no.mesta.mipss.persistence.mipssfield.PunktregStatus.new";
	public static final String R2_FORM = "r2Form";
	public static final String R5_FORM = "r5Form";
	public static final String R11_FORM = "r11Form";
	public static final String RECEIVED_BY = "receivedBy";
	public static final String RECEIVED_DATE = "receivedDate";
	public static final String REGISTRERT = "registrert";
	public static final String REMOVED_AND_SAVED = "removedAndSaved";
	public static final String REPORT_FAILED_TXT = "reportFailedTxt";
	public static final String REPORTS = "reports";
	public static final String RESET = "reset";
	public static final String RESTORE = "restore";
	public static final String RESTORE_DISCOVERY_TITLE = "restoreDiscoveryTitle";
	public static final String RESTORE_DISCOVERY_TXT = "restoreDiscoveryTxt";
	public static final String RESTORE_INCIDENTS_TITLE = "restoreIncidentsTitle";
	public static final String RESTORE_INCIDENTS_TXT = "restoreIncidentsTxt";
	public static final String RESTORE_INSPECTIONS_TITLE = "restoreInspectionsTitle";
	public static final String RESTORE_INSPECTIONS_TXT = "restoreInspectionsTxt";
	public static final String ROAD = "road";
	public static final String ROAD_CATEGORY = "roadCategory";
	public static final String ROAD_DIALOGUE_TITLE = "roadDialogueTitle";
	public static final String ROAD_NO = "roadNo";
	public static final String ROAD_STATUS = "roadStatus";
	public static final String ROAD_TYPE = "roadType";
	public static final String SAVE = "save";
	public static final String SAVE_OR_ABORT_BEFORE_ADD = "saveOrAbortBeforeAdd";
	public static final String SAVE_OR_ABORT_BEFORE_CONTINUE = "saveOrAbortBeforeContinue";
	public static final String SAVING_FAILED = "savingFailed";
	public static final String SEARCH = "search";
	public static final String SEARCH_PDA = "searchPDA";
	public static final String SEARCH_ROAD = "searchRoad";
	public static final String SELECT_INCIDENT = "selectIncident";
	public static final String SELECT_PUNKTREG = "selectPunktreg";
	public static final String SEND_ELRAPP="sendElrapp";
	public static final String SORRY_NOT_IMPLEMENTED = "sorryNotImplemented";
	public static final String STATUS = "status";
	public static final String STREATCHES = "streatches";
	public static final String TEMPERATURE = "temperature";
	public static final String TIME_AND_PLACE = "timeAndPlace";
	public static final String TO = "to";
	public static final String TOTAL = "total";
	public static final String TRAFFICSAFETY = "trafficSafety";
	public static final String TYPE = "type";
	public static final String UNATTACTH = "unattatch";
	public static final String UNDER_PROCESS = "underProcess";
	public static final String UNSAVED_CHANGES_TITLE = "unsavedChangesTitle";
	public static final String UNSAVED_CHANGES_TXT = "unsavedChangesTxt";
	public static final String WARNING = "warning";
	public static final String WEATHER = "weather";
	public static final String WIND = "wind";
	public static final String WRONG_ESTIMATE_VALUE = "wrongEstimateValue";
	public static final String YES = "yes";
	
	
	public static final String SKADE_MELD= "skadeMeld";
	public static final String MELD_AV= "meldAv";
	public static final String SKADE_DATO= "skadeDato";
	public static final String OPPDAGET_DATO= "oppdagetDato";
	public static final String HVEM= "hvem";
	public static final String ANMELDT_POLITI= "anmeldtPoliti";
	public static final String SKADE_OMFANG= "skadeOmfang";
	public static final String KOSTNAD= "kostnad";
	public static final String REPARERT= "reparert";
	public static final String LANG_TID= "langTid";
	public static final String KONTAKTER = "kontakter";
	public static final String REGNR = "regnr";
	public static final String HENGER = "henger";
	public static final String MERKE = "merke";
	public static final String INFO = "info";
	public static final String INFO_SKADE = "infoSkade";
	public static final String KONTAKT_TYPE = "kontaktType";
	public static final String FIRMA = "firma";
	public static final String NAVN = "navn";
	public static final String ADRESSE = "adresse";
	public static final String POST_NR_STED = "postnrSted";
	public static final String TELEFON = "telefon";
	public static final String E_POST = "ePost";
	public static final String RESULTAT_INFO = "resultatInfo";
	public static final String PERSON = "person";
	public static final String RESULTAT = "resultat";
	public static final String OPPRETT_R5 = "opprettR5";
	public static final String VIS_R5 = "visR5";
	public static final String OPPRETT_R11 = "opprettR11";
	public static final String VIS_R11 = "visR11";
	public static final String SKADESTED="skadested";
	public static final String VEGNR="vegnr";
	public static final String KM="km";
	public static final String ANTATT_DATO="antattDato";
	public static final String SKADEVOLDER="skadevolder";
	public static final String FORSIKRINGSELSK="forsikringsselsk";
	public static final String NAVN_EIER="navnEier";
	public static final String TELEFONNR="telefonnr";
	public static final String ADRESSE_EIER="adresseEier";
	public static final String NAVN_FORER="navnForer";
	public static final String SNOBROYT="snobroyt";
	public static final String SKADEVOLDER_KJENT="skadevolderKjent";
	public static final String SKADEVOLDER_KJENT_INFO="skadevolderKjentInfo";
	
	public static final String VITNER ="vitner";
	public static final String VITNER_HVEM ="vitnerHvem";
	public static final String POLITI ="politi";
	public static final String POLITI_RESULTAT ="resultatPoliti";
	public static final String BERGINGSELSK ="bergingselsk";
	public static final String NAVN_BERGING ="navnBerging";
	public static final String TLF_BERGING="tlfBerging";
	public static final String KONTAKTPERSON ="kontaktPerson";
	public static final String KONTAKTPERSON_TLF ="kontaktPersonTlf";
	public static final String FIRE1 ="fire1";
	public static final String FIRE2 ="fire2";
	public static final String RESULTAT_BERGING ="resultatBerging";
	public static final String ANMELD_POLITI ="anmeldtPoliti";
	public static final String TILLEGG ="tillegg";
	public static final String KOPI_ANMELD="kopiAnmeld";
	public static final String PREVIEW="preview";
	public static final String WARNING_SAVE_INSPECTION="ikkeAvsluttet";
	public static final String TOOLTIP_OVERVIEWPDF = "tooltip_overviewpdf";
	
	public static final String GET_DISCORVERY_W_ID="hentPunktregMedId";
	public static final String GET_INCIDENT_W_ID="hentHendelseMedId";
	public static final String GET_INSPECTION_W_ID="hentInspeksjonMedId";
	
	public static final String ELRAPP_EMNE = "elrappEmne";
	
	public static final String R11_EMNE="r11.emne";
	public static final String R11_TITTEL="r11.tittel";
	public static final String R11_FK="r11.fk";
	public static final String R11_NAVN="r11.navn";
	public static final String R11_REG_AV="r11.regAv";
	public static final String R11_HEADER_TITLE="r11.headerTitle";
	public static final String R11_EMNE_TITLE="r11.emneTitle";
	
	public static final String R5_TITTEL="r5.tittel";
	public static final String R5_SKADEVOLDERWARNING="r5.skadevolderwarning";
	public static final String R5_SKADEVOLDERWARNING_TITLE="r5.skadevolderwarning.title";
	public static final String R11_STED_TITLE = "r11.stedTitle";
	public static final String R11_SKREDDATO="r11.skreddato";
	public static final String R11_STEDSNAVN = "r11.stedsnavn";
	public static final String R11_VEI = "r11.vei";
	public static final String R11_FRAHP = "r11.fraHp";
	public static final String R11_TILHP = "r11.tilHp";
	public static final String R11_M = "r11.m";
	public static final String R11_FELT = "r11.felt";
	public static final String R11_STENGTRAS = "r11.stengtRas";
	public static final String R11_KOORDINAT_FRA = "r11.koordinatFra";
	public static final String R11_KOORDINAT_TIL = "r11.koordinatTil";
	public static final String R11_EAST= "r11.east";
	public static final String R11_SKRED_TITLE = "r11.skredTitle";
	public static final String R11_TYPESKRED = "r11.typeSkred";
	public static final String R11_BESKRIVELSE = "r11.beskrivelse";
	public static final String R11_VOLUM = "r11.volum";
	public static final String R11_VOLUMVEI = "r11.volumVeg";
	public static final String R11_VOLUMTOT = "r11.volumTot";
	public static final String R11_VOLUMM2 = "r11.volumM2";
	public static final String R11_VEIINFO_TITLE = "r11.veiinfoTitle";
	public static final String R11_HOYDE = "r11.hoyde";
	public static final String R11_SKADE = "r11.skade";
	public static final String R11_VAERFORHOLD= "r11.vaerforhold";
	public static final String R11_REGN = "r11.regn";
	public static final String R11_SNO = "r11.sno";
	public static final String R11_VINDRETNING = "r11.vindretning";
	public static final String R11_TEMPERATUR = "r11.temperatur";
	public static final String R11_GRADER = "r11.grader";
	public static final String R11_NEDBOR = "r11.nedbor";
	public static final String R11_BLOKKERING_TITLE = "r11.blokkeringTitle";
	public static final String R11_BILDEKOMMENTAR_TITLE = "r11.bildekommentarTitle";
	public static final String R11_BLOKKERING = "r11.blokkering";
	public static final String R11_STENGING = "r11.stenging";
	public static final String R11_VAER = "r11.vaer";
	public static final String R11_STENGT = "r11.stengt";
	public static final String R11_APNET = "r11.apnet";
	public static final String R11_KL = "r11.kl";
	public static final String R11_GS = "r11.gs";
	public static final String R11_GJELDERVEI = "r11.gjelderVei";
	public static final String R11_VATSNO = "r11.vatSno";
	public static final String R11_VINDSTYRKE = "r11.vindstyrke";
	public static final String R11_NEDBORSISTE3 = "r11.nedborSiste3";
	public static final String R11_MMDG = "r11.mmdg";
	public static final String R11_MS = "r11.ms";
	public static final String R11_MM3DG = "r11.mm3dg";
	public static final String R11_PREVIEW_RAPPORT="r11.previewRapport";
	
	public static final String R11_LEGGVEDBILDER = "r11.leggVedBilder";
	public static final String R11_KOMMENTARER = "r11.kommentarer";
	public static final String ELRAPP = "elrapp";
	public static final String HENTER_DATA="henterData";
	public static final String ELRAPP_IKKESENDT = "elrappstatus.ikkeSendt.tooltip";
	public static final String ELRAPP_ENDRET = "elrappstatus.endret.tooltip";
	public static final String ELRAPP_OK = "elrappstatus.ok.tooltip";
	public static final String KAN_IKKE_KOBLE_HENDELSE = "kanIkkekobleHendelse";
	public static final String KAN_IKKE_KOBLE_HENDELSE_TITLE = "kanIkkekobleHendelse.title";
	
	public static final String SEND_ELRAPP_MESSAGE = "sendElrappMessage";
	public static final String SEND_ELRAPP_MESSAGE_OK = "sendElrappMessageOk";
	public static final String SEND_ELRAPP_MESSAGE_OK_TITLE = "sendElrappMessageOk.title";
	public static final String ELRAPP_GRENSE_DATO_WARNING = "elrappGrenseDatoWarning";
	public static final String ELRAPP_GRENSE_DATO_WARNING_TITLE = "elrappGrenseDatoWarning.title";
	public static final String COPY_IMAGES_HENDELSE = "copyImagesHendelse";
	public static final String COPY_IMAGES_HENDELSE_TITLE = "copyImagesHendelse.title";
	public static final String COPY_IMAGES_FUNN = "copyImagesFunn";
	public static final String COPY_IMAGES_FUNN_TITLE = "copyImagesFunn.title";
	public static final String INFO_STATUSDATO = "infoStatusdato";
	
    /**
     * Fyller ut poperties
     * 
     * @return
     */
    private static PropertyResourceBundleUtil getProps() {
        if(props == null) {
            props = PropertyResourceBundleUtil.getPropertyBundle("mipssFieldText");
        }
        
        return props;
    }
    
    /**
     * Henter ut GUI teksten
     * 
     * @param key
     * @return
     */
    public static String getText(String key) {
        return getProps().getGuiString(key);
    }
    
    /**
     * Henter GUI teksten, og fletter inn objekter
     * 
     * @param key
     * @param values
     * @return
     */
    public static String getText(String key, Object... values) {
        return getProps().getGuiString(key, values);
    }
}
