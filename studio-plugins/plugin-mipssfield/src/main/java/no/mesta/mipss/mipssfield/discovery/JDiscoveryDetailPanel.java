package no.mesta.mipss.mipssfield.discovery;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractDetailPanel;
import no.mesta.mipss.mipssfield.JDetailFrame;
import no.mesta.mipss.mipssfield.JMapPanel;
import no.mesta.mipss.mipssfield.JTopLine;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.vo.PunktregResult;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BorderResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JLoadingPanel;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.process.JProcessPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Detalj gui for et funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class JDiscoveryDetailPanel extends JAbstractDetailPanel<PunktregSok, Avvik> implements
		MipssBeanLoaderListener {
	private static final Logger logger = LoggerFactory.getLogger(JDiscoveryDetailPanel.class);
	private static final int STRUT = 2;
	private final JDetailActions actionsPanel;
	private boolean isBound;
	private final JMapPanel mapPanel;
	private final MipssPunktregPictureModel model;
	private boolean modelLoaded;
	private boolean newPunktreg;
	private final MipssFieldModule parentPlugin;
	private final JDiscoveryPicturePanel picturePanel;
	private Avvik punktreg;
	private boolean punktregLoaded;
	private final PunktregSok view;
	private JProcessPanel processPanel;

	/**
	 * Konstruktør
	 * 
	 * @param parentPlugin
	 * @param owner
	 * @param view
	 * @param punktreg
	 */
	public JDiscoveryDetailPanel(final MipssFieldModule parentPlugin, final JDetailFrame owner, final PunktregSok view, final AbstractAction saveAction) {
		super(view, owner, parentPlugin, saveAction);
		setSaveStateObserver(new SaveStatusObserver());
		this.parentPlugin = parentPlugin;
		logger.debug("Showing " + punktreg);
		
		if (view == null) {
			punktreg = parentPlugin.newPunktregInstance();
			this.view = PunktregSok.newInstance(punktreg);
			newPunktreg = true;
			
		} else {
			this.view = view;
		}
		
		model = new MipssPunktregPictureModel();
		model.setBeanInstance(getMipssField());
		model.setBeanInterface(MipssField.class);
		model.setBeanMethod("hentPunktregBildeInfo");
		model.setBeanMethodArguments(new Class<?>[] { String.class });
		model.setBeanMethodArgValues(new Object[] { this.view.getPunktregGuid() });
		model.addMipssBeanLoaderListener(this);
		model.loadBilder();

		JPanel prosessPanel = createProsessPanel();
		JLoadingPanel timeAndPlace = createTimeAndPlacePanel();
		JLoadingPanel assessmentPanel = createAssessmentPanel();
		mapPanel = createMapPanel();
		JLoadingPanel commentPanel = createCommentPanel();
		picturePanel = createPicturePanel();
		JLoadingPanel historyPanel = createHistoryPanel();

		actionsPanel = new JDetailActions(this);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(new JTopLine(parentPlugin.getDiscoveryColor()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, timeAndPlace, BoxUtil.createHorizontalStrut(STRUT), prosessPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, commentPanel, BoxUtil.createHorizontalStrut(STRUT), assessmentPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(0, picturePanel, BoxUtil.createHorizontalStrut(STRUT), mapPanel));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(actionsPanel);
	}
	
	private void bind() {
		if (punktregLoaded && modelLoaded && !isBound) {
			isBound = true;
			punktreg.setBilder(model.getPunktregBilder());
			getFieldToolkit().registerCangeListener(Avvik.class, punktreg, "bilder");
			for (AvvikBilde pb : model.getPunktregBilder()) {
				getFieldToolkit().registerCangeListener(AvvikBilde.class, pb, "statusType");
				getFieldToolkit().registerCangeListener(AvvikBilde.class, pb, "${bilde.beskrivelse}");
			}
			getFieldToolkit().bind();
		}
	}
	private JProcessPanel createProsessPanel(){
		processPanel = new JProcessPanel();
		processPanel.setSingleSelectionMode(true);
		processPanel.setKunGyldigForArbeid(true);
		processPanel.setKontraktId(view.getKontraktId());
		if (view.getUnderprosessId()==null)
			processPanel.setSelectedProsessId(view.getProsessId());
		else
			processPanel.setSelectedProsessId(view.getUnderprosessId());
		
		ComponentSizeResources.setComponentSizes(processPanel, JTimeAndPlace.MIN_SIZE, JTimeAndPlace.PREF_SIZE, JTimeAndPlace.MAX_SIZE);
		return processPanel;
	}
	private JLoadingPanel createAssessmentPanel() {
		JLoadingPanel panel = new JLoadingPanel(JAssessmentPanel.class, new Class<?>[] { JDiscoveryDetailPanel.class, Avvik.class });
		panel.setArgument(JDiscoveryDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JAssessmentPanel.MIN_SIZE, JAssessmentPanel.PREF_SIZE, JAssessmentPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.ASSESSMENT)));
		return panel;
	}
	
	private JLoadingPanel createCommentPanel() {
		JLoadingPanel panel = new JLoadingPanel(JCommentPanel.class, new Class<?>[] { MipssFieldModule.class,
				FieldToolkit.class, Avvik.class });
		panel.setArgument(MipssFieldModule.class, parentPlugin);
		panel.setArgument(FieldToolkit.class, getFieldToolkit());
		ComponentSizeResources.setComponentSizes(panel, JCommentPanel.MIN_SIZE, JCommentPanel.PREF_SIZE, JCommentPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.COMMENT)));
		return panel;
	}

	/** {@inheritDoc} */
	@Override
	public AbstractAction createDefaultSaveAction() {
		return new SaveAction(GUITexts.getText(GUITexts.SAVE), IconResources.SAVE_ICON);
	}

	private JLoadingPanel createHistoryPanel() {
		JLoadingPanel panel = new JLoadingPanel(JStatusHistoryPanel.class, new Class<?>[] { JDiscoveryDetailPanel.class, Avvik.class });
		panel.setArgument(JDiscoveryDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JStatusHistoryPanel.MIN_SIZE, JStatusHistoryPanel.PREF_SIZE, JStatusHistoryPanel.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.HISTORY)));
		return panel;
	}

	private JMapPanel createMapPanel() {
		JMapPanel panel = new JMapPanel(parentPlugin, this);
		panel.setBorder(BorderResources.createComponentBorder());
		ComponentSizeResources.setComponentSizes(panel, JMapPanel.MIN_SIZE, JMapPanel.PREF_SIZE, JMapPanel.MAX_SIZE);
		return panel;
	}

	private JDiscoveryPicturePanel createPicturePanel() {
		JDiscoveryPicturePanel picturePanel = new JDiscoveryPicturePanel(this, model);
		picturePanel.setBorder(BorderResources.createComponentBorder());
		return picturePanel;
	}

	private JLoadingPanel createTimeAndPlacePanel() {
		JLoadingPanel panel = new JLoadingPanel(JTimeAndPlace.class, new Class<?>[] { JDiscoveryDetailPanel.class,Avvik.class });
		panel.setArgument(JDiscoveryDetailPanel.class, this);
		ComponentSizeResources.setComponentSizes(panel, JTimeAndPlace.MIN_SIZE, JTimeAndPlace.PREF_SIZE, JTimeAndPlace.MAX_SIZE);
		loadingPanels.add(panel);
		panel.setBorder(BorderResources.createComponentTitleBorder(GUITexts.getText(GUITexts.TIME_AND_PLACE)));
		return panel;
	}

	/** {@inheritDoc} */
	@Override
	public void dispose() {
		super.dispose();

		punktreg = null;
	}

	/** {@inheritDoc} */
	@Override
	public Avvik getDetailObject() {
		return punktreg;
	}

	/** {@inheritDoc} */
	@Override
	public String getTabTitle() {
		return view.getTextForGUI();
	}

	/** {@inheritDoc} */
	@Override
	public Integer getType() {
		return DISCOVERY_TYPE;
	}

	/**
	 * View objektet som vises i detaljvinduet
	 * 
	 * @return
	 */
	@Override
	public PunktregSok getViewObject() {
		return view;
	}

	/** {@inheritDoc} */
	@Override
	public String getWindowTitle() {
		return view.getTextForGUI();
	}

	public boolean isNewPunktreg() {
		return newPunktreg;
	}
	private boolean loaded;
	/** {@inheritDoc} */
	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		logger.debug("loadingFinished()");
		if(loaded)
			return;
		modelLoaded = true;
		if (!newPunktreg) {
			SwingWorker<Void, Void> loadPunktreg = new SwingWorker<Void, Void>() {
				/** {@inheritDoc} */
				@Override
				protected Void doInBackground() throws Exception {
					Avvik funn = parentPlugin.getMipssField().hentPunktreg(view.getPunktregGuid());
					setPunktreg(funn);
					return null;
				}
			};
			loadPunktreg.execute();
		} else {
			setPunktreg(punktreg);
		}
		loaded=true;
	}

	/** {@inheritDoc} */
	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		modelLoaded = false;
	}
	
	public void redrawMap() {
		mapPanel.setPunktreg(punktreg);
	}

	/**
	 * Resetter GUI
	 * 
	 */
	public void reset() {
		getFieldToolkit().reset();
	}

	private void setPunktreg(Avvik punktreg) {
		getSaveStateObserver().setDetail(punktreg);
		logger.debug("setPunktreg()");
		this.punktreg = punktreg;
		punktregLoaded = true;

		mapPanel.setPunktreg(punktreg);
		Binding binding = processPanel.getBindingToModel(punktreg, "prosess");
		getFieldToolkit().addBinding(binding);
		getFieldToolkit().registerCangeListener(Avvik.class, punktreg, "prosess");
		
		for (JLoadingPanel panel : loadingPanels) {
			panel.setArgument(Avvik.class, punktreg);
		}
		picturePanel.setPunktreg(punktreg);

		actionsPanel.ready();
		waitAndBindWhenReady();
		
	}
	public void reloadPicturePanel(){
		for (AvvikBilde pb:punktreg.getBilder()){
			model.addPunktregBilde(pb);
		}
		picturePanel.reload();
	}
	@Override
	protected void doneLoading(){
		if (!isBound) {
			bind();
		}
		getFieldToolkit().setEnableFields(true);
		getOwner().repaint();
        if(!parentPlugin.hasWriteAccess()) {
        	getFieldToolkit().setEnableFields(false);
        }
	}
	/**
	 * Lagrer endringer
	 * 
	 */
	private class SaveAction extends AbstractAction {

		/**
		 * Konstruktør
		 * 
		 * @param name
		 * @param icon
		 */
		public SaveAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		public void actionPerformed(ActionEvent e) {
			getParentPlugin().enforceWriteAccess();
			try {
				Avvik punkt = getDetailObject();
				PunktregSok view = getViewObject();
				logger.debug("lagre " + punkt);
				PunktregResult result = getParentPlugin().getMipssField().oppdaterPunktreg(punkt);
				view.merge(result.getView());
				logger.debug("merged " + view);
				punkt.merge(result.getPunktreg());
				logger.debug("merged " + punkt);
				reset();
				
				if(isNewPunktreg() && !getParentPlugin().getDicoveryTableModel().contains(view)) {
					getParentPlugin().getDiscoveryTable().clearSelection();
					getParentPlugin().getDicoveryTableModel().addItem(view);
				}
			} catch (Throwable t) {
				getParentPlugin().getLoader().handleException(t, getParentPlugin(), GUITexts.getText(GUITexts.SAVING_FAILED));
			}
		}
	}
}
