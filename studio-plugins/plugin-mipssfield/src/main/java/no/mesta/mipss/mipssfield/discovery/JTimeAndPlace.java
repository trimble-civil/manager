package no.mesta.mipss.mipssfield.discovery;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.PunktveirefValidator;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.JAbstractInfoPanel;
import no.mesta.mipss.mipssfield.LineBox;
import no.mesta.mipss.mipssfield.incident.JIncidentDetailPanel;
import no.mesta.mipss.mipssfield.incident.JIncidentLocatorDialog;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseSokView;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.InspeksjonSokView;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.Binding;

/**
 * Panel for å vise tid og sted for et funn
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JTimeAndPlace extends JAbstractInfoPanel implements PropertyChangeListener {
	
	private static final Dimension BUTTON_SIZE = new Dimension(37, 22);
	private static final Dimension FIELD_BIG_PREF_SIZE = new Dimension(450 + 37, 22);
	private static final Dimension FIELD_BIGGER_PREF_SIZE = new Dimension(450 + (37 * 2), 22);
	private static final Dimension FIELD_MAX_SIZE = new Dimension(Short.MAX_VALUE, 22);
	private static final Dimension FIELD_MIN_SIZE = new Dimension(220, 22);
	private static final Dimension FIELD_PREF_SIZE = new Dimension(450, 22);
	private static final String HENDELSE = "hendelse";
	private static final String HENDELSE_TEXT_FOR_GUI = "${hendelse.textForGUI}";
	private static final Logger logger = LoggerFactory.getLogger(JTimeAndPlace.class);
	public static final Dimension MAX_SIZE = new Dimension(Short.MAX_VALUE, 169);
	public static final Dimension MIN_SIZE = new Dimension(100, 169);
	public static final Dimension PREF_SIZE = new Dimension(450, 169);
	private static final Integer STRUT = 2;
	private final JTextField hendelseField;
	private final JDiscoveryDetailPanel parent;
	private final Avvik punktreg;
	private final LineBox<Punktveiref> veiBox;
	private final JButton veiEdit = new JButton();
	private final JTextField veiField;

	public JTimeAndPlace(final JDiscoveryDetailPanel parent, final Avvik punktreg) {
		logger.debug("setting up kontraktField");
		this.punktreg = punktreg;
		this.parent = parent;

		setFieldToolkit(parent.getFieldToolkit());

		JTextField kontraktField = toolkit.createTextInputField(Avvik.class, punktreg, "${kontrakt.textForGUI}", null);
		kontraktField.setEditable(false);
		kontraktField.setEnabled(false);
		ComponentSizeResources.setComponentSizes(kontraktField, FIELD_MIN_SIZE, FIELD_BIGGER_PREF_SIZE, FIELD_MAX_SIZE);
		
		JTextField refnummerField = toolkit.createTextInputField(Avvik.class, punktreg, "refnummer", null);
		ComponentSizeResources.setComponentSizes(refnummerField, new Dimension(100,22), new Dimension(100, 22), new Dimension(100, 22));
		refnummerField.setEditable(false);
		refnummerField.setEnabled(false);
		
		logger.debug("setting up opprettetField");
		JTextField opprettetField = toolkit.createTextInputField(Avvik.class, punktreg, "textForGUI", null);
		opprettetField.setEditable(false);
		ComponentSizeResources.setComponentSizes(opprettetField, FIELD_MIN_SIZE, FIELD_BIGGER_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up veiField");
		veiField = toolkit.createTextInputField(Avvik.class, punktreg, "${veiref.textForGUI}", null);
		veiField.setEditable(false);
		ComponentSizeResources.setComponentSizes(veiField, FIELD_MIN_SIZE, FIELD_BIG_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up stedfestField");
		JTextField stedfestField = toolkit.createTextInputField(Avvik.class, punktreg, "${stedfesting.textForGUI}", null);
		stedfestField.setEditable(false);
		ComponentSizeResources.setComponentSizes(stedfestField, FIELD_MIN_SIZE, FIELD_BIGGER_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up inspeksjonField");
		JTextField inspeksjonField = toolkit.createTextInputField(Avvik.class, punktreg, "${inspeksjon.textForGUI}", null);
		inspeksjonField.setEditable(false);
		ComponentSizeResources.setComponentSizes(inspeksjonField, FIELD_MIN_SIZE, FIELD_BIG_PREF_SIZE, FIELD_MAX_SIZE);

		logger.debug("setting up hendelseField");
		hendelseField = toolkit.createTextInputField(Avvik.class, punktreg, HENDELSE_TEXT_FOR_GUI, null);
		hendelseField.setEditable(false);
		punktreg.addPropertyChangeListener(HENDELSE, this);
		ComponentSizeResources.setComponentSizes(hendelseField, FIELD_MIN_SIZE, FIELD_PREF_SIZE, FIELD_MAX_SIZE);

		JButton inspeksjonLink = new JButton(GUITexts.getText(GUITexts.OPEN));
		inspeksjonLink.setMargin(new Insets(1, 1, 1, 1));
		inspeksjonLink.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO utkommentert pga modellendring
//				if (punktreg.getInspeksjonGuid() != null) {
//					if (!parent.getOwner().navigateToDetailTab(punktreg.getInspeksjon())) {
//						InspeksjonSokView s = parent.getParentPlugin().getMipssField().hentInspeksjonSok(
//								punktreg.getInspeksjonGuid());
//						if (s != null) {
//							parent.getOwner().addDetail(s);
//						}
//					}
//				}

			}
		});
		getFieldToolkit().controlEnabledField(inspeksjonLink);
		ComponentSizeResources.setComponentSize(inspeksjonLink, BUTTON_SIZE);

		final String enabledField = "enabled";
		Binding<Avvik, Boolean, JButton, Boolean> enableInspeksjonLink = BindingHelper.createbinding(punktreg,
				"${inspeksjonGuid != null}", inspeksjonLink, enabledField);
		toolkit.addBinding(enableInspeksjonLink);
		ComponentSizeResources.setComponentSize(inspeksjonLink, BUTTON_SIZE);

		JButton hendelseLink = new JButton(GUITexts.getText(GUITexts.OPEN));
		hendelseLink.setMargin(new Insets(1, 1, 1, 1));
		hendelseLink.addActionListener(new ActionListener() {
			/** {@inheritDoc} */
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO utkommentert pga modellendring
//				if (punktreg.getHendelse() != null) {
//					if (!parent.getOwner().navigateToDetailTab(punktreg.getHendelse())) {
//						HendelseSokView h = parent.getOwner().getParentPlugin().getMipssField().hentHendelseSok(
//								punktreg.getHendelseGuid());
//						if (h != null) {
//							parent.getOwner().addDetail(h);
//						}
//					}
//				}
			}
		});
		Binding<Avvik, Boolean, JButton, Boolean> enableHendelseLink = BindingHelper.createbinding(punktreg,
				"${hendelseGuid != null}", hendelseLink, enabledField);
		toolkit.addBinding(enableHendelseLink);
		ComponentSizeResources.setComponentSize(hendelseLink, BUTTON_SIZE);

		veiEdit.setAction(new PlaceDiscoveryAction(parent));
		veiEdit.setText(GUITexts.getText(GUITexts.EDIT));
		veiEdit.setMargin(new Insets(1, 1, 1, 1));
		veiEdit.setEnabled(false);
		getFieldToolkit().controlEnabledField(veiEdit);
		ComponentSizeResources.setComponentSize(veiEdit, BUTTON_SIZE);
		punktreg.addPropertyChangeListener("veiref", this);
		if(!parent.getParentPlugin().hasWriteAccess()) {
			veiEdit.setVisible(false);
		}

		List<JMenuItem> items = new ArrayList<JMenuItem>();
		items.add(new JMenuItem(new NewHendelseAction(GUITexts.getText(GUITexts.NEW), null)));
		items.add(new JMenuItem(new AttatchAction(GUITexts.getText(GUITexts.ATTATCH), null)));
		items.add(new JMenuItem(new UnAttatchAction(GUITexts.getText(GUITexts.UNATTACTH), null)));

		JPopupListMenu attatchHendelse = new JPopupListMenu(GUITexts.getText(GUITexts.EDIT), null, items);
		attatchHendelse.setMargin(new Insets(1, 1, 1, 1));
		getFieldToolkit().controlEnabledField(attatchHendelse);
		ComponentSizeResources.setComponentSize(attatchHendelse, BUTTON_SIZE);

		bindRoad();

		PunktveirefValidator<Avvik> veirefValidator = null;
		if (parent.isNewPunktreg()) {
			veirefValidator = new PunktveirefValidator<Avvik>(punktreg, "veiref");
			parent.getSaveStateObserver().addValidator(veirefValidator);
		}
		veiBox = new LineBox<Punktveiref>(GUITexts.getText(GUITexts.ROAD), veiField, veirefValidator);

		JLabel idLabel = new JLabel(GUITexts.getText(GUITexts.ID));
		ComponentSizeResources.setComponentSize(idLabel, new Dimension(22, 22));
		idLabel.setHorizontalAlignment(JLabel.RIGHT);
		idLabel.setHorizontalTextPosition(JLabel.RIGHT);
		idLabel.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
		
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Driftkontrakt>(GUITexts.getText(GUITexts.CONTRACT), kontraktField, null), 
				idLabel, refnummerField, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<String>(GUITexts.getText(GUITexts.CREATED), opprettetField,null), Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, veiBox, BoxUtil.createHorizontalStrut(1), veiEdit, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Punktstedfest>(GUITexts.getText(GUITexts.POSITION), stedfestField, null),Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Inspeksjon>(GUITexts.getText(GUITexts.INSPECTION), inspeksjonField, null), 
				BoxUtil.createHorizontalStrut(1), inspeksjonLink, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));
		add(BoxUtil.createHorizontalBox(STRUT, new LineBox<Hendelse>(GUITexts.getText(GUITexts.INCIDENT), hendelseField, null), 
				BoxUtil.createHorizontalStrut(1), hendelseLink, attatchHendelse, Box.createHorizontalGlue()));
		add(BoxUtil.createVerticalStrut(STRUT));

		setMinimumSize(MIN_SIZE);
		setPreferredSize(PREF_SIZE);
		setMaximumSize(MAX_SIZE);
	}

	private void bindRoad() {
		String[] fields = new String[] { HENDELSE, "stedfesting", "${veiref}", "${veiref.fylkesnummer}",
				"${veiref.kommunenummer}", "${veiref.veikategori}", "${veiref.veistatus}", "${veiref.veinummer}",
				"${veiref.hp}", "${veiref.meter}", "${veiref.kjorefelt}" };

		for (String field : fields) {
			getFieldToolkit().registerCangeListener(Avvik.class, punktreg, field);
		}
	}

	public PunktveirefValidator<Avvik> getVeiValidator() {
		return (PunktveirefValidator<Avvik>) veiBox.getValidator();
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println(evt.getPropertyName());
		System.out.println(evt.getNewValue());
		if (evt.getSource() == punktreg && StringUtils.equals(HENDELSE, evt.getPropertyName())) {
			if (evt.getNewValue() == null) {
				hendelseField.setText(null);
			}
		} else if (evt.getSource() == punktreg && StringUtils.equals("veiref", evt.getPropertyName())) {
			if (evt.getNewValue() != null && evt.getOldValue() == null) {
				Punktveiref vei = (Punktveiref) evt.getNewValue();
				veiField.setText(vei.getTextForGUI());
			}
		}
	}

	public void setVeiValidator(PunktveirefValidator<Avvik> validator) {
		veiBox.setValidator(validator);
	}

	private class AttatchAction extends AbstractAction {

		public AttatchAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent e) {
			parent.getParentPlugin().enforceWriteAccess();
			boolean cancel = parent.saveOrCancel();
			if (cancel) {
				return;
			}

			double x, y;
			Punktstedfest sted = punktreg.getStedfesting();
			if (sted.getSnappetX() != null && sted.getSnappetY() != null) {
				x = sted.getSnappetX();
				y = sted.getSnappetY();
			} else if (sted.getX() != null && sted.getY() != null) {
				x = sted.getX();
				y = sted.getY();
			} else {
				return;
			}

			Driftkontrakt kontrakt = parent.getParentPlugin().getCommonFilterPanel().getValgtKontrakt();
			JIncidentLocatorDialog dialog = new JIncidentLocatorDialog(punktreg, parent.getOwner(), kontrakt, x, y);
			Hendelse hendelse = dialog.getHendelse();
			if (hendelse != null) {
				parent.getParentPlugin().getMipssField().setHendelse(punktreg, hendelse,
						parent.getParentPlugin().getLoader().getLoggedOnUser(false));
				parent.getViewObject().setHendelseGuid(hendelse.getGuid());
				getFieldToolkit().resetValue(Avvik.class, punktreg, HENDELSE);
				getFieldToolkit().resetValue(Avvik.class, punktreg, HENDELSE_TEXT_FOR_GUI);
				MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.INFO_ICON, GUITexts
						.getText(GUITexts.INFORMATION), GUITexts.getText(GUITexts.ADDED_AND_SAVED),
						MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
				dialogue.askUser();
			}
		}

	}

	private class NewHendelseAction extends AbstractAction {

		public NewHendelseAction(String name, Icon icon) {
			super(name, icon);
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent e) {
			parent.getParentPlugin().enforceWriteAccess();
			boolean cancel = parent.saveOrCancel();
			if (cancel) {
				return;
			}

			final AbstractAction saveAction = new SaveNewHendelseAction();
			parent.getOwner().addDetail((HendelseSokView) null, saveAction);
			SwingWorker<Void, Void> copyWorker = new SwingWorker<Void, Void>() {
				/** {@inheritDoc} */
				@Override
				protected Void doInBackground() throws Exception {
					JIncidentDetailPanel panel = parent.getOwner().getDetailPanelBySaveAction(saveAction);
					Thread.sleep(1000);
					while (parent.getDetailObject()==null || !parent.getFieldToolkit().isBound()) {
						Thread.yield();
					}
					
					Punktstedfest sted = new Punktstedfest(punktreg.getStedfesting());
					sted.setGuid(ServerUtils.getInstance().fetchGuid());
					Punktveiref vei = new Punktveiref(punktreg.getVeiref());
					sted.addPunktveiref(vei);
					List<Punktstedfest> stedList = new ArrayList<Punktstedfest>();
					stedList.add(sted);
					//TODO utkommentert pga modellendring
//					sted.setHendelse(panel.getDetailObject());
					panel.getDetailObject().getSak().setStedfesting(stedList);
					panel.getDetailObject().setDato(punktreg.getOpprettetDato());
					
					int r = JOptionPane.showConfirmDialog(parent.getParent(), GUITexts.getText(GUITexts.COPY_IMAGES_FUNN), GUITexts.getText(GUITexts.COPY_IMAGES_FUNN_TITLE), JOptionPane.YES_NO_OPTION);
					if (r==JOptionPane.YES_OPTION){
						Avvik punktreg = parent.getDetailObject();
						
						for (AvvikBilde b:punktreg.getBilder()){
							Bilde bilde = new Bilde();
							bilde.setGuid(ServerUtils.getInstance().fetchGuid());
							bilde.mergeValues(b.getBilde());
							bilde.setKjoretoyList(null);
							
							panel.getDetailObject().addBilde(bilde);
						}
					}
					panel.reloadPicturePanel();
					panel.redrawMap();
					return null;
				}
			};
			copyWorker.execute();
		}
	}

	/**
	 * Brukes når man lagrer en ny hendelse som kobles til funn
	 * 
	 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
	 */
	private class SaveNewHendelseAction extends AbstractAction {

		private static final String HENDELSE_TEXT_FOR_GUI = "${hendelse.textForGUI}";

		public SaveNewHendelseAction() {
			super(GUITexts.getText(GUITexts.SAVE), IconResources.SAVE_ICON);
		}

		/** {@inheritDoc} */
		@Override
		public void actionPerformed(ActionEvent e) {
			parent.getParentPlugin().enforceWriteAccess();
			JIncidentDetailPanel panel = parent.getOwner().getDetailPanelBySaveAction(this);
			AbstractAction save = panel.createDefaultSaveAction();
			save.actionPerformed(e);
			//TODO utkommentert pga modellendring
//			punktreg.setHendelse(panel.getDetailObject());
//			panel.getDetailObject().addPunktreg(punktreg);

			parent.getParentPlugin().getMipssField().setHendelse(punktreg, panel.getDetailObject(),
					parent.getParentPlugin().getLoader().getLoggedOnUser(false));
			parent.getViewObject().setHendelseGuid(panel.getDetailObject().getGuid());
			getFieldToolkit().resetValue(Avvik.class, punktreg, HENDELSE);
			getFieldToolkit().resetValue(Avvik.class, punktreg, HENDELSE_TEXT_FOR_GUI);
			MipssDialogue dialogue = new MipssDialogue(parent.getOwner(), IconResources.INFO_ICON, GUITexts
					.getText(GUITexts.INFORMATION), GUITexts.getText(GUITexts.ADDED_AND_SAVED),
					MipssDialogue.Type.INFO, new MipssDialogue.Button[] { MipssDialogue.Button.OK });
			dialogue.askUser();
		}
	}

	
	private class UnAttatchAction extends AbstractAction {
		public UnAttatchAction(String name, Icon icon) {
			super(name, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			hendelseField.setText(null);
		}
	}
}
