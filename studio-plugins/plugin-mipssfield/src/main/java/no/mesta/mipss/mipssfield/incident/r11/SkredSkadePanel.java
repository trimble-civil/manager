package no.mesta.mipss.mipssfield.incident.r11;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfield.GUITexts;
import no.mesta.mipss.mipssfield.incident.r11.R11Controller.EgenskapCheck;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

@SuppressWarnings("serial")
public class SkredSkadePanel extends JPanel{
	private Font bold;
	private R11Controller controller;
	private JComboBox regnCombo;
	private JComboBox snoCombo;

	public SkredSkadePanel(R11Controller controller){
		this.controller = controller;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(createTypePanel(), 	 new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
		add(createVaerSkadePanel(), new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));

		
	}
	private JPanel createTypePanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		
		JLabel typeLabel = new JLabel(GUITexts.getText(GUITexts.R11_TYPESKRED));
		JLabel beskrivelseLabel = new JLabel(GUITexts.getText(GUITexts.R11_BESKRIVELSE));
		
		JLabel volumLabel = new JLabel(GUITexts.getText(GUITexts.R11_VOLUM));
		JLabel volumVeiLabel = new JLabel(GUITexts.getText(GUITexts.R11_VOLUMVEI));
		JLabel volumTotLabel = new JLabel(GUITexts.getText(GUITexts.R11_VOLUMTOT));
		JLabel volumM2Label = new JLabel(GUITexts.getText(GUITexts.R11_VOLUMM2));
		bold = volumLabel.getFont();
		bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
		volumLabel.setFont(bold);
		typeLabel.setFont(bold);
		beskrivelseLabel.setFont(bold);
		
		JComboBox typeCombo = new JComboBox();
		JComboBox losneCombo = new JComboBox();
		JComboBox volumVeiCombo= new JComboBox();
		JTextField volumTotField = new JTextField();
		Dimension size = volumTotField.getPreferredSize();
		size.width=100;
		volumTotField.setPreferredSize(size);
		
		Egenskap skredtype = controller.findEgenskap(R11Panel.SKREDTYPE_ID);
		Egenskap losneomrade = controller.findEgenskap(R11Panel.LOSNEOMRADE_ID);
		Egenskap volum = controller.findEgenskap(R11Panel.VOLUM_ID);
		
		MipssComboBoxModel<Egenskapverdi> typeModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(skredtype));
		MipssComboBoxModel<Egenskapverdi> losneModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(losneomrade));
		MipssComboBoxModel<Egenskapverdi> volumModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(volum));
		
		typeCombo.setModel(typeModel);
		losneCombo.setModel(losneModel);
		volumVeiCombo.setModel(volumModel);
		
		EgenskapItemListener typeListener = new EgenskapItemListener(skredtype, controller.getSkred());
		EgenskapItemListener losneListener = new EgenskapItemListener(losneomrade, controller.getSkred());
		EgenskapItemListener volumListener = new EgenskapItemListener(volum, controller.getSkred());
		
		typeCombo.addItemListener(typeListener);
		losneCombo.addItemListener(losneListener);
		volumVeiCombo.addItemListener(volumListener);
		
		typeCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(skredtype));
		losneCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(losneomrade));
		volumVeiCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(volum));
		
		if (controller.getOriginalSkred()==null){
			typeCombo.setSelectedIndex(0);
			losneCombo.setSelectedIndex(0);
			volumVeiCombo.setSelectedIndex(0);
		}
		
		BindingHelper.createbinding(controller.getSkred(), "anslagTotalSkredstorrelse", volumTotField, "text", BindingHelper.READ_WRITE).bind();
		
		JPanel volumPanel = new JPanel(new GridBagLayout());
		volumPanel.add(volumTotField, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		volumPanel.add(volumM2Label, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,0), 0,0));
		
		panel.add(typeLabel, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		panel.add(beskrivelseLabel, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		panel.add(volumLabel, 		new GridBagConstraints(2,0, 2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,0,5), 0,0));
		
		panel.add(typeCombo, 		new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(losneCombo, new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(volumVeiLabel, 	new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(volumVeiCombo, 	new GridBagConstraints(3,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(volumTotLabel, 	new GridBagConstraints(2,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));
		panel.add(volumPanel, 		new GridBagConstraints(3,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));

		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_SKRED_TITLE)));
		return panel;
	}
	
	private JPanel createVaerSkadePanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		
		JLabel hoydeLabel = new JLabel(GUITexts.getText(GUITexts.R11_HOYDE));
		JLabel skadeLabel = new JLabel(GUITexts.getText(GUITexts.R11_SKADE));
		JLabel vaerLabel = new JLabel(GUITexts.getText(GUITexts.R11_VAERFORHOLD));
		
		hoydeLabel.setFont(bold);
		skadeLabel.setFont(bold);
		vaerLabel.setFont(bold);
		
		JLabel regnLabel = new JLabel(GUITexts.getText(GUITexts.R11_REGN));
		JLabel snoLabel = new JLabel(GUITexts.getText(GUITexts.R11_SNO));
		JLabel vindLabel = new JLabel(GUITexts.getText(GUITexts.R11_VINDRETNING));
		JLabel temperaturLabel = new JLabel(GUITexts.getText(GUITexts.R11_TEMPERATUR));
		JLabel graderLabel = new JLabel(GUITexts.getText(GUITexts.R11_GRADER));
		
		
		JComboBox hoydeCombo = new JComboBox();
		regnCombo = new JComboBox();
		snoCombo = new JComboBox();
		JComboBox vindCombo = new JComboBox();
		
		JCheckBox nedborCheck = new JCheckBox(GUITexts.getText(GUITexts.R11_NEDBOR));
		
		JTextField temperaturField = new JTextField();
		
		Egenskap hoyde = controller.findEgenskap(R11Panel.HOYDE_ID);
		Egenskap regn = controller.findEgenskap(R11Panel.REGN_ID);
		Egenskap sno = controller.findEgenskap(R11Panel.SNO_ID);
		Egenskap vind = controller.findEgenskap(R11Panel.VIND_ID);
		
		MipssComboBoxModel<Egenskapverdi> hoydeModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(hoyde));
		MipssComboBoxModel<Egenskapverdi> regnModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(regn));
		MipssComboBoxModel<Egenskapverdi> snoModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(sno));
		MipssComboBoxModel<Egenskapverdi> vindModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(vind));
		
		hoydeCombo.setModel(hoydeModel);
		regnCombo.setModel(regnModel);
		snoCombo.setModel(snoModel);
		vindCombo.setModel(vindModel);
		
		EgenskapItemListener hoydeListener = new EgenskapItemListener(hoyde, controller.getSkred());
		EgenskapItemListener regnListener = new EgenskapItemListener(regn, controller.getSkred());
		EgenskapItemListener snoListener = new EgenskapItemListener(sno, controller.getSkred());
		EgenskapItemListener vindListener = new EgenskapItemListener(vind, controller.getSkred());
		
		hoydeCombo.addItemListener(hoydeListener);
		regnCombo.addItemListener(regnListener);
		snoCombo.addItemListener(snoListener);
		vindCombo.addItemListener(vindListener);
		
		hoydeCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(hoyde));
		regnCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(regn));
		snoCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(sno));
		vindCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(vind));
		
		Egenskap skade = controller.findEgenskap(R11Panel.SKADE_ID);
		List<Egenskapverdi> skadeTypeList = controller.getEgenskapverdi(skade);
		skadeTypeList.remove(0);
		List<R11Controller.EgenskapCheck> skadeCheckList = new ArrayList<R11Controller.EgenskapCheck>();
		for (Egenskapverdi e:skadeTypeList){
			EgenskapCheck wrapper = new EgenskapCheck(e);
			skadeCheckList.add(wrapper);
			if (controller.isEgenskapVerdiPrensent(e)){
				wrapper.setSelected(true);
			}
			wrapper.addItemListener(new ItemListener(){
				@Override
				public void itemStateChanged(ItemEvent e) {
					EgenskapCheck egenskap = (EgenskapCheck)e.getItem();
					if (egenskap.isSelected()){
						controller.addEgenskapverdi(egenskap.getVerdi());
					}else{
						controller.slettEgenskapverdi(egenskap.getVerdi());
					}
				}
			});
		}
		
		if (controller.getOriginalSkred()==null){
			hoydeCombo.setSelectedIndex(0);
			regnCombo.setSelectedIndex(0);
			snoCombo.setSelectedIndex(0);
			vindCombo.setSelectedIndex(0);
		}
		
		BindingHelper.createbinding(controller.getSkred(), "temperatur", temperaturField, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(controller.getSkred(), "ingenNedbor", nedborCheck, "selected", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(this, "ingenNedbor", nedborCheck, "${!selected}", BindingHelper.READ_WRITE).bind();
		
		JPanel tempPanel = new JPanel(new GridBagLayout());
		tempPanel.add(temperaturField, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		tempPanel.add(graderLabel, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		
		panel.add(hoydeLabel, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(skadeLabel, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vaerLabel, new GridBagConstraints(2,0, 2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		
		panel.add(hoydeCombo, new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		int i=1;
		for (R11Controller.EgenskapCheck e:skadeCheckList){
			panel.add(e, new GridBagConstraints(1,i++, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		}
		panel.add(nedborCheck, 		new GridBagConstraints(2,1, 2,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(regnLabel, 		new GridBagConstraints(2,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(regnCombo, 		new GridBagConstraints(3,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(snoLabel, 		new GridBagConstraints(2,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(snoCombo, 		new GridBagConstraints(3,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(vindLabel,		new GridBagConstraints(2,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(vindCombo, 		new GridBagConstraints(3,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.add(temperaturLabel, 	new GridBagConstraints(2,5, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0,0));
		panel.add(tempPanel,	 	new GridBagConstraints(3,5, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,0,5), 0,0));
		panel.setBorder(BorderFactory.createTitledBorder(GUITexts.getText(GUITexts.R11_VEIINFO_TITLE)));
		return panel;
	}
	
	public void setIngenNedbor(Boolean enabled){
		regnCombo.setEnabled(enabled);
		snoCombo.setEnabled(enabled);
		if (!enabled){
			regnCombo.setSelectedIndex(0);
			snoCombo.setSelectedIndex(0);
		}
	}
}
