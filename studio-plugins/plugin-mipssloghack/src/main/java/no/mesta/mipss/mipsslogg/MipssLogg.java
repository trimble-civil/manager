package no.mesta.mipss.mipsslogg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.core.KonfigparamPreferences;

public class MipssLogg {
	private static final List<Process> mipsslogs = new ArrayList<Process>();
	private static boolean firstTimer = true;
	
	public static void startMipssLogg(String... commands){
		String[] com = new String[commands.length+1];
		String fileLocation = loadExefile();
		com[0]=fileLocation;
		int i=1;
		for (String s:commands){
			com[i] = s;
			i++;
		}
		
		ProcessBuilder pb = new ProcessBuilder(com);
		try {
			final Process start = pb.start();
			mipsslogs.add(start);
			if (firstTimer){
				addProcessKiller();
				firstTimer = false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	private static String loadExefile() {
		nukeTempFiles();
		String filename = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipsslogghack", "filename").getVerdi();
		String fileLocation="";
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource("native/"+filename+".exe");
		InputStream is=null;
		try {
			is = url.openStream();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		try {
			int[] line = new int[is.available()];
			File myFile = File.createTempFile(filename, ".exe");
			myFile.deleteOnExit();
			myFile.setExecutable(true);
			FileOutputStream out = new FileOutputStream(myFile.getAbsolutePath());
			int i = 0;
			int c;
			while((c=is.read()) != -1){
				line[i]=c;
				i++;
			}
			is.close();

			for(int j= 0;j<line.length;j++){
				out.write(line[j]);
			}
			out.close();
			fileLocation = myFile.getAbsolutePath();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return fileLocation;
	}
	private static void nukeTempFiles(){
		String tmpDir = System.getProperty("java.io.tmpdir");
		File f = new File(tmpDir);
		for (File fi:f.listFiles()){
			if (fi.getName().indexOf("MIPSSLog_II")!=-1){
				fi.delete();
			}
		}
	}
	private static String getTempDir(){
		String tmpDir = System.getProperty("java.io.tmpdir");
		return tmpDir;
	}
	
	private static void addProcessKiller(){
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				for (Process p:mipsslogs){
					p.destroy();
				}
			}
		});
	}
}
