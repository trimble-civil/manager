package no.mesta.mipss.admin;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.admin.common.editor.EntityCellEditor;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.applikasjon.Menygruppe;
import no.mesta.mipss.persistence.applikasjon.Modul;
import no.mesta.mipss.persistence.applikasjon.Tilgangspunkt;
import no.mesta.mipss.persistence.areal.Arealtype;
import no.mesta.mipss.persistence.datafangsutstyr.Dfukategori;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sesong;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrprodusent;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Driftregion;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Prosessett;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskaptype;
import no.mesta.mipss.persistence.tilgangskontroll.Selskap;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinetttype;
import no.mesta.mipss.ui.table.MipssTableComboBoxRenderer;

/**
 * Enumerated values correlating entities and their default renderers.
 * 
 * @author Christian Wiik (Mesan)
 */
public enum EntityRenderEnum {
	AREALTYPE(Arealtype.class),
	DFUKATEGORI(Dfukategori.class),
	DRIFTKONTRAKT(Driftkontrakt.class),
	DRIFTREGION(Driftregion.class),
//	IKON(Ikon.class),
	PRODTYPE(Prodtype.class),
	PROSESS(Prosess.class),
	PROSESSETT(Prosessett.class),
	PUNKTREGSTATUSTYPE(AvvikstatusType.class),
	SELSKAP(Selskap.class),
	RODETYPE(Rodetype.class),
	SESONG(Sesong.class),
	UTSTYRGRUPPE(Utstyrgruppe.class),
	UTSTYRPRODUSENT(Utstyrprodusent.class),
	UTSTYRSUBGRUPPE(Utstyrsubgruppe.class),
	UTSTYRMODELL(Utstyrmodell.class),
	DFUVAREKATALOG(Dfuvarekatalog.class),
	KJORETOYMODELL(Kjoretoymodell.class),
	DFUMODELL(Dfumodell.class),
	VEINETTTYPE(Veinetttype.class),
	TILGANGSPUNKT(Tilgangspunkt.class),
	MODUL(Modul.class),
	MENYGRUPPE(Menygruppe.class),
	EGENSKAPTYPE(Egenskaptype.class),
	EGENSKAP(Egenskap.class),
	VEINETT(Veinett.class);

	private Class<? extends IRenderableMipssEntity> entityClass;
	private TableCellRenderer cellRenderer;
	private TableCellEditor cellEditor;
	
	private EntityRenderEnum(Class<? extends IRenderableMipssEntity> entityClass) {
		this.entityClass = entityClass;
		this.cellRenderer = new MipssTableComboBoxRenderer();
		this.cellEditor = new EntityCellEditor(entityClass);
	}
		
	/**
	 * Returns the entity class held in the enum.
	 * 
	 * @return entityClass - the entity class held in the enum. 
	 */
	public Class<? extends IRenderableMipssEntity> getEntityClass() {
		return this.entityClass;
	}
	
	/**
	 * Returns the cell renderer object held in the enum.
	 * 
	 * @return cellRenderer - the cell renderer held in the enum. 
	 */
	public TableCellRenderer getCellRenderer() {
		return this.cellRenderer;
	}
	
	/**
	 * Returns the cell editor object held in the enum.
	 * 
	 * @return cellEditor - the cell editor held in the enum. 
	 */
	public TableCellEditor getCellEditor() {
		return this.cellEditor;
	}
	
}