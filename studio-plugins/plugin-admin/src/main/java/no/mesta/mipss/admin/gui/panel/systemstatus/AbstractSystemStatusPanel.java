package no.mesta.mipss.admin.gui.panel.systemstatus;

import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.admin.common.factory.SystemStatusTableFactory;

/**
 * Base panel containing commonalities for system status panels.
 * 
 * @author Christian Wiik (Mesan)
 */
public abstract class AbstractSystemStatusPanel extends JPanel {
	
	private static final long serialVersionUID = 5013840903701127246L;
	protected SystemStatusTableFactory tableFactory;
	
	public AbstractSystemStatusPanel() {
		tableFactory = SystemStatusTableFactory.getInstance();
	}
	
	protected abstract void init();
	
	/**
	 * Creates a title border stating the name of the component.
	 * 
	 * @return titleBorder - the title panel border.
	 */
	protected Border getPanelBorder(SystemStatusComponentEnum statusComponent) {
		
		TitledBorder titleBorder = BorderFactory.createTitledBorder(statusComponent.getDisplayName());
		
		Font titleFont = new Font("Arial", Font.BOLD, 14);
		titleBorder.setTitleFont(titleFont);
		
		return titleBorder;
	}
	
	protected abstract void setBusy(boolean busy);
	
}