package no.mesta.mipss.admin;

import java.util.HashMap;
import java.util.Map;

public enum TableEnum {
	Appstatus("APPSTATUS", TableEnum.P_APPLIKASJON),
	Arbeidstype("ARBEIDSTYPE", TableEnum.P_FELT),
	Areal("AREAL", TableEnum.P_AREAL),
	Arealtype("AREALTYPE", TableEnum.P_AREAL),
	Dfubeststatus("DFUBESTSTATUS", TableEnum.P_DATAFANGSTUTSTYRBESTILLING),
	Dfukategori("DFUKATEGORI", TableEnum.P_DATAFANGSTUTSTYR),
	Dfuleverandor("DFULEVERANDOR", TableEnum.P_DATAFANGSTUTSTYRBESTILLING),
	Dfumodell("DFUMODELL", TableEnum.P_DATAFANGSTUTSTYR),
	Dfustatus("DFUSTATUS", TableEnum.P_DATAFANGSTUTSTYR),
	Dfuvarekatalog("DFUVAREKATALOG", TableEnum.P_DATAFANGSTUTSTYRBESTILLING),
	DfuDfuvare("DFU_DFUVARE", TableEnum.P_DATAFANGSTUTSTYRBESTILLING),
	UtstyrDfuvare("UTSTYR_DFU_VARE", TableEnum.P_DATAFANGSTUTSTYRBESTILLING),
	Dokformat("DOKFORMAT", TableEnum.P_DOKARKIV),
	Doktype("DOKTYPE", TableEnum.P_DOKARKIV),
	Driftdistrikt("DRIFTDISTRIKT", TableEnum. P_KONTRAKT),
	Driftregion("DRIFTREGION", TableEnum.P_KONTRAKT),
	Egenskap("EGENSKAP", TableEnum.P_FELT_R),
	Egenskaptype("EGENSKAPTYPE", TableEnum.P_FELT_R),
	Egenskapverdi("EGENSKAPVERDI", TableEnum.P_FELT_R),
	Hendelsesaarsak("HENDELSESAARSAK", TableEnum.P_FELT),
	Hendelsestype("HENDELSESTYPE", TableEnum.P_FELT),
	Installasjonstatus("INSTALLASJONSTATUS", TableEnum.P_DATAFANGSTUTSTYR),
	Ikon("IKON", TableEnum.P_DOKARKIV),
	Ioteknologi("IOTEKNOLOGI", TableEnum.P_DATAFANGSTUTSTYR),
	Kjoretoymodell("KJORETOYMODELL", TableEnum.P_KJORETOY),
	Kjoretoyordrestatus("KJORETOYORDRESTATUS", TableEnum.P_KJORETOYORDRE),
	Kjoretoytype("KJORETOYTYPE", TableEnum.P_KJORETOY),
	Konfigparam("KONFIGPARAM", TableEnum.P_APPLIKASJON),
	Kjoretoykontakttype("KJORETOYKONTAKTTYPE", TableEnum.P_KONTRAKT),
	Kontraktkontakttype("KONTRAKTKONTAKTTYPE", TableEnum.P_KONTRAKT_KONTAKT),
	Kontrakttype("KONTRAKTTYPE", TableEnum.P_KONTRAKT),
	Kunderegion("KUNDEREGION", TableEnum.P_KONTRAKT),
	Materielltype("MATERIELLTYPE", TableEnum.P_FELT),
	Menygruppe("MENYGRUPPE", TableEnum.P_APPLIKASJON),
	Menypunkt("MENYPUNKT", TableEnum.P_APPLIKASJON),
	Modul("MODUL", TableEnum.P_APPLIKASJON),
	Prodtype("PRODTYPE", TableEnum.P_KJORETOYUTSTYR),
	Prosess("PROSESS", TableEnum.P_FELT),
	Prosessett("PROSESSETT", TableEnum.P_FELT),
	PunktregstatusType("PUNKTREGSTATUS", TableEnum.P_FELT),
	Punktregtilstandtype("PUNKTREGTILSTANDTYPE", TableEnum.P_FELT),
	Ressurstype("RESSURSTYPE", TableEnum.P_FELT),
	Rodetype("RODETYPE", TableEnum.P_KONTRAKT),
	Sensortype("SENSORTYPE", TableEnum.P_KJORETOYUTSTYR),
	Sesong("SESONG", TableEnum.P_KJORETOYUTSTYR),
	Skadekontakttype("SKADEKONTAKTTYPE", TableEnum.P_FELT),
	Stroproduktgruppe("STROPRODUKTGRUPPE", TableEnum.P_STROING),
	Tilgangsnivaa("TILGANGSNIVAA", TableEnum.P_TILGANGSKONTROLL),
	Tilgangspunkt("TILGANGSPUNKT", TableEnum.P_APPLIKASJON),
	Trafikktiltaktype("TRAFIKKTILTAKTYPE", TableEnum.P_FELT),
	Underprosess("UNDERPROSESS", TableEnum.P_FELT),
	Utstyrgruppe("UTSTYRGRUPPE", TableEnum.P_KJORETOYUTSTYR),
	Utstyrmodell("UTSTYRMODELL", TableEnum.P_KJORETOYUTSTYR),
	Utstyrprodusent("UTSTYRPRODUSENT", TableEnum.P_KJORETOYUTSTYR),
	Utstyrsubgruppe("UTSTYRSUBGRUPPE", TableEnum.P_KJORETOYUTSTYR),
	Veinetttype("VEINETTTYPE", TableEnum.P_VEINETT);
	
	/** Pakker representert i enumen*/
	
	private static final String P_DATAFANGSTUTSTYR = "no.mesta.mipss.persistence.datafangsutstyr";
	private static final String P_DATAFANGSTUTSTYRBESTILLING = "no.mesta.mipss.persistence.datafangsutstyrbestilling";
	private static final String P_AREAL = "no.mesta.mipss.persistence.areal";
	private static final String P_DOKARKIV = "no.mesta.mipss.persistence.dokarkiv";
	private static final String P_KONTRAKT = "no.mesta.mipss.persistence.kontrakt";
	private static final String P_KONTRAKT_KONTAKT = "no.mesta.mipss.persistence.kontrakt.kontakt";
	private static final String P_FELT = "no.mesta.mipss.persistence.mipssfield";
	
	private static final String P_FELT_R = "no.mesta.mipss.persistence.mipssfield.r.common";
	
	private static final String P_KJORETOY = "no.mesta.mipss.persistence.kjoretoy";
	private static final String P_KJORETOYORDRE = "no.mesta.mipss.persistence.kjoretoyordre";
	private static final String P_APPLIKASJON = "no.mesta.mipss.persistence.applikasjon";
	private static final String P_KJORETOYUTSTYR = "no.mesta.mipss.persistence.kjoretoyutstyr";
	private static final String P_STROING = "no.mesta.mipss.persistence.stroing";
	private static final String P_TILGANGSKONTROLL = "no.mesta.mipss.persistence.tilgangskontroll";
	private static final String P_VEINETT = "no.mesta.mipss.persistence.veinett";
	private String tableName;
	private String packageName;
	
	private TableEnum(String tableName, String packageName){
		this.tableName = tableName;
		this.packageName = packageName;
	}
	
	public String getTableName(){
		return tableName;
	}
	
	public String getPackageName(){
		return packageName;
	}
	
	public static Map<String, String> getPackages(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("Datafangstutstyr", P_DATAFANGSTUTSTYR);
		map.put("Datafangstutstyrbestilling", P_DATAFANGSTUTSTYRBESTILLING);
		map.put("Areal", P_AREAL);
		map.put("Dokarkiv", P_DOKARKIV);
		map.put("Kontrakt", P_KONTRAKT);
		map.put("Kontraktkontakt", P_KONTRAKT_KONTAKT);
		map.put("Felt", P_FELT);
		map.put("Felt.R", P_FELT_R);
		map.put("Kjoretoy", P_KJORETOY);
		map.put("Kjoretoyordre", P_KJORETOYORDRE);
		map.put("Kjoretoyutstyr", P_KJORETOYUTSTYR);
		map.put("Applikasjon", P_APPLIKASJON);
		map.put("Stroing", P_STROING);
		map.put("Tilgangskontroll", P_TILGANGSKONTROLL);
		map.put("Veinett", P_VEINETT);
		return map;
	}
	@Override
	public String toString(){
		return getTableName();
	}
}
