package no.mesta.mipss.admin.common.enumeration;

/**
 * Enumerated values representing monitored components.
 * 
 * @author Christian Wiik (Mesan)
 */
public enum SystemStatusComponentEnum {
	
	DATABASE_SYSTEM("Database"),
	DATEK_SYSTEM("Datek Web Server"),
	KART_SYSTEM("Kartserver"),
	KVERNEMOTOR_SYSTEM("Kvernemotor"),
	M2M_SYSTEM("M2M Server"),
	MOBILE_SYSTEM("Mobile Server");
	
	private String displayName;
	
	private SystemStatusComponentEnum(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}