package no.mesta.mipss.admin.gui.panel.configparam2;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.AndroidManifestReader;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
@SuppressWarnings("serial")
public class MipssFeltUpdatePanel extends JPanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");
	
	private JLabel lblSisteVersjonKode;
	private JLabel lblSisteVersjonNavn;
	private JLabel lblApkUrl;
	private JLabel lblForceUpdate;
	private JLabel lblApkFil;
	private JLabel lblBrukermanual;
	private JLabel lblManualChecksum;
	private JLabel lblBrukermanualUrl;
	
	
	private JTextField fldSisteVersjonKode;
	private JTextField fldSisteVersjonNavn;
	private JTextField fldApkUrl;
	private JTextField fldApkFil;
	private JTextField fldBrukermanual;
	private JTextField fldManualChecksum;
	private JTextField fldBrukermanualUrl;
	
	private JCheckBox chkForceUpdate;
	
	private JButton btnFil;
	private JButton btnBrukermanual;
	private Konfigparam sisteVersjonKode;
	private Konfigparam apkUrl;
	private Konfigparam sisteVersjonNavn;
	private Konfigparam forceUpdate;
	private Konfigparam checksum;
	private Konfigparam brukermanualUrl;
	
	private File apkFil;
	private File brukermanualFil;
	
	private boolean apkSet;
	private boolean brukermanualSet;
	
	public MipssFeltUpdatePanel(){
		initComponents();
		initGui();
	}

	private void initComponents() {
		sisteVersjonKode = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT_ANDROID", "APK_NEWEST_VERSION_CODE");
		apkUrl = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT_ANDROID", "APK_URL");
		brukermanualUrl = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT_ANDROID", "MANUAL_URL");
		sisteVersjonNavn = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT_ANDROID", "APK_NEWEST_VERSION_NAME");
		forceUpdate = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT_ANDROID", "APK_FORCE_UPGRADE");
		checksum = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT_ANDROID", "MANUAL_CHECKSUM");
		
		
		
		lblApkFil = new JLabel(resources.getGuiString("label.apkFil"));
		lblSisteVersjonKode = new JLabel(resources.getGuiString("label.versjonKode"));
		lblSisteVersjonNavn = new JLabel(resources.getGuiString("label.versjonNavn"));
		lblForceUpdate = new JLabel(resources.getGuiString("label.forceUpdate"));
		lblApkUrl = new JLabel(resources.getGuiString("label.apkUrl"));
		lblBrukermanual = new JLabel(resources.getGuiString("label.brukermanual"));
		lblManualChecksum = new JLabel(resources.getGuiString("label.manualCrc"));
		lblBrukermanualUrl = new JLabel(resources.getGuiString("label.brukermanualUrl"));
		
		fldApkFil = new JTextField(10);
		fldSisteVersjonKode = new JTextField(10);
		fldSisteVersjonNavn = new JTextField(10);
		fldApkUrl = new JTextField(10);
		fldBrukermanualUrl = new JTextField(10);
		fldBrukermanual = new JTextField(10);
		fldManualChecksum = new JTextField(10);
		
		
		chkForceUpdate = new JCheckBox();
		
		btnFil = new JButton(getOpenApkAction());
		btnBrukermanual = new JButton(getOpenBrukermanualAction());
		
		fldApkFil.setEditable(false);
		fldApkFil.setText("<Blob fra databasen>");
		fldBrukermanual.setEditable(false);
		fldBrukermanual.setText("<Blob fra databasen>");
		
		fldSisteVersjonKode.setText(sisteVersjonKode.getVerdi());
		fldSisteVersjonNavn.setText(sisteVersjonNavn.getVerdi());
		fldManualChecksum.setText(checksum.getVerdi());
		
		fldSisteVersjonKode.setEditable(false);
		fldSisteVersjonNavn.setEditable(false);
		fldManualChecksum.setEditable(false);
		
		chkForceUpdate.setSelected("1".equals(forceUpdate.getVerdi()));
		fldApkUrl.setText(apkUrl.getVerdi());
		fldBrukermanualUrl.setText(brukermanualUrl.getVerdi());
		
		
	}

	
	private void initGui() {
		setLayout(new GridBagLayout());
		JPanel pnlApk = new JPanel(new GridBagLayout());
		pnlApk.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("border.apk")));
		pnlApk.add(lblApkFil, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 11, 0, 5), 0,0 ));
		pnlApk.add(lblSisteVersjonKode, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		pnlApk.add(lblSisteVersjonNavn, new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		pnlApk.add(lblForceUpdate, 	 	new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		pnlApk.add(lblApkUrl, 	 		new GridBagConstraints(0,4, 1,1, 0.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		
		pnlApk.add(fldApkFil, 			new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11, 0, 0, 5), 0,0 ));
		pnlApk.add(btnFil, 			 	new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 0, 0, 11), 0,0 ));
		pnlApk.add(fldSisteVersjonKode, new GridBagConstraints(1,1, 2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 11), 0,0 ));
		pnlApk.add(fldSisteVersjonNavn, new GridBagConstraints(1,2, 2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 11), 0,0 ));
		pnlApk.add(chkForceUpdate, 	 	new GridBagConstraints(1,3, 2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 11), 0,0 ));
		pnlApk.add(fldApkUrl, 	 		new GridBagConstraints(1,4, 2,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 11), 0,0 ));
		
		JPanel pnlManual = new JPanel(new GridBagLayout());
		pnlManual.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("border.brukermanual")));
		pnlManual.add(lblBrukermanual, 	 new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		pnlManual.add(lblManualChecksum, 	 new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		pnlManual.add(lblBrukermanualUrl,  new GridBagConstraints(0,2, 1,1, 0.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 11, 0, 5), 0,0 ));
		
		pnlManual.add(fldBrukermanual,  	 new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11, 0, 0, 5), 0,0 ));
		pnlManual.add(btnBrukermanual, 	 new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 0, 0, 11), 0,0 ));
		pnlManual.add(fldManualChecksum, 	 new GridBagConstraints(1,1, 2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 11), 0,0 ));
		pnlManual.add(fldBrukermanualUrl,  new GridBagConstraints(1,2, 2,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 11), 0,0 ));
		
		add(pnlApk, 	new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,0 ));
		add(pnlManual, 	new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,0 ));
	}
	
	private Action getOpenApkAction(){
		return new AbstractAction(resources.getGuiString("button.openFil")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser f = new JFileChooser();
				f.setFileFilter(new FileFilter() {
					@Override
					public boolean accept(File file) {
						return "apk".equals(getExtension(file));
					}
					@Override
					public String getDescription() {
						return "Android .apk fil";
					}
				});
				int val = f.showOpenDialog(MipssFeltUpdatePanel.this);
				if (val==JFileChooser.APPROVE_OPTION){
					apkFil = f.getSelectedFile();
					fldApkFil.setText(apkFil.getPath());
					readApkFile(apkFil);
					apkSet = true;
				}
			}
		};
	}
	public String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
	
	private Action getOpenBrukermanualAction(){
		return new AbstractAction(resources.getGuiString("button.openFil")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser f = new JFileChooser();
				f.setFileFilter(new FileFilter() {
					@Override
					public boolean accept(File file) {
						return "pdf".equals(getExtension(file));
					}
					@Override
					public String getDescription() {
						return "PDF fil";
					}
				});
				int val = f.showOpenDialog(MipssFeltUpdatePanel.this);
				if (val==JFileChooser.APPROVE_OPTION){
					brukermanualFil = f.getSelectedFile();
					fldBrukermanual.setText(brukermanualFil.getPath());
					try {
						fldManualChecksum.setText(getChecksum(getSelectedBrukermanualFile()));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					brukermanualSet = true;
				}
			}
		};
	}
	private void readApkFile(File file){
		AndroidManifestReader reader = new AndroidManifestReader(file.getPath());
		String code = reader.getVersionCode();
		String name = reader.getVersionName();
		
		if (StringUtils.isEmpty(code)){
			fldSisteVersjonKode.setEditable(true);
		}else{
			fldSisteVersjonKode.setEditable(false);
		}
		if (StringUtils.isEmpty(name)){
			fldSisteVersjonNavn.setEditable(true);
		}else{
			fldSisteVersjonNavn.setEditable(false);
		}
		fldSisteVersjonKode.setText(code);
		fldSisteVersjonNavn.setText(name);
	}
	
	public List<Konfigparam> getConfigs(){
		sisteVersjonKode.setVerdi(fldSisteVersjonKode.getText());
		apkUrl.setVerdi(fldApkUrl.getText());
		sisteVersjonNavn.setVerdi(fldSisteVersjonNavn.getText());
		forceUpdate.setVerdi(chkForceUpdate.isSelected()?"1":"0");
		checksum.setVerdi(fldManualChecksum.getText());
		brukermanualUrl.setVerdi(fldBrukermanualUrl.getText());
		
		List<Konfigparam> params = new ArrayList<Konfigparam>();
		params.add(sisteVersjonKode);
		params.add(apkUrl);
		params.add(brukermanualUrl);
		params.add(sisteVersjonNavn);
		params.add(forceUpdate);
		params.add(checksum);
		return params;
	}
	
	private String getChecksum(byte[] fil){
		Checksum checksum = new CRC32();
		checksum.update(fil, 0, fil.length);
		long value = checksum.getValue();
		return Long.toHexString(value);
	}
	
	public byte[] getSelectedApkFile() throws IOException{
		return FileUtils.readFileToByteArray(apkFil);
	}
	public byte[] getSelectedBrukermanualFile() throws IOException{
		byte[] fil =  FileUtils.readFileToByteArray(brukermanualFil);
		return fil;
	}
	
	public boolean isApkSet(){
		return apkSet;
	}
	public boolean isBrukermanualSet(){
		return brukermanualSet;
	}
	
}
