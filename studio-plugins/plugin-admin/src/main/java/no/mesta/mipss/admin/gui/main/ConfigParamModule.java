package no.mesta.mipss.admin.gui.main;

import javax.swing.JPanel;

import no.mesta.mipss.admin.gui.panel.configparam.ConfigParamGUIPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

/**
 * The entry point and main window of the MIPSS config param module.
 * 
 * @author Christian Wiik (Mesan)
 */
public class ConfigParamModule extends MipssPlugin {

	private static final long serialVersionUID = -8675286433039926476L;
	
//	private static PropertyAccessor pa;

	private ConfigParamGUIPanel configParamGUIPanel;
	
	@Override
	public JPanel getModuleGUI() {
		return configParamGUIPanel;
	}

	/** GUI initialization. */
	@Override
	protected void initPluginGUI() {
		configParamGUIPanel = new ConfigParamGUIPanel(this);
	}
	
	/** Application shutdown method. */
	@Override
	public void shutdown() {
		configParamGUIPanel.dispose();
	}
	@Override
	public void readyForShutdown(MipssDialogue dialog){
		if (!configParamGUIPanel.getContentPanel().getContentPanel().isDataSaved()){
			dialog.askUser();
		}
	}

}