package no.mesta.mipss.admin;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.List;

import no.mesta.mipss.common.MipssObservableListListener;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.dialogue.MipssDialogue.Button;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.observablecollections.ObservableList;

public class GrunndataEntityChangeListener implements MipssObservableListListener<MipssEntityBean<?>>{
	
	private final IMipssStudioLoader loader;
	private final MipssObservableList<MipssEntityBean<?>> observableList;
	private final GrunndataController controller;
	
	public GrunndataEntityChangeListener(IMipssStudioLoader loader, MipssObservableList<MipssEntityBean<?>> observableList, GrunndataController controller){
		this.loader = loader;
		this.observableList = observableList;
		this.controller = controller;
		this.observableList.addObservableListListener(this);
		
	}
	
	private MipssEntityBean<?> prev = null;
	
	@Override
	public void listElementPropertyChanged(ObservableList<MipssEntityBean<?>> list, int index,PropertyChangeEvent evt) {
		MipssEntityBean<?> changed = list.get(index);
		String prop = evt.getPropertyName();
		if (changed.isIdField(prop)&&prev==null&&evt.getOldValue()!=null){
			Button response = showPkWarning();
			if (response == MipssDialogue.Button.YES){
				MipssEntityBean<?> newEntityToDelete = cloneEntity(changed);
				changed.setNewEntity(true);
				touchNew(changed);
				revertValue(newEntityToDelete, evt);
				if (!newEntityToDelete.isNewEntity())
					controller.addRemovedEntity(newEntityToDelete);
				
				if (!controller.isEntityChanged(changed))
					controller.addChangedEntity(changed);
			}
			if (response == MipssDialogue.Button.NO){
				MipssEntityBean<?> newEntity = cloneEntity(changed);
				newEntity.setNewEntity(true);
				touchNew(newEntity);
				prev = changed;
				revertValue(changed, evt);
				observableList.add(newEntity);
			}
			if (response == MipssDialogue.Button.CANCEL){
				prev = changed;
				revertValue(changed, evt);
			}
		}
		else if (prev!=null){
			//vil komme hit hvis man har endret og tilbakestilt en parameter
			//ettersom setXxx(...) metodene kan føre til et nytt propertyChangeEvent
			prev = null;
		}else{//helt ny verdi
			if (changed.isIdField(prop)&&evt.getOldValue()==null){
				touchNew(changed);
			}else if (!controller.isEntityChanged(changed)){
				touch(changed);
				controller.addChangedEntity(changed);
			}
		}
	}
	
	private MipssEntityBean<?> cloneEntity(MipssEntityBean<?> entity){
		MipssEntityBean<?> newInstance = null;
		Class<? extends MipssEntityBean> newEntity = entity.getClass();
		try {
			newInstance = newEntity.newInstance();
			List<String> fields = entity.getWritableFieldNames();
			for (String f:fields){
				newInstance.setFieldValue(f, entity.getFieldValue(f));
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return newInstance;
	}
	
	private void revertValue(MipssEntityBean<?> entity, PropertyChangeEvent evt){
		entity.setFieldValue(evt.getPropertyName(), evt.getOldValue());
	}
	
	private static final String ENDRET_AV_FIELD = "endretAv";
	private static final String ENDRET_DATO_FIELD = "endretDato";
	private static final String OWNED_ENTITY_FIELD = "ownedMipssEntity";
	private static final String OPPRETTET_DATO_FIELD = "opprettetDato";
	private static final String OPPRETTET_AV_FIELD = "opprettetAv";
	
	private void touch(MipssEntityBean<?> entity){
		List<String> fields = entity.getWritableFieldNames();
		for (String f:fields){
			if (StringUtils.equals(f, OWNED_ENTITY_FIELD)){
				OwnedMipssEntity t = entity.getFieldValue(OWNED_ENTITY_FIELD);
				t.setEndretAv(loader.getLoggedOnUserSign());
				t.setEndretDato(Clock.now());
			}
			if (StringUtils.equals(f, ENDRET_AV_FIELD)){
				entity.setFieldValue(ENDRET_AV_FIELD, loader.getLoggedOnUserSign());
			}
			if (StringUtils.equals(f, ENDRET_DATO_FIELD)){
				entity.setFieldValue(ENDRET_DATO_FIELD, Clock.now());
			}
		}
	}
	
	private void touchNew(MipssEntityBean<?> entity){
		List<String> fields = entity.getWritableFieldNames();
		for (String f:fields){
			if (StringUtils.equals(f, OWNED_ENTITY_FIELD)){
				OwnedMipssEntity t = entity.getFieldValue(OWNED_ENTITY_FIELD);
				t.setOpprettetAv(loader.getLoggedOnUserSign());
				t.setOpprettetDato(Clock.now());
			}
			if (StringUtils.equals(f, OPPRETTET_AV_FIELD)){
				entity.setFieldValue(OPPRETTET_AV_FIELD, loader.getLoggedOnUserSign());
			}
			if (StringUtils.equals(f, OPPRETTET_DATO_FIELD)){
				entity.setFieldValue(OPPRETTET_DATO_FIELD, Clock.now());
			}
		}
	}
	@Override
	public void listElementsAdded(ObservableList list, int index, int arg2) {
		MipssEntityBean<?> added = (MipssEntityBean<?>)list.get(index);
		added.setNewEntity(true);
		if (!controller.isEntityChanged(added))
			controller.addChangedEntity(added);
	}

	@Override
	public void listElementsRemoved(ObservableList list, int index, List removedElementsList) {
		MipssEntityBean<?> remove = (MipssEntityBean<?>)removedElementsList.get(0);
		if (controller.isEntityChanged(remove)){
			controller.removeChangedEntity(remove);
		}
		
		if (!controller.isEntityRemoved(remove)){
			if (!remove.isNewEntity()){ //trenger ikke slette fra databasen når den enda ikke er persistert
				controller.addRemovedEntity(remove);
			}
		}
	}

	@Override
	public void vetoableChange(PropertyChangeEvent evt)throws PropertyVetoException {}
	@Override
	public void listElementPropertyChanged(ObservableList arg0, int arg1) {
		
	}
	@Override
	public void listElementReplaced(ObservableList arg0, int arg1, Object arg2) {}

	private Button showPkWarning(){
		String title = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.pk.title");
		String msg = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.pk");
		MipssDialogue d = new MipssDialogue(loader.getApplicationFrame(), IconResources.SERIOUS_OK_ICON, title, msg, MipssDialogue.Type.QUESTION, MipssDialogue.Button.YES, MipssDialogue.Button.NO, MipssDialogue.Button.CANCEL);
		Button response = d.askUser();
		return response;
	}
	
	
}
