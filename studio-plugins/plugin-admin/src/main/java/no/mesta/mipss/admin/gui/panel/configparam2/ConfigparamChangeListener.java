package no.mesta.mipss.admin.gui.panel.configparam2;

import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import no.mesta.mipss.admin.gui.main.ConfigParamModule;
import no.mesta.mipss.common.MipssObservableListListener;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.dialogue.MipssDialogue.Button;

import org.jdesktop.observablecollections.ObservableList;

public class ConfigparamChangeListener implements MipssObservableListListener<Konfigparam>{
	private List<Konfigparam> changedList = new ArrayList<Konfigparam>();
	private List<Konfigparam> removedList = new ArrayList<Konfigparam>();
	private List<Konfigparam> addedList = new ArrayList<Konfigparam>();
	
	private final ConfigParamModule plugin;

	
	public ConfigparamChangeListener(ConfigParamModule plugin) {
		this.plugin = plugin;
	}

	@Override
	public void listElementPropertyChanged(ObservableList list, int index) {
		
	}
	@Override
	public void listElementReplaced(ObservableList list, int index, Object arg2) {
		
		
	}
	@Override
	public void listElementsAdded(ObservableList list, int index, int arg2) {
		Konfigparam added = (Konfigparam)list.get(index);
		addedList.add(added);
	}

	@Override
	public void listElementsRemoved(ObservableList list, int index, List removedElementsList) {
		Konfigparam remove = (Konfigparam)removedElementsList.get(0);
		if (addedList.contains(remove)){
			addedList.remove(remove);
		}
		else if (!removedList.contains(remove)){
			if (changedList.contains(remove)){
				changedList.remove(remove);
			}
			removedList.add(remove);
		}
	}
	private Konfigparam prev = null;
	@Override
	public void listElementPropertyChanged(ObservableList<Konfigparam> list, int index, PropertyChangeEvent evt) {
		Konfigparam changed = list.get(index);
		String prop = evt.getPropertyName();
		if ((prop.equals("navn")||prop.equals("applikasjon"))&&prev==null&&evt.getOldValue()!=null){
			Button response = showPkWarning();
			if (response == MipssDialogue.Button.YES){
				Konfigparam newp = new Konfigparam();
				newp.setApplikasjon(changed.getApplikasjon());
				newp.setBeskrivelse(changed.getBeskrivelse());
				newp.setNavn(changed.getNavn());
				newp.setVerdi(changed.getVerdi());
				newp.setVersion(changed.getVersion());
				newp.getOwnedMipssEntity().setOpprettetAv(plugin.getLoader().getLoggedOnUserSign());
				newp.getOwnedMipssEntity().setOpprettetDato(Clock.now());
				
				Konfigparam old = new Konfigparam();
				old.setApplikasjon(evt.getPropertyName().equals("applikasjon")?(String)evt.getOldValue():changed.getApplikasjon());
				old.setNavn(evt.getPropertyName().equals("navn")?(String)evt.getOldValue():changed.getNavn());
				old.setBeskrivelse(changed.getBeskrivelse());
				old.setVersion(changed.getVersion());
				old.setVerdi(changed.getVerdi());
				old.setOwnedMipssEntity(changed.getOwnedMipssEntity());
				if (changedList.contains(old)){
					changedList.remove(old);
				}
				addedList.add(newp);
				removedList.add(old);
			}
			if (response == MipssDialogue.Button.NO){
				if (!changedList.contains(changed)&&!addedList.contains(changed)){
					changed.getOwnedMipssEntity().setEndretAv(plugin.getLoader().getLoggedOnUserSign());
					changed.getOwnedMipssEntity().setEndretDato(Clock.now());
					changedList.add(changed);
				}
			}
			if (response == MipssDialogue.Button.CANCEL){
				prev = changed;
				changed.setApplikasjon(evt.getPropertyName().equals("applikasjon")?(String)evt.getOldValue():changed.getApplikasjon());
				changed.setNavn(evt.getPropertyName().equals("navn")?(String)evt.getOldValue():changed.getNavn());
			}
		}else if (prev!=null){
			//vil komme hit hvis man har endret og tilbakestilt en parameter
			//ettersom kodelinjen changed.setNavn(... fører til et nytt change event.
			prev = null;
		}else{//helt ny verdi
			if (!changedList.contains(changed)&&!addedList.contains(changed)){
				changed.getOwnedMipssEntity().setEndretAv(plugin.getLoader().getLoggedOnUserSign());
				changed.getOwnedMipssEntity().setEndretDato(Clock.now());
				changedList.add(changed);
			}
		}
	}

	@Override
	public void vetoableChange(PropertyChangeEvent evt)
			throws PropertyVetoException {
		
	}
	private Button showPkWarning(){
		Window w = SwingUtilities.windowForComponent(plugin.getModuleGUI());
		String title = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.pk.title");
		String msg = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.pk");
		MipssDialogue d = new MipssDialogue((JFrame)w, IconResources.SERIOUS_OK_ICON, title, msg, MipssDialogue.Type.QUESTION, MipssDialogue.Button.YES, MipssDialogue.Button.NO, MipssDialogue.Button.CANCEL);
		Button response = d.askUser();
		return response;
	}
	

	public List<Konfigparam> getChangedList() {
		return changedList;
	}

	public List<Konfigparam> getRemovedList() {
		return removedList;
	}

	public List<Konfigparam> getAddedList() {
		return addedList;
	}
	
	public void clearLists(){
		changedList.clear();
		removedList.clear();
		addedList.clear();
	}
}