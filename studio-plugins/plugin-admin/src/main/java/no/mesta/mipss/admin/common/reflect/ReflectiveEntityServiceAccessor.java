package no.mesta.mipss.admin.common.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.exception.ServiceException;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.entityservice.exception.EntityServiceException;
import no.mesta.mipss.persistence.IRenderableMipssEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * <p>Dynamisk utførende kall til entity service basert på reflection.</p>
 * 
 * <p>Skjermer klienter for reflection API og lavnivå exceptions.</p>
 * 
 * @author Christian Wiik (Mesan)
 */
public class ReflectiveEntityServiceAccessor {
	
	private MipssEntityService entityService;
	private Logger log = LoggerFactory.getLogger(ReflectiveEntityServiceAccessor.class);
	
	public ReflectiveEntityServiceAccessor() {
		entityService = BeanUtil.lookup(MipssEntityService.BEAN_NAME, MipssEntityService.class);
	}
	
	/**
	 * Metoden kaller metoden angitt ved <tt>methodName</tt> med parameter <tt>listParam</tt>.
	 * 
	 * @param methodName - metodens navn
	 * @param listParam - parameter til metoden
	 * @return returnObject - eventuelt returobjekt
	 */
	public Object invokeMethodSingleListParameter(String methodName, List<IRenderableMipssEntity> listParam) throws ServiceException, EntityServiceException {
		
		Class[] paramTypesArray = new Class[] { List.class };

		Method method;
		Object returnObject = null;
		
		try {
			
			method = entityService.getClass().getDeclaredMethod(methodName, paramTypesArray);
			returnObject = method.invoke(entityService, listParam);
		
		} catch (EntityServiceException e) {
			log.error(e.getMessage(), e);
			throw e;
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("A SecurityException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (NoSuchMethodException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("A NoSuchMethodException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("An IllegalAccessException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		}catch (InvocationTargetException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("An InvocationTargetException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (RuntimeException e) {
			throw new ServiceException("A RuntimeException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		}
		
		return returnObject;
	}
	
	/**
	 * Metoden kaller metoden angitt ved <tt>methodName</tt> uten parametre.
	 * 
	 * @param methodName - metodens navn
	 * @return returnObject - eventuelt returobjekt
	 */
	public Object invokeMethodZeroParameters(String methodName) {
		
		Method method;
		Object returnObject = null;
		
		try {
			
			method = entityService.getClass().getDeclaredMethod(methodName);
			returnObject = method.invoke(entityService);
			
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("A SecurityException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (NoSuchMethodException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("A NoSuchMethodException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("An IllegalAccessException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (InvocationTargetException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException("An InvocationTargetException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		} catch (RuntimeException e) {
			throw new ServiceException("A RuntimeException was caught while reflectively invoking the method '" + methodName + "' on the interface 'MipssEntityService", e);
		}
		
		return returnObject;
	}
	
}
