package no.mesta.mipss.admin.gui.panel.configparam;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import no.mesta.mipss.admin.gui.main.ConfigParamModule;

/**
 * Panel containing the entire config param GUI.
 * 
 * @author Christian Wiik 
 */
public class ConfigParamGUIPanel extends JPanel{

	private static final long serialVersionUID = 3682296148192618296L;
	private ConfigParamContentPanel contentPanel;
	private ConfigParamModule plugin;

	public ConfigParamGUIPanel(ConfigParamModule plugin) {
		this.plugin = plugin;
		init();
	}
	
	private void init() {
		setLayout(new BorderLayout());
		contentPanel = new ConfigParamContentPanel(plugin);
		add(contentPanel, BorderLayout.CENTER);
	}
	public ConfigParamContentPanel getContentPanel() {
		return contentPanel;
	}

	public void dispose(){
		plugin = null;
		contentPanel.dispose();
	}
}
