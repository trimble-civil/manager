package no.mesta.mipss.admin.common.renderer;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.systemstatus.dto.StatusFlag;

/**
 * Renderer for visning av <tt>StatusFlag</tt> objekt i tabell-celle.
 * 
 * @author Christian Wiik (Mesan)
 */
public class StatusFlagCellRenderer implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		
		StatusFlag flag = (StatusFlag)value;
		
		JPanel cellPanel = new JPanel(new BorderLayout());
		cellPanel.setBackground(flag.getColor());
		
		JLabel flagLabel = new JLabel(flag.getDescription());
		flagLabel.setHorizontalAlignment(SwingConstants.CENTER);
		cellPanel.add(flagLabel, BorderLayout.CENTER);
		
		return cellPanel;
	}

}