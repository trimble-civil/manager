package no.mesta.mipss.admin;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import no.mesta.mipss.persistence.MipssEntityBean;

public class GrunndataModuleGuiDelegate extends JPanel{

	private EntityListPanel entityListPanel;
	private EditTablePanel editTablePanel;
	private GrunndataController grunndataController;
	private final GrunndataModule plugin;
	
	public GrunndataModuleGuiDelegate(GrunndataModule plugin){
		this.plugin = plugin;
		grunndataController = new GrunndataController(this);
		
		initGui();
	}

	private void initGui() {
		entityListPanel = new EntityListPanel(this);
		editTablePanel = new EditTablePanel(grunndataController, plugin);
		
		setLayout(new BorderLayout());
		add(entityListPanel, BorderLayout.WEST);
		add(editTablePanel, BorderLayout.CENTER);
	}
	
	@SuppressWarnings("unchecked")
	public boolean setSelectedClass(TableEnum en){
		try {
			Class<MipssEntityBean<?>> clazz = (Class<MipssEntityBean<?>>) Class.forName(en.getPackageName()+"."+en.name());
			return editTablePanel.setEntity(clazz, en.name());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
	public void reloadTableView(){
		editTablePanel.reloadTable();
	}
}
