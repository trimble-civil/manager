package no.mesta.mipss.admin.rapporter.gui;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class TimestampCellRenderer extends DefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (value!=null){
			String name = value.getClass().getName();
			if (name.equals("oracle.sql.TIMESTAMP")){
				try {
					Method method = value.getClass().getMethod("timestampValue", new Class[]{});
					java.sql.Timestamp timestamp= (java.sql.Timestamp)method.invoke(value, new Object[]{});
					value = timestamp;
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		return renderer;
	}

}
