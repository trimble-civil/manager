package no.mesta.mipss.admin.gui.panel.systemstatus3;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.HorizontalAlignment;

@SuppressWarnings("serial")
public class RingPanel extends JPanel {
	
	private Double maxValue;
	private Double value;
	private Double lastValue;
	private DefaultPieDataset dataset;
	private CustomRingPlot plot;
	private JFreeChart chart;
	private final String title;
	
	public RingPanel(String title, int maxValue){
		this.title = title;
		this.maxValue = new Double(maxValue);
		this.value = 0.0;
		this.lastValue = 0.0;
		createDataset();
		initGui();
	}
	
	
	private void initGui() {
		plot = new CustomRingPlot(dataset);		
		chart = createChart(plot);
		configureTitle();
		configurePlot();
		ChartPanel cp1 = new ChartPanel(chart);
		
		setLayout(new GridLayout());
		add(cp1);
	}
	
	public void configureTitle(){
        TextTitle t = chart.getTitle();
        t.setHorizontalAlignment(HorizontalAlignment.CENTER);
        t.setPaint(Color.DARK_GRAY);
        t.setFont(new Font("Arial", Font.BOLD, 26));
	}

	public void configurePlot(){
		plot.setBackgroundPaint(null);
        plot.setOutlineVisible(false);
        plot.setLabelGenerator(null);
        plot.setSectionPaint("A", Color.ORANGE);
        plot.setSectionPaint("B", new Color(100, 100, 100));
        plot.setSectionDepth(0.08);
        plot.setSectionOutlinesVisible(false);
        plot.setShadowPaint(null);
	}
	
	public void setValue(double value){
		if (value == 0) {
			this.value = 0.1;
		} else{
			this.value = value;
		}	
		dataset.setValue("A", this.value);
		dataset.setValue("B", maxValue - this.value);
		
		if (value == 0) {
			plot.setSectionPaint("A", Color.GREEN);
			plot.setSectionPaint("B", Color.GREEN);
		} else if (value >= 1) {
	        plot.setSectionPaint("A", Color.RED);
	        plot.setSectionPaint("B", new Color(100, 100, 100));
		}
		lastValue = value;
		plot.datasetChanged(null);
	}
	
	private PieDataset createDataset() {
        dataset = new DefaultPieDataset();
        if (value.intValue() <= maxValue.intValue()) {
        	dataset.setValue("A", value);
            dataset.setValue("B", maxValue - value);
		} else {
			dataset.setValue("A", value);
	        dataset.setValue("B", 0);
		}
        return dataset;
    }
	
	private JFreeChart createChart(CustomRingPlot plot){
		JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        return chart;
	}
	
	public Double getLastValue(){
		return lastValue;
	}
}
