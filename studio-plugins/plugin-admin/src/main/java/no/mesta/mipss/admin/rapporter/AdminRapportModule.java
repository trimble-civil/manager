package no.mesta.mipss.admin.rapporter;

import javax.swing.JComponent;

import no.mesta.mipss.admin.rapporter.gui.AdminRapportPanel;
import no.mesta.mipss.plugin.MipssPlugin;

@SuppressWarnings("serial")
public class AdminRapportModule extends MipssPlugin{
	private AdminRapportPanel mainPanel;
	
	@Override
	public JComponent getModuleGUI() {
		return mainPanel;
	}

	@Override
	protected void initPluginGUI() {
		mainPanel = new AdminRapportPanel(this);
	}

	@Override
	public void shutdown() {
		
	}

}
