package no.mesta.mipss.admin.gui.panel.systemstatus3;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import no.mesta.mipss.admin.gui.main.SystemStatusFeilModule;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.resources.images.IconResources;

public class SystemStatusFeilController {

	private final SystemStatusFeilModule plugin;
	private SystemStatusFeilPanel panel;

	public SystemStatusFeilController (SystemStatusFeilModule plugin) {
		this.plugin = plugin;
	}

	public String getFeilkoQuery() {
		return "select count(distinct(tranid$$)) from MOBILEADMIN.c$eq";
	}
	
	public String getInspeksjonerQuery() {
		return "select count(*) from table (MIPSS.valider_pk.oversikt_synkprodpunkt_f) where melding is not null";
	}
	
	public String getFeilmeldingerQuery(){
		return "select count(*) from mipss.feilmelding where behandlet_flagg = 0";
	}
	
	public String getSynkbildeQuery(){
		return "select count(*) from mipss.synkbilde_koe";
	}
	
	public String getRefnummerQuery(){
		return "select count(*) from mipss.refnummer_koe";
	}
	
	
	public double poll(String sql){
		double value = 0;
		try (Connection c = getConnection();
			PreparedStatement rows = c.prepareStatement(sql);
			ResultSet r = rows.executeQuery()) {
			if (r.next()){
				value = r.getDouble(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	private Connection getConnection() {

		String jdbc = KonfigparamPreferences.getInstance().hentVerdiForApp("mipss.admin", "feilko.jdbc");
		String usr = "mipss";
		String pwd = "mipss";
		Connection c=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			c = DriverManager.getConnection(jdbc, usr, pwd);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Feilkø panel:"+e.getMessage());
		}
		return c;
	}
	
	
	public void play(String sound)
	{	    
		try{ 
			
			if (sound.equals("doh.wav")) {
				panel.changeImage(IconResources.HOMER_DOH);
			}else if (sound.equals("ahhh.wav")) {
				panel.changeImage(IconResources.HOMER_AHH);
			}else if (sound.equals("thissucks.wav")) {
				panel.changeImage(IconResources.HOMER_THISSUCKS);
			}else if (sound.equals("hehe.wav")) {
				panel.changeImage(IconResources.HOMER_HAPPY);
			}
			
			URL url = this.getClass().getResource("/sounds/"+sound); 
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url); 
			Clip clip = AudioSystem.getClip(); 
			clip.open(audioInputStream); 
			clip.start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public void setModuleGui(SystemStatusFeilPanel panel){
		this.panel = panel;
	}	
}
