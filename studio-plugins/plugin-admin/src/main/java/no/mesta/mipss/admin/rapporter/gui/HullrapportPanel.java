package no.mesta.mipss.admin.rapporter.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import no.mesta.mipss.admin.rapporter.AdminRapportController;
import no.mesta.mipss.admin.rapporter.AdminRapportModule;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.rapportfunksjoner.Hullrapport;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;
@SuppressWarnings("serial")
public class HullrapportPanel extends JPanel{
	private MipssBeanTableModel<Hullrapport> tblModelRapport;
	private JMipssBeanTable<Hullrapport> tblRapport;
	private JXBusyLabel bsyRapport;
	private JScrollPane scrRapport;
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");
	
	private final AdminRapportController controller;
	private final AdminRapportModule plugin;
	private JButton btnExcel;
	private JButton btnLukk;
	
	public HullrapportPanel(AdminRapportModule plugin, AdminRapportController controller){
		this.plugin = plugin;
		this.controller = controller;
		initComponents();
		initGui();
		
	}

	private void initGui() {
		setLayout(new BorderLayout());
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(btnExcel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,0), 0, 0));
		buttonPanel.add(btnLukk, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0, 0));
		
		add(scrRapport, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	private void initComponents() {
		bsyRapport = new JXBusyLabel();
		bsyRapport.setText(resources.getGuiString("label.henterHullrapport"));
		scrRapport = new JScrollPane();
		
		tblModelRapport = new MipssBeanTableModel<Hullrapport>(Hullrapport.class, "kontraktNummer", "kontraktNavn", "kmOverlapp", "kmHull", "kmUtenfor");
		tblModelRapport.setBeanInstance(controller);
		tblModelRapport.setBeanInterface(AdminRapportController.class);
		tblModelRapport.setBeanMethod("getHullOverlapp");
		tblModelRapport.addTableLoaderListener(new MipssBeanLoaderListener(){
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				bsyRapport.setBusy(false);
				scrRapport.setViewportView(tblRapport);
			}
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {
				bsyRapport.setBusy(true);
				scrRapport.setViewportView(bsyRapport);
			}
		});
		
		MipssRenderableEntityTableColumnModel colModelRapport = new MipssRenderableEntityTableColumnModel(Hullrapport.class, tblModelRapport.getColumns());
		tblRapport  = new JMipssBeanTable<Hullrapport>(tblModelRapport, colModelRapport);
		tblRapport.setFillsViewportWidth(true);
		tblRapport.setDoWidthHacks(false);
		tblRapport.getColumn(0).setPreferredWidth(5);
		tblRapport.getColumn(1).setPreferredWidth(40);
		tblRapport.getColumn(2).setPreferredWidth(5);
		tblRapport.getColumn(3).setPreferredWidth(5);
		
		btnExcel = new JButton(controller.getExportExcelAction(tblRapport));
		btnLukk = new JButton(getLukkAction());
		
	}
	public void loadRapport(){
		tblModelRapport.loadData();
	}
	
	private Action getLukkAction(){
		return new AbstractAction("Lukk", IconResources.ABORT_ICON){

			@Override
			public void actionPerformed(ActionEvent e) {
				Window window = SwingUtilities.windowForComponent(HullrapportPanel.this);
				window.dispose();
			}
		};
	}
	
}
