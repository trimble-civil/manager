package no.mesta.mipss.admin.common.editor;

import java.awt.Component;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTable;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.entityservice.MipssGrunndataService;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.table.AbstractCellEditor;

/**
 * Editor for editering av <tt>IRenderableMipssEntity</tt> objekt i tabell-celle.
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link IRenderableMipssEntity#getTextForGUI() }
 */
public class EntityCellEditor extends AbstractCellEditor {
	
	private List<MipssEntityBean<?>> entities;
	private final Class<? extends IRenderableMipssEntity> entityClass;
	
	public EntityCellEditor(Class<? extends IRenderableMipssEntity> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		
		JComboBox comboBox = new JComboBox();
		entities = getEntities(entityClass);
		//if(value != null) {
			comboBox = (JComboBox)super.getTableCellEditorComponent(table, value, isSelected, row, column);
			comboBox.removeAllItems();
						
			for(IRenderableMipssEntity entity : entities) {
				comboBox.addItem(entity);
			}
			
			comboBox.setSelectedItem(value);
		//}
		
		comboBox.setRenderer(new MipssListCellRenderer<IRenderableMipssEntity>());
		
		return comboBox;
	}
	
	/**
	 * Henter liste med entiteter.
	 * 
	 * @param entityClass - klasse type for enititeter som skal hentes.
	 * @return entityList - liste med enititeter.
	 */
	private List<MipssEntityBean<?>> getEntities(Class<? extends IRenderableMipssEntity> entityClass) {
		MipssGrunndataService bean = BeanUtil.lookup(MipssGrunndataService.BEAN_NAME, MipssGrunndataService.class);
		String clazz = entityClass.getName().substring(entityClass.getName().lastIndexOf('.')+1);
		List<MipssEntityBean<?>> entityList = bean.getEntityList(clazz);
		return entityList;
//		String listMethod = Properties.LIST_PREFIX.getPropertyName() + entityClass.getSimpleName() + Properties.LIST_SUFFIX.getPropertyName();
//		ReflectiveEntityServiceAccessor entityServiceAccessor = new ReflectiveEntityServiceAccessor();
//		List<IRenderableMipssEntity> entityList = (Vector<IRenderableMipssEntity>)entityServiceAccessor.invokeMethodZeroParameters(listMethod);
//		return entityList;
	}

}