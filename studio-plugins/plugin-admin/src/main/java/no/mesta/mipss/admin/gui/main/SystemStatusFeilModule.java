package no.mesta.mipss.admin.gui.main;

import javax.swing.JComponent;

import no.mesta.mipss.admin.gui.panel.systemstatus3.SystemStatusFeilController;
import no.mesta.mipss.admin.gui.panel.systemstatus3.SystemStatusFeilPanel;
import no.mesta.mipss.plugin.MipssPlugin;

public class SystemStatusFeilModule extends MipssPlugin {

	private static final long serialVersionUID = -1327898183788929615L;
	
	private SystemStatusFeilController controller;
	private SystemStatusFeilPanel moduleGui;

	@Override
	public JComponent getModuleGUI() {
		return moduleGui;
	}

	@Override
	protected void initPluginGUI() {
		controller = new SystemStatusFeilController(this);
		moduleGui = new SystemStatusFeilPanel(this);
		controller.setModuleGui(moduleGui);		
	}
	
	public SystemStatusFeilController getController(){
		return controller;
	}

	@Override
	public void shutdown() {
		moduleGui.stopJobs();
	}
}
