package no.mesta.mipss.admin.gui.panel.systemstatus2.items;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import no.mesta.mipss.admin.gui.panel.systemstatus2.SurveillanceItem;
import no.mesta.mipss.core.KonfigparamPreferences;

/**
 * Overvåking av datekweb
 * @author harkul
 *
 */
public class SurveillanceDatekItem extends SurveillanceItem{

	@Override
	public void checkStatus() {
		String urlStr = KonfigparamPreferences.getInstance().hentEnForApp("mipss.admin", "datek.server.url").getVerdi();
		try {
			URL url = new URL(urlStr);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.connect();
			connection.disconnect();
			setState(SurveillanceItem.STATE.OK);
		} catch (MalformedURLException e) {
			setState(SurveillanceItem.STATE.FAILED);
		} catch (IOException e) {
			setState(SurveillanceItem.STATE.FAILED);
		}		
	}

}
