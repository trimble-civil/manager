package no.mesta.mipss.admin.gui.panel.systemstatus3;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.plot.PiePlotState;
import org.jfree.chart.plot.RingPlot;
import org.jfree.data.general.PieDataset;
import org.jfree.text.TextUtilities;
import org.jfree.ui.TextAnchor;

@SuppressWarnings("serial")
public class CustomRingPlot extends RingPlot{

	private Font centerTextFont; 

    private Color centerTextColor;
    
    private PieDataset dataset;

    public CustomRingPlot(PieDataset dataset) {
        super(dataset);
        updateDataset(dataset);
        this.centerTextFont = new Font(Font.SANS_SERIF, Font.BOLD, 54);
        this.centerTextColor = Color.DARK_GRAY;
    }

    @Override
    protected void drawItem(Graphics2D g2, int section, 
            Rectangle2D dataArea, PiePlotState state, int currentPass) {
        super.drawItem(g2, section, dataArea, state, currentPass);
        if (currentPass == 1 && section == 0) {
            Number n = this.getDataset().getValue(section);
            g2.setFont(this.centerTextFont);
            g2.setPaint(this.centerTextColor);
            if (n.doubleValue() == 0.1) {
            	TextUtilities.drawAlignedString("0", g2, 
                        (float) dataArea.getCenterX(), 
                        (float) dataArea.getCenterY(),  
                        TextAnchor.CENTER);
			} else {
				TextUtilities.drawAlignedString(n.intValue() + "", g2, 
	                    (float) dataArea.getCenterX(), 
	                    (float) dataArea.getCenterY(),  
	                    TextAnchor.CENTER);
			}           
        }
    }
    
    public void updateDataset(PieDataset dataset){
    	this.dataset = dataset;
    }
}
