package no.mesta.mipss.admin.gui.panel.systemstatus;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Panel containing the different system status panels.
 * 
 * @author Christian Wiik 
 */
public class SystemStatusContentPanel extends JPanel {

	private static final long serialVersionUID = -3941397215595803831L;
	
	public SystemStatusContentPanel() {
		init();
	}
	
	private void init() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		Box boxOne = new Box(BoxLayout.X_AXIS);
		boxOne.add(new SystemStatusDatabasePanel());
		boxOne.add(new SystemStatusDatekPanel());
		boxOne.setBorder(BorderFactory.createLineBorder(this.getBackground(), 3));
		
		Box boxTwo = new Box(BoxLayout.X_AXIS);
		boxTwo.add(new SystemStatusKartPanel());
		boxTwo.add(new SystemStatusKvernemotorPanel());
		boxTwo.setBorder(BorderFactory.createLineBorder(this.getBackground(), 3));
		
		Box boxThree = new Box(BoxLayout.X_AXIS);
		boxThree.add(new SystemStatusM2MPanel());
		boxThree.add(new SystemStatusMobilePanel());
		boxThree.setBorder(BorderFactory.createLineBorder(this.getBackground(), 3));
		
		add(boxOne);
		add(boxTwo);
		add(boxThree);
	}

}