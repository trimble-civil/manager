package no.mesta.mipss.admin.gui.panel.systemstatus2;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.SwingUtilities;

import no.mesta.mipss.admin.gui.main.SystemStatusModule;
import no.mesta.mipss.admin.gui.panel.systemstatus2.items.SurveillanceDatekItem;
import no.mesta.mipss.core.KonfigparamPreferences;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.SchedulerException;

public class SystemStatusController {

	private KonfigparamPreferences konfigParam = KonfigparamPreferences.getInstance();
	private final SystemStatusModule plugin;
	
	public SystemStatusController(SystemStatusModule plugin){
		this.plugin = plugin;
		
	}
	/**
	 * Henter spørring for å hente ut antall strekninger pr minutt
	 * @return
	 */
	public String getStrekningQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.kvern");
	}
	/**
	 * Henter spørring for å hente ut rundetiden til kvernemotor1
	 * @return
	 */
	public String getRundetidQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.rundetid");
	}
	/**
	 * Henter spørring for å hente ut rundetiden til kvernemotor2
	 * @return
	 */
	public String getRundetid2Query(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.rundetid2");
	}
	
	public String getRundetid3Query(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.rundetid3");
	}
	/**
	 * Spørring som for å hente ut antall tiltak pr minutt
	 * @return
	 */
	public String getTiltakQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.tiltak");
	}
	/**
	 * Henter spørring for å hente ut antall prodpunkt pr minutt
	 * @return
	 */
	public String getProdpunktQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.prodpunkt");
	}
	
	/**
	 * Henter spørring for å hente data til tabellen
	 * @return
	 */
	public String getTableQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.table");
		
	}
	
	public int getKvernemotorInterval(){
		return Integer.parseInt(konfigParam.hentVerdiForApp("mipss.admin", "kvernemotorSjekk"));
	}
	
	public String getKvern1OkQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.kvern1OK");
	}
	
	public String getKvern2OkQuery(){
		return konfigParam.hentVerdiForApp("mipss.admin", "query.kvern2OK");
	}

	
	public SurveillanceItem createDatekItem(){
		final SurveillanceItem item = new SurveillanceDatekItem();
		item.setName("Datekweb");
		return item;
	}
	/**
	 * Send spørring til databasen, kjøres på edt
	 * @param sql
	 * @return
	 */
	public double poll(String sql){
		double value = 0;
		try (Connection c = plugin.getLoader().getConnection();
			PreparedStatement rows = c.prepareStatement(sql);
			ResultSet r = rows.executeQuery()) {
			if (r.next()){
				value = r.getDouble(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	public void pauseKverneJob(JobKey jobKey) {
		((SystemStatusPanel) plugin.getModuleGUI()).pauseKverneJob(jobKey);
		playSound("alarm.wav");
	}
	
	public void resumeKverneJob(JobKey jobKey) {
		((SystemStatusPanel) plugin.getModuleGUI()).resumeKverneJob(jobKey);
		
	}
	
	public void playSound(String sound)
	{	    
		try{ 		
			URL url = this.getClass().getResource("/sounds/"+sound); 
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url); 
			Clip clip = AudioSystem.getClip(); 
			clip.open(audioInputStream); 
			clip.start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}