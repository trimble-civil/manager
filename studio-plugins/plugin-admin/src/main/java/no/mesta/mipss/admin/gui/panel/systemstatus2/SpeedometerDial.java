package no.mesta.mipss.admin.gui.panel.systemstatus2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;

import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.PropertySetter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.plot.dial.DialBackground;
import org.jfree.chart.plot.dial.DialCap;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.DialTextAnnotation;
import org.jfree.chart.plot.dial.DialValueIndicator;
import org.jfree.chart.plot.dial.StandardDialFrame;
import org.jfree.chart.plot.dial.StandardDialRange;
import org.jfree.chart.plot.dial.StandardDialScale;
import org.jfree.data.Range;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.data.general.SeriesChangeEvent;
import org.jfree.data.general.SeriesChangeListener;
import org.jfree.data.time.DateRange;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;

@SuppressWarnings("serial")
public class SpeedometerDial extends JPanel{
	DefaultValueDataset dataset;
	private Animator currentAnimation;
	private double prevValue;
	private double value;
	private TimeSeries tidDataset;
	private final String text;
	private final int max;
	private int warn;
	private JScrollBar scroll;
	private JFreeChart chart;
	public SpeedometerDial(int max, String text) {
		this(max, 0, text);
	}
	
	public SpeedometerDial(int max, int warn, String text) {
		this.max = max;
		this.warn = warn;
		this.text = text;
		dataset = new DefaultValueDataset(0.0);
		initGui();
	}

	private void initGui() {
		DialPlot plot = new DialPlot();
		plot.setView(0.0, 0.0, 1.0, 1.0);
		plot.setDataset(dataset);
		
		StandardDialFrame dialFrame = new StandardDialFrame();
		dialFrame.setBackgroundPaint(Color.lightGray);
		dialFrame.setForegroundPaint(Color.darkGray);
		plot.setDialFrame(dialFrame);
		
		BufferedImage image = ImageUtil.convertImageToBufferedImage(IconResources.CARBON_PATTERN.getImage());
		TexturePaint tex = new TexturePaint(image, new Rectangle2D.Double(1, 0, 10, 8));
		DialBackground db = new DialBackground(tex);
		plot.setBackground(db);
		
		DialTextAnnotation annotation1 = new DialTextAnnotation(text);
		annotation1.setFont(new Font("Dialog", Font.BOLD, 14));
		annotation1.setRadius(0.7);
		annotation1.setPaint(new Color(205, 255, 239));

		plot.addLayer(annotation1);

		DialValueIndicator dvi = new DialValueIndicator(0);
		dvi.setOutlinePaint(Color.lightGray);
		dvi.setPaint(Color.black);
		
		plot.addLayer(dvi);
		
		int majorTick = max/10;
		
		StandardDialScale scale = new StandardDialScale(0, max, 210, -240, majorTick, 5);
		scale.setTickRadius(0.88);
		scale.setTickLabelOffset(0.15);
		scale.setTickLabelPaint(new Color(230,230,255));
		scale.setMajorTickPaint(Color.white);
		scale.setMinorTickPaint(Color.lightGray);
		
		scale.setTickLabelFont(new Font("Dialog", Font.PLAIN, 14));
		plot.addScale(0, scale);

		double red = max-(max*0.15);
		double orange = (warn!=0?warn:(max-(max*0.30)));
		double min = 0;
		StandardDialRange range = new StandardDialRange(red, max, Color.red);
		range.setInnerRadius(0.52);
		range.setOuterRadius(0.55);
		plot.addLayer(range);

		StandardDialRange range2 = new StandardDialRange(orange, red,Color.orange);
		range2.setInnerRadius(0.52);
		range2.setOuterRadius(0.55);
		plot.addLayer(range2);

		StandardDialRange range3 = new StandardDialRange(min, orange, Color.green);
		range3.setInnerRadius(0.52);
		range3.setOuterRadius(0.55);
		plot.addLayer(range3);

		DialPointer.Pointer needle = new DialPointer.Pointer();
		needle.setFillPaint(Color.lightGray);
		needle.setOutlinePaint(Color.white);
		
		plot.addLayer(needle);

		DialCap cap = new DialCap();
		cap.setRadius(0.10);

		cap.setFillPaint(Color.darkGray);
		cap.setOutlinePaint(tex);
		plot.setCap(cap);

		JFreeChart chart1 = new JFreeChart(plot);
		ChartPanel cp1 = new ChartPanel(chart1);
		cp1.setPreferredSize(new Dimension(255, 210));
		
		tidDataset = new TimeSeries("Tid");
		
		
		JPanel content = new JPanel(new BorderLayout());
		content.add(cp1);
		setLayout(new GridBagLayout());
		JButton btn = new JButton(getButtonAction());
		add(content, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(btn, new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
	}
	
	private void animateToAngle(){
		if (currentAnimation==null||!currentAnimation.isRunning()){
			int traveltime = 500;
			currentAnimation = PropertySetter.createAnimator(traveltime,this, "animValue", new Double[] { prevValue, value});
			currentAnimation.start();
			
		}
	}
	
	public void setAnimValue(double value){
		dataset.setValue(Double.valueOf(value));
	}
	
	public void setValue(double value){
		tidDataset.add(RegularTimePeriod.createInstance(Second.class, new Date(), TimeZone.getDefault()), value);
		this.value = value;
		animateToAngle();
		prevValue = value;
	}
	
	public AbstractAction getButtonAction(){
		return new AbstractAction("Vis graf"){
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame f = new JFrame();
				f.setAlwaysOnTop(true);
				f.add(getPlotPanel());
				f.pack();
				f.setLocationRelativeTo(SpeedometerDial.this);
				f.setVisible(true);
			}
		};
	}
	public JPanel getPlotPanel(){
		if (chart==null){
			TimeSeriesCollection c = new TimeSeriesCollection();
			c.addSeries(tidDataset);
			chart = ChartFactory.createTimeSeriesChart(
					   text,
					   "Tid",
					   "Antall",
					   c,
					   false,
					   true,
					   false);
			chart.setAntiAlias(false);
			
			tidDataset.addChangeListener(new SeriesChangeListener() {
				@Override
				public void seriesChanged(SeriesChangeEvent event) {
					seriesChanges(event, chart);
				}
			});
//			chart.getXYPlot().addChangeListener(new PlotChangeListener() {
//				@Override
//				public void plotChanged(PlotChangeEvent event) {
//					changePlot(event, chart);
//				}
//			});
		}
		ChartPanel cp2 = new ChartPanel(chart);
		cp2.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				mouseReleases(e);
				
			}
		});
		JPanel content = new JPanel(new BorderLayout());
		content.add(cp2);
		scroll = new JScrollBar(JScrollBar.HORIZONTAL);
		scroll.setMaximum(110);
		scroll.setMinimum(0);
		scroll.addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				adjustmenValueChanged(e, chart);
			}
		});
		
		
		long timespan = 1000 * 60 * 60 * 24; //en dag
		DateAxis domainAxis = (DateAxis)chart.getXYPlot().getDomainAxis();
		TimeSeriesDataItem first = tidDataset.getDataItem(0);
		TimeSeriesDataItem last = tidDataset.getDataItem(tidDataset.getItemCount()-1);
		long minimum = first.getPeriod().getFirstMillisecond();
		long maximum = last.getPeriod().getFirstMillisecond();
		if (maximum-minimum>timespan){
			DateRange newRange = new DateRange(maximum-timespan, maximum);
	        domainAxis.setRange(newRange);
		}
		content.add(scroll, BorderLayout.SOUTH);
		return content;
	}
	private void mouseReleases(MouseEvent e){
		XYPlot plot = chart.getXYPlot();
		
		DateAxis domainAxis = (DateAxis)plot.getDomainAxis();
		Range range = domainAxis.getRange();
		double lowerBound = (double)range.getLowerBound();
		
		TimeSeriesDataItem first = tidDataset.getDataItem(0);
		TimeSeriesDataItem last = tidDataset.getDataItem(tidDataset.getItemCount()-1);
		double minimum = first.getPeriod().getFirstMillisecond();
		double maximum = last.getPeriod().getFirstMillisecond();
		double dateRange = maximum-minimum;
		double positionInTime = lowerBound-minimum;
        
		double percent = positionInTime/dateRange*100;
        if (percent<0)
        	percent = 0;
        scroll.setValue((int)percent);
	}
	
	private void seriesChanges(SeriesChangeEvent e, JFreeChart chart){
		if (scroll.getValue()==100){
			recalculateChartRange(scroll, chart);
		}
	}
	private void recalculateChartRange(JScrollBar scroll, JFreeChart chart){
		long lowerPadding = 1000;
		long upperPadding = lowerPadding*2;
		
		TimeSeriesDataItem first = tidDataset.getDataItem(0);
		TimeSeriesDataItem last = tidDataset.getDataItem(tidDataset.getItemCount()-1);
		long minimum = first.getPeriod().getFirstMillisecond();
        long maximum = last.getPeriod().getFirstMillisecond();
        
        long scrollValue = scroll.getValue();
        long dateRange = maximum-minimum;
        long pos = ((dateRange/100)*scrollValue)+minimum;
        
        DateAxis domainAxis = (DateAxis)chart.getXYPlot().getDomainAxis();
        Range range = domainAxis.getRange();
        
        long lowerBound = (long)range.getLowerBound();
        long  upperBound = (long)range.getUpperBound();
        long  rangeBound = (long)upperBound-lowerBound;
        lowerBound = pos;
        upperBound = pos+rangeBound;
        
        if (upperBound>=maximum+upperPadding){
        	upperBound = maximum+upperPadding;
        	lowerBound = maximum-rangeBound+upperPadding;
        }
        
        DateRange newRange = new DateRange(lowerBound-lowerPadding, upperBound-lowerPadding);
        domainAxis.setRange(newRange);
	}
	private void adjustmenValueChanged(AdjustmentEvent e, JFreeChart chart){
		recalculateChartRange(scroll, chart);
	}
}
