package no.mesta.mipss.admin.gui.model;

import java.util.Enumeration;
import java.util.List;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import javax.swing.ListSelectionModel;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;

import no.mesta.mipss.systemstatus.dto.StatusItem;

import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;

/**
 * Column model implementation for system status objects.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SystemStatusColumnModel implements TableColumnModelExt {
	
	private PropertyResourceBundle bundle;
	private DefaultTableColumnModelExt tableColumnModel = new DefaultTableColumnModelExt();
	
	public SystemStatusColumnModel(String... columns) {
        bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("tableColumns");
        
        int columnIndex = 0;
        
        for(String s:columns) {
            String identifier = StatusItem.class.getName() + "." + s;
            TableColumnExt tc = new TableColumnExt();
            String header = null;
            
            try{
                header = bundle.getString(identifier);
            } catch (MissingResourceException e) {
                header = "TODO: " + identifier;
            }
            
            tc.setIdentifier(identifier);
            tc.setModelIndex(columnIndex);
            tc.setHeaderValue(header);

            tableColumnModel.addColumn(tc);
            columnIndex++;
        }
        
//      This code is a bit brittle, but done because of poor API support. The lines below:
//      - Assumes that SimpleStatusItem fields "detail" and "flag" are represented by the the two last (viewed rightmost in the table) columns in the column model
//      - Sets the two columns' max with to a reasonable, fixed-number of pixels that ought to be enough for displaying the data

        int columnCount = tableColumnModel.getColumnCount();
        tableColumnModel.getColumn(columnCount - 1).setMaxWidth(150);
        tableColumnModel.getColumn(columnCount - 2).setMaxWidth(150);
        tableColumnModel.getColumn(columnCount - 3).setMaxWidth(150);
    }

	@Override
	public void addColumnModelListener(TableColumnModelListener listener) {
		tableColumnModel.addColumnModelListener(listener);
	}

	@Override
	public int getColumnCount(boolean includeHidden) {
		return tableColumnModel.getColumnCount(includeHidden);
	}

	@Override
	public TableColumnExt getColumnExt(Object identifier) {
		return tableColumnModel.getColumnExt(identifier);
	}

	@Override
	public TableColumnExt getColumnExt(int columnIndex) {
		return tableColumnModel.getColumnExt(columnIndex);
	}

	@Override
	public List<TableColumn> getColumns(boolean includeHidden) {
		return tableColumnModel.getColumns(includeHidden);
	}

	@Override
	public void addColumn(TableColumn column) {
		tableColumnModel.addColumn(column);
	}

	@Override
	public TableColumn getColumn(int columnIndex) {
		return tableColumnModel.getColumn(columnIndex);
	}

	@Override
	public int getColumnCount() {
		return tableColumnModel.getColumnCount();
	}

	@Override
	public int getColumnIndex(Object columnIdentifier) {
		return tableColumnModel.getColumnIndex(columnIdentifier);
	}

	@Override
	public int getColumnIndexAtX(int position) {
		return tableColumnModel.getColumnIndexAtX(position);
	}

	@Override
	public int getColumnMargin() {
		return tableColumnModel.getColumnMargin();
	}

	@Override
	public boolean getColumnSelectionAllowed() {
		return tableColumnModel.getColumnSelectionAllowed();
	}

	@Override
	public Enumeration<TableColumn> getColumns() {
		return tableColumnModel.getColumns();
	}

	@Override
	public int getSelectedColumnCount() {
		return tableColumnModel.getSelectedColumnCount();
	}

	@Override
	public int[] getSelectedColumns() {
		return tableColumnModel.getSelectedColumns();
	}

	@Override
	public ListSelectionModel getSelectionModel() {
		return tableColumnModel.getSelectionModel();
	}

	@Override
	public int getTotalColumnWidth() {
		return tableColumnModel.getTotalColumnWidth();
	}

	@Override
	public void moveColumn(int columnIndex, int newIndex) {
		tableColumnModel.moveColumn(columnIndex, newIndex);
	}

	@Override
	public void removeColumn(TableColumn column) {
		tableColumnModel.removeColumn(column);
	}

	@Override
	public void removeColumnModelListener(TableColumnModelListener listener) {
		tableColumnModel.removeColumnModelListener(listener);
	}

	@Override
	public void setColumnMargin(int newMargin) {
		tableColumnModel.setColumnMargin(newMargin);
	}

	@Override
	public void setColumnSelectionAllowed(boolean flag) {
		tableColumnModel.setColumnSelectionAllowed(flag);
	}

	@Override
	public void setSelectionModel(ListSelectionModel newModel) {
		tableColumnModel.setSelectionModel(newModel);
	}

}