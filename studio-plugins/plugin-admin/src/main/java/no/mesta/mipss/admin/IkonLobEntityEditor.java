package no.mesta.mipss.admin;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;

import no.mesta.mipss.core.ImageFileFilter;
import no.mesta.mipss.persistence.ImageUtil;

@SuppressWarnings("serial")
public class IkonLobEntityEditor extends AbstractCellEditor implements TableCellEditor, ActionListener{ 

	private JButton editorButton = new JButton("...");
	private JTable table;
	private int row;
	private int column;
	private byte[] value;
	public IkonLobEntityEditor(){
		editorButton.addActionListener(this);
		editorButton.setMargin(new Insets(0,2,0,2));
	}
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		this.table = table; 
        this.row = row; 
        this.column = column; 
        
		JPanel panel = new JPanel(new GridBagLayout());
	
        JLabel label = new JLabel();
        panel.setOpaque(false);
        if (isSelected)
        	label.setBackground(table.getSelectionBackground());
        
        if (value instanceof byte[]){
        	this.value = (byte[]) value;
			BufferedImage image = ImageUtil.bytesToImage((byte[])value);
			ImageIcon i = new ImageIcon(image);
			if (i.getIconHeight()>30||i.getIconWidth()>30){
				i = new ImageIcon(ImageUtil.scaleImageFast(i, 30, 30));
			}
			
			label.setText("");
			label.setIcon(i);
		}
    	panel.add(label, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		panel.add(editorButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,2,0), 0,0));
		return panel;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		cancelCellEditing(); 
        editCell(table, row, column);
        
	}
	
	private void editCell(JTable table, int row, int column){
		JFileChooser chooser = new JFileChooser();
		chooser.addChoosableFileFilter(new ImageFileFilter());
		int result = chooser.showOpenDialog(SwingUtilities.windowForComponent(table));
		if (result==JFileChooser.APPROVE_OPTION){
			File file = chooser.getSelectedFile();
			byte[] b = new byte[(int) file.length()];
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				fileInputStream.read(b);
				table.setValueAt(b, row, column);
				this.value = b;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}

	@Override
	public Object getCellEditorValue() {
		return value;
	}
}
