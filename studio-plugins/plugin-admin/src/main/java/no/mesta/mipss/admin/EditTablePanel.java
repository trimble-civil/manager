package no.mesta.mipss.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.dokarkiv.Ikon;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.dialogue.MipssDialogue.Button;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

public class EditTablePanel extends JPanel implements MipssBeanLoaderListener{

	private final GrunndataController controller;
	private ButtonPanel buttonPanel;
	private JPanel tablePanel;
	private JPanel emptyPanel;
	private JPanel waitPanel;
	private JXBusyLabel waitLabel; 
	private JScrollPane tableScroll;
	private GrunndataEntityChangeListener changeListener;
	
	private JMipssBeanTable<MipssEntityBean<?>> beanTable;
	private MipssBeanTableModel<MipssEntityBean<?>> tableModel;
	private MipssRenderableEntityTableColumnModel columnModel;
	private final GrunndataModule plugin;
	private MipssObservableList<MipssEntityBean<?>> observableList;
	private ColorHighlighter idFieldsHightlighter;
	private Class<MipssEntityBean<?>> tableClass;
	private String className;
	
	public EditTablePanel(GrunndataController controller, GrunndataModule plugin){
		this.controller = controller;
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui() {
		setLayout(new BorderLayout());
		waitPanel = new JPanel();
		waitLabel = new JXBusyLabel();
		waitLabel.setText("Vennligst vent, henter data...");
		waitPanel.add(waitLabel);
		waitLabel.setBusy(true);
		
		tablePanel = new JPanel(new BorderLayout());
		buttonPanel = new ButtonPanel(controller, this);
		
		emptyPanel = new JPanel();
		tablePanel.add(emptyPanel);

		add(tablePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
	}
	private Button showSaveWarning(){
		String title = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.continue.title");
		String msg = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.continue");
		MipssDialogue d = new MipssDialogue(plugin.getLoader().getApplicationFrame(), IconResources.SERIOUS_OK_ICON, title, msg, MipssDialogue.Type.QUESTION, MipssDialogue.Button.OK, MipssDialogue.Button.CANCEL);
		Button response = d.askUser();
		return response;
	}
	public void createNewEntity(){
		MipssEntityBean<?> bean;
		try {
			bean = tableClass.newInstance();
			bean.setNewEntity(true);
			observableList.add(bean);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	public void removeSelectedEntity(){
		List<MipssEntityBean<?>> selectedEntities = beanTable.getSelectedEntities();
		if (selectedEntities!=null&&selectedEntities.size()==1){
			observableList.remove(selectedEntities.get(0));
		}
	}
	
	public void reloadTable(){
		setEntity(tableClass, className);
	}
	public boolean setEntity(Class<MipssEntityBean<?>> tableClass, String className){
		
		if (!controller.isListsEmpty()){
			Button response = showSaveWarning();
			if (response != MipssDialogue.Button.OK){
				return false;
			}
		}
		controller.clearChanges();
		this.tableClass = tableClass;
		this.className = className;
		try {
			MipssEntityBean<?> bean = tableClass.newInstance();
			List<String> fields = bean.getWritableFieldNamesWithoutRelationsAnd("endretAv", "endretDato", "opprettetAv", "opprettetDato");
			int[] editable = new int[fields.size()];
			for (int i = 0;i<editable.length;i++){
				editable[i]= i;
			}
			List<Integer> ids = new ArrayList<Integer>();
			int count = 0;
			List<String> fieldNames = new ArrayList<String>();
			for (String s:fields){
				if (bean.isIdField(s)){
					fieldNames.add(0, s);
					ids.add(count);
					count++;
				}
				else{
					fieldNames.add(s);
				}
			}
						
			tableModel = new MipssBeanTableModel<MipssEntityBean<?>>(tableClass, editable, fieldNames.toArray(new String[]{}));
			tableModel.setBeanInstance(controller);
			tableModel.setBeanInterface(GrunndataController.class);
			tableModel.setBeanMethod("getListForEntity");
			tableModel.setBeanMethodArguments(new Class[]{String.class});
			tableModel.setBeanMethodArgValues(new Object[]{className});
			tableModel.addTableLoaderListener(this);
			
			columnModel = new MipssRenderableEntityTableColumnModel(tableClass, fieldNames.toArray(new String[]{}));
			if (beanTable==null){
				createBeanTable();
				tablePanel.remove(emptyPanel);
				tableScroll = new JScrollPane(beanTable);
				tablePanel.add(tableScroll);
				tablePanel.updateUI();
			}
			int[] idFields = new int[ids.size()];
			for (int i=0;i<ids.size();i++){
				idFields[i]=ids.get(i);
			}
			if (idFieldsHightlighter!=null)
				beanTable.removeHighlighter(idFieldsHightlighter);
			Color forground = beanTable.getSelectionForeground();
			Color background = beanTable.getSelectionBackground();
			idFieldsHightlighter = new ColorHighlighter(new HighlightPredicate.ColumnHighlightPredicate(idFields), new Color(220,220,220), Color.black, background, forground);
			beanTable.addHighlighter(idFieldsHightlighter);

			tableModel.loadData();
			
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return true;
		
	}
	
	private void createBeanTable(){
		beanTable = new JMipssBeanTable<MipssEntityBean<?>>(tableModel, columnModel);
		beanTable.setDoWidthHacks(true);
		beanTable.setFillsViewportHeight(true);
		beanTable.setFillsViewportWidth(true);
		beanTable.setEditable(true);
		beanTable.setRowHeightEnabled(true);
		beanTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		beanTable.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnAdded(TableColumnModelEvent e) {}
            public void columnRemoved(TableColumnModelEvent e) {}
            public void columnMoved(TableColumnModelEvent e) {}
            public void columnSelectionChanged(ListSelectionEvent e) {}
            public void columnMarginChanged(ChangeEvent e) {
                for (int row = 0; row < beanTable.getRowCount(); row++) {
                    int rowHeight = 0;
                    for (int column = 0; column < beanTable.getColumnCount(); column++) {
                        Component comp = beanTable.prepareRenderer(beanTable.getCellRenderer(row, column), row, column);
                        rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
                    }
                    beanTable.setRowHeight(row, rowHeight); 
                }
            }
        });
	    for(EntityRenderEnum entityArtifact : EntityRenderEnum.values()) {
	    	beanTable.setDefaultRenderer(entityArtifact.getEntityClass(), entityArtifact.getCellRenderer());
	    	beanTable.setDefaultEditor(entityArtifact.getEntityClass(), entityArtifact.getCellEditor());
	    }
	    setRenderersAndEditors();
	}

	public void setRenderersAndEditors(){
		beanTable.setDefaultRenderer(byte[].class, new IkonLobEntityRenderer());
	    beanTable.setDefaultEditor(byte[].class, new IkonLobEntityEditor());
	    beanTable.setDefaultRenderer(Ikon.class, new IkonTableComboBoxRenderer());
	    beanTable.setDefaultEditor(Ikon.class, new IkonTableComboBoxEditor());
	}
	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		
		observableList = new MipssObservableList<MipssEntityBean<?>>(tableModel.getEntities(), true);
		changeListener = new GrunndataEntityChangeListener(plugin.getLoader(), observableList, controller);
		tableModel.setEntities(observableList);
		
		beanTable.setModel(tableModel);
		beanTable.setColumnModel(columnModel);
		beanTable.packAll();
		
		if (tableClass.equals(Ikon.class)){
			setRenderersAndEditors();
		}
		
		tablePanel.remove(waitPanel);
		tablePanel.add(tableScroll);
		tablePanel.updateUI();
		
	}

	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		tablePanel.remove(tableScroll);
		tablePanel.add(waitPanel);
		tablePanel.updateUI();
	}
}
