package no.mesta.mipss.admin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import no.mesta.mipss.resources.images.IconResources;

public class EntityListPanel extends JPanel{
	
	private JTree entityTree;
	private final GrunndataModuleGuiDelegate guiDelegate;
	
	public EntityListPanel(GrunndataModuleGuiDelegate guiDelegate){
		this.guiDelegate = guiDelegate;
		initGui();
		
	}

	private void initGui() {
		setLayout(new BorderLayout());
		entityTree = createTree();
		add(new JScrollPane(entityTree));
		
		entityTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		entityTree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener(){
			private boolean treeSelectionListenerEnabled = true;
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				if (treeSelectionListenerEnabled) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) entityTree.getLastSelectedPathComponent();
					if (node.isLeaf()){
						boolean setSelectedClass = guiDelegate.setSelectedClass((TableEnum)node.getUserObject());
						if (!setSelectedClass){
							TreePath treePath = e.getOldLeadSelectionPath();
							treeSelectionListenerEnabled = false;
		                    try {
		                        entityTree.setSelectionPath(treePath);
		                    } finally {
		                        treeSelectionListenerEnabled = true;
		                    }
						}
					}else{
						TreePath treePath = e.getOldLeadSelectionPath();
						treeSelectionListenerEnabled = false;
	                    try {
	                        entityTree.setSelectionPath(treePath);
	                    } finally {
	                        treeSelectionListenerEnabled = true;
	                    }
					}
				}
			}
		});
	}
	
	private JTree createTree(){
		final DefaultMutableTreeNode root = new DefaultMutableTreeNode("Grunndatatabeller");
		
		TableEnum[] values = TableEnum.values();
		Map<String, String> packages = TableEnum.getPackages();
		Set<String> pck = new TreeSet<String>(packages.keySet());		
		
		for (String p:pck){
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(p);
			for (TableEnum e:values){
				if (e.getPackageName().equals(packages.get(p))){
					DefaultMutableTreeNode sub = new DefaultMutableTreeNode(e);
					node.add(sub);
				}
			}
			root.add(node);
		}
		
		JTree tree = new JTree(root);
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer(){
			@Override
			public Component getTreeCellRendererComponent(JTree tree, Object value,
					  boolean sel,
					  boolean expanded,
					  boolean leaf, int row,
					  boolean hasFocus) {
				JLabel l = (JLabel)super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
				
				if (leaf){
					l.setIcon(IconResources.TABLE3_ICON);
				}else{
					if (node==root)
						l.setIcon(IconResources.TABLES_ICON);
					else{
						l.setIcon(IconResources.TABLE3_PCK_ICON);
					}
				}
				return l;
			}
		};
		renderer.setLeafIcon(IconResources.TABLE3_ICON);
		renderer.setOpenIcon(IconResources.TABLES_ICON);
		renderer.setClosedIcon(IconResources.TABLES_ICON);
		tree.setCellRenderer(renderer);
		int rowCount = tree.getRowCount();
		for (int i=0;i<rowCount;i++){
			tree.expandRow(i);
			rowCount = tree.getRowCount();
		}
		
		return tree;
	}
}
