package no.mesta.mipss.admin.rapporter.gui.statistikk;

import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

@SuppressWarnings("serial")
public class KontraktTableObject implements CheckBoxTableObject{
	private Driftkontrakt kontrakt;
	private boolean valgt;
	public KontraktTableObject(Driftkontrakt kontrakt){
		this.kontrakt = kontrakt;
		
	}
	
	public String getKontraktnummer(){
		return kontrakt.getKontraktnummer();
	}
	public String getKontraktnavn(){
		return kontrakt.getKontraktnavn();
	}
	
	public Driftkontrakt getKontrakt(){
		return kontrakt;
	}
	@Override
	public String getTextForGUI() {
		return null;
	}

	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
		
	}

	@Override
	public Boolean getValgt() {
		return valgt;
	}

}