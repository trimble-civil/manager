package no.mesta.mipss.admin;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellEditor;

import no.mesta.mipss.admin.common.enumeration.Properties;
import no.mesta.mipss.admin.common.reflect.ReflectiveEntityServiceAccessor;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Ikon;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

public class IkonTableComboBoxEditor extends AbstractCellEditor implements TableCellEditor{ 
	
	private List<Ikon> entities;
	private JComboBox comboBox;
	private MipssComboBoxModel model;
	
	public IkonTableComboBoxEditor(){
		entities = getEntities(Ikon.class);
		model = new MipssComboBoxModel(entities, true);
		comboBox = new JComboBox();
		comboBox.setRenderer(new ComboBoxRenderer());
		comboBox.setModel(model);
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		
		comboBox.setSelectedItem(value);
		return comboBox;
	}
	
	class ComboBoxRenderer extends JLabel implements ListCellRenderer {

		public ComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
		}
		
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}
			Ikon ikon =(Ikon)value;
			if (ikon!=null){
				BufferedImage image = ImageUtil.bytesToImage(ikon.getIkonLob());
				ImageIcon i = new ImageIcon(image);
				if (i.getIconHeight()>30||i.getIconWidth()>30){
					i = new ImageIcon(ImageUtil.scaleImageFast(i, 30, 30));
				}
				setIcon(i);
				setText("");
			}else{
				setText("< ikke valgt >");
				setIcon(null);
			}
			
			return this;
		}
	}
	/**
	 * Henter liste med entiteter.
	 * 
	 * @param entityClass - klasse type for enititeter som skal hentes.
	 * @return entityList - liste med enititeter.
	 */
	@SuppressWarnings("unchecked")
	private List<Ikon> getEntities(Class<Ikon> entityClass) {
		String listMethod = Properties.LIST_PREFIX.getPropertyName() + entityClass.getSimpleName() + Properties.LIST_SUFFIX.getPropertyName();
		ReflectiveEntityServiceAccessor entityServiceAccessor = new ReflectiveEntityServiceAccessor();
		List<Ikon> entityList = (Vector<Ikon>)entityServiceAccessor.invokeMethodZeroParameters(listMethod);
		return entityList;
	}

	@Override
	public Object getCellEditorValue() {
		return comboBox.getSelectedItem();
	}

}