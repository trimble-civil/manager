package no.mesta.mipss.admin.gui.panel.systemstatus2.items;

import java.util.Date;

import javax.swing.JOptionPane;

import no.mesta.mipss.admin.gui.panel.systemstatus2.SystemStatusController;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.resources.images.IconResources;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class KvernOkJob implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		JobDataMap map = context.getJobDetail().getJobDataMap();
		String sql = (String)map.get("sql");
		SystemStatusController controller = (SystemStatusController)map.get("controller");
		String jobName = context.getJobDetail().getKey().getName();
		
		
		double strekninger = controller.poll(sql);
	
		if (strekninger == 0) {
			String kvernemotor = "1";
			int minutes = map.getInt("minutes");
			if (jobName.contains("2")) {
				kvernemotor = "2";
			}
			StringBuilder sb = new StringBuilder();
			sb.append("<html><b>KVERNEMOTOR " + kvernemotor + " HAR STOPPET!</b></html>\n\n");
			sb.append("<html><b>DATO: " + MipssDateFormatter.formatDate(new Date(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT) + "</b></html>\n\n");
			sb.append("<html><b>DET ER IKKE GENERERT STREKNINGER FOR KVERN " + kvernemotor + " DE SISTE " + minutes + " MINUTTER</b></html>\n\n");
			controller.pauseKverneJob(context.getJobDetail().getKey());
			int choice = JOptionPane.showConfirmDialog(null, sb.toString(), "KVERNEMOTOR " + kvernemotor + " ER OK", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, IconResources.WARNING_128);
			if (choice == JOptionPane.OK_OPTION || choice == JOptionPane.CANCEL_OPTION) {
				controller.resumeKverneJob(context.getJobDetail().getKey());
			}
		}
	}
}
