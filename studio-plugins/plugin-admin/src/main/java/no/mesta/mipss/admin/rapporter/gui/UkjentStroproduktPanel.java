package no.mesta.mipss.admin.rapporter.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.admin.rapporter.AdminRapportController;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

import org.jdesktop.swingx.JXDatePicker;

public class UkjentStroproduktPanel extends JPanel{
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");

	private JLabel lblFra;
	private JLabel lblTil;
	private JXDatePicker fraDatoPicker;
	private JXDatePicker tilDatoPicker;
	private JButton btnHentRapport;

	private final AdminRapportController controller;
	
	public UkjentStroproduktPanel(AdminRapportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}

	private void initComponents() {
		
		lblFra = new JLabel(resources.getGuiString("label.fra"));
		lblTil = new JLabel(resources.getGuiString("label.til"));
		
		fraDatoPicker = new JXDatePicker(CalendarUtil.getDaysAgo(1));
		fraDatoPicker.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (fraDatoPicker.getDate().after(tilDatoPicker.getDate())) {
					fraDatoPicker.setDate(tilDatoPicker.getDate());
				}
				
			}
		});
		tilDatoPicker = new JXDatePicker(new Date());
		tilDatoPicker.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tilDatoPicker.getDate().before(fraDatoPicker.getDate())) {
					tilDatoPicker.setDate(fraDatoPicker.getDate());
				}				
			}
		});
		DatePickerConstraintsUtils.setNotBeforeDate(controller.getPlugin().getLoader(), fraDatoPicker, tilDatoPicker);
		
		btnHentRapport = new JButton(controller.getUkjentStroproduktAction(fraDatoPicker, tilDatoPicker));
	}

	private void initGui() {

		setLayout(new GridBagLayout());
		
		add(lblFra, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        add(fraDatoPicker, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        add(lblTil, 		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        add(tilDatoPicker, 	new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        add(btnHentRapport, new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));		
	}
}
