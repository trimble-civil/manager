package no.mesta.mipss.admin.gui.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.PropertyUtil;
import no.mesta.mipss.systemstatus.MipssSystemStatusService;
import no.mesta.mipss.systemstatus.dto.StatusItem;
import no.mesta.mipss.systemstatus.dto.SystemStatus;
import no.mesta.mipss.systemstatus.service.SystemStatusServiceStub;

/**
 * Table model implementation for system status objects.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SystemStatusTableModel implements TableModel {
	
	private MipssSystemStatusService service;
	private SystemStatusServiceStub serviceStub;
	
	private String[] columns;
	private List<StatusItem> items;
	private List<TableModelListener> tableModelListeners;
	private SystemStatus systemStatus;
	
	public SystemStatusTableModel(SystemStatusComponentEnum system) {
		this.columns = new String[] {"item", "status", "detail", "flag"};
		this.tableModelListeners = new ArrayList<TableModelListener>();
		this.service = BeanUtil.lookup(MipssSystemStatusService.BEAN_NAME, MipssSystemStatusService.class);
		this.serviceStub = new SystemStatusServiceStub();
		this.items = getSystemStatus(system).getStatusItems();
	}
	
	@Override
	public void addTableModelListener(TableModelListener listener) {
		this.tableModelListeners.add(listener);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return PropertyUtil.getBeanFieldType(StatusItem.class, columns[columnIndex]);
	}

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columns[columnIndex];
	}
	
	public String[] getColumnNames() {
		return columns;
	}

	@Override
	public int getRowCount() {
		return items==null?0:items.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return getValueFromField(items.get(rowIndex), columns[columnIndex]);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener listener) {
		tableModelListeners.remove(listener);
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
	}
	
	private Object getValueFromField(StatusItem item, String field) {

        Object returnValue = null;
        try {Method method = item.getClass().getMethod(getBeanField("get", field));
            returnValue = method.invoke(item, (Object[]) null);
        } catch (IllegalAccessException iae) {
//            log.debug("getValueFromField(entity, field), iae: " + iae);
        } catch (InvocationTargetException ite) {
//            log.debug("getValueFromField(entity, field), iae: " + ite);
        } catch (SecurityException e) {
//			log.debug("getValueFromField(entity, field), iae: " + e);
		} catch (NoSuchMethodException e) {
//			log.debug("getValueFromField(entity, field), iae: " + e);
		}
//        log.trace("getValueFromField(" + field + "), value:" + returnValue);
        return returnValue;
    }
	
	private String getBeanField(String prefix, String field) {
        String beanField = prefix + field.substring(0,1).toUpperCase() + field.substring(1);
        return beanField;
    }
	
	/**
	 * Returns the system status object.
	 */
	private SystemStatus getSystemStatus(SystemStatusComponentEnum system) {
		
		switch (system) {

		case DATABASE_SYSTEM:
			systemStatus = serviceStub.getDatabaseStatus();
			break;
			
		case DATEK_SYSTEM:
			systemStatus = service.getDatekServerStatus();
			break;
			
		case KART_SYSTEM:
			systemStatus = service.getKartServerStatus();
			break;
		
		case KVERNEMOTOR_SYSTEM:
			systemStatus = serviceStub.getKvernemotorStatus();
			break;
			
		case M2M_SYSTEM:
			systemStatus = serviceStub.getM2MSServerStatus();
			break;
		
		case MOBILE_SYSTEM:
			systemStatus = serviceStub.getMobileServerStatus();
			break;
			
		default:
			throw new IllegalArgumentException("The system identified by the SystemStatusComponentEnum " + system.toString() + " was not recognized by " + this.getClass().getSimpleName());
				
		}
		
		return systemStatus;
	}
	
}