package no.mesta.mipss.admin.gui.panel.systemstatus;

import java.awt.BorderLayout;
import java.util.concurrent.ExecutionException;

import javax.swing.JScrollPane;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.admin.gui.model.SystemStatusTableModel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

/**
 * Panel containing GUI for the Oracle Mobile Server system status.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SystemStatusMobilePanel extends AbstractSystemStatusPanel {

	private static final long serialVersionUID = 5818448139986527555L;

	private JXBusyLabel busyLabel;
	private JXTable table;
	private JScrollPane scrollPane;
	
	public SystemStatusMobilePanel() {
		init();
	}
	
	protected void init() {
		this.setLayout(new BorderLayout());
		this.setBorder(getPanelBorder(SystemStatusComponentEnum.MOBILE_SYSTEM));
		
		table = tableFactory.getTable(SystemStatusComponentEnum.MOBILE_SYSTEM);
		
		busyLabel = new JXBusyLabel();
		scrollPane = new JScrollPane(table);
		
		add(scrollPane, BorderLayout.CENTER);
		
		final SystemStatusWorker w = new SystemStatusWorker(this, SystemStatusComponentEnum.MOBILE_SYSTEM);
		w.execute();
		new Thread(){
			public void run(){
				SystemStatusTableModel tableModel;
				try {
					tableModel = w.get();
					table.setModel(tableModel);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	@Override	
	protected void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		table.setVisible(!busy);
		scrollPane.setViewportView(busy ? busyLabel : table);
	}

}