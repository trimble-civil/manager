package no.mesta.mipss.admin.rapporter.gui;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import no.mesta.mipss.admin.rapporter.AdminRapportController;
import no.mesta.mipss.admin.rapporter.AdminRapportModule;
import no.mesta.mipss.admin.rapporter.gui.statistikk.SlettKontraktStatistikkPanel;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.UserUtils;

import org.jdesktop.swingx.VerticalLayout;

@SuppressWarnings("serial")
public class AdminRapportPanel extends JPanel{

	private final AdminRapportModule plugin;
	private final AdminRapportController controller;
	private RoderapportPanel pnlRoderapport;
	private KjoretoyrapportPanel pnlKjoretoyrapport;
	private SprederRapportPanel pnlSprederrapport;
	private SynkrapportPanel pnlSynkrapport;
	private SlettKontraktStatistikkPanel pnlSlettStatistikk;
	private UkjentStroproduktPanel pnlUkjentStroprodukt;
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");
	
	
	public AdminRapportPanel(AdminRapportModule plugin) {
		this.plugin = plugin;
		controller = new AdminRapportController(plugin);
		initComponents();
		initGui();
	}

	private void initGui() {
		setLayout(new VerticalLayout(5));
		add(pnlRoderapport);
		add(pnlKjoretoyrapport);
		add(pnlSprederrapport);
		add(pnlSynkrapport);
		add(pnlUkjentStroprodukt);
		if (UserUtils.isAdmin(plugin.getLoader().getLoggedOnUser(true))){
			add(pnlSlettStatistikk);
		}
	}
	
	private void initComponents(){
		pnlRoderapport = new RoderapportPanel(controller);
		pnlRoderapport.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("title.roderapporter")));
		
		pnlKjoretoyrapport = new KjoretoyrapportPanel(controller);
		pnlKjoretoyrapport.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("title.kjoretoyrapporter")));
		
		pnlSprederrapport = new SprederRapportPanel(controller);
		pnlSprederrapport.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("title.sprederrapporter")));
		
		pnlSynkrapport = new SynkrapportPanel(controller);
		pnlSynkrapport.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("title.synkrapporter")));
		
		pnlSlettStatistikk = new SlettKontraktStatistikkPanel(controller);
		pnlSlettStatistikk.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("title.slettFeltstatistikk")));
		
		pnlUkjentStroprodukt = new UkjentStroproduktPanel(controller);
		pnlUkjentStroprodukt.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("title.ukjentStroprodukt")));
	}
}
