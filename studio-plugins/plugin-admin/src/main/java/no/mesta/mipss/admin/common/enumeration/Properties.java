package no.mesta.mipss.admin.common.enumeration;

import no.mesta.mipss.common.property.PropertyName;

/**
 * Application properties. The property name represents a key used 
 * for property value lookup in an external .properties file
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link PropertyAccessor}
 */
public enum Properties implements PropertyName {
	
	// General
	TITLE("title"),
	FILE("adminText_no_NO"),
	LOOKANDFEEL("lookandfeel"),
	WINDOW_WIDTH("window.width"),
	WINDOW_HEIGHT("window.height"),
	
	// Property lookup prefixes and suffixes	
	PERSIST_PREFIX("persist"),
	PERSIST_SUFFIX("List"),
	LIST_PREFIX("get"),
	LIST_SUFFIX("List"),
	REMOVE_PREFIX("remove"),
	REMOVE_SUFFIX("List"),
	
	// Messages
	DEFAULT_ERRORMESSAGE("default.errormessage"),
	ENTITY_OPTIMISTICLOCK("entity.optimisticlock"),
	WARNING_REMOVE("warning.slett"), 
	DELETE1_WARNING("warning.delete"), 
	DELETEMULTIPLE_WARNING("warning.deleteMultiple"), 
	DELETE_TITLE("warning.delete.title");
		
	private final String propertyName;
	
	private Properties(String propertyName) {
		this.propertyName = propertyName;
	}
	
	/**
	 * Get the name of this property.
	 * 
	 * @return the name of this property
	 */
	public String getPropertyName() {
		return this.propertyName;
	}
		
	
}