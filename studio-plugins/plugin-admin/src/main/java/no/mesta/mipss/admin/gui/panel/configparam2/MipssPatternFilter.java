package no.mesta.mipss.admin.gui.panel.configparam2;

import org.jdesktop.swingx.decorator.PatternFilter;

public class MipssPatternFilter extends PatternFilter{

	public MipssPatternFilter(String regularExpr, int matchFlags, int col){
		super(regularExpr, matchFlags, col);
	}
	/**
     * {@inheritDoc}
     */
    @Override
    protected int mapTowardModel(int row) {
    	if (getSize()>row){
    		return super.mapTowardModel(row);
    	}else if (getSize()>0){
    		return getSize();
    	}else
    		return -1;
    }
}
