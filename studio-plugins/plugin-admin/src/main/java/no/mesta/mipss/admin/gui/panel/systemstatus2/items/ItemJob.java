package no.mesta.mipss.admin.gui.panel.systemstatus2.items;

import no.mesta.mipss.admin.gui.panel.systemstatus2.SurveillanceItem;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ItemJob implements Job{
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		SurveillanceItem item = (SurveillanceItem)map.get("item");
		item.checkStatus();
	}

}
