package no.mesta.mipss.admin.gui.panel.systemstatus;

import java.awt.BorderLayout;
import java.util.concurrent.ExecutionException;

import javax.swing.JScrollPane;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.admin.gui.model.SystemStatusTableModel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

/**
 * Panel containing GUI for the M2M system status.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SystemStatusM2MPanel extends AbstractSystemStatusPanel {

	private static final long serialVersionUID = 4868990187036834134L;
	
	private JXBusyLabel busyLabel;
	private JXTable table;
	private JScrollPane scrollPane;
	
	public SystemStatusM2MPanel() {
		init();
	}
	
	protected void init() {
		this.setLayout(new BorderLayout());
		this.setBorder(getPanelBorder(SystemStatusComponentEnum.M2M_SYSTEM));
		
		table = tableFactory.getTable(SystemStatusComponentEnum.M2M_SYSTEM);
		
		busyLabel = new JXBusyLabel();
		scrollPane = new JScrollPane(table);
		
		add(scrollPane, BorderLayout.CENTER);
		
		final SystemStatusWorker w = new SystemStatusWorker(this, SystemStatusComponentEnum.M2M_SYSTEM);
		w.execute();
		new Thread(){
			public void run(){
				SystemStatusTableModel tableModel;
				try {
					tableModel = w.get();
					table.setModel(tableModel);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	@Override	
	protected void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		table.setVisible(!busy);
		scrollPane.setViewportView(busy ? busyLabel : table);
	}

}