package no.mesta.mipss.admin.gui.panel.systemstatus;

import java.awt.BorderLayout;

import javax.swing.JPanel;

/**
 * Panel containing the entire system status GUI.
 * 
 * @author Christian Wiik 
 */
@SuppressWarnings("serial")
public class SystemStatusGUIPanel extends JPanel {
	
	private JPanel contentPanel;
	
	public SystemStatusGUIPanel() {
		init();
	}
	
	private void init() {
		setLayout(new BorderLayout());
		contentPanel = new SystemStatusContentPanel();
		add(contentPanel);
	}

}