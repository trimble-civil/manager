package no.mesta.mipss.admin.rapporter.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

import no.mesta.mipss.admin.rapporter.AdminRapportController;

@SuppressWarnings("serial")
public class SynkrapportPanel extends JPanel{
	private final AdminRapportController controller;
	private JButton button;
	
	public SynkrapportPanel(AdminRapportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}
	
	private void initComponents() {
		button = new JButton(controller.getSynkOversiktAction());
		
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(button, new GridBagConstraints(0,0,1,1,1.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0),0,0));
		
	}

}
