package no.mesta.mipss.admin;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

public class ButtonPanel extends JPanel{

	private final GrunndataController controller;
	private final EditTablePanel tablePanel;
	
	public ButtonPanel(GrunndataController controller, EditTablePanel tablePanel){
		this.controller = controller;
		this.tablePanel = tablePanel;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JButton visButton = new JButton(new VisEndringerAction("Vis endringer", IconResources.HISTORY_ICON));
		JButton nyButton = new JButton(new NyAction("Ny", IconResources.NEW_ICON));
		JButton slettButton = new JButton(new SlettAction("Slett", IconResources.BUTTON_CANCEL_ICON));
		JButton lagreButton = new JButton(new LagreAction("Lagre", IconResources.SAVE_ICON));
		
		add(visButton, 	 new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,0), 0,0));
		add(nyButton, 	 new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,5,5,0), 0,0));
		add(slettButton, new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,5,5,0), 0,0));
		add(lagreButton, new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
		Binding<GrunndataController, Object, JButton, Object> visBinding = BindingHelper.createbinding(controller, "${!listsEmpty}", visButton, "enabled");
		//Binding<GrunndataController, Object, JButton, Object> slettBinding = BindingHelper.createbinding(controller, "${!listsEmpty}", slettButton, "enabled");
		Binding<GrunndataController, Object, JButton, Object> lagreBinding = BindingHelper.createbinding(controller, "${!listsEmpty}", lagreButton, "enabled");
		BindingGroup bg = new BindingGroup();
		bg.addBinding(visBinding);
		//bg.addBinding(slettBinding);
		bg.addBinding(lagreBinding);
		bg.bind();
	}
	
	private class VisEndringerAction extends AbstractAction{

		public VisEndringerAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent ev) {
			StringBuilder removed = new StringBuilder("Slettet:\n\n");
			
			for (MipssEntityBean<?> e:controller.getRemovedEntities()){
				removed.append(e.toString());
				removed.append("\n");
			}
			StringBuilder changed = new StringBuilder("Endret/lagt til:\n\n");
			for (MipssEntityBean<?> e:controller.getChangedEntities()){
				changed.append(e.toString());
				changed.append("\n");
			}
			JOptionPane.showMessageDialog(null, changed.toString()+"\n"+removed.toString());
		}
		
	}
	
	private class NyAction extends AbstractAction{

		public NyAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			tablePanel.createNewEntity();
		}
		
	}
	private class SlettAction extends AbstractAction{

		public SlettAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			tablePanel.removeSelectedEntity();
		}
		
	}
	private class LagreAction extends AbstractAction{

		public LagreAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			controller.persistChanges();
		}
		
	}
}
