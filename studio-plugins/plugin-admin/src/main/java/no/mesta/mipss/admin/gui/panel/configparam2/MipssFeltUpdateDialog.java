package no.mesta.mipss.admin.gui.panel.configparam2;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mipssfelt.AndroidUpdateService;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.plugin.MipssPlugin;

import org.jdesktop.swingx.JXBusyLabel;
@SuppressWarnings("serial")
public class MipssFeltUpdateDialog extends JDialog{
	private AndroidUpdateService update;
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");
	private JButton btnOk;
	private JButton btnCancel;
	private MipssFeltUpdatePanel updatePanel;
	
	private JXBusyLabel lblBusy;
	private JDialog busyDialog;
	private final MipssPlugin plugin;
	
	public MipssFeltUpdateDialog(JFrame owner, MipssPlugin plugin){
		super(owner);
		this.plugin = plugin;
		
		update = BeanUtil.lookup(AndroidUpdateService.BEAN_NAME, AndroidUpdateService.class);
		initComponents();
		initGui();
	}

	private void initGui() {
		setLayout(new BorderLayout());
		
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		pnlButtons.add(btnOk, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 0, 11, 5), 0,0));
		pnlButtons.add(btnCancel, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 0, 11, 11), 0,0));
		
		add(updatePanel);
		add(pnlButtons, BorderLayout.SOUTH);
	}
	
	private void initComponents(){
		
		busyDialog = new JDialog(MipssFeltUpdateDialog.this);
		busyDialog.setTitle("Oppdaterer mipss-felt");
		lblBusy = new JXBusyLabel();
		
		busyDialog.add(lblBusy);
		busyDialog.setSize(250, 100);
		busyDialog.setLocationRelativeTo(this);
		
		btnOk = new JButton(getOkAction());
		btnCancel = new JButton(getCancelAction());
		
		updatePanel = new MipssFeltUpdatePanel();
	}
	
	private Action getOkAction(){
		return new AbstractAction(resources.getGuiString("button.ok")){

			@Override
			public void actionPerformed(ActionEvent e) {
				busyDialog.setVisible(true);
				btnOk.setEnabled(false);
				btnCancel.setEnabled(false);
				getOkWorker().execute();
			}
		};
	}
	
	private SwingWorker<Void, Void> getOkWorker(){
		return new SwingWorker<Void, Void>(){
			@Override
			protected Void doInBackground() throws Exception {
				List<Konfigparam> configs = updatePanel.getConfigs();
				lblBusy.setBusy(true);
				for (Konfigparam p:configs){
					lblBusy.setText("Oppdaterer:"+p.getNavn());
					KonfigparamPreferences.getInstance().oppdaterKonfig(p);
				}
				if (updatePanel.isApkSet()){
					lblBusy.setText("oppdaterer: Apk-fil");
					try{
						update.update(updatePanel.getSelectedApkFile(), plugin.getLoader().getLoggedOnUserSign());
					} catch(Throwable t){
						t.printStackTrace();
					}
				}
				if (updatePanel.isBrukermanualSet()){
					lblBusy.setText("oppdaterer: Brukermanual");
					try{
						update.updateBrukermanual(updatePanel.getSelectedBrukermanualFile(), plugin.getLoader().getLoggedOnUserSign());
					}catch (Throwable t){
						t.printStackTrace();
					}
				}
				
				return null;
			}
			@Override
			protected void done(){
				lblBusy.setBusy(false);
				busyDialog.dispose();
				MipssFeltUpdateDialog.this.dispose();
			}
		};
	}
	
	
	private Action getCancelAction(){
		return new AbstractAction(resources.getGuiString("button.cancel")){

			@Override
			public void actionPerformed(ActionEvent e) {
				MipssFeltUpdateDialog.this.dispose();
			}
		};
	}
}
