package no.mesta.mipss.admin.gui.panel.systemstatus3;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class FeilPanelSQLJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		JobDataMap map = context.getJobDetail().getJobDataMap();
		String sql = (String) map.get("sql");
		SystemStatusFeilController controller = (SystemStatusFeilController)map.get("controller");
		RingPanel panel = (RingPanel)map.get("panel");
		double poll = controller.poll(sql);
		
		String name = context.getJobDetail().getKey().getName();
		
		if (name.equalsIgnoreCase("Feilkø")) {
			if (poll > 0 && poll > panel.getLastValue()) {
				controller.play("doh.wav");
			} else if (poll == 0 && panel.getLastValue() > 0) {
				controller.play("hehe.wav");
			}			
		} else if (name.equalsIgnoreCase("Inspeksjoner")) {
			if (poll > 0 && poll > panel.getLastValue()) {
				controller.play("thissucks.wav");
			} else if (poll == 0 && panel.getLastValue() > 0) {
				controller.play("hehe.wav");
			}
		} else if (name.equalsIgnoreCase("Feilmeldinger")) {
			if (poll > 0 && poll > panel.getLastValue()) {
				controller.play("ahhh.wav");
			} else if (poll == 0 && panel.getLastValue() > 0) {
				controller.play("hehe.wav");
			}		
		}	
		
		panel.setValue(poll);
	}
}
