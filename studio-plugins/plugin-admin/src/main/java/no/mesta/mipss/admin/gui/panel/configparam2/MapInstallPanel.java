package no.mesta.mipss.admin.gui.panel.configparam2;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.geotools.data.ResourceInfo;
import org.geotools.data.ServiceInfo;
import org.geotools.data.ows.CRSEnvelope;
import org.geotools.data.ows.Layer;
import org.geotools.data.ows.StyleImpl;
import org.geotools.data.ows.WMSCapabilities;
import org.geotools.data.wms.WebMapServer;
import org.geotools.ows.ServiceException;
import org.xml.sax.SAXException;

public class MapInstallPanel {

	public static void main(String[] args) throws IOException {
		String getCapabilitiesURL = "http://wms.nordeca.com/geoserver/ows?service=wms&version=1.3.0&request=GetCapabilities";
		URL url = null;
		try {
		  url = new URL(getCapabilitiesURL);
		} catch (MalformedURLException e) {
		  //will not happen
		}

		WebMapServer wms = null;
		try {
		  wms = new WebMapServer(url);
		  WMSCapabilities capabilities = wms.getCapabilities();
		  List<Layer> layers = capabilities.getLayerList();
		  ServiceInfo i = wms.getInfo();
		  
		  for(Layer l:layers){
			  // Print layer info
			  System.out.println("Layer:"+l.getName());
			  System.out.println("       "+l.getTitle());
			  System.out.println("       "+l.getChildren().length);
			  System.out.println("       "+l.getBoundingBoxes());
			  
			  CRSEnvelope env = l.getLatLonBoundingBox();
			  System.out.println("       "+env.getLowerCorner()+" x "+env.getUpperCorner());
			  if ("nor_basic_ip".equals(l.getName())){
				  ResourceInfo info = wms.getInfo(l);
				  
			  }
		  }
		} catch (IOException e) {
			e.printStackTrace();
		  //There was an error communicating with the server
		  //For example, the server is down
		} catch (ServiceException e) {
			e.printStackTrace();
		  //The server returned a ServiceException (unusual in this case)
		} catch (SAXException e) {
			e.printStackTrace();
		  //Unable to parse the response from the server
		  //For example, the capabilities it returned was not valid
		}
	}
}
