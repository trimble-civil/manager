package no.mesta.mipss.admin;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import no.mesta.mipss.persistence.ImageUtil;

@SuppressWarnings("serial")
public class IkonLobEntityRenderer extends DefaultTableCellRenderer{

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		JPanel panel = new JPanel(new GridBagLayout());
		panel.setOpaque(false);
		
		if (value instanceof byte[]){
			BufferedImage image = ImageUtil.bytesToImage((byte[])value);
			ImageIcon i = new ImageIcon(image);
			if (i.getIconHeight()>30||i.getIconWidth()>30){
				i = new ImageIcon(ImageUtil.scaleImageFast(i, 30, 30));
			}
			
			label.setText("");
			label.setIcon(i);
		}else{
			label.setIcon(null);
		}
		
		JButton button = new JButton("...");
		button.setMargin(new Insets(0,2,0,2));
		
		panel.add(label, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		panel.add(button, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,2,0), 0,0));
		return panel;
	}

}
