package no.mesta.mipss.admin;

import java.awt.Component;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Ikon;

public class IkonTableComboBoxRenderer implements TableCellRenderer {
	private JComboBox comboBox;
	
	public IkonTableComboBoxRenderer(){
		comboBox = new JComboBox();
		comboBox.setRenderer(new ComboBoxRenderer());

	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Ikon ikon = (Ikon)value;
		comboBox.removeAllItems();
		comboBox.addItem(ikon);
		return comboBox; 
	}
	
	class ComboBoxRenderer extends JLabel implements ListCellRenderer {

		public ComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
		}
		
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			Ikon ikon=(Ikon)value;
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}
			if (ikon!=null){
				BufferedImage image = ImageUtil.bytesToImage(ikon.getIkonLob());
				ImageIcon i = new ImageIcon(image);
				if (i.getIconHeight()>30||i.getIconWidth()>30){
					i = new ImageIcon(ImageUtil.scaleImageFast(i, 30, 30));
				}
				setIcon(i);
			}else{
				setIcon(null);
			}
			
			return this;
		}
	}

}