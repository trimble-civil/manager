package no.mesta.mipss.admin;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.entityservice.MipssGrunndataService;
import no.mesta.mipss.persistence.MipssEntityBean;

public class GrunndataController implements PropertyChangeSource{

	private MipssGrunndataService bean = BeanUtil.lookup(MipssGrunndataService.BEAN_NAME, MipssGrunndataService.class);
	private List<MipssEntityBean<?>> changedList = new ArrayList<MipssEntityBean<?>>();
	private List<MipssEntityBean<?>> removedList = new ArrayList<MipssEntityBean<?>>();
	protected PropertyChangeSupport props = new PropertyChangeSupport(this);
	private final GrunndataModuleGuiDelegate grunndataModuleGuiDelegate;
	
	public GrunndataController(GrunndataModuleGuiDelegate grunndataModuleGuiDelegate) {
		this.grunndataModuleGuiDelegate = grunndataModuleGuiDelegate;
	}

	public List<? extends MipssEntityBean<?>> getListForEntity(String name){
		return bean.getEntityList(name);
	}
	
	public void persistChanges(){
		bean.removeEntities(removedList);
		bean.persistEntities(changedList);
		clearChanges();
		grunndataModuleGuiDelegate.reloadTableView();
	}
	
	public boolean isEntityChanged(MipssEntityBean<?> entity){
		return changedList.contains(entity);
	}
	public boolean isEntityRemoved(MipssEntityBean<?> entity){
		return removedList.contains(entity);
	}
	
	public void addChangedEntity(MipssEntityBean<?> entity){
		boolean old = isListsEmpty();
		changedList.add(entity);
		boolean n = isListsEmpty();
		props.firePropertyChange("listsEmpty", old, n);
	}
	public void addRemovedEntity(MipssEntityBean<?> entity){
		boolean old = isListsEmpty();
		removedList.add(entity);
		boolean n = isListsEmpty();
		props.firePropertyChange("listsEmpty", old, n);
	}
	
	public void removeChangedEntity(MipssEntityBean<?> entity){
		boolean old = isListsEmpty();
		changedList.remove(entity);
		boolean n = isListsEmpty();
		props.firePropertyChange("listsEmpty", old, n);
		
	}
	public void removeRemovedEntity(MipssEntityBean<?> entity){
		boolean old = isListsEmpty();
		removedList.remove(entity);
		boolean n = isListsEmpty();
		props.firePropertyChange("listsEmpty", old, n);
	}
	
	public boolean isListsEmpty(){
		return changedList.isEmpty()&&removedList.isEmpty();
	}
	/**
	 * Returnerer en array med entitetene som er endret eller
	 * lagt til.
	 * 
	 * @return
	 */
	public MipssEntityBean<?>[] getChangedEntities(){
		return changedList.toArray(new MipssEntityBean<?>[]{});
	}
	
	/**
	 * Returnerer en array med entitetene som er slettet
	 * 
	 * @return
	 */
	public MipssEntityBean<?>[] getRemovedEntities(){
		return removedList.toArray(new MipssEntityBean<?>[]{});
	}
	
	public void clearChanges(){
		boolean old = isListsEmpty();
		changedList.clear();
		removedList.clear();
		boolean n = isListsEmpty();
		props.firePropertyChange("listsEmpty", old, n);
	}


	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}
	
	/** {@inheritDoc} */
	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}
	
	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	/** {@inheritDoc} */
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
}
