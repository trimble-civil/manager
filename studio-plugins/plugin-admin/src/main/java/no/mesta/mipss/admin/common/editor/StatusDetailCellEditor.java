package no.mesta.mipss.admin.common.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;

import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.systemstatus.dto.StatusDetail;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.ui.table.AbstractCellEditor;

/**
 * Editor for editering av <tt>StatusDetails</tt> objekt i tabell-celle.
 * 
 * @author Christian Wiik (Mesan)
 */
public class StatusDetailCellEditor extends AbstractCellEditor {
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		
		JPanel cellPanel = new JPanel(new BorderLayout());
		StatusDetail detail = (StatusDetail)value;
		JButton detailButton = null;
		
		cellPanel.setBackground(Color.WHITE);
		
		detailButton = new JButton("(..)");
		detailButton.addActionListener(new DetailButtonListener(detail));
		detailButton.setBackground(Color.WHITE);
		
		cellPanel.add(detailButton);
		
		return detailButton;
		
	}
	
	public class DetailButtonListener implements ActionListener {
		
		private StatusDetail detail;
		
		public DetailButtonListener(StatusDetail detail) {
			this.detail = detail;
		}
		
		public void actionPerformed(ActionEvent event) {
			MipssDialogue dialogue = new MipssDialogue(
						null,
						IconResources.INFO_ICON,
						"Detaljert status",
						detail.getDescription(),
						MipssDialogue.Type.INFO,
						new MipssDialogue.Button[] { MipssDialogue.Button.OK } );
			
			dialogue.askUser();
		}
		
		public StatusDetail getDetail() {
			return detail;
		}
		
	}

}