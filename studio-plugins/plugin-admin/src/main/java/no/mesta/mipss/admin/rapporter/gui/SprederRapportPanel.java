package no.mesta.mipss.admin.rapporter.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import no.mesta.mipss.admin.rapporter.AdminRapportController;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;

@SuppressWarnings("serial")
public class SprederRapportPanel extends JPanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");

	
	private JButton btnSprederrapport;
	private JLabel lblSerienummer;
	private JTextField txtSerienummer;
	private JMipssContractPicker contractPickerSpreder;
	private MipssDateTimePickerHolder fraDatoSpreder;
	private MipssDateTimePickerHolder tilDatoSpreder;
	private JMipssDatePicker fraPickerSpreder;
	private JMipssDatePicker tilPickerSpreder;
	
	private JButton btnSprederdetalj;
	private JLabel lblSerienummerDetalj;
	private JTextField txtSerienummerDetalj;
	private MipssDateTimePickerHolder fraDatoDetalj;
	private MipssDateTimePickerHolder tilDatoDetalj;
	private final AdminRapportController controller;
	private JMipssDatePicker tilPickerDetalj;
	private JMipssDatePicker fraPickerDetalj;
	private JSpinner fraSpinnerSpreder;
	private JSpinner tilSpinnerSpreder;
	private JSpinner fraSpinnerDetalj;
	private JSpinner tilSpinnerDetalj;
	
	
	public SprederRapportPanel(AdminRapportController controller) {
		this.controller = controller;
		initComponents();
		initGui();
	}

	private void initComponents() {
		txtSerienummer = new JTextField(10);
		contractPickerSpreder = new JMipssContractPicker(false);
		
		fraPickerSpreder = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		tilPickerSpreder = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		fraPickerSpreder.pairWith(tilPickerSpreder);
		
		Dimension spinnerSize = new Dimension(70, 22);
		fraSpinnerSpreder = new JSpinner(new SpinnerDateModel(fraPickerSpreder.getDate(), null, null, Calendar.MINUTE));
        fraSpinnerSpreder.setPreferredSize(spinnerSize);
        fraSpinnerSpreder.setMinimumSize(spinnerSize);
        fraSpinnerSpreder.setMaximumSize(spinnerSize);
        fraSpinnerSpreder.setEditor(new JSpinner.DateEditor(fraSpinnerSpreder, "HH:mm:ss"));
        
        Date midnight = tilPickerSpreder.getDate();
		Calendar c = Calendar.getInstance();
		c.setTime(midnight);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		
		tilSpinnerSpreder = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        tilSpinnerSpreder.setPreferredSize(spinnerSize);
        tilSpinnerSpreder.setMinimumSize(spinnerSize);
        tilSpinnerSpreder.setMaximumSize(spinnerSize);
        tilSpinnerSpreder.setEditor(new JSpinner.DateEditor(tilSpinnerSpreder, "HH:mm:ss"));
        
        fraDatoSpreder = new MipssDateTimePickerHolder(fraPickerSpreder, fraSpinnerSpreder);
        tilDatoSpreder = new MipssDateTimePickerHolder(tilPickerSpreder, tilSpinnerSpreder);
        btnSprederrapport = new JButton(controller.getSprederRapportAction(txtSerienummer, contractPickerSpreder, fraDatoSpreder, tilDatoSpreder));
        lblSerienummer = new JLabel(resources.getGuiString("label.serienummer"));
        
        
		fraPickerDetalj = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		tilPickerDetalj = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		fraPickerDetalj.pairWith(tilPickerDetalj);
		
		fraSpinnerDetalj = new JSpinner(new SpinnerDateModel(fraPickerDetalj.getDate(), null, null, Calendar.MINUTE));
        fraSpinnerDetalj.setPreferredSize(spinnerSize);
        fraSpinnerDetalj.setMinimumSize(spinnerSize);
        fraSpinnerDetalj.setMaximumSize(spinnerSize);
        fraSpinnerDetalj.setEditor(new JSpinner.DateEditor(fraSpinnerDetalj, "HH:mm:ss"));
        
		tilSpinnerDetalj = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
		tilSpinnerDetalj.setPreferredSize(spinnerSize);
		tilSpinnerDetalj.setMinimumSize(spinnerSize);
		tilSpinnerDetalj.setMaximumSize(spinnerSize);
		tilSpinnerDetalj.setEditor(new JSpinner.DateEditor(tilSpinnerDetalj, "HH:mm:ss"));
        
        fraDatoDetalj = new MipssDateTimePickerHolder(fraPickerDetalj, fraSpinnerDetalj);
        tilDatoDetalj= new MipssDateTimePickerHolder(tilPickerDetalj, tilSpinnerDetalj);
        
        txtSerienummerDetalj = new JTextField(10);
        btnSprederdetalj = new JButton(controller.getSprederDetaljAction(txtSerienummerDetalj, fraDatoDetalj, tilDatoDetalj));
        lblSerienummerDetalj = new JLabel(resources.getGuiString("label.serienummer"));
	}

	private void initGui() {
		JPanel fraDatoSprederPanel = new JPanel(new GridBagLayout());
        JPanel tilDatoSprederPanel = new JPanel(new GridBagLayout());
        fraDatoSprederPanel.add(fraPickerSpreder,  new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        fraDatoSprederPanel.add(fraSpinnerSpreder, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        tilDatoSprederPanel.add(tilPickerSpreder,  new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        tilDatoSprederPanel.add(tilSpinnerSpreder, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
        
        JPanel fraDatoDetaljPanel = new JPanel(new GridBagLayout());
        JPanel tilDatoDetaljPanel = new JPanel(new GridBagLayout());
        fraDatoDetaljPanel.add(fraPickerDetalj,  new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        fraDatoDetaljPanel.add(fraSpinnerDetalj, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        tilDatoDetaljPanel.add(tilPickerDetalj,  new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
        tilDatoDetaljPanel.add(tilSpinnerDetalj, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		
        setLayout(new GridBagLayout());
        
        JPanel sprederrapportPanel = new JPanel(new GridBagLayout());
        sprederrapportPanel.add(lblSerienummer, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        sprederrapportPanel.add(txtSerienummer, 		new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        sprederrapportPanel.add(contractPickerSpreder, 	new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        sprederrapportPanel.add(fraDatoSprederPanel, 	new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        sprederrapportPanel.add(tilDatoSprederPanel, 	new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        sprederrapportPanel.add(btnSprederrapport, 		new GridBagConstraints(5, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5), 0, 0));
        
        JPanel sprederdetaljPanel = new JPanel(new GridBagLayout());
        sprederdetaljPanel.add(lblSerienummerDetalj, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0, 0));
        sprederdetaljPanel.add(txtSerienummerDetalj, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0, 0));
        sprederdetaljPanel.add(fraDatoDetaljPanel, 		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0, 0));
        sprederdetaljPanel.add(tilDatoDetaljPanel, 		new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0, 0));
        sprederdetaljPanel.add(btnSprederdetalj, 		new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,5), 0, 0));
        
        add(sprederrapportPanel, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
        add(sprederdetaljPanel, 	new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));

	}

}
