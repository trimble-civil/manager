package no.mesta.mipss.admin.gui.panel.systemstatus;

import java.awt.BorderLayout;
import java.util.concurrent.ExecutionException;

import javax.swing.JScrollPane;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.admin.gui.model.SystemStatusTableModel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

/**
 * Panel containing GUI for the kart system status.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SystemStatusKartPanel extends AbstractSystemStatusPanel {
	
	private static final long serialVersionUID = -8380894006363598136L;
	
	private JXBusyLabel busyLabel;
	private JXTable table;
	private JScrollPane scrollPane;
	
	public SystemStatusKartPanel() {
		init();
	}
	
	protected void init() {
		this.setLayout(new BorderLayout());
		this.setBorder(getPanelBorder(SystemStatusComponentEnum.KART_SYSTEM));
		
		table = tableFactory.getTable(SystemStatusComponentEnum.KART_SYSTEM);
		
		busyLabel = new JXBusyLabel();
		scrollPane = new JScrollPane(table);
		
		add(scrollPane, BorderLayout.CENTER);
		
		final SystemStatusWorker w = new SystemStatusWorker(this, SystemStatusComponentEnum.KART_SYSTEM);
		w.execute();
		new Thread(){
			public void run(){
				SystemStatusTableModel tableModel;
				try {
					tableModel = w.get();
					table.setModel(tableModel);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	@Override	
	protected void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		table.setVisible(!busy);
		scrollPane.setViewportView(busy ? busyLabel : table);
		
	}

}