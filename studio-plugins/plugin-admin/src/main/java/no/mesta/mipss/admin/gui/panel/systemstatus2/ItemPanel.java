package no.mesta.mipss.admin.gui.panel.systemstatus2;

import static java.awt.GridBagConstraints.HORIZONTAL;
import static java.awt.GridBagConstraints.NONE;
import static java.awt.GridBagConstraints.WEST;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class ItemPanel extends JPanel implements PropertyChangeListener {
	
	private SurveillanceItem item;
	private JLabel red = new JLabel(IconResources.RED_LIGHT);
	private JLabel yellow = new JLabel(IconResources.YELLOW_LIGHT);
	private JLabel green = new JLabel(IconResources.GREEN_LIGHT);
	
	
	public ItemPanel(SurveillanceItem item){
		this.item = item;
		item.addPropertyChangeListener(this);
		initGui();
		updateTrafficLight();
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		JLabel label = new JLabel(item.getName());
		label.setFont(new Font("Arial", Font.BOLD, 15));
		
		JButton info = new JButton("Info");
		if (item.getInfoAction()!=null){
			info.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					item.getInfoAction().actionPerformed(e);
				}
			});
		}else{
			info.setEnabled(false);
		}
		
		add(label, new GridBagConstraints(0,0, 1,1, 1.0,0.0, WEST, HORIZONTAL, new Insets(0,0,0,10), 0,0));
		add(red, new GridBagConstraints(1,0, 1,1, 0.0,0.0, WEST, NONE, new Insets(0,0,0,5), 0,0));
		add(yellow, new GridBagConstraints(2,0, 1,1, 0.0,0.0, WEST, NONE, new Insets(0,0,0,5), 0,0));
		add(green, new GridBagConstraints(3,0, 1,1, 0.0,0.0, WEST, NONE, new Insets(0,0,0,5), 0,0));
		add(info, new GridBagConstraints(4,0, 1,1, 0.0,0.0, WEST, NONE, new Insets(0,10,0,0), 0,0));
	}
	
	private void updateTrafficLight(){
		red.setEnabled(false);
		green.setEnabled(false);
		yellow.setEnabled(false);
		
		if (item.getState()!=null){
			switch (item.getState()){
				case OK : green.setEnabled(true);
					break;
				case WARN : yellow.setEnabled(true);
					break;
				case FAILED : red.setEnabled(true);
					break;
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateTrafficLight();
	}
	
}
