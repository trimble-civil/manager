package no.mesta.mipss.admin.gui.panel.systemstatus2;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.CENTER;
import static java.awt.GridBagConstraints.NONE;
import static java.awt.GridBagConstraints.WEST;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import no.mesta.mipss.admin.gui.main.SystemStatusModule;
import no.mesta.mipss.admin.gui.panel.systemstatus2.items.ItemJob;
import no.mesta.mipss.admin.gui.panel.systemstatus2.items.KvernOkJob;
import no.mesta.mipss.admin.gui.panel.systemstatus2.items.SpeedometerSQLJob;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

@SuppressWarnings("serial")
public class SystemStatusPanel extends JPanel{
	private SpeedometerDial prodpunktDial;
	private SpeedometerDial rundetidDial;
	private SpeedometerDial rundetid2Dial;
	private SpeedometerDial strekningDial;
	private SpeedometerDial tiltakDial;
	private Scheduler scheduler;
	
	private final SystemStatusModule plugin;
	private SpeedometerDial rundetid3Dial;
	
	public SystemStatusPanel(SystemStatusModule plugin){
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui() {
		int jobnr = (int) (Math.random()*10000);
		setLayout(new GridBagLayout());
		prodpunktDial = new SpeedometerDial(100, 40, "Prodpunkt/sek");
		rundetidDial =  new SpeedometerDial(200, 100, "Rundetid (kvern1)");
		rundetid2Dial = new SpeedometerDial(200, 100, "Rundetid (kvern2)");
		rundetid3Dial = new SpeedometerDial(200, 100, "Rundetid (kvern3)");
		strekningDial = new SpeedometerDial(480, "Strekninger/min");
		tiltakDial = new SpeedometerDial(480, "Tiltak/min");
		startJobScheduler();
		startJob(plugin.getController().getProdpunktQuery(), prodpunktDial, 5, "Prodpunkt/sek " + jobnr);
		startJob(plugin.getController().getRundetidQuery(),  rundetidDial, 30, "Rundetid (kvern1) " + jobnr);
		startJob(plugin.getController().getRundetid2Query(), rundetid2Dial, 30, "Rundetid (kvern2) " + jobnr);
		startJob(plugin.getController().getRundetid3Query(), rundetid3Dial, 30, "Rundetid (kvern3) " + jobnr);
		startJob(plugin.getController().getStrekningQuery(), strekningDial, 5, "Strekninger/min " + jobnr);
		startJob(plugin.getController().getTiltakQuery(), tiltakDial, 2, "Tiltak/min " + jobnr);

		SurveillanceItem datekItem = plugin.getController().createDatekItem();
		ItemPanel datekPanel = new ItemPanel(datekItem);
		startItemJob(datekItem, 10, "DatekWeb " + jobnr);
		
		JPanel dialPanel = new JPanel(new GridBagLayout());
		dialPanel.add(rundetidDial,  new GridBagConstraints(0,0, 1,1, 1.0,1.0, CENTER, BOTH, new Insets(0,0,0,0), 0,0));
		dialPanel.add(rundetid2Dial, new GridBagConstraints(1,0, 1,1, 1.0,1.0, CENTER, BOTH, new Insets(0,0,0,0), 0,0));
		dialPanel.add(rundetid3Dial,  new GridBagConstraints(2,0, 1,1, 1.0,1.0, CENTER, BOTH, new Insets(0,0,0,0), 0,0));
		
		JPanel dialPanel2 = new JPanel(new GridBagLayout());
		dialPanel2.add(prodpunktDial, 	new GridBagConstraints(0,0, 1,1, 1.0,1.0, CENTER, BOTH, new Insets(0,0,0,0), 0,0));
		dialPanel2.add(tiltakDial,  	new GridBagConstraints(1,0, 1,1, 1.0,1.0, CENTER, BOTH, new Insets(0,0,0,0), 0,0));
		dialPanel2.add(strekningDial, 	new GridBagConstraints(2,0, 1,1, 1.0,1.0, CENTER, BOTH, new Insets(0,0,0,0), 0,0));
		
		add(datekPanel, new GridBagConstraints(0,0, 1,1, 1.0,0.0, WEST, NONE, new Insets(0,0,0,0), 0,0));
		add(dialPanel2, new GridBagConstraints(0,1, 1,1, 1.0,1.0, WEST, BOTH, new Insets(0,0,0,0), 0,0));
		add(dialPanel, 	new GridBagConstraints(0,2, 1,1, 1.0,1.0, WEST, BOTH, new Insets(0,0,0,0), 0,0));
		
		//Starter jobber som sjekker at kvernemotorene fungerer som de skal
		startErKvernOkJob(plugin.getController().getKvern1OkQuery(), plugin.getController().getKvernemotorInterval(), "ErKvernOk (kvern1) " + jobnr);
		startErKvernOkJob(plugin.getController().getKvern2OkQuery(), plugin.getController().getKvernemotorInterval(), "ErKvernOk (kvern2) " + jobnr);
	}
	
	private void startJobScheduler(){
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	private void startItemJob(SurveillanceItem item, int intervalSeconds, String jobName){
		JobDataMap map = new JobDataMap();
		map.put("item", item);
		JobDetail job = JobBuilder.newJob(ItemJob.class)
			.withIdentity(jobName, "itemGroup")
			.usingJobData(map)
			.build();
		
		SimpleTrigger trigger = TriggerBuilder.newTrigger()
		.withIdentity("trigger"+jobName, "itemGroup")
		.startNow()
		.withSchedule(SimpleScheduleBuilder.simpleSchedule()
				.withIntervalInSeconds(intervalSeconds)
				.repeatForever())
			.build();
		try {
			scheduler.scheduleJob(job, trigger);
			
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	private void startJob(String sql, SpeedometerDial dial, int interval, String jobName){
		JobDataMap map = new JobDataMap();
		map.put("controller", plugin.getController());
		map.put("sql", sql);
		map.put("dial", dial);
		JobDetail job = JobBuilder.newJob(SpeedometerSQLJob.class)
			.withIdentity(jobName, "sqlGroup")
			.usingJobData(map)
			.build();
		
		SimpleTrigger trigger = TriggerBuilder.newTrigger()
		.withIdentity("trigger"+jobName, "sqlGroup")
		.startNow()
		.withSchedule(SimpleScheduleBuilder.simpleSchedule()
				.withIntervalInSeconds(interval)
				.repeatForever())
			.build();
		
		try {
			scheduler.scheduleJob(job, trigger);
			
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	private void startErKvernOkJob(String sql, int interval, String jobName){
		
		JobDataMap map = new JobDataMap();
		map.put("controller", plugin.getController());
		map.put("sql", sql);
		map.put("minutes", interval);
		JobDetail job = JobBuilder.newJob(KvernOkJob.class)
			.withIdentity(jobName, "jobName")
			.usingJobData(map)
			.build();
		
		SimpleTrigger trigger = TriggerBuilder.newTrigger()
		.withIdentity("trigger"+jobName)
		.withSchedule(SimpleScheduleBuilder.simpleSchedule()
				.repeatMinutelyForever(interval))
				.build();
		try {
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void stopJobs(){
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void pauseKverneJob(JobKey jobKey){
		try {
			scheduler.pauseJob(jobKey);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void resumeKverneJob(JobKey jobKey){
		try {
			scheduler.resumeJob(jobKey);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}	
}
