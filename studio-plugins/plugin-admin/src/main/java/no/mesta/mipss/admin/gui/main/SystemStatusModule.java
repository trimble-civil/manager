package no.mesta.mipss.admin.gui.main;

import javax.swing.JPanel;

import no.mesta.mipss.admin.gui.panel.systemstatus2.SystemStatusController;
import no.mesta.mipss.admin.gui.panel.systemstatus2.SystemStatusPanel;
import no.mesta.mipss.plugin.MipssPlugin;

/**
 * The entry point and main window of the MIPSS monitoring module.
 * 
 * @author Christian Wiik
 */
public class SystemStatusModule extends MipssPlugin {
	
	private static final long serialVersionUID = 6389658205665868075L;
	private SystemStatusController controller;
	
	private SystemStatusPanel moduleGui;
	@Override
	public JPanel getModuleGUI() {
		return moduleGui;
	}
	
	@Override
	protected void initPluginGUI() {
		controller = new SystemStatusController(this);
		moduleGui = new SystemStatusPanel(this);
		
	}
	
	public SystemStatusController getController(){
		return controller;
	}

	@Override
	public void shutdown() {
		moduleGui.stopJobs();
	}

}