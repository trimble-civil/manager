package no.mesta.mipss.admin.rapporter.gui;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class ByteArrayCellRenderer extends DefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (value instanceof byte[]){
			StringBuilder stringValue = new StringBuilder();
			for (byte b:(byte[])value){
				String hexString = Integer.toHexString(b);
				hexString = String.format("%2s", hexString).replace(' ', '0');
				stringValue.append(hexString);
			}
			value = stringValue.toString();
		}
		Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		return renderer;
	}

}
