package no.mesta.mipss.admin.common.renderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.systemstatus.dto.StatusDetail;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

/**
 * Renderer for visning av <tt>StatusDetail</tt> objekt i tabell-celle.
 * 
 * @author Christian Wiik (Mesan)
 */
public class StatusDetailCellRenderer implements TableCellRenderer {
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		
		StatusDetail detail = (StatusDetail)value;
		
		JPanel cellPanel = new JPanel(new BorderLayout());
		cellPanel.setBackground(Color.WHITE);
		
		JButton detailButton = new JButton("...");
		detailButton.addActionListener(new DetailButtonListener(detail));
		detailButton.setBackground(Color.WHITE);
		
		cellPanel.add(detailButton);
		return cellPanel;
	}
	
	private class DetailButtonListener implements ActionListener {
		
		private StatusDetail detail;
		
		public DetailButtonListener(StatusDetail detail) {
			this.detail = detail;
		}
		
		public void actionPerformed(ActionEvent event) {
			MipssDialogue dialogue = new MipssDialogue(
						null,
						IconResources.INFO_ICON,
						"Detaljert status",
						detail.getDescription(),
						MipssDialogue.Type.INFO,
						new MipssDialogue.Button[] { MipssDialogue.Button.OK } );
			
			dialogue.askUser();
		}
		
		public StatusDetail getDetail() {
			return detail;
		}
		
	}

}
	
	
