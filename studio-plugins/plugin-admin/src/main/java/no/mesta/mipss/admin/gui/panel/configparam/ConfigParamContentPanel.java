package no.mesta.mipss.admin.gui.panel.configparam;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import no.mesta.mipss.admin.gui.main.ConfigParamModule;
import no.mesta.mipss.admin.gui.panel.configparam2.ConfigParamPanel;

/**
 * Panel containing the different config param panels.
 * 
 * @author Christian Wiik 
 */
public class ConfigParamContentPanel extends JPanel {
	
	private static final long serialVersionUID = -5120589710933685580L;
	private ConfigParamModule plugin;
	private ConfigParamPanel panel;

	public ConfigParamContentPanel(ConfigParamModule plugin) {
		this.plugin = plugin;
		init();
	}
	
	private void init() {
		
		this.setLayout(new BorderLayout());
		this.setName("Konfigparam");
		
//		ConfigParamDataPanel dataPanel = new ConfigParamDataPanel();
//		ConfigParamButtonPanel buttonPanel = new ConfigParamButtonPanel();
//
//		add(dataPanel, BorderLayout.CENTER);
//		add(buttonPanel, BorderLayout.SOUTH);
//		
		panel = new ConfigParamPanel(plugin);
		add(getContentPanel());
	}
	
	public void dispose(){
		plugin = null;
		getContentPanel().dispose();
	}


	public ConfigParamPanel getContentPanel() {
		return panel;
	}
	
}