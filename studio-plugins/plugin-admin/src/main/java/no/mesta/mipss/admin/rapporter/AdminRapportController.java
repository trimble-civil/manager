package no.mesta.mipss.admin.rapporter;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import no.mesta.mipss.admin.rapporter.gui.HullrapportPanel;
import no.mesta.mipss.admin.rapporter.gui.SqlRapportPanel;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.persistence.kontrakt.SlettFeltstatistikk;
import no.mesta.mipss.persistence.rapportfunksjoner.Hullrapport;
import no.mesta.mipss.produksjonsrapport.AdminrapportService;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.util.EpostUtils;

@SuppressWarnings("serial")
public class AdminRapportController {

	
	private final AdminRapportModule plugin;
	private AdminrapportService rapportService;
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");
	
	public AdminRapportModule getPlugin(){
		return plugin;
	}
	public AdminRapportController(AdminRapportModule plugin){
		this.plugin = plugin;
		rapportService = BeanUtil.lookup(AdminrapportService.BEAN_NAME, AdminrapportService.class);
	}
	
	public List<Hullrapport> getHullOverlapp(){
		return rapportService.hentHullrapport();
	}
	public Map<String, Vector<?>> getOversiktSynkprodpunkt(){
		return rapportService.hentOversiktSynkprodpunkt();
	}
	public Map<String, Vector<?>> getSpokelserapport(){
		return rapportService.hentSpokelsesrapport();
	}
	public Map<String, Vector<?>> getSprederrapport(String serienummer, Long kontraktId, Date fraDato, Date tilDato) throws Exception{
		return rapportService.hentSprederrapport(serienummer, kontraktId, fraDato, tilDato);
	}
	public Map<String, Vector<?>> getSprederdetalj(String serienummer, Date fraDato, Date tilDato) throws Exception{
		Map<String, Vector<?>> detalj = rapportService.hentSprederdetalj(serienummer, fraDato, tilDato);
		return detalj;
	}
	public Map<String, Vector<?>> getUkjentStroproduktAdmin(Date fraDato, Date tilDato) throws Exception{
		Map<String, Vector<?>> ukjentStroprodukt = rapportService.hentUkjenteStroprodukterAdmin(fraDato, tilDato);
		return ukjentStroprodukt;
	}
	
	public Action getExportExcelAction(final JMipssBeanTable<Hullrapport> tblRapport){
		return new AbstractAction(resources.getGuiString("label.eksporterExcel"), IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				File excelFile = tblRapport.exportToExcel(JMipssBeanTable.EXPORT_TYPE.ALL_ROWS);
				DesktopHelper.openFile(excelFile);
			}
		};
	}
	
	public Action getOverlappHullRapportAction(){
		return new AbstractAction(resources.getGuiString("label.overlappHull"), IconResources.SPREADSHEET_SMALL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog d = new JDialog(plugin.getLoader().getApplicationFrame(), resources.getGuiString("label.overlappHull"));
				HullrapportPanel panel = new HullrapportPanel(plugin, AdminRapportController.this);
				d.add(panel);
				d.setSize(500, 500);
				panel.loadRapport();
				d.setLocationRelativeTo(d.getParent());
				d.setVisible(true);
			}
		};
	}

	public Action getSpokelseRapportAction() {
		return createSqlRapportAction(resources.getGuiString("label.spokelse"), new SqlRapportPanel(){
			@Override
			protected Map<String, Vector<?>> getData(){
				try {
					return getSpokelserapport();
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, resources.getGuiString("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}
	
	public Action getSynkOversiktAction() {
		return createSqlRapportAction(resources.getGuiString("label.synkOversikt"), new SqlRapportPanel(resources.getGuiString("title.synkprodpunktRapport")){
			@Override
			protected Map<String, Vector<?>> getData(){
				try {
					return getOversiktSynkprodpunkt();
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, resources.getGuiString("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}
	
	private Action createSqlRapportAction(final String title, final SqlRapportPanel panel){
		return new AbstractAction(title, IconResources.SPREADSHEET_SMALL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog d = new JDialog(plugin.getLoader().getApplicationFrame(), title);
				panel.loadTable();
				d.add(panel);
				d.setSize(500, 500);
				d.setLocationRelativeTo(d.getParent());
				d.setVisible(true);
			}
		};
	}
	
	public Action getSprederRapportAction(final JTextField txtSerienummer, final JMipssContractPicker contractPickerSpreder, final MipssDateTimePickerHolder fraDatoSpreder, final MipssDateTimePickerHolder tilDatoSpreder){
		return createSqlRapportAction(resources.getGuiString("label.sprederrapport"), 
				new SqlRapportPanel(resources.getGuiString("title.sprederRapport")){
			@Override
			protected Map<String, Vector<?>> getData(){
				try {
					return getSprederrapport(txtSerienummer.getText(), contractPickerSpreder.getValgtKontrakt().getId(), fraDatoSpreder.getDate(), tilDatoSpreder.getDate());
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, resources.getGuiString("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}

	public Action getSprederDetaljAction(final JTextField txtSerienummerDetalj, final MipssDateTimePickerHolder fraDatoDetalj, final MipssDateTimePickerHolder tilDatoDetalj){
		return createSqlRapportAction(resources.getGuiString("label.sprederdetalj"), 
				new SqlRapportPanel(resources.getGuiString("title.sprederDetaljRapport")){
			@Override
			protected Map<String, Vector<?>> getData(){
				try {
					return getSprederdetalj(txtSerienummerDetalj.getText(), fraDatoDetalj.getDate(), tilDatoDetalj.getDate());
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, resources.getGuiString("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}
	
	public Action getUkjentStroproduktAction(final JXDatePicker fraDato, final JXDatePicker tilDato) {
		
		return createSqlRapportAction(resources.getGuiString("label.ukjentStroprodukt"), new SqlRapportPanel(resources.getGuiString("title.ukjentStroprodukt")) {
			
			@Override
			protected Map<String, Vector<?>> getData() {
				try {
					return getUkjentStroproduktAdmin(fraDato.getDate(), tilDato.getDate());
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.showError(e, resources.getGuiString("error.feilHentingAvRapport", EpostUtils.getSupportEpostAdresse()));
				}
				return null;
			}
		});
	}
	
	public List<SlettFeltstatistikk> getAlleAktiveSlettinger(){
		return rapportService.getAlleAktiveSlettingerStatistikk();
	}
	public void persistSlettFeltstatistikk(List<SlettFeltstatistikk> stats){
		for (SlettFeltstatistikk fs:stats){
			persistSlettFeltstatistikk(fs);
		}
	}
	public void persistSlettFeltstatistikk(SlettFeltstatistikk stat){
		rapportService.persistSlettFeltstatistikk(stat);
	}
}