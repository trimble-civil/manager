package no.mesta.mipss.admin.common.factory;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.admin.common.renderer.StatusDetailCellRenderer;
import no.mesta.mipss.admin.common.renderer.StatusFlagCellRenderer;
import no.mesta.mipss.systemstatus.dto.StatusDetail;
import no.mesta.mipss.systemstatus.dto.StatusFlag;

import org.jdesktop.swingx.JXTable;

/**
 * Factory creating, caching and passing out references to system status tables.
 * 
 * @author Christian Wiik (Mesan)
 */
public class SimpleSystemStatusTableFactory extends SystemStatusTableFactory {
	
	/**
	 * <p>Retrieves a table reference for a given system enumerated value.</p>
	 * 
	 * <p>The table is created and cached upon the first call (involving the system) to this method.</p>
	 * 
	 * @param system - the system enum value.
	 * @return statusTable - a status table for the system.
	 */
	@Override
	public JXTable getTable(SystemStatusComponentEnum system) {
		JXTable statusTable = createTable(system);
		return statusTable;
	}
	
	/**
	 * Creates a status table for a given system.
	 * 
	 * @param system - the enum value representing the system/component.
	 * @return statusTable - a status table for the system.
	 */
	private JXTable createTable(SystemStatusComponentEnum system) {
		
		JXTable statusTable = new JXTable();
		
		statusTable.setDefaultRenderer(StatusDetail.class, new StatusDetailCellRenderer());
		statusTable.setDefaultRenderer(StatusFlag.class, new StatusFlagCellRenderer());
		
		return statusTable;
	}
	
}