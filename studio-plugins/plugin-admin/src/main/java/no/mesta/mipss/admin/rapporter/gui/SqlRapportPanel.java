package no.mesta.mipss.admin.rapporter.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ExcelHelper;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public abstract class SqlRapportPanel extends JPanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");

	private JButton btnExcel;
	private JButton btnLukk;
	private JXTable tblData;
	private JXBusyLabel bsyRapport;
	private JScrollPane scrRapport;

	protected Map<String, Vector<?>> rapport;

	private String title;
	
	public SqlRapportPanel(){
		title = resources.getGuiString("spokelsesrapport.excel");
		initComponents();
		initGui();
	}
	
	public SqlRapportPanel(String title){
		this();
		this.title = title;
	}

	private void initComponents() {
		bsyRapport = new JXBusyLabel();
		bsyRapport.setText(resources.getGuiString("label.henterSpokelserapport"));
		scrRapport = new JScrollPane();
		
		btnLukk = new JButton(getLukkAction());
		btnExcel = new JButton(getExcelAction());
		tblData = new JXTable();
	}

	private void initGui() {
		setLayout(new BorderLayout());
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(btnExcel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0, 0));
		buttonPanel.add(btnLukk,  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0, 0));
		
		add(scrRapport, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}
	
	protected abstract Map<String, Vector<?>> getData();
	
	public void loadTable(){
		bsyRapport.setBusy(true);
		scrRapport.setViewportView(bsyRapport);
		new SwingWorker<Void, Void>(){
			Map<String, Vector<?>> dataRapport;
			@Override
			protected Void doInBackground() throws Exception {
				try{
					dataRapport = getData();
					bsyRapport.setBusy(false);
				} catch (Throwable t){
					t.printStackTrace();
				}
				return null;
			}
			
			protected void done(){
				SqlRapportPanel.this.rapport = dataRapport;
				DefaultTableModel model = new DefaultTableModel(dataRapport.get("data"), dataRapport.get("columns"));
				tblData.setModel(model);
				tblData.setDefaultRenderer(byte[].class, new ByteArrayCellRenderer());
				tblData.setDefaultRenderer(Object.class, new TimestampCellRenderer());
				bsyRapport.setBusy(false);
				scrRapport.setViewportView(tblData);
			}
		}.execute();
	}
	public void exportToExcel(){
		Vector<?> columns = rapport.get("columns");
		Vector<Vector<Object>> data = (Vector<Vector<Object>>) rapport.get("data");
		
		ExcelHelper excel = new ExcelHelper();
		WritableWorkbook workbook = excel.createNewWorkbook(title);
		WritableSheet rodeSheet = excel.createNewSheet("rodeinfo", workbook, 0);
		String[] headerColumns = columns.toArray(new String[]{});
		excel.writeHeaders(rodeSheet, 0, 0, true, headerColumns);
		
		int row = 2;
		for (int i=0;i<data.size();i++){
			Vector<Object> vector = data.get(i);
			int col = 0;
			for (Object s:vector){
				excel.writeData(rodeSheet, row, col++, s);
			}
			row++;
		}
		excel.closeWorkbook(workbook);
		DesktopHelper.openFile(excel.getExcelFile());
	}
	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {
		ResultSetMetaData metaData = rs.getMetaData();
		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}
		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				Object object = rs.getObject(columnIndex);
				if (object instanceof byte[]){
					object = new String((byte[])object);
				}
				vector.add(object);
			}
			data.add(vector);
		}
		DefaultTableModel model =  new DefaultTableModel(data, columnNames);
		return model;

	}
	
	private Action getExcelAction(){
		return new AbstractAction("Excel", IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				exportToExcel();
			}
		};
	}
	
	private Action getLukkAction(){
		return new AbstractAction("Lukk", IconResources.ABORT_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				Window window = SwingUtilities.windowForComponent(SqlRapportPanel.this);
				window.dispose();
			}
		};
	}
}
