package no.mesta.mipss.admin.gui.panel.systemstatus;

import javax.swing.SwingWorker;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;
import no.mesta.mipss.admin.gui.model.SystemStatusTableModel;

public class SystemStatusWorker extends SwingWorker<SystemStatusTableModel, Integer> {
	
	private SystemStatusComponentEnum system;
	private AbstractSystemStatusPanel panel;

	public SystemStatusWorker(AbstractSystemStatusPanel panel, SystemStatusComponentEnum system) {
		this.panel = panel;
		this.system = system;
		panel.setBusy(true);
	}
	@Override
	protected SystemStatusTableModel doInBackground() throws Exception {
		SystemStatusTableModel model = new SystemStatusTableModel(system);
		
		return model;
	}
	
	public void done() {
		panel.setBusy(false);
	}
	
	

}