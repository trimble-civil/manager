package no.mesta.mipss.admin.gui.panel.configparam2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.admin.common.enumeration.Properties;
import no.mesta.mipss.admin.common.reflect.ReflectiveEntityServiceAccessor;
import no.mesta.mipss.admin.gui.main.ConfigParamModule;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.applikasjon.Konfigparamlogg;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.UIUtils;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.TextAreaProvider;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.decorator.Filter;
import org.jdesktop.swingx.decorator.FilterPipeline;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

public class ConfigParamPanel extends JPanel{

	private JMipssBeanTable<Konfigparam> table;
	private MipssBeanTableModel<Konfigparam> model;
	private ConfigParamModule plugin;
	private MipssObservableList<Konfigparam> observableList;
	private ConfigparamChangeListener changeListener;
	private JList appList;
	private ListSelectionListener appListAction;
	private DefaultListModel appListModel;
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");
	
	public ConfigParamPanel(ConfigParamModule plugin){
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui() {
		setLayout(new BorderLayout());
		
		JPanel applicationPanel = createChooseApplicationPanel();
		JPanel tablePanel = createTablePanel();
		JPanel buttonPanel = createButtonPanel();
		
		add(applicationPanel, BorderLayout.WEST);
		add(tablePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
	}
	
	public void dispose(){
		plugin = null;
	}
	
	private JPanel createButtonPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		JButton newButton = new JButton(new NewAction("Ny", IconResources.NEW_ICON));
		JButton saveButton = new JButton(new SaveAction("Lagre", IconResources.SAVE_ICON));
		JButton deleteButton = new JButton(new DeleteAction("Fjern", IconResources.BUTTON_CANCEL_ICON));
		JButton viewButton = new JButton(new ViewAction("Vis endringer", IconResources.ACCESS_LEVEL));
		JButton loggButton = new JButton(new HistoryAction("Vis historikk", IconResources.HISTORY_ICON));
		JButton clearEm = new JButton(new ClearEmAction("Clear EM", IconResources.SERIOUS_OK_ICON));
		JButton btnMFUpdate = new JButton(new MipssFeltUpdateAction("Oppdater Mipss-felt", IconResources.ACTIVATE_16));
		panel.add(UIUtils.getHStrut(5));
		panel.add(viewButton);
		panel.add(UIUtils.getHStrut(5));
		panel.add(loggButton);
		panel.add(UIUtils.getHStrut(15));
		panel.add(clearEm);
		panel.add(UIUtils.getHStrut(15));
		panel.add(btnMFUpdate);
		panel.add(Box.createHorizontalGlue());
		panel.add(newButton);
		panel.add(UIUtils.getHStrut(5));
		panel.add(deleteButton);
		panel.add(UIUtils.getHStrut(5));
		panel.add(saveButton);
		panel.add(UIUtils.getHStrut(5));
		JPanel c = new JPanel();
		c.setLayout(new BoxLayout(c, BoxLayout.PAGE_AXIS));
		c.add(UIUtils.getVStrut(5));
		c.add(panel);
		c.add(UIUtils.getVStrut(5));
		return c;
	}
	
	private void resetAppListModel(){
		appListModel = new DefaultListModel();
		appListModel.addElement("<Alle>");
		for (String appName:getApplicationNames()){
			appListModel.addElement(appName);
		}
		appList.setModel(appListModel);
	}
	
	private JPanel createChooseApplicationPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		appList = new JList();
		resetAppListModel();
	
		appListAction = new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent e) {
				JList list = (JList)e.getSource();
				int idx = list.getSelectedIndex();
				
				String filter = (String)list.getModel().getElementAt(idx);
				System.out.println(filter);
				if(filter.equalsIgnoreCase("<alle>")) {
					table.setFilters(null);
				}else {
					FilterPipeline fp = null;
					MipssPatternFilter pf = new MipssPatternFilter("^"+filter+"$", 0, 1);
					fp = new FilterPipeline(new Filter[] { pf });
					table.setFilters(fp);
				}
			}
			
		};
		appList.setSelectedIndex(0);
		appList.addListSelectionListener(appListAction);
		panel.add(new JLabel("Begrens listen til applikasjon:"), BorderLayout.NORTH);
		panel.add(new JScrollPane(appList), BorderLayout.CENTER);
		
//		JPanel p = new JPanel();
//		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
//		p.add(UIUtils.getVStrut(5));
//		p.add(panel);
//		p.add(UIUtils.getVStrut(5));
		return panel;
	}
	
	private JPanel createTablePanel(){
		JPanel panel = new JPanel(new BorderLayout());
		String[] columns = new String[]{"navn", "applikasjon", "verdi", "beskrivelse"};
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Konfigparam.class, columns);
		int[] editableColumns = new int[]{0,1,2,3};
		model = new MipssBeanTableModel<Konfigparam>(Konfigparam.class, editableColumns, columns);
		
		model.setBeanInstance(BeanUtil.lookup(MipssEntityService.BEAN_NAME, MipssEntityService.class));
		model.setBeanInterface(MipssEntityService.class);		
		String listMethod = Properties.LIST_PREFIX.getPropertyName() + Konfigparam.class.getSimpleName() + Properties.LIST_SUFFIX.getPropertyName();
		model.setBeanMethod(listMethod);
		model.loadData();
		
		while (model.isLoadingData()){
			Thread.yield();
		}
		observableList = new MipssObservableList<Konfigparam>(model.getEntities(), true);
		changeListener = new ConfigparamChangeListener(plugin);
		observableList.addObservableListListener(changeListener);
		model.setEntities(observableList);
		
		table = new JMipssBeanTable<Konfigparam>(model, columnModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFillsViewportWidth(true);
		table.setEditable(true);
		table.setRowHeightEnabled(true);
		table.setHighlighters(HighlighterFactory.createAlternateStriping());
		table.setDefaultRenderer(Object.class, new DefaultTableRenderer(new TextAreaProvider()));
		table.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnAdded(TableColumnModelEvent e) {}
            public void columnRemoved(TableColumnModelEvent e) {}
            public void columnMoved(TableColumnModelEvent e) {}
            public void columnSelectionChanged(ListSelectionEvent e) {}
            public void columnMarginChanged(ChangeEvent e) {
                for (int row = 0; row < table.getRowCount(); row++) {
                    int rowHeight = 0;
                    for (int column = 0; column < table.getColumnCount(); column++) {
                        Component comp = table.prepareRenderer(
                        		table.getCellRenderer(row, column), row, column);
                        rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
                    }
                    table.setRowHeight(row, rowHeight); 
                }
            }
        });
		
		JScrollPane scroll = new JScrollPane(table);
		panel.add(scroll);
		return panel;
	}
	public boolean isDataSaved(){
		if (!changeListener.getChangedList().isEmpty()||!changeListener.getRemovedList().isEmpty()||!changeListener.getAddedList().isEmpty()){
			return false;
		}
		return true;
	}
	/**
	 * Henter initiell liste med alle applikasjons-navn.
	 * 
	 * @return entityList - liste med alle applikasjons-navn.
	 */
	private List<String> getApplicationNames() {
		List<String> applicationNames = KonfigparamPreferences.getInstance().hentApplikasjonListe();
		return applicationNames;
	}
	
	private class NewAction extends AbstractAction {
		public NewAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			
			Konfigparam newParam = new Konfigparam();
			newParam.getOwnedMipssEntity().setOpprettetAv(plugin.getLoader().getLoggedOnUserSign());
			newParam.getOwnedMipssEntity().setOpprettetDato(Clock.now());
			
			int idx = appList.getSelectedIndex();
			String item = (String)appListModel.getElementAt(idx);
			if (!item.equalsIgnoreCase("<alle>")){
				newParam.setApplikasjon(item);
			}
			model.getEntities().add(0, newParam);
			table.getSelectionModel().setLeadSelectionIndex(0);
			table.requestFocus();
		}
	}
	
	private class SaveAction extends AbstractAction {
		public SaveAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			String storeMethodName = Properties.PERSIST_PREFIX.getPropertyName() + Konfigparam.class.getSimpleName() + Properties.PERSIST_SUFFIX.getPropertyName();
			String removeMethodName = Properties.REMOVE_PREFIX.getPropertyName() + Konfigparam.class.getSimpleName() + Properties.REMOVE_SUFFIX.getPropertyName();
			
			ReflectiveEntityServiceAccessor entityServiceAccessor = new ReflectiveEntityServiceAccessor();
			
			List<IRenderableMipssEntity> removed = new ArrayList<IRenderableMipssEntity>();
			removed.addAll(changeListener.getRemovedList());
			List<IRenderableMipssEntity> changed = new ArrayList<IRenderableMipssEntity>();
			changed.addAll(changeListener.getAddedList());
			changed.addAll(changeListener.getChangedList());
			if (removed.size()>0){
				String slettWarning = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.slett");
				String slettWarningInfo = PropertyResourceBundleUtil.getPropertyBundle("adminText").getGuiString("warning.slett.info");
				int c = 1;
				for (Konfigparam p:changeListener.getRemovedList()){
					slettWarning+="\n"+c+": "+p.toString().substring(p.toString().indexOf('['));
					c++;
				} 
				int rvl = JOptionPane.showConfirmDialog(ConfigParamPanel.this, slettWarning+"\n\nEr du sikker? "+slettWarningInfo, "Sletting av konfigparams", JOptionPane.YES_NO_CANCEL_OPTION); 
				if (rvl == JOptionPane.YES_OPTION){
					entityServiceAccessor.invokeMethodSingleListParameter(removeMethodName, removed);
				}else
					return;
			}
			if (changed.size()>0){
				try{
					entityServiceAccessor.invokeMethodSingleListParameter(storeMethodName, changed);
				} catch (Exception ex){
					ErrorInfo info = new ErrorInfo("Parameteren er låst", "Klarte ikke å lagre alle parameterne!\n Kanskje det finnes en nyere versjon av en av parameterne på serveren? \n", null, "Advarsel", ex, Level.SEVERE, null);
			        JXErrorPane.showDialog(plugin.getModuleGUI(), info);
				}
			}
			changeListener.clearLists();
			int idx = appList.getSelectedIndex();
			String selected = (String)appList.getModel().getElementAt(idx);
			appList.removeListSelectionListener(appListAction);
			resetAppListModel();
			appList.addListSelectionListener(appListAction);
			
			if (comboboxContainsApp(selected)){
				appList.setSelectedValue(selected, true);
			}else{
				appList.setSelectedIndex(0);
			}
			
			model.loadData();
			while (model.isLoadingData()){
				Thread.yield();
			}
			observableList.removeObservableListListener(changeListener);
			
			observableList = new MipssObservableList<Konfigparam>(model.getEntities(), true);
			observableList.addObservableListListener(changeListener);
			model.setEntities(observableList);
		}
	}
	
	private boolean comboboxContainsApp(String app){
		
		for (int i =0;i<appList.getModel().getSize();i++){
			String s = (String)appList.getModel().getElementAt(i);
			if (s.equals(app)){
				return true;
			}
		}
		return false;
	}
//	/**
//	 * litt hacky..
//	 * @param list
//	 */
//	private void addApplikasjonToComboBox(List<Konfigparam> list){
//		int items = appList.getModel().getSize();
//		for (Konfigparam p:list){
//			boolean newitem = true;
//			for (int i=0;i<items;i++){
//				String s = (String)appList.getModel().getElementAt(i);
//				if (s.equals(p.getApplikasjon())){
//					newitem = false;
//				}
//			}
//			if (newitem){
//				appListModel.addElement(p.getApplikasjon());
//			}
////				comboBox.addItem(p.getApplikasjon());
//		}
//	}
	private class DeleteAction extends AbstractAction {
		public DeleteAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			int modelRow =table.convertRowIndexToModel(table.getSelectedRow());
			table.getSelectionModel().clearSelection();
			model.getEntities().remove(modelRow);
			model.fireTableModelEvent(0, 0, TableModelEvent.DELETE);
		}
		
	}
	
	private class ViewAction extends AbstractAction {
		public ViewAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			String removed = "";
			int c=1;
			for (Konfigparam p:changeListener.getRemovedList()){
				removed+="\n"+c+": "+p.toString().substring(p.toString().indexOf('['));
				c++;
			}
			String added = "";
			c=0;
			for (Konfigparam p:changeListener.getAddedList()){
				added+="\n"+c+": "+p.toString().substring(p.toString().indexOf('['));
				c++;
			}
			String changed = "";
			c=0;
			for (Konfigparam p:changeListener.getChangedList()){
				changed+="\n"+c+": "+p.toString().substring(p.toString().indexOf('['));
				c++;
			}
			String info = "Slettet:"+removed+"\n\nLagt til:"+added+"\n\nEndret:"+changed;
			JOptionPane.showMessageDialog(ConfigParamPanel.this, info, "Endringer i konfigparam", JOptionPane.INFORMATION_MESSAGE);

		}
	}
	
	@SuppressWarnings("serial")
	private class ClearEmAction extends AbstractAction{
		public ClearEmAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e){
			String message = resources.getGuiString("clearEm.message");
			String title = resources.getGuiString("clearEm.title");
			int r = JOptionPane.showConfirmDialog(plugin.getLoader().getApplicationFrame(), message, title, JOptionPane.YES_NO_CANCEL_OPTION);
			if (r==JOptionPane.YES_OPTION){
				ServerUtils.getInstance().clearEM();
			}
			
		}
	}
	@SuppressWarnings("serial")
	private class MipssFeltUpdateAction extends AbstractAction{
		public MipssFeltUpdateAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e){
			MipssFeltUpdateDialog dialog = new MipssFeltUpdateDialog(plugin.getLoader().getApplicationFrame(), plugin);
			dialog.setSize(500, 400);
//			dialog.setResizable(false);
			dialog.setLocationRelativeTo(ConfigParamPanel.this);
			dialog.setVisible(true);
		}
	}
	@SuppressWarnings("serial")
	private class HistoryAction extends AbstractAction{
		public HistoryAction(String text, ImageIcon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			List<Konfigparam> selectedEntities = table.getSelectedEntities();
			if (selectedEntities.size()==1){
				Konfigparam p = selectedEntities.get(0);
				List<Konfigparamlogg> loggList = KonfigparamPreferences.getInstance().hentLoggForKonfigparam(p);
				
				
				String[] columns = new String[]{"id", "applikasjon", "navn", "verdi", "beskrivelse", "opprettetAv", "opprettetDato", "endretAv", "endretDato",
						"slettetAv", "slettetDato"};
				MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Konfigparamlogg.class, columns);
				MipssBeanTableModel<Konfigparamlogg> model = new MipssBeanTableModel<Konfigparamlogg>(Konfigparamlogg.class, columns);

				final JMipssBeanTable<Konfigparamlogg> table = new JMipssBeanTable<Konfigparamlogg>(model, columnModel);
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				table.setFillsViewportWidth(true);
				table.setEditable(true);
				table.setRowHeightEnabled(true);
				table.setHighlighters(HighlighterFactory.createAlternateStriping());
				table.setDefaultRenderer(Object.class, new DefaultTableRenderer(new TextAreaProvider()));
				table.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
		            public void columnAdded(TableColumnModelEvent e) {}
		            public void columnRemoved(TableColumnModelEvent e) {}
		            public void columnMoved(TableColumnModelEvent e) {}
		            public void columnSelectionChanged(ListSelectionEvent e) {}
		            public void columnMarginChanged(ChangeEvent e) {
		                for (int row = 0; row < table.getRowCount(); row++) {
		                    int rowHeight = 0;
		                    for (int column = 0; column < table.getColumnCount(); column++) {
		                        Component comp = table.prepareRenderer(
		                        		table.getCellRenderer(row, column), row, column);
		                        rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
		                    }
		                    table.setRowHeight(row, rowHeight); 
		                }
		            }
		        });
				
				JScrollPane scroll = new JScrollPane(table);
				JDialog d = new JDialog(SwingUtilities.windowForComponent(plugin.getModuleGUI()));
				d.add(scroll);
				d.pack();
				d.setLocationRelativeTo(plugin.getModuleGUI());
				model.setEntities(loggList);
				d.setVisible(true);
			}
			
		}
		
	}


}
