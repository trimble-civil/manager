package no.mesta.mipss.admin.common.factory;

import no.mesta.mipss.admin.common.enumeration.SystemStatusComponentEnum;

import org.jdesktop.swingx.JXTable;

/**
 * Abstract factory for system status table creation.
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link SimpleSystemStatusTableFactory}
 */
public abstract class SystemStatusTableFactory {
	
	public static SystemStatusTableFactory instance;
	
	public abstract JXTable getTable(SystemStatusComponentEnum system);
	
	/**
	 * Retrieves a system status table factory instance. Should preferred over direct instantiation of the factory.
	 * 
	 * @return a system status table factory instance.
	 */
	public static SystemStatusTableFactory getInstance() {
		if(instance == null) {
			instance = new SimpleSystemStatusTableFactory();
		}
		
		return instance;
	}
	
}