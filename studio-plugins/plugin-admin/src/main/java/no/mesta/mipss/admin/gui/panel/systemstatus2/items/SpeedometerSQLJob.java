package no.mesta.mipss.admin.gui.panel.systemstatus2.items;

import no.mesta.mipss.admin.gui.panel.systemstatus2.SpeedometerDial;
import no.mesta.mipss.admin.gui.panel.systemstatus2.SystemStatusController;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class SpeedometerSQLJob implements Job{
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		String sql = (String) map.get("sql");
		SystemStatusController controller = (SystemStatusController)map.get("controller");
		SpeedometerDial dial = (SpeedometerDial)map.get("dial");
		double poll = controller.poll(sql);
		dial.setValue(poll);
//		SwingUtilities.invokeLater(new Runnable(){
//			@Override
//			public void run() {
//				double poll = controller.poll(sql);
//				dial.setValue(poll);
//			}
//		});
	}

	
}
