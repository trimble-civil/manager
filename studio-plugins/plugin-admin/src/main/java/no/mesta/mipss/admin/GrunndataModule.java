package no.mesta.mipss.admin;

import javax.swing.JComponent;

import no.mesta.mipss.plugin.MipssPlugin;
// replaces : no.mesta.mipss.admin.gui.main.SourceDataModule
public class GrunndataModule extends MipssPlugin{
	private GrunndataModuleGuiDelegate pluginGui;
	
	@Override
	public JComponent getModuleGUI() {
		return pluginGui;
	}

	@Override
	protected void initPluginGUI() {
		pluginGui = new GrunndataModuleGuiDelegate(this);
	}

	@Override
	public void shutdown() {
		
	}

}
