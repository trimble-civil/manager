package no.mesta.mipss.admin.gui.panel.systemstatus2;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.Action;

import no.mesta.mipss.common.PropertyChangeSource;

public abstract class SurveillanceItem implements PropertyChangeSource{
	public enum STATE{
		OK, WARN, FAILED
	}
	
	private String name;
	private STATE state;
	private Action infoAction;
	
	private PropertyChangeSupport props = getProps();

	
	public SurveillanceItem(){
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public STATE getState() {
		return state;
	}
	public void setState(STATE state) {
		STATE old = this.state;
		this.state = state;
		props.firePropertyChange("state", old, state);
	}
	
	public Action getInfoAction() {
		return infoAction;
	}
	public abstract void checkStatus(); 
	
	public void setInfoAction(Action infoAction) {
		this.infoAction = infoAction;
	}
	
	protected PropertyChangeSupport getProps() {
		if (props == null) {
			props = new PropertyChangeSupport(this);
		}

		return props;
	}
	
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
		
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
	
	
	
}
