package no.mesta.mipss.admin.gui.panel.systemstatus3;

import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.admin.gui.main.SystemStatusFeilModule;
import no.mesta.mipss.resources.images.IconResources;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

@SuppressWarnings("serial")
public class SystemStatusFeilPanel extends JPanel {

	private final SystemStatusFeilModule plugin;
	
	private RingPanel feilkoPanel;
	private RingPanel inspeksjonerPanel;
	private RingPanel feilmeldingerPanel;
	private RingPanel refnummerPanel;
	private RingPanel synkbildePanel;
	
	private JPanel homerPanel;
	private JLabel picLabel;

	private Scheduler scheduler;	

	public SystemStatusFeilPanel(SystemStatusFeilModule plugin) {
		this.plugin = plugin;
		initGui();
	}

	private void initGui() {
		
		setLayout(new GridLayout(2, 1));
		
		JPanel pnlTop = new JPanel(new GridLayout(1, 3));
		JPanel pnlBottom = new JPanel(new GridLayout(1, 3));
		
		feilkoPanel = new RingPanel("Feilkø", 10);
		inspeksjonerPanel = new RingPanel("Inspeksjoner", 10);
		feilmeldingerPanel = new RingPanel("Feilmeldinger", 10);
		refnummerPanel = new RingPanel("Refnummer-kø", 10);
		synkbildePanel = new RingPanel("Synkbilde-kø", 10);
		
		startJobScheduler();
		startJob(plugin.getController().getFeilkoQuery(), 		feilkoPanel, 		60, "Feilkø");
		startJob(plugin.getController().getInspeksjonerQuery(), inspeksjonerPanel, 	60, "Inspeksjoner");
		startJob(plugin.getController().getFeilmeldingerQuery(),feilmeldingerPanel, 60, "Feilmeldinger");
		startJob(plugin.getController().getRefnummerQuery(), 	refnummerPanel, 	60, "Refnummer");
		startJob(plugin.getController().getSynkbildeQuery(), 	synkbildePanel, 	60, "Synkbilde");

		ImageIcon img = IconResources.HOMER_HAPPY;
		homerPanel = new JPanel();
		picLabel = new JLabel();
		homerPanel.add(picLabel);
		changeImage(img);	
		
		pnlTop.add(feilkoPanel);
		pnlTop.add(inspeksjonerPanel);
		pnlTop.add(feilmeldingerPanel);
		pnlBottom.add(refnummerPanel);
		pnlBottom.add(synkbildePanel);
		pnlBottom.add(homerPanel);
		
		add(pnlTop);
		add(pnlBottom);
				
		setTestData();
	}

	private void startJob(String sql, RingPanel panel, int interval, String jobName) {
		
		JobDataMap map = new JobDataMap();
		map.put("controller", plugin.getController());
		map.put("sql", sql);
		map.put("panel", panel);
		
		JobDetail job = JobBuilder.newJob(FeilPanelSQLJob.class)
			.withIdentity(jobName, "sqlGroup")
			.usingJobData(map)
			.build();
		
		SimpleTrigger trigger = TriggerBuilder.newTrigger()
			.withIdentity("trigger"+jobName, "sqlGroup")
			.startNow()
			.withSchedule(SimpleScheduleBuilder.simpleSchedule()
					.withIntervalInSeconds(interval)
					.repeatForever())
			.build();
		
		try {
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		
	}
	
	public void stopJobs(){
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	private void setTestData() {
		feilkoPanel.setValue(0);
		inspeksjonerPanel.setValue(0);
		feilmeldingerPanel.setValue(0);
		refnummerPanel.setValue(0);
		synkbildePanel.setValue(0);		
	}
	
	private void startJobScheduler(){
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void changeImage(ImageIcon img){
		
		Image dimg = img.getImage().getScaledInstance(200, 200, Image.SCALE_SMOOTH);
		
		ImageIcon newImg = new ImageIcon(dimg);
		picLabel.setIcon(newImg);
	}
}
