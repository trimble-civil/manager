package no.mesta.mipss.admin.rapporter.gui.statistikk;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.admin.rapporter.AdminRapportController;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.SlettFeltstatistikk;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;

@SuppressWarnings("serial")
public class SlettKontraktStatistikkPanel extends JPanel{
	private final AdminRapportController controller;

	private MipssBeanTableModel<KontraktTableObject> mdlKontrakter;
	private JXTable tblKontrakter;
	private JCheckTablePanel<KontraktTableObject> scrKontrakter;
	private List<SlettFeltstatistikk> alleAktiveSlettinger;
	private JButton btnSlett;
	private JLabel lblInfo;
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("adminText");

	
	public SlettKontraktStatistikkPanel(AdminRapportController controller){
		this.controller = controller;
		initComponents();
		initGui();
		updateKontrakter();
	}

	private void initComponents() {
		mdlKontrakter = new MipssBeanTableModel<KontraktTableObject>(KontraktTableObject.class, new int[] { 0 }, "valgt", "kontraktnummer", "kontraktnavn");
		MipssRenderableEntityTableColumnModel kontraktColumnModel = new MipssRenderableEntityTableColumnModel(KontraktTableObject.class, mdlKontrakter.getColumns());
		tblKontrakter = new JXTable(mdlKontrakter,kontraktColumnModel) {
			@Override
			public boolean getScrollableTracksViewportWidth() {
				return true && getParent() instanceof JViewport && (((JViewport) getParent()).getWidth() > getPreferredSize().width);
			}
			protected boolean shouldSortOnChange(TableModelEvent e) {
				return false;
			}
		};
		tblKontrakter.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.DATE_FORMAT));
		
		tblKontrakter.setFillsViewportHeight(true);
		tblKontrakter.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
		tblKontrakter.getColumn(0).setPreferredWidth(40);
		tblKontrakter.getColumn(1).setPreferredWidth(65);
		tblKontrakter.getColumn(2).setPreferredWidth(160);

		scrKontrakter = new JCheckTablePanel<KontraktTableObject>(
				tblKontrakter,
				mdlKontrakter,
				new JCheckBox(),
				"Vennligst vent, henter kontrakter", false);
		
		tblKontrakter.setDefaultRenderer(Boolean.class, new DefaultTableRenderer(){
			@Override
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				Component cr = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				KontraktTableObject kontrakt = mdlKontrakter.get(row);
				JCheckBox c = new JCheckBox();
				c.setOpaque(false);
				c.setBackground(cr.getBackground());
				c.setForeground(cr.getForeground());
				JPanel panel = new JPanel(new GridBagLayout());
				panel.setBackground(cr.getBackground());
				panel.setForeground(cr.getForeground());
				panel.add(c, new GridBagConstraints(0,0,0,0,1.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
				Boolean v = (Boolean)value;
				c.setSelected(v);
				if (isAktive(kontrakt.getKontrakt())){
					c.setEnabled(false);
					kontrakt.setValgt(Boolean.FALSE);
				}else{
					c.setEnabled(true);
				}
				
				return panel;
			}
		});
		
		btnSlett = new JButton(getSlettAction());
		lblInfo = new JLabel(resources.getGuiString("label.slettFeltstatistikk"));
	}
	
	private Action getSlettAction() {
		return new AbstractAction("Slett valgte", IconResources.DELETE_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				slettValgte();
			}
		};
	}

	private void slettValgte(){
		int r = JOptionPane.showConfirmDialog(this, resources.getGuiString("slett.message"), resources.getGuiString("slett.title"), JOptionPane.YES_NO_OPTION);
		if (r!=JOptionPane.YES_OPTION)
			return;
		List<KontraktTableObject> alleValgte = scrKontrakter.getAlleValgte();
		List<SlettFeltstatistikk> toDelete = new ArrayList<SlettFeltstatistikk>();
		for (KontraktTableObject o:alleValgte){
			SlettFeltstatistikk fs = new SlettFeltstatistikk();
			fs.setKontraktId(o.getKontrakt().getId());
			fs.setOpprettetAv(controller.getPlugin().getLoader().getLoggedOnUserSign());
			fs.setOpprettetDato(Clock.now());
			toDelete.add(fs);
		}
		controller.persistSlettFeltstatistikk(toDelete);
		updateKontrakter();
	}
	public void updateKontrakter(){
		JMipssContractPicker cmbKontrakt = new JMipssContractPicker(controller.getPlugin().getLoader().getLoggedOnUser(false), true);
		List<Driftkontrakt> kontrakter = cmbKontrakt.getModel().getKontrakter();
		List<KontraktTableObject> k = new ArrayList<KontraktTableObject>();
		for (Driftkontrakt dk:kontrakter){
			k.add(new KontraktTableObject(dk));
		}
		mdlKontrakter.setEntities(k);
		alleAktiveSlettinger = controller.getAlleAktiveSlettinger();
	}
	
	private boolean isAktive(Driftkontrakt dk){
		for (SlettFeltstatistikk fs:alleAktiveSlettinger){
			if (fs.getKontraktId().equals(dk.getId())){
				return true;
			}
		}
		return false;
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		scrKontrakter.setPreferredSize(new Dimension(300, 180));
		btnSlett.setPreferredSize(new Dimension(300, 25));
		add(lblInfo, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		add(scrKontrakter, 	new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		add(btnSlett, 		new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,0), 0,0));
	}
}
