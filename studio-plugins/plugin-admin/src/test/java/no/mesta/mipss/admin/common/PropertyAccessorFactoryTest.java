package no.mesta.mipss.admin.common;

import junit.framework.TestCase;
import no.mesta.mipss.common.property.PropertyAccessor;
import no.mesta.mipss.common.property.PropertyAccessorFactory;

/**
 * Unit test for <tt>PropertyAccessorFactory</tt>
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link PropertyAccessorFactory}
 */
public class PropertyAccessorFactoryTest extends TestCase {
		
	public void testCanObtainPropertyAccessorFromFactory() {
		PropertyAccessor pa = PropertyAccessorFactory.getPropertyAccessor("adminText");
		assertNotNull(pa);
	}

}