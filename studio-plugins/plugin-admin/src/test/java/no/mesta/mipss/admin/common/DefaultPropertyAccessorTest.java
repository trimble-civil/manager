package no.mesta.mipss.admin.common;

import junit.framework.TestCase;
import no.mesta.mipss.admin.common.enumeration.Properties;
import no.mesta.mipss.common.property.DefaultPropertyAccessor;
import no.mesta.mipss.common.property.PropertyAccessor;

/**
 * Unit test for <tt>DefaultPropertyAccessor</tt>
 * 
 * @author Christian Wiik (Mesan)
 * @see {@link DefaultPropertyAccessor}
 */
public class DefaultPropertyAccessorTest extends TestCase {
	
	/**
	 * Tests that a <tt>String</tt> property can be found by enum value.
	 */
	public void testCanFetchStringProperty() {
		PropertyAccessor pa = new DefaultPropertyAccessor(Properties.FILE.getPropertyName());
		String expected = "MIPSS Administrasjon";
		String actual = pa.getString(Properties.TITLE);		
		assertEquals(expected, actual);
	}
	
	/**
	 * Tests that an <tt>int</tt> property can be found by enum value.
	 */
	public void testCanFetchIntProperty() {
		PropertyAccessor pa = new DefaultPropertyAccessor(Properties.FILE.getPropertyName());
		int expected = 1024;
		int actual = pa.getInt(Properties.WINDOW_WIDTH);		
		assertEquals(expected, actual);
	}

}