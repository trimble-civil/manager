package no.mesta.mipss.dfu;

import java.awt.Dimension;

import javax.swing.JComponent;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.dfu.swing.MainPluginPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.service.dfu.DfuService;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class DfuModule extends MipssPlugin {
	private BindingGroup bindingGroup;
	private DfuController dfuController;
	private MainPluginPanel mainPluginPanel;
	
	@Override
	public JComponent getModuleGUI() {
		return mainPluginPanel;
	}

	@Override
	protected void initPluginGUI() {
		DfuService dfuService = BeanUtil.lookup(DfuService.BEAN_NAME, DfuService.class);
		bindingGroup = new BindingGroup();
		dfuController = new DfuController(dfuService, bindingGroup, getLoader().getApplicationFrame(), getLoader().getLoggedOnUserSign());
		mainPluginPanel = new MainPluginPanel(dfuController);
		bindingGroup.bind();
		mainPluginPanel.setPreferredSize(new Dimension(930,500));
	}

	@Override
	public void shutdown() {
		bindingGroup.unbind();
	}
}
