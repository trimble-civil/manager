package no.mesta.mipss.dfu.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.MipssListCellRenderer;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.swingx.JXDatePicker;

public class DfuKontraktDialog extends JDialog {
	private DfuKontrakt dfuKontrakt;
	private List<Driftkontrakt> driftkontraktList;
	
	private JPanel pnlMain = new JPanel();
	private JLabel lblKontrakt = new JLabel();
	private JComboBox cbxKontrakt = new JComboBox();
	private JLabel lblNavn = new JLabel();
	private JFormattedTextField txtNavn = new JFormattedTextField();
	private JLabel lblFraDato = new JLabel();
	private JXDatePicker datePckrFraDato = new JXDatePicker();
	private JLabel lblTilDato = new JLabel();
	private JXDatePicker datePckrTilDato = new JXDatePicker();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	
	private DfuKontrakt oldValues;
	private BindingGroup bindingGroup = new BindingGroup();
	private boolean tillatNyKontrakt;
	
	public DfuKontraktDialog(JFrame parentFrame, List<Driftkontrakt> driftkontraktList) {
		super(parentFrame, DfuController.getGuiProps().getGuiString("title.dfuKontrakt"), true);
		this.driftkontraktList = driftkontraktList;
	}
	
	/**
	 * Jobber på instansen som sendes inn men returnerer denne til gammel stand ved avbryt
	 * Returnerer null om brukeren avbryter
	 * 
	 * @param dfuKontrakt
	 * @return
	 */
	public DfuKontrakt showDialog(DfuKontrakt dfuKontrakt, boolean tillatNyKontrakt) {
		this.dfuKontrakt = dfuKontrakt;
		this.tillatNyKontrakt = tillatNyKontrakt;
		this.oldValues = new DfuKontrakt();
		this.oldValues.setDriftkontrakt(dfuKontrakt.getDriftkontrakt());
		this.oldValues.setDfuindivid(dfuKontrakt.getDfuindivid());
		this.oldValues.merge(dfuKontrakt);
		
		initLabels();
		initTextFields();
		initDatePickers();
		initButtons();
		initComboboxes();
		initBindings();
		initGui();
		
		bindingGroup.bind();
		
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		
		bindingGroup.unbind();
		
		return this.dfuKontrakt;
	}
	
	@SuppressWarnings("unchecked")
	private void initComboboxes() {
		cbxKontrakt.setRenderer((ListCellRenderer)new MipssListCellRenderer(DfuController.getGuiProps().getGuiString("cbx.text.ingenKontrakt")));
        JComboBoxBinding modellCBBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ, driftkontraktList, cbxKontrakt);
        bindingGroup.addBinding(modellCBBinding);
        bindingGroup.addBinding(BindingHelper.createbinding(dfuKontrakt, "driftkontrakt", cbxKontrakt, "selectedItem", BindingHelper.READ_WRITE));       
        
        if(!tillatNyKontrakt) {
        	cbxKontrakt.setEnabled(false);
        }
        
		cbxKontrakt.setPreferredSize(new Dimension(200, 19));
	}
	
	private void initButtons() {
		btnOk.setText(DfuController.getGuiProps().getGuiString("button.ok"));
		btnAvbryt.setText(DfuController.getGuiProps().getGuiString("button.avbryt"));
		
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(dfuKontrakt.getDriftkontrakt() == null) {
					JOptionPane.showMessageDialog(DfuKontraktDialog.this, DfuController.getGuiProps().getGuiString("message.dfuKontraktUtenKontrakt"));
				} else if(dfuKontrakt.getFraDato() == null) {
					JOptionPane.showMessageDialog(DfuKontraktDialog.this, DfuController.getGuiProps().getGuiString("message.dfuKontraktUtenFraDato"));					
				} else {
					Driftkontrakt driftkontrakt = dfuKontrakt.getDriftkontrakt();
					bindingGroup.unbind();
					dfuKontrakt.setDriftkontrakt(driftkontrakt);
					dispose();
				}
			}
		});
		
		btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bindingGroup.unbind();
				dfuKontrakt.setDfuindivid(oldValues.getDfuindivid());
				dfuKontrakt.setDriftkontrakt(oldValues.getDriftkontrakt());
				dfuKontrakt.merge(oldValues);
				dfuKontrakt = null;
				dispose();
			}
		});
	}
	
	private void initLabels() {
		lblKontrakt.setText(DfuController.getGuiProps().getGuiString("label.dfuKontrakt.kontrakt"));
		lblNavn.setText(DfuController.getGuiProps().getGuiString("label.dfuKontrakt.navn"));
		lblFraDato.setText(DfuController.getGuiProps().getGuiString("label.dfuKontrakt.fraDato"));
		lblTilDato.setText(DfuController.getGuiProps().getGuiString("label.dfuKontrakt.tilDato"));
	}
	
	private void initTextFields() {
		txtNavn.setPreferredSize(new Dimension(200, 19));
		
		txtNavn.setDocument(new DocumentFilter(30, null, false));
	}
	
	private void initDatePickers() {
		datePckrFraDato.setPreferredSize(new Dimension(200, 19));
		datePckrTilDato.setPreferredSize(new Dimension(200, 19));
	}
	
	private void initBindings() {
		bindingGroup.addBinding(BindingHelper.createbinding(dfuKontrakt, "navn", txtNavn, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(dfuKontrakt, "fraDato", datePckrFraDato, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(dfuKontrakt, "tilDato", datePckrTilDato, "date", BindingHelper.READ_WRITE));
	}
	
	private void initGui() {
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblKontrakt, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxKontrakt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblNavn, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(txtNavn, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblFraDato, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(datePckrFraDato, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblTilDato, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(datePckrTilDato, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}
}
