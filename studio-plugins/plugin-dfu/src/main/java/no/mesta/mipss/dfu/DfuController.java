package no.mesta.mipss.dfu;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.dfu.swing.DfuKontraktDialog;
import no.mesta.mipss.dfu.swing.DfuStatusDialog;
import no.mesta.mipss.mipsslogg.MipssLogg;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.Dfu_Status;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfulogg;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.dfu.DfuService;
import no.mesta.mipss.ui.MipssTextAreaInputDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXTable;

public class DfuController implements PropertyChangeSource, PropertyChangeListener {
	private static PropertyResourceBundleUtil guiProps;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private DfuService dfuService;
	private BindingGroup bindingGroup;
	private JFrame parentFrame;
	private String loggedOnUser;
	private Dfuindivid dfuindivid;
	private JMipssBeanTable<DfuInfoV> tblResultat; //Pga designfeil i JXTable trenger controlleren desverre å vite om denne....
	private MipssBeanTableModel<DfuInfoV> sokResutlatTableModel;
	private ListSelectionModel sokResultatSelectionModel;
	private Boolean dfuSearchBusy = Boolean.FALSE;
	private Boolean dfuSelectedBusy = Boolean.FALSE;
	private Boolean lagreEnabled = Boolean.FALSE;
	private String resultCountString;
	private List<Driftkontrakt> driftkontraktList;
	
	public DfuController(DfuService dfuService, BindingGroup bindingGroup, JFrame parentFrame, String loggedOnUser) {
		this.dfuService = dfuService;
		this.bindingGroup = bindingGroup;
		this.parentFrame = parentFrame;
		this.loggedOnUser = loggedOnUser;
		
		sokResutlatTableModel = new MipssBeanTableModel<DfuInfoV>(DfuInfoV.class, "id", "modellNavn", "serienummer", "tlfnummer", "simkortnummer", "versjon", "statusNavn", "sisteLivstegnDato", "regnr", "maskinnummer", "eksterntNavn", "typeOgModell", "kontraktTekst");
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(DfuInfoV.class, sokResutlatTableModel.getColumns());
		sokResultatSelectionModel = new DefaultListSelectionModel();
		sokResultatSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblResultat = new JMipssBeanTable<DfuInfoV>(sokResutlatTableModel, colModel);
		tblResultat.setSelectionModel(sokResultatSelectionModel);
		tblResultat.setDoWidthHacks(false);
		tblResultat.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() >= 2) {
					openSelectedDfu();
				}
			}
		});
	}
	
	public Dfuindivid getDfuindivid() {
		return dfuindivid;
	}

	public void setDfuindivid(Dfuindivid dfuindivid) {
		Dfuindivid old = this.dfuindivid;
		this.dfuindivid = dfuindivid;
		
		if(old != null) {
			old.removePropertyChangeListener(this);
		}
		
		if(dfuindivid != null) {
			dfuindivid.addPropertyChangeListener(this);
		}
		
		setLagreEnabled(Boolean.FALSE);
		
		props.firePropertyChange("dfuindivid", old, dfuindivid);
	}

	public Boolean getDfuSearchBusy() {
		return dfuSearchBusy;
	}

	public void setDfuSearchBusy(Boolean dfuSearchBusy) {
		Boolean old = this.dfuSearchBusy;
		this.dfuSearchBusy = dfuSearchBusy;
		props.firePropertyChange("dfuSearchBusy", old, dfuSearchBusy);
	}

	public Boolean getDfuSelectedBusy() {
		return dfuSelectedBusy;
	}

	public void setDfuSelectedBusy(Boolean dfuSelectedBusy) {
		Boolean old = this.dfuSelectedBusy;
		this.dfuSelectedBusy = dfuSelectedBusy;
		props.firePropertyChange("dfuSelectedBusy", old, dfuSelectedBusy);
	}

	public String getResultCountString() {
		return resultCountString;
	}
	
	public void setResultCountString(String resultCountString) {
		String old = this.resultCountString;
		this.resultCountString = resultCountString;
		props.firePropertyChange("resultCountString", old, resultCountString);
	}
	
	public BindingGroup getBindingGroup() {
		return bindingGroup;
	}
	
    public static PropertyResourceBundleUtil getGuiProps() {
        if(guiProps == null) {
            guiProps = PropertyResourceBundleUtil.getPropertyBundle("dfuText");
        }    
        return guiProps;
    }
    
	public List<Dfumodell> getDfumodellList() {
		List<Dfumodell> list = dfuService.getDfumodellList();
		
		if(list != null) {
			Collections.sort(list, new Comparator<Dfumodell>() {
				@Override
				public int compare(Dfumodell o1, Dfumodell o2) {
					return o1.getNavn().compareTo(o2.getNavn());
				}
			});
		}
		
		return list;
	}
    
    public List<Driftkontrakt> getDriftkontraktList() {
		if(driftkontraktList == null) {
			driftkontraktList = dfuService.getListDriftkontrakt();
		}
		
    	return driftkontraktList;
	}

	@SuppressWarnings("unchecked")
	private void doDfuSearch(final String sokArg) {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
		    	setDfuSearchBusy(Boolean.TRUE);
		    	setResultCountString("");
		    	
				String arg = sokArg != null && sokArg.length() > 0 ? sokArg : null;
				
				List<DfuInfoV> dfuindividList = dfuService.searchDfus(arg);
				tblResultat.clearSelection();
				sokResutlatTableModel.setEntities(dfuindividList);
				setResultCountString(dfuindividList != null ? dfuindividList.size() + " treff" : null);
				
				setDfuSearchBusy(Boolean.FALSE);
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				} 
    		}
    	}.start();
    }
    
	private void openSelectedDfu() {
		int selectedRow = sokResultatSelectionModel.getAnchorSelectionIndex();
		selectedRow = tblResultat.convertRowIndexToModel(selectedRow);
		if(selectedRow >= 0 && selectedRow < sokResutlatTableModel.getRowCount()) {
			DfuInfoV dfuInfoV = sokResutlatTableModel.getEntities().get(selectedRow);
			openDfu(dfuInfoV.getId());
		}
    }
    
    @SuppressWarnings("unchecked")
	private void openDfu(final Long dfuId) {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
				if(!kanBytteDfuindivid()) {
					return null;
				}
				
		    	setDfuSelectedBusy(Boolean.TRUE);
				if(dfuId != null) {
					Dfuindivid dfuindivid = dfuService.getDfuindividForAdmin(dfuId);
					setDfuindivid(dfuindivid);
				}
		    	setDfuSelectedBusy(Boolean.FALSE);
		    	
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				} 
    		}
    	}.start();
    }
    
    @SuppressWarnings("unchecked")
	private void openNewDfu() {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
				if(!kanBytteDfuindivid()) {
					return null;
				}
				
				setDfuSelectedBusy(Boolean.TRUE);
				Dfuindivid dfuindivid = dfuService.prepareNewDfuindivid(loggedOnUser);
				setDfuindivid(dfuindivid);
				setLagreEnabled(Boolean.TRUE);
				setDfuSelectedBusy(Boolean.FALSE);
		    	
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				} 
    		}
    	}.start();
    }
    
    private void reopenDfu() {
    	if(!kanBytteDfuindivid()) {
			return;
		}
    	
    	Long dfuId = getDfuindivid() != null ? getDfuindivid().getId() : null;
    	if(dfuId != null) {
    		setDfuindivid(null);
    		openDfu(dfuId);
    	}
    }
    
    /**
     * Returnerer true om lagring gikk bra
     * 
     * @return
     */
    private boolean lagreDfu() {
    	if(getDfuindivid() != null) {
	    	if(getDfuindivid().getSerienummer() == null || getDfuindivid().getSerienummer().length() == 0) {
	    		JOptionPane.showMessageDialog(parentFrame, getGuiProps().getGuiString("message.manglerSerienummer"));
	    		return false;
	    	} else if(getDfuindivid().getVersjon() == null || getDfuindivid().getVersjon().length() == 0) {
	    		JOptionPane.showMessageDialog(parentFrame, getGuiProps().getGuiString("message.manglerVersjon"));
	    		return false;
	    	}
	    	
	    	boolean nyDfu = getDfuindivid().getId() == null;
    		Dfuindivid dfuindivid = dfuService.oppdaterDfuindivid(getDfuindivid(), loggedOnUser);
	    	setLagreEnabled(Boolean.FALSE);
	    	if(nyDfu) {
	    		setDfuindivid(dfuindivid);
	    	} else {
	    		reopenDfu();
	    	}
	    	return true;
    	} else {
    		return true;
    	}
    }
    
    private boolean kanBytteDfuindivid() {
    	boolean kanBytte = true;
    	boolean lagreForBytte = false;
    	
    	if(getDfuindivid() != null) {
    		if(getLagreEnabled()) {
    			int svar = JOptionPane.showConfirmDialog(parentFrame, getGuiProps().getGuiString("message.lagreEndringer"), getGuiProps().getGuiString("message.lagreEndringer.title"), JOptionPane.YES_NO_CANCEL_OPTION);
    			kanBytte = svar == JOptionPane.YES_OPTION || svar == JOptionPane.NO_OPTION;
    			lagreForBytte = svar == JOptionPane.YES_OPTION;
    		} else {
    			kanBytte = true;
    		}
    	}
    	
    	if(lagreForBytte) {
    		kanBytte = lagreDfu();
    	}
    	
    	if(kanBytte) {
    		setLagreEnabled(Boolean.FALSE);
    	}
    	
    	return kanBytte;
    }
    
    public Action getLagreAction(final JButton btnLagre) {
    	getBindingGroup().addBinding(BindingHelper.createbinding(DfuController.this, "lagreEnabled", btnLagre, "enabled", BindingHelper.READ));
    	
    	return new AbstractAction(getGuiProps().getGuiString("button.lagre"), IconResources.SAVE_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				lagreDfu();
			}
    	};
    }
    
    public Action getAvbrytAction(final JButton btnAvbryt) {
    	getBindingGroup().addBinding(BindingHelper.createbinding(DfuController.this, "${dfuindivid != null}", btnAvbryt, "enabled", BindingHelper.READ));
    	
    	return new AbstractAction(getGuiProps().getGuiString("button.avbryt"), IconResources.CANCEL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				reopenDfu();
			}
    	};
    }
    
    public Action getNyDfuAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.nyDfu"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				openNewDfu();
			}
    	};
    }
    
    public Action getSokAction(final JFormattedTextField txtSok) {
    	return new AbstractAction(getGuiProps().getGuiString("button.sok"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!getDfuSearchBusy()) {
					doDfuSearch(txtSok.getText());
				}
			}
    	};
    }
    
    public Action getForsteAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.forste"), IconResources.REMOVE_ALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!kanBytteDfuindivid()) {
					return;
				}
				int currentSelection = sokResultatSelectionModel.getAnchorSelectionIndex();
				if(sokResutlatTableModel.getRowCount() > 0 && currentSelection != 0) {
					sokResultatSelectionModel.clearSelection();
					sokResultatSelectionModel.setSelectionInterval(0, 0);
					openSelectedDfu();
				}
			}
    	};
    }
    
    public Action getForrigeAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.forrige"), IconResources.REMOVE_ONE_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!kanBytteDfuindivid()) {
					return;
				}
				int currentSelection = sokResultatSelectionModel.getAnchorSelectionIndex();
				if(currentSelection > 0 && sokResutlatTableModel.getRowCount() > 1) {
					sokResultatSelectionModel.clearSelection();
					sokResultatSelectionModel.setSelectionInterval(currentSelection-1, currentSelection-1);
					openSelectedDfu();
				}
			}
    	};
    }
    
    public Action getNesteForrigeAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.neste"), IconResources.ADD_ONE_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!kanBytteDfuindivid()) {
					return;
				}
				int currentSelection = sokResultatSelectionModel.getAnchorSelectionIndex();
				if(currentSelection >= 0 && sokResutlatTableModel.getRowCount()-1 > currentSelection) {
					sokResultatSelectionModel.clearSelection();
					sokResultatSelectionModel.setSelectionInterval(currentSelection+1, currentSelection+1);
					openSelectedDfu();
				}
			}
    	};
    }
    
    public Action getSisteAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.siste"), IconResources.ADD_ALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!kanBytteDfuindivid()) {
					return;
				}
				int currentSelection = sokResultatSelectionModel.getAnchorSelectionIndex();
				if(sokResutlatTableModel.getRowCount() > 0 && currentSelection != sokResutlatTableModel.getRowCount()-1) {
					sokResultatSelectionModel.clearSelection();
					sokResultatSelectionModel.setSelectionInterval(sokResutlatTableModel.getRowCount()-1, sokResutlatTableModel.getRowCount()-1);
					openSelectedDfu();
				}
			}
    	};
    }
    
    public Action getResultatTilExcelAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.resultatTilExcel"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				File excelFile = tblResultat.exportToExcel(JMipssBeanTable.EXPORT_TYPE.ALL_ROWS);
				DesktopHelper.openFile(excelFile);
			}
    	};
    }
    
    public Action getVelgDfuAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.velgDfu"), IconResources.DETALJER_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				openSelectedDfu();
			}
    	};
    }
	
    public Action getNyDfuLoggAction(final JTextArea txtArea) {
    	return new AbstractAction(getGuiProps().getGuiString("button.nyDfuLogg"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(getDfuindivid() != null) {
					MipssTextAreaInputDialog inputDialog = new MipssTextAreaInputDialog(parentFrame, getGuiProps().getGuiString("title.nyLogg"));
					String tekst = inputDialog.getInput();
					if(tekst != null) {
						tekst = tekst.length() > 4000 ? tekst.substring(0, 4000) : tekst;
						Dfulogg nyEntry = new Dfulogg();
						nyEntry.setFritekst(tekst);
						nyEntry.setDfuindivid(getDfuindivid());
						nyEntry.setOpprettetAv(loggedOnUser);
						nyEntry.setOpprettetDato(new Timestamp(Calendar.getInstance().getTimeInMillis()));
						getDfuindivid().getDfuloggList().add(0, nyEntry);
						
						String oldText = txtArea.getText();
						oldText = nyEntry.getStringForLoggPanel() + "\n\n" + oldText;
						txtArea.setText(oldText);
						
						setLagreEnabled(Boolean.TRUE);
					}
				}
			}
    	};
    }
    
    public Action getEndreDfuKontraktAction(final MipssBeanTableModel<DfuKontrakt> dfuKontraktModel, final JXTable tblDfuKontraktList) {
    	return new AbstractAction(getGuiProps().getGuiString("button.endreDfuKontrakt"), IconResources.DETALJER_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblDfuKontraktList.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblDfuKontraktList.getRowCount()) {
					DfuKontrakt dfuKontrakt = dfuKontraktModel.getEntities().get(tblDfuKontraktList.convertRowIndexToModel(selectedRow));
					DfuKontraktDialog dfuKontraktDialog = new DfuKontraktDialog(parentFrame, getDriftkontraktList());
					
					dfuKontrakt = dfuKontraktDialog.showDialog(dfuKontrakt, false);
					if(dfuKontrakt != null) {
						setLagreEnabled(Boolean.TRUE);
					}
					dfuKontraktModel.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
				}
			}
    	};
    }
    
    public Action getNyDfuKontraktAction(final MipssBeanTableModel<DfuKontrakt> dfuKontraktModel, final JXTable tblDfuKontraktList) {
    	return new AbstractAction(getGuiProps().getGuiString("button.nyDfuKontrakt"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
					DfuKontraktDialog dfuKontraktDialog = new DfuKontraktDialog(parentFrame, getDriftkontraktList());
					DfuKontrakt dfuKontrakt = new DfuKontrakt();
					dfuKontrakt.setDfuindivid(getDfuindivid());
					
					dfuKontrakt = dfuKontraktDialog.showDialog(dfuKontrakt, true);
					if(dfuKontrakt != null) {
						for(DfuKontrakt oldKnytining : getDfuindivid().getDfuKontraktList()) {
							if(oldKnytining.getKontraktId().equals(dfuKontrakt.getKontraktId())) {
								JOptionPane.showMessageDialog(parentFrame, DfuController.getGuiProps().getGuiString("message.duplikatKontraktknytning"));
								return;
							}
						}
						getDfuindivid().getDfuKontraktList().add(dfuKontrakt);
						dfuKontraktModel.fireTableModelEvent(0, dfuKontraktModel.getRowCount(), TableModelEvent.UPDATE);
						
						setLagreEnabled(Boolean.TRUE);
					}
			}
    	};
    }
    
    public Action getSlettDfuKontraktAction(final MipssBeanTableModel<DfuKontrakt> dfuKontraktModel, final JXTable tblDfuKontraktList) {
    	return new AbstractAction(getGuiProps().getGuiString("button.slettDfuKontrakt"), IconResources.REMOVE_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblDfuKontraktList.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblDfuKontraktList.getRowCount()) {
					DfuKontrakt dfuKontrakt = dfuKontraktModel.getEntities().get(tblDfuKontraktList.convertRowIndexToModel(selectedRow));
					
					if(dfuKontrakt != null) {
						int svar = JOptionPane.showConfirmDialog(parentFrame, DfuController.getGuiProps().getGuiString("message.slettKnytning"), DfuController.getGuiProps().getGuiString("message.slettKnytning.title"), JOptionPane.YES_NO_OPTION);
						if(svar == JOptionPane.YES_OPTION) {
							dfuService.slettDfuKontrakt(dfuKontrakt);
							dfuKontraktModel.getEntities().remove(dfuKontrakt);
							dfuKontraktModel.fireTableModelEvent(0, dfuKontraktModel.getRowCount(), TableModelEvent.UPDATE);
						}
					}
				}
			}
    	};
    }
    
    public Action getNyDfuStatusAction(final MipssBeanTableModel<Dfu_Status> dfuStatusModel) {
    	return new AbstractAction(getGuiProps().getGuiString("button.nyDfuStatus"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(getDfuindivid() != null) {
					DfuStatusDialog dfuStatusDialog = new DfuStatusDialog(parentFrame, dfuService.getDfustatusList());
					Dfu_Status dfu_Status = dfuStatusDialog.showDialog(getDfuindivid(), loggedOnUser);
					if(dfu_Status != null) {
						getDfuindivid().getDfu_StatusList().add(0, dfu_Status);
						dfuStatusModel.fireTableModelEvent(0, dfuStatusModel.getRowCount(), TableModelEvent.UPDATE);
						
						setLagreEnabled(Boolean.TRUE);
					}
				}
			}
    	};
    }
    
	public JXTable getTblResultat() {
		return tblResultat;
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setLagreEnabled(Boolean.TRUE);
	}

	public Boolean getLagreEnabled() {
		return lagreEnabled;
	}

	public void setLagreEnabled(Boolean lagreEnabled) {
		Boolean old = this.lagreEnabled;
		this.lagreEnabled = lagreEnabled;
		
		props.firePropertyChange("lagreEnabled", old, lagreEnabled);
	}
}
