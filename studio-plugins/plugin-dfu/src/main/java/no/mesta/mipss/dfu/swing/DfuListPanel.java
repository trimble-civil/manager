package no.mesta.mipss.dfu.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

public class DfuListPanel extends JPanel {
	private DfuController dfuController;
	
	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneDfu = new JScrollPane();
	private JXTable tblDfu = new JXTable();
	private JButton btnTilExcel = new JButton();
	private JButton btnVelgDfu = new JButton();
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private JLabel lblResultCount = new JLabel();

	public DfuListPanel(DfuController dfuController) {
		this.dfuController = dfuController;
		
		initButtons();
		initTable();
		initLabels();
		initGui();
		initBindings();
	}
	
	public void setBusy(Boolean busy) {
		if(busy.booleanValue()) {
			busyLabel.setBusy(true);
			scrlPaneDfu.setViewportView(busyLabel);
			btnTilExcel.setEnabled(false);
			btnVelgDfu.setEnabled(false);
		} else {
			busyLabel.setBusy(false);
			scrlPaneDfu.setViewportView(tblDfu);
			btnTilExcel.setEnabled(true);
			btnVelgDfu.setEnabled(true);
		}
	}
	
	private void initBindings() {
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuSearchBusy", this, "busy", BindingHelper.READ));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "resultCountString", lblResultCount, "text", BindingHelper.READ));
	}
	
	private void initTable() {
		tblDfu = dfuController.getTblResultat();
	}
	
	private void initButtons() {
		btnTilExcel.setAction(dfuController.getResultatTilExcelAction());
		btnVelgDfu.setAction(dfuController.getVelgDfuAction());
	}
	
	private void initLabels() {
		busyLabel.setText(DfuController.getGuiProps().getGuiString("label.dfuSearchBusy"));
	}
	
	private void initGui() {
		scrlPaneDfu.setViewportView(tblDfu);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneDfu, new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(lblResultCount, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 0, 0, 0), 0, 0));
		pnlMain.add(btnVelgDfu, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(11, 0, 0, 5), 0, 0));
		pnlMain.add(btnTilExcel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(11, 0, 0, 0), 0, 0));
	
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
