package no.mesta.mipss.dfu.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfulogg;

public class DfuLoggPanel extends JPanel {
	private DfuController dfuController;

	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneLogg = new JScrollPane();
	private JTextArea txtAreaLogg = new JTextArea();
	private JButton btnNyLogg = new JButton();
	
	public DfuLoggPanel(DfuController dfuController) {
		this.dfuController = dfuController;
	
		initTextFields();
		initButtons();
		initBindings();
		initGui();
	}
	
	public void setDfuindivid(Dfuindivid dfuindivid) {
		StringBuffer sb = new StringBuffer();
		if(dfuindivid != null && dfuindivid.getDfuloggList() != null) {
			List<Dfulogg> tmpList = new ArrayList<Dfulogg>(dfuindivid.getDfuloggList());
			Collections.sort(tmpList, new Comparator<Dfulogg>() {
				@Override
				public int compare(Dfulogg o1, Dfulogg o2) {
					return o1.getOpprettetDato().compareTo(o2.getOpprettetDato()) * -1;
				}
			});
			for(Dfulogg dfulogg : tmpList) {
				sb.append(dfulogg.getStringForLoggPanel());
				sb.append("\n\n");
			}
		}
		txtAreaLogg.setText(sb.toString());
	}
	
	private void initBindings() {
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuindivid", this, "dfuindivid", BindingHelper.READ));
	}
	
	private void initTextFields() {
		txtAreaLogg.setLineWrap(true);
		txtAreaLogg.setWrapStyleWord(true);
		txtAreaLogg.setEditable(false);
	}
	
	private void initButtons() {
		btnNyLogg.setAction(dfuController.getNyDfuLoggAction(txtAreaLogg));
	}
	
	private void initGui() {
		scrlPaneLogg.setViewportView(txtAreaLogg);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneLogg, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(btnNyLogg, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
