package no.mesta.mipss.dfu.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

public class DfuKontraktPanel extends JPanel {
	private DfuController dfuController;

	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneDfuKontrakt = new JScrollPane();
	private JMipssBeanTable<DfuKontrakt> tblDfuKontrakt;
	private JButton btnEndreDfuKontrakt = new JButton();
	private JButton btnNyDfuKontrakt = new JButton();
	private JButton btnSlettDfuKontrakt = new JButton();
	private MipssBeanTableModel<DfuKontrakt> dfuKontraktModel;
	
	public DfuKontraktPanel(DfuController dfuController) {
		this.dfuController = dfuController;
		
		initTables();
		initButtons();
		initGui();
	}
	
	private void initTables() {
		dfuKontraktModel = new MipssBeanTableModel<DfuKontrakt>("dfuKontraktList", DfuKontrakt.class, "driftkontrakt", "navn", "fraDato", "tilDato");
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuindivid", dfuKontraktModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelDfuKontrakt = new MipssRenderableEntityTableColumnModel(DfuKontrakt.class, dfuKontraktModel.getColumns());

		tblDfuKontrakt = new JMipssBeanTable<DfuKontrakt>(dfuKontraktModel, columnModelDfuKontrakt);
		tblDfuKontrakt.setDefaultRenderer(Driftkontrakt.class, new RenderableMipssEntityTableCellRenderer<Driftkontrakt>());
		
		tblDfuKontrakt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblDfuKontrakt.setDoWidthHacks(false);
		tblDfuKontrakt.addMouseListener(new MouseAdapter() {
			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && tblDfuKontrakt.rowAtPoint(e.getPoint()) != -1) {
					if (e.getClickCount() == 2) {
						dfuController.getEndreDfuKontraktAction(dfuKontraktModel, tblDfuKontrakt).actionPerformed(null);
					}
				}
			}
		});
	}
	
	private void initButtons() {
		btnEndreDfuKontrakt.setAction(dfuController.getEndreDfuKontraktAction(dfuKontraktModel, tblDfuKontrakt));
		btnNyDfuKontrakt.setAction(dfuController.getNyDfuKontraktAction(dfuKontraktModel, tblDfuKontrakt));
		btnSlettDfuKontrakt.setAction(dfuController.getSlettDfuKontraktAction(dfuKontraktModel, tblDfuKontrakt));
	}
	
	private void initGui() {
		scrlPaneDfuKontrakt.setViewportView(tblDfuKontrakt);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneDfuKontrakt, new GridBagConstraints(0, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(btnNyDfuKontrakt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnEndreDfuKontrakt, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnSlettDfuKontrakt, new GridBagConstraints(1, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
