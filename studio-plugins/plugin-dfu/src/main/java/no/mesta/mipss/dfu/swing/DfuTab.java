package no.mesta.mipss.dfu.swing;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXBusyLabel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;

public class DfuTab extends JPanel {
	private static final String BUSY_KEY = "busy";
	private static final String NORMAL_KEY = "normal";
	
	private DfuController dfuController;
	
	private CardLayout cardLayout = new CardLayout();
	private JPanel dfuPanel;
	private JPanel pnlBusy = new JPanel();
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private JPanel pnlMain = new JPanel();
	
	private JPanel pnlButtons = new JPanel();
	private JButton btnLagre = new JButton();
	private JButton btnAvbryt = new JButton();
	
	public DfuTab(DfuController dfuController, JPanel dfuPanel) {
		this.dfuController = dfuController;
		this.dfuPanel = dfuPanel;
		
		initLabels();
		initButtons();
		initGui();
		initBindings();
	}

	public void setBusy(Boolean busy) {
		busyLabel.setBusy(busy);
		
		if(busy) {
			cardLayout.show(pnlMain, BUSY_KEY);
		} else {
			cardLayout.show(pnlMain, NORMAL_KEY);
		}
	}
	
	private void initBindings() {
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuSelectedBusy", this, "busy", BindingHelper.READ));
	}
	
	private void initLabels() {
		busyLabel.setText(DfuController.getGuiProps().getGuiString("label.lasterDfuindivid"));
	}
	
	private void initButtons() {
		btnLagre.setAction(dfuController.getLagreAction(btnLagre));
		btnAvbryt.setAction(dfuController.getAvbrytAction(btnAvbryt));
	}
	
	private void initGui() {
		pnlBusy.setLayout(new GridBagLayout());
		pnlBusy.add(busyLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlMain.setLayout(cardLayout);
		pnlMain.add(dfuPanel, NORMAL_KEY);
		pnlMain.add(pnlBusy, BUSY_KEY);
		cardLayout.show(pnlMain, NORMAL_KEY);
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}
}
