package no.mesta.mipss.dfu.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.datafangsutstyr.Dfu_Status;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.swingx.JXDatePicker;

public class DfuMainPanel extends JPanel {
	private DfuController dfuController;
	
	private JPanel pnlMain = new JPanel();
	private JPanel pnlTop = new JPanel();
	private JPanel pnlMiddleTop = new JPanel();
	private JPanel pnlLeftTop = new JPanel();
	private JPanel pnlRightTop = new JPanel();
	private JLabel lblFritekst = new JLabel();
	private JScrollPane scrlPaneFritekst = new JScrollPane();
	private JTextArea txtAreaFritekst = new JTextArea();
	private JLabel lblDfuId = new JLabel();
	private JFormattedTextField txtDfuId = new JFormattedTextField();
	private JLabel lblKategori = new JLabel();
	private JFormattedTextField txtKategori = new JFormattedTextField();
	private JLabel lblDfumodel = new JLabel();
	private JComboBox cbxDfumodel = new JComboBox();
	private JLabel lblSerienummer = new JLabel();
	private JFormattedTextField txtSerienummer = new JFormattedTextField();
	private JLabel lblVersjon = new JLabel();
	private JFormattedTextField txtVersjon = new JFormattedTextField();
	private JLabel lblTelefonnummer = new JLabel();
	private JFormattedTextField txtTelefonnummer = new JFormattedTextField();
	private JLabel lblStatus = new JLabel();
	private JLabel lblSisteLivstegn = new JLabel();
	private JXDatePicker datePckrSisteLivstegn = new JXDatePicker();
	private JButton btnNyStatus = new JButton();
	private JLabel lblSimkortnummer = new JLabel();
	private JFormattedTextField txtSimkortnummer = new JFormattedTextField();
	private JLabel lblAnskaffetDato = new JLabel();
	private JXDatePicker datePckrAnskaffetDato = new JXDatePicker();
	private JLabel lblSlettetFlagg = new JLabel();
	private JCheckBox chkSlettetFlagg = new JCheckBox();
	private JLabel lblProgramvareversjon = new JLabel();
	private JFormattedTextField txtProgramvareversjon  = new JFormattedTextField();
	
	private JPanel pnlStatus = new JPanel();
	private JScrollPane scrlPaneStatus = new JScrollPane();
	private JMipssBeanTable<Dfu_Status> tblDfuStatus;
	private MipssBeanTableModel<Dfu_Status> dfuStatusTableModel;
	
	public DfuMainPanel(DfuController dfuController) {
		this.dfuController = dfuController;
		
		initComboboxes();
		initTables();
		initLabels();
		initTextFields();
		initButtons();
		initBindings();
		initGui();
	}
	
	@SuppressWarnings("unchecked")
	private void initBindings() {
		LongStringConverter longStringConverter = new LongStringConverter();
		Binding tlfBinding = BindingHelper.createbinding(dfuController, "${dfuindivid.tlfnummer}", txtTelefonnummer, "text", BindingHelper.READ_WRITE);
		tlfBinding.setConverter(longStringConverter);
		dfuController.getBindingGroup().addBinding(tlfBinding);
		
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.simkortnummer}", txtSimkortnummer, "text", BindingHelper.READ_WRITE));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.id}", txtDfuId, "text", BindingHelper.READ));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.dfumodell.dfukategori.navn}", txtKategori, "text", BindingHelper.READ));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.serienummer}", txtSerienummer, "text", BindingHelper.READ_WRITE));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.versjon}", txtVersjon, "text", BindingHelper.READ_WRITE));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.dfuinfo.sisteLivstegnDato}", datePckrSisteLivstegn, "date", BindingHelper.READ));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.fritekst}", txtAreaFritekst, "text", BindingHelper.READ_WRITE));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.programvareversjon}", txtProgramvareversjon, "text", BindingHelper.READ));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.anskaffetDato}", datePckrAnskaffetDato, "date", BindingHelper.READ_WRITE));
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.slettet}", chkSlettetFlagg, "selected", BindingHelper.READ_WRITE));
	}
	
	@SuppressWarnings("unchecked")
	private void initComboboxes() {
		List<Dfumodell> dfumodellTyper = dfuController.getDfumodellList();

		cbxDfumodel.setRenderer((ListCellRenderer)new MipssListCellRenderer(DfuController.getGuiProps().getGuiString("cbx.text.ingenModel")));
        JComboBoxBinding modellCBBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ, dfumodellTyper, cbxDfumodel);
        dfuController.getBindingGroup().addBinding(modellCBBinding);
        dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid.dfumodell}", cbxDfumodel, "selectedItem", BindingHelper.READ_WRITE));       
        dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "${dfuindivid != null}", cbxDfumodel, "enabled", BindingHelper.READ));       
        dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(cbxDfumodel, "selectedItem", this, "repaint", BindingHelper.READ));	
	}
	
	public void setRepaint(Dfumodell dfumodell) {
		cbxDfumodel.repaint();
	}
	
	private void initButtons() {
		btnNyStatus.setAction(dfuController.getNyDfuStatusAction(dfuStatusTableModel));
	}
	
	private void initTextFields() {
		txtAreaFritekst.setLineWrap(true);
		txtAreaFritekst.setWrapStyleWord(true);
		
		txtDfuId.setPreferredSize(new Dimension(150, 19));
		txtKategori.setPreferredSize(new Dimension(150, 19));
		cbxDfumodel.setPreferredSize(new Dimension(150, 19));
		txtSerienummer.setPreferredSize(new Dimension(150, 19));
		txtVersjon.setPreferredSize(new Dimension(150, 19));
		txtTelefonnummer.setPreferredSize(new Dimension(150, 19));
		datePckrSisteLivstegn.setPreferredSize(new Dimension(150, 19));
		txtSimkortnummer.setPreferredSize(new Dimension(150, 19));
		datePckrAnskaffetDato.setPreferredSize(new Dimension(150, 19));
		txtProgramvareversjon.setPreferredSize(new Dimension(150, 19));
		
		txtDfuId.setEditable(false);
		txtKategori.setEditable(false);
		datePckrSisteLivstegn.setEditable(false);
		txtProgramvareversjon.setEnabled(false);
		
		txtVersjon.setDocument(new DocumentFilter(20, null, false));
		txtSerienummer.setDocument(new DocumentFilter(25, null, false));
		txtTelefonnummer.setDocument(new DocumentFilter(12));
		txtSimkortnummer.setDocument(new DocumentFilter(100));
	}
	
	private void initLabels() {
		lblDfuId.setText(DfuController.getGuiProps().getGuiString("label.dfuId"));
		lblFritekst.setText(DfuController.getGuiProps().getGuiString("label.dfuFritekst"));
		lblKategori.setText(DfuController.getGuiProps().getGuiString("label.dfuKategori"));
		lblDfumodel.setText(DfuController.getGuiProps().getGuiString("label.dfuModel"));
		lblSerienummer.setText(DfuController.getGuiProps().getGuiString("label.dfuSerienummer"));
		lblVersjon.setText(DfuController.getGuiProps().getGuiString("label.dfuVersjon"));
		lblTelefonnummer.setText(DfuController.getGuiProps().getGuiString("label.dfuTelefonnummer"));
		lblStatus.setText(DfuController.getGuiProps().getGuiString("label.dfuStatus"));
		lblSisteLivstegn.setText(DfuController.getGuiProps().getGuiString("label.dfuSisteLivstegn"));
		lblSimkortnummer.setText(DfuController.getGuiProps().getGuiString("label.simkortnummer"));
		lblAnskaffetDato.setText(DfuController.getGuiProps().getGuiString("label.anskaffetDato"));
		lblSlettetFlagg.setText(DfuController.getGuiProps().getGuiString("label.slettetFlagg"));
		lblProgramvareversjon.setText(DfuController.getGuiProps().getGuiString("label.programvareversjon"));
	}
	
	private void initTables() {
		dfuStatusTableModel = new MipssBeanTableModel<Dfu_Status>("dfu_StatusList", Dfu_Status.class, "dfustatus", "opprettetDato", "opprettetAv");
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuindivid", dfuStatusTableModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelDfuInstallasjon = new MipssRenderableEntityTableColumnModel(Dfu_Status.class, dfuStatusTableModel.getColumns());

		tblDfuStatus = new JMipssBeanTable<Dfu_Status>(dfuStatusTableModel, columnModelDfuInstallasjon);
		tblDfuStatus.setDefaultRenderer(Dfustatus.class, new RenderableMipssEntityTableCellRenderer<Dfustatus>());
		
		tblDfuStatus.setDoWidthHacks(false);
		tblDfuStatus.setPreferredScrollableViewportSize(new Dimension(200, 0));
	}
	
	private void initGui() {
		pnlLeftTop.setLayout(new GridBagLayout());
		pnlLeftTop.add(lblDfuId, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeftTop.add(txtDfuId, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlLeftTop.add(lblKategori, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeftTop.add(txtKategori, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlLeftTop.add(lblDfumodel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeftTop.add(cbxDfumodel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlLeftTop.add(lblProgramvareversjon, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeftTop.add(txtProgramvareversjon, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlLeftTop.add(lblSisteLivstegn, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlLeftTop.add(datePckrSisteLivstegn, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		pnlMiddleTop.setLayout(new GridBagLayout());
		pnlMiddleTop.add(lblSerienummer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMiddleTop.add(txtSerienummer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMiddleTop.add(lblTelefonnummer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMiddleTop.add(txtTelefonnummer, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMiddleTop.add(lblSimkortnummer, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMiddleTop.add(txtSimkortnummer, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMiddleTop.add(lblVersjon, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMiddleTop.add(txtVersjon, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlMiddleTop.add(lblAnskaffetDato, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMiddleTop.add(datePckrAnskaffetDato, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlRightTop.setLayout(new GridBagLayout());
		pnlRightTop.add(lblSlettetFlagg, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlRightTop.add(chkSlettetFlagg, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));

		pnlTop.setLayout(new GridBagLayout());
		pnlTop.add(pnlLeftTop, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 11), 0, 0));
		pnlTop.add(pnlMiddleTop, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 11), 0, 0));
		pnlTop.add(pnlRightTop, new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		scrlPaneFritekst.setViewportView(txtAreaFritekst);		
		scrlPaneStatus.setViewportView(tblDfuStatus);
		
		pnlStatus.setLayout(new GridBagLayout());
		pnlStatus.add(lblStatus, new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlStatus.add(scrlPaneStatus, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlStatus.add(btnNyStatus, new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(pnlTop, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 11, 11), 0, 0));
		pnlMain.add(lblFritekst, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 11), 0, 0));
		pnlMain.add(scrlPaneFritekst, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 11), 0, 0));
		pnlMain.add(pnlStatus, new GridBagConstraints(1, 0, 1, 3, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
