package no.mesta.mipss.dfu.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;

public class SearchPanel extends JPanel implements PropertyChangeListener {
	private DfuController dfuController;
	
	private JPanel pnlMain = new JPanel();
	private JButton btnNyDfu = new JButton();
	private JButton btnNeste = new JButton();
	private JButton btnForrige = new JButton();
	private JButton btnForste = new JButton();
	private JButton btnSiste = new JButton();
	private JButton btnSok = new JButton();
	private JFormattedTextField txtSok = new JFormattedTextField();
	private JLabel lblValgtDfu = new JLabel();
	
	public SearchPanel(DfuController dfuController) {
		this.dfuController = dfuController;
		
		initButtons();
		initTextFields();
		initGui();
		initBindings();
		
		dfuController.addPropertyChangeListener("dfuindivid", this);
	}
	
	public void setBusy(Boolean busy) {
		btnSok.setEnabled(!busy.booleanValue());
		btnNeste.setEnabled(!busy.booleanValue());
		btnForrige.setEnabled(!busy.booleanValue());
		btnForste.setEnabled(!busy.booleanValue());
		btnSiste.setEnabled(!busy.booleanValue());
		txtSok.setEditable(!busy.booleanValue());
	}
	
	private void initBindings() {
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuSearchBusy", this, "busy", BindingHelper.READ));
	}
	
	private void initButtons() {
		btnNyDfu.setAction(dfuController.getNyDfuAction());
		btnNeste.setAction(dfuController.getNesteForrigeAction());
		btnForrige.setAction(dfuController.getForrigeAction());
		btnForste.setAction(dfuController.getForsteAction());
		btnSiste.setAction(dfuController.getSisteAction());
		btnSok.setAction(dfuController.getSokAction(txtSok));
	}
	
	private void initTextFields() {
		txtSok.setPreferredSize(new Dimension(150, 19));
		txtSok.setMinimumSize(new Dimension(150, 19));
		
		txtSok.setAction(dfuController.getSokAction(txtSok));
	}
	
	private void initGui() {
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(btnNyDfu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 11), 0, 0));
		pnlMain.add(txtSok, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(btnSok, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(btnForste, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(btnForrige, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(btnNeste, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(btnSiste, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(lblValgtDfu, new GridBagConstraints(7, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
	
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(evt.getPropertyName().equals("dfuindivid")) {
			if(evt.getNewValue() == null) {
				lblValgtDfu.setText("");
			} else {
				Dfuindivid dfuindivid = (Dfuindivid)evt.getNewValue();
				if(dfuindivid.getId() == null) {
					lblValgtDfu.setText("<html><b>Nytt utstyr</b></html>");
				} else {
					lblValgtDfu.setText("<html><b>ID:</b>" + dfuindivid.getId() + " <b>Mod:</b>" + dfuindivid.getModellNavn() + " <b>Tlf:</b>" + dfuindivid.getTlfnummerString() + " <b>S/N:</b>" + dfuindivid.getSerienummer() + "</html>");
				}
			}
		}
	}
}
