package no.mesta.mipss.dfu.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

public class DfuInstallasjonPanel extends JPanel {
	private DfuController dfuController;

	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneDfuInstallasjon = new JScrollPane();
	private JMipssBeanTable<Installasjon> tblDfuInstallasjon;
	private MipssBeanTableModel<Installasjon> dfuInstallasjonModel;

	public DfuInstallasjonPanel(DfuController dfuController) {
		this.dfuController = dfuController;
		initTables();
		initGui();
	}
	
	private void initTables() {
		dfuInstallasjonModel = new MipssBeanTableModel<Installasjon>("installasjonList", Installasjon.class, "kjoretoy", "aktivFraDato", "aktivTilDato", "ignorerMaske");
		dfuController.getBindingGroup().addBinding(BindingHelper.createbinding(dfuController, "dfuindivid", dfuInstallasjonModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelDfuInstallasjon = new MipssRenderableEntityTableColumnModel(Installasjon.class, dfuInstallasjonModel.getColumns());

		tblDfuInstallasjon = new JMipssBeanTable<Installasjon>(dfuInstallasjonModel, columnModelDfuInstallasjon);
		tblDfuInstallasjon.setDefaultRenderer(Kjoretoy.class, new RenderableMipssEntityTableCellRenderer<Kjoretoy>());
		
		tblDfuInstallasjon.setDoWidthHacks(false);
	}
	
	private void initGui() {
		scrlPaneDfuInstallasjon.setViewportView(tblDfuInstallasjon);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneDfuInstallasjon, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
