package no.mesta.mipss.dfu.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.Dfu_Status;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfustatus;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

public class DfuStatusDialog extends JDialog {
	private List<Dfustatus> dfuStatusList;
	private boolean isOk;
	private MipssComboBoxModel<Dfustatus> cbxModel;
	
	private JPanel pnlMain = new JPanel();
	private JLabel lblDfuStatus = new JLabel();
	private JComboBox cbxDfuStatus = new JComboBox();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	
	public DfuStatusDialog(JFrame parentFrame, List<Dfustatus> dfuStatusList) {
		super(parentFrame, DfuController.getGuiProps().getGuiString("title.dfuStatus"), true);
		this.dfuStatusList = dfuStatusList;
		Collections.sort(this.dfuStatusList, new Comparator<Dfustatus>() {
			@Override
			public int compare(Dfustatus o1, Dfustatus o2) {
				return o1.getNavn().compareTo(o2.getNavn());
			}
		});
	}
	
	public Dfu_Status showDialog(Dfuindivid dfuindivid, String bruker) {
		initLabels();
		initComboboxes();
		initButtons();
		initGui();
		
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		
		if(isOk) {
			Dfu_Status status = new Dfu_Status();
			status.setDfuindivid(dfuindivid);
			status.setDfustatus(cbxModel.getSelectedItem());
			status.setOpprettetDato(Clock.now());
			status.setOpprettetAv(bruker);
			return status;
		} else {
			return null;
		}
	}
	
	private void initLabels() {
		lblDfuStatus.setText(DfuController.getGuiProps().getGuiString("label.dfuStatus"));
	}
	
	private void initComboboxes() {
		cbxModel = new MipssComboBoxModel<Dfustatus>(dfuStatusList, true);
		cbxDfuStatus.setModel(cbxModel);
		cbxDfuStatus.setRenderer(new MipssListCellRenderer<Dfustatus>());
		
		cbxDfuStatus.setPreferredSize(new Dimension(150, 19));
	}
	
	private void initButtons() {
		btnOk.setText(DfuController.getGuiProps().getGuiString("button.ok"));
		btnAvbryt.setText(DfuController.getGuiProps().getGuiString("button.avbryt"));
		
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isOk = true;
				dispose();
			}
		});
		
		btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isOk = false;
				dispose();
			}
		});
	}
	
	private void initGui() {
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblDfuStatus, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(cbxDfuStatus, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));

	}
}
