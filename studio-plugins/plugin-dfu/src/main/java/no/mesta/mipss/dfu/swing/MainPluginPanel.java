package no.mesta.mipss.dfu.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.dfu.DfuController;
import no.mesta.mipss.resources.images.IconResources;

public class MainPluginPanel extends JPanel implements PropertyChangeListener {
	private DfuController dfuController;
	
	private JPanel pnlMain = new JPanel();
	private DfuListPanel dfuListPanel;
	private SearchPanel searchPanel;
	private DfuTab dfuMainPanel;
	private DfuTab dfuLoggPanel;
	private DfuTab dfuKontraktPanel;
	private DfuTab dfuInstallasjonPanel;
	private JTabbedPane tbdPaneDfu = new JTabbedPane();
	
	public MainPluginPanel(DfuController dfuController) {
		this.dfuController = dfuController;
		
		dfuController.addPropertyChangeListener(this);
		
		initGui();
	}
	
	private void initGui() {
		dfuListPanel = new DfuListPanel(dfuController);
		searchPanel = new SearchPanel(dfuController);
		dfuMainPanel = new DfuTab(dfuController, new DfuMainPanel(dfuController));
		dfuLoggPanel = new DfuTab(dfuController, new DfuLoggPanel(dfuController));
		dfuKontraktPanel = new DfuTab(dfuController, new DfuKontraktPanel(dfuController));
		dfuInstallasjonPanel = new DfuTab(dfuController, new DfuInstallasjonPanel(dfuController));
		
		tbdPaneDfu.addTab(DfuController.getGuiProps().getGuiString("tab.title.resultat"), IconResources.SEARCH_ICON, dfuListPanel);
		tbdPaneDfu.addTab(DfuController.getGuiProps().getGuiString("tab.title.dfuMain"), IconResources.DETALJER_SMALL_ICON, dfuMainPanel);
		tbdPaneDfu.addTab(DfuController.getGuiProps().getGuiString("tab.title.dfuLogg"), IconResources.EDIT_ICON, dfuLoggPanel);
		tbdPaneDfu.addTab(DfuController.getGuiProps().getGuiString("tab.title.dfuKontrakt"), IconResources.HISTORY_ICON, dfuKontraktPanel);
		tbdPaneDfu.addTab(DfuController.getGuiProps().getGuiString("tab.title.dfuInstallasjon"), IconResources.BIL_SMALL_ICON, dfuInstallasjonPanel);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(searchPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
		pnlMain.add(tbdPaneDfu, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 0, 0, 0), 0, 0));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(evt.getPropertyName().equals("dfuSearchBusy")) {
			tbdPaneDfu.setSelectedComponent(dfuListPanel);
		} else if(evt.getPropertyName().equals("dfuSelectedBusy")) {
			if(tbdPaneDfu.getSelectedComponent().equals(dfuListPanel)) {
				tbdPaneDfu.setSelectedComponent(dfuMainPanel);
			}
		}
	}
}