import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.webservice.ReflinkStrekning;

import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FeatureStore;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;


public class LagShape {

	public void lagShape(List<ReflinkStrekning> rs) throws Exception{
	    final SimpleFeatureType TYPE = createFeatureType(); 
	    FeatureCollection<SimpleFeatureType, SimpleFeature> collection = FeatureCollections.newCollection();
	    GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);
	    
	    for (ReflinkStrekning r:rs){
		    List<Coordinate> cl = new ArrayList<Coordinate>();
			for (java.awt.Point p:r.getVectors()){
				CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");	
				CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32632");
					
				MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
				// Transform the long/lat coordinate to the target map CRS
				Coordinate targetCoord = JTS.transform(new Coordinate(p.getX(), p.getY()), new Coordinate(), transform);
//					Coordinate targetCoord = new Coordinate(p.getX(), p.getY());
				System.out.println(targetCoord.x+" "+targetCoord.y);
				cl.add(targetCoord);
			}
			
			Coordinate[] ca = cl.toArray(new Coordinate[]{});
			LineString line = geometryFactory.createLineString(ca);
			
			featureBuilder.add(line);
			featureBuilder.add("Tomkjøring");
			SimpleFeature feature = featureBuilder.buildFeature(null);
	        collection.add(feature);
	    }
	    
	    File newFile = new File("D:/ms/h6/mipssstr.shp");//getNewShapeFile(new File(""));
	    
	    DataStoreFactorySpi dataStoreFactory = new ShapefileDataStoreFactory();
		
	    Map<String, Serializable> params = new HashMap<String, Serializable>();
	    params.put("url", newFile.toURI().toURL());
	    params.put("create spatial index", Boolean.TRUE);
	
	    ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
	    newDataStore.createSchema(TYPE);
//	    newDataStore.forceSchemaCRS(DefaultGeographicCRS.WGS84);
	    CoordinateReferenceSystem crs = CRS.decode("EPSG:32632");
	    newDataStore.forceSchemaCRS(crs);

	    /*
	     * Write the features to the shapefile
	     */
	    Transaction transaction = new DefaultTransaction("create");
	
	    String typeName = newDataStore.getTypeNames()[0];
	    FeatureStore<SimpleFeatureType, SimpleFeature> featureStore =(FeatureStore<SimpleFeatureType, SimpleFeature>) newDataStore.getFeatureSource(typeName);
	
	    featureStore.setTransaction(transaction);
	    try {
	        featureStore.addFeatures(collection);
	        transaction.commit();
	
	    } catch (Exception problem) {
	        problem.printStackTrace();
	        transaction.rollback();
	
	    } finally {
	        transaction.close();
	    }
	
	    System.exit(0); 
	}
    /**
     * Here is how you can use a SimpleFeatureType builder to create the schema
     * for your shapefile dynamically.
     * <p>
     * This method is an improvement on the code used in the main method above
     * (where we used DataUtilities.createFeatureType) because we can set a
     * Coordinate Reference System for the FeatureType and a a maximum field
     * length for the 'name' field
     * dddd
     */
    private static SimpleFeatureType createFeatureType() {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        //builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system
		try {
			CoordinateReferenceSystem crs= CRS.decode("EPSG:32633");
			builder.setCRS(crs);
		} catch (NoSuchAuthorityCodeException e) {
			e.printStackTrace();
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	    

        // add attributes in order
        builder.add("Location", LineString.class);
        builder.length(15).add("Name", String.class); // <- 15 chars width for name field

        // build the type
        final SimpleFeatureType LOCATION = builder.buildFeatureType();
        
        return LOCATION;
    }
}
