package no.mesta.mipss.mapplugin.utm;

import java.awt.geom.Point2D;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.UTMWMSService;
import no.mesta.mipss.mapplugin.Resolutions;

import org.jdesktop.swingx.mapviewer.TileFactoryInfo;

/**
 * TileFactoryInfo the encapsulates all information related to the 
 * UTM tilecache server.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class UTMTileFactoryInfo extends TileFactoryInfo {

	private float transparency = 1f;
    

	/**
     * 
     */
    private Resolutions resolutions;
    /**
     * 
     */
    private Bounds bounds;
    
    private String tilesOrigin;
    /**
     * 
     */
    private UTMWMSService wms;
	
     /**
     * Creates a new instance of TileFactoryInfo. Note that TileFactoryInfo
     * should be considered invariate, meaning that subclasses should
     * ensure all of the properties stay the same after the class is
     * constructed. Returning different values of getTileSize() for example
     * is considered an error and may result in unexpected behavior.
     * 
     * @param tileSize the size of the tiles in pixels (must be square)
     * @param xr2l if the x goes r to l (is this backwards?)
     * @param yt2b if the y goes top to bottom
     * @param baseURL the base url for grabbing tiles
     * @param xparam the x parameter for the tile url
     * @param yparam the y parameter for the tile url
     * @param zparam the z parameter for the tile url
     */
    public UTMTileFactoryInfo(int tileSize, boolean xr2l, boolean yt2b,
                    String baseURL, String xparam, String yparam, String zparam,
                    Resolutions resolutions, Bounds bounds, String tilesOrigin, UTMWMSService wms) {
                    
        super(resolutions.getMinResolutionIndex(), resolutions.getMaxResolutionIndex(), resolutions.getTotalResolutions(), tileSize, xr2l, yt2b,
                        baseURL, xparam, yparam, zparam);
        this.resolutions = resolutions;
        this.bounds = bounds;
        this.tilesOrigin = tilesOrigin;
        this.wms = wms;
        wms.setInfo(this);
    }
	
    /**
     * Returns the tiles url for a tile with the specified indices and zoomlevel,
     * the returned url will be created from the @see CentosTileCacheWMSService class
     * @param x the tiles x index
     * @param y the tiles y index
     * @param zoom current zoomlevel
     * @return a URL to the tile
     */
    public String getTileUrl(int x, int y, int zoom) {
        double r = resolutions.getResolutionForZoom(zoom);
        double metersPrTile = r*getTileSize(zoom);    
        int tilesHigh = (int)Math.floor(((Math.abs(bounds.getUry())-Math.abs(bounds.getLly()))/metersPrTile));
        int z = tilesHigh-y;
        return wms.toWMSURL(x, z, zoom, getTileSize(zoom));
    }
	
    /**
     * Return the maps width in tiles at the specified zoom level
     * the width is calculated based on the resolution for the zoom level
     * @param zoom the zoom level to get the width for
     * @return the maps width in tiles
     */
    public int getMapWidthInTilesAtZoom(int zoom) {
        double r = resolutions.getResolutionForZoom(zoom);
        double metersPrTile = r*getTileSize(zoom);    
        int tilesWide = (int)Math.ceil(((Math.abs(bounds.getUrx())+Math.abs(bounds.getLlx()))/metersPrTile));
        return tilesWide;
    }
    /**
     * Return the maps height in tiles at the specified zoom level
     * the height is calculated based on the resolution for the zoom level
     * @param zoom the zoom level to get the height for
     * @return the maps height in tiles
     */
    public int getMapHeightInTilesAtZoom(int zoom) {
        double r = resolutions.getResolutionForZoom(zoom);
        double metersPrTile = r*getTileSize(zoom);    
        int tilesHigh = (int)Math.ceil(((Math.abs(bounds.getUry())-Math.abs(bounds.getLly()))/metersPrTile));
        return tilesHigh;
    }
	
	
    /**
     * Get the maps center position in pixels at the specified zoom level
     * @param zoom the current zoom level
     * @return Point2D the current center point
     */
    public Point2D getMapCenterInPixelsAtZoom(int zoom) {
    	int width = (getMapWidthInTilesAtZoom(zoom)*getTileSize(zoom))/2;
        int height = (getMapHeightInTilesAtZoom(zoom)*getTileSize(zoom))/2;
    	Point2D center = new Point2D.Double(width, height);
    	return center;
    }
    
    /**
     * Get the resolutions for the map
     * @return
     */
    public Resolutions getResolutions() {
        return resolutions;
    }
	
    /**
     * Get the maps boundaries
     * @return
     */
    public Bounds getBounds() {
        return bounds;
    }
    
    public String getTilesOrigin(){
        return tilesOrigin;
    }
    
    /**
	 * @return the transparency
	 */
	public float getTransparency() {
		return transparency;
	}

	/**
	 * @param transparency the transparency to set
	 */
	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}
}
