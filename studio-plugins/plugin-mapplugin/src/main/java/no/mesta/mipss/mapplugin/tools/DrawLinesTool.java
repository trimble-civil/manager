package no.mesta.mipss.mapplugin.tools;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.MouseInputListener;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.resources.images.IconResources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * @author Harald A Kulø
  */
public class DrawLinesTool implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MapPanel map;
    private LineListLayer layer;
    private boolean active = false;
    private Image cursorImage = IconResources.PEN_CURSOR_ICON.getImage();
    private Point hotspot = new Point(0, 30);
    private Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, hotspot, "pen");
    
    public DrawLinesTool(MapPanel map, LineListLayer layer) {
        this.map = map;
        this.layer = layer;
        map.setCursor(cursor);
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getButton()==MouseEvent.BUTTON3){
            active = false;
        }else{
            active = true;
            List<MipssGeoPosition[]> points = layer.getPoints();
            
            MipssGeoPosition[] geoLine = new MipssGeoPosition[2];
            //GeoPosition pointInGeo = map.getTileFactory().pixelToGeo(e.getPoint(), map.getZoom());
            GeoPosition tempPoint = map.convertPointToGeoPosition(e.getPoint());
            MipssGeoPosition pointInGeo = new MipssGeoPosition(tempPoint.getLatitude(), tempPoint.getLongitude());
            Bounds newBounds = new Bounds();
            newBounds.setLlx(pointInGeo.getLongitude());
            newBounds.setLly(pointInGeo.getLatitude());
            newBounds.setUrx(pointInGeo.getLongitude());
            newBounds.setUry(pointInGeo.getLatitude());
            
            //no points present, create a geoLine
            if (points==null){
                geoLine[0] = pointInGeo;
            }else{
                //points present
                if (points.size()>0){
                    
                	MipssGeoPosition[] gp = points.get(points.size()-1);
                    if (gp[1]!=null){
                        geoLine[0]=gp[1];
                        geoLine[1]=pointInGeo;
                    }
                    
                    //second point of last line is not present, set second point to the clicked point
                    if (gp[1]==null){
                        gp[1]=pointInGeo;
                    }else{//add the geoLine with second point as null
                        points.add(geoLine);
                        layer.getBounds().addBounds(newBounds);
                    }
                }
            }
            
            //add the first point to layer
            if (points == null){
                points = new ArrayList<MipssGeoPosition[]>();
                points.add(geoLine);
                layer.setBounds(newBounds);
                layer.setPoints(points);
            }
            map.getLayerHandler().changeLayer(layer);
            map.repaint();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
        if (active){
            List<MipssGeoPosition[]> points = layer.getPoints();
            GeoPosition tempPoint = map.convertPointToGeoPosition(e.getPoint());
            MipssGeoPosition pointInGeo = new MipssGeoPosition(tempPoint.getLatitude(), tempPoint.getLongitude());
            if (points!=null){
                if (points.size()>0){
                    MipssGeoPosition[] lastPoint = points.get(points.size()-1);
                    lastPoint[1] = pointInGeo;
                    map.repaint();
                }
            }
        }
    }
}
