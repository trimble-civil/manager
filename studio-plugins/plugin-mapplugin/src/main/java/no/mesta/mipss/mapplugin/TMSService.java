package no.mesta.mipss.mapplugin;
/**
 * TMSService til kartet.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class TMSService extends TileCacheWMSService {
	public TMSService(String layers) {
		super(layers);
	}

	/**
     * Creates a request for the TMS. 
     * @param x horizontal position for the tile
     * @param y vertical position for the tile
     * @param zoom the zoom level, used to find the resolution for the current zoom level 
     * @param tileSize the size of a Tile in pixels
     * @return a TMS request
     */
	@Override
    public String toWMSURL(int x, int y, int zoom, int tileSize) {
    	//http://10.60.71.57:8080/geoserver/gwc/service/tms/1.0.0/MestaGWC@EPSG%3A32633@png/2/0/0.png
		String delim = "/";
		StringBuilder sb = new StringBuilder(getBaseUrl());
		//baseUrl/zoom/x/y.png
		sb.append(delim);
		sb.append(zoom);
		sb.append(delim);
		sb.append(x);
		sb.append(delim);
		sb.append(y);
		sb.append(".png");
        return sb.toString();
    }
}
