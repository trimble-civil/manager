package no.mesta.mipss.mapplugin;

import no.mesta.mipss.mapplugin.utm.UTMTileFactoryInfo;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * @author <a mailto:harkul@mesta.no>Harald A Kulø</a>
 */
public class DefaultWMSService extends UTMWMSService{
	private UTMTileFactoryInfo info;
	private String layers;
	public DefaultWMSService(String layers){
		this.layers = layers;
	}
    /**
     * Sets the UTMTileFactoryInfo for the map server
     * @param info
     */
	public void setInfo(UTMTileFactoryInfo info) {
		this.info = info;
	}
	
	public UTMTileFactoryInfo getInfo(){
		return info;
	}
	
	public String toWMSURL(int x, int y, int zoom, int tileSize) {
		
        String format = "image/png";
        String srs = "EPSG:41000";
//        String tiled = "true";
//        String service = "wms";
        String version = "1.1.1";
        String request = "GetMap";
//        String exceptions = "application/vnd.ogc.se_inimage";

        if  (zoom>info.getResolutions().getMaxResolutionIndex())
            return null;
        double resolution = info.getResolutions().getResolutionForZoom(zoom);

        //ll, ur bounds for the map
        Bounds bounds = info.getBounds();
        //calculate ll and ur
        double llx = bounds.getLlx() + (resolution * x * tileSize);
        double lly = bounds.getLly() + (resolution * y * tileSize);
        double urx = bounds.getLlx() + (resolution * (x+1) * tileSize);
        double ury = bounds.getLly() + (resolution * (y+1) * tileSize);
//        try{
//	        CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");	
//			CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:41000");
//			MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
//			Coordinate targetLL = JTS.transform(new Coordinate(llx, lly), new Coordinate(), transform);
//			Coordinate targetUR = JTS.transform(new Coordinate(urx, ury), new Coordinate(), transform);
//			System.out.println(llx);
//			llx = targetLL.x;
//			System.out.println(llx);
//			lly = targetLL.y;
//			urx = targetUR.x;
//			ury = targetUR.y;
//        } catch (Throwable t){
//        	t.printStackTrace();
//        }
        
        String bbox = llx + "," +lly+","+urx+","+ury;
        
        //create wms-request
        String url = getBaseUrl() +
        				"VERSION="+version+
        				"&REQUEST="+request+
        				"&STYLES="+
        				"&SRS="+srs+
        				"&FORMAT="+format+
        				"&BBOX="+bbox+
        				"&HEIGHT="+tileSize+
                        "&WIDTH="+tileSize+
        				"&LAYERS="+layers
        				;
        
//        System.out.println(url);
        return url;
    }
}
