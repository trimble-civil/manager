package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;

/**
 * A collection of LineListLayer objects to create a series of lines where each 
 * line collection has its own visual appearance
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class LineListLayerCollection extends Layer implements LayerCollection, Serializable, SelectableLayer{
    private transient Logger log = LoggerFactory.getLogger(this.getClass());
    private List<? extends CollectionEntity> lineLayers;
    private CollectionPainter collectionPainter;
    private boolean returnThisOnLayerCloseTo = false;
    private LineListLayerCollection selected;
    /**
     * Create a new LineListLayerCollection
	 *
     * @param id programmatic Id for this layercollection
     * @param lineLayers the layers in this collection
     * @param bounds the boundingbox for the collection
     * @param isTriggerRecenterAndZoom
     */
    public LineListLayerCollection(String id, List<? extends CollectionEntity> lineLayers, Bounds bounds, boolean isTriggerRecenterAndZoom) {
        super(id, isTriggerRecenterAndZoom);
        this.lineLayers = lineLayers;
        setBounds(bounds);
        
        //set this layer as parent for all the layers in this collection
        for (CollectionEntity l:lineLayers){
            l.setParent(this);
        }
    }
    
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        if (collectionPainter!=null){
            collectionPainter.paint(g, map, w, h);
        }
    }
    /**
     * Warning: this will override any previous colors. A collection
     * may contain different color values on contained layers. All layers 
     * in this collection will get equal colors.
     */
    public void setColor(Color color){
    	super.setColor(color);
    	for (CollectionEntity e:getLayers()){
    		if (e instanceof Layer){
    			Layer l = (Layer)e;
    			l.setColor(color);
    		}
    	}
    }
    public List<? extends CollectionEntity> getLayers() {
        return lineLayers;
    }
    
    /**
     * For Collections with a single sublayer, return the
     * TextboxWidget for the sublayer. 
     */
    @Override 
    public TextboxWidget getTextboxLayer(){
    	if (lineLayers!=null&&lineLayers.size()==1){
    		return lineLayers.get(0).getTextboxLayer();
    	}
    	return super.getTextboxLayer();
    }
    public int getIndexOfLayer(CollectionEntity l) {
        return 0;
    }
    
    public void setCollectionPainter(CollectionPainter collectionPainter) {
        this.collectionPainter = collectionPainter;
    }

	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition gp,	double searchRadius) {
		for (SelectableLayer l:lineLayers){
			SelectableLayer sl = l.getLayerCloseTo(gp, searchRadius);
			if (sl!=null){
				if (returnThisOnLayerCloseTo)
					return this;
				else
					return sl;
			}
		}
		return null;
	}

	@Override
	public Layer getSelectionLayer() {
		if (!returnThisOnLayerCloseTo)
			return null;
		else if (selected==null){
			String color = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "selectionColor").getVerdi();
	        final Color selectionColor = ColorUtil.fromHexString(color);
	        String selectedWidth = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "antallPixlerSelect").getVerdi();
	        int selectedStroke = Integer.parseInt(selectedWidth);
	        
			List<PeriodLineListLayer> list = new ArrayList<PeriodLineListLayer>();
			for (CollectionEntity ll:lineLayers){
				if (ll instanceof PeriodLineListLayer){
					PeriodLineListLayer l = (PeriodLineListLayer)ll;
					PeriodLineListLayer clone = l.clone();
					clone.setStroke(selectedStroke);
					clone.setColor(selectionColor);
					list.add(clone);
				}
			}
			selected = new LineListLayerCollection(getId(), list, getBounds(),isTriggerRecenterAndZoom());
			selected.setTextboxProducer(getTextboxProducer());
			selected.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
			selected.setCollectionPainter(new CollectionPainter(selected.getLayers(), selected));
			
		}
		return selected;
	}

	public void setReturnThisOnLayerCloseTo(boolean returnThisOnLayerCloseTo) {
		this.returnThisOnLayerCloseTo = returnThisOnLayerCloseTo;
	}
}


