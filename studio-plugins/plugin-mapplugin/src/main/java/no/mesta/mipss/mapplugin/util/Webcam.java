package no.mesta.mipss.mapplugin.util;

import no.mesta.mipss.mapplugin.MipssGeoPosition;

public class Webcam {
	private String url;
	private String stedsnavn;
	private String veg;
	private String landsdel;
	private double breddegrad;
	private double lengdegrad;
	private String vaervarselUrl;
	private String info;
	private MipssGeoPosition point;
	
	public Webcam(){
	}
	
	public Webcam (String url, String stedsnavn, String veg, String landsdel, double breddegrad, double lengdegrad, String vaervarselUrl, String info){
		this.url = url;
		this.stedsnavn = stedsnavn;
		this.veg = veg;
		this.landsdel = landsdel;
		this.breddegrad = breddegrad;
		this.lengdegrad = lengdegrad;
		this.vaervarselUrl = vaervarselUrl;
		this.info = info;
		
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getStedsnavn() {
		return stedsnavn;
	}
	public void setStedsnavn(String stedsnavn) {
		this.stedsnavn = stedsnavn;
	}
	public String getVeg() {
		return veg;
	}
	public void setVeg(String veg) {
		this.veg = veg;
	}
	public String getLandsdel() {
		return landsdel;
	}
	public void setLandsdel(String landsdel) {
		this.landsdel = landsdel;
	}
	public double getBreddegrad() {
		return breddegrad;
	}
	public void setBreddegrad(double breddegrad) {
		this.breddegrad = breddegrad;
	}
	public double getLengdegrad() {
		return lengdegrad;
	}
	public void setLengdegrad(double lengdegrad) {
		this.lengdegrad = lengdegrad;
	}
	public String getVaervarselUrl() {
		return vaervarselUrl;
	}
	public void setVaervarselUrl(String vaervarselUrl) {
		this.vaervarselUrl = vaervarselUrl;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public void setPoint(MipssGeoPosition point) {
		this.point = point;
	}

	public MipssGeoPosition getPoint() {
		return point;
	}
	
	
	
}
