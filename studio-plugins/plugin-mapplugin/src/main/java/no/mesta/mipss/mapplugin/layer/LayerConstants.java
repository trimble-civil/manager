package no.mesta.mipss.mapplugin.layer;

public interface LayerConstants {
    public static final int DEFAULT_POINT_DIAMETER = 10;
    public static final int DEFAULT_POINT_DIAMETER_SELECTED = 17;
    public static final int DEFAULT_DIRECTION_ARROW_LENGTH = 13;
    
    public static final int LOCATION_LL = 0;
    public static final int LOCATION_LR = 1;
    public static final int LOCATION_UR = 2;
    public static final int LOCATION_UL = 3;
    
}
