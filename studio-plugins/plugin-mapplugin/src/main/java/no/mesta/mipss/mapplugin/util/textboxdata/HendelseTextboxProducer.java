package no.mesta.mipss.mapplugin.util.textboxdata;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Hendelse;

public class HendelseTextboxProducer implements TextboxProducer{
	
	private String hendelseGuid;
	private final IMipssStudioLoader loader;
	
	public HendelseTextboxProducer(IMipssStudioLoader loader, String hendelseGuid){
		this.loader = loader;
		this.hendelseGuid = hendelseGuid;
	}
	
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
//		MipssField bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
//		return new HendelseSokViewTextboxProducer(loader, bean.hentHendelseSok(hendelseGuid)).getTextboxData();
//		throw new RuntimeException("Ikke reimplementert!!");
//		return null;
		RskjemaService bean = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
		Hendelse hendelse = bean.hentHendelse(hendelseGuid);
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		TextboxData data = new TextboxData();
		data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.hendelse.title"));
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.dato"), n(MipssDateFormatter.formatDate(hendelse.getOwnedMipssEntity().getOpprettetDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT)));
		String arsak ="";
		if (hendelse.getAarsak()!=null)
			arsak = hendelse.getAarsak().getNavn();
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.arsak"), n(arsak));
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.tiltak"), n(hendelse.getTiltakstekster()));
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.hp"), n(hendelse.getVeiref().getVeiGUIText()));
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.kommune"), n(hendelse.getSak().getKommune()));
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.skadested"), n(hendelse.getSak().getSkadested()));
		data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.registrert"), n(hendelse.getOwnedMipssEntity().getOpprettetAv()));
		dataList.add(data);
		if (hendelse.getBilder()!=null){
			for (Bilde b:hendelse.getBilder()){
				data.addBilde(b);
			}
		}
		return dataList;
	}
	private String n(String str){
		if (str==null)
			return " ";
		return str;
	}
}
