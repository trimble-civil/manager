package no.mesta.mipss.mapplugin.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.ui.CrossButton;

import org.jdesktop.swingx.JXImageView;

/**
 * Dialog that displays a zoomed image.
 * 
 * @author Harald A. Kulø
 */
public class QuickZoomDialog extends JDialog implements MouseInputListener{
    private ImagePanel zoomPanel;
    private JPanel dialogPanel;
    private int zoom = 4;
    private JLabel headerLabel;
    private JPopupMenu zoomMenu;
    /**
     * Creates a new instance of QuickZoomDialog
     * @param owner the owner of the dialog
     */
    public QuickZoomDialog(Frame owner) { //Window?
        super(owner);
        setUndecorated(true);
        setLayout(new BorderLayout());
        zoomMenu = createZoomMenu();
        
        
        headerLabel = new JLabel("Zoom vindu ("+zoom+"x)");
        headerLabel.setForeground(Color.white);
        headerLabel.addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
                zoomMenu.setLabel("Velg zoom");
                zoomMenu.show(QuickZoomDialog.this, e.getX(), e.getY());
            }
        });
        CrossButton closeButton = new CrossButton(new Dimension(20,20), "");
        closeButton.addActionListener(new ActionListener(){    
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        
        JPanel headerPanel = new JPanel(new BorderLayout());
        headerPanel.setBackground(new Color(0,0,100));
        JPanel textPanel = new JPanel(new FlowLayout());
        textPanel.setOpaque(false);
        textPanel.add(headerLabel);
        //JLabel endreLabel = new JLabel("(klikk=endre)");
        //endreLabel.setForeground(Color.white);
        //textPanel.add(endreLabel);
        headerPanel.add(textPanel, BorderLayout.WEST);
        headerPanel.add(closeButton, BorderLayout.EAST);
        
        zoomPanel = new ImagePanel(null);
        
        dialogPanel = new JPanel(new BorderLayout());
        dialogPanel.add(headerPanel, BorderLayout.NORTH);
        dialogPanel.add(zoomPanel, BorderLayout.CENTER);
        dialogPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        
        add(dialogPanel);
        addMouseMotionListener(this);
        addMouseListener(this);
        
    }
    /**
     * Creates the zoom popup menu
     */
    private JPopupMenu createZoomMenu(){
        JPopupMenu zoomMenu = new JPopupMenu("Velg zoom");
        
        zoomMenu.add(createMenuItem(1));
        zoomMenu.add(createMenuItem(2));
        zoomMenu.add(createMenuItem(3));
        zoomMenu.add(createMenuItem(4));
        zoomMenu.add(createMenuItem(5));
        zoomMenu.add(createMenuItem(6));
        zoomMenu.add(createMenuItem(7));
        zoomMenu.add(createMenuItem(8));
        zoomMenu.add(createMenuItem(9));
        zoomMenu.add(createMenuItem(10));
        return zoomMenu;
        
    }
    /**
     * Creates a menuItem for the PopupMenu and append an actionlistener
     * @param zoom the zoom level
     * @return JMenuItem
     */
    private JMenuItem createMenuItem(final int zoom){
        JMenuItem item = new JMenuItem(zoom+"x");
        item.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
               setZoom(zoom);
           }
        });
        return item;
    }
    /**
     * Get the current zoom 
     */
    public int getZoom(){
        return zoom;
    }
    /**
     * Set a new zoom factor, this also changes the headerlabel
     */
    private void setZoom(int zoom){
        headerLabel.setText("Zoom vindu ("+zoom+"x)");
        this.zoom = zoom;
    }
    
    /**
     * Set the current zoomed image, this is just a delegator for ZoomPanel
     * @param image
     */
    public void setZoomImage(BufferedImage image){
        zoomPanel.setZoomImage(image);
    }


    private Point location;
    private MouseEvent pressed;
    
    public void mousePressed(MouseEvent e) {
        pressed = e;
    }
    public void mouseDragged(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        location = getLocation(location);
        int x = location.x - pressed.getX() + e.getX();
        int y = location.y - pressed.getY() + e.getY();
        setLocation(x, y);    
    }
    /**
     * Reset the cursor
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
    
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseMoved(MouseEvent e) {}

    
    
    class ImagePanel extends JXImageView{
        public ImagePanel(BufferedImage image){
            setImage(image);
        }
        public void setZoomImage(BufferedImage image){
            setImage(image);
            setScale(zoom);
        }
    }
    
    /**
     * Class that contains the zoomed image. When setZoomImage is invoked, the image
     * gets enlarged by a factor of 4.
     * @author Harald A. Kulø
     * @obsolete
     *
    class ZoomPanel extends JPanel{
        private BufferedImage image;
        
        public void setZoomImage(BufferedImage image){
            this.image = enlarge(image, 4);
            repaint();
        }
        
        public void paint(Graphics g){
            if (image!=null){
                g.drawImage(image, 0,0,this);
                g.setColor(Color.black);
                g.drawLine(getWidth()/2-3, getHeight()/2, getWidth()/2+3, getHeight()/2);
                g.drawLine(getWidth()/2, getHeight()/2-3, getWidth()/2, getHeight()/2+3);
            }
        }
        public void update(Graphics g){
            paint(g);
        }
        private BufferedImage enlarge(BufferedImage image, int n) {
            int w = n * image.getWidth();
            int h = n * image.getHeight();
            BufferedImage enlargedImage = new BufferedImage(w, h, image.getType());
            
            for (int y=0; y < h; ++y)
                for (int x=0; x < w; ++x)
                    enlargedImage.setRGB(x, y, image.getRGB(x/n, y/n));
            
            return enlargedImage;
        }
    }*/
}
