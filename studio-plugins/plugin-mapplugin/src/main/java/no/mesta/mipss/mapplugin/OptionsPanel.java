package no.mesta.mipss.mapplugin;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EtchedBorder;
import javax.swing.tree.DefaultMutableTreeNode;


import org.jdesktop.swingx.JXDatePicker;

/**
 * @author <a mailto:harkul@mesta.no>Harald A Kulø</a>
 */
public class OptionsPanel extends JPanel {
    private BorderLayout borderLayout1 = new BorderLayout();
    //private JPanel this = new JPanel();

    private JPanel jPanel1 = new JPanel();
    private JPanel jPanel2 = new JPanel();
    private JPanel jPanel3 = new JPanel();
    private JButton jButton9 = new JButton();
    private JTree jTree1 = new JTree();
    private JButton jButton1 = new JButton();
    private JCheckBox jCheckBox1 = new JCheckBox();
    private JButton jButton2 = new JButton();
    private JCheckBox jCheckBox2 = new JCheckBox();
    private JButton jButton3 = new JButton();
    private JButton jButton4 = new JButton();
    private JButton jButton5 = new JButton();
    private JButton jButton6 = new JButton();
    private JButton jButton7 = new JButton();
    private JCheckBox jCheckBox3 = new JCheckBox();
    private JCheckBox jCheckBox4 = new JCheckBox();
    private JCheckBox jCheckBox5 = new JCheckBox();
    private JCheckBox jCheckBox6 = new JCheckBox();
    private JCheckBox jCheckBox7 = new JCheckBox();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel jLabel1 = new JLabel();
    private JRadioButton jRadioButton1 = new JRadioButton();
    private JRadioButton jRadioButton2 = new JRadioButton();
    private JPanel jPanel4 = new JPanel();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    private JLabel jLabel2 = new JLabel();
    private JXDatePicker jXDatePicker1 = new JXDatePicker();
    private JLabel jLabel3 = new JLabel();
    private JXDatePicker jXDatePicker2 = new JXDatePicker();
    private JCheckBox jCheckBox8 = new JCheckBox();
    private JPanel jPanel6 = new JPanel();
    private JButton jButton8 = new JButton();
    private GridBagLayout gridBagLayout5 = new GridBagLayout();
    private JButton jButton10 = new JButton();
    private JButton jButton11 = new JButton();
    private JButton jButton12 = new JButton();
    private JButton jButton13 = new JButton();
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private GridBagLayout gridBagLayout6 = new GridBagLayout();

    public OptionsPanel() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void createTree() {
        DefaultMutableTreeNode root = 
            new DefaultMutableTreeNode("Valgte kontrakter");
        root.add(node("0301 - Oslo ytre"));
        root.add(node("0603 - Kongsberg"));
        root.add(node("2007 - Hammerfest"));
        jTree1 = new JTree(root);

    }
    
    private void group(){
        ButtonGroup g = new ButtonGroup();
        g.add(jRadioButton1);
        g.add(jRadioButton2);
    }

    private DefaultMutableTreeNode node(String value) {
        return new DefaultMutableTreeNode(value);
    }

    private void jbInit() throws Exception {
        createTree();
        group();
        this.setLayout(borderLayout1);
        this.setSize(new Dimension(189, 752));
        this.setLayout(gridBagLayout6);
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        this.setPreferredSize(new Dimension(200, 308));
        jPanel1.setBorder(BorderFactory.createTitledBorder("Oppsett"));
        jPanel1.setLayout(gridBagLayout2);
        jPanel2.setBorder(BorderFactory.createTitledBorder("Grunndata"));
        jPanel2.setLayout(gridBagLayout1);
        jPanel3.setBorder(BorderFactory.createTitledBorder("Produksjon"));
        jPanel3.setLayout(gridBagLayout3);
        jButton9.setText("Velg kontrakt");
        jButton1.setText("Kartlag");
        jButton2.setText("Områder");
        jButton3.setText("Kontraktvei");
        jButton4.setText("Roder");
        jButton5.setText("NVBD Obj");
        jButton6.setText("Vær stat.");
        jButton7.setText("Punkt");
        jLabel1.setText("Periode :");
        jRadioButton1.setText("Sanntid");
        jRadioButton2.setText("Periode");
        jPanel4.setLayout(gridBagLayout4);
        jLabel2.setText("Fra:");
        jLabel3.setText("Til:");
        jCheckBox8.setText("Nåtid");
        jCheckBox8.setToolTipText("null");
        jPanel6.setLayout(gridBagLayout5);
        jButton8.setText("Funn");
        jButton10.setText("Inspeksjon");
        jButton11.setText("Friksjon");
        jButton12.setText("Produksjon");
        jButton13.setText("Kjøretøy");
        jPanel1.add(jButton9, 
                    new GridBagConstraints(0, 0, GridBagConstraints.REMAINDER, 
                                           1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        jPanel1.add(jTree1,
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                           new Insets(0, 0, 5, 0), 62, 0));
        jPanel2.add(jButton1, 
                    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(5, 0, 0, 5), 20, 0));jPanel2.add(jCheckBox1, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(5, 5, 0, 0), 21, 0));jPanel2.add(jButton2, 
                    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(6, 0, 0, 5), 12, 0));jPanel2.add(jCheckBox2, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(9, 5, 0, 0), 21, 0));jPanel2.add(jButton3, 
                    new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(6, 0, 0, 5), 0, 0));jPanel2.add(jButton4, 
                    new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(6, 0, 0, 5), 25, 0));jPanel2.add(jButton5, 
                    new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(6, 0, 0, 5), 6, 0));jPanel2.add(jButton6, 
                    new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(6, 0, 0, 5), 8, 0));jPanel2.add(jButton7, 
                    new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(6, 0, 6, 5), 28, 0));jPanel2.add(jCheckBox3, 
                    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(9, 5, 0, 0), 21, 0));jPanel2.add(jCheckBox4, 
                    new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(9, 5, 0, 0), 21, 0));jPanel2.add(jCheckBox5, 
                    new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(9, 5, 0, 0), 21, 0));jPanel2.add(jCheckBox6, 
                    new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(9, 5, 0, 0), 21, 0));jPanel2.add(jCheckBox7, 
                    new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(9, 5, 9, 0), 21, 0));
        this.add(jPanel1, 
                 new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, 
                                        new Insets(5, 0, 0, 0), 0, 0));
        this.add(jPanel2, 
                 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 0, 0));
        jPanel3.add(jLabel1, 
                    new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 10, 0, 0), 0, 0));
        jPanel3.add(jRadioButton1, 
                    new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 10), 0, 0));
        jPanel3.add(jRadioButton2, 
                    new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 10), 0, 0));
        jPanel4.add(jLabel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                    new Insets(0, 0, 0, 0), 0, 0));
        jPanel4.add(jXDatePicker1, 
                    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(5, 0, 0, 0), 0, 0));
        jPanel4.add(jLabel3, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        jPanel4.add(jXDatePicker2, 
                    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(5, 0, 0, 0), 0, 0));
        jPanel4.add(jCheckBox8, 
                    new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 10, 0, 0), 0, 0));
        jPanel3.add(jPanel4, 
                    new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        jPanel6.add(jButton8, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(5, 0, 5, 0), 0, 0));
        jPanel6.add(jButton10, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        jPanel6.add(jButton11, 
                    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        jPanel6.add(jButton12, 
                    new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        jPanel6.add(jButton13, 
                    new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        jPanel3.add(jPanel6, 
                    new GridBagConstraints(0, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(10, 0, 0, 0), 0, 0));
        this.add(jPanel3, 
                 new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, 
                                        new Insets(0, 0, 0, 0), 0, 0));
    }
    private boolean playing = false;

    
}
