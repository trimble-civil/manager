package no.mesta.mipss.mapplugin.layer;

import no.mesta.mipss.mapplugin.MipssGeoPosition;

/**
 * Defines a SelectableLayer that can be put into a CollectionLayer
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public interface CollectionEntity extends SelectableLayer{

    /**
     * Get the parent collection layer
     * @return
     */
    LayerCollection getParent();
    void setParent(LayerCollection parent);
    /**
     * Determins if this layer is close to the GeoPosition
     * 
     * @param gp the position to search from
     * @param searchRadius the radius for the search.
     * @return
     */
    SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius);
    /**
     * Sets the layers visibility
     * @param visible
     */
    void setVisible(boolean visible);
    /**
     * Check visibility
     * @return
     */
    boolean isVisible();
}
