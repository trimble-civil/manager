package no.mesta.mipss.mapplugin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.jms.MessageListener;
import javax.swing.JLabel;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mapplugin.tools.JFreeChartTool;
import no.mesta.mipss.mapplugin.tools.PanMapTool;
import no.mesta.mipss.mapplugin.util.CoordinateEvent;
import no.mesta.mipss.mapplugin.util.CoordinateMessageListener;
import no.mesta.mipss.mapplugin.util.LayerListener;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MouseListenerManager;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.utm.UTMProjectionTileFactory;
import no.mesta.mipss.mapplugin.utm.UTMTileFactoryInfo;
import no.mesta.mipss.mapplugin.widgets.CompassWidget;
import no.mesta.mipss.mapplugin.widgets.LabelWidget;
import no.mesta.mipss.mapplugin.widgets.ResolutionsWidget;
import no.mesta.mipss.mapplugin.widgets.ScaleWidget;
import no.mesta.mipss.mapplugin.widgets.Widget;
import no.mesta.mipss.mapplugin.widgets.WidgetController;
import no.mesta.mipss.mapplugin.widgets.WorldWidget;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.picturepanel.ImageSelection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.PropertySetter;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Tile;
import org.jdesktop.swingx.mapviewer.TileFactory;
import org.jdesktop.swingx.mapviewer.TileFactoryInfo;
import org.jdesktop.swingx.mapviewer.Waypoint;

/**
 * 
 * A generic class to create a simple MapPanel. This panel can be used -as is-
 * or be extended to add more specialized functionality.
 * 
 * @author Harald A. Kulø
 */
@SuppressWarnings("serial")
public abstract class MapPanel extends JXMapViewer implements LayerListener {
	private LayerHandler layerHandler = new LayerHandler();
	private WidgetController widgets = new WidgetController();

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private Resolutions resolutions;
	private Bounds bounds;
	private String tilesOrigin;
	private String baseURL;
	private String layers;
	private boolean useTMS;
	private List<CoordinateMessageListener> messageListeners;
	private MouseListenerManager mouseListenerManager;
	private JLabel scaleLabel = new JLabel(ResourceUtil.getResource("mapPlugin.label.malestokk"));

	private boolean zoomChanging = false;

	private Image tempImage;

	private boolean allTilesPainted = false;
	private LabelWidget zoomLoadingLabel = new LabelWidget(this, 10,Widget.GLUE.TOP_CENTER, "Henter kartgrunnlag...", 14);

	/** TODO finne en bedre løsning */
	private JFreeChartTool jfreeTool = new JFreeChartTool(this);
	public JFreeChartTool getJFreeChartTool(){
		return jfreeTool;
	}
	/**
	 * Creates a new MapPanel
	 * 
	 * @param boundsAndResolutions
	 *            the inputStream to a xml file that contains the settings for
	 *            the mapserver
	 */
	public MapPanel() {
		readConfig();
		for (KeyListener k : getKeyListeners()) {
			removeKeyListener(k);
		}
		initGui();
		try {
			setLoadingImage(ImageIO.read(getClass().getResource("/images/loading.png")));
		} catch (IOException e) {
		}
		
		final UTMWMSService wms;
		
		if (useTMS) {
			wms = new TMSService(layers);	
		} else {
			wms = new TileCacheWMSService(layers);
		}
		
		wms.setBaseUrl(baseURL);
		UTMTileFactoryInfo info = new UTMTileFactoryInfo(256, true, false, "","x", "y", "zoom", resolutions, bounds, tilesOrigin, wms);
		UTMProjectionTileFactory fac = new UTMProjectionTileFactory(info);
		setTileFactory(fac);

		messageListeners = new ArrayList<CoordinateMessageListener>();

		setZoom(0);
		setRestrictOutsidePanning(true);
		setHorizontalWrapped(false);

		double width = bounds.getUrx() - bounds.getLlx();
		double centerX = bounds.getLlx() + (width / 2);
		double height = bounds.getUry() - bounds.getLly();
		double centerY = bounds.getUry() - height / 2;

		GeoPosition center = new GeoPosition(centerY, centerX);
		setAddressLocation(center);

		zoomLoadingLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		widgets.addWidget(zoomLoadingLabel);
		widgets.addWidget(new CompassWidget(this, 60, 60, 20,CompassWidget.Type.CLOSED, Widget.GLUE.TOP_RIGHT));
		widgets.addWidget(new ScaleWidget(this, Widget.GLUE.BOTTOM_LEFT));
		widgets.addWidget(new ResolutionsWidget(this, 16, 200, 40, Widget.GLUE.RIGHT_CENTER));
//		widgets.addWidget(new WorldWidget(this, 10, Widget.GLUE.BOTTOM_RIGHT));
		
		
		layerHandler.setWidgetController(widgets);

		setOverlayPainter(layerHandler);
		layerHandler.addLayerListener(this);
		this.removeMouseWheelListener(getMouseWheelListeners()[0]);
		addMouseWheelListener(new ZoomMouseWheelListener());

		setZoom(getResolutions().getMinResolutionIndex());

		addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_G) {
					setDrawTileBorders(true);
				} else {
					setDrawTileBorders(false);
				}
			}
		});

		addMouseMotionListener(getRemoveTempImageListener());
		addMouseListener(widgets);
		addMouseMotionListener(widgets);
		layerHandler.addLayerListener(jfreeTool);
		setCursor(Toolkit.getDefaultToolkit().createCustomCursor(IconResources.HAND_OPEN_ICON.getImage(), new Point(16,16), "OpenHandCursor"));
	}

	private MouseMotionListener getRemoveTempImageListener() {
		MouseMotionListener ml = new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent e) {
				tempImage = null;
			}

			@Override
			public void mouseMoved(MouseEvent e) {
			}

		};
		return ml;
	}

	/**
	 * Repaint the map
	 * 
	 * @param g
	 *            graphics context
	 * @param mapOnly
	 *            true if the @see {LayerHandler} and other components should be
	 *            removed before repainting, this is usefull if we're interested
	 *            in only painting the underlying map on the specified graphics
	 *            context. False if we want to paint the entire contents on a
	 *            graphics context other than the screen's graphics context.
	 *            This sometimes creates a unrecoverable deadlock in the gui
	 *            when the LayerHandler's paint method gets called at the same
	 *            time a repaint is in progress.
	 */
	private void paint(Graphics g, boolean mapOnly) {
		// if (!mapOnly){
		// paint(g);
		// }else{
		setOverlayPainter(null);
		paint(g);
		setOverlayPainter(layerHandler);
		// }
	}

	// zooms using the mouse wheel
	private class ZoomMouseWheelListener implements MouseWheelListener {
		public void mouseWheelMoved(MouseWheelEvent e) {
			if (isZoomEnabled()) {
				int newZoom = getZoom() + (-e.getWheelRotation());
				if (newZoom >= resolutions.getMinResolutionIndex()
						&& newZoom <= resolutions.getMaxResolutionIndex()) {// .getResolutions().length)

					GeoPosition mouseCoordinate = convertPointToGeoPosition(e.getPoint());
					setDisplayLocation(e.getPoint(), mouseCoordinate, newZoom);
					((UTMProjectionTileFactory) MapPanel.this.getTileFactory()).purgeLoading();
				}
			}
		}
	}
	

	/**
	 * Set's the MapPanels MouseListener and MouseMotionListener and removes all
	 * other MouseListeners and MouseMotionListeners. This is for uses where we
	 * need control over where the MouseEvent is consumed.
	 * 
	 * @param manager
	 */
	public void setMouseListenerManager(MouseListenerManager manager) {
		mouseListenerManager = manager;
		MouseListener[] ml = getMouseListeners();
		MouseMotionListener[] mml = getMouseMotionListeners();
		for (MouseListener m : ml) {
			removeMouseListener(m);
		}
		for (MouseMotionListener m : mml) {
			removeMouseMotionListener(m);
		}
		addMouseListener(manager);
		addMouseMotionListener(manager);

		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				GeoPosition pos = convertPointToGeoPosition(e.getPoint());
				fireCoordinateChanged(pos.getLatitude() + "", ""+ pos.getLongitude());
			}
		});
		addMouseMotionListener(getRemoveTempImageListener());
		addMouseListener(widgets);
		addMouseMotionListener(widgets);
	}

	public void activateCoordinateEvents() {
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				GeoPosition pos = convertPointToGeoPosition(e.getPoint());
				fireCoordinateChanged(pos.getLatitude() + "", pos.getLongitude()+ "");
			}
		});
	}

	/**
	 * Get the MouseListenerManager
	 * 
	 * @return
	 */
	public MouseListenerManager getMouseListenerManager() {
		return mouseListenerManager;
	}

	/**
	 * Adds a panning mouse listener
	 * 
	 */
	public void addPanMouseListener() {
		MouseInputListener mia = new PanMapTool(this);
		addMouseListener(mia);
		addMouseMotionListener(mia);
	}

	/**
	 * Initialize the extra controls on the map panel, such as zoom slider and
	 * map scale
	 */
	private void initGui() {
	}

	private Image createBigImage() {
		if (getWidth() > 0 && getHeight() > 0) {
			BufferedImage image = new BufferedImage(getWidth(), getHeight(),
					BufferedImage.TYPE_INT_ARGB);// r.createScreenCapture(new
													// Rectangle(p.x, p.y,
													// getWidth(),getHeight()));
			paint(image.getGraphics(), true);

			return image;
		}
		return null;
	}

	public void copyToClipboard() {
		if (getWidth() > 0 && getHeight() > 0) {
			BufferedImage image = new BufferedImage(getWidth(), getHeight(),
					BufferedImage.TYPE_INT_ARGB);// r.createScreenCapture(new
													// Rectangle(p.x, p.y,
													// getWidth(),getHeight()));
			paint(image.getGraphics());
			ImageSelection.copyImageToClipboard(image);
		}
	}
	
	public BufferedImage getMapImage(){
		if (getWidth() > 0 && getHeight() > 0) {
			BufferedImage image = new BufferedImage(getWidth(), getHeight(),
					BufferedImage.TYPE_INT_ARGB);// r.createScreenCapture(new
													// Rectangle(p.x, p.y,
													// getWidth(),getHeight()));
			paint(image.getGraphics());
			return image;
		}
		return null;
	}

	private double scale = 1;

	private double getScale() {
		return scale;
	}

	private int prevZoom = -1;

	public void setZoom(int zoom) {
		prevZoom = getZoom();
		scale = resolutions.getResolutionForZoom(prevZoom)/ resolutions.getResolutionForZoom(zoom);
		tempImage = createBigImage();
		zoomChanging = true;
		Rectangle viewportBounds = getViewportBounds();
		int centerX = viewportBounds.x + (viewportBounds.width / 2);
		int centerY = viewportBounds.y + (viewportBounds.height / 2);
		GeoPosition center = getTileFactory().pixelToGeo(
				new Point2D.Double(centerX, centerY), getZoom());

		super.setZoom(zoom);

		double res = this.getResolutions().getResolutionForZoom(zoom);
		scaleLabel.setText(ResourceUtil
				.getResource("mapPlugin.label.malestokk")
				+ res);
		setAddressLocation(center);
		zoomChanging = false;
	}

	public void setZoom(int zoom, Point c) {
		prevZoom = getZoom();
		scale = resolutions.getResolutionForZoom(prevZoom)
				/ resolutions.getResolutionForZoom(zoom);
		tempImage = createBigImage();
		zoomChanging = true;

		GeoPosition pos = getTileFactory().pixelToGeo(c, getZoom());
		Point2D p2 = convertGeoPositionToPoint(pos);
		Rectangle view = getViewportBounds();
		int centerX = view.x + (view.width / 2) + (int) p2.getX();
		int centerY = view.y + (view.height / 2) + (int) p2.getY();
		GeoPosition center = getTileFactory().pixelToGeo(
				new Point2D.Double(centerX, centerY), getZoom());

		super.setZoom(zoom);

		Point2D currentCenter = this.getCenter();
		double xdiff = currentCenter.getX() - c.getX();
		double ydiff = currentCenter.getY() - c.getY();
		double res = this.getResolutions().getResolutionForZoom(zoom);
		double nex = (xdiff * res) / 2;
		double ney = (ydiff * res) / 2;
		c.setLocation(c.x + (int) nex, c.y + (int) ney);

		scaleLabel.setText(ResourceUtil
				.getResource("mapPlugin.label.malestokk")
				+ res);
		setAddressLocation(center);
		zoomChanging = false;
	}
	
	/**
	 * 
	 * @param mapPoint
	 * @param position
	 * @param zoom
	 */
	public void setDisplayLocation(Point mapPoint, GeoPosition position, int zoom){
		Point2D cent = convertGeoPositionToPoint(getAddressLocation());
		Point2D point = convertGeoPositionToPoint(position);
		double dx = cent.getX()-point.getX();
		double dy = cent.getY()-point.getY();
		offset = new Point2D.Double(dx, dy);
		setZoom(zoom);
		GeoPosition currentCenter = getAddressLocation();
		
		
		
		//hvor langt blir det fra nye center til position?
		GeoPosition newPosition = convertPointToGeoPosition(mapPoint);
		
		double offx = position.getLongitude()-newPosition.getLongitude();
		double offy = position.getLatitude()-newPosition.getLatitude();
		Point2D.Double p = new Point2D.Double();
		p.x = currentCenter.getLongitude()+offx;
		p.y = currentCenter.getLatitude()+offy;
		setAddressLocation(p);
        
	}


	public boolean isZoomChanging() {
		return zoomChanging;
	}

	private void readConfig() {
		String app = "studio.mipss.mapplugin";

		KonfigparamPreferences k = KonfigparamPreferences.getInstance();
		String[] layersString = k.hentEnForApp(app, "mapserverLayers").getVerdi().split(",");
		baseURL = k.hentEnForApp(app, "mapserverBaseURL").getVerdi();
		String[] resolutionsString = k
				.hentEnForApp(app, "mapserverResolutions").getVerdi()
				.split(",");
		tilesOrigin = k.hentEnForApp(app, "mapserverTilesOrigin").getVerdi();
		String llx = k.hentEnForApp(app, "mapserverLlx").getVerdi();
		String lly = k.hentEnForApp(app, "mapserverLly").getVerdi();
		String urx = k.hentEnForApp(app, "mapserverUrx").getVerdi();
		String ury = k.hentEnForApp(app, "marserverUry").getVerdi();

		// read the bounds from properties
		bounds = new Bounds();
		bounds.setLlx(Double.parseDouble(llx));
		bounds.setLly(Double.parseDouble(lly));
		bounds.setUrx(Double.parseDouble(urx));
		bounds.setUry(Double.parseDouble(ury));

		double[] res = new double[resolutionsString.length];

		for (int i = 0; i < resolutionsString.length; i++) {
			res[i] = Double.parseDouble(resolutionsString[i]);
		}
		resolutions = new Resolutions(res);

		String l = Arrays.toString(layersString);
		l = l.replaceAll(" ", "");
		layers = l.substring(1, l.length() - 1);
		
		useTMS = k.hentEnForApp(app, "bruk_TMS").getVerdi().equals("true");

	}

	/**
	 * Read the config file stored as XML. This file contains information about
	 * the tilecache server such as: bounds resolutions baseURL
	 * 
	 * @param boundsAndResolutions
	 */
	@SuppressWarnings("unused")
	private void readConfig(InputStream boundsAndResolutions) {

		bounds = new Bounds();
		try {
			// create properties from xml file
			Properties p = new Properties();
			p.loadFromXML(boundsAndResolutions);

			// read the bounds from properties
			bounds.setLlx(Double.parseDouble("" + p.get("llx")));
			bounds.setLly(Double.parseDouble("" + p.get("lly")));
			bounds.setUrx(Double.parseDouble("" + p.get("urx")));
			bounds.setUry(Double.parseDouble("" + p.get("ury")));

			// read the resolutions from properties
			String[] resolutionsString = p.getProperty("resolutions")
					.split(",");
			double[] res = new double[resolutionsString.length];

			for (int i = 0; i < resolutionsString.length; i++) {
				res[i] = Double.parseDouble(resolutionsString[i]);
				log.debug(res[i] + " " + i);
			}
			resolutions = new Resolutions(res);

			// read baseURL
			baseURL = "" + p.get("baseURL");

			// read layers
			String[] layersString = p.getProperty("layers").split(",");
			String l = Arrays.toString(layersString);
			l = l.replaceAll(" ", "");
			layers = l.substring(1, l.length() - 1);
			tilesOrigin = p.getProperty("tilesOrigin");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Recenter the map with an animation at the specified waypoints
	 * geoposition.
	 * 
	 * @param wp
	 *            Waypoint that contains the new center location for the map
	 * @param traveltime
	 *            the animations runtime
	 */
	public void panTo(final Waypoint wp, int traveltime) {
		if (currentAnimation==null||!currentAnimation.isRunning()){
			Point2D start = getCenter();
			Point2D end = getTileFactory().geoToPixel(wp.getPosition(), getZoom());
			currentAnimation = PropertySetter.createAnimator(traveltime,this, "center", new Point2D[] { start, end });
			currentAnimation.start();
		}

	}

	/**
	 * Recenter the map with an animation
	 * 
	 * @param position
	 * @param traveltime
	 */
	public void panTo(MipssGeoPosition position, int traveltime) {
		if (currentAnimation==null||!currentAnimation.isRunning()){
			Point2D start = new Point2D.Double(getAddressLocation().getLongitude(), getAddressLocation().getLatitude());// getCenter();
			Point2D end = new Point2D.Double(position.getLongitude(), position.getLatitude());// getTileFactory().geoToPixel(position,// getZoom());
			currentAnimation = PropertySetter.createAnimator(traveltime,this, "addressLocation", new Point2D[] { start, end });
			currentAnimation.start();
		}
	}
	
	private Animator moveInDirectionAnimation;
	public double moveInDirection(double speed, double angle){
		if (moveInDirectionAnimation!=null){
			moveInDirectionAnimation.stop();
			moveInDirectionAnimation= null;
		}
		Point2D intersection = calcIntersection(getCenter(), angle);
		double distanceInPx = intersection.distance(getCenter());
		//calc traveltime...
		int durationInSec = (int)(distanceInPx/speed*1000);
		moveInDirectionAnimation = PropertySetter.createAnimator(durationInSec, this, "center", new Point2D[]{getCenter(), intersection});
		moveInDirectionAnimation.start();
		double res = getResolutions().getResolutionForZoom(getZoom());
		double distanceInKm =(distanceInPx*res);
		return (distanceInKm/durationInSec)*60*60;
//		System.out.println("moving in:"+degree+" direction @"+speed+" mph");
	}
	public void endMoveInDirection(){
		if (moveInDirectionAnimation!=null){
			moveInDirectionAnimation.stop();
			moveInDirectionAnimation= null;
		}
	}
	
	public Point2D calcIntersection(Point2D p, double angle){
		Point2D intersect = new Point2D.Double();
		GeoPosition lowlef = new GeoPosition(bounds.getLly(), bounds.getLlx());
		GeoPosition uprigh = new GeoPosition(bounds.getUry(), bounds.getUrx());
		
		Point2D ll = getTileFactory().geoToPixel(lowlef, getZoom());
		Point2D ur = getTileFactory().geoToPixel(uprigh, getZoom());
		
		double dec = angle/Math.PI;
		//check quadrant
		if (dec>=0.25&&dec<=0.75){
			//LEFT
			double o = p.getX()/Math.tan(angle);
			intersect.setLocation(ll.getX(), p.getY()-o);
		}
		if (dec<=-0.25&&dec>=-0.75){
			//RIGHT
			double l = ur.getX()-p.getX();
			double o = l/Math.tan(angle);
			intersect.setLocation(ur.getX(), p.getY()+(int)o);
		}
		if (dec<0.25&&dec>-0.25){
			//TOP
			double l = p.getY();
			double o = l*Math.tan(angle);
			intersect.setLocation(p.getX()-o, ur.getY());
		}
		if (dec>=0.75||dec<=-0.75){
			//BOTTOM
			double l = p.getY();
			double o = l*Math.tan(angle);
			intersect.setLocation(p.getX()+o, ll.getY());
		}
		return intersect;
	}
	/**
	 * Guanrantees that the spesified position is visible on the map. If the
	 * position is outside the maps viewport, the map will recenter around the
	 * GeoPosition with a panning motion
	 * 
	 * @param position
	 *            the position to check for visibility
	 * @param movetime
	 *            the time the panning animation will run
	 */
	public void ensureVisibilityForPosition(MipssGeoPosition position,
			int movetime) {
		Rectangle bounds = this.getViewportBounds();
		// defines how many pixels the point can be from the edge before moving
		// the map
		int OFFSET = 10;
		Point2D p = this.getTileFactory()
				.geoToPixel(
						new GeoPosition(position.getLatitude(), position
								.getLongitude()), getZoom());
		int x = bounds.x + OFFSET;
		int y = bounds.y + OFFSET;
		int w = bounds.width - OFFSET;
		int h = bounds.height - OFFSET;
		Rectangle smallBounds = new Rectangle(x, y, w, h);

		if (!smallBounds.contains(p)) {
			panTo(position, movetime);
		}
	}

	/**
	 * Calculates the zoom for all layers then recenter the map
	 * 
	 * @param onlyVisible
	 *            true if only visible layers will be taken into consideration
	 */
	public void calculateZoomForAllLayers(boolean onlyVisible) {
		Map<LayerType, List<Layer>> layers = layerHandler.getLayers();
		Set<LayerType> keys = layers.keySet();
		Bounds b = null;

		LayerType[] o = keys.toArray(new LayerType[0]);
		for (int i = 0; i < o.length; i++) {
			List<Layer> ls = layers.get(o[i]);
			for (Layer l : ls) {
				if (l.getBounds() != null) {
					if (onlyVisible) {
						if (l.isVisible()) {
							if (b == null) {
								b = new Bounds(l.getBounds());
							} else {
								b.addBounds(l.getBounds());
							}
						}
					} else {
						if (b == null) {
							b = new Bounds(l.getBounds());
						} else {
							b.addBounds(l.getBounds());
						}
					}
				}
			}
		}
		if (b != null) {
			setZoom(calculateZoomForBounds(b));
			setAddressLocation(calculateCenterForBounds(b));
		}
	}

	/**
	 * Calculates the zoom required for a specified Layer and recenters the map
	 * around the center of the layer
	 * 
	 * @param l
	 */
	public void calculateZoomForLayer(Layer l) {
		setZoom(calculateZoomForBounds(l.getBounds()));
		setAddressLocation(calculateCenterForBounds(l.getBounds()));
	}

	/**
	 * Calculates the geographic center for the specified Bounds
	 * 
	 * @param layerBounds
	 *            bounds for the area
	 * @return GeoPosition center of the area
	 */
	public GeoPosition calculateCenterForBounds(Bounds layerBounds) {
		// bredde og høyde til veinettet i UTM
		double width = layerBounds.getUrx() - layerBounds.getLlx();
		double height = layerBounds.getUry() - layerBounds.getLly();
		// geografisk senter av kontraktveinettet
		double centerX = layerBounds.getLlx() + (width / 2);
		double centerY = layerBounds.getUry() - height / 2;
		return new GeoPosition(centerY, centerX);
	}

	/**
	 * Calculate wich zoomlevel that would cover the specified bounds
	 * 
	 * @param layerBounds
	 *            bounds for the area
	 * @return int the calculated zoom level
	 */
	public int calculateZoomForBounds(Bounds layerBounds) {
		// bredde og høyde til veinettet i UTM
		double width = layerBounds.getUrx() - layerBounds.getLlx();
		double height = layerBounds.getUry() - layerBounds.getLly();
		// bredde og høyde i skjermkoordinater
		double screenWidth = this.getViewportBounds().getWidth();
		double screenHeight = this.getViewportBounds().getHeight();
		// finn zoomnivå
		double pixelsNeeded = Math.max((width / screenWidth), (height / screenHeight));
		
		return getResolutions().getClosestResolutionIndex(pixelsNeeded);
	}
	
	/**
	 * Recenter and zoom the map to cover the specified Rectangle
	 * 
	 * @param rectangle
	 */
	public void recenterAndZoom(Rectangle rectangle) {
		double x = rectangle.getX();
		double y = rectangle.getY();
		double w = rectangle.getWidth();
		double h = rectangle.getHeight();

		Point2D ur = new Point2D.Double(x + w, y);
		Point2D ll = new Point2D.Double(x, y + h);

		GeoPosition UTMur = convertPointToGeoPosition(ur);
		GeoPosition UTMll = convertPointToGeoPosition(ll);

		Bounds b = new Bounds();
		b.setLlx(UTMll.getLongitude());
		b.setLly(UTMll.getLatitude());
		b.setUrx(UTMur.getLongitude());
		b.setUry(UTMur.getLatitude());
		setZoom(calculateZoomForBounds(b));
		setAddressLocation(calculateCenterForBounds(b));
	}

	public void ensureVisibilityForBounds(Bounds bounds) {
		if (bounds.getLlx() != 0 && bounds.getLly() != 0) {
			setZoom(calculateZoomForBounds(bounds));
			setAddressLocation(calculateCenterForBounds(bounds));
		}
	}

	/**
	 * Returns the layerhandler which paints all layers.
	 * 
	 * @return
	 */
	public LayerHandler getLayerHandler() {
		return layerHandler;
	}

	/**
	 * Sets the resolutions available for the map
	 * 
	 * @param resolutions
	 */
	public void setResolutions(Resolutions resolutions) {
		this.resolutions = resolutions;
	}

	/**
	 * Returns the resolutions
	 * 
	 * @return
	 */
	public Resolutions getResolutions() {
		return resolutions;
	}

	/**
	 * Set the map's boundingbox
	 * 
	 * @param bounds
	 */
	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}

	/**
	 * Relocate the map. does not change the maps center, but centers the map
	 * around the point
	 * 
	 * @param addressLocation
	 */
	public void setAddressLocation(Point2D addressLocation) {
		GeoPosition gp = new GeoPosition(addressLocation.getY(),
				addressLocation.getX());
		super.setAddressLocation(gp);
	}

	@Override
	public void setCenter(Point2D center) {
		super.setCenter(center);
	}

	public void addCoordinateMessageListener(
			CoordinateMessageListener messageListener) {
		messageListeners.add(messageListener);
	}

	public void removeCoordinateMessageListener(MessageListener messageListener) {
		messageListeners.remove(messageListener);
	}

	public CoordinateMessageListener[] getCoordinateMessageListeners() {
		return messageListeners.toArray(new CoordinateMessageListener[0]);
	}

	private void fireCoordinateChanged(String northing, String easting) {
		CoordinateEvent e = null;
		for (CoordinateMessageListener ml : getCoordinateMessageListeners()) {
			if (e == null) {
				e = new CoordinateEvent(northing, easting);
			}
			ml.coordinateChanged(e);

		}
	}

	/**
	 * Verifiserer at området er innenfor den del av kartet som vises
	 * 
	 * @param rec
	 * @return
	 */
	public boolean isWithinBounds(Rectangle rec) {
		Rectangle rect = getBounds();
		Rectangle r = new Rectangle(0, 0, (int) rect.getWidth(), (int) rect
				.getHeight());
		if (rec != null) {
			Rectangle bounds = new Rectangle(0, 0, rec.width, rec.height);
			return r.contains(bounds);
		}
		return true;
	}

	/**
	 * Kill the tilefactory when the map is closed.
	 */
	public void dispose() {
		TileFactory f = getTileFactory();
		if (f instanceof UTMProjectionTileFactory) {
			UTMProjectionTileFactory tf = (UTMProjectionTileFactory) f;
			tf.closeFactory();
		}
		this.setOverlayPainter(null);
		layerHandler.removeAllLayers();
		layerHandler = null;
	}

	/**
	 * tatt rett fra JXMapViewer for å endre bakgrunnsfargen til kartet når en
	 * tile ikke er lastet
	 */
	/**
	 * Draw the map tiles. This method is for implementation use only.
	 * 
	 * @param g
	 *            Graphics
	 * @param zoom
	 *            zoom level to draw at
	 * @param viewportBounds
	 *            the bounds to draw within
	 */
	protected void drawMapTiles(final Graphics g, final int zoom, Rectangle viewportBounds) {

		if (tempImage != null && prevZoom != getZoom()) {
			if (tempImage != null) {
				Point2D center = new Point2D.Double(getWidth() / 2, getHeight() / 2);
				Point2D loc = new Point2D.Double();
				double width = tempImage.getWidth(null) * getScale();
				double height = tempImage.getHeight(null) * getScale();
				if (offset!=null){
					center.setLocation(center.getX()+offset.getX(), center.getY()+offset.getY());
				}
				
				loc.setLocation(center.getX() - width / 2, center.getY()- height / 2);
				g.drawImage(tempImage, (int) loc.getX(), (int) loc.getY(), (int) width, (int) height, null);
				zoomLoadingLabel.setVisible(true);
			}
		}
		allTilesPainted = true;
		int size = getTileFactory().getTileSize(zoom);
		Dimension mapSize = getTileFactory().getMapSize(zoom);

		// calculate the "visible" viewport area in tiles
		int numWide = viewportBounds.width / size + 2;
		int numHigh = viewportBounds.height / size + 2;

		TileFactoryInfo info = getTileFactory().getInfo();
		int tpx = (int) Math.floor(viewportBounds.getX() / info.getTileSize(0));
		int tpy = (int) Math.floor(viewportBounds.getY() / info.getTileSize(0));

		for (int x = 0; x <= numWide; x++) {
			for (int y = 0; y <= numHigh; y++) {
				int itpx = x + tpx;// topLeftTile.getX();
				int itpy = y + tpy;// topLeftTile.getY();
				// only proceed if the specified tile point lies within the area
				// being painted
				if (g.getClipBounds().intersects(
						new Rectangle(itpx * size - viewportBounds.x, itpy
								* size - viewportBounds.y, size, size))) {
					LayeredTile tile = (LayeredTile) getTileFactory().getTile(
							itpx, itpy, zoom);
					tile.addUniquePropertyChangeListener("loaded",
							tileLoadListener); // this is a filthy hack
					int ox = ((itpx * getTileFactory().getTileSize(zoom)) - viewportBounds.x);
					int oy = ((itpy * getTileFactory().getTileSize(zoom)) - viewportBounds.y);

					// if the tile is off the map to the north/south, then just
					// don't paint anything
					if (isTileOnMap(itpx, itpy, mapSize)) {
						if (isOpaque()) {
							g.setColor(getBackground());
							g.fillRect(ox, oy, size, size);
						}
					} else if (tile.isLoaded()) {
						g.drawImage(tile.getImage(), ox, oy, null);
					} else if (!tile.isLoaded()) {
						allTilesPainted = false;
						if (tempImage == null) {
							int imageX = (getTileFactory().getTileSize(zoom) - getLoadingImage()
									.getWidth(null)) / 2;
							int imageY = (getTileFactory().getTileSize(zoom) - getLoadingImage()
									.getHeight(null)) / 2;
							g.setColor(Color.white);
							g.fillRect(ox, oy, size, size);
							g.drawImage(getLoadingImage(), ox + imageX, oy
									+ imageY, null);
						}
					}
					if (isDrawTileBorders()) {

						g.setColor(Color.black);
						g.drawRect(ox, oy, size, size);
						g
								.drawRect(ox + size / 2 - 5, oy + size / 2 - 5,
										10, 10);
						g.setColor(Color.white);
						g.drawRect(ox + 1, oy + 1, size, size);

						String text = itpx + ", " + itpy + ", " + getZoom();
						g.setColor(Color.BLACK);
						g.drawString(text, ox + 10, oy + 30);
						g.drawString(text, ox + 10 + 2, oy + 30 + 2);
						g.setColor(Color.WHITE);
						g.drawString(text, ox + 10 + 1, oy + 30 + 1);
					}
				}
			}
		}
		if (allTilesPainted) {
			tempImage = null;
			zoomLoadingLabel.setVisible(false);
//			offset=null;
		}
	}

	private boolean isTileOnMap(int x, int y, Dimension mapSize) {
		return y < 0 || y >= mapSize.getHeight();
	}

	private TileLoadListener tileLoadListener = new TileLoadListener();
	private Animator currentAnimation;
	private Point2D.Double offset;

	private final class TileLoadListener implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent evt) {
			if ("loaded".equals(evt.getPropertyName())
					&& Boolean.TRUE.equals(evt.getNewValue())) {
				Tile t = (Tile) evt.getSource();
				if (t.getZoom() == getZoom()) {
					repaint();
				}
			}
		}
	}

	@Override
	public void finalize() throws Throwable {
		layerHandler = null;
		super.finalize();
	}
}

// Hele kartet
// region.add(new GeoPosition(bounds[3], bounds[0]));
// region.add(new GeoPosition(bounds[1], bounds[0]));
// region.add(new GeoPosition(bounds[1], bounds[2]));
// region.add(new GeoPosition(bounds[3], bounds[2]));

// Norge ØST
// region.add(new GeoPosition(7017938.0683,-148135.0098));
// region.add(new GeoPosition(6421381.0691,-148135.0098));
// region.add(new GeoPosition(6421381.0691,523382.0312));
// region.add(new GeoPosition(7017938.0683,523382.0312));

