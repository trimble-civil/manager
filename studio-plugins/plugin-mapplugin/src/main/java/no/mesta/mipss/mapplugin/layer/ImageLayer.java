package no.mesta.mipss.mapplugin.layer;

import java.awt.Graphics2D;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.tools.MoveLayerTool;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * Layer for å vise et flyttbart bilde i kartet
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class ImageLayer extends Layer implements MovableLayer, CollectionEntity{
    private transient Logger log = LoggerFactory.getLogger(ImageLayer.class);
    private GeoPosition position = null;
    private ImageIcon image = null;
    private Point2D point = null;
    private MapPanel map = null;
    private LayerCollection parent;
	private TextboxData data;
	
    /**
     * Konstruktør
     * 
     * @param id
     * @param map
     * @param image
     * @param position
     */
    public ImageLayer(String id, MapPanel map, ImageIcon image, GeoPosition position) {
        super(id, false);
        this.image = image;
        this.position = position;
        this.map = map;
        MoveLayerTool.decorateMap(map);
    }

    /** {@inheritDoc} */
    public void paint(Graphics2D g, JXMapViewer map, int i, 
                      int i1) {

        g = super.initGraphics(g, map);
        point = map.getTileFactory().geoToPixel(position, map.getZoom());
        int x = (int)point.getX();
        int y = (int)point.getY() - image.getIconHeight();

        g.drawImage(image.getImage(), x,y,null);
    }

    /** {@inheritDoc} */
    public boolean moveTo(Point location) {
        Rectangle r = map.getViewportBounds();
        location.translate(r.x, r.y);
        this.position = map.getTileFactory().pixelToGeo(location, map.getZoom());
        return true;
    }

    /** {@inheritDoc} */
    public Rectangle2D getLayerBounds() {
        Rectangle r = map.getViewportBounds();
        return new Rectangle((int)point.getX() - r.x, (int)point.getY() - r.y - image.getIconHeight(), image.getIconWidth(), image.getIconHeight());
    }

    /** {@inheritDoc} */
    public void setStartPoint(Point location) {
        //Rectangle r = map.getViewportBounds();
        //location.translate(r.x, r.y);
        //point = new Point((int) point.getX()-location.x, (int)point.getY()-location.y);
        //this.position = map.getTileFactory().pixelToGeo(location, map.getZoom());
    }

    
	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius) {
		if (position.getLatitude()< gp.getLatitude()+searchRadius&&
			position.getLatitude()> gp.getLatitude()-searchRadius&&
			position.getLongitude()<gp.getLongitude()+searchRadius&&
			position.getLongitude()>gp.getLongitude()-searchRadius){
            return this;
        }
        else{
            return null;
        }
	}

	@Override
	public LayerCollection getParent() {
		return parent;
	}

	@Override
	public void setParent(LayerCollection parent) {
		this.parent = parent;
		
	}

	@Override
	public Layer getSelectionLayer() {
		return new ImageLayer(getId(), map, image, position);
	}
}
