package no.mesta.mipss.mapplugin.util;

/**
* @author Harald A Kulø
*/
public interface CoordinateMessageListener {
    void coordinateChanged(CoordinateEvent e);
}