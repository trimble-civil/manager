package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;


@SuppressWarnings("serial")
public class KontraktveiCollectionLayer extends LineListLayerCollection{

	private final List<? extends KontraktveiLayer> veinett;

	public KontraktveiCollectionLayer(String id, List<? extends KontraktveiLayer> lineLayers, Bounds bounds) {
		super(id, lineLayers, bounds, true);
		veinett = lineLayers;
	}

	public void showEv(boolean visible){
		setLayerVisible("e", visible, Color.yellow);
	}
	public void showRv(boolean visible){
		setLayerVisible("r", visible, Color.magenta);		
	}
	public void showFv(boolean visible){
		setLayerVisible("f", visible, Color.cyan);
	}
	public void showKv(boolean visible){
		setLayerVisible("k", visible, Color.white);
	}
	
	private void setLayerVisible(String kat, boolean visible, Color color){
		for (KontraktveiLayer l:veinett){
			if (l.getVeikategori().toLowerCase().equals(kat)){
				l.setVisible(visible);
				l.setColor(color);
			}
		}
	}

}
