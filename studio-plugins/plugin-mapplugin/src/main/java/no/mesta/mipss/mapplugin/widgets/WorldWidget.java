package no.mesta.mipss.mapplugin.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.MovableLayer;
import no.mesta.mipss.mapsgen.Bbox;
import no.mesta.mipss.mapsgen.MapsGen;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class WorldWidget extends Widget implements MovableLayer{
	private Point location = new Point(10,10);
	private Point dragStartPoint;

    private int size=120;

	private BufferedImage world;
	private Bbox bbox;
	
	public WorldWidget(MapPanel map, int pad, GLUE glue){
		super(map, 0, 0, pad, glue);//BOTTOM_RIGHT glue
		world = getWorld(map);
		
		setHeight(size);
		setWidth(size);
		JLabel jLabel = new JLabel(new ImageIcon(world));
		JFrame f = new JFrame();
		f.setSize(400,400);
		f.add(jLabel);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}

	private BufferedImage getWorld(MapPanel map) {
		MapsGen gen = BeanUtil.lookup(MapsGen.BEAN_NAME, MapsGen.class);
		bbox = new Bbox();
		String urx = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "mapserverUrx").getVerdi();
		String ury = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "marserverUry").getVerdi();
		String llx = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "mapserverLlx").getVerdi();
		String lly = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "mapserverLly").getVerdi();
		bbox.setUrx(Double.valueOf(urx));
		bbox.setUry(Double.valueOf(ury));
		bbox.setLlx(Double.valueOf(llx));
		bbox.setLly(Double.valueOf(lly));
		Dimension mapSize = new Dimension(size, size);
		byte[] worldMap = gen.getMap(bbox, mapSize,false,"");
		return ImageUtil.bytesToImage(worldMap);
	}

	@Override
	public boolean moveTo(Point point) {
        location.setLocation(point.x+dragStartPoint.x, point.y+dragStartPoint.y);
        return true;
	}

	@Override
	public void setStartPoint(Point point) {
       dragStartPoint = new Point(location.x-point.x, location.y-point.y);
	}

	@Override
	public Rectangle2D getLayerBounds() {
		return new Rectangle(location.x, location.y, size, size);
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer map, int width, int height) {
		copyState(g);
		initXY();
		g.translate(getX(), getY());
		g.drawImage(world, 0,0,null);
		
		Rectangle rectangle = map.getViewportBounds();
		
		double x = rectangle.getX();
		double y = rectangle.getY();
		double w = rectangle.getWidth();
		double h = rectangle.getHeight();
		Point2D ul = new Point2D.Double(x, y);
		Point2D lr = new Point2D.Double(x+w, y + h);

		GeoPosition UTMul = map.getTileFactory().pixelToGeo(ul, map.getZoom());
		GeoPosition UTMlr = map.getTileFactory().pixelToGeo(lr, map.getZoom());
		
		double p1x = UTMul.getLongitude();
		double p1y = UTMul.getLatitude();
		double p2x = UTMlr.getLongitude();
		double p2y = UTMlr.getLatitude();
		
		Point2D p1 = new Point2D.Double(p1x, p1y);
		Point2D p2 = new Point2D.Double(p2x, p2y);
		
		Point startXY = geoToPixel(p1, bbox, size);
		Point endXY = geoToPixel(p2, bbox, size);
		
		double ww = Math.abs(endXY.getX()-startXY.getX());
		double hh = Math.abs(startXY.getY()-endXY.getY());
		
		g.setColor(Color.red);
		g.drawRect(startXY.x, startXY.y, (int)ww, (int)hh);

		resetState(g);
	}

	private Point geoToPixel(Point2D geo, Bbox box, int size){
		double width = box.getWidth();//box.getUrx() - box.getLlx();
        double res = width/size;
    	double x = (geo.getX() - box.getLlx()) / res;
    	double ury = size * res+box.getLly();
    	double y = (ury - geo.getY()) / res;
    	return new Point((int)x, (int)y);
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

}
