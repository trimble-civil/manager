package no.mesta.mipss.mapplugin.layer;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;

/**
 * Defines the rules for a selectable layer (a layer that can be selected by mouseclick)
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public interface SelectableLayer {
    
    String getId();
	
    /**
     * 
     * @param gp
     * @param searchRadius
     * @return
     */
    SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius);
    
    /**
     * Returns the layer that will display information on this layer when it is selected.
     * @return
     */
    TextboxWidget getTextboxLayer();
    
    /**
     * Returns the layer that wraps around this layer in order to visually show that
     * the layer is selected
     * @return
     */
    Layer getSelectionLayer();
}
