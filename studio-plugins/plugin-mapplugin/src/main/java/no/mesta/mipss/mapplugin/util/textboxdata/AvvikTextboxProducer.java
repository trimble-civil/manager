package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.felt.FeltModule;
import no.mesta.mipss.felt.OpenDetailMessage;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.AvvikService;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.plugin.MipssPlugin;

import org.apache.commons.lang.CharUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AvvikTextboxProducer implements TextboxProducer{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private List<Avvik> pList;
	private List<String> avvikGuids;
	private IMipssStudioLoader loader;
	private boolean loaded = false;
	public AvvikTextboxProducer(IMipssStudioLoader loader, Avvik avvik){
		pList = new ArrayList<Avvik>();
		pList.add(avvik);
		this.loader = loader;
		loaded = true;
	}
	public AvvikTextboxProducer(IMipssStudioLoader loader, String avvikGuid){
		avvikGuids = new ArrayList<String>();
		avvikGuids.add(avvikGuid);
		this.loader = loader;
		loaded = false;
	}
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		log.debug("getTextboxData");
		final List<TextboxData> dataList = new ArrayList<TextboxData>();
		new Thread(){
			public void run(){
				if (!loaded){
					AvvikService bean = BeanUtil.lookup(AvvikService.BEAN_NAME, AvvikService.class);
					pList = new ArrayList<Avvik>();
					for (String guid:avvikGuids){
						Avvik a = bean.hentAvvik(guid);
						pList.add(a);
					}
					loaded = true;
				}
				int idx = 1;
				for (Avvik p:pList){
					TextboxData data = new TextboxData();
					data.setLoadingDone(false);
					data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.funn.title"));
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.id"), String.valueOf(p.getRefnummer()));
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.dato"), MipssDateFormatter.formatDate(p.getOpprettetDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.prosess"), p.getProsess()==null?"":p.getProsess().getTextForGUI());
					try{
						data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.stedfestning"), p.getProdHpGUIText());
					} catch (Exception e){
					}
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.status"), p.getStatus().getTextForGUI());
//					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.utfort"), p.getLukketDato()==null?"Nei":"Ja");
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.frist"), MipssDateFormatter.formatDate(p.getTiltaksDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.registrert"), p.getOpprettetAv());
					data.addData(ResourceUtil.getResource("mapPlugin.textbox.sak.id"),  String.valueOf(p.getSak().getRefnummer()));
					data.addData("Kommentar", TextBoxHelper.wordwrap(p.getBeskrivelse()));
					if (p.getBilde()!=null&&p.getBilde().getBildeLob()!=null){
						for (AvvikBilde b:p.getBilder()){
							data.addBilde(b.getBilde());
						}
					}
					data.setAction(getActionAvvik(p));
					idx++;
					dataList.add(data);
				}
				for (TextboxData d:dataList){
					d.setLoadingDone(true);
				}
			}

			
		}.start();
		return dataList;
	}
	
	public void addPunktreg(Avvik p){
		pList.add(p);
	}
	public void addAvvikGuid(String guid){
		avvikGuids.add(guid);
	}
	public int getAntallPunkter(){
		return pList.size();
	}

	public Action getActionAvvik(final Avvik p) {
		Action a = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {	
				MipssFieldDetailItem item = p;
				List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
				list.add(item);
				MipssPlugin plugin = loader.getPlugin(FeltModule.class);
				OpenDetailMessage m = new OpenDetailMessage(list);
				m.setTargetPlugin(plugin);
				loader.sendMessage(m);
			}
		};
		a.putValue("TEXT", ResourceUtil.getResource("textbox.open"));
		return a;
	}
}
