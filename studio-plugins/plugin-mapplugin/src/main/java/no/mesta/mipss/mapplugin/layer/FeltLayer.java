package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import javax.swing.ImageIcon;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.PinLayer.ORIENTATION;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.textboxdata.FunnTextboxProducer;
import no.mesta.mipss.mapplugin.util.textboxdata.PunktregOverviewTextboxProducer;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * Kopi av Pin layer tilpasset til å kun vise frem mipss-felt entiteter
 * @author harkul
 *
 */
public class FeltLayer extends Layer implements CollectionEntity{
	public enum REGISTRERINGSTYPE{
		AVVIK, 
		HENDELSE,
		FORSIKRINGSSKADE,
		SKRED
	}
	
	private REGISTRERINGSTYPE type;
	private GeoPosition pos;
	private LayerCollection parent;
	private ImageIcon image;
	private GeoPosition clickPos;
	private boolean merk;
	
	public FeltLayer(String id, boolean triggerRecenterAndZoom, REGISTRERINGSTYPE orientation, GeoPosition pos, boolean merk) {
		super(id, triggerRecenterAndZoom);
		this.type = orientation;
		this.pos = pos;
		this.merk = merk;
	}
	public void setImage(ImageIcon image){
		this.image = image;
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
		if (pos!=null){
			//last bilde første gang..
			if (image==null){
	        	switch(type){
	        		case AVVIK: image = IconResources.MAP_PIN_AVVIK; break;
	        		case HENDELSE: image = IconResources.MAP_PIN_HENDELSE; break;
	        		case FORSIKRINGSSKADE: image = IconResources.MAP_PIN_FORSIKRINGSSKADE; break;
	        		case SKRED: image = IconResources.MAP_PIN_SKRED; break;
	        	}
	        }
			
			g = super.initGraphics(g, map);
	        Point2D point = map.getTileFactory().geoToPixel(pos, map.getZoom());
	        Rectangle viewport = map.getViewportBounds();
	        viewport.height=viewport.height+image.getIconHeight();
	        viewport.x=viewport.x-image.getIconWidth();
	        viewport.width=viewport.width+image.getIconWidth();
	        if (viewport.contains(point)){
		        switch(type){
					case AVVIK:
						int x = (int)point.getX();
				        int y = (int)point.getY() - image.getIconHeight();
						clickPos = map.getTileFactory().pixelToGeo(new Point2D.Double(x+17, y+12), map.getZoom());
						g.drawImage(image.getImage(), x,y,null);
						g.setColor(Color.black);
						if (getTextboxProducer() instanceof FunnTextboxProducer){
							g.drawString(((FunnTextboxProducer)getTextboxProducer()).getAntallPunkter()+"", x+14, y+16);
						}
						if (getTextboxProducer() instanceof PunktregOverviewTextboxProducer){
							g.drawString(((PunktregOverviewTextboxProducer)getTextboxProducer()).getAntallPunkter()+"", x+14, y+16);
						}
						break;
					case HENDELSE: 
						x = (int)point.getX();
				        y = (int)point.getY() - image.getIconHeight();
						clickPos = map.getTileFactory().pixelToGeo(new Point2D.Double(x+36, y+12), map.getZoom());
						g.drawImage(image.getImage(), x,y,null);
						g.setColor(Color.white);
						g.drawString("R2", x+32, y+16);
						break;
					case FORSIKRINGSSKADE:
						x = (int)point.getX() - image.getIconWidth();
				        y = (int)point.getY() - image.getIconHeight();
						clickPos = map.getTileFactory().pixelToGeo(new Point2D.Double(x+12, y+11), map.getZoom());
						g.drawImage(image.getImage(), x,y,null);
						g.setColor(Color.white);
						g.drawString("R5", x+7, y+16);
						
						break;
					case SKRED:
						x = (int)point.getX() - image.getIconWidth();
				        y = (int)point.getY() - image.getIconHeight();
						clickPos = map.getTileFactory().pixelToGeo(new Point2D.Double(x+12, y+12), map.getZoom());
						g.drawImage(image.getImage(), x,y,null);
						g.setColor(Color.white);
						g.drawString("R11", x+4, y+16);
						break;
//					case CENTER:
//						x = (int)point.getX()-8;
//				        y = (int)point.getY() - image.getIconHeight();
//				        clickPos = null;
//				        g.drawImage(image.getImage(), x,y,null);
//						break;
		        }
		        if (merk){
		//        	g.drawRect((int)point.getX()-image.getIconWidth(), (int)point.getY()-image.getIconHeight(), image.getIconWidth(), image.getIconHeight());
		        }
	        }
		}
	}

	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius) {
//		searchRadius +=3;
		if (clickPos==null){
			return null;
		}
		if (clickPos.getLatitude()< gp.getLatitude()+searchRadius&&
			clickPos.getLatitude()> gp.getLatitude()-searchRadius&&
			clickPos.getLongitude()<gp.getLongitude()+searchRadius&&
			clickPos.getLongitude()>gp.getLongitude()-searchRadius){
            return this;
        }
        else{
            return null;
        }
	}

	@Override
	public LayerCollection getParent() {
		return parent;
	}

	@Override
	public void setParent(LayerCollection parent) {
		this.parent = parent;
	}

	@Override
	public Layer getSelectionLayer() {
		
		Layer layer = new Layer("pinlayerselection", false){
			public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
				ImageIcon orig = image;
				image = ImageUtil.toGrayScale(image);
				FeltLayer.this.paint(g,map,w,h);
				image = orig;
			}
		};
		layer.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
        return layer;
	}

	public void setPos(GeoPosition pos) {
		this.pos = pos;
	}
	
	public REGISTRERINGSTYPE getRegistreringstype() {
		return type;
	}
}
