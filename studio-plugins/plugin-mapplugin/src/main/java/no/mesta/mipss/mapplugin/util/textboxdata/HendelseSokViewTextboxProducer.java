package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.felt.FeltModule;
import no.mesta.mipss.felt.OpenDetailMessage;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.plugin.MipssPlugin;

public class HendelseSokViewTextboxProducer implements TextboxProducer{
	
	private Hendelse hendelse;
	private String hendelseGuid;
	private IMipssStudioLoader loader;
	
	public HendelseSokViewTextboxProducer(IMipssStudioLoader loader, Hendelse hendelse){
		this.hendelse = hendelse;
		this.loader = loader;
	}
	public HendelseSokViewTextboxProducer(IMipssStudioLoader loader, String hendelseGuid){
		this.hendelseGuid = hendelseGuid;
		this.loader = loader;
	}
	
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		final List<TextboxData> dataList = new ArrayList<TextboxData>();

		final TextboxData data = new TextboxData();
		data.setLoadingDone(false);
		new Thread(){
			public void run(){
				if (hendelse==null){
					RskjemaService bean = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
					hendelse = bean.hentHendelse(hendelseGuid);
				}
				data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.hendelse.title"));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.id"), String.valueOf(hendelse.getRefnummer()));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.dato"), MipssDateFormatter.formatDate(hendelse.getOwnedMipssEntity().getOpprettetDato(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.arsak"), hendelse.getAarsak()==null?" ":hendelse.getAarsak().getNavn());
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.tiltak"), hendelse.getTiltakstekster()==null?" ":hendelse.getTiltakstekster());
//				data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.hp"), hendelse.getSak().getS==null?" ":hendelse.getHpGuiText());
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.kommune"), hendelse.getSak().getKommune()==null?" ":hendelse.getSak().getKommune());
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.hendelse.skadested"), hendelse.getSak().getSkadested()==null?" ":hendelse.getSak().getSkadested());
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.funn.registrert"), hendelse.getOwnedMipssEntity().getOpprettetAv());
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.sak.id"),  String.valueOf(hendelse.getSak().getRefnummer()));
				data.addData("Kommentar", TextBoxHelper.wordwrap(hendelse.getBeskrivelse()));
				data.setAction(getAction());
				dataList.add(data);
				Integer antallBilder = hendelse.getAntallBilder();
				if (antallBilder != null && antallBilder!=0){
					if (hendelse.getBilder()!=null){
						for (Bilde b:hendelse.getBilder()){
							data.addBilde(b);
						}
					}
				}
				data.setLoadingDone(true);
			}
		}.start();
		
		
		return dataList;
	}

	public Action getAction() {
		Action a = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {	
				MipssFieldDetailItem item = hendelse;
				List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
				list.add(item);
				MipssPlugin plugin = loader.getPlugin(FeltModule.class);
				OpenDetailMessage m = new OpenDetailMessage(list);
				m.setTargetPlugin(plugin);
				loader.sendMessage(m);
			}
		};
		a.putValue("TEXT", ResourceUtil.getResource("textbox.open"));
		return a;
	}
}
