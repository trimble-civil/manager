package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.mipssfield.MipssFieldModule;
import no.mesta.mipss.mipssfield.OpenDetailMessage;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.plugin.MipssPlugin;

public class InspeksjonTextboxProducer implements TextboxProducer{
	
//	private Inspeksjon inspeksjon;
//	private MipssField bean;
	private InspeksjonSokResult view;
	private IMipssStudioLoader loader;
	private final String guid;
	
//	public InspeksjonTextboxProducer(IMipssStudioLoader loader, Inspeksjon inspeksjon){
//		this.loader = loader;
//		this.inspeksjon = inspeksjon;
//	}
	public InspeksjonTextboxProducer(IMipssStudioLoader loader, InspeksjonSokResult view, String guid){
		this.loader = loader;
		this.view = view;
		this.guid = guid;
	}
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
//		if (bean==null){
//			bean = BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class);
//		}
//		if (view==null)
//			view = bean.hentInspeksjonSok(inspeksjon.getGuid());
		
		
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		TextboxData info = new TextboxData();
        info.setTitle(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.title"));
        
        if (view==null){
			//TODO hent inspeksjon...
		}
        
        info.addData(ResourceUtil.getResource("inspeksjonview.id"), String.valueOf(view.getRefnummer()));
        info.addData(ResourceUtil.getResource("inspeksjonview.fra"), MipssDateFormatter.formatDate(view.getFraTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        info.addData(ResourceUtil.getResource("inspeksjonview.til"), MipssDateFormatter.formatDate(view.getTilTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        info.addData(ResourceUtil.getResource("inspeksjonview.etterslep"), view.getEtterslep()?"Ja":"Nei");
//        info.addData(ResourceUtil.getResource("inspeksjonview.antfunn"), view.getAntallAvvik()+"");
//        info.addData(ResourceUtil.getResource("inspeksjonview.anthendelse"), view.getAntallHendelser()+"");
        String prosesser = view.getProsessInspText();
        
        if (prosesser!=null&&prosesser.length()>40){
        	int c = prosesser.length()/40+1;
        	String s = prosesser.substring(0, 40);
        	info.addData(ResourceUtil.getResource("inspeksjonview.prosesser"), s);
        	
        	for (int i=1;i<c;i++){
        		int ix = i*40;
        		int idx = ix+40;
        		if (prosesser.length()<idx){
        			idx = prosesser.length();
        		}
        		info.addData("", prosesser.substring(ix, idx));
        	}
        }else{
        	info.addData(ResourceUtil.getResource("inspeksjonview.prosesser"), view.getProsessInspText());
        }
        
        info.addData(ResourceUtil.getResource("inspeksjonview.var"), view.getVaerNavn());
        info.addData(ResourceUtil.getResource("inspeksjonview.vind"), view.getVindNavn());
        info.addData(ResourceUtil.getResource("inspeksjonview.temp"), view.getTemperaturNavn());
        info.addData(ResourceUtil.getResource("inspeksjonview.utfortav"), view.getOpprettetAv());
        double tot = view.getKmEvInspisert()+view.getKmRvInspisert()+view.getKmFvInspisert()+view.getKmKvInspisert();
        info.addData(ResourceUtil.getResource("inspeksjonview.totv"), tot+"");
        info.addData(ResourceUtil.getResource("inspeksjonview.kmev"), view.getKmEvInspisert()+"");
        info.addData(ResourceUtil.getResource("inspeksjonview.kmrv"), view.getKmRvInspisert()+"");
        info.addData(ResourceUtil.getResource("inspeksjonview.kmfv"), view.getKmFvInspisert()+"");
        info.addData(ResourceUtil.getResource("inspeksjonview.kmkv"), view.getKmKvInspisert()+"");
        info.addData(ResourceUtil.getResource("inspeksjonview.pda"), view.getPdaDfuNavn());
//        info.setAction(getAction());
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.guid"), inspeksjon.getGuid());
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.antall"), funn+"");
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.etterslep"), etterslep?"Ja":"Nei");
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.prosesser"), getProsesserString(inspeksjon.getProsesser()));
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.vaer"), inspeksjon.getVaer().getTextForGUI());
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.vind"), inspeksjon.getVind().getTextForGUI());
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.temp"), inspeksjon.getTemperatur().getTextForGUI());
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.start"), MipssDateFormatter.formatDate(inspeksjon.getFraTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
//        info.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjonstr.stopp"), MipssDateFormatter.formatDate(inspeksjon.getTilTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        dataList.add(info);
        return dataList;
	}
	private String getProsesserString(List<Prosess> list){
		String s = "";
		if (list==null||list.size()==0){
			s+="Ingen";
		}
		for (Prosess p:list){
			s+=p.getProsessKode()+", ";
		}
		return s;
	}
//	private List<String> splitString(String string){
//		String[] st = string.split(",");
//		List<String> strings = new ArrayList<String>();
//		String temp = "";
//		for (int i=0;i<st.length;i++){
//			if ((i%3==0&&i!=0)||i==st.length-1){
//				strings.add(temp);
//				temp = "";
//			}
//			temp += st[i];
//		}
//		return strings;
//	}
	public Action getAction() {
		Action a = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {	
//				TODO HUSK DETTE!!
//				MipssFieldDetailItem item = view;
//				List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
//				list.add(item);
//				MipssPlugin plugin = loader.getPlugin(MipssFieldModule.class);
//				OpenDetailMessage m = new OpenDetailMessage(list);
//				m.setTargetPlugin(plugin);
//				loader.sendMessage(m);
			}
		};
		a.putValue("TEXT", ResourceUtil.getResource("textbox.open"));
		return a;
	}

}
