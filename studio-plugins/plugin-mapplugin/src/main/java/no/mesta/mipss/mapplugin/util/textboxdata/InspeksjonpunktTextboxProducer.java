package no.mesta.mipss.mapplugin.util.textboxdata;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.persistence.kvernetf1.Prodpunktinfo;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProduksjonService;

public class InspeksjonpunktTextboxProducer implements TextboxProducer{
	
	private ProdpunktVO prodpunkt;
	private final IMipssStudioLoader loader;
	private ProduksjonService prod;
	private Prodpunktinfo pi;
	public InspeksjonpunktTextboxProducer(IMipssStudioLoader loader, ProdpunktVO prodpunkt){
		this.loader = loader;
		this.prodpunkt=prodpunkt;
	}
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		
		if (prod==null){
			prod = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		}
		if (pi==null)
        	pi = prod.getProdpunktInfo(prodpunkt.getDfuId(), prodpunkt.getGmtTidspunkt());
		String vei = pi.getFylkesnummer()+"/"+pi.getKommunenummer()+" "+pi.getVeikategori()+pi.getVeistatus()+pi.getVeinummer()+" (hp:"+pi.getHp()+" meter:"+pi.getMeter()+")";
		if (pi.getMeter()==null)
			vei="";
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		String dfu = prodpunkt.getDfuId()+"";
		
        TextboxData data = new TextboxData();
        data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.inspeksjon.title"));
        data.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjon.id"), prodpunkt.getInspeksjonId());
        data.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjon.tidspunkt"), MipssDateFormatter.formatDate(prodpunkt.getCetTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        data.addData("Retning", prodpunkt.getRetning()+"");
        data.addData(ResourceUtil.getResource("mapPlugin.textbox.inspeksjon.dfu"), dfu);
        data.addData("Veiinfo", vei);
        data.addData("Produksjontype", "Inspeksjon");
        if (prodpunkt.getSnappetX()!=null&&prodpunkt.getSnappetY()!=null){
	        data.addData("Nord", prodpunkt.getSnappetY()+"");
	        data.addData(ResourceUtil.getResource("east"), prodpunkt.getSnappetX()+"");
        }else{
        	data.addData("Lengdegrad", prodpunkt.getGpsX()+"");
        	data.addData("Breddegrad", prodpunkt.getGpsY()+"");
        }
        
        dataList.add(data);
        return dataList;
    }
}
