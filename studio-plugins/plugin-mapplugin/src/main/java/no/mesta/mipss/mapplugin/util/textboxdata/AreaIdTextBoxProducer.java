package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import net.sf.json.JSONArray;
import net.sf.json.util.JSONUtils;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.util.ProxyUtil;
import no.mesta.mipss.webservice.NvdbWebservice;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;

public class AreaIdTextBoxProducer implements TextboxProducer{

	private final int[] indexes;
	private final int areaId;
	private final double[] bbox;
	private NvdbWebservice nvdb;

	public AreaIdTextBoxProducer(int[] indexes, int areaId, double[] bbox){
		this.indexes = indexes;
		this.areaId = areaId;
		this.bbox = bbox;
		nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
		
	}
	@Override
	public List<TextboxData> getTextboxData() {
		final List<TextboxData> dataList = new ArrayList<TextboxData>();
		TextboxData data = new TextboxData();
		data.setTitle("Area info");
		data.setLoadingDone(true);
		data.addData("East index", String.valueOf(indexes[0]));
		data.addData("North index", String.valueOf(indexes[1]));
		data.addData("Area id", String.valueOf(areaId));
		data.addData("Bbox", bbox[0]+", "+bbox[1]+", "+bbox[2]+", "+bbox[3]);
		data.setAction(getDataFromTrionaAction());
		dataList.add(data);
		
		return dataList;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}
	public Action getDataFromTrionaAction() {
		Action a = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
	    		DefaultHttpClient client = new DefaultHttpClient();
				HttpGet method = new HttpGet("http://lighthouse.triona.no/area/"+areaId);
				try {
					HttpResponse response = client.execute((HttpUriRequest) method);
					InputStream rstream = response.getEntity().getContent();
					String s = convertStreamToString(rstream);
					String[] split = StringUtils.split(s, "[");
					for (String st:split){
						st = "["+st;
					}
					s = StringUtils.join(split, "\n[");
//					JSONUtils.valueToString(value, indentFactor, indent)
					JDialog d = new JDialog();
					d.setTitle(""+areaId);
					JTextArea a = new JTextArea();
					a.setLineWrap(true);
					JScrollPane p = new JScrollPane(a);
					
					a.setText(s);
					d.add(p);
					d.setSize(400,400);
					d.setLocationRelativeTo(null);
					d.setVisible(true);
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		};
		a.putValue("TEXT", "Invoke");
		return a;
	}
	public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
