package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.felt.FeltModule;
import no.mesta.mipss.felt.OpenDetailMessage;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.ForsikringsskadeBilde;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.plugin.MipssPlugin;

public class ForsikringsskadeTextboxProducer implements TextboxProducer{

	private Forsikringsskade fs;
	private String forsikringsskadeGuid;
	private final IMipssStudioLoader loader;

	public ForsikringsskadeTextboxProducer(IMipssStudioLoader loader, Forsikringsskade fs){
		this.loader = loader;
		this.fs = fs;
	}
	public ForsikringsskadeTextboxProducer(IMipssStudioLoader loader, String forsikringsskadeGuid){
		this.loader = loader;
		this.forsikringsskadeGuid = forsikringsskadeGuid;
	}
	@Override
	public List<TextboxData> getTextboxData() {
		final List<TextboxData> dataList = new ArrayList<TextboxData>();

		final TextboxData data = new TextboxData();
		data.setLoadingDone(false);
		new Thread(){
			public void run(){
				if (fs==null){
					RskjemaService bean = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
					fs = bean.hentForsikringsskade(forsikringsskadeGuid);
				}
				data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.forsikringsskade.title"));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.forsikringsskade.id"), String.valueOf(fs.getRefnummer()));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.sak.id"), String.valueOf(fs.getSak().getRefnummer()));
				data.addData("Kommentar", TextBoxHelper.wordwrap(fs.getFritekst()));
				Integer antallBilder = fs.getAntallBilder();
				if (antallBilder != null && antallBilder!=0){
					if (fs.getBilder()!=null){
						for (ForsikringsskadeBilde b:fs.getBilder()){
							data.addBilde(b.getBilde());
						}
					}
				}
				data.setAction(getAction());
				dataList.add(data);
				data.setLoadingDone(true);
			}
		}.start();
		
		return dataList;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}
	
	public Action getAction() {
		Action a = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {	
				MipssFieldDetailItem item = fs;
				List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
				list.add(item);
				MipssPlugin plugin = loader.getPlugin(FeltModule.class);
				OpenDetailMessage m = new OpenDetailMessage(list);
				m.setTargetPlugin(plugin);
				loader.sendMessage(m);
			}
		};
		a.putValue("TEXT", ResourceUtil.getResource("textbox.open"));
		return a;
	}
}
