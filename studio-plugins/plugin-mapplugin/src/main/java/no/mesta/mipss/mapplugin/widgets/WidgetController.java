package no.mesta.mipss.mapplugin.widgets;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.MouseInputListener;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.painter.Painter;

public class WidgetController implements Painter<JXMapViewer>, MouseInputListener {

	private List<Widget> widgets = new ArrayList<Widget>();
	
	public WidgetController() {
		
	}
	
	public void addWidget(Widget widget){
		widgets.add(widget);
	}
	
	public void removeWidget(Widget widget){
		widgets.remove(widget);
	}
	
	public void removeWidgets(List<Widget> removeList){
		widgets.removeAll(removeList);
	}
	public List<Widget> getWidgets(){
		return widgets;
	}
	public List<Widget> getWidgetsOfClass(Class widget){
		List<Widget> list = new ArrayList<Widget>();
		for (Widget w:widgets){
			if (w.getClass().equals(widget)){
				list.add(w);
			}
		}
		return list;
	}
	public void paint(Graphics2D g, JXMapViewer map, int w, int h){
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			if (l.isVisible()){
				l.paint(g, map, w, h);
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void mouseClicked(MouseEvent e) {
		ArrayList<Widget> wi = (ArrayList<Widget>)((ArrayList<Widget>)widgets).clone();
		for (Widget l:wi){
			l.mouseClicked(e);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			l.mouseEntered(e);
			
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			l.mouseExited(e);
			
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			l.mousePressed(e);
			
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			l.mouseReleased(e);
			
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			l.mouseDragged(e);
			
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		Widget[] wid = widgets.toArray(new Widget[widgets.size()]);
		for (Widget l:wid){
			l.mouseMoved(e);
			
		}
	}
}
