package no.mesta.mipss.mapplugin.widgets;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import no.mesta.mipss.mapplugin.MapPanel;

import org.jdesktop.swingx.JXMapViewer;

public class ScaleWidget extends Widget{
	private String text = "";
	
	public ScaleWidget(MapPanel map, GLUE glue) {
		this(map, 300, 20, 10, glue);
	}
	
	public ScaleWidget(MapPanel map, int width, int height, int pad, GLUE glue) {
		super(map, width, height, pad, glue);
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer object, int w, int h) {
		copyState(g);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		initXY();
		Graphics2D g2 = (Graphics2D)g;
		g2.setColor(paneColor);
		g2.fillRoundRect(getX(), getY(), getWidth(), getHeight(), 5, 5);
		g2.setColor(borderColor);
		g2.drawRoundRect(getX(), getY(), getWidth(), getHeight(), 5, 5);
		g2.setStroke(new BasicStroke(1));
		g2.setColor(textColor.darker());
		try{
		int px = getPixelSpace();
		int nr = (getWidth()-50)/px;
		g2.drawString(text, getX()+10, getY()+13);
		for (int i=0;i<nr;i++){
			g2.drawLine(getX()+i*px+50, getY()+4, getX()+i*px+50, getY()+14);
		}
		} catch (Throwable t){
			t.printStackTrace();
		}
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		resetState(g);
	}
	public int getPixelSpace(){
		double scale = map.getResolutions().getResolutionForZoom(map.getZoom());
		int pxMil = (int)(100000 * (1/(scale)));//100km
		int pxKm = (int)(10000 * (1/scale));//10km
		int pxH = (int)(1000 * (1/scale));//1000m
		int pxT = (int)(100 * (1/scale));//100m
		int pxM = (int)(10 * (1/scale));//10m
		int px = (int)(1 * (1/scale));//1m
		if (scale<0){
			text = "1 m";
			return px;
		}
		if (scale<1){
			text = "10 m";
			return pxM;
		}
		if (scale<10){
			text = "100 m";
			return pxT;
		}
		if (scale<100){
			text = "1 Km";
			return pxH;
		}
		if (scale<600){
			text = "10 Km";
			return pxKm;
		}
		if (scale>500){
			text = "100 Km";
			return pxMil;
		}
		return -1;
	}
	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof ScaleWidget)){
			return false;
		}
		if (o==this){
			return true;
		}
		return false;
	}
}
