package no.mesta.mipss.mapplugin;

import no.mesta.mipss.mapplugin.utm.UTMTileFactoryInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WMSService for tilecache.
 * 
 * to get a tile we first need to calculate the UTM coordinates for the Tile from the 
 * screen coordinates, then create a WMS request
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class TileCacheWMSService extends UTMWMSService {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private UTMTileFactoryInfo info;
    private String layers;
    /**
     * Default constructor
     */
    public TileCacheWMSService(String layers) {
        this.layers = layers;
    }
    
    /**
     * Get the UTMTileFactoryInfo for the map server
     * @return
     */
    public UTMTileFactoryInfo getInfo() {
		return info;
	}
    
    /**
     * Sets the UTMTileFactoryInfo for the map server
     * @param info
     */
	public void setInfo(UTMTileFactoryInfo info) {
		this.info = info;
	}

	/**
     * Creates a WMS request for the tilecache server. 
     * @param x horizontal position for the tile
     * @param y vertical position for the tile
     * @param zoom the zoom level, used to find the resolution for the current zoom level 
     * @param tileSize the size of a Tile in pixels
     * @return a WMS request
     */
    public String toWMSURL(int x, int y, int zoom, int tileSize) {
        String format = "image/png";
        String srs = "EPSG:32633";
        String service = "WMS";
        String version = "1.1.1";
        String request = "GetMap";
        String exceptions = "application/vnd.ogc.se_inimage";

        if  (zoom>info.getResolutions().getMaxResolutionIndex())
            return null;
        double resolution = info.getResolutions().getResolutionForZoom(zoom);
        //ll, ur bounds for the map
        Bounds bounds = info.getBounds();
        //calculate ll and ur
        double llx = bounds.getLlx() + (resolution * x * tileSize);
        double lly = bounds.getLly() + (resolution * y * tileSize);
        double urx = bounds.getLlx() + (resolution * (x+1) * tileSize);
        double ury = bounds.getLly() + (resolution * (y+1) * tileSize);
        
        String bbox = llx + "," +lly+","+urx+","+ury;
        
        String url = getBaseUrl() +
        				"LAYERS="+layers+
        				"&FORMAT="+format+
        				"&SERVICE="+service+
        				"&VERSION="+version+
        				"&REQUEST="+request+
        				"&STYLES="+
        				"&EXCEPTIONS="+exceptions+
        				"&SRS="+srs+
        				"&BBOX="+bbox+
        				"&WIDTH="+tileSize+
        				"&HEIGHT="+tileSize+
                        "";
        return url;
    }
}
