package no.mesta.mipss.mapplugin.layer;

import java.util.List;

import javax.swing.Action;

import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;

/**
 * Defines the rules for a textbox producer, a producer is a class that creates
 * TextboxData object. 
 * 
 * @see TextboxData
 */
public interface TextboxProducer {
    List<TextboxData> getTextboxData();
    LayerType getPreferredLayerType();
}
