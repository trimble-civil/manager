package no.mesta.mipss.mapplugin.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;

import javax.swing.SwingUtilities;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class CompassWidget extends Widget{
	public enum Type{
		CLOSED,
		OPEN
	}
	private Polygon top;
	private Polygon bottom;
	private Polygon right;
	private Polygon left;
	private int triSize = 10;
	private final Type type;
	
	private Shape circle;
	
	private boolean topFilled;
	private boolean bottomFilled;
	private boolean rightFilled;
	private boolean leftFilled;
	private BasicStroke fat = new BasicStroke(4);
	private BasicStroke plain = new BasicStroke(2);
	private BasicStroke one = new BasicStroke(1);
	private RadialGradientPaint fill;
	
	private int groundSpeed;
	
	private int x =0;
	private int y =0;
	
	private boolean hidden=false;
	
	public CompassWidget(MapPanel map, int w, int h, int pad, Type type, Widget.GLUE location) {
		super(map, w, h, pad, location);
		this.type = type;
		setVisible(true);
	}
	
	@Override
	public void paint(Graphics2D g, JXMapViewer map, int width, int height) {
		initXY();
		copyState(g);
		circle = new Ellipse2D.Float(getX(), getY(), getWidth(), getHeight());
		fill = new RadialGradientPaint(getX()+(getWidth()/2), getY()+(getHeight()/2), getWidth()/2, new float[]{0.0f, 0.6f, 1.0f}, new Color[]{new Color(1, 1, 1, 0f), new Color(1, 1, 1, 0f), new Color(102,136,204)});
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		int cx = getX()+getWidth()/2;
		int cy = getY()+getHeight()/2;
		int pad = 5;
		
		g.setStroke(new BasicStroke(4));
		g.setColor(new Color(0.2f, 0.2f, 0.2f, 0.5f));
		g.drawOval(getX()+1, getY()+1, getWidth()-1, getHeight());
		g.setColor(paneColor);
		g.setStroke(one);
		g.fill(circle);
		
		if (x!=0&&y!=0){
			double ang = calcAngle(getX()+(getWidth()/2), getY()+(getHeight()/2), x, y);
			g.setPaint(fill);
			g.fillArc(getX(), getY(), getWidth(), getHeight(), (int)Math.toDegrees(ang) + 90 - 25, 50);
		}
		
		g.setColor(Color.lightGray);
		g.drawOval(getX(), getY(), getWidth(), getHeight());
		
		g.setColor(textColor);
		switch(type){
		case CLOSED:
			g.drawString("N", cx-g.getFontMetrics().charWidth('N')/2, getY()+pad+triSize+g.getFontMetrics().getHeight());
			break;
		case OPEN:
			g.drawLine(cx, getY()+pad, cx, getY()+getHeight()-pad);
			g.drawLine(getX()+pad, cy, getX()+getWidth()-pad, cy);
			break;
		}
		
		top = new Polygon();
		top.addPoint(cx+triSize, getY()+pad+triSize);
		top.addPoint(cx, getY()+pad);
		top.addPoint(cx-triSize, getY()+pad+triSize);

		
		bottom = new Polygon();
		bottom.addPoint(cx+triSize, getY()+getHeight()-pad-triSize);
		bottom.addPoint(cx, getY()+getHeight()-pad);
		bottom.addPoint(cx-triSize, getY()+getHeight()-pad-triSize);
		
		left = new Polygon();
		left.addPoint(getX()+pad+triSize, cy+triSize);
		left.addPoint(getX()+pad, cy);
		left.addPoint(getX()+pad+triSize, cy-triSize);

		
		right = new Polygon();
		right.addPoint(getX()+getWidth()-pad-triSize, cy-triSize);
		right.addPoint(getX()+getWidth()-pad, cy);
		right.addPoint(getX()+getWidth()-pad-triSize, cy+triSize);
		
		
		switch(type){
		case CLOSED:
			if (topFilled){
				g.fill(top);
			}else{
				g.draw(top);
			}
			if (bottomFilled){
				g.fill(bottom);
			}else{
				g.draw(bottom);
			}
			if (leftFilled){
				g.fill(left);
			}else{
				g.draw(left);
			}
			if (rightFilled){
				g.fill(right);
			}else{
				g.draw(right);
			}
		break;
		case OPEN:
			g.setStroke(getStroke(topFilled));
			g.drawPolyline(top.xpoints, top.ypoints, top.npoints);
			g.setStroke(getStroke(bottomFilled));
			g.drawPolyline(bottom.xpoints, bottom.ypoints, bottom.npoints);	
			g.setStroke(getStroke(leftFilled));
			g.drawPolyline(left.xpoints, left.ypoints, left.npoints);
			g.setStroke(getStroke(rightFilled));
			g.drawPolyline(right.xpoints, right.ypoints, right.npoints);
			break;
		}
		String txt = groundSpeed+" Km/t";
		int tw = g.getFontMetrics().stringWidth(txt);
		int tx = getX()+getWidth()/2-tw/2;
		int ty = getY()-10;
		
		g.setColor(paneColor);
		g.fillRect(tx, ty-11, tw, 12);
		g.setColor(Color.black);
		g.drawString(txt, tx, ty);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		resetState(g);
	}
	public BasicStroke getStroke(boolean b){
		if (b){
			return fat; 
		}return plain;
	}
	public double calcAngle(int x1, int y1, int x2, int y2){
         int x = x1-x2;
         int y = y1-y2;
         double rad = Math.atan2(x, y);
         return rad;
	}
	
	public double getSpeed(int x1, int y1, int x2, int y2){
		double x = Math.pow(x1-x2, 2);
		double y = Math.pow(y1-y2, 2);		
		double l = Math.sqrt(x+y);
		return l;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if (left.contains(e.getPoint())){
			GeoPosition pos = map.getTileFactory().pixelToGeo(map.getCenter(), map.getZoom());
			map.setAddressLocation(pos);
			double res = map.getResolutions().getResolutionForZoom(map.getZoom());
			double lon = pos.getLongitude()- (400*res);
			map.panTo(new MipssGeoPosition(pos.getLatitude(), lon), 400);
		}
		if (right.contains(e.getPoint())){
			GeoPosition pos = map.getTileFactory().pixelToGeo(map.getCenter(), map.getZoom());
			map.setAddressLocation(pos);
			double res = map.getResolutions().getResolutionForZoom(map.getZoom());
			double lon = pos.getLongitude()+ (400*res);
			map.panTo(new MipssGeoPosition(pos.getLatitude(), lon), 400);
		}
		if (bottom.contains(e.getPoint())){
			GeoPosition pos = map.getTileFactory().pixelToGeo(map.getCenter(), map.getZoom());
			map.setAddressLocation(pos);
			double res = map.getResolutions().getResolutionForZoom(map.getZoom());
			double lat = pos.getLatitude()- (400*res);
			map.panTo(new MipssGeoPosition(lat, pos.getLongitude()), 400);
		}
		if (top.contains(e.getPoint())){
			GeoPosition pos = map.getTileFactory().pixelToGeo(map.getCenter(), map.getZoom());
			map.setAddressLocation(pos);
			double res = map.getResolutions().getResolutionForZoom(map.getZoom());
			double lat = pos.getLatitude()+ (400*res);
			map.panTo(new MipssGeoPosition(lat, pos.getLongitude()), 400);
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if (!circle.contains(e.getPoint())&&!hidden){
			hidden = true;
			startAnimOn();
		}
		if(circle.contains(e.getPoint())&&hidden){
			hidden=false;
			startAnimOff();
		}
		boolean moved = false;
		if (left.contains(e.getPoint())){
			if (!leftFilled)
				moved = true;
			leftFilled=true;
		}else{
			if (leftFilled)
				moved=true;
			leftFilled=false;
		}
		if (right.contains(e.getPoint())){
			if (!rightFilled)
				moved=true;
			rightFilled=true;
		}else{
			if (rightFilled)
				moved = true;
			rightFilled=false;
		}
		if (top.contains(e.getPoint())){
			if (!topFilled)
				moved = true;
			topFilled=true;
		}else{
			if (topFilled)
				moved=true;
			topFilled=false;
		}
		if (bottom.contains(e.getPoint())){
			if (!bottomFilled)
				moved = true;
			bottomFilled=true;
		}else{
			if (bottomFilled)
				moved = true;
			bottomFilled=false;
		}
		if (moved)
			map.repaint();
		
	}
	
	private boolean sticky = false;
	@Override
	public void mouseDragged(MouseEvent e){
		if (circle.contains(e.getPoint()) || sticky) {
			if (SwingUtilities.isLeftMouseButton(e)) {
				map.getMouseListenerManager().invalidateAllBut(null);
				x = e.getX();
				y = e.getY();
				getMap().repaint();
				sticky = true;
				
				int x1 = getX()+getWidth()/2;
				int y1 = getY()+getHeight()/2;
				double speed = getSpeed(x1, y1, x, y);
				double ang = calcAngle(x1, y1, x, y);
				groundSpeed = (int)getMap().moveInDirection(speed, ang);
				e.consume();
			}
		}
	}
	@Override
	public void mouseReleased(MouseEvent e){
		sticky = false;
		map.getMouseListenerManager().reset();
		x=0;
		y=0;
		groundSpeed=0;
		getMap().repaint();
		getMap().endMoveInDirection();
	}
	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof CompassWidget)){
			return false;
		}
		if (o==this){
			return true;
		}
		return false;
	}
}
