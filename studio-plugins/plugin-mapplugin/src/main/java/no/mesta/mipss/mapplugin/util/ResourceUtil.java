package no.mesta.mipss.mapplugin.util;


import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class ResourceUtil {

	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "mapPluginText";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
}
