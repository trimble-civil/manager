package no.mesta.mipss.mapplugin.util.textboxdata;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;

/**
 * Lager en textboks for kontraktveinett
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class KontraktveiTextboxProducer implements TextboxProducer{

	private Driftkontrakt kontrakt;
	private String veinettInfo;
	private String veinettInfoValue;
	
	public KontraktveiTextboxProducer (Driftkontrakt kontrakt){
		this.kontrakt = kontrakt;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		TextboxData info = new TextboxData();
        info.setTitle("Kontraktsveinett");
        info.addData("Kontrakt", kontrakt.getKontraktnummer()+" - "+kontrakt.getKontraktnavn());
        if (veinettInfo!=null){
        	info.addData(veinettInfo, veinettInfoValue);
        }
        info.addData("Gyldig fra", MipssDateFormatter.formatDate(kontrakt.getGyldigFraDato(), MipssDateFormatter.DATE_TIME_FORMAT));
        info.addData("Gyldig til", MipssDateFormatter.formatDate(kontrakt.getGyldigTilDato(), MipssDateFormatter.DATE_TIME_FORMAT));
        info.addData("Opprettet", MipssDateFormatter.formatDate(kontrakt.getOpprettetDato(), MipssDateFormatter.DATE_TIME_FORMAT));
        MipssMap mipss = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
        Double veinettLengde = mipss.getVeinettLengde(kontrakt.getVeinettId());
        DecimalFormat format = new DecimalFormat("####.00");
        info.addData("Lengde", format.format(veinettLengde)+" km");
		dataList.add(info);
        return dataList;
	}
	public void setVeinettInfo(String veinettInfo){
		this.veinettInfo = veinettInfo;
	}
	public void setVeinettInfoValue(String veinettInfoValue){
		this.veinettInfoValue = veinettInfoValue;
	}
}
