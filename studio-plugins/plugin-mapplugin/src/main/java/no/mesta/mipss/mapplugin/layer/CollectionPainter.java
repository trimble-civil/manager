package no.mesta.mipss.mapplugin.layer;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.painter.Painter;

/**
 * A Painter for  CollectionLayers, 
 * 
 * Implements an algorithm for painting vectors or images. Usually we want
 * to generate images and move the generated image when the user pans the map. 
 * This is generally faster and requires less CPU. 
 * If the image is too large, we will get an OutOfMemoryError, if that happens we have to fail-safe
 * and paint the vectors. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */

public class CollectionPainter<T extends JXMapViewer> implements Painter<T>, Serializable{
    private transient Logger log = LoggerFactory.getLogger(this.getClass());
    private List<Layer> layers;
    private LayerCollection parentLayer;
    private boolean drawVector;
    
    private BufferedImage image;
    private int zoom = -1;
    //private Rectangle viewport;
    private boolean vector = false;
    private boolean error;
    
    /**
     * Create a new CollectionPainter
     * @param layers
     * @param parentLayer
     */
    public CollectionPainter(List<Layer> layers, LayerCollection parentLayer) {
        this.layers = layers;
        this.parentLayer = parentLayer;   
    }
    
    public List<Layer> getLayers(){
    	return layers;
    }
    /**
     * Paint the layers
     * @param g
     * @param map
     * @param w
     * @param h
     */
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        double res = ((MapPanel)map).getResolutions().getResolutionForZoom(map.getZoom());
        int width = (int)(parentLayer.getBounds().getWidth()/res);
        int height = (int)(parentLayer.getBounds().getHeight()/res);
        
        Rectangle viewport = map.getViewportBounds();
        if (viewport.getWidth()*3<width || viewport.getHeight()*3<height){
            drawVector = true;
            if (image!=null){
            	image.flush();
            	image = null;
            }
        }
        
        if (vector!=drawVector){
            log.debug("painting with vectors:"+!vector+" "+parentLayer);
        }
        vector = drawVector;
        if (!drawVector){
        	drawVector = paintImage(g, map, w, h, width, height, viewport);
        }
        
        if (drawVector){
        	paintVector(g, map, w, h);
        }
    }
    
    /**
     * Paint the layers as images. 
     * @param g graphics context
     * @param map the map
     * @param w width
     * @param h height 
     * @param width parentlayers width
     * @param height parentlayers height
     * @param viewport viewport bounds
     * @return
     */
    public boolean paintImage(Graphics2D g, JXMapViewer map, int w, int h, int width, int height, Rectangle viewport){
    	MipssGeoPosition ul = new MipssGeoPosition(parentLayer.getBounds().getUry(), parentLayer.getBounds().getLlx());
        Point2D p = map.getTileFactory().geoToPixel(new GeoPosition(ul.getLatitude(), ul.getLongitude()), map.getZoom());
    	//repaint the layer to an image when something has changed
        if (zoom!=map.getZoom() || parentLayer.isChanged()){
        	g = parentLayer.initGraphics(g, map);
            zoom = map.getZoom();
            parentLayer.setChanged(false);
                 
            //create an image with the same size as the layer on the current resolution
            //good for high zoom-levels, bad for low ones...(OutOfMemory)
            try{
                if (image!=null)
                	image.flush();
            	image = new BufferedImage(width==0?1:width+20, //image width can not be <= 0
                                          height==0?1:height+20, //image height can not be <= 0
                                      BufferedImage.TYPE_INT_ARGB);
            	error = false;
            } catch (OutOfMemoryError e){
                drawVector = true;
                error = true;
                
                log.error("OUT OF MEMORY!!!, attempting to draw vectors instead...");
                return true;
            } 
            if (!drawVector){
            	
                //translate the graphics to match the upper left corner of the layer
                Graphics2D ig = (Graphics2D)image.getGraphics();
                ig.translate(-viewport.x + (viewport.x - p.getX()), 
                             -viewport.y + (viewport.y - p.getY()));
                
                for(Layer l:layers){
                	l.setDrawOutsideViewport(true);
                	if (l.isVisible())
                		l.paint(ig, map, w, h);
                }
                //clipping rectangle for the image
                Rectangle clip = new Rectangle((int)(viewport.x + (p.getX() - viewport.x)), 
                                               (int)(viewport.y + (p.getY() - viewport.y)), 
                                               width+20, 
                                               height+20);
                //only paint if the image is visible on screen
                if (g.getClipBounds().intersects(clip)){
                    //draw the image with the same distance from the upper left corner as the layer
                    g.drawImage(image, clip.x, clip.y, map);
                }
            }
        }
        
        //the only thing changed is the location on screen
        else{
        	//clipping rectangle for the layer
            Rectangle clip = new Rectangle((viewport.x - viewport.x) + (int)(p.getX() - viewport.x), 
                                           (viewport.y - viewport.y) + (int)(p.getY()  -viewport.y), 
                                           image.getWidth(map), 
                                           image.getHeight(map));
            //only paint if the image is visible on screen
            if (g.getClipBounds().intersects(clip)){
                g.drawImage(image, clip.x, clip.y, map);
            }
        }
        return false;
    }
    /**
     * Paint the layers as vectors. 
     * @param g graphics context
     * @param map the map
     * @param w width
     * @param h height 
     * @return
     */
    public void paintVector(Graphics2D g, JXMapViewer map, int w, int h){
    	g = parentLayer.initGraphics(g, map);
        Iterator<Layer> it = layers.iterator();
        while (it.hasNext()){
            Layer l = it.next();
            l.setDrawOutsideViewport(false);
            if (l.isVisible()){
                l.paint(g, map, w, h);
            }
        }
        drawVector = false;
        zoom = map.getZoom();
    }
}
