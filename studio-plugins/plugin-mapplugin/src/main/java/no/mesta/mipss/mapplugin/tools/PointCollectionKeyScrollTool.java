package no.mesta.mipss.mapplugin.tools;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.LayerCollection;
import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mapplugin.layer.PointCollectionLayer;
import no.mesta.mipss.mapplugin.layer.PointLayer;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.SelectableLayer;
import no.mesta.mipss.mapplugin.util.LayerEvent;
import no.mesta.mipss.mapplugin.util.LayerListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PointCollectionKeyScrollTool implements KeyListener, LayerListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private LayerCollection selectedLayer;
    private int selectedIndex = 0;
    private MapPanel mapPanel;
    
    public PointCollectionKeyScrollTool(MapPanel mapPanel) {
        this.mapPanel = mapPanel;
    }

    public void keyTyped(KeyEvent e) {
        
    }
    
    private void first(){
        selectedIndex = 0;
        
    }
    private void last(){
        selectedIndex = selectedLayer.getLayers().size()-1;
       
    }
    private void next(){
        if (selectedIndex+1<selectedLayer.getLayers().size())
            selectedIndex++;
       
    }
    
    private void previous(){
        if (selectedIndex!=0)
            selectedIndex--;
    }
    
    private void select(){
        mapPanel.getLayerHandler().selectLayer((SelectableLayer)selectedLayer.getLayers().get(selectedIndex));
        mapPanel.ensureVisibilityForPosition(((PointLayer)selectedLayer.getLayers().get(selectedIndex)).getPoint(), 1000);
    }
    
    public void keyPressed(KeyEvent e) {
        if (selectedLayer!=null){
            switch (e.getKeyCode()) {
                case KeyEvent.VK_LEFT: 
                    previous();
                    select();
                    break;
                case KeyEvent.VK_RIGHT: 
                    next();
                    select();
                    break;
                case KeyEvent.VK_UP: 
                    next();
                    select();
                    break;
                case KeyEvent.VK_DOWN: 
                    previous();
                    select();
                    break;
                case KeyEvent.VK_HOME:
                    first();
                    select();
                    break;
                case KeyEvent.VK_END:
                    last();
                    select();
                    break;
                case KeyEvent.VK_PAGE_UP:
                    for (int i=0;i<10;i++){
                        next();
                    }
                    select();
                    break;
                case KeyEvent.VK_PAGE_DOWN:
                    for (int i=0;i<10;i++){
                        previous();
                    }
                    select();
                    break;
            }
        }
        
    }

    public void keyReleased(KeyEvent e) {
    }


    public void layerChanged(LayerEvent e) {
        if (e.getType()==LayerEvent.TYPE_SELECTED){
            LayerHandler h = (LayerHandler)e.getSource();
            if (h.getCurrentSelected() instanceof PointLayer){
                PointLayer p = (PointLayer)h.getCurrentSelected();
                if (p.getParent()!=null){
                    selectedLayer=p.getParent();
                    int index = selectedLayer.getIndexOfLayer(p);
                    if (index!=-1)
                        selectedIndex = index;
                }
            }
        }
    }
}
