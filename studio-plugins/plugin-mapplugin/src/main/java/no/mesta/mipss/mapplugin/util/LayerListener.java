package no.mesta.mipss.mapplugin.util;


import java.util.EventListener;

/**
 * Lytterinterface for når et lag endres, legges til eller slettes.
 * 
 * @author Harald A. Kulø
 */
public interface LayerListener extends EventListener{
    
    /**
     * Et Layer ble endret
     * @param e
     */
    void layerChanged(LayerEvent e);

}
