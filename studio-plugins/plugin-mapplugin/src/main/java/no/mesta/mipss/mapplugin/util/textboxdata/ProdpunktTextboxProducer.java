package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chart.messages.OpenSingleChartMessage;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.tools.JFreeChartTool;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kvernetf1.Prodpunktinfo;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProdtypeVO;
import no.mesta.mipss.service.produksjon.ProduksjonService;

/**
 * Lager en textboks for prodpunkter
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProdpunktTextboxProducer implements TextboxProducer {
	
	private ProdpunktVO prodpunkt;
	private MipssMap bean;
	private ProduksjonService prod;
	private Prodpunktinfo pi;
	private Date serieFra;
	private Date serieTil;
	private final IMipssStudioLoader loader;
	private final MapPanel map;
	private boolean skjulId;
	
	public ProdpunktTextboxProducer(IMipssStudioLoader loader, MapPanel map, ProdpunktVO prodpunkt, Date serieFra, Date serieTil){
		this.loader = loader;
		this.map = map;
		this.prodpunkt=prodpunkt;
		this.serieFra = serieFra;
		this.serieTil = serieTil;
	}
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}
	
	public void setSerieFra(Date serieFra){
		this.serieFra = serieFra;
	}
	public void setSerieTil(Date serieTil){
		this.serieTil = serieTil;
	}

	@Override
	public List<TextboxData> getTextboxData() {
		if (bean==null||prod==null){
			bean = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
			prod = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		}
		
		List<TextboxData> dataList = new ArrayList<TextboxData>();
        TextboxData data = new TextboxData();
        data.setTitle("Prodpunktinfo");
        String utstyr="";
        String bitnr="";
        String prodtype="";
        
        if (prodpunkt.getProdtypeList()!=null){
	        for (ProdtypeVO pt:prodpunkt.getProdtypeList()){
	        	utstyr+=pt.getUtstyrsnavn()+" ";
	        	if (pt.getBitNr()!=null)
	        		bitnr +=pt.getBitNr().toString()+" ";
	        	
	        	prodtype+=pt.getProdtype().getNavn()+" ";
	        }
        }
        if (pi==null){
//        	long start = System.currentTimeMillis();
        	pi = prod.getProdpunktInfo(prodpunkt.getDfuId(), prodpunkt.getGmtTidspunkt());
        }
        if (pi!=null){
	        String vei = pi.getFylkesnummer()+"/"+pi.getKommunenummer()+" "+pi.getVeikategori()+pi.getVeistatus()+pi.getVeinummer()+" (hp:"+pi.getHp()+" meter:"+pi.getMeter()+")";
	        if (pi.getMeter()==null)
	        	vei="";
			
			data.addData("Tidspunkt", MipssDateFormatter.formatDate(pi.getCetTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
	        data.addData("Hastighet", prodpunkt.getHastighet()+"");
	        Kjoretoy k = bean.getKjoretoyFromDfuAndTime(pi.getDfuId(), pi.getCetTidspunkt());
	        if (k!=null){
	        	if (!skjulId){
		        	data.addData(ResourceUtil.getResource("prodpunkt.textbox.kjoretoy"), k.getEksterntNavn());
		        	String eier = k.getKjoretoyeier()!=null?k.getKjoretoyeier().getNavn():"Ukjent";
		        	data.addData("Eier", eier);
	        	}
	        }else{
	        	//PDA..
	        	data.addData("PDA-Id", pi.getDfuId()+"");
	        }
	        
	        data.addData("Utstyr", utstyr);
	        data.addData("Produksjontype", prodtype);
	        data.addData("Veiinfo", vei);
	        data.addData("Retning", prodpunkt.getRetning()+"");
	        if (prodpunkt.getSnappetX()!=null&&prodpunkt.getSnappetY()!=null){
		        data.addData("Nord", prodpunkt.getSnappetY()+"");
		        data.addData(ResourceUtil.getResource("east"), prodpunkt.getSnappetX()+"");
	        }else{
	        	data.addData("Lengdegrad", prodpunkt.getGpsX()+"");
	        	data.addData("Breddegrad", prodpunkt.getGpsY()+"");
	        }
	        data.addData("Digi", prodpunkt.getDigiAndMask());
	        if (prodpunkt.getPdop()!=null)
	        	data.addData("PDOP", ""+prodpunkt.getPdop());
        
	        if (k!=null){
	        	data.setAction(getAction(k));
	        }
        }else{
        	data.setTitle("Prodpunkt");
        	data.addData("Tidspunkt", MipssDateFormatter.formatDate(prodpunkt.getCetTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        	data.addData("Hastighet", prodpunkt.getHastighet()+"");
        	data.addData("DFU ID", prodpunkt.getDfuId()+"");
        	data.addData("Retning", prodpunkt.getRetning()+"");
        	data.addData("Lengdegrad", prodpunkt.getGpsX()+"");
        	data.addData("Breddegrad", prodpunkt.getGpsY()+"");
        	data.addData("Digi", prodpunkt.getDigiAndMask());
        	if (prodpunkt.getPdop()!=null)
        		data.addData("PDOP", prodpunkt.getPdop()+"");
        }
        dataList.add(data);
        return dataList;
    }
	
	private Action getAction(final Kjoretoy k){
		Action a = new AbstractAction(){
			
			@Override
			public void actionPerformed(ActionEvent e) {	
				MipssPlugin plugin = loader.getPlugin(ChartModule.class);
				JFreeChartTool selectTool = map.getJFreeChartTool();
				OpenSingleChartMessage m = new OpenSingleChartMessage(k.getId(), serieFra, serieTil, selectTool);
				m.setTargetPlugin(plugin);
				loader.sendMessage(m);
			}
		};
		
		//Sørger for at de som ikke har tilgang til grafmodulen ikke får opp valget "Vis graf"
		MipssPlugin plugin = loader.getPlugin(ChartModule.class);
		if (plugin == null) {
			return null;
		} else{
			a.putValue("TEXT", ResourceUtil.getResource("textbox.graf"));
			return a;
		}		
	}
	
	public void setSkjulId(boolean skjulId) {
		this.skjulId = skjulId;
	}
	public boolean isSkjulId() {
		return skjulId;
	}
}
