package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.mapplugin.widgets.Widget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.painter.Painter;

/**
 * This class is the superclass for all layers in the map
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public abstract class Layer implements Painter<JXMapViewer>, Comparable, Serializable {
    private boolean antialias = false;
    private Bounds bounds;
    
	private boolean changed;
    private Color color = Color.yellow;
    private boolean drawOutsideViewport = false;
    private String id;
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MapPanel mapPanel;
    private LayerType preferredLayerType;
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    private TextboxWidget textBoxLayer;
    private TextboxProducer textboxProducer;
    private boolean triggerRecenterAndZoom = false;

	private boolean visible = true;
    
    /**
     * Create a new layer
     * @param id the programmatic id for the layer
     * @param triggerRecenterAndZoom true if this layer (when added to the map) will
     *        trigger a recenter and zoom operation
     */
    public Layer(String id, boolean triggerRecenterAndZoom){
        this.id = id;
        this.triggerRecenterAndZoom = triggerRecenterAndZoom;
    }
    
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + l + ")");
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        log.debug("addPropertyChangeListener(" + propName + ", " + l + ")");
        props.addPropertyChangeListener(propName, l);
    }
    /**
     * Compare this layer to another layer
     * @param o
     * @return true if this layer.id = other.id
     */
    public int compareTo(Object o) {
        if (o instanceof Layer){
            return ((Layer)o).getId().compareTo(getId());
        }
        return -1;
    }
    public boolean equals(Object o){
        if (o instanceof Layer){
            return ((Layer)o).getId().equals(getId());
        }
        return false;
    }
    
    /**
     * Returns the layers bounding box
     * @return
     */
    public Bounds getBounds() {
        return bounds;
    }
    
    /**
     * Returns the layers color
     * @return
     */
    public Color getColor() {
        return color;
    }    
    
    public void setId(String id){
    	this.id = id;
    }
    /**
     * Returns the programmatic id for the layer
     * @return
     */
    public String getId() {
        return id;
    }
    
    /**
     * Returns the preferred layer type. By giving the
     * Layer a preferred layer type you indicate which LayerType
     * this layer prefers to be to the layer handler. It's up to 
     * the layer handler to decide what to do with that.
     * @return
     */
    public LayerType getPreferredLayerType() {
		return preferredLayerType;
	}
    
    public TextboxWidget getTextboxLayer(){
		if (textBoxLayer==null){
			if (getTextboxProducer()!=null){
				textBoxLayer = new TextboxWidget(mapPanel, 10, Widget.GLUE.TOP_LEFT, getTextboxProducer().getTextboxData());
				textBoxLayer.setPreferredLayerType(getTextboxProducer().getPreferredLayerType());
			}
		}
		return textBoxLayer;
	}
    
    /**
     * Returns the TextboxProducer for this layer
     * @return
     */
    public TextboxProducer getTextboxProducer() {
        return textboxProducer;
    }
    public int hashCode(){
        return getId().hashCode();
    }
    
    /**
     * Initialize the graphics context. Returns a Graphics2D object that is translated
     * to the current viewport of the map, and the rendering hint for antialias sat. 
     * 
     * @param g
     * @param map
     * @return
     */
    public Graphics2D initGraphics(Graphics2D g, JXMapViewer map){
        g = (Graphics2D)g.create();
        //convert from viewport to world bitmap
        Rectangle rect = map.getViewportBounds();
        g.translate(-rect.x, -rect.y);
        if (!antialias)
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        else
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        return g;
    }
    public boolean isAntialias() {
        return antialias;
    }
    
    /**
     * Returns true is something in this layer has changed
     * @return
     */
    public boolean isChanged() {
        return changed;
    }
    
    //public abstract String getInfo(GeoPosition point);
    
    /**
     * Returns true if this layer should be painted even if it is outside the visible viewport,
     * false otherwise
     * @return
     */
    public boolean isDrawOutsideViewport() {
        return drawOutsideViewport;
    }
    /**
     * Returns true if this layer should trigger a recenter and zoom event when added to the map
     * @return
     */
    public boolean isTriggerRecenterAndZoom() {
        return triggerRecenterAndZoom;
    }
    
    /**
     * Returns the visibility of the layer
     * @return
     */
    public boolean isVisible() {
        return visible;
    }
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + l + ")");
        props.removePropertyChangeListener(l);
    }
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        log.debug("removePropertyChangeListener(" + propName + ", " + l + ")");
        props.removePropertyChangeListener(propName, l);
    }
    /**
     * Set this layer to be drawn anti-aliased
     * @param antialias
     */
    public void setAntialias(boolean antialias) {
        this.antialias = antialias;
    }

    /**
     * Set the layers bounding box. The layers bounding box the geospatial area that this layer covers
     * @param bounds
     */
    public void setBounds(Bounds bounds){
        this.bounds = bounds;
    }

    /**
     * Indicate that something in this layer has changed
     * @param changed
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }
    /**
     * Set this layer's color
     * @param color
     */
    public void setColor(Color color) {
        changed=true;
        this.color = color;
    }
    /**
     * Set to true if this layer should be painted even if it is outside the visible viewport default = false
     * @param drawOutsideViewport
     */
    public void setDrawOutsideViewport(boolean drawOutsideViewport) {
        this.drawOutsideViewport = drawOutsideViewport;
    }
	
	/**
     * Set the map panel
     * @param panel
     */
    public void setMapPanel(MapPanel mapPanel){
    	this.mapPanel = mapPanel;
    }
	/**
     * Sets the preferred LayerType for this Layer.
     * @param preferredLayerType
     */
	public void setPreferredLayerType(LayerType preferredLayerType) {
		this.preferredLayerType = preferredLayerType;
	}
	
    public void setTextboxLayer(TextboxWidget textBoxLayer){
		this.textBoxLayer = textBoxLayer;
		
	}

    /**
     * Set a textbox producer, this is the object that generate a textbox for
     * the layer when a user clicks on the layer
     * @param textboxProducer
     */
    public void setTextboxProducer(TextboxProducer textboxProducer) {
        this.textboxProducer = textboxProducer;
    }

    /**
     * Set the trigger value for the layer
     * @param triggerRecenterAndZoom
     */
    public void setTriggerRecenterAndZoom(boolean triggerRecenterAndZoom) {
        this.triggerRecenterAndZoom = triggerRecenterAndZoom;
    }

    /**
     * Set the visibility of the layer
     * @param visible
     */
    public void setVisible(boolean visible) {
        changed = isVisible()!=visible;
        if (changed&&mapPanel!=null)
        	mapPanel.repaint();
        this.visible = visible;
        
    }
}
