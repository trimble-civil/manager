package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Iterator;
import java.util.Set;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * A collection of GeoPositions to create a series of waypoints on a map with the same visual appearances.
 * 
 * @author Harald A. Kulø
 */
public class PointCollectionLayer extends Layer{
    private Set<MipssGeoPosition> points;
    
    private transient Logger log = LoggerFactory.getLogger(this.getClass());
   
    public PointCollectionLayer(String id, Set<MipssGeoPosition>points, Color color, Bounds bounds, boolean triggerRecenterAndZoom) {
        super(id, triggerRecenterAndZoom);
        this.points = points;
        setColor(color);
        setBounds(bounds);
    }
    
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = super.initGraphics(g, map);
        
        Iterator<MipssGeoPosition> it = points.iterator();
        while (it.hasNext()){
            MipssGeoPosition gp = it.next();         
            java.awt.geom.Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(gp.getLatitude(), gp.getLongitude()), map.getZoom());
            int x = (int)pt1.getX();
            int y = (int)pt1.getY();
            g.fillOval(x,y, LayerConstants.DEFAULT_POINT_DIAMETER, LayerConstants.DEFAULT_POINT_DIAMETER);
        }
    }
}
