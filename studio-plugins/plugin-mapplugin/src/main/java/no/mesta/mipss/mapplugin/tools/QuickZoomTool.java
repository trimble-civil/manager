package no.mesta.mipss.mapplugin.tools;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.MouseEvent;

import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tool for zooming a portion of the map
 *
 * @author Harald A. Kulø
 */
public class QuickZoomTool implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private QuickZoomDialog dialog;
    
    /**
     * Creates a new instance of QuickZoomTool
     * @param dialog the zoom dialog that displays the results
     */
    public QuickZoomTool(QuickZoomDialog dialog) {
        this.dialog = dialog;    
    }

    public void mouseClicked(MouseEvent e) {}
    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseDragged(MouseEvent e) {}
    
    public void mouseMoved(MouseEvent e) {
        if (e.getModifiers()!=MouseEvent.CTRL_MASK){
            Robot r=null;
            try {
                r = new Robot();
            } catch (AWTException f) {
            }
            int zoom = (dialog.getWidth()*2)/dialog.getZoom();
            
            Point p = new Point(e.getPoint().x-zoom/2, e.getPoint().y-zoom/2);
            
            SwingUtilities.convertPointToScreen(p, ((Component)e.getSource()));
            BufferedImage image = r.createScreenCapture(new Rectangle(p.x, p.y, zoom,zoom));
            dialog.setZoomImage(image);
        }
    }
}
