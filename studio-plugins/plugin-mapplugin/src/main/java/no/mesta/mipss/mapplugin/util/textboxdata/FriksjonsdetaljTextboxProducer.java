package no.mesta.mipss.mapplugin.util.textboxdata;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.service.produksjon.ProdpunktVO;

public class FriksjonsdetaljTextboxProducer implements TextboxProducer {

	private final Friksjonsdetalj f;

	public FriksjonsdetaljTextboxProducer(Friksjonsdetalj f){
		this.f = f;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		
		TextboxData data = new TextboxData();
		data.setTitle("Friksjonsdetalj");
		data.addData("Dato", f.getDato());
		data.addData("Kl", f.getKl());
		data.addData("Regnr", f.getRegnr());
		data.addData(ResourceUtil.getResource("maalertype"), f.getMaalertype());
		data.addData("Veiinfo", f.getVegnrhp());
		data.addData("Retning", f.getRetning());
		data.addData("Friksjon", f.getFriksjon()+"");
		data.addData("Temp", f.getTemperatur()+"");
		data.addData(ResourceUtil.getResource("nedbor"), f.getNedbor()!=null?f.getNedbor().getNavn():"Ukjent");
		data.addData(ResourceUtil.getResource("fore"), f.getFore()!=null?f.getFore().getNavn():"Ukjent");
		data.addData("Start tiltak", f.getStartTiltak()==null?"":f.getStartTiltak());
		data.addData(ResourceUtil.getResource("fulftilt"), f.getFullfortTiltak()==null?"":f.getFullfortTiltak());
		data.addData("Hvem", f.getUtfortAv());
		data.addData("Merknader", f.getBeskrivelse());
		dataList.add(data);
		
		return dataList; 
	}
}
