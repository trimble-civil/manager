package no.mesta.mipss.mapplugin;

import no.mesta.mipss.mapplugin.utm.UTMTileFactoryInfo;

import org.jdesktop.swingx.mapviewer.wms.WMSService;

/**
 * All WMSServices to be used in the MapPanel must extend this class
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public abstract class UTMWMSService extends WMSService{

    /**
     * Get the UTMTileFactoryInfo for the map server
     * @return
     */
    public abstract UTMTileFactoryInfo getInfo();
    
    /**
     * Sets the UTMTileFactoryInfo for the map server
     * @param info
     */
	public abstract void setInfo(UTMTileFactoryInfo info);
}
