package no.mesta.mipss.mapplugin;

import java.awt.Point;
import java.io.Serializable;

import no.mesta.mipss.mapplugin.util.MapUtilities;

/**
 * This class represents the bounding box for a map or a Layer
 * 
 * @see no.mesta.mipss.mapplugin.layer.Layer#setBounds
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class Bounds implements Serializable{
    private double llx;
    private double lly;
    private double urx;
    private double ury;


    public Bounds() {
    }
    public Bounds(Bounds bounds){
        setBounds(bounds);
    }
	
    /**
     * Get the lower left x coordinate
     * @return
     */
    public double getLlx() {
        return llx;
    }

    /**
     * Set the lower left x coordinate
     * @param llx new x coordinate
     */
    public void setLlx(double llx) {
        this.llx = llx;
    }
    
    /**
     * Get the lower left y coordinate
     * @return
     */
    public double getLly() {
        return lly;
    }
    
    /**
     * Set the lower left y coordinate
     * @param lly new y coordinate
     */
    public void setLly(double lly) {
        this.lly = lly;
    }
    
    /**
     * Get the upper right x coordinate
     * @return
     */
    public double getUrx() {
        return urx;
    }
    
    /**
     * Set the upper right x coordinate
     * @param urx new x coordinate
     */
    public void setUrx(double urx) {
        this.urx = urx;
    }
    
    /**
     * Get the upper right y coordinate
     * @return
     */
    public double getUry() {
        return ury;
    }

    /**
     * Set the upper right y coordinate
     * @param ury new y coordinate
     */
    public void setUry(double ury) {
        this.ury = ury;
    }
    
    /**
     * Set the bounding box of this Bounds to the specified bounds
     * @param b
     */
    public void setBounds(Bounds b){
        setLlx(b.getLlx());
        setLly(b.getLly());
        setUrx(b.getUrx());
        setUry(b.getUry());
    }
    
    /**
     * Add the specified bounds to this bounds
     * @param b
     */
    public void addBounds(Bounds b){
    	if (b.isNull())
    		return;
    	if (isNull()){
    		setLlx(b.getLlx());
    		setLly(b.getLly());
    		setUrx(b.getUrx());
    		setUry(b.getUry());
    	}else{
	        setLlx(b.getLlx()<getLlx()?b.getLlx():getLlx());
	        setLly(b.getLly()<getLly()?b.getLly():getLly());
	        setUrx(b.getUrx()>getUrx()?b.getUrx():getUrx());
	        setUry(b.getUry()>getUry()?b.getUry():getUry());
    	}
    }
    
    public void addBounds(Point[] c){
    	for (Point point:c){
    		Bounds b = new Bounds();
    		b.setLlx(point.getX());
            b.setUrx(point.getX());
            b.setLly(point.getY());
            b.setUry(point.getY());
            addBounds(b);
    	}
    }
    /**
     * Returns the width of this Bounds.
     * Calculates the actual width in the real-world and not in pixel space
     * @return
     */
    public double getWidth(){   
    	Point p1 = new Point();
    	p1.setLocation(getLlx(), getUry());
    	Point p2 = new Point();
    	p2.setLocation(getUrx(), getUry());
    	return MapUtilities.calculateDistance(p1, p2);
    }
    
    /**
     * Returns the height of this Bounds
     * Calculates the actual height in the real-world and not in pixel space
     * @return
     */
    public double getHeight(){
    	Point p1 = new Point();
    	p1.setLocation(getLlx(), getLly());
    	Point p2 = new Point();
    	p2.setLocation(getLlx(), getUry());
    	return MapUtilities.calculateDistance(p1, p2);
    }
    
    
    public boolean contains(double x, double y){
    	return (x>llx && x<urx && y<ury && y>lly);
    }
    /**
     * Returns true if all coordinates is 0
     * @return
     */
    public boolean isNull(){
        return llx==0&&lly==0&&urx==0&&ury==0;
    }
    public String toString() {
        return "llx:" + llx + ", lly:" + lly + ", urx:" + urx + ", ury:" + ury+" : width:"+getWidth()+" height:"+getHeight();
    }
    

}
