package no.mesta.mipss.mapplugin.util;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.event.MouseInputListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manager for the MouseInputListeners at work. The manager delegates the
 * MouseEvent to a MouseInputListener if the event is not consumed. A listener
 * added to this manager can invalidate all other listeners so that the listener
 * is the only one receiving events. 
 * 
 * @author Harald A. Kulø
 */
public class MouseListenerManager implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private List<MouseInputListener> listenerList = new ArrayList<MouseInputListener>();
    private List<Class<MouseInputListener>> notInvalidated = new ArrayList<Class<MouseInputListener>>();
    
    public boolean invalidated = false;
    
    public MouseListenerManager() {
    }
    public List<MouseInputListener> getListenerList(){
    	return listenerList;
    }
    /**
     * Invalidate all listeners exept those of the specified class
	*
     * @param clazz <? extends MouseInputListener>
     */
    public void invalidateAllBut(Class clazz){
        notInvalidated.add(clazz);        
        invalidated=true;
    }
    /**
     * Remove the listener that is not invalidated and uninvalidate other if
     * the list is empty
     * @param clazz <? extends MouseInputListener>
     */
    public void removeUninvalidated(Class clazz){
        notInvalidated.remove(clazz);
        if (notInvalidated.isEmpty())
            invalidated=false;
    }
    
    public void reset(){
        notInvalidated.clear();
        invalidated=false;
    }
    /**
     * Add a listener to the manager
     * @param index the index the listener should be added, the index defines in with order the
     * listener should be triggered. If the event is consumed before the listener the event never
     * reaches the listener. 
     * @param listener the listener to be added
     */
    public void addListener(int index, MouseInputListener listener){
        listenerList.add(index, listener);
    }
    
    /**
     * Remove a listener from the manager. If this listener is blocking other listener
     * the invalidatation is removed
     * @param listener <? extends MouseInputListener>
     */
    public void removeListener(Class listener){
        removeUninvalidated(listener);
        
        List<MouseInputListener> toRemove = new ArrayList<MouseInputListener>();
        for (MouseInputListener l:listenerList){
            if (l.getClass()==listener){
                toRemove.add(l);
            }
        }
        for (MouseInputListener l:toRemove){
            listenerList.remove(l);
        }
    }
    
    public void mouseClicked(MouseEvent e) {
        Iterator<MouseInputListener> it = listenerList.iterator();
        
        if (invalidated){
            while (it.hasNext()){
                MouseInputListener l = it.next();
                if (notInvalidated.contains(l.getClass())){
                    l.mouseClicked(e);                    
                }
            }
        }else{
            while (!e.isConsumed()&&it.hasNext()){
                it.next().mouseClicked(e);
            }
        }
    }

    public void mousePressed(MouseEvent e) {
        Iterator<MouseInputListener> it = listenerList.iterator();
        if (invalidated){
            while (it.hasNext()){
                MouseInputListener l = it.next();
                if (notInvalidated.contains(l.getClass())){
                    l.mousePressed(e);                    
                }
            }
        }else{
            while (!e.isConsumed()&&it.hasNext()){
                it.next().mousePressed(e);
            }
        }
    }

    public void mouseReleased(MouseEvent e) {
        Iterator<MouseInputListener> it = listenerList.iterator();
        if (invalidated){
            while (it.hasNext()){
                MouseInputListener l = it.next();
                if (notInvalidated.contains(l.getClass())){
                    l.mouseReleased(e);                    
                }
            }
        }else{
            while (!e.isConsumed()&&it.hasNext()){
                it.next().mouseReleased(e);
            }
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        Iterator<MouseInputListener> it = listenerList.iterator();
        if (invalidated){
            while (it.hasNext()){
                MouseInputListener l = it.next();
                if (notInvalidated.contains(l.getClass())){
                    l.mouseDragged(e);                    
                }
            }
        }else{
            while (!e.isConsumed()&&it.hasNext()){
                it.next().mouseDragged(e);
            }            
        }
    }

    public void mouseMoved(MouseEvent e) {
            Iterator<MouseInputListener> it = listenerList.iterator();
            if (invalidated){
                while (it.hasNext()){
                    MouseInputListener l = it.next();
                    if (notInvalidated.contains(l.getClass())){
                        l.mouseMoved(e);                    
                    }
                }
            }else{
                while (!e.isConsumed()&&it.hasNext()){
                    it.next().mouseMoved(e);
                }            
            }
        }
}
