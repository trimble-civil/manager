package no.mesta.mipss.mapplugin.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.List;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.utm.UTMProjectionTileFactory;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;


/**
 * A collection of GeoPosition's to create a grid or series of lines. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class LineListLayer extends Layer implements SelectableLayer, CollectionEntity, Serializable{
    private List<MipssGeoPosition[]> points;
    private transient Logger log = LoggerFactory.getLogger(this.getClass());
    private BasicStroke lineStroke = new BasicStroke(1);
    private int selectedStroke =2;//= new BasicStroke(3);
    private TextboxData info;
    private boolean initGraphics = true;
    private boolean connectorMarkers = false;
    private Color connectorColor = Color.blue;
    private int connectorSize = 5;
    
    private LayerCollection parent;
    
    private Layer selected;
    
    /**
     * @param id
     * @param triggerRecenterAndZoom
     * @param bounds
     */
    public LineListLayer(String id, TextboxData info, boolean triggerRecenterAndZoom, Bounds bounds){
        super(id, triggerRecenterAndZoom);
        this.info = info;
        setBounds(bounds);
    }
    /**
     * Create a LineCollectionLayer
     * @param id
     * @param points
     * @param color
     * @param triggerRecenterAndZoom
     * @param bounds
     */
    public LineListLayer(String id, TextboxData info, List<MipssGeoPosition[]> points, Color color, boolean triggerRecenterAndZoom, Bounds bounds) {
        super(id, triggerRecenterAndZoom);
        this.points = points;
        this.info = info;
        setColor(color);
        this.lineStroke = new BasicStroke(2);
        setBounds(bounds);
    }

    /**
     * Creates a new instance of LineCollectionLayer 
     * @param id
     * @param points
     * @param color
     * @param lineWidth
     * @param triggerRecenterAndZoom
     * @param bounds
     */
    public LineListLayer(String id, TextboxData info, List<MipssGeoPosition[]> points, Color color, int lineWidth, boolean triggerRecenterAndZoom, Bounds bounds, boolean initGraphics) {
        super(id, triggerRecenterAndZoom);
        this.points = points;
        this.info = info;
        setColor(color);
        this.lineStroke = new BasicStroke(lineWidth);
        setBounds(bounds);
        this.initGraphics = initGraphics;
    }
    

//    private Rectangle viewport;
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        if (initGraphics){
            g = super.initGraphics(g, map);
        }
        
        Rectangle viewport = map.getViewportBounds();
        boolean paint = false;
        if (points!=null){
            g.setColor(getColor());
            g.setStroke(lineStroke);
            if (isDrawOutsideViewport()){ //usually means that this layer is part of an image.
	            for (MipssGeoPosition[] gp:points){
	                Point2D pt1 = ((UTMProjectionTileFactory)map.getTileFactory()).geoToPixel(gp[0].getLatitude(), gp[0].getLongitude(), map.getZoom());
	                Point2D pt2 = ((UTMProjectionTileFactory)map.getTileFactory()).geoToPixel(gp[1].getLatitude(), gp[1].getLongitude(), map.getZoom());
	                g.drawLine((int)pt1.getX(), (int)pt1.getY(), (int)pt2.getX(), (int)pt2.getY());  
	            }
            }
            
            if (!isDrawOutsideViewport()){
            	for (MipssGeoPosition[] gp:points){
	                Point2D pt1 = ((UTMProjectionTileFactory)map.getTileFactory()).geoToPixel(gp[0].getLatitude(), gp[0].getLongitude(), map.getZoom());
	                if (gp[1]!=null){
		                Point2D pt2 = ((UTMProjectionTileFactory)map.getTileFactory()).geoToPixel(gp[1].getLatitude(), gp[1].getLongitude(), map.getZoom());
		                if (map.getViewportBounds().contains(pt1)||map.getViewportBounds().contains(pt2)){
		                    g.drawLine((int)pt1.getX(), (int)pt1.getY(), (int)pt2.getX(), (int)pt2.getY());  
		                }
	                }
	            }
            }
        }
    }

    /**
     * Get the GeoPosition's that represents this layer
     * @return
     */
    public List<MipssGeoPosition[]> getPoints() {
        return points;
    }

    /**
     * Set new GeoPosition's 
     * @param points
     */
    public void setPoints(List<MipssGeoPosition[]> points) {
        this.points = points;
    }

    /**
     * Get the current stroke
     * @return
     */
    public int getStroke() {
        return (int)lineStroke.getLineWidth();
    }
    private int oldStroke;
    /**
     * Set the current stroke
     * @param stroke
     */
    public void setStroke(int stroke) {
    	oldStroke = (int)lineStroke.getLineWidth();
    	lineStroke = new BasicStroke(stroke);
    }
    public void restoreStroke(){
    	setStroke(oldStroke);
    	
    }
    public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, 
                                           double searchRadius) {
    	if (getBounds().contains(gp.getLongitude(), gp.getLatitude())){
	        for (MipssGeoPosition[] p:points){
	            Polygon pol = new Polygon();
	            pol.addPoint((int)(p[0].getLongitude()-searchRadius), (int)(p[0].getLatitude()-searchRadius));
	            pol.addPoint((int)(p[1].getLongitude()-searchRadius), (int)(p[1].getLatitude()-searchRadius));
	            pol.addPoint((int)(p[1].getLongitude()+searchRadius), (int)(p[1].getLatitude()+searchRadius));
	            pol.addPoint((int)(p[0].getLongitude()+searchRadius), (int)(p[0].getLatitude()+searchRadius));
	            Point point = new Point((int)gp.getLongitude(), (int)gp.getLatitude());
	            if (pol.contains(point)){
	                //log.debug(this);
	                return this;
	            }
	        }
    	}else{
    	}
        return null;
    }

    public Layer getSelectionLayer() {
    	if (selected!=null)
    		return selected;
        String color = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "selectionColor").getVerdi();
        final Color selectionColor = ColorUtil.fromHexString(color);
        String selectedWidth = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "antallPixlerSelect").getVerdi();
        if (selectedWidth!=null)
        	selectedStroke = Integer.parseInt(selectedWidth);
        
        //TODO this got kinda hacky and should be moved to another class
        selected = new Layer(getId(), false){
            private Image image;
            private int zoom = -1;
            private Graphics2D ig;
            private Color color = selectionColor==null?Color.orange:selectionColor;
            private boolean drawVectors = false;
            //private BasicStroke stroke = new BasicStroke(1);
            public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
                
                
                double res = ((MapPanel)map).getResolutions().getResolutionForZoom(map.getZoom());
                int width = (int)(LineListLayer.this.getBounds().getWidth()/res);
                int height = (int)(LineListLayer.this.getBounds().getHeight()/res);
                Rectangle r = map.getViewportBounds();
                if (r.getWidth()*3<width && r.getHeight()*3<height)
                    drawVectors = true;
                    
                
                //calculate the layers upper left location in pixels
                MipssGeoPosition ul = new MipssGeoPosition(LineListLayer.this.getBounds().getUry(), LineListLayer.this.getBounds().getLlx());
                
                Point2D p = map.getTileFactory().geoToPixel(new GeoPosition(ul.getLatitude(), ul.getLongitude()), map.getZoom());
                Stroke lineStroke = new BasicStroke(selectedStroke);
                Rectangle viewport = map.getViewportBounds();
                if (!drawVectors){
                    if (zoom!=map.getZoom() || LineListLayer.this.isChanged()){
                        g = LineListLayer.this.initGraphics(g, map);
                        LineListLayer.this.setChanged(false);
                        
                        
                        zoom = map.getZoom();
                        
                        //create an image with the same size as the layer on the current resolution
                        //good for high zoom-levels, bad for low ones...(OutOfMemory)
                        try{
                            image = new BufferedImage(width==0?1:width, //image width can not be <= 0
                                                      height==0?1:height, //image height can not be <= 0
                                                  BufferedImage.TYPE_INT_ARGB);
                        } catch (OutOfMemoryError e){
                            log.trace("OUT OF MEMORY!!!, attempting to draw vectors instead...");
                            drawVectors = true;
                        }
                        
                        //translate the graphics to match the upper left corner of the layer
                        ig = (Graphics2D)image.getGraphics();
                        ig.translate(-viewport.x + (viewport.x - p.getX()), 
                                     -viewport.y + (viewport.y - p.getY()+2));
                                     
                        ig.setColor(color);
                        ig.setStroke(lineStroke);
                        
                        for (MipssGeoPosition[] gp:points){
                            java.awt.geom.Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(gp[0].getLatitude(), gp[0].getLongitude()), map.getZoom());
                            java.awt.geom.Point2D pt2 = map.getTileFactory().geoToPixel(new GeoPosition(gp[1].getLatitude(), gp[1].getLongitude()), map.getZoom());
                            //if (map.getViewportBounds().contains(pt1)||map.getViewportBounds().contains(pt2)){
                                //also draw the lines that is outside the viewportbounds.
                                ig.drawLine((int)pt1.getX(), (int)pt1.getY(), (int)pt2.getX(), (int)pt2.getY());
//                                log.debug((int)pt1.getX()+" "+ (int)pt1.getY()+" "+ (int)pt2.getX()+" "+ (int)pt2.getY());
                                //g.drawLine((int)pt1.getX()+2, (int)pt1.getY()+2, (int)pt2.getX()+2, (int)pt2.getY()+2);
                                //g.drawLine((int)pt1.getX()-2, (int)pt1.getY()-2, (int)pt2.getX()-2, (int)pt2.getY()-2);
                            //}
                        }
//                        log.debug("test");
                         //clipping rectangle for the image
                         Rectangle clip = new Rectangle((int)(viewport.x + (p.getX() - viewport.x)), 
                                                        (int)(viewport.y + (p.getY() - viewport.y)), 
                                                        width, 
                                                        height);
                        //only paint if the image is visible on screen
                        if (g.getClipBounds().intersects(clip)){
                            //draw the image with the same distance from the upper left corner as the layer
                            g.drawImage(image, clip.x, clip.y, map);
                        }                                                    
                    }
                    //the only thing changed is the location on screen
                    else{
                        //calculate new position for the image and draw it on the graphics context
                        //clipping rectangle for the layer
                        Rectangle clip = new Rectangle((viewport.x - r.x) + (int)(p.getX() - viewport.x), 
                                                       (viewport.y - r.y) + (int)(p.getY()  -viewport.y), 
                                                       image.getWidth(map), 
                                                       image.getHeight(map));
                        //only paint if the image is visible on screen
                        if (g.getClipBounds().intersects(clip)){
                            g.drawImage(image, clip.x, clip.y, map);
                        }
                    }
                }
                else{
                    g = initGraphics(g, map);
                    g.setColor(Color.orange);
                    g.setStroke(lineStroke);
                    for (MipssGeoPosition[] gp:points){
                        java.awt.geom.Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(gp[0].getLatitude(), gp[0].getLongitude()), map.getZoom());
                        java.awt.geom.Point2D pt2 = map.getTileFactory().geoToPixel(new GeoPosition(gp[1].getLatitude(), gp[1].getLongitude()), map.getZoom());
                        if (map.getViewportBounds().contains(pt1)||map.getViewportBounds().contains(pt2)){
                            g.drawLine((int)pt1.getX(), (int)pt1.getY(), (int)pt2.getX(), (int)pt2.getY());  
                        }
                    }
                }
                drawVectors = false;
            }
            
            
        };
        
        selected.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
        return selected;
    }
    
    @Override
    public void setVisible(boolean visible){
        super.setVisible(visible);
        if (parent!=null)
            if (isChanged()){
                parent.setChanged(true);       
            }
    }
    
    public void setParent(LineListLayerCollection parent) {
        this.parent = parent;
    }

    public LayerCollection getParent() {
        return parent;
    }

    public void setParent(LayerCollection parent) {
        this.parent = parent;
    }
    /**
     * not implemented..
     * @param connectionMarkers
     */
    public void setConnectorMarkers(boolean connectionMarkers) {
        this.connectorMarkers = connectionMarkers;
    }

    public boolean isConnectorMarkers() {
        return connectorMarkers;
    }

    public void setConnectorColor(Color connectorColor) {
        this.connectorColor = connectorColor;
    }

    public Color getConnectorColor() {
        return connectorColor;
    }

    public void setConnectorSize(int connectorSize) {
        this.connectorSize = connectorSize;
    }

    public int getConnectorSize() {
        return connectorSize;
    }
    
    public void setInitGraphics(boolean  initGraphics){
    	this.initGraphics = initGraphics;
    }
    
//	public TextboxLayer getTextboxLayer(){
//		return new TextboxLayer(getId(), info);
//	}
}
