package no.mesta.mipss.mapplugin;

import java.awt.image.BufferedImage;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.mapplugin.utm.UTMProjectionTileFactory;

import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.Tile;

/**
 * This is a special kind of Tile that must be used together with UTMProjectionTileFactory, and supports
 * multiple urls for the tile. You can add urls to the Tile by using the UTMTileFactoryInfo's getTileUrl(...)
 * 
 * Some reflection hacking has occurred in this class in order to realize this class' potiential  
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class LayeredTile extends Tile{

	private List<TileUrl> urls = new ArrayList<TileUrl>();
	Priority priority = Priority.High;
	private DefaultTileFactory dtf;
	private SoftReference<BufferedImage> image = new SoftReference<BufferedImage>(null);
	
	public LayeredTile(int x, int y, int zoom) {
		super(x, y, zoom);
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("x="+getX());
		sb.append(" y="+getY());
		sb.append(" z="+getZoom());
		sb.append(" url:"+getUrls().get(0).getUrl());
		return sb.toString();
	}
	/**
	 * Removes an URL from the tile
	 * @param url
	 */
	public void removeUrl(TileUrl url){
		urls.remove(url);
	}
	/**
	 * Adds an URL to the tile
	 * @param url
	 */
	public void addUrl(TileUrl url){
		urls.add(url);
	}
	/**
	 * @return the URLs
	 */
	public List<TileUrl> getUrls() {
		return urls;
	}

	/**
	 * @param urls the URLs to set
	 */
	public void setUrls(List<TileUrl> urls) {
		this.urls = urls;
	}
	
	/**
	 * @return the priority
	 */
	public Priority getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	/**
	 * @return the dtf
	 */
	public DefaultTileFactory getDtf() {
		return dtf;
	}

	/**
	 * @param dtf the dtf to set
	 */
	public void setDtf(DefaultTileFactory dtf) {
		this.dtf = dtf;
	}
	public synchronized boolean isLoading(){
		try {
			Class<?> c = Tile.class;
			Field isLoading = c.getDeclaredField("isLoading");
			isLoading.setAccessible(true);
			return isLoading.getBoolean(this);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public synchronized void setLoading(boolean loading){
		try {
			Class<?> c = Tile.class;
			Field isLoading = c.getDeclaredField("isLoading");
			isLoading.setAccessible(true);
			isLoading.set(this, loading);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	public synchronized void setLoaded(final boolean loaded){
		try {
			Class<?> c = Tile.class;
			Method setLoaded = c.getDeclaredMethod("setLoaded", boolean.class);
			setLoaded.setAccessible(true);
			setLoaded.invoke(this, loaded);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public void setImage(BufferedImage image){
		this.image = new SoftReference<BufferedImage>(image);
	}
	
	public BufferedImage getImage(){
		BufferedImage image = this.image.get();
		if (image == null){
			setLoaded(false);
			((UTMProjectionTileFactory)dtf).startLoading(this);
		}
		return image;
	}

}
