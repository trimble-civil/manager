package no.mesta.mipss.mapplugin.utm;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.LayeredTile;
import no.mesta.mipss.mapplugin.Resolutions;
import no.mesta.mipss.mapplugin.TileUrl;
import no.mesta.mipss.util.MapURLHelper;

import org.jdesktop.swingx.image.ColorTintFilter;
import org.jdesktop.swingx.mapviewer.AbstractTileFactory;
import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Tile;
import org.jdesktop.swingx.mapviewer.TileCache;
import org.jdesktop.swingx.mapviewer.util.GeoUtil;
import org.jfree.util.Log;

/**
 * A TileFactory for UTM projection of maps. This class implements logic to  
 * ask for tiles and conversion between screen coordinates and UTM and back 
 *  
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class UTMProjectionTileFactory extends DefaultTileFactory {
    
    private Resolutions resolutions;
    private Bounds bounds;
    private ColorTintFilter filter;
    private Color filterColor;
    private Float mixValue;
    private ExecutorService service;
    private Map<String, LayeredTile> tileMap = new HashMap<String, LayeredTile>();
    private String username;
    private String password;
    private boolean useAuthentication;
    private URLConnection connection;
	String token = "";
    
    private List<UTMTileFactoryInfo> infoList = new ArrayList<UTMTileFactoryInfo>();
    
    /**
     * Creates a new UTMProjectionTileFactory
     * @param info information object for the UTMProjection
     */
    public UTMProjectionTileFactory(UTMTileFactoryInfo info) {
        super(info);
        this.resolutions = info.getResolutions();
        this.bounds = info.getBounds();
        infoList.add(info);
    }
    
    public void clearInfo(){
    	infoList.clear();
    }
    public void addInfo(UTMTileFactoryInfo info){
    	infoList.add(info);
    }
    /**
     * Returns a Dimension containing the width and height of the map, 
     * in tiles at the
     * current zoom level.
     * So a Dimension that returns 10x20 would be 10 tiles wide and 20 tiles
     * tall. These values can be multipled by getTileSize() to determine the
     * pixel width/height for the map at the given zoom level
     * @return the size of the world bitmap in tiles
     * @param zoom the current zoom level
     */
    public Dimension getMapSize(int zoom, double r) {
       	if (zoom<=resolutions.getMaxResolutionIndex()){//resolutions.getResolutions().length){
    		//double r = resolutions.getResolutionForZoom(zoom);//[resolutions.length-zoom-1];
            double metersPrTile = r*256;//getTileSize(zoom);    
            
            int tilesWide = (int)Math.ceil(((Math.abs(bounds.getUrx())+Math.abs(bounds.getLlx()))/metersPrTile));
            int tilesHigh = (int)Math.ceil(((Math.abs(bounds.getUry())-Math.abs(bounds.getLly()))/metersPrTile));	        
            return new Dimension(tilesWide, tilesHigh);
    	}
    	return new Dimension(4, 4);
    }
    
    public int getMapHeight(int zoom, double r){
    	return (int)Math.ceil((Math.abs(bounds.getUry())-Math.abs(bounds.getLly()))/(r*256));
    }
    
    @Override
    public Dimension getMapSize(int zoom){
    	return getMapSize(zoom, resolutions.getResolutionForZoom(zoom));
    }
 
       
    private long downScale(double coord){
    	coord=Math.abs(coord+0xFFFFFFF);
    	return (long)(coord*10);
    }
    
    /**
     * 
     * @param gp
     * @param zoom
     * @return
     */
    private long getKey(double latitude, double longitude, int zoom){
    	long lon = downScale(longitude);
    	long lat = downScale(latitude);
    	long key = ((lon&0x1FFFFFFF)<<34)|((lat&0x1FFFFFFF)<<4)|zoom;
    	return key;
    }

	public static double round2(double aParam) {    
		return (Math.floor((aParam + 0.005d) * 100 )/100);
	}
	
	private static final int HTSIZE = 4703;
	private static final Point2D.Double[] gpHash = new Point2D.Double[HTSIZE];
	private static final long[] keyHash = new long[HTSIZE];
	   /**
     * Convert a GeoPosition to a pixel position in the world bitmap
     * a the specified zoom level.
     * @param c a GeoPosition
     * @param zoom the zoom level to extract the pixel coordinate for
     * @return the pixel point
     */
    public Point2D geoToPixel(GeoPosition c, int zoom) {
    	return geoToPixel(c.getLatitude(), c.getLongitude(), zoom);
    }
    
    /**
     * Convert a GeoPosition to a pixel position in the world bitmap
     * a the specified zoom level.
     * @param latitude 
     * @params longitude
     * @param zoom the zoom level to extract the pixel coordinate for
     * @return the pixel point
     */
    public Point2D geoToPixel(double latitude, double longitude, int zoom) {        
    	long key = getKey(latitude, longitude, zoom);
    	int index = (int)(key%HTSIZE);
    	Point2D.Double pos = gpHash[index];
    	
    	if (pos==null||keyHash[index]!=key){
	    	double res = resolutions.getResolutionForZoom(zoom);
	    	double x = (longitude - bounds.getLlx()) / res;
	    	
	    	int height = getMapHeight(zoom, res);
	    	double ury = height * 256 * res+bounds.getLly();
	    	double y = (ury - latitude) / res;
	    	
	    	pos = new Point2D.Double(x, y);
	    	gpHash[index]=pos;
	    	keyHash[index]=key;
    	}
    	return pos;
    }
    
    /**
     * Convert a pixel in the world bitmap at the specified
     * zoom level into a GeoPosition
     * @param pixelCoordinate a Point2D representing a pixel in the world bitmap
     * @param zoom the zoom level of the world bitmap
     * @return the converted GeoPosition
     */
    public GeoPosition pixelToGeo(Point2D pix, int zoom) {
    	double res = resolutions.getResolutionForZoom(zoom);//[resolutions.length-zoom-1];
    	Dimension mapSize = getMapSize(zoom);
    	//calculate ury
    	double ury = mapSize.getHeight()*getTileSize(zoom)*res+bounds.getLly();
    	double x = bounds.getLlx() + (pix.getX()*res);
    	double y = ury - (pix.getY()*res);
    	
    	return new GeoPosition(y, x);
    }
    
    
    /**
     * Get a tile from the tile provider. mod Y med max height in order to 
     * request the tiles from position y=0 and inkrement.
     * @param x the tiles x index
     * @param y the tiles y index
     * @param zoom the zoomlevel
     * 
     */
    public Tile getTile(int x, int y, int zoom) {
    	//wrap the tiles vertically --> mod the Y with the max height
    	int tileY = y;//tilePoint.getX();
        int numTilesHigh = (int)getMapSize(zoom).getHeight();
        if (tileY < 0) {
            tileY = numTilesHigh - (Math.abs(tileY)  % numTilesHigh);
        }
        tileY = tileY % numTilesHigh;
        Tile tile = getTile(x, tileY, zoom, true);//super.getTile(x, tileY, zoom);
        
    	return tile;
    }
   
    private Tile getTile(int tpx, int tpy, int zoom, boolean eagerLoad) {
        //wrap the tiles horizontally --> mod the X with the max width
        //and use that
        int tileX = tpx;//tilePoint.getX();
        int numTilesWide = (int)getMapSize(zoom).getWidth();
        if (tileX < 0) {
            tileX = numTilesWide - (Math.abs(tileX)  % numTilesWide);
        }
        
        tileX = tileX % numTilesWide;
        int tileY = tpy;
        //TilePoint tilePoint = new TilePoint(tileX, tpy);
        List<TileUrl> urls = new ArrayList<TileUrl>();
        for (UTMTileFactoryInfo i:infoList){
        	TileUrl url = new TileUrl();
        	url.setUrl(i.getTileUrl(tileX, tileY, zoom));
        	url.setTransparency(i.getTransparency());
        	urls.add(url);
        }
        String url = tileX+","+tileY+","+zoom;
        //String url = getInfo().getTileUrl(tileX, tileY, zoom);//tilePoint);
        //System.out.println("loading: " + url);
        
        
        Tile.Priority pri = Tile.Priority.High;
        if (!eagerLoad) {
            pri = Tile.Priority.Low;
        }
        LayeredTile tile = null;
        //System.out.println("testing for validity: " + tilePoint + " zoom = " + zoom);
        if (!tileMap.containsKey(url)) {
            if (!GeoUtil.isValidTile(tileX, tileY, zoom, getInfo())) {
                tile = new LayeredTile(tileX, tileY, zoom);
            }  else {
                tile = new LayeredTile(tileX, tileY, zoom);//newTile(tileX, tileY, zoom, url, pri, this);
                tile.setUrls(urls);
                tile.setPriority(pri);
                tile.setDtf(this);
                startLoading(tile);
            }
            tileMap.put(url,tile);
        }  else {
            tile = tileMap.get(url);
            // if its in the map but is low and isn't loaded yet
            // but we are in high mode
            if (tile.getPriority()  == Tile.Priority.Low && eagerLoad && !tile.isLoaded()) {
                //System.out.println("in high mode and want a low");
                //tile.promote();
                promote(tile);
            }
        }
        
        return tile;
    }
    
//    private Tile newTile(int x, int y, int zoom, String url, Priority priority, DefaultTileFactory dtf){
//    	Class tileClass = Tile.class;
//        Field urlField = null;
//        Field priorityField = null;
//        Field dtfField = null;
//        try {
//        	urlField = tileClass.getDeclaredField("url");
//        	priorityField = tileClass.getDeclaredField("priority");
//            dtfField = tileClass.getDeclaredField("dtf");
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        }
//        urlField.setAccessible(true);
//        priorityField.setAccessible(true);
//        dtfField.setAccessible(true);
//        Tile tile = new Tile(x, y, zoom);
//        try {
//			urlField.set(tile, url);
//			priorityField.set(tile, priority);
//		    dtfField.set(tile, dtf);
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		}       
//        return tile;
//    }
    
    /**
     * Sets a colorfilter on the tiles that are loaded from now on. In order to 
     * apply colorfilter to all tiles we have to reset the TileCache and repaint the
     * map. 
     * @param color
     * @param mix
     */
    public void setColorFilter(Color color, Float mix){
        
        tileMap = new HashMap<String, LayeredTile>();
        setTileCache(new TileCache());
        if (color!=null)
            filter = new ColorTintFilter(color, mix);
    }
    
    /**
     * Returns the color for the ColorTintFilter
     * @return
     */
    public Color getColorForFilter(){
        if (filter==null){
            if (filterColor!=null)
                return filterColor;
            return null;
        }
        return filter.getMixColor();
    }
    
    /**
     * Return the mixvalue for the ColorTintFilter
     * @return
     */
    public float getMixValueForFilter(){
        if (filter==null){
            if (mixValue!=null)
                return mixValue;
            return 0.5f;
        }
        return filter.getMixValue();
    }
    /**
     * Remove the color filter
     */
    public void removeColorFilter(){
    	tileMap = new HashMap<String, LayeredTile>();
        setTileCache(new TileCache());
        if (filter!=null){
            filterColor = filter.getMixColor();
            mixValue = filter.getMixValue();
        }
        filter = null;
    }
    
    protected synchronized ExecutorService getService() {
    	if(service == null) {
    		service = new ThreadPoolExecutor(4, 10, 5000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
    			private int count = 0;
                    
                public Thread newThread(Runnable r) {
                	Thread t = new Thread(r, "tile-pool-" + count++);
                    t.setPriority(Thread.MAX_PRIORITY);
                    t.setDaemon(false);
                    return t;
                }
    		});
    	}
    	return service;
    }
    
    public void purgeLoading(){
    	getTileQueue().clear();    	
		tileMap.clear();
    	if (service!=null){
	    	service.shutdownNow();
	    	service = null;
    	}
    }
    
    public void closeFactory(){
        getTileQueue().clear();
        service.shutdown();
        tileMap.clear();
    }
       
    public synchronized void startLoading(LayeredTile tile) {
        if(tile.isLoading()) {
            System.out.println("already loading. bailing");
            return;
        }
        tile.setLoading(true);
        try {
        	getTileQueue().put(tile);
            getService().submit(createTileRunner(tile));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /**
     * overridden from AbstractTileFactory, when the swingx-ws project has fixed their bug on 
     * TileCaching, this call will be removed
     * @return
     */
    private Runnable r = null;
    protected Runnable createTileRunner(Tile tile) {
        if (r==null)
            r = new TileRunner();
        return r;
    }
    
    @SuppressWarnings("unchecked")
	private BlockingQueue<LayeredTile> getTileQueue(){
        Class<?> tileFactory = AbstractTileFactory.class;
        Field tq = null;
        try {
            tq = tileFactory.getDeclaredField("tileQueue");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        tq.setAccessible(true);
        BlockingQueue<LayeredTile> tileQueue = null;
        try {
            tileQueue = (PriorityBlockingQueue<LayeredTile>)tq.get(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return tileQueue;
    }
    /**
     * An inner class that load the tiles. Overridden from AbstractTileFactory in order to 
     * fix a bug, and accommodate for multiple sources for the Tile!
     * 
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a> 
     */
    private class TileRunner implements Runnable {
        /**
         * implementation of the Runnable interface.
         */
    	KonfigparamPreferences k = null;
    	String app = "";
    	
    	public TileRunner(){
    		super();
    		 /*
			 * Sjekker om autentisering skal benyttes.
			 * Dersom verdien fra bruk_autentisering ikke er hentet enda, så hentes denne, samt verdiene for brukernavn og passord.
			 * Det gjøres på denne måten for å slippe å hente konfig-verdiene fra basen hver gang en tile skal hentes. 
			 */
    		app = "studio.mipss.mapplugin";
			k = KonfigparamPreferences.getInstance();
			useAuthentication = Boolean.parseBoolean(k.hentEnForApp(app, "bruk_nordeca_autentisering").getVerdi());
			if(useAuthentication){
				username = k.hentEnForApp(app, "brukernavn").getVerdi();
				password = k.hentEnForApp(app, "passord").getVerdi();
				token = MapURLHelper.hentTokenString(token, k.hentEnForApp(app, "mapserverTokenURL").getVerdi(), username, password);
			}
    	}
    	
        public void run() {

            BlockingQueue<LayeredTile> tileQueue = getTileQueue();
            final LayeredTile tile = tileQueue.remove();
            int tries = 3;
           
			
            while (!tile.isLoaded() && tries > 0) {
                try {
                	//Token skal bare bruke i tilfeller der man benytter autentisering
                	if(useAuthentication){
                		tile.setUrls(modifyURLToToken(tile));
                	}
                    BufferedImage img = cacheInputStream(tile);
                    if(img == null) {
                        System.err.println("UTMProjectionTileFactory: error loading: " + tile);
                        tries--;
                    } else {
                        final BufferedImage i = img;
                        Runnable r = null;
                        try{
                        	r = new Runnable() {
	                            public void run() {
	                            	tile.setImage(i);
	                            	tile.setLoaded(true);
	                                BufferedImage image = tile.getImage();
	                                if (image!=null&&filter!=null){
	                                    filter.filter(tile.getImage(), tile.getImage());
	                                }
	                            }
                        	};
	                        SwingUtilities.invokeAndWait(r);
	                        
                        }catch (InterruptedException e){
                        }
                    }
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } catch (Throwable e) {
                	System.out.println("Kartmodul - Token er mest sannsynlig utgått. Henter ny token for autentisering.");
                	token = "";
                	token = MapURLHelper.hentTokenString(token, k.hentEnForApp(app, "mapserverTokenURL").getVerdi(),username,password);
//                    e.printStackTrace(); //Skriver ikke ut stacktrace, da det egentlig ikke er en feil. Dette vil oppstå når token har utløpt uansett.
                    tile.setError(e);
                    if (tries == 0) {
                    } else {
                        tries--;
                    }
                }
            }
            tile.setLoading(false);
        }

        private BufferedImage cacheInputStream(LayeredTile tile) throws IOException {
        	List<BufferedImage> images = new ArrayList<BufferedImage>();
 
        	for (TileUrl t:tile.getUrls()){
        		URI uri=null;
				try {
					uri = new URI(t.getUrl());
				} catch (URISyntaxException e) {
					e.printStackTrace();
					continue;
				}
				
				InputStream ins;
				
				ins = uri.toURL().openStream();

        		ByteArrayOutputStream bout = new ByteArrayOutputStream();
                byte[] buf = new byte[256];
                while(true) {
                    int n = ins.read(buf);
                    if(n == -1) break;
                    bout.write(buf,0,n);
                }
                BufferedImage image = loadImage(bout);
                images.add(image);
        	}
        	return merge(images, tile);
        }
        private BufferedImage loadImage(ByteArrayOutputStream bout) throws IOException{
        	ByteArrayInputStream imgStream = new ByteArrayInputStream(bout.toByteArray());
            BufferedImage image = ImageIO.read(imgStream);
            if(image == null) return null;
            GraphicsConfiguration defaultConfiguration = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
            BufferedImage compatibleImage = defaultConfiguration.createCompatibleImage(image.getWidth(), image.getHeight(), Transparency.TRANSLUCENT);
            Graphics g = compatibleImage.getGraphics();
            g.setColor(Color.white);
            g.fillRect(0, 0, image.getWidth(), image.getHeight());
            g.drawImage(image, 0, 0, null);
            g.dispose();
            imgStream.close();
            return compatibleImage;
        }
        private BufferedImage merge(List<BufferedImage> images, LayeredTile tile){
        	if (images.size()==1){
        		BufferedImage image = images.get(0);
        		tile.setImage(image);
        		return image;
        	}
        	BufferedImage image = new BufferedImage(256, 256, BufferedImage.TYPE_INT_ARGB);
        	Graphics2D g = (Graphics2D)image.getGraphics();
        	
            int idx=0;
            
        	for (BufferedImage i:images){
        		float t = tile.getUrls().get(idx).getTransparency();
        		Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, t);
                g.setComposite(c);
        		g.drawImage(i, 0, 0, null);
        		idx++;
        	}
        	return image;
        }
    }
   

	/**
	 * Legger inn token som parameter i en tile-url
	 * @param tile
	 * @return
	 */
    
	public List<TileUrl> modifyURLToToken(LayeredTile tile) {
    	TileUrl modifiedTileUrl = new TileUrl();
    	String urlTxt = cleanTokenFromUrl(tile.getUrls().get(0).getUrl());
    	String correctUrl = urlTxt + "&tokenstring=" + token;
    	modifiedTileUrl.setUrl(correctUrl);
    	ArrayList<TileUrl> urlList = new ArrayList<TileUrl>();
    	urlList.add(modifiedTileUrl);
    	return urlList;
	}

	/**
	 * Vi har tilfeller der vi får flere tokenstrings etter hverandre i URL'en, dette unngår vi ved å vaske 
	 * url'en for tokens før vi evt legger på en ny.
	 * @param url
	 * @return
	 */
	private String cleanTokenFromUrl(String url) {
		
		String [] stringarray = url.split("&");
		StringBuilder cleanUrl = new StringBuilder();
		
		for (int i = 0; i < stringarray.length; i++) {
			String string = stringarray[i];
			
			if (!string.contains("tokenstring")) {
				if (i > 0) {
					cleanUrl.append("&");
				}
				cleanUrl.append(string);
			} else{
			}
		}
		return cleanUrl.toString();
	}
}