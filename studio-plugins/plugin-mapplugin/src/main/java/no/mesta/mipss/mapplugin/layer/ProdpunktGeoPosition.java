package no.mesta.mipss.mapplugin.layer;

import java.awt.geom.Point2D;

import no.mesta.mipss.core.UTMUtils;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.persistence.kvernetf1.Prodpunktinfo;
import no.mesta.mipss.service.produksjon.ProdpunktVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasse som inneholder en GeoPosition og et Prodpunkt.
 * Oppretter et GeoPosition objekt for bruk i kart ved instansiering.
 *
 */
public class ProdpunktGeoPosition {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MipssGeoPosition geoPosition;
    private Prodpunktinfo prodpunktInfo;
    private ProdpunktVO prodpunktVO;
    private boolean grunndata;
    /**
     * Konstruktør
     * @param prodpunkt
     */
    public ProdpunktGeoPosition(Prodpunktinfo prodpunkt) {
    	
        geoPosition = new MipssGeoPosition(prodpunkt.getSnappetY(), prodpunkt.getSnappetX());
        this.prodpunktInfo = prodpunkt;
    }
    /**
     * Konstruktør som også markerer om dette geoposition objektet er grunndata, dersom man ikke 
     * vil ta stilling til det benyttes den andre konstruktøren 
     * @param prodpunkt
     * @param grunndata
     */
    public ProdpunktGeoPosition(ProdpunktVO prodpunkt, boolean grunndata){
    	this(prodpunkt);
    	this.grunndata = grunndata;
    }
    /**
     * ny instans av ProdpunktGeoPosition, 
     * For et prodpunkt som ikke er snappet benyttes gps koordinatene
     * dersom det ikke finnes snappede koordinater vil punktet markers som grunndata
     * @param prodpunkt
     */
    public ProdpunktGeoPosition(ProdpunktVO prodpunkt){
//    	if (prodpunkt.getPdop()!=null){
//    		if (prodpunkt.getGPSXX()!=null){
//				geoPosition = getGeoPositionFromGPSString(prodpunkt.getGpsY(), prodpunkt.getGPSXX());
//				grunndata=true;
//			}
//    		else if (prodpunkt.getSnappetY()!=null&&prodpunkt.getSnappetX()!=null){//snappet til veg
//    			geoPosition = new MipssGeoPosition(prodpunkt.getSnappetY(), prodpunkt.getSnappetX());
//    		}
//    		else if(prodpunkt.getGpsX()!=null&&prodpunkt.getGpsY()!=null){//usnappet
//    			geoPosition = getGeoPositionFromGPS(prodpunkt.getGpsY(), prodpunkt.getGpsX());
//    			grunndata=true;
//    		}
//    		
//    	}else{
    		if (prodpunkt.getSnappetY()==null&&prodpunkt.getSnappetX()==null){
    			if (prodpunkt.getGPSXX()!=null){
    				geoPosition = getGeoPositionFromGPSString(prodpunkt.getGpsY(), prodpunkt.getGPSXX());
    				grunndata=true;
    			}
    			else if (prodpunkt.getGpsX()!=null&&prodpunkt.getGpsY()!=null){
    	    		geoPosition = getGeoPositionFromGPS(prodpunkt.getGpsY(), prodpunkt.getGpsX());
    	    		grunndata=true;
    			}else{
    				geoPosition = new MipssGeoPosition(0,0);
    				grunndata=true;
    			}
    		}
    		else{
    			geoPosition = new MipssGeoPosition(prodpunkt.getSnappetY(), prodpunkt.getSnappetX());
    		}
//    	}
        setProdpunktVO(prodpunkt);
    }
    
    private MipssGeoPosition getGeoPositionFromGPSString(Double lat, String lon){
    	UTMUtils u = new UTMUtils();
    	Point2D coord = new Point2D.Double();
    	Double longitude = u.getUtmCoordinate(lon);
    	Double latitude = u.getUtmCoordinate(lat);
    	u.convert2UTM(latitude, longitude, coord);
    	MipssGeoPosition gp = new MipssGeoPosition(coord.getX(), coord.getY());
    	return gp;
    }
    
    private MipssGeoPosition getGeoPositionFromGPS(Double y, Double x){
    	UTMUtils u = new UTMUtils();
    	Point2D coord = new Point2D.Double();
    	u.convert2UTM(u.getUtmCoordinate(y), u.getUtmCoordinate(x), coord);
    	MipssGeoPosition gp = new MipssGeoPosition(coord.getX(), coord.getY());
    	return gp;
    }
    
    public void setGeoPosition(MipssGeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public MipssGeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setProdpunktinfo(Prodpunktinfo prodpunkt) {
        this.prodpunktInfo = prodpunkt;
    }

    public Bounds getBounds(){
    
        Bounds b = new Bounds();
        b.setLlx(geoPosition.getLongitude());
        b.setLly(geoPosition.getLatitude());
        b.setUrx(geoPosition.getLongitude());
        b.setUry(geoPosition.getLatitude());
        return b;
    }

	public void setProdpunktVO(ProdpunktVO prodpunktVO) {
		this.prodpunktVO = prodpunktVO;
	}

	public ProdpunktVO getProdpunktVO() {
		return prodpunktVO;
	}
	public boolean isGrunndata() {
		return grunndata;
	}
}
