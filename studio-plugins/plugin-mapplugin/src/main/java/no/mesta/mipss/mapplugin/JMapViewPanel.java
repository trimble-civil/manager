package no.mesta.mipss.mapplugin;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;

import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mapplugin.util.LayerEvent;

import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * Map som kan legges inn i skjermbilder
 * 
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class JMapViewPanel extends MapPanel {

    /**
     * Lager et kart
     * 
     * @param metersPerPixel
     * @param enableMouseMovement
     */
    public JMapViewPanel(double metersPerPixel, boolean enableMouseMovement) {
        this(null, metersPerPixel, enableMouseMovement);
    }
    
    /**
     * Lager et kart sentrert på center
     * 
     * @param center
     * @param metersPerPixel
     * @param enableMouseMovement
     */
    public JMapViewPanel(GeoPosition center, double metersPerPixel, boolean enableMouseMovement) {
        super();
        
        if(!enableMouseMovement) {
            for(MouseListener m:getMouseListeners()) {
                removeMouseListener(m);
            }
            for(MouseMotionListener m:getMouseMotionListeners()) {
                removeMouseMotionListener(m);
            }
        }
        
        Resolutions res = getResolutions();
        int zoom = res.getClosestResolutionIndex(metersPerPixel);
        setZoom(zoom);
        
        if(center != null) {
            Point2D c = getTileFactory().geoToPixel(center, getZoom());
            setCenter(c);
        }
    }
    
    /**
     * Lager et kart
     * 
     */
    public JMapViewPanel(){
    }

    /**
     * Oppdater kartet når et Layer endres. Hvis LayerEventet er triggerRecenterAndZoom
     * skal kartet flyttes og nytt zoomnivå regnes ut. 
     * 
     * @param e eventet 
     */
    public void layerChanged(LayerEvent e) {
        if (e.getSource()!=null&&e.getSource() instanceof LayerHandler){
            LayerHandler handler = (LayerHandler)e.getSource();
            Layer l = handler.getLastChangedLayer();
            if (e.isTriggerRecenterAndZoom()){
                Bounds layerBounds = l.getBounds();
                setZoom(calculateZoomForBounds(layerBounds));
                setAddressLocation(calculateCenterForBounds(layerBounds));
                
            }        
        }
        repaint();
    }
}
