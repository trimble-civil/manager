package no.mesta.mipss.mapplugin.util;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.*;
import no.mesta.mipss.mapplugin.util.textboxdata.*;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.service.produksjon.*;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;
import no.mesta.mipss.service.veinett.KontraktveinettVO;
import no.mesta.mipss.webservice.*;
import no.mesta.mipss.webservice.service.ReflinkSeksjon;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * 
 * Denne klasse bygger lag til kartet. Moduler som bruker kartplugin kan bruke denne klassen
 * til å lage lag til sitt kart.  
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class LayerBuilder {
	private IMipssStudioLoader loader;
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private NvdbWebservice service;
	private final MapPanel map;
	
	public LayerBuilder(IMipssStudioLoader loader, MapPanel map){
		this.loader = loader;
		this.map = map;
	}
	/**
     * Konverterer en liste med Prodpunkter til en liste med ProdpunktPointLayer
     * @param list
     * @return
     */
    public List<CollectionEntity> convertProdpunkToPointLayers(List<ProdpunktVO> list, Bounds bounds){
        List<CollectionEntity> region = new ArrayList<CollectionEntity>();
        Date serieFra = list.get(0).getCetTidspunkt();
    	Date serieTil = list.get(list.size()-1).getCetTidspunkt();
        for (final ProdpunktVO p:list){
            region.add(getPointLayerFromProdpunktVO(p, bounds, serieFra, serieTil));
        }
        return region;
    }
    
    public ProdpunktPointLayer getPointLayerFromProdpunktVO(ProdpunktVO p, Bounds bounds, Date serieFra, Date serieTil){
    	if (serieFra==null){
    		serieFra = p.getCetTidspunkt();
    	}
    	if (serieTil==null){
    		serieTil = p.getCetTidspunkt();
    	}
    	Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
    	final ProdpunktGeoPosition pgp = new ProdpunktGeoPosition(p);
        if (bounds.isNull()){
            bounds.setBounds(pgp.getBounds());
        }else{
            bounds.addBounds(pgp.getBounds());
        }
        Color color = null;
        //hvis ingen prodtype er satt, skal fargen settes ut i fra digitale inn bits der
        //digi AND ignore skal være større enn 3 for produksjon og under for tomkjøring. 
        if (p.getProdtypeList()==null){
            int s = Integer.parseInt(p.getDigiAndMask(), 16);
            boolean status = s > 3;
            color = status?green:red;
        }
        else{
        	//fargelegg ihht produksjonstypen.
        	Color[] colors = new Color[p.getProdtypeList().size()];
        	for (int i=0;i<p.getProdtypeList().size();i++){
        		ProdtypeVO pt = p.getProdtypeList().get(i);
        		colors[i] = ColorUtil.fromHexString(pt.getProdtype().getGuiFarge());
        	}
        	color = ColorUtil.mergeAllColors(colors);
        }
        
        String id = p.getDfuId()+" "+p.getGmtTidspunkt();
        
        ProdpunktPointLayer layer = new ProdpunktPointLayer(id, pgp, color, false);
        layer.setBounds(pgp.getBounds());
        layer.setTextboxProducer(new ProdpunktTextboxProducer(loader, map, p, serieFra, serieTil));
        return layer;
    }
    
    public ProdpunktPointLayer getPointLayerFromFriksjonsdetalj(Friksjonsdetalj f, ProdpunktVO prodpunkt){
    	Color bg = null;
		Color green = Color.green;
		Color lime = new Color(170,255,0);
		Color yellow = Color.yellow;
		Color orange = new Color(255,170,0);
		Color red = Color.red;
		Color dark = new Color(170, 0, 0);
		double v = f.getFriksjon();
		
		if (v>=0.35)
			bg = green;
		else if (v >=0.3)
			bg = lime;
		else if (v >= 0.25)
			bg = yellow;
		else if (v >= 0.2)
			bg = orange;
		else if (v >= 0.15)
			bg = red;
		else if (v< 0.15){
			bg = dark;
		}
		final ProdpunktGeoPosition pgp = new ProdpunktGeoPosition(prodpunkt);
		
		String id = prodpunkt.getDfuId()+" "+prodpunkt.getGmtTidspunkt();
		ProdpunktPointLayer l = new ProdpunktPointLayer(id, pgp, bg, false);
		l.setBounds(pgp.getBounds());
		l.setTextboxProducer(new FriksjonsdetaljTextboxProducer(f));
		l.setInitGraphics(true);
		return l;
    }
    
    public List<CollectionEntity> getPointlayersFromFriksjonVOList(List<FriksjonVO> list, Bounds bounds){
        List<CollectionEntity> region = new ArrayList<CollectionEntity>();
        Date serieFra = list.get(0).getDate();
    	Date serieTil = list.get(list.size()-1).getDate();
    	
        for (final FriksjonVO p:list){
            region.add(getPointLayerFromFriksjonVO(p, bounds));
        }
        return region;
    }
    
    public ProdpunktPointLayer getPointLayerFromFriksjonVO(FriksjonVO f, Bounds bounds){
    	Color bg = null;
		Color green = Color.green;
		Color lime = new Color(170,255,0);
		Color yellow = Color.yellow;
		Color orange = new Color(255,170,0);
		Color red = Color.red;
		Color dark = new Color(170, 0, 0);
		double v = f.getFriksjon();
		
		if (v>=0.35)
			bg = green;
		else if (v >=0.3)
			bg = lime;
		else if (v >= 0.25)
			bg = yellow;
		else if (v >= 0.2)
			bg = orange;
		else if (v >= 0.15)
			bg = red;
		else if (v< 0.15){
			bg = dark;
		}
		
//		Prodpunktinfo prodpunkt = new Prodpunktinfo();
//		prodpunkt.setSnappetX(f.getSnappetX());
//		prodpunkt.setSnappetY(f.getSnappetY());
		
		ProdpunktVO pv = new ProdpunktVO();
		pv.setDfuId(f.getDfuId());
		pv.setSnappetX(f.getSnappetX());
		pv.setSnappetY(f.getSnappetY());
		pv.setCetTidspunkt(f.getDate());
		pv.setRetning(f.getRetning());
		
		final ProdpunktGeoPosition pgp = new ProdpunktGeoPosition(pv);
		
		if (bounds.isNull()){
            bounds.setBounds(pgp.getBounds());
        }else{
            bounds.addBounds(pgp.getBounds());
        }
		
		String id = pv.getDfuId()+" "+pv.getGmtTidspunkt();
		ProdpunktPointLayer l = new ProdpunktPointLayer(id, pgp, bg, false);
		l.setBounds(pgp.getBounds());
		l.setTextboxProducer(new FriksjonVOTextboxProducer(f));
		l.setInitGraphics(true);
		l.setPreferredLayerType(MipssMapLayerType.FRIKSJONPUNKT.getType());
		return l;
    }

    /**
     * Konverterer en liste med Prodpunkter til en liste med ProdpunktPointLayer
     * @param list
     * @return
     */
    public List<CollectionEntity> convertProdpunkToPDOPLayers(List<ProdpunktVO> list, Bounds bounds, double rmax, double rmin, double ymax, double ymin, double gmax, double gmin, boolean redVisible, boolean yellowVisible, boolean greenVisible){
        List<CollectionEntity> region = new ArrayList<CollectionEntity>();
        for (final ProdpunktVO p:list){
            final ProdpunktGeoPosition pgp = new ProdpunktGeoPosition(p);
            
            Color color = null;
            
            boolean addLayer = true;
        	//fargelegg ihht pdop.
        	Double pdop = pgp.getProdpunktVO().getPdop();
        	if (pdop == null)
        		color = Color.white;
        	else{
        		
        		if (pdop <= rmax && pdop >= rmin){
        			color = Color.red;
        			if (!redVisible)
        				addLayer = false;
        		}
        		if (pdop < ymax && pdop >=ymin){
        			color = Color.yellow;
        			if (!yellowVisible)
        				addLayer = false;
        		}
        		if (pdop < gmax && pdop >=gmin){
        			color = Color.green;
        			if (!greenVisible)
        				addLayer = false;
        		}
        	}
        	if (color == null)
        		color = Color.gray;
        	if (addLayer){
        		if (bounds.isNull()){
                    bounds.setBounds(pgp.getBounds());
                }else{
                    bounds.addBounds(pgp.getBounds());
                }
	            String id = p.getDfuId()+" "+p.getGmtTidspunkt();
	            ProdpunktPointLayer layer = new ProdpunktPointLayer(id, pgp, color, false);
	            layer.setTextboxProducer(new ProdpunktTextboxProducer(loader, map, p, list.get(0).getCetTidspunkt(), list.get(list.size()-1).getCetTidspunkt()));
	            region.add(layer);
        	}
        }
        return region;
    }
    public List<MipssGeoPosition[]> convertNestedPointListToGeoPositionList(List<List<Point[]>> vertices, Bounds b){
    	List<MipssGeoPosition[]> geoVertices = new ArrayList<MipssGeoPosition[]>();
    	for (List<Point[]> l:vertices){
    		geoVertices.addAll(convertPointListToGeoPositionList(l, b));
    	}
    	return geoVertices;
    }
    /**
     * Konverterer en liste med punkter til en liste med geoPosisjoner.
     * @param vertices
     * @param b
     * @return
     */
    public static List<MipssGeoPosition[]> convertPointListToGeoPositionList(List<Point[]> vertices, Bounds b){
        List<MipssGeoPosition[]> geoVertices = new ArrayList<MipssGeoPosition[]>();
        
        for (Point[] point:vertices){
            //setter boundingboxen til produksjonen.
        	b.addBounds(point);
            
        	MipssGeoPosition[] gp = new MipssGeoPosition[2];
        	gp[0] = new MipssGeoPosition(point[0].getY(), point[0].getX());
        	gp[1] = new MipssGeoPosition(point[1].getY(), point[1].getX());
        }
        return geoVertices;
    }
    /**
     * Konverterer en liste med punkter til en liste med layere
     * @param vertices
     * @return
     */
    public List<CollectionEntity> convertPointVerticesToLineListLayer(List<StrekningVO> vertices, Bounds bounds){
        List<CollectionEntity> layerList = new ArrayList<CollectionEntity>();
        Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
        
        log.debug("Antall strekninger:"+vertices.size());
        for (StrekningVO strekning:vertices){
        	Date start = strekning.getProdstrekning().getFraTidspunkt();
        	Date stopp = strekning.getProdstrekning().getTilTidspunkt();
        	log.debug("start-stopp:"+start+" "+stopp);
        	List<MipssGeoPosition[]> geoList = convertPointListToGeoPositionList(strekning.getPointList(), bounds);
        	Integer digiInt = Integer.decode("0x"+strekning.getProdstrekning().getDigi());
        	short digi = Short.parseShort(digiInt+"");
            boolean status = (digi|2) > 3;
        	PeriodLineListLayer layer = new PeriodLineListLayer("testId", null, geoList, /*c*/status?green:red, 2,  false, bounds, start, stopp, strekning.getProdstrekning().getDfuId());
        	layerList.add(layer);
        }
        
        return layerList;
    }
	/**
     * Konverterer en liste med Prodpunkter til en liste med ProdpunktPointLayer
     * @param list
     * @return
     */
   /* public List<ProdpunktPointLayer> convertInspeksjonPunktinfoToPointLayers(List<Prodpunktinfo> list, Bounds bounds, Color defaultColor){
    	List<ProdpunktPointLayer> region = new ArrayList<ProdpunktPointLayer>();
        Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
        for (Prodpunktinfo p:list){
        	if (p!=null){
		            ProdpunktGeoPosition pgp = new ProdpunktGeoPosition(p);
		            if (bounds.isNull()){
		                bounds.setBounds(pgp.getBounds());
		            }else{
		                bounds.addBounds(pgp.getBounds());
		            }
		            Color color=null;
		            if (defaultColor==null){
			            short digi = Short.decode(p.getProdpunkt().getDigi());
			            short ignore = 0;
			            if (p.getProdpunkt().getIgnorerMaske()!=null)
			            	ignore = Short.decode(p.getProdpunkt().getIgnorerMaske());
			            boolean status = (digi|ignore) > 3;
			            color = status?green:red;
		            }else
		            	color = defaultColor;
		            String id = p.getProdpunkt().getDfuId()+" "+p.getProdpunkt().getGmtTidspunkt();
		            ProdpunktPointLayer layer = new ProdpunktPointLayer(id, pgp, color, false);
//		            layer.setTextboxProducer(new InspeksjonpunktTextboxProducer(p));
		            region.add(layer);
        	}
        }
        return region;
    }
	*/
	/**
     * Konverterer en liste med Prodpunkter til en liste med ProdpunktPointLayer
     * @param list
     * @return
     */
    public List<ProdpunktPointLayer> convertInspeksjonPunktVOToPointLayers(List<ProdpunktVO> list, Bounds bounds, Color defaultColor){
    	List<ProdpunktPointLayer> region = new ArrayList<ProdpunktPointLayer>();
        Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
        for (ProdpunktVO p:list){
        	if (p!=null){
		            ProdpunktGeoPosition pgp = new ProdpunktGeoPosition(p);
		            
		            if (bounds.isNull()){
		                bounds.setBounds(pgp.getBounds());
		            }else{
		                bounds.addBounds(pgp.getBounds());
		            }
		            Color color=null; 
		            if (defaultColor==null){
			            int s = Integer.parseInt(p.getDigiAndMask(), 16);
			            boolean status = s > 3;
			            color = status?green:red;
		            }else
		            	color = defaultColor;
		            String id = p.getDfuId()+" "+p.getGmtTidspunkt();
		            ProdpunktPointLayer layer = new ProdpunktPointLayer(id, pgp, color, false);
		            layer.setTextboxProducer(new InspeksjonpunktTextboxProducer(loader, p));
		            region.add(layer);
        	}
        }
        return region;
    }
	public LineListLayer convertPointVerticesToLineListLayer(List<ReflinkStrekning> geometry){
    	Bounds bounds = new Bounds();
    	List<MipssGeoPosition[]> geoList = convertReflinkStrekningListToGeoPositionList(geometry, bounds);
    	LineListLayer layer = new LineListLayer("", null, geoList, Color.green, false, bounds);
    	return layer;
	}

	public List<PeriodLineListLayer> createPeriodLineListLayerFromProdReflinkStrekning(String guid, List<ProdReflinkStrekning> list, Bounds bounds, boolean addTextBox, Color defaultColor){
		List<PeriodLineListLayer> layerList = new ArrayList<PeriodLineListLayer>();
        Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
        
        String value = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "antallPixlerProduksjon").getVerdi();
        int width = 1;
        
        if (value!=null)
    		width = Integer.parseInt(value);
        for (final ProdReflinkStrekning strekning:list){
        	
        	if (strekning.getStrekningList()!=null){
	        	final Date start = strekning.getProdstrekning().getFraTidspunkt();
	        	final Date stopp = strekning.getProdstrekning().getTilTidspunkt();
	        	Bounds layerBounds = new Bounds();
	        	List<MipssGeoPosition[]> geoList = convertReflinkStrekningListToGeoPositionList(strekning.getStrekningList(), layerBounds);
	        	if (layerBounds.getLlx()==0){
		        	log.debug("layer bounds:"+layerBounds);
		        	log.debug("antall pos i strekning:"+geoList.size());
		        	log.debug(strekning.getProdstrekning().getDigi()+" "+strekning.getProdstrekning().getTextForGUI()+" "+strekning.getProdstrekning().getFraKm()+" "+strekning.getProdstrekning().getTilKm()+start+" "+stopp);
	        	}
	        	
	        	Color color = null;
	        	if (defaultColor==null){
		        	if (strekning.getProdtypeList()==null){
			            short digi = Short.parseShort(strekning.getProdstrekning().getDigi());
			            boolean status = digi > 3;
			            color = status?green:red;
			        }else{
		            	//fargelegg ihht produksjonstypen.
		            	Color[] colors = new Color[strekning.getProdtypeList().size()];
		            	for (int i=0;i<strekning.getProdtypeList().size();i++){
		            		ProdtypeVO pt = strekning.getProdtypeList().get(i);
		            		colors[i] = ColorUtil.fromHexString(pt.getProdtype().getGuiFarge());
		            	}
		            	color = ColorUtil.mergeAllColors(colors);
		            }
	        	}else
	        		color = defaultColor;
	        	
	        	PeriodLineListLayer layer = new PeriodLineListLayer("", null, geoList, color, width,  false, layerBounds, start, stopp, strekning.getProdstrekning().getDfuId());
	        	layer.setGuid(guid);
	        	if (addTextBox){
	        		layer.setTextboxProducer(new ProdstrekningTextboxProducer(strekning));
	        	}
	        	
	        	if (!layer.getBounds().isNull()){
		        	layerList.add(layer);
		        	bounds.addBounds(layerBounds);
		        	
	        	}
        	}else{
        		log.debug("ProdReflinkStrekning's strekninger er null:"+strekning.getProdstrekning());
        	}
        }
        return layerList;
	}
    /**
     * Konverterer en liste med punkter til en liste med layere
     * @param vertices
     * @return
     */
    public List<PeriodLineListLayer> convertProdReflinkStrekningToLineListLayer(InspeksjonVO inspeksjon, Bounds bounds, boolean addTextBox, Color defaultColor){
        List<PeriodLineListLayer> layerList = new ArrayList<PeriodLineListLayer>();
        Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
        
        String value = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "antallPixlerProduksjon").getVerdi();
        int width = 1;
        
        if (value!=null)
    		width = Integer.parseInt(value);
        
        for (final ProdReflinkStrekning strekning:inspeksjon.getStrekninger()){
        	if (strekning.getStrekningList()!=null){
	        	final Date start = strekning.getProdstrekning().getFraTidspunkt();
	        	final Date stopp = strekning.getProdstrekning().getTilTidspunkt();
	        	Bounds layerBounds = new Bounds();
	        	List<MipssGeoPosition[]> geoList = convertReflinkStrekningListToGeoPositionList(strekning.getStrekningList(), layerBounds);
	        	if (layerBounds.getLlx()==0){
		        	log.debug("layer bounds:"+layerBounds);
		        	log.debug("antall pos i strekning:"+geoList.size());
		        	log.debug(strekning.getProdstrekning().getDigi()+" "+strekning.getProdstrekning().getTextForGUI()+" "+strekning.getProdstrekning().getFraKm()+" "+strekning.getProdstrekning().getTilKm()+start+" "+stopp);
	        	}
	        	
	        	Color color = null;
	        	if (defaultColor==null){
		        	if (strekning.getProdtypeList()==null){
			            short digi = Short.parseShort(strekning.getProdstrekning().getDigi());
			            boolean status = digi > 3;
			            color = status?green:red;
			        }else{
		            	//fargelegg ihht produksjonstypen.
		            	Color[] colors = new Color[strekning.getProdtypeList().size()];
		            	for (int i=0;i<strekning.getProdtypeList().size();i++){
		            		ProdtypeVO pt = strekning.getProdtypeList().get(i);
		            		colors[i] = ColorUtil.fromHexString(pt.getProdtype().getGuiFarge());
		            	}
		            	color = ColorUtil.mergeAllColors(colors);
		            }
	        	}else
	        		color = defaultColor;
	        	
	        	PeriodLineListLayer layer = new PeriodLineListLayer("", null, geoList, color, width,  false, layerBounds, start, stopp, strekning.getProdstrekning().getDfuId());
	        	layer.setGuid(inspeksjon.getInspeksjon().getGuid());
	        	if (addTextBox){
	        		layer.setTextboxProducer(new ProdstrekningTextboxProducer(strekning));
	        	}
	        	
	        	if (!layer.getBounds().isNull()){
		        	layerList.add(layer);
		        	bounds.addBounds(layerBounds);
	        	}
        	}else{
        		log.debug("ProdReflinkStrekning er null:"+strekning.getProdstrekning());
        	}
        }
        
        return layerList;
    }
	/**
     * Konverterer en liste med punkter til en liste med layere
     * @param vertices
     * @return
     */
    public List<PeriodLineListLayer> convertProdReflinkStrekningToLineListLayer(List<ProdReflinkStrekning> vertices, Bounds bounds, boolean addTextBox, Color defaultColor){
        List<PeriodLineListLayer> layerList = new ArrayList<PeriodLineListLayer>();
        Color green = new Color(0,255,0);
        Color red = new Color(255,0,0);
        
        String value = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "antallPixlerProduksjon").getVerdi();
        int width = 1;
        
        if (value!=null)
    		width = Integer.parseInt(value);
        
        for (final ProdReflinkStrekning strekning:vertices){
        	if (strekning.getStrekningList()!=null){
	        	final Date start = strekning.getProdstrekning().getFraTidspunkt();
	        	final Date stopp = strekning.getProdstrekning().getTilTidspunkt();
	        	Bounds layerBounds = new Bounds();
	        	List<MipssGeoPosition[]> geoList = convertReflinkStrekningListToGeoPositionList(strekning.getStrekningList(), layerBounds);
	        	if (layerBounds.getLlx()==0){
		        	log.debug("layer bounds:"+layerBounds);
		        	log.debug("antall pos i strekning:"+geoList.size());
		        	log.debug(strekning.getProdstrekning().getDigi()+" "+strekning.getProdstrekning().getTextForGUI()+" "+strekning.getProdstrekning().getFraKm()+" "+strekning.getProdstrekning().getTilKm()+start+" "+stopp);
	        	}
	        	
	        	Color color = null;
	        	if (defaultColor==null){
		        	if (strekning.getProdtypeList()==null){
			            short digi = Short.parseShort(strekning.getProdstrekning().getDigi());
			            boolean status = digi > 3;
			            color = status?green:red;
			        }else{
		            	//fargelegg ihht produksjonstypen.
		            	Color[] colors = new Color[strekning.getProdtypeList().size()];
		            	for (int i=0;i<strekning.getProdtypeList().size();i++){
		            		ProdtypeVO pt = strekning.getProdtypeList().get(i);
		            		colors[i] = ColorUtil.fromHexString(pt.getProdtype().getGuiFarge());
		            	}
		            	color = ColorUtil.mergeAllColors(colors);
		            }
	        	}else
	        		color = defaultColor;
	        	
	        	PeriodLineListLayer layer = new PeriodLineListLayer("", null, geoList, color, width,  false, layerBounds, start, stopp, strekning.getProdstrekning().getDfuId());
	        	if (addTextBox){
	        		layer.setTextboxProducer(new ProdstrekningTextboxProducer(strekning));
	        	}
	        	if (!layer.getBounds().isNull()){
		        	layerList.add(layer);
		        	bounds.addBounds(layerBounds);
	        	}
        	}else{
        		log.debug("ProdReflinkStrekning er null:"+strekning.getProdstrekning());
        	}
        }
        
        return layerList;
    }
	
	/**
     * Konverterer en liste med punkter til en liste med geoPosisjoner.
     * @param vertices
     * @param b
     * @return
     */
    public List<MipssGeoPosition[]> convertReflinkStrekningListToGeoPositionList(List<ReflinkStrekning> vertices, Bounds b){
        List<MipssGeoPosition[]> geoVertices = new ArrayList<MipssGeoPosition[]>();
        
        for (ReflinkStrekning point:vertices){
            //setter boundingboxen til produksjonen.
        	b.addBounds(point.getVectors());
        	Point prev=null;
        	int count = 0;
        	for (Point p:point.getVectors()){
        		if ((count!=0)){
        			MipssGeoPosition[] gp = new MipssGeoPosition[2];
        			gp[0] = new MipssGeoPosition(prev.getY(), prev.getX());
        			gp[1] = new MipssGeoPosition(p.getY(), p.getX());
        			geoVertices.add(gp);
        			prev = p;
        		}
        		if (count==0)
                  prev = p;
              count++;
        	}
        }
        return geoVertices;
    }

	public List<FeltLayer> getLayersFromPunktregsFromInspeksjon(List<PunktregVO> list, Map<MipssGeoPosition, FeltLayer> funnmap){
		List<FeltLayer> layers = new ArrayList<FeltLayer>();
		for (PunktregVO p:list){
			MipssGeoPosition gp = null;
			if (p.getSnappetX()==null||p.getSnappetY()==null){
				if (p.getY()==null||p.getX()==null)//mangler geoposisjon
					continue;
				
				gp = new MipssGeoPosition(p.getY(), p.getX());
			}else{
				gp = new MipssGeoPosition(p.getSnappetY(), p.getSnappetX());
			}
			
			Bounds b = new Bounds();
			b.setLlx(gp.getLongitude());
			b.setLly(gp.getLatitude());
			b.setUrx(gp.getLongitude());
			b.setUry(gp.getLatitude());
			FeltLayer l = null;
			if (p.getType().equals(PunktregVO.TYPE.FUNN)){
				FeltLayer cached = funnmap.get(gp);
				if (cached==null){
					l = new FeltLayer("funnPin"+p.getGuid(), false, FeltLayer.REGISTRERINGSTYPE.AVVIK, new GeoPosition(gp.getLatitude(), gp.getLongitude()), false);
					l.setTextboxProducer(new FunnTextboxProducer(loader, p.getGuid()));
					l.setPreferredLayerType(MipssMapLayerType.INSPEKSJONFUNN.getType());
					funnmap.put(gp, l);
					l.setBounds(b);
					layers.add(l);

				}else{
					((FunnTextboxProducer)cached.getTextboxProducer()).addAvvik(p.getGuid());
				}
			}
			if (p.getType().equals(PunktregVO.TYPE.HENDELSE)){
				l = new FeltLayer("hendelsePin"+p.getGuid(), false, FeltLayer.REGISTRERINGSTYPE.HENDELSE, new GeoPosition(gp.getLatitude(), gp.getLongitude()), false);
				l.setTextboxProducer(new HendelseTextboxProducer(loader, p.getGuid()));
				l.setPreferredLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
				l.setBounds(b);
				layers.add(l);
			}
			if (p.getType().equals(PunktregVO.TYPE.FORSIKRINGSSKADE)){
				l = new FeltLayer("forsikringsskadePin"+p.getGuid(), false, FeltLayer.REGISTRERINGSTYPE.FORSIKRINGSSKADE, new GeoPosition(gp.getLatitude(), gp.getLongitude()), false);
				l.setTextboxProducer(new ForsikringsskadeTextboxProducer(loader, p.getGuid()));
				l.setPreferredLayerType(MipssMapLayerType.INSPEKSJONHENDELSE.getType());
				l.setBounds(b);
				layers.add(l);
			}
			if (p.getType().equals(PunktregVO.TYPE.SKRED)){
				l = new FeltLayer("skredPin"+p.getGuid(), false, FeltLayer.REGISTRERINGSTYPE.SKRED, new GeoPosition(gp.getLatitude(), gp.getLongitude()), false);
				l.setTextboxProducer(new SkredTextboxProducer(loader, p.getGuid()));
				l.setPreferredLayerType(MipssMapLayerType.INSPEKSJONSKRED.getType());
				l.setBounds(b);
				layers.add(l);
			}
			
		}
		return layers;
	}
	/**
	/**
	 * Lager et layer i kartet for funn
	 * @param punkt
	 * @param bounds
	 * @param map
	 */
	public void getAvvikLayerFromAvvik(AvvikSokResult avvik, Bounds bounds, Map<MipssGeoPosition, FeltLayer> map) {
		Double x = avvik.getSnappetX()!=null?avvik.getSnappetX():avvik.getX();
		Double y = avvik.getSnappetY()!=null?avvik.getSnappetY():avvik.getY();
		if (x==null||y==null)
			return;
		MipssGeoPosition gp = new MipssGeoPosition(y, x);
		
		
		FeltLayer cached = map.get(gp);
		if (cached==null){
			Bounds b = new Bounds();
			b.setLlx(gp.getLongitude());
			b.setLly(gp.getLatitude());
			b.setUrx(gp.getLongitude());
			b.setUry(gp.getLatitude());
			bounds.addBounds(b);
			FeltLayer l = new FeltLayer("avvikPin"+avvik.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.AVVIK, new GeoPosition(gp.getLatitude(), gp.getLongitude()), false);
			l.setBounds(b);
			l.setTextboxProducer(new AvvikTextboxProducer(loader, avvik.getGuid()));
			l.setPreferredLayerType(MipssMapLayerType.FUNN.getType());
			map.put(gp, l);
		}else{
			log.debug("using cached instance...");
			((AvvikTextboxProducer)cached.getTextboxProducer()).addAvvikGuid(avvik.getGuid());
		}
	}
	
	public FeltLayer getPinLayerFromAvvik(Avvik avvik, Bounds bounds){
		Punktstedfest punkt = avvik.getStedfesting();
		MipssGeoPosition gp = new MipssGeoPosition(punkt.getY(), punkt.getX());
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("avvikPin"+avvik.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.AVVIK, new GeoPosition(gp.getLatitude(), gp.getLongitude()), false);
		l.setBounds(b);
		l.setTextboxProducer(new AvvikTextboxProducer(loader, avvik));
		l.setPreferredLayerType(MipssMapLayerType.FUNN.getType());
		return l;
	}
	public FeltLayer getHendelseLayer(Hendelse h, Bounds bounds){
		if (h.getSak().getStedfesting().size()==0)
			return null;
		
		Punktstedfest p = h.getSak().getStedfesting().get(0); 
		Double x = p.getSnappetX()!=null?p.getSnappetX():p.getX();
		Double y = p.getSnappetY()!=null?p.getSnappetY():p.getY();
		if (x==null||y==null)
			return null;
		GeoPosition gp = new GeoPosition(p.getY(), p.getX());
		
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("hendelsePin"+h.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.HENDELSE, gp, false);
		l.setTextboxProducer(new HendelseSokViewTextboxProducer(loader, h));
		l.setPreferredLayerType(MipssMapLayerType.HENDELSE.getType());
		l.setBounds(b);
		return l;
	}
	
	public FeltLayer getHendelseLayer(RskjemaSokResult h, Bounds bounds){
		Double x = h.getSnappetX()!=null?h.getSnappetX():h.getX();
		Double y = h.getSnappetY()!=null?h.getSnappetY():h.getY();
		if (x==null||y==null)
			return null;
		GeoPosition gp = new GeoPosition(y, x);
		
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("hendelsePin"+h.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.HENDELSE, gp, false);
		l.setTextboxProducer(new HendelseSokViewTextboxProducer(loader, h.getGuid()));
		l.setPreferredLayerType(MipssMapLayerType.HENDELSE.getType());
		l.setBounds(b);
		return l;
	}
	
	public FeltLayer getSkredLayer(Skred skred, Bounds bounds){
		Punktstedfest p = skred.getSak().getStedfesting().get(0); 
		GeoPosition gp = new GeoPosition(p.getY(), p.getX());
		
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("skredPin"+skred.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.SKRED, gp, false);
		l.setTextboxProducer(new SkredTextboxProducer(loader, skred));
		l.setPreferredLayerType(MipssMapLayerType.SKRED.getType());
		l.setBounds(b);
		return l;
	}
	public FeltLayer getSkredLayer(RskjemaSokResult skred, Bounds bounds){
		Double x = skred.getSnappetX()!=null?skred.getSnappetX():skred.getX();
		Double y = skred.getSnappetY()!=null?skred.getSnappetY():skred.getY();
		if (x==null||y==null)
			return null;
		GeoPosition gp = new GeoPosition(y, x);
		
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("skredPin"+skred.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.SKRED, gp, false);
		l.setTextboxProducer(new SkredTextboxProducer(loader, skred.getGuid()));
		l.setPreferredLayerType(MipssMapLayerType.SKRED.getType());
		l.setBounds(b);
		return l;
	}
	public FeltLayer getForsikringsskadeLayer(Forsikringsskade fs, Bounds bounds){
		Punktstedfest p = fs.getSak().getStedfesting().get(0); 
		GeoPosition gp = new GeoPosition(p.getY(), p.getX());
		
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("forsikringsskadePin"+fs.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.FORSIKRINGSSKADE, gp, false);
		l.setTextboxProducer(new ForsikringsskadeTextboxProducer(loader, fs));
		l.setPreferredLayerType(MipssMapLayerType.FORSIKRINGSSKADE.getType());
		l.setBounds(b);
		return l;
	}
	public FeltLayer getForsikringsskadeLayer(RskjemaSokResult fs, Bounds bounds){
		Double x = fs.getSnappetX()!=null?fs.getSnappetX():fs.getX();
		Double y = fs.getSnappetY()!=null?fs.getSnappetY():fs.getY();
		if (x==null||y==null)
			return null;
		GeoPosition gp = new GeoPosition(y, x);
		
		Bounds b = new Bounds();
		b.setLlx(gp.getLongitude());
		b.setLly(gp.getLatitude());
		b.setUrx(gp.getLongitude());
		b.setUry(gp.getLatitude());
		bounds.addBounds(b);
		FeltLayer l = new FeltLayer("forsikringsskadePin"+fs.getRefnummer(), false, FeltLayer.REGISTRERINGSTYPE.FORSIKRINGSSKADE, gp, false);
		l.setTextboxProducer(new ForsikringsskadeTextboxProducer(loader, fs.getGuid()));
		l.setPreferredLayerType(MipssMapLayerType.FORSIKRINGSSKADE.getType());
		l.setBounds(b);
		return l;
	}
	/**
	 * Lager et layer av en liste med prodpunkter. 
	 * @param list prodpunktene som skal bli til et layer
	 * @param color fargen til punktene i kartet. 
	 * @return
	 */
	public List<Layer> getInspeksjonListLayerFromProdpunkVO(List<List<ProdpunktVO>> list, Color color){
		List<Layer> layerlist = new ArrayList<Layer>();
		for (List<ProdpunktVO> pl:list){
			layerlist.add(getInspeksjonLayerFromProdpunkVO(pl, color));
		}
		return layerlist;
	}
	
	/**
	 * Lager et layer av en liste med prodpunkter. 
	 * @param list prodpunktene som skal bli til et layer
	 * @param color fargen til punktene i kartet. 
	 * @return
	 */
	public PointLayerCollection getInspeksjonLayerFromProdpunkVO(List<ProdpunktVO> list, Color color){
		Bounds bounds = new Bounds();
		List<ProdpunktPointLayer> region = convertInspeksjonPunktVOToPointLayers(list, bounds, color);
		PointLayerCollection lcoll = new PointLayerCollection("", region, bounds, false);
		lcoll.setCollectionPainter(new CollectionPainter(region, lcoll));
		return lcoll;
	}
	
	public Layer getLayerFromKontraktveinettVO(KontraktveinettVO veinett){
		Bounds bounds = new Bounds();
		List<MipssGeoPosition[]> layerList = convertReflinkStrekningListToGeoPositionList(veinett.getVeinettIndices(), bounds);
		String value = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "antallPixlerKontraktveinett").getVerdi();
		int width = 1;
        if (value!=null)
    		width = Integer.parseInt(value);    	
		LineListLayer layer = new LineListLayer(""+veinett.getKontrakt().getId().intValue(), null, layerList, Color.yellow, width, true, bounds, false);
		layer.setTextboxProducer(new KontraktveiTextboxProducer(veinett.getKontrakt()));
		List<LineListLayer> list = new ArrayList<LineListLayer>();
        list.add(layer);
		LineListLayerCollection col = new LineListLayerCollection(veinett.getKontrakt().getId().intValue()+"", list, layer.getBounds(), true);
		col.setCollectionPainter(new CollectionPainter(list, col));
		return col;
	}
	
	/**
	 * 
	 * @param veinett
	 * @return
	 */
	public KontraktveiCollectionLayer getKontraktveiLayer(KontraktveinettVO veinett){		
		List<ReflinkStrekning> evs = new ArrayList<ReflinkStrekning>();
		List<ReflinkStrekning> rvs = new ArrayList<ReflinkStrekning>();
		List<ReflinkStrekning> fvs = new ArrayList<ReflinkStrekning>();
		List<ReflinkStrekning> kvs = new ArrayList<ReflinkStrekning>();
		
		for (ReflinkStrekning str:veinett.getVeinettIndices()){
			if (str.getVeikategori().toLowerCase().equals("e")){
				evs.add(str);
			}
			if (str.getVeikategori().toLowerCase().equals("r")){
				rvs.add(str);
			}
			if (str.getVeikategori().toLowerCase().equals("f")){
				fvs.add(str);
			}
			if (str.getVeikategori().toLowerCase().equals("k")){
				kvs.add(str);
			}
		}
		Bounds evBounds = new Bounds();
		Bounds rvBounds = new Bounds();
		Bounds fvBounds = new Bounds();
		Bounds kvBounds = new Bounds();
		
		List<MipssGeoPosition[]> evList = convertReflinkStrekningListToGeoPositionList(evs, evBounds);
		List<MipssGeoPosition[]> rvList = convertReflinkStrekningListToGeoPositionList(rvs, rvBounds);
		List<MipssGeoPosition[]> fvList = convertReflinkStrekningListToGeoPositionList(fvs, fvBounds);
		List<MipssGeoPosition[]> kvList = convertReflinkStrekningListToGeoPositionList(kvs, kvBounds);
		
		String value = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "antallPixlerKontraktveinett").getVerdi();
		int width = 1;
        if (value!=null)
    		width = Integer.parseInt(value);
        
        KontraktveiLayer evLayer = new KontraktveiLayer("EV"+veinett.getKontrakt().getId(), evList, evs, Color.yellow, width, evBounds, false);
        KontraktveiLayer rvLayer = new KontraktveiLayer("RV"+veinett.getKontrakt().getId(), rvList, rvs, Color.yellow, width, rvBounds, false);
        KontraktveiLayer fvLayer = new KontraktveiLayer("FV"+veinett.getKontrakt().getId(), fvList, fvs, Color.yellow, width, fvBounds, false);
        KontraktveiLayer kvLayer = new KontraktveiLayer("KV"+veinett.getKontrakt().getId(), kvList, kvs, Color.yellow, width, kvBounds, false);
        KontraktveiTextboxProducer evInfo = new KontraktveiTextboxProducer(veinett.getKontrakt());
        KontraktveiTextboxProducer rvInfo = new KontraktveiTextboxProducer(veinett.getKontrakt());
        KontraktveiTextboxProducer fvInfo = new KontraktveiTextboxProducer(veinett.getKontrakt());
        KontraktveiTextboxProducer kvInfo = new KontraktveiTextboxProducer(veinett.getKontrakt());
        evInfo.setVeinettInfo("Veinettstatus");
        evInfo.setVeinettInfoValue("EV");
        rvInfo.setVeinettInfo("Veinettstatus");
        rvInfo.setVeinettInfoValue("RV");
        fvInfo.setVeinettInfo("Veinettstatus");
        fvInfo.setVeinettInfoValue("FV");
        kvInfo.setVeinettInfo("Veinettstatus");
        kvInfo.setVeinettInfoValue("KV");
        evLayer.setTextboxProducer(evInfo);
        rvLayer.setTextboxProducer(rvInfo);
        fvLayer.setTextboxProducer(fvInfo);
        kvLayer.setTextboxProducer(kvInfo);
        
		List<KontraktveiLayer> list = new ArrayList<KontraktveiLayer>();
		list.add(evLayer);
		list.add(rvLayer);
		list.add(fvLayer);
		list.add(kvLayer);
		Bounds b = new Bounds();
		b.setBounds(evBounds);
		b.addBounds(rvBounds);
		b.addBounds(fvBounds);
		b.addBounds(kvBounds);
		
        KontraktveiCollectionLayer col = new KontraktveiCollectionLayer(veinett.getKontrakt().getId().intValue()+"", list, b);
		col.setCollectionPainter(new CollectionPainter(list, col));
		return col;
	}
	
	/**
	 * Lager et layer til kartet fra en liste med prodstrekninger. 
	 * @param list
	 * @return
	 */
	public Layer getLayerFromProdstrekningList(List<Prodstrekning> list) {
		if (list==null||list.size()==0){
			return null;
		}
		List<ReflinkSeksjon> reflinkSeksjonList = new ArrayList<ReflinkSeksjon>();
		for (Prodstrekning p:list){
			List<ReflinkSeksjon> reflinkSeksjoner = NvdbTranslator.translateFromProdreflinkSeksjonToReflinkseksjon(p.getProdreflinkseksjonList());
			reflinkSeksjonList.addAll(reflinkSeksjoner);
		}
		List<ReflinkStrekning> geometry = getNvdbWebservice().getGeometryWithinReflinkSections(SerializableReflinkSection.createSerializableSections(reflinkSeksjonList));
		Layer l = convertPointVerticesToLineListLayer(geometry);
		return l;
	}

	/**
	 * Lager et layer for en rode
	 * 
	 * @param rode Roden som skal lages layer av, denne brukes til å lage textboksdata for layeret
	 * @param veinett veinettet til roden, med tilhørende Veinettreflinkseksjon 
	 * @return
	 */
	public Layer getLayerFromRode(final Rode rode, Veinett veinett){
		if (veinett.getVeinettreflinkseksjonList()==null)
			throw new IllegalArgumentException ("Reflinkseksjonlisten til veinettet kan ikke være null: veinett_id="+veinett.getId());
		if (veinett.getVeinettreflinkseksjonList().size()==0)
			throw new IllegalArgumentException ("Veinettet inneholder ingen veinett, umulig å lage layer: veinett_id="+veinett.getId());
		List<ReflinkStrekning> rodeGeometry = getNvdbWebservice().getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(veinett.getVeinettreflinkseksjonList());
		Bounds bounds = new Bounds();
		List<MipssGeoPosition[]> mapGeometry = convertReflinkStrekningListToGeoPositionList(rodeGeometry, bounds);
    	int px = veinett.getGuiTykkelse();
    	if (px==0){
    		px = Integer.parseInt(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mipssmap", "antallPixlerRodeDefault").getVerdi());
    	}
		LineListLayer layer = new LineListLayer(""+rode.getId().intValue(), null, mapGeometry, ColorUtil.fromHexString(veinett.getGuiFarge()), px, false, bounds, false);
		layer.setTextboxProducer(new RodeTextboxProducer(rode));
		layer.setDrawOutsideViewport(false);
		
		
        List<LineListLayer> list = new ArrayList<LineListLayer>();
        list.add(layer);
		LineListLayerCollection col = new LineListLayerCollection(rode.getId().intValue()+"", list, layer.getBounds(), false);
		col.setCollectionPainter(new CollectionPainter(list, col));
		return col;
	}
	
	public LineListLayer getLayerFromVeinettveireferanse(Veinettveireferanse veiref){
		List<Veinettreflinkseksjon> reflink = getNvdbWebservice().getReflinkSectionsWithinRoadref(veiref);
		List<ReflinkStrekning> geometry = getNvdbWebservice().getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(reflink);
		if (geometry==null||geometry.size()==0)
			return null;
		LineListLayer l = convertPointVerticesToLineListLayer(geometry);
		return l;
	}
	
	public LineListLayer getLayerFromVeinettveireferanseList(List<Veinettveireferanse> list){
		if (list==null||list.size()==0)
			return null;
		List<Veinettreflinkseksjon> reflinks = new ArrayList<Veinettreflinkseksjon>();
		for (Veinettveireferanse v:list){
			reflinks.addAll(getNvdbWebservice().getReflinkSectionsWithinRoadref(v));
		}
		List<ReflinkStrekning> geometry = getNvdbWebservice().getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(reflinks);
		if (geometry==null||geometry.size()==0)
			return null;
		LineListLayer l = convertPointVerticesToLineListLayer(geometry);
		return l;
	}
	
	private NvdbWebservice getNvdbWebservice() {
    	if (service == null){
    		service = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
        }
    	return service;
	}
	

	/**
	 * Lager et layer fra en liste med StrekningVO objekter. 
	 * @param strekninger
	 * @return
	 */
	public LineListLayerCollection getPeriodLineListLayerFromProdReflinkStrekning(List<ProdReflinkStrekning> strekninger){
		Bounds bounds = new Bounds();
		List<PeriodLineListLayer> lineListLayers = convertProdReflinkStrekningToLineListLayer(strekninger, bounds, true, null);
		LineListLayerCollection l = new LineListLayerCollection("", lineListLayers, bounds, true);
		log.debug("creating periodlinelistlayer with bounds:"+bounds);
		if (bounds.isNull()){
			return null;
		}
        l.setCollectionPainter(new CollectionPainter(l.getLayers(), l));
		return l;
	}
	
	/**
	 * Lager et layer av hendelser
	 * @param punktList
	 * @return
	 */
	public Layer getPointLayerCollectionFromHendelser(List<RskjemaSokResult> hendelseList){
		if (hendelseList.size()==0)
			return null;
		List<FeltLayer> layers = new ArrayList<FeltLayer>();
		Bounds bounds = new Bounds();
		for (RskjemaSokResult hendelse:hendelseList){
			FeltLayer hl = getHendelseLayer(hendelse, bounds);
			if (hl==null)
				continue;
			layers.add(hl);
		}
		return new PointLayerCollection("hendelsePins", layers, bounds, true);
	}

	/**
	 * Lager et layer av avvik
	 * @param punktList
	 * @return
	 */
	public Layer getPointLayerCollectionFromAvvik(List<AvvikSokResult> punktList){
		if (punktList.size()==0)
			return null;
		List<FeltLayer> layers = new ArrayList<FeltLayer>();
		Map<MipssGeoPosition, FeltLayer> map = new HashMap<MipssGeoPosition, FeltLayer>();
		Bounds bounds = new Bounds();
		for (AvvikSokResult avvik:punktList){
//			PinLayer al = getPinLayerFromAvvik(avvik, bounds);
			getAvvikLayerFromAvvik(avvik, bounds, map);
		}
		
		Iterator<MipssGeoPosition> it = map.keySet().iterator();
		while (it.hasNext()){
			layers.add(map.get(it.next()));
		}
		return new PointLayerCollection("avvikPins", layers, bounds, true);
	}
	/**
	 * Lager et layer av forsikringsskade
	 * @param punktList
	 * @return
	 */
	public Layer getPointLayerCollectionFromForsikringsskade(List<RskjemaSokResult> fsList){
		if (fsList.size()==0)
			return null;
		List<FeltLayer> layers = new ArrayList<FeltLayer>();
		Bounds bounds = new Bounds();
		for (RskjemaSokResult fs:fsList){
			FeltLayer hl = getForsikringsskadeLayer(fs, bounds);
			if (hl==null)
				continue;
			layers.add(hl);
		}
		return new PointLayerCollection("forsikringsskadePins", layers, bounds, true);
	}
	/**
	 * Lager et layer av skred
	 * @param punktList
	 * @return
	 */
	public Layer getPointLayerCollectionFromSkred(List<RskjemaSokResult> skredList){
		if (skredList.size()==0)
			return null;
		List<FeltLayer> layers = new ArrayList<FeltLayer>();
		Bounds bounds = new Bounds();
		for (RskjemaSokResult skred:skredList){
			FeltLayer hl = getSkredLayer(skred, bounds);
			if (hl==null)
				continue;
			layers.add(hl);
		}
		return new PointLayerCollection("skredPins", layers, bounds, true);
	}

}

