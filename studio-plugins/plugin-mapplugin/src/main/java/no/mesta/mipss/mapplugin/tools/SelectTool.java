package no.mesta.mipss.mapplugin.tools;

import java.awt.event.MouseEvent;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.MovableLayer;
import no.mesta.mipss.mapplugin.layer.SelectableLayer;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jfree.chart.JFreeChart;

public class SelectTool implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MapPanel mapPanel;
    
    public SelectTool(MapPanel mapPanel) {
        this.mapPanel = mapPanel;
    }

    public void mouseClicked(MouseEvent e) {
    	log.debug("mouseClicked...");
    	e.consume();
    	if (e.getButton()==MouseEvent.BUTTON1){
    		log.debug("button1");
    		MovableLayer layer = mapPanel.getLayerHandler().getMovableLayerAt(e.getPoint());
    		log.debug("found layer:"+layer);
	        if (!(layer instanceof TextboxWidget)){
		    	mapPanel.getLayerHandler().unselectAll();
		    	log.debug("all unselected");
		        GeoPosition tempPos = mapPanel.convertPointToGeoPosition(e.getPoint());
		        log.debug("converted to geoposition");
		        MipssGeoPosition pos = new MipssGeoPosition(tempPos.getLatitude(), tempPos.getLongitude());
		        double res = mapPanel.getResolutions().getResolutionForZoom(mapPanel.getZoom());
		        String searchRadius = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "selectionSearchRadius").getVerdi();
		        log.debug("got searchradius:"+searchRadius);
		        int radius = 5;
		        if (searchRadius!=null)
		        	radius = Integer.parseInt(searchRadius);
		        log.debug("selecting layer:"+pos+" "+(res*radius));
		        SelectableLayer l = mapPanel.getLayerHandler().selectLayer(pos, res*radius);
		        log.debug("selected layer:"+l);
	        }
	        
    	}
    }
    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }
}
