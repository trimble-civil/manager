package no.mesta.mipss.mapplugin;

import java.io.Serializable;

/**
 * @author <a mailto:harkul@mesta.no>Harald A Kulø</a>
 */
public class MipssGeoPosition implements Serializable{
	
	private double latitude;
	private double longitude;
	
	public MipssGeoPosition(double lat, double lon){
		this.latitude = lat;
		this.longitude = lon;	
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public boolean equals(Object o){
		if (!(o instanceof MipssGeoPosition)) return false;
		if (o==null) return false;
		if (o==this) return true;
		
		MipssGeoPosition other = (MipssGeoPosition)o;
		return other.getLongitude()==getLongitude()&&other.getLatitude()==getLatitude();
	}
	
	public int hashCode(){
		return (""+getLongitude()+getLatitude()).hashCode();
	}
	
	public String toString(){
		return "lat:"+latitude+" lon:"+longitude;
	}
	
	
}
