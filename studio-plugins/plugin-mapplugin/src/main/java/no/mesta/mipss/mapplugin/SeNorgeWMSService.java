package no.mesta.mipss.mapplugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.mesta.mipss.mapplugin.utm.UTMTileFactoryInfo;
/**
 * WMSService for arcus.nve.no
 * 
 * @author @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 * Disse lagene støttes av wms tjenesten:
 * 
 * "swepr" gir Snømengde i prosent
 * "swerk" gir Snømengde rangert
 * "swewk" gir Endring i snømengde siste uke
 * "sd" gir Snødybde
 * "fsw" gir Nysnømengde siste døgn
 * "fswwk" gir Nysnømengde siste uke
 * "sdfsw" gir Nysnødybde siste døgn
 * "qsw" gir Snøsmelting siste døgn
 * "qswwk" gir Snøsmelting siste uke
 * "lwc" gir Snøtilstand
 * "ski" gir Skiføre
 * "qtt" gir Sum regn og snøsmelting for døgnet
 * "qttwk" gir Sum regn og snøsmelting for uken. Døgndata går normalt frem til og med morgendagen.
 */
public class SeNorgeWMSService extends UTMWMSService{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private UTMTileFactoryInfo info;
	
	public SeNorgeWMSService(){
		setBaseUrl("http://arcus.nve.no/WMS_server/wms_server.aspx?");
	}
    /**
     * Sets the UTMTileFactoryInfo for the map server
     * @param info
     */
	public void setInfo(UTMTileFactoryInfo info) {
		this.info = info;
	}
	
	public UTMTileFactoryInfo getInfo(){
		return info;
	}
	
	public String toWMSURL(int x, int y, int zoom, int tileSize) {
		
        String format = "image/png";
        String srs = "EPSG:32633";
        String tiled = "true";
        String service = "wms";
        String version = "1.1.1";
        String request = "GetMap";
        String exceptions = "application/vnd.ogc.se_inimage";

        if  (zoom>info.getResolutions().getMaxResolutionIndex())
            return null;
        //if ((info.getResolutions().getResolutions().length-zoom-1)<0)
        //    return null;
           // log.debug(zoom);
        double resolution = info.getResolutions().getResolutionForZoom(zoom);//[info.getResolutions().length-zoom-1];
       // log.debug(resolution);
        //ll, ur bounds for the map
        Bounds bounds = info.getBounds();
        //calculate ll and ur
        double llx = bounds.getLlx() + (resolution * x * tileSize);
        double lly = bounds.getLly() + (resolution * y * tileSize);
        double urx = bounds.getLlx() + (resolution * (x+1) * tileSize);
        double ury = bounds.getLly() + (resolution * (y+1) * tileSize);
        
        String bbox = llx + "," +lly+","+urx+","+ury;
        
        //create wms-request
        String url = getBaseUrl() + 
        				"VERSION="+version+
        				"&REQUEST="+request+
        				"&STYLES="+
        				"&SRS="+srs+
        				"&FORMAT="+format+
        				"&BBOX="+bbox+
        				"&HEIGHT="+tileSize+
                        "&WIDTH="+tileSize+
        				"&LAYERS=lwc"+
                        "";
//            log.debug(url);
        return url;
    }
}
