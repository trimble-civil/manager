package no.mesta.mipss.mapplugin.tools;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import java.awt.geom.Point2D;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXMapViewer;

/**
 * A tool to pan the map e-w-s-n. Originally a part of JXMapViewer but extracted
 * in order to manage it. 
 * 
 * @author Harald A. Kulø
 */
public class PanMapTool implements MouseInputListener {
    private Point prev;
    
    private JXMapViewer comp;
    
    /**
     * Constructor
     * @param comp the map to be panned
     */
    public PanMapTool(JXMapViewer comp){
        this.comp = comp;
        comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
    
    /**
     * Event for when the mouse is pressed. If the mouse is pressed twice
     * the map should be recentered. Else rememer the point for panning
     * @param evt
     */
    public void mousePressed(MouseEvent evt) {
        //if the middle mouse button is clicked, recenter the view
        if (comp.isRecenterOnClickEnabled() && (SwingUtilities.isMiddleMouseButton(evt) ||
                (SwingUtilities.isLeftMouseButton(evt) && evt.getClickCount() == 2))) {
            recenterMap(evt);
        } else {
            //otherwise, just remember this point (for panning)
            prev = evt.getPoint();
        }
    }
    
    /**
     * Recenter the map
     * @param evt
     */
    private void recenterMap(MouseEvent evt) {
        Rectangle bounds = comp.getViewportBounds();
        double x = bounds.getX() + evt.getX();
        double y = bounds.getY() + evt.getY();
        comp.setCenter(new Point2D.Double(x, y));
        comp.repaint();
    }
    
    /**
     * Pan the map.
     * @param evt
     */
    public void mouseDragged(MouseEvent evt) {
        if(comp.isPanEnabled()) {
        	
            Point current = evt.getPoint();
            if (prev==null)
            	prev = current;
            double x = comp.getCenter().getX() - (current.x - prev.x);
            double y = comp.getCenter().getY() - (current.y - prev.y);
            
            //if(!comp.isNegativeYAllowed) {
                if (y < 0) {
                    y = 0;
                }
            //}
            
            int maxHeight = (int)(comp.getTileFactory().getMapSize(comp.getZoom()).getHeight()*comp.getTileFactory().getTileSize(comp.getZoom()));
            if (y > maxHeight) {
                y = maxHeight;
            }
            
            prev = current;
            comp.setCenter(new Point2D.Double(x, y));
            comp.repaint();
//            comp.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(IconResources.HAND_CLOSED_ICON.getImage(), new Point(0,0), "closed hand"));
            comp.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(IconResources.HAND_CLOSED_ICON.getImage(), new Point(16,16), "ClosedHandCursor"));
//            comp.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        }
    }
    
    public void mouseReleased(MouseEvent evt) {
        prev = null;
//        comp.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(IconResources.HAND_OPEN_ICON.getImage(), new Point(16,16), "ClosedHandCursor"));
        comp.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
    
    public void mouseMoved(MouseEvent e) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                comp.requestFocusInWindow();
            }
        });
    }
    
    public void mouseClicked(MouseEvent e) {
    }
    
    public void mouseEntered(MouseEvent e) {
    }
    
    public void mouseExited(MouseEvent e) {
    }
}