package no.mesta.mipss.mapplugin.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.MovableLayer;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.ui.picturepanel.DefaultMipssPictureModel;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.painter.BusyPainter;

/**
 * A layer that draws a Textbox on the map.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class TextboxWidget extends Widget implements MovableLayer{ 
    
    private Timer busy;
    private BusyPainter busyPainter;
    
    private List<TextboxData> data;
    
    private Font dataFont = new Font(null, Font.PLAIN, 11);
    private Font titleFont = new Font(null, Font.BOLD, 13);
    private Font subTitleFont = new Font(null, Font.BOLD, 11);
    
    private Point location = new Point(10,10);
    private Point dragStartPoint;
    
    private int dataWidth;
    private int subtitleWidth;
    private int height;
	private int width;
	private int currentDataIndex = 0;

	private Map<Rectangle, Bilde> images = new HashMap<Rectangle, Bilde>();
	private Rectangle imageButton;
    private Rectangle actionButton;
    private Rectangle closeButton;
    private Polygon leftArrow;
    private Polygon rightArrow;
    
    private Color rightArrowColor = Color.white;
       
    private boolean actionOn;
    private boolean removeOnClick = true;
    private boolean inited;
    private boolean imageButtonDown;
    
    private String waitInfo =null;
	
	public TextboxWidget(MapPanel map, int pad, GLUE glue, List<TextboxData> data){
		super(map, 0, 0, pad, glue);
		this.data = data;
	}
	public TextboxWidget(String id, MapPanel map, int pad, GLUE glue, List<TextboxData> data){
		super(id, map, 0, 0, pad, glue);
		this.data = data;
	}
    public void action(){
		Action action = data.get(currentDataIndex).getAction();
		if (action!=null){
			action.actionPerformed(null);
		}
	}
    
    public void buttonDown(){
		actionOn = true;
	}
    
    public void buttonUp(){
		actionOn= false;
	}
    
    private int calcHeight(int lineHeight) {
		int h = data.get(currentDataIndex).getData().size()*lineHeight+40;
		//Image in textbox, append height and check width
        if (data.get(currentDataIndex).getThumbs().size()>0){
        	int thumbwidth = 0;
        	//to bilder i bredden..
        	for (int i=0;i<data.get(currentDataIndex).getThumbs().size();i+=2){
        		ImageIcon ic = data.get(currentDataIndex).getThumbs().get(i);
        		h += ic.getIconHeight();
        		thumbwidth = ic.getIconHeight();
        	}
        	if (thumbwidth>0){
        		if (data.get(currentDataIndex).getThumbs().size()>1){
        			if ((thumbwidth*2)>width){
            			width = thumbwidth*2 +25;
            		}
        		}else{
        			if (thumbwidth>width){
        				width = thumbwidth+25;
        			}
        		}
        	}
        	h +=30;
        }
        return h;
	}
    private int calculateWidth(Graphics2D g) {
		int calcWidth = 0;
		
		for (TextboxData.TextboxLine s:data.get(currentDataIndex).getData()){
			
            int sw = g.getFontMetrics(subTitleFont).stringWidth(s.getTitle()+" : ");
            if (sw>subtitleWidth){
            	subtitleWidth= sw;
            }
            int dw = 0;
            if (s.getData()!=null)
            		dw = g.getFontMetrics(dataFont).stringWidth(s.getData());
            if (dw>dataWidth)
            	dataWidth = dw;
        }
        calcWidth = subtitleWidth+dataWidth+20;
        int tw = g.getFontMetrics(titleFont).stringWidth(data.get(currentDataIndex).getTitle());
        if (tw>calcWidth){
        	calcWidth = tw+20;
        }
        if (calcWidth<220){
        	int images = data.get(currentDataIndex).getThumbs()!=null?data.get(currentDataIndex).getThumbs().size():0;
        	if (images>1){
        		calcWidth=220;
        	}
        }
        
        return calcWidth;
	}
    
    private void drawActionButton(Graphics2D g, Action a) {
		String txt = (String)a.getValue("TEXT");
		int x = location.x+width -55;
		int y = location.y+5;
		int wi = 10+g.getFontMetrics().stringWidth(txt);
		int hi = 20;
		if (actionOn){
			paintFakeButton(g, x, y, wi, hi, txt, false);
		}else{
			paintFakeButton(g, x, y, wi, hi, txt, true);
		}
		actionButton = new Rectangle(x, y, wi, hi);
	}
    
    private void drawCloseButton(Graphics2D g){
    	int x = location.x+5;
    	int y = location.y+5;
    	int w = 16;
    	int h = 16;
    	
		paintFakeButton(g, x, y, w, h, "", true);
		x+=4;
		y+=4;
    	
    	g.setStroke(new BasicStroke(2));
    	g.setColor(textColor);
    	g.drawLine(x, y, x+8, y+8);
    	g.drawLine(x+8, y, x, y+8);
    	
    	closeButton = new Rectangle(x, y, w, h);
    	
    }
	private void drawBackground(Graphics2D g) {
		g.setPaint(paneColor);
        g.fillRoundRect(location.x, location.y, width, height, 10, 10);
	}
	
	private void drawBorder(Graphics2D g) {
		g.setPaint(borderColorT);
        g.setStroke(borderStroke);
        g.drawRoundRect(location.x,location.y,width, height, 10, 10);
        g.setPaint(borderColor);
        g.setStroke(borderStrokeOne);
        int lw = (int)strokeWidth/2;
        g.drawRoundRect(location.x+lw,location.y+lw,width-strokeWidth, height-strokeWidth, 10, 10);
	}
	
	private void drawImages(Graphics2D g, int lineHeight, int i) {
		images.clear();
		String s = "Vis store bilder";
		int wi = 100;
		int hi = 22;
		if (imageButtonDown){
			paintFakeButton(g, location.x+((width-wi)/2), i-lineHeight/2, wi, hi, s, false);
		}else{
			paintFakeButton(g, location.x+((width-wi)/2), i-lineHeight/2, wi, hi, s, true);
		}
		imageButton = new Rectangle(location.x+((width-wi)/2), i-lineHeight/2, wi, hi);
		i+=hi;
		int posx = location.x+10;
		int posy = i;
		
		for (int j=0;j<data.get(currentDataIndex).getBilder().size();j++){
			ImageIcon ic1 = data.get(currentDataIndex).getThumbs().get(j);
			
			if ((j%2==0&&j!=0)){
				posy +=ic1.getIconHeight();
				posx = location.x+10;
			}else if (j!=0)
				posx +=ic1.getIconWidth();
			
			
			g.drawImage(ic1.getImage(), posx, posy, null);
			images.put(new Rectangle(posx, posy, ic1.getIconWidth(), ic1.getIconHeight()), data.get(currentDataIndex).getBilder().get(j));
		}
	}
    
    private void drawNavigationButtons(Graphics2D g) {
		Color arrowColor = null;
		if (currentDataIndex==0)
			arrowColor=textColorT;
		else{
			arrowColor = textColor;
		}
		int center = width/2;
		int aSize = 16;
		int lpx = center-50;
		int hpx = center+50-aSize;
		int apy = 10;
		
		leftArrow = new Polygon();
		leftArrow.addPoint(location.x+lpx, location.y+apy+aSize/2);
		leftArrow.addPoint(location.x+lpx+aSize, location.y+apy);
		leftArrow.addPoint(location.x+lpx+aSize, location.y+apy+aSize);
		g.setPaint(arrowColor);
		g.fillPolygon(leftArrow);
		
		g.setColor(textColor);
		g.drawString(""+(currentDataIndex+1)+" av "+ data.size(), location.x+center-15, location.y+apy+10);
		if (currentDataIndex+1==data.size())
			rightArrowColor = textColorT;
		else
			rightArrowColor = textColor;
		rightArrow = new Polygon();
		rightArrow.addPoint(location.x+hpx, location.y+apy);
		rightArrow.addPoint(location.x+hpx, location.y+apy+aSize);
		rightArrow.addPoint(location.x+hpx+aSize, location.y+apy+aSize/2);
		g.setPaint(rightArrowColor);
		g.fillPolygon(rightArrow);
	}
    
	private int drawText(Graphics2D g, int lineHeight, Rectangle2D titleBounds) {
		g.setFont(dataFont);
        int i = location.y+40+(int)titleBounds.getHeight();
        for (TextboxData.TextboxLine s:data.get(currentDataIndex).getData()){
        	g.setColor(textColor);
        	g.setFont(subTitleFont);
        	g.drawString(s.getTitle(), location.x+10, i);
        	g.setFont(dataFont);
        	if (s.getData()!=null){
        		g.setColor(textColor.darker());
        		g.drawString(": "+s.getData(), location.x+10+subtitleWidth, i);
        	}
            i+=lineHeight;
        }
		return i;
	}
	
	private void drawTitle(Graphics2D g, Rectangle2D titleBounds) {
		g.setPaint(textColor);
        g.drawString(data.get(currentDataIndex).getTitle(), location.x+(int)(width-titleBounds.getWidth())/2, location.y+40);
	}
	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof TextboxWidget)){
			return false;
		}
		if (o==this){
			return true;
		}
		return false;
	}
    
    public Map<Rectangle, Bilde> getImages() {
		return images;
	}
	/**
     * Returns the bounds in pixel space for this MovableLayer
     * @return
     */
    public Rectangle getLayerBounds(){
        return new Rectangle(location.x, location.y, width, height);
    }
    
    public String getWaitInfo() {
		return waitInfo;
	}
    public void imageButtonDown(){
		imageButtonDown = true;
	}
    public void imageButtonUp(){
    	imageButtonDown = false;
	}
    
	public boolean isActionButton(Point point){
		if (actionButton==null||actionOn)
			return false;
		boolean val = actionButton.contains(point);
		return val;
	}
    
	public boolean isCloseButton(Point point){
		if (closeButton==null){
			return false;
		}
		return closeButton.contains(point);
	}
	
	public boolean isImageButton(Point point) {
		if (imageButton==null)
			return false;
		
		boolean val = imageButton.contains(point);
		return val;
	}
	public boolean isLeftButton(Point point){
		if (leftArrow==null)
			return false;
		Rectangle rect = new Rectangle(point.x-3, point.y-3, 6, 6);
		boolean val = leftArrow.getBounds().intersects(rect);
		return val;
	}
	public boolean isRemoveOnClick() {
		return removeOnClick;
	}
	public boolean isRightButton(Point point){
		if (rightArrow == null)
			return false;
		Rectangle rect = new Rectangle(point.x-3, point.y-3, 6, 6);
		boolean val = rightArrow.getBounds().intersects(rect);
		return val;
	}
	public void leftButtonClick(){
		if (currentDataIndex-1>-1){
			currentDataIndex--;
		}
	}
	public void mouseClicked(MouseEvent e){
    	if (isLeftButton(e.getPoint())){
    		leftButtonClick();
    		map.repaint();
    	}
    	if (isRightButton(e.getPoint())){
    		rightButtonClick();
    		map.repaint();
    	}
    	if (isActionButton(e.getPoint())){
    		new Thread(){
    			public void run(){
    				buttonDown();
    				map.repaint();
    				action();
    				buttonUp();
    				map.repaint();
    			}
    		}.start();
    	}
    	if (isImageButton(e.getPoint())){
    		new Thread(){
    			public void run(){
    				imageButtonDown();
    				map.repaint();
	        		final Map<Rectangle, Bilde> images = getImages();
	        		DefaultMipssPictureModel model = new DefaultMipssPictureModel();
					Iterator<Rectangle> iterator = images.keySet().iterator();
					while (iterator.hasNext()){
						Rectangle r = iterator.next();
						Bilde bilde = images.get(r);
						model.addBilde(bilde);
					}
					JPictureDialogue dialog = new JPictureDialogue((JFrame)SwingUtilities.windowForComponent(map), model);
					dialog.setVisible(true);
					imageButtonUp();
    				map.repaint();
    			}
    		}.start();
    	}
    	if (isCloseButton(e.getPoint())){
    		getMap().getLayerHandler().unselectAll();
    	}
    }
	public void mouseMoved(MouseEvent e){
    	if (actionButton!=null){
    		
    	}
    	
    	if (leftArrow!=null){
    		
    	}
    	
    	if (rightArrow!=null){
    		
    	}
    	
    }
	public void mousePressed(MouseEvent e){
    	
    }
	/**
     * Move the layer to the specified location and repaint the map
     * @param m
     */
    public boolean moveTo(Point m) {
        location.setLocation(m.x+dragStartPoint.x, m.y+dragStartPoint.y);
        return true;
    }
	/**
     * Paints the TextboxLayer on the graphics context, uses a two-pass algoritm
     * to first calculate the bounds and then paintin the graphics.
     * @param g
     * @param map
     * @param w
     * @param h
     */
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
    	copyState(g);
    	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	if (getMap()==null)
    		setMap((MapPanel)map);
        if (currentDataIndex>=data.size()||!data.get(currentDataIndex).isLoadingDone()){
        	waitPaint(g, map, w, h);	
        	return;
        }
        if (busy!=null){
        	busy.stop();
        	busy = null;
        }
        int lineHeight = g.getFontMetrics().getHeight()+3;
        subtitleWidth = 0;
        dataWidth = 0;
        width = calculateWidth(g);
        height = calcHeight(lineHeight);
        g.setFont(titleFont);
        Rectangle2D titleBounds = g.getFontMetrics().getStringBounds(data.get(currentDataIndex).getTitle(), g);
        height += titleBounds.getHeight();
        
        if (!inited){
        	
        	setWidth(width);
        	setHeight(height);
        	initXY();
    		location.x+=getX();
    		location.y+=getY();
    		inited=true;
    	}
        drawBackground(g);
        drawBorder(g);
        drawTitle(g, titleBounds);
        if (data.size()>1){
        	drawNavigationButtons(g);
        }
        int i = drawText(g, lineHeight, titleBounds);
        if (data.get(currentDataIndex).getBilder().size()>0){
        	drawImages(g, lineHeight, i);
        }
        Action a = data.get(currentDataIndex).getAction();
        if (a!=null){
        	drawActionButton(g, a);
        }
        drawCloseButton(g);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        resetState(g);
    }

	public void paintFakeButton(Graphics2D g, int x, int y, int w, int h, String txt, boolean up){
    	g.setStroke(new BasicStroke(1));
    	g.setColor(borderColor);
    	g.drawRoundRect(x+1, y+1, w, h, 5, 5);
    	g.setColor(up?paneColor:textColor);
    	g.fillRoundRect(x, y, w, h, 5, 5);
    	g.setColor(textColor);
    	g.drawRoundRect(x, y, w, h, 5, 5);
    	g.setColor(up?textColor:paneColor);
    	
    	g.setFont(subTitleFont);
    	int stringWidth = g.getFontMetrics().stringWidth(txt);
    	int stringHeight = g.getFontMetrics().getHeight();
    	
    	int xx = (w-stringWidth)/2+(up?0:1);
    	int yy = (h/2)+(stringHeight/2)+(up?0:1);
    	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    	
    	g.drawString(txt, x+xx, y+yy-2);
    	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	
    	
    	/*g.setColor(up?new Color(212, 208, 200):new Color(233, 231, 227));
    	g.fillRect(x, y, w, h);
    	g.setColor(up?new Color(128, 128, 128):new Color(212, 208, 200));
    	g.drawLine(x+w-1, y, x+w-1, y+h);
    	g.drawLine(x+w-1, y+h-1, x, y+h-1);
    	g.setColor(up?Color.white:new Color(64, 64, 64));
    	g.drawLine(x, y, x+w, y);
    	g.drawLine(x, y, x, y+h);
    	g.setColor(up?new Color(64, 64, 64):Color.white);
    	g.drawLine(x+w, y, x+w, y+h);
    	g.drawLine(x+w, y+h, x, y+h);
    	
    	g.setColor(Color.black);
    	
    	int stringWidth = g.getFontMetrics().stringWidth(txt);
    	int stringHeight = g.getFontMetrics().getHeight();
    	
    	int xx = (w-stringWidth)/2+(up?0:1);
    	int yy = (h/2)+(stringHeight/2)+(up?0:1);
    	g.drawString(txt, x+xx, y+yy-2);
    	*/
    }

	public void rightButtonClick(){
		if (currentDataIndex+1<data.size()){
			currentDataIndex++;
		}
	}

	public void setRemoveOnClick(boolean removeOnClick) {
		this.removeOnClick = removeOnClick;
	}
	/**
     * Store the point where the MovableLayer started to move
     * @param point
     */
    public void setStartPoint(Point point) {
        dragStartPoint = new Point(location.x-point.x, location.y-point.y);
    }
	public void setWaitInfo(String waitInfo) {
		this.waitInfo = waitInfo;
	}
	public void waitPaint(final Graphics2D g, final JXMapViewer map, final int w,final int h){
		int width = 200;
    	int height = 50;
    	g.setPaint(paneColor);
        g.fillRoundRect(location.x, location.y, width, height, 10, 10);
        
        g.setPaint(borderColorT);
        g.setStroke(borderStroke);
        g.drawRoundRect(location.x,location.y,width, height, 10, 10);
        g.setPaint(borderColor);
        g.setStroke(borderStrokeOne);
        int lw = (int)strokeWidth/2;
        g.drawRoundRect(location.x+lw,location.y+lw,width-strokeWidth, height-strokeWidth, 10, 10);        
        
        g.setPaint(textColor);
        g.setFont(titleFont);
        g.drawString(getWaitInfo()==null?"Henter data...":getWaitInfo(), location.x+35, location.y+30);
        if (busy==null){
	        JXBusyLabel l = new JXBusyLabel(new Dimension(20, 20));
			l.setPreferredSize(new Dimension (20, 20));
			busyPainter = l.getBusyPainter();
			busyPainter.setBaseColor(textColorT);
			busyPainter.setHighlightColor(textColor.darker());
			busyPainter.setPaintCentered(true);
			busy = new Timer(100, new ActionListener() {
			    int frame = busyPainter.getPoints();
			    public void actionPerformed(ActionEvent e) {
			    	frame = (frame+1)%busyPainter.getPoints();
			    	busyPainter.setFrame(frame);
			    	map.repaint();
			    }
			});
			busy.start();
    	}else{
    		g.translate(location.x+10,location.y+15);
    		busyPainter.paint((Graphics2D)g, null, 20, 20);
    	}
    }
}
