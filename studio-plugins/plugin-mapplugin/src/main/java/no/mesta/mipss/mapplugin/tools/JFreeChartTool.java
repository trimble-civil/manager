package no.mesta.mipss.mapplugin.tools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.CollectionEntity;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.layer.LayerHandler;
import no.mesta.mipss.mapplugin.layer.PointLayerCollection;
import no.mesta.mipss.mapplugin.layer.ProdpunktGeoPosition;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.layer.SelectableLayer;
import no.mesta.mipss.mapplugin.util.LayerEvent;
import no.mesta.mipss.mapplugin.util.LayerListener;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;
import org.jfree.ui.RectangleEdge;

/**
 * Verktøy for å synkronisere valg ved museklikk mellom graf og kartvisning. 
 * Støtter flere samtidige grafer og kjøretøy i kart samt avspilling.
 * Valgene går begge veier, dvs. et klikk i kart fører til en markør i grafen
 * og ett klikk i graf fører til markering i kartet.  
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class JFreeChartTool implements ChangeListener, LayerListener{

	private final MapPanel map;
	private Map<Long, ChartPanel> charts;
	private Map<XYPlot, Long> dfus;
	private Map<Long, ValueMarker> markers;
	private Map<Long, Long> timerWait;
	
	private Long dfu;
	private boolean inAnimation;
	
	public JFreeChartTool(MapPanel map){
		this.map = map;
		charts = new HashMap<Long, ChartPanel>();
		markers = new HashMap<Long, ValueMarker>();
		timerWait = new HashMap<Long, Long>();
		dfus = new HashMap<XYPlot, Long>();
		
	}
	/**
	 * En ny graf er laget. 
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource()instanceof ChartPanel){
			ChartPanel chart = (ChartPanel) e.getSource();
			chart.getChart().setAntiAlias(false);
			charts.put(dfu, chart);
			dfus.put(chart.getChart().getXYPlot(), dfu);
			registerMouseListener(chart);
		}
	}
	
	/**
	 * Registrer muselytter på panelet
	 * @param panel
	 */
	private void registerMouseListener(final ChartPanel panel){
		panel.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				XYPlot plot = (XYPlot) panel.getChart().getXYPlot(); 
				Rectangle2D plotArea = panel.getChartRenderingInfo().getPlotInfo().getDataArea();
		        ValueAxis domainAxis = plot.getDomainAxis();
		        RectangleEdge domainAxisEdge = plot.getDomainAxisEdge();
		        long chartX = (long)domainAxis.java2DToValue(e.getPoint().getX(),plotArea,domainAxisEdge);
		        Long id = dfus.get(plot);
		        Date date = new Date(chartX);
		        selectLayer(id, date);
			}
		});
	}
	/**
	 * Leter etter et layer å velge i layerhandleren.
	 * @param id
	 * @param date
	 */
	private void selectLayer(Long id, Date date){
		List<Layer> layers = map.getLayerHandler().getLayers(MipssMapLayerType.PRODUKSJONPUNKT.getType());
		for (Layer l:layers){
			if (l instanceof PointLayerCollection){
				PointLayerCollection ll = (PointLayerCollection)l;
				List<? extends CollectionEntity> la = ll.getLayers();
				for (CollectionEntity ce:la){
					if (ce instanceof ProdpunktPointLayer){
						ProdpunktPointLayer pl = (ProdpunktPointLayer)ce;
						if (pl.getProdpunktGeoPosition().getProdpunktVO().getDfuId().intValue()==id.intValue()){
							Date d = pl.getProdpunktGeoPosition().getProdpunktVO().getCetTidspunkt();
							if (d.after(date)){
								map.getLayerHandler().selectLayer(pl);
								map.ensureVisibilityForPosition(pl.getPoint(), 1000);
								return;
							}
						}
					}
				}
			}
		}
	}
	/**
	 * Denne vil fyres når noe blir valgt i kartet
	 */
	@Override
	public void layerChanged(LayerEvent e) {
		synchronized(this){
			if (e.getType().equals(LayerEvent.TYPE_SELECTED)&&!isInAnimation()){
				LayerHandler l = (LayerHandler) e.getSource();
				SelectableLayer selected = l.getCurrentSelected();
				if (selected instanceof ProdpunktPointLayer){
					ProdpunktPointLayer p = (ProdpunktPointLayer) selected;
					ProdpunktGeoPosition gp = p.getProdpunktGeoPosition();
					if (gp.getProdpunktVO()!=null){
						dfu = gp.getProdpunktVO().getDfuId();
						ChartPanel chart = charts.get(dfu);
						if (chart!=null){
							setMarker(chart.getChart(),gp.getProdpunktVO().getCetTidspunkt());
						}
					}
				}
			}
		}
	}
	
	/**
	 * Setter en markør på alle grafer 
	 * @param date
	 */
	public void setTimeMarkerAll(Date date){
		for (Long l:charts.keySet()){
			ChartPanel chart = charts.get(l);
			dfu = l;
			setMarker(chart.getChart(), date);
		}
	}
	
	/**
	 * Sørger for at angitt tidspunkt er synlig i grafen, sentrere grafen rundt tiden
	 * @param chart
	 * @param date
	 */
	private void ensureVisibilityForTime(JFreeChart chart, Date date){
		ValueAxis axis = ((XYPlot)chart.getPlot()).getDomainAxis();
		Range range = axis.getRange();
		double lower = range.getLowerBound();
		double upper= range.getUpperBound();
		double value = date.getTime();
		double length = range.getLength();
		Range newRange=null;
		if (value<lower){
			newRange = new Range(value-length/2, value+length/2);
		}
		if (value>upper){
			newRange = new Range(value-length/2, value+length/2);
		}
		if (newRange!=null){
			axis.setRange(newRange);
		}
	}
	
	/**
	 * Sett en markør på en graf med angitt tid
	 * @param chart
	 * @param date
	 */
	private void setMarker(JFreeChart chart, Date date){
		Long prevMark = timerWait.get(dfu);
		if (prevMark==null || System.currentTimeMillis()-prevMark>500){
			chart.setNotify(false);
			removeMarker(chart);
			ensureVisibilityForTime(chart, date);
			if (chart.getPlot() instanceof CombinedDomainXYPlot){
				CombinedDomainXYPlot plot = (CombinedDomainXYPlot) chart.getPlot();
				List subplots = plot.getSubplots();
				ValueMarker marker = markers.get(dfu);
				if (marker==null){
					marker = new ValueMarker(date.getTime());
					marker.setPaint(Color.black);
					marker.setAlpha(1);
					marker.setStroke(new BasicStroke(2));
					markers.put(dfu, marker);
				}else{
					marker.setValue(date.getTime());
				}
	            for (int i=0;i<subplots.size();i++){
	            	if (subplots.get(i)instanceof XYPlot){
	            		XYPlot pl = (XYPlot) subplots.get(i);
	            		pl.addDomainMarker(marker);
	            	}
	            }
	            
			}
			prevMark = System.currentTimeMillis();
			timerWait.put(dfu, prevMark);
			chart.setNotify(true);
            chart.fireChartChanged();
		}
	}
	
	/**
	 * Fjern markør fra grafen
	 * @param chart
	 */
	private void removeMarker(JFreeChart chart){
		ValueMarker marker = markers.get(dfu);
		if (chart.getPlot() instanceof CombinedDomainXYPlot&&marker!=null){
			CombinedDomainXYPlot plot = (CombinedDomainXYPlot) chart.getPlot();
			List subplots = plot.getSubplots();
            for (int i=0;i<subplots.size();i++){
            	if (subplots.get(i)instanceof XYPlot){
            		XYPlot pl = (XYPlot) subplots.get(i);
            		if (pl.getDomainMarkers(org.jfree.ui.Layer.FOREGROUND)!=null)
            			pl.removeDomainMarker(marker);
            	}
            }
		}
	}
	/**
	 * Indiker at en avspilling er under veis
	 * @param inAnimation
	 */
	public void setInAnimation(boolean inAnimation) {
		this.inAnimation = inAnimation;
	}
	/**
	 * Indikerer om en avspilling er under veis
	 * @return
	 */
	public boolean isInAnimation() {
		return inAnimation;
	}
	
}
