package no.mesta.mipss.mapplugin.widgets;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;

import no.mesta.mipss.mapplugin.MapPanel;

import org.jdesktop.swingx.JXMapViewer;

public class ResolutionsWidget extends Widget{

	private Rectangle zoomInButton;
	private Rectangle zoomOutButton;
	private Rectangle resolution;
	private Rectangle pole;
	private int currentZoomIndex;
	
	private int buttonSize;
	private int zoomButtonHeight=11;
	
	private BasicStroke thick = new BasicStroke(3);
	private BasicStroke medium = new BasicStroke(2);
	private BasicStroke one = new BasicStroke(1);
	
	private boolean zoomOutOver;
	private boolean zoomInOver;
	private boolean zoomValueOver;
	
	private int poleWidth;
	private boolean sticky;
	
	private int mapZoom=0;
	private int mapHeight=0;
	private int mapWidth=0;
	public ResolutionsWidget(MapPanel map, int width, int height, int pad,GLUE glue) {
		super(map, width, height, pad, glue);
		poleWidth=width/3;
		if (poleWidth%2!=0)
			poleWidth++;
		buttonSize=width;
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer object, int w, int h) {
		copyState(g);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		initXY();
		if (isMapChanged())
			resolution=null;
		mapZoom=map.getZoom();
		drawButtons(g);
		int resSize = drawCenterBar(g);
		drawResolutionMarker(g, resSize);
		resetState(g);
	}
	
	private boolean isMapChanged(){
		return mapZoom!=map.getZoom()||mapHeight!=map.getHeight()||mapWidth!=map.getWidth();
		
	}
	/**
	 * Tegner knappen som kan flyttes for å indkere hvilket zoomlevel man er på
	 * @param g
	 * @param resSize
	 */
	private void drawResolutionMarker(Graphics2D g, int resSize) {
		if (resolution==null||currentZoomIndex!=map.getResolutions().getTotalResolutions()-map.getZoom()-1){
			currentZoomIndex = map.getResolutions().getTotalResolutions()-map.getZoom()-1;
			resolution = new Rectangle(getX()-5, pole.y+(resSize*currentZoomIndex), buttonSize+5, zoomButtonHeight);
		}
		Rectangle res = new Rectangle(resolution.x+5, resolution.y, resolution.width-5, resolution.height);
		g.setColor(zoomValueOver?textColor:paneColor);
		g.fillRoundRect(res.x, res.y, res.width, res.height, 10, 10);
		g.setColor(borderColor);
		g.drawRoundRect(res.x, res.y, res.width, res.height, 10, 10);
		g.setStroke(medium);
		g.setColor(zoomValueOver?paneColor:textColor);
		g.drawLine(res.x+5, res.y+res.height/2+1, res.x+res.width-4, res.y+res.height/2+1);
	}
	
	/**
	 * 
	 * @param g
	 * @return
	 */
	private int drawCenterBar(Graphics2D g) {
		g.setStroke(one);
		pole = new Rectangle(getX()+getWidth()/2-poleWidth/2, zoomInButton.y+zoomInButton.height, poleWidth, getHeight()-(buttonSize*2));
		g.setColor(paneColor);
		g.fill(pole);
		g.setColor(borderColor);
		g.draw(pole);
		g.setColor(textColor);
		int resCount = map.getResolutions().getTotalResolutions();
		int resSize = pole.height/resCount;
		for (int i=0;i<resCount;i++){
			int x1 = pole.x+2;
			int x2 = pole.x+pole.width-2;
			int y = pole.y+ (resSize*i) +resSize/2;
			g.drawLine(x1, y, x2, y);
		}
		return resSize;
	}
	
	/**
	 * Tegner zoom in og zoom ut knappene
	 * @param g
	 */
	private void drawButtons(Graphics2D g) {
		zoomInButton = new Rectangle(getX(), getY(), buttonSize, buttonSize);
		zoomOutButton = new Rectangle(getX(), getY()+getHeight()-buttonSize, buttonSize, buttonSize);
		g.setColor(zoomInOver?textColor:paneColor);
		g.fillRoundRect(zoomInButton.x, zoomInButton.y, zoomInButton.width, zoomInButton.height, 7, 7);
		g.setColor(zoomOutOver?textColor:paneColor);
		g.fillRoundRect(zoomOutButton.x, zoomOutButton.y, zoomOutButton.width, zoomOutButton.height, 7, 7);
		
		g.setColor(borderColor);
		g.drawRoundRect(zoomInButton.x, zoomInButton.y, zoomInButton.width, zoomInButton.height, 7, 7);
		g.drawRoundRect(zoomOutButton.x, zoomOutButton.y, zoomOutButton.width, zoomOutButton.height, 7, 7);
		
		g.setStroke(thick);
		g.setColor(zoomInOver?paneColor:textColor);
		g.drawLine(zoomInButton.x+4, zoomInButton.y+zoomInButton.height/2, zoomInButton.x+zoomInButton.width-4, zoomInButton.y+zoomInButton.height/2);
		g.drawLine(zoomInButton.x+zoomInButton.width/2, zoomInButton.y+4, zoomInButton.x+zoomInButton.width/2, zoomInButton.y+zoomInButton.height-4);
		g.setColor(zoomOutOver?paneColor:textColor);
		g.drawLine(zoomOutButton.x+4, zoomOutButton.y+zoomOutButton.height/2, zoomOutButton.x+zoomOutButton.width-4, zoomOutButton.y+zoomOutButton.height/2);
	}
	
	@Override
	public void mouseClicked(MouseEvent e){
		if (zoomOutButton.contains(e.getPoint())){
			int z = map.getZoom()-1;
			if (z>=0){
				map.setZoom(map.getZoom()-1);
				resolution=null;
			}
		}
		if (zoomInButton.contains(e.getPoint())){
			int z = map.getZoom()+1;
			if (z<map.getResolutions().getTotalResolutions()){
				map.setZoom(map.getZoom()+1);
				resolution=null;
			}
		}
	}
	@Override
	public void mouseMoved(MouseEvent e){
		boolean moved = false;
		if (zoomOutButton.contains(e.getPoint())){
			if (!zoomOutOver){
				moved = true;
			}
			zoomOutOver = true;
		}
		else if (zoomInButton.contains(e.getPoint())){
			if (!zoomInOver){
				moved = true;
			}
			zoomInOver = true;
		}else if (resolution.contains(e.getPoint())){
			if (!zoomValueOver){
				moved = true;
			}
			zoomValueOver = true;
		}else{
			if (zoomInOver||zoomOutOver||zoomValueOver){
				zoomInOver=false;
				zoomOutOver=false;
				zoomValueOver=false;
				moved = true;
			}
		}
		if (moved){
			map.repaint();
		}
		
		Rectangle rect = new Rectangle(getX(), getY(), getWidth(), getHeight());
		if (!rect.contains(e.getPoint())&&!hidden){
			hidden = true;
			startAnimOn();
		}
		if(rect.contains(e.getPoint())&&hidden){
			hidden=false;
			startAnimOff();
		}
		
	}
	private boolean hidden;
	@Override
	public void mouseDragged(MouseEvent e){
		if ((resolution.contains(e.getPoint())||sticky)&&!e.isConsumed()){
			
			if (e.getPoint().y>=zoomInButton.y+zoomInButton.height+zoomButtonHeight/2&&e.getPoint().y<zoomOutButton.y-zoomButtonHeight){
				map.getMouseListenerManager().invalidateAllBut(null);
				resolution.y= e.getPoint().y-resolution.height/2;
				sticky = true;
				map.setZoom(getResolutionForPixel(e.getPoint().y));
				map.repaint();
			}
		}
	}
	
	private int getResolutionForPixel(int y){
		int resCount = map.getResolutions().getTotalResolutions();
		int resSize = pole.height/resCount;
		for (int i=0;i<resCount;i++){
			if (y< pole.y+ (resSize*i)){
				return map.getResolutions().getTotalResolutions()-i;
			}
		}
		return 0;
	}
	@Override
	public void mouseReleased(MouseEvent e){
		if (resolution.contains(e.getPoint())){
			map.setZoom(getResolutionForPixel(e.getPoint().y));
			map.getMouseListenerManager().reset();
		}
		if (sticky){
			sticky = false;
			map.setZoom(getResolutionForPixel(e.getPoint().y));
			map.getMouseListenerManager().reset();
		}
	}
	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof ResolutionsWidget)){
			return false;
		}
		if (o==this){
			return true;
		}
		return false;
	}
}
