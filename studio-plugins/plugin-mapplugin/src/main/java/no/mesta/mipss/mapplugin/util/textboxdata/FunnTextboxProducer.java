package no.mesta.mipss.mapplugin.util.textboxdata;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.AvvikService;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.PunktregSok;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FunnTextboxProducer implements TextboxProducer{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private List<String> pList;
	private IMipssStudioLoader loader;
	
	public FunnTextboxProducer(IMipssStudioLoader loader, String avvikGuid){
		pList = new ArrayList<String>();
		pList.add(avvikGuid);
		this.loader = loader;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		AvvikService bean = BeanUtil.lookup(AvvikService.BEAN_NAME, AvvikService.class);
		Avvik avvik = bean.hentAvvik(pList.get(0));
		PunktregOverviewTextboxProducer tp = new PunktregOverviewTextboxProducer(loader, avvik);
		for (int i=1;i<pList.size();i++){
			log.debug("creating punktreg");
			Avvik a = bean.hentAvvik(pList.get(i));
			tp.addAvvik(a);
		}
		return tp.getTextboxData();
	}
	
	public void addAvvik(String p){
		for (String pr:pList){
			if(pr.equals(p))
				return;
		}
		pList.add(p);
	}
	
	public int getAntallPunkter(){
		return pList.size();
	}
}
