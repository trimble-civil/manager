package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.webservice.ReflinkStrekning;

/**
 * Representerer en del av eller helekontraktveinettet
 * 
 * @author harkul
 *
 */
public class KontraktveiLayer extends LineListLayer{

	private final List<ReflinkStrekning> veinett;

    public KontraktveiLayer(String id, List<MipssGeoPosition[]> points, List<ReflinkStrekning> veinett, Color color, int lineWidth, Bounds bounds, boolean initGraphics) {
    	super(id, null, points, color, lineWidth, false, bounds, initGraphics);
    	this.veinett = veinett;
    }
    public String getVeikategori(){
    	if (veinett.size()>0){
    		return veinett.get(0).getVeikategori();
    	}
    	return "";
    }


}
