package no.mesta.mipss.mapplugin.util.textboxdata;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.VeiInfoUtil;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.service.produksjon.ProdReflinkStrekning;
import no.mesta.mipss.service.produksjon.ProdtypeVO;

/**
 * Lager en tekstboks for Prodstrekning
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class ProdstrekningTextboxProducer implements TextboxProducer{

	private ProdReflinkStrekning strekning;
	public ProdstrekningTextboxProducer(ProdReflinkStrekning strekning){
		this.strekning = strekning;
	}

	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		TextboxData info = new TextboxData();
        info.setTitle("Prodstrekning");
        info.addData("Starttidspunkt", MipssDateFormatter.formatDate(strekning.getProdstrekning().getFraTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        info.addData("Stopptidspunkt", MipssDateFormatter.formatDate(strekning.getProdstrekning().getTilTidspunkt(), MipssDateFormatter.LONG_DATE_TIME_FORMAT));
        String prodtype="";
        if (strekning.getProdtypeList()!=null){
	        for (ProdtypeVO pvo:strekning.getProdtypeList()){
	        	prodtype+=pvo.getProdtype().getNavn();
	        }
        }
 
        NumberFormat formatter = new DecimalFormat("#0.000");
        
        info.addData("Produksjonstyper", prodtype);
        info.addData("Lengde(KM)", formatter.format(strekning.getProdstrekning().getLengde()));
         
        Prodstrekning p = strekning.getProdstrekning();
        String fraHp = p.getFylkesnummer()+"/"+p.getKommunenummer()+" "+p.getVeikategori()+""+p.getVeistatus()+""+p.getVeinummer()+"(HP "+p.getHp()+": "+p.getFraKm()+"km -"+p.getTilKm()+"km)";
        info.addData("Strekning",fraHp);
        dataList.add(info);
        return dataList;
	}
	
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}
}
