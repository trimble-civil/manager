package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.LayerEvent;
import no.mesta.mipss.mapplugin.util.LayerListener;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.mapplugin.widgets.Widget;
import no.mesta.mipss.mapplugin.widgets.WidgetController;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.painter.Painter;


/**
 * @author Harald A. Kulø
 */
public class LayerHandler implements Painter<JXMapViewer>  {
    private static final  Logger log = LoggerFactory.getLogger(LayerHandler.class);

	private List<PeriodLineListLayer> animatableList;
	
    private SelectableLayer currentSelected;

	private Layer lastChangedLayer;
    private Map<LayerType, List<Layer>> layers;
    private List<LayerType> layerTypes;
    
    private ArrayList<LayerListener> listeners;
    private List<MovableLayer> movableList = new ArrayList<MovableLayer>();
    
    private WidgetController widgetController;
    private boolean showZeroMeridian;
    /**
     * New Instance of LayerHandler
     */
    public LayerHandler() {
        layers = new TreeMap<LayerType, List<Layer>>();
        listeners = new ArrayList<LayerListener>();
        showZeroMeridian=KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "showZeroMeridian").getVerdi().equals("true");
        if (showZeroMeridian)
        	addZeroMeridianMarker();
        
    }
    
    public void addAnimatableLayer(PeriodLineListLayer layer){
    	animatableList.add(layer);
    }
    
    /**
     * Add a new layer to the map, if notifyListeners == true an event will be fired
     * @param type the new layers type
     * @param p the new layer
     * @param notifyListeners fire event if true
     */ 
    public void addLayer(LayerType type, Layer p, boolean notifyListeners) {
        synchronized(this){
	    	if (type==null)
	        	throw new IllegalArgumentException("LayerType can not be null!");
	        if(p!=null){
	            //plukk opp flyttbare lag...
	            if (p instanceof MovableLayer){
	                addMovableLayer((MovableLayer)p);
	            }
	            //plukk opp animerbare lag..
	            if (p instanceof PeriodLineListLayer){
	            	animatableList.add((PeriodLineListLayer)p);
	            }
	            if (layers.containsKey(type)){
	                List<Layer> layerList = layers.get(type);
	                layerList.add(p);
	            }else{
	                List<Layer> layerList = new ArrayList<Layer>();
	                layerList.add(p);
	                layers.put(type, layerList);
	            }
	            lastChangedLayer = p;
	            if (notifyListeners)
	                fireLayerChanged(this, LayerEvent.TYPE_ADDED, p.isTriggerRecenterAndZoom());
	        }
        }
    }
    
    /**
     * add a LayerListener
     * @param l
     */
    public void addLayerListener(LayerListener l){
        listeners.add(l);
    }
    /**
     * Adds a movable layer to the layer handler
     */
    public void addMovableLayer(MovableLayer layer){
        movableList.add(layer);
    }
    
    private void addZeroMeridianMarker(){
    	MipssGeoPosition top = new MipssGeoPosition(8445691, 0);
        MipssGeoPosition bottom = new MipssGeoPosition(6278226, 0);
        LineLayer l = new LineLayer("ZEROMERIDIAN", top, bottom, false);
        l.setColor(new Color(0f,0f,1f,0.2f));
        addLayer(MipssMapLayerType.ALLWAYSONTOP.getType(), l, true);
    }
    /**
     * Indicates that this layer has changed, fire an event to trigger a repaint
     * @param l
     */
    public void changeLayer(Layer l){
        lastChangedLayer = l;
        fireLayerChanged(this, LayerEvent.TYPE_MODIFIED, false);
    }
    
    @Override
    public void finalize() throws Throwable{
    	listeners.clear();
    	listeners = null;
        
        layers.clear();
        layers = null;
        lastChangedLayer = null;
        
        movableList.clear();
        movableList = null;
        
        currentSelected = null;
        super.finalize();
    }
    /**
     * Fire an event to all LayerListeners
     * @param source the source of the event
     * @param type LayerEvent.TYPE_* to indicate the type of the event
     *        the listener can check the event type and update acordingly
     */
    protected void fireLayerChanged(Object source, String type, boolean triggerRecenterAndZoom){
        Object[] layerListeners = listeners.toArray();
        LayerEvent e = null;
        for (int i=0;i<layerListeners.length;i++){
            LayerListener l = (LayerListener)layerListeners[i];
            if (e==null)
                e = new LayerEvent(source, type, triggerRecenterAndZoom);
            l.layerChanged(e);
        }
    }
    
    protected void fireLayerSelected(Object source){
    	LayerListener[] layerListeners = listeners.toArray(new LayerListener[]{});
        LayerEvent e = null;
        for (int i=0;i<layerListeners.length;i++){
            LayerListener l = layerListeners[i];
            if (e==null)
                e = new LayerEvent(source, LayerEvent.TYPE_SELECTED, false);
            l.layerChanged(e);
        }
    }
    
    
    public List<PeriodLineListLayer> getAnimatableLayers(){
    	return animatableList;
    }
   
    /**
     * Returns the currently selected layer
     * @return
     */
    public SelectableLayer getCurrentSelected(){
        return currentSelected;
    }
    
    public Layer getLastChangedLayer(){
        return lastChangedLayer;
    }
    
    /**
     * Search for a layer with the specified type and id
     * @param type the layers type
     * @param id the layers id. 
     */
    public Layer getLayer(LayerType type, String id){
    	synchronized(this){
	        if (layers.containsKey(type)){
	            for (Layer l:layers.get(type)){
	            	if (l.getId().equals(id)){
	            		return l;
	            	}
	            }
	        }
	        return null;
    	}
    }
    
    /**
     * Returns the entire layer map.
     * @return
     */
    public Map<LayerType, List<Layer>> getLayers(){
        return layers;
    }
    
    /**
     * Returns all layers of the specified type
     * @param type
     * @return
     */
    public List<Layer> getLayers(LayerType type){
        log.trace(type+""+layers.get(type));
        return layers.get(type);
    }
    /**
     * Get the MovableLayer at the specified point, if any..
     */
    public MovableLayer getMovableLayerAt(Point point){
    	synchronized(this){
	        for (MovableLayer l: movableList){
	            if (l.getLayerBounds().contains(point)){
	            	if (((Layer)l).isVisible())
	            		return l;
	            }
	        }
	        if (widgetController!=null){
	        	for (Widget w:widgetController.getWidgets()){
	        		if (w instanceof MovableLayer){
	        			if (((MovableLayer) w).getLayerBounds().contains(point)){
	        				if (((Layer)w).isVisible())
	        					return (MovableLayer) w;
	        			}
	        		}
	        	}
	        }
	    	
	        return null;
    	}
    }
    
    public WidgetController getWidgetController() {
		return widgetController;
	}
    /**
     * Hides a layer and fires an event
     * @param type the layers type
     * @param id the layers id
     */
    public void hideLayer(LayerType type, String id){
        Layer layer = null;
        
        if (layers.containsKey(type)){
        	for (Layer l:layers.get(type)){
        		if (l.getId().equals(id)){
        			Layer selectedLayer= getLayer(MipssMapLayerType.SELECTED.getType(), id);
        			if (selectedLayer!=null)
        				widgetController.removeWidget(selectedLayer.getTextboxLayer());
        			removeLayer(MipssMapLayerType.SELECTED.getType(), id);
        			l.setVisible(false);
        			if (l.getTextboxLayer()!=null){
        				widgetController.removeWidget(l.getTextboxLayer());
        			}
        			layer = l;
        		}
        	}
        }
        lastChangedLayer = layer;
        fireLayerChanged(this, LayerEvent.TYPE_MODIFIED, false);
    }
    /**
     * Hide a certain layer type
     * @param layerType
     */
    public void hideLayerType(LayerType layerType){
        if (layers.containsKey(layerType)){
        	for (Layer l:layers.get(layerType)){
        		l.setVisible(false);
        	}
            lastChangedLayer = null;
            fireLayerChanged(new Object(), LayerEvent.TYPE_MODIFIED, false);
        }
    }
    
    /**
     * Check if the layer is present in the layer handler
     * @param type the layers type
     * @param id the layers is
     * @return true if present, false otherwise
     */
    public boolean isLayerPresent(LayerType type, String id){
        if (layers.containsKey(type)){
        	for (Layer l:layers.get(type)){
        		if (l.getId().equals(id)){
        			return true;
        		}
        	}
        }
        return false;
    }
    
    /**
     * Move the MovableLayer to the specified point
     */
    public void moveTo(MovableLayer layer, Point point){
        layer.moveTo(point);
        fireLayerChanged(this, LayerEvent.TYPE_REMOVED, false);
    }
    
    
    /**
     * Paints all layers in the LayerHandler, invoked by the mapviewer
     * @param g graphics context
     * @param obj map
     * @param w width 
     * @param h height
     */
     
    public void paint(final Graphics2D g, final JXMapViewer obj, final int w, final int h) {
    	g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
    	g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
    	Set<LayerType> keys = layers.keySet();
    	LayerType[] o = keys.toArray(new LayerType[0]);
    	for (int i=0;i<o.length;i++){
    		Layer[] ls = layers.get(o[i]).toArray(new Layer[0]);
    		for (int j=0;j<ls.length;j++){
    			Layer l = ls[j];
    			if (l!=null&&l.isVisible()){
    				l.paint(g, obj, w, h);
    			}
    		}
    	}
    	if (widgetController!=null){
    		widgetController.paint(g, obj, w, h);
    	}
    	
//    	//crosshair i center (TEST)
//    	g.setColor(Color.gray);
//    	g.drawLine(obj.getWidth()/2, 0, obj.getWidth()/2, obj.getHeight());
//    	g.drawLine(0, obj.getHeight()/2, obj.getWidth(), obj.getHeight()/2);
    }
    /**
     * removes all layers from the layer handler
     */
     public void removeAllLayers(){
    	 log.debug("removeAllLayers()");
         layers = new TreeMap<LayerType, List<Layer>>();
         widgetController.removeWidgets(widgetController.getWidgetsOfClass(TextboxWidget.class));
         movableList = new ArrayList<MovableLayer>();
         animatableList = new ArrayList<PeriodLineListLayer>();
         lastChangedLayer = null;
         fireLayerChanged(this, LayerEvent.TYPE_REMOVED, false);
         if (showZeroMeridian)
        	 addZeroMeridianMarker();
     }
    
/**
     * remove a layer from the layer handler
     * @param id 
     */
    public synchronized void removeLayer(LayerType type, String id){
    	boolean removed = false;
        if (layers.containsKey(type)){
            Layer layer = null;
            Layer[] ls = layers.get(type).toArray(new Layer[0]);
            for (int i=0;i<ls.length;i++){
                Layer l = ls[i];
                if (l!=null && l.getId().equals(id)){
                    layer = l;
                }
            }
            removed = true;
            layers.get(type).remove(layer);
            if (layer!=null&&layer.getTextboxLayer()!=null)
            	widgetController.removeWidget(layer.getTextboxLayer());
            
            lastChangedLayer = layer;
        }
        
        boolean removeMovable = false;
        for (MovableLayer l:movableList){
       	 if (((Layer)l).getPreferredLayerType()!=null && ((Layer)l).getPreferredLayerType().equals(type)){
       		 removeMovable= true;
       		 removed=true;
       		 break;
       	 }
        }
        if (removeMovable)
       	 movableList = new ArrayList<MovableLayer>();
        boolean removeAnimatable = false;
        
        if (animatableList!=null){
	        for (PeriodLineListLayer l:animatableList){
	        	if (((Layer)l).getPreferredLayerType().equals(type)){
	        		removeAnimatable = true;
					removed=true;
					break;
				}
	        }
        }
        if (removeAnimatable)
       	 	animatableList = new ArrayList<PeriodLineListLayer>();
        if (removed){
        	fireLayerChanged(this, LayerEvent.TYPE_REMOVED, false);
        }
    }
    
    
    /**
     * remove a LayerListener
     * @param l
     */
    public void removeLayerListener(LayerListener l){
        listeners.remove(l);
    }
    
    /**
     * removes all layers with a certain type from the layer handler
     * @param type
     */
     public void removeLayerType(LayerType type){
    	 boolean removed = false;
         if (layers.containsKey(type)){
             layers.remove(type);         
             lastChangedLayer = null;
             removed = true;
         }
         
         boolean removeMovable = false;
         for (MovableLayer l:movableList){
        	 if (((Layer)l).getPreferredLayerType()!=null){
	        	 if (((Layer)l).getPreferredLayerType().equals(type)){
	        		 removeMovable= true;
	        		 removed = true;
	        		 break;
	        	 }
        	 }
         }
         if (removeMovable)
        	 movableList = new ArrayList<MovableLayer>();
         boolean removeAnimatable = false;
         if (animatableList!=null){
	         for (PeriodLineListLayer l:animatableList){
	        	 if (((Layer)l).getPreferredLayerType().equals(type)){
	        		 removeAnimatable = true;
	        		 removed=true;
	        		 break;
	        	 }
	         }
         }
         if (removeAnimatable)
        	 animatableList = new ArrayList<PeriodLineListLayer>();
         if (removed){
        	 fireLayerChanged(this, LayerEvent.TYPE_REMOVED, false);
         }
     }
    /**
     * Search for a SelectableLayer
     * @param gp
     * @param searchRadius
     * @return
     */
    public SelectableLayer selectLayer(MipssGeoPosition gp, double searchRadius){
        LayerType[] types = layers.keySet().toArray(new LayerType[0]);
        for (int i=types.length-1;i>=0;i--){
            List<Layer> layerss = layers.get(types[i]);
            for (Layer l:layerss){
                if (l instanceof SelectableLayer && l.isVisible()){
                	SelectableLayer pl = ((SelectableLayer)l).getLayerCloseTo(gp, searchRadius);
                    if (pl!=null){
                        if (pl.getLayerCloseTo(gp, searchRadius)!=null){
                            selectLayer(pl);
                            return pl;
                        }
                    }
                }
            }
        }
        currentSelected = null;
        fireLayerChanged(this, LayerEvent.TYPE_UNSELECTED, false);
                                
        return null;
    }
    
    /**
     * Create a select layer from the specified layer. 
     * @param layer
     */
    public void selectLayer(SelectableLayer layer){
        log.debug("Unselecting all layers");
    	unselectAll();
        log.debug("selecting layer:"+layer);
        if (layer!=null){
        	Layer selected = layer.getSelectionLayer();
            addLayer(selected.getPreferredLayerType(), selected, true);
            TextboxWidget textboxLayer = layer.getTextboxLayer();
            log.debug("got textboxlayer:"+textboxLayer);
            if (textboxLayer!=null){
            	if (widgetController==null){
            		widgetController = new WidgetController();
            	}
            	widgetController.addWidget(textboxLayer);
            	
            	log.debug("added textboxlayer to widgetcontroller");
//	            addLayer((textboxLayer).getPreferredLayerType(), textboxLayer, false);
            }else{
            	log.warn("Attempt to select a selectable layer without textbox");
            }
            log.debug("adding layer to layerhandler");
            currentSelected = layer;
            log.debug("notify listeners...");
            fireLayerSelected(this);
        }
    }
    public void fakeSelect(SelectableLayer layer, String id){
    	Layer selected = layer.getSelectionLayer();
    	selected.setId(id);
    	addLayer(selected.getPreferredLayerType(), selected, true);
    	currentSelected = layer;
        fireLayerSelected(this);
    }

    public void setCurrentSelected(SelectableLayer currentSelected){
        this.currentSelected = currentSelected;
    }

    public void setLayerTypes(List<LayerType> layerTypes){
    	this.layerTypes = layerTypes;
    }
    
    public void setWidgetController(WidgetController widgetController) {
		this.widgetController = widgetController;
	}
    /**
     * set a certain layer visible
     * @param id lagets id
     */
    public void showLayer(LayerType type, String id){
        Layer layer = null;
        if (layers.containsKey(type)){
            Iterator<Layer> it = layers.get(type).iterator();
            while (it.hasNext()){
                Layer l = it.next();
                if (l.getId().equals(id)){
                    l.setVisible(true);
                    layer = l;
                }
            }
        }
        lastChangedLayer = layer;
        fireLayerChanged(this, LayerEvent.TYPE_MODIFIED, false);
    }
    
    /**
     * show a certain layer type
     * @param layerType
     */
    public void showLayerType(LayerType layerType){
        if (layers.containsKey(layerType)){
            Iterator<Layer> it = layers.get(layerType).iterator();
            while (it.hasNext()){
                Layer l = it.next();
                l.setVisible(true);
            }
            lastChangedLayer = null;
            fireLayerChanged(new Object(), LayerEvent.TYPE_MODIFIED, false);
        }
    }
    
    /**
     * Make sure no layers are selected
     */
    public void unselectAll(){
    	List<MovableLayer> remove = new ArrayList<MovableLayer>();
    	List<Widget> removew = new ArrayList<Widget>();
    	
    	for (LayerType t:layerTypes){
    		if (t.isLoose()){
    			removeLayerType(t);
    			if (widgetController!=null){
    				for (Widget w:widgetController.getWidgets()){
    					if (w instanceof TextboxWidget){
    						removew.add(w);
    					}
    				}
    				
    			}
    			for (MovableLayer ml:movableList){
    				if (ml instanceof TextboxWidget){
    					remove.add(ml);
    				}
    			}
    		}
    	}
    	for (MovableLayer m:remove){
    		movableList.remove(m);
    	}
    	for (Widget w:removew){
    		widgetController.removeWidget(w);
    	}
    }
}
