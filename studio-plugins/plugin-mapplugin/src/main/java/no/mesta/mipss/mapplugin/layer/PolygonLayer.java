package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * A layer that draws a Polygon on the map
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class PolygonLayer extends Layer implements SelectableLayer {

    private MipssGeoPosition[] points;
    private Color foreground;
    private Color border;
    private Paint fillPattern;
	private JXMapViewer map;

    /**
     * Create a polygon with the given coordinates with default settings. 
     * @param points
     */
    public PolygonLayer(String id, MipssGeoPosition[] points, boolean triggerRecenterAndZoom) {
        super(id, triggerRecenterAndZoom);
        this.points = points;
        foreground = Color.blue;
        border = Color.blue;
    }

    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        this.map = map;
		g = super.initGraphics(g, map);

        //create a polygon
        Polygon poly = new Polygon();
        for (MipssGeoPosition gp: points) {
            //convert geo to world bitmap pixel
            Point2D pt = map.getTileFactory().geoToPixel(new GeoPosition(gp.getLatitude(), gp.getLongitude()), map.getZoom());
            poly.addPoint((int)pt.getX(), (int)pt.getY());
        }
        if (fillPattern != null) {
            g.setPaint(fillPattern);
        }

        //do the drawing
        g.setColor(foreground);
//        g.fill(poly);

        g.setColor(border);
        g.draw(poly);
        g.dispose();
    }

    /**
     * Get the coordinates for the polygon
     * @return
     */
    public MipssGeoPosition[] getPoints() {
        return points;
    }

    /**
     * Set new coordinates
     * @param points
     */
    public void setPoints(MipssGeoPosition[] points) {
        this.points = points;
    }

    /**
     * Get the foreground color of this polygon
     * @return
     */
    public Color getForeground() {
        return foreground;
    }

    /**
     * Set new foreground color
     * @param foreground
     */
    public void setForeground(Color foreground) {
        this.foreground = foreground;
    }

    /**
     * Get the border color of the polygon
     * @return
     */
    public Color getBorder() {
        return border;
    }

    /**
     * Set new border color
     * @param border
     */
    public void setBorder(Color border) {
        this.border = border;
    }

    /**
     * Get the fillPattern for the polygon
     * @return
     */
    public Paint getFillPattern() {
        return fillPattern;
    }

    /**
     * Set new fillPattern, can be any type that is compatible with the
     * java.awt.Paint class.  
     * @param fillPattern
     */
    public void setFillPattern(Paint fillPattern) {
        this.fillPattern = fillPattern;
    }

	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition other, double searchRadius) {
		Polygon poly = new Polygon();
        for (MipssGeoPosition gp: points) {
            //convert geo to world bitmap pixel
        	Double pt = new Point2D.Double(gp.getLongitude(), gp.getLatitude());
            poly.addPoint((int)pt.getX(), (int)pt.getY());
        }
		if (poly.contains(new Point((int)other.getLongitude(), (int)other.getLatitude()))){
			return this;
		}
		return null;
	}

	@Override
	public Layer getSelectionLayer() {
		String color = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.mapplugin", "selectionColor").getVerdi();
        final Color selectionColor = ColorUtil.fromHexString(color);
        
        PolygonLayer p = new PolygonLayer("selected"+getId(), points, false);
        p.setBorder(selectionColor);
        p.setForeground(new Color(selectionColor.getRed(), selectionColor.getGreen(), selectionColor.getBlue(), 50));
        p.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
		return p;
	}


}
