package no.mesta.mipss.mapplugin.layer;

import java.awt.Graphics2D;

import java.awt.Shape;

import org.jdesktop.swingx.JXMapViewer;

/**
 * A layer that draws a line around another layer to show that the layer is
 * selected by mouse.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class SelectionLayer extends Layer{
    
    private Layer shape;
    
    public SelectionLayer(String id, Layer shape) {
        super(id, false);
        this.shape = shape;
    }

    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        shape.paint(g, map, h, w);
    }
}
