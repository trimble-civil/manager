package no.mesta.mipss.mapplugin.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;
import no.mesta.mipss.mapplugin.widgets.Widget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class ProdpunktPointLayer extends PointLayer implements SelectableLayer, CollectionEntity{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    
    private Color pointColorFill;
    private Color pointColorBorder;
    private Color pointColorBorderAlpha;
    
    private int arrowEndOffsetX;
    private int arrowEndOffsetY;
    private int arrowHead1X;
    private int arrowHead1Y;
    private int arrowHead2X;
    private int arrowHead2Y;
    private boolean drawArrow = false;
    private ProdpunktGeoPosition position;
    private boolean initGraphics;
    
    private BasicStroke outline = new BasicStroke(3);
    public ProdpunktPointLayer(String id, ProdpunktGeoPosition position, Color color, boolean triggerRecenterAndZoom) {
        super(id, position.getGeoPosition(), color, triggerRecenterAndZoom);
        
        this.position = position;
        this.pointColorFill = color;
        this.pointColorBorder = new Color(pointColorFill.getRed(), pointColorFill.getGreen(), pointColorFill.getBlue()).darker();
        this.pointColorBorderAlpha = new Color(pointColorBorder.getRed(),pointColorBorder.getGreen(), pointColorBorder.getBlue(), 128);
        
        if (position.getProdpunktVO()!=null&&position.getProdpunktVO().getRetning()!=null){
	        double retning = position.getProdpunktVO().getRetning();
	        double vinkel = (90 - retning) * Math.PI / 180.0;
	        double pilVinkel1 = vinkel + Math.PI*0.8;
	        double pilVinkel2 = vinkel - Math.PI*0.8;
	        arrowEndOffsetX = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.cos(vinkel));
	        arrowEndOffsetY = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.sin(vinkel));
	        arrowHead1X = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.cos(pilVinkel1));
	        arrowHead1Y = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.sin(pilVinkel1));
	        arrowHead2X = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.cos(pilVinkel2));
	        arrowHead2Y = (int)(LayerConstants.DEFAULT_DIRECTION_ARROW_LENGTH*Math.sin(pilVinkel2));
	        drawArrow=true;
        }
    }
     private Rectangle viewport;
     public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
    	 if (isInitGraphics()){
    		 g = super.initGraphics(g, map);
    	 }
         g.setColor(pointColorFill);
         
         boolean paint = false;
         viewport = map.getViewportBounds();
         
         g.setColor(pointColorFill);
         GeoPosition gp = new GeoPosition(position.getGeoPosition().getLatitude(), position.getGeoPosition().getLongitude());
         Point2D pt1 = map.getTileFactory().geoToPixel(gp, map.getZoom());
         
         if (!isDrawOutsideViewport()){
             if (viewport.contains(pt1)){
                 paint = true;
             }
         }else
             paint = true;
         
         
         if (paint){
             
             g.setStroke(new BasicStroke(2));
             int x = (int)pt1.getX();
             int y = (int)pt1.getY();
             int dia = LayerConstants.DEFAULT_POINT_DIAMETER;
             g.fillOval(x-(dia/2), y-(dia/2), dia+1, dia+1);
             //pil
             if (drawArrow){
            	 g.drawLine(x+arrowEndOffsetX/2, y-arrowEndOffsetY/2, x+arrowEndOffsetX, y-arrowEndOffsetY);
            	 g.drawLine(x+arrowEndOffsetX, y-arrowEndOffsetY, x+arrowEndOffsetX+(int)(arrowHead1X/1.5), y-arrowEndOffsetY-(int)(arrowHead1Y/1.5));
            	 g.drawLine(x+arrowEndOffsetX, y-arrowEndOffsetY, x+arrowEndOffsetX+(int)(arrowHead2X/1.5), y-arrowEndOffsetY-(int)(arrowHead2Y/1.5));
             }
             g.setColor(pointColorBorder);
             g.drawOval(x-(dia/2), y-(dia/2), dia, dia);
             if (position.isGrunndata()){
            	 g.setColor(pointColorBorderAlpha);
            	 g.setStroke(outline);
            	 g.drawOval(x-(dia/2)-5, y-(dia/2)-5, dia+10, dia+10);
             }
         }
     }
     /**
      * Returnerer TextboxData med informasjon om layeret. 
      * 
      * @return
      */
     public List<TextboxData> getInfo() {
         if (getTextboxProducer()!=null){
             return getTextboxProducer().getTextboxData();
         }
         return null;
     }
     public Layer getSelectionLayer() {
    	 Layer l = super.getSelectionLayer();
    	 l.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
    	 return l;
     }
     @Override
     public TextboxWidget getTextboxLayer(){
    	 TextboxWidget layer = new TextboxWidget(null, 10, Widget.GLUE.TOP_LEFT, getInfo());
    	 layer.setPreferredLayerType(MipssMapLayerType.TEXTBOX.getType());
    	 return layer;
     }
     
     public ProdpunktGeoPosition getProdpunktGeoPosition(){
         return position;
     }
	public boolean isInitGraphics() {
		return initGraphics;
	}
	public void setInitGraphics(boolean initGraphics) {
		this.initGraphics = initGraphics;
	}
 }
