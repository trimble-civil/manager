package no.mesta.mipss.mapplugin.util;

import java.awt.Point;
import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.persistence.areal.Areal;

import org.jdesktop.swingx.mapviewer.GeoPosition;

public class MapUtilities {
	
	public static Bounds convertArealToBounds(Areal a){
    	Bounds b= new Bounds();
    	b.setLlx(a.getX1());
    	b.setLly(a.getY1()-a.getDeltaY());
    	b.setUrx(a.getX1()+a.getDeltaX());
    	b.setUry(a.getY1());
    	return b;
    }
	
	public static double calculateDistance(GeoPosition gp1, GeoPosition gp2){
    	double x1 = Math.pow(gp1.getLongitude()-gp2.getLongitude(), 2);
		double y1 = Math.pow(gp1.getLatitude()-gp2.getLatitude(), 2);		
		double l = Math.sqrt(x1+y1);
		return l;
    }
	
	public static double calculateDistance(MipssGeoPosition gp1, MipssGeoPosition gp2){
    	double x1 = Math.pow(gp1.getLongitude()-gp2.getLongitude(), 2);
		double y1 = Math.pow(gp1.getLatitude()-gp2.getLatitude(), 2);		
		double l = Math.sqrt(x1+y1);
		return l;
    }
	
	public static double calculateDistance(Point p1, Point p2){
    	double x1 = Math.pow(p1.getX()-p2.getX(), 2);
		double y1 = Math.pow(p1.getY()-p2.getY(), 2);		
		double l = Math.sqrt(x1+y1);
		return l;
    }
	
	public static double calculateDistance(List<MipssGeoPosition[]> list){
		double dist = 0;
		for (MipssGeoPosition[] gp:list){
			dist+=calculateDistance(gp[0], gp[1]);
		}
		return dist;
	}
}
