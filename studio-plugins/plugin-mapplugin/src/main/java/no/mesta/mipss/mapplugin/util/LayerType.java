package no.mesta.mipss.mapplugin.util;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class defines a LayerType. What that mean is in which order
 * the associated layer should be painted on the map. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class LayerType implements Comparable<LayerType>{
    private int order;
    private String name;
    private boolean loose;

	/**
     * Constructor
	 *
     * @param order the order of the layer
     * @param name a plain name used for human identification
     */
    public LayerType(int order, String name){
        this.order = order;
        this.name = name;
    }
    
    /**
     * Constructor
     * @param order the order of the layer
     * @param name a plain name used for human identification
     * @param loose set this LayerType to be loose
     * 		  A loose LayerType is a layer that will be removed by 
     * 		  the layer handler when another LayerType needs its place. 
     * 		  (i.e a SelectionLayer is a typical loose LayerType)
     */
    public LayerType(int order, String name, boolean loose){
        this.order = order;
        this.name = name;
        this.loose = loose;
    }
    /**
     * Returns the ordering number
     * @return
     */
    public int getOrder(){
        return order;
    }
    
    /**
     * Returns the name
     * @return
     */
    public String getName(){
        return name;
    }
    
    /**
     * Compare this LayerType to another, gives it a natural ordering.
     * @param o the other LayerType
     * @return -1 if this.order is smaller than o.value
     *          1 if this.order is larger than o.value
     *          0 if this.order is equal to o.value
     */
    public int compareTo(LayerType o) {
        int value = o.getOrder();
        return (order<value? -1 : (order==value? 0 : 1));
    }
    
    /**
     * Returns true if o is a LayerType object and o.order == this.order
     */
    public boolean equals(Object o){
        if (o instanceof LayerType){
            return (order == ((LayerType)o).getOrder())&&name.equals(((LayerType)o).getName());
        }
        return false;
    }
    
    public String toString(){
    	return new ToStringBuilder(this).append(order).append(name).append(loose).toString();
    }
    /**
     * Returns true if this LayerType is a loose LayerType.
     * A loose LayerType is a layer that will be removed by the layer handler
     * when another LayerType needs its place. (i.e a SelectionLayer is a typical loose LayerType
     * @return
     */
    public boolean isLoose() {
		return loose;
	}
    
    /**
     * Set this LayerType to be loose
     * @param loose
     */
	public void setLoose(boolean loose) {
		this.loose = loose;
	}
}