package no.mesta.mipss.mapplugin.util.textboxdata;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssmapserver.MipssMap;
import no.mesta.mipss.persistence.kontrakt.Rode;

/**
 * Lager en textboks for Roder
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RodeTextboxProducer implements TextboxProducer{

	private Rode rode;
	public RodeTextboxProducer(Rode rode){
		this.rode = rode;
	}
	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		TextboxData info = new TextboxData();
        info.setTitle("Rode");
        info.addData("Rodenavn", rode.getNavn());
        info.addData("Rodetype", rode.getRodetype().getNavn());
        info.addData("Gyldig fra", MipssDateFormatter.formatDate(rode.getGyldigFraDato(), MipssDateFormatter.DATE_TIME_FORMAT));
        info.addData("Gyldig til", MipssDateFormatter.formatDate(rode.getGyldigTilDato(), MipssDateFormatter. DATE_TIME_FORMAT));
        info.addData("Opprettet dato", MipssDateFormatter.formatDate(rode.getOwnedMipssEntity().getOpprettetDato(), MipssDateFormatter.DATE_TIME_FORMAT));
        info.addData("Opprettet av", rode.getOwnedMipssEntity().getOpprettetAv());
        info.addData("Endret dato", MipssDateFormatter.formatDate(rode.getOwnedMipssEntity().getEndretDato(), MipssDateFormatter.DATE_TIME_FORMAT));
        info.addData("Endret av", rode.getOwnedMipssEntity().getEndretAv());
        MipssMap m = BeanUtil.lookup(MipssMap.BEAN_NAME, MipssMap.class);
		Double rodeLengde = m.getRodeLengde(rode.getId());
		DecimalFormat df2 = new DecimalFormat("####.000");
		info.addData("Rodelengde", df2.format(rodeLengde)+" km");
        dataList.add(info);
        return dataList;
	}
}
