package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.widgets.TextboxData;


public class PeriodLineListLayer extends LineListLayer implements Serializable{
    private Date start;
    private Date stopp;
    private Long dfuId;
	private String guid;
    /**
     * 
     * @param id
     * @param info
     * @param points
     * @param color
     * @param lineWidth
     * @param triggerRecenterAndZoom
     * @param bounds
     * @param start
     * @param stopp
     * @param enhetId
     */
    public PeriodLineListLayer(String id, TextboxData info, List<MipssGeoPosition[]> points, Color color, int lineWidth, boolean triggerRecenterAndZoom, Bounds bounds, Date start, Date stopp, Long dfuId) {
        super(id, info, points, color, lineWidth, triggerRecenterAndZoom, bounds, false);
        this.start = start;
        this.stopp = stopp;
        this.dfuId = dfuId;
    }

    public Date getStart() {
        return start;
    }
    public void setStart(Date start){
    	this.start = start;
    }
    public Date getStopp() {
        return stopp;
    }
    public void setStopp(Date stopp){
    	this.stopp = stopp;
    }
    public Long getDfuId() {
        return dfuId;
    }
    public void setDfuId(Long dfuId){
    	this.dfuId = dfuId;
    }
    
    public PeriodLineListLayer clone(){
    	return new PeriodLineListLayer(getId(), null, getPoints(), getColor(), getStroke(),  false, getBounds(), start, stopp, dfuId);
    }

	public void setGuid(String guid) {
		this.guid = guid;	
	}
	public String getGuid(){
		return guid;
	}

	
}
