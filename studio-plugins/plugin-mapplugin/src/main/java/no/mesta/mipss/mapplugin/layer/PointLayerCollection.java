package no.mesta.mipss.mapplugin.layer;

import java.awt.Graphics2D;
import java.util.Iterator;
import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;

/**
 * A collection of PointLayer objects to create a series of waypoints where each 
 * waypoint has its own visual appearance.
 * 
 * @author Harald A. Kulø
 */
public class PointLayerCollection extends Layer implements LayerCollection, SelectableLayer{

    private List<? extends CollectionEntity> pointLayers;
    private TextboxData data;
    private transient Logger log = LoggerFactory.getLogger(this.getClass());
    
    private CollectionPainter collectionPainter;
    
    public PointLayerCollection(String id, List<? extends CollectionEntity> pointLayers, Bounds bounds, boolean isTriggerRecenterAndZoom) {
        super(id, isTriggerRecenterAndZoom);
        this.pointLayers = pointLayers;
        setBounds(bounds);
        
        //set this layer as parent for all the layers in this collection
        for (CollectionEntity l:pointLayers){
            l.setParent(this);
        }
    }
    
    public void addPointLayer(CollectionEntity layer){
    	layer.setParent(this);
    	List<CollectionEntity> p = (List<CollectionEntity>)pointLayers;
    	p.add(layer);
    	if (getBounds().isNull())
    		getBounds().setBounds(((Layer)layer).getBounds());
    	else
    		getBounds().addBounds(((Layer)layer).getBounds());
    	
    	if (collectionPainter!=null)
    		collectionPainter.getLayers().add(layer);
    }
    
    public void addPointLayers(List<CollectionEntity> layerList){
    	List<CollectionEntity> p = (List<CollectionEntity>)pointLayers;
    	p.addAll(layerList);
    	
    	for (CollectionEntity layer:layerList){
    		layer.setParent(this);
	    	if (getBounds().isNull())
	    		getBounds().setBounds(((Layer)layer).getBounds());
	    	else
	    		getBounds().addBounds(((Layer)layer).getBounds());
    	}
    }
    
    public void clearLayers(){
    	pointLayers.clear();
    	getBounds().setBounds(new Bounds());
    	if (collectionPainter!=null){
    		collectionPainter.getLayers().clear();
    	}
    }
     public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
         if (collectionPainter!=null){
             collectionPainter.paint(g, map, w, h);
         }else{
        	 for (CollectionEntity e:pointLayers){
        		 if (e.isVisible())
        			 ((Layer)e).paint(g, map, w, h);
        	 }
         }
     }
    /**
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = super.initGraphics(g, map);
        Iterator<PointLayer> it = pointLayers.iterator();
        while (it.hasNext()){
            Layer l = it.next();
            if (l.isVisible()){
                l.paint(g, map, w, h);
            }
        }   
    }
    */
    public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius){
        Iterator<? extends CollectionEntity> it = pointLayers.iterator();
        while (it.hasNext()){
            CollectionEntity l = it.next();
            if (l.getLayerCloseTo(gp, searchRadius)!=null&&l.isVisible()){
                return l;
            }
        }
        return null;
    }
    
    public List<? extends CollectionEntity> getLayers(){
        return pointLayers;
    }
    
    public int getIndexOfLayer(CollectionEntity p){
        for (int i=0;i<getLayers().size();i++){
            if (getLayers().get(i)==p){
                return i;
            }
        }
        return -1;
    }

    public Layer getSelectionLayer() {
        return null;
    }

    public void setCollectionPainter(CollectionPainter collectionPainter) {
        this.collectionPainter = collectionPainter;
    }
}
