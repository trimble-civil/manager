package no.mesta.mipss.mapplugin.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import no.mesta.mipss.mapplugin.MipssGeoPosition;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * A layer class for creating a single line as it's own layer
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class LineLayer extends Layer {

    private MipssGeoPosition start;
    private MipssGeoPosition stop;

    private Color color;
    private int stroke;
    
    /**
     * Creates a new line layer
     * @param start lat/long for the start position
     * @param stopp lat/long for the end position
     */
    public LineLayer(String id, MipssGeoPosition start, MipssGeoPosition stop, boolean triggerRecenterAndZoom) {
        super(id, triggerRecenterAndZoom);
        this.start = start;
        this.stop = stop;
        color = Color.blue;
        stroke = 1;//new BasicStroke(1);
    }

    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = super.initGraphics(g, map);

        Stroke lineStroke = new BasicStroke(stroke);
        g.setColor(color);
        g.setStroke(lineStroke);

        java.awt.geom.Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(start.getLatitude(), start.getLongitude()), map.getZoom());
        java.awt.geom.Point2D pt2 = map.getTileFactory().geoToPixel(new GeoPosition(stop.getLatitude(), stop.getLongitude()), map.getZoom());
        g.drawLine((int)pt1.getX(), (int)pt1.getY(), (int)pt2.getX(), (int)pt2.getY());
    }

    /**
     * Get the current start location
     * @return
     */
    public MipssGeoPosition getStart() {
        return start;
    }

    /**
     * Set new start location
     * @param start
     */
    public void setStart(MipssGeoPosition start) {
        this.start = start;
    }

    /**
     * Get the current stop location
     * @return
     */
    public MipssGeoPosition getStop() {
        return stop;
    }

    /**
     * Set new stop location
     * @param stop
     */
    public void setStop(MipssGeoPosition stop) {
        this.stop = stop;
    }

    /**
     * Get the current color
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     * Set new color
     * @param color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Get the current stroke
     * @return
     */
    public int getStroke() {
        return stroke;
    }

    /**
     * Set new stroke
     * @param stroke
     */
    public void setStroke(int stroke) {
        this.stroke = stroke;
    }
    
    
}
