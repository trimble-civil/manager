package no.mesta.mipss.mapplugin.util.textboxdata;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.klimadata.wsklima.Klimadata;
import no.mesta.mipss.klimadata.wsklima.KlimadataVO;
import no.mesta.mipss.klimadata.wsklima.StasjonVO;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;

public class VaerstasjonTextboxProducer implements TextboxProducer{
	
	private StasjonVO stasjon;
	public VaerstasjonTextboxProducer(StasjonVO stasjon){
		this.stasjon = stasjon;
	}
	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		Klimadata bean = BeanUtil.lookup(Klimadata.BEAN_NAME, Klimadata.class);
		List<KlimadataVO> klimadata = bean.getDataFromStation(stasjon.getStnr());
		
		TextboxData data = new TextboxData();
    	data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.vaerstasjon.title"));
    	
    	data.addData(ResourceUtil.getResource("mapPlugin.textbox.vaerstasjon.navn"), stasjon.getNavn());
    	data.addData(ResourceUtil.getResource("mapPlugin.textbox.vaerstasjon.fylke"), stasjon.getFylke());
    	data.addData(ResourceUtil.getResource("mapPlugin.textbox.vaerstasjon.stnr"), stasjon.getStnr()+"");
    	for (KlimadataVO k:klimadata){
    		String name = k.getElement().getName();
    		String value = k.getValue();
    		data.addData(name, value);
    	}
    	if (klimadata.isEmpty()){
    		data.addData("Fant ikke klimadata", "");
    	}
    	dataList.add(data);
		return dataList;
	}
	public LayerType getPreferredLayerType(){
		return MipssMapLayerType.TEXTBOX.getType();
	}
}
