package no.mesta.mipss.mapplugin.util;


public enum MipssMapLayerType {
    
    KONTRAKTVEI 				(new LayerType(10, "kontraktvei", false)),
    PRODUKSJONPUNKT     		(new LayerType(15, "produksjonpunkt", false)),
    FRIKSJONPUNKT     			(new LayerType(15, "friksjonpunkt", false)),
    INSPEKSJONPUNKT				(new LayerType(18, "inspeksjonpunkt", false)),
    RODE        				(new LayerType(20, "rode", false)),
    TEGNERODE        			(new LayerType(20, "tegnerode", false)),
    RODESTREKNING 				(new LayerType(30, "rodestrekning", false)),
    STREKNING       			(new LayerType(40, "strekning", false)),
    PRODUKSJONSTREKNING 		(new LayerType(50, "produksjonstrekning", false)),
    INSPEKSJONSTREKNING 		(new LayerType(70, "inspeksjonstrekning", false)),
    FUNN						(new LayerType(90, "funn", false)),
    HENDELSE 					(new LayerType(100, "hendelse", false)),
    FORSIKRINGSSKADE			(new LayerType(103, "forsikringsskade", false)),
    SKRED	 					(new LayerType(106, "skred", false)),
    INSPEKSJONFUNN				(new LayerType(90, "inspeksjonfunn", false)),
    INSPEKSJONHENDELSE 			(new LayerType(100, "inspeksjonhendelse", false)),
    INSPEKSJONFORSIKRINGSSKADE	(new LayerType(104, "inspeksjonfunn", false)),
    INSPEKSJONSKRED 			(new LayerType(107, "inspeksjonhendelse", false)),
    PIN 						(new LayerType(90, "pin", false)),
    VAERSTASJON					(new LayerType(110, "vaerstasjon", false)),
    WEBCAM						(new LayerType(115, "webcam", false)),
    SELECTED            		(new LayerType(120, "selected", true)),
    LABEL               		(new LayerType(140, "label", false)),
    PRODUKSJONLABEL     		(new LayerType(150, "produksjonlabel", false)),
    INSPEKSJONLABEL     		(new LayerType(160, "inspeksjonlabel", false)),
    TEXTBOX             		(new LayerType(170, "textbox", true)),
    ALLWAYSONTOP				(new LayerType(1000, "top", false));
    
    
    private final LayerType type;
    
    MipssMapLayerType(LayerType type){
        this.type = type;
    }
    
    public LayerType getType(){
        return type;
    }
    
}
