package no.mesta.mipss.mapplugin.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;

import javax.swing.event.MouseInputListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.Layer;

import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.KeyFrames;
import org.jdesktop.animation.timing.interpolation.KeyTimes;
import org.jdesktop.animation.timing.interpolation.KeyValues;
import org.jdesktop.animation.timing.interpolation.PropertySetter;
import org.jdesktop.swingx.JXMapViewer;

public abstract class Widget extends Layer implements MouseInputListener{
	private int x;
	private int y;
	protected Color paneColor=new Color(1, 1, 1, 0.85f);
	protected Color textColor= new Color(102,136,204);
	protected Color textColorT= new Color((float)textColor.getRed()/255,(float)textColor.getGreen()/255,(float)textColor.getBlue()/255, 0.3f);
	protected Color bgColor = Color.white;      
	protected Color borderColor = Color.gray;
	protected Color borderColorT = new Color((float)borderColor.getRed()/255,(float)borderColor.getGreen()/255,(float)borderColor.getBlue()/255, 0.5f);
	protected int strokeWidth = 5;
	protected BasicStroke borderStroke = new BasicStroke(strokeWidth);
	
	protected BasicStroke borderStrokeOne = new BasicStroke(1);
	public enum GLUE{
		TOP_LEFT, 
		TOP_RIGHT, 
		TOP_CENTER,
		BOTTOM_LEFT,
		BOTTOM_RIGHT,
		BOTTOM_CENTER,
		RIGHT_CENTER,
		LEFT_CENTER,
		CENTER
	}
	protected GLUE glue;
	
	protected MapPanel map;
	private int width;
	private final int pad;
	private int height;
	private Stroke stroke;
	private AffineTransform transform;
	private Font font;

	public Widget(MapPanel map, int width, int height, int pad, GLUE glue) {
		super("", false);
		this.map = map;
		this.width = width;
		this.height = height;
		this.pad = pad;
		this.glue = glue;
	}
	public Widget(String id, MapPanel map, int width, int height, int pad, GLUE glue) {
		super(id, false);
		this.map = map;
		this.width = width;
		this.height = height;
		this.pad = pad;
		this.glue = glue;
	}
	protected void initXY(){
		if (map!=null && glue !=null){
			switch(glue){
			case TOP_RIGHT : 
				x = map.getWidth()-getWidth()-getPad();
				y = getPad();
				break;
			case TOP_CENTER :
				x = map.getWidth()/2-getWidth()/2;
				y = getPad();
				break;
			case TOP_LEFT:
				x = getPad();
				y = getPad();
				break;
			case BOTTOM_RIGHT:
				x = map.getWidth()-getWidth()-getPad();
				y = map.getHeight()-getHeight()-getPad();
				break;
			case BOTTOM_CENTER:
				x = map.getWidth()/2-getWidth()/2;
				y = map.getHeight()-getHeight()-getPad();
				break;
			case BOTTOM_LEFT:
				x = getPad();
				y = map.getHeight()-getHeight()-getPad();
				break;
			case LEFT_CENTER:
				x = getPad();
				y = map.getHeight()/2-getHeight()/2;
				break;
			case RIGHT_CENTER:
				x = map.getWidth()-getWidth()-getPad();
				y = map.getHeight()/2-getHeight()/2;
				break;
			case CENTER:
				x = map.getWidth()/2-getWidth()/2;
				y = map.getHeight()/2-getHeight()/2;
				break;
			}
		}
	}
	
	public void copyState(Graphics2D g){
		transform = g.getTransform();
		font = g.getFont();
		stroke = g.getStroke();
	}
	public void resetState(Graphics2D g){
		g.setTransform(transform);
		g.setFont(font);
		g.setStroke(stroke);
	}
	@Override
	public abstract void paint(Graphics2D g, JXMapViewer object, int width, int height);
	
	@Override
	public abstract boolean equals(Object o);
	
	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}

	public void setGlue(GLUE glue) {
		this.glue = glue;
	}

	public GLUE getGlue() {
		return glue;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getX() {
		return x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return y;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setWidth(int width) {
		this.width=width;
	}
	public void setHeight(int height) {
		this.height=height;
	}
	
	private int getPad() {
		return pad;
	}
	public MapPanel getMap() {
		return map;
	}
	public void setMap(MapPanel map) {
		this.map = map;
	}
	
	public void setPaneColor(Color paneColor){
		this.paneColor = paneColor;
		if(map!=null)
			map.repaint();
	}
	
	private Animator fadeBg;
	
	protected void startAnimOn(){
		if (fadeBg!=null){
			fadeBg.stop();
		}
		KeyValues kv = KeyValues.create(Color.white, new Color(1,1,1,0f));
        KeyTimes times = new KeyTimes(0f, 1.0f);
        KeyFrames f = new KeyFrames(kv, times);
        fadeBg  = PropertySetter.createAnimator(700, this, "paneColor", f);
        fadeBg.start();
	}
	protected void startAnimOff(){
		if (fadeBg!=null){
			fadeBg.stop();
		}
		KeyValues kv = KeyValues.create(new Color(1,1,1,0f),Color.white);
        KeyTimes times = new KeyTimes(0f, 1.0f);
        KeyFrames f = new KeyFrames(kv, times);
        fadeBg  = PropertySetter.createAnimator(200, this, "paneColor", f);
        fadeBg.start();
	}
}
