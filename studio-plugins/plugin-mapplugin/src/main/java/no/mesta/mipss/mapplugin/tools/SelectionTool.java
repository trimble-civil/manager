package no.mesta.mipss.mapplugin.tools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.event.EventListenerList;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.ui.components.AbstractComponentDecorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates a selection tool. And fires a event to all listeners when the mouse is
 * released. It's up to the listeneners what they want to do with the event, 
 * and can be used for zooming, multiple selection etc..
 * 
 * code originates from http://rabbit-hole.blogspot.com/ and is modified for our purpose.
 * 
 * @author Harald A. Kulø
 */
public class SelectionTool extends AbstractComponentDecorator implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    final int LINE_WIDTH = 4;
    private float phase = 0f;
    private Point origin = new Point(0, 0);
    private Timer timer;
    protected EventListenerList listenerList = new EventListenerList();
    private MapPanel mapPanel;
    public SelectionTool(MapPanel mapPanel) {
        super(mapPanel, 100);
        this.mapPanel = mapPanel;
        setDecorationBounds(new Rectangle(0, 0, 0, 0));
        Cursor zoomCursor = Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon(getClass().getResource("/images/zoomcursor.png")).getImage(), new Point(13,13), "zoom");
        mapPanel.setCursor(zoomCursor);
    }

    public void paint(Graphics graphics) {
        Graphics2D g = (Graphics2D)graphics;
        g.setColor(Color.blue);
        Rectangle r = getDecorationBounds();
        g.setStroke(new BasicStroke(LINE_WIDTH, BasicStroke.CAP_BUTT, 
                                    BasicStroke.JOIN_ROUND, 10.0f, 
                                    new float[] { 4.0f }, phase));
        g.drawRect(r.x, r.y, r.width, r.height);
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        timer = new Timer();
        TimerTask animate = new TimerTask() {
            public void run() {
                        phase += 1.0f;
                        repaint();
                    }
            };
        timer.schedule(animate, 0, 50);
        setDecorationBounds(new Rectangle(e.getX() - LINE_WIDTH / 2, 
                                          e.getY() - LINE_WIDTH / 2, 0, 0));
        origin.setLocation(e.getX(), e.getY());
    }

    public void mouseReleased(MouseEvent e) {
        timer.cancel();
        //only let bounds bigger than 10x10 fire an action event
        //so if the user double-clicks the click will be ignored. 
        if(getDecorationBounds().width>10&& getDecorationBounds().height>10){
        	Rectangle selection = getSelection();
            mapPanel.recenterAndZoom(selection);
//        	fireActionPerformed(null);
        }
        setDecorationBounds(new Rectangle(0, 0, 0, 0));
    }
    
    public Rectangle getSelection(){
        return getDecorationBounds();
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        e.consume();
        int width = Math.abs(origin.x - e.getX());
        int height = Math.abs(origin.y - e.getY());
        int x = Math.min(e.getX(), origin.x);
        int y = Math.min(e.getY(), origin.y);
        setDecorationBounds(new Rectangle(x, y, width, height));
        
    }

    public void mouseMoved(MouseEvent e) {
    }
    
    public void addActionListener(ActionListener l) {
        listenerList.add(ActionListener.class, l);
    }
    
    protected void fireActionPerformed(ActionEvent event) {
        Object[] listeners = listenerList.getListenerList();
        ActionEvent e = null;
        
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i]==ActionListener.class) {
                if (e == null) {
                      e = new ActionEvent(SelectionTool.this,ActionEvent.ACTION_PERFORMED,null);
                }
                ((ActionListener)listeners[i+1]).actionPerformed(e);
            }          
        }
    }
}
