package no.mesta.mipss.mapplugin.util.textboxdata;

public class TextBoxHelper {

	public static String wordwrap(String beskrivelse) {
		return beskrivelse!=null?beskrivelse.length()>40?beskrivelse.substring(0, 40)+"...":beskrivelse:null;
	}
}
