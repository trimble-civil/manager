package no.mesta.mipss.mapplugin;

/**
 * @author <a mailto:harkul@mesta.no>Harald A Kulø</a>
 */
public class TileUrl {
	private String url;
	private float transparency;
	
	public TileUrl(){
		
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the transparency
	 */
	public float getTransparency() {
		return transparency;
	}
	/**
	 * @param transparency the transparency to set
	 */
	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}
	
	public int hashCode(){
		return url.hashCode();
	}
	public boolean equals(Object o){
		if (o==this)return true;
		if (!(o instanceof TileUrl))return false;
		TileUrl other = (TileUrl)o;
		return other.getUrl().equals(getUrl());
	}
	
	
}
