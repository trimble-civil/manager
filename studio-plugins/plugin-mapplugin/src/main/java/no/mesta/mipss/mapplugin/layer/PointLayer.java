package no.mesta.mipss.mapplugin.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.DefaultLayerTypes;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.widgets.TextboxData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * Draws a filled circle on a given GeoPosition.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class PointLayer extends Layer implements SelectableLayer, CollectionEntity{
    public static final String POINTLAYER_SELECTION_ID = "pointlayerselection";
    private transient Logger log = LoggerFactory.getLogger(this.getClass());
    private List<TextboxData> data;
    //private GpsGeoPosition position;
    private Color pointColorFill;
    private Color pointColorBorder;
    /*private int arrowEndOffsetX;
    private int arrowEndOffsetY;
    private int arrowHead1X;
    private int arrowHead1Y;
    private int arrowHead2X;
    private int arrowHead2Y;*/
    private LayerCollection parent;
    private MipssGeoPosition point;
    private String id ;
    
    
    
    public PointLayer(String id, MipssGeoPosition point, Color color, boolean triggerRecenterAndZoom) {
        super(id, triggerRecenterAndZoom);
        this.id = id;
        this.point = point;
        
        this.pointColorFill = color;
        this.pointColorBorder = new Color(pointColorFill.getRed(), pointColorFill.getGreen(), pointColorFill.getBlue()).darker();
    }
    
    private Rectangle viewport; 
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = super.initGraphics(g, map);
        
        viewport = map.getViewportBounds();
        
        g.setColor(pointColorFill);
        Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(point.getLatitude(), point.getLongitude()), map.getZoom());
        
        boolean paint = false;
        if (!isDrawOutsideViewport()){
            if (viewport.contains(pt1)){
                paint = true;
            }
        }else
            paint = true;
        
        
        if (paint){
            g.setStroke(new BasicStroke(1));
            int x = (int)pt1.getX();
            int y = (int)pt1.getY();
            int dia = LayerConstants.DEFAULT_POINT_DIAMETER;
            g.fillOval(x-(dia/2), y-(dia/2), dia+1, dia+1);
            g.setColor(pointColorBorder);
            g.drawOval(x-(dia/2), y-(dia/2), dia, dia);
        }
    }

    public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius) {
        if (point.getLatitude()< gp.getLatitude()+searchRadius&&
            point.getLatitude()> gp.getLatitude()-searchRadius&&
            point.getLongitude()<gp.getLongitude()+searchRadius&&
            point.getLongitude()>gp.getLongitude()-searchRadius){
            return this;
        }
        else{
            return null;
        }
    }
    
    @Override
    public void setVisible(boolean visible){
        super.setVisible(visible);
        if (parent!=null)
            if (isChanged()){
                parent.setChanged(true);       
            }
    }
    
    public Layer getSelectionLayer() {
        String ids = id;
        Layer selected = new Layer(POINTLAYER_SELECTION_ID+ids, false){
            
            public void paint(Graphics2D g, JXMapViewer map, int w, int h){
                g = super.initGraphics(g, map);
                Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(point.getLatitude(), point.getLongitude()), map.getZoom());
                if (map.getViewportBounds().contains(pt1)){
                    int x = (int)pt1.getX();
                    int y = (int)pt1.getY();
                    g.setColor(Color.yellow);
                    g.setStroke(new BasicStroke(2));
                    int d = LayerConstants.DEFAULT_POINT_DIAMETER_SELECTED;
                    g.drawOval(x-(d/2), y-(d/2), d, d);
                }
            }
        };
        LayerType type = DefaultLayerTypes.SEVENTH_LAYER.getType();
        type.setLoose(true);
        selected.setPreferredLayerType(type);
        return selected;
    }

    public void setParent(LayerCollection parent) {
        this.parent = parent;
    }

    public LayerCollection getParent() {
        return parent;
    }

    public MipssGeoPosition getPoint() {
        return point;
    }
//    @Override
//    public TextboxLayer getTextboxLayer(){
//    	if (data==null){
//			data = new ArrayList<TextboxData>();
//			
//			data.addData("nodata", "nodata");
//		}
//    	return new TextboxLayer(getId(), data);
//    }
}
