package no.mesta.mipss.mapplugin.util;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.layer.LineListLayer;
import no.mesta.mipss.mapplugin.layer.PeriodLineListLayer;
import no.mesta.mipss.mapplugin.layer.ProdpunktGeoPosition;
import no.mesta.mipss.mapplugin.layer.ProdpunktPointLayer;
import no.mesta.mipss.mapplugin.util.textboxdata.InspeksjonpunktTextboxProducer;
import no.mesta.mipss.mapplugin.util.textboxdata.ProdstrekningTextboxProducer;
import no.mesta.mipss.persistence.kvernetf1.Prodpunktinfo;
import no.mesta.mipss.service.produksjon.InspeksjonVO;
import no.mesta.mipss.service.produksjon.ProdReflinkStrekning;
import no.mesta.mipss.service.produksjon.ProdpunktVO;
import no.mesta.mipss.service.produksjon.ProdtypeVO;
import no.mesta.mipss.webservice.ReflinkStrekning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @author Harald A Kulø
*/
public class LayerTranslator {
    private static Logger log = LoggerFactory.getLogger(LayerTranslator.class);
    
    
   
//    /**
//     * Konverterer en liste med punkter til en liste med geoPosisjoner.
//     * @param vertices
//     * @param b
//     * @return
//     */
//    public static List<MipssGeoPosition[]> convertReflinkStrekningListToGeoPositionList(List<ReflinkStrekning> vertices, Bounds b){
//        List<MipssGeoPosition[]> geoVertices = new ArrayList<MipssGeoPosition[]>();
//        
//        for (ReflinkStrekning point:vertices){
//            //setter boundingboxen til produksjonen.
//        	b.addBounds(point.getVectors());
//        	Point prev=null;
//        	int count = 0;
//        	for (Point p:point.getVectors()){
//        		if ((count!=0)){
//        			MipssGeoPosition[] gp = new MipssGeoPosition[2];
//        			gp[0] = new MipssGeoPosition(prev.getY(), prev.getX());
//        			gp[1] = new MipssGeoPosition(p.getY(), p.getX());
//        			geoVertices.add(gp);
//        			prev = p;
//        		}
//        		if (count==0)
//                  prev = p;
//              count++;
//        	}
//        }
//        return geoVertices;
//    }
    
    
    
}
