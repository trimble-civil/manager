package no.mesta.mipss.mapplugin.widgets;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;

import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

/**
 * A pojo that contains the data for a textbox layer.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public class TextboxData implements Serializable{
    private String title;
    
    private List<TextboxLine> tdata;
//    private byte[] image;
    private List<Bilde> bilder;
    private List<ImageIcon> thumbs;
    private boolean loadingDone=true;
	private Action action;
	
    public TextboxData() {
        tdata = new ArrayList<TextboxLine>();
        bilder = new ArrayList<Bilde>();
        thumbs = new ArrayList<ImageIcon>();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    
    public void addData(String title, String data){
        tdata.add(new TextboxLine(title, data));
    }

    public List<TextboxLine> getData(){
        return tdata;
    }

    public List<ImageIcon> getThumbs() {
    	return thumbs;
	}
    public Action getAction(){
    	return action;
    }
    public void setAction(Action action){
    	this.action = action;
    }
//    public byte[] getImageArray(){
//    	return image;
//    }
    public void addBilde(Bilde bilde){
    	bilder.add(bilde);
    	thumbs.add(new ImageIcon(ImageUtil.scaleImage(new ImageIcon(bilde.getBildeLob()), 100, 100)));
    }
    public List<Bilde> getBilder(){
    	return bilder;
    }
    
    public void setLoadingDone(boolean loadingDone){
    	this.loadingDone = loadingDone;
    }
    public boolean isLoadingDone(){
    	return loadingDone;
    }
//	public void setImage(byte[] image) {
//		this.image = image;
//		thumb = new ImageIcon(ImageUtil.scaleImage(new ImageIcon(image), 100, 100));
//	}

	public String toString(){
        return title+" "+tdata;
    }
    
    class TextboxLine implements Serializable{
        private String title;
        private String data;
        public TextboxLine(String title, String data){
            this.title = title;
            this.data = data;
        }
        
        public String getTitle(){
            return title;
        }
        public String getData(){
            return data;
        }
    }
}
