package no.mesta.mipss.mapplugin.widgets;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

import no.mesta.mipss.mapplugin.MapPanel;

import org.jdesktop.swingx.JXMapViewer;

public class LabelWidget extends Widget{

	private final String text;
	private BasicStroke one = new BasicStroke(1);
	private Font font;
	
	public LabelWidget(MapPanel map, int pad, GLUE glue, String text, int fontSize) {
		super(map, 0, 0, pad, glue);
		this.text = text;
		font = new Font("Tahoma", Font.PLAIN, fontSize);
	}
	public void setFont(Font font){
		this.font = font;
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer object, int w, int h) {
		copyState(g);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setFont(font);
		int textWidth = g.getFontMetrics().stringWidth(text);
		int textHeight = g.getFontMetrics().getHeight();
		setWidth(textWidth+10);
		setHeight(textHeight+10);
		initXY();
		
		g.translate(getX(), getY());
		g.setStroke(one);
		g.setColor(paneColor);
		g.fillRoundRect(0, 0, textWidth+10, textHeight+10, 10, 10);
		g.setColor(borderColor);
		g.drawRoundRect(0, 0, textWidth+10, textHeight+10, 10, 10);
		g.setColor(textColor);
		g.drawString(text, 5, textHeight);
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		resetState(g);
	}
	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof LabelWidget)){
			return false;
		}
		if (o==this){
			return true;
		}
		return false;
	}
}
