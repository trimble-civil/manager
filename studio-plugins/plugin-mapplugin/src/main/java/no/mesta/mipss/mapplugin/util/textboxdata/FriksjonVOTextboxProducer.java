package no.mesta.mipss.mapplugin.util.textboxdata;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;

public class FriksjonVOTextboxProducer implements TextboxProducer {

	private final FriksjonVO f;
	private ProduksjonService bean;
	public FriksjonVOTextboxProducer(FriksjonVO f){
		this.f = f;
		bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> dataList = new ArrayList<TextboxData>();
		String eksterntNavn = bean.getEksterntnavnFromDfu(f.getDfuId(), f.getDate(), f.getDate());
		
		String date = MipssDateFormatter.formatDate(f.getDate(), MipssDateFormatter.DATE_FORMAT);
    	String kl = MipssDateFormatter.formatDate(f.getDate(), MipssDateFormatter.TIME_FORMAT);
		
		TextboxData data = new TextboxData();
		data.setTitle("Friksjonsdata");
		data.addData(ResourceUtil.getResource("prodpunkt.textbox.kjoretoy"), eksterntNavn);
		data.addData("Dato", date);
		data.addData("Kl", kl);
//		data.addData("Regnr", f.getRegnr());
//		data.addData(ResourceUtil.getResource("maalertype"), f.getMaalertype());
//		data.addData("Veiinfo", f.getVegnrhp());
//		data.addData("Retning", f.getRetning());
		data.addData("Friksjon", f.getFriksjon()+"");
		data.addData("Hastighet", f.getHastighet()+"");
		String vei = f.getVeikategori()+f.getVeistatus()+f.getVeinummer()+" hp:"+f.getHp()+" meter:"+f.getMeter();
		data.addData("Vei", vei);
		data.addData("NMEA", f.getNmea());
//		data.addData("Temp", f.getTemperatur()+"");
//		data.addData(ResourceUtil.getResource("nedbor"), f.getNedbor()!=null?f.getNedbor().getNavn():"Ukjent");
//		data.addData(ResourceUtil.getResource("fore"), f.getFore()!=null?f.getFore().getNavn():"Ukjent");
//		data.addData("Start tiltak", f.getStartTiltak()==null?"":f.getStartTiltak());
//		data.addData(ResourceUtil.getResource("fulftilt"), f.getFullfortTiltak()==null?"":f.getFullfortTiltak());
//		data.addData("Hvem", f.getUtfortAv());
//		data.addData("Merknader", f.getBeskrivelse());
		dataList.add(data);
		
		return dataList; 
	}
}