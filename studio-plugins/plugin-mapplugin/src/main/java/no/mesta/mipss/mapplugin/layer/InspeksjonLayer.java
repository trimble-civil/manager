package no.mesta.mipss.mapplugin.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;
import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class InspeksjonLayer extends Layer implements SelectableLayer, LayerCollection{
	
	private List<PeriodLineListLayer> inspeksjonStrekninger;
	private CollectionPainter collectionPainter;
	public InspeksjonLayer(String id, boolean triggerRecenterAndZoom, List<PeriodLineListLayer> inspeksjonStrekninger, Bounds bounds){
		super(id, triggerRecenterAndZoom);
		this.inspeksjonStrekninger = inspeksjonStrekninger;
		setBounds(bounds);
		
	}
	@Override
	public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
		if (collectionPainter!=null){
            collectionPainter.paint(g, map, w, h);
        }
	}
	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius) {
		for (PeriodLineListLayer l:inspeksjonStrekninger){
			if (l.getLayerCloseTo(gp, searchRadius)!=null)
				return this;
		}
		return null;
	}
	
	@Override
	public Layer getSelectionLayer() {
		/*Layer l = new Layer("inspeksjonlayerselection", false){
			@Override
			public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
				for (PeriodLineListLayer l:inspeksjonStrekninger){
					l.getSelectionLayer().paint(g, map, w, h);
				}
			}
			@Override
			public LayerType getPreferredLayerType() {
				return MipssMapLayerType.SELECTED.getType();
			}
		};
		return l;*/
		Layer l = new Layer("inspeksjonlayerselection", false){
			private BasicStroke lineStroke = new BasicStroke(3);
			@Override
			public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
				g = super.initGraphics(g, map);
				g.setColor(Color.orange);
                g.setStroke(lineStroke);
				for (PeriodLineListLayer pl:inspeksjonStrekninger){
					List<MipssGeoPosition[]> points = pl.getPoints();
					for (MipssGeoPosition[] gp:points){
                        java.awt.geom.Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(gp[0].getLatitude(), gp[0].getLongitude()), map.getZoom());
                        java.awt.geom.Point2D pt2 = map.getTileFactory().geoToPixel(new GeoPosition(gp[1].getLatitude(), gp[1].getLongitude()), map.getZoom());
                        if (map.getViewportBounds().contains(pt1)||map.getViewportBounds().contains(pt2)){
                        	g.drawLine((int)pt1.getX(), (int)pt1.getY(), (int)pt2.getX(), (int)pt2.getY());
                        }
                    }
				}
			}
			@Override
			public LayerType getPreferredLayerType() {
				return MipssMapLayerType.SELECTED.getType();
			}
		};
		return l;
	}
	/**
	 * @return the inspeksjonStrekninger
	 */
	public List<PeriodLineListLayer> getInspeksjonStrekninger() {
		return inspeksjonStrekninger;
	}
	@Override
	public int getIndexOfLayer(CollectionEntity l) {
		//TODO implement
		return 0;
	}
	@Override
	public List<? extends CollectionEntity> getLayers() {
		return inspeksjonStrekninger;
	}
	@Override
	public void setCollectionPainter(CollectionPainter painter) {
		this.collectionPainter = painter;
		
	}
	
	
	
}
