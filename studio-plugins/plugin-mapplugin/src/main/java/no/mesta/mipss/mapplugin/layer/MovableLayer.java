package no.mesta.mipss.mapplugin.layer;

import java.awt.Point;
import java.awt.geom.Rectangle2D;

/**
 * Interface that defines the rules for a layer to be movable by mouse or keyboard events
 * It's up to the implementing class if it needs the starting point for the move operation, 
 * if it's a static move from one location to another the moveTo(Point) method should suffice.
 * 
 * MovableLayers are handeled by the MoveLayerTool, the setStartPoint is invoked in the mousePressed
 * method and moveTo is invoked in the mouseDragged method. 
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public interface MovableLayer {

    /**
     * Set a new location for the layer. 
     * 
     * @param location in pixels in the components coordinate space. (ie. mouseDragged.getX() & getY())
     * @return
     */
    boolean moveTo(Point location);
    
    /**
     * Sets the start location for the move operation. This method is
     * invoked when the layer starts to move to give the implementing class
     * the original location from where the layer starts to move
     * 
     * @param location in pixels in the components coordinate space. (ie. mousePressed.getX() & getY())
     */
    void setStartPoint(Point location);
    /**
     * The bounding box for the layer, this defines the area that the user can click 
     * on to move the layer in pixel space
     * @return
     */
    Rectangle2D getLayerBounds();
    
}
