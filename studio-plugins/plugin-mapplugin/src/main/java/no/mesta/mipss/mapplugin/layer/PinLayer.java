package no.mesta.mipss.mapplugin.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import javax.swing.ImageIcon;

import no.mesta.mipss.mapplugin.MipssGeoPosition;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.textboxdata.FunnTextboxProducer;
import no.mesta.mipss.mapplugin.util.textboxdata.PunktregOverviewTextboxProducer;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class PinLayer extends Layer implements CollectionEntity{
	public enum ORIENTATION{
		LEFT, 
		RIGHT,
		CENTER
	}
	
	private ORIENTATION orientation;
	

	private GeoPosition pos;
	private LayerCollection parent;
	private ImageIcon image;
	private GeoPosition clickPos;
	private boolean merk;
	
	public PinLayer(String id, boolean triggerRecenterAndZoom, ORIENTATION orientation, GeoPosition pos, boolean merk) {
		super(id, triggerRecenterAndZoom);
		this.orientation = orientation;
		this.pos = pos;
		this.merk = merk;
	}
	public void setImage(ImageIcon image){
		this.image = image;
	}

	@Override
	public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
		if (pos!=null){
			//last bilde første gang..
			if (image==null){
	        	switch(orientation){
	        		case LEFT: image = IconResources.MAP_PIN_LEFT; break;
	        		case RIGHT: image = IconResources.MAP_PIN_RIGHTY; break;
	        		case CENTER: image = IconResources.MAP_PIN_CENTER; break;
	        	}
	        }
			
			g = super.initGraphics(g, map);
	        Point2D point = map.getTileFactory().geoToPixel(pos, map.getZoom());
	        
	        if (map.getViewportBounds().contains(point)){
		        switch(orientation){
					case LEFT:
						int x = (int)point.getX() - image.getIconWidth();
				        int y = (int)point.getY() - image.getIconHeight();
						clickPos = map.getTileFactory().pixelToGeo(new Point2D.Double(x+10, y+10), map.getZoom());
						g.drawImage(image.getImage(), x,y,null);
						g.setColor(Color.black);
						if (getTextboxProducer() instanceof FunnTextboxProducer){
							g.drawString(((FunnTextboxProducer)getTextboxProducer()).getAntallPunkter()+"", x+9, y+15);
						}
						if (getTextboxProducer() instanceof PunktregOverviewTextboxProducer){
							g.drawString(((PunktregOverviewTextboxProducer)getTextboxProducer()).getAntallPunkter()+"", x+9, y+15);
						}
						break;
					case RIGHT: 
						x = (int)point.getX();
				        y = (int)point.getY() - image.getIconHeight();
						clickPos = map.getTileFactory().pixelToGeo(new Point2D.Double(x+10, y+10), map.getZoom());
						g.drawImage(image.getImage(), x,y,null);
						break;
					case CENTER:
						x = (int)point.getX()-8;
				        y = (int)point.getY() - image.getIconHeight();
				        clickPos = null;
				        g.drawImage(image.getImage(), x,y,null);
						break;
		        }
		        if (merk){
		//        	g.drawRect((int)point.getX()-image.getIconWidth(), (int)point.getY()-image.getIconHeight(), image.getIconWidth(), image.getIconHeight());
		        }
	        }
		}
	}

	@Override
	public SelectableLayer getLayerCloseTo(MipssGeoPosition gp, double searchRadius) {
		searchRadius +=3;
		if (clickPos==null){
			return null;
		}
		if (clickPos.getLatitude()< gp.getLatitude()+searchRadius&&
			clickPos.getLatitude()> gp.getLatitude()-searchRadius&&
			clickPos.getLongitude()<gp.getLongitude()+searchRadius&&
			clickPos.getLongitude()>gp.getLongitude()-searchRadius){
            return this;
        }
        else{
            return null;
        }
	}

	@Override
	public LayerCollection getParent() {
		return parent;
	}

	@Override
	public void setParent(LayerCollection parent) {
		this.parent = parent;
	}

	@Override
	public Layer getSelectionLayer() {
		
		Layer layer = new Layer("pinlayerselection", false){
			public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
				ImageIcon orig = image;
				image = ImageUtil.toGrayScale(image);
				PinLayer.this.paint(g,map,w,h);
				image = orig;
			}
		};
		layer.setPreferredLayerType(MipssMapLayerType.SELECTED.getType());
        return layer;
	}

	public void setPos(GeoPosition pos) {
		this.pos = pos;
	}
	
	public ORIENTATION getOrientation() {
		return orientation;
	}
}
