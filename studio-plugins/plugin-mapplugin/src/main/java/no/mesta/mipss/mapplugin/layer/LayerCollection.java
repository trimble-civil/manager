package no.mesta.mipss.mapplugin.layer;

import java.awt.Graphics2D;

import java.util.List;

import no.mesta.mipss.mapplugin.Bounds;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.painter.Painter;

/**
 * Defines the rules for a collection that contains layers
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public interface LayerCollection {
   
    /**
     * Returns ths layers in this collection
	 *
     * @return
     */
    List<? extends CollectionEntity> getLayers();
    /**
     * Returns the index of the specifies layer
     * @param l
     * @return
     */
    int getIndexOfLayer(CollectionEntity l);
    /**
     * returns the bounding box for all the layers in the collection
     * @return
     */
    Bounds getBounds();
    /**
     * Returns true if something has changed in either the layers or in the collection
     * @return
     */
    boolean isChanged();
    /**
     * Set to true if something has changed in the layer or in the collection
     * @param changed
     */
    void setChanged(boolean changed);
    /**
     * Initialize the graphics contex
     * @param g
     * @param map
     * @return
     */
    Graphics2D initGraphics(Graphics2D g, JXMapViewer map);
    /**
     * Set the current painter for the collection
     * @param painter
     */
    void setCollectionPainter(CollectionPainter painter);
}
