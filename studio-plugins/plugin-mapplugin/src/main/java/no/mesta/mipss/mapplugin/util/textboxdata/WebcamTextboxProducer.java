package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.Webcam;
import no.mesta.mipss.mapplugin.widgets.TextboxData;

public class WebcamTextboxProducer implements TextboxProducer{

	private final Webcam webcam;
	private final Action action;

	public WebcamTextboxProducer(Webcam webcam, Action action){
		this.webcam = webcam;
		this.action = action;
		action.putValue("TEXT", "Vis bilde");
	}
	
	@Override
	public List<TextboxData> getTextboxData() {
		List<TextboxData> l = new ArrayList<TextboxData>();
		TextboxData d = new TextboxData();
		d.setTitle("Webkamera SVV");
		d.addData("Veg", webcam.getVeg());
		d.addData("Stedsnavn", webcam.getStedsnavn());
		d.addData("Landsdel", webcam.getLandsdel());
		d.addData("Info", webcam.getInfo()!=null?webcam.getInfo():"");
		d.addData("Lengdegrad", webcam.getLengdegrad()+"");
		d.addData("Breddegrad", webcam.getBreddegrad()+"");
		d.setAction(action);
		d.setLoadingDone(true);
		l.add(d);
		return l;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}

}
