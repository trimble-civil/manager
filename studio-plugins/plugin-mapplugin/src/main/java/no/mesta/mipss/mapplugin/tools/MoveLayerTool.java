package no.mesta.mipss.mapplugin.tools;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.event.MouseInputListener;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.mapplugin.layer.MovableLayer;
import no.mesta.mipss.mapplugin.widgets.TextboxWidget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A tool to move a layer. In order to move a layer the Layer has to implement
 * the Movable interface and it's up to the LayerHandler to find the layer to move.
 * 
 * @author Harald A. Kulø
 */
public class MoveLayerTool implements MouseInputListener{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private MapPanel mapPanel;
    
    private MovableLayer current;
    
    /**
     * Constructor
     * @param layerHandler The object responsible for the layers
     */
    public MoveLayerTool(MapPanel mapPanel) {
        this.mapPanel = mapPanel;
    }

    /**
     * Legger en movelayer tool på et kart
     * 
     * @param mapPanel
     */
    public static void decorateMap(MapPanel mapPanel) {
        MoveLayerTool tool = new MoveLayerTool(mapPanel);
        mapPanel.addMouseListener(tool);
        mapPanel.addMouseMotionListener(tool);
    }
    
    public void mouseClicked(MouseEvent e) {
    	
    }
    
    /**
     * Event for when the mouse is pressed. Initializes the move procedure
     * by setting the click point. Store the layer that was found to use it in the mouseDragged event
     * @param e
     */
    public void mousePressed(MouseEvent e){
    	if (current==null){
            current = mapPanel.getLayerHandler().getMovableLayerAt(e.getPoint());
        }
        
        if (current!=null){
            current.setStartPoint(e.getPoint());
            e.consume();
        }
    }
    
    /**
     * Nuke the layer from this tool when the mouse button is released
     * @param e
     */
    public void mouseReleased(MouseEvent e) {
        if (current!=null){
            mapPanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        current = null;
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
    
    /**
     * Move the layer to another position, It's up to the 
     * Layer if it wants to move or not, ie. if the point is out of bounds
     * the layer can reject the move operation
     * @param e
     */
    public void mouseDragged(MouseEvent e){
        if (current!=null){
            
            if (current.moveTo(e.getPoint())){
                mapPanel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                mapPanel.repaint();
            }
            e.consume();
        }
    }

    public void mouseMoved(MouseEvent e) {
    }
}
