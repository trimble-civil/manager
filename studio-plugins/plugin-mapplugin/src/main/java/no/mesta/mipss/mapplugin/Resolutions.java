package no.mesta.mipss.mapplugin;

/**
 * This class contains the resolutions (zoom levels) supported by the 
 * map server and operations to find zoom levels
 * 
 * @author Harald A. Kulø
 */
public class Resolutions {
    /** The resolutions array */
    private double[] resolutions;
    private int maxResIndex =0;
    
    /**
     * Constructor
     * @param resolutions the resolutions covered by the mapserver
     */
    public Resolutions(double[] resolutions){
        this.resolutions = resolutions;
        maxResIndex = resolutions.length-1;
    }
    
    /**
     * Sets new resolutions
     * @param resolutions the resolutions covered by the mapserver
     */
    public void setResolutions(double[] resolutions) {
        this.resolutions = resolutions;
        maxResIndex = resolutions.length-1;
    }
    
    /**
     * Returns the resolution for the specified zoom
     * @param zoom the zoomlevel to get resolutions from 
     * @return the resolution at this zoomlevel
     */
    public double getResolutionForZoom(int zoom){
        return resolutions[zoom];
    }
    /**
     * Returns the value for the highest resolution
     * @return the highest resolution value
     */
    public double getMaxResolution(){
        return resolutions[maxResIndex];
    }
    /**
     * Get the index for the highest resolution
     * @return the index for the highest resolution
     */
    public int getMaxResolutionIndex(){
        return maxResIndex;
    }
    /**
     * Returns the value for the lowest resolution
     * @return the lowest resolution value
     */
    public double getMinResolution(){
        return resolutions[0];
    }
    /**
     * Get the indx for the lowest resolution
     * @return the index for the lowest resolution
     */
    public int getMinResolutionIndex(){
        return 0;
    }
    
    /**
     * Get the total number of resolutions
     * @return the number of resolutions
     */
    public int getTotalResolutions(){
        return resolutions.length;
    }
    /**
     * Find the resolution that is closest to covering the number of pixels 
     * needed. If the metersPrPixels needed is higher than the zoomlevels covered then
     * return getMaxResolution(), if lower return getMinResolution(). If metersPrPixels
     * is within bounds, find the zoomlevel index that is the closest match. 
     * 
     * @param metersPrPixel how many meter pr pixel is needed
     * @return the zoomlevel closest to covering metersPrPixel within the resolutions
     */
    public int getClosestResolutionIndex(double metersPrPixel){
        int resolutionIndex = -1;
        //metersPrPixel might be out of bounds, if above or equal to maxresolution return maxResolutionIndex()
        //if below or equals to minresolution return minResolutionIndex()
        if (metersPrPixel<=getMaxResolution()){
            resolutionIndex = getMaxResolutionIndex();
        }
        else if (metersPrPixel>=getMinResolution()){
            resolutionIndex = getMinResolutionIndex();
        }
        //not out of bounds, find the zoomlevel that can at least
        //cover the pixels needed.
        else{
            for (double r:resolutions){
                if (r>metersPrPixel)
                    resolutionIndex++;
                else
                    break;
            }
        }
        return resolutionIndex;
    }
}
