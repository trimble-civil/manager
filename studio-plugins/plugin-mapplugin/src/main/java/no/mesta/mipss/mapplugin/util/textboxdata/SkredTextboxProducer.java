package no.mesta.mipss.mapplugin.util.textboxdata;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.felt.FeltModule;
import no.mesta.mipss.felt.OpenDetailMessage;
import no.mesta.mipss.mapplugin.layer.TextboxProducer;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.ResourceUtil;
import no.mesta.mipss.mapplugin.widgets.TextboxData;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.plugin.MipssPlugin;

public class SkredTextboxProducer implements TextboxProducer{

	private Skred skred;
	private String skredGuid;
	private final IMipssStudioLoader loader;

	public SkredTextboxProducer(IMipssStudioLoader loader, Skred skred){
		this.loader = loader;
		this.skred = skred;
	}
	public SkredTextboxProducer(IMipssStudioLoader loader, String skredGuid){
		this.loader = loader;
		this.skredGuid = skredGuid;
	}
	@Override
	public List<TextboxData> getTextboxData() {
		final List<TextboxData> dataList = new ArrayList<TextboxData>();

		final TextboxData data = new TextboxData();
		data.setLoadingDone(false);
		new Thread(){
			public void run(){
				if (skred==null){
					RskjemaService bean = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
					skred = bean.hentSkred(skredGuid);
				}
				data.setTitle(ResourceUtil.getResource("mapPlugin.textbox.skred.title"));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.skred.id"), String.valueOf(skred.getRefnummer()));
				data.addData(ResourceUtil.getResource("mapPlugin.textbox.sak.id"), String.valueOf(skred.getSak().getRefnummer()));
				data.addData("Kommentar", TextBoxHelper.wordwrap(skred.getKommentar()));
				data.setAction(getAction());
				dataList.add(data);
				Integer antallBilder = skred.getAntallBilder();
				if (antallBilder != null && antallBilder!=0){
					if (skred.getBilder()!=null){
						for (Bilde b:skred.getBilder()){
							data.addBilde(b);
						}
					}
				}
				data.setLoadingDone(true);
			}
		}.start();
		
		return dataList;
	}

	@Override
	public LayerType getPreferredLayerType() {
		return MipssMapLayerType.TEXTBOX.getType();
	}
	
	public Action getAction() {
		Action a = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {	
				MipssFieldDetailItem item = skred;
				List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
				list.add(item);
				MipssPlugin plugin = loader.getPlugin(FeltModule.class);
				OpenDetailMessage m = new OpenDetailMessage(list);
				m.setTargetPlugin(plugin);
				loader.sendMessage(m);
			}
		};
		a.putValue("TEXT", ResourceUtil.getResource("textbox.open"));
		return a;
	}

}
