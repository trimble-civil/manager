package no.mesta.mipss.mapplugin.util;

import java.util.EventObject;

/**
 * Eventklasse som inneholder informasjon om hva som skjedde med det Layer som ble endret.
 * 
 * @author Harald A. Kulø
 */
public class LayerEvent extends EventObject {
    public static final String TYPE_REMOVED = "removed";
    public static final String TYPE_ADDED  = "added";
    public static final String TYPE_MODIFIED = "modified";
    public static final String TYPE_SELECTED = "selected";
    public static final String TYPE_UNSELECTED = "unselected";
    
    private String type;
    private boolean triggerRecenterAndZoom = false;
    
    /**
     * Opprett et nyee LayerEvent 
     * @param source kilden til det som førte til eventet (Layer)
     * @param type hvilken type LayerEvent TYPE_REMOVED|ADDED|MODIFIED
     * @param triggerRecenterAndZoom true hvis kartet skal resentreres og zoomes rundt det nye laget
     */
    public LayerEvent(Object source, String type, boolean triggerRecenterAndZoom) {
        super(source);
        this.type = type;
        this.triggerRecenterAndZoom = triggerRecenterAndZoom;
    }
    
    /**
     * Hent type
     * @return
     */
    public String getType() {
        return type;
    }
    
    /**
     * True hvis kartet skal resentreres og zoomes rundt det nye laget
     * @return
     */
    public boolean isTriggerRecenterAndZoom() {
        return triggerRecenterAndZoom;
    }
}
