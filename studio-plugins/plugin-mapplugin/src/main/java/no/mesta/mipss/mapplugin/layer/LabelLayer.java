package no.mesta.mipss.mapplugin.layer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import no.mesta.mipss.mapplugin.MipssGeoPosition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * A layer to create a label on the map.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class LabelLayer extends Layer implements MovableLayer{ 
    private Polygon triangle;
    private Color bubbleFill;
    private Color bubbleBorder;
    private String[] label;
    
    private int width = 0;
    private int height = 0;
    
    private BasicStroke one = new BasicStroke(1);
    private BasicStroke two = new BasicStroke(2);
    
    private MipssGeoPosition position;
    private Point2D ul;
    private Rectangle labelRect;
    private Rectangle viewPort;
	private Color textColor;
    
    public LabelLayer(String id, MipssGeoPosition position, String[] label) {
        super(id, false);
        this.position = position;
        this.label = label;
        bubbleFill = new Color(0,0,1,0.5f);
        bubbleBorder = new Color(0,0,255);
        triangle = new Polygon();
        textColor = Color.white;
    }
    
    public LabelLayer(String id, MipssGeoPosition position, String[] label, Color fill, Color outline, Color text) {
        super(id, false);
        this.position = position;
        this.label = label;
        bubbleFill = fill;
        bubbleBorder = outline;
		this.textColor = text;
        triangle = new Polygon();
    }
    
    
    int intersect = 0;
    public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
        g = super.initGraphics(g, map);
        Point2D pt1 = map.getTileFactory().geoToPixel(new GeoPosition(position.getLatitude(), position.getLongitude()), map.getZoom());
        int x = (int)pt1.getX();
        int y = (int)pt1.getY();
        //calculate the height of the box
        int lineHeight = g.getFontMetrics().getHeight()-1;
        height = (label.length) * lineHeight;
        //calculate width
        for (String text:label){
            if (g.getFontMetrics().stringWidth(text)>width){
                width = g.getFontMetrics().stringWidth(text)+7;
            }
        }
        
        ul = new Point2D.Double((x),(y-height-20));
        labelRect = new Rectangle((int)ul.getX()+location.x, (int)ul.getY()+location.y, width, height);
        viewPort = map.getViewportBounds();
        
        g.setPaint(bubbleFill);
        g.fillRoundRect((int)ul.getX()+location.x, (int)ul.getY()+location.y, width, height, 10, 10);
        
        createTriangle(labelRect, x, y);
        if (triangle.npoints==1){
        	triangle.addPoint(labelRect.x+labelRect.width/2-5, labelRect.y+labelRect.height);
            triangle.addPoint(labelRect.x+labelRect.width/2+5, labelRect.y+labelRect.height);
        }
        g.setPaint(bubbleBorder);
        g.setStroke(two);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.fillPolygon(triangle);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g.setStroke(one);
        g.drawRoundRect((int)ul.getX()+location.x, (int)ul.getY()+location.y, width, height, 10, 10);
        g.setPaint(textColor);
        int yPos = 0;
        for (String text:label){
            g.drawString(text, (int)ul.getX()+3+location.x, (int)ul.getY()+10+yPos+location.y);
            yPos+=14;
        }
    }
    
    private void createTriangle(Rectangle rect, int x, int y){
    	triangle.reset();
    	triangle.addPoint(x, y);
    	if (y>rect.y && x>rect.x && x<rect.x+rect.width){
    		//South
    		triangle.addPoint(rect.x+rect.width/2-5, rect.y+rect.height);
        	triangle.addPoint(rect.x+rect.width/2+5, rect.y+rect.height);
    	}else if (y<rect.y && x>rect.x && x<rect.x+rect.width){
    		//North
	    	triangle.addPoint(rect.x+rect.width/2-5, rect.y);
	    	triangle.addPoint(rect.x+rect.width/2+5, rect.y);
    	}else if (x>rect.x && y>rect.y && y<rect.y+rect.height){
    		//East
    		triangle.addPoint(rect.x+rect.width, rect.y+rect.height/2+5);
        	triangle.addPoint(rect.x+rect.width, rect.y+rect.height/2-5);
    	}else if (x<rect.x && y>rect.y && y<rect.y+rect.height){
    		//West
    		triangle.addPoint(rect.x, rect.y+rect.height/2+5);
        	triangle.addPoint(rect.x, rect.y+rect.height/2-5);
    	}else if (y > rect.y && x>rect.x+rect.width){
    		//SOUTH_EAST
    		triangle.addPoint(rect.x+rect.width-5, rect.y+rect.height);
        	triangle.addPoint(rect.x+rect.width, rect.y+rect.height-5);
    	}else if (y > rect.y && x<rect.x){
    		//SOUTH_WEST
    		triangle.addPoint(rect.x+5, rect.y+rect.height);
        	triangle.addPoint(rect.x, rect.y+rect.height-5);
    	}else if (x > rect.x && y<=rect.y){
    		//NORTH_EAST
    		triangle.addPoint(rect.x+rect.width-5, rect.y);
        	triangle.addPoint(rect.x+rect.width, rect.y+5);
    	}else if (x <= rect.x && y<=rect.y){
    		//NORTH_WEST
    		triangle.addPoint(rect.x+5, rect.y);
        	triangle.addPoint(rect.x, rect.y+5);
    	}
    }
    
    public Rectangle getLabelRect(){
    	return labelRect;
    }
    public String[] getLabelText(){
    	return label;
    }
    public void setLabelText(String[] labelText){
    	this.label = labelText;
    }
    
	@Override
	public Rectangle2D getLayerBounds() {
		if (labelRect==null||viewPort==null)
			return new Rectangle(0,0,0,0);
		Rectangle rect = new Rectangle(labelRect.x-viewPort.x, labelRect.y-viewPort.y, labelRect.width, labelRect.height);
		return rect;
	}
	Point location = new Point(0,0);
	Point moved = new Point(0,0);
	@Override
	public boolean moveTo(Point m) {
		location.setLocation(m.x-dragStartPoint.x+moved.x, m.y-dragStartPoint.y+moved.y);
        return true;
	}
	Point dragStartPoint = null;
	@Override
	public void setStartPoint(Point point) {
		moved.setLocation(location);
		dragStartPoint = point;//new Point(rect.x-point.x, rect.y-point.y);
	}
	
	public LabelLayer clone(){
		LabelLayer l = new LabelLayer(getId(), position, label);
		l.location = location;
		l.moved = moved;
		l.setPreferredLayerType(getPreferredLayerType());
		l.bubbleFill = bubbleFill;
        l.bubbleBorder = bubbleBorder;
        l.textColor = textColor;
		return l;
        
        
	}
	public void setPosition(MipssGeoPosition position) {
		this.position = position;
	}
	public MipssGeoPosition getPosition(){
		return position;
	}
}
