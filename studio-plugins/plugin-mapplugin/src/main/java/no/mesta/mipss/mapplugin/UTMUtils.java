package no.mesta.mipss.mapplugin;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FeatureStore;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.BasicFeatureTypes;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;

/**
 * @author <a mailto:harkul@mesta.no>Harald A Kulø</a>
 */
public class UTMUtils {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    public enum Hemisphere {
        North,
        South;
    }

    private Hemisphere m_Hemisphere = Hemisphere.North;
    private int m_UtmZone = 33;
    private double m_e2;
    private double m_n;
    private double m_e_note_2;
    long m_a;
    
    public static void main (String[] args){
        UTMUtils u = new UTMUtils();
        Point2D.Double p = new Point2D.Double();
        String lat="59.743639";

        String lon = "10.209045";
        
        u.convert2UTM(lat, lon, p);
        System.out.println(p);
    }
    public UTMUtils() {

        // Initialzing variables used by Convert2UTM(), Convert2LLA()
        m_a = 6378137; // WGS-84 6378388;  Internasjonal
        double f = 1 / 298.257223563; // WGS-84 (1.0/297);Internasjonal
        m_e2 = f * (2 - f);
        m_e_note_2 = m_e2 / (1 - m_e2);
        m_n = f / (2 - f);
    }

    // Se: http://www.uaex.edu/Other_Areas/publications/HTML/FSA-1031.asp
    // Denne har ikke noe med selve formelen å gjøre, men er kun forklaring.
    // ---------------------------------------------------------------------

    public Point2D.Double convertToUtm32(Point2D.Double coord) throws Exception{
		CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32633");
		CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32632");
		MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
		Coordinate targetCoord = JTS.transform(new Coordinate(coord.getX(), coord.getY()), new Coordinate(), transform);
		return new Point2D.Double(targetCoord.x, targetCoord.y);
    }
    
    public boolean convert2UTM(String latitude, String longitude, 
                               Point2D utm) {

        double lat = getUtmCoordinate(latitude);
        double lon = getUtmCoordinate(longitude);
        return convert2UTM(lat, lon, utm);

    }

    public boolean convert2UTM(double latitude, double longitude, 
                               Point2D utm) {
        int nUtmUnit = 6; // 1 UTM zone is equal to 6 degrees

        if (latitude == 0 || longitude == 0)
            return false;

        double m_dY = latitude;
        double m_dX = longitude;

        if (m_UtmZone < 1 || m_UtmZone > 60)
            return false;

        double lambda0 = 0;

        if (m_UtmZone > 0 && m_UtmZone < 31)
            lambda0 = 
                    (Math.PI * ((m_UtmZone * nUtmUnit + 180) - (nUtmUnit / 2))) / 
                    180;
        else
            lambda0 = 
                    (Math.PI * ((m_UtmZone * nUtmUnit - 180) - (nUtmUnit / 2))) / 
                    180;

        m_dX = m_dX - lambda0;

        double W = Math.sqrt(1 - m_e2 * Math.pow(Math.sin(m_dY), 2));
        double cos2DX = Math.cos(2 * m_dY);

        double theta = 
            m_dY - ((3.0 / 2) * m_n - (31.0 / 24) * Math.pow(m_n, 3) - 
                    ((15.0 / 8) * Math.pow(m_n, 2) - 
                     (435.0 / 128) * Math.pow(m_n, 4)) * cos2DX + 
                    (35.0 / 12) * Math.pow(m_n, 3) * Math.pow(cos2DX, 2) - 
                    (315.0 / 64) * Math.pow(m_n, 4) * Math.pow(cos2DX, 3)) * 
            Math.sin(2 * m_dY);

        double cosDX = Math.cos(m_dY);
        double sinDY = Math.sin(m_dX);

        double eta = 0.5 * Math.log((1 + cosDX * sinDY) / (1 - cosDX * sinDY));
        double ksi = Math.atan(Math.tan(m_dY) / Math.cos(m_dX)) - m_dY;

        double N = m_a / W;
        double delta_gamma = 
            N * eta * (1 + (m_e_note_2 / 6) * (Math.pow(cosDX, 2)) * 
                       Math.pow(eta, 2));
        double delta_X = 
            N * ksi * (1 + (m_e_note_2 / 6) * (Math.pow(cosDX, 2)) * 
                       Math.pow(eta, 2));

        double S = 
            (m_a / (1 + m_n)) * (1 + (1 / 4) * Math.pow(m_n, 2) + (1 / 64) * 
                                 Math.pow(m_n, 4)) * theta;

        if (m_Hemisphere == Hemisphere.North)
            m_dY = 0.9996 * (delta_X + S);
        else if (m_Hemisphere == Hemisphere.South)
            m_dY = m_dY - 10.0e6;

        m_dX = 0.9996 * delta_gamma + 500000;

        utm.setLocation(m_dY, m_dX);
        return true;
    }

    private double getUtmCoordinate(String latLongCoordinate) {
        // m_LatitudeStr:    5923.9876N
        // m_LongitudeStr:  01042.1234E
        //log.trace("Get utm coordinate:"+latLongCoordinate);
        String tmp = latLongCoordinate.replaceAll("E", "").replaceAll("N", "").replace(',','.');
        double coord = Double.parseDouble(tmp);
        return coord / 180 * Math.PI;
    }
    
    public Point2D.Double convertTo(Point2D.Double coord, String srcCRS, String tarCRS) throws Exception{
		CoordinateReferenceSystem targetCRS = CRS.decode(tarCRS);
		CoordinateReferenceSystem sourceCRS = CRS.decode(srcCRS);
		MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
		Coordinate targetCoord = JTS.transform(new Coordinate(coord.getX(), coord.getY()), new Coordinate(), transform);
		return new Point2D.Double(targetCoord.x, targetCoord.y);
    }
    
	public void createPolygonShape(MipssGeoPosition[] coords, String name) throws Exception{
		final SimpleFeatureType TYPE = createFeatureType();//BasicFeatureTypes.POLYGON; 
	    FeatureCollection<SimpleFeatureType, SimpleFeature> collection = FeatureCollections.newCollection();
	    GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);
	    Coordinate[] c = new Coordinate[coords.length];
	    int i=0;
	    for (MipssGeoPosition gp:coords){
	    	Coordinate co = new Coordinate(gp.getLongitude(), gp.getLatitude());
//	    	CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:32633");
//	    	CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:32632");
	    	CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:25833");
	    	CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:25832");
			
			
			MathTransform transform = CRS.findMathTransform(sourceCRS,targetCRS, true);
			// Transform the long/lat coordinate to the target map CRS
			Coordinate targetCoord = JTS.transform(co, new Coordinate(), transform);
			c[i]=targetCoord;
			i++;
	    }
	    LinearRing linearRing = geometryFactory.createLinearRing(c);
	    featureBuilder.add(linearRing);
	    SimpleFeature feature = featureBuilder.buildFeature(null);
        collection.add(feature);
	    
	    File newFile = new File("D:/ms/h3/"+name+".shp");
	    DataStoreFactorySpi dataStoreFactory = new ShapefileDataStoreFactory();
		
	    Map<String, Serializable> params = new HashMap<String, Serializable>();
	    params.put("url", newFile.toURI().toURL());
	    params.put("create spatial index", Boolean.TRUE);
	
	    ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
	    newDataStore.createSchema(TYPE);
	    CoordinateReferenceSystem crs = CRS.decode("EPSG:25832");
	    newDataStore.forceSchemaCRS(crs);

	    /*
	     * Write the features to the shapefile
	     */
	    Transaction transaction = new DefaultTransaction("create");
	
	    String typeName = newDataStore.getTypeNames()[0];
	    FeatureStore<SimpleFeatureType, SimpleFeature> featureStore =(FeatureStore<SimpleFeatureType, SimpleFeature>) newDataStore.getFeatureSource(typeName);
	
	    featureStore.setTransaction(transaction);
	    try {
	        featureStore.addFeatures(collection);
	        transaction.commit();
	
	    } catch (Exception problem) {
	        problem.printStackTrace();
	        transaction.rollback();
	
	    } finally {
	        transaction.close();
	    }
	    //System.exit(0);
	    
	}
	
    private static SimpleFeatureType createFeatureType() {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        //builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system
		try {
			CoordinateReferenceSystem crs= CRS.decode("EPSG:32633");
			builder.setCRS(crs);
		} catch (NoSuchAuthorityCodeException e) {
			e.printStackTrace();
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	    
		builder.setName( "polygonFeature" );
        // add attributes in order
        builder.add("Location", LinearRing.class);

        // build the type
        final SimpleFeatureType LOCATION = builder.buildFeatureType();
        
        return LOCATION;
    }
}
