package no.mesta.mipss.mapplugin.util;
import javax.swing.JCheckBox;

/**
 * En Checkbox som er knyttet til en lagtype i kartpanelet
 * 
 * @author Harald A. Kulø
 */
public class JLayerCheckBox extends JCheckBox{
    private String[] layerType;
    
    public JLayerCheckBox(String layerType) {
        this.layerType = new String[]{layerType};
    }
    
    public JLayerCheckBox(String[] layerType){
        this.layerType = layerType;
    }

    public String[] getLayerType() {
        return layerType;
    }
}
