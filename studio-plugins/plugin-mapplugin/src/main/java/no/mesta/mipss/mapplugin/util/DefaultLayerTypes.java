package no.mesta.mipss.mapplugin.util;

/**
 * An enum with default layer types, this can be used where
 * the map only needs to know in which order the layer
 * is painted on the map. the layer with type FIRST_LAYER will be painted
 * first and NINTH_LAYER will be painted last. This only provides control
 * of the painting sequence, and not the purpose of the layer.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
public enum DefaultLayerTypes {
    FIRST_LAYER     (new LayerType(1, "first_layer")),
    SECOND_LAYER    (new LayerType(2, "second_layer")),
    THRID_LAYER     (new LayerType(3, "third_layer")),
    FOURTH_LAYER    (new LayerType(4, "fourth_layer")),
    FIFTH_LAYER     (new LayerType(5, "fifth_layer")),
    SIXTH_LAYER     (new LayerType(6, "sixth_layer")), 
    SEVENTH_LAYER   (new LayerType(7, "seventh_layer")),
    EIGHT_LAYER     (new LayerType(8, "eight_layer")),
    NINTH_LAYER     (new LayerType(9, "ninth_layer"));
    
    
    private final LayerType type;
    
    DefaultLayerTypes(LayerType type){
        this.type = type;
    }
    
    public LayerType getType(){
        return type;
    }
}
