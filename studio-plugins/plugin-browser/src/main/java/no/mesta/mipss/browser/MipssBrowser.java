package no.mesta.mipss.browser;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.io.FileNotFoundException;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.buttons.ButtonToolkit;

import org.apache.commons.lang.StringUtils;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

import org.jdic.web.BrTabbed;
import org.jdic.web.event.BrComponentEvent;
import org.jdic.web.event.BrComponentListener;


/**
 * MIPSS Browser plugin
 * 
 * Denne plugin benytter JDIC Plus for å embedde Internet Explorer.
 *  
 * Til forskjell fra andre plugins så startes denne plugin ved at man i menypunkt
 * legger inn en URL. Da vil MIPSS Studio spesialbehandle det menypunktet.
 *  
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
public class MipssBrowser extends MipssPlugin implements BrComponentListener{
    private BrTabbed browser = null;
    private URL url;
    private JPanel pluginPanel = null;
    
    private JButton bnPrevious = ButtonToolkit.createGoBackButton();
    private JButton bnNext = ButtonToolkit.createGoForwardButton();
    private JButton bnStop = ButtonToolkit.createStopButton();
    private JButton bnRefresh = ButtonToolkit.createRefreshButton();
    private JProgressBar pbDownload = new JProgressBar();
    private JTextField statusText = new JTextField();
    private JButton secureEmblem = ButtonToolkit.createSecureEmblem();
    
    /**
     * Konstruktøren MIPSS Studio benytter.
     * 
     * @param urlString
     */
    public MipssBrowser(final String urlString){
        try {
            this.url = new URL(urlString);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    
    /**
     * Metode for testing fra kommandolinje
     * 
     * @param args
     */
    public static void main(String[] args){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) { e.printStackTrace();}
        
        JFrame frame = new JFrame();
        frame.setSize(500,500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            MipssBrowser b = new MipssBrowser("http://www.digi.no");
            b.init();
            frame.add(b.getModuleGUI());
        } catch (Exception e) {
            e.printStackTrace();
        }
        frame.setVisible(true);
    }
    
    /**
     * Initierer std gui komponenter: Toolbar, nettleser og omsluttende. Åpnes
     * standard i ikke-fane modus.
     */
    private void init(){
        // Sørger for at JDIC plus ikke er i debug modus
        org.jdic.web.BrComponent.DESIGN_MODE = false;
        
        // JDICplus nettleseren
        browser = new BrTabbed();
        browser.setURL(url.toString());

        // Selve hovedpanelet til plugin
        pluginPanel = new JPanel();
        pluginPanel.setLayout(new BorderLayout());

        // Toolbar med knapper for tilbake, frem, refresh og stopp
        JToolBar toolbar = new JToolBar();
        toolbar.setRollover(true);
        toolbar.add(bnPrevious);
        bnPrevious.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        browser.back();
                    }
                });
        toolbar.add(bnNext);
        bnNext.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        browser.forward();
                    }
                });
        toolbar.add(bnRefresh);
        bnRefresh.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        browser.refresh();
                    }
                });
        toolbar.add(bnStop);
        bnStop.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        browser.stop();
                    }
                });
        
        // Toolbar for status
        JToolBar statusbar = new JToolBar();
        statusbar.setRollover(true);
        statusbar.setFloatable(false);
        statusText.setEditable(false);
        statusbar.add(statusText);
        statusbar.add(pbDownload);
        statusbar.add(secureEmblem);
        
        // Legger til alle komponentene til plugin
        pluginPanel.add(toolbar, BorderLayout.NORTH);
        pluginPanel.add(browser, BorderLayout.CENTER);
        pluginPanel.add(statusbar, BorderLayout.SOUTH);
        
        // Sørger for at funksjoner og felter i IE oppdaterer knapper og felter i plugin gui
        BindingGroup bindingGroup = new BindingGroup();
        // Status når man holder over linker
        Binding binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, browser, ELProperty.create("${statusText}"), statusText, BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        // om fremover knappen er aktiv eller ikke
        binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ_WRITE, browser, ELProperty.create("${goForwardEnable}"), bnNext, BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);
        // om tilbake knappen er aktiv eller ikke
        binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ_WRITE, browser, ELProperty.create("${goBackEnable}"), bnPrevious, BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);
        // om refresh knappen er aktiv
        binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ_WRITE, browser, ELProperty.create("${documentReady}"), bnRefresh, BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);
        // aktiver alle knytninger      
        bindingGroup.bind();
        
        // lytter på nettleseren for om tilkoblingen er sikker, og progress
        browser.addPropertyChangeListener(new PropertyChangeListener() {
                     public void propertyChange(PropertyChangeEvent evt) {
                         pluginPropertyChange(evt);
                     }
                 });
    }

    /**
     * Fyres fra IE
     * vi lytter på progress og om det er sikker tilkobling
     * 
     */
    private void pluginPropertyChange(PropertyChangeEvent evt) {
        String property = evt.getPropertyName();
        if (StringUtils.equals(property, "securityIcon") || 
            StringUtils.equals(property, "navigatedURL") || 
            StringUtils.equals(property, "progressBar")) {
            String value = (String) evt.getNewValue();
            if(null == value) {
                value = "";
            }
            String[] values = value.split(",");
            
            if(StringUtils.equals(property, "progressBar")) {
                int max = Integer.parseInt(values[0]), pos = Integer.parseInt(values[1]);
                if(max == 0) {
                    pbDownload.setVisible(false);
                } else {
                    pbDownload.setMaximum(max);
                    pbDownload.setValue(pos);
                    pbDownload.setVisible(true);
                }
            } else if(StringUtils.equals(property, "securityIcon")) {
                secureEmblem.setVisible(!StringUtils.equals(value, "0"));
            }
        }
    }
    
    /**
     * Kalles når plugin skal lukkes
     */
    public void shutdown() {
    }

    /**
     * Kalles når plugin skal starte opp
     * 
     */
    public void initPluginGUI() {
        init();
    }

    /**
     * Kalles av applikasjonen for å hente GUI til plugin
     * 
     * @return
     */
    public JPanel getModuleGUI() {
        return pluginPanel;
    }
	

    public URL getUrl() {
        return url;
    }
    
    public void setUrl(final URL url) {
        this.url = url;
        SwingUtilities.invokeLater( new Runnable() {
                    public void run() {
                        try {
                            browser.setURL(url);
                        } catch(FileNotFoundException e) {
                            throw new IllegalArgumentException("URL not found:" + url, e);
                        }
                    }
                });
    }
    
    public void onClosingWindowByScript(boolean isChildWindow){
         if(!isChildWindow && JOptionPane.YES_OPTION==JOptionPane.showConfirmDialog(
             getLoader().getApplicationFrame(),
             "Websiden du leser forsøker å lukke vinduet.\n" +
             "Ønsker du å lukke vinduet?",
             "Advarsel",
             JOptionPane.YES_NO_OPTION,
             JOptionPane.QUESTION_MESSAGE))
         {
             browser.closeCurrentPage();
         }
     }
     
     public String sync(BrComponentEvent e) {
         switch(e.getID()){
         case BrComponentEvent.DISPID_WINDOWCLOSING:
             String stValue = e.getValue();
             if(null!=stValue){
                 //OLE boolean: -1 - true, 0 - false; params:(bCancel, bIsChildWindow)                                       
                 final boolean isChildWindow = (0!=Integer.valueOf(stValue.split(",")[1]));
                 SwingUtilities.invokeLater ( new Runnable() { public void run() { 
                             onClosingWindowByScript(isChildWindow);
                 }});
             }    
             break;
         }
         return null;
     }
}
