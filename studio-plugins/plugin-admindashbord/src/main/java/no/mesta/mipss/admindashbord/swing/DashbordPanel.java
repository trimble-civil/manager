package no.mesta.mipss.admindashbord.swing;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.admindashbord.DashbordController;

@SuppressWarnings("serial")
public class DashbordPanel extends JPanel{
	
	private final DashbordController controller;

	public DashbordPanel(DashbordController controller){
		this.controller = controller;
		initGui();
	}

	private void initGui() {
		setLayout(new BorderLayout());
		JTabbedPane tab = new JTabbedPane();
		add(tab);
		
		AllUsersPanel allPanel = new AllUsersPanel(controller);
		OneUserPanel onePanel = new OneUserPanel(controller);
		ReportPanel reportPanel = new ReportPanel(controller);
		tab.addTab("Alle brukere", allPanel);
		tab.addTab("En bruker", onePanel);
		tab.addTab("Rapporter", reportPanel);
	}

}
