package no.mesta.mipss.admindashbord.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import no.mesta.mipss.accesscontrol.InnloggedeBrukereVO;
import no.mesta.mipss.admindashbord.AdminChartFactory;
import no.mesta.mipss.admindashbord.DashbordController;
import no.mesta.mipss.admindashbord.DashbordController.Tidsperiode;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.SortOrder;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;

@SuppressWarnings("serial")
public class AllUsersPanel extends JPanel{

	private final DashbordController controller;

	private JLabel antallLabelValue;
	private JButton refreshAntall;
	private JButton visBrukere;
	
	private JTabbedPane brukereTab;
	private JFreeChart dagChart;
	private JFreeChart ukeChart;
	private JFreeChart mndChart;
	
	private JTabbedPane modulTab;
	private JFreeChart dagModulChart;
	private JFreeChart ukeModulChart;
	private JFreeChart mndModulChart;
	
	public AllUsersPanel(DashbordController controller){
		this.controller = controller;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		brukereTab = new JTabbedPane();
		brukereTab.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				switch (brukereTab.getSelectedIndex()){
					case 0: 
						AdminChartFactory.update(dagChart, controller.getDatasetLogin(null, Tidsperiode.DAG));
						break;
					case 1: 
						AdminChartFactory.update(ukeChart, controller.getDatasetLogin(null, Tidsperiode.UKE));
						break;
					case 2: 
						AdminChartFactory.update(mndChart, controller.getDatasetLogin(null, Tidsperiode.MND));
						break;
				}
			}
		});
		modulTab = new JTabbedPane();
		modulTab.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				switch (modulTab.getSelectedIndex()){
					case 0: 
						AdminChartFactory.update(dagModulChart, controller.getDatasetModul(null, Tidsperiode.DAG));
						break;
					case 1: 
						AdminChartFactory.update(ukeModulChart, controller.getDatasetModul(null, Tidsperiode.UKE));
						break;
					case 2: 
						AdminChartFactory.update(mndModulChart, controller.getDatasetModul(null, Tidsperiode.MND));
						break;
				}
			}
		});

		dagChart = AdminChartFactory.createInnloggedeBrukereChart(null, new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("ddMMyy")));
		ukeChart = AdminChartFactory.createInnloggedeBrukereChart(null, new DateTickUnit(DateTickUnitType.DAY, 7, new SimpleDateFormat("ww.yy")));
		mndChart = AdminChartFactory.createInnloggedeBrukereChart(null, new DateTickUnit(DateTickUnitType.MONTH, 1, new SimpleDateFormat("MMM.yy")));
		brukereTab.addTab("Dag", new ChartPanel(dagChart));
		brukereTab.addTab("Uke", new ChartPanel(ukeChart));
		brukereTab.addTab("Mnd", new ChartPanel(mndChart));
		
		dagModulChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("ddMMyyyy")));
		ukeModulChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.DAY, 7, new SimpleDateFormat("ww.yy")));
		mndModulChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.MONTH, 1, new SimpleDateFormat("MMM.yy")));
		
		modulTab.addTab("Dag", new ChartPanel(dagModulChart));
		modulTab.addTab("Uke", new ChartPanel(ukeModulChart));
		modulTab.addTab("Mnd", new ChartPanel(mndModulChart));
		
		JPanel chartPanel = new JPanel(new BorderLayout());
		chartPanel.setBorder(BorderFactory.createTitledBorder("Innloggede brukere over tid"));
		chartPanel.add(brukereTab);
		
		JPanel mestAktivePanel = new JPanel(new BorderLayout());
		mestAktivePanel.setBorder(BorderFactory.createTitledBorder("Mest aktive"));
		JScrollPane scroll = new JScrollPane(createUserTable());
		mestAktivePanel.add(scroll);
		
		
		JPanel modulPanel = new JPanel(new BorderLayout());
		modulPanel.setBorder(BorderFactory.createTitledBorder("Moduler"));
		modulPanel.add(modulTab);
		
		JLabel antallLabel = new JLabel("Innloggede brukere:");
		refreshAntall = new JButton(getRefreshAction());
		refreshAntall.setMargin(new Insets(0,0,0,0));
		
		visBrukere = new JButton(getVisBrukereAction());
		visBrukere.setMargin(new Insets(0,0,0,0));
		visBrukere.setToolTipText("Viser innloggede brukere i en liste");
		
		antallLabelValue = new JLabel(String.valueOf(controller.getInnloggedeBrukere().size()));
		
		
		Font f = new Font("Arial", Font.BOLD, 12);
		antallLabel.setFont(f);
		antallLabelValue.setFont(f);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(antallLabel);
		buttonPanel.add(antallLabelValue);
		buttonPanel.add(refreshAntall);
		buttonPanel.add(visBrukere);
		
		add(buttonPanel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0,0));
		
		add(chartPanel, new GridBagConstraints(0,1, 2,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0,0));
		
		add(mestAktivePanel, new GridBagConstraints(0,2, 1,1, 0.1,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0,0));
		add(modulPanel, new GridBagConstraints(1,2, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0,0));
		
	}
	
	private JXTable createUserTable(){
		JXTable table = new JXTable();
		final Map<String, Integer> a = controller.getMestAktiveBrukere();
		
		Object[] keys = new Object[a.size()];
		Object[] values = new Object[a.size()];
		Object[] plass = new Object[a.size()];
		
		List<String> ks = new ArrayList<String>(a.keySet());
		Collections.sort(ks, new Comparator<String>(){
			@Override
			public int compare(String o1, String o2) {
				return a.get(o2).compareTo(a.get(o1));
			}
		});
		int c =0;
		for (String s:ks){
			plass[c]=c+1;
			keys[c] = s;
			values[c] = a.get(s);
			c++;
		}
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Plass", plass);
		model.addColumn("Sign", keys);
		model.addColumn("Score", values);
		table.setModel(model);
		table.setPreferredScrollableViewportSize(new Dimension(0,0));
		table.setAutoCreateColumnsFromModel(true);
		table.setSortOrder(0, SortOrder.ASCENDING);
		return table;
	}
	
	private AbstractAction getRefreshAction(){
		return new AbstractAction(null, IconResources.REFRESH_VIEW_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {				
				String v = String.valueOf(controller.getInnloggedeBrukere().size());
				antallLabelValue.setText(v);
			}
		};
	}
	private AbstractAction getVisBrukereAction(){
		return new AbstractAction(null, IconResources.HISTORY_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				Window owner = SwingUtilities.windowForComponent(AllUsersPanel.this);
				JDialog d = new JDialog(owner);
				d.setTitle("Innloggede brukere i dag");
				
				List<InnloggedeBrukereVO> innloggedeBrukere = controller.getInnloggedeBrukere();
				Object[] sign= new Object[innloggedeBrukere.size()];
				Object[] sisteInnlogg = new Object[innloggedeBrukere.size()];
				Object[] sisteLogg = new Object[innloggedeBrukere.size()];
				int c=0;
				SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
				for (InnloggedeBrukereVO v:innloggedeBrukere){
					sign[c]=v.getSignatur();
					sisteInnlogg[c]=f.format(v.getDato());
					sisteLogg[c]=f.format(v.getSisteLoggTid());
					c++;
				}
				
				JXTable table = new JXTable();
				DefaultTableModel model = new DefaultTableModel();
				model.addColumn("Sign", sign);
				model.addColumn("Siste innlogging", sisteInnlogg);
				model.addColumn("Siste loggtidspunkt", sisteLogg);
				
				table.setModel(model);
				table.setPreferredScrollableViewportSize(new Dimension(0,0));
				table.setAutoCreateColumnsFromModel(true);
				
				table.getColumn(0).setPreferredWidth(20);
				d.add(new JScrollPane(table));
				d.setSize(new Dimension(300,200));
				d.setLocationRelativeTo(owner);
				d.setVisible(true);
			}
		};
	}
}
