package no.mesta.mipss.admindashbord.swing;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.admindashbord.AdminChartFactory;
import no.mesta.mipss.admindashbord.DashbordController;
import no.mesta.mipss.admindashbord.DashbordController.Tidsperiode;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;

@SuppressWarnings("serial")
public class OneUserPanel extends JPanel{

	private final DashbordController controller;
	private JComboBox brukerCombo;
	
	private JTabbedPane brukereTab;
	private JFreeChart dagChart;
	private JFreeChart ukeChart;
	private JFreeChart mndChart;
	
	private JTabbedPane modulTab;
	private JFreeChart dagModulChart;
	private JFreeChart ukeModulChart;
	private JFreeChart mndModulChart;
	
	public OneUserPanel(DashbordController controller){
		this.controller = controller;
		initGui();
		
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		List<String> brukereList = controller.getBrukereILogg();
		brukereList.add(0, "<Ikke valgt>");
		brukerCombo = new JComboBox(brukereList.toArray());
		brukerCombo.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				updateBrukere();
				updateModuler();
			}
			
		});
		brukereTab = new JTabbedPane();
		brukereTab.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				updateBrukere();
			}
		});
		modulTab = new JTabbedPane();
		modulTab.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				updateModuler();
			}
		});
		dagChart = AdminChartFactory.createInnloggedeBrukereChart(null, new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("dd.MM.yyyy")));
		ukeChart = AdminChartFactory.createInnloggedeBrukereChart(null, new DateTickUnit(DateTickUnitType.DAY, 7, new SimpleDateFormat("ww")));
		mndChart = AdminChartFactory.createInnloggedeBrukereChart(null, new DateTickUnit(DateTickUnitType.MONTH, 1, new SimpleDateFormat("MMM.yyyy")));
		
		dagModulChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("dd.MM.yyyy")));
		ukeModulChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.DAY, 7, new SimpleDateFormat("ww")));
		mndModulChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.MONTH, 1, new SimpleDateFormat("MMM.yyyy")));
		
		brukereTab.addTab("Dag", new ChartPanel(dagChart));
		brukereTab.addTab("Uke", new ChartPanel(ukeChart));
		brukereTab.addTab("Mnd", new ChartPanel(mndChart));
		
		modulTab.addTab("Dag", new ChartPanel(dagModulChart));
		modulTab.addTab("Uke", new ChartPanel(ukeModulChart));
		modulTab.addTab("Mnd", new ChartPanel(mndModulChart));
		
		JPanel chartPanel = new JPanel(new BorderLayout());
		chartPanel.setBorder(BorderFactory.createTitledBorder("Antall innlogginger"));
		chartPanel.add(brukereTab);
		
		JPanel modulPanel = new JPanel(new BorderLayout());
		modulPanel.setBorder(BorderFactory.createTitledBorder("Moduler"));
		modulPanel.add(modulTab);
		
		JPanel brukerPanel = new JPanel();
		brukerPanel.add(new JLabel("Velg bruker:"));
		brukerPanel.add(brukerCombo);
		
		add(brukerPanel, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0,0));
		add(chartPanel, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0,0));
		add(modulPanel, new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0,0));

	}
	private void updateBrukere(){
		if (brukerCombo.getSelectedIndex()!=0){
			String bruker = brukerCombo.getSelectedItem().toString();
			switch (brukereTab.getSelectedIndex()){
				case 0: 
					AdminChartFactory.update(dagChart, controller.getDatasetLogin(bruker, Tidsperiode.DAG));
					dagChart.setTitle(bruker);
					break;
				case 1: 
					AdminChartFactory.update(ukeChart, controller.getDatasetLogin(bruker, Tidsperiode.UKE));
					ukeChart.setTitle(bruker);
					break;
				case 2: 
					AdminChartFactory.update(mndChart, controller.getDatasetLogin(bruker, Tidsperiode.MND));
					mndChart.setTitle(bruker);
					break;
			}
		}
	}
	private void updateModuler(){
		if (brukerCombo.getSelectedIndex()!=0){
			switch (modulTab.getSelectedIndex()){
				case 0: 
					AdminChartFactory.update(dagModulChart, controller.getDatasetModul(brukerCombo.getSelectedItem().toString(), Tidsperiode.DAG));
					break;
				case 1: 
					AdminChartFactory.update(ukeModulChart, controller.getDatasetModul(brukerCombo.getSelectedItem().toString(), Tidsperiode.UKE));
					break;
				case 2: 
					AdminChartFactory.update(mndModulChart, controller.getDatasetModul(brukerCombo.getSelectedItem().toString(), Tidsperiode.MND));
					break;
			}
		}
	}
}
