package no.mesta.mipss.admindashbord;

import javax.swing.JComponent;

import no.mesta.mipss.accesscontrol.BrukerLoggService;
import no.mesta.mipss.admindashbord.swing.DashbordPanel;
import no.mesta.mipss.plugin.MipssPlugin;

@SuppressWarnings("serial")
public class AdmindashbordModule extends MipssPlugin{
	
	private DashbordPanel panel;
	private BrukerLoggService logg;
	private DashbordController controller;
	@Override
	public JComponent getModuleGUI() {
		return panel;
	}

	@Override
	protected void initPluginGUI() {
		controller = new DashbordController();
		panel = new DashbordPanel(controller);
	}

	@Override
	public void shutdown() {
		
	}

}
