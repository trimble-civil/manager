package no.mesta.mipss.admindashbord.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.SimpleDateFormat;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.admindashbord.AdminChartFactory;
import no.mesta.mipss.admindashbord.DashbordController;
import no.mesta.mipss.admindashbord.DashbordController.Tidsperiode;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;

@SuppressWarnings("serial")
public class ReportPanel extends JPanel{

	private JTabbedPane reportTab;
	private JFreeChart dagReportChart;
	private JFreeChart ukeReportChart;
	private JFreeChart mndReportChart;
	
	private final DashbordController controller;
	public ReportPanel(DashbordController controller) {
		this.controller = controller;
		initGui();
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		reportTab = new JTabbedPane();
		reportTab.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				switch (reportTab.getSelectedIndex()){
					case 0: 
						AdminChartFactory.update(dagReportChart, controller.getDatasetRapport(Tidsperiode.DAG));
						break;
					case 1: 
						AdminChartFactory.update(ukeReportChart, controller.getDatasetRapport(Tidsperiode.UKE));
						break;
					case 2: 
						AdminChartFactory.update(mndReportChart, controller.getDatasetRapport(Tidsperiode.MND));
						break;
				}
			}
		});
		dagReportChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("ddMMyyyy")));
		ukeReportChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.DAY, 7, new SimpleDateFormat("ww.yy")));
		mndReportChart = AdminChartFactory.createModulerStartetChart(null, new DateTickUnit(DateTickUnitType.MONTH, 1, new SimpleDateFormat("MMM.yy")));
		
		reportTab.addTab("Dag", new ChartPanel(dagReportChart));
		reportTab.addTab("Uke", new ChartPanel(ukeReportChart));
		reportTab.addTab("Mnd", new ChartPanel(mndReportChart));
		
		add(reportTab, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0,0));


	}


}
