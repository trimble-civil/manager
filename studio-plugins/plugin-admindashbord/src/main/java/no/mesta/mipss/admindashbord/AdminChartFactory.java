package no.mesta.mipss.admindashbord;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTick;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.ClusteredXYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.StandardGradientPaintTransformer;
import org.jfree.ui.TextAnchor;

public class AdminChartFactory {

	public static JFreeChart createInnloggedeBrukereChart(IntervalXYDataset dataset, DateTickUnit dateTickUnit){
		
		final JFreeChart chart = ChartFactory.createXYBarChart(
	            null,      // chart title
	            null,                      // x axis label
	            true,
	            null,                      // y axis label
	            dataset,                  // data
	            PlotOrientation.VERTICAL,
	            true,                     // include legend
	            true,                     // tooltips
	            false                     // urls
	        );
      XYPlot plot = chart.getXYPlot();
      ClusteredXYBarRenderer renderer = new ClusteredXYBarRenderer(0.4, true);
      renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER));
      renderer.setShadowVisible(true);
      renderer.setSeriesPaint(0, new Color(0,0,255));
      renderer.setSeriesPaint(1, new Color(0, 255, 0));
      renderer.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.HORIZONTAL));
      plot.setRenderer(renderer);
//      DateAxis axis = (DateAxis) plot.getDomainAxis();
//      axis.setTickUnit(dateTickUnit);
//      axis.setLabelAngle(Math.toRadians(45));
      
      final DateAxis axis = new DateAxis() {
          @SuppressWarnings("unchecked")
          @Override
          protected List refreshTicksHorizontal(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
             List ticks = super.refreshTicksHorizontal(g2, dataArea, edge);
             List ret = new ArrayList();
             for (Tick tick : (List<Tick>)ticks) {
                if (tick instanceof DateTick) {
                   DateTick dateTick = (DateTick)tick;
                   ret.add(new DateTick(dateTick.getDate(), dateTick.getText(), dateTick.getTextAnchor(), dateTick.getRotationAnchor(), Math.PI / -4.0));
                } else {
                   ret.add(tick);
                }
             }
             return ret;
          }
          protected double findMaximumTickLabelHeight(java.util.List ticks, java.awt.Graphics2D g2, java.awt.geom.Rectangle2D drawArea, boolean vertical) {                  
             return super.findMaximumTickLabelHeight(ticks, g2, drawArea, vertical) * Math.sin(Math.PI / 4.0);
          }                                                     
       };
       axis.setVerticalTickLabels(true);       
       axis.setTickUnit(dateTickUnit, true, true);
       
       plot.setDomainAxis(axis);
       
      return chart;
	}
	
	public static JFreeChart createModulerStartetChart(XYDataset dataset, DateTickUnit dateTickUnit){
      final JFreeChart chart = ChartFactory.createTimeSeriesChart(
          null,      // chart title
          null,                      // x axis label
          null,                      // y axis label
          dataset,                  // data
          true,                     // include legend
          true,                     // tooltips
          false                     // urls
      );
		
		
      XYPlot plot = chart.getXYPlot();
      XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
      renderer.setBaseShapesVisible(true);
      renderer.setBaseStroke(new BasicStroke(2));
      renderer.setAutoPopulateSeriesStroke(false);
      DateAxis axis = (DateAxis) plot.getDomainAxis();
      axis.setTickUnit(dateTickUnit);
      return chart;
	}
	
	public static void update(JFreeChart chart, XYDataset dataset){
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDataset(dataset);
	}
}
