package no.mesta.mipss.admindashbord;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.mesta.mipss.accesscontrol.BrukerLoggService;
import no.mesta.mipss.accesscontrol.InnloggedeBrukereVO;
import no.mesta.mipss.accesscontrol.LoginStat;
import no.mesta.mipss.accesscontrol.ModulStat;
import no.mesta.mipss.common.BeanUtil;

import org.jfree.data.time.Day;
import org.jfree.data.time.Month;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.Week;
import org.jfree.data.xy.XYDataset;

public class DashbordController {
	public enum Tidsperiode{
		DAG, 
		UKE, 
		MND
	}
	private BrukerLoggService loggBean;
	
	public DashbordController(){
		loggBean = BeanUtil.lookup(BrukerLoggService.BEAN_NAME, BrukerLoggService.class);
	}
	
	public XYDataset getDatasetLogin(String bruker, Tidsperiode p){
		switch (p){
			case DAG: 
				List<LoginStat> loginPrDag = getLoginPrDag(bruker);
				return getDatasetLogin(bruker, "Innlogginger pr dag", "Unike pr dag", loginPrDag, Day.class);
			case UKE: 
				List<LoginStat> loginPrUke = getLoginPrUke(bruker);
				return getDatasetLogin(bruker, "Innlogginger pr uke", "Unike pr uke", loginPrUke, Week.class);
			case MND: 
				List<LoginStat> loginPrMnd = getLoginPrMnd(bruker);
				return getDatasetLogin(bruker, "Innlogginger pr mnd", "Unike pr mnd", loginPrMnd, Month.class);
			default :
				throw new IllegalArgumentException("Tidsperiode kan ikke være null og må være enten DAG, UKE eller MND");
		}
	}

	private XYDataset getDatasetLogin(String bruker, String titleDs1, String titleDs2, List<LoginStat> dataset, Class<? extends RegularTimePeriod> timePeriodClass){
		TimeSeries s1 = new TimeSeries(titleDs1);
		TimeSeries s2 = null;
		if (bruker!=null){
			s2 = new TimeSeries(titleDs2);
		}
		for (LoginStat s: dataset){
			RegularTimePeriod timePeriod = getTimePeriod(s.getDato(), timePeriodClass);
			s1.add(timePeriod, s.getAntall());
			if (s2!=null)
				s2.add(timePeriod, s.getUnike());
		}
		TimeSeriesCollection series = new TimeSeriesCollection();
		series.addSeries(s1);
		if (s2!=null)
			series.addSeries(s2);
		return series;
	}
	
	public XYDataset getDatasetModul(String bruker, Tidsperiode p){
		switch (p){
			case DAG: 
				Map<Date, List<ModulStat>> modulPrDag = getModulPrDag(bruker);
				return getDatasetModulPrTimePeriod(modulPrDag, Day.class);
			case UKE: 
				Map<Date, List<ModulStat>> modulPrUke = getModulPrUke(bruker);
				return getDatasetModulPrTimePeriod(modulPrUke, Week.class);
			case MND: 
				Map<Date, List<ModulStat>> modulPrMnd = getModulPrMnd(bruker);
				return getDatasetModulPrTimePeriod(modulPrMnd, Month.class);
			default :
				throw new IllegalArgumentException("Tidsperiode kan ikke være null og må være enten DAG, UKE eller MND");
		}
	}
	
	public XYDataset getDatasetRapport(Tidsperiode p){
		switch (p){
			case DAG: 
				Map<Date, List<ModulStat>> modulPrDag = getRapportPrDag();
				return getDatasetModulPrTimePeriod(modulPrDag, Day.class);
			case UKE: 
				Map<Date, List<ModulStat>> modulPrUke = getRapportPrUke();
				return getDatasetModulPrTimePeriod(modulPrUke, Week.class);
			case MND: 
				Map<Date, List<ModulStat>> modulPrMnd = getRapportPrMnd();
				return getDatasetModulPrTimePeriod(modulPrMnd, Month.class);
			default :
				throw new IllegalArgumentException("Tidsperiode kan ikke være null og må være enten DAG, UKE eller MND");
		}
	}
	

	private XYDataset getDatasetModulPrTimePeriod(Map<Date, List<ModulStat>> dataset, Class<? extends RegularTimePeriod> timePeriodClass){
		Map<String, TimeSeries> datasets = new HashMap<String, TimeSeries>();
		TimeSeriesCollection series = new TimeSeriesCollection();
		for (Date d:dataset.keySet()){
			for (ModulStat ms:dataset.get(d)){
				TimeSeries ts = datasets.get(ms.getModul());
				if (ts==null){
					ts = new TimeSeries(ms.getModul());
					series.addSeries(ts);
					datasets.put(ms.getModul(), ts);
				}
				ts.add(getTimePeriod(ms.getDato(), timePeriodClass), ms.getAntall());
			}
		}		
		return series;
	}

	private RegularTimePeriod getTimePeriod(Date date, Class<? extends RegularTimePeriod> timePeriodClass){
		if (timePeriodClass.equals(Day.class)){
			return new Day(date);
		}
		if (timePeriodClass.equals(Week.class)){
			return new Week(date);
		}
		if (timePeriodClass.equals(Month.class)){
			return new Month(date);
		}
		throw new RuntimeException("RegularTimePeriod specified is not supported");
	}
	

	public List<LoginStat> getLoginPrDag(String sign){
		return sign==null?loggBean.getLoginPrDag():loggBean.getLoginPrDag(sign);
	}
	public List<LoginStat> getLoginPrUke(String sign){
		return sign==null?loggBean.getLoginPrUke():loggBean.getLoginPrUke(sign);
	}
	public List<LoginStat> getLoginPrMnd(String sign){
		return sign==null?loggBean.getLoginPrMnd():loggBean.getLoginPrMnd(sign);
	}
	
	public List<InnloggedeBrukereVO> getInnloggedeBrukere(){
		return loggBean.getInnloggedeBrukere();
	}
	
	public Map<Date, List<ModulStat>> getModulPrDag(String sign){
		return sign==null?loggBean.getModulPrDag():loggBean.getModulPrDag(sign);
	}
	public Map<Date, List<ModulStat>> getModulPrUke(String sign){
		return sign==null?loggBean.getModulPrUke():loggBean.getModulPrUke(sign);	
	}
	public Map<Date, List<ModulStat>> getModulPrMnd(String sign){
		return sign==null?loggBean.getModulPrMnd():loggBean.getModulPrMnd(sign);
	}
	
	public Map<Date, List<ModulStat>> getRapportPrDag(){
		return loggBean.getRapportPrDag();
	}
	public Map<Date, List<ModulStat>> getRapportPrUke(){
		return loggBean.getRapportPrUke();	
	}
	public Map<Date, List<ModulStat>> getRapportPrMnd(){
		return loggBean.getRapportPrMnd();
	}
	
	public Map<String, Integer> getMestAktiveBrukere(){
		return loggBean.getMestAktiveBrukere();
	}
	
	public List<String> getBrukereILogg(){
		return loggBean.getBrukereILogg();
	}
}
