package no.mesta.mipss.ordre;

import java.awt.Dimension;

import javax.swing.JComponent;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.ordre.swing.MainPluginPanel;
import no.mesta.mipss.plugin.MipssPlugin;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class OrdreModule extends MipssPlugin {
	private BindingGroup bindingGroup;
	private OrdreController ordreController;
	private MainPluginPanel mainPluginPanel;
	private boolean eksternLeverandor;
	@Override
	public JComponent getModuleGUI() {
		return mainPluginPanel;
	}

	@Override
	protected void initPluginGUI() {
		eksternLeverandor = (this instanceof OrdreModuleEksternLeverandor);
		MaintenanceContract maintenanceContract = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		bindingGroup = new BindingGroup();
		ordreController = new OrdreController(getLoader(), bindingGroup, maintenanceContract, getLoader().getApplicationFrame(), getLoader().getLoggedOnUserSign(), eksternLeverandor);
		mainPluginPanel = new MainPluginPanel(ordreController);
		bindingGroup.bind();
		mainPluginPanel.setPreferredSize(new Dimension(500,400));
	}
		
	@Override
	public void shutdown() {
		bindingGroup.unbind();
	}
}
