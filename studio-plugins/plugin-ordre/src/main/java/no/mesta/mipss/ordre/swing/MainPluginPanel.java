package no.mesta.mipss.ordre.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import no.mesta.mipss.ordre.OrdreController;

public class MainPluginPanel extends JPanel {
	private OrdreController ordreController;
	private OrdreListPanel ordreListPanel;
	private JPanel pnlMain = new JPanel();
	
	public MainPluginPanel(OrdreController ordreController) {
		this.ordreController = ordreController;
		
		ordreListPanel = new OrdreListPanel(ordreController);
		
		initGui();
	}
	
	private void initGui() {
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(ordreListPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));		
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
