package no.mesta.mipss.ordre;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.bestilling.BestillingSkjema;
import no.mesta.mipss.bestilling.BestillingSkjemaType;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ExcelHelper;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.kjoretoy.plugin.MainInstrumentationPanel;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.messages.AapneKjoretoyMessage;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestillingmaster;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestvarelinjer;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.DfulevBestilling;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreInfoV;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreUtstyr;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre_Status;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.reports.BeanDataSource;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.util.KjoretoyUtils;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class OrdreController implements PropertyChangeSource {
	private static PropertyResourceBundleUtil guiProps;
	private PropertyChangeSupport props;
	
	private MaintenanceContract maintenanceContract;
	
	private JMipssBeanTable<KjoretoyordreInfoV> tblResultat; //Pga designfeil i JXTable trenger controlleren desverre å vite om denne....
	private MipssBeanTableModel<KjoretoyordreInfoV> sokResutlatTableModel;
	
	private BindingGroup bindingGroup;
	private JFrame parentFrame;
	private String loggedOnUserSignature;
	
	private Boolean ordreSearchBusy = Boolean.FALSE;
	private String resultCountString;
	private IMipssStudioLoader loader;
	private final List<Kjoretoyordrestatus> kjoretoyordrestatusList;
	private final boolean eksternLeverandor;
	
	public OrdreController(IMipssStudioLoader loader, BindingGroup bindingGroup, MaintenanceContract maintenanceContract, JFrame parentFrame, String loggedOnUserSignature, boolean eksternLeverandor) {
		this.loader = loader;
		this.bindingGroup = bindingGroup;
		this.maintenanceContract = maintenanceContract;
		this.parentFrame = parentFrame;
		this.loggedOnUserSignature = loggedOnUserSignature;
		this.eksternLeverandor = eksternLeverandor;
		
		props = new PropertyChangeSupport(this);
		
		sokResutlatTableModel = new MipssBeanTableModel<KjoretoyordreInfoV>(KjoretoyordreInfoV.class, "id", "typeNavn", "prosjektNr", "signatur", "statusNavn", "statusOpprettetDato", "eksterntNavn", "regnr", "maskinnummer", "serienummer", "typeOgModell", "navn");
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(KjoretoyordreInfoV.class, sokResutlatTableModel.getColumns());
		tblResultat = new JMipssBeanTable<KjoretoyordreInfoV>(sokResutlatTableModel, colModel);
		tblResultat.setDoWidthHacks(false);
		tblResultat.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblResultat.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() >= 2) {
					visSelectedOrdre();
				}
			}
		});
		
		kjoretoyordrestatusList = maintenanceContract.getKjoretoyordrestatusList();
	}
	public boolean isEksternLeverandor(){
		return eksternLeverandor;
	}
	public Boolean getOrdreSearchBusy() {
		return ordreSearchBusy;
	}

	public void setOrdreSearchBusy(Boolean ordreSearchBusy) {
		Boolean old = this.ordreSearchBusy;
		this.ordreSearchBusy = ordreSearchBusy;
		props.firePropertyChange("ordreSearchBusy", old, ordreSearchBusy);
	}

	public String getResultCountString() {
		return resultCountString;
	}
	
	public void setResultCountString(String resultCountString) {
		String old = this.resultCountString;
		this.resultCountString = resultCountString;
		props.firePropertyChange("resultCountString", old, resultCountString);
	}

	public JMipssBeanTable<KjoretoyordreInfoV> getResultTable() {
		return tblResultat;
	}
	
    public static PropertyResourceBundleUtil getGuiProps() {
        if(guiProps == null) {
            guiProps = PropertyResourceBundleUtil.getPropertyBundle("ordreText");
        }    
        return guiProps;
    }
	
    @SuppressWarnings("unchecked")
	private void doOrdreSok(final String sokArg, final List<String> statuser) {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
		    	setOrdreSearchBusy(Boolean.TRUE);
		    	setResultCountString("");
		    	
				String arg = sokArg != null && sokArg.length() > 0 ? sokArg : null;
				
				List<KjoretoyordreInfoV> list = KjoretoyUtils.getKjoretoyBean().searchKjoretoyordre(arg, statuser);
				tblResultat.clearSelection();
				sokResutlatTableModel.setEntities(list);
				
				setResultCountString(list != null ? list.size() + " treff" : null);
				
				setOrdreSearchBusy(Boolean.FALSE);
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				} 
    		}
    	}.start();
    }
    
    /**
     * 
     */
	private void visSelectedOrdre() {
    	int selectedRow = tblResultat.getSelectedRow();
		if(selectedRow >= 0 && selectedRow < tblResultat.getRowCount()) {
			KjoretoyordreInfoV kjoretoyordreInfoV = sokResutlatTableModel.get(tblResultat.convertRowIndexToModel(selectedRow));
			if(kjoretoyordreInfoV != null) {
				tblResultat.setCursor(new Cursor(Cursor.WAIT_CURSOR));
				final BestillingSkjema bestillingSkjema = 
					new BestillingSkjema(BestillingSkjemaType.RAPPORTUTTAK, 
							null,
							kjoretoyordreInfoV.getId(),
							loader.getLoggedOnUserSign());
				bestillingSkjema.genererRapport();
				tblResultat.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		}
    }
    
    public void createAndOpenPdf(Kjoretoyordre ordre){
    	BeanDataSource<Kjoretoyordre> ds = new BeanDataSource<Kjoretoyordre>(ordre);
    	JReportBuilder builder = new JReportBuilder(Report.KJORETOYORDRE,
				Report.KJORETOYORDRE.getDefaultParameters(), ds, ReportHelper.EXPORT_PDF,
				parentFrame);
		File report = builder.getGeneratedReportFile();

		if (report != null) {
			DesktopHelper.openFile(report);
		}
    }
    
	public Action getAapneKjoretoyAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.aapneKjoretoy"), IconResources.BIL_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblResultat.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblResultat.getRowCount()) {
					KjoretoyordreInfoV kjoretoyordreInfoV = sokResutlatTableModel.get(tblResultat.convertRowIndexToModel(selectedRow));
					if(kjoretoyordreInfoV != null) {
						MipssPlugin plugin = loader.getPlugin(MainInstrumentationPanel.class);
						AapneKjoretoyMessage m = new AapneKjoretoyMessage(kjoretoyordreInfoV.getKjoretoyId());
						m.setTargetPlugin(plugin);
						loader.sendMessage(m);
					}
				}
			}
    	};
    }
    
    public Action getTilExcelAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.excel"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				File excelFile = tblResultat.exportToExcel(JMipssBeanTable.EXPORT_TYPE.ALL_ROWS);
				DesktopHelper.openFile(excelFile);
			}
    	};
    }
    
    public Action getSokAction(final JFormattedTextField txtSok) {
    	return new AbstractAction(getGuiProps().getGuiString("button.sok"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				doOrdreSok(txtSok.getText(), null);
			}
    	};
    }
    
    public Action getTilPDFAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.pdf"), IconResources.ADOBE_16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblResultat.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblResultat.getRowCount()) {
					KjoretoyordreInfoV kjoretoyordreInfoV = sokResutlatTableModel.get(tblResultat.convertRowIndexToModel(selectedRow));
					if(kjoretoyordreInfoV != null) {
						Kjoretoyordre fraDb = KjoretoyUtils.getKjoretoyBean().getKjoretoyordreForAdmin(kjoretoyordreInfoV.getId());
						createAndOpenPdf(fraDb);
					}
				}
			}
    	};
    }
    
	public Action getBehandleBestillingAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.behandleBestilling"), IconResources.EDIT_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblResultat.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblResultat.getRowCount()) {
					KjoretoyordreInfoV kjoretoyordreInfoV = sokResutlatTableModel.get(tblResultat.convertRowIndexToModel(selectedRow));
					if(kjoretoyordreInfoV != null) {
						KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
						String ordrestatusOverlevertKoordNavn = params.hentEnForApp("studio.mipss.montering", "ordrestatusOverlevertKoord").getVerdi();
						String ordrestatusUnderArbeidKoord = params.hentEnForApp("studio.mipss.montering", "ordrestatusUnderArbeidKoord").getVerdi();
						String ordrestatusFullfortKoord = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortKoord").getVerdi();
						
						MipssKjoretoy kjoretoyBean = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
						Kjoretoyordre ko = kjoretoyBean.getKjoretoyordreForAdmin(kjoretoyordreInfoV.getId());
						Kjoretoyordrestatus sisteKjoretoyordrestatus = ko.getSisteKjoretoyordrestatus();
						if(sisteKjoretoyordrestatus.getIdent().equals(ordrestatusOverlevertKoordNavn)||
								sisteKjoretoyordrestatus.getIdent().equals(ordrestatusUnderArbeidKoord)||
								sisteKjoretoyordrestatus.getIdent().equals(ordrestatusFullfortKoord)) {
							final BestillingSkjema bestillingSkjema = 
								new BestillingSkjema(BestillingSkjemaType.ADMIN_BEHANDLING, 
										null,
										kjoretoyordreInfoV.getId(),
										loader.getLoggedOnUserSign());
							bestillingSkjema.showWizard();
							reloadKjoretoyordreInfoV(kjoretoyordreInfoV);
						} else {
							JOptionPane.showMessageDialog(loader.getApplicationFrame(), 
									getGuiProps().getGuiString("message.kanIkkeBehandle"));
						}
					}
				}
			}
    	};
	}
    
    public Action getVisBestillingsskjemaAction() {
    	return new AbstractAction(getGuiProps().getGuiString("button.visBestillingsskjema"), IconResources.ADOBE_16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				visSelectedOrdre();
			}
    	};
    }
	
    public Action getSokOrdreTilKoordAction(final JFormattedTextField txtSok) {
    	return new AbstractAction(getGuiProps().getGuiString("button.sokOrdreTilKoord"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
				String ordrestatuserTilKoord = params.hentEnForApp("studio.mipss.ordre", "ordrestatuserTilKoord").getVerdi();
				doOrdreSok(txtSok.getText(), strenglisteFraKommaseparertStreng(ordrestatuserTilKoord));
			}
    	};
    }
    
    public Action getSokOrdreTilEksternLevAction(final JFormattedTextField txtSok) {
    	return new AbstractAction(getGuiProps().getGuiString("button.sokOrdreTilEksternLeverandor"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
				String ordrestatuserTilKoord = params.hentEnForApp("studio.mipss.ordre", "ordrestatuserTilEksternLev").getVerdi();
				doOrdreSok(txtSok.getText(), strenglisteFraKommaseparertStreng(ordrestatuserTilKoord));
			}
    	};
    }
    
    public void setStatusUnderBehandlingLeverandor(Kjoretoyordre kjoretoyordre){
    	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
		String ordrestatusUnderBehandlingLeverandorIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusUnderArbeidLev").getVerdi();
		String ordrestatusFullfortKoord = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortKoord").getVerdi();
		//har feil statusen for å sette status, returner
		String sisteStatusIdent = kjoretoyordre.getSisteKjoretoyordrestatus().getIdent();
		if (!sisteStatusIdent.equals(ordrestatusFullfortKoord)){
			return;
		}
		
		//finn statusen i listen og legg til
		Kjoretoyordrestatus underBehandlingEkstLev = null;
		for(Kjoretoyordrestatus kos : kjoretoyordrestatusList) {
			if(kos.getIdent().equals(ordrestatusUnderBehandlingLeverandorIdent)) {
				underBehandlingEkstLev = kos;
				break;
			}
		}
		if(underBehandlingEkstLev == null) {
			throw new IllegalStateException("Finner ikke status for under behandling, ekstern leverandør! Kontroller konfigparam!");
		}
		leggTilKjoretoyordreStatus(underBehandlingEkstLev, kjoretoyordre);
		KjoretoyUtils.getKjoretoyBean().oppdaterKjoretoyordre(kjoretoyordre, loggedOnUserSignature);
    	
	}

	private void leggTilKjoretoyordreStatus(Kjoretoyordrestatus kjoretoyordrestatus, Kjoretoyordre kjoretoyordre) {
		Kjoretoyordre_Status kjoretoyordreStatus = new Kjoretoyordre_Status();
		kjoretoyordreStatus.getOwnedMipssEntity().setOpprettetAv(loggedOnUserSignature);
		kjoretoyordreStatus.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		kjoretoyordreStatus.setKjoretoyordre(kjoretoyordre);
		kjoretoyordreStatus.setKjoretoyordrestatus(kjoretoyordrestatus);
		kjoretoyordre.getKjoretoyordre_StatusList().add(kjoretoyordreStatus);
	}
	
	public Action getVareListeRapportAction() {
		return new AbstractAction(getGuiProps().getGuiString("button.vareListeRapport"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblResultat.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblResultat.getRowCount()) {
					KjoretoyordreInfoV kjoretoyordreInfoV = sokResutlatTableModel.get(tblResultat.convertRowIndexToModel(selectedRow));
					if(kjoretoyordreInfoV != null) {
						Kjoretoyordre ko = KjoretoyUtils.getKjoretoyBean().getKjoretoyordreForAdmin(kjoretoyordreInfoV.getId());
						Kjoretoy k = ko.getKjoretoy();
						String leverandorNr = ko.getKjoretoyLeverandorNr();
						String mestaLeverandorNr = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.ordre", "leverandornrMesta").getVerdi();
						boolean isMestaKjoretoy = leverandorNr.toLowerCase().equals(mestaLeverandorNr.toLowerCase());
						
						Long serieportutstyrId = null;
						List<KjoretoyordreUtstyr> koUtstyr = ko.getKjoretoyordreUtstyrList();
						
						for (KjoretoyordreUtstyr uts:koUtstyr){
							KjoretoyUtstyr ku = uts.getKjoretoyUtstyr();
							serieportutstyrId = KjoretoyUtils.getKjoretoyBean().getDatekIdForKjoretoyUtstyr(ku);
							if (serieportutstyrId!=null)
								break;
						}
						String serieportId = serieportutstyrId!=null?serieportutstyrId.toString():"";
						String regnr = k.getRegnr();
						String prosjektnr = ko.getProsjektNr();
						String ansvar = ko.getAnsvar();
						Dfubestillingmaster master = ko.getDfubestillingmasterForBestilling();
						String mottaker = master.getLeveringsNavn();
						String adresse = master.getLeveringsAdresse();
						String postnummer = master.getLeveringsPostnummer().toString();
						String poststed = master.getLeveringsPoststed();
						String merknad = master.getFritekst();
						String ordreId = String.valueOf(ko.getId());
						String bedriftsPakke = master.getBedriftspakkeFlaggBoolean()?"x":"";
						
						ExcelHelper excel = new ExcelHelper();
						WritableWorkbook workbook = excel.createNewWorkbook("ordreExport");
						WritableSheet kjoretoySheet = excel.createNewSheet("kjoretoy", workbook, 0);
						excel.writeHeaders(kjoretoySheet, 0, 0, true, "Regnr", "Mesta-bil", "Prosjektnr", "Ansvar", "Mottaker", "Adresse", "Postnr", "Poststed", "Forsendelsen merkes", "Ordre Id", "Serieportutstyr", "Ekspress");
						int c = 0;
						int r = 1;
						excel.writeData(kjoretoySheet, r, c++, regnr);
						excel.writeData(kjoretoySheet, r, c++, isMestaKjoretoy?"x":"");
						excel.writeData(kjoretoySheet, r, c++, prosjektnr);
						excel.writeData(kjoretoySheet, r, c++, ansvar);
						excel.writeData(kjoretoySheet, r, c++, mottaker);
						excel.writeData(kjoretoySheet, r, c++, adresse);
						excel.writeData(kjoretoySheet, r, c++, postnummer);
						excel.writeData(kjoretoySheet, r, c++, poststed);
						excel.writeData(kjoretoySheet, r, c++, merknad);
						excel.writeData(kjoretoySheet, r, c++, ordreId);
						excel.writeData(kjoretoySheet, r, c++, serieportId);
						excel.writeData(kjoretoySheet, r, c++, bedriftsPakke);
						c=0;
						
						Dfubestillingmaster dfubestillingmaster= ko.getDfubestillingmasterForBestilling();
						if (dfubestillingmaster!=null){
							DfulevBestilling best = dfubestillingmaster.getDfuLDfulevBestillingForBestilling();
							if (best!=null){
								List<Dfubestvarelinjer> list = best.getDfubestvarelinjerList();
								if (list!=null){
									WritableSheet bestSheet = excel.createNewSheet("ordrelinjer", workbook, 1);
									excel.writeHeaders(bestSheet, 0, 0, true, "Varenummer", "Antall");
									for (Dfubestvarelinjer l:list){
										excel.writeData(bestSheet, r, 0, l.getVarenummer());
										excel.writeData(bestSheet, r, 1, l.getAntall());
										r++;
									}
								}
							}
						}
						excel.closeWorkbook(workbook);
						DesktopHelper.openFile(excel.getExcelFile());
						if (isEksternLeverandor())
							setStatusUnderBehandlingLeverandor(ko);
					}
					
				}
			}
    	};
	}
	
	
	
	public Action getSetStatusBehandletEkstLevAction() {
		return new AbstractAction(getGuiProps().getGuiString("button.settStatusBehandletEkstLev"), IconResources.MODULE_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblResultat.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblResultat.getRowCount()) {
					KjoretoyordreInfoV kjoretoyordreInfoV = sokResutlatTableModel.get(tblResultat.convertRowIndexToModel(selectedRow));
					if(kjoretoyordreInfoV != null) {
						if (kjoretoyordreInfoV.getSerienummer()==null||kjoretoyordreInfoV.getSerienummer().equals("")){
							JOptionPane.showMessageDialog(loader.getApplicationFrame(), getGuiProps().getGuiString("message.manglerSerienummer"));
							return;
						}
						
						KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
						String ordrestatusFullfoertKoordDIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortKoord").getVerdi();
						String ordrestatusBehandletEksternLeverandorIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortEkstLev").getVerdi();
						String ordrestatusUnderArbeidKoordIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusUnderArbeidLev").getVerdi();
						
						if(kjoretoyordreInfoV.getStatusIdent().equals(ordrestatusFullfoertKoordDIdent)||
							kjoretoyordreInfoV.getStatusIdent().equals(ordrestatusUnderArbeidKoordIdent)) {
							settOrdreFerdigbehandletHosEksternLeverandor(kjoretoyordreInfoV.getId());
							reloadKjoretoyordreInfoV(kjoretoyordreInfoV);
							JOptionPane.showMessageDialog(loader.getApplicationFrame(), getGuiProps().getGuiString("message.ordreBehandletHosEksternLeverandor", new Object[]{kjoretoyordreInfoV.getId()}));
						} else if(kjoretoyordreInfoV.getStatusIdent().equals(ordrestatusBehandletEksternLeverandorIdent)) {
							JOptionPane.showMessageDialog(loader.getApplicationFrame(), getGuiProps().getGuiString("message.ferdigbehandletHosEksternLev"));
						} else {
							JOptionPane.showMessageDialog(loader.getApplicationFrame(), getGuiProps().getGuiString("message.kanIkkeBehandlesHosEksternLev"));
						}
					}
				}
			}
    	};
	}
	
	private void reloadKjoretoyordreInfoV(final KjoretoyordreInfoV kjoretoyordreInfoV) {
		KjoretoyordreInfoV newKjoretoyordreInfoV = KjoretoyUtils.getKjoretoyBean().getKjoretoyordreInfoV(kjoretoyordreInfoV.getId());
		int i = sokResutlatTableModel.getEntities().indexOf(kjoretoyordreInfoV);
		sokResutlatTableModel.getEntities().set(i, newKjoretoyordreInfoV);
		sokResutlatTableModel.fireTableModelEvent(i, i, TableModelEvent.UPDATE);
	}
    
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	public BindingGroup getBindingGroup() {
		return bindingGroup;
	}

	private List<String> strenglisteFraKommaseparertStreng(String s) {
		StringTokenizer st = new StringTokenizer(s, ",");
		List<String> statuser = new ArrayList<String>();
		while(st.hasMoreTokens()) {
			String token = st.nextToken();
			statuser.add(token);
		}
		return statuser.size() > 0 ? statuser : null;
	}
	
	/**
	 * Ferdigbehandler en ordre. Med lagring. Oppdaterer ikke visningslisten med nytt view
	 * 
	 * @param ordreId
	 */
	private void settOrdreFerdigbehandletHosEksternLeverandor(Long ordreId) {
		KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
		String ordrestatusBehandletEksternLeverandorIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortEkstLev").getVerdi();
		
		Kjoretoyordrestatus statusFerdigEkstLev = null;
		
		for(Kjoretoyordrestatus kos : kjoretoyordrestatusList) {
			if(kos.getIdent().equals(ordrestatusBehandletEksternLeverandorIdent)) {
				statusFerdigEkstLev = kos;
				break;
			}
		}
		if(statusFerdigEkstLev == null) {
			throw new IllegalStateException("Finner ikke status for ekstern leverandør! Kontroller konfigparam!");
		}
		
		Kjoretoyordre kjoretoyordre = KjoretoyUtils.getKjoretoyBean().getKjoretoyordreForAdmin(ordreId);
		leggTilKjoretoyordreStatus(statusFerdigEkstLev, kjoretoyordre);
		
		KjoretoyUtils.getKjoretoyBean().oppdaterKjoretoyordre(kjoretoyordre, loggedOnUserSignature);
	}
}
