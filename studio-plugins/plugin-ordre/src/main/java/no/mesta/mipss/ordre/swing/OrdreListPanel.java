package no.mesta.mipss.ordre.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.ordre.OrdreController;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreInfoV;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;

import org.jdesktop.swingx.JXBusyLabel;

public class OrdreListPanel extends JPanel {
	private OrdreController ordreController;
	
	private JPanel pnlMain = new JPanel();
	private JPanel pnlSok = new JPanel();
	private JFormattedTextField txtSok = new JFormattedTextField();
	private JButton btnSok = new JButton();
	private JButton btnSokOrdreTilKoord = new JButton();
	private JButton btnSokOrdreTilEksternLeverandor = new JButton();
	private JScrollPane scrlPaneOrdre = new JScrollPane();
	private JMipssBeanTable<KjoretoyordreInfoV> tblOrdre;
	private JPanel pnlSidebuttons = new JPanel();
	private JButton btnAapneKjoretoy = new JButton();
	private JPanel pnlButtons = new JPanel();
	private JButton btnTilExcel = new JButton();
	private JButton btnBehandleBestilling = new JButton();
	private JXBusyLabel lblBusy = new JXBusyLabel();
	private JLabel lblResultCount = new JLabel();
    private JButton btnVisBestillingsskjema = new JButton();
    private JButton btnSettStatusBehandletAvEksterLeverandor = new JButton();
    private JButton btnVarelisteRapport = new JButton();

	
	public OrdreListPanel(OrdreController ordreController) {
		this.ordreController = ordreController;
		
		initLabels();
		initTextFields();
		initTables();
		initButtons();
		initGui();
		initBindings();
	}
	
	public void setBusy(Boolean busy) {
		boolean eksternLeverandor = ordreController.isEksternLeverandor();
		if(busy.booleanValue()) {
			lblBusy.setBusy(true);
			scrlPaneOrdre.setViewportView(lblBusy);
			btnTilExcel.setEnabled(false);
			btnAapneKjoretoy.setEnabled(false);
			btnSok.setEnabled(false);
			btnSokOrdreTilKoord.setEnabled(false);
			btnSokOrdreTilEksternLeverandor.setEnabled(false);
			btnBehandleBestilling.setEnabled(false);
			btnVisBestillingsskjema.setEnabled(false);
			btnSettStatusBehandletAvEksterLeverandor.setEnabled(false);
			btnVarelisteRapport.setEnabled(false);
		} else {
			lblBusy.setBusy(false);
			scrlPaneOrdre.setViewportView(tblOrdre);
			btnTilExcel.setEnabled(true);
			btnAapneKjoretoy.setEnabled(!eksternLeverandor);
			btnSok.setEnabled(!eksternLeverandor);
			btnSokOrdreTilKoord.setEnabled(!eksternLeverandor);
			btnSokOrdreTilEksternLeverandor.setEnabled(true);
			btnBehandleBestilling.setEnabled(!eksternLeverandor);
			btnVisBestillingsskjema.setEnabled(true);
			btnSettStatusBehandletAvEksterLeverandor.setEnabled(true);
			btnVarelisteRapport.setEnabled(true);
		}
	}
	
	private void initBindings() {
		ordreController.getBindingGroup().addBinding(BindingHelper.createbinding(ordreController, "ordreSearchBusy", this, "busy", BindingHelper.READ));
		ordreController.getBindingGroup().addBinding(BindingHelper.createbinding(ordreController, "resultCountString", lblResultCount, "text", BindingHelper.READ));
	}
	
	private void initTextFields() {
		txtSok.setPreferredSize(new Dimension(150, 19));
		if(ordreController.isEksternLeverandor()) {
			txtSok.setAction(ordreController.getSokOrdreTilEksternLevAction(txtSok));
		} else {
			txtSok.setAction(ordreController.getSokAction(txtSok));
		}
	}
	
	private void initTables() {
		tblOrdre = ordreController.getResultTable();
	}
	
	private void initButtons() {
		btnAapneKjoretoy.setAction(ordreController.getAapneKjoretoyAction());
		btnTilExcel.setAction(ordreController.getTilExcelAction());
		btnSok.setAction(ordreController.getSokAction(txtSok));
		btnSokOrdreTilKoord.setAction(ordreController.getSokOrdreTilKoordAction(txtSok));
		btnSokOrdreTilEksternLeverandor.setAction(ordreController.getSokOrdreTilEksternLevAction(txtSok));
		btnBehandleBestilling.setAction(ordreController.getBehandleBestillingAction());
		btnVisBestillingsskjema.setAction(ordreController.getVisBestillingsskjemaAction());
		btnSettStatusBehandletAvEksterLeverandor.setAction(ordreController.getSetStatusBehandletEkstLevAction());
		btnVarelisteRapport.setAction(ordreController.getVareListeRapportAction());
	}
	
	private void initLabels() {
		lblBusy.setText(OrdreController.getGuiProps().getGuiString("label.ordreSearchBusy"));
	}
	
	private void initGui() {
		scrlPaneOrdre.setViewportView(tblOrdre);
		
		pnlSok.setLayout(new GridBagLayout());
		pnlSok.add(txtSok, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));		
		pnlSok.add(btnSok, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));		
		pnlSok.add(btnSokOrdreTilKoord, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));		
		pnlSok.add(btnSokOrdreTilEksternLeverandor, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));		
		
		pnlSidebuttons.setLayout(new GridBagLayout());
		pnlSidebuttons.add(btnAapneKjoretoy, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlSidebuttons.add(btnBehandleBestilling, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlSidebuttons.add(btnVisBestillingsskjema, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlSidebuttons.add(btnVarelisteRapport, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlSidebuttons.add(btnSettStatusBehandletAvEksterLeverandor, new GridBagConstraints(0, 4, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(lblResultCount, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		pnlButtons.add(btnTilExcel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));		
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(pnlSok, new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 0), 0, 0));
		pnlMain.add(scrlPaneOrdre, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));		
		pnlMain.add(pnlSidebuttons, new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.VERTICAL, new Insets(0, 5, 0, 0), 0, 0));
		pnlMain.add(pnlButtons, new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(11, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
}
