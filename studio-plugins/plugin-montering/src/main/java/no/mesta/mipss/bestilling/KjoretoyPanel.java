package no.mesta.mipss.bestilling;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;

public class KjoretoyPanel extends JPanel {
	private JPanel pnlKjoretoyId = new JPanel();
	private JPanel pnlTypeOgModell = new JPanel();
	private JPanel pnlUtstyr = new JPanel();
	
	private JRadioButton rbtnMaskinnrEllerRegnr = new JRadioButton();
	private JRadioButton rbtnAnnenId = new JRadioButton();
	private JRadioButton rbtnRegSenere = new JRadioButton();
	
	private JLabel lblMaskinnummer = new JLabel();
	private JLabel lblRegnr = new JLabel();
	private JLabel lblKjoretoyId = new JLabel();
	private JLabel lblType = new JLabel();
	private JLabel lblModell = new JLabel();
	
	private JFormattedTextField txtMaskinnummer = new JFormattedTextField();
	private JFormattedTextField txtRegnr = new JFormattedTextField();
	private JFormattedTextField txtKjoretoyId = new JFormattedTextField();
	
	private JComboBox cbxType = new JComboBox();
	private JComboBox cbxModell = new JComboBox();
	
	private JButton btnLeggTilUtstyr = new JButton();
	private JButton btnSlettUtstyr = new JButton();
	
	private JMipssBeanTable<KjoretoyUtstyr> tblUtstyr;
	private JScrollPane scrlPaneUtstyr = new JScrollPane();
	
	private JCheckBox chkKjoretoyUtenUtstyr = new JCheckBox();
	private JCheckBox chkKjoretoyMaaHaBryterboks = new JCheckBox();
	
	private final BestillingController bestillingController;
	private MipssBeanTableModel<KjoretoyUtstyr> tblModelKjoretoyUtstyr;
	
	public KjoretoyPanel(final BestillingController bestillingController) {
		this.bestillingController = bestillingController;
		initTable();
		initTextFields();
		initButtons();
		initComboBox();
		initCheckBox();
		
		initGui();
		updateTxtVisibility();
	}
	
	private void initTable() {
		tblModelKjoretoyUtstyr = new MipssBeanTableModel<KjoretoyUtstyr>(KjoretoyUtstyr.class, "utstyrgruppe", "utstyrsubgruppe", "utstyrmodell");
		
		BindingHelper.createbinding(bestillingController, "kjoretoyUtstyrList", tblModelKjoretoyUtstyr, "entities", BindingHelper.READ).bind();
		
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(KjoretoyUtstyr.class, tblModelKjoretoyUtstyr.getColumns());
		tblUtstyr = new JMipssBeanTable<KjoretoyUtstyr>(tblModelKjoretoyUtstyr, colModel);
		tblUtstyr.setFillsViewportWidth(true);
		tblUtstyr.setDoWidthHacks(false);
		tblUtstyr.setDefaultRenderer(Utstyrmodell.class, new RenderableMipssEntityTableCellRenderer<Utstyrmodell>());
		tblUtstyr.setDefaultRenderer(Utstyrgruppe.class, new RenderableMipssEntityTableCellRenderer<Utstyrgruppe>());
		tblUtstyr.setDefaultRenderer(Utstyrsubgruppe.class, new RenderableMipssEntityTableCellRenderer<Utstyrsubgruppe>());
	}
	
	private void initTextFields() {
		txtMaskinnummer.setDocument(new DocumentFilter(8));
		txtRegnr.setDocument(new DocumentFilter(7, null, true));
		txtKjoretoyId.setDocument(new DocumentFilter(20, null, true));
		
		Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, bestillingController, BeanProperty.create("kjoretoy.regnrValid"), txtRegnr, BeanProperty.create("text"), null).bind();
		Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, bestillingController, BeanProperty.create("kjoretoy.nummerEllerNavn"), txtKjoretoyId, BeanProperty.create("text")).bind();
		
		Binding binding = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, bestillingController, BeanProperty.create("kjoretoy.maskinnummerValid"), txtMaskinnummer, BeanProperty.create("text"), null);
		binding.setConverter(new LongStringConverter());
		binding.bind();
		
		bestillingController.addPropertyChangeListener("kjoretoyNyopprettet", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				updateTxtVisibility();
			}
		});
	}
	
	private void initButtons() {
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(rbtnMaskinnrEllerRegnr);
		buttonGroup.add(rbtnAnnenId);
		buttonGroup.add(rbtnRegSenere);
		
		
		ActionListener updateTxtVisibility = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				updateTxtVisibility();
			}
		};
		rbtnMaskinnrEllerRegnr.addActionListener(updateTxtVisibility);
		rbtnAnnenId.addActionListener(updateTxtVisibility);
		
		rbtnRegSenere.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				updateTxtVisibility();
				String regnr = "NY_BIL_"+ServerUtils.getInstance().hentSekvens("ny_bil_seq");
				txtKjoretoyId.setText(regnr);
				System.out.println(bestillingController.getKjoretoy().getEksterntNavn());
			}
		});
		
		btnLeggTilUtstyr.setAction(bestillingController.getNyttKjoretoyUtstyrAction(tblModelKjoretoyUtstyr));
		btnSlettUtstyr.setAction(bestillingController.getSlettKjoretoyUtstyrAction(tblUtstyr, tblModelKjoretoyUtstyr));
		
		BindingHelper.createbinding(bestillingController, "${!kjoretoyUtenUtstyr}", btnLeggTilUtstyr, "enabled", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "${!kjoretoyUtenUtstyr}", btnSlettUtstyr, "enabled", BindingHelper.READ).bind();
	}
	
	private void initComboBox() {
		List<Kjoretoytype> kjoretoytypeList = bestillingController.fetchKjoretoytypeList();
		MipssComboBoxModel<Kjoretoytype> cbxKjoretoytype = new MipssComboBoxModel<Kjoretoytype>(kjoretoytypeList, false);
        cbxType.setModel(cbxKjoretoytype);
        cbxType.setRenderer(new MipssListCellRenderer<Kjoretoytype>());
                
		BindingHelper.createbinding(bestillingController, "kjoretoy.kjoretoytype", cbxType, "selectedItem", BindingHelper.READ_WRITE).bind();

		List<Kjoretoymodell> kjoretoymodellList = bestillingController.fetchKjoretoymodellList();
		MipssComboBoxModel<Kjoretoymodell> cbxKjoretoymodell = new MipssComboBoxModel<Kjoretoymodell>(kjoretoymodellList, false);
        cbxModell.setModel(cbxKjoretoymodell);
        cbxModell.setRenderer(new MipssListCellRenderer<Kjoretoymodell>());
                
		BindingHelper.createbinding(bestillingController, "kjoretoy.kjoretoymodell", cbxModell, "selectedItem", BindingHelper.READ_WRITE).bind();
	}
	
	private void initCheckBox() {
		BindingHelper.createbinding(bestillingController, "kjoretoyUtenUtstyr", chkKjoretoyUtenUtstyr, "selected", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "kjoretoyMaaHaBryterboks", chkKjoretoyMaaHaBryterboks, "selected", BindingHelper.READ_WRITE).bind();
		bestillingController.addUtstyrActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chkKjoretoyUtenUtstyr.setEnabled(bestillingController.getKjoretoy().getKjoretoyUtstyrList().isEmpty());
			}
		});
		if (bestillingController.getKjoretoyordre().getUtenUtstyr()!=null){
			if (bestillingController.getKjoretoyordre().getUtenUtstyr()){
				bestillingController.setKjoretoyUtenUtstyr(Boolean.TRUE);
			}else{
				bestillingController.setKjoretoyUtenUtstyr(Boolean.FALSE);
			}
		}else{
			bestillingController.setKjoretoyUtenUtstyr(Boolean.FALSE);
		}
		
		if (bestillingController.getKjoretoyordre().getMaaHaBryterboks()!=null){
			if (bestillingController.getKjoretoyordre().getMaaHaBryterboks()){
				bestillingController.setKjoretoyMaaHaBryterboks(Boolean.TRUE);
			}else{
				bestillingController.setKjoretoyMaaHaBryterboks(Boolean.FALSE);
			}
		}else{
			bestillingController.setKjoretoyMaaHaBryterboks(Boolean.FALSE);
		}
	}
	
	private void updateTxtVisibility() {
		final boolean nyttKjoretoy = bestillingController.getKjoretoyNyopprettet();
		final boolean mnrOrRegnr = rbtnMaskinnrEllerRegnr.isSelected();
		final boolean kId = rbtnAnnenId.isSelected();
		
		txtMaskinnummer.setEditable(mnrOrRegnr && nyttKjoretoy);
		txtMaskinnummer.setFocusable(mnrOrRegnr && nyttKjoretoy);
		
		txtRegnr.setEditable(mnrOrRegnr && nyttKjoretoy);
		txtRegnr.setFocusable(mnrOrRegnr && nyttKjoretoy);
		
		txtKjoretoyId.setEditable(kId && nyttKjoretoy);
		txtKjoretoyId.setFocusable(kId && nyttKjoretoy);
		
	}
	
	private void initGui() {
		scrlPaneUtstyr.setViewportView(tblUtstyr);
		
		rbtnMaskinnrEllerRegnr.setText(BestillingController.getResourceBundle().getString("label.maskinnummerEllerRegnr"));
		rbtnAnnenId.setText(BestillingController.getResourceBundle().getString("label.annenId"));
		rbtnAnnenId.setToolTipText(BestillingController.getResourceBundle().getString("tooltip.annenId"));
		rbtnRegSenere.setToolTipText(BestillingController.getResourceBundle().getString("tooltip.regsenere"));
		rbtnRegSenere.setText(BestillingController.getResourceBundle().getString("label.regsenere"));

		JLabel lblRegsenereInfo = new JLabel(IconResources.INFO_16);
		lblRegsenereInfo.setToolTipText(BestillingController.getResourceBundle().getString("tooltip.regsenere"));
		JLabel lblAnnenIdInfo = new JLabel(IconResources.INFO_16);
		lblAnnenIdInfo.setToolTipText(BestillingController.getResourceBundle().getString("tooltip.annenId"));
		
		lblMaskinnummer.setText(BestillingController.getResourceBundle().getString("label.maskinnummer"));
		lblRegnr.setText(BestillingController.getResourceBundle().getString("label.regnr"));
		lblKjoretoyId.setText(BestillingController.getResourceBundle().getString("label.kjoretoyId"));
		
		lblType.setText(BestillingController.getResourceBundle().getString("label.type"));
		lblModell.setText(BestillingController.getResourceBundle().getString("label.modell"));
		chkKjoretoyUtenUtstyr.setText(BestillingController.getResourceBundle().getString("checkbox.kjoretoyUtenUtstyr"));
		chkKjoretoyMaaHaBryterboks.setText(BestillingController.getResourceBundle().getString("checkbox.kjoretoyMaaHaBryterboks"));
		
		txtMaskinnummer.setPreferredSize(new Dimension(100, 19));
		txtRegnr.setPreferredSize(new Dimension(100, 19));
		txtKjoretoyId.setPreferredSize(new Dimension(100, 19));
		cbxType.setPreferredSize(new Dimension(100, 19));
		cbxModell.setPreferredSize(new Dimension(100, 19));
		
		this.setLayout(new GridBagLayout());
		pnlKjoretoyId.setLayout(new GridBagLayout());
		pnlTypeOgModell.setLayout(new GridBagLayout());
		pnlUtstyr.setLayout(new GridBagLayout());
		
		pnlKjoretoyId.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.kjoretoyId")));
		pnlTypeOgModell.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.typeOgModell")));
		pnlUtstyr.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.utstyr")));
		
		pnlKjoretoyId.add(rbtnMaskinnrEllerRegnr, 	new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKjoretoyId.add(lblMaskinnummer, 			new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 5), 0, 0));
		pnlKjoretoyId.add(txtMaskinnummer, 			new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKjoretoyId.add(lblRegnr, 				new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 5), 0, 0));
		pnlKjoretoyId.add(txtRegnr, 				new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKjoretoyId.add(rbtnAnnenId, 				new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKjoretoyId.add(lblAnnenIdInfo, 			new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKjoretoyId.add(lblKjoretoyId, 			new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 5), 0, 0));
		pnlKjoretoyId.add(txtKjoretoyId, 			new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKjoretoyId.add(rbtnRegSenere,			new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKjoretoyId.add(lblRegsenereInfo,			new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlTypeOgModell.add(lblType, 				new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTypeOgModell.add(cbxType, 				new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlTypeOgModell.add(lblModell, 				new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlTypeOgModell.add(cbxModell, 				new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlUtstyr.add(chkKjoretoyUtenUtstyr, 		new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlUtstyr.add(chkKjoretoyMaaHaBryterboks, 	new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlUtstyr.add(scrlPaneUtstyr, 				new GridBagConstraints(0, 2, 1, 2, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 0), 0, 0));
		pnlUtstyr.add(btnLeggTilUtstyr, 			new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		pnlUtstyr.add(btnSlettUtstyr, 				new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		
		this.add(pnlKjoretoyId, 					new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 11, 0, 5), 0, 0));
		this.add(pnlTypeOgModell, 					new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 6, 0, 11), 0, 0));
		this.add(pnlUtstyr, 						new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(5, 11, 0, 11), 0, 0));
	}
}
