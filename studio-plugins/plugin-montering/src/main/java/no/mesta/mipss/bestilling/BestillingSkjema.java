package no.mesta.mipss.bestilling;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.UIManager;

import no.mesta.mipss.bestilling.wizard.KjoretoyWizardPage;
import no.mesta.mipss.bestilling.wizard.KontraktWizardPage;
import no.mesta.mipss.bestilling.wizard.MonteringWizardPage;
import no.mesta.mipss.bestilling.wizard.SluttforWizardPage;
import no.mesta.mipss.bestilling.wizard.VareWizardPage;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyKontaktperson;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyKontakttype;
import no.mesta.mipss.persistence.kjoretoyordre.Monteringsskjema;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.reports.BeanDataSource;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.reports.ReportParameter;
import no.mesta.mipss.resources.images.IconResources;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage;


public class BestillingSkjema {
	private static final ResourceBundle resourceBundle = PropertyResourceBundleUtil.getPropertyBundle("monteringText");;
	private final BestillingController bestillingController;
	private final WizardDialog wizardDialog;
	private final BestillingSkjemaType bestillingSkjemaType;
	
	public BestillingSkjema(BestillingSkjemaType bestillingSkjemaType, Driftkontrakt driftkontrakt, Long kjoretoyOrdreId, String loggedOnUser) {
		this.bestillingSkjemaType = bestillingSkjemaType;
		wizardDialog = new WizardDialog(null);
		this.bestillingController = new BestillingController(bestillingSkjemaType, driftkontrakt, kjoretoyOrdreId, loggedOnUser, wizardDialog);
	}
	
	public void showWizard() {
		Wizard wizard;
		switch (bestillingSkjemaType) {
		case OPPRETTING: {
			WizardPage kontrakt = new KontraktWizardPage(bestillingController);
			WizardPage kjoretoy = new KjoretoyWizardPage(bestillingController);
			WizardPage montering = new MonteringWizardPage(bestillingController);
			WizardPage[] pages = {kontrakt, kjoretoy, montering};
			wizard = WizardPage.createWizard(pages, getResultProducer());
			break;
		}
		case ENDRING:{
			WizardPage kontrakt = new KontraktWizardPage(bestillingController);
			WizardPage kjoretoy = new KjoretoyWizardPage(bestillingController);
			WizardPage montering = new MonteringWizardPage(bestillingController);
			WizardPage[] pages = {kontrakt, kjoretoy, montering};
			wizard = WizardPage.createWizard(pages, getResultProducer());
			break;
		}
		case ADMIN_BEHANDLING: {
			WizardPage kontrakt = new KontraktWizardPage(bestillingController);
			WizardPage kjoretoy = new KjoretoyWizardPage(bestillingController);
			WizardPage montering = new MonteringWizardPage(bestillingController);
			WizardPage varer = new VareWizardPage(bestillingController);
			
			bestillingController.setStausUnderBehandling();
			WizardPage[] pages = {kontrakt, kjoretoy, montering, varer};
			wizard = WizardPage.createWizard(pages, getResultProducer());
			break;
		}
		case KONTRAKT_BEHANDLING: {
			WizardPage sluttfor = new SluttforWizardPage(bestillingController);
			WizardPage[] pages = {sluttfor};
			wizard = WizardPage.createWizard(pages, getResultProducer());
			break;
		}
		default:
			wizard = null;
			break;
		}
		UIManager.put("wizard.sidebar.image", ImageUtil.convertImageToBufferedImage(IconResources.WIZARD_ICON.getImage()));
		wizardDialog.startWizard(wizard);
	}
	
	private WizardPage createWizardPage(JPanel panel, String description) {
		WizardPage wizardPage = new WizardPage(description);
		wizardPage.setLayout(new BorderLayout());
		wizardPage.add(panel, BorderLayout.CENTER);
		return wizardPage;
	}
	
	private class WizardDialog extends JDialog{
		public WizardDialog(JDialog parent){
			super(parent, true);
		}
		public void startWizard(Wizard wizard){
			GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0);
			this.setLayout(new GridBagLayout());
			WizardDisplayer.installInContainer(WizardDialog.this, gbc, wizard, null, null, new WizardResultReceiver(){
				@SuppressWarnings("unchecked")
				@Override
				public void cancelled(Map arg0) {
					dispose();
				}
				@Override
				public void finished(Object arg0) {
					dispose();
				}			
			});
			setSize(new Dimension(800, 600));
			setLocationRelativeTo(null);
			setVisible(true);
		}
	}
	
	private WizardPage.WizardResultProducer getResultProducer() {
		return new WizardPage.WizardResultProducer() {
			@Override
			public Object finish(Map arg0) throws WizardException {
				bestillingController.finishPressed();
				return null;
			}
			
			@Override
			public boolean cancel(Map arg0) {
				bestillingController.cancelPressed();
				return false;
			}
		};
	}
	
	public void genererRapport() {
		final Monteringsskjema monteringsskjema = createMonteringsskjema();
		final ReportParameter datoParam = 
    		new ReportParameter("datoForPort", 
    				Date.class, 
    				bestillingController.getKjoretoyordre().getOwnedMipssEntity().getOpprettetDato());
    	
		final Report rapp = new Report(resourceBundle.getString("bestilling"), "best_hovedskjema.jasper");
    	rapp.getDefaultParameters().add(datoParam);

    	
    	final BeanDataSource<Monteringsskjema> ds = new BeanDataSource<Monteringsskjema>(monteringsskjema);
    	final JReportBuilder builder = new JReportBuilder(rapp, rapp.getDefaultParameters(), ds, ReportHelper.EXPORT_PDF, null);
    	final File report = builder.getGeneratedReportFile();

		if (report != null) {
			DesktopHelper.openFile(report);
		}
	}

	private Monteringsskjema createMonteringsskjema() {
		final Monteringsskjema monteringsskjema = new Monteringsskjema(IconResources.MESTA_HOVEDLOGO.getImage());
		monteringsskjema.setOrdreId(String.valueOf(bestillingController.getKjoretoyordre().getId()));
		monteringsskjema.setKjoretoyordretype(bestillingController.getKjoretoyordre().getKjoretoyordretype());
		monteringsskjema.setKjoretoy(bestillingController.getKjoretoy());
		
		monteringsskjema.setProsjektNr(bestillingController.getKjoretoyordre().getProsjektNr());
		monteringsskjema.setAnsvarNr(bestillingController.getKjoretoyordre().getAnsvar());
		monteringsskjema.setLeverandor(bestillingController.getKjoretoy().getLeverandor());
		monteringsskjema.setKontraktKontakttype(bestillingController.getDriftkontrakt().getDriftsleder());
		monteringsskjema.setMonteringsOpplysninger(bestillingController.getKjoretoyordre().getFritekst());
		if(bestillingController.getInstallasjon() != null && bestillingController.getInstallasjon().getDfumodell() != null) {
			monteringsskjema.setDfumodell(bestillingController.getInstallasjon().getDfumodell());
		}
		monteringsskjema.setDriftkontrakt(bestillingController.getDriftkontrakt());
		monteringsskjema.setKjoretoyordreUtstyrList(bestillingController.getKjoretoyordre().getKjoretoyordreUtstyrList());
		
		List<KjoretoyKontaktperson> kontaktPersonerList = new ArrayList<KjoretoyKontaktperson>();
		kontaktPersonerList.add(bestillingController.getKjoretoy().getKjoretoyeier());
		
		KjoretoyKontakttype kk = new KjoretoyKontakttype();
		kk.setKontaktnavn(bestillingController.getKjoretoyordre().getKontaktnavn());
		kk.setTlfnummer1(bestillingController.getKjoretoyordre().getKontakttelefon());
		kontaktPersonerList.add(kk);
		
		monteringsskjema.setKontaktPersonerList(kontaktPersonerList);
		
		monteringsskjema.setSendesEkspress(bestillingController.getDfubestillingmaster().getBedriftspakkeFlaggBoolean());
		monteringsskjema.setNavn(bestillingController.getDfubestillingmaster().getLeveringsNavn());
		monteringsskjema.setAddresse(bestillingController.getDfubestillingmaster().getLeveringsAdresse());
		monteringsskjema.setPostnr(bestillingController.getDfubestillingmaster().getLeveringsPostnummer().toString());
		monteringsskjema.setPoststed(bestillingController.getDfubestillingmaster().getLeveringsPoststed());
		monteringsskjema.setForsendelseMerknad(bestillingController.getDfubestillingmaster().getFritekst());
		
		monteringsskjema.setMipssDfuvareList(bestillingController.getDfulevBestilling().getDfubestvarelinjerList());
		
		if(bestillingController.getInstallasjon() != null) {
			monteringsskjema.setInstallasjonKommentar(bestillingController.getInstallasjon().getMonteringsinfo().getFritekst());
			monteringsskjema.setPlasseringsnotat(bestillingController.getInstallasjon().getMonteringsinfo().getPlasseringsnotat());
			monteringsskjema.setSpenningTenning(bestillingController.getInstallasjon().getMonteringsinfo().getSpenningTenning());
			monteringsskjema.setSpenningPluss(bestillingController.getInstallasjon().getMonteringsinfo().getSpenningPluss());
			monteringsskjema.setSpenningJord(bestillingController.getInstallasjon().getMonteringsinfo().getSpenningJord());
			monteringsskjema.setMontornavn(bestillingController.getInstallasjon().getMonteringsinfo().getMontornavn());
			monteringsskjema.setInstallasjonsted(bestillingController.getInstallasjon().getMonteringsinfo().getInstallasjonsted());
			monteringsskjema.setSigneringsdato(bestillingController.getInstallasjon().getMonteringsinfo().getSigneringsdato());
			if(bestillingController.getInstallasjon().getDfuindivid() != null) {
				monteringsskjema.setDfuSerienummer(bestillingController.getInstallasjon().getDfuindivid().getSerienummer());
				monteringsskjema.setDfuTelefonnummer(bestillingController.getInstallasjon().getDfuindivid().getTlfnummerString());
			}
		}
		
		monteringsskjema.setInstallasjon(bestillingController.getInstallasjon());
		monteringsskjema.setMaaHaBryterboks(bestillingController.getKjoretoyordre().getMaaHaBryterboks());
		
		return monteringsskjema;
	}
}
