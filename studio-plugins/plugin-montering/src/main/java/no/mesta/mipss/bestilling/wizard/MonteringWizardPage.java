package no.mesta.mipss.bestilling.wizard;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.bestilling.BestillingController;
import no.mesta.mipss.bestilling.MonteringPanel;

public class MonteringWizardPage extends AbstractBestillingWizardPage {
	public MonteringWizardPage(final BestillingController bestillingController) {
		super(bestillingController, 
				BestillingController.getResourceBundle().getString("wizard.step.montering"), 
				new MonteringPanel(bestillingController),
				"dfubestillingmaster.leveringsNavn",
				"dfubestillingmaster.leveringsAdresse",
				"dfubestillingmaster.leveringsPostnummer",
				"dfubestillingmaster.leveringsPoststed");
	}

	@Override
	protected void updateNextStatus() {
		List<String> problemStringList = new ArrayList<String>();
		
		if(bestillingController.getDfubestillingmaster().getLeveringsNavn() == null
				|| bestillingController.getDfubestillingmaster().getLeveringsNavn().length() == 0) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.montering.mottaker"));
		} 
		if(bestillingController.getDfubestillingmaster().getLeveringsAdresse() == null
				|| bestillingController.getDfubestillingmaster().getLeveringsAdresse().length() == 0) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.montering.adresse"));
		}
		if(bestillingController.getDfubestillingmaster().getLeveringsPostnummer() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.montering.postnummer"));
		} 
		if(bestillingController.getDfubestillingmaster().getLeveringsPoststed() == null
				|| bestillingController.getDfubestillingmaster().getLeveringsPoststed().length() == 0) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.montering.poststed"));
		}
		
		updateNextStatus(problemStringList);
	}
}
