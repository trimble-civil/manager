package no.mesta.mipss.bestilling.wizard;

import java.awt.BorderLayout;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.bestilling.BestillingController;
import no.mesta.mipss.core.BindingHelper;

import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

public abstract class AbstractBestillingWizardPage extends WizardPage {
	protected BestillingController bestillingController;
	
	public AbstractBestillingWizardPage(BestillingController bestillingController, String stepDescription, JPanel panel, String... endringsProperties) {
		super(stepDescription, false);
		this.bestillingController = bestillingController;
		this.setLayout(new BorderLayout());
		this.add(panel, BorderLayout.CENTER);
		for(String s : endringsProperties) {
			lyttPaaEndring(s);
		}
	}
	
	private void lyttPaaEndring(String property) {
		BindingHelper.createbinding(bestillingController, property, this, "notify", BindingHelper.READ).bind();
	}
	
	@Override
	protected void renderingPage() {
		doUpdate();
	}

	public void setNotify(Object o) {
		doUpdate();
	}
	
	protected void doUpdate() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				updateNextStatus();
			}
		});
	}
	
	private String stringListToString(final List<String> problemStringList) {
		final StringBuilder sb = new StringBuilder("Mangler: ");
		for(final String s : problemStringList) {
			sb.append(s + ", ");
		}
		sb.delete(sb.length()-2, sb.length());
		return sb.toString();
	}
	
	protected void updateNextStatus(final List<String> problemStringList) {
		if(problemStringList.size() > 0) {
			setProblem(stringListToString(problemStringList));
		} else {
			setProblem(null);
			setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
		}
	}
	
	/**
	 * Blir kalt når siden skal oppdatere knappene i wizarden.
	 * Bør kalle updateNextStatus(final List<String> problemStringList) med en liste av problemer.
	 */
	protected abstract void updateNextStatus();
}
