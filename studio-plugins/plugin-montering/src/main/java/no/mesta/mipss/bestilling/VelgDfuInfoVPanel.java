package no.mesta.mipss.bestilling;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.kjoretoy.DfuInfoVQueryFilter;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.SokPanel;
import no.mesta.mipss.util.KjoretoyUtils;

public class VelgDfuInfoVPanel extends AbstractRelasjonPanel<DfuInfoV> {
	private SokPanel<DfuInfoV> sokPanel;
	
	public VelgDfuInfoVPanel(MipssKjoretoy mipssKjoretoy, String leverandorNavn, Boolean skjulMonterteDfu) {
		final DfuInfoVQueryFilter filter = new DfuInfoVQueryFilter(skjulMonterteDfu);
		filter.setLeverandorNavn(leverandorNavn);
		sokPanel = new SokPanel<DfuInfoV>(DfuInfoV.class,
				  filter,
				  mipssKjoretoy,
				  "getDfuInfoVList",
				  MipssKjoretoy.class,
				  false,
				  "serienummer",
				  "tlfnummer",
				  "modellNavn");
		
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void updateComplete() {
		setComplete(Boolean.valueOf(sokPanel.getSelectedEntity() != null));
	}
	
	private void initGui() {
		this.setLayout(new GridBagLayout());
		this.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));

	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(500, 500);
	}

	@Override
	public DfuInfoV getNyRelasjon() {
		return sokPanel.getSelectedEntity();
	}

	@Override
	public String getTitle() {
		return KjoretoyUtils.getPropertyString("dfu.velgIndivid.dialogTitle");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
