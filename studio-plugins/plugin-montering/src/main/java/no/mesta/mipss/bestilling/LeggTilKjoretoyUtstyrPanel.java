package no.mesta.mipss.bestilling;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

public class LeggTilKjoretoyUtstyrPanel extends AbstractRelasjonPanel<KjoretoyUtstyr> {
	private JPanel pnlMain = new JPanel();
	
	private JLabel lblUtstyrgruppe = new JLabel();
	private JLabel lblUtstyrSubgruppe = new JLabel();
	private JLabel lblUtstyrModell = new JLabel();
	
	private JComboBox cbxUtstyrgruppe = new JComboBox();
	private JComboBox cbxUtstyrSubgruppe = new JComboBox();
	private JComboBox cbxUtstyrModell = new JComboBox();

	private final KjoretoyUtstyrController kjoretoyUtstyrController;
	
		
	public LeggTilKjoretoyUtstyrPanel(final KjoretoyUtstyrController kjoretoyUtstyrController) {
		this.kjoretoyUtstyrController = kjoretoyUtstyrController;
	
		initComboboxes();
		initGui();
	}
	
	private void initComboboxes() {
		MipssComboBoxModel<Utstyrgruppe> cbxModelUtstyrgruppe = new MipssComboBoxModel<Utstyrgruppe>(new ArrayList<Utstyrgruppe>(), true);
        cbxUtstyrgruppe.setModel(cbxModelUtstyrgruppe);
        cbxUtstyrgruppe.setRenderer(new MipssListCellRenderer<Utstyrgruppe>());
        
        MipssComboBoxModel<Utstyrsubgruppe> cbxModelUtstyrsubgruppe = new MipssComboBoxModel<Utstyrsubgruppe>(new ArrayList<Utstyrsubgruppe>(), true);
        cbxUtstyrSubgruppe.setModel(cbxModelUtstyrsubgruppe);
        cbxUtstyrSubgruppe.setRenderer(new MipssListCellRenderer<Utstyrsubgruppe>());
        
        MipssComboBoxModel<Utstyrmodell> cbxModelUtstyrmodell = new MipssComboBoxModel<Utstyrmodell>(new ArrayList<Utstyrmodell>(), true);
        cbxUtstyrModell.setModel(cbxModelUtstyrmodell);
        cbxUtstyrModell.setRenderer(new MipssListCellRenderer<Utstyrmodell>());
        
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrgruppe", cbxUtstyrgruppe, "selectedItem", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrsubgruppe", cbxUtstyrSubgruppe, "selectedItem", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrmodell", cbxUtstyrModell, "selectedItem", BindingHelper.READ_WRITE).bind();
        
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrgruppeList", cbxModelUtstyrgruppe, "entities", BindingHelper.READ).bind();
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrsubgruppeList", cbxModelUtstyrsubgruppe, "entities", BindingHelper.READ).bind();
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrmodellList", cbxModelUtstyrmodell, "entities", BindingHelper.READ).bind();
        
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrgruppeEnabled", cbxUtstyrgruppe, "enabled", BindingHelper.READ).bind();
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrsubgruppeEnabled", cbxUtstyrSubgruppe, "enabled", BindingHelper.READ).bind();
        BindingHelper.createbinding(kjoretoyUtstyrController, "utstyrmodellEnabled", cbxUtstyrModell, "enabled", BindingHelper.READ).bind();
	
        kjoretoyUtstyrController.addPropertyChangeListener("complete", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				setComplete((Boolean)evt.getNewValue());
			}
		});
	}
	
	private void initGui() {
		lblUtstyrgruppe.setText(BestillingController.getResourceBundle().getString("label.utstyrgruppe"));
		lblUtstyrSubgruppe.setText(BestillingController.getResourceBundle().getString("label.utstyrsubgruppe"));
		lblUtstyrModell.setText(BestillingController.getResourceBundle().getString("label.utstyrmodell"));
		
		cbxUtstyrgruppe.setPreferredSize(new Dimension(100, 19));
		cbxUtstyrSubgruppe.setPreferredSize(new Dimension(100, 19));
		cbxUtstyrModell.setPreferredSize(new Dimension(100, 19));
		
		pnlMain.setLayout(new GridBagLayout());
		
		pnlMain.add(lblUtstyrgruppe, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxUtstyrgruppe, 		new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblUtstyrSubgruppe,		new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(cbxUtstyrSubgruppe, 	new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblUtstyrModell, 		new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(cbxUtstyrModell, 		new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, 					new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}

	@Override
	public Dimension getDialogSize() {
		return new Dimension(300, 160);
	}

	/**
	 * Utstyret som returneres har kun gruppe, subgruppe og modell satt
	 */
	@Override
	public KjoretoyUtstyr getNyRelasjon() {
		KjoretoyUtstyr kjoretoyUtstyr = new KjoretoyUtstyr();
		kjoretoyUtstyr.setUtstyrgruppe(kjoretoyUtstyrController.getUtstyrgruppe());
		kjoretoyUtstyr.setUtstyrsubgruppe(kjoretoyUtstyrController.getUtstyrsubgruppe());
		kjoretoyUtstyr.setUtstyrmodell(kjoretoyUtstyrController.getUtstyrmodell());
		return kjoretoyUtstyr;
	}

	@Override
	public String getTitle() {
		return BestillingController.getResourceBundle().getString("dialogTitle.utstyr");
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
