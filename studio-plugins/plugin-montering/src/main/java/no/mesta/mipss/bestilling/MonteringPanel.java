package no.mesta.mipss.bestilling;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.ui.DocumentFilter;

import org.jdesktop.beansbinding.Binding;

public class MonteringPanel extends JPanel {
	private JPanel pnlMonteringOpplysninger = new JPanel();
	private JPanel pnlLeveringsadresse = new JPanel();
	
	private JTextArea txtAreaOpplysningerOmMontering = new JTextArea();
	private JScrollPane scrlPaneOpplysninger = new JScrollPane();
	
	private JCheckBox chkBedriftspakke = new JCheckBox();
//	private JCheckBox chkVerskstedAdmin = new JCheckBox();
	
	private JLabel lblMottaker = new JLabel();
	private JLabel lblAdresse = new JLabel();
	private JLabel lblPostadresse = new JLabel();
	private JLabel lblForsendelseMerknad = new JLabel();
	
	private JFormattedTextField txtMottaker = new JFormattedTextField();
	private JFormattedTextField txtAdresse = new JFormattedTextField();
	private JFormattedTextField txtPostnummer = new JFormattedTextField();
	private JFormattedTextField txtPoststed = new JFormattedTextField();
	private JFormattedTextField txtForsendelseMerknad = new JFormattedTextField();
	
	private final BestillingController bestillingController;
	
	public MonteringPanel(final BestillingController bestillingController) {
		this.bestillingController = bestillingController;
		
		initTextfields();
		initGui();
	}

	private void initTextfields() {
		LongStringConverter longStringConverter = new LongStringConverter();
		
		txtAreaOpplysningerOmMontering.setDocument(new DocumentFilter(4000, null, false));
		txtMottaker.setDocument(new DocumentFilter(255, null, false));
		txtAdresse.setDocument(new DocumentFilter(255, null, false));
		txtPostnummer.setDocument(new DocumentFilter(4, "0123456789", false));
		txtPoststed.setDocument(new DocumentFilter(60, null, false));
		txtForsendelseMerknad.setDocument(new DocumentFilter(4000, null, false));
		
		txtAreaOpplysningerOmMontering.setLineWrap(true);
		txtAreaOpplysningerOmMontering.setWrapStyleWord(true);
		
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.fritekst", txtAreaOpplysningerOmMontering, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "dfubestillingmaster.leveringsNavn", txtMottaker, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "dfubestillingmaster.leveringsAdresse", txtAdresse, "text", BindingHelper.READ_WRITE).bind();
		Binding postnrBinding = BindingHelper.createbinding(bestillingController, "dfubestillingmaster.leveringsPostnummer", txtPostnummer, "text", BindingHelper.READ_WRITE);
		BindingHelper.createbinding(bestillingController, "dfubestillingmaster.leveringsPoststed", txtPoststed, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "dfubestillingmaster.fritekst", txtForsendelseMerknad, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "dfubestillingmaster.bedriftspakkeFlaggBoolean", chkBedriftspakke, "selected", BindingHelper.READ_WRITE).bind();
		
//		BindingHelper.createbinding(bestillingController, "kjoretoyordre.verkstedAdminFlagg", chkVerskstedAdmin, "selected", BindingHelper.READ_WRITE).bind();

		
		postnrBinding.setConverter(longStringConverter);
		postnrBinding.bind();
		
		//TODO bedriftspakkeboolean!
	}

	private void initGui() {
		scrlPaneOpplysninger.setViewportView(txtAreaOpplysningerOmMontering);
		
		chkBedriftspakke.setText(BestillingController.getResourceBundle().getString("label.sendesSomBedriftspakke"));
//		chkVerskstedAdmin.setText(BestillingController.getResourceBundle().getString("label.verkstedAdmin"));
		lblMottaker.setText(BestillingController.getResourceBundle().getString("label.mottaker"));
		lblAdresse.setText(BestillingController.getResourceBundle().getString("label.adresse"));
		lblPostadresse.setText(BestillingController.getResourceBundle().getString("label.postadresse"));
		lblForsendelseMerknad.setText(BestillingController.getResourceBundle().getString("label.forsendelseMerkes"));
		
		txtMottaker.setPreferredSize(new Dimension(100, 19));
		txtAdresse.setPreferredSize(new Dimension(100, 19));
		txtPostnummer.setPreferredSize(new Dimension(100, 19));
		txtPoststed.setPreferredSize(new Dimension(100, 19));
		txtForsendelseMerknad.setPreferredSize(new Dimension(100, 19));
		
		this.setLayout(new GridBagLayout());
		pnlMonteringOpplysninger.setLayout(new GridBagLayout());
		pnlLeveringsadresse.setLayout(new GridBagLayout());
		
		pnlMonteringOpplysninger.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.monteringOpplysningner")));
		pnlLeveringsadresse.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.leveringsadresse")));
		
		pnlMonteringOpplysninger.add(scrlPaneOpplysninger,	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
//		pnlMonteringOpplysninger.add(chkVerskstedAdmin,	new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlLeveringsadresse.add(chkBedriftspakke, 			new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeveringsadresse.add(lblMottaker, 				new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeveringsadresse.add(txtMottaker, 				new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeveringsadresse.add(lblAdresse, 				new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeveringsadresse.add(txtAdresse, 				new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeveringsadresse.add(lblPostadresse, 			new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeveringsadresse.add(txtPostnummer, 				new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeveringsadresse.add(txtPoststed, 				new GridBagConstraints(2, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeveringsadresse.add(lblForsendelseMerknad, 		new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeveringsadresse.add(txtForsendelseMerknad, 		new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		this.add(pnlMonteringOpplysninger, 					new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 11, 0, 11), 0, 0));
		this.add(pnlLeveringsadresse, 						new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 0, 11), 0, 0));
	}
}
