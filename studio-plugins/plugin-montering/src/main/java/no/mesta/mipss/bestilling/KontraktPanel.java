package no.mesta.mipss.bestilling;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.ui.DocumentFilter;

import org.jdesktop.beansbinding.Binding;

public class KontraktPanel extends JPanel {
	private final BestillingController bestillingController;
	
	private JPanel pnlOrdre = new JPanel();
	private JPanel pnlKontrakt = new JPanel();
	private JPanel pnlKontaktPerson = new JPanel();
	private JPanel pnlLeverandorOgEier = new JPanel();
	
	private JLabel lblOrdreType = new JLabel();
	private JLabel lblOrdreStatus = new JLabel();
	private JLabel lblKontraktsleder = new JLabel();
	private JLabel lblKontraktsted = new JLabel();
	private JLabel lblBestiller = new JLabel();
	private JLabel lblDato = new JLabel();
	private JLabel lblProsjektnummer = new JLabel();
	private JLabel lblKontaktperson = new JLabel();
	private JLabel lblTelefonnummer = new JLabel();
	private JLabel lblLeverandor = new JLabel();
	private JLabel lblEier = new JLabel();
	
	private JFormattedTextField txtOrdreType = new JFormattedTextField();
	private JFormattedTextField txtOrdreStatus = new JFormattedTextField();
	private JFormattedTextField txtKontraktsleder = new JFormattedTextField();
	private JFormattedTextField txtKontraktsted = new JFormattedTextField();
	private JFormattedTextField txtBestiller = new JFormattedTextField();
	private JFormattedTextField txtDato = new JFormattedTextField();
	private JFormattedTextField txtProsjektnummer = new JFormattedTextField();
	private JFormattedTextField txtKontaktperson = new JFormattedTextField();
	private JFormattedTextField txtTelefonnummer = new JFormattedTextField();
	private JFormattedTextField txtLeverandor = new JFormattedTextField();
	private JFormattedTextField txtEier = new JFormattedTextField();
	
	private JButton btnLeverandorSok = new JButton();
	private JButton btnEierSok = new JButton();

	public KontraktPanel(final BestillingController bestillingController) {
		this.bestillingController = bestillingController;		
		
		initGui();
		initBindings();
		initButtons();
	}
	
	private void initBindings() {
		LongStringConverter longStringConverter = new LongStringConverter();
		
		txtTelefonnummer.setDocument(new DocumentFilter(8, "0123456789", false));
		
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.kjoretoyordretype.navn", txtOrdreType, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.sisteKjoretoyordrestatus.navn", txtOrdreStatus, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "driftkontrakt.driftsleder.kontaktnavn", txtKontraktsleder, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "driftkontrakt.textForGUI", txtKontraktsted, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.signatur", txtBestiller, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.ownedMipssEntity.opprettetDatoString", txtDato, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.prosjektNr", txtProsjektnummer, "text", BindingHelper.READ).bind();
		
		BindingHelper.createbinding(bestillingController, "kjoretoy.leverandor.textForGUI", txtLeverandor, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "kjoretoy.kjoretoyeier.textForGUI", txtEier, "text", BindingHelper.READ).bind();
		
		BindingHelper.createbinding(bestillingController, "kjoretoyordre.kontaktnavn", txtKontaktperson, "text", BindingHelper.READ_WRITE).bind();
		Binding b = BindingHelper.createbinding(bestillingController, "kjoretoyordre.kontakttelefon", txtTelefonnummer, "text", BindingHelper.READ_WRITE);
	
		b.setConverter(longStringConverter);
		b.bind();
	}
	
	private void initButtons() {
		btnLeverandorSok.setAction(bestillingController.getVelgLeverandorAction());
		btnEierSok.setAction(bestillingController.getVelgEierAction());
	}
	
	public void initGui() {
		txtOrdreStatus.setEditable(false);
		txtOrdreType.setEditable(false);
		txtKontraktsleder.setEditable(false);
		txtKontraktsted.setEditable(false);
		txtBestiller.setEditable(false);
		txtDato.setEditable(false);
		txtProsjektnummer.setEditable(false);
		txtLeverandor.setEditable(false);
		txtEier.setEditable(false);
		txtOrdreStatus.setFocusable(false);
		txtOrdreType.setFocusable(false);
		txtKontraktsleder.setFocusable(false);
		txtKontraktsted.setFocusable(false);
		txtBestiller.setFocusable(false);
		txtDato.setFocusable(false);
		txtProsjektnummer.setFocusable(false);
		txtLeverandor.setFocusable(false);
		txtEier.setFocusable(false);
		
		lblOrdreType.setText(BestillingController.getResourceBundle().getString("label.ordreType"));
		lblOrdreStatus.setText(BestillingController.getResourceBundle().getString("label.ordreStatus"));
		lblKontraktsleder.setText(BestillingController.getResourceBundle().getString("label.driftsleder"));
		lblKontraktsted.setText(BestillingController.getResourceBundle().getString("label.kontraktssted"));
		lblBestiller.setText(BestillingController.getResourceBundle().getString("label.bestiller"));
		lblDato.setText(BestillingController.getResourceBundle().getString("label.dato"));
		lblProsjektnummer.setText(BestillingController.getResourceBundle().getString("label.prosjektnummer"));
		lblKontaktperson.setText(BestillingController.getResourceBundle().getString("label.kontaktPerson"));
		lblTelefonnummer.setText(BestillingController.getResourceBundle().getString("label.telefonnummer"));
		lblLeverandor.setText(BestillingController.getResourceBundle().getString("label.leverandor"));
		lblEier.setText(BestillingController.getResourceBundle().getString("label.eier"));
		
		txtOrdreType.setPreferredSize(new Dimension(100, 19));
		txtOrdreStatus.setPreferredSize(new Dimension(100, 19));
		txtKontraktsleder.setPreferredSize(new Dimension(100, 19));
		txtKontraktsted.setPreferredSize(new Dimension(100, 19));
		txtBestiller.setPreferredSize(new Dimension(100, 19));
		txtDato.setPreferredSize(new Dimension(100, 19));
		txtProsjektnummer.setPreferredSize(new Dimension(100, 19));
		txtKontaktperson.setPreferredSize(new Dimension(100, 19));
		txtTelefonnummer.setPreferredSize(new Dimension(100, 19));
		txtLeverandor.setPreferredSize(new Dimension(100, 19));
		txtEier.setPreferredSize(new Dimension(100, 19));
		
		this.setLayout(new GridBagLayout());
		pnlOrdre.setLayout(new GridBagLayout());
		pnlKontrakt.setLayout(new GridBagLayout());
		pnlKontaktPerson.setLayout(new GridBagLayout());
		pnlLeverandorOgEier.setLayout(new GridBagLayout());
		
		pnlOrdre.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.ordre")));
		pnlKontrakt.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.kontrakt")));
		pnlKontaktPerson.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.kontaktPerson")));
		pnlLeverandorOgEier.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.leverandorOgEier")));
		
		pnlOrdre.add(lblOrdreType, 					new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlOrdre.add(txtOrdreType, 					new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlOrdre.add(lblOrdreStatus, 				new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlOrdre.add(txtOrdreStatus, 				new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlKontrakt.add(lblKontraktsleder, 			new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKontrakt.add(txtKontraktsleder, 			new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlKontrakt.add(lblKontraktsted, 			new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKontrakt.add(txtKontraktsted, 			new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlKontrakt.add(lblBestiller, 				new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontrakt.add(txtBestiller, 				new GridBagConstraints(3, 0, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontrakt.add(lblDato, 					new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontrakt.add(txtDato, 					new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlKontrakt.add(lblProsjektnummer, 			new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontrakt.add(txtProsjektnummer, 			new GridBagConstraints(5, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlKontaktPerson.add(lblKontaktperson, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKontaktPerson.add(txtKontaktperson, 		new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlKontaktPerson.add(lblTelefonnummer, 		new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlKontaktPerson.add(txtTelefonnummer, 		new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlLeverandorOgEier.add(lblLeverandor, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeverandorOgEier.add(txtLeverandor, 		new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeverandorOgEier.add(btnLeverandorSok, 	new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeverandorOgEier.add(lblEier, 			new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlLeverandorOgEier.add(txtEier, 			new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlLeverandorOgEier.add(btnEierSok, 		new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		this.add(pnlOrdre, 							new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 0, 11), 0, 0));
		this.add(pnlKontrakt, 						new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 0, 11), 0, 0));
		this.add(pnlKontaktPerson, 					new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 0, 5), 0, 0));
		this.add(pnlLeverandorOgEier, 				new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 6, 0, 11), 0, 0));
	}
}
