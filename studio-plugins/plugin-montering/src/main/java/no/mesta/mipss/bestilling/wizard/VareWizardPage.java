package no.mesta.mipss.bestilling.wizard;

import no.mesta.mipss.bestilling.BestillingController;
import no.mesta.mipss.bestilling.VarePanel;

public class VareWizardPage extends AbstractBestillingWizardPage {
	public VareWizardPage(final BestillingController bestillingController) {
		super(bestillingController, BestillingController.getResourceBundle().getString("wizard.step.varer"), new VarePanel(bestillingController));
	}

	@Override
	protected void updateNextStatus() {
		// TODO Auto-generated method stub
		
	}
}
