package no.mesta.mipss.bestilling;

public enum BestillingSkjemaType {
	/** En ordre/bestilling opprettes **/
	OPPRETTING,
	/** En ordre/bestilling endres **/
	ENDRING, 
	/** Bestilling behandles hos admin **/
	ADMIN_BEHANDLING,
	/** Bestilling behandles hos kontrakten **/
	KONTRAKT_BEHANDLING,
	/** Skjema tas ut som en rapport **/
	RAPPORTUTTAK;
}
