package no.mesta.mipss.bestilling;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestvarelinjer;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

public class VarePanel extends JPanel {
	private JPanel pnlDfuleverandor = new JPanel();
	private JPanel pnlDfu = new JPanel();
	private JPanel pnlVare = new JPanel();
	private JPanel pnlVarekatalog = new JPanel();
	private JPanel pnlValgteVarer = new JPanel();
	private JPanel pnlButtons = new JPanel();
	
	private JLabel lblDfuleverandor = new JLabel();
	private JLabel lblDfuModell = new JLabel();
	private JLabel lblVarekatalog = new JLabel();
	private JLabel lblValgteVarer = new JLabel();
	private JLabel lblValgteVarerPris = new JLabel();
	
	private JFormattedTextField txtDfuleverandor = new JFormattedTextField();
	private JComboBox cbxDfuModell = new JComboBox();
	
	private JButton btnLeggTilVare = new JButton();
	private JButton btnFjernVare = new JButton();
	
	private JScrollPane scrlPaneVarekatalog = new JScrollPane();
	private JScrollPane scrlPaneValgteVarer = new JScrollPane();
	
	private JMipssBeanTable<Dfuvarekatalog> tblVarekatalog;
	private JMipssBeanTable<Dfubestvarelinjer> tblValgteVarer;
	
	private final BestillingController bestillingController;
	private MipssComboBoxModel<Dfumodell> cbxModelDfumodell;
	private MipssBeanTableModel<Dfubestvarelinjer> tblModelDfubestvarelinjer;
	private MipssBeanTableModel<Dfuvarekatalog> tblModelDfuvarekatalog;
	
	public VarePanel(final BestillingController bestillingController) {
		this.bestillingController = bestillingController;
		
		initTextFields();
		initCombobox();
		initTables();
		initButtons();
		initGui();
	}

	private void initTextFields() {
		BindingHelper.createbinding(bestillingController, "dfulevBestilling.leverandorNavn", txtDfuleverandor, "text", BindingHelper.READ).bind();
	}
	
	private void initCombobox() {
		List<Dfumodell> dfumodellList = bestillingController.fetchDfumodellList();
		cbxModelDfumodell = new MipssComboBoxModel<Dfumodell>(dfumodellList, false);
        cbxDfuModell.setModel(cbxModelDfumodell);
        cbxDfuModell.setRenderer(new MipssListCellRenderer<Dfumodell>());
	}
	
	private void initButtons() {
		final Action refreshAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tblValgteVarer.tableChanged(new TableModelEvent(tblModelDfubestvarelinjer));
				lblValgteVarerPris.setText(bestillingController.getTotalVarePrisString());
			}
		}; 
		
		btnLeggTilVare.setAction(bestillingController.getLeggTilVareAction(tblModelDfuvarekatalog, tblVarekatalog, refreshAction));
		btnFjernVare.setAction(bestillingController.getFjernVareAction(tblModelDfubestvarelinjer, tblValgteVarer, refreshAction));
	}
	
	private void initTables() {
		tblModelDfubestvarelinjer = new MipssBeanTableModel<Dfubestvarelinjer>("dfubestvarelinjerList", Dfubestvarelinjer.class, "varenummer", "beskrivelse", "antall", "totalPris");
		
		BindingHelper.createbinding(bestillingController, "dfulevBestilling", tblModelDfubestvarelinjer, "sourceObject", BindingHelper.READ).bind();
		
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Dfubestvarelinjer.class, tblModelDfubestvarelinjer.getColumns());
		tblValgteVarer = new JMipssBeanTable<Dfubestvarelinjer>(tblModelDfubestvarelinjer, colModel);
		tblValgteVarer.setFillsViewportWidth(true);
		tblValgteVarer.setDoWidthHacks(false);
		tblValgteVarer.setDefaultRenderer(Dfuvarekatalog.class, new RenderableMipssEntityTableCellRenderer<Utstyrmodell>());
		tblValgteVarer.getColumn(0).setMaxWidth(60);
		tblValgteVarer.getColumn(2).setMaxWidth(40);
		tblValgteVarer.getColumn(3).setMaxWidth(50);
		tblValgteVarer.addMouseMotionListener(new MouseMotionListener(){
			@Override
			public void mouseDragged(MouseEvent e) {
			}
			@Override
			public void mouseMoved(MouseEvent e) {
				int row = tblValgteVarer.rowAtPoint(e.getPoint());
				if (row!=-1){
					Dfubestvarelinjer kat = tblModelDfubestvarelinjer.get(tblValgteVarer.convertRowIndexToModel(row));
					String tooltip = kat.getDfuvarekatalog().getFritekst()!=null?kat.getDfuvarekatalog().getFritekst():"Mangler fritekst";
				    StringBuilder sb = new StringBuilder();
					sb.append("<html>");
					if (tooltip.indexOf('\n')==-1){
						sb.append(wrap(tooltip, 80));	
					}else{
						sb.append(tooltip.replaceAll("\n", "<br>"));
					}
					
					sb.append("</html>");
					tblValgteVarer.setToolTipText(sb.toString());
				}
			}
		});
		
		
		tblModelDfuvarekatalog = new MipssBeanTableModel<Dfuvarekatalog>(Dfuvarekatalog.class, "varenummer", "beskrivelse", "pris");
		MipssRenderableEntityTableColumnModel vareColModel = new MipssRenderableEntityTableColumnModel(Dfuvarekatalog.class, tblModelDfuvarekatalog.getColumns());
		tblVarekatalog = new JMipssBeanTable<Dfuvarekatalog>(tblModelDfuvarekatalog, vareColModel);
		tblVarekatalog.setFillsViewportWidth(true);
		tblVarekatalog.setDoWidthHacks(false);
		tblVarekatalog.getColumn(0).setMaxWidth(60);
		tblVarekatalog.getColumn(2).setMaxWidth(50);
		
		tblModelDfuvarekatalog.setEntities(bestillingController.fetchDfuvarekatalogList());
		
		
		tblVarekatalog.addMouseMotionListener(new MouseMotionListener(){
			@Override
			public void mouseDragged(MouseEvent e) {
			}
			@Override
			public void mouseMoved(MouseEvent e) {
				int row = tblVarekatalog.rowAtPoint(e.getPoint());
				if (row!=-1){
					Dfuvarekatalog kat = tblModelDfuvarekatalog.get(tblValgteVarer.convertRowIndexToModel(row));
					String tooltip = kat.getFritekst()!=null?kat.getFritekst():"Mangler fritekst";
				    StringBuilder sb = new StringBuilder();
					sb.append("<html>");
					if (tooltip.indexOf('\n')==-1){
						sb.append(wrap(tooltip, 80));	
					}else{
						sb.append(tooltip.replaceAll("\n", "<br>"));
					}
					
					sb.append("</html>");
					tblVarekatalog.setToolTipText(sb.toString());
				}
			}
		});
	}
	private String wrap(String line, int maxLineLen) {
		   if (line.length() <= maxLineLen) {
		      return line;
		   }
		   int indexOfBlank = line.lastIndexOf(" ", maxLineLen);
		   int split;
		   int offset;
		   if (indexOfBlank > -1) {
		      split = indexOfBlank;
		      offset = 1;
		   } else {
		      split = maxLineLen;
		      offset = 0;
		   }
		   return line.substring(0, split) + "<br>" +
		      wrap(line.substring(split + offset), maxLineLen);
	}
	
	private void initGui() {
		scrlPaneVarekatalog.setViewportView(tblVarekatalog);
		scrlPaneValgteVarer.setViewportView(tblValgteVarer);
		
		lblDfuleverandor.setText(BestillingController.getResourceBundle().getString("label.dfuleverandor"));
		lblDfuModell.setText(BestillingController.getResourceBundle().getString("label.dfuModell"));
		lblVarekatalog.setText(BestillingController.getResourceBundle().getString("label.varekatalog"));
		lblValgteVarer.setText(BestillingController.getResourceBundle().getString("label.valgteVarer"));
		
		cbxDfuModell.setPreferredSize(new Dimension(150, 19));
		txtDfuleverandor.setPreferredSize(new Dimension(150, 19));
		txtDfuleverandor.setEditable(false);
		
		this.setLayout(new GridBagLayout());
		pnlDfu.setLayout(new GridBagLayout());
		pnlVare.setLayout(new GridBagLayout());
		pnlVarekatalog.setLayout(new GridBagLayout());
		pnlValgteVarer.setLayout(new GridBagLayout());
		pnlButtons.setLayout(new GridBagLayout());
		pnlDfuleverandor.setLayout(new GridBagLayout());
		
		pnlDfuleverandor.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.dfuleverandor")));
		pnlDfu.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.dfu")));
		pnlVare.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.vare")));
		
		pnlDfuleverandor.add(lblDfuleverandor,	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDfuleverandor.add(txtDfuleverandor,	new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlDfu.add(lblDfuModell,				new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDfu.add(cbxDfuModell,				new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlVarekatalog.add(lblVarekatalog, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlVarekatalog.add(scrlPaneVarekatalog, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlValgteVarer.add(lblValgteVarer, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlValgteVarer.add(scrlPaneValgteVarer,	new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlButtons.add(btnLeggTilVare, 			new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnFjernVare,			new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlVare.add(pnlVarekatalog, 			new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlVare.add(pnlButtons, 				new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
		pnlVare.add(pnlValgteVarer, 			new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		pnlVare.add(lblValgteVarerPris, 		new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		
		this.add(pnlDfuleverandor, 				new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 0, 11), 0, 0));
		this.add(pnlDfu, 						new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 0, 11), 0, 0));
		this.add(pnlVare, 						new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(5, 11, 0, 11), 0, 0));
	}
}
