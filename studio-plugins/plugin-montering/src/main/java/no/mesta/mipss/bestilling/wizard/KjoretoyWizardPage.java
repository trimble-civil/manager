package no.mesta.mipss.bestilling.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import no.mesta.mipss.bestilling.BestillingController;
import no.mesta.mipss.bestilling.KjoretoyPanel;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPanelNavResult;

public class KjoretoyWizardPage extends AbstractBestillingWizardPage {
	public KjoretoyWizardPage(final BestillingController bestillingController) {
		super(bestillingController, 
				BestillingController.getResourceBundle().getString("wizard.step.kjoretoy"), 
				new KjoretoyPanel(bestillingController),
				"kjoretoy.nummerEllerNavn",
				"kjoretoy.kjoretoytype",
				"kjoretoy.kjoretoymodell",
				"kjoretoyUtenUtstyr",
				"kjoretoy.maskinnummer",
				"kjoretoy.regnr",
				"kjoretoy.eksterntNavn");
		this.bestillingController = bestillingController;
		bestillingController.addUtstyrActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doUpdate();
			}
		});
	}

	@Override
	protected void updateNextStatus() {
		List<String> problemStringList = new ArrayList<String>();
		Kjoretoy kjoretoy = bestillingController.getKjoretoy();
		if(!kjoretoy.isComplete()) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.identifikasjon"));
		}
		if(kjoretoy.getKjoretoytype() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.type"));
		} 
		if(kjoretoy.getKjoretoymodell() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.modell"));
		}
		if(!bestillingController.getKjoretoyUtenUtstyr()
				&& kjoretoy.getKjoretoyUtstyrList().isEmpty()) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.utstyr"));
		}
		if (kjoretoy.getRegnr()!=null&&(!"".equals(kjoretoy.getRegnr()))){
			if (!validateRegnr(kjoretoy.getRegnr())){
				problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.regnr"));
			}
		}
		if (kjoretoy.getMaskinnummer()!=null){
			if (!validateMaskinnummer(kjoretoy.getMaskinnummerString())){
				problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.maskinnummer"));
			}
		}
		if (kjoretoy.getRegnr()==null&&kjoretoy.getMaskinnummer()==null){
			String eksterntNavn = kjoretoy.getNummerEllerNavn();
			if (eksterntNavn!=null&&(!"".equals(eksterntNavn))){
				if (validateMaskinnummer(eksterntNavn)){
					problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.eksternt_navn.maskinnummer"));
				}
				if (validateRegnr(eksterntNavn)){
					problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.eksternt_navn.regnr"));
				}
				if (checkForEquals(eksterntNavn)){
					problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.regnrlookalike"));
				}
				if (!validateAnnenId(eksterntNavn)){
					problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kjoretoy.annenId"));
				}
			}
		}
		updateNextStatus(problemStringList);
	}
	public static void main(String[] args) {
		System.out.println(validateAnnenId("NY_BIL_1234"));
	}
	private static boolean validateAnnenId(String annenId){
		Pattern p = Pattern.compile("\\b([A-Z]{4,6})(_)([A-Z]{2})\\d{1}$");
		Pattern a = Pattern.compile("\\b(NY_BIL_)\\d{4,5}");//Pattern for autogenerert annenId
		if (a.matcher(annenId).find())
			return true;
		return p.matcher(annenId).find();
	}
	private static boolean checkForEquals(String eksterntNavn){
		Pattern p = Pattern.compile("\\b(([a-z]{2})(\\s|-|_)|([a-z]{2}))\\d{4,5}$", Pattern.CASE_INSENSITIVE);
		return p.matcher(eksterntNavn).find();
	}
	private static boolean validateMaskinnummer(String maskinNummer){
		Pattern p = Pattern.compile("\\b(\\d{8})$");
		return p.matcher(maskinNummer).find();
	}
	private static boolean validateRegnr(String regnr){
		Pattern p = Pattern.compile("\\b([A-FHJKLNPR-Z]{2}|GA|AW)\\d{4,5}$");
		boolean match = p.matcher(regnr).find();
		return match;
	}
	@Override
	public WizardPanelNavResult allowNext(String stepName, Map settings, Wizard wizard) {
		return bestillingController.tillatKjoretoy();
	}
}
