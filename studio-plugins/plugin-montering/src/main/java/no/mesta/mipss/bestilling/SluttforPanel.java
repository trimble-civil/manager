package no.mesta.mipss.bestilling;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssTableComboBoxEditor;
import no.mesta.mipss.ui.table.MipssTableComboBoxRenderer;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.swingx.JXDatePicker;

public class SluttforPanel extends JPanel {
	private JPanel pnlDatoOgRtcu = new JPanel();
	private JPanel pnlKommentar = new JPanel();
	private JPanel pnlPlassering = new JPanel();
	private JPanel pnlSpenning = new JPanel();
	private JPanel pnlMontor = new JPanel();
	private JPanel pnlUtstyr = new JPanel();
	
	private JLabel lblMontUtfDato = new JLabel();
	private JLabel lblRtcuSN = new JLabel();
	private JLabel lblRtcuTlf = new JLabel();
	private JLabel lblTenning = new JLabel();
	private JLabel lblPluss = new JLabel();
	private JLabel lblJord = new JLabel();
	private JLabel lblMontor = new JLabel();
	private JLabel lblInstSted = new JLabel();
	private JLabel lblDato = new JLabel();
	
	private JXDatePicker dtpMontUtfDato = new JXDatePicker();
	private JXDatePicker dtpDato = new JXDatePicker();
	
	private JFormattedTextField txtRtcuSN = new JFormattedTextField();
	private JFormattedTextField txtRtcuTlf = new JFormattedTextField();
	private JFormattedTextField txtTenning = new JFormattedTextField();
	private JFormattedTextField txtPluss = new JFormattedTextField();
	private JFormattedTextField txtJord = new JFormattedTextField();
	private JFormattedTextField txtMontor = new JFormattedTextField();
	private JFormattedTextField txtInstSted = new JFormattedTextField();
	
	private JTextArea txtAreaKommentar = new JTextArea();
	private JTextArea txtAreaPlassering = new JTextArea();
	
	private JScrollPane scrlPaneUtstyr = new JScrollPane();
	private JScrollPane scrlPaneKommentar = new JScrollPane();
	private JScrollPane scrlPanePlassering = new JScrollPane();
	
	private JButton btnVelgRtcu = new JButton();
	
	private JMipssBeanTable<KjoretoyUtstyrIo> tblUtstyrIo;
	private final BestillingController bestillingController;
	
	public SluttforPanel(final BestillingController bestillingController) {
		this.bestillingController = bestillingController;
		
		initTextFields();
		initTables();
		initButtons();
		initGui();
	}

	private void initButtons() {
		btnVelgRtcu.setAction(bestillingController.getVelgRtcuAction());
	}

	private void initTextFields() {
		txtAreaKommentar.setLineWrap(true);
		txtAreaKommentar.setWrapStyleWord(true);
		txtAreaPlassering.setLineWrap(true);
		txtAreaPlassering.setWrapStyleWord(true);
		
		txtRtcuSN.setEditable(false);
		txtRtcuSN.setFocusable(false);
		txtRtcuTlf.setEditable(false);
		txtRtcuTlf.setFocusable(false);
		
		
		BindingHelper.createbinding(bestillingController, "installasjon.aktivFraDato", dtpMontUtfDato, "date", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.dfuindivid.serienummer", txtRtcuSN, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.dfuindivid.tlfnummerString", txtRtcuTlf, "text", BindingHelper.READ).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.fritekst", txtAreaKommentar, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.plasseringsnotat", txtAreaPlassering, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.spenningTenning", txtTenning, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.spenningPluss", txtPluss, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.spenningJord", txtJord, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.montornavn", txtMontor, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.installasjonsted", txtInstSted, "text", BindingHelper.READ_WRITE).bind();
		BindingHelper.createbinding(bestillingController, "installasjon.monteringsinfo.signeringsdato", dtpDato, "date", BindingHelper.READ_WRITE).bind();
	}
	
	private void initTables() {
		final MipssBeanTableModel<KjoretoyUtstyrIo> tblModelKjoretoyUtstyrIo = 
			new MipssBeanTableModel<KjoretoyUtstyrIo>("kjoretoyUtstyrList", 
					KjoretoyUtstyrIo.class, 
					new int[]{3, 4}, 
					"utstyrgruppe", "utstyrsubgruppe", "utstyrmodell", "sensortype", "ioliste");
		
		BindingHelper.createbinding(bestillingController, "kjoretoyUtstyrIoList", tblModelKjoretoyUtstyrIo, "entities", BindingHelper.READ).bind();
		
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(KjoretoyUtstyrIo.class, tblModelKjoretoyUtstyrIo.getColumns());
		tblUtstyrIo = new JMipssBeanTable<KjoretoyUtstyrIo>(tblModelKjoretoyUtstyrIo, colModel);
		tblUtstyrIo.setFillsViewportWidth(true);
		tblUtstyrIo.setDoWidthHacks(false);
		tblUtstyrIo.setDefaultRenderer(Utstyrmodell.class, new RenderableMipssEntityTableCellRenderer<Utstyrmodell>());
		tblUtstyrIo.setDefaultRenderer(Utstyrgruppe.class, new RenderableMipssEntityTableCellRenderer<Utstyrgruppe>());
		tblUtstyrIo.setDefaultRenderer(Utstyrsubgruppe.class, new RenderableMipssEntityTableCellRenderer<Utstyrsubgruppe>());
		tblUtstyrIo.setDefaultRenderer(Sensortype.class, new MipssTableComboBoxRenderer());
		tblUtstyrIo.setDefaultRenderer(Ioliste.class, new MipssTableComboBoxRenderer());
		tblUtstyrIo.setDefaultEditor(Sensortype.class, new MipssTableComboBoxEditor(bestillingController.fetchSensortypeList()));
		tblUtstyrIo.setDefaultEditor(Ioliste.class, new IOListComboBoxEditor(bestillingController.getIolisteList()));
		
		bestillingController.addPropertyChangeListener("iolisteList", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				tblUtstyrIo.setDefaultEditor(Ioliste.class, new MipssTableComboBoxEditor(bestillingController.getIolisteList()));
				tblModelKjoretoyUtstyrIo.fireTableModelEvent(0, tblModelKjoretoyUtstyrIo.getRowCount(), TableModelEvent.UPDATE);
			}
		});
		
		tblModelKjoretoyUtstyrIo.addTableModelListener(bestillingController.getIOListener(tblModelKjoretoyUtstyrIo, tblUtstyrIo));
	}
	
	private void initGui() {
		scrlPaneKommentar.setViewportView(txtAreaKommentar);
		scrlPanePlassering.setViewportView(txtAreaPlassering);
		scrlPaneUtstyr.setViewportView(tblUtstyrIo);
		
		lblMontUtfDato.setText(BestillingController.getResourceBundle().getString("label.montUtfDato"));
		lblRtcuSN.setText(BestillingController.getResourceBundle().getString("label.rtcuSN"));
		lblRtcuTlf.setText(BestillingController.getResourceBundle().getString("label.rtcuTlf"));
		lblTenning.setText(BestillingController.getResourceBundle().getString("label.tenning"));
		lblPluss.setText(BestillingController.getResourceBundle().getString("label.pluss"));
		lblJord.setText(BestillingController.getResourceBundle().getString("label.jord"));
		lblMontor.setText(BestillingController.getResourceBundle().getString("label.montor"));
		lblInstSted.setText(BestillingController.getResourceBundle().getString("label.inststed"));
		lblDato.setText(BestillingController.getResourceBundle().getString("label.sluttforMontorDato"));
		
		dtpMontUtfDato.setPreferredSize(new Dimension(100, 19));
		dtpDato.setPreferredSize(new Dimension(100, 19));
		txtRtcuSN.setPreferredSize(new Dimension(100, 19));
		txtRtcuTlf.setPreferredSize(new Dimension(100, 19));
		txtTenning.setPreferredSize(new Dimension(100, 19));
		txtPluss.setPreferredSize(new Dimension(100, 19));
		txtJord.setPreferredSize(new Dimension(100, 19));
		txtMontor.setPreferredSize(new Dimension(100, 19));
		txtInstSted.setPreferredSize(new Dimension(100, 19));
		
		scrlPanePlassering.setMinimumSize(new Dimension(100, 50));
		
		this.setLayout(new GridBagLayout());
		pnlDatoOgRtcu.setLayout(new GridBagLayout());
		pnlKommentar.setLayout(new GridBagLayout());
		pnlPlassering.setLayout(new GridBagLayout());
		pnlSpenning.setLayout(new GridBagLayout());
		pnlMontor.setLayout(new GridBagLayout());
		pnlUtstyr.setLayout(new GridBagLayout());
		
		pnlDatoOgRtcu.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.datoOgRtcu")));
		pnlKommentar.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.kommentar")));
		pnlPlassering.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.plassering")));
		pnlSpenning.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.spenning")));
		pnlMontor.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.montor")));
		pnlUtstyr.setBorder(BorderFactory.createTitledBorder(BestillingController.getResourceBundle().getString("border.sluttforUtstyr")));
		
		pnlDatoOgRtcu.add(lblMontUtfDato,		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoOgRtcu.add(dtpMontUtfDato,		new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlDatoOgRtcu.add(lblRtcuSN,			new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoOgRtcu.add(txtRtcuSN,			new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		pnlDatoOgRtcu.add(btnVelgRtcu,			new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlDatoOgRtcu.add(lblRtcuTlf,			new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoOgRtcu.add(txtRtcuTlf,			new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlKommentar.add(scrlPaneKommentar, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlPlassering.add(scrlPanePlassering,	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlSpenning.add(lblTenning, 			new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlSpenning.add(txtTenning,				new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlSpenning.add(lblPluss, 				new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlSpenning.add(txtPluss,				new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlSpenning.add(lblJord, 				new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlSpenning.add(txtJord,				new GridBagConstraints(5, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlMontor.add(lblMontor, 				new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlMontor.add(txtMontor, 				new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlMontor.add(lblInstSted, 				new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMontor.add(txtInstSted, 				new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 11), 0, 0));
		pnlMontor.add(lblDato, 					new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMontor.add(dtpDato, 					new GridBagConstraints(5, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
		
		pnlUtstyr.add(scrlPaneUtstyr, 			new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		this.add(pnlDatoOgRtcu, 				new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 5, 0), 0, 0));
		this.add(pnlKommentar, 					new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 5, 5, 11), 0, 0));
		this.add(pnlPlassering, 				new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 11, 5, 11), 0, 0));
		this.add(pnlSpenning, 					new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 11, 5, 11), 0, 0));
		this.add(pnlMontor, 					new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 5, 11), 0, 0));
		this.add(pnlUtstyr, 					new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 11, 0, 11), 0, 0));
	}
	
	private class IOListComboBoxEditor extends MipssTableComboBoxEditor {

		private final List<Ioliste> entityList;

		public IOListComboBoxEditor(List<Ioliste> list){
			super(list);
			this.entityList = list;	
		}
		
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			JComboBox comboBox = new JComboBox();
			comboBox = (JComboBox)super.getTableCellEditorComponent(table, value, isSelected, row, column);
			comboBox.removeAllItems();

			Utstyrgruppe gruppe = (Utstyrgruppe) table.getModel().getValueAt(row, 0);
			Utstyrsubgruppe subgruppe = (Utstyrsubgruppe) table.getModel().getValueAt(row, 1);
			Utstyrmodell modell = (Utstyrmodell) table.getModel().getValueAt(row, 2);

			for (Ioliste io : entityList) {
				if (	(modell != null && modell.getSerieportFlagg() != null && modell.getSerieportFlagg()) ||
						(subgruppe != null && subgruppe.getSerieportFlagg() != null && subgruppe.getSerieportFlagg()) ||
						(gruppe != null && gruppe.getSerieportFlagg() !=null && gruppe.getSerieportFlagg())) {

					if (io.getId() == 1 || io.getId() == 2) {
						comboBox.addItem(io);
					}

				} else{
					if (io.getId() != 1 && io.getId() != 2) {
						comboBox.addItem(io);
					}
				}
			}

			comboBox.setSelectedItem(value);
			
			comboBox.setRenderer(new MipssListCellRenderer<IRenderableMipssEntity>());
			return comboBox;
		}
	}
}
