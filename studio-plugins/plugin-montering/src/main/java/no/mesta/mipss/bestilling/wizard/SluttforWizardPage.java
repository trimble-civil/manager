package no.mesta.mipss.bestilling.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.bestilling.BestillingController;
import no.mesta.mipss.bestilling.SluttforPanel;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;

public class SluttforWizardPage extends AbstractBestillingWizardPage {
	public SluttforWizardPage(final BestillingController bestillingController) {
		super(bestillingController, 
				BestillingController.getResourceBundle().getString("wizard.step.sluttfor"), 
				new SluttforPanel(bestillingController),
				"installasjon.aktivFraDato",
				"installasjon.dfuindivid");
		bestillingController.addUtstyrActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doUpdate();
			}
		});
	}

	@Override
	protected void updateNextStatus() {
		List<String> problemStringList = new ArrayList<String>();
		
		if(bestillingController.getInstallasjon().getAktivFraDato() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.sluttfor.monteringUtfDato"));
		}
		if(bestillingController.getInstallasjon().getDfuindivid() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.sluttfor.dfu"));
		} 
		for(KjoretoyUtstyrIo kjoretoyUtstyrIo : bestillingController.getKjoretoyUtstyrIoList()) {
			if(kjoretoyUtstyrIo.getSensortype() == null || kjoretoyUtstyrIo.getIoliste() == null) {
				problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.sluttfor.io"));
				break;
			}
		}
		
		updateNextStatus(problemStringList);
	}
}
