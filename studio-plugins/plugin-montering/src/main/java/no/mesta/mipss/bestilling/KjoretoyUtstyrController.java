package no.mesta.mipss.bestilling;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrgruppe;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrmodell;
import no.mesta.mipss.persistence.kjoretoyutstyr.Utstyrsubgruppe;


public class KjoretoyUtstyrController implements PropertyChangeSource {
	private final PropertyChangeSupport props = new PropertyChangeSupport(this); 
	private final MipssKjoretoy mipssKjoretoy;

	private Utstyrgruppe utstyrgruppe;
	private Utstyrsubgruppe utstyrsubgruppe;
	private Utstyrmodell utstyrmodell;
	
	private List<Utstyrgruppe> utstyrgruppeList;
	private List<Utstyrsubgruppe> utstyrsubgruppeList;
	private List<Utstyrmodell> utstyrmodellList;
	
	public KjoretoyUtstyrController(final MipssKjoretoy mipssKjoretoy) {
		this.mipssKjoretoy = mipssKjoretoy;
		
		setUtstyrsubgruppeList(new ArrayList<Utstyrsubgruppe>());
		setUtstyrmodellList(new ArrayList<Utstyrmodell>());
		setUtstyrgruppeList(fetchUtstyrgruppeList());
		
		initListeners();
	}
	
	private void initListeners() {
		this.addPropertyChangeListener("utstyrgruppe", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				final List<Utstyrsubgruppe> newList;
				if(utstyrgruppe != null) {
					newList = fetchUtstyrsubgruppeList(utstyrgruppe);
				} else {
					newList = new ArrayList<Utstyrsubgruppe>();
				}
				setUtstyrsubgruppeList(newList);
			}
		});
		this.addPropertyChangeListener("utstyrsubgruppe", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				final List<Utstyrmodell> newList;
				if(utstyrsubgruppe != null) {
					newList = fetchUtstyrmodellList(utstyrsubgruppe);
				} else {
					newList = new ArrayList<Utstyrmodell>();
				}
				setUtstyrmodellList(newList);
			}
		});
		this.addPropertyChangeListener("utstyrsubgruppeList", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				setUtstyrsubgruppe(null);
			}
		});
		this.addPropertyChangeListener("utstyrmodellList", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				setUtstyrmodell(null);
			}
		});
	}

	public Utstyrgruppe getUtstyrgruppe() {
		return utstyrgruppe;
	}

	public void setUtstyrgruppe(Utstyrgruppe utstyrgruppe) {
		Object old = this.utstyrgruppe;
		Object oldComplete = getComplete();
		this.utstyrgruppe = utstyrgruppe;
		props.firePropertyChange("utstyrgruppe", old, utstyrgruppe);
		props.firePropertyChange("complete", oldComplete, getComplete());
	}

	public Utstyrsubgruppe getUtstyrsubgruppe() {
		return utstyrsubgruppe;
	}

	public void setUtstyrsubgruppe(Utstyrsubgruppe utstyrsubgruppe) {
		Object old = this.utstyrsubgruppe;
		Object oldComplete = getComplete();
		this.utstyrsubgruppe = utstyrsubgruppe;
		props.firePropertyChange("utstyrsubgruppe", old, utstyrsubgruppe);
		props.firePropertyChange("complete", oldComplete, getComplete());
	}

	public Utstyrmodell getUtstyrmodell() {
		return utstyrmodell;
	}

	public void setUtstyrmodell(Utstyrmodell utstyrmodell) {
		Object old = this.utstyrmodell;
		Object oldComplete = getComplete();
		this.utstyrmodell = utstyrmodell;
		props.firePropertyChange("utstyrmodell", old, utstyrmodell);
		props.firePropertyChange("complete", oldComplete, getComplete());
	}

	public List<Utstyrgruppe> getUtstyrgruppeList() {
		return utstyrgruppeList;
	}

	public void setUtstyrgruppeList(List<Utstyrgruppe> utstyrgruppeList) {
		Object old = this.utstyrgruppeList;
		Object oldEnabled = getUtstyrgruppeEnabled();
		this.utstyrgruppeList = utstyrgruppeList;
		props.firePropertyChange("utstyrgruppeList", old, utstyrgruppeList);
		props.firePropertyChange("utstyrgruppeEnabled", oldEnabled, getUtstyrgruppeEnabled());
	}

	public List<Utstyrsubgruppe> getUtstyrsubgruppeList() {
		return utstyrsubgruppeList;
	}

	public void setUtstyrsubgruppeList(List<Utstyrsubgruppe> utstyrsubgruppeList) {
		Object old = this.utstyrsubgruppeList;
		Object oldEnabled = getUtstyrsubgruppeEnabled();
		this.utstyrsubgruppeList = utstyrsubgruppeList;
		props.firePropertyChange("utstyrsubgruppeList", old, utstyrsubgruppeList);
		props.firePropertyChange("utstyrsubgruppeEnabled", oldEnabled, getUtstyrsubgruppeEnabled());
	}

	public List<Utstyrmodell> getUtstyrmodellList() {
		return utstyrmodellList;
	}

	public void setUtstyrmodellList(List<Utstyrmodell> utstyrmodellList) {
		Object old = this.utstyrmodellList;
		Object oldEnabled = getUtstyrmodellEnabled();
		this.utstyrmodellList = utstyrmodellList;
		props.firePropertyChange("utstyrmodellList", old, utstyrmodellList);
		props.firePropertyChange("utstyrmodellEnabled", oldEnabled, getUtstyrmodellEnabled());
	}

	public boolean getUtstyrgruppeEnabled() {
		return utstyrgruppeList != null && utstyrgruppeList.size() > 0;
	}
	
	public boolean getUtstyrsubgruppeEnabled() {
		return utstyrsubgruppeList != null && utstyrsubgruppeList.size() > 0;
	}
	
	public boolean getUtstyrmodellEnabled() {
		return utstyrmodellList != null && utstyrmodellList.size() > 0;
	}
	
	public boolean getComplete() {
		if(utstyrgruppe == null) {
			return false;
		} else {
			if(!utstyrgruppe.getKreverSubgrFlagg()) {
				return true;
			} else {
				if(utstyrsubgruppe == null) {
					return false;
				} else {
					if(!utstyrsubgruppe.getKreverModellFlagg()) {
						return true;
					} else {
						return utstyrmodell != null;						
					}
				}
			}
		}
	}
	
	private List<Utstyrgruppe> fetchUtstyrgruppeList() {
		List<Utstyrgruppe> utstyrgruppeList = mipssKjoretoy.getUtstyrgruppeList();
		Collections.sort(utstyrgruppeList);
		return utstyrgruppeList;
	}
	
	private List<Utstyrsubgruppe> fetchUtstyrsubgruppeList(final Utstyrgruppe utstyrgruppe) {
		List<Utstyrsubgruppe> utstyrsubgruppeListFor = mipssKjoretoy.getUtstyrsubgruppeListFor(utstyrgruppe.getId());
		Collections.sort(utstyrsubgruppeListFor);
		return utstyrsubgruppeListFor;
	}
	
	private List<Utstyrmodell> fetchUtstyrmodellList(final Utstyrsubgruppe utstyrsubgruppe) {
		List<Utstyrmodell> utstyrmodellListFor = mipssKjoretoy.getUtstyrmodellListFor(utstyrsubgruppe.getId());
		Collections.sort(utstyrmodellListFor);
		return utstyrmodellListFor;
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
}
