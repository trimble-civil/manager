package no.mesta.mipss.bestilling.wizard;

import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.bestilling.BestillingController;
import no.mesta.mipss.bestilling.KontraktPanel;

public class KontraktWizardPage extends AbstractBestillingWizardPage {
	public KontraktWizardPage(final BestillingController bestillingController) {
		super(bestillingController, 
				BestillingController.getResourceBundle().getString("wizard.step.kontrakt"), 
				new KontraktPanel(bestillingController),
				"kjoretoy.kjoretoyeier.textForGUI",
				"kjoretoy.leverandor.textForGUI",
				"kjoretoyordre.kontaktnavn",
				"kjoretoyordre.kontakttelefon");
	}
	
	@Override
	protected void updateNextStatus() {
		List<String> problemStringList = new ArrayList<String>();
		
		if(bestillingController.getDriftkontrakt().getDriftsleder() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kontrakt.driftsleder"));
		} 
		if(bestillingController.getKjoretoyordre().getProsjektNr() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kontrakt.prosjektnummer"));
		}
		if(bestillingController.getKjoretoyordre().getKontaktnavn() == null 
				|| bestillingController.getKjoretoyordre().getKontaktnavn().length() == 0) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kontrakt.kontaktperson"));
		} 
		if(bestillingController.getKjoretoyordre().getKontakttelefon() == null
				|| bestillingController.getKjoretoyordre().getKontakttelefon() == 0) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kontrakt.telefonnummer"));
		} 
		if(bestillingController.getKjoretoy().getKjoretoyeier() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kontrakt.eier"));
		} 
		if(bestillingController.getKjoretoy().getLeverandor() == null) {
			problemStringList.add(BestillingController.getResourceBundle().getString("wizard.warning.kontrakt.leverandor"));
		}
		
		updateNextStatus(problemStringList);
    }
	
	public void setNotify(Object o) {
		updateNextStatus();
	}
}
