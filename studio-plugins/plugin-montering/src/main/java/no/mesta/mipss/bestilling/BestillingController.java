package no.mesta.mipss.bestilling;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.kjoretoy.KjoretoyQueryFilter;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.DfuInfoV;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.persistence.datafangsutstyr.Installasjon;
import no.mesta.mipss.persistence.datafangsutstyr.Ioliste;
import no.mesta.mipss.persistence.datafangsutstyr.Monteringsinfo;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestillingmaster;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfubestvarelinjer;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.DfulevBestilling;
import no.mesta.mipss.persistence.datafangsutstyrbestilling.Dfuvarekatalog;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoyeier;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoymodell;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoytype;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.KjoretoyordreUtstyr;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre_Status;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyr;
import no.mesta.mipss.persistence.kjoretoyutstyr.KjoretoyUtstyrIo;
import no.mesta.mipss.persistence.kjoretoyutstyr.Sensortype;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.KjoretoyLeggTilEierPanel;
import no.mesta.mipss.ui.KjoretoyVelgLeverandorPanel;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.util.EpostUtils;

import org.netbeans.spi.wizard.WizardPanelNavResult;

public class BestillingController implements PropertyChangeSource {
	private final PropertyChangeSupport props = new PropertyChangeSupport(this);
	private static final PropertyResourceBundleUtil resourceBundle = PropertyResourceBundleUtil.getPropertyBundle("monteringText");;
	private List<ActionListener> utstyrOgIoActionListeners = new ArrayList<ActionListener>();
	
	private Kjoretoyordre kjoretoyordre;
	private Kjoretoy kjoretoy;
	private Driftkontrakt driftkontrakt;
	private String loggedOnUser;
	private Dfubestillingmaster dfubestillingmaster;
	private DfulevBestilling dfulevBestilling;
	private Installasjon installasjon;
	private List<KjoretoyUtstyrIo> kjoretoyUtstyrIoList = new ArrayList<KjoretoyUtstyrIo>();
	private List<KjoretoyUtstyr> kjoretoyUtstyrList = new ArrayList<KjoretoyUtstyr>();
	private Boolean kjoretoyUtenUtstyr = Boolean.FALSE;
	private Boolean kjoretoyMaaHaBryterboks = Boolean.FALSE;
	private Boolean kjoretoyNyopprettet = Boolean.FALSE;
	
	private final BestillingSkjemaType bestillingSkjemaType;
	
	private MipssKjoretoy mipssKjoretoy;
	private MaintenanceContract maintenanceContract;
	
	private Kjoretoyordrestatus statusNyopprettet;
	private Kjoretoyordrestatus statusOverlevertKoord;
	private Kjoretoyordrestatus statusUnderArbeidKoord;
	private Kjoretoyordrestatus statusFullfortKoord;
	private Kjoretoyordrestatus statusUnderArbeidLev;
	private Kjoretoyordrestatus statusFullfortKontrakt;
	private Kjoretoyordretype typeNymontering;
	private String hardkodetDfuleverandor; //Etter en diskusjon har vi bestemt at denne inntil videre hardkodes
	private final JDialog parentDialog;
	private final Long kjoretoyOrdreId;
	
	public BestillingController(final BestillingSkjemaType bestillingSkjemaType, final Driftkontrakt driftkontrakt,
			final Long kjoretoyOrdreId, final String loggedOnUser, JDialog parent) {
//		if( (!bestillingSkjemaType.equals(BestillingSkjemaType.OPPRETTING) || 
//			!bestillingSkjemaType.equals(BestillingSkjemaType.ENDRING)) && 
//			kjoretoyOrdreId == null) {
//			
//			throw new IllegalArgumentException("Mangler kjoretoyOrdreId");
//		}
		this.bestillingSkjemaType = bestillingSkjemaType;
		this.driftkontrakt = driftkontrakt;
		this.kjoretoyOrdreId = kjoretoyOrdreId;
		this.loggedOnUser = loggedOnUser;
		this.parentDialog = parent;
		mipssKjoretoy = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
		maintenanceContract = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
		init();
	}

	private void init() {
		initGrunndata();
		
		switch (bestillingSkjemaType) {
		case OPPRETTING:
			initNyttKjoretoy();
			initNyKjoretoyOrdre();
			break;
		case ENDRING:
			hentEksisterendeKjoretoyOgOrdre();
			hentKjoretoyUtstyr();
			break;
		case ADMIN_BEHANDLING:
			hentEksisterendeKjoretoyOgOrdre();
			hentKjoretoyUtstyr();
			break;
		case KONTRAKT_BEHANDLING:
			hentEksisterendeKjoretoyOgOrdre();
			hentKjoretoyUtstyr();
			opprettInstallasjonOgKjoretoyUtstyrIo();
			break;
		case RAPPORTUTTAK:
			hentEksisterendeKjoretoyOgOrdre();
			hentKjoretoyUtstyr();
			hentInstallasjonOgKjoretoyUtstyrIo();
			break;
		}
		
		initListeners();
	}

	private void initGrunndata() {
		KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
    	String oppettetStatusIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusOpprettet").getVerdi();
    	String statusOverlevertKoordIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusOverlevertKoord").getVerdi();
    	String statusUnderArbeidKoordIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusUnderArbeidKoord").getVerdi();
    	String statusUnderArbeidLevIndent = params.hentEnForApp("studio.mipss.montering", "ordrestatusUnderArbeidLev").getVerdi();
    	String behandletAdminStatusIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortKoord").getVerdi();
    	String ferdigstiltFraKontraktStatusIdent = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortKontrakt").getVerdi();
    	
    	List<Kjoretoyordrestatus> kjoretoyordrestatusList = fetchKjoretoyordrestatusList();
		for(Kjoretoyordrestatus kjoretoyordrestatus: kjoretoyordrestatusList) {
			if(kjoretoyordrestatus.getIdent().equals(oppettetStatusIdent)) {
				statusNyopprettet = kjoretoyordrestatus;
			}
			if(kjoretoyordrestatus.getIdent().equals(statusOverlevertKoordIdent)) {
				statusOverlevertKoord = kjoretoyordrestatus;
			}
			if(kjoretoyordrestatus.getIdent().equals(statusUnderArbeidKoordIdent)) {
				statusUnderArbeidKoord = kjoretoyordrestatus;
			}
			if(kjoretoyordrestatus.getIdent().equals(behandletAdminStatusIdent)) {
				statusFullfortKoord = kjoretoyordrestatus;
			}
			if (kjoretoyordrestatus.getIdent().equals(statusUnderArbeidLevIndent)){
				statusUnderArbeidLev = kjoretoyordrestatus;
			}
			if(kjoretoyordrestatus.getIdent().equals(ferdigstiltFraKontraktStatusIdent)) {
				statusFullfortKontrakt = kjoretoyordrestatus;
			}
		}
		
    	String nymonteringTypeNavn = params.hentEnForApp("studio.mipss.montering", "kjoretoyordretypeNymontering").getVerdi();
		List<Kjoretoyordretype> kjoretoyordretypeList = fetchKjoretoyordretypeList();
		for(Kjoretoyordretype kjoretoyordretype : kjoretoyordretypeList) {
			if(kjoretoyordretype.getNavn().equals(nymonteringTypeNavn)) {
				typeNymontering = kjoretoyordretype;
				break;
			}
		}
		
		hardkodetDfuleverandor = params.hentEnForApp("studio.mipss.montering", "standardDfuleverandorNavn").getVerdi();
	}

	private void initNyKjoretoyOrdre() {
		kjoretoyordre = new Kjoretoyordre();
		kjoretoyordre.getOwnedMipssEntity().setOpprettetAv(getLoggedOnUser());
		kjoretoyordre.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		kjoretoyordre.setKjoretoyordretype(typeNymontering);
		kjoretoyordre.setSignatur(getLoggedOnUser());
		kjoretoyordre.setVerkstedAdminFlagg(false);
		kjoretoyordre.setDriftkontrakt(driftkontrakt);
		if(getDriftkontrakt() != null && getDriftkontrakt().getStandardKontraktProsjekt() != null) {
			kjoretoyordre.setProsjektNr(getDriftkontrakt().getStandardKontraktProsjekt().getProsjektNr());
		}
		
		dfubestillingmaster = new Dfubestillingmaster();
		dfubestillingmaster.setSignatur(getLoggedOnUser());
		dfubestillingmaster.setOpprettetAv(getLoggedOnUser());
		dfubestillingmaster.setOpprettetDato(Clock.now());
		dfubestillingmaster.setKjoretoyordre(kjoretoyordre);
		kjoretoyordre.getDfubestillingmasterList().add(dfubestillingmaster);
		
		dfulevBestilling = new DfulevBestilling();
		dfulevBestilling.setOpprettetAv(getLoggedOnUser());
		dfulevBestilling.setOpprettetDato(Clock.now());
		dfulevBestilling.setDfubestillingmaster(dfubestillingmaster);
		dfulevBestilling.setLeverandorNavn(hardkodetDfuleverandor);
		dfubestillingmaster.getDfulevBestillingList().add(dfulevBestilling);
	}

	private void leggTilKjoretoyordreStatus(Kjoretoyordrestatus kjoretoyordrestatus) {
		Kjoretoyordre_Status kjoretoyordreStatus = new Kjoretoyordre_Status();
		kjoretoyordreStatus.getOwnedMipssEntity().setOpprettetAv(getLoggedOnUser());
		kjoretoyordreStatus.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		kjoretoyordreStatus.setKjoretoyordre(kjoretoyordre);
		kjoretoyordreStatus.setKjoretoyordrestatus(kjoretoyordrestatus);
		kjoretoyordre.getKjoretoyordre_StatusList().add(kjoretoyordreStatus);
	}

	private void initNyttKjoretoy() {
		kjoretoy = new Kjoretoy();
		kjoretoy.getOwnedMipssEntity().setOpprettetAv(getLoggedOnUser());
		kjoretoy.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		kjoretoy.setSlettetFlagg(0L);
		setKjoretoyNyopprettet(Boolean.TRUE);
	}
	
	private void hentEksisterendeKjoretoyOgOrdre() {
		this.kjoretoyordre = mipssKjoretoy.getKjoretoyordreForAdmin(kjoretoyOrdreId);
		this.kjoretoy = maintenanceContract.getKjoretoy(this.kjoretoyordre.getKjoretoy().getId());
		this.dfubestillingmaster = kjoretoyordre.getDfubestillingmasterForBestilling();
		this.dfulevBestilling = dfubestillingmaster.getDfuLDfulevBestillingForBestilling();
		int indexOf = this.kjoretoy.getKjoretoyordreList().indexOf(this.kjoretoyordre);
		this.kjoretoy.getKjoretoyordreList().set(indexOf, this.kjoretoyordre);
		this.driftkontrakt = kjoretoyordre.getDriftkontrakt();
		
		setKjoretoyNyopprettet(Boolean.FALSE);
	}
	
	private void opprettInstallasjonOgKjoretoyUtstyrIo() {
		installasjon = new Installasjon();
		installasjon.setMonteringsinfo(new Monteringsinfo());
		installasjon.setOpprettetAv(getLoggedOnUser());
		installasjon.setOpprettetDato(Clock.now());
		installasjon.setDfuindivid(kjoretoyordre.getDfuindivid());
		
		for(KjoretoyordreUtstyr kjoretoyordreUtstyr : kjoretoyordre.getKjoretoyordreUtstyrList()) {
    		lagNyKjoretoyUtstyrIoForKjoretoyordreUtstyr(kjoretoyordreUtstyr);
		}
	}
	
	private void hentKjoretoyUtstyr() {
		for(KjoretoyUtstyr ku : kjoretoy.getKjoretoyUtstyrList()) {
			for(KjoretoyordreUtstyr tmpKou : kjoretoyordre.getKjoretoyordreUtstyrList()) {
				if(tmpKou.getUtstyrId().equals(ku.getId())) {
					getKjoretoyUtstyrList().add(ku);
					break;
				}
			}
		}
	}
	
	/**
	 * Antar at KjorertoyUtstyr er hentet
	 */
	private void hentInstallasjonOgKjoretoyUtstyrIo() {
		if(kjoretoyordre.getInstallasjon() != null) {
			final Long installasjonId = kjoretoyordre.getInstallasjon().getId();
			for(Installasjon ins : kjoretoy.getInstallasjonList()) {
				if(ins.getId().equals(installasjonId)) {
					setInstallasjon(ins);
					break;
				}
			}
		}
		
		for(KjoretoyUtstyr ku : getKjoretoyUtstyrList()) {
			KjoretoyordreUtstyr kou = null;
			for(KjoretoyordreUtstyr tmpKou : kjoretoyordre.getKjoretoyordreUtstyrList()) {
				if(tmpKou.getUtstyrId().equals(ku.getId())) {
					kou = tmpKou;
					break;
				}
			}
			
			if(kou != null) {
				for(KjoretoyUtstyrIo tmpKuio : ku.getKjoretoyUtstyrIoList()) {
					if(tmpKuio.getId().equals(kou.getUtstyrIoId())) {
						getKjoretoyUtstyrIoList().add(tmpKuio);
					}
				}
			}
		}
	}
	
	private void initListeners() {
		BindingHelper.createbinding(this, "installasjon.aktivFraDato", this, "notifyInstallasjonAktivFraDato", BindingHelper.READ).bind();
		BindingHelper.createbinding(this, "installasjon.dfumodell", this, "notifyDfumodell", BindingHelper.READ).bind();
	}
	
	public void setNotifyInstallasjonAktivFraDato(Date dato) {
		monteringsdatoOppdatert(dato);
	}
	
	public void setNotifyDfumodell(Dfumodell dfumodell) {
		dfumodellEndret(dfumodell);
	}
	
	public Action getVelgLeverandorAction() {
		return new AbstractAction(getResourceBundle().getString("button.sokLeverandor"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<LevStedView> leggTilLeverandorDialog = 
					new LeggTilRelasjonDialog<LevStedView>(parentDialog, new KjoretoyVelgLeverandorPanel(maintenanceContract));
				
				LevStedView levStedView = leggTilLeverandorDialog.showDialog();
				if(levStedView != null) {
					Leverandor leverandor = maintenanceContract.opprettLeverandor(levStedView.getLevNr());
					if(leverandor != null) {
						kjoretoy.setLeverandor(leverandor);
					}
				}
			}
		};
	}
	
	public Action getVelgEierAction() {
		return new AbstractAction(getResourceBundle().getString("button.sokEier"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<Kjoretoyeier> leggTilRelasjonDialog = 
					new LeggTilRelasjonDialog<Kjoretoyeier>(parentDialog, new KjoretoyLeggTilEierPanel(getKjoretoy(), mipssKjoretoy));
            	
            	Kjoretoyeier kjoretoyeier = leggTilRelasjonDialog.showDialog();
            	
            	if(kjoretoyeier != null) {
            		getKjoretoy().setKjoretoyeier(kjoretoyeier);
            	}
			}
		};
	}
	
	public Action getNyttKjoretoyUtstyrAction(final MipssBeanTableModel<KjoretoyUtstyr> tableModel) {
		return new AbstractAction(getResourceBundle().getString("button.nyttUtstyr"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<KjoretoyUtstyr> leggTilRelasjonDialog = 
					new LeggTilRelasjonDialog<KjoretoyUtstyr>(parentDialog, 
							new LeggTilKjoretoyUtstyrPanel(new KjoretoyUtstyrController(mipssKjoretoy)));
            	
				KjoretoyUtstyr kjoretoyUtstyr = leggTilRelasjonDialog.showDialog();
            	
            	if(kjoretoyUtstyr != null) {
            		kjoretoyUtstyr.getOwnedMipssEntity().setOpprettetDato(Clock.now());
            		kjoretoyUtstyr.getOwnedMipssEntity().setOpprettetAv(loggedOnUser);
            		kjoretoy.addKjoretoyUtstyr(kjoretoyUtstyr);
            		getKjoretoyUtstyrList().add(kjoretoyUtstyr);
            		fireUtstyrOgIoAction();
            		props.firePropertyChange("kjoretoyUtstyrIoList", null, getKjoretoyUtstyrIoList());
            		tableModel.fireTableModelEvent(0, tableModel.getRowCount(), TableModelEvent.UPDATE);
            	}
			}
		};
	}
	
	public Action getSlettKjoretoyUtstyrAction(final JMipssBeanTable<KjoretoyUtstyr> table, 
			final MipssBeanTableModel<KjoretoyUtstyr> tableModel) {
		return new AbstractAction(getResourceBundle().getString("button.slettUtstyr"), IconResources.REMOVE_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<KjoretoyUtstyr> remList = new ArrayList<KjoretoyUtstyr>();
				int[] selectedRows = table.getSelectedRows();
				for(int i=0; i<selectedRows.length; i++) {
					int selectedRow = selectedRows[i];
					int convertedRowIndex = table.convertRowIndexToModel(selectedRow);
					KjoretoyUtstyr kjoretoyUtstyr = tableModel.get(convertedRowIndex);
					remList.add(kjoretoyUtstyr);

					if(kjoretoyUtstyr.getId() == null) {
						kjoretoy.getKjoretoyUtstyrList().remove(kjoretoyUtstyr);
					}
				}
				tableModel.getEntities().removeAll(remList);
				tableModel.fireTableModelEvent(0, tableModel.getRowCount(), TableModelEvent.UPDATE);
				fireUtstyrOgIoAction();
				props.firePropertyChange("kjoretoyUtstyrIoList", null, getKjoretoyUtstyrIoList());
			}
		};
	}
	
	public Action getLeggTilVareAction(final MipssBeanTableModel<Dfuvarekatalog> tblModelDfuvarekatalog,
			final JMipssBeanTable<Dfuvarekatalog> table, final Action refreshTableAction) {
		return new AbstractAction(getResourceBundle().getString("button.leggTilVare"), IconResources.ADD_ONE_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = table.getSelectedRows();
				for(int i=0; i<selectedRows.length; i++) {
					int selectedRow = selectedRows[i];
					int convertedRowIndex = table.convertRowIndexToModel(selectedRow);
					Dfuvarekatalog dfuvarekatalog = tblModelDfuvarekatalog.get(convertedRowIndex);
					addDfuvarekatalog(dfuvarekatalog, 1L);
				}
				refreshTableAction.actionPerformed(null);
			}
		};
	}
	
	public Action getFjernVareAction(final MipssBeanTableModel<Dfubestvarelinjer> tblModelDfubestvarelinjer,
			final JMipssBeanTable<Dfubestvarelinjer> table, final Action refreshTableAction) {
		return new AbstractAction(getResourceBundle().getString("button.fjernVare"), IconResources.REMOVE_ONE_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRowCount() > 0) {
					List<Dfubestvarelinjer> remList = new ArrayList<Dfubestvarelinjer>();
					int[] selectedRows = table.getSelectedRows();
					for(int i=0; i<selectedRows.length; i++) {
						int selectedRow = selectedRows[i];
						int convertedRowIndex = table.convertRowIndexToModel(selectedRow);
						Dfubestvarelinjer dfubestvarelinjer = tblModelDfubestvarelinjer.get(convertedRowIndex);
						remList.add(dfubestvarelinjer);
					}
					dfulevBestilling.getDfubestvarelinjerList().removeAll(remList);
					refreshTableAction.actionPerformed(null);
				}
			}
		};
	}

	public TableModelListener getIOListener(final MipssBeanTableModel<KjoretoyUtstyrIo> tableModel, final JMipssBeanTable<KjoretoyUtstyrIo> table) {
		return new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				if(e.getType() == TableModelEvent.UPDATE && tableModel.getRowCount() > 0) {
					//Antar at kun en rad er endret (firstRow)
					if(e.getFirstRow() != e.getLastRow()) {
						return;
					}
					
					int firstRow = e.getFirstRow();
					int convertedRowIndex = table.convertRowIndexToModel(firstRow);
					final KjoretoyUtstyrIo endretKjoretoyUtstyrIo = tableModel.get(convertedRowIndex);
					for(int i=0; i<tableModel.getRowCount(); i++) {
						if(i == convertedRowIndex) continue;
						final KjoretoyUtstyrIo kjoretoyUtstyrIo = tableModel.get(i);
						if(kjoretoyUtstyrIo.getIoliste() != null 
								&& kjoretoyUtstyrIo.getIoliste().equals(endretKjoretoyUtstyrIo.getIoliste())) {
							kjoretoyUtstyrIo.setIoliste(null);
						}
					}
										
					fireUtstyrOgIoAction();
					tableModel.fireTableModelEvent(0, tableModel.getRowCount(), TableModelEvent.UPDATE);
					
					if (endretKjoretoyUtstyrIo.getIoliste() != null && endretKjoretoyUtstyrIo.getIoliste().getId() == 2) {
						JOptionPane.showMessageDialog(table, getResourceBundle().getString("message.serieportWarning"), 
								getResourceBundle().getString("message.serieportWarning.title"), JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		};
	}
	
	public Action getVelgRtcuAction() {
		return new AbstractAction(getResourceBundle().getString("button.sokRtcu"), IconResources.SEARCH_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilRelasjonDialog<DfuInfoV> leggTilRelasjonDialog = 
					new LeggTilRelasjonDialog<DfuInfoV>(parentDialog, new VelgDfuInfoVPanel(mipssKjoretoy, hardkodetDfuleverandor, true));
            	
				DfuInfoV dfuInfoV = leggTilRelasjonDialog.showDialog();
            	
				
            	if(dfuInfoV != null) {
            		if (kjoretoyordre.getDfuindivid()!=null){
            			if (!kjoretoyordre.getDfuindivid().getId().equals(dfuInfoV.getId())){
            				//hvis det er valgt en annen dfu enn den som er forhåndsdefinert må bruker ta stilling til
            				//om dette skal gjøres.
            				int svar = JOptionPane.showConfirmDialog(parentDialog, 
        							getResourceBundle().getGuiString("message.ikkeSammeDfuSomFraOrdre", EpostUtils.getSupportEpostAdresse()), 
        							getResourceBundle().getString("message.ikkeSammeDfuSomFraOrdre.title"), 
        							JOptionPane.YES_NO_OPTION);
            				if (svar!=JOptionPane.YES_OPTION){
            					return;
            				}
            			}
    				}
            		final Dfuindivid dfuindivid = mipssKjoretoy.getDfuindivid(dfuInfoV.getId());
            		getInstallasjon().setDfuindivid(dfuindivid);
            	}
			}
		};
	}
	
	private void addDfuvarekatalog(Dfuvarekatalog dfuvarekatalog, Long antall) {
		for(final Dfubestvarelinjer dfubestvarelinjer : dfulevBestilling.getDfubestvarelinjerList()) {
			if(dfubestvarelinjer.getDfuvarekatalog().equals(dfuvarekatalog)) {
				dfubestvarelinjer.addToAntall(antall);
				return;
			}
		}
		
		Dfubestvarelinjer dfubestvarelinjer = new Dfubestvarelinjer();
		dfubestvarelinjer.setDfulevBestilling(dfulevBestilling);
		dfubestvarelinjer.setDfuvarekatalog(dfuvarekatalog);
		dfubestvarelinjer.addToAntall(antall);
		dfulevBestilling.getDfubestvarelinjerList().add(dfubestvarelinjer);
	}
	
	private void monteringsdatoOppdatert(Date dato) {
		for(KjoretoyUtstyrIo kjoretoyUtstyrIo : getKjoretoyUtstyrIoList()) {
			kjoretoyUtstyrIo.setFraDato(dato);
		}
		//Dette kan endre listen
		props.firePropertyChange("kjoretoyUtstyrIoList", null, getKjoretoyUtstyrIoList());
	}
	
	private void dfumodellEndret(Dfumodell dfumodell) {
		for(KjoretoyUtstyrIo kjoretoyUtstyrIo : getKjoretoyUtstyrIoList()) {
			kjoretoyUtstyrIo.setIoliste(null);
		}
		//Dette kan endre listen
		props.firePropertyChange("kjoretoyUtstyrIoList", null, getKjoretoyUtstyrIoList());
		props.firePropertyChange("iolisteList", null, getIolisteList());
	}
	
	private List<Kjoretoyordrestatus> fetchKjoretoyordrestatusList() {
		return maintenanceContract.getKjoretoyordrestatusList();
	}
	
	private List<Kjoretoyordretype> fetchKjoretoyordretypeList() {
		return maintenanceContract.getKjoretoyordretypeValidList();
	}
	
	public String getTotalVarePrisString() {
		Double d = 0d;
		for(Dfubestvarelinjer dfubestvarelinjer : dfulevBestilling.getDfubestvarelinjerList()) {
			d += dfubestvarelinjer.getTotalPris();
		}
		
		NumberFormat nf = NumberFormat.getCurrencyInstance();
	    return nf.format(d);
	}
	
	public List<Kjoretoytype> fetchKjoretoytypeList() {
		return mipssKjoretoy.getKjoretoytypeList();
	}
	
	public List<Kjoretoymodell> fetchKjoretoymodellList() {
		return mipssKjoretoy.getKjoretoymodellList();
	}
	
	public List<Dfumodell> fetchDfumodellList() {
		return mipssKjoretoy.getDfumodellListForLeverandor(hardkodetDfuleverandor);
	}
	
	public List<Dfuvarekatalog> fetchDfuvarekatalogList() {
		final List<Dfuvarekatalog> list = mipssKjoretoy.getDfuvarekatalogListForLeverandor(hardkodetDfuleverandor);
		Collections.sort(list, new Comparator<Dfuvarekatalog>() {
			@Override
			public int compare(Dfuvarekatalog o1, Dfuvarekatalog o2) {
				Long l1 = o1.getGuiRekkefolge() != null ? o1.getGuiRekkefolge() : 0L;
				Long l2 = o2.getGuiRekkefolge() != null ? o2.getGuiRekkefolge() : 0L;
				return l1.compareTo(l2);
			}
		});
		return list;
	}
	
	public List<Sensortype> fetchSensortypeList() {
		return mipssKjoretoy.getSensortypeList();
	}
	
	public List<Ioliste> getIolisteList() {		
		if(getInstallasjon() != null && getInstallasjon().getDfumodell() != null) {
			List<Dfumodell> dfumodellList = fetchDfumodellList();			
			for(Dfumodell dfumodell : dfumodellList) {
				if(dfumodell.equals(getInstallasjon().getDfumodell())) {
					return dfumodell.getIolisteForMonteringsskjema();
				}
			}
		}
		
		return new ArrayList<Ioliste>();
	}
	
	public static PropertyResourceBundleUtil getResourceBundle() {
		return resourceBundle;
	}

	public Kjoretoyordre getKjoretoyordre() {
		return kjoretoyordre;
	}

	public void setKjoretoyordre(Kjoretoyordre kjoretoyordre) {
		Object old = this.kjoretoyordre;
		this.kjoretoyordre = kjoretoyordre;
		props.firePropertyChange("kjoretoyordre", old, kjoretoyordre);
	}

	public Kjoretoy getKjoretoy() {
		return kjoretoy;
	}

	public void setKjoretoy(Kjoretoy kjoretoy) {
		Object old = this.kjoretoy;
		this.kjoretoy = kjoretoy;
		props.firePropertyChange("kjoretoy", old, kjoretoy);
	}

	public Driftkontrakt getDriftkontrakt() {
		return driftkontrakt;
	}

	public void setDriftkontrakt(Driftkontrakt driftkontrakt) {
		Object old = this.driftkontrakt;
		this.driftkontrakt = driftkontrakt;
		props.firePropertyChange("driftkontrakt", old, driftkontrakt);
	}

	public Dfubestillingmaster getDfubestillingmaster() {
		return dfubestillingmaster;
	}

	public void setDfubestillingmaster(Dfubestillingmaster dfubestillingmaster) {
		Object old = this.dfubestillingmaster;
		this.dfubestillingmaster = dfubestillingmaster;
		props.firePropertyChange("dfubestillingmaster", old, dfubestillingmaster);
	}
	
	public DfulevBestilling getDfulevBestilling() {
		return dfulevBestilling;
	}
	
	public void setDfulevBestilling(DfulevBestilling dfulevBestilling) {
		Object old = this.dfulevBestilling;
		this.dfulevBestilling = dfulevBestilling;
		props.firePropertyChange("dfulevBestilling", old, dfulevBestilling);
	}

	public Installasjon getInstallasjon() {
		return installasjon;
	}

	public void setInstallasjon(Installasjon installasjon) {
		Object old = this.installasjon;
		this.installasjon = installasjon;
		props.firePropertyChange("installasjon", old, installasjon);
	}

	public String getLoggedOnUser() {
		return loggedOnUser;
	}

	public void setLoggedOnUser(String loggedOnUser) {
		Object old = this.loggedOnUser;
		this.loggedOnUser = loggedOnUser;
		props.firePropertyChange("loggedOnUser", old, loggedOnUser);
	}

	public List<KjoretoyUtstyrIo> getKjoretoyUtstyrIoList() {
		return kjoretoyUtstyrIoList;
	}

	public List<KjoretoyUtstyr> getKjoretoyUtstyrList() {
		return kjoretoyUtstyrList;
	}

	public Boolean getKjoretoyUtenUtstyr() {
		return kjoretoyUtenUtstyr;
	}

	public void setKjoretoyUtenUtstyr(Boolean kjoretoyUtenUtstyr) {
		Object old = this.kjoretoyUtenUtstyr;
		this.kjoretoyUtenUtstyr = kjoretoyUtenUtstyr;
		props.firePropertyChange("kjoretoyUtenUtstyr", old, kjoretoyUtenUtstyr);
		getKjoretoyordre().setUtenUtstyr(kjoretoyUtenUtstyr);
	}

	public Boolean getKjoretoyMaaHaBryterboks(){
		return kjoretoyMaaHaBryterboks;
	}
	public void setKjoretoyMaaHaBryterboks(Boolean kjoretoyMaaHaBryterboks){
		Boolean old = this.kjoretoyMaaHaBryterboks;
		this.kjoretoyMaaHaBryterboks = kjoretoyMaaHaBryterboks;
		props.firePropertyChange("kjoretoyMaaHaBryterboks", old, kjoretoyMaaHaBryterboks);
		getKjoretoyordre().setMaaHaBryterboks(kjoretoyMaaHaBryterboks);
	}
	public Boolean getKjoretoyNyopprettet() {
		return kjoretoyNyopprettet;
	}

	public void setKjoretoyNyopprettet(Boolean kjoretoyNyopprettet) {
		Object old = this.kjoretoyNyopprettet;
		this.kjoretoyNyopprettet = kjoretoyNyopprettet;
		props.firePropertyChange("kjoretoyNyopprettet", old, kjoretoyNyopprettet);
	}

	/**
	 * Opprettet io for utstyret og legger denne på kjøretøyets KjoretoyUtstyr og på ordrens KjoretoyordreUtstyr
	 * OBS! Legger også til io i utstyrets liste!
	 * 
	 * @param kjoretoyUtstyr
	 * @return
	 */
	private void lagNyKjoretoyUtstyrIoForKjoretoyordreUtstyr(KjoretoyordreUtstyr kjoretoyordreUtstyr) {
		//Finner utstyret på kjørerøyet
		KjoretoyUtstyr kjoretoyetsUtstyr = null;
		for(KjoretoyUtstyr ku : kjoretoy.getKjoretoyUtstyrList()) {
			if(ku.getId().equals(kjoretoyordreUtstyr.getKjoretoyUtstyr().getId())) {
				kjoretoyetsUtstyr = ku;
			}
		}
		
		if(kjoretoyetsUtstyr == null) {
			throw new IllegalStateException("Kjøretøy har ikke utstyr som ligger på ordren!");
		}
		
		final KjoretoyUtstyrIo kjoretoyUtstyrIo = new KjoretoyUtstyrIo();
		kjoretoyUtstyrIo.setFraDato(getInstallasjon() != null ? getInstallasjon().getAktivFraDato() : Clock.now());
		kjoretoyUtstyrIo.setKjoretoyUtstyr(kjoretoyetsUtstyr);
		kjoretoyUtstyrIo.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		kjoretoyUtstyrIo.getOwnedMipssEntity().setOpprettetAv(loggedOnUser);
		kjoretoyetsUtstyr.getKjoretoyUtstyrIoList().add(kjoretoyUtstyrIo);
		getKjoretoyUtstyrIoList().add(kjoretoyUtstyrIo);
		
		//IO må legges til KjoretoyordreUtstyr
		kjoretoyordreUtstyr.getKjoretoyUtstyr().getKjoretoyUtstyrIoList().add(kjoretoyUtstyrIo);
		kjoretoyordreUtstyr.setKjoretoyUtstyrIo(kjoretoyUtstyrIo);
		
		props.firePropertyChange("kjoretoyUtstyrIoList", null, getKjoretoyUtstyrIoList());
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}
	
	public void addUtstyrActionListener(ActionListener a) {
		utstyrOgIoActionListeners.add(a);
	}
	
	public void removeUtstyrActionListener(ActionListener a) {
		utstyrOgIoActionListeners.remove(a);
	}
	
	private void fireUtstyrOgIoAction() {
		for(final ActionListener a : utstyrOgIoActionListeners) {
			a.actionPerformed(null);
		}
	}
	
	public void finishPressed() {
		List<KjoretoyordreUtstyr> slettetKjoretoyordreUtstyr = null;
		if(bestillingSkjemaType.equals(BestillingSkjemaType.OPPRETTING)) {
			knyttKjoretoyTilKontrakt();
			//TODO knytt leverandør til kontrakt.
			opprettKjoretoyordreUtstyr();
			
			kjoretoy.addKjoretoyordre(kjoretoyordre);
			
			leggTilKjoretoyordreStatus(statusOverlevertKoord);
		}
		if (bestillingSkjemaType.equals(BestillingSkjemaType.ENDRING)){
			opprettKjoretoyordreUtstyr(); //Potensielt er nytt utstyr lagt til eller fjernet
			slettetKjoretoyordreUtstyr = 
				fjernKjoretoyordreUtstyrForSlettetUtstyr();
		}
		if(bestillingSkjemaType.equals(BestillingSkjemaType.ADMIN_BEHANDLING)) {
			opprettKjoretoyordreUtstyr(); //Potensielt er nytt utstyr lagt til
			slettetKjoretoyordreUtstyr = 
				fjernKjoretoyordreUtstyrForSlettetUtstyr();
			
			leggTilKjoretoyordreStatus(statusFullfortKoord);
		}
		
		if(bestillingSkjemaType.equals(BestillingSkjemaType.KONTRAKT_BEHANDLING)) {
			if(!kjoretoy.getInstallasjonList().contains(installasjon)) {
				kjoretoy.getInstallasjonList().add(installasjon);
				installasjon.setKjoretoy(kjoretoy);
			}
			
			
			kjoretoyordre.setInstallasjon(installasjon);
			leggTilKjoretoyordreStatus(statusFullfortKontrakt);
			
			avsluttEksisterendeKjoretoyUtstyrIO();
			
			for(KjoretoyUtstyrIo koi : kjoretoyUtstyrIoList) {
				koi.setFraDato(getInstallasjon().getAktivFraDato());
				koi.setTilDato(getInstallasjon().getAktivTilDato());
			}
			
			avsluttEksisterendeInstallasjoner(getInstallasjon());
			String rvl = mipssKjoretoy.prodsettKjoretoyDatek(installasjon.getDfuindivid().getSerienummer());
			if (rvl!=null){
				String body = "Kjoretoyordre med id:"+kjoretoyordre.getId()+ " feilet automatisk prodsetting!\n\nMelding fra datek:"+rvl;
				String subject = "Ny prodsetting av kjoretoy! (ordreid="+kjoretoyordre.getId()+")";
				mipssKjoretoy.sendFeilEpost(body, subject);
			}
		}
		
		//Lagrer kjøretøy og ordre
		mipssKjoretoy.oppdaterKjoretoyUnderBestilling(kjoretoy, slettetKjoretoyordreUtstyr);
		
		visKvittering();
	}

	/**
	 * Knytter kjoretoy mot kontrakt dersom denne knytningen ikke finnes fra før
	 */
	private void knyttKjoretoyTilKontrakt() {
		boolean harAnsvarligKontrakt = false;
		
		for(KontraktKjoretoy kontraktKjoretoy : kjoretoy.getKontraktKjoretoyList()) {
			if(kontraktKjoretoy.getDriftkontrakt().equals(driftkontrakt)) {
				return;
			} else if(kontraktKjoretoy.getAnsvarligFlaggBoolean()) {
				harAnsvarligKontrakt = true;
			}
		}
		
		KontraktKjoretoy kk = new KontraktKjoretoy();
		kk.setAnsvarligFlagg(harAnsvarligKontrakt ? 0L : 1L);
		kk.setDriftkontrakt(driftkontrakt);
		kk.setKjoretoy(kjoretoy);
		kjoretoy.getKontraktKjoretoyList().add(kk);
	}

	/**
	 * Setter kjoretoyUtstyrIo på dette kjøretøyet, som ikke er knyttet til ordre til avsluttet på dagens dato.
	 * Dersom noen io-er starter etter dagens dato, flyttes startdato til dagens dato.
	 */
	private void avsluttEksisterendeKjoretoyUtstyrIO() {
		for(KjoretoyUtstyr ku : kjoretoy.getKjoretoyUtstyrList()) {
			for(KjoretoyUtstyrIo kuio : ku.getKjoretoyUtstyrIoList()) {
				if(!getKjoretoyUtstyrIoList().contains(kuio)) {
					if(kuio.getFraDato().after(Clock.now())) {
						kuio.setFraDato(Clock.now());
					}
					kuio.setTilDato(Clock.now());
				}
			}
		}
	}
	
	/**
	 * Opprettet KjoretoyordreUtstyr for alt utstyr som ligger i den lokale utstyrslisten.
	 * Tar høyde for at KjoretoyordreUtstyr kan være opprettet fra før
	 */
	private void opprettKjoretoyordreUtstyr() {
		for(KjoretoyUtstyr ku : getKjoretoyUtstyrList()) {
			boolean alleredeLagtTil = false;
			
			//Sjekker at det ikke allerede ligger knytning mellom dette utstyret og ordren
			for(KjoretoyordreUtstyr kou : kjoretoyordre.getKjoretoyordreUtstyrList()) {
				if(kou.getKjoretoyUtstyr().equals(ku)) {
					alleredeLagtTil = true;
					break;
				}
			}
			
			if(!alleredeLagtTil) {
				KjoretoyordreUtstyr kjoretoyordreUtstyr = new KjoretoyordreUtstyr();
				kjoretoyordreUtstyr.setKjoretoyordre(kjoretoyordre);
				kjoretoyordreUtstyr.setKjoretoyUtstyr(ku);
				kjoretoyordreUtstyr.setOpprettetDato(Clock.now());
				kjoretoyordre.getKjoretoyordreUtstyrList().add(kjoretoyordreUtstyr);
			}
		}
	}
	
	/**
	 * Fjerner KjoretoyordreUtstyr for utstyr som er fjernet fra den lokale utstyrslisten men eksisterer i ordren
	 */
	private List<KjoretoyordreUtstyr> fjernKjoretoyordreUtstyrForSlettetUtstyr() {
		List<KjoretoyordreUtstyr> remList = new ArrayList<KjoretoyordreUtstyr>();
		
		for(KjoretoyordreUtstyr kjoretoyordreUtstyr : kjoretoyordre.getKjoretoyordreUtstyrList()) {
			boolean erFjernet = true;
			KjoretoyUtstyr kjoretoyUtstyr = kjoretoyordreUtstyr.getKjoretoyUtstyr();
			for(KjoretoyUtstyr ku : getKjoretoyUtstyrList()) {
				if(ku.equals(kjoretoyUtstyr)) {
					erFjernet = false;
				}
			}
			
			if(erFjernet) {
				remList.add(kjoretoyordreUtstyr);
			}
		}
		
		kjoretoyordre.getKjoretoyordreUtstyrList().removeAll(remList);
		
		return remList;
	}
	
	private void avsluttEksisterendeInstallasjoner(final Installasjon nyInstallasjon) {
		for(Installasjon ins : kjoretoy.getInstallasjonList()) {
			if(ins.equals(installasjon)) {
				continue;
			}
			if(ins.getAktivTilDato() == null || ins.getAktivTilDato().after(nyInstallasjon.getAktivFraDato())) {
				ins.setAktivTilDato(nyInstallasjon.getAktivFraDato());
			}
		}
	}

	public void cancelPressed() {
		
	}
	
	public WizardPanelNavResult tillatKjoretoy() {
		if(bestillingSkjemaType.equals(BestillingSkjemaType.OPPRETTING) && kjoretoy.getId() == null) {
			KjoretoyQueryFilter kqf = new KjoretoyQueryFilter();
			kqf.setEksterntNavn(kjoretoy.getEksterntNavn());
			kqf.setRegNr(kjoretoy.getRegnr());
			List<Kjoretoy> kjoretoyList = mipssKjoretoy.getKjoretoyList(kqf);
			if(kjoretoyList.size() > 0) {
				JOptionPane.showMessageDialog(parentDialog, 
						getResourceBundle().getGuiString("message.eksisterendeKjoretoyUtenDFU.message", EpostUtils.getSupportEpostAdresse()), 
						getResourceBundle().getString("message.eksisterendeKjoretoyUtenDFU.title"), 
						JOptionPane.ERROR_MESSAGE);
				return WizardPanelNavResult.REMAIN_ON_PAGE;
			}
			if (getKjoretoyUtenUtstyr()){
				int v = JOptionPane.showConfirmDialog(parentDialog, 
						getResourceBundle().getString("warning.utenUtstyr.message"),
						getResourceBundle().getString("warning.utenUtstyr.title"),
						JOptionPane.YES_NO_OPTION);
				if (v == JOptionPane.NO_OPTION){
					return WizardPanelNavResult.REMAIN_ON_PAGE;
				}
			}
		}
		return WizardPanelNavResult.PROCEED;
	}
	
	/**
	 * Viser kvittering for wizard
	 */
	private void visKvittering() {
		if(bestillingSkjemaType.equals(BestillingSkjemaType.OPPRETTING)) {
			JOptionPane.showMessageDialog(parentDialog, 
					getResourceBundle().getString("message.opprettetKvittering.message"), 
					getResourceBundle().getString("message.opprettetKvittering.title"), 
					JOptionPane.INFORMATION_MESSAGE);
		} else if(bestillingSkjemaType.equals(BestillingSkjemaType.KONTRAKT_BEHANDLING)) {
			JOptionPane.showMessageDialog(parentDialog, 
					getResourceBundle().getString("message.prodsattKvittering.message"), 
					getResourceBundle().getString("message.prodsattKvittering.title"), 
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void setStausUnderBehandling() {
		// TODO Auto-generated method stub
		if (kjoretoyordre.getSisteKjoretoyordrestatus().getIdent().equals(statusUnderArbeidKoord.getIdent())||
			kjoretoyordre.getSisteKjoretoyordrestatus().getIdent().equals(statusFullfortKoord.getIdent())){
			return;
		}
		leggTilKjoretoyordreStatus(statusUnderArbeidKoord);
		mipssKjoretoy.oppdaterKjoretoyUnderBestilling(kjoretoy, null);
	}
}	
