package no.mesta.mipss.driftslogg;

import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chart.messages.OpenSingleChartMessage;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams.MENGDERAPPORT_TYPE;
import no.mesta.mipss.driftslogg.rapport.ApprovedAmountData;
import no.mesta.mipss.driftslogg.rapport.DriftsloggRapportData;
import no.mesta.mipss.driftslogg.rapport.OutsideTripData;
import no.mesta.mipss.driftslogg.rapport.VaerRapportObject;
import no.mesta.mipss.driftslogg.ui.*;
import no.mesta.mipss.driftslogg.ui.agressoinfo.OverforAgressoPanel;
import no.mesta.mipss.driftslogg.ui.detaljui.ActivitiesOverviewPanel;
import no.mesta.mipss.driftslogg.ui.fastprissjekk.FastprisAntallPanel;
import no.mesta.mipss.driftslogg.ui.fastprissjekk.FastprisPanel;
import no.mesta.mipss.driftslogg.ui.fastprissjekk.FastprisSlettPanel;
import no.mesta.mipss.driftslogg.ui.listeners.ChangeMonitor;
import no.mesta.mipss.driftslogg.ui.listeners.ChangeRegistry;
import no.mesta.mipss.driftslogg.ui.mipssrapport.MipssRapportTableModel;
import no.mesta.mipss.driftslogg.ui.mipssrapport.MipssrapportTreeView;
import no.mesta.mipss.driftslogg.ui.periode.rapport.*;
import no.mesta.mipss.driftslogg.util.ActivityUtil;
import no.mesta.mipss.driftslogg.util.AgressoUtil;
import no.mesta.mipss.driftslogg.util.CalendarHelper;
import no.mesta.mipss.driftslogg.util.ExportToExcel;
import no.mesta.mipss.excelrapport.ExcelreportModule;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.messages.OpenDetaljertProdrapportMessage;
import no.mesta.mipss.mipssmap.MipssMapTripMessage;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.driftslogg.*;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.WaitPanel;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.ToolTipHeader;
import no.mesta.mipss.util.EpostUtils;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static no.mesta.mipss.common.MipssDateFormatter.SHORT_DAY_MONTH_TIME_FORMAT;
import static no.mesta.mipss.ui.Colors.TABLE_GRID_COLOR;

@SuppressWarnings("serial")
public class DriftsloggController {

	private static final Logger log = LoggerFactory.getLogger(DriftsloggController.class);

	private final DriftsloggModule plugin;

	private MipssRapportTableModel mdlMipsstall;
	private JMipssTreeTable<MipssrapportTreeView> tblMipsstall;

	private TripTableModel mdlTrip;
	private JMipssTreeTable<TripTreeView> tblTrip;

	private BindingGroup bindings = new BindingGroup();

	private DriftsloggQueryParams queryParams;

	private DriftsloggService driftslogg;

	private ChangeMonitor<Aktivitet> aktiviteterChangedMonitor;
	private ChangeMonitor<Stroproduktmengde> stromengderChangedMonitor;
	private ChangeRegistry saveStatusMonitor = new ChangeRegistry();

	private MipssBeanLoaderListener aktivitetLoaderListener;
	private List<Aktivitet> valgteAktiviteter;
	private List<Trip> selectedTrips;
	private boolean isTripDetailOpen = false;

	//Eksport til excel
	private JMipssTreeTable<TripTreeView> tblTripForEksport;
	private JMipssTreeTable<AktivitetTreeView> tblAktivitetForEksport;
	private TripTableModelExportEgendef tripsToExport;

	private ActivityUtil activityUtil;
	private CellHighlightPredicate cellHighlighter;

	public DriftsloggController(DriftsloggModule plugin){
		this.plugin = plugin;
		driftslogg = BeanUtil.lookup(DriftsloggService.BEAN_NAME, DriftsloggService.class);
		queryParams = new DriftsloggQueryParams();
		queryParams.addPropertyChangeListener(evt -> setReloadTable(true));
		initMipsstallTable();
		initTripTable();
	}

	public ChangeRegistry getSaveStatusMonitor(){
		return saveStatusMonitor;
	}

	public DriftsloggModule getPlugin(){
		return plugin;
	}

	public BindingGroup getBindings(){
		return bindings;
	}

	public void bind(){
		Binding<JMipssTreeTable<TripTreeView>, Object, DriftsloggController, Object> b = BindingHelper.createbinding(getTripTable(), "selectedEntities", this, "mipssTiltak");
		getBindings().addBinding(b);
		getQueryParams().addPropertyChangeListener(evt -> setMipssTiltak(null));

		bindings.bind();
	}

	private void initMipsstallTable(){
		MipssRenderableEntityTableColumnModel miprrapportColumnModel = new MipssRenderableEntityTableColumnModel(
				MipssrapportTreeView.class, "tiltak", "km", "timer", "stroprodukt", "tonnTorr", "literLosningTotalt", "kgLosning");
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_TILTAK).setPreferredWidth(200);
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_KM).setPreferredWidth(60);
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_TIMER).setPreferredWidth(40);
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_STROPRODUKT).setPreferredWidth(60);
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_TONNTORR).setPreferredWidth(60);
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_LITER_LOSNING_TOTALT).setPreferredWidth(60);
		miprrapportColumnModel.getColumn(MipssRapportTableModel.COL_KG_LOSNING).setPreferredWidth(60);


		mdlMipsstall= new MipssRapportTableModel();
		tblMipsstall = new JMipssTreeTable<>(mdlMipsstall);
		mdlMipsstall.setTreeView(tblMipsstall);
		tblMipsstall.setColumnModel(miprrapportColumnModel);
		tblMipsstall.setFillsViewportHeight(true);
		tblMipsstall.setLeafIcon(null);
		tblMipsstall.setShowGrid(true, true);
		tblMipsstall.setGridColor(new Color(230,230,230));

		//Oppretter nye headere med tooltip
		ToolTipHeader header = new ToolTipHeader(tblMipsstall.getColumnModel());
		header.setToolTip(MipssRapportTableModel.COL_TONNTORR, Resources.getResource("mipsstall.header.tonnTorrstoff"));
		header.setToolTip(MipssRapportTableModel.COL_LITER_LOSNING_TOTALT, Resources.getResource("mipsstall.header.literLosning"));
		header.setToolTip(MipssRapportTableModel.COL_KG_LOSNING, Resources.getResource("mipsstall.header.kgLosning"));
	    tblMipsstall.setTableHeader(header);
	}

	/**
	 * Henter Open Route-data for alle aktivitetene og samler dem pr rode. Like tiltak på samme rode blir slått sammen.
	 * @param trips
	 * @return
	 */
	public List<MipssRodeTiltak> searchORData(List<Trip> trips){
		if (trips == null)
			return null;

		Map<String, MipssRodeTiltak> tiltakMap = new HashMap<>();
		for (Trip t : trips) {
		    if (t.getId() == null) {
		        continue;
            }

			List<MipssRodeTiltak> rodeTiltak = driftslogg.searchORData(t, queryParams.getKontrakt().getId());

			if (rodeTiltak==null)
				continue;

			for (MipssRodeTiltak rt:rodeTiltak) {
				MipssRodeTiltak rode = tiltakMap.get(rt.getRode());

				if (rode == null) { //ny rode, legg til og fortsett med neste
					tiltakMap.put(rt.getRode(), rt);
					continue;
				}

				for (MipssTiltak mt:rt.getTiltak()) {
					MipssTiltak eksisterende = getTiltakFromList(mt, rode.getTiltak());

					if (eksisterende==null){//nytt tiltak på roden, legg til og fortsett med neste
						rode.getTiltak().add(mt);
					} else { //slå sammen verdier med forrige like tiltak
						eksisterende.setKm(add(eksisterende.getKm(), mt.getKm()));
						eksisterende.setTimer(add(eksisterende.getTimer(), mt.getTimer()));
						eksisterende.setTonn(add(eksisterende.getTonn(), mt.getTonn()));
					}
				}
			}
		}

		//flat ut mappet til en liste, legg på evt sortering..
		List<MipssRodeTiltak> tiltak = new ArrayList<>();
		for (String key:tiltakMap.keySet()) {
			MipssRodeTiltak mipssRodeTiltak = tiltakMap.get(key);
			tiltak.add(mipssRodeTiltak);

			Collections.sort(mipssRodeTiltak.getTiltak(), new Comparator<MipssTiltak>() {
				@Override
				public int compare(MipssTiltak o1, MipssTiltak o2) {
					return o1.getNavn().compareTo(o2.getNavn());
				}
			});
		}

		Collections.sort(tiltak, new Comparator<MipssRodeTiltak>() {
			@Override
			public int compare(MipssRodeTiltak o1, MipssRodeTiltak o2) {
				if ("Sum".equals(o1.getRode())){
					return -1;
				}
				return o1.getRode().compareTo(o2.getRode());
			}
		});
		return tiltak;
	}

	private Double add(Double d1, Double d2){
		if (d1==null&&d2==null){
			return null;
		} else if (d1==null){
			return d2;
		} else if (d2==null){
			return d1;
		}
		return d1 + d2;
	}

	private MipssTiltak getTiltakFromList(MipssTiltak mt, List<MipssTiltak> eksisterende){
		for (MipssTiltak e:eksisterende){
			if (mt.getNavn().equals(e.getNavn())){
				if (mt.getStroprodukt()==null&&e.getStroprodukt()==null)
					return e;
				if (mt.getStroprodukt()!=null&& mt.getStroprodukt().equals(e.getStroprodukt())){
					return e;
				}
			}
		}
		return null;
	}

	private void initTripTable() {
		MipssRenderableEntityTableColumnModel tripColumnModel = getTripColumnModel();

		tripColumnModel.getColumn(TripTableModel.COL_PERIOD_STATUS).setPreferredWidth(35);
		tripColumnModel.getColumn(TripTableModel.COL_PERIOD_STATUS).setMaxWidth(35);

		tripColumnModel.getColumn(TripTableModel.COL_PLATFORM).setPreferredWidth(45);
        tripColumnModel.getColumn(TripTableModel.COL_PLATFORM).setMaxWidth(45);
		tripColumnModel.getColumn(TripTableModel.COL_PLATFORM).setCellRenderer(new PlatformTableCellRenderer());

        tripColumnModel.getColumn(TripTableModel.COL_PROCESS_STATUS).setPreferredWidth(50);
        tripColumnModel.getColumn(TripTableModel.COL_PROCESS_STATUS).setMaxWidth(50);
		tripColumnModel.getColumn(TripTableModel.COL_PROCESS_STATUS).setCellRenderer(new StatusTableCellRenderer());

		tripColumnModel.getColumn(TripTableModel.COL_WARNINGS).setCellRenderer(new WarningTableCellRenderer());

		tripColumnModel.getColumn(TripTableModel.COL_PROCESS_TRIP).setPreferredWidth(115);
        tripColumnModel.getColumn(TripTableModel.COL_PROCESS_TRIP).setMaxWidth(115);
		tripColumnModel.getColumn(TripTableModel.COL_PROCESS_TRIP).setCellRenderer(new ProcessTripTableCell(this));
		tripColumnModel.getColumn(TripTableModel.COL_PROCESS_TRIP).setCellEditor(new ProcessTripTableCell(this));

		tripColumnModel.getColumn(TripTableModel.COL_START_TIME).setPreferredWidth(75);
		tripColumnModel.getColumn(TripTableModel.COL_START_TIME).setCellRenderer(new DateTimeRenderer(SHORT_DAY_MONTH_TIME_FORMAT));

		tripColumnModel.getColumn(TripTableModel.COL_END_TIME).setPreferredWidth(75);
		tripColumnModel.getColumn(TripTableModel.COL_END_TIME).setCellRenderer(new DateTimeRenderer(SHORT_DAY_MONTH_TIME_FORMAT));

		tripColumnModel.getColumn(TripTableModel.COL_ID).setCellRenderer(new CenteredLabelRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_DURATION).setCellRenderer(new CenteredLabelRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_VEHICLE).setCellRenderer(new CenteredLabelRenderer());

		tripColumnModel.getColumn(TripTableModel.COL_AMOUNT).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_HOURS_TOTAL).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_PLOWING_TOTAL).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_PLANING_TOTAL).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_HEAVY_PLANING_TOTAL).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_SIDE_PLOUGH_TOTAL).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_DRY_GRITTING_TOTAL).setCellRenderer(new BoldTripCellRenderer());
		tripColumnModel.getColumn(TripTableModel.COL_WET_GRITTING_TOTAL).setCellRenderer(new BoldTripCellRenderer());

		mdlTrip = new TripTableModel(this);
		//mdlTrip.addTableLoaderListener(aktivitetLoaderListener);

		Map<Integer, String> tooltips = new HashMap<>();

		tblTrip = new TripTable(mdlTrip, tooltips);
		new TripTreeSorter(mdlTrip, tblTrip, tripColumnModel);
		tblTrip.setRowHeight(56);
		tblTrip.setColumnModel(tripColumnModel);
		tblTrip.setFillsViewportHeight(true);
		tblTrip.setLeafIcon(null);
		tblTrip.setShowsRootHandles(false);
		tblTrip.setColumnControlVisible(true);
		tblTrip.getTableHeader().setDefaultRenderer(new CenterHeaderRenderer(tblTrip));
		tblTrip.setGridColor(TABLE_GRID_COLOR);
		tblTrip.setShowGrid(true, true);
		tblTrip.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount() == 2) {
					if (tblTrip.getSelectedEntities().size() > 0) {
                        TripTreeView ttv = tblTrip.getSelectedEntities().get(0);
                        if (ttv.getTrip() == null) {
                            ttv = ttv.getParent();
                        }

                        openTrip(ttv.getTrip());
					}
				}
			}
		});
		tblTrip.setTreeCellRenderer(new PeriodStatusTableCellRenderer(tblTrip.getSelectionBackground(), tblTrip.getSelectionForeground()));
	}

	//Greit å vite
	//Stringene representerer nøkkelen i tableColumns.properties. Det er kun nøkler med unike navn som er med i eksporten til excel. Det betyr at f.eks innholdet i kolonnene "empty" ikke vises i excel.

	private MipssRenderableEntityTableColumnModel getTripColumnModel() {
		return new MipssRenderableEntityTableColumnModel(
				TripTreeView.class, "periodStatus", "id", "empty", "processStatus", "warnings", "processTrip", "start", "end",
				"duration", "vendor", "vehicle", "amount", "hoursTotal", "plowingTotal", "planingTotal", "heavyPlaningTotal",
				"sidePloughTotal", "dryGrittingTotal", "wetGrittingTotal");
	}

    public void openTrip(Trip trip) {
		if (!isTripDetailOpen) {
			isTripDetailOpen = true;

			JFrame parent = plugin.getLoader().getApplicationFrame();

			JDialog tripDialog = new JDialog(parent);
			tripDialog.setTitle(Resources.getResource("title.tripDetails"));
			tripDialog.setModalityType(Dialog.ModalityType.MODELESS);
			tripDialog.setSize(parent.getWidth() - 50, parent.getHeight() - 50);
			tripDialog.setLocationRelativeTo(parent);
			final ActivitiesOverviewPanel pnl = new ActivitiesOverviewPanel(this, tripDialog, trip, getTableSortedTrips());
			tripDialog.add(pnl);
			tripDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			tripDialog.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					pnl.dispose();
					isTripDetailOpen = false;
				}
			});
			tripDialog.setVisible(true);
		}
    }

	private List<Trip> getTableSortedTrips() {
		List<Trip> trips = new ArrayList<>();

		int[] rows = new int[tblTrip.getRowCount()];
		for (int i = 0; i < tblTrip.getRowCount(); i++) {
			rows[i] = i;
		}

		List<TripTreeView> tableTrips = tblTrip.getEntities(rows);

		for (TripTreeView ttv : tableTrips) {
			// We don't want to include the "sum" row (which doesn't have an trip id).
			if (ttv.getTrip().getId() != null) {
				trips.add(ttv.getTrip());
			}
		}

		return trips;
	}

	public MipssRapportTableModel getMipsstallTableModel(){
		return mdlMipsstall;
	}

	public JMipssTreeTable<MipssrapportTreeView> getMipsstallTable(){
		return tblMipsstall;
	}

	public TripTableModel getTripTableModel() {
		return mdlTrip;
	}

	public JMipssTreeTable<TripTreeView> getTripTable(){
		return tblTrip;
	}

	public DriftsloggQueryParams getQueryParams() {
		return queryParams;
	}

	/**
	 * Laster tabellen over økter på nytt
	 * @param changed
	 */
	public void setReloadTable(boolean changed){
		if (changed){
			if (queryParams.getKontrakt()!=null && queryParams.getMonth()!=null){
				//TODO håndter eventuelle ikke-lagrede endringer i tabellen..
				if (saveStatusMonitor.isChanged()){
					int confirm = JOptionPane.showConfirmDialog(plugin.getModuleGUI(), Resources.getResource("message.ikkeLagret"), Resources.getResource("message.ikkeLagret.title"), JOptionPane.YES_NO_CANCEL_OPTION);
					if (confirm!=JOptionPane.YES_OPTION){
						return;
					}
				}
				mdlTrip.loadData(this);
			}
		}
	}

	private List<Object[]> createOriginalValuesTrip(List<Trip> list){
		List<Object[]> ov = new ArrayList<>();
		for (Trip t : list) {
			ov.add(createOriginalValuesTrip(t));
		}
		return ov;
	}

	private Object[] createOriginalValuesTrip(Trip t) {
		return new Object[]{null, null, null, null};
	}

	private List<Object[]> createOriginalValuesStromengde(List<Stroproduktmengde> list){
		List<Object[]> ov = new ArrayList<>();
		for (Stroproduktmengde s:list){
			ov.add(createOriginalValuesStro(s));
		}
		return ov;
	}

	private Object[] createOriginalValuesStro(Stroproduktmengde s){
		return new Object[]{s.getStroprodukt(), s.getOmforentMengdeTort(), s.getOmforentMengdeVaatt()};
	}

	/**
	 * Henter den valgte driftskontraktens leverandører
	 * @return
	 */
	public List<Leverandor> getLeverandorer(){
		Driftkontrakt kontrakt = queryParams.getKontrakt();
		return driftslogg.getLeverandorer(kontrakt.getId());
	}

	/**
	 * Henter den valgte dirfitskontraktens kontraktsleverandører
	 * @return
	 */
	public List<KontraktLeverandor> getKontraktLeverandorer(){
		Driftkontrakt kontrakt = queryParams.getKontrakt();
		return driftslogg.getKontraktLeverandorer(kontrakt.getId());
	}

	/**
	 * Henter den valgte leverandørens agressokontrakter
	 * @return
	 */
	public List<Levkontrakt> getLevkontrakter(){
		Leverandor leverandor = queryParams.getLeverandor();
		return driftslogg.getKontrakterForLeverandor(leverandor.getNr());
	}

	public List<Aktivitet> getAktiviteter(DriftsloggQueryParams params){
		return driftslogg.sokAktiviteter(params);
	}

	public List<Trip> getTrips(DriftsloggQueryParams params, Date fromDate, Date toDate) {
		List<Trip> trips = driftslogg.searchTrips(params, fromDate, toDate);

		Trip summedTrip = sumTrips(trips);
		if(summedTrip != null) {
			trips.add(0, summedTrip);
		}
		return trips;
	}

    public List<Trip> getTrips(DriftsloggQueryParams params) {
        List<Trip> trips = driftslogg.searchTrips(params);

        Trip summedTrip = sumTrips(trips);
        if (summedTrip != null) {
            trips.add(0, summedTrip);
        }

        return trips;
    }

    private Trip sumTrips(List<Trip> trips) {
        if (trips.size() > 0) {
            Trip trip = new Trip();
            trip.setIgnored(true);

            for (Trip t : trips) {
                if (t.getAmount() != null)
                    trip.addAmount(t.getAmount());

                if (t.getHoursTotal() != null)
                    trip.addHours(t.getHoursTotal());

                if (t.getPlowingTotalKm() != null)
                    trip.addPlowingKm(t.getPlowingTotalKm());

                if (t.getPlaningTotalKm() != null)
                    trip.addPlaningKm(t.getPlaningTotalKm());

                if (t.getHeavyPlaningTotalKm() != null)
                    trip.addHeavyPlaningKm(t.getHeavyPlaningTotalKm());

                if (t.getSidePloughTotalKm() != null)
                    trip.addSidePloughKm(t.getSidePloughTotalKm());

                if (t.getDryGrittingTotalKg() != null)
                    trip.addDryGrittingKg(t.getDryGrittingTotalKg());

                if (t.getWetGrittingTotalLiter() != null)
                    trip.addWetGrittingLiter(t.getWetGrittingTotalLiter());
            }

            return trip;
        }
        return null;
    }

	public List<Aktivitet> getValgteAktiviteter(){
		return valgteAktiviteter;
	}

	public List<Trip> getSelectedTrips() {
		return selectedTrips;
	}

	// TODO: needed for detail view of a trip (showing just activities)
	/*public void setMipssTiltak(List<AktivitetTreeView> aktivitetList){
		if (aktivitetList==null||aktivitetList.isEmpty()){
			//TODO nuke table
			valgteAktiviteter = null;
			mdlMipsstall.loadData(this);
			return;
		}
		List<Aktivitet> aktiviteter = new ArrayList<Aktivitet>();
		for (AktivitetTreeView atl:aktivitetList){
			if (atl.getAktivitet()==null){
				atl = atl.getParent();
			}
			aktiviteter.add(atl.getAktivitet());
		}
		valgteAktiviteter = aktiviteter;
		mdlMipsstall.loadData(this);
	}*/

	public void setMipssTiltak(List<TripTreeView> tripList){
		if (tripList == null || tripList.isEmpty()) {
			//TODO nuke table
			selectedTrips = null;
			mdlMipsstall.loadData(this);
			return;
		}

		List<Trip> trips = new ArrayList<Trip>();
		for (TripTreeView ttl : tripList) {
			if (ttl.getTrip() == null) {
				ttl = ttl.getParent();
			}
			trips.add(ttl.getTrip());
		}
		selectedTrips = trips;
		mdlMipsstall.loadData(this);
	}

	/**
	 * Used by ActivitiesOverviewPanel when paging between trips so we are able to populate .
	 * @param trip
	 */
	public void setMipssTiltakForSingleTrip(Trip trip){
		if (trip != null) {
			List<Trip> trips = new ArrayList<>();
			trips.add(trip);

			selectedTrips = trips;
		} else {
			selectedTrips = null;
		}

		mdlMipsstall.loadData(this);
	}

	// TODO: needed for the activity detail view
	/*public boolean showSumWarningMessage(){
		List<Aktivitet> valgteAktiviteter = getValgteAktiviteter();
		if (valgteAktiviteter == null || valgteAktiviteter.size() == 0) {
			return false;
		}
		List<Aktivitet> utenOverlappListe = getAntallValgteAktiviteterUtenOverlapp();

		return valgteAktiviteter.size() != utenOverlappListe.size();
	}*/

	/**
	 * Metode som sjekker om aktivitetene til like kjøretøy overlapper på tid.
	 * @return Liste med aktiviteter som ikke overlapper på tid
	 */
	private List<Trip> getSelectedTripsWithoutOverlap() {
		List<Trip> utenOverlappListe = new ArrayList<>();

		for (int i = 0; i < getSelectedTrips().size(); i++) {
			Trip trip = getSelectedTrips().get(i);
			boolean skalLeggesTil = true;
			if (i == 0) {
				utenOverlappListe.add(trip);
				continue;
			}

			for(Trip t : utenOverlappListe) {
				if (t.getId() != null && trip.getId() != null && trip.getVehicleId() != null && t.getVehicleId() != null) {
					if (trip.getVehicleId().longValue() == t.getVehicleId().longValue()) {
						boolean fraDatoBeforeFraDato = trip.getStartTime().before(t.getStartTime());
						boolean tilDatoAfterTilDato = trip.getEndTime().after(t.getEndTime());

						boolean fraDatoAfterFraDato = trip.getStartTime().after(t.getStartTime());
						boolean fraDatoBeforeTilDato = trip.getStartTime().before(t.getEndTime());

						boolean tilDatoAfterFraDato = trip.getEndTime().after(t.getStartTime());
						boolean tilDatoBeforeTilDato = trip.getEndTime().before(t.getEndTime());

						boolean fraDatoErLikFraDato = trip.getStartTime().getTime() == t.getStartTime().getTime();
						boolean tilDatoErLikTilDato = trip.getEndTime().getTime() == t.getEndTime().getTime();

						if (fraDatoBeforeFraDato && (tilDatoAfterFraDato && tilDatoBeforeTilDato)) {
							skalLeggesTil = false;
						} else if (fraDatoAfterFraDato && tilDatoBeforeTilDato) {
							skalLeggesTil = false;
						}  else if ((fraDatoAfterFraDato && fraDatoBeforeTilDato) && tilDatoAfterTilDato){
							skalLeggesTil = false;
						} else if (trip.getStartTime() == t.getStartTime() && trip.getEndTime() == t.getEndTime()) {
							skalLeggesTil = false;
						} else if (fraDatoErLikFraDato || tilDatoErLikTilDato) {
							skalLeggesTil = false;
						}
					}
				}
			}
			if (skalLeggesTil) {
				utenOverlappListe.add(trip);
			}
		}
		return utenOverlappListe;
	}

	public Action getAktivitetSokAction() {
		return new AbstractAction(Resources.getResource("button.sok"), IconResources.SEARCH_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				mdlMipsstall.load(null);
				setReloadTable(true);
			}
		};
	}

	public Action getExportMonthAction() {
		return new AbstractAction(Resources.getResource("button.exportToExcelMonth"), IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				ExportToExcel excel = new ExportToExcel(getTripTable());
				File excelFile = excel.exportToExcel();
				DesktopHelper.openFile(excelFile);
			}
		};
	}

	public Action getExportEgendefAction() {
		return new AbstractAction(Resources.getResource("button.exportToExcelEgendef"), IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new JDialog(plugin.getLoader().getApplicationFrame(), true);
				ExportEgendefToExcelPanel pnl = new ExportEgendefToExcelPanel(DriftsloggController.this);
				dialog.add(pnl);
				dialog.setSize(350,250);
				dialog.setLocationRelativeTo(plugin.getModuleGUI());
				dialog.setVisible(true);
				if (pnl.isCancelled()){
					return;
				}
				final Date fraDato = pnl.getFraDato();
				final Date tilDato = pnl.getTilDato();
				final JDialog d = displayWait();
				d.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							createExportToExcelEgendef(fraDato, tilDato);
						}catch (Exception e){
							e.printStackTrace();
						}
						return null;
					}

					@Override
					protected void done(){
						d.dispose();
					}
				}.execute();
			}
		};
	}

	public Action getAktivitetSokLagreAction() {
		return new AbstractAction(Resources.getResource("button.lagre"), IconResources.SAVE_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				saveChanges();
			}
		};
	}

	/**
	 * Lagrer alle endringer som er gjort
	 */
	private void saveChanges(){
		// TODO: uncomment this at a later point!

		/*boolean commentsRequired = false;
		String activityString = null;
		for(Aktivitet activity : mdlAktivitet.getAktiviteter()) {
			if(activityUtil.needsComment(activity)) {
				if(activityString == null) {
					activityString = String.valueOf(activity.getId());
				} else {
					activityString = activityString+", "+String.valueOf(activity.getId());
				}
				int i=0;
				for(Aktivitet tableActivity : mdlAktivitet.getAktiviteter()) {
					if(tableActivity.equals(activity)) {
						cellHighlighter.addHighlight(i, AktivitetTableModel.COL_INTERN_KOMMENTAR);
					}
					i++;
				}
				commentsRequired = true;
			}
		}

		if(commentsRequired) {
			JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.intention.content", activityString), Resources.getResource("message.intention.title"), JOptionPane.OK_OPTION);
			return;
		}

		addKommentarerTilEndredeStromengder(stromengderChangedMonitor.getChangedEntities());
		oppdaterEndretAvOgDato(stromengderChangedMonitor.getChangedEntities());
		driftslogg.persistStromengder(stromengderChangedMonitor.getChangedEntities());
		driftslogg.persistAktiviteter(aktiviteterChangedMonitor.getChangedEntities());
		stromengderChangedMonitor.setOverriddenChanged(false);
		aktiviteterChangedMonitor.setOverriddenChanged(false);
		cellHighlighter.removeHighlights();
		setReloadTable(true);*/
	}

	private void oppdaterEndretAvOgDato(List<Stroproduktmengde> changedStroproduktmengder) {
		for (Stroproduktmengde s : changedStroproduktmengder) {
			s.setEndretAv(getPlugin().getLoader().getLoggedOnUser(true).getSignatur());
			s.setEndretDato(new Date());
		}
	}

	public void saveSingle(Aktivitet aktivitet){
		addKommentarerTilEndredeStromengder(aktivitet.getStroprodukter());
		oppdaterEndretAvOgDato(stromengderChangedMonitor.getChangedEntities());
		List<Aktivitet> a = new ArrayList<>();
		a.add(aktivitet);

		driftslogg.persistAktiviteter(a);

		if (aktivitet.getStroprodukter()!=null){//tilbakestill stroproduktmonitoren
			for (Stroproduktmengde s:aktivitet.getStroprodukter()){
				stromengderChangedMonitor.resetEntity(s);
			}
		}
		//tilbakestill endringsmonitoren for denne aktiviteten
		aktiviteterChangedMonitor.resetEntity(aktivitet);
	}

	public void resetStroproduktmengde(Aktivitet aktivitet, Aktivitet orginalAktivitet){

		if (aktivitet.getStroprodukter()!=null){//tilbakestill stroproduktmonitoren
			for (Stroproduktmengde s : aktivitet.getStroprodukter()) {
				for (Stroproduktmengde orginal : orginalAktivitet.getStroprodukter()) {
					if (s.getId().longValue() == orginal.getId().longValue()) {
						s.setStroprodukt(orginal.getStroprodukt() != null ? orginal.getStroprodukt() : null);
						s.setStroproduktId(orginal.getStroproduktId() != null ? orginal.getStroproduktId() : null);
						s.setMeldtMengdeTort(orginal.getMeldtMengdeTort() != null ? orginal.getMeldtMengdeTort() : null);
						s.setMeldtMengdeVaatt(orginal.getMeldtMengdeVaatt() != null ? orginal.getMeldtMengdeVaatt() : null);
						s.setOmforentMengdeTort(orginal.getOmforentMengdeTort() != null ? orginal.getOmforentMengdeTort() : null);
						s.setOmforentMengdeVaatt(orginal.getOmforentMengdeVaatt() != null ? orginal.getOmforentMengdeVaatt() : null);
						s.setEnhetVaatt(s.getEnhetVaatt() != null ? s.getEnhetVaatt() : "...");
					}
				}
			}
		}
	}

	private void addKommentarerTilEndredeStromengder(List<Stroproduktmengde> changedEntities) {
		for (Stroproduktmengde stroproduktmengde : changedEntities) {
			Stroproduktmengde original = driftslogg.getStroproduktmengdeById(stroproduktmengde.getId());
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			String timeNow = dateFormatter.format(new Date());
			String brukernavn = getPlugin().getLoader().getLoggedOnUser(true).getSignatur();

			//Endret Strøprodukt
			if (stroproduktmengde.getStroproduktId().longValue() != original.getStroproduktId().longValue()) {

				String kommentar = Resources.getResource("message.stroproduktErEndret",
																		timeNow,
																		brukernavn,
																		original.getStroprodukt().getNavn(),
																		original.getMeldtMengdeTort() + "",
																		original.getMeldtMengdeVaatt() + "",
																		stroproduktmengde.getStroprodukt().getNavn());

				leggTilStromengdeKommentar(stroproduktmengde, kommentar);
				leggTilStromengdeInternKommentar(stroproduktmengde, kommentar);

				nullUtFelter(stroproduktmengde);

			}

			//Endret enhetstype
			if (stroproduktmengde.getEnhetVaatt() != null) {

				if (original.getEnhetVaatt() == null) {

					String enhetlagtTilKommentar = Resources.getResource("message.enhetstypeErLagtTil", timeNow, brukernavn, stroproduktmengde.getEnhetVaatt());

					leggTilStromengdeKommentar(stroproduktmengde, enhetlagtTilKommentar);
					leggTilStromengdeInternKommentar(stroproduktmengde, enhetlagtTilKommentar);

				} else if (!stroproduktmengde.getEnhetVaatt().equals(original.getEnhetVaatt())) {

					String enhetEndretKommentar = Resources.getResource("message.enhetstypeErEndret", timeNow, brukernavn, original.getEnhetVaatt(), stroproduktmengde.getEnhetVaatt());

					leggTilStromengdeKommentar(stroproduktmengde, enhetEndretKommentar);
					leggTilStromengdeInternKommentar(stroproduktmengde, enhetEndretKommentar);

				}
			} else if (stroproduktmengde.getEnhetVaatt() == null && original.getEnhetVaatt() != null) {

				String enhetFjernetKommentar = Resources.getResource("message.enhetstypeErFjernet", timeNow, brukernavn, original.getEnhetVaatt());

				leggTilStromengdeKommentar(stroproduktmengde, enhetFjernetKommentar);
				leggTilStromengdeInternKommentar(stroproduktmengde, enhetFjernetKommentar);
			}
		}
	}

	/**
	 *
	 * @param stroproduktmengde
	 */
	private void nullUtFelter(Stroproduktmengde stroproduktmengde) {
		if (stroproduktmengde.getStroprodukt().getVaattFlagg() == 0 && stroproduktmengde.getStroprodukt().getToKammerFlagg() == 0) {
			stroproduktmengde.setEnhetVaatt("...");
			stroproduktmengde.setOmforentMengdeVaatt(null);
		}
		else if (stroproduktmengde.getStroprodukt().getVaattFlagg() == 1) {
			stroproduktmengde.setOmforentMengdeTort(null);
		}
		stroproduktmengde.setMeldtMengdeTort(null);
		stroproduktmengde.setMeldtMengdeVaatt(null);
	}

	private void leggTilStromengdeKommentar(Stroproduktmengde stroproduktmengde, String kommentar){
		if (stroproduktmengde.getAktivitet().getKommentar() == null) {
			stroproduktmengde.getAktivitet().setKommentar(kommentar);
		} else{
			stroproduktmengde.getAktivitet().setKommentar(stroproduktmengde.getAktivitet().getKommentar() + "\n" + kommentar);
		}
	}

	private void leggTilStromengdeInternKommentar(Stroproduktmengde stroproduktmengde, String internKommentar){
		if (stroproduktmengde.getAktivitet().getInternKommentar() == null) {
			stroproduktmengde.getAktivitet().setInternKommentar(internKommentar);
		} else{
			stroproduktmengde.getAktivitet().setInternKommentar(stroproduktmengde.getAktivitet().getInternKommentar() + "\n" + internKommentar);
		}
	}

	public List<LeverandorKontraktVO> getLevkontrakt(DriftsloggQueryParams queryParams) {
		List<LeverandorKontraktVO> sokPerioder = driftslogg.sokPerioder(queryParams);
		if (sokPerioder==null){
			sokPerioder = new ArrayList<>();
		}
		return sokPerioder;
	}

	public List<Rode> getRodeForKontrakt() {
		Long id = getQueryParams().getKontrakt().getId();
		Long stroBroyt = Long.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("FELLES", "stro_broyte_rodetype_id").getVerdi());
		return driftslogg.getRodeForKontrakt(stroBroyt, id);
	}

	public List<Kjoretoy> getKjoretoyForLeverandor(){
		return  driftslogg.getKjoretoyForLeverandor(getQueryParams().getLeverandor().getNr());
	}

	public Action getMengdeRapportMndAction(){
		return new AbstractAction(Resources.getResource("button.mengdeRapportMnd"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JDialog d = displayWait();
				d.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							Date fraDato = CalendarHelper.getFirstDateInMonth(getQueryParams().getMonth());
							Date tilDato = CalendarHelper.getLastTimeInMonth(getQueryParams().getMonth());
							createMengdeRapport(fraDato, tilDato, MENGDERAPPORT_TYPE.MND);
						}catch (Exception e){
							e.printStackTrace();
						}
						return null;
					}
					@Override
					protected void done(){
						d.dispose();
					}
				}.execute();
			}
		};
	}

	public Action getMengdeRapportEgenDefAction(){
		return new AbstractAction(Resources.getResource("button.mengdeRapportEgenDef"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new JDialog(plugin.getLoader().getApplicationFrame(), true);
				RapportPeriodePanel pnl = new RapportPeriodePanel(DriftsloggController.this);
				dialog.add(pnl);
				dialog.setSize(350,250);
				dialog.setLocationRelativeTo(plugin.getModuleGUI());
				dialog.setVisible(true);
				if (pnl.isCancelled()){
					return;
				}
				final Date fraDato = pnl.getFraDato();
				final Date tilDato = pnl.getTilDato();
				//TODO
				final JDialog d = displayWait();
				d.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							//TODO hent egne datoer i et view..
							createMengdeRapport(fraDato, tilDato, MENGDERAPPORT_TYPE.EGENDEF);
						}catch (Exception e){
							e.printStackTrace();
						}
						return null;
					}
					@Override
					protected void done(){
						d.dispose();
					}
				}.execute();
			}
		};
	}

	private JDialog displayWait(){
		JDialog dialog = new JDialog(SwingUtilities.getWindowAncestor(plugin.getModuleGUI()));
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		WaitPanel panel = new WaitPanel("Vennligst vent mens rapporten genereres");
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		return dialog;
	}

	public Action getSendIOAction() {
		return new AbstractAction(Resources.getResource("button.io", " .. "), IconResources.AGRESSO_16){
			@Override
			public void actionPerformed(ActionEvent e) {
				Date uke = getQueryParams().getMonth();
				Calendar c = Calendar.getInstance();
				c.setTime(uke);
				Long mnd = (long)c.get(Calendar.MONTH);
				Long aar = (long) c.get(Calendar.YEAR);

				String konfig = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.driftslogg", "attestantVikarSjekkAktivert").getVerdi();
				Boolean sjekkAttestant = Boolean.valueOf(konfig);

				List<Long> perioderSomManglerAttestant = null;
				if (sjekkAttestant){//sjekk av attestering er skrudd på
					List<PeriodeKontrakt> statusPerioder = driftslogg.getStatusPerioder(getQueryParams().getKontrakt().getId(), mnd, aar);
					List<PeriodeKontrakt> ikkeGodkjent = sjekkAttestering(statusPerioder);
					perioderSomManglerAttestant = new ArrayList<>();
					for (PeriodeKontrakt p:ikkeGodkjent){
						perioderSomManglerAttestant.add(p.getPeriodeId());
					}
					if (!sjekkBrukerSomAttestant(ikkeGodkjent)){
						return;
					}
				}

				Window parentW = SwingUtilities.windowForComponent(plugin.getModuleGUI());
				JFrame f = (JFrame)parentW;

				OverforAgressoPanel overforAgressoPanel = new OverforAgressoPanel(DriftsloggController.this, perioderSomManglerAttestant, mnd, aar);

				JDialog d = new JDialog(f, true);
				d.add(overforAgressoPanel);
				d.setSize(850, 600);
				d.setLocationRelativeTo(plugin.getModuleGUI());
				d.setVisible(true);

				if (!overforAgressoPanel.isCancelled()) {
					driftslogg.sendToIO(overforAgressoPanel.getSelectedEntities(), mnd, aar, plugin.getLoader().getLoggedOnUserSign());
					driftslogg.flagActivitiesAsProcessed(overforAgressoPanel.getNonTransferableActivities(), mnd, aar);
					driftslogg.saveProcessedActivities(overforAgressoPanel.getNonTransferableActivities());
				} else {
					return;
				}

				setReloadTable(true);
			}
		};
	}

	public List<MipssInputTreeView> getArtiklerTilAgresso(Long mnd, Long aar, List<Long> perioderSomManglerAttestant){
		return driftslogg.getAktiviteterTilAgresso(getQueryParams().getKontrakt().getId(), mnd, aar, perioderSomManglerAttestant, plugin.getLoader().getLoggedOnUserSign());
	}

	public List<MipssInputTreeView> getArtiklerIkkeTilAgresso(Long mnd, Long aar, List<Long> perioderSomManglerAttestant){
		return driftslogg.getAktiviteterIkkeOverfores(getQueryParams().getKontrakt().getId(), mnd, aar, perioderSomManglerAttestant, plugin.getLoader().getLoggedOnUserSign());
	}

	private boolean sjekkBrukerSomAttestant(List<PeriodeKontrakt> ikkeGodkjent){
		if (ikkeGodkjent!=null&& !ikkeGodkjent.isEmpty()){//det finnes kontrakter som mangler attestant/vikar
			List<String> levkontraktIdent = new ArrayList<>();
			for (PeriodeKontrakt p:ikkeGodkjent){
				if (!levkontraktIdent.contains(p.getLevkontrakt())){
					levkontraktIdent.add(p.getLevkontrakt());
				}
			}
			String msg = Resources.getResource("message.attestantmangler",StringUtils.join(levkontraktIdent, "\n"));
			int v = JOptionPane.showConfirmDialog(plugin.getModuleGUI(), msg, Resources.getResource("message.attestantmangler.title"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
			if (v!=JOptionPane.YES_OPTION){
				return false;
			}
		}
		return true;
	}

	/**
	 * Returnerer en liste med PeriodeKontrakter der innlogget bruker ikke er godkjent som attestant eller vikar
	 *
	 * @param perioder
	 * @return
	 */
	private List<PeriodeKontrakt> sjekkAttestering(List<PeriodeKontrakt> perioder){
		String bruker = plugin.getLoader().getLoggedOnUserSign();
		List<PeriodeKontrakt> ikkeGodkjent = new ArrayList<>();
		for (PeriodeKontrakt p:perioder){
			boolean sjekkAttestering = driftslogg.sjekkAttestering(bruker, p.getLevkontrakt());
			if (!sjekkAttestering){
				ikkeGodkjent.add(p);
			}
		}
		return ikkeGodkjent;
	}

	private void createMengdeRapport(Date fraDato, Date tilDato, MENGDERAPPORT_TYPE type){
		DriftsloggRapportData rapport;
		try {
			rapport = driftslogg.getRapport(getQueryParams(), fraDato, tilDato);
			if (rapport==null||rapport.size()==0){
				JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.ingenmengder"), Resources.getResource("message.ingenmengder.title"), JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			boolean harR12 = driftslogg.sjekkR12(getQueryParams());
			if (!harR12){
				JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.manglerr12"), Resources.getResource("message.manglerr12.title"), JOptionPane.INFORMATION_MESSAGE);
			}
			if (harR12 && rapport.getR12rapport().isEmpty()){
				JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.manglerr12.produksjon"), Resources.getResource("message.manglerr12.produksjon.title"), JOptionPane.INFORMATION_MESSAGE);
			}
			new MengdeRapport(getQueryParams(), rapport, type, fraDato, tilDato).finalizeAndOpen();
		} catch (NoArtikkelException e) {
			ErrorHandler.showError(e, Resources.getResource("message.ingenmengder.error"));
			e.printStackTrace();
		} catch (Throwable t){
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten", EpostUtils.getSupportEpostAdresse()));
			t.printStackTrace();
		}
	}

	public Action getOpenGrafAction(Trip selectedTrip){
		return new AbstractAction(Resources.getResource("button.graf"), IconResources.GRAF_ICON_SMALL){
			@Override
			public void actionPerformed(ActionEvent e) {
				openGraf(selectedTrip);
			}
		};
	}

	private void openGraf(Trip selectedTrip) {
		Long vehicleId = selectedTrip.getVehicleId();
		Date fromDateTime = selectedTrip.getStartTime();
		Date toDateTime = selectedTrip.getEndTime();

		if (vehicleId == null) {
			JOptionPane.showMessageDialog(
					null,
					Resources.getResource("message.manglerKjoretoyId"),
					Resources.getResource("message.manglerKjoretoyId.title"),
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		OpenSingleChartMessage m = new OpenSingleChartMessage(vehicleId, fromDateTime, toDateTime, null);
		m.setTargetPlugin(plugin.getLoader().getPlugin(ChartModule.class));
		plugin.getLoader().sendMessage(m);
	}

	public boolean visBekreftMengde(Double mengde) {
		String message = Resources.getResource("message.bekreftMengde", String.valueOf(mengde));
		String title = Resources.getResource("message.bekreftMengde.title");

		int val = JOptionPane.showConfirmDialog(getPlugin().getModuleGUI(), message, title, JOptionPane.YES_NO_OPTION);
		return val == JOptionPane.YES_OPTION;
	}

	public String getAgressoOverforingDatoString(){
		Date agressoOverforingDato = AgressoUtil.getAgressoOverforingDato();
		SimpleDateFormat f = new SimpleDateFormat("EEEE d. MMM 'kl.' HH:mm");
		return f.format(agressoOverforingDato);
	}

	public Action getFastprisSjekkAction(final JPanel parent) {
		return new AbstractAction(Resources.getResource("button.fastprissjekk", " .. "), IconResources.SAKSBEHANDLING_ICON_16){
			public void actionPerformed(ActionEvent e){

				JDialog d = new JDialog(plugin.getLoader().getApplicationFrame());

				d.setSize(700, 600);
				d.setLocationRelativeTo(plugin.getModuleGUI());
				final FastprisPanel pnl = new FastprisPanel(DriftsloggController.this);
				d.add(pnl);
				d.addWindowListener(new WindowAdapter(){
					public void windowClosing(WindowEvent e){
						pnl.dispose();
					}
				});

				d.setVisible(true);
			}
		};
	}

	public List<FastprisStatus> getFastpriserForKontrakt(){
		Date uke = queryParams.getMonth();
		Calendar c = Calendar.getInstance();
		c.setTime(uke);
		int mnd = c.get(Calendar.MONTH);
		int aar = c.get(Calendar.YEAR);

		return driftslogg.sjekkFastpris(queryParams.getKontrakt(), (long) mnd, (long) aar);
	}

	private List<PeriodeSok> sokPerioderMedParams(Date uke, String levkontraktIdent) {
		return  driftslogg.sokPeriodeStatusMedParams(uke, levkontraktIdent);
	}

	public Action getSlettAktivitetAction(final JMipssBeanTable<Aktivitet> tblAktivitet, final MipssBeanTableModel<Aktivitet> mdlAktivitet) {
		return new AbstractAction(Resources.getResource("button.slett"), IconResources.REMOVE_ITEM_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Aktivitet> selectedEntities = tblAktivitet.getSelectedEntities();
				slettAktiviteter(selectedEntities, mdlAktivitet);
			}
		};
	}

	private void slettAktiviteter(List<Aktivitet> aktiviteter, MipssBeanTableModel<Aktivitet> mdlAktivitet){
		int rvl = JOptionPane.showConfirmDialog(SwingUtilities.windowForComponent(tblTrip), Resources.getResource("message.slettFastpris"), Resources.getResource("message.slettFastpris.title"), JOptionPane.YES_NO_OPTION);
		if (rvl == JOptionPane.YES_OPTION){
			for (Aktivitet a:aktiviteter){
				slettAktivitet(a);
				if (mdlAktivitet!=null){
					mdlAktivitet.removeItem(a);
				}
			}
		}
	}

	private void slettAktivitet(Aktivitet aktivitet){
		driftslogg.removeAktivitet(aktivitet);
	}

	public Action getOpenProdrappAction(ActivitiesOverviewPanel activityPanel) {
		return new AbstractAction(Resources.getResource("button.prodrapp"), IconResources.EXCEL_ICON16){
			@Override
			public void actionPerformed(ActionEvent e) {
				openProdrapp(activityPanel);
			}
		};
	}

	private void openProdrapp(ActivitiesOverviewPanel activityPanel){
		List<AktivitetTreeView> selected = activityPanel.getActivityTable().getSelectedEntities();
		if(!selected.isEmpty()) {
			AktivitetTreeView aktivitet = selected.get(0);
			Long kjoretoyId = aktivitet.getAktivitet().getKjoretoyId();
			Date fraDatoTid = aktivitet.getAktivitet().getFraDatoTid();
			Date tilDatoTid = aktivitet.getAktivitet().getTilDatoTid();
			Long kontraktId = getQueryParams().getKontrakt().getId();

			if (kjoretoyId == null) {
				JOptionPane.showMessageDialog(null, Resources.getResource("message.manglerKjoretoyId"), Resources.getResource("message.manglerKjoretoyId.title"), JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			MipssPlugin target = plugin.getLoader().getPlugin(ExcelreportModule.class);
			OpenDetaljertProdrapportMessage m = new OpenDetaljertProdrapportMessage(kontraktId, kjoretoyId, fraDatoTid, tilDatoTid);
			m.setTargetPlugin(target);
			plugin.getLoader().sendMessage(m);
		} else {
			JOptionPane.showMessageDialog(null, Resources.getResource("message.noSelection"), Resources.getResource("message.noSelectionTitle"), JOptionPane.ERROR_MESSAGE);
		}
	}

	public List<Stroprodukt> getStroprodukter() {
		return driftslogg.getAllStroprodukter();
	}

	public Action getVaerRapportAction() {
		return new AbstractAction("Værrapport", IconResources.EXCEL_ICON16) {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new JDialog(plugin.getLoader().getApplicationFrame(), true);
				RapportPeriodePanel pnl = new RapportPeriodePanel(DriftsloggController.this);
				dialog.add(pnl);
				dialog.setSize(350,250);
				dialog.setLocationRelativeTo(plugin.getModuleGUI());
				dialog.setVisible(true);
				if (pnl.isCancelled()){
					return;
				}
				final Date fraDato = pnl.getFraDato();
				final Date tilDato = pnl.getTilDato();
				//TODO
				final JDialog d = displayWait();
				d.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						try{
							createVaerRapport(fraDato, tilDato);
						}catch (Exception e){
							e.printStackTrace();
						}
						return null;
					}

					@Override
					protected void done(){
						d.dispose();
					}
				}.execute();
			}
		};
	}

	public Action getUkjentStroproduktAction() {
		return new AbstractAction(Resources.getResource("button.ukjentStroprodukt"), IconResources.WARNING16) {

			@Override
			public void actionPerformed(ActionEvent e) {
				createUkjentStroproduktRapport();
			}
		};
	}

	private void createVaerRapport(Date fraDato, Date tilDato) {

		List<VaerRapportObject> data;
		try {
			data = driftslogg.getVaerRapport(getQueryParams(), fraDato, tilDato);
			new VaerRapport(getQueryParams(), data, fraDato, tilDato).finalizeAndOpen();
		} catch (Throwable t){
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten", EpostUtils.getSupportEpostAdresse()));
			t.printStackTrace();
		}
	}

	private void createUkjentStroproduktRapport() {
		try {
			// TODO: Needs to be rewritten for trip/activity
			/*List<AktivitetTreeView> selected = tblAktivitet.getSelectedEntities();
			AktivitetTreeView aktivitet = selected.get(0);
			Prodrapport rapport = BeanUtil.lookup(Prodrapport.BEAN_NAME, Prodrapport.class);
			List<UkjentStroprodukt> ukjenteStroprodukter = rapport.genererUkjentStroproduktRapport(null, aktivitet.getFraDatoTid(), aktivitet.getTilDatoTid(), aktivitet.getAktivitet().getKjoretoyId());
			if (ukjenteStroprodukter.isEmpty()) {
				JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(plugin.getModuleGUI()), "Fant ingen data for utvalget", "Ingen data funnet", JOptionPane.INFORMATION_MESSAGE);
			} else{
				new UkjentStroproduktRapport(ukjenteStroprodukter, aktivitet.getFraDatoTid(), aktivitet.getTilDatoTid(), aktivitet.getAktivitet().getKjoretoyId()).finalizeAndOpen();
			}*/
		} catch (Throwable t) {
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten", EpostUtils.getSupportEpostAdresse()));
			t.printStackTrace();
		}
	}

	/**
	 * Oppretter en rapport som benytter ExportToExcel for å vise data fra driftsloggen.
	 * Inneholder en egen model og treetable som kun benyttes for å kunne mate data til eksport-funskjonen
	 *
	 * @param fraDato
	 * @param tilDato
	 */
	private void createExportToExcelEgendef(Date fraDato, Date tilDato) {
        tripsToExport = new TripTableModelExportEgendef(this, fraDato, tilDato);
		tblAktivitetForEksport = new JMipssTreeTable<>(tripsToExport);
		tblAktivitetForEksport.setColumnModel(getTripColumnModel());

		tripsToExport.addTableLoaderListener(new MipssBeanLoaderListener() {

			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {

			}

			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				ExportToExcel excel = new ExportToExcel(tblAktivitetForEksport);
				File excelFile = excel.exportToExcel();
				DesktopHelper.openFile(excelFile);

			}
		});

		tripsToExport.loadData(this);
	}

	public List<Aktivitet> getExportEgendefAktiviteter(DriftsloggQueryParams queryParams, Date fraDato, Date tilDato) {
		List<Aktivitet> data;
		try{
			data = driftslogg.getExportToExcelEgendefRapport(getQueryParams(), fraDato, tilDato);
			return data;
		}catch (Throwable t) {
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten", EpostUtils.getSupportEpostAdresse()));
			t.printStackTrace();
			return null;
		}
	}

	public String getLeverandorerSomIkkeSkalOverfores(){

		List<Leverandor> ikkeOverfores = driftslogg.getLeverandorerSomIkkeSkalOverfores(queryParams.getKontrakt().getId());

		if (ikkeOverfores == null || ikkeOverfores.size() == 0) {
			return "";
		} else{
			StringBuilder sb = new StringBuilder();
			sb.append("<li>Aktiviteter fra ");

			for (int i = 0; i < ikkeOverfores.size(); i++) {
				if (i != 0) {
					sb.append(", ");
				}
				Leverandor leverandor = ikkeOverfores.get(i);
				sb.append(leverandor.getNr()).append(" (").append(leverandor.getNavn()).append(")");
			}
			sb.append("</li>");
			return sb.toString();
		}
	}

    public Action getApprovedAmountsReport() {
        return new AbstractAction(Resources.getResource("button.amountReport"), IconResources.EXCEL_ICON16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JDialog d = displayWait();
                d.setVisible(true);
                new SwingWorker<Void, Void>(){
                    @Override
                    protected Void doInBackground() throws Exception {
						Date fraDato = CalendarHelper.getFirstDateInMonth(getQueryParams().getMonth());
						Date tilDato = CalendarHelper.getLastTimeInMonth(getQueryParams().getMonth());

						createApprovedAmountsReport(fraDato, tilDato);
                        return null;
                    }
                    @Override
                    protected void done(){
                        d.dispose();
                    }
                }.execute();
            }
        };
    }

	public Action getOutsideTripReport() {
		return new AbstractAction(Resources.getResource("button.outsideTripReport"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JDialog waitdialog = displayWait();
				waitdialog.setVisible(true);

				new SwingWorker<Void, Void>() {

					@Override
					protected Void doInBackground() throws Exception {
						Date fromDate = CalendarHelper.getFirstDateInMonth(getQueryParams().getMonth());
						Date toDate = CalendarHelper.getLastTimeInMonth(getQueryParams().getMonth());
						createOutsideTripReport(fromDate, toDate);
						return null;
					}
					@Override
					protected void done() {
						waitdialog.dispose();
					}
				}.execute();
			}
		};
	}

    private void createApprovedAmountsReport(Date fraDato, Date tilDato) {
        try {
            List<ApprovedAmountData> data = driftslogg.getApprovedAmounts(getQueryParams(), fraDato, tilDato);

            if (data == null || data.size() == 0 ) {
                JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.ingenmengder"),
                        Resources.getResource("message.ingenmengder.title"), JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            new ApprovedAmountsReport(getQueryParams(), data, fraDato, tilDato).finalizeAndOpen();
        } catch (Throwable t){
            ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten",
                    EpostUtils.getSupportEpostAdresse()));
            log.error("Could not retrieve approved amounts report", t);
        }
    }

    private void createOutsideTripReport(Date fromDate, Date toDate) {
		try {
			List<OutsideTripData> data = driftslogg.getAmountOutsideTrip(getQueryParams(), fromDate, toDate);

			if(data==null || data.size()==0) {
				JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.outsideTrip.noData"),
						Resources.getResource("message.outsideTrip.noData.title"), JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			new OutsideTripReport(getQueryParams(), data, fromDate, toDate).finalizeAndOpen();
		} catch (Throwable t) {
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten", EpostUtils.getSupportEpostAdresse()));
			log.error("Could not retrieve outside trip report", t);
		}
	}

	public List<PeriodeSok> sokPerioder(){
		List<PeriodeSok> sokPeriodeStatus = driftslogg.sokPeriodeStatus(getQueryParams());
		return sokPeriodeStatus;
	}

	public Action getDeviantActivitiesReport() {
		return new AbstractAction(Resources.getResource("button.deviantReport"), IconResources.EXCEL_ICON16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JDialog d = displayWait();
				d.setVisible(true);
				new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						Date fraDato = CalendarHelper.getFirstDateInMonth(getQueryParams().getMonth());
						Date tilDato = CalendarHelper.getLastTimeInMonth(getQueryParams().getMonth());

						createDeviantActivitiesReport(fraDato, tilDato);
						return null;
					}
					@Override
					protected void done(){
						d.dispose();
					}
				}.execute();
			}
		};
	}

	private void createDeviantActivitiesReport(Date fraDato, Date tilDato) {
		try {
			List<DeviantActivity> data = driftslogg.getDeviantActivities(getQueryParams(), fraDato, tilDato);

			if (data == null || data.size() == 0 ) {
				JOptionPane.showMessageDialog(getPlugin().getModuleGUI(), Resources.getResource("message.ingenmengder"),
						Resources.getResource("message.ingenmengder.title"), JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			new DeviantActivitiesReport(getQueryParams(), data, fraDato, tilDato).finalizeAndOpen();
		} catch (Throwable t){
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten",
					EpostUtils.getSupportEpostAdresse()));
			log.error("Unable to create deviant activities report", t);
		}
	}

	public void automaticallyProcessTrip(Trip trip) {
		trip.setProcessStatusId(TripProcessStatus.PROCESSED_BY_CONTRACTOR.getId());
		updateTrip(trip);
		setReloadTable(true);
	}

	public List<Trip> getTripsForPeriodId(Long periodId) {
		return driftslogg.getTripsForPeriodId(periodId);
	}

	public void closePeriods(List<Periode> periods) {
		driftslogg.closePeriods(getPeriodStatusesFromPeriods(periods));
	}

	public void openPeriods(List<Periode> periods) {
		driftslogg.openPeriods(getPeriodStatusesFromPeriods(periods));
	}

	private List<PeriodeStatus> getPeriodStatusesFromPeriods(List<Periode> periods) {
		List<PeriodeStatus> periodStatuses = new ArrayList<>();

		String email = getPlugin().getLoader().getLoggedOnUser(true).getEpost();

		for (Periode p : periods) {
			PeriodeStatus ps = new PeriodeStatus();
			ps.setOpprettetDato(Clock.now());
			ps.setPeriode(p);
			ps.setOpprettetAv(email);

			periodStatuses.add(ps);
		}

		return periodStatuses;
	}

    public List<AgrArtikkelV> getArticlesForTrip(Trip trip) {
	    return driftslogg.getArticlesForTrip(trip);
    }

    public List<Prodtype> getProdTypesForDropdown() {
		List<Prodtype> prodTypes = driftslogg.getProdtypes();
		List<Long> approvedIds = Arrays.asList(
				ManagerConstants.ProdType.BROYTING.getId(),
				ManagerConstants.ProdType.FEIING.getId(),
				ManagerConstants.ProdType.KLIPPING.getId(),
				ManagerConstants.ProdType.RENHOLD.getId(),
				ManagerConstants.ProdType.SIDEPLOG.getId(),
				ManagerConstants.ProdType.SNORYDDING.getId(),
				ManagerConstants.ProdType.TUNG_HOVLING.getId());

		// Only keep the approved choices
		prodTypes.removeIf(prodType -> !approvedIds.contains(prodType.getId()));

		List<Stroprodukt> gritProducts = driftslogg.getAllStroprodukter();

		// Add all the possible gritting products
		for(Stroprodukt gritProduct : gritProducts) {
			if(gritProduct.getId() >= 1 && gritProduct.getId() <= 7) {
				prodTypes.add(new Prodtype(5L, "Strøing : " + gritProduct.getNavn(), true));
			}
		}

		prodTypes.sort(Comparator.comparing(Prodtype::getNavn));

		// Don't sort this one! We want it to appear at the top of the list regardless of the other production types.
		prodTypes.add(0, new Prodtype(-1L, "Annet", false));

		return prodTypes;
	}

	public List<Prodtype> getProdtypes() {
		return driftslogg.getProdtypes();
	}

	public Trip updateTrip(Trip trip) {
		return driftslogg.persistTrip(trip);
	}

	public Stroproduktmengde addOrUpdateGritProductAmount(Stroproduktmengde gritProductAmount) {
	    return driftslogg.addOrUpdateGritProductAmount(gritProductAmount);
    }

    public Aktivitet addOrUpdateActivity(Aktivitet a) {
	    return driftslogg.addOrUpdateActivity(a);
    }

    public TripReturnCause addTripReturnCause(Long tripId, String cause) {
		return driftslogg.addTripReturnCause(new TripReturnCause(
				tripId, new Date(), plugin.getLoader().getLoggedOnUser(true).getSignatur(), cause));
	}

	public boolean sendEmail(Trip trip, String cause) {
		Bruker user = plugin.getLoader().getLoggedOnUser(true);

		String contractorEmail = findContractorEmail(trip);

		if (contractorEmail != null) {
			String baseUrl = KonfigparamPreferences.getInstance().hentEnForApp("driftslogg.web", "trip_return_email_url").getVerdi();

			StringBuilder message = new StringBuilder();
			message.append("En økt er blitt sendt tilbake for ny behandling i Driftslogg web.<br /><br />");

			message.append("Melding fra ").append(user.getNavn()).append(":<br />");
			message.append(cause).append("<br /><br />");

			message.append("Gjelder økt ").append(trip.getId()).append(":<br />");
			message.append("Starttidspunkt ").append(MipssDateFormatter.formatDate(trip.getStartTime())).append("<br />");
			message.append("Kjøretøy ").append(trip.getVehicle().getEksterntNavn()).append("<br /><br />");

			message.append("<a href=\"");
			message.append(baseUrl).append("/editApproval");
			message.append("?tripId=").append(trip.getId());
			message.append("&leverandorNr=").append(trip.getVendorId());
			message.append("&levkontraktId=").append(trip.getContractorContract().getLevkontraktIdent());
			message.append("\">Trykk her for å gå direkte til økten</a>");

			driftslogg.sendMail(
					user.getEpost(),
					contractorEmail,
					"Melding om tilbakesendt økt i Driftslogg web (" + trip.getId() + ")", message.toString());

			return true;
		} else {
			return false;
		}
	}

	private String findContractorEmail(Trip trip) {
		KontraktLeverandor contractor = driftslogg.getContractContractor(trip.getContractorContract().getDriftkontraktId(), trip.getVendorId());

		if (contractor != null) {
			if (contractor.getEpostAdresse() != null) {
				return contractor.getEpostAdresse();
			} else if (contractor.getKopiEpostAdresse() != null) {
				return contractor.getKopiEpostAdresse();
			}
		}

		return null;
	}

	public void openTripInMap(Trip trip) {
		MipssPlugin target;
		try {
			target = plugin.getLoader().getPlugin((Class<? extends MipssPlugin>) Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));

			MipssMapTripMessage message = new MipssMapTripMessage(trip);
			message.setTargetPlugin(target);

			plugin.getLoader().sendMessage(message);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public List<KjoretoyRode> getVehicleRodeConnectionsForVehicle(Long vehicleId) {
		return driftslogg.getVehicleRodeConnectionsForVehicle(queryParams.getKontrakt().getId(), vehicleId);
	}

}