package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.driftslogg.util.ActivityUtil;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class TableDirtyActivityHighlightPredicate implements HighlightPredicate {

    private final AktivitetTableModel model;
    private ActivityUtil activityUtil;

    public TableDirtyActivityHighlightPredicate(AktivitetTableModel mdlAktivitet, ActivityUtil activityUtil) {
        this.model = mdlAktivitet;
        this.activityUtil = activityUtil;
    }

    @Override
    public boolean isHighlighted(Component component, ComponentAdapter componentAdapter) {
        return isHighlighted(componentAdapter.row);
    }

    boolean isHighlighted(int row) {
        Aktivitet activity = model.getAktiviteter().get(row);
        return activity.isArticleChanged() ||  activity.hasNoPayment() || activityUtil.isProductionOutsideConnectedRode(activity);
    }

}