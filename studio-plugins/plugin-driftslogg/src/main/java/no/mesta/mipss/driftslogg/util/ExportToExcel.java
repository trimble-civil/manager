package no.mesta.mipss.driftslogg.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.table.TableColumn;

import jxl.CellView;
import jxl.Workbook;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static no.mesta.mipss.common.TripProcessStatus.*;
import static no.mesta.mipss.driftslogg.ui.TripTableModel.*;

public class ExportToExcel {

	private static final Logger log = LoggerFactory.getLogger(ExportToExcel.class);

	private final JMipssTreeTable treeTable;
	private final Map<Class<?>, WritableCellFormat> formats = new HashMap<>();
	public ExportToExcel(JMipssTreeTable treeTable){
		this.treeTable = treeTable;
		formats.put(Integer.class, new WritableCellFormat(NumberFormats.INTEGER));
		formats.put(Date.class, new WritableCellFormat(new DateFormat(MipssDateFormatter.SHORT_DATE_TIME_FORMAT)));
		formats.put(Double.class, new WritableCellFormat(NumberFormats.FLOAT));
	}
	
		
	/**
	 * Eksporterer innholdet i denne tabellen til Excel format
	 * 
	 * @return
	 */
	public File exportToExcel() {
		File tempFile = createTempFile("tabellEksport", ".xls");
		WritableWorkbook workbook = createWorkbook(tempFile);
		WritableSheet sheet = workbook.createSheet("Tittel", 0);
		writeExcelHeaders(sheet);
		writeExcelData(sheet);
		closeWorkbook(workbook);

		return tempFile;
	}
	private void closeWorkbook(WritableWorkbook workbook) {
		try {
			workbook.write();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		try {
			workbook.close();
		} catch (WriteException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	/**
	 * Skriv denne tabellen til Excelarket
	 * @param sheet Excelarket som skal skrives i
	 */
	private void writeExcelData(WritableSheet sheet) {
		MipssRenderableEntityTableColumnModel columnModel = (MipssRenderableEntityTableColumnModel) treeTable.getColumnModel();

		int col = 0;
		for (TableColumn column : columnModel.getColumns(true)) {
			TableColumnExt columnEx = columnModel.getColumnExt(col);

			if (!columnEx.isVisible()) {
				continue;
			}

			int colWidth = 0;
			Class<?> clazz = treeTable.getColumnClass(col);
			int row = 1;

			log.debug("Processing column '{}' ({}) of type '{}'", column.getHeaderValue(), col, clazz.getSimpleName());
			
			for (int i = 0; i < treeTable.getRowCount(); i++) {
				
				Object data = treeTable.getValueAt(i, col);
				
				if (data != null) {
					if (clazz.equals(String.class)) {
						addString(sheet, col, row, (String) data);
					} else if (clazz.equals(Long.class)) {
						addLong(sheet, col, row, (Long) data);
					} else if (clazz.equals(Double.class)) {
						addDouble(sheet, col, row, (Double) data);
					} else if (clazz.equals(Integer.class)) {
						addInteger(sheet, col, row, (Integer) data);
					} else if (clazz.equals(Date.class)) {
						addDate(sheet, col, row, (Date) data);
					} else if (clazz.equals(Boolean.class)) {
						addBoolean(sheet, col, row, (Boolean) data);
					} else if (clazz.equals(Trip.class)) {
						handleTrip(sheet, col, row, (Trip) data);
					} else if (clazz.equals(TripProcessStatus.class)) {
						addString(sheet, col, row, ((TripProcessStatus)data).getText());
                    } else{
						try{
							addInteger(sheet, col, row, Integer.parseInt(data.toString()));
						}catch (NumberFormatException e){
							try{
								addDouble(sheet, col, row, Double.parseDouble(data.toString()));
							}catch (NumberFormatException ex){
								try{
									excelAddCell(sheet, new Label(col, row, data.toString()));
								} catch (Throwable e1){
									e1.printStackTrace();
								}
							}
						}
					}
					
					colWidth = computeNewColWidth(colWidth, data);
				}
				row++;
			}
			CellView cellView = sheet.getColumnView(col);
			if (colWidth > (cellView.getSize() / 256)) {
				sheet.setColumnView(col, colWidth);
			}
			col++;
		}
	}

    private void handleTrip(WritableSheet sheet, int col, int row, Trip trip) {
        switch(col) {
			case COL_PERIOD_STATUS:
				if(trip.getPeriod() != null && trip.getPeriod().getStatus()!= null) {
					addString(sheet, col, row, String.valueOf(trip.getPeriod().getStatus().getStatus().getStatus()));
				} else {
					addString(sheet, col, row, "Ukjent");
				}
				break;
			case COL_PLATFORM:
				addString(sheet, col, row, trip.getPlatform().getIdentifier());
				break;
            case COL_WARNINGS:
                addString(sheet, col, row, trip.getWarningString());
                break;
            case COL_PROCESS_TRIP:
                int level = trip.getProcessStatus().getLevel();
                if(level >PROCESSED_BY_CONTRACTOR.getLevel() && level < UNKNOWN.getLevel()) {
                    addString(sheet, col, row, "Behandlet");
                } else {
                    addString(sheet, col, row, "Ubehandlet");
                }
                break;
        }
    }

    private void addString(WritableSheet sheet, int col, int row, String data) {
		excelAddCell(sheet, new Label(col, row, data));
	}

	private void addBoolean(WritableSheet sheet, int col, int row, Boolean data) {
		Boolean value = data;
		Integer number = value != null && Boolean.TRUE.equals(value) ? 1 : 0;
		excelAddCell(sheet, new jxl.write.Number(col, row, number , formats.get(Integer.class)));
	}

	private void addDate(WritableSheet sheet, int col, int row, Date data) {
		excelAddCell(sheet, new DateTime(col, row, data, formats.get(Date.class)));
	}

	private void addInteger(WritableSheet sheet, int col, int row, Integer data) {
		excelAddCell(sheet, new jxl.write.Number(col, row, data, formats.get(Integer.class)));
	}

	private void addDouble(WritableSheet sheet, int col, int row, Double data) {
		excelAddCell(sheet, new jxl.write.Number(col, row, data, formats.get(Double.class)));
	}

	private void addLong(WritableSheet sheet, int col, int row, Long data) {
		excelAddCell(sheet, new jxl.write.Number(col, row, data, formats.get(Integer.class)));
	}

	private int computeNewColWidth(int colWidth, Object data) {
		int width = 0;
		if (data instanceof String) {
			width = ((String) data).length();
			width = width > 200 ? 200 : width;
		} else if (data instanceof Long) {
			width = String.valueOf((Long) data).length();
		} else if (data instanceof Double) {
			width = String.valueOf((Double) data).length();
		} else if (data instanceof Integer) {
			width = String.valueOf((Integer) data).length();
		} else if (data instanceof Date) {
			width = MipssDateFormatter.SHORT_DATE_TIME_FORMAT.length();
		} else if (data instanceof Boolean) {
			width = 2;
		}

		return Math.max(colWidth, width);
	}
	private File createTempFile(String prefix, String suffix) {
		File tempFile = null;
		try {
			tempFile = File.createTempFile(prefix, suffix);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return tempFile;
	}
	private WritableWorkbook createWorkbook(File tempFile) {
		WritableWorkbook workbook = null;
		try {
			workbook = Workbook.createWorkbook(tempFile);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return workbook;
	}
	private void writeExcelHeaders(WritableSheet sheet) {
		TableColumnModelExt columnModel = (TableColumnModelExt) treeTable.getColumnModel();
		for (int columnIndex = 0; columnIndex < columnModel.getColumnCount(true); columnIndex++) {
			TableColumnExt tc = (TableColumnExt) columnModel.getColumn(columnIndex);
			if (tc.isVisible()) {
				WritableFont times10font = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, true);
				WritableCellFormat times10format = new WritableCellFormat(times10font);
				String title = (String) tc.getHeaderValue();
				title = removeHtmlFormatting(title);
				if(title.isEmpty()) {
					switch(columnIndex) {
						case COL_PERIOD_STATUS: title = "Periodestatus"; break;
						case COL_PLATFORM: title = "Plattform"; break;
					}
				}
				excelAddCell(sheet, new Label(columnIndex, 0, title, times10format));
				sheet.setColumnView(columnIndex, title.length());
			}
		}
	}
	private static String removeHtmlFormatting(String title){
		String htmlRemoved = title.replaceAll("\\<.*?\\>", "");
		return htmlRemoved;
	}
	private void excelAddCell(WritableSheet sheet, WritableCell cell) {
		try {
			sheet.addCell(cell);
		} catch (RowsExceededException e) {
			throw new IllegalStateException(e);
		} catch (WriteException e) {
			throw new IllegalStateException(e);
		}
	}
}
