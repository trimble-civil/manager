package no.mesta.mipss.driftslogg.ui.detaljui;

import no.mesta.mipss.driftslogg.ui.AktivitetTableModel;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.ui.table.NumberCellRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Hashtable;
import java.util.Map;
import java.util.function.Predicate;

public class StrikethroughNumberCellRenderer extends NumberCellRenderer {

    private AktivitetTableModel model;
    private int columnToStrikethrough;
    private Predicate<Aktivitet> strikethroughPredicate;

    public StrikethroughNumberCellRenderer(AktivitetTableModel model, Predicate<Aktivitet> strikethroughPredicate) {
        this.model = model;
        this.strikethroughPredicate = strikethroughPredicate;
    }

    /** {@inheritDoc} */
    public Component getTableCellRendererComponent(JTable table, 
                                                   Object value, 
                                                   boolean isSelected, 
                                                   boolean hasFocus, 
                                                   int row, int column) {
        Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (renderer instanceof JLabel) {
            JLabel label = (JLabel) renderer;
            Aktivitet aktivitet = model.getAktiviteter().get(row);

            boolean shouldStrikethrough = !strikethroughPredicate.test(aktivitet);

            if (shouldStrikethrough) {
                label.setFont(getStrikthroughFont(label.getFont()));
                return renderer;
            }
        }

        return renderer;
    }

    private Font getStrikthroughFont(Font labelFont) {
        Map<TextAttribute, Object> map = new Hashtable<>();
        map.put(TextAttribute.FONT, labelFont);
        map.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);

        return Font.getFont(map);
    }

}