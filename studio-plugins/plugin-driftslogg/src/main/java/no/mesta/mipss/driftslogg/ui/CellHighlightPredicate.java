package no.mesta.mipss.driftslogg.ui;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class CellHighlightPredicate implements HighlightPredicate {

    Set<Position> positions;

    public CellHighlightPredicate() {
        this.positions = new HashSet<>();
    }

    @Override
    public boolean isHighlighted(Component component, ComponentAdapter adapter) {
        for(Position position : positions) {
            if(position.row==adapter.row && position.column==adapter.column) {
                return true;
            }
        }
        return false;
    }

    public void addHighlight(int row, int column) {
        positions.add(new Position(row, column));
    }

    public void removeHighlight(int row, int column) {
        for (Position position : positions) {
            if (position.row == row && position.column == column) {
                positions.remove(position);
                break;
            }
        }
    }

    public void removeHighlights() {
        positions.clear();
    }

    private class Position {
        final int row;
        final int column;

        public Position(int row, int column) {
            this.row = row;
            this.column = column;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Position position = (Position) o;

            if (row != position.row) return false;
            return column == position.column;
        }

        @Override
        public int hashCode() {
            int result = row;
            result = 31 * result + column;
            return result;
        }
    }
}
