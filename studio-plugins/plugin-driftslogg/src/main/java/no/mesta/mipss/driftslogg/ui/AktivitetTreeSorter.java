package no.mesta.mipss.driftslogg.ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.TableColumn;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

public class AktivitetTreeSorter {
	public enum SORTBY{
		ID,
		LEVERANDOR,
		KJORETOY,
		RODE,
		ARBEID,
		MENGDE, 
		GODKJENT, 
		ENHETSPRIS
	}
	private final AktivitetTableModel model;
	private final JMipssTreeTable<AktivitetTreeView> treeTable;
	private Map<Integer, AktivitetComparator> columnSorters = new HashMap<Integer, AktivitetComparator>();
	private final MipssRenderableEntityTableColumnModel columnModel;
	
	public AktivitetTreeSorter(AktivitetTableModel model, JMipssTreeTable<AktivitetTreeView> treeTable, MipssRenderableEntityTableColumnModel columnModel){
		this.model = model;
		this.treeTable = treeTable;
		this.columnModel = columnModel;
		initSorter();
	}

	private void initSorter() {
		treeTable.getTableHeader().addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				
				int index = treeTable.columnAtPoint(e.getPoint());
				index = treeTable.convertColumnIndexToModel(index);
				//int index = tblAktivitet.getColumnModel().getColumnIndexAtX(e.getX());
				List<Aktivitet> aktiviteter = model.getAktiviteter();
				AktivitetComparator comparatorForSelectedColumn = columnSorters.get(index);
				if (comparatorForSelectedColumn==null||aktiviteter==null)
					return;
				toggle(comparatorForSelectedColumn, aktiviteter);
				model.fireTreeChanged();
			}
		});
		
		addComparators();
	}
	private void addComparators(){
		columnSorters.put(AktivitetTableModel.COL_ID, new AktivitetComparator(SORTBY.ID, AktivitetTableModel.COL_ID));
		columnSorters.put(AktivitetTableModel.COL_RODE, new AktivitetComparator(SORTBY.RODE, AktivitetTableModel.COL_RODE));
		columnSorters.put(AktivitetTableModel.COL_ARTICLE, new AktivitetComparator(SORTBY.ARBEID, AktivitetTableModel.COL_ARTICLE));
		columnSorters.put(AktivitetTableModel.COL_UE_AGREED_AMOUNT, new AktivitetComparator(SORTBY.MENGDE, AktivitetTableModel.COL_UE_AGREED_AMOUNT));
		columnSorters.put(AktivitetTableModel.COL_UE_AGREED_AMOUNT, new AktivitetComparator(SORTBY.GODKJENT, AktivitetTableModel.COL_UE_AGREED_AMOUNT));
		columnSorters.put(AktivitetTableModel.COL_SUM, new AktivitetComparator(SORTBY.ENHETSPRIS, AktivitetTableModel.COL_SUM));
	}
	
	private void toggle(AktivitetComparator comp, List<Aktivitet> aktiviteter){
		comp.toggle();
		Collections.sort(aktiviteter, comp);
		for (Integer i:columnSorters.keySet()){
			AktivitetComparator aktivitetComparator = columnSorters.get(i);
			if (aktivitetComparator!=comp){
				aktivitetComparator.reset();
			}
		}	
	}
	
	class AktivitetComparator implements Comparator<Aktivitet>{
		private SORTBY sortingBy;
		private Integer currentSortOrdering;//0,1,2 ASC DESC NONE
		private final int columnIndex;
		public AktivitetComparator(SORTBY sortingBy, int columnIndex){
			this.sortingBy = sortingBy;
			this.columnIndex = columnIndex;
			currentSortOrdering=2;
		}

		
		public void toggle(){
			if (currentSortOrdering==2){
				currentSortOrdering=0;
			}else{
				currentSortOrdering++;
			}
			int index = treeTable.convertColumnIndexToView(columnIndex);
			
			TableColumn column = columnModel.getColumn(index);
			if (column.getHeaderRenderer()==null){
				TreeTableHeaderCellRenderer headerRenderer = new TreeTableHeaderCellRenderer(String.valueOf(column.getHeaderValue()));
				column.setHeaderRenderer(headerRenderer);
			}
			TreeTableHeaderCellRenderer renderer = (TreeTableHeaderCellRenderer)column.getHeaderRenderer();
			if (currentSortOrdering==0){
				renderer.setIcon(IconResources.SORT_ASC);
			}
			if (currentSortOrdering==1){
				renderer.setIcon(IconResources.SORT_DESC);				
			}
			if (currentSortOrdering==2){
				column.setHeaderRenderer(null);
			}
		}
		
		public void reset(){
			currentSortOrdering=2;
			int index = treeTable.convertColumnIndexToView(columnIndex);
			TableColumn column = columnModel.getColumn(index);
			column.setHeaderRenderer(null);
		}
		@Override
		public int compare(Aktivitet o1, Aktivitet o2) {
			int res = 0;
			switch(sortingBy){
				case ID: res = o1.getId().compareTo(o2.getId());break;
				case LEVERANDOR: res = compareString(o1.getViewLeverandorNavn(),o2.getViewLeverandorNavn());break;
				case KJORETOY: res = compareString(o1.getViewKjoretoyNavn(),o2.getViewKjoretoyNavn());break;
				case RODE: res = compareString(o1.getViewRodeNavn(), o2.getViewRodeNavn());break;
				case ARBEID: res = compareString(o1.getViewArtikkel().getArtikkelNavn(), o2.getViewArtikkel().getArtikkelNavn());break;
				case MENGDE: res = o1.getUtfortMengde().compareTo(o2.getUtfortMengde());
				case GODKJENT: res = compareDouble(o1.getGodkjentMengde(), o2.getGodkjentMengde());
				case ENHETSPRIS: res = o1.getEnhetsPris().compareTo(o2.getEnhetsPris());
			}
			if (currentSortOrdering==0){
				return res;
			}
			if (currentSortOrdering==1){
				return res*-1;
			}
			return 0;
		}
		
		private int compareDouble(Double d1, Double d2){
			if (d1==null && d2!=null)
				return -1;
			if (d2==null && d1!=null)
				return 1;
			if (d2==null && d1==null)
				return 0;
			return d1.compareTo(d2);
		}
		private int compareString(String s1, String s2){
			if (s1==null && s2!=null)
				return -1;
			if (s2==null && s1!=null)
				return 1;
			if (s2==null && s1==null)
				return 0;
			return s1.compareTo(s2);
		}
	}

}
