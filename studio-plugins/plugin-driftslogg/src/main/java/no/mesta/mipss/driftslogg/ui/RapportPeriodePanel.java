package no.mesta.mipss.driftslogg.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.driftslogg.util.CalendarHelper;
import org.jdesktop.swingx.JXHeader;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

@SuppressWarnings("serial")
public class RapportPeriodePanel extends JPanel{

	private JLabel lblFra;
	private JLabel lblTil;
	
	private JButton btnOk;
	private JButton btnAvbryt;
	
	private JMipssDatePicker dateFra;
	private JMipssDatePicker dateTil;
	private final DriftsloggController controller;
	
	private boolean cancelled=true;
	
	protected DriftsloggQueryParams params;
	
	public RapportPeriodePanel(DriftsloggController controller){
		this.controller = controller;
		this.params = controller.getQueryParams();
		initComponents();
		initGui();
	}
	
	public boolean isCancelled(){
		return cancelled;
	}
	
	private void initComponents() {
		dateFra = new JMipssDatePicker();
		dateFra.setTimeFormat(JMipssDatePicker.TIME_FORMAT.START_OF_DAY);
		dateFra.setDate(CalendarHelper.getFirstDateInMonth(params.getMonth()));
		dateTil = new JMipssDatePicker();
		dateTil.setTimeFormat(JMipssDatePicker.TIME_FORMAT.END_OF_DAY);
		dateTil.setDate(CalendarHelper.getLastTimeInMonth(params.getMonth()));
		dateFra.pairWith(dateTil);
		DatePickerConstraintsUtils.setNotBeforeDate(controller.getPlugin().getLoader(), dateFra, dateTil);
		
		lblFra = new JLabel(Resources.getResource("label.fra"));
		lblTil = new JLabel(Resources.getResource("label.til"));
		
		btnOk = new JButton(getOkAction());
		btnAvbryt = new JButton(getAvbrytAction());
	}
	
	protected JXHeader createHeader(){
		return new JXHeader(Resources.getResource("rapportperiode.title"), Resources.getResource("rapportperiode.description"), IconResources.HELP_ICON);
	}
	
	private void initGui(){
		setLayout(new BorderLayout());
		JPanel pnlButton = new JPanel(new GridBagLayout());
		pnlButton.add(btnOk, 	 new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		pnlButton.add(btnAvbryt, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		
		JPanel pnlMain = new JPanel(new GridBagLayout());
		pnlMain.add(lblFra,		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		pnlMain.add(dateFra, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		pnlMain.add(lblTil,		new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5),0,0));
		pnlMain.add(dateTil, 	new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,5),0,0));
		
		add(createHeader(), BorderLayout.NORTH);
		add(pnlMain);
		add(pnlButton, BorderLayout.SOUTH);
	}
	
	public Date getFraDato(){
		return dateFra.getDate();
	}
	public Date getTilDato(){
		return dateTil.getDate();
	}
	
	private Action getOkAction(){
		return new AbstractAction(Resources.getResource("button.hentRapport"), IconResources.OK2_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=false;
				SwingUtilities.windowForComponent(RapportPeriodePanel.this).dispose();
				
			}
		};
	}
	
	private Action getAvbrytAction(){
		return new AbstractAction(Resources.getResource("button.avbryt"), IconResources.ABORT_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.windowForComponent(RapportPeriodePanel.this).dispose();
			}
		};
	}
}
