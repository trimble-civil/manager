package no.mesta.mipss.driftslogg.ui.agressoinfo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftslogg.*;
import no.mesta.mipss.driftslogg.util.CalendarHelper;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssTreeTableScrollPane;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.table.CurrencyRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXHeader;

@SuppressWarnings("serial")
public class OverforAgressoPanel extends JPanel{
	
	private JButton btnOverfor;
	private JButton btnAvbryt;
	
	private JCheckBox chkTidligerePerioder;
	private JCheckBox chkVisAlleAktiviteter;
	
	private JMipssTreeTableScrollPane treeTableScroll;
	private AktiviteterAgressoTableModel mdlAktiviteter;
	private JMipssTreeTable<MipssInputTreeView> tblAktiviteter;
	
	
	private JMipssTreeTableScrollPane treeTableScrollIkke;
	private AktiviteterAgressoTableModel mdlAktiviteterIkke;
	private JMipssTreeTable<MipssInputTreeView> tblAktiviteterIkke;
	
	private boolean canceled = true;
	private boolean inited = false;
	
	private final DriftsloggController controller;
	private final List<Long> perioderSomManglerAttestant;
	private final Long mnd;
	private final Long aar;
	
	private JLabel lblInfoOverforing;
	private JLabel lblInfoIkkeOverforing;

	public OverforAgressoPanel(DriftsloggController controller, List<Long> perioderSomManglerAttestant, Long mnd, Long aar){
		this.controller = controller;
		this.perioderSomManglerAttestant = perioderSomManglerAttestant;
		this.mnd = mnd;
		this.aar = aar;
		initComponents();
		initGui();
		bind();
	}
	
	private JXHeader getHeader(){
		return new JXHeader(Resources.getResource("overforAgresso.title"), Resources.getResource("overforAgresso.descr", controller.getLeverandorerSomIkkeSkalOverfores()), IconResources.AGRESSO_16);
	}

	private void initComponents() {
		btnOverfor = new JButton(getOverforAction());
		btnAvbryt = new JButton(getAvbrytAction());
		btnOverfor.setToolTipText(Resources.getResource("button.overforAgresso.tooltip"));
		btnAvbryt.setToolTipText(Resources.getResource("button.avbrytAgresso.tooltip"));
		chkTidligerePerioder = new JCheckBox(Resources.getResource("label.tidligerePerioder"));
		chkTidligerePerioder.setToolTipText(Resources.getResource("label.tidligerePerioder.tooltip"));	
		chkTidligerePerioder.setSelected(true);
		
		chkVisAlleAktiviteter= new JCheckBox(Resources.getResource("label.alleAktiviteter"));
		chkVisAlleAktiviteter.setToolTipText(Resources.getResource("label.alleAktiviteter.tooltip"));
		chkVisAlleAktiviteter.setSelected(false);
		
		lblInfoIkkeOverforing = new JLabel(Resources.getResource("label.ikkeoverforing"));
		lblInfoOverforing = new JLabel(Resources.getResource("label.overforingsinfo"));
		initTable();
		BindingHelper.createbinding(chkTidligerePerioder, "selected", this, "tidligerePerioder").bind();
		BindingHelper.createbinding(chkVisAlleAktiviteter, "selected", this, "visAlleAktiviteter").bind();
		inited = true;
	}
	
	
	public void setVisAlleAktiviteter(boolean valgt){
		if(valgt){
			tblAktiviteter.expandAll();
			tblAktiviteterIkke.expandAll();
		}
		else{
			tblAktiviteter.collapseAll();
			tblAktiviteterIkke.collapseAll();
		}
		
	}
	
	
	public void setTidligerePerioder(boolean valgt){
		if (!inited)
			return;
		Date first = CalendarHelper.getFirstDateInMonth(mnd, aar);
		Date last = CalendarHelper.getLastDateInMonth(mnd, aar);
		if (!valgt){
			//kun behold aktiviteter fra denne måneden.
			List<MipssInputTreeView> overfores = new ArrayList<MipssInputTreeView>();
			List<MipssInputTreeView> aktiviteter = mdlAktiviteter.getAktiviteter();
			for (MipssInputTreeView a:aktiviteter){
				
				List<MipssInputTreeView> toRemove = new ArrayList<MipssInputTreeView>();
				for (MipssInputTreeView tv:a.getMipssInputList()){
					Date delivDate = tv.getMipssInput().getDelivDate();
					if (!CalendarUtil.isOverlappende(first, last, delivDate, delivDate, true)){
						toRemove.add(tv);
						MipssInputTreeView overfort = null;
						int indexOf = overfores.indexOf(a);
						if (indexOf!=-1){
							 overfort = overfores.get(indexOf);
						}else{
							overfort = new MipssInputTreeView(a.getLeverandor(), a.getLeverandorNr());
							overfores.add(overfort);
						}
						overfort.addChild(tv);
						tv.setKommentar("Tilhører tidligere periode.");
					}
				}
				a.getMipssInputList().removeAll(toRemove);
			}
			mdlAktiviteter.setAktiviteter(aktiviteter);
			
			//legg til aktivitetene til tabellen under
			List<MipssInputTreeView> aktiviteterIkke = mdlAktiviteterIkke.getAktiviteter();
			aktiviteterIkke.addAll(overfores);
			mdlAktiviteterIkke.setAktiviteter(aktiviteterIkke);
		}else{
			mdlAktiviteter.loadData(controller);
			mdlAktiviteterIkke.loadData(controller);
		}
	}
	
	private void initTable(){
		
		MipssRenderableEntityTableColumnModel aktiviteterColumnModel = new MipssRenderableEntityTableColumnModel(
				MipssInputTreeView.class, "info", "levkontraktIdent", "artikkel", "enhet", "antall", "sum", "tripId");
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_TREEVIEW).setPreferredWidth(200);
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_LEVKONTRAKT).setPreferredWidth(60);
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_ARTIKKEL).setPreferredWidth(60);
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_ENHET).setPreferredWidth(30);
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_ANTALL).setPreferredWidth(30);
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_PRIS).setPreferredWidth(30);
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_TRIP_ID).setPreferredWidth(60);
		
		aktiviteterColumnModel.getColumn(AktiviteterAgressoTableModel.COL_PRIS).setCellRenderer(new CurrencyRenderer());
		mdlAktiviteter = new AktiviteterAgressoTableModel(perioderSomManglerAttestant, mnd, aar, true);
		tblAktiviteter = new JMipssTreeTable<MipssInputTreeView>(mdlAktiviteter);
		tblAktiviteter.setColumnModel(aktiviteterColumnModel);
		tblAktiviteter.setFillsViewportHeight(true);
		tblAktiviteter.setLeafIcon(null);
		tblAktiviteter.setShowGrid(true, true);
		tblAktiviteter.setGridColor(new Color(230,230,230));
		treeTableScroll = new JMipssTreeTableScrollPane(tblAktiviteter, mdlAktiviteter);
		
		mdlAktiviteter.loadData(controller);
		mdlAktiviteter.addTableLoaderListener(new MipssBeanLoaderListener(){
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {}
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				tblAktiviteter.collapseAll();
			}
		});
		
		MipssRenderableEntityTableColumnModel aktiviteterColumnModelIkke = new MipssRenderableEntityTableColumnModel(
				MipssInputTreeView.class, "info", "levkontraktIdent", "artikkel", "enhet", "antall", "sum", "tripId", "kommentar");
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_TREEVIEW).setPreferredWidth(150);
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_LEVKONTRAKT).setPreferredWidth(60);
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_ARTIKKEL).setPreferredWidth(60);
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_ENHET).setPreferredWidth(30);
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_ANTALL).setPreferredWidth(0);
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_PRIS).setPreferredWidth(30);
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_TRIP_ID).setPreferredWidth(60);
		
		aktiviteterColumnModelIkke.getColumn(AktiviteterAgressoTableModel.COL_PRIS).setCellRenderer(new CurrencyRenderer());
		mdlAktiviteterIkke = new AktiviteterAgressoTableModel(perioderSomManglerAttestant, mnd, aar, false);
		tblAktiviteterIkke = new JMipssTreeTable<MipssInputTreeView>(mdlAktiviteterIkke);
		tblAktiviteterIkke.setColumnModel(aktiviteterColumnModelIkke);
		tblAktiviteterIkke.setFillsViewportHeight(true);
		tblAktiviteterIkke.setLeafIcon(null);
		tblAktiviteterIkke.setShowGrid(true, true);
		tblAktiviteterIkke.setGridColor(new Color(230,230,230));
		treeTableScrollIkke = new JMipssTreeTableScrollPane(tblAktiviteterIkke, mdlAktiviteterIkke);
		
		mdlAktiviteterIkke.loadData(controller);
		mdlAktiviteterIkke.addTableLoaderListener(new MipssBeanLoaderListener(){
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {}
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				tblAktiviteterIkke.collapseAll();
			}
		});
		
	}
	
	private Action getAvbrytAction(){
		return new AbstractAction(Resources.getResource("button.avbryt"), IconResources.ABORT_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.windowForComponent(OverforAgressoPanel.this).dispose();
			}
		};
	}
	
	private Action getOverforAction(){
		return new AbstractAction(Resources.getResource("button.overforAgresso"), IconResources.OK2_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				canceled = false;
				SwingUtilities.windowForComponent(OverforAgressoPanel.this).dispose();
			}
		};
	}
	public boolean isCancelled(){
		return canceled;
	}
	
	public List<MipssInput> getSelectedEntities() {
		return getEntities(mdlAktiviteter);
	}

	public List<MipssInput> getNonTransferableActivities() {
		return getEntities(mdlAktiviteterIkke);
	}

	private List<MipssInput> getEntities(AktiviteterAgressoTableModel activities) {
		List<MipssInput> entities = new ArrayList<>();
		List<MipssInputTreeView> aktiviteter = activities.getAktiviteter();

		for (MipssInputTreeView p : aktiviteter) {
			for (MipssInputTreeView a : p.getMipssInputList()) {
				entities.add(a.getMipssInput());
			}
		}

		return entities;
	}

	private void initGui() {
		setLayout(new BorderLayout());
		
		JPanel pnlMain = new JPanel(new GridBagLayout());
		pnlMain.add(lblInfoOverforing, 		new GridBagConstraints(0,0,1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10,5,5,5),0,0));
		pnlMain.add(chkTidligerePerioder, 	new GridBagConstraints(1,0,1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(10,5,5,5),0,0));
		pnlMain.add(treeTableScroll, 		new GridBagConstraints(0,1,2,1, 1.0,1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(5,0,5,5),0,0));
		pnlMain.add(lblInfoIkkeOverforing, 	new GridBagConstraints(0,2,2,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,0,5,5),0,0));
		pnlMain.add(treeTableScrollIkke, 	new GridBagConstraints(0,3,2,1, 1.0,1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(5,0,5,5),0,0));
		pnlMain.add(chkVisAlleAktiviteter, 	new GridBagConstraints(0,4,1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5),0,0));
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		pnlButtons.add(btnAvbryt, 	new GridBagConstraints(0,0,1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		pnlButtons.add(btnOverfor, 	new GridBagConstraints(1,0,1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		add(getHeader(), BorderLayout.NORTH);
		add(pnlMain);
		add(pnlButtons, BorderLayout.SOUTH);
		
	}

	private void bind() {
		
	}

}
