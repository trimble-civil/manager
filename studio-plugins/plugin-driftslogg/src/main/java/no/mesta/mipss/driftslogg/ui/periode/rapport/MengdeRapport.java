package no.mesta.mipss.driftslogg.ui.periode.rapport;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams.MENGDERAPPORT_TYPE;
import no.mesta.mipss.driftslogg.rapport.DriftsloggRapportData;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MengdeRapport {

	private static final String RAPPORT_TEMPLATE = "driftslogg_mengder_kombinert";
	private File file;
	private XSSFWorkbook workbook;
	
	public MengdeRapport(DriftsloggQueryParams params, DriftsloggRapportData data, MENGDERAPPORT_TYPE type, Date fraDato, Date tilDato){
		int weekForDate = CalendarUtil.getWeekForDate(fraDato);
		
		String periodeText = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.DATE_FORMAT)+" - "+MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.DATE_FORMAT);
		String rappDatoText = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		String ukeText = String.valueOf(weekForDate);

		Calendar c = Calendar.getInstance();
		c.setTime(fraDato);

		String mndText = c.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("NO", "no"));
		String typeString;
		switch(type){
			case EGENDEF: typeString = "Egendefinert periode";break;
			default: typeString = type.toString();
		}
		String verdiString = "";
		switch(type){
			case UKE: verdiString = ukeText;break;
			case MND: verdiString = mndText;break;
		}
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("r12Data", data.getR12rapport());
		map.put("r12Sum", data.getR12Summer());
		map.put("prisData", data.getPrisrapport());
		map.put("periode", periodeText);
		map.put("artikkelSum", data.getSumPrisArtikkelGruppe());
		map.put("params", params);
		map.put("type", typeString);
		map.put("typeVerdi", verdiString);
		map.put("dato", rappDatoText);
		
		file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
		if (data.getR12rapport().isEmpty() && data.getPrisrapport().isEmpty()){
			workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map, "R12", "Pris");	
		}else if (data.getR12rapport().isEmpty()){
			workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map, "R12");	
		}else if (data.getPrisrapport().isEmpty()){
			workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map, "Pris");	
		}else{		
			workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map);
		}
	}
	
	/**
	 * Lukker arbeidsboken og åpner den i Excel
	 */
	public void finalizeAndOpen(){
		RapportHelper.finalizeAndOpen(workbook, file);
	}

}