package no.mesta.mipss.driftslogg.ui.detaljui;

import no.mesta.mipss.persistence.driftslogg.TripReturnCause;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Comparator;
import java.util.List;

import static no.mesta.mipss.common.MipssDateFormatter.LONG_TIME_YEARDATE_FORMAT;

class ActivityReturnCausesPanel extends JPanel {

    private final static int COL_CREATED = 0;
    private final static int COL_SIGNATURE = 1;

    private JScrollPane scrollPane;

    private List<TripReturnCause> returnCauses;

    ActivityReturnCausesPanel(List<TripReturnCause> returnCauses) {
        this.returnCauses = returnCauses;

        initComponents();
        initGui();
    }

    private void initComponents() {
        // Latest date first.
        returnCauses.sort(Comparator.comparing(TripReturnCause::getCreated).reversed());

        MipssBeanTableModel<TripReturnCause> dataModel = new MipssBeanTableModel<>(
                TripReturnCause.class, "created", "signature", "cause");
        dataModel.setEntities(returnCauses);

        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(TripReturnCause.class, dataModel.getColumns());
        columnModel.getColumn(COL_CREATED).setCellRenderer(new DateTimeRenderer(LONG_TIME_YEARDATE_FORMAT));
        columnModel.getColumn(COL_CREATED).setMinWidth(120);
        columnModel.getColumn(COL_CREATED).setPreferredWidth(120);
        columnModel.getColumn(COL_CREATED).setMaxWidth(120);
        columnModel.getColumn(COL_SIGNATURE).setMaxWidth(50);

        JXTable tblReturnCauses = new JXTable(dataModel, columnModel);
        tblReturnCauses.setFillsViewportHeight(true);
        tblReturnCauses.setColumnControlVisible(true);

        scrollPane = new JScrollPane(tblReturnCauses);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(new EmptyBorder(5, 5, 5, 5));

        add(scrollPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

}