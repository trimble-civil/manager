package no.mesta.mipss.driftslogg.ui.listeners;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.BeanFieldListener;
import no.mesta.mipss.core.IBeanFieldChange;
import no.mesta.mipss.persistence.MipssEntityBean;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import org.jdesktop.beansbinding.BindingGroup;

/**
 * Endringsmonitor som holder øye med gitte felter i en entitet og holder styr på hvilke
 * entiteter som er endret i listen. 
 * 
 * @author harkul
 *
 * @param <T>
 */
public class ChangeMonitor<T extends MipssEntityBean<?>> implements IBeanFieldChange, PropertyChangeSource{
	
	private final List<T> changedEntities = new ArrayList<T>();
	private final Map<T, List<BeanFieldListener>> changeTracker = new HashMap<>();
	private final BindingGroup bg = new BindingGroup();
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private boolean changed;
	private String[] fields;
	
	/**
	 * 
	 * @param entities Entitetene som skal lyttes på
	 * @param originalValues Liste med de orginale verdiene på hver entitiet(må matche med fields)
	 * @param fields feltene som skal lyttes på (må matche med originalValues)
	 */
	public ChangeMonitor(List<T> entities, List<Object[]> originalValues, String... fields){
		int i = 0;
		for (T bean:entities){
			Object[] ov = originalValues.get(i);
			List<BeanFieldListener> listeners = new ArrayList<>();
			int c = 0;
			for (String field:fields){
				listeners.add(new BeanFieldListener(bean, field, bg, this, ov[c]));
				c++;
			}
			changeTracker.put(bean, listeners);
			i++;
		}
		this.fields = fields;
	}
	public void bind(){
		bg.bind();
	}
	
	public void unbind() {
		bg.unbind();
		changeTracker.clear();
		changedEntities.clear();
	}
	
	public List<T> getChangedEntities(){
		return changedEntities;
	}
	
	@SuppressWarnings("unchecked")
	public void resetEntity(Object instance){
		T bean = (T)instance;
		List<BeanFieldListener> list = changeTracker.get(bean);
		for (BeanFieldListener l:list){
			l.reset(true);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setFieldChanged(Object instance, String beanField, boolean changed, boolean forceRemoval) {
		T bean = (T)instance;
		boolean changedState = true;
		if (forceRemoval){
			changedEntities.remove(bean);
			if (changedEntities.isEmpty())
				changedState = false;
			boolean old = isChanged(); 
			this.changed = changedState;
			props.firePropertyChange("changed", old, this.changed);
			return;
		}
		
		
		List<BeanFieldListener> list = changeTracker.get(bean);
		boolean alreadyChangeTracked = changedEntities.contains(bean);
		if (changed && alreadyChangeTracked)
			return;
		
		boolean changedEntity = false;
		for (BeanFieldListener l:list){
			if (l.isChanged()){
				changedEntity = true;
				break;
			}
		}
		
		if (changedEntity){
			if (!alreadyChangeTracked) {
				changedEntities.add(bean);
			}
			changedState = true;
		}else{
			changedEntities.remove(bean);
			if (changedEntities.isEmpty())
				changedState = false;
		}
		boolean old = isChanged(); 
		this.changed = changedState;
		props.firePropertyChange("changed", old, this.changed);
	}
	public boolean isChanged(){
		return changed;
	}
	public void setOverriddenChanged(boolean changed){
		boolean old = this.changed;
		this.changed = changed;
		props.firePropertyChange("changed", old, this.changed);
	}
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
		
	}
	@Override
	public void addPropertyChangeListener(String propName,PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}
	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}
	@Override
	public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	public void addEntity(T bean, Object[] originalValues) {
		List<BeanFieldListener> listeners = new ArrayList<>();

		int c = 0;
		for (String field : fields) {
			listeners.add(new BeanFieldListener(bean, field, bg, this, originalValues[c]));
			c++;
		}

		changeTracker.put(bean, listeners);
		bg.bind();
	}

}
