package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.persistence.driftslogg.PeriodeStatus.StatusType;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.net.URL;

public class StatusTableCellRenderer extends DefaultTableCellRenderer {

	private static final String STATUS_PAID = "statusPaid.png";
	private static final String STATUS_REGISTERED = "statusRegistered.png";
	private static final String STATUS_RETURNED = "statusReturned.png";
	private static final String STATUS_PROCESSED = "statusProcessed.png";
	private static final String STATUS_SUBMITTED = "statusSubmitted.png";

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		JPanel cell = new JPanel(new GridBagLayout());
		cell.setBackground(table.getSelectionBackground());
		cell.setForeground(table.getSelectionForeground());
		cell.setOpaque(isSelected);

		if (value == null)
			return cell;

		if (value instanceof TripProcessStatus) {
			TripProcessStatus status = (TripProcessStatus)value;
			String tooltip = "";

			switch(status){
				case SUBMITTED_BY_SUBCONTRACTOR:
				case UNDER_PROCESS_BY_CONTRACTOR:
					tooltip = "label.submitted.tooltip";
					addIcon(cell, STATUS_SUBMITTED);
					break;
				case PROCESSED_BY_CONTRACTOR:
					tooltip = "label.processed.tooltip";
					addIcon(cell, STATUS_PROCESSED);
					break;
				case RETURNED_TO_SUBCONTRACTOR:
					tooltip = "label.returned.status.tooltip";
					addIcon(cell, STATUS_RETURNED);
					break;
				case PROCESSED_BY_SUBCONTRACTOR:
					tooltip = "label.registered.status.tooltip";
					addIcon(cell, STATUS_REGISTERED);
					break;
				case SENT_TO_IO:
					tooltip = "label.paid.tooltip";
					addIcon(cell, STATUS_PAID);
					break;
				default:
					break;
			}

			setToolTipText((!tooltip.isEmpty()) ? Resources.getResource(tooltip) : null);
		}

		return cell;
	}

	private void addIcon(JPanel panel, String imageUrl) {
		JLabel icon = new JLabel(getIcon(imageUrl));
		icon.setOpaque(false);

		panel.add(icon, new GridBagConstraints());
	}

	private ImageIcon getIcon(String name) {
		URL imageUrl = this.getClass().getClassLoader().getResource(name);
		return new ImageIcon(imageUrl);
	}
	
}