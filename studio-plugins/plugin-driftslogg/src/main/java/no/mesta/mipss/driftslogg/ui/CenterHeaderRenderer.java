package no.mesta.mipss.driftslogg.ui;

import org.jdesktop.swingx.table.ColumnHeaderRenderer;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class CenterHeaderRenderer implements TableCellRenderer {

    private ColumnHeaderRenderer renderer;

    public CenterHeaderRenderer(JTable table) {
        renderer = (ColumnHeaderRenderer) table.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        return renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
    }

}