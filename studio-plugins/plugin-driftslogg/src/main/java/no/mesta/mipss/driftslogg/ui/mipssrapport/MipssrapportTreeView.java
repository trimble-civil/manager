package no.mesta.mipss.driftslogg.ui.mipssrapport;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.MipssTiltak;

public class MipssrapportTreeView implements IRenderableMipssEntity{

	private String navn;
	private MipssTiltak tiltak;
	
	private List<MipssrapportTreeView> children = new ArrayList<MipssrapportTreeView>();
	private MipssrapportTreeView parent;
	private DecimalFormat format;

	public MipssrapportTreeView(MipssrapportTreeView parent){
		this.parent = parent;
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
    	symbols.setDecimalSeparator(',');
    	format = new DecimalFormat("#.###", symbols);
	}
	
	public void setNavn(String navn){
		this.navn = navn;
	}
	public void setTiltak(MipssTiltak tiltak){
		this.tiltak = tiltak;
	}
	public MipssTiltak getMipssTiltak(){
		return tiltak;
	}
	private String getStrogruppe(){
		return tiltak.getStroproduktgruppe();
	}
	public String getTiltak(){
		if (tiltak!=null){
			return tiltak.getNavn();
		}
		return navn;
	}
	
	public String getKm(){
		return tiltak!=null?format(tiltak.getKm()):boldFormat(getSumChildKm()); 
	}
	private Double getKmD(){
		return tiltak!=null?tiltak.getKm():null;
	}
	private Double getSumChildKm(){
		if (children==null){
			return null;
		}
		double sumKm = 0;
		for (MipssrapportTreeView t:children){
			if (t.getKmD()!=null){				
				if (!t.getMipssTiltak().getProdtype().contains("intensjon")) {
					sumKm+=t.getKmD();
				}
			}
		}
		return sumKm==0?null:sumKm;
		
	}
	public String getTimer(){
		return tiltak!=null?format(tiltak.getTimer()):boldFormat(getSumChildTimer());
	}
	private Double getTimerD(){
		return tiltak!=null?tiltak.getTimer():null;
	}
	
	private Double getSumChildTimer(){
		if (children==null){
			return null;
		}
		double sumTimer =0;
		for (MipssrapportTreeView t:children){
			if (t.getTimerD()!=null){
				if (!t.getMipssTiltak().getProdtype().contains("intensjon")) {
					sumTimer += t.getTimerD();
				}				
			}
		}
		return sumTimer==0?null:sumTimer;
	}
	public String getStroprodukt(){
		if (tiltak==null){
			List<String> sumtyper = new ArrayList<String>();
			String stroprodukt = null;
			for (MipssrapportTreeView t:children){
				if (!sumtyper.contains(t.getStroprodukt())&&!StringUtils.isBlank(t.getStroprodukt())){
					sumtyper.add(t.getStroprodukt());
					stroprodukt = t.getStroprodukt();
				}
			}
			if (sumtyper.size()==1){
				return "<html><i>"+stroprodukt+"</i></html>";
			}
			return "<html><i>"+StringUtils.join(sumtyper, ',')+"</i></html>";
		}
		return tiltak.getStroprodukt();
	}
	public String getTonnTorr(){
		return tiltak!=null?format(tiltak.getTonn()):boldFormat(getSumChildTonnTorr());
	}
	
	private Double getTonnTorrD(){
		return tiltak!=null?tiltak.getTonn():null;
	}
	private Double getSumChildTonnTorr(){
		if (children==null){
			return null;
		}
		double sumTonn = 0;
		for (MipssrapportTreeView t:children){
			if (t.getTonnTorrD()!=null){
				sumTonn+=t.getTonnTorrD();
			}
		}
		return sumTonn==0?null:sumTonn;
	}

	private Double round(Double value){
		if (value==null)
			return null;
		int temp=(int)((value*Math.pow(10,3)));
		return (((double)temp)/Math.pow(10,3));

	}
	public List<MipssrapportTreeView> getChildren(){
		return children;
	}
	public boolean isLeaf(){
		return children.isEmpty();
	}
	public MipssrapportTreeView getParent(){
		return parent;
	}

	@Override
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}
	private String format(Double value){
		if (value==null)
			return null;
		return format.format(round(value));
	}
	private String boldFormat(Double value){
		if (value==null)
			return null;
		return "<html><b>"+format.format(round(value))+"</b></html>";
	}
	
	public String getLiterLosningTotalt(){
		return tiltak!=null?format(tiltak.getLiterLosningTotalt()):boldFormat(getSumChildLiterLosningTotalt());
	}

	public Double getLiterLosningTotaltD(){
		return tiltak!=null?tiltak.getLiterLosningTotalt():null;
	}
	
	public String getKgLosning(){
		return tiltak!=null?format(tiltak.getKgLosning()):boldFormat(getSumChildKgLosning());
	}
	
	public Double getKgLosningD(){
		return tiltak!=null?tiltak.getKgLosning():null;
	}
	
	private Double getSumChildLiterLosningTotalt(){
		if (children==null){
			return null;
		}
		double sumLiterLosning = 0;
		for (MipssrapportTreeView t:children){
			if (t.getLiterLosningTotalt()!=null){
				sumLiterLosning	+=t.getLiterLosningTotaltD();
			}
		}
		return sumLiterLosning==0?null:sumLiterLosning;
	}
	
	private Double getSumChildKgLosning(){
		if (children==null){
			return null;
		}
		double sumKgLosning = 0;
		for (MipssrapportTreeView t:children){
			if (t.getKgLosning()!=null){
				sumKgLosning+=t.getKgLosningD();
			}
		}
		return sumKgLosning==0?null:sumKgLosning;
	}
}
