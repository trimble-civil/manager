package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.driftslogg.Trip;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.net.URL;

import static no.mesta.mipss.persistence.driftslogg.PeriodeStatus.StatusType;

public class PeriodStatusTableCellRenderer implements TreeCellRenderer {

	private static final String CLOSED_URL = "closed.png";
	private static final String SENT_IO_URL = "sent_io.png";

	private Color selectionBackground;
	private Color selectionForeground;

	public PeriodStatusTableCellRenderer(Color selectionBackground, Color selectionForeground) {
		this.selectionBackground = selectionBackground;
		this.selectionForeground = selectionForeground;
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		if (value instanceof TripTreeView) {
			TripTreeView ttv = (TripTreeView) value;
			Trip t = ttv.getTrip();

			if (t != null && t.getId() != null && t.getPeriod() != null) {
				return getPeriodStatusInfo(t.getPeriod().getStatus().getStatusType(), tree, selected);
			}

			return getDefaultPanel(selected);
		}
		throw new IllegalArgumentException("Can only render TripTreeView");
	}

	private JPanel getPeriodStatusInfo(StatusType status, JTree tree, boolean isSelected) {
		JPanel cell = getDefaultPanel(isSelected);

		switch (status) {
			case CLOSED:
				addIcon(cell, CLOSED_URL);
				break;
			case SENT_IO:
				addIcon(cell, SENT_IO_URL);
				break;
		}

		return cell;
	}

	private JPanel getDefaultPanel(boolean isSelected) {
		JPanel cell = new JPanel(new GridBagLayout());
		cell.setBackground(selectionBackground);
		cell.setForeground(selectionForeground);
		cell.setOpaque(isSelected);
		return cell;
	}

	private void addIcon(JPanel panel, String imageUrl) {
		JLabel icon = new JLabel(getIcon(imageUrl));
		icon.setOpaque(false);
		icon.setBorder(new EmptyBorder(0, 7, 0, 0));

		panel.add(icon, new GridBagConstraints());
	}

	private ImageIcon getIcon(String name) {
		URL imageUrl = this.getClass().getClassLoader().getResource(name);
		return new ImageIcon(imageUrl);
	}

}