package no.mesta.mipss.driftslogg.ui.detaljui;

import no.mesta.mipss.driftslogg.ui.AktivitetTableModel;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class IntentionCheckboxEnabler implements HighlightPredicate {

	private final AktivitetTableModel model;

	public IntentionCheckboxEnabler(AktivitetTableModel model) {
		this.model = model;
	}

	@Override
	public boolean isHighlighted(Component component, ComponentAdapter componentAdapter) {
		Aktivitet activity = model.getAktiviteter().get(componentAdapter.row);

		if (activity.getR12_intention() == null && componentAdapter.column == AktivitetTableModel.COL_APPROVED_INTENTION) {
			component.setEnabled(false);
		} else if (componentAdapter.column == AktivitetTableModel.COL_APPROVED_INTENTION) {
			component.setEnabled(true);
		}

		return false;
	}

}