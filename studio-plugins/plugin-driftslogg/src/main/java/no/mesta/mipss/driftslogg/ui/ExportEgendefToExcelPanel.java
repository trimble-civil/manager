package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXHeader;
/**
 * Arver fra RapportPeriodePanel. Sørger for at headerteksten i panelet blir riktig i forhold til export-funskjonen.
 * @author larnym
 */
@SuppressWarnings("serial")
public class ExportEgendefToExcelPanel extends RapportPeriodePanel {

	public ExportEgendefToExcelPanel(DriftsloggController controller) {
		super(controller);
	}
	
	@Override
	public JXHeader createHeader() {
		
		StringBuilder status = new StringBuilder();

		if (params.isSubmitted()) {
			status.append("INNSENDT ");
		}

		if (params.isProcessed()) {
			status.append("BEHANDLET ");
		}
		
		if (!params.isSubmitted() && !params.isProcessed()) {
			status.append("ALLE");
		}
		
		return new JXHeader(Resources.getResource("exportEgendef.title"), 
				Resources.getResource("exportEgendef.description", 
						params.getKontrakt() != null ? params.getKontrakt().getKontraktnummer() + " - " + params.getKontrakt().getKontraktnavn() : "MÅ VELGES",
						params.getLeverandor() != null ? params.getLeverandor().getNavn() : "Alle",
						params.getLevkontrakt() != null ? params.getLevkontrakt().getLevkontraktIdent(): "Alle",
						params.getKjoretoy() != null ? params.getKjoretoy().getEksterntNavn(): "Alle",
						status.toString()), 
				IconResources.HELP_ICON);
	}
}
