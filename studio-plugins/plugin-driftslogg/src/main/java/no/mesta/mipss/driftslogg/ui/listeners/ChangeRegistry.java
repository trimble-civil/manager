package no.mesta.mipss.driftslogg.ui.listeners;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.common.PropertyChangeSource;

public class ChangeRegistry implements PropertyChangeListener, PropertyChangeSource{
	private List<ChangeMonitor<?>> monitorList = new ArrayList<ChangeMonitor<?>>();
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private boolean isChanged = false;
	
	public void addMonitor(ChangeMonitor<?> changeMonitor){
		monitorList.add(changeMonitor);
		changeMonitor.addPropertyChangeListener(this);
	}
	public void removeMonitor(ChangeMonitor<?> monitor) {
		monitorList.remove(monitor);
		monitor.removePropertyChangeListener(this);
	}
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(evt.getPropertyName().equals("changed")) {
			setChanged(getGlobalChange());
		}
	}
	private boolean getGlobalChange() {
		Boolean isChanged = Boolean.FALSE;
		
		for(ChangeMonitor<?> monitor : monitorList) {
			if(monitor.isChanged()) {
				isChanged = Boolean.TRUE;
				break;
			}
		}
		return isChanged;
	}
	public void setChanged(boolean changed) {
		Object oldValue = isChanged;
		isChanged = changed;
		props.firePropertyChange("changed", oldValue, changed);
	}
	public boolean isChanged(){
		return isChanged;
	}
	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		props.addPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.addPropertyChangeListener(propName, l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		props.removePropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(String propName,
			PropertyChangeListener l) {
		props.removePropertyChangeListener(propName, l);
	}

	
}
