package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.PeriodeStatus.StatusType;
import no.mesta.mipss.persistence.driftslogg.Stroproduktmengde;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AktivitetTreeView implements IRenderableMipssEntity {

	private Aktivitet aktivitet;
	private Stroproduktmengde mengde;
	private List<AktivitetTreeView> children = new ArrayList<AktivitetTreeView>();
	private AktivitetTreeView parent;
	
	public AktivitetTreeView(AktivitetTreeView parent){
		this.parent = parent;
	}
	public Long getId() {
		return aktivitet!=null?aktivitet.getId():null;
	}
	
	public AktivitetTreeView getParent(){
		return parent;
	}
	
	public void setAktivitet(Aktivitet aktivitet){
		this.aktivitet = aktivitet;
	}
	public void setStroproduktmengde(Stroproduktmengde mengde){
		this.mengde= mengde;
	}
	public Aktivitet getAktivitet(){
		return aktivitet;
	}
	public Date getFraDatoTid(){
		return aktivitet!=null?aktivitet.getFraDatoTid():null;
	}
	public Date getTilDatoTid(){
		return aktivitet!=null?aktivitet.getTilDatoTid():null;
	}

	public String getArtikkelIdent() {
		if (aktivitet!=null){
			if (aktivitet.getViewArtikkel()!=null){
				return aktivitet.getViewArtikkel().getArtikkelNavn();
			}

			if (aktivitet.getId() == null) {
				return null;
			}
		}
		if(aktivitet!=null && aktivitet.getArtikkelIdent()==null && aktivitet.hasNoPayment()) {
			return "Intet oppgjør";
		}

		return aktivitet!=null?"Artikkel fjernet fra Agresso":null;
	}

	public String getEnhet(){
		return aktivitet!=null?aktivitet.getEnhet():null;
	}
	
	public Double getOmforentMengde(){
		return aktivitet!=null?aktivitet.getOmforentMengde():null;
	}
	
	public String getKommentar(){
		if (aktivitet!=null)
			return aktivitet.getKommentar();
		if (mengde!=null)
			return mengde.getKommentar();
		return null;
	}
	public String getInternkommentar(){
		if (aktivitet!=null)
			return aktivitet.getInternKommentar();
		if (mengde!=null){
			return mengde.getInternKommentar();
		}
		return null;
	}
	
	public List<AktivitetTreeView> getChildren(){
		return children;
	}
	
	public boolean isLeaf(){
		return children.isEmpty();
	}

	@Override
	public String getTextForGUI() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setOmforentMengdeTort(Double value) {
		if (mengde!=null&&isEditStatus())
			mengde.setOmforentMengdeTort(value);
	}
	public void setOmforentMengdeVaatt(Double value) {
		if (mengde!=null&&isEditStatus())
			mengde.setOmforentMengdeVaatt(value);
	}
	public void setInternkommentar(String value) {
		if (aktivitet != null) {
			if (value.isEmpty()) {
				aktivitet.setInternKommentar(null);
			} else {
				aktivitet.setInternKommentar(value);
			}
		}

		if (mengde!=null)
			mengde.setInternKommentar(value);
	}
	public boolean isEditStatus(){
		Aktivitet akt = null;

		if (aktivitet!=null){
			akt = aktivitet;
		}

		if (mengde!=null){
			akt = mengde.getAktivitet();
		}

		if (akt!=null&&akt.getSendtAgresso()!=null&&akt.getSendtAgresso()){
			return false;
		}

		return true;
	}

	public boolean isApprovedUE() {
		return aktivitet!=null?aktivitet.isApprovedUE():null;
	}

	public boolean isApprovedBuild() {
		return aktivitet!=null?aktivitet.isApprovedBuild():null;
	}

	public boolean isApprovedIntention() { return aktivitet!=null?aktivitet.isApprovedIntention():null; }

	public void setApprovedUE(boolean value) {
		if(aktivitet!=null&&isEditStatus()) aktivitet.setApprovedUE(value);
	}

	public void setApprovedBuild(boolean value) {
		if(aktivitet!=null&&isEditStatus()) aktivitet.setApprovedBuild(value);
	}

	public void setApprovedIntention(boolean value) {
		if(aktivitet!=null&&isEditStatus()) aktivitet.setApprovedIntention(value);
	}

	public String getProductionType() {
		return aktivitet!=null?aktivitet.getProductionType(): null;
	}

	public Double getR12Plowing() {
		return aktivitet!=null?aktivitet.getR12_broyting():null;
	}

	public Double getR12SidePlough() {
		return aktivitet!=null?aktivitet.getR12_sideplog():null;
	}

	public Double getR12HeavyPlaning() {
		return aktivitet!=null?aktivitet.getR12_hovling():null;
	}

	public Double getR12Intention() {
		return aktivitet!=null?aktivitet.getR12_intention():null;
	}

	public void setArticle(Object article) {
		if (article != null && article instanceof AgrArtikkelV) {
			AgrArtikkelV a = (AgrArtikkelV) article;
			aktivitet.setViewArtikkel(a);
			aktivitet.setArtikkelIdent(a.getArtikkelIdent());
			aktivitet.setEnhet(a.getEnhet());
			aktivitet.setEnhetsPris(a.getPris());
			recalculateSum();
		}
	}

	public Long getRodeId() {
		return (aktivitet != null) ? aktivitet.getRodeId() : null;
	}

	public void setRodeId(Long rodeId) {
		if (aktivitet != null) {
			aktivitet.setRodeId(rodeId);
		}
	}

	public AgrArtikkelV getArticle() {
		return (aktivitet != null) ? aktivitet.getViewArtikkel() : null;
	}

	public void setR12Plowing(Double value) {
		if (aktivitet != null)
			aktivitet.setR12_broyting(value);
	}

	public void setR12HeavyPlaning(Double value) {
		if (aktivitet != null)
			aktivitet.setR12_hovling(value);
	}

	public void setR12SidePlough(Double value) {
		if (aktivitet != null)
			aktivitet.setR12_sideplog(value);
	}

	public void setR12Intention(Double value) {
		if (aktivitet != null)
			aktivitet.setR12_intention(value);
	}

	public void setProductionType(Object productionType) {
		if (aktivitet != null && productionType != null) {
			String prodType = productionType.toString();

			aktivitet.setProductionType(productionType.toString());

			if (prodType.contains(":")) {
				aktivitet.setGritProductName(
						prodType.substring(prodType.indexOf(":") + 1, prodType.length()).trim());
			} else {
				aktivitet.setGritProductName(null);
			}
		}
	}

	public String getGritProductName() {
		return aktivitet.getGritProductName();
	}

	public void setAgreedAmount(Double amount) {
		if (aktivitet != null) {
			aktivitet.setOmforentMengde(amount);
			recalculateSum();
		}
	}

	public Double getSum() {
		return (aktivitet != null) ? aktivitet.getSum() : null;
	}

	private void recalculateSum() {
		if (aktivitet != null && aktivitet.getOmforentMengde() != null && aktivitet.getEnhetsPris() != null) {
			aktivitet.setSum(aktivitet.getOmforentMengde() * aktivitet.getEnhetsPris());
		} else {
			aktivitet.setSum(null);
		}
	}

	public Double getDryGritAmount() {
		return (aktivitet != null) ? aktivitet.getDryGritAmount() : null;
	}

	public void setDryGritAmount(Double dry) {
		if (aktivitet != null) {
			aktivitet.setDryGritAmount(dry);
		}
	}

	public Double getWetGritAmount() {
		return (aktivitet != null) ? aktivitet.getWetGritAmount() : null;
	}

	public void setWetGritAmount(Double wet) {
		if (aktivitet != null) {
			aktivitet.setWetGritAmount(wet);
		}
	}

	public void setRodeViewName(Object name) {
		if (aktivitet != null && name != null) {
			aktivitet.setViewRodeNavn(name.toString());
		}
	}

	public void setComment(String comment) {
		if (aktivitet != null) {
			if (comment.isEmpty()) {
				aktivitet.setKommentar(null);
			} else {
				aktivitet.setKommentar(comment);
			}
		}
	}

}