package no.mesta.mipss.driftslogg.ui;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class TableManualManagerActivityHighlightPredicate implements HighlightPredicate {

    private final AktivitetTableModel model;

    public TableManualManagerActivityHighlightPredicate(AktivitetTableModel mdlAktivitet) {
        this.model = mdlAktivitet;
    }

    @Override
    public boolean isHighlighted(Component component, ComponentAdapter componentAdapter) {
        return model.getAktiviteter().get(componentAdapter.row).isCreatedManager();
    }

}