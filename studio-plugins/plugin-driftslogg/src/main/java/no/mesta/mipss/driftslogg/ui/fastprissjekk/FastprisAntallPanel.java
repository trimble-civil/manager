package no.mesta.mipss.driftslogg.ui.fastprissjekk;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.swingx.JXHeader;

@SuppressWarnings("serial")
public class FastprisAntallPanel extends JPanel{

	private JLabel lblAntall;
	private JComboBox cbxAntall;
	private DefaultComboBoxModel mdlAntall;
	
	private JButton btnOk;
	private JButton btnAvbryt;
	
	private boolean cancelled=true;
	
	public FastprisAntallPanel(){
		initComponents();
		initGui();
	}
	public boolean isCancelled(){
		return cancelled;
	}
	
	private void initComponents() {
		lblAntall = new JLabel(Resources.getResource("label.antall"));
		
		mdlAntall = new DefaultComboBoxModel(new Object[]{1d, 0.5d});
		cbxAntall = new JComboBox(mdlAntall);
		btnOk = new JButton(getOkAction());
		btnAvbryt = new JButton(getAvbrytAction());
	}
	
	public Double getAntall(){
		return (Double)cbxAntall.getSelectedItem();
	}
	private JXHeader createHeader(){
		return new JXHeader(Resources.getResource("message.leggeTilFastpris.title"), Resources.getResource("message.leggeTilFastpris"), IconResources.HELP_ICON);
	}
	private Action getOkAction(){
		return new AbstractAction(Resources.getResource("button.leggTil"), IconResources.OK2_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=false;
				SwingUtilities.windowForComponent(FastprisAntallPanel.this).dispose();
				
			}
		};
	}
	
	private Action getAvbrytAction(){
		return new AbstractAction(Resources.getResource("button.avbryt"), IconResources.ABORT_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.windowForComponent(FastprisAntallPanel.this).dispose();
			}
		};
	}
	private void initGui() {
		setLayout(new BorderLayout());
		
		JPanel pnlButton = new JPanel(new GridBagLayout());
		pnlButton.add(btnOk, 	 new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		pnlButton.add(btnAvbryt, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		
		JPanel pnlMain = new JPanel(new GridBagLayout());
		pnlMain.add(lblAntall,	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		pnlMain.add(cbxAntall, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,5),0,0));
		
		add(createHeader(), BorderLayout.NORTH);
		add(pnlMain);
		add(pnlButton, BorderLayout.SOUTH);
	}
}
