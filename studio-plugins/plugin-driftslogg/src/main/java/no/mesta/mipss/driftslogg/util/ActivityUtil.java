package no.mesta.mipss.driftslogg.util;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.PeriodeStatus;
import no.mesta.mipss.persistence.driftslogg.Stroproduktmengde;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;

import java.util.ArrayList;
import java.util.List;

public class ActivityUtil {

    private final List<Prodtype> prodtypes;
    private List<KjoretoyRode> vehicleRodeConnections;
    private List<Aktivitet> activitiesInNeedOfComments;

    public ActivityUtil(List<Prodtype> prodtypes) {
        this.prodtypes = prodtypes;
        this.vehicleRodeConnections = new ArrayList<>();
        this.activitiesInNeedOfComments = new ArrayList<>();
    }

    public boolean needsComment(Aktivitet activity) {
        if (activity.getInternKommentar() != null && !activity.getInternKommentar().trim().isEmpty()) {
            activitiesInNeedOfComments.remove(activity);
            return false;
        }

        if (activity.isCreatedManager()) {
            addToActivitiesInNeedOfComments(activity);
            return true;
        }

        if (activity.isApprovedIntention() && isProductionOutsideConnectedRode(activity)) {
            addToActivitiesInNeedOfComments(activity);
            return true;
        }

        if (!containsValues(activity)) {
            activitiesInNeedOfComments.remove(activity);
            return false;
        }

        boolean needsComment = shouldHaveApprovedBuild(activity) != activity.isApprovedBuild();
        if (needsComment) {
            addToActivitiesInNeedOfComments(activity);
        } else {
            activitiesInNeedOfComments.remove(activity);
        }

        return needsComment;
    }

    boolean containsValues(Aktivitet activity) {
        if((activity.getR12_broyting() != null && activity.getR12_broyting()>0) ||
                (activity.getR12_sideplog() != null && activity.getR12_sideplog()>0) ||
                (activity.getR12_hovling() != null && activity.getR12_hovling()>0))
            return true;

        for(Stroproduktmengde amount : activity.getStroprodukter()) {
            if(amount.getOmforentMengdeTort()!= null && amount.getOmforentMengdeTort()>0)
                return true;

            if(amount.getOmforentMengdeVaatt()!=null && amount.getOmforentMengdeVaatt()>0)
                return true;
        }

        return false;
    }

    boolean shouldHaveApprovedBuild(Aktivitet activity) {
        if(activity.isManuallyCreated() || activity.isProductionOutsideRode() || activity.isProductionOutsideContract()) {
            return false;
        }

        for(Prodtype currentType : parseProdTypeString(activity.getProductionType())) {
            if(currentType.getDauFlagg()) {
                return true;
            }
        }

        return false;
    }

    List<Prodtype> parseProdTypeString(String prodTypeString) {
        List<Prodtype> prodtypes = new ArrayList<>();
        if(prodTypeString.indexOf(':')>0) {
            prodTypeString = prodTypeString.substring(0, prodTypeString.indexOf(':')).trim();
        }
        for(String name : prodTypeString.split("\\+")) {
            for(Prodtype prodtype : this.prodtypes) {
                if(prodtype.getNavn().equals(name)) {
                    prodtypes.add(prodtype);
                }
            }
        }
        return prodtypes;
    }

    public void setVehicleRodeConnections(List<KjoretoyRode> vehicleRodeConnections) {
        this.vehicleRodeConnections = vehicleRodeConnections;
    }

    public void clearActivitiesInNeedOfComments() {
        activitiesInNeedOfComments.clear();
    }

    /**
     * This only applies to activities where production involves plowing/"brøyting".
     * @param activity
     * @return
     */
    public boolean isProductionOutsideConnectedRode(Aktivitet activity) {
        if (activity.getProductionType() != null && activity.getProductionType().toLowerCase().contains("brøyting")) {
            for (KjoretoyRode connection : vehicleRodeConnections) {
                if (connection.getRodeId().equals(activity.getRodeId())) {
                    return false;
                }
            }

            // Activities created in Manager needs to have this flag set while activities from DL-web are already flagged.
            activity.setProductionOutsideRode(true);
            return true;
        }
        return false;
    }

    public void addToActivitiesInNeedOfComments(Aktivitet activity) {
        if (!activitiesInNeedOfComments.contains(activity)) {
            activitiesInNeedOfComments.add(activity);
        }
    }

    public List<Aktivitet> getActivitiesInNeedOfComments() {
        return activitiesInNeedOfComments;
    }

}