package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.driftslogg.util.ActivityUtil;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TableMissingInternalCommentHighlightPredicate implements HighlightPredicate {

    private final AktivitetTableModel model;
    private ActivityUtil activityUtil;

    public TableMissingInternalCommentHighlightPredicate(AktivitetTableModel mdlAktivitet, ActivityUtil activityUtil) {
        this.model = mdlAktivitet;
        this.activityUtil = activityUtil;
    }

    @Override
    public boolean isHighlighted(Component component, ComponentAdapter componentAdapter) {
        Aktivitet activity = model.getAktiviteter().get(componentAdapter.row);
        return componentAdapter.column == AktivitetTableModel.COL_INTERN_KOMMENTAR && activityUtil.needsComment(activity);
    }

}