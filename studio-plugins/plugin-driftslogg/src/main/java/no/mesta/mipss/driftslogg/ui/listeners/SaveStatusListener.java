package no.mesta.mipss.driftslogg.ui.listeners;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import no.mesta.mipss.core.SaveStatusMonitor;

public class SaveStatusListener implements PropertyChangeListener{
	List changedEntitiesList = new ArrayList();
	@SuppressWarnings("rawtypes")
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("changed".equals(evt.getPropertyName())){
			
			if (evt.getSource() instanceof SaveStatusMonitor){
				SaveStatusMonitor sm = (SaveStatusMonitor)evt.getSource();
				sm.getBeanInstance();
			}
				
			if (evt.getNewValue().equals(Boolean.TRUE)){
				changedEntitiesList.add(null);
			}
			if (evt.getNewValue().equals(Boolean.FALSE)){
				changedEntitiesList.remove(null);
			}
		}
	}
	public List getChangedEntities(){
		return changedEntitiesList;
	}
}