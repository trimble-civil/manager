package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.Stroproduktmengde;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.ui.table.AbstractMipssTreeTableModel;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class AktivitetTableModel extends AbstractMipssTreeTableModel {
	public static final int COL_ID 							= 0;
	public static final int COL_RODE                        = 1;
	public static final int COL_ARTICLE                     = 2;
	public static final int COL_UNIT                        = 3;
	public static final int COL_UE_AGREED_AMOUNT 			= 4;
	public static final int COL_SUM 						= 5;
	public static final int COL_UE_COMMENT                  = 6;
	public static final int COL_WARNINGS					= 7;
	public static final int COL_APPROVED_UE					= 8;
	public static final int COL_APPROVED_BUILD				= 9;
	public static final int COL_APPROVED_INTENTION          = 10;
	public static final int COL_PRODUCTION_TYPE 			= 11;
	public static final int COL_R12_PLOWING 				= 12;
	public static final int COL_R12_INTENTION               = 13;
	public static final int COL_R12_SIDE_PLOUGH 			= 14;
	public static final int COL_R12_HEAVY_PLANING 			= 15;
	public static final int COL_STROPRODUKTNAVN 			= 16;
	public static final int COL_DRY_GRIT 					= 17;
	public static final int COL_WET_GRIT 					= 18;
	public static final int COL_INTERN_KOMMENTAR			= 19;
	
	private AktivitetTreeView myroot = new AktivitetTreeView(null);
	

	private List<Aktivitet> aktiviteter;
	private List<Stroproduktmengde> mengder;
    private List<Rode> rodes;
	
	private boolean editable=true;
	private DecimalFormat format;
	private final DriftsloggController driftsloggController;
	
	public AktivitetTableModel(DriftsloggController driftsloggController) {
		this.driftsloggController = driftsloggController;
		
		DecimalFormatSymbols formatSymbols = DecimalFormatSymbols.getInstance();
		formatSymbols.setDecimalSeparator(',');
		format = new DecimalFormat("#.###", formatSymbols);
	}

    public AktivitetTableModel(DriftsloggController driftsloggController, List<Rode> rodes) {
        this.driftsloggController = driftsloggController;
        this.rodes = rodes;

        DecimalFormatSymbols formatSymbols = DecimalFormatSymbols.getInstance();
        formatSymbols.setDecimalSeparator(',');
        format = new DecimalFormat("#.###", formatSymbols);
    }

	public void setEditable(boolean editable){
		this.editable = editable;
	}
	public void loadData(DriftsloggController controller){
		startLoading(controller);
	}
	
	@Override
	protected void load(Object beanSourceController, SwingWorker currentWorker) {
		
		DriftsloggController controller = (DriftsloggController)beanSourceController;
		List<Aktivitet> a = controller.getAktiviteter(controller.getQueryParams());
		if (!currentWorker.isCancelled()){
			load(a);
		}
		
	}
	
	public void load(List<Aktivitet> aktiviteter){
		this.aktiviteter = aktiviteter;
		mengder = new ArrayList<>();
		
		Object[] children = myroot.getChildren().toArray();
		TreePath p = new TreePath(myroot);
		int[] idx = new int[children.length];
		for (int i=0;i<children.length;i++){
			idx[i]=i;
		}
		myroot.getChildren().clear();
		modelSupport.fireChildrenRemoved(p, idx, children);
		
		for (Aktivitet a:aktiviteter){
			AktivitetTreeView at = new AktivitetTreeView(myroot);
			at.setAktivitet(a);

			if (a.getStroprodukter()!=null&&!a.getStroprodukter().isEmpty()){
				at.setStroproduktmengde(a.getStroprodukter().get(0));
				mengder.add(a.getStroprodukter().get(0));

				for (int i=1;i<a.getStroprodukter().size();i++){
					Stroproduktmengde stroproduktmengde = a.getStroprodukter().get(i);
					mengder.add(stroproduktmengde);
					AktivitetTreeView st = new AktivitetTreeView(at);
					st.setStroproduktmengde(stroproduktmengde);
					at.getChildren().add(st);
				}
			} else {
			    at.setStroproduktmengde(new Stroproduktmengde());
            }

			myroot.getChildren().add(at);
			modelSupport.fireChildAdded(new TreePath(myroot), myroot.getChildren().size()-1, at);
		}
	}
	
	public void fireTreeChanged(){
		load(aktiviteter);
	}
	
	public List<Aktivitet> getAktiviteter(){
		return aktiviteter;
	}
	public List<Stroproduktmengde> getStroproduktmengder(){
		return mengder;
	}
	@Override
	public Object getRoot(){
		return myroot;
	}
	
	@Override
	public int getColumnCount() {
		return COL_INTERN_KOMMENTAR;
	}

	@Override
	public Object getValueAt(Object node, int column) {
		AktivitetTreeView a = (AktivitetTreeView)node;
		switch(column){
			case COL_ID:return a.getId();
			case COL_RODE:return getRodeNameFromRodeId(a.getRodeId());
			case COL_ARTICLE:return a.getArticle();
			case COL_UE_AGREED_AMOUNT:return a.getOmforentMengde();
			case COL_UNIT: return a.getEnhet();
			case COL_SUM:return a.getSum();
			case COL_UE_COMMENT:return a.getKommentar();
			case COL_WARNINGS: return a.getAktivitet();
			case COL_APPROVED_UE: return a.isApprovedUE();
			case COL_APPROVED_BUILD: return a.isApprovedBuild();
			case COL_APPROVED_INTENTION: return a.isApprovedIntention();
			case COL_PRODUCTION_TYPE: return a.getProductionType();
			case COL_R12_PLOWING: return a.getR12Plowing();
			case COL_R12_INTENTION: return a.getR12Intention();
			case COL_R12_SIDE_PLOUGH: return a.getR12SidePlough();
			case COL_R12_HEAVY_PLANING: return a.getR12HeavyPlaning();
			case COL_STROPRODUKTNAVN:return a.getGritProductName();
			case COL_DRY_GRIT:return a.getDryGritAmount();
			case COL_WET_GRIT: return a.getWetGritAmount();
			case COL_INTERN_KOMMENTAR:return a.getInternkommentar();
		}
		return "<UKJENT KOLONNE>";
	}
	
	/**
     * {@inheritDoc}
     */
    public Class<?> getColumnClass(int column) {
    	switch(column){
			case COL_ID:return Long.class;
			case COL_RODE:return String.class;
			case COL_ARTICLE:return AgrArtikkelV.class;
            case COL_UNIT: return String.class;
			case COL_UE_AGREED_AMOUNT:return Double.class;
			case COL_SUM:return Double.class;
			case COL_UE_COMMENT:return String.class;
			case COL_WARNINGS: return Aktivitet.class;
			case COL_APPROVED_UE: return Boolean.class;
			case COL_APPROVED_BUILD: return Boolean.class;
			case COL_APPROVED_INTENTION: return Boolean.class;
			case COL_PRODUCTION_TYPE: return String.class;
			case COL_R12_PLOWING: return Double.class;
			case COL_R12_INTENTION: return Double.class;
			case COL_R12_SIDE_PLOUGH: return Double.class;
			case COL_R12_HEAVY_PLANING: return Double.class;
			case COL_STROPRODUKTNAVN:return String.class;
			case COL_DRY_GRIT: return Double.class;
			case COL_WET_GRIT: return Double.class;
			case COL_INTERN_KOMMENTAR:return String.class;
			default:
			return Object.class;
    	}
    }

    private String getRodeNameFromRodeId(Long rodeId) {
        for (Rode r : rodes) {
            if (r.getId().equals(rodeId)) {
                return r.getNavn();
            }
        }
        return null;
    }

    private Long getRodeIdFromRodeName(Object rodeName) {
        if (rodeName != null) {
            for (Rode r : rodes) {
                if (r.getNavn().equalsIgnoreCase(rodeName.toString())) {
                    return r.getId();
                }
            }
        }
        return null;
    }

	@Override
	public void setValueAt(Object value, Object node, int column) {
		if (!editable)
			return;
		AktivitetTreeView a = (AktivitetTreeView)node;
		
		
		//TODO refactor dette med et event slik at dette kan håndteres i controller
		if (column== COL_UE_AGREED_AMOUNT ||column== COL_DRY_GRIT ||column== COL_WET_GRIT){
			Double valueDouble = getDouble(value);
			if (valueDouble!=null && valueDouble<=0){
				if (!driftsloggController.visBekreftMengde(valueDouble)){
					return;
				}
			}
		}

        switch (column){
            case COL_RODE:
                a.setRodeId(getRodeIdFromRodeName(value));
                a.setRodeViewName(value);
                break;
            case COL_ARTICLE:
                a.setArticle(value);
                // We need to fire a child changed event to display the unit associated with this article.
                modelSupport.fireChildChanged(new TreePath(myroot), myroot.getChildren().indexOf(a), a);
                break;
			case COL_UE_COMMENT:
				a.setComment(value.toString());
				break;
            case COL_UE_AGREED_AMOUNT:
                a.setAgreedAmount(getDouble(value));
                // We need to fire a child changed event to recalculate the new sum.
                modelSupport.fireChildChanged(new TreePath(myroot), myroot.getChildren().indexOf(a), a);
                break;
			case COL_APPROVED_BUILD:
				a.setApprovedBuild(getBoolean(value));
				if (!a.isApprovedBuild()) {
					a.setApprovedIntention(false);
				}
				break;
			case COL_APPROVED_UE:
				a.setApprovedUE(getBoolean(value));
				modelSupport.fireChildChanged(new TreePath(myroot), myroot.getChildren().indexOf(a), a);
				break;
			case COL_APPROVED_INTENTION:
				a.setApprovedIntention(getBoolean(value));
				if (a.isApprovedIntention()) {
					a.setApprovedBuild(true);
				}
				break;
            case COL_PRODUCTION_TYPE:
                a.setProductionType(value);
                // We need to fire a child changed event to display the grit type obtained from this production type.
                modelSupport.fireChildChanged(new TreePath(myroot), myroot.getChildren().indexOf(a), a);
                break;
            case COL_R12_PLOWING: a.setR12Plowing(getDouble(value)); break;
			case COL_R12_INTENTION: a.setR12Intention(getDouble(value)); break;
            case COL_R12_SIDE_PLOUGH: a.setR12SidePlough(getDouble(value)); break;
            case COL_R12_HEAVY_PLANING: a.setR12HeavyPlaning(getDouble(value)); break;
        	case COL_DRY_GRIT: a.setDryGritAmount(getDouble(value)); break;
        	case COL_WET_GRIT: a.setWetGritAmount(getDouble(value)); break;
        	case COL_INTERN_KOMMENTAR: a.setInternkommentar(value.toString());break;
        }
	}

	private Boolean getBoolean(Object value) {
		if(value instanceof Boolean) {
			return (Boolean)value;
		}
		throw new IllegalArgumentException("Expected a boolean value");
	}
	
	private Double getDouble(Object value){
        if (value==null||"".equals(value))
            return null;
        try{
            String valueS = value.toString();
            if (valueS.contains(".")){
                valueS = valueS.replace('.', ',');
            }
            Double parsed = format.parse(valueS).doubleValue();
            return parsed;
        } catch (ParseException e) {
            return null;
        }
    }

	@Override
    public boolean isCellEditable(Object node, int column) {
		if (!editable) return false;

		AktivitetTreeView a = (AktivitetTreeView)node;
		if (!a.isEditStatus()){
			return false;
		}

		if (a.getAktivitet().isCreatedManager()) {
		    return
                    column != COL_ID &&
                    column != COL_UNIT &&
                    column != COL_SUM &&
                    column != COL_STROPRODUKTNAVN;
        }

        if (a.getAktivitet().getR12_intention() == null && column == COL_APPROVED_INTENTION) {
			return false;
		}

		return 	column == COL_APPROVED_BUILD ||
				column == COL_APPROVED_UE ||
				column == COL_APPROVED_INTENTION ||
				column == COL_INTERN_KOMMENTAR;
    }

	@Override
	public Object getChild(Object parent, int index) {
		AktivitetTreeView node =(AktivitetTreeView)parent;
		return node.getChildren().get(index);
	}

	@Override
	public int getChildCount(Object parent) {
		AktivitetTreeView node =(AktivitetTreeView)parent;
		return node.getChildren().size();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		AktivitetTreeView node =(AktivitetTreeView)parent;
		for (int i=0;i<node.getChildren().size();i++){
			if (node.getChildren().get(i)==child){
				return i;
			}
		}
		return 0;
	}
	
	public boolean isLeaf( Object node ){
		AktivitetTreeView view =(AktivitetTreeView)node;
		return view.isLeaf();
	}

}