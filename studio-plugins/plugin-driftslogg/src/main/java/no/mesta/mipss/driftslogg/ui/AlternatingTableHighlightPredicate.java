package no.mesta.mipss.driftslogg.ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.text.Highlighter;

import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

public class AlternatingTableHighlightPredicate implements HighlightPredicate{
	@Override
	public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
		return adapter.row%2==0;
	}
	
	public ColorHighlighter getHighlighter(){
		return new ColorHighlighter(this ,new Color(230, 236, 243), null,null, null);
	}
}
