package no.mesta.mipss.driftslogg.ui;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

import static no.mesta.mipss.driftslogg.ui.AktivitetTableModel.*;

public class TripTableHighlightPredicate implements HighlightPredicate {

    private final AktivitetTableModel model;

    public TripTableHighlightPredicate(AktivitetTableModel mdlAktivitet) {
        this.model = mdlAktivitet;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        return isHighlightedRow(adapter.row) && isHighlightedColumn(adapter.column);
    }

    boolean isHighlightedRow(int rowNumber) {
        boolean isHighlighted = true;

        for (int i = 1; i <= rowNumber; i++) {
            long currentTripId = model.getAktiviteter().get(i).getTrip().getId();
            long previousTripId = model.getAktiviteter().get(i - 1).getTrip().getId();
            if (currentTripId != previousTripId) {
                isHighlighted = !isHighlighted;
            }
        }

        return isHighlighted;
    }

    boolean isHighlightedColumn(int colNumber) {
        switch (colNumber) {
            case COL_WARNINGS:
            case COL_APPROVED_BUILD:
            case COL_APPROVED_UE:
            case COL_APPROVED_INTENTION:
                return false;
            default:
                return true;
        }
    }
}
