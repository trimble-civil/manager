package no.mesta.mipss.driftslogg.ui.fastprissjekk;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.FastprisStatus;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.driftslogg.ui.AlternatingTableHighlightPredicate;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.decorator.Highlighter;

@SuppressWarnings("serial")
public class FastprisSlettPanel extends JPanel{
	private JMipssBeanTable<Aktivitet> tblAktivitet;
	private MipssBeanTableModel<Aktivitet> mdlAktivitet;
	private JMipssBeanScrollPane scrAktivitet;
	
	private JButton btnSlettValgte;
	private JButton btnAvbryt;
	private JLabel lblSletteInfo;
	
	private final FastprisStatus toRemove;
	private final DriftsloggController controller;
	
	private BindingGroup bg = new BindingGroup();
	
	public FastprisSlettPanel(DriftsloggController controller, FastprisStatus toRemove){
		
		this.controller = controller;
		this.toRemove = toRemove;
		initComponents();
		initGui();
		bind();
	}

	private void initComponents() {
		mdlAktivitet = new MipssBeanTableModel<Aktivitet>(Aktivitet.class, "id", "artikkelIdent", "enhetsPris", "fraDatoTid", "tilDatoTid", "omforentMengde", "opprettetAv", "opprettetDato");
		MipssRenderableEntityTableColumnModel aktivitetColumnModel = new MipssRenderableEntityTableColumnModel(Aktivitet.class, mdlAktivitet.getColumns());
		
		tblAktivitet = new JMipssBeanTable<Aktivitet>(mdlAktivitet, aktivitetColumnModel);
		tblAktivitet.setFillsViewportHeight(true);
		tblAktivitet.setFillsViewportWidth(true);
		tblAktivitet.setDoWidthHacks(true);
		AlternatingTableHighlightPredicate highlight = new AlternatingTableHighlightPredicate();
		tblAktivitet.setHighlighters(new Highlighter[] {highlight.getHighlighter()});
		tblAktivitet.getColumnExt(0).setPreferredWidth(30);
		tblAktivitet.getColumnExt(2).setPreferredWidth(60);
		tblAktivitet.getColumnExt(3).setPreferredWidth(65);
		tblAktivitet.getColumnExt(4).setPreferredWidth(65);
		tblAktivitet.getColumnExt(5).setPreferredWidth(45);
		
		
		mdlAktivitet.setEntities(toRemove.getAktiviteter());
		scrAktivitet = new JMipssBeanScrollPane(tblAktivitet, mdlAktivitet);
		btnSlettValgte = new JButton(controller.getSlettAktivitetAction(tblAktivitet, mdlAktivitet));
		btnAvbryt = new JButton(getAvbrytAction());
		
		lblSletteInfo = new JLabel();
		lblSletteInfo.setForeground(Color.red);
	}
	
	private void bind(){
		Binding<JMipssBeanTable<Aktivitet>, Object, FastprisSlettPanel, Object> selectBinding = BindingHelper.createbinding(tblAktivitet, "selectedEntities", this, "selectedEntities");
		bg.addBinding(selectBinding);
		bg.bind();
	}
	
	public void setSelectedEntities(List<Aktivitet> entities){
		btnSlettValgte.setEnabled(true);
		lblSletteInfo.setText("");
		for (Aktivitet a:entities){
			if (!a.getOpprettetAv().startsWith("MIPSS_")){
				btnSlettValgte.setEnabled(false);
				lblSletteInfo.setText(Resources.getResource("label.kanIkkeSlette"));
			}
			if (a.getSendtAgresso()){
				btnSlettValgte.setEnabled(false);
				lblSletteInfo.setText(Resources.getResource("label.kanIkkeSletteSendtAgresso"));	
			}
		}
	}

	private AbstractAction getAvbrytAction(){
		return new AbstractAction(Resources.getResource("button.lukk"), IconResources.CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.windowForComponent(FastprisSlettPanel.this).dispose();
			}
		};
	}
	
	private JXHeader createHeader(){
		JXHeader head = new JXHeader(Resources.getResource("fastpris.slett.title"), Resources.getResource("fastpris.slett.description"));
		head.setIcon(IconResources.SHRED_48);
		return head;
	}
	private void initGui() {
		setLayout(new BorderLayout());
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		pnlButtons.add(lblSletteInfo,	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10,5,10,5),0,0));
		pnlButtons.add(btnSlettValgte,	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,10,5),0,0));
		pnlButtons.add(btnAvbryt, 		new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,10,5),0,0));
		
		add(createHeader(), BorderLayout.NORTH);
		add(scrAktivitet);
		add(pnlButtons, BorderLayout.SOUTH);
		
	}

}
