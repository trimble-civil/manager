package no.mesta.mipss.driftslogg.ui.agressoinfo;

import no.mesta.mipss.driftslogg.MipssInput;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

@SuppressWarnings("serial")
public class MipssInputTableObject extends MipssInput implements CheckBoxTableObject{
	
	public MipssInputTableObject(MipssInput input){
		super(input);
	}
	private boolean valgt=true;
	
	@Override
	public void setValgt(Boolean valgt) {
		this.valgt = valgt;
	}

	@Override
	public Boolean getValgt() {
		return valgt;
	}

}
