package no.mesta.mipss.driftslogg.ui;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.Leverandor;

public class LeverandorCellRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object> {
	
	private String emptyText = null;
	private List<KontraktLeverandor> kontraktLeverandorer;
    
    public LeverandorCellRenderer(String emptyText, List<KontraktLeverandor> kontraktLeverandorer) {
    	this.emptyText = emptyText;
    	this.kontraktLeverandorer = kontraktLeverandorer;
    }
    
    @Override
	public Component getListCellRendererComponent(JList list, Object value, 
			int index, 
			boolean isSelected, 
			boolean cellHasFocus) {
    	Leverandor entity = null;

		if(value != null && value instanceof Leverandor) { //.class.equals(value.getClass())) {
			entity = (Leverandor) value;
		}

		StringBuilder text = new StringBuilder();
		if(entity != null) {
			text.append(entity.getTextForGUI());
		} else {
            text.append(emptyText);
        }
		
		//Legger til utgått-info på kontraktleverandørene
		if (kontraktLeverandorer != null) {
			for (KontraktLeverandor kl : kontraktLeverandorer) {
				if (kl != null && entity != null) {
					if (kl.getLeverandorNr().equals(entity.getNr())) {
						text.append(" (" + entity.getNr() + ")");
						if (kl.getTilDato().before(Clock.now())) {							
							text.append(", utgått " + MipssDateFormatter.formatDate(kl.getTilDato(), MipssDateFormatter.DATE_FORMAT));
							list.setForeground(Color.GRAY);
						} else{
							list.setForeground(Color.BLACK);
						}
					}
				}			
			}
		}		
		
		return super.getListCellRendererComponent(list, text.toString(), index, isSelected, cellHasFocus);
	}
}
