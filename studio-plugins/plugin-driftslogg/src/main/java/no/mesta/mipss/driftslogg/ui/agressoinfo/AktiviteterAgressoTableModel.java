package no.mesta.mipss.driftslogg.ui.agressoinfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.SwingWorker;
import javax.swing.tree.TreePath;

import org.apache.commons.digester.SetRootRule;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.MipssInputTreeView;
import no.mesta.mipss.ui.table.AbstractMipssTreeTableModel;

public class AktiviteterAgressoTableModel extends AbstractMipssTreeTableModel{
	public static final int COL_TREEVIEW 					= 0;//aktivitetId/leverandor
	public static final int COL_LEVKONTRAKT					= 1;//levkontrakt
	public static final int COL_ARTIKKEL					= 2;//artikkel
	public static final int COL_ENHET						= 3;//enhet
	public static final int COL_ANTALL						= 4;//antall
	public static final int COL_PRIS						= 5;//pris
	public static final int COL_TRIP_ID						= 6;//trip id
	public static final int COL_KOMMENTAR					= 7;//ukenummer
	
	private MipssInputTreeView myroot = new MipssInputTreeView(null, null);
	private final List<Long> perioderSomManglerAttestant;
	private final Long mnd;
	private final Long aar;
	private final boolean skalOverfores;
	private List<MipssInputTreeView> artikler;
	
	public AktiviteterAgressoTableModel(List<Long> perioderSomManglerAttestant, Long mnd, Long aar, boolean skalOverfores){
		this.perioderSomManglerAttestant = perioderSomManglerAttestant;
		this.mnd = mnd;
		this.aar = aar;
		this.skalOverfores = skalOverfores;
	}
	public List<MipssInputTreeView> getAktiviteter(){
		return artikler;
	}
	
	@Override
	public int getColumnCount() {
		return 8;
	}
	@Override
	public Object getValueAt(Object node, int column) {
		MipssInputTreeView a = (MipssInputTreeView)node;
		switch(column){
			case COL_TREEVIEW:return a.getInfo();
			case COL_LEVKONTRAKT:return a.getLevkontraktIdent();
			case COL_ARTIKKEL:return a.getArtikkel();
			case COL_ENHET:return a.getEnhet();
			case COL_ANTALL:return a.getAntall();
			case COL_PRIS:return a.getSum();
			case COL_TRIP_ID:return a.getTripId();
			case COL_KOMMENTAR:return a.getKommentar();
		}
		return "<UKJENT KOLONNE>";
	}
	@Override
	public Object getChild(Object parent, int index) {
		MipssInputTreeView node =(MipssInputTreeView)parent;
		return node.getMipssInputList().get(index);
	}
	@Override
	public int getChildCount(Object parent) {
		MipssInputTreeView node =(MipssInputTreeView)parent;
		return node.getMipssInputList().size();
	}
	@Override
	public int getIndexOfChild(Object parent, Object child) {
		MipssInputTreeView node =(MipssInputTreeView)parent;
		for (int i=0;i<node.getMipssInputList().size();i++){
			if (node.getMipssInputList().get(i)==child){
				return i;
			}
		}
		return 0;
	}
	@Override
	protected void load(Object beanSourceController, SwingWorker currentWorker) {
		DriftsloggController controller = (DriftsloggController)beanSourceController;
		setAktiviteter(skalOverfores?
				controller.getArtiklerTilAgresso(mnd, aar, perioderSomManglerAttestant):
				controller.getArtiklerIkkeTilAgresso(mnd, aar, perioderSomManglerAttestant));
	}
	public void setAktiviteter(List<MipssInputTreeView> artikler){
		Object[] children = myroot.getMipssInputList().toArray();
		TreePath p = new TreePath(myroot);
		int[] idx = new int[children.length];
		for (int i=0;i<children.length;i++){
			idx[i]=i;
		}
		myroot.getMipssInputList().clear();
		modelSupport.fireChildrenRemoved(p, idx, children);
		
		this.artikler = artikler;
		Comparator<MipssInputTreeView> comp = new Comparator<MipssInputTreeView>() {
			@Override
			public int compare(MipssInputTreeView o1, MipssInputTreeView o2) {
				return o1.getMipssInput().getDelivDate().compareTo(o2.getMipssInput().getDelivDate());
			}
		};
		
		if (skalOverfores){
			for (MipssInputTreeView view:artikler){
				if (view.getOverforLeverandor()) {					
					MipssInputTreeView viewInTree = new MipssInputTreeView(view.getLeverandor(), view.getLeverandorNr());
					List<MipssInputTreeView> cleanList = new ArrayList<MipssInputTreeView>();
					for (MipssInputTreeView ch:view.getMipssInputList()){
						if (!ch.getMipssInput().skalOverfores())
							continue;	
						cleanList.add(ch);
					}
					Collections.sort(cleanList, comp);
					for (MipssInputTreeView ch:cleanList){
						viewInTree.addChild(ch);
					}

					myroot.getMipssInputList().add(viewInTree);
					modelSupport.fireChildAdded(new TreePath(myroot), myroot.getMipssInputList().size()-1, view);	
				}
			}
		}else{
			for (MipssInputTreeView view:artikler){
				Collections.sort(view.getMipssInputList(), comp);
				myroot.getMipssInputList().add(view);
				modelSupport.fireChildAdded(new TreePath(myroot), myroot.getMipssInputList().size()-1, view);
			}
		}
	}

	public void loadData(DriftsloggController controller) {
		startLoading(controller);
	}
	
	@Override
	public Object getRoot(){
		return myroot;
	}
}
