package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

import static no.mesta.mipss.driftslogg.ui.AktivitetTableModel.*;

public class TablePristineActivityHighlightPredicate implements HighlightPredicate {

    private final AktivitetTableModel model;

    public TablePristineActivityHighlightPredicate(AktivitetTableModel mdlAktivitet) {
        this.model = mdlAktivitet;
    }

    @Override
    public boolean isHighlighted(Component component, ComponentAdapter componentAdapter) {
        return !model.getAktiviteter().get(componentAdapter.row).containsWarnings();
    }

}
