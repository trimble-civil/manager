package no.mesta.mipss.driftslogg.ui.mipssrapport;

import java.util.List;

import javax.swing.SwingWorker;
import javax.swing.tree.TreePath;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.MipssRodeTiltak;
import no.mesta.mipss.persistence.driftslogg.MipssTiltak;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.table.AbstractMipssTreeTableModel;

public class MipssRapportTableModel extends AbstractMipssTreeTableModel{
	public static final int COL_TILTAK 		= 0;
	public static final int COL_KM			= 1;
	public static final int COL_TIMER		= 2;
	public static final int COL_STROPRODUKT	= 3;
	public static final int COL_TONNTORR	= 4;
	public static final int COL_LITER_LOSNING_TOTALT = 5;
	public static final int COL_KG_LOSNING = 6;
	
	private MipssrapportTreeView myroot = new MipssrapportTreeView(null);
	private JMipssTreeTable<MipssrapportTreeView> tblMipsstall;

	public MipssRapportTableModel() {
	}

	/**
	 * Laster tabellen 
	 * @param controller
	 */
	public void loadData(DriftsloggController controller){
		startLoading(controller);
	}
	
	@Override
	protected void load(Object beanSourceController, SwingWorker currentWorker) {
		DriftsloggController controller = (DriftsloggController)beanSourceController;
		load(controller.searchORData(controller.getSelectedTrips()));
	}
	
	public void load(List<MipssRodeTiltak> list){
		Object[] children = myroot.getChildren().toArray();
		TreePath p = new TreePath(myroot);
		int[] idx = new int[children.length];
		for (int i=0;i<children.length;i++){
			idx[i]=i;
		}
		myroot.getChildren().clear();
		modelSupport.fireChildrenRemoved(p, idx, children);
		if (list==null)
			return;
		for (MipssRodeTiltak rt:list){
			MipssrapportTreeView view = new MipssrapportTreeView(myroot);
			view.setNavn(rt.getRode());
			for (MipssTiltak t: rt.getTiltak()){
				MipssrapportTreeView tiltak = new MipssrapportTreeView(view);
				tiltak.setTiltak(t);
				view.getChildren().add(tiltak);
			}
			myroot.getChildren().add(view);
			modelSupport.fireChildAdded(new TreePath(myroot), myroot.getChildren().size()-1, view);
		}
		TreePath sum = tblMipsstall.getPathForRow(0);
		tblMipsstall.expandPath(sum);
	}
	
	@Override
	public Object getRoot(){
		return myroot;
	}
	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(Object node, int column) {
		MipssrapportTreeView r = (MipssrapportTreeView)node;
		switch(column){
		case COL_TILTAK: return r.getTiltak();
		case COL_KM: return r.getKm();
		case COL_TIMER: return r.getTimer();
		case COL_STROPRODUKT: return r.getStroprodukt();
		case COL_TONNTORR: return r.getTonnTorr();
		case COL_LITER_LOSNING_TOTALT: return r.getLiterLosningTotalt();
		case COL_KG_LOSNING: return r.getKgLosning();
		}
		return "<Ukjent kolonne>";
	}

	@Override
	public Object getChild(Object parent, int index) {
		MipssrapportTreeView r = (MipssrapportTreeView)parent;
		return r.getChildren().get(index);
	}

	@Override
	public int getChildCount(Object parent) {
		MipssrapportTreeView r = (MipssrapportTreeView)parent;
		return r.getChildren().size();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		MipssrapportTreeView r = (MipssrapportTreeView)parent;
		for (int i=0;i<r.getChildren().size();i++){
			if (r.getChildren().get(i)==child){
				return i;
			}
		}
		return 0;
	}
	
	public boolean isLeaf(Object node){
		MipssrapportTreeView view =(MipssrapportTreeView)node;
		return view.isLeaf();
	}

	public void setTreeView(JMipssTreeTable<MipssrapportTreeView> tblMipsstall) {
		this.tblMipsstall = tblMipsstall;
	}
}
