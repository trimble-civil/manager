package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.persistence.driftslogg.Warning;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.net.URL;

import static no.mesta.mipss.ui.Colors.OK_COLOR;
import static no.mesta.mipss.ui.Colors.WARNING_COLOR;

public class WarningTableCellRenderer extends DefaultTableCellRenderer {

	private static final String CHANGED_ARTICLE_URL = "warningChangedArticle.png";
	private static final String MANUALLY_CREATED_URL = "warningManuallyCreated.png";
	private static final String NO_PAY_URL = "warningNoPay.png";
	private static final String OUTSIDE_CONTRACT_URL = "warningOutsideContract.png";
	private static final String OUTSIDE_RODE_URL = "warningOutsideRode.png";

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (value instanceof Warning) {
			Warning warning = (Warning) value;
			return getWarningInfo(warning, table.getBackground(), table.getSelectionBackground(), isSelected);
		}
		throw new IllegalArgumentException("Can only render Warning");
	}

	public JPanel getWarningInfo(Warning warning, Color normalBackground, Color selectedBackground, boolean isSelected) {
		JPanel cell = new JPanel(new BorderLayout());
		JPanel container = new JPanel(new FlowLayout());
		container.setOpaque(false);

        JLabel noWarnings = new JLabel("Ingen");

		if (warning == null || warning.isIgnored()) {
            cell.setBackground((isSelected) ? selectedBackground : normalBackground);
			return cell;
		}

		if (!warning.containsWarnings()) {
            container.setLayout(new GridBagLayout());
			container.add(noWarnings, new GridBagConstraints());

			cell.setBackground(OK_COLOR);
		} else {
			if (warning.isProductionOutsideRode()) {
				addIcon(container, OUTSIDE_RODE_URL);
			}
			if (warning.isProductionOutsideContract()) {
				addIcon(container, OUTSIDE_CONTRACT_URL);
			}
			if (warning.hasNoPayment()) {
				addIcon(container, NO_PAY_URL);
			}
			if (warning.isArticleChanged()) {
				addIcon(container, CHANGED_ARTICLE_URL);
			}
			if (warning.isManuallyCreated()) {
				addIcon(container, MANUALLY_CREATED_URL);
			}

			cell.setBackground(WARNING_COLOR);
		}

		if (warning instanceof Trip) {
		    Trip trip = (Trip) warning;

		    if (trip.getProcessStatus().getLevel() >= TripProcessStatus.PROCESSED_BY_CONTRACTOR.getLevel()) {
                noWarnings.setForeground((isSelected) ? Color.WHITE : Color.BLACK);
                cell.setBackground((isSelected) ? selectedBackground : normalBackground);
            }
        }

		cell.add(container, BorderLayout.CENTER);
		cell.setToolTipText(warning.getWarningString());

		return cell;
	}

	private void addIcon(JPanel panel, String imageUrl) {
		panel.add(new JLabel(getIcon(imageUrl)));
	}

	private ImageIcon getIcon(String name) {
		URL imageUrl = this.getClass().getClassLoader().getResource(name);
		return new ImageIcon(imageUrl);
	}

}