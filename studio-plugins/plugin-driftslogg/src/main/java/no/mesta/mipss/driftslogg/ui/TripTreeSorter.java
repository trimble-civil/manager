package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import javax.swing.table.TableColumn;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

public class TripTreeSorter {

	public enum SORTBY {
		PERIOD_STATUS,
		ID,
		PLATFORM,
		PROCESS_STATUS,
		WARNINGS,
		START_TIME,
		END_TIME,
		DURATION,
		VEHICLE,
		VENDOR,
		AMOUNT,
		HOURS_TOTAL,
		PLOWING_TOTAL,
		PLANING_TOTAL,
		HEAVY_PLANING_TOTAL,
		SIDE_PLOUGH_TOTAL,
		GRITTING_DRY_TOTAL,
		GRITTING_WET_TOTAL
	}

	private final TripTableModel model;
	private final JMipssTreeTable<TripTreeView> treeTable;
	private final MipssRenderableEntityTableColumnModel columnModel;

	private Map<Integer, TripComparator> columnSorters = new HashMap<>();

	public TripTreeSorter(TripTableModel model, JMipssTreeTable<TripTreeView> treeTable, MipssRenderableEntityTableColumnModel columnModel){
		this.model = model;
		this.treeTable = treeTable;
		this.columnModel = columnModel;
		initSorter();
	}

	private void initSorter() {
		treeTable.getTableHeader().addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				int index = treeTable.columnAtPoint(e.getPoint());
				index = treeTable.convertColumnIndexToModel(index);
				//int index = tblAktivitet.getColumnModel().getColumnIndexAtX(e.getX());
				List<Trip> trips = model.getTrips();
				TripComparator comparatorForSelectedColumn = columnSorters.get(index);
				if (comparatorForSelectedColumn==null||trips==null)
					return;
				toggle(comparatorForSelectedColumn, trips);
				model.fireTreeChanged();
			}
		});
		
		addComparators();
	}
	private void addComparators() {
		columnSorters.put(TripTableModel.COL_PERIOD_STATUS, new TripComparator(SORTBY.PERIOD_STATUS, TripTableModel.COL_PERIOD_STATUS));
		columnSorters.put(TripTableModel.COL_ID, new TripComparator(SORTBY.ID, TripTableModel.COL_ID));
		columnSorters.put(TripTableModel.COL_PLATFORM, new TripComparator(SORTBY.PLATFORM, TripTableModel.COL_PLATFORM));
		columnSorters.put(TripTableModel.COL_PROCESS_STATUS, new TripComparator(SORTBY.PROCESS_STATUS, TripTableModel.COL_PROCESS_STATUS));
		columnSorters.put(TripTableModel.COL_WARNINGS, new TripComparator(SORTBY.WARNINGS, TripTableModel.COL_WARNINGS));
		columnSorters.put(TripTableModel.COL_START_TIME, new TripComparator(SORTBY.START_TIME, TripTableModel.COL_START_TIME));
		columnSorters.put(TripTableModel.COL_END_TIME, new TripComparator(SORTBY.END_TIME, TripTableModel.COL_END_TIME));
		columnSorters.put(TripTableModel.COL_DURATION, new TripComparator(SORTBY.DURATION, TripTableModel.COL_DURATION));
		columnSorters.put(TripTableModel.COL_VENDOR, new TripComparator(SORTBY.VENDOR, TripTableModel.COL_VENDOR));
		columnSorters.put(TripTableModel.COL_VEHICLE, new TripComparator(SORTBY.VEHICLE, TripTableModel.COL_VEHICLE));
		columnSorters.put(TripTableModel.COL_AMOUNT, new TripComparator(SORTBY.AMOUNT, TripTableModel.COL_AMOUNT));
		columnSorters.put(TripTableModel.COL_HOURS_TOTAL, new TripComparator(SORTBY.HOURS_TOTAL, TripTableModel.COL_HOURS_TOTAL));
		columnSorters.put(TripTableModel.COL_PLOWING_TOTAL, new TripComparator(SORTBY.PLOWING_TOTAL, TripTableModel.COL_PLOWING_TOTAL));
		columnSorters.put(TripTableModel.COL_PLANING_TOTAL, new TripComparator(SORTBY.PLANING_TOTAL, TripTableModel.COL_PLANING_TOTAL));
		columnSorters.put(TripTableModel.COL_HEAVY_PLANING_TOTAL, new TripComparator(SORTBY.HEAVY_PLANING_TOTAL, TripTableModel.COL_HEAVY_PLANING_TOTAL));
		columnSorters.put(TripTableModel.COL_SIDE_PLOUGH_TOTAL, new TripComparator(SORTBY.SIDE_PLOUGH_TOTAL, TripTableModel.COL_SIDE_PLOUGH_TOTAL));
		columnSorters.put(TripTableModel.COL_DRY_GRITTING_TOTAL, new TripComparator(SORTBY.GRITTING_DRY_TOTAL, TripTableModel.COL_DRY_GRITTING_TOTAL));
		columnSorters.put(TripTableModel.COL_WET_GRITTING_TOTAL, new TripComparator(SORTBY.GRITTING_WET_TOTAL, TripTableModel.COL_WET_GRITTING_TOTAL));
	}
	
	private void toggle(TripComparator comp, List<Trip> trips){
		comp.toggle();
		Collections.sort(trips, comp);
		for (Integer i:columnSorters.keySet()){
			TripComparator tripComparator = columnSorters.get(i);
			if (tripComparator!=comp){
				tripComparator.reset();
			}
		}	
	}
	
	class TripComparator implements Comparator<Trip> {
		private SORTBY sortingBy;
		private Integer currentSortOrdering;//0,1,2 ASC DESC NONE
		private final int columnIndex;
		public TripComparator(SORTBY sortingBy, int columnIndex){
			this.sortingBy = sortingBy;
			this.columnIndex = columnIndex;
			currentSortOrdering=2;
		}

		
		public void toggle(){
			if (currentSortOrdering==2){
				currentSortOrdering=0;
			}else{
				currentSortOrdering++;
			}
			int index = treeTable.convertColumnIndexToView(columnIndex);
			
			TableColumn column = columnModel.getColumn(index);
			if (column.getHeaderRenderer()==null){
				TreeTableHeaderCellRenderer headerRenderer = new TreeTableHeaderCellRenderer(String.valueOf(column.getHeaderValue()));
				column.setHeaderRenderer(headerRenderer);
			}
			TreeTableHeaderCellRenderer renderer = (TreeTableHeaderCellRenderer)column.getHeaderRenderer();
			if (currentSortOrdering==0){
				renderer.setIcon(IconResources.SORT_ASC);
			}
			if (currentSortOrdering==1){
				renderer.setIcon(IconResources.SORT_DESC);				
			}
			if (currentSortOrdering==2){
				column.setHeaderRenderer(null);
			}
		}
		
		public void reset(){
			currentSortOrdering=2;
			int index = treeTable.convertColumnIndexToView(columnIndex);
			TableColumn column = columnModel.getColumn(index);
			column.setHeaderRenderer(null);
		}
		@Override
		public int compare(Trip o1, Trip o2) {
			int res = 0;

			switch (sortingBy) {
				case PERIOD_STATUS:
					if (o1.getId() != null && o2.getId() != null) {
						res = o1.getPeriod().getStatus().compareTo(o2.getPeriod().getStatus());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case ID:
					if (o1.getId() != null && o2.getId() != null) {
						res = o1.getId().compareTo(o2.getId());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case PLATFORM:
					if (o1.getId() != null && o2.getId() != null) {
						res = o1.getPlatform().compareTo(o2.getPlatform());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case PROCESS_STATUS:
					if (o1.getId() != null && o2.getId() != null) {
						res = o1.getProcessStatus().getId().compareTo(o2.getProcessStatus().getId());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case WARNINGS:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareString(o1.getWarningString(), o2.getWarningString());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case START_TIME:
					if (o1.getId() != null && o2.getId() != null) {
						res = o1.getStartTime().compareTo(o2.getStartTime());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case END_TIME:
					if (o1.getId() != null && o2.getId() != null) {
						res = o1.getEndTime().compareTo(o2.getEndTime());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case DURATION:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getDuration(), o2.getDuration());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case VENDOR:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareString(o1.getContractorContract().getLeverandor().getNavn(), o2.getContractorContract().getLeverandor().getNavn());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case VEHICLE:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareString(o1.getVehicle().getEksterntNavn(), o2.getVehicle().getEksterntNavn());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case AMOUNT:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getAmount(), o2.getAmount());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case HOURS_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getHoursTotal(), o2.getHoursTotal());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case PLOWING_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getPlowingTotalKm(), o2.getPlowingTotalKm());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case PLANING_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getPlaningTotalKm(), o2.getPlaningTotalKm());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case HEAVY_PLANING_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getHeavyPlaningTotalKm(), o2.getHeavyPlaningTotalKm());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case SIDE_PLOUGH_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getSidePloughTotalKm(), o2.getSidePloughTotalKm());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case GRITTING_DRY_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getDryGrittingTotalKg(), o2.getDryGrittingTotalKg());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
				case GRITTING_WET_TOTAL:
					if (o1.getId() != null && o2.getId() != null) {
						res = compareDouble(o1.getWetGrittingTotalLiter(), o2.getWetGrittingTotalLiter());
					} else {
						return compareAgainstSumTrip(o1, o2);
					}
					break;
			}

			if (currentSortOrdering==0){
				return res;
			}

			if (currentSortOrdering==1){
				return res*-1;
			}

			return 0;
		}

		private int compareAgainstSumTrip(Trip o1, Trip o2) {
			if (o1.getId() == null) {
				return -1;
			} else if (o2.getId() == null) {
				return 1;
			} else {
				return 0;
			}
		}
		
		private int compareDouble(Double d1, Double d2){
			if (d1==null && d2!=null)
				return -1;
			if (d2==null && d1!=null)
				return 1;
			if (d2==null && d1==null)
				return 0;
			return d1.compareTo(d2);
		}
		private int compareString(String s1, String s2){
			if (s1==null && s2!=null)
				return -1;
			if (s2==null && s1!=null)
				return 1;
			if (s2==null && s1==null)
				return 0;
			return s1.compareTo(s2);
		}
	}

}