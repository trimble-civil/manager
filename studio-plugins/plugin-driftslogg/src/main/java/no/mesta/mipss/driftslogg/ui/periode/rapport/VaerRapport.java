package no.mesta.mipss.driftslogg.ui.periode.rapport;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.rapport.VaerRapportObject;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Excel rapport som viser en liste over aktiviteter med tilknyttede værdata
 * (Rapportert av UE fra DL-web)
 * 
 * @author larnym
 */
public class VaerRapport {

	private static final String RAPPORT_TEMPLATE = "vaer_rapport";
	private File file;
	private XSSFWorkbook workbook;
	private ExcelPOIHelper excel;
	
	public VaerRapport(DriftsloggQueryParams params, List<VaerRapportObject> data, Date fraDato, Date tilDato) {

		String periodeText = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.DATE_FORMAT)+" - "+MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.DATE_FORMAT);
		String rappDatoText = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

		HashMap<String, Object> map = new HashMap<>();
		map.put("fraDato", fraDato);
		map.put("tilDato", tilDato);
		map.put("vaerData", data);
		map.put("params", params);
		map.put("dato", rappDatoText);
		map.put("periode", periodeText);
		
		file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
		workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map);
		excel = new ExcelPOIHelper(workbook);
	}
	
	/**
	 * Lukker arbeidsboken og åpner den i Excel
	 */
	public void finalizeAndOpen(){
		RapportHelper.finalizeAndOpen(workbook, file);
	}

}
