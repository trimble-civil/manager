package no.mesta.mipss.driftslogg.ui;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class TreeTableHeaderCellRenderer implements TableCellRenderer{

	private final String text;
	private JLabel renderer;
	
	public TreeTableHeaderCellRenderer(String text){
		this.text = text;
		renderer = new JLabel(text);
	}
	
	public void setIcon(Icon icon){
		renderer.setIcon(icon);
	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		return renderer;
	}

}
