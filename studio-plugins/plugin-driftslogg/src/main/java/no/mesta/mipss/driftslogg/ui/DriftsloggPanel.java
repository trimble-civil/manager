package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.ui.mipssrapport.MipssrapportPanel;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class DriftsloggPanel extends JPanel{

	private final DriftsloggController controller;
	
	private UtvalgskriterierPanel pnlParams;
	private TripPanel tripPanel;
	private MipssrapportPanel pnlMipssrapport;

	public DriftsloggPanel(DriftsloggController controller){
		this.controller = controller;
		initComponents();
		initGui();
		setPreferredSize(new Dimension(800, 600));
	}

	private void initComponents() {
		pnlParams = new UtvalgskriterierPanel(controller);
		tripPanel = new TripPanel(controller);
		pnlMipssrapport = new MipssrapportPanel(controller);
	}

	private void initGui() {
		setLayout(new GridBagLayout());

		JPanel panel = new JPanel(new GridBagLayout());
		panel.add(pnlParams, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,5,2,2),5,5));
		panel.add(pnlMipssrapport, 	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, new Insets(0,2,2,5),5,5));

		add(panel,			new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST,	 	GridBagConstraints.BOTH, new Insets(10,0,0,0),0,0));
		add(tripPanel,		new GridBagConstraints(0,1, 2,1, 1.0,1.0, GridBagConstraints.NORTH, 		GridBagConstraints.BOTH, new Insets(2,5,10,5),5,5));
	}

	public UtvalgskriterierPanel getUtvalgsPanel() {
		return pnlParams;		
	}

	public TripPanel getTripPanel() {
		return tripPanel;
	}
}