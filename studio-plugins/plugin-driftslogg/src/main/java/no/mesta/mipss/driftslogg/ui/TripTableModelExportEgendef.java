package no.mesta.mipss.driftslogg.ui;

import java.util.Date;
import java.util.List;

import javax.swing.SwingWorker;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.persistence.driftslogg.Trip;

/**
 * Klasse som arver fra AktivitetTableModel.
 * Kun overskrevet metoden load(), da det er nødvendig å hente ut et annet datasett enn det som benyttes i mor-klassen.
 * 
 * @author larnym
 */
public class TripTableModelExportEgendef extends TripTableModel {
	
	private Date fraDato;
	private Date tilDato;

	public TripTableModelExportEgendef(DriftsloggController driftsloggController, Date fraDato, Date tilDato) {
		super(driftsloggController);
		this.fraDato = fraDato;
		this.tilDato = tilDato;
	}
	
	@Override
	public void load(Object beanSourceController, SwingWorker currentWorker) {
		DriftsloggController controller = (DriftsloggController)beanSourceController;

		List<Trip> a = controller.getTrips(controller.getQueryParams(), fraDato, tilDato);
		if (!currentWorker.isCancelled()){
			load(a);
		}
	}

}
