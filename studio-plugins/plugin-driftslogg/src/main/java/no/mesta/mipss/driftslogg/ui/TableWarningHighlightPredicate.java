package no.mesta.mipss.driftslogg.ui;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

import static no.mesta.mipss.driftslogg.ui.AktivitetTableModel.*;

public class TableWarningHighlightPredicate implements HighlightPredicate {

    private final AktivitetTableModel model;

    public TableWarningHighlightPredicate(AktivitetTableModel mdlAktivitet) {
        this.model = mdlAktivitet;
    }

    @Override
    public boolean isHighlighted(Component component, ComponentAdapter componentAdapter) {
        return isHighlighted(componentAdapter.row, componentAdapter.column);
    }

    boolean isHighlighted(int row, int column) {
        return (model.getAktiviteter().get(row).containsWarnings() && isWarningColumn(column))
                || (model.getAktiviteter().get(row).isManuallyCreated() && isProductionTypeColumn(column));
    }

    boolean isWarningColumn(int colNumber) {
        switch(colNumber) {
            case COL_WARNINGS:
            case COL_APPROVED_BUILD:
            case COL_APPROVED_UE:
            case COL_APPROVED_INTENTION:
                return true;
            default:
                return false;
        }
    }

    boolean isProductionTypeColumn(int colNumber) {
        return colNumber == COL_PRODUCTION_TYPE;
    }
}
