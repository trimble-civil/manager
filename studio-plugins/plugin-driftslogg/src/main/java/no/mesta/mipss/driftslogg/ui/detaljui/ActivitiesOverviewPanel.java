package no.mesta.mipss.driftslogg.ui.detaljui;

import no.mesta.mipss.common.ManagerConstants;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.MipssNumberFormatter;
import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftslogg.AktivitetTable;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.driftslogg.ui.*;
import no.mesta.mipss.driftslogg.ui.listeners.ChangeMonitor;
import no.mesta.mipss.driftslogg.ui.listeners.ChangeRegistry;
import no.mesta.mipss.driftslogg.ui.mipssrapport.MipssRapportTableModel;
import no.mesta.mipss.driftslogg.ui.mipssrapport.MipssrapportPanel;
import no.mesta.mipss.driftslogg.ui.mipssrapport.MipssrapportTreeView;
import no.mesta.mipss.driftslogg.util.ActivityUtil;
import no.mesta.mipss.persistence.driftslogg.*;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.Colors;
import no.mesta.mipss.ui.JMipssTreeTableScrollPane;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssTableComboBoxEditor;
import no.mesta.mipss.ui.table.ToolTipHeader;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.decorator.ColorHighlighter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.Duration;
import java.util.*;
import java.util.List;

import static no.mesta.mipss.driftslogg.ui.AktivitetTableModel.*;
import static no.mesta.mipss.ui.Colors.VALIDATION_HIGHLIGHT_COLOR;

@SuppressWarnings("serial")
public class ActivitiesOverviewPanel extends JPanel{

	private final DriftsloggController controller;

    private JTextField txtCurrentTripId;
    private JButton btnPreviousTrip;
    private JButton btnNextTrip;
    private JLabel lblStartEnd;
    private JLabel lblActualStartEnd;
    private JLabel lblVendor;
    private JLabel lblActualVendor;
    private JLabel lblVehicle;
    private JLabel lblActualVehicle;
    private JLabel lblPurpose;
    private JLabel lblActualPurpose;
    private JLabel lblDistance;
    private JLabel lblActualDistance;
    private JLabel lblGritting;
    private JLabel lblActualGritting;
    private JLabel lblWeather;
    private JLabel lblActualWeather;
    private JLabel lblComment;
    private JTextArea txtActualComment;
    private JScrollPane scroll;
    private JLabel lblSum;
    private JLabel lblActualSum;
    private JLabel lblDuration;
    private JLabel lblActualDuration;
    private JTextField txtTripProcessStatus;
    private MipssrapportPanel pnlMipssrapport;
    private AktivitetTable tblAktivitet;
    private JMipssTreeTableScrollPane treeTableScroll;
    private JPopupListMenu btnProductionReports;
    private JButton btnShowMap;
    private JButton btnSaveChanges;
    private JButton btnAddNewActivity;
    private JButton btnReturnTrip;
    private JButton btnSetTripProcessed;
    private BindingGroup bindings = new BindingGroup();
    private JButton btnReturnHistory;

    private List<Aktivitet> originalActivities;
    private AktivitetTableModel mdlAktivitet;
    private List<Aktivitet> activities;
    private List<AgrArtikkelV> articleList;
    private Trip selectedTrip;
    private List<Trip> relatedTrips;
    private int currentIndex;
    private ChangeRegistry saveStatusRegistry = new ChangeRegistry();
    private ChangeMonitor<Aktivitet> aktiviteterChangedMonitor;
    private ActivityUtil activityUtil;
    private JDialog parent;
    private Double sumApproved;
    private List<Stroprodukt> gritProductList;
    private TableDirtyActivityHighlightPredicate dirtyActivityHighlighter;
    private TableMissingInternalCommentHighlightPredicate missingInternalCommentHighlighter;

	public ActivitiesOverviewPanel(DriftsloggController controller, JDialog parent, Trip selectedTrip, List<Trip> relatedTrips){
		this.controller = controller;
		this.selectedTrip = selectedTrip;
		this.relatedTrips = relatedTrips;
		this.parent = parent;

		originalActivities = new ArrayList<>();
        activities = new ArrayList<>();
        articleList = new ArrayList<>();

		initComponents();
		initGui();
		loadTrip();
		bind();
	}

    public ActivitiesOverviewPanel() {
		initComponents();
		initGui();
        controller = null;
        loadTrip();
        bind();
	}

	public void dispose() {
	    if (canDiscardChanges()) {
            selectedTrip.setActivities(originalActivities);

            if (selectedTrip.getProcessStatus() == TripProcessStatus.UNDER_PROCESS_BY_CONTRACTOR) {
                setTripProcessStatus(TripProcessStatus.SUBMITTED_BY_SUBCONTRACTOR);
            }

            controller.setReloadTable(true);
            parent.dispose();
        }
	}

	private void bind() {
        Binding<ChangeRegistry, Object, JButton, Object> changedBind = BindingHelper.createbinding(saveStatusRegistry, "changed", btnSaveChanges, "enabled");
        bindings.addBinding(changedBind);
        bindings.bind();
    }

	private void initComponents() {
        activityUtil = new ActivityUtil(controller.getProdtypes());
        gritProductList = controller.getStroprodukter();

        initTripInfoPanel();

        MipssRapportTableModel model = controller.getMipsstallTableModel();
        JMipssTreeTable<MipssrapportTreeView> table = initLoggedAmountsTable(model);
		pnlMipssrapport = new MipssrapportPanel(controller, table, model);

		initActivitiesPanel();
	}

    private JMipssTreeTable<MipssrapportTreeView> initLoggedAmountsTable(MipssRapportTableModel model) {
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
                MipssrapportTreeView.class,
                "tiltak", "km", "timer", "stroprodukt", "tonnTorr", "literLosningTotalt", "kgLosning");
        columnModel.getColumn(MipssRapportTableModel.COL_TILTAK).setPreferredWidth(200);
        columnModel.getColumn(MipssRapportTableModel.COL_KM).setPreferredWidth(60);
        columnModel.getColumn(MipssRapportTableModel.COL_TIMER).setPreferredWidth(40);
        columnModel.getColumn(MipssRapportTableModel.COL_STROPRODUKT).setPreferredWidth(60);
        columnModel.getColumn(MipssRapportTableModel.COL_TONNTORR).setPreferredWidth(60);
        columnModel.getColumn(MipssRapportTableModel.COL_LITER_LOSNING_TOTALT).setPreferredWidth(60);
        columnModel.getColumn(MipssRapportTableModel.COL_KG_LOSNING).setPreferredWidth(60);

        JMipssTreeTable<MipssrapportTreeView> table = new JMipssTreeTable<>(model);
        model.setTreeView(table);

        table.setColumnModel(columnModel);
        table.setFillsViewportHeight(true);
        table.setLeafIcon(null);
        table.setShowGrid(true, true);
        table.setGridColor(Colors.TABLE_GRID_COLOR);

        // Oppretter nye headere med tooltip
        ToolTipHeader header = new ToolTipHeader(table.getColumnModel());
        header.setToolTip(MipssRapportTableModel.COL_TONNTORR, Resources.getResource("mipsstall.header.tonnTorrstoff"));
        header.setToolTip(MipssRapportTableModel.COL_LITER_LOSNING_TOTALT, Resources.getResource("mipsstall.header.literLosning"));
        header.setToolTip(MipssRapportTableModel.COL_KG_LOSNING, Resources.getResource("mipsstall.header.kgLosning"));
        table.setTableHeader(header);

        return table;
    }

	private void initTripInfoPanel() {
        txtCurrentTripId = new JTextField();
        txtCurrentTripId.setEditable(false);
        txtCurrentTripId.setBackground(Color.WHITE);

        initTripInfoButtons();

        lblStartEnd = new JLabel(Resources.getResource("label.startEnd"));
        lblActualStartEnd = new JLabel();
        lblVendor = new JLabel(Resources.getResource("label.vendor"));
        lblActualVendor = new JLabel();
        lblVehicle = new JLabel(Resources.getResource("label.vehicle"));
        lblActualVehicle = new JLabel();
        lblPurpose = new JLabel(Resources.getResource("label.purpose"));
        lblActualPurpose = new JLabel();
        lblDistance = new JLabel(Resources.getResource("label.distance"));
        lblActualDistance = new JLabel();
        lblGritting = new JLabel(Resources.getResource("label.gritting"));
        lblActualGritting = new JLabel();
        lblWeather = new JLabel(Resources.getResource("label.weather"));
        lblActualWeather = new JLabel();

        lblComment = new JLabel(Resources.getResource("label.commentDriver"));
        txtActualComment = new JTextArea();
        txtActualComment.setEditable(false);
        txtActualComment.setLineWrap(true);
        txtActualComment.setWrapStyleWord(true);
        txtActualComment.enableInputMethods(false);
        txtActualComment.setBackground(getBackground());
        txtActualComment.setFont(lblComment.getFont());

        scroll = new JScrollPane(txtActualComment);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(null);
        scroll.setMinimumSize(new Dimension(100, 45));
        scroll.setPreferredSize(new Dimension(100, 45));
        scroll.setMaximumSize(new Dimension(100, 45));

        lblSum = new JLabel(Resources.getResource("label.approveSum"));
        lblActualSum = new JLabel();
        lblDuration = new JLabel(Resources.getResource("label.duration"));
        lblActualDuration = new JLabel();

        txtTripProcessStatus = new JTextField();
        txtTripProcessStatus.setEditable(false);
        txtTripProcessStatus.setHorizontalAlignment(JLabel.CENTER);
        Font defaultFont = txtTripProcessStatus.getFont();
        txtTripProcessStatus.setFont(new Font(defaultFont.getName(), Font.BOLD, defaultFont.getSize()));
    }

    private void initTripInfoButtons() {
        btnPreviousTrip = new JButton(Resources.getResource("label.previousTrip"), IconResources.PREVIOUS);
        btnPreviousTrip.addActionListener(e -> {
            if (canDiscardChanges()) {
                selectedTrip.setActivities(originalActivities);

                if (selectedTrip.getProcessStatus() == TripProcessStatus.UNDER_PROCESS_BY_CONTRACTOR) {
                    setTripProcessStatus(TripProcessStatus.SUBMITTED_BY_SUBCONTRACTOR);
                }

                currentIndex--;
                if (currentIndex == 0) {
                    btnPreviousTrip.setEnabled(false);
                }
                btnNextTrip.setEnabled(true);

                selectedTrip = relatedTrips.get(currentIndex);
                loadTrip();
            }
        });

        btnNextTrip = new JButton(Resources.getResource("label.nextTrip"), IconResources.NEXT);
        btnNextTrip.setHorizontalTextPosition(SwingConstants.LEFT); // moves the icon to the right side of the button
        btnNextTrip.addActionListener(e -> {
            if (canDiscardChanges()) {
                selectedTrip.setActivities(originalActivities);

                if (selectedTrip.getProcessStatus() == TripProcessStatus.UNDER_PROCESS_BY_CONTRACTOR) {
                    setTripProcessStatus(TripProcessStatus.SUBMITTED_BY_SUBCONTRACTOR);
                }

                currentIndex++;
                if (currentIndex == relatedTrips.size() - 1) {
                    btnNextTrip.setEnabled(false);
                }
                btnPreviousTrip.setEnabled(true);

                selectedTrip = relatedTrips.get(currentIndex);
                loadTrip();
            }
        });

        for (int i = 0; i < relatedTrips.size(); i++) {
            Trip t = relatedTrips.get(i);

            if (t.getId().equals(selectedTrip.getId())) {
                currentIndex = i;
                break;
            }
        }

        if (relatedTrips.size() == 1) {
            btnPreviousTrip.setEnabled(false);
            btnNextTrip.setEnabled(false);
        } else if (currentIndex == 0) {
            btnPreviousTrip.setEnabled(false);
        } else if (currentIndex == relatedTrips.size() - 1) {
            btnNextTrip.setEnabled(false);
        }
    }

    private boolean canDiscardChanges() {
        if (saveStatusRegistry.isChanged()) {
            int confirm = JOptionPane.showConfirmDialog(
                    controller.getPlugin().getModuleGUI(),
                    Resources.getResource("message.ikkeLagret"),
                    Resources.getResource("message.ikkeLagret.title"),
                    JOptionPane.YES_NO_CANCEL_OPTION);
            return confirm == JOptionPane.YES_OPTION;
        }
        return true;
    }

    private void initActivitiesPanel() {
        List<JMenuItem> popupProdList = new ArrayList<>();
        JMenuItem grafrapp = new JMenuItem(controller.getOpenGrafAction(selectedTrip));
        grafrapp.setToolTipText(Resources.getResource("button.grafrapp.tooltip"));
        JMenuItem prodrapp = new JMenuItem(controller.getOpenProdrappAction(this));
        prodrapp.setToolTipText(Resources.getResource("button.prodrapp.tooltip"));
        popupProdList.add(grafrapp);
        popupProdList.add(prodrapp);
	    btnProductionReports = new JPopupListMenu(Resources.getResource("button.prodrapport"), IconResources.REPORTS, popupProdList);
        btnProductionReports.setToolTipText(Resources.getResource("button.prodrapport.tooltip"));

        btnShowMap = new JButton(Resources.getResource("button.showMap"), IconResources.MAP_MARKER);
        btnShowMap.addActionListener(showMap());
        btnReturnHistory = new JButton(Resources.getResource("button.returnHistory"), IconResources.HISTORY2);
        btnReturnHistory.addActionListener(showReturnHistory());
        btnSaveChanges = new JButton(Resources.getResource("button.save"), IconResources.SAVE2_ICON);
        btnSaveChanges.addActionListener(saveTrip());
        btnSaveChanges.setEnabled(false);
        btnAddNewActivity = new JButton(Resources.getResource("button.addActivityLine"), IconResources.ADD_ROW);
        btnAddNewActivity.addActionListener(addNewActivityAction());
        btnReturnTrip = new JButton(Resources.getResource("button.returnTrip"), IconResources.RETURN);
        btnReturnTrip.addActionListener(returnTrip());
        btnSetTripProcessed = new JButton(Resources.getResource("button.setTripProcessed"), IconResources.OK3_ICON);
        btnSetTripProcessed.addActionListener(processTrip());

        initActivitiesTable();
    }

    private ActionListener showMap() {
	    return e -> controller.openTripInMap(selectedTrip);
    }

    private ActionListener showReturnHistory() {
	    return e -> {
            JFrame parent = controller.getPlugin().getLoader().getApplicationFrame();

            JDialog d = new JDialog(parent);
            d.setTitle(Resources.getResource("button.returnHistory"));
            d.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            d.setSize(600, 200);
            d.setLocationRelativeTo(parent);
            final ActivityReturnCausesPanel pnl = new ActivityReturnCausesPanel(selectedTrip.getReturnCauses());
            d.add(pnl);
            d.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            d.setVisible(true);
        };
    }

    private ActionListener returnTrip() {
	    return e -> {
            String cause = JOptionPane.showInputDialog(
                    controller.getPlugin().getModuleGUI(),
                    Resources.getResource("message.activitiesOverview.return"),
                    Resources.getResource("button.returnTrip"),
                    JOptionPane.INFORMATION_MESSAGE);

            if (cause != null && !cause.trim().isEmpty() && cause.length() <= 1000) {
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                boolean successfullySentEmail = controller.sendEmail(selectedTrip, cause);

                selectedTrip.getReturnCauses().add(controller.addTripReturnCause(selectedTrip.getId(), cause));
                setTripProcessStatus(TripProcessStatus.RETURNED_TO_SUBCONTRACTOR);
                setTripProcessStatusIndicator(selectedTrip);

                tblAktivitet.setEnabled(false);
                btnReturnHistory.setEnabled(true);
                btnReturnTrip.setEnabled(false);
                btnSetTripProcessed.setEnabled(false);
                btnSaveChanges.setEnabled(false);

                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

                if (!successfullySentEmail) {
                    JOptionPane.showMessageDialog(
                            controller.getPlugin().getModuleGUI(),
                            Resources.getResource("message.activitiesOverview.emailNotSent"),
                            Resources.getResource("button.returnTrip"),
                            JOptionPane.INFORMATION_MESSAGE);
                }
            } else if (cause != null && cause.length() > 1000) {
                JOptionPane.showMessageDialog(
                        controller.getPlugin().getModuleGUI(),
                        Resources.getResource("message.activitiesOverview.returnCauseTooLong"),
                        Resources.getResource("button.returnTrip"),
                        JOptionPane.WARNING_MESSAGE);
            }
        };
    }

    private ActionListener processTrip() {
        return e -> {
            if (activityUtil.getActivitiesInNeedOfComments().isEmpty()) {
                btnSaveChanges.doClick();
                btnSetTripProcessed.setEnabled(false);

                setTripProcessStatus(TripProcessStatus.PROCESSED_BY_CONTRACTOR);
                setTripProcessStatusIndicator(selectedTrip);
            } else {
                displayMissingInternalMessageDialog();
            }
        };
    }

    private void displayMissingInternalMessageDialog() {
        String concat = "";
        StringBuilder sb = new StringBuilder();
        for(Aktivitet aktivitet : activityUtil.getActivitiesInNeedOfComments()) {
            if (aktivitet.getId() != null) {
                sb.append(concat).append(aktivitet.getId());
                concat = ", ";
            }
        }

        JOptionPane.showMessageDialog(
                controller.getPlugin().getModuleGUI(),
                Resources.getResource("message.activitiesOverview.missingInternalComment", sb.toString()),
                Resources.getResource("message.activitiesOverview.missingInternalComment.title"),
                JOptionPane.INFORMATION_MESSAGE);
    }

    private ActionListener saveTrip() {
        return e -> {
            if (saveStatusRegistry.isChanged() && activityUtil.getActivitiesInNeedOfComments().isEmpty()) {
                updateChangedActivities();

                selectedTrip = controller.updateTrip(selectedTrip);
                relatedTrips.set(currentIndex, selectedTrip);

                originalActivities = copyActivities(selectedTrip.getActivities());

                controller.setMipssTiltakForSingleTrip(selectedTrip);
                populateActivitiesTable();
                setupActivitiesMonitoring();
            } else {
                displayMissingInternalMessageDialog();
            }
        };
    }

    private void updateChangedActivities() {
        String signature = controller.getPlugin().getLoader().getLoggedOnUser(true).getSignatur();
        Date now = new Date();

        for (Aktivitet a : aktiviteterChangedMonitor.getChangedEntities()) {
            if (a.getId() != null) {
                a.setEndretAv(signature);
                a.setEndretDato(now);
            }

            updateGritProductAmounts(a, signature, now);
        }
    }

    private void updateGritProductAmounts(Aktivitet activity, String signature, Date timestamp) {
        // We need to add/update Stroproduktmengde entities for manager created activities for reports to
        // correctly pick up these gritting amounts.
        if (activity.isCreatedManager() && (activity.getDryGritAmount() != null || activity.getWetGritAmount() != null)) {
            Stroproduktmengde newGritProductAmount = null;

            if (activity.getStroprodukter().isEmpty()) {
                newGritProductAmount = createGritProductAmountForProdType(activity);
            } else {
                // Manager created activities will never have more than one Stroproduktmengde entity associated with them.
                Stroproduktmengde existingGritProductAmount = activity.getStroprodukter().get(0);
                ManagerConstants.Stroprod gritTypeEnum = ManagerConstants.Stroprod.navnTilEnum(activity.getGritProductName());
                setAppropriateGritProductFields(gritTypeEnum, existingGritProductAmount, activity);

                existingGritProductAmount.setEndretAv(signature);
                existingGritProductAmount.setEndretDato(timestamp);
            }

            if (newGritProductAmount != null) {
                // The temp id represents activities not yet added to the database.
                Long id = activity.getTempId();
                if (activity.getId() != null) {
                    id = activity.getId();
                }

                // To be able to add an Stroproduktmengde entity to the database we're required to have a valid activity id.
                Aktivitet newActivityEntity = controller.addOrUpdateActivity(activity);

                ListIterator<Aktivitet> it = selectedTrip.getActivities().listIterator();
                while (it.hasNext()) {
                    Aktivitet existingActivity = it.next();

                    if ((existingActivity.getId() != null && existingActivity.getId().equals(id)) ||
                            (existingActivity.getTempId() != null && existingActivity.getTempId().equals(id))) {
                        // We need to replace the selected trip's previous activity with the newly made one so the selected
                        // trip is up-to-date when we persist activities without Stroproduktmengde entities.
                        it.set(newActivityEntity);
                        break;
                    }
                }

                newGritProductAmount.setAktivitetId(newActivityEntity.getId());
                newGritProductAmount.setAktivitet(newActivityEntity);
                newGritProductAmount.setOpprettetAv(signature);
                newGritProductAmount.setOpprettetDato(timestamp);
                newGritProductAmount = controller.addOrUpdateGritProductAmount(newGritProductAmount);

                newActivityEntity.getStroprodukter().add(newGritProductAmount);
            }
        }
    }

    private Stroproduktmengde createGritProductAmountForProdType(Aktivitet activity) {
	    String prodType = activity.getProductionType();
        Stroproduktmengde newGritProductAmount = null;

        if (prodType != null && prodType.contains(":")) {
            String gritTypeStr = prodType.substring(prodType.indexOf(":") + 1, prodType.length()).trim();
            ManagerConstants.Stroprod gritTypeEnum = ManagerConstants.Stroprod.navnTilEnum(gritTypeStr);

            newGritProductAmount = new Stroproduktmengde();
            setAppropriateGritProductFields(gritTypeEnum, newGritProductAmount, activity);
        }

        return newGritProductAmount;
    }

    private void setAppropriateGritProductFields(ManagerConstants.Stroprod gritTypeEnum, Stroproduktmengde gritProductAmount, Aktivitet activity) {
        for (Stroprodukt gritProd : gritProductList) {
            if (gritTypeEnum.id() == gritProd.getId()) {
                gritProductAmount.setStroproduktId(gritProd.getId());
                gritProductAmount.setStroprodukt(gritProd);
                break;
            }
        }

        switch (gritTypeEnum) {
            case TORRSALT:
            case SAND_MED_SALT:
            case REN_SAND:
            case SLURRY:
            case BEFUKTET_SALT:
            case FAST_SAND:
                gritProductAmount.setOmforentMengdeTort(activity.getDryGritAmount());
                gritProductAmount.setOmforentMengdeVaatt(null);
                break;
            case SALTLOSNING:
                gritProductAmount.setOmforentMengdeTort(null);
                gritProductAmount.setOmforentMengdeVaatt(activity.getWetGritAmount());
                break;
            case UKJENT:
                gritProductAmount.setOmforentMengdeTort(null);
                gritProductAmount.setOmforentMengdeVaatt(null);
                break;
            case MGCL_LOSNING:
            case CACL_LOSNING:
            case MGCL_TORT:
            case CACL_TORT:
            case BEFUKTET_CACL:
            case SAND_M_SALTLOSNING:
            default:
                gritProductAmount.setOmforentMengdeTort(activity.getDryGritAmount());
                gritProductAmount.setOmforentMengdeVaatt(activity.getWetGritAmount());
                break;
        }
    }

    private ActionListener addNewActivityAction() {
        return e -> {
            Aktivitet activity = new Aktivitet();
            activity.setKjoretoyId(selectedTrip.getVehicleId());
            activity.setFraDatoTid(selectedTrip.getStartTime());
            activity.setTilDatoTid(selectedTrip.getEndTime());
            activity.setOpprettetAv(controller.getPlugin().getLoader().getLoggedOnUser(true).getSignatur());
            activity.setOpprettetDato(new Date());
            activity.setEnhet("");
            activity.setSendtAgresso(false);
            activity.setTrip(selectedTrip);
            activity.setTempId(System.currentTimeMillis());
            activity.setCreatedManager(true);

            selectedTrip.getActivities().add(activity);
            mdlAktivitet.getAktiviteter().add(activity);
            mdlAktivitet.fireTreeChanged();

            aktiviteterChangedMonitor.addEntity(activity, createOriginalValuesAkt(activity));
        };
    }

    private void initActivitiesTable() {
        List<Rode> rodes = controller.getRodeForKontrakt();

        JComboBox cbxRodes = new JComboBox();
        cbxRodes.removeAllItems();
        for (Rode r : rodes) {
            cbxRodes.addItem(r.getNavn());
        }

        List<Prodtype> prodTypes = controller.getProdTypesForDropdown();

        JComboBox cbxProductionTypes = new JComboBox();
        for (Prodtype prodType : prodTypes) {
            cbxProductionTypes.addItem(prodType.getNavn());
        }

        mdlAktivitet = new AktivitetTableModel(null, rodes);
        Map<Integer, String> tooltips = new HashMap<>();

        MipssRenderableEntityTableColumnModel aktivitetColumnModel = getAktivitetColumnModel();
        aktivitetColumnModel.getColumn(COL_ID).setPreferredWidth(65);
        aktivitetColumnModel.getColumn(COL_RODE).setCellEditor(new ComboBoxCellEditor(cbxRodes));
        aktivitetColumnModel.getColumn(COL_UNIT).setPreferredWidth(50);
        aktivitetColumnModel.getColumn(COL_SUM).setPreferredWidth(60);
        aktivitetColumnModel.getColumn(COL_SUM).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, Aktivitet::isApprovedUE));
        aktivitetColumnModel.getColumn(COL_WARNINGS).setCellRenderer(new WarningTableCellRenderer());
        aktivitetColumnModel.getColumn(COL_PRODUCTION_TYPE).setCellEditor(new ComboBoxCellEditor(cbxProductionTypes));
        aktivitetColumnModel.getColumn(COL_R12_PLOWING).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, Aktivitet::isApprovedBuild));
        aktivitetColumnModel.getColumn(COL_R12_INTENTION).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, a -> a.isApprovedBuild() && a.isApprovedIntention()));
        aktivitetColumnModel.getColumn(COL_R12_SIDE_PLOUGH).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, Aktivitet::isApprovedBuild));
        aktivitetColumnModel.getColumn(COL_R12_HEAVY_PLANING).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, Aktivitet::isApprovedBuild));
        aktivitetColumnModel.getColumn(COL_DRY_GRIT).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, Aktivitet::isApprovedBuild));
        aktivitetColumnModel.getColumn(COL_WET_GRIT).setCellRenderer(new StrikethroughNumberCellRenderer(mdlAktivitet, Aktivitet::isApprovedBuild));

        tblAktivitet = new AktivitetTable(mdlAktivitet, tooltips);
        tblAktivitet.setLeafIcon(null);
        tblAktivitet.setShowsRootHandles(false);
        tblAktivitet.setRowHeight(30);
        tblAktivitet.setShowGrid(true, true);
        tblAktivitet.setGridColor(Colors.TABLE_GRID_COLOR);
        tblAktivitet.setColumnControlVisible(true);
        tblAktivitet.getTableHeader().setDefaultRenderer(new CenterHeaderRenderer(tblAktivitet));
        tblAktivitet.setColumnModel(aktivitetColumnModel);
        tblAktivitet.getModel().addTableModelListener(updateTripSum());

        dirtyActivityHighlighter = new TableDirtyActivityHighlightPredicate(mdlAktivitet, activityUtil);
        missingInternalCommentHighlighter = new TableMissingInternalCommentHighlightPredicate(mdlAktivitet, activityUtil);

        tblAktivitet.setHighlighters(
                new ColorHighlighter(new TablePristineActivityHighlightPredicate(mdlAktivitet), Colors.PRISTINE_ACTIVITY, Color.BLACK, Colors.PRISTINE_ACTIVITY, Color.BLACK),
                new ColorHighlighter(dirtyActivityHighlighter, Colors.DIRTY_ACTIVITY, Color.BLACK, Colors.DIRTY_ACTIVITY, Color.BLACK),
                new ColorHighlighter(new TableManualWebActivityHighlightPredicate(mdlAktivitet), Colors.MANUAL_WEB_ACTIVITY, Color.BLACK, Colors.MANUAL_WEB_ACTIVITY, Color.BLACK),
                new ColorHighlighter(new TableManualManagerActivityHighlightPredicate(mdlAktivitet), Colors.MANUAL_MANAGER_ACTIVITY, Color.BLACK, Colors.MANUAL_MANAGER_ACTIVITY, Color.BLACK),
                new ColorHighlighter(missingInternalCommentHighlighter, VALIDATION_HIGHLIGHT_COLOR, Color.BLACK, VALIDATION_HIGHLIGHT_COLOR, Color.BLACK),
                new ColorHighlighter(new IntentionCheckboxEnabler(mdlAktivitet), VALIDATION_HIGHLIGHT_COLOR, Color.BLACK, VALIDATION_HIGHLIGHT_COLOR, Color.BLACK));

        new AktivitetTreeSorter(mdlAktivitet, tblAktivitet, aktivitetColumnModel);

        treeTableScroll = new JMipssTreeTableScrollPane(tblAktivitet, mdlAktivitet);
    }

    private TableModelListener updateTripSum() {
        return e -> {
            sumApproved = 0.0;
            Double tempSum = 0.0;

            for (Aktivitet akt : mdlAktivitet.getAktiviteter()) {
                if (akt.getSum() != null) {
                    if (!akt.hasNoPayment() && akt.isApprovedUE()) {
                        sumApproved += akt.getSum();
                    }

                    tempSum += akt.getSum();
                }
            }

            lblActualSum.setText(
                    MipssNumberFormatter.formatNumber(sumApproved) + " kr (" +
                    MipssNumberFormatter.formatNumber(tempSum) + " kr)");
        };
    }

    private MipssRenderableEntityTableColumnModel getAktivitetColumnModel() {
        return new MipssRenderableEntityTableColumnModel(
                AktivitetTreeView.class, "id", "rode", "article", "unit", "UEquantity",
                "sum", "UEcomment", "warnings", "approveUE", "approveBuild", "approveIntention", "productionType",
                "R12plowing", "R12intention", "R12sideplough", "R12heavyPlaning", "gritProduct", "dryGritQuantity",
                "wetGritQuantity","internalComment");
    }

	private JPanel getTripInfoPanel() {
		JPanel tripInfo = new JPanel(new GridBagLayout());

		tripInfo.add(getButtons(), 			new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,0,0,0), 0, 0));
		tripInfo.add(getInfo(), 			new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(4,0,0,0), 0, 0));
		tripInfo.add(txtTripProcessStatus, 	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(4,0,0,0), 0, 5));

        tripInfo.setMinimumSize(new Dimension(450, 301));
		tripInfo.setPreferredSize(new Dimension(450, 301));
		tripInfo.setMaximumSize(new Dimension(450, 301));

		return tripInfo;
	}

	private JPanel getButtons() {
		JPanel buttons = new JPanel(new GridBagLayout());

		buttons.add(txtCurrentTripId, 	new GridBagConstraints(0,0, 1,1, 0.8,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, 		new Insets(0,0,0,0), 0, 0));
		buttons.add(btnPreviousTrip,	new GridBagConstraints(1,0, 1,1, 0.1,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,0,0,0), 0, 0));
		buttons.add(btnNextTrip,		new GridBagConstraints(2,0, 1,1, 0.1,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,0,0,0), 0, 0));

		return buttons;
	}

	private JPanel getInfo() {
		JPanel top = new JPanel(new GridBagLayout());
		top.setBorder(new EmptyBorder(5, 5, 5, 5));

		top.add(lblStartEnd, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(0,0,0,0), 10, 0));
		top.add(lblActualStartEnd, 	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,0,0,0), 0, 0));

		top.add(lblVendor, 			new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(5,0,0,0), 10, 0));
		top.add(lblActualVendor, 	new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(5,0,0,0), 0, 0));

		top.add(lblVehicle, 		new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(5,0,0,0), 10, 0));
		top.add(lblActualVehicle, 	new GridBagConstraints(1,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(5,0,0,0), 0, 0));

		top.add(lblPurpose, 		new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(5,0,0,0), 10, 0));
		top.add(lblActualPurpose, 	new GridBagConstraints(1,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(5,0,0,0), 0, 0));

		top.add(lblDistance, 		new GridBagConstraints(0,4, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(5,0,0,0), 10, 0));
		top.add(lblActualDistance, 	new GridBagConstraints(1,4, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(5,0,0,0), 0, 0));

		top.add(lblGritting, 		new GridBagConstraints(0,5, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(5,0,0,0), 10, 0));
		top.add(lblActualGritting, 	new GridBagConstraints(1,5, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(5,0,0,0), 0, 0));

		top.add(lblWeather, 		new GridBagConstraints(0,6, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(5,0,0,0), 10, 0));
		top.add(lblActualWeather, 	new GridBagConstraints(1,6, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(5,0,0,0), 0, 0));

		top.add(lblComment, 		new GridBagConstraints(0,7, 1,1, 0.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE,	 	new Insets(7,0,0,0), 10, 0));
		top.add(scroll, 			new GridBagConstraints(1,7, 1,1, 1.0,0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,	new Insets(5,0,0,0), 0, 0));

		JPanel bottom = new JPanel(new GridBagLayout());
		bottom.setBorder(new EmptyBorder(5, 5, 5,5));

		bottom.add(lblSum, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(0,0,0,0), 10, 0));
		bottom.add(lblActualSum, 	new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,			new Insets(0,0,0,0), 0, 0));

		bottom.add(lblDuration, 		new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,	 		new Insets(0,0,0,0), 10, 0));
		bottom.add(lblActualDuration,	new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,0,0,0), 0, 0));

		JPanel info = new JPanel(new GridBagLayout());
		info.setBorder(BorderFactory.createTitledBorder(""));

		info.add(top, 		new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		info.add(bottom, 	new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10,0,0,0), 0, 0));

		return info;
	}

	private JPanel getActivitiesPanel() {
        JPanel leftButtons = new JPanel(new GridBagLayout());
        leftButtons.add(btnProductionReports,   new GridBagConstraints(0, 0, 1, 1, 1, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        leftButtons.add(btnShowMap,             new GridBagConstraints(1, 0, 1, 1, 1, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        leftButtons.add(btnReturnHistory,       new GridBagConstraints(2, 0, 1, 1, 1, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        JPanel buttons = new JPanel(new GridBagLayout());

        buttons.add(leftButtons,        new GridBagConstraints(0, 0, 1, 1, 1, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        buttons.add(btnSaveChanges,     new GridBagConstraints(1, 0, 1, 1, 0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        // TODO: This button will be reintroduced at a later point when we implement the logic necessary.
        //buttons.add(btnAddNewActivity,  new GridBagConstraints(2, 0, 1, 1, 0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

        buttons.add(btnReturnTrip,      new GridBagConstraints(2, 0, 1, 1, 0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        buttons.add(btnSetTripProcessed,     new GridBagConstraints(3, 0, 1, 1, 0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

        JPanel activities = new JPanel(new GridBagLayout());

        activities.add(treeTableScroll, new GridBagConstraints(0, 0, 1, 1, 1, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,        new Insets(0, 0, 0, 0), 0, 0));
        activities.add(buttons,         new GridBagConstraints(0, 1, 1, 1, 0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,  new Insets(5, 0, 0, 0), 0, 0));

		return activities;
	}

	private void initGui() {
		setLayout(new GridBagLayout());

		add(getTripInfoPanel(),		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 4, 3), 0, 0));
		add(pnlMipssrapport, 		new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 0, 4, 5), 0, 0));
		add(getActivitiesPanel(),	new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
	}

    private void loadTrip() {
        setTripActionButtonsState();

        originalActivities = copyActivities(selectedTrip.getActivities());

        if (selectedTrip.getVehicle() != null) {
            activityUtil.setVehicleRodeConnections(controller.getVehicleRodeConnectionsForVehicle(selectedTrip.getVehicle().getId()));
        }

        activityUtil.clearActivitiesInNeedOfComments();

        populateTripInfo(selectedTrip);
        setTripProcessStatusIndicator(selectedTrip);

        articleList = controller.getArticlesForTrip(selectedTrip);
        tblAktivitet.getColumn(COL_ARTICLE).setCellEditor(new MipssTableComboBoxEditor(articleList));

        controller.setMipssTiltakForSingleTrip(selectedTrip);
        populateActivitiesTable();
        setupActivitiesMonitoring();
    }

    private void setTripActionButtonsState() {
        if (selectedTrip.getPeriod().getStatus().getStatusType() == PeriodeStatus.StatusType.OPEN) {
            switch (selectedTrip.getProcessStatus()) {
                case SUBMITTED_BY_SUBCONTRACTOR:
                case UNDER_PROCESS_BY_CONTRACTOR:
                    setTripProcessStatus(TripProcessStatus.UNDER_PROCESS_BY_CONTRACTOR);
                    tblAktivitet.setEnabled(true);
                    btnAddNewActivity.setEnabled(true);
                    btnReturnTrip.setEnabled(true);
                    btnSetTripProcessed.setEnabled(true);
                    break;
                case PROCESSED_BY_SUBCONTRACTOR:
                case RETURNED_TO_SUBCONTRACTOR:
                    tblAktivitet.setEnabled(false);
                    btnSaveChanges.setEnabled(false);
                    btnAddNewActivity.setEnabled(false);
                    btnReturnTrip.setEnabled(false);
                    btnSetTripProcessed.setEnabled(false);
                    break;
                case PROCESSED_BY_CONTRACTOR:
                    tblAktivitet.setEnabled(true);
                    btnAddNewActivity.setEnabled(true);
                    btnReturnTrip.setEnabled(true);
                    btnSetTripProcessed.setEnabled(false);
                    break;
                default:
                    btnReturnTrip.setEnabled(false);
                    btnSetTripProcessed.setEnabled(false);
            }
        } else {
            tblAktivitet.setEnabled(false);
            btnSaveChanges.setEnabled(false);
            btnAddNewActivity.setEnabled(false);
            btnReturnTrip.setEnabled(false);
            btnSetTripProcessed.setEnabled(false);
        }

        btnReturnHistory.setEnabled(!selectedTrip.getReturnCauses().isEmpty());
    }

    private void setTripProcessStatus(TripProcessStatus status) {
        selectedTrip.setProcessStatusId(status.getId());
        controller.updateTrip(selectedTrip);
    }

    private void populateTripInfo(Trip trip) {
        txtCurrentTripId.setText(Resources.getResource("label.tripIdDynamic", trip.getId().toString()));
        lblActualStartEnd.setText(formatStartEnd(trip.getStartTime(), trip.getEndTime()));
        lblActualVendor.setText(trip.getContractorContract().getLeverandor().getNavn());

        if (trip.getVehicle() != null) {
            lblActualVehicle.setText(trip.getVehicle().getEksterntNavn() + " (" + trip.getPerson().getFullName() + ")");
        }

        lblActualPurpose.setText(formatPurpose(trip));
        lblActualDistance.setText((trip.getDistance() != null) ? MipssNumberFormatter.formatNumber(trip.getDistance()) + " km" : "");
        lblActualGritting.setText(formatGritting(trip));
        lblActualWeather.setText(formatWeather(trip));
        txtActualComment.setText(trip.getTripComment());
        lblActualDuration.setText(formatDuration(trip.getStartTime(), trip.getEndTime()));
    }

    private List<Aktivitet> copyActivities(List<Aktivitet> activities) {
        List<Aktivitet> copiedActivities = new ArrayList<>();

        for (Aktivitet old : activities) {
            copiedActivities.add(new Aktivitet(old));
        }

        return copiedActivities;
    }

    private void populateActivitiesTable() {
        activities.clear();

        for (Aktivitet activity : selectedTrip.getActivities()) {
            for (AgrArtikkelV article : articleList) {
                if (article.getArtikkelIdent().equalsIgnoreCase(activity.getArtikkelIdent())) {
                    activity.setViewArtikkel(article);
                    break;
                }
            }

            if (activity.getEnhetsPris() != null && activity.getOmforentMengde() != null) {
                activity.setSum(activity.getEnhetsPris() * activity.getOmforentMengde());
            }

            String prodType = activity.getProductionType();

            if (prodType != null && prodType.contains(":")) {
                activity.setGritProductName(
                        prodType.substring(prodType.indexOf(":") + 1, prodType.length()).trim());
            }

            List<Stroproduktmengde> gritProducts = activity.getStroprodukter();
            Double dryGritAmount = null;
            Double wetGritAmount = null;

            for (Stroproduktmengde gritAmount : gritProducts) {
                if (gritAmount.getOmforentMengdeTort() != null) {
                    if (dryGritAmount != null) {
                        dryGritAmount += gritAmount.getOmforentMengdeTort();
                    } else {
                        dryGritAmount = gritAmount.getOmforentMengdeTort();
                    }
                }

                if (gritAmount.getOmforentMengdeVaatt() != null) {
                    if (wetGritAmount != null) {
                        wetGritAmount += gritAmount.getOmforentMengdeVaatt();
                    } else {
                        wetGritAmount = gritAmount.getOmforentMengdeVaatt();
                    }
                }
            }

            activity.setDryGritAmount(dryGritAmount);
            activity.setWetGritAmount(wetGritAmount);

            activities.add(activity);
        }

        mdlAktivitet.load(activities);
    }

    private void setupActivitiesMonitoring() {
        if (aktiviteterChangedMonitor != null) {
            aktiviteterChangedMonitor.unbind();
            saveStatusRegistry.removeMonitor(aktiviteterChangedMonitor);
        }

        aktiviteterChangedMonitor = createChangeMonitor(mdlAktivitet.getAktiviteter());
        aktiviteterChangedMonitor.bind();

        saveStatusRegistry.setChanged(false);
        saveStatusRegistry.addMonitor(aktiviteterChangedMonitor);
    }

    private ChangeMonitor<Aktivitet> createChangeMonitor(List<Aktivitet> activities) {
        return new ChangeMonitor<>(
                activities, createOriginalValuesAktivitet(activities),
                "rodeId", "artikkelIdent", "omforentMengde", "kommentar", "approvedUE", "approvedBuild",
                "approvedIntention", "productionType", "r12_broyting", "r12_intention", "r12_sideplog", "r12_hovling",
                "dryGritAmount", "wetGritAmount", "internKommentar");
    }

    private List<Object[]> createOriginalValuesAktivitet(List<Aktivitet> list) {
        List<Object[]> ov = new ArrayList<>();
        for (Aktivitet a:list) {
            ov.add(createOriginalValuesAkt(a));
        }
        return ov;
    }

    private Object[] createOriginalValuesAkt(Aktivitet a) {
        return new Object[]{
                a.getRodeId(),
                a.getArtikkelIdent(),
                a.getOmforentMengde(),
                a.getKommentar(),
                a.isApprovedUE(),
                a.isApprovedBuild(),
                a.isApprovedIntention(),
                a.getProductionType(),
                a.getR12_broyting(),
                a.getR12_intention(),
                a.getR12_sideplog(),
                a.getR12_hovling(),
                a.getDryGritAmount(),
                a.getWetGritAmount(),
                a.getInternKommentar()
        };
    }

    private String formatGritting(Trip t) {
	    String formatted = "";

        if (t.hasUsedGrit()) {
            if (t.getGritSalt() != null) {
                formatted = "salt: " + MipssNumberFormatter.formatNumber(t.getGritSalt()) + " " + t.getGritSaltUnit();
            }

            if (t.getGritSand() != null) {
                String sand = "sand: " + MipssNumberFormatter.formatNumber(t.getGritSand()) + " " + t.getGritSandUnit();

                if (formatted.isEmpty()) {
                    formatted = sand;
                } else {
                    formatted += ", " + sand;
                }
            }

            if (t.getGritSolution() != null) {
                String solution = "saltløsning: " + MipssNumberFormatter.formatNumber(t.getGritSolution()) + " " + t.getGritSolutionUnit();

                if (formatted.isEmpty()) {
                    formatted = solution;
                } else {
                    formatted += ", " + solution;
                }
            }
        }

	    return formatted;
    }

    private String formatPurpose(Trip t) {
	    StringBuilder formatted = new StringBuilder();

	    String spacer = "";
        for (WorkType workType : t.getWork()) {
            formatted.append(spacer).append(workType.getName());

            if (workType.getName().equalsIgnoreCase("strøing") && t.getWorkGritProduct() != null && t.getWorkGritDosage() != null) {
                formatted.append(": ").append(t.getWorkGritProduct().getName());
                formatted.append(" (").append(MipssNumberFormatter.formatNumber(t.getWorkGritDosage())).append(" g/m2)");
            }

            spacer = ", ";
        }

        return formatted.toString().toLowerCase();
    }

    private void setTripProcessStatusIndicator(Trip t) {
	    String status;
	    Color color;

	    switch (t.getProcessStatus()) {
            case RETURNED_TO_SUBCONTRACTOR:
                color = Colors.RETURNED;
                status = "Tilbakesendt";
                break;
            case PROCESSED_BY_SUBCONTRACTOR:
                color = Colors.RETURNED;
                status = "Behandlet av UE";
                break;
            case SUBMITTED_BY_SUBCONTRACTOR:
            case UNDER_PROCESS_BY_CONTRACTOR:
                color = Colors.SUBMITTED;
                status = "Innsendt av UE";
                break;
            case PROCESSED_BY_CONTRACTOR:
                color = Colors.PROCESSED;
                status = "Behandlet av entreprenør";
                break;
            case SENT_TO_IO:
                color = Colors.SENT_IO;
                status = "Sendt til IO";
                break;
            default:
                color = Color.WHITE;
                status = "Ukjent";
                break;
        }

        txtTripProcessStatus.setBackground(color);
        txtTripProcessStatus.setText(Resources.getResource("label.tripProcessStatus", status));
    }

    private String formatWeather(Trip t) {
	    String formatted = "";

	    String type = t.getWeatherTypeName();
	    if (type != null) {
	        formatted = type;
        }

        String temp = t.getWeatherTempName();
        if (!formatted.isEmpty() && temp != null) {
            formatted += ", " + temp;
        } else if (temp != null) {
            formatted = temp;
        }

        if (formatted.isEmpty()) {
            formatted = "TODO: FØRE";
        } else {
            formatted += ", TODO: FØRE";
        }

        return formatted;
    }

    private String formatStartEnd(Date start, Date end) {
        String startDate = MipssDateFormatter.formatDate(start, MipssDateFormatter.LONG_TIME_YEARDATE_FORMAT);
        String endDate = MipssDateFormatter.formatDate(end, MipssDateFormatter.LONG_TIME_YEARDATE_FORMAT);

        return startDate + " - " + endDate;
    }

    private String formatDuration(Date start, Date end) {
        Duration dur = Duration.between(start.toInstant(), end.toInstant());

        long hours = dur.toHours();
        long minutes = dur.minusHours(hours).toMinutes();

        String formatted = "";
        if (hours > 0) {
            formatted = hours + " t";
        }

        if (hours > 0 & minutes > 0) {
            formatted += " " + minutes + " min";
        } else if (minutes > 0) {
            formatted += minutes + " min";
        }

        double hoursDecimal = (double) dur.toMinutes() / 60;
        formatted += " (" + MipssNumberFormatter.formatNumber(hoursDecimal) + " t)";

        return formatted;
    }

	public static void main(String[] args) {
		JDialog d = new JDialog();

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		d.setSize(1400, 800);
		d.add(new ActivitiesOverviewPanel());
		d.setVisible(true);
		d.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				d.dispose();
			}
		});
	}

    public AktivitetTable getActivityTable() {
        return tblAktivitet;
    }

}