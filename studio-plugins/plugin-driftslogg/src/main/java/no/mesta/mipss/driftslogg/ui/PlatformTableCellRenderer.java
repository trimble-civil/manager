package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.driftslogg.Trip;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.net.URL;

public class PlatformTableCellRenderer extends DefaultTableCellRenderer {

	private static final String APP_URL = "appAdded.png";
	private static final String MANUALLY_URL = "manuallyAdded.png";

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (value instanceof Trip) {
			Trip trip = (Trip) value;
			return getPlatformInfo(trip, table, isSelected);
		}
		throw new IllegalArgumentException("Can only render Trip");
	}

	private JPanel getPlatformInfo(Trip trip, JTable table, boolean isSelected) {
		JPanel cell = new JPanel(new BorderLayout());
		cell.setBackground(table.getSelectionBackground());
		cell.setForeground(table.getSelectionForeground());
		cell.setOpaque(isSelected);

		if (trip.getId() != null) {
			switch (trip.getPlatform()) {
				case WEB:
					addIcon(cell, MANUALLY_URL);
					setToolTipText("Lagt inn manuelt");
					break;
				case WEBAPP:
					addIcon(cell, APP_URL);
					setToolTipText("Lagt inn via app");
					break;
			}
		}

		return cell;
	}

	private void addIcon(JPanel panel, String imageUrl) {
		JLabel icon = new JLabel(getIcon(imageUrl));
		icon.setOpaque(false);

		panel.add(icon, BorderLayout.CENTER);
	}

	private ImageIcon getIcon(String name) {
		URL imageUrl = this.getClass().getClassLoader().getResource(name);
		return new ImageIcon(imageUrl);
	}

}