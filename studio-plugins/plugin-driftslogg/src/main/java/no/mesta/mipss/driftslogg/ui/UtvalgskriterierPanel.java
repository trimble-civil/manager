package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.calendar.DateSelectionModel.SelectionMode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

@SuppressWarnings("serial")
public class UtvalgskriterierPanel extends JPanel {

    private final DriftsloggController controller;

	private JLabel lblKontrakt;
	private JLabel lblLeverandor; 
	private JLabel lblLevkontrakt;
	private JLabel lblKjoretoy;
	private JLabel lblMonth;
	
	private JMipssContractPicker contractPicker;
	private JComboBox cmbLeverandor;
	private JComboBox cmbLevkontrakt;
	private JComboBox cmbKjoretoy;
	
	private JMipssDatePicker dateMonth;
	
	private JButton btnForrige;
	private JButton btnNeste;

	private JCheckBox chkRegistered;
	private JLabel lblRegistered;
	private JCheckBox chkSubmitted;
	private JLabel lblSubmitted;
	private JCheckBox chkProcessed;
	private JLabel lblProcessed;
	private JCheckBox chkPaid;
	private JLabel lblPaid;
	
	//Brukes kun for å vise innholdet i nedtrekkslisten riktig
	private List<KontraktLeverandor> kontraktLeverandorer;
	
	public UtvalgskriterierPanel(DriftsloggController controller){
		this.controller = controller;
		initComponents();
		initGui();
		bind();
    }

	private void bind() {
		Binding<JMipssContractPicker, Boolean, UtvalgskriterierPanel, Boolean> enableThis = BindingHelper.createbinding(contractPicker, "${valgtKontrakt != null}", this, "enabled");

		//bindinger for spørringsparametere for å hente data
		Binding<JMipssContractPicker, Object, DriftsloggQueryParams, Object> dkqBinding = BindingHelper.createbinding(contractPicker, "valgtKontrakt", controller.getQueryParams(), "kontrakt");
		Binding<JComboBox, Object, DriftsloggQueryParams, Object> levqBinding = BindingHelper.createbinding(cmbLeverandor, "selectedItem", controller.getQueryParams(), "leverandor");
		Binding<JComboBox, Object, DriftsloggQueryParams, Object> levkqBinding = BindingHelper.createbinding(cmbLevkontrakt, "selectedItem", controller.getQueryParams(), "levkontrakt");
		Binding<JComboBox, Object, DriftsloggQueryParams, Object> kjoretoyBinding = BindingHelper.createbinding(cmbKjoretoy, "selectedItem", controller.getQueryParams(), "kjoretoy");
		Binding<JMipssDatePicker, Object, DriftsloggQueryParams, Object> monthqBinding = BindingHelper.createbinding(dateMonth, "date", controller.getQueryParams(), "month", UpdateStrategy.READ_WRITE);
        Binding<JCheckBox, Object, DriftsloggQueryParams, Object> registeredBinding = BindingHelper.createbinding(chkRegistered, "selected", controller.getQueryParams(), "registered");
        Binding<JCheckBox, Object, DriftsloggQueryParams, Object> submittedBinding = BindingHelper.createbinding(chkSubmitted, "selected", controller.getQueryParams(), "submitted");
		Binding<JCheckBox, Object, DriftsloggQueryParams, Object> processedBinding = BindingHelper.createbinding(chkProcessed, "selected", controller.getQueryParams(), "processed");
		Binding<JCheckBox, Object, DriftsloggQueryParams, Object> paidBinding = BindingHelper.createbinding(chkPaid, "selected", controller.getQueryParams(), "paid");
		
		//bindinger for å sette verdier i comboboxene
		Binding<DriftsloggQueryParams, Object, UtvalgskriterierPanel, Object> thiskBinding = BindingHelper.createbinding(controller.getQueryParams(), "kontrakt", this, "kontrakt");
		Binding<DriftsloggQueryParams, Object, UtvalgskriterierPanel, Object> thislBinding = BindingHelper.createbinding(controller.getQueryParams(), "leverandor", this, "leverandor");
		BindingGroup bindings = controller.getBindings();
		bindings.addBinding(enableThis);
		bindings.addBinding(dkqBinding);
		bindings.addBinding(levqBinding);
		bindings.addBinding(levkqBinding);
		bindings.addBinding(kjoretoyBinding);
		bindings.addBinding(monthqBinding);
		bindings.addBinding(registeredBinding);
		bindings.addBinding(processedBinding);
		bindings.addBinding(submittedBinding);
		bindings.addBinding(paidBinding);

		bindings.addBinding(thiskBinding);
		bindings.addBinding(thislBinding);

		chkSubmitted.setSelected(true);
	}
	
	/**
	 * Henter leverandører tilknyttet kontrakter når det velges en ny driftkontrakt. 
	 * Leverandørene bindes til comboboxen for leverandører
	 * @param kontrakt
	 */
	public void setKontrakt(Driftkontrakt kontrakt){
		if (kontrakt==null){
			cmbLeverandor.setModel(new DefaultComboBoxModel());
			return;
		}
		List<Leverandor> leverandorer = controller.getLeverandorer();
		cmbLeverandor.setModel(new MipssComboBoxModel<Leverandor>(leverandorer, true));
		
		//Oppretter listen med kontraktsleverandorer og setter ny renderer
		kontraktLeverandorer = controller.getKontraktLeverandorer();
		cmbLeverandor.setRenderer(new LeverandorCellRenderer("<Alle>", kontraktLeverandorer));
	}
	
	/**
	 * Henter levkontrakter tilknyttet leverandøren når leverandør velges. 
	 * Bindes til levkontrakt comboboxen.
	 * @param leverandor
	 */
	public void setLeverandor(Leverandor leverandor){
		if (leverandor==null){
			cmbLevkontrakt.setModel(new DefaultComboBoxModel());
			cmbKjoretoy.setModel(new DefaultComboBoxModel());
			return;
		}
		List<Levkontrakt> levkontrakter = controller.getLevkontrakter();
		cmbLevkontrakt.setModel(new MipssComboBoxModel<Levkontrakt>(levkontrakter, true));
		cmbKjoretoy.setModel(new MipssComboBoxModel<Kjoretoy>(controller.getKjoretoyForLeverandor(), true));
	}
	
	/**
	 * Setter alle komponentene til enabled true/false
	 */
	public void setEnabled(boolean enabled){
		cmbLeverandor.setEnabled(enabled);
		cmbLevkontrakt.setEnabled(enabled);
		cmbKjoretoy.setEnabled(enabled);
		dateMonth.setEnabled(enabled);
		btnForrige.setEnabled(enabled);
		btnNeste.setEnabled(enabled);
		chkRegistered.setEnabled(enabled);
		chkSubmitted.setEnabled(enabled);
		chkProcessed.setEnabled(enabled);
		chkPaid.setEnabled(enabled);
	}
	
	private void initComponents() {
		lblKontrakt = new JLabel(Resources.getResource("label.kontrakt"));
		lblLeverandor = new JLabel(Resources.getResource("label.leverandor"));
		lblLevkontrakt = new JLabel(Resources.getResource("label.levkontrakt"));
		lblKjoretoy = new JLabel(Resources.getResource("label.kjoretoy"));
		lblMonth = new JLabel(Resources.getResource("label.month"));
		Font f = lblMonth.getFont();
		Font bold = new Font(f.getName(), Font.BOLD, f.getSize());
		lblMonth.setFont(bold);

		chkRegistered = new JCheckBox();
		chkRegistered.setToolTipText(Resources.getResource("label.registered.tooltip"));
		lblRegistered = new JLabel(Resources.getResource("label.registered"));
		lblRegistered.setToolTipText(Resources.getResource("label.registered.tooltip"));
		lblRegistered.setIcon(IconResources.PERIOD_STATUS_REGISTERED_16);

		chkSubmitted = new JCheckBox();
		chkSubmitted.setToolTipText(Resources.getResource("label.submitted.tooltip"));
		lblSubmitted = new JLabel(Resources.getResource("label.submitted"));
		lblSubmitted.setToolTipText(Resources.getResource("label.submitted.tooltip"));
		lblSubmitted.setIcon(IconResources.PERIOD_STATUS_SUBMITTED_16);

		chkProcessed = new JCheckBox();
		chkProcessed.setToolTipText(Resources.getResource("label.processed.tooltip"));
		lblProcessed = new JLabel(Resources.getResource("label.processed"));
		lblProcessed.setToolTipText(Resources.getResource("label.processed.tooltip"));
		lblProcessed.setIcon(IconResources.PERIOD_STATUS_PROCESSED_16);

		chkPaid = new JCheckBox();
		chkPaid.setToolTipText(Resources.getResource("label.paid.tooltip"));
		lblPaid = new JLabel(Resources.getResource("label.paid"));
		lblPaid.setToolTipText(Resources.getResource("label.paid.tooltip"));
		lblPaid.setIcon(IconResources.PERIOD_STATUS_PAID_16);
		
		contractPicker = new JMipssContractPicker(controller.getPlugin(),false);
		contractPicker.getDropdown().setPreferredSize(null);
		
		cmbLeverandor = new JComboBox();
		cmbLevkontrakt = new JComboBox();
		cmbKjoretoy = new JComboBox();
		
		cmbLeverandor.setRenderer(new MipssListCellRenderer<Leverandor>("<Alle>"));
		cmbLevkontrakt.setRenderer(new MipssListCellRenderer<Levkontrakt>("<Alle>"));
		cmbKjoretoy.setRenderer(new MipssListCellRenderer<Kjoretoy>("<Alle>"));
		
		btnForrige = new JButton(getForrigeAction());
		btnForrige.setPreferredSize(new Dimension(110, 25));
		btnNeste = new JButton(getNesteAction());
		btnNeste.setPreferredSize(new Dimension(110, 25));

		dateMonth = new JMipssDatePicker();
		dateMonth.getMonthView().getSelectionModel().addDateSelectionListener(ev -> {
            SortedSet<Date> selection = ev.getSelection();

            if (selection.isEmpty())
                return;

            Date startOfMonth = CalendarUtil.getFirstDateGivenMonth(selection.first());
            Date endOfMonth = CalendarUtil.getLastDateGivenMonth(selection.last());

            dateMonth.getMonthView().setSelectionInterval(startOfMonth, endOfMonth);
        });

		dateMonth.setPreferredSize(new Dimension(110, 23));
		dateMonth.getMonthView().setShowingWeekNumber(true);
		dateMonth.getMonthView().setShowingLeadingDays(true);
		dateMonth.getMonthView().setShowingTrailingDays(true);
		dateMonth.setFormats("MMMM yyyy");
		dateMonth.getMonthView().setSelectionMode(SelectionMode.MULTIPLE_INTERVAL_SELECTION);
		dateMonth.getEditor().setEditable(false);
		dateMonth.getEditor().setHorizontalAlignment(JTextField.CENTER);
	}

    private Action getForrigeAction(){
		return new AbstractAction(Resources.getResource("button.forrige"), IconResources.GO_PREVIOUS_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				decreaseMonth();
			}
		};
	}

	private void decreaseMonth(){
		SortedSet<Date> selection = dateMonth.getMonthView().getSelection();

		if (selection.isEmpty())
			return;

		Date first = selection.first();
		first = CalendarUtil.add(first, Calendar.MONTH, -1);

		Date last = selection.last();
		last = CalendarUtil.add(last, Calendar.MONTH, -1);

		dateMonth.getMonthView().getSelectionModel().setSelectionInterval(first, last);
	}

	private Action getNesteAction(){
		return new AbstractAction(Resources.getResource("button.neste"), IconResources.GO_NEXT_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				increaseMonth();
			}
		};
	}
	
	private void increaseMonth(){
		SortedSet<Date> selection = dateMonth.getMonthView().getSelection();

		if (selection.isEmpty())
			return;

		Date first = selection.first();
		first = CalendarUtil.add(first, Calendar.MONTH, 1);

		Date last = selection.last();
		last = CalendarUtil.add(last, Calendar.MONTH, 1);

		dateMonth.getMonthView().getSelectionModel().setSelectionInterval(first, last);
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.selectionCriteria")));

		JPanel pnlStatus = new JPanel(new GridBagLayout());
		pnlStatus.add(chkRegistered, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(lblRegistered, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(chkSubmitted,  new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(lblSubmitted,  new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(chkProcessed,  new GridBagConstraints(4,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(lblProcessed,  new GridBagConstraints(5,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(chkPaid, 		 new GridBagConstraints(6,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		pnlStatus.add(lblPaid, 		 new GridBagConstraints(7,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));

		JPanel pnlKontraktKjt = new JPanel(new GridBagLayout());
		pnlKontraktKjt.add(cmbLevkontrakt,  new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,0,0,0),0,0));
		pnlKontraktKjt.add(lblKjoretoy, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		new Insets(0,5,0,0),0,0));
		pnlKontraktKjt.add(cmbKjoretoy, 	new GridBagConstraints(2,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 	new Insets(0,5,0,0),0,0));

        add(lblMonth, 		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(0,0,0,0), 0, 0));
        add(dateMonth, 		new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,		  new Insets(0,5,0,0), 0, 0));
        add(btnForrige, 	new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,new Insets(0,5,0,0), 0, 0));
        add(btnNeste, 		new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0), 0, 0));

		add(lblKontrakt, 	new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(5,0,0,0), 0, 0));
		add(contractPicker, new GridBagConstraints(1,1, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,new Insets(5,5,0,0), 0, 0));
		
		add(lblLeverandor, 	new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(10,0,0,0), 0, 0));
		add(cmbLeverandor, 	new GridBagConstraints(1,2, 3,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,new Insets(10,5,0,0), 0, 0));
		
		add(lblLevkontrakt, new GridBagConstraints(0,3, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 	  new Insets(10,0,0,0), 0, 0));
		add(pnlKontraktKjt, new GridBagConstraints(1,3, 3,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,new Insets(10,5,0,0), 0, 0));

		add(pnlStatus, 		new GridBagConstraints(0,5, 4,1, 0.0,0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 	  new Insets(10,0,0,0), 0, 0));
	}
	
	public void setCurrentMonth() {
		Date startOfMonth = CalendarUtil.getFirstDateCurrentMonth();
		Date endOfMonth = CalendarUtil.getLastDateCurrentMonth();

		dateMonth.getMonthView().setSelectionInterval(startOfMonth, endOfMonth);
		dateMonth.getMonthView().getSelectionModel().setSelectionInterval(startOfMonth, endOfMonth);
	}

}