package no.mesta.mipss.driftslogg;

import javax.swing.JComponent;

import no.mesta.mipss.driftslogg.ui.DriftsloggPanel;
import no.mesta.mipss.plugin.MipssPlugin;

@SuppressWarnings("serial")
public class DriftsloggModule extends MipssPlugin{
	
	private DriftsloggPanel moduleGui;
	private DriftsloggController controller;
	
	@Override
	public JComponent getModuleGUI() {
		return moduleGui;
	}

	@Override
	protected void initPluginGUI() {
		controller = new DriftsloggController(this);
		moduleGui = new DriftsloggPanel(controller);
		controller.bind();
		moduleGui.getUtvalgsPanel().setCurrentMonth();
	}

	@Override
	public void shutdown() {
		controller.getBindings().unbind();
	}
}
