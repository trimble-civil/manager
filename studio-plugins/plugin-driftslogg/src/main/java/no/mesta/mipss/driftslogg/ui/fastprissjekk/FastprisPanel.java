package no.mesta.mipss.driftslogg.ui.fastprissjekk;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Formatter;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.FastprisStatus;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.driftslogg.ui.AlternatingTableHighlightPredicate;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.BooleanRendererToolTip;
import no.mesta.mipss.ui.table.CurrencyRenderer;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.decorator.SortOrder;

@SuppressWarnings("serial")
public class FastprisPanel extends JPanel{

	private final DriftsloggController controller;

	private MipssBeanTableModel<FastprisStatus> mdlFastpris;
	private JMipssBeanScrollPane scrFastpris;
	private JLabel lblSumPris;
	private JTextField txtSumPris;
	private JButton btnLukk;
	
	private BindingGroup bg = new BindingGroup();
	
	public FastprisPanel(DriftsloggController controller){
		this.controller = controller;
		initComponents();
		initGui();
		bind();
	}

	private void bind() {
		mdlFastpris.loadData();
		bg.bind();
	}
	public void dispose(){
		bg.unbind();
	}

	private void setFastprisStatus(List<FastprisStatus> fastprisStatus){
		Double sum = 0.0;
		for (FastprisStatus fs:fastprisStatus){
			sum+=fs.getSumPris();
		}
		
		Formatter formatter = new Formatter();
    	formatter = formatter.format(DoubleRenderer.MEDIUM_FORMAT, sum);
    	
		txtSumPris.setText(formatter.toString());
	}

	private void initComponents() {
		mdlFastpris = new MipssBeanTableModel<>(FastprisStatus.class, "containsEditable", "${leverandor.navn}", "${artikkel.levkontraktIdent}", "${artikkel.artikkelNavn}", "${artikkel.pris}", "antallRegistrert", "antallGodkjent", "sumPris", "uker");
		mdlFastpris.setBeanInstance(controller);
		mdlFastpris.setBeanMethod("getFastpriserForKontrakt");
		mdlFastpris.setBeanInterface(DriftsloggController.class);
		mdlFastpris.addTableLoaderListener(new MipssBeanLoaderListener() {
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {
			}
			
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				setFastprisStatus(mdlFastpris.getEntities());
			}
		});
		
		MipssRenderableEntityTableColumnModel fastprisColumnModel = new MipssRenderableEntityTableColumnModel(FastprisStatus.class, mdlFastpris.getColumns());
		JMipssBeanTable<FastprisStatus> tblFastpris = new JMipssBeanTable<>(mdlFastpris, fastprisColumnModel);
		DoubleRenderer render = new DoubleRenderer(DoubleRenderer.MEDIUM_FORMAT);
		render.setTransparentBackground(true);
		tblFastpris.setDefaultRenderer(Double.class,  render);
		
		CurrencyRenderer cRenderer = new CurrencyRenderer();
		tblFastpris.getColumnExt(4).setCellRenderer(cRenderer);
		
		tblFastpris.setDefaultRenderer(Boolean.class, new BooleanRendererToolTip(IconResources.LOCK_ICON_16, true, Resources.getResource("fastpris.tooltip")));
		tblFastpris.setFillsViewportHeight(true);
		tblFastpris.setFillsViewportWidth(true);
		tblFastpris.setDoWidthHacks(true);
		tblFastpris.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
		tblFastpris.setColumnControlVisible(true);
		tblFastpris.setSortOrder(5, SortOrder.ASCENDING);
		tblFastpris.getColumnExt(0).setPreferredWidth(10);
		tblFastpris.getColumnExt(1).setPreferredWidth(150);
		tblFastpris.getColumnExt(2).setPreferredWidth(70);
		tblFastpris.getColumnExt(3).setPreferredWidth(120);
		tblFastpris.getColumnExt(6).setPreferredWidth(60);
		tblFastpris.getColumnExt(7).setPreferredWidth(60);

		AlternatingTableHighlightPredicate highlight = new AlternatingTableHighlightPredicate();
		tblFastpris.setHighlighters(highlight.getHighlighter());
		
		scrFastpris = new JMipssBeanScrollPane(tblFastpris, mdlFastpris);
		
		lblSumPris = new JLabel(Resources.getResource("label.sumPris"));
		txtSumPris = new JTextField(10);
		txtSumPris.setEditable(false);
		
		btnLukk = new JButton(getLukkAction());
	}
	
	private JXHeader createHeader(){
		JXHeader head = new JXHeader(Resources.getResource("fastpris.title", CalendarUtil.getDisplayNameForMonth(controller.getQueryParams().getMonth())), Resources.getResource("fastpris.description"));
		head.setIcon(IconResources.SHAKING_HAND_64);
		return head;
	}
	
	private Action getLukkAction(){
		return new AbstractAction(Resources.getResource("button.lukk"), IconResources.CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				FastprisPanel.this.dispose();
				SwingUtilities.windowForComponent(FastprisPanel.this).dispose();
			}
		};
	}
	
	private void initGui() {
		setLayout(new BorderLayout());
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		pnlButtons.add(btnLukk, 	new GridBagConstraints(2,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10,5,10,5),0,0));
		
		JPanel pnlBottom = new JPanel(new GridBagLayout());
		pnlBottom.add(lblSumPris, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10,5,10,5),0,0));
		pnlBottom.add(txtSumPris, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
		pnlBottom.add(pnlButtons, new GridBagConstraints(2,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
		
		
		add(createHeader(), BorderLayout.NORTH);
		add(scrFastpris);
		add(pnlBottom, BorderLayout.SOUTH);
	}

}