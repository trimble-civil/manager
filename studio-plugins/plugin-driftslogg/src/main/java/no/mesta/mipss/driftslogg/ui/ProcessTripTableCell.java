package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.ui.Colors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.net.URL;

import static no.mesta.mipss.common.TripProcessStatus.PROCESSED_BY_CONTRACTOR;
import static no.mesta.mipss.common.TripProcessStatus.PROCESSED_BY_SUBCONTRACTOR;
import static no.mesta.mipss.common.TripProcessStatus.RETURNED_TO_SUBCONTRACTOR;
import static no.mesta.mipss.driftslogg.ui.ProcessTripTableCell.ProcessAction.OPEN_TRIP_DETAIL_VIEW;
import static no.mesta.mipss.driftslogg.ui.ProcessTripTableCell.ProcessAction.PROCESS_TRIP_AUTOMATICALLY;

public class ProcessTripTableCell extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {

    private static final String PROCESSING_COMPLETED_URL = "processingCompleted.png";

	private DriftsloggController controller;
	private Trip trip;
	private String showTripText;
	private String processTripText;

	private JPanel cellUnprocessed;
    private JPanel cellProcessed;
	private JButton openTripDetailView;
    private JButton processTripAutomatically;
    private EmptyBorder spacing;

	enum ProcessAction {
		OPEN_TRIP_DETAIL_VIEW, PROCESS_TRIP_AUTOMATICALLY
	}

	public ProcessTripTableCell(DriftsloggController controller) {
		this.controller = controller;

		showTripText = Resources.getResource("button.show");
		processTripText = Resources.getResource("button.process");

		openTripDetailView = getFancyButton("button.process", Colors.BOOTSTRAP_BLUE, OPEN_TRIP_DETAIL_VIEW);
        processTripAutomatically = getFancyButton("button.automaticallyProcess", Colors.BOOTSTRAP_GREEN, PROCESS_TRIP_AUTOMATICALLY);

        cellUnprocessed = new JPanel(new BorderLayout());

        cellProcessed = new JPanel(new GridBagLayout());
        JLabel processingCompleted = new JLabel(getIcon(PROCESSING_COMPLETED_URL));
        processingCompleted.setOpaque(false);
        cellProcessed.add(processingCompleted, new GridBagConstraints());

        spacing = new EmptyBorder(3, 3, 3, 3);
	}

	private JButton getFancyButton(String resourceName, Color background, ProcessAction processAction) {
		JButton button = new JButton(Resources.getResource(resourceName));
		button.setForeground(Color.WHITE);
		button.setIcon(new Icon() {
			@Override
			public void paintIcon(Component c, Graphics g, int x, int y) {
				g.setColor(background);
				g.fillRect(0, 0, c.getWidth(), c.getHeight());
			}

			@Override
			public int getIconWidth() {
				return 0;
			}

			@Override
			public int getIconHeight() {
				return 0;
			}
		});

		switch (processAction) {
			case OPEN_TRIP_DETAIL_VIEW:
				button.addActionListener(e -> controller.openTrip(trip));
				break;
			case PROCESS_TRIP_AUTOMATICALLY:
				button.addActionListener(e -> controller.automaticallyProcessTrip(trip));
				break;
		}

		return button;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return getComponent(table, value, isSelected);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return getComponent(table, value, isSelected);
	}

	@Override
	public Object getCellEditorValue() {
		return null;
	}

	private void setBackground(JPanel cell, JTable table, boolean isSelected) {
        cell.setBackground(table.getSelectionBackground());
        cell.setForeground(table.getSelectionForeground());
        cell.setOpaque(isSelected);
	}

	private JComponent getComponent(JTable table, Object value, boolean isSelected) {
		if (value instanceof Trip) {
			Trip trip = (Trip) value;
			this.trip = trip;

			if (trip.getProcessStatus().getLevel() >= PROCESSED_BY_CONTRACTOR.getLevel() &&
					trip.getProcessStatus().getLevel() < TripProcessStatus.UNKNOWN.getLevel()) {
                setBackground(cellProcessed, table, isSelected);
                return cellProcessed;
            } else if (trip.getId() != null) {
				setBackground(cellUnprocessed, table, isSelected);
                cellUnprocessed.removeAll();

				if (!trip.containsWarnings()) {
                    setTwoButtonLayout(cellUnprocessed, openTripDetailView, processTripAutomatically);
                } else {
					// We want to change the wording on the button in certain scenarios.
					if (trip.getProcessStatus() == RETURNED_TO_SUBCONTRACTOR ||
							trip.getProcessStatus() == PROCESSED_BY_SUBCONTRACTOR) {
						openTripDetailView.setText(showTripText);
					} else {
						openTripDetailView.setText(processTripText);
					}

                    setOneButtonLayout(cellUnprocessed, openTripDetailView);
                }

				return cellUnprocessed;
			} else {
				JPanel cell = new JPanel();
                setBackground(cell, table, isSelected);
				return cell;
			}
		}
		throw new IllegalArgumentException("Can only render/edit Trip objects");
	}

    private ImageIcon getIcon(String name) {
        URL imageUrl = this.getClass().getClassLoader().getResource(name);
        return new ImageIcon(imageUrl);
    }

    private void setTwoButtonLayout(JPanel panel, JButton top, JButton bottom) {
        panel.setLayout(new BorderLayout());
        panel.setBorder(spacing);
        panel.add(top, BorderLayout.NORTH);
        panel.add(bottom, BorderLayout.SOUTH);
    }

    private void setOneButtonLayout(JPanel panel, JButton button) {
        panel.setLayout(new GridBagLayout());
		panel.setBorder(spacing);
        panel.add(button, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
    }

}