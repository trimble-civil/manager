package no.mesta.mipss.driftslogg;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.driftslogg.ui.PlatformTableCellRenderer;
import no.mesta.mipss.driftslogg.ui.StatusTableCellRenderer;
import no.mesta.mipss.driftslogg.ui.TripTableModel;
import no.mesta.mipss.driftslogg.ui.WarningTableCellRenderer;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;

import javax.swing.table.TableCellRenderer;
import java.awt.event.MouseEvent;
import java.util.Map;

public class TripTable extends JMipssTreeTable {

    private final TripTableModel model;

    public TripTable(TripTableModel model, Map tooltips) {
        super(model, tooltips);
        this.model = model;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        int column = this.columnAtPoint(event.getPoint());
        int row = this.rowAtPoint(event.getPoint());

        TableCellRenderer renderer = getCellRenderer(row, column);

        if (renderer instanceof WarningTableCellRenderer) {
            if (row >= 0 && row < model.getTrips().size()) {
                Trip trip = model.getTrips().get(row);
                return trip.getWarningString();
            }
        }

        if (renderer instanceof PlatformTableCellRenderer) {
            if (row >= 0 && row < model.getTrips().size()) {
                return ((PlatformTableCellRenderer) renderer).getToolTipText();
            }
        }

        if (renderer instanceof StatusTableCellRenderer) {
            if (row >= 0 && row < model.getTrips().size()) {
                return ((StatusTableCellRenderer) renderer).getToolTipText();
            }
        }

        // Finds tool tip text for period status (padlock cells). First row is special and contains a dummy trip object.
        if (getColumn(column).getIdentifier().toString().equalsIgnoreCase(
                "no.mesta.mipss.driftslogg.ui.TripTreeView.periodStatus") && row > 0) {
            Trip trip = model.getTrips().get(row);

            String monthName = MipssDateFormatter.formatDate(trip.getPeriod().getFraDato(), "MMMMM");
            String contractMonth = trip.getPeriod().getLevkontraktIdent() + " " + monthName;

            switch (trip.getPeriod().getStatus().getStatusType()) {
                case CLOSED:
                    return Resources.getResource("toolTip.tripTable.periodStatusClosed", contractMonth);
                case SENT_IO:
                    return Resources.getResource("toolTip.tripTable.periodStatusSentIO", contractMonth);
            }
        }

        return null;
    }

}