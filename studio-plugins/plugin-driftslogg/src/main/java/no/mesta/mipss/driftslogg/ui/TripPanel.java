package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.persistence.driftslogg.Periode;
import no.mesta.mipss.persistence.driftslogg.PeriodeStatus;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.Colors;
import no.mesta.mipss.ui.JMipssTreeTableScrollPane;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXBusyLabel;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@SuppressWarnings("serial")
public class TripPanel extends JPanel{

	private final DriftsloggController controller;

	private JMipssTreeTableScrollPane treeTableScroll;
	private JTabbedPane tab;
	private JLabel warningLabel;
	private JButton btnSok;
	private JPopupListMenu btnExportToExcel;
	private JMenuItem exportMonth;
	private JMenuItem exportEgendef;
	private JButton btnFastpris;
	private JButton btnIO;
	private JLabel lblIoInfo;
	private JPopupListMenu btnRapport;
	private JPopupListMenu btnAmountReportOperationClass;
	private String closeText;
	private String closeToolTip;
	private ImageIcon closeIcon;
	private String openText;
	private String openToolTip;
	private ImageIcon openIcon;
	private JButton btnChangePeriodStatus;
	private JMenuItem mndrapp;
	private JMenuItem egendefrapp;
	private JMenuItem amountReport;
	private JMenuItem deviantReport;
	private JMenuItem outsideTripReport;
	private JButton btnVaerRapport;

	private PeriodeStatus.StatusType periodStatus = PeriodeStatus.StatusType.OPEN;
	private boolean conflictingPeriodStatus = false;

	public TripPanel(DriftsloggController controller){
		this.controller = controller;
		initComponents();
		initGui();
		bind();
	}

	private void initComponents() {
		tab = new JTabbedPane();
		warningLabel = new JLabel();
		warningLabel.setVisible(false);
		warningLabel.setBackground(Colors.WARNING_COLOR);
		warningLabel.setHorizontalAlignment(JLabel.CENTER);
		warningLabel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED, Colors.ERROR_COLOR, Colors.WARNING_COLOR));
		warningLabel.setOpaque(true);
		treeTableScroll = new JMipssTreeTableScrollPane(controller.getTripTable(), controller.getTripTableModel());
		tab.addTab("Variable ting", treeTableScroll);
		tab.addTab("Faste ting", new JPanel());
		btnSok = new JButton(controller.getAktivitetSokAction());
		btnFastpris = new JButton(controller.getFastprisSjekkAction(this));
		btnIO = new JButton(controller.getSendIOAction());
		btnIO.setToolTipText(Resources.getResource("button.io.toolip"));
		
		String enableAgresso = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.driftslogg", "enableAgresso").getVerdi();
		btnIO.setEnabled(Boolean.valueOf(enableAgresso));
		
		List<JMenuItem> popupList = new ArrayList<>();
		mndrapp = new JMenuItem(controller.getMengdeRapportMndAction());
		mndrapp.setToolTipText(Resources.getResource("button.mengdeRapportMnd.tooltip"));
		egendefrapp = new JMenuItem(controller.getMengdeRapportEgenDefAction());
		egendefrapp.setToolTipText(Resources.getResource("button.mengdeRapportEgenDef.tooltip"));
		
		popupList.add(mndrapp);
		popupList.add(egendefrapp);
		btnRapport = new JPopupListMenu(Resources.getResource("button.mengdeRapport"), IconResources.EXCEL_ICON16, popupList);
		btnRapport.setToolTipText(Resources.getResource("button.mengdeRapport.tooltip"));

		popupList = new ArrayList<>();
		deviantReport = new JMenuItem(controller.getDeviantActivitiesReport());
		amountReport = new JMenuItem(controller.getApprovedAmountsReport());
		outsideTripReport = new JMenuItem(controller.getOutsideTripReport());

		popupList.add(deviantReport);
		popupList.add(amountReport);
		popupList.add(outsideTripReport);
		btnAmountReportOperationClass = new JPopupListMenu(Resources.getResource("button.amountReportOperationClass"), IconResources.EXCEL_ICON16, popupList);
		btnAmountReportOperationClass.setToolTipText(Resources.getResource("button.mengdeRapport.tooltip"));
		
		List<JMenuItem> exportValgList = new ArrayList<>();
		exportMonth = new JMenuItem(controller.getExportMonthAction());
		exportEgendef = new JMenuItem(controller.getExportEgendefAction());
		exportValgList.add(exportMonth);
		exportValgList.add(exportEgendef);
		btnExportToExcel = new JPopupListMenu(Resources.getResource("button.exportToExcel"), IconResources.EXCEL_ICON16, exportValgList);
		btnExportToExcel.setToolTipText(Resources.getResource("button.exportToExcel.tooltip"));

		StringBuilder agrText = new StringBuilder();
		agrText.append("<html>");
		agrText.append(KonfigparamPreferences.getInstance().hentEnForApp(
				"studio.mipss.driftslogg", "operationLogTransferMessage").getVerdi());
		agrText.append(" <b>");
		agrText.append(controller.getAgressoOverforingDatoString());
		agrText.append("</b></html>");

		lblIoInfo = new JLabel(agrText.toString());
		lblIoInfo.setToolTipText(agrText.toString());
		lblIoInfo.setHorizontalAlignment(SwingConstants.CENTER);
		
		btnVaerRapport = new JButton(controller.getVaerRapportAction());

		controller.getTripTable().getSelectionModel().addListSelectionListener(this::getPeriodStatusListener);

		closeText = Resources.getResource("button.closePeriod");
		closeToolTip = Resources.getResource("button.closePeriod.tooltip");
		closeIcon = IconResources.LOCK2_ICON_16;
		openText = Resources.getResource("button.openPeriod");
		openToolTip = Resources.getResource("button.openPeriod.tooltip");
		openIcon = IconResources.UNLOCK2_ICON_16;

		btnChangePeriodStatus = new JButton(getPeriodStatusAction());
		btnChangePeriodStatus.setPreferredSize(new Dimension(129, 25));
		btnChangePeriodStatus.setEnabled(false);
	}
		
	private void bind(){
		Binding<DriftsloggQueryParams, Object, TripPanel, Object> mndBind = BindingHelper.createbinding(controller.getQueryParams(), "month", this, "month");

		Binding<DriftsloggQueryParams, Object, JButton, Object> sokBind = BindingHelper.createbinding(controller.getQueryParams(), "${month!=null&&kontrakt!=null}", btnSok, "enabled");
		Binding<DriftsloggQueryParams, Object, JButton, Object> fastprisBind = BindingHelper.createbinding(controller.getQueryParams(), "${month!=null&&kontrakt!=null}", btnFastpris, "enabled");
		Binding<DriftsloggQueryParams, Object, JButton, Object> excelBind = BindingHelper.createbinding(controller.getQueryParams(), "${kontrakt != null}", btnExportToExcel, "enabled");
		
		Binding<DriftsloggQueryParams, Object, JButton, Object> vaerRappBind = BindingHelper.createbinding(controller.getQueryParams(), "${kontrakt != null}", btnVaerRapport, "enabled");
		Binding<DriftsloggQueryParams, Object, JPopupListMenu, Object> amountRappBind = BindingHelper.createbinding(controller.getQueryParams(), "${kontrakt != null}", btnRapport, "enabled");
		Binding<DriftsloggQueryParams, Object, JPopupListMenu, Object> amountOperationClassAmountvaerRappBind = BindingHelper.createbinding(controller.getQueryParams(), "${kontrakt != null}", btnAmountReportOperationClass, "enabled");
		Binding<DriftsloggQueryParams, Object, JButton, Object> ioBind = BindingHelper.createbinding(controller.getQueryParams(), "${kontrakt != null}", btnIO, "enabled");

        controller.getBindings().addBinding(mndBind);
        controller.getBindings().addBinding(sokBind);
		controller.getBindings().addBinding(fastprisBind);
		controller.getBindings().addBinding(excelBind);
		controller.getBindings().addBinding(amountRappBind);
		controller.getBindings().addBinding(amountOperationClassAmountvaerRappBind);
        controller.getBindings().addBinding(ioBind);

		controller.getBindings().addBinding(vaerRappBind);
	}
	
	public void setMonth(Date month) {
		if (month == null)
			return;

		Calendar c = Calendar.getInstance();
		c.setTime(month);
		String monthStr = c.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("NO", "no"));

		btnIO.setText(Resources.getResource("button.io"));
		btnFastpris.setText(Resources.getResource("button.fastprissjekk", monthStr));
		mndrapp.setText(Resources.getResource("button.mengdeRapportMnd", monthStr));
		deviantReport.setText(Resources.getResource("button.deviantReport", monthStr));
		amountReport.setText(Resources.getResource("button.amountReport", monthStr));
		outsideTripReport.setText(Resources.getResource("button.outsideTripReport", monthStr));
		exportMonth.setText(Resources.getResource("button.exportToExcelMonth", monthStr));
		controller.getTripTable().getSelectionModel().clearSelection();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.trips")));

		add(createButtonPanelNorth(), new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,0), 0, 0));
		add(warningLabel,       	  new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		add(treeTableScroll,		  new GridBagConstraints(0,2, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, 		new Insets(0,0,0,0), 0, 0));
		add(createButtonPanelSouth(), new GridBagConstraints(0,3, 1,1, 1.0,0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
	}

	private JPanel createButtonPanelNorth() {
		JPanel panel = new JPanel(new GridBagLayout());

        panel.add(btnSok, 			new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0, 0));
		panel.add(btnChangePeriodStatus, 	new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0, 0));
        panel.add(btnFastpris,		new GridBagConstraints(2,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,5), 0, 0));
        panel.add(btnExportToExcel,	new GridBagConstraints(3,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5,0,5,0), 0, 0));

		return panel;
	}

	private JPanel createButtonPanelSouth() {
		JPanel panel = new JPanel(new GridBagLayout());

        panel.add(createButtonPanelSouthWest(), new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST,	   GridBagConstraints.NONE, new Insets(5,0,5,0), 0, 0));
        panel.add(createButtonPanelSouthEast(), new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.EAST,	   GridBagConstraints.NONE, new Insets(5,5,5,0), 0, 0));
        panel.add(lblIoInfo, 					new GridBagConstraints(0,1, 2,1, 1.0,0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,0,5,0), 0, 0));

		return panel;
	}

	private JPanel createButtonPanelSouthWest() {
		JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));

        panel.add(btnVaerRapport);
        panel.add(createSpacer());
        panel.add(btnRapport);
        panel.add(createSpacer());
        panel.add(btnAmountReportOperationClass);

		return panel;
	}

	private JPanel createButtonPanelSouthEast() {
		JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));

        panel.add(btnIO);

		return panel;
	}

	private Component createSpacer() {
		return Box.createRigidArea(new Dimension(5, 0));
	}

	public void showWarning(String warning) {
		warningLabel.setText(warning);
		warningLabel.setVisible(true);
		repaint();
	}

	public void removeWarning() {
		warningLabel.setVisible(false);
		repaint();
	}

	private class FetchTripsWorker extends SwingWorker<Void, Void> {

		Map<Long, List<Trip>> periodIdTrips;
		private List<TripTreeView> selectedEntities;
		private LoadingDialog dialog;

		FetchTripsWorker(List<TripTreeView> selectedEntities, LoadingDialog dialog) {
			this.selectedEntities = selectedEntities;
			this.dialog = dialog;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected Void doInBackground() {
			periodIdTrips = fetchTrips();
			return null;
		}

		private Map<Long, List<Trip>> fetchTrips() {
			Map<Long, List<Trip>> trips = new HashMap<>();

			for (TripTreeView ttv : selectedEntities) {
				Trip t = ttv.getTrip();

				if (t.getId() != null) {
					trips.computeIfAbsent(t.getPeriod().getId(), controller::getTripsForPeriodId);
				}
			}

			return trips;
		}

		@Override
		protected void done() {
			btnChangePeriodStatus.setEnabled(true);
			dialog.close();
			if (otherTripsMeetClosingRequirements(periodIdTrips)) {
                controller.getMipsstallTableModel().load(null);
				controller.setReloadTable(true);
			}
		}

	}

	private class LoadingDialog extends JDialog {

		LoadingDialog(JFrame parent, String title) {
			super(parent, title, true);

			JXBusyLabel busy = new JXBusyLabel();
			busy.setBusy(true);

			setLayout(new GridBagLayout());
			getContentPane().add(busy, new GridBagConstraints());
			setPreferredSize(new Dimension(300, 100));
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);

			new FetchTripsWorker(controller.getTripTable().getSelectedEntities(), this).execute();

			pack();
			setLocationRelativeTo(parent);
			setVisible(true);
		}

		void close() {
			setVisible(false);
			dispose();
		}

	}

	private Action getPeriodStatusAction() {
		return new AbstractAction(openText, openIcon) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (conflictingPeriodStatus) {
					showConflictingPeriodStatusMessage();
				} else {
					switch (periodStatus) {
						case OPEN:
							btnChangePeriodStatus.setEnabled(false);
							new LoadingDialog((JFrame) getRootPane().getParent(), Resources.getResource("loadingTrips.title"));
							break;
						case CLOSED:
                            controller.getMipsstallTableModel().load(null);
							openPeriodsForSelectedTrips();
							controller.setReloadTable(true);
							break;
					}
				}
			}
		};
	}

	private boolean otherTripsMeetClosingRequirements(Map<Long, List<Trip>> periodIdTrips) {
		List<TripTreeView> selectedEntities = controller.getTripTable().getSelectedEntities();

		if (selectedEntities.size() > 0 && periodIdTrips.size() > 0) {
			List<ClosingPeriodTableRow> periods = new ArrayList<>();

			boolean periodHasOptionalTrips = false;
			boolean periodHasRequiedTrips = false;

			for (TripTreeView ttv : selectedEntities) {
				Trip t = ttv.getTrip();

				if (t.getId() != null && !periods.contains(new ClosingPeriodTableRow(t.getPeriod(), 0))) {
					List<Trip> trips = periodIdTrips.get(t.getPeriod().getId());

					for (Trip trip : trips) {
						int tripStatusLevel = trip.getProcessStatus().getLevel();

						if (tripStatusLevel < TripProcessStatus.SUBMITTED_BY_SUBCONTRACTOR.getLevel()) {
							periodHasOptionalTrips = true;
						} else if (tripStatusLevel < TripProcessStatus.PROCESSED_BY_CONTRACTOR.getLevel()) {
							periodHasRequiedTrips = true;
							break;
						}
					}

					periods.add(new ClosingPeriodTableRow(t.getPeriod(), trips.size()));
				}

				if (periodHasRequiedTrips) {
					break;
				}
			}

			if (periodHasRequiedTrips) {
				JOptionPane.showMessageDialog(
						getParent(),
						Resources.getResource("message.cannotClosePeriods.message"),
						Resources.getResource("message.closePeriods.title"),
						JOptionPane.INFORMATION_MESSAGE);
				return false;
			} else if (periodHasOptionalTrips) {
				int choice = JOptionPane.showConfirmDialog(
						getParent(),
						Resources.getResource("message.canClosePeriodsIfUserApproves.message"),
						Resources.getResource("message.closePeriods.title"),
						JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
				return choice == JOptionPane.YES_OPTION && closePeriods(periods);
			} else {
				return closePeriods(periods);
			}
		}

		return false;
	}

	private class ClosingPeriodTableRow {

		private Periode period;
		private int numberOfTrips;

		public ClosingPeriodTableRow(Periode period, int numberOfTrips) {
			this.period = period;
			this.numberOfTrips = numberOfTrips;
		}

		public Periode getPeriod() {
			return period;
		}

		public int getNumberOfTrips() {
			return numberOfTrips;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			ClosingPeriodTableRow that = (ClosingPeriodTableRow) o;

			return period.equals(that.period);
		}

		@Override
		public int hashCode() {
			return period.hashCode();
		}

	}

	private boolean closePeriods(List<ClosingPeriodTableRow> tableRow) {
		List<Periode> periods = new ArrayList<>();

		StringBuilder message = new StringBuilder();
		message.append(Resources.getResource("message.closePeriods.message.tableHeader"));

		SimpleDateFormat sdf = new SimpleDateFormat("MMMM");

		for (ClosingPeriodTableRow row : tableRow) {
			Periode p = row.getPeriod();

			String vendorContract = p.getLevkontraktIdent();
			String vendor = p.getLevkontrakt().getLeverandor().getNavn();

			message.append("<tr>");
			message.append("<td>").append(sdf.format(p.getTilDato())).append("</td>");
			message.append("<td>").append(vendorContract).append("</td>");
			message.append("<td>").append(vendor).append("</td>");
			message.append("<td>").append(row.getNumberOfTrips()).append("</td>");
			message.append("</tr>");

			periods.add(p);
		}

		message.append(Resources.getResource("message.closePeriods.message.tableFooter"));

		int choice = JOptionPane.showConfirmDialog(
				getParent(),
				message.toString(),
				Resources.getResource("message.closePeriods.title"),
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
		if (choice == JOptionPane.YES_OPTION) {
			controller.closePeriods(periods);
			return true;
		}

		return false;
	}

	private void openPeriodsForSelectedTrips() {
		List<TripTreeView> selectedEntities = controller.getTripTable().getSelectedEntities();

		if (selectedEntities.size() > 0) {
			List<Periode> periods = new ArrayList<>();

			for (TripTreeView ttv : selectedEntities) {
				Trip t = ttv.getTrip();

				if (t.getId() != null && !periods.contains(t.getPeriod())) {
					periods.add(t.getPeriod());
				}
			}

			controller.openPeriods(periods);
		}
	}

	private void setPeriodStatusUI() {
	    if (periodStatus != null) {
            switch (periodStatus) {
                case CLOSED:
                    btnChangePeriodStatus.setText(openText);
                    btnChangePeriodStatus.setIcon(openIcon);
                    btnChangePeriodStatus.setToolTipText(openToolTip);
                    break;
                case OPEN:
                    btnChangePeriodStatus.setText(closeText);
                    btnChangePeriodStatus.setIcon(closeIcon);
                    btnChangePeriodStatus.setToolTipText(closeToolTip);
                    break;
                case SENT_IO:
                    btnChangePeriodStatus.setEnabled(false);
                    break;
            }
        } else {
            btnChangePeriodStatus.setEnabled(false);
        }
	}

    private void getPeriodStatusListener(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            List<TripTreeView> selectedEntities = controller.getTripTable().getSelectedEntities();

			conflictingPeriodStatus = false;
			btnChangePeriodStatus.setEnabled(selectedEntities.size() > 0);

            int openCounter = 0;
            int closedCounter = 0;

            for (TripTreeView ttv : selectedEntities) {
                Trip trip = ttv.getTrip();

                if (trip.getId() != null) {
                    switch (trip.getPeriod().getStatus().getStatusType()) {
                        case OPEN:
                            openCounter++;
                            break;
                        case CLOSED:
                            closedCounter++;
                            break;
                        case SENT_IO:
                            periodStatus = PeriodeStatus.StatusType.SENT_IO;
                            setPeriodStatusUI();
                            return;
                    }
                } else {
                    periodStatus = null;
                    setPeriodStatusUI();
                    return;
                }
            }

            if (openCounter > 0 && closedCounter == 0) {
				periodStatus = PeriodeStatus.StatusType.OPEN;
			} else if (closedCounter > 0 && openCounter == 0) {
				periodStatus = PeriodeStatus.StatusType.CLOSED;
			} else {
				conflictingPeriodStatus = true;
			}

			setPeriodStatusUI();
        }
    }

    private void showConflictingPeriodStatusMessage() {
		JOptionPane.showMessageDialog(
				this.getParent(),
				Resources.getResource("message.conflictingPeriodStatus.message"),
				Resources.getResource("message.conflictingPeriodStatus.title"),
				JOptionPane.INFORMATION_MESSAGE);
	}

}