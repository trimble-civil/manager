package no.mesta.mipss.driftslogg.ui.periode.rapport;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.driftslogg.DriftsloggQueryParams;
import no.mesta.mipss.driftslogg.rapport.ApprovedAmountData;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.produksjonsrapport.excel.RapportHelper;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ApprovedAmountsReport {

    private static final String RAPPORT_TEMPLATE = "mengderapport";
    private File file;
    private XSSFWorkbook workbook;

    public ApprovedAmountsReport(DriftsloggQueryParams queryParams, List<ApprovedAmountData> data, Date fraDato, Date tilDato) {
        String periodeText = MipssDateFormatter.formatDate(fraDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT) +
                " - " + MipssDateFormatter.formatDate(tilDato, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
        String rappDatoText = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);

        HashMap<String, Object> map = new HashMap<>();
        map.put("data", data);
        map.put("kontrakt", queryParams.getKontrakt());
        map.put("periode", periodeText);
        map.put("dato", rappDatoText);

        file = RapportHelper.createTempFile(RAPPORT_TEMPLATE);
        workbook = RapportHelper.readTemplate(RAPPORT_TEMPLATE, map);
    }

    /**
     * Lukker arbeidsboken og åpner den i Excel
     */
    public void finalizeAndOpen(){
        RapportHelper.finalizeAndOpen(workbook, file);
    }

}