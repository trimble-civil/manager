package no.mesta.mipss.driftslogg.util;

import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.persistence.Clock;

import java.util.Calendar;
import java.util.Date;

public class AgressoUtil {

	public static final Date getOverforingDato(Date now){
		String hourMinutesStr = KonfigparamPreferences.getInstance().hentEnForApp(
				"studio.mipss.driftslogg", "operationLogTransferTime").getVerdi();

		String[] timestamp = hourMinutesStr.split(":");

		int hour;
		int minute;

		try {
			hour = Integer.parseInt(timestamp[0]);
			minute = Integer.parseInt(timestamp[1]);
		} catch (NumberFormatException e) {
			hour = 12;
			minute = 0;
		}

		Date firstWedInMonth = getFirstWeekDayInMonth(Calendar.WEDNESDAY, hour, minute, now);
		if (firstWedInMonth.after(now)){
			return firstWedInMonth;
		}else{
			Calendar nowCal = Calendar.getInstance();
			nowCal.setTime(now);
			nowCal.add(Calendar.MONTH, 1);
			nowCal.getTime();
			return getFirstWeekDayInMonth(Calendar.WEDNESDAY, hour, minute, nowCal.getTime());
		}
	}
	
	public static Date getFirstWeekDayInMonth(int dayOfWeek, int hourOfDay, int minute, Date now){
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		c.set(Calendar.HOUR_OF_DAY, hourOfDay);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.getTime();
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.getTime();
		
		Calendar nowCal = Calendar.getInstance();
		nowCal.setTime(now);
		if (c.get(Calendar.MONTH)!=nowCal.get(Calendar.MONTH)){
			c.add(Calendar.WEEK_OF_YEAR, 1);
		}
		c.set(Calendar.DAY_OF_WEEK, dayOfWeek);
		return c.getTime();
	}
	public static final Date getAgressoOverforingDato(){
		return getOverforingDato(Clock.now());
//		Date date = CalendarUtil.getNextOccurrenceOfDayOfWeek(Calendar.WEDNESDAY, 8);
//		Calendar c = Calendar.getInstance();
//		c.setTime(date);
//		c.set(Calendar.HOUR_OF_DAY, 8);
//		c.set(Calendar.MINUTE, 0);
//		
//		//agressooverføring skjer første onsdag i første HELE uke i måneden.. dvs: mandag må minst være den 1.
//		//sjekk om hele uken er i samme måned.
//		int month = c.get(Calendar.MONTH);
//		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
//		int monthOnMonday = c.get(Calendar.MONTH);
//		c.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
//		if (month == monthOnMonday)//uken er hel i måneden
//			return c.getTime();
//		
//		
//		c.add(Calendar.WEEK_OF_YEAR, 1);//bruk neste uke
//		return c.getTime();
		
	}
}
