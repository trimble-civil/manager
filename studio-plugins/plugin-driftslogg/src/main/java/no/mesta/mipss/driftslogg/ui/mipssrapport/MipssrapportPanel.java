package no.mesta.mipss.driftslogg.ui.mipssrapport;

import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.ui.JMipssTreeTableScrollPane;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class MipssrapportPanel extends JPanel {

	private final DriftsloggController controller;
	private JMipssTreeTableScrollPane scrTable;
	private JButton btnUkjentStroproduktRapport;
	
	public MipssrapportPanel(DriftsloggController controller){
		this.controller = controller;
		initComponents();
		initGui();
	}

	public MipssrapportPanel(DriftsloggController controller, JMipssTreeTable<MipssrapportTreeView> table, MipssRapportTableModel model) {
		this.controller = controller;
		initComponents(table, model);
		initGui();
	}

	private void initComponents(JMipssTreeTable<MipssrapportTreeView> table, MipssRapportTableModel model) {
		scrTable = new JMipssTreeTableScrollPane(table, model);
		scrTable.setPreferredSize(new Dimension(200,100));
		btnUkjentStroproduktRapport = new JButton(controller.getUkjentStroproduktAction()); // TODO: denne må vi håndtere spesielt her
		addTableListener(); // TODO: denne må vi håndtere spesielt her ettersom addTableListener() er knyttet mot controller.getMipsstallTableModel()
	}

	private void initComponents() {
		scrTable = new JMipssTreeTableScrollPane(controller.getMipsstallTable(), controller.getMipsstallTableModel());
		scrTable.setPreferredSize(new Dimension(200,100));
		btnUkjentStroproduktRapport = new JButton(controller.getUkjentStroproduktAction());
		addTableListener();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.loggedAmount")));
		add(scrTable, 						new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		add(btnUkjentStroproduktRapport, 	new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
	}
	
	private void addTableListener(){
		controller.getMipsstallTableModel().addTableLoaderListener(new MipssBeanLoaderListener() {
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {}
			
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				boolean showButton = false;

				int rows = controller.getMipsstallTable().getRowCount();
				
				for (int i = 1; i <= rows; i++) {
					String stroprodukt = (String) controller.getMipsstallTable().getValueAt(i, 3);
					if (stroprodukt != null && stroprodukt.toUpperCase().contains("UKJENT")) {
						showButton = true;
						break;
					}
				}
				
				btnUkjentStroproduktRapport.setVisible(showButton);
			}
		});
	}

}