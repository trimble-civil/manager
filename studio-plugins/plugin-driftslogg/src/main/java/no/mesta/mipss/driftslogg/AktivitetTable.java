package no.mesta.mipss.driftslogg;

import no.mesta.mipss.driftslogg.ui.AktivitetTableModel;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.ui.beantable.JMipssTreeTable;

import java.awt.event.MouseEvent;
import java.util.Map;

public class AktivitetTable extends JMipssTreeTable {

    private final AktivitetTableModel model;

    public AktivitetTable(AktivitetTableModel model, Map tooltips) {
        super(model, tooltips);
        this.model = model;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        int column = this.columnAtPoint(event.getPoint());
        int row = this.rowAtPoint(event.getPoint());
        if(column== AktivitetTableModel.COL_WARNINGS) {
            Aktivitet aktivitet = model.getAktiviteter().get(row);
            return aktivitet.getWarningString();
        }
        return null;
    }
}
