package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.driftslogg.Trip;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TripTreeView implements IRenderableMipssEntity {

	private List<TripTreeView> children = new ArrayList<>();
	private TripTreeView parent;

    private Trip trip;

	TripTreeView(TripTreeView parent){
		this.parent = parent;
	}
	public Long getId() {
		return trip !=null? trip.getId():null;
	}
	
	public TripTreeView getParent(){
		return parent;
	}
	
	public void setTrip(Trip trip){
		this.trip = trip;
	}

	public Trip getTrip(){
		return trip;
	}

	public Date getStartTime(){
		return trip !=null ? trip.getStartTime() : null;
	}

	public Date getEndTime() {
		return trip !=null ? trip.getEndTime() : null;
	}

	public Double getDuration() {
		return trip.getDuration();
	}

	public String getVendor() {
		return (trip.getContractorContract() != null) ? trip.getContractorContract().getLeverandor().getNavn() : "";
	}

	public String getVehicle() {
		return (trip.getVehicle() != null) ? trip.getVehicle().getEksterntNavn() : "";
	}

	public Double getAmount() {
		return trip.getAmount();
	}

	public Double getHoursTotal() {
		return trip.getHoursTotal();
	}

	public Double getPlowingTotal() {
		return trip.getPlowingTotalKm();
	}

	public Double getPlaningTotal() {
		return trip.getPlaningTotalKm();
	}

	public Double getHeavyPlaningTotal() {
		return trip.getHeavyPlaningTotalKm();
	}

	public Double getSidePloughTotal() {
		return trip.getSidePloughTotalKm();
	}

	public Double getDryGrittingTotal() {
		return trip.getDryGrittingTotalKg();
	}

	public Double getWetGrittingTotal() {
		return trip.getWetGrittingTotalLiter();
	}
	
	public List<TripTreeView> getChildren(){
		return children;
	}
	
	boolean isLeaf(){
		return children.isEmpty();
	}

	@Override
	public String getTextForGUI() {
		return null;
	}

}