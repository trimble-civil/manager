package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.common.TripProcessStatus;
import no.mesta.mipss.driftslogg.DriftsloggController;
import no.mesta.mipss.persistence.driftslogg.Stroproduktmengde;
import no.mesta.mipss.persistence.driftslogg.Trip;
import no.mesta.mipss.ui.table.AbstractMipssTreeTableModel;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TripTableModel extends AbstractMipssTreeTableModel {

	public static final int COL_PERIOD_STATUS				= 0;
	public static final int COL_ID 							= 1;
	public static final int COL_PLATFORM 					= 2;
	public static final int COL_PROCESS_STATUS 				= 3;
	public static final int COL_WARNINGS					= 4;
	public static final int COL_PROCESS_TRIP 				= 5;
	public static final int COL_START_TIME	 				= 6;
	public static final int COL_END_TIME	 				= 7;
	public static final int COL_DURATION	 				= 8;
	public static final int COL_VENDOR	 					= 9;
	public static final int COL_VEHICLE						= 10;
	public static final int COL_AMOUNT 						= 11;
	public static final int COL_HOURS_TOTAL					= 12;
	public static final int COL_PLOWING_TOTAL				= 13;
	public static final int COL_PLANING_TOTAL				= 14;
	public static final int COL_HEAVY_PLANING_TOTAL			= 15;
	public static final int COL_SIDE_PLOUGH_TOTAL			= 16;
	public static final int COL_DRY_GRITTING_TOTAL			= 17;
	public static final int COL_WET_GRITTING_TOTAL          = 18;

	private TripTreeView myroot = new TripTreeView(null);


	private List<Trip> trips;
	private List<Stroproduktmengde> mengder;

	private boolean editable=true;
	private DecimalFormat format;
	private final DriftsloggController driftsloggController;

	public TripTableModel(DriftsloggController driftsloggController){
		this.driftsloggController = driftsloggController;
		
		DecimalFormatSymbols formatSymbols = DecimalFormatSymbols.getInstance();
		formatSymbols.setDecimalSeparator(',');
		format = new DecimalFormat("#.###", formatSymbols);
		
	}
	
	public void setEditable(boolean editable){
		this.editable = editable;
	}
	public void loadData(DriftsloggController controller){
		startLoading(controller);
	}
	
	@Override
	protected void load(Object beanSourceController, SwingWorker currentWorker) {
		
		DriftsloggController controller = (DriftsloggController)beanSourceController;
		List<Trip> a = controller.getTrips(controller.getQueryParams());
		if (!currentWorker.isCancelled()){
			load(a);
		}
		
	}
	
	public void load(List<Trip> trips){
		this.trips = trips;
		mengder = new ArrayList<>();
		
		Object[] children = myroot.getChildren().toArray();
		TreePath p = new TreePath(myroot);
		int[] idx = new int[children.length];
		for (int i=0;i<children.length;i++){
			idx[i]=i;
		}
		myroot.getChildren().clear();
		modelSupport.fireChildrenRemoved(p, idx, children);
		
		for (Trip t : trips){
			TripTreeView tt = new TripTreeView(myroot);
			tt.setTrip(t);
			// TODO: is this needed?
			/*if (a.getStroprodukter()!=null&&!a.getStroprodukter().isEmpty()){
				at.setStroproduktmengde(a.getStroprodukter().get(0));
				mengder.add(a.getStroprodukter().get(0));
				for (int i=1;i<a.getStroprodukter().size();i++){
					Stroproduktmengde stroproduktmengde = a.getStroprodukter().get(i);
					mengder.add(stroproduktmengde);
					AktivitetTreeView st = new AktivitetTreeView(at);
					st.setStroproduktmengde(stroproduktmengde);
					at.getChildren().add(st);
				}
			}*/
			myroot.getChildren().add(tt);
			modelSupport.fireChildAdded(new TreePath(myroot), myroot.getChildren().size()-1, tt);
		}
	}
	
	public void fireTreeChanged(){
		load(trips);
	}
	
	public List<Trip> getTrips(){
		return trips;
	}

	public List<Stroproduktmengde> getStroproduktmengder(){
		return mengder;
	}

	@Override
	public Object getRoot(){
		return myroot;
	}
	
	@Override
	public int getColumnCount() {
		return COL_WET_GRITTING_TOTAL;
	}

	@Override
	public Object getValueAt(Object node, int column) {
		TripTreeView t = (TripTreeView) node;

		switch(column) {
			case COL_PERIOD_STATUS: return t.getTrip();
			case COL_ID: return t.getId();
			case COL_PLATFORM: return t.getTrip();
			case COL_PROCESS_STATUS: return t.getTrip().getProcessStatus();
			case COL_WARNINGS: return t.getTrip();
			case COL_PROCESS_TRIP: return t.getTrip();
			case COL_START_TIME: return t.getStartTime();
			case COL_END_TIME: return t.getEndTime();
			case COL_DURATION: return t.getDuration();
			case COL_VENDOR: return t.getVendor();
			case COL_VEHICLE: return t.getVehicle();
			case COL_AMOUNT: return t.getAmount();
			case COL_HOURS_TOTAL: return t.getHoursTotal();
			case COL_PLOWING_TOTAL: return t.getPlowingTotal();
			case COL_PLANING_TOTAL: return t.getPlaningTotal();
			case COL_HEAVY_PLANING_TOTAL: return t.getHeavyPlaningTotal();
			case COL_SIDE_PLOUGH_TOTAL: return t.getSidePloughTotal();
			case COL_DRY_GRITTING_TOTAL: return t.getDryGrittingTotal();
			case COL_WET_GRITTING_TOTAL: return t.getWetGrittingTotal();
		}

		return "<UKJENT KOLONNE>";
	}
	
	/**
     * {@inheritDoc}
     */
    public Class<?> getColumnClass(int column) {
    	switch(column) {
			case COL_PERIOD_STATUS: return Trip.class;
			case COL_ID: return Long.class;
			case COL_PLATFORM: return Trip.class;
			case COL_PROCESS_STATUS: return TripProcessStatus.class;
			case COL_WARNINGS: return Trip.class;
			case COL_PROCESS_TRIP: return Trip.class;
			case COL_START_TIME: return Date.class;
			case COL_END_TIME: return Date.class;
			case COL_DURATION: return Double.class;
			case COL_VENDOR: return String.class;
			case COL_VEHICLE: return String.class;
			case COL_AMOUNT: return Double.class;
			case COL_HOURS_TOTAL: return Double.class;
			case COL_PLOWING_TOTAL: return Double.class;
			case COL_PLANING_TOTAL: return Double.class;
			case COL_HEAVY_PLANING_TOTAL: return Double.class;
			case COL_SIDE_PLOUGH_TOTAL: return Double.class;
			case COL_DRY_GRITTING_TOTAL: return Double.class;
			case COL_WET_GRITTING_TOTAL: return Double.class;
			default: return Object.class;
    	}
    }

	private Boolean getBoolean(Object value) {
		if(value instanceof Boolean) {
			return (Boolean)value;
		}
		throw new IllegalArgumentException("Expected a boolean value");
	}
	
	private Double getDouble(Object value){
		if (value==null||"".equals(value))
			return null;
		try{
			String valueS = value.toString();
			if (valueS.contains(".")){
				valueS = valueS.replace('.', ',');
			}
			Double parsed = format.parse(valueS).doubleValue();
			return parsed;
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
    public boolean isCellEditable(Object node, int column) {
		return column == COL_PROCESS_TRIP;
    }

	@Override
	public Object getChild(Object parent, int index) {
		TripTreeView node =(TripTreeView) parent;
		return node.getChildren().get(index);
	}

	@Override
	public int getChildCount(Object parent) {
		TripTreeView node =(TripTreeView) parent;
		return node.getChildren().size();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		TripTreeView node =(TripTreeView) parent;
		for (int i=0;i<node.getChildren().size();i++){
			if (node.getChildren().get(i)==child){
				return i;
			}
		}
		return 0;
	}
	
	public boolean isLeaf( Object node ){
		TripTreeView view =(TripTreeView) node;
		return view.isLeaf();
	}

}