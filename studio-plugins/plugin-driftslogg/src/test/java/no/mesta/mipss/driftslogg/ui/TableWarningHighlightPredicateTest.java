package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static no.mesta.mipss.driftslogg.ui.AktivitetTableModel.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TableWarningHighlightPredicateTest {

    private TableWarningHighlightPredicate predicate;

    @Before
    public void setUp() throws Exception {
        AktivitetTableModel model = mock(AktivitetTableModel.class);
        List<Aktivitet> aktiviteter = new ArrayList<>();
        aktiviteter.add(getAktivitet(true, false));
        aktiviteter.add(getAktivitet(false, false));
        aktiviteter.add(getAktivitet(true, true));

        when(model.getAktiviteter()).thenReturn(aktiviteter);

        predicate = new TableWarningHighlightPredicate(model);
    }

    private Aktivitet getAktivitet(boolean containsWarnings, boolean manuallyCreated) {
        Aktivitet aktivitet = mock(Aktivitet.class);
        when(aktivitet.containsWarnings()).thenReturn(containsWarnings);
        when(aktivitet.isManuallyCreated()).thenReturn(manuallyCreated);
        return aktivitet;
    }

    @Test
    public void willHighlightAktivitetApproveIntentionWithWarning() throws Exception {
        boolean actual = predicate.isHighlighted(0, COL_APPROVED_INTENTION);
        assertEquals(true, actual);
    }

    @Test
    public void willHighlightAktivitetApproveUEWithWarning() throws Exception {
        boolean actual = predicate.isHighlighted(0, COL_APPROVED_UE);
        assertEquals(true, actual);
    }

    @Test
    public void willHighlightAktivitetApproveBuildWithWarning() throws Exception {
        boolean actual = predicate.isHighlighted(0, COL_APPROVED_BUILD);
        assertEquals(true, actual);
    }

    @Test
    public void willNotHighlightAktivitetProductionTypeWithWarning() throws Exception {
        boolean actual = predicate.isHighlighted(0, COL_PRODUCTION_TYPE);
        assertEquals(false, actual);
    }

    @Test
    public void willNotHighlightAktivitetApproveUEWithoutWarning() throws Exception {
        boolean actual = predicate.isHighlighted(1, COL_APPROVED_UE);
        assertEquals(false, actual);
    }

    @Test
    public void willHighlightProductionTypeWhenManuallyCreated() throws Exception {
        boolean actual = predicate.isHighlighted(2, COL_PRODUCTION_TYPE);
        assertEquals(true, actual);

    }
}