package no.mesta.mipss.driftslogg.util;

import no.mesta.mipss.driftslogg.ui.TableDirtyActivityHighlightPredicate;
import no.mesta.mipss.persistence.driftslogg.*;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ActivityUtilTest {

    private ActivityUtil util;
    private Stroprodukt salineSolution;
    private Prodtype ploughing;
    private Prodtype gritting;
    private Prodtype idling;

    @Before
    public void setUp() throws Exception {
        ploughing = new Prodtype(1L, "Brøyting", true);
        gritting = new Prodtype(2L, "Strøing", true);
        idling = new Prodtype(98L, "Tomkjøring", false);

        List<Prodtype> prodtypes = new ArrayList<>();
        prodtypes.add(ploughing);
        prodtypes.add(gritting);
        prodtypes.add(idling);

        List<Stroprodukt> grittingProducts = new ArrayList<>();
        salineSolution = new Stroprodukt(1L, "Saltløsning", 1L);
        grittingProducts.add(salineSolution);

        List<KjoretoyRode> vehicleRodeConnections = new ArrayList<>();
        vehicleRodeConnections.add(getKjoretoyRode(1L));
        vehicleRodeConnections.add(getKjoretoyRode(2L));
        vehicleRodeConnections.add(getKjoretoyRode(3L));

        util = new ActivityUtil(prodtypes);
        util.setVehicleRodeConnections(vehicleRodeConnections);
    }

    @Test
    public void plougingOnPloughingRodeShouldHaveApprovedBuild() throws Exception {
        Aktivitet activity = getActivity(1L, 1L, "Brøyting", false, false, false, false);

        assertTrue(util.shouldHaveApprovedBuild(activity));
    }

    @Test
    public void ploughingOutsidePloughingRodeShouldNotHaveApprovedBuild() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Brøyting", false, false, true, false);

        assertFalse(util.shouldHaveApprovedBuild(activity));
    }

    @Test
    public void idlingShouldNotRequireComment() throws Exception {
        Aktivitet activity = getActivity(1L, 1L, "Tomkjøring", false, false, false, true);

        assertFalse(util.needsComment(activity));
    }

    @Test
    public void handlesNullArticle() throws Exception {
        Aktivitet activity = getActivity(1L, 1L, "Tomkjøring");

        util.shouldHaveApprovedBuild(activity);
    }

    @Test
    public void handlesNullVehicle() throws Exception {
        Aktivitet activity = getActivity(null, 1L, "Tomkjøring");

        util.shouldHaveApprovedBuild(activity);
    }

    @Test
    public void handlesNullRode() throws Exception {
        Aktivitet activity = getActivity(1L, null, "Tomkjøring");

        util.shouldHaveApprovedBuild(activity);
    }

    @Test
    public void checksApprovedBuildFlagVersusExpectedValue() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Brøyting", false, false, true, false);
        activity.setApprovedBuild(true);

        assertFalse(util.shouldHaveApprovedBuild(activity));
        assertTrue(util.needsComment(activity));
    }

    @Test
    public void approveBuildShouldBeFalseForManuallyCreatedRows() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Brøyting", true, false, false, false);

        assertFalse(util.shouldHaveApprovedBuild(activity));
    }

    @Test
    public void shouldNotRequireCommentWhenNoValues() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Brøyting", true, false, false, false);
        activity.setR12_broyting(0.0);
        activity.setR12_sideplog(0.0);
        activity.setR12_hovling(0.0);
        activity.setApprovedBuild(true);

        assertFalse(util.needsComment(activity));
    }

    @Test
    public void ploughingCountsAsValues() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Strøing", true, false, false, false);
        activity.setR12_sideplog(0.0);
        activity.setR12_hovling(0.0);

        assertTrue(util.containsValues(activity));
    }

    @Test
    public void SideploughCountsAsValues() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Strøing", true, false, false, false);
        activity.setR12_broyting(0.0);
        activity.setR12_hovling(0.0);

        assertTrue(util.containsValues(activity));
    }

    @Test
    public void PlaningCountsAsValues() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Strøing", true, false, false, false);
        activity.setR12_broyting(0.0);
        activity.setR12_sideplog(0.0);

        assertTrue(util.containsValues(activity));
    }

    @Test
    public void grittingCountsAsValues() throws Exception {
        Aktivitet activity = getActivity(1L, 2L, "Strøing", true, false, false, false);
        activity.setR12_broyting(0.0);
        activity.setR12_sideplog(0.0);
        activity.setR12_hovling(0.0);
        double wet = 1;
        double dry = 1;
        addGrittingAmount(activity, getGrittingAmount(salineSolution, wet, dry));
        activity.setApprovedBuild(true);

        assertTrue(util.containsValues(activity));
    }

    @Test
    public void canParsePloughing() throws Exception {
        List<Prodtype> actual = util.parseProdTypeString(ploughing.getNavn());

        assertEquals(1, actual.size());
        assertEquals(ploughing, actual.get(0));
    }

    @Test
    public void canParseGritting() throws Exception {
        List<Prodtype> actual = util.parseProdTypeString(gritting.getNavn()+" : "+salineSolution.getNavn());

        assertEquals(1, actual.size());
        assertEquals(gritting, actual.get(0));
    }

    @Test
    public void canParseCoProduction() throws Exception {
        List<Prodtype> actual = util.parseProdTypeString(ploughing.getNavn()+"+"+gritting.getNavn()+ " : "+salineSolution.getNavn());

        assertEquals(2, actual.size());
        assertEquals(ploughing, actual.get(0));
        assertEquals(gritting, actual.get(1));
    }

    @Test
    public void nonPlowingProductionOnConnectedRode() {
        Aktivitet activity = getActivity(1L, 3L, "Tomkjøring", false, false, false, true);
        boolean actual = util.isProductionOutsideConnectedRode(activity);
        assertFalse(actual);
    }

    @Test
    public void nonPlowingProductionOutsideOfConnectedRode() {
        Aktivitet activity = getActivity(1L, 4L, "Strøing", false, false, false, true);
        boolean actual = util.isProductionOutsideConnectedRode(activity);
        assertFalse(actual);
    }

    @Test
    public void plowingOutsideOfConnectedRode() {
        Aktivitet activity = getActivity(1L, 4L, "Brøyting", false, false, false, true);
        boolean actual = util.isProductionOutsideConnectedRode(activity);
        assertTrue(actual);
    }

    @Test
    public void plowingOnConnectedRode() {
        Aktivitet activity = getActivity(1L, 3L, "Brøyting", false, false, false, true);
        boolean actual = util.isProductionOutsideConnectedRode(activity);
        assertFalse(actual);
    }

    private Stroproduktmengde getGrittingAmount(Stroprodukt grittingProduct, double wet, double dry) {
        Stroproduktmengde stroproduktmengde = new Stroproduktmengde();
        stroproduktmengde.setStroprodukt(grittingProduct);
        stroproduktmengde.setOmforentMengdeVaatt(wet);
        stroproduktmengde.setOmforentMengdeTort(dry);

        return stroproduktmengde;
    }

    private Aktivitet getActivity(Long kjoretoyId, Long rodeId, String productionType) {
        return getActivity(kjoretoyId, rodeId, productionType, false, false, false, false);
    }

    private Aktivitet getActivity(Long kjoretoyId, Long rodeId, String productionType, boolean manuallyCreated, boolean productionOutsideContract, boolean productionOutsideRode, boolean noPayment) {
        Aktivitet activity = new Aktivitet(kjoretoyId, rodeId, productionType, manuallyCreated, productionOutsideContract, productionOutsideRode, noPayment);
        activity.setR12_broyting(10.0);
        activity.setR12_hovling(10.0);
        activity.setR12_sideplog(10.0);
        setStatus(activity, PeriodeStatus.StatusType.OPEN);
        return activity;
    }

    private void setStatus(Aktivitet activity, PeriodeStatus.StatusType statusType) {
        Periode periode = mock(Periode.class);
        Trip trip = new Trip();
        trip.setPeriod(periode);
        PeriodeStatus status = new PeriodeStatus();
        status.setStatusId(Long.valueOf(statusType.ordinal())+1);
        when(periode.getStatus()).thenReturn(status);
        activity.setTrip(trip);
    }

    private void addGrittingAmount(Aktivitet activity, Stroproduktmengde... grittingAmounts) {
        activity.setStroprodukter(Arrays.asList(grittingAmounts));
    }

    private KjoretoyRode getKjoretoyRode(Long rodeId) {
        KjoretoyRode kjoretoyRode = mock(KjoretoyRode.class);
        when(kjoretoyRode.getRodeId()).thenReturn(rodeId);
        return kjoretoyRode;
    }

}