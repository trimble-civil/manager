package no.mesta.mipss.driftslogg.ui;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class CellHighlightPredicateTest {

    private ComponentAdapter adapter;
    private CellHighlightPredicate predicate;

    @Before
    public void setUp() throws Exception {
        predicate = new CellHighlightPredicate();
        predicate.addHighlight(1, 1);

        adapter = mock(ComponentAdapter.class);
        adapter.row = 1;
        adapter.column  = 1;
    }

    @Test
    public void canHighlightCell() throws Exception {
        assertTrue(predicate.isHighlighted(null, adapter));
    }

    @Test
    public void dowNotHighlightAnotherCell() throws Exception {
        adapter.column  = 2;

        assertFalse(predicate.isHighlighted(null, adapter));
    }

    @Test
    public void removeHighlightsRemovesPreviousHighlights() throws Exception {
        predicate.removeHighlights();

        assertFalse(predicate.isHighlighted(null, adapter));
    }
}