package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.driftslogg.Trip;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static no.mesta.mipss.driftslogg.ui.AktivitetTableModel.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TripTableHighlightPredicateTest {

    private TripTableHighlightPredicate predicate;

    @Before
    public void setUp() throws Exception {
        AktivitetTableModel model = mock(AktivitetTableModel.class);
        List<Aktivitet> aktiviteter = new ArrayList<>();
        aktiviteter.add(getAktivitet(1, 1));
        aktiviteter.add(getAktivitet(2, 1));
        aktiviteter.add(getAktivitet(3, 2));
        aktiviteter.add(getAktivitet(4, 3));

        when(model.getAktiviteter()).thenReturn(aktiviteter);

        predicate = new TripTableHighlightPredicate(model);
    }

    private Aktivitet getAktivitet(long id, long tripId) {
        Aktivitet aktivitet = new Aktivitet();
        aktivitet.setId(id);
        Trip trip = new Trip();
        trip.setId(tripId);
        aktivitet.setTrip(trip);
        return aktivitet;
    }

    @Test
    public void willHighlightFirstRowOfFirstTrip() throws Exception {
        boolean actual = predicate.isHighlightedRow(0);
        assertEquals(true, actual);
    }

    @Test
    public void willHighlightSecondRowOfFirstTrip() throws Exception {
        boolean actual = predicate.isHighlightedRow(1);
        assertEquals(true, actual);
    }

    @Test
    public void willNotHightlightSecondTrip() throws Exception {
        boolean actual = predicate.isHighlightedRow(2);
        assertEquals(false, actual);
    }

    @Test
    public void willHighlightThirdTrip() throws Exception {
        boolean actual = predicate.isHighlightedRow(3);
        assertEquals(true, actual);
    }

    @Test
    public void willNotHighlightWarningColumn() throws Exception {
        boolean actual = predicate.isHighlightedColumn(COL_WARNINGS);
        assertEquals(false, actual);
    }

    @Test
    public void willHightlightIdColumn() throws Exception {
        boolean actual = predicate.isHighlightedColumn(COL_ID);
        assertEquals(true, actual);
    }

    @Test
    public void willNotHighlightApproveBuildColumn() throws Exception {
        boolean actual = predicate.isHighlightedColumn(COL_APPROVED_BUILD);
        assertEquals(false, actual);
    }

    @Test
    public void willNotHighlightApproveUEColumn() throws Exception {
        boolean actual = predicate.isHighlightedColumn(COL_APPROVED_UE);
        assertEquals(false, actual);
    }

    @Test
    public void willNotHighlightApproveIntentionColumn() throws Exception {
        boolean actual = predicate.isHighlightedColumn(COL_APPROVED_INTENTION);
        assertEquals(false, actual);
    }
}