package no.mesta.mipss.driftslogg.ui;

import no.mesta.mipss.driftslogg.util.ActivityUtil;
import no.mesta.mipss.persistence.driftslogg.Aktivitet;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyRode;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TableDrityActivityHighlightPredicateTest {

    private ActivityUtil activityUtil;
    private TableDirtyActivityHighlightPredicate predicate;

    @Before
    public void setUp() throws Exception {
        List<Aktivitet> aktiviteter = new ArrayList<>();
        aktiviteter.add(getAktivitet(true, false, null, null));
        aktiviteter.add(getAktivitet(false, true, null, null));
        aktiviteter.add(getAktivitet(false, false, "Brøyting", 4L));
        aktiviteter.add(getAktivitet(false, false, "Brøyting", 3L));
        aktiviteter.add(getAktivitet(false, false, "Strøing", 3L));

        AktivitetTableModel model = mock(AktivitetTableModel.class);
        when(model.getAktiviteter()).thenReturn(aktiviteter);

        activityUtil = mock(ActivityUtil.class);
        when(activityUtil.needsComment(anyObject())).thenReturn(false);
        when(activityUtil.isProductionOutsideConnectedRode(anyObject())).thenReturn(false);

        predicate = new TableDirtyActivityHighlightPredicate(model, activityUtil);
    }

    private Aktivitet getAktivitet(boolean hasNoPayment, boolean isArticleChanged, String production, Long rodeId) {
        Aktivitet aktivitet = mock(Aktivitet.class);
        when(aktivitet.hasNoPayment()).thenReturn(hasNoPayment);
        when(aktivitet.isArticleChanged()).thenReturn(isArticleChanged);
        when(aktivitet.getProductionType()).thenReturn(production);
        when(aktivitet.getRodeId()).thenReturn(rodeId);
        return aktivitet;
    }

    @Test
    public void isHighlighted_noPaymentTrue_highlighted() {
        assertTrue(predicate.isHighlighted(0));
    }

    @Test
    public void isHighlighted_articleChangedTrue_highlighted() {
        assertTrue(predicate.isHighlighted(1));
    }

    @Test
    public void isHighlighted_productionOnWrongRode_highlighted() {
        when(activityUtil.isProductionOutsideConnectedRode(anyObject())).thenReturn(true);
        assertTrue(predicate.isHighlighted(2));
    }

    @Test
    public void isHighlighted_activityOk_notHighlighted() {
        assertFalse(predicate.isHighlighted(3));
    }

    @Test
    public void isHighlighted_grittingDoesNotTriggerHighlighting_notHighlighted() {
        assertFalse(predicate.isHighlighted(4));
    }

}