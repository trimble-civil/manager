package no.mesta.mipss;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftslogg.util.AgressoUtil;
import no.mesta.mipss.persistence.Clock;
import org.junit.Ignore;

@Ignore
public class TestAgressoUtil extends TestCase {

	@Ignore
	public void testOverforingDato(){
		Date march13 = MipssDateFormatter.parseDate("20.03.2013");
		Date april1 = MipssDateFormatter.parseDate("01.04.2013 07:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date april = MipssDateFormatter.parseDate("20.04.2013");
		Date mai1 = MipssDateFormatter.parseDate("03.05.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date mai = MipssDateFormatter.parseDate("08.05.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date sept = MipssDateFormatter.parseDate("18.09.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		
		Date overforingApril = AgressoUtil.getOverforingDato(march13);
		Date overforingApril2 = AgressoUtil.getOverforingDato(april1);
		Date overforingMai = AgressoUtil.getOverforingDato(april);
		Date overforingMai1 = AgressoUtil.getOverforingDato(mai1);
		Date overforingJuni = AgressoUtil.getOverforingDato(mai);
		Date overforingOkt = AgressoUtil.getOverforingDato(sept);
		
		
		System.out.println(overforingApril);
		System.out.println(overforingApril2);
		System.out.println(overforingMai);
		System.out.println(overforingMai1);
		System.out.println(overforingJuni);
		System.out.println(overforingOkt);

		Date aprilExpected = MipssDateFormatter.parseDate("03.04.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date maiExpected = MipssDateFormatter.parseDate("08.05.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date mai1Expected = MipssDateFormatter.parseDate("08.05.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date juniExpected = MipssDateFormatter.parseDate("05.06.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		Date oktExpected = MipssDateFormatter.parseDate("09.10.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
		
		assertEquals(aprilExpected, overforingApril);
		assertEquals(aprilExpected, overforingApril2);
		assertEquals(maiExpected, overforingMai);
		assertEquals(mai1Expected, overforingMai1);
		assertEquals(juniExpected, overforingJuni);
		assertEquals(oktExpected, overforingOkt);
		
	}
//	public void testGetFirstWeekDayInMonth(){
//		Date now = MipssDateFormatter.parseDate("04.05.2013 08:00", MipssDateFormatter.DATE_TIME_FORMAT);
//		Date firstWed = CalendarUtil.getFirstWeekDayInMonth(Calendar.MONDAY, now);
//		System.out.println(firstWed);
//	}
}
