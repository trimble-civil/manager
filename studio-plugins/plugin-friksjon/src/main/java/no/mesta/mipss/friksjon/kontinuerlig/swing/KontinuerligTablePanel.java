package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTableCellRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings({"serial", "unchecked"})
public class KontinuerligTablePanel extends JPanel implements MipssBeanLoaderListener{

	private final FriksjonModule plugin;
	private JMipssBeanTable<Prodstrekning> friksjonTable;
	private JScrollPane friksjonScroll = new JScrollPane();
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private MipssBeanTableModel model;
	
	public KontinuerligTablePanel(FriksjonModule plugin){
		this.plugin = plugin;
		createTable();
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		busyLabel.setText(GuiTexts.getText(GuiTexts.HENTER_FRIKSJON));
		friksjonTable.setPreferredScrollableViewportSize(new Dimension(0, 0));
		friksjonScroll.setViewportView(friksjonTable);
		this.add(friksjonScroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

	}
	private void createTable(){
		model = new MipssBeanTableModel(Prodstrekning.class, "fraTidspunkt", "tilTidspunkt", "veitype", "veinummer", "hp", "fraKm", "tilKm", "lengde");
		
//		model.setBeanInstance(BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class));
//		model.setBeanInterface(ProduksjonService.class);
//		model.setBeanMethod("getKontinuerligFriksjonsdetalj");
//		model.setBeanMethodArguments(new Class[]{FriksjonQueryParams.class});
//		model.addTableLoaderListener(this);
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Prodstrekning.class, model.getColumns());
		friksjonTable = new JMipssBeanTable<Prodstrekning>(model, colModel);
		friksjonTable.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		
//		friksjonTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		friksjonTable.setFillsViewportWidth(true);
		friksjonTable.setDoWidthHacks(false);
//		friksjonTable.getColumn(9).setPreferredWidth(40);
//		friksjonTable.getColumn(10).setPreferredWidth(40);
	}
	
	public void setProdstrekninger(List<Prodstrekning> ps){
		model.getEntities().clear();
		model.setEntities(ps);
	}
	
	public void loadData(FriksjonQueryParams params){
		model.setBeanMethodArgValues(new Object[]{params});
		model.loadData();
	}
	

	@Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		busyLabel.setBusy(false);
		friksjonScroll.setViewportView(friksjonTable);
		
	}

	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		busyLabel.setBusy(true);
		friksjonScroll.setViewportView(busyLabel);
		
	}
	public Prodstrekning selectNext(){
		int row = friksjonTable.getSelectedRow();
		if (row<friksjonTable.getRowCount()-1){
			friksjonTable.getSelectionModel().setSelectionInterval(++row, row);
			friksjonTable.scrollRowToVisible(row);
			return getSelectedStrekning();
		}
		return null;
	}
	public Prodstrekning selectPrev(){
		int row = friksjonTable.getSelectedRow();
		if (row>-1){
			friksjonTable.getSelectionModel().setSelectionInterval(--row, row);
			friksjonTable.scrollRowToVisible(row);
			return getSelectedStrekning();
		}
		return null;
	}
	public JMipssBeanTable<Prodstrekning> getFriksjonTable() {
		return friksjonTable;
	}

	public Prodstrekning getSelectedStrekning() {
		List<Prodstrekning> selectedEntities = friksjonTable.getSelectedEntities();
		if (selectedEntities.size()>0){
			return selectedEntities.get(0);
		}
		return null;
	}
	public List<Prodstrekning> getSelectedStrekningList(){
		return friksjonTable.getSelectedEntities();
	}
	
	
}
