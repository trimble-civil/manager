package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.friksjon.FriksjonChartFactory;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonRapportHelper;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.jfree.data.xy.XYDataset;

@SuppressWarnings("serial")
public class FriksjonChartPanel extends JPanel{

	private JTabbedPane tab;
	private JButton nextButton;
	private JButton prevButton;
	
	private final KontinuerligTablePanel tablePanel;
	private final FriksjonModule plugin;
	
	
	private TableChartPanel grunndataPanel;
	private TableChartPanel rapportPanel;
	
	public FriksjonChartPanel(FriksjonModule plugin, KontinuerligTablePanel tablePanel) {
		this.plugin = plugin;
		this.tablePanel = tablePanel;
		initGui();
	}

	private void initGui() {
		tab = new JTabbedPane();
		
		grunndataPanel = new DateTableChartPanel(plugin, FriksjonChartFactory.createFriksjonChart(null, "Friksjon"));
		rapportPanel = new MeterTableChartPanel(plugin, FriksjonChartFactory.createFriksjonChartMeter(null, "Friksjon"));
		if (UserUtils.isAdmin(plugin.getLoader().getLoggedOnUser(true))){
			tab.addTab("Grunndata", grunndataPanel);
		}
		tab.addTab("Rapport", rapportPanel);
		
		nextButton = new JButton(getNextAction());
		prevButton = new JButton(getPrevAction());
		setLayout(new GridBagLayout());
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		
		buttonPanel.add(prevButton, new GridBagConstraints(0,0, 1,1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		buttonPanel.add(nextButton, new GridBagConstraints(1,0, 1,1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(buttonPanel, new GridBagConstraints(0,0, 1,1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		add(tab, 		 new GridBagConstraints(0,1, 1,1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
	}
	
	
	
	public void updateChartData(Prodstrekning ps){
		FriksjonRapportHelper fh = new FriksjonRapportHelper();
		List<FriksjonVO> friksjonList = plugin.getController().getFriksjonForProdstrekning(ps);
		XYDataset ds = plugin.getController().getDatasetForFriksjon(friksjonList);
		XYDataset hastighetDS = plugin.getController().getDatasetForHastighet(friksjonList);
		String title = "Friksjon ("+ps.getVei()+"HP"+ps.getHp()+")";
		grunndataPanel.updateDataset(ds, friksjonList, hastighetDS, title);
		
		if (friksjonList.size()>1){
			List<FriksjonVO> fl = fh.genererStrekninger(friksjonList, 20);
			XYDataset dsx = plugin.getController().getDatasetForFriksjonMeter(fl);
			XYDataset hastighetDs = plugin.getController().getDatasetForHastighetMeter(fl);
			rapportPanel.updateDataset(dsx, fl, hastighetDs, title);
			rapportPanel.setInverted(fh.isInverted(fl));
		}
		else{
			rapportPanel.updateDataset(null, null, null, title);
		}
		
	}
	
	private Action getNextAction(){
		return new AbstractAction("", IconResources.ADD_ONE_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				Prodstrekning ps = tablePanel.selectNext();
				if (ps!=null){
					updateChartData(ps);
				}
			}
		};
	}
	
	private Action getPrevAction(){
		return new AbstractAction("", IconResources.REMOVE_ONE_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				Prodstrekning ps = tablePanel.selectPrev();
				if (ps!=null){
					updateChartData(ps);
				}
			}
		};
	}

	

}
