package no.mesta.mipss.friksjon.kontinuerlig.swing;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;

@SuppressWarnings("serial")
public class MeterTableChartPanel extends TableChartPanel{

	public MeterTableChartPanel(FriksjonModule plugin, JFreeChart chart) {
		super(plugin, chart);
	}

	@Override
	protected void setMarker(double xValue, Double yValue) {
		getMarker().setValue(xValue);
		ensureVisibilityForMeter(xValue, yValue);
		chart.setNotify(true);
	    chart.fireChartChanged();
		
	}
	
	private void ensureVisibilityForMeter(Double meter, Double rangeValue){
		ValueAxis axis = ((XYPlot)chart.getPlot()).getDomainAxis();
		ValueAxis raxis = ((XYPlot)chart.getPlot()).getRangeAxis();
		calculateRange(axis, meter);
		calculateRange(raxis, rangeValue);
	}

	@Override
	protected boolean after(FriksjonVO f, double chartX) {
		if (inverted){
			return f.getMeter()<chartX;
		}
		return f.getMeter()>chartX;
	}

	@Override
	protected void setMarker(FriksjonVO f) {
		setMarker(f.getMeter(), f.getFriksjon());
	}

}
