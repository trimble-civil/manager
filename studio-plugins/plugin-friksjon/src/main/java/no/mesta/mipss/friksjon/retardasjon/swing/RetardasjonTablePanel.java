package no.mesta.mipss.friksjon.retardasjon.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssObservableListListener;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.kvernetf1.Fore;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.persistence.kvernetf1.Nedbor;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssTableComboBoxEditor;
import no.mesta.mipss.ui.table.MipssTableComboBoxRenderer;

import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;

/**
 * Panel som holder på friksjonsdetaljtabellen. 
 * <p>
 * For å hente data til tabellen kalles <code>loadData()</code> med driftkontraktId og fra og til dato, disse datoene er i CET 
 * </p>
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class RetardasjonTablePanel extends JPanel implements PropertyChangeSource{
	private RetardasjonTableModel friksjonTableModel;
	private JScrollPane friksjonScroll = new JScrollPane();
	private JMipssBeanTable friksjonTable;

	public MipssBeanTableModel<Friksjonsdetalj> getFriksjonTableModel() {
		return friksjonTableModel;
	}

	public JMipssBeanTable getFriksjonTable() {
		return friksjonTable;
	}

	private final FriksjonModule plugin;
	
	public RetardasjonTablePanel(FriksjonModule plugin){
		this.plugin = plugin;
		createTable();
		initGui();
	}
	
	private void createTable(){
		int[] editableColumns = new int[]{13, 14, 16};
		friksjonTableModel = new RetardasjonTableModel(editableColumns,
				"dato", "kl", "regnr", "maalertype", 
				"veikategoriStatus", "veinummer", "hp", "km", 
				"retning", "friksjon", "temperatur", 
				"nedbor", "fore", "startTiltak", 
				"fullfortTiltak", "utfortAv", "beskrivelse");
		friksjonTableModel.setBeanInstance(BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class));
		friksjonTableModel.setBeanInterface(ProduksjonService.class);
		friksjonTableModel.setBeanMethod("getFriksjonsdetalj");
		friksjonTableModel.setBeanMethodArguments(new Class[]{Long.class, Date.class, Date.class});//driftkontrakt, fradato, tildato
		friksjonTableModel.addTableLoaderListener(new FriksjonLoaderListener(friksjonScroll));
		
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Friksjonsdetalj.class, friksjonTableModel.getColumns());
		friksjonTable = new JMipssBeanTable(friksjonTableModel, colModel);
		
		MipssObservableList<Friksjonsdetalj> observableList = new MipssObservableList<Friksjonsdetalj>(friksjonTableModel.getEntities(), true);
		FriksjonTableChangeListener changeListener = new FriksjonTableChangeListener();
		observableList.addObservableListListener(changeListener);
		friksjonTableModel.setEntities(observableList);
		
		friksjonTable.setDefaultRenderer(Fore.class, new MipssTableComboBoxRenderer());
		friksjonTable.setDefaultRenderer(Nedbor.class, new MipssTableComboBoxRenderer());
		friksjonTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		friksjonTable.setDefaultEditor(Fore.class, new MipssTableComboBoxEditor(plugin.getForeList()));
		friksjonTable.setDefaultEditor(Nedbor.class, new MipssTableComboBoxEditor(plugin.getNedborList()));
		friksjonTable.setFillsViewportWidth(true);
		friksjonTable.setDoWidthHacks(false);
		friksjonTable.setRowHeightEnabled(true);
		friksjonTable.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnAdded(TableColumnModelEvent e) {}
            public void columnRemoved(TableColumnModelEvent e) {}
            public void columnMoved(TableColumnModelEvent e) {}
            public void columnSelectionChanged(ListSelectionEvent e) {}
            public void columnMarginChanged(ChangeEvent e) {
                for (int row = 0; row < friksjonTable.getRowCount(); row++) {
                    int rowHeight = 0;
                    for (int column = 0; column < friksjonTable.getColumnCount(); column++) {
                        Component comp = friksjonTable.prepareRenderer(
                        		friksjonTable.getCellRenderer(row, column), row, column);
                        rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
                    }
                    friksjonTable.setRowHeight(row, rowHeight); 
                }
            }
        });
		friksjonTable.getColumn(0).setPreferredWidth(90);
		friksjonTable.getColumn(1).setPreferredWidth(40);
		friksjonTable.getColumn(2).setPreferredWidth(90);
		friksjonTable.getColumn(3).setPreferredWidth(60);
		friksjonTable.getColumn(4).setPreferredWidth(60);
		friksjonTable.getColumn(5).setPreferredWidth(45);
		friksjonTable.getColumn(6).setPreferredWidth(30);
		friksjonTable.getColumn(7).setPreferredWidth(50);
		friksjonTable.getColumn(8).setPreferredWidth(35);
		friksjonTable.getColumn(9).setPreferredWidth(40);
		friksjonTable.getColumn(10).setPreferredWidth(40);
		friksjonTable.setHighlighters(new Highlighter[] { new ColorHighlighter(new FriksjonHighlightPredicate(),
				Color.YELLOW, null,null, null) });
	}
	
	public boolean isReadyToSave(){
		return (!changedList.isEmpty()||!removedList.isEmpty());
	}
	public void clear(){
		boolean old = isReadyToSave();
		changedList.clear();
		firePropertyChange("readyToSave", old, isReadyToSave());
	}
	public void clearRemoved(){
		boolean old = isReadyToSave();
		removedList.clear();
		firePropertyChange("readyToSave", old, isReadyToSave());
	}
	private List<Friksjonsdetalj> changedList = new ArrayList<Friksjonsdetalj>();
	private List<Friksjonsdetalj> removedList = new ArrayList<Friksjonsdetalj>();
	
	class FriksjonTableChangeListener implements MipssObservableListListener{
		
		
		@Override
		public void listElementPropertyChanged(ObservableList list, int index, PropertyChangeEvent evt) {
			boolean old = isReadyToSave();
			Friksjonsdetalj changed = (Friksjonsdetalj)list.get(index);
			if (!changedList.contains(changed)){
				changedList.add(changed);
			}
			
				String sign = plugin.getLoader().getLoggedOnUserSign();
				changed.setEndretAv(sign);
				changed.setEndretDato(Clock.now());
			
			firePropertyChange("readyToSave", old, isReadyToSave());
		}
		

		@Override
		public void listElementsAdded(ObservableList list, int index, int arg2) {
			boolean old = isReadyToSave();
			Friksjonsdetalj added = (Friksjonsdetalj)list.get(index);
			String sign = plugin.getLoader().getLoggedOnUserSign();
			added.setOpprettetAv(sign);
			added.setOpprettetDato(Clock.now());
			changedList.add(added);
			firePropertyChange("readyToSave", old, isReadyToSave());
		}

		@Override
		public void listElementsRemoved(ObservableList list, int index, List removedElements) {
			boolean old = isReadyToSave();
			Friksjonsdetalj remove = (Friksjonsdetalj)removedElements.get(0);
			if (!removedList.contains(remove)){
				if (changedList.contains(remove)){
					changedList.remove(remove);
				}
				removedList.add(remove);
			}
			firePropertyChange("readyToSave", old, isReadyToSave());
		}
		@Override
		public void listElementPropertyChanged(ObservableList list, int index) {
		}

		@Override
		public void listElementReplaced(ObservableList list, int index, Object newValue) {
		}

		@Override
		public void vetoableChange(PropertyChangeEvent evt)
				throws PropertyVetoException {
			
		}
	}
	
	public List<Friksjonsdetalj> getFriksjonsdetaljer(){
		return friksjonTableModel.getEntities();
	}
	private class FriksjonHighlightPredicate implements HighlightPredicate {

		/** {@inheritDoc} */
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			int row = adapter.row;
			int rowModel = friksjonTable.convertRowIndexToModel(row);
			Friksjonsdetalj f = friksjonTableModel.get(rowModel);
			Color bg = null;
			Color fg = Color.black;
			
			Color green = Color.green;
			Color lime = new Color(170,255,0);
			Color yellow = Color.yellow;
			Color orange = new Color(255,170,0);
			Color red = Color.red;
			Color dark = new Color(170, 0, 0);
			if (f.getFriksjon()!=null){
				double v = f.getFriksjon();
				if (v>0.35)
					bg = green;
				else if (v >0.3)
					bg = lime;
				else if (v > 0.25)
					bg = yellow;
				else if (v > 0.2)
					bg = orange;
				else if (v >= 0.15)
					bg = red;
				else if (v< 0.15){
					bg = dark;
					fg = Color.white;
				}
				
				if (friksjonTable.getSelectedRow()!=row){
					renderer.setBackground(bg);
				}
			}
			boolean highlight = false;
			return highlight;
		}
	}
	/**
	 * <p>Laster data til tabellen</p>
	 * 
	 * @param driftkontraktId
	 * @param fraDatoCet
	 * @param tilDatoCet
	 */
	public void loadData(Long driftkontraktId, Date fraDatoCet, Date tilDatoCet){
		boolean old = isReadyToSave();
		if (old){
			if (!showWarning())
				return;
		}
		changedList = new ArrayList<Friksjonsdetalj>();
		removedList = new ArrayList<Friksjonsdetalj>();
		
		friksjonTableModel.setBeanMethodArgValues(new Object[]{driftkontraktId, fraDatoCet, tilDatoCet});
		friksjonTableModel.loadData();
		new Thread(){
			public void run(){
				while (friksjonTableModel.isLoadingData()){
					Thread.yield();
				}
				MipssObservableList<Friksjonsdetalj> observableList = new MipssObservableList<Friksjonsdetalj>(friksjonTableModel.getEntities(), true);
				FriksjonTableChangeListener changeListener = new FriksjonTableChangeListener();
				observableList.addObservableListListener(changeListener);
				friksjonTableModel.setEntities(observableList);
				
			}
		}.start();
		firePropertyChange("readyToSave", old, false);
		
	}
	public List<Friksjonsdetalj> getUpdatedFriksjonsdetaljList(){
		List<Friksjonsdetalj> updated = new ArrayList<Friksjonsdetalj>();
		updated.addAll(changedList);
		return updated;
	}
	public List<Friksjonsdetalj> getRemovedFriksjonsdetaljList(){
		return removedList;
	}
	private boolean showWarning(){
		String info = "Du har endringer som ikke er lagret.\n Forkaste disse?";//\n\nSlettet:"+removed+"\n\nLagt til:"+added+"\n\nEndret:"+changed;
		int rvl = JOptionPane.showConfirmDialog(this, info, "Du har endringer som ikke er lagret", JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION);
		if (rvl==JOptionPane.YES_OPTION)
			return true;
		return false;
		

	}
	
	private void initGui(){
		setLayout(new GridBagLayout());
		friksjonTable.setPreferredScrollableViewportSize(new Dimension(0, 0));
		friksjonScroll.setViewportView(friksjonTable);
		this.add(friksjonScroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	/**
	 * Lytter som legge på en busylabel når modellen lastes. 
	 * 
	 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
	 *
	 */
	class FriksjonLoaderListener implements MipssBeanLoaderListener{
		private JXBusyLabel busyLabel;
		private final JScrollPane scroll;
		private Component view;
		
		public FriksjonLoaderListener(JScrollPane scroll){
			this.scroll = scroll;
			busyLabel = new JXBusyLabel();
			busyLabel.setText(GuiTexts.getText(GuiTexts.VENNLIGST_VENT));
			view = scroll.getViewport().getView();
		}
		
		@Override
		public void loadingFinished(MipssBeanLoaderEvent event) {
			busyLabel.setBusy(false);
			scroll.setViewportView(view);
		}

		@Override
		public void loadingStarted(MipssBeanLoaderEvent event) {
			view = scroll.getViewport().getView();
			busyLabel.setBusy(true);
			scroll.setViewportView(busyLabel);
			
		}
	}
}
