package no.mesta.mipss.friksjon.retardasjon.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JPanel;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;

public class RetardasjonPanel extends JPanel{
	private final FriksjonModule plugin;
	private RetardasjonTablePanel friksjonTablePanel;
	
	public RetardasjonPanel(FriksjonModule plugin){
		this.plugin = plugin;
		initGui();
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		friksjonTablePanel = new RetardasjonTablePanel(plugin);
		this.add(new RetardasjonParamPanel(plugin, friksjonTablePanel), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		this.add(friksjonTablePanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		this.add(new RetardasjonButtonPanel(plugin, friksjonTablePanel), 	 new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	public List<Friksjonsdetalj> getFriksjonsdetaljer(){
		return friksjonTablePanel.getFriksjonsdetaljer();
	}
	
	public RetardasjonTablePanel getFriksjonTablePanel(){
		return friksjonTablePanel;
	}
}
