package no.mesta.mipss.friksjon;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonRapportHelper;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FriksjonExcelHelper {
	private final FriksjonController controller;
	private final FriksjonModule plugin;
	private final ExcelPOIHelper excel = new ExcelPOIHelper();
	private XSSFWorkbook workbook;
	
	private Date periodeFra = null;
	private Date periodeTil = null;
    private byte[] imageBytes = ImageUtil.imageToBytes(ImageUtil.scaleImage(IconResources.MESTA_LOGO_SHADOW, 320, 76));
	
	public FriksjonExcelHelper(FriksjonController controller, FriksjonModule plugin){
		this.controller = controller;
		this.plugin = plugin;
	}
	
	public void openExcelFile(List<Prodstrekning> ps) throws IOException{
		
		File tempFile = File.createTempFile("friksjonExport", ".xlsx");
		workbook = excel.createWorkbook(tempFile, excel.getExcelTemplate("Friksjon_template.xlsx"));
		excel.setDateFormat(MipssDateFormatter.LONG_DATE_TIME_FORMAT);
		//logo på første faneark
		XSSFSheet forside = workbook.getSheetAt(0);
        XSSFRow forsideRow2 = forside.createRow(1);
        forsideRow2.setHeight((short)(18*20));
        
        int imageForside = workbook.addPicture(imageBytes, XSSFWorkbook.PICTURE_TYPE_PNG);
        XSSFClientAnchor anchorForside = new XSSFClientAnchor(0,0,0,0, (short)6,1,(short)9,5);
        anchorForside.setAnchorType( 3);
        XSSFDrawing patriarchForside = forside.createDrawingPatriarch();
        patriarchForside.createPicture(anchorForside, imageForside);
        int forsideIndex = 7;
		for (Prodstrekning p:ps){
			List<FriksjonVO> friksjonList = controller.getFriksjonForProdstrekning(p);
			FriksjonRapportHelper fh = new FriksjonRapportHelper();
			friksjonList = fh.genererStrekninger(friksjonList, 20);
			
			String title = "Friksjon ("+p.getVei()+ "HP"+p.getHp()+")";
			BufferedImage chartImage = controller.createChartImage(friksjonList, title);
			chartImage = ImageUtil.overlay(chartImage, IconResources.MESTA_LOGO_SHADOW, 540, 3, 124, 29, 1f);
			XSSFSheet sheet = createSheetTemplate(p, chartImage);
			addFriksjondata(sheet, friksjonList, p, forsideIndex);
			forsideIndex++;
		}
		addForsideHeader();
		excel.closeWorkbook(workbook);
		DesktopHelper.openFile(excel.getExcelFile());
	}
	private void addForsideHeader(){
		String kontrakt = plugin.getValgtKontrakt().getKontraktnummer()+" "+plugin.getValgtKontrakt().getKontraktnavn();
		String periode = MipssDateFormatter.formatDate(periodeFra, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)+" - "+MipssDateFormatter.formatDate(periodeTil, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT);
		XSSFSheet forside = workbook.getSheetAt(0);
		excel.setCellValue(forside.getRow(2).getCell((short)1), kontrakt);
		excel.setCellValue(forside.getRow(3).getCell((short)1), periode);
		excel.setCellValue(forside.getRow(4).getCell((short)1), MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
		forside.getRow(4).getCell((short)1);
		
	}
	private void addFriksjondata(XSSFSheet sheet, List<FriksjonVO> friksjonList, Prodstrekning p, int forsideIndex){
		int startRow = 38;
		
		Date startTidspunkt = p.getFraTidspunkt();
		String vei = p.getVei();
		String hp = String.valueOf(p.getHp());
		Date tilTidspunkt = p.getTilTidspunkt();
		
		Double minFriksjon = null;
		Long fraMeter = null;
		Long tilMeter = null;
		
		for (FriksjonVO f:friksjonList){
			XSSFRow row = excel.createRow(sheet, startRow);
			int c = 0;
			excel.writeData(row, c++, f.getMeter());
			excel.writeData(row, c++, f.getFriksjon());
			excel.writeDouble(row, c++, f.getHastighet(), excel.getDecimalStyleShort(false));
			excel.writeData(row, c++, f.getDate());
			startRow++;
			
			if (minFriksjon==null|| minFriksjon>f.getFriksjon())
				minFriksjon = f.getFriksjon();
			if (fraMeter==null||fraMeter<f.getMeter())
				fraMeter = f.getMeter();
			if (tilMeter==null||tilMeter>f.getMeter())
				tilMeter = f.getMeter();
			
			if (periodeFra==null||periodeFra.after(f.getDate()))
				periodeFra = f.getDate();
			if (periodeTil==null||periodeTil.before(f.getDate()))
				periodeTil = f.getDate();
		}
		XSSFSheet forside = workbook.getSheetAt(0);
		XSSFRow row = excel.createRow(forside, forsideIndex);
		String datoLabel = MipssDateFormatter.formatDate(startTidspunkt, MipssDateFormatter.DATE_TIME_FORMAT);
		
		String sheetName = generateSheetName(p);
		int c =0;
		excel.createLinkCell(row, c++, "\""+datoLabel+"\"", "\"#"+sheetName+"!A1\"", false);
		excel.createLinkCell(row, c++, "\""+vei+"\"", "\"#"+sheetName+"!A1\"", false);
		excel.createLinkCell(row, c++, "\""+hp+"\"", "\"#"+sheetName+"!A1\"", false);
		excel.writeData(row, c++, minFriksjon);
		excel.writeData(row, c++, fraMeter);
		excel.writeData(row, c++, tilMeter);
		if (fraMeter!=null)
			excel.writeData(row, c++, Math.abs(fraMeter-tilMeter));
		else
			excel.writeData(row, c++, "");
		excel.writeData(row, c++, startTidspunkt);
		excel.writeData(row, c++, tilTidspunkt);
	}
	
	
	
	private XSSFSheet createSheetTemplate(Prodstrekning p, BufferedImage chartImage){
		XSSFSheet sheet = excel.createSheet(generateSheetName(p), workbook);
		//legg på Mestalogo
		int image = workbook.addPicture(imageBytes, XSSFWorkbook.PICTURE_TYPE_PNG);
        XSSFClientAnchor anchor = new XSSFClientAnchor(0,0,0,0, (short)5,1,(short)10,5);
        anchor.setAnchorType( 3);
        XSSFDrawing patriarch = sheet.createDrawingPatriarch();
        patriarch.createPicture(anchor, image);
        
        //rapport tittel
		XSSFRow titleRow = excel.createRow(sheet, 0);
		XSSFCell titleCell = excel.createCell(titleRow, 0);
		titleCell.setCellStyle(excel.getTitleStyle(false));
		excel.setCellValue(titleCell, "Friksjonsrapport");
		
		//Link til forsiden
		XSSFRow linkRow = excel.createRow(sheet, 1);
		excel.createLinkCell(linkRow, 0, "\"<- Tilbake til forsiden\"", "\"#Forside!A1\"", false);
		linkRow.setHeight((short)(18*20));
		
		//header til grafen
		createChartTitleRow(sheet, 6, "Vei:", p.getVei()+ "HP"+p.getHp());
		createChartTitleRow(sheet, 7, "Periode:", MipssDateFormatter.formatDate(p.getFraTidspunkt(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT)+" - "+MipssDateFormatter.formatDate(p.getTilTidspunkt(), MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
		createChartTitleRow(sheet, 8, "Kontrakt:", plugin.getValgtKontrakt().getKontraktnummer()+" "+plugin.getValgtKontrakt().getKontraktnavn());
		
		//header til grunnlagsdata
		XSSFRow grunndataTitleRow = excel.createRow(sheet, 34);
		XSSFCell grunndataCell = excel.createCell(grunndataTitleRow, 0);
		grunndataCell.setCellStyle(excel.getTitleStyle(false));
		excel.setCellValue(grunndataCell, "Grunnlagsdata");
		
		//tabellheader til grunnlagsdata
		XSSFRow grunndataHeaderRow = excel.createRow(sheet, 36);
		createTableHeaderRow(grunndataHeaderRow, 0, "Meter");
		createTableHeaderRow(grunndataHeaderRow, 1, "Friksjon");
		createTableHeaderRow(grunndataHeaderRow, 2, "Hastighet");
		createTableHeaderRow(grunndataHeaderRow, 3, "Tidspunkt");
		grunndataHeaderRow.setHeight((short)(18*20));
		sheet.setColumnWidth((short)0, (short)(11.5*256));
		sheet.setColumnWidth((short)2, (short)(11*256));
		sheet.setColumnWidth((short)3, (short)(18.5*256));
		
		addChartImage(sheet, chartImage, patriarch);
		return sheet;
	}
	private void addChartImage(XSSFSheet sheet, BufferedImage chartImage, XSSFDrawing patriarch){
		byte[] imageBytes = ImageUtil.imageToBytes(chartImage);
		int image = workbook.addPicture(imageBytes, XSSFWorkbook.PICTURE_TYPE_PNG);
		XSSFClientAnchor anchor = new XSSFClientAnchor(0,0,0,0, (short)0,9,(short)10,32);
        anchor.setAnchorType( 3);
        patriarch.createPicture(anchor, image);
	}
	private void createTableHeaderRow(XSSFRow row, int col, String label){
		XSSFCell cell = excel.createCell(row, col);
		cell.setCellStyle(excel.getHeaderStyle(false));
		excel.setCellValue(cell, label);
	}
	private void createChartTitleRow(XSSFSheet sheet, int row, String label, String value){
		XSSFRow titleRow = excel.createRow(sheet, row);
		XSSFCell labelCell = excel.createCell(titleRow, 0);
		XSSFCell valueCell = excel.createCell(titleRow, 1);
		labelCell.setCellStyle(excel.getHeaderStyle(false));
		valueCell.setCellStyle(excel.getHeaderStyle(false));
		excel.mergeCells(sheet, 1, 9, row, row);
		excel.setCellValue(labelCell, label);
		excel.setCellValue(valueCell, value);
	}
	
	private String generateSheetName(Prodstrekning p){
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yy_hhmmss");
		String dato = f.format(p.getFraTidspunkt());
		StringBuilder sb = new StringBuilder(dato);
		sb.append("_");
		sb.append(p.getVeikategori());
		sb.append(p.getVeistatus());
		sb.append(p.getVeinummer());
		sb.append("HP");
		sb.append(p.getHp());
		return sb.toString();
	}
}
