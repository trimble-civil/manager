package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.applikasjon.Konfigparam;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

@SuppressWarnings("serial")
public class KontinuerligParamPanel extends JPanel{
	
	private static final String PARAM_APP = "MIPSS_FELT";
	private static final String PARAM_VALUE = "RoadType";
	private static final String HP = "hp";
	private static final String VEINR = "veinr";

	private String[] roadTypes;
	
	private JMipssDatePicker fraDato;
	private JMipssDatePicker tilDato;
	private final FriksjonModule plugin;

	private DefaultComboBoxModel fylkeModel = new DefaultComboBoxModel();
	private DefaultComboBoxModel kommuneModel = new DefaultComboBoxModel();
	private JComboBox veiTypeCombo;
	private JTextField veinrField;
	private JTextField hpField;
	private final KontinuerligPanel mainPanel;
	private List<Long> valgtKjoretoy;
	
//	private final KontinuerligTablePanel tablePanel;
	
	public KontinuerligParamPanel(FriksjonModule plugin, KontinuerligPanel mainPanel){
		this.plugin = plugin;
		this.mainPanel = mainPanel;
//		this.tablePanel = tablePanel;
		initRoadTypes();
		plugin.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				updateFylkeOgKommunenummer();
			}
		});
		initGui();
		updateFylkeOgKommunenummer();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		fraDato = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		tilDato = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		fraDato.pairWith(tilDato);
		DatePickerConstraintsUtils.setNotBeforeDate(plugin.getLoader(), fraDato, tilDato);

		JLabel fraLabel = new JLabel(GuiTexts.getText(GuiTexts.FRA_DATO));
		JLabel tilLabel = new JLabel(GuiTexts.getText(GuiTexts.TIL_DATO));
		JButton sokButton = new JButton(new SokAction(GuiTexts.getText(GuiTexts.HENT_MALINGER), IconResources.SEARCH_ICON));
		
		JComboBox kommuneCombo = new JComboBox(kommuneModel);
		JComboBox fylkeCombo = new JComboBox(fylkeModel);
		
		veiTypeCombo = new JComboBox(roadTypes);

		veinrField = new JTextField(5);
		veinrField.setDocument(new DocumentFilter(5));

		hpField = new JTextField(3);
		hpField.setDocument(new DocumentFilter(3));
		
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", sokButton, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", kommuneCombo, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", fylkeCombo, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", veiTypeCombo, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", veinrField, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", hpField, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", fraDato, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", tilDato, "enabled").bind();
		
		JLabel fylkeLabel = new JLabel("Fylke");
		JLabel kommuneLabel = new JLabel("Kommune");
		JLabel veiLabel = new JLabel("Veitype");
		JLabel nrLabel = new JLabel("Veinr");
		JLabel hpLabel = new JLabel("HP");
		
		JButton kjoretoyButton = new JButton(plugin.getController().getVelgKjoretoyAction(getKjoretoyCallbackAction(), this));
		
		JPanel veiPanel = new JPanel(new GridBagLayout());
		veiPanel.add(fylkeLabel, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(kommuneLabel, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(veiLabel, 		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(nrLabel, 		new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(hpLabel, 		new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(fylkeCombo, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(kommuneCombo, 	new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(veiTypeCombo, 	new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(veinrField, 	new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(hpField, 		new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		veiPanel.add(kjoretoyButton,new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		
		this.add(fraLabel, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(fraDato,  	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(tilLabel, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(tilDato,  	new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(sokButton,	new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
		
		this.add(veiPanel,  new GridBagConstraints(2, 0, 1, 2, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

	}
	
	private Action getKjoretoyCallbackAction(){
		return new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("KontinuerligParamPanel closed.. :)");
			}
			
		};
	}
	private void initRoadTypes() {
		if (roadTypes == null) {
			Konfigparam konfig = KonfigparamPreferences.getInstance().hentEnForApp(PARAM_APP, PARAM_VALUE);
			if (konfig == null || StringUtils.isBlank(konfig.getVerdi())) {
				throw new IllegalStateException("No MIPSS_FELT RoadType configured in Konfigparam");
			}
			roadTypes = konfig.getVerdi().split(",");
			for (int i = 0; i < roadTypes.length; i++) {
				roadTypes[i] = roadTypes[i].trim();
			}
			String[] rt = new String[roadTypes.length+1];
			rt[0]="";
			for (int i=0;i<roadTypes.length;i++){
				rt[i+1]=roadTypes[i];
			}
			roadTypes = rt;
		}
	}
	
	public void updateFylkeOgKommunenummer(){
		ProduksjonService p = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		Driftkontrakt d = plugin.getValgtKontrakt();
		if (d!=null){
			List<Integer> kommunenummerList = p.getKommunenummerFromDriftkontrakt(d.getId());
			List<Integer> fylkesnummerList = p.getFylkenummerFromDriftkontrakt(d.getId());
			kommuneModel.removeAllElements();
			fylkeModel.removeAllElements();
			kommuneModel.addElement(null);
			fylkeModel.addElement(null);
			for (Integer i:kommunenummerList){
				kommuneModel.addElement(i);
			}
			for (Integer i:fylkesnummerList){
				fylkeModel.addElement(i);
			}
		}
	}
	
	public FriksjonQueryParams getQueryParams(){
		FriksjonQueryParams p = new FriksjonQueryParams();
		
		Long driftkontraktId = plugin.getValgtKontrakt().getId();
		Date fra = fraDato.getDate();
		Date til = tilDato.getDate();
		fylkeModel.getSelectedItem();
		kommuneModel.getSelectedItem();
		veiTypeCombo.getSelectedItem();
		
		p.setDriftkontraktId(driftkontraktId);
		p.setFraDatoCet(fra);
		p.setTilDatoCet(til);
		
		p.setDfuIdList(valgtKjoretoy);

		if (veinrField.getText()!=null&&!veinrField.getText().equals("")){
			p.setVeinummer(Long.valueOf(veinrField.getText()));
		}
		if (hpField.getText()!=null&&!hpField.getText().equals("")){
			p.setHp(Long.valueOf(hpField.getText()));
		}
		if (veiTypeCombo.getSelectedItem()!=null){
			String vsk = (String)veiTypeCombo.getSelectedItem();
			if (vsk!=null&&vsk.length()==2){
				p.setVeikategori(String.valueOf(vsk.charAt(0)));
				p.setVeistatus(String.valueOf(vsk.charAt(1)));
			}
		}
		return p;
	}
	
	public void setValgtKjoretoy(List<Long> valgtKjoretoy){
		this.valgtKjoretoy = valgtKjoretoy;
	}
	private class SokAction extends AbstractAction{
		public SokAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			if(Days.daysBetween(new DateTime(fraDato.getDate()), new DateTime(tilDato.getDate())).getDays()>7){
				JOptionPane.showMessageDialog(mainPanel, GuiTexts.getText("sokeDatoFeilKontinuerlig"));
				return;
			}
			mainPanel.loadData(getQueryParams());
		}
	}
}
