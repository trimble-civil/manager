package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.friksjon.FriksjonChartFactory;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTableCellRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.border.DropShadowBorder;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

public abstract class TableChartPanel extends JPanel {

	private JMipssBeanTable<FriksjonVO> table;
	private JScrollPane scroll = new JScrollPane();
	private MipssBeanTableModel<FriksjonVO> model;
	private ValueMarker marker;
	private ChartPanel chartPanel;
	private final FriksjonModule plugin;
	protected JFreeChart chart;
	protected boolean inverted; 

	public TableChartPanel(FriksjonModule plugin, JFreeChart chart){
		this.plugin = plugin;
		this.chart = chart;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		createTable();
		chartPanel = new ChartPanel(chart);
		DropShadowBorder b = new DropShadowBorder(Color.black, 10, .5f, 12, true, true, true, true);
		chartPanel.setBorder(b); 
		

		registerMouseListener(chartPanel);
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				List<FriksjonVO> ent = table.getSelectedEntities();
				if (ent.size()>0){
					setMarker(ent.get(0));
				}
			}
		});
		scroll.setViewportView(table);
		add(chartPanel,	new GridBagConstraints(0,0, 1,1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(scroll, 	new GridBagConstraints(0,1, 1,1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));

	}
	
	public ChartPanel getChartPanel(){
		return chartPanel;
	}
	public void updateDataset(XYDataset friksjon, List<FriksjonVO> friksjonList, XYDataset hastighet, String title){
		
		if (friksjonList==null){
			model.setEntities(new ArrayList<FriksjonVO>());
			XYPlot plot = (XYPlot) chart.getPlot();
			plot.setDataset(null);
			plot.setDataset(1, null);
			return;
		}
		model.setEntities(friksjonList);
		chart.setTitle(title);
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDataset(friksjon);
		
		plot.setDataset(1, hastighet);
		List<Integer> axisIndices = new ArrayList<Integer>();
        axisIndices.add(new Integer(1));
		plot.mapDatasetToRangeAxes(1, axisIndices);
			
		Dimension panelSize = chartPanel.getPreferredSize();
		chartPanel.setMinimumDrawHeight(0);
		chartPanel.setMaximumDrawHeight(2000);
		chartPanel.setMinimumDrawWidth(0);
		chartPanel.setMaximumDrawWidth(2000);
		chartPanel.setPreferredSize(panelSize);
	}
	public void setInverted(boolean inverted){
		this.inverted = inverted;
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.getDomainAxis().setInverted(inverted);
	}
	
	private void createTable(){
		boolean admin = false;
		if (UserUtils.isAdmin(plugin.getLoader().getLoggedOnUser(true))){
			model = new MipssBeanTableModel<FriksjonVO>(FriksjonVO.class, "dfuId", "eksterntNavn", "date", "friksjon", "hastighet", "aktivitet", "nmea", "meter");
			admin = true;
		}else{
			model = new MipssBeanTableModel<FriksjonVO>(FriksjonVO.class, "eksterntNavn", "date", "friksjon", "hastighet", "meter");
		}
		
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(FriksjonVO.class, model.getColumns());
		table = new JMipssBeanTable<FriksjonVO>(model, colModel);
		table.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFillsViewportWidth(true);
		table.setDoWidthHacks(false);
		
		if (admin){
			table.getColumn(0).setPreferredWidth(10);
			table.getColumn(1).setPreferredWidth(20);
			table.getColumn(2).setPreferredWidth(70);
			table.getColumn(3).setPreferredWidth(15);
			table.getColumn(4).setPreferredWidth(15);
			table.getColumn(5).setPreferredWidth(15);
			table.getColumn(6).setPreferredWidth(100);
			table.getColumn(7).setPreferredWidth(10);
		}
	}
	
	private void registerMouseListener(final ChartPanel panel){
		panel.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				XYPlot plot = (XYPlot) panel.getChart().getXYPlot(); 
				Rectangle2D plotArea = panel.getChartRenderingInfo().getPlotInfo().getDataArea();
		        ValueAxis domainAxis = plot.getDomainAxis();
		        RectangleEdge domainAxisEdge = plot.getDomainAxisEdge();
		        long chartX = (long)domainAxis.java2DToValue(e.getPoint().getX(),plotArea,domainAxisEdge);
		        
		        for (FriksjonVO v:model.getEntities()){
		        	if (after(v, chartX)){
		        		int vr = table.convertRowIndexToView(model.getEntities().indexOf(v));
		        		table.getSelectionModel().setSelectionInterval(vr, vr);
		        		table.scrollRowToVisible(vr);
		        		setMarker(v);
		        		return;
		        	}
		        }
			}
		});
	}
	
	protected abstract boolean after(FriksjonVO f, double chartX);
	
	protected abstract void setMarker(double xValue, Double yValue);
	protected abstract void setMarker(FriksjonVO f);
	
//	private void setMarker(double xValue, Double value){
//		getMarker().setValue(xValue);
////		ensureVisibilityForTime(date, value);
//		chart.setNotify(true);
//        chart.fireChartChanged();
//	}
	
	protected ValueMarker getMarker(){
		if (marker==null){
			marker = new ValueMarker(0);
			marker.setPaint(Color.black);
			marker.setAlpha(1);
			marker.setStroke(new BasicStroke(2));
			XYPlot plot = (XYPlot) chart.getXYPlot();
			plot.addDomainMarker(marker);
		}
		return marker;
	}
//		
//	private void ensureVisibilityForTime(Date date, Double rangeValue){
//		ValueAxis axis = ((XYPlot)chart.getPlot()).getDomainAxis();
//		ValueAxis raxis = ((XYPlot)chart.getPlot()).getRangeAxis();
//		calculateRange(axis, date.getTime());
//		calculateRange(raxis, rangeValue);
//		
//	}
//	
	protected void calculateRange(ValueAxis axis, double value){
		Range range = axis.getRange();
		double lower = range.getLowerBound();
		double upper= range.getUpperBound();
		
		double length = range.getLength();
		Range newRange=null;
		if (value<lower){
			newRange = new Range(value-length/2, value+length/2);
		}
		if (value>upper){
			newRange = new Range(value-length/2, value+length/2);
		}
		if (newRange!=null){
			axis.setRange(newRange);
		}
	}
}
