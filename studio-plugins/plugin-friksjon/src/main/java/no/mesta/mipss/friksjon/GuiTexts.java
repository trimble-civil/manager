package no.mesta.mipss.friksjon;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class GuiTexts {
	public static final String KONTRAKT = "kontrakt";
	public static final String FRA_DATO = "fraDato";
	public static final String TIL_DATO = "tilDato";
	public static final String RAPPORT_R7 = "rapportR7";
	public static final String HENT_MALINGER = "hent";
	public static final String VIS_I_KART = "vis";
	public static final String RAPPORT_PDF = "rapportPdf";
	public static final String EXCEL = "excel";
	public static final String DETALJ = "detalj";
	public static final String NY_LINJE = "nylinje";
	public static final String LAGRE = "lagre";
	public static final String VENNLIGST_VENT = "vent";
	public static final String ENDRE = "endre";
	public static final String FORE = "fore";
	public static final String NEDBOR = "nedbor";
	public static final String START_TILTAK = "startTiltak";
	public static final String FULLFORT_TILTAK = "fullortTiltak";
	public static final String HVEM = "hvem";
	public static final String MERKNADER = "merknader";
	public static final String SLETT = "slett";
	public static final String MANUELL_WARNING = "manuellWarning";
	public static final String MANUELL_WARNING_TITLE = "manuellWarningTitle";
	public static final String HENTER_FRIKSJON = "henterFriksjon";
	public static final String HENTER_KJORETOY = "henterKjoretoy";
	public static final String SOKERESULTAT = "sokeresultat";
	public static final String KJORETOY = "kjoretoy";
	public static final String KJORETOY_TITLE = "kjoretoyDialog.title";
	public static final String KJORETOY_DESCRIPTION = "kjoretoyDialog.description";
	
	private static PropertyResourceBundleUtil props;
	/**
     * Fyller ut poperties
     * 
     * @return
     */
    private static PropertyResourceBundleUtil getProps() {
        if(props == null) {
            props = PropertyResourceBundleUtil.getPropertyBundle("friksjonText");
        }
        
        return props;
    }
    
    /**
     * Henter ut GUI teksten
     * 
     * @param key
     * @return
     */
    public static String getText(String key) {
        return getProps().getGuiString(key);
    }
    
    /**
     * Henter GUI teksten, og fletter inn objekter
     * 
     * @param key
     * @param values
     * @return
     */
    public static String getText(String key, Object... values) {
        return getProps().getGuiString(key, values);
    }
}
