package no.mesta.mipss.friksjon;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;

import no.mesta.mipss.ui.paint.MipssLinearGradientPaint;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

public class FriksjonChartFactory {

	public static JFreeChart createFriksjonChart(XYDataset dataset, String title){//Friksjon hp
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(title, "Tid", "Antall", dataset, true, true,false);
		XYPlot plot = chart.getXYPlot();
		
		appendColors(plot);
		createFriksjonAxis(plot);
		createHastighetAxis(plot);
		createRenderer(plot, 0, Color.red);
	    createRenderer(plot, 1, Color.blue);
		return chart;
	}
	
	public static JFreeChart createFriksjonChartMeter(XYDataset dataset, String title){
		final JFreeChart chart = ChartFactory.createXYLineChart(title, "Meter", "Friskjon", dataset, PlotOrientation.VERTICAL,true,true,false);
		XYPlot plot = chart.getXYPlot();
		
		appendColors(plot);
		createFriksjonAxis(plot);
		createHastighetAxis(plot);
		createRenderer(plot, 0, Color.red);
	    createRenderer(plot, 1, Color.blue);
		return chart;
	}
	
	private static void appendColors(XYPlot plot){
		Paint paint = new MipssLinearGradientPaint(new Color(190, 218, 231), Color.white);
		plot.setBackgroundPaint(paint);
		plot.setDomainGridlinePaint(Color.gray);
		plot.setRangeGridlinePaint(Color.gray);
	}
	private static void createFriksjonAxis(XYPlot plot){
		NumberAxis friksjonAxis = new NumberAxis("Friksjon");
		friksjonAxis.setRange(0,1.2001);
		plot.setRangeAxis(0, friksjonAxis);
		plot.setRangeAxisLocation(0, AxisLocation.BOTTOM_OR_LEFT);
	}
	
	private static void createHastighetAxis(XYPlot plot){
		NumberAxis hastighetAxis = new NumberAxis("Hastighet");
		hastighetAxis.setRange(0, 120);
		plot.setRangeAxis(1, hastighetAxis);
	    plot.setRangeAxisLocation(1, AxisLocation.TOP_OR_RIGHT);
	}
	
	private static void createRenderer(XYPlot plot, int index, Color color){
		XYLineAndShapeRenderer hastighetRenderer = new XYLineAndShapeRenderer();
	    hastighetRenderer.setBaseStroke(new BasicStroke(2));
	    hastighetRenderer.setSeriesPaint(index, color);
	    hastighetRenderer.setBaseShapesVisible(false);
	    plot.setRenderer(1, hastighetRenderer);
	}
}
