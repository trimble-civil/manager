package no.mesta.mipss.friksjon.r7;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.reports.ReportHelper;

public class ReportManager {
    private List<Friksjonsdetalj> friksjonList;
    
    public ReportManager(List<Friksjonsdetalj> friksjonList) {
        
    	this.friksjonList = new ArrayList<Friksjonsdetalj>();
    	for (Friksjonsdetalj f:friksjonList){
    		this.friksjonList.add(f.clone());
    	}
    }
    
    public JasperPrint generateReport(String kontraktNrNavn){
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("kontraktNrNavn", kontraktNrNavn);
        JasperPrint report = null;
        int maxWidth = 140;
        int counter = 0;
        List<String> merknader = new ArrayList<String>();
        
        Font f = new Font("SansSerif", Font.PLAIN, 10);
        FontRenderContext fc = new FontRenderContext(null, false, false);
        
        for(Friksjonsdetalj fo:friksjonList){
            String merkn = fo.getBeskrivelse();
            if (merkn!=null&&!merkn.equals("")){
            	double width = f.getStringBounds(merkn, fc).getWidth();
	            if (width>maxWidth){
	                merknader.add("*"+counter+" "+merkn);
	                fo.setBeskrivelse("*"+counter);
	                counter++;
	            }
            }
        }
        
    	JasperReport subReport = ReportHelper.findReport("R7Data.jasper");
    	JasperReport merknaderReport = ReportHelper.findReport("R7Merknader.jasper");
        
        MerknaderDataSource mds = new MerknaderDataSource(merknader);
        ReportDataSource subsource = new ReportDataSource(friksjonList, merknaderReport, mds);
        
        ReportMainDataSource mainDataSource = new ReportMainDataSource(subReport, subsource, merknaderReport, mds);

        File repo = ReportHelper.buildReport("R7noCrossTab.jasper", mainDataSource, ReportHelper.EXPORT_PDF, parameters);
        DesktopHelper.openFile(repo);

        return report;
    }
}
