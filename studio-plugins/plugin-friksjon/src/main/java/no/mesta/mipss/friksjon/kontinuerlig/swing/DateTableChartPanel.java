package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.util.Date;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;

@SuppressWarnings("serial")
public class DateTableChartPanel extends TableChartPanel {

	public DateTableChartPanel(FriksjonModule plugin, JFreeChart chart) {
		super(plugin, chart);
	}

	@Override
	protected void setMarker(double xValue, Double yValue) {
		getMarker().setValue(xValue);
		ensureVisibilityForTime(new Date((long) xValue), yValue);
		chart.setNotify(true);
	    chart.fireChartChanged();
	}
	
	private void ensureVisibilityForTime(Date date, Double rangeValue){
		ValueAxis axis = ((XYPlot)chart.getPlot()).getDomainAxis();
		ValueAxis raxis = ((XYPlot)chart.getPlot()).getRangeAxis();
		calculateRange(axis, date.getTime());
		calculateRange(raxis, rangeValue);
	}

	@Override
	protected boolean after(FriksjonVO f, double chartX) {
		return (f.getDate().after(new Date((long) chartX)));
	}

	@Override
	protected void setMarker(FriksjonVO f) {
		setMarker(f.getDate().getTime(), f.getFriksjon());
	}

}
