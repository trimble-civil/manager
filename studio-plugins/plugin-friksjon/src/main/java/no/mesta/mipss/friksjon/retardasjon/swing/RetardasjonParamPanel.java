package no.mesta.mipss.friksjon.retardasjon.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

@SuppressWarnings("serial")
public class RetardasjonParamPanel extends JPanel{
	private JMipssDatePicker fraDato;
	private JMipssDatePicker tilDato;
	private final FriksjonModule plugin;
	private final RetardasjonTablePanel retardasjonTable;
	
	public RetardasjonParamPanel(FriksjonModule plugin, RetardasjonTablePanel retardasjonTable){
		this.plugin = plugin;
		this.retardasjonTable = retardasjonTable;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		fraDato = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		tilDato = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), 1));
		fraDato.pairWith(tilDato);
		DatePickerConstraintsUtils.setNotBeforeDate(plugin.getLoader(), fraDato, tilDato);
//		JLabel kontraktLabel = new JLabel(GuiTexts.getText(GuiTexts.KONTRAKT));
		JLabel fraLabel = new JLabel(GuiTexts.getText(GuiTexts.FRA_DATO));
		JLabel tilLabel = new JLabel(GuiTexts.getText(GuiTexts.TIL_DATO));
		
		JButton sokButton = new JButton(new SokAction(GuiTexts.getText(GuiTexts.HENT_MALINGER), IconResources.SEARCH_ICON));
		
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", sokButton, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", fraDato, "enabled").bind();
		BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", tilDato, "enabled").bind();

//		this.add(kontraktLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//		this.add(contractPicker, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		this.add(fraLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(fraDato,  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		this.add(tilLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(tilDato,  new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(sokButton,new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
	}
	
	
	private class SokAction extends AbstractAction{
		public SokAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			Long driftkontraktId = plugin.getValgtKontrakt().getId();
			Date fra = fraDato.getDate();
			Date til = tilDato.getDate();
			fra = CalendarUtil.round(fra, true);
			til = CalendarUtil.round(til, false);
			retardasjonTable.loadData(driftkontraktId, fra, til);
		}
	}
}
