package no.mesta.mipss.friksjon.retardasjon.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.messages.MipssMapFriksjonMessage;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.jdesktop.beansbinding.Binding;

/**
 * Knappepanelet til friksjonsmålinger
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
public class RetardasjonButtonPanel extends JPanel{
	
	private final FriksjonModule plugin;
	private final RetardasjonTablePanel tablePanel;
	public RetardasjonButtonPanel(FriksjonModule plugin, RetardasjonTablePanel tablePanel){
		this.plugin = plugin;
		this.tablePanel = tablePanel;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		JButton r7Button = new JButton(new R7Action(GuiTexts.getText(GuiTexts.RAPPORT_R7), IconResources.ADOBE_16));
		JButton visButton = new JButton(new VisIKartAction(GuiTexts.getText(GuiTexts.VIS_I_KART), IconResources.NORGE_ICON));
		
		JButton slettButton = new JButton(new RemoveAction(GuiTexts.getText(GuiTexts.SLETT), IconResources.BUTTON_CANCEL_ICON));
		JButton lagreButton = new JButton(new SaveAction(GuiTexts.getText(GuiTexts.LAGRE), IconResources.SAVE_ICON));
		
		Binding slettActivate = BindingHelper.createbinding(tablePanel.getFriksjonTable(), "${noOfSelectedRows == 1}", slettButton, "enabled");
		Binding visiKartActivate = BindingHelper.createbinding(tablePanel.getFriksjonTable(), "${noOfSelectedRows == 1}", visButton, "enabled");
		Binding r7Activate = BindingHelper.createbinding(tablePanel.getFriksjonTableModel(), "${size > 0}", r7Button, "enabled");
		Binding lagreActivate = BindingHelper.createbinding(tablePanel, "${readyToSave==true}", lagreButton, "enabled");
		
		slettActivate.bind();
		visiKartActivate.bind();
		r7Activate.bind();
		lagreActivate.bind();
		
		this.add(r7Button, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
		this.add(visButton, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(slettButton, 	new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(lagreButton, 	new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
	}
	
	private class R7Action extends AbstractAction{
		public R7Action(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			plugin.createReport();
		}
	}
	private class VisIKartAction extends AbstractAction{
		public VisIKartAction(String text, Icon icon){
			super(text, icon);
		}
		@SuppressWarnings("unchecked")
		@Override
		public void actionPerformed(ActionEvent e) {
			List<Friksjonsdetalj> selectedEntities = tablePanel.getFriksjonTable().getSelectedEntities();
			Friksjonsdetalj f = selectedEntities.get(0);
//			if (f.isManuell()){
//				JOptionPane.showMessageDialog(plugin.getModuleGUI(), GuiTexts.getText(GuiTexts.MANUELL_WARNING), GuiTexts.getText(GuiTexts.MANUELL_WARNING_TITLE), JOptionPane.WARNING_MESSAGE);
//				return;
//			}
			List<Friksjonsdetalj> friksjonsdetaljer = new ArrayList<Friksjonsdetalj>();
			friksjonsdetaljer.add(f);
			MipssPlugin target;
			try {
				target = plugin.getLoader().getPlugin((Class<? extends MipssPlugin>)Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));
				MipssMapFriksjonMessage msg = new MipssMapFriksjonMessage(friksjonsdetaljer);
				msg.setTargetPlugin(target);
				plugin.getLoader().sendMessage(msg);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}
	private class NyLinjeAction extends AbstractAction{
		public NyLinjeAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			Friksjonsdetalj f = new Friksjonsdetalj();
			f.setManuell(true);
			tablePanel.getFriksjonTableModel().getEntities().add(f);
//			tablePanel.getFriksjonTableModel().addItem(f);
		}
	}
	private class SaveAction extends AbstractAction{
		public SaveAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			List<Friksjonsdetalj> fl= tablePanel.getUpdatedFriksjonsdetaljList();
			ProduksjonService bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
			try{
			
				bean.deleteFriksjonsdetaljer(tablePanel.getRemovedFriksjonsdetaljList());
				tablePanel.clearRemoved();
			} catch (Exception ex){
				JOptionPane.showMessageDialog(plugin.getModuleGUI(), "Beklager, en feil oppstod i forbindelse med sletting\n"+ex.getMessage(), "Feil", JOptionPane.ERROR_MESSAGE);
			}
			try{
				bean.updateFriksjonsdetaljer(fl);
				tablePanel.clear();
			} catch (Exception ex){
				JOptionPane.showMessageDialog(plugin.getModuleGUI(), "Beklager, en feil oppstod i forbindelse med lagring\n"+ex.getMessage(), "Feil", JOptionPane.ERROR_MESSAGE);
			}
			
		}
	}
	
	private class RemoveAction extends AbstractAction{
		public RemoveAction(String text, Icon icon){
			super(text, icon);
		}
		public void actionPerformed(ActionEvent e){
			JMipssBeanTable table = tablePanel.getFriksjonTable();
			MipssBeanTableModel<Friksjonsdetalj> model = tablePanel.getFriksjonTableModel();
			
			int modelRow =table.convertRowIndexToModel(table.getSelectedRow());
			if (modelRow==-1)
				return;
			table.getSelectionModel().clearSelection();
			model.getEntities().remove(modelRow);
			model.fireTableModelEvent(0, 0, TableModelEvent.DELETE);
			
		}
	}
	
}

