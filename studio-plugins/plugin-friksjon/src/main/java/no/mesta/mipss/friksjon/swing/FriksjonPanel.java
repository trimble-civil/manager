package no.mesta.mipss.friksjon.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.friksjon.kontinuerlig.swing.KontinuerligPanel;
import no.mesta.mipss.friksjon.retardasjon.swing.RetardasjonPanel;
import no.mesta.mipss.friksjon.retardasjon.swing.RetardasjonTablePanel;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.ui.JMipssContractPicker;

/**
 * Hovedpanelet til friksjonsmålinger.
 * 
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 *
 */
@SuppressWarnings("serial")
public class FriksjonPanel extends JPanel{
	private JMipssContractPicker contractPicker;

	private final FriksjonModule plugin;
	private RetardasjonPanel retardasjonPanel;
	private KontinuerligPanel kontinuerligPanel;
	
	public FriksjonPanel(FriksjonModule plugin){
		this.plugin = plugin;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		JLabel kontraktLabel = new JLabel(GuiTexts.getText(GuiTexts.KONTRAKT));
		contractPicker = new JMipssContractPicker(plugin, false);
		contractPicker.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				plugin.setValgtKontrakt((Driftkontrakt)evt.getNewValue());
			}
		});
		
		retardasjonPanel = new RetardasjonPanel(plugin);
		kontinuerligPanel = new KontinuerligPanel(plugin);
		
		JTabbedPane tab = new JTabbedPane();
		tab.addTab("Kontinuerlig", kontinuerligPanel);
		tab.addTab("Retardasjon", retardasjonPanel);
		
		this.add(kontraktLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(contractPicker, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		add(tab, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		
	}
	
	public List<Friksjonsdetalj> getFriksjonsdetaljerRetardasjon(){
		return retardasjonPanel.getFriksjonsdetaljer();
	}
	
	public RetardasjonTablePanel getRetardasjonTablePanel(){
		return retardasjonPanel.getFriksjonTablePanel();
	}
}
