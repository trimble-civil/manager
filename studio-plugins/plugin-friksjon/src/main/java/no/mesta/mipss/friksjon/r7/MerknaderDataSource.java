package no.mesta.mipss.friksjon.r7;

import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;

public class MerknaderDataSource implements JRDataSource{
    
    private int index = -1;
    private List merknader;
    public MerknaderDataSource(List merknader) {
        this.merknader = merknader;
    }

    public boolean next() {
        index++;
        return index<merknader.size();
    }

    public Object getFieldValue(JRField jrField) {
        return merknader.get(index);
    }
}
