package no.mesta.mipss.friksjon.r7;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JasperReport;

public class ReportMainDataSource implements JRDataSource {

    private JasperReport subreport;
    private ReportDataSource subdatasource;
    
    private JasperReport merknaderReport;
    private MerknaderDataSource merknaderDatasource;

    private int index =-1;
    
    public ReportMainDataSource(JasperReport subreport, ReportDataSource subdatasource, JasperReport merknaderReport, MerknaderDataSource merknaderDatasource) {
        this.subreport = subreport;
        this.subdatasource = subdatasource;
        this.merknaderReport = merknaderReport;
        this.merknaderDatasource = merknaderDatasource;
    }

    public boolean next() {
        index++;
        return index < 1;
    }

    public Object getFieldValue(JRField field) {
        String fieldName = field.getName();
        
        if (fieldName.equals("SubReport")){
                return subreport;
        }
        if (fieldName.equals("SubReport2")){
            return merknaderReport;
        }
        if (fieldName.equals("SubreportDataSource")){
                return subdatasource;
        }
        if (fieldName.equals("SubreportDataSource2")){
                return merknaderDatasource;
        }
        return null;
    }
}
