package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class KontinuerligPanel extends JPanel{

	private final FriksjonModule plugin;
	private KontinuerligParamPanel kontinuerligParamPanel;
	
	private KjoretoyTab defaultPane;
	private JTabbedPane resultTab;
	
	private JPanel busy = new JPanel();
	private JXBusyLabel busyLabel = new JXBusyLabel();

	public KontinuerligPanel(FriksjonModule plugin) {
		this.plugin = plugin;
		initGui();
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		busy.setLayout(new GridBagLayout());
		busyLabel.setText(GuiTexts.getText(GuiTexts.HENTER_FRIKSJON));
		busy.add(busyLabel, new GridBagConstraints(0,0,1,1,1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		
		
		kontinuerligParamPanel = new KontinuerligParamPanel(plugin, this);
		
		defaultPane = new KjoretoyTab(plugin, null);
		resultTab = new JTabbedPane();
		resultTab.addTab(GuiTexts.getText(GuiTexts.SOKERESULTAT), defaultPane);
		
		this.add(kontinuerligParamPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		this.add(resultTab, 			 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	public void loadData(final FriksjonQueryParams p){
		setBusy();
		new SwingWorker<Void, Void>(){
			public Void doInBackground(){
				
				Map<String, KjoretoyTab> dataPrTab = plugin.getController().searchForData(p);
				resultTab.removeAll();
				if (!dataPrTab.isEmpty()){
					for (String title:dataPrTab.keySet()){
						resultTab.addTab(title, dataPrTab.get(title));
					}
				}else{
					resultTab.addTab(GuiTexts.getText(GuiTexts.SOKERESULTAT), defaultPane);
				}
				return null;
				
			}
			public void done(){
				doneLoading();
			}
		}.execute();
	}
	
	public void setBusy(){
		busyLabel.setBusy(true);
		this.remove(resultTab);
		this.add(busy, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		revalidate();
		resultTab.repaint();
	}
	private void doneLoading(){
		busyLabel.setBusy(false);
		this.remove(busy);
		this.add(resultTab, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		revalidate();
		resultTab.repaint();
		
	}
}
