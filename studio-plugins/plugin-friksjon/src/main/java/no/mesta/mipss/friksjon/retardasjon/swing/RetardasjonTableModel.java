package no.mesta.mipss.friksjon.retardasjon.swing;

import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

public class RetardasjonTableModel extends MipssBeanTableModel<Friksjonsdetalj>{

	public RetardasjonTableModel(int[] editableColumnIndices, String... beanColumns) {
		super(Friksjonsdetalj.class, editableColumnIndices, beanColumns);
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		Friksjonsdetalj friksjonsdetalj = getEntities().get(rowIndex);
//		if (friksjonsdetalj.isManuell()){
//			return true;
//		}
		return super.isCellEditable(rowIndex, columnIndex);
	}

}
