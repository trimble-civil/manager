package no.mesta.mipss.friksjon;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ExcelHelper;
import no.mesta.mipss.friksjon.kontinuerlig.swing.KjoretoyTab;
import no.mesta.mipss.friksjon.kontinuerlig.swing.KontinuerligParamPanel;
import no.mesta.mipss.friksjon.kontinuerlig.swing.VelgKjoretoyDialog;
import no.mesta.mipss.kjoretoy.KjoretoyDfuVO;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonRapportHelper;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonVO;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;



@SuppressWarnings("serial")
public class FriksjonController {
	private ProduksjonService service;
	private final FriksjonModule plugin;
	public FriksjonController(FriksjonModule plugin){
		this.plugin = plugin;
		service = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
		
	}
	public List<FriksjonVO> getFriksjonForProdstrekning(Prodstrekning ps){
		List<FriksjonVO> friksjonList = service.hentFriksjonsdataForProdstrekning(ps);
		
		//TODO dette gjøres serverside ved neste deploy og kan fjernes..
		List<FriksjonVO> cleanList = new ArrayList<FriksjonVO>();
		for (FriksjonVO f:friksjonList){
			if (f.getFriksjon()!=null)
				cleanList.add(f);
		}
		//END..
		
		return cleanList;
	}
	public XYDataset getDatasetForFriksjon(List<FriksjonVO> friksjonList){
		TimeSeriesCollection series = new TimeSeriesCollection();
		TimeSeries ts = new TimeSeries("Friksjon");
		series.addSeries(ts);
		for (FriksjonVO f:friksjonList){
			Second s = new Second(f.getDate());
			if (f.getFriksjon()!=null)
				ts.add(s, f.getFriksjon());
		}
				
		return series;
	}
	public XYDataset getDatasetForHastighet(List<FriksjonVO> friksjonList){
		TimeSeriesCollection series = new TimeSeriesCollection();
		TimeSeries ts = new TimeSeries("Hastighet");
		series.addSeries(ts);
		for (FriksjonVO f:friksjonList){
			Second s = new Second(f.getDate());
			if (f.getFriksjon()!=null)
				ts.add(s, f.getHastighet());
		}
				
		return series;
	}
	
	public XYDataset getDatasetForFriksjonMeter(List<FriksjonVO> friksjonList){
		XYSeries xy = new XYSeries("Friksjon");
		for (FriksjonVO f:friksjonList){
			xy.add(f.getMeter(), f.getFriksjon());
		}
		
		return new XYSeriesCollection(xy);
	}
	public XYDataset getDatasetForHastighetMeter(List<FriksjonVO> friksjonList){
		XYSeries xy = new XYSeries("Hastighet");
		for (FriksjonVO f:friksjonList){
			xy.add(f.getMeter(), f.getHastighet());
		}
		return new XYSeriesCollection(xy);
	}
	
	
	public Map<String, KjoretoyTab> searchForData(FriksjonQueryParams p){
		Map<String, List<Prodstrekning>> friksjonsDataPrKjoretoy = service.getKontinuerligFriksjonsdetalj(p);
		Map<String, KjoretoyTab> panels = new HashMap<String, KjoretoyTab>();
		for (String eksterntNavn :friksjonsDataPrKjoretoy.keySet()){
			List<Prodstrekning> ps = friksjonsDataPrKjoretoy.get(eksterntNavn);
			KjoretoyTab t = new KjoretoyTab(plugin, eksterntNavn);
			t.setProdstrekninger(ps);
			panels.put(eksterntNavn, t);
		}
		return panels;
	}
	
	public AbstractAction getVelgKjoretoyAction(final Action callback, final KontinuerligParamPanel params) {
		return new AbstractAction(GuiTexts.getText(GuiTexts.KJORETOY), IconResources.BIL_SMALL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				VelgKjoretoyDialog dialog = new VelgKjoretoyDialog(plugin, params);
				dialog.setModal(true);
//				dialog.pack();
				dialog.setLocationRelativeTo(plugin.getModuleGUI());
				dialog.setVisible(true);
				if (!dialog.isCancelled()){
					ActionEvent ev = new ActionEvent(dialog, 0, null);
					callback.actionPerformed(ev);
				}
			}
			
		};
	}
	
	public List<KjoretoyDfuVO> getDfuMedFriksjon(FriksjonQueryParams p){
		MipssKjoretoy k = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
		p.setFraDatoCet(CalendarUtil.round(p.getFraDatoCet(), true));
		p.setTilDatoCet(CalendarUtil.round(p.getTilDatoCet(), false));
		return k.getDfuMedFriksjon(p.getDriftkontraktId(), p.getFraDatoCet(), p.getTilDatoCet());
	}
	
	/**
	 * Lager en graf av prodstrekningen og returnerer dette som et BufferdImage 
	 * @param ps
	 * @return
	 */
	public BufferedImage createChartImage(List<FriksjonVO> friksjonList, String chartTitle){
		FriksjonRapportHelper fh = new FriksjonRapportHelper();
		List<FriksjonVO> fl = fh.genererStrekninger(friksjonList, 20);
		XYDataset friksjonDs = plugin.getController().getDatasetForFriksjonMeter(fl);
		XYDataset hastighetDs = plugin.getController().getDatasetForHastighetMeter(fl);
		
		JFreeChart chart = FriksjonChartFactory.createFriksjonChartMeter(null, chartTitle);
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDataset(friksjonDs);
		
		plot.setDataset(1, hastighetDs);
		List<Integer> axisIndices = new ArrayList<Integer>();
        axisIndices.add(new Integer(1));
		plot.mapDatasetToRangeAxes(1, axisIndices);
			
		if (fh.isInverted(friksjonList)){
			plot.getDomainAxis().setInverted(true);
		}
		return chart.createBufferedImage(734, 391);
	}
	
//	public void openExcelFile(Prodstrekning ps){
//		new FriksjonExcelHelper(this, plugin).openExcelFile(ps);
//	}
	public void openExcelFile(List<Prodstrekning> strekningList){
		try {
			new FriksjonExcelHelper(this, plugin).openExcelFile(strekningList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
