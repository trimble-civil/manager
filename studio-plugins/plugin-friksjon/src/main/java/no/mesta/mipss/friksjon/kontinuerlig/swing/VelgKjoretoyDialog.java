package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import no.mesta.mipss.friksjon.FriksjonController;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.kjoretoy.KjoretoyDfuVO;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.friksjon.FriksjonQueryParams;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTableCellRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXHeader;

@SuppressWarnings("serial")
public class VelgKjoretoyDialog extends JDialog implements MipssBeanLoaderListener{
	
	private boolean cancelled = true;
	private final FriksjonModule plugin;
	private JMipssBeanTable<KjoretoyDfuVO> kjoretoyTable;
	private JScrollPane kjoretoyScroll = new JScrollPane();
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private MipssBeanTableModel<KjoretoyDfuVO> model;
	private final KontinuerligParamPanel paramspanel;
	
	public VelgKjoretoyDialog(FriksjonModule plugin, KontinuerligParamPanel p){
		this.plugin = plugin;
		this.paramspanel = p;
		initGui();
		
		loadData(p.getQueryParams());
		setSize(380,300);	
	}

	private void initGui() {
		setLayout(new BorderLayout());
		
		createTable();
		
		add(getHeader(), BorderLayout.NORTH);
		add(getKjoretoyPanel(), BorderLayout.CENTER);
	}
	
	private JPanel getKjoretoyPanel(){
		busyLabel.setText(GuiTexts.getText(GuiTexts.HENTER_KJORETOY));
		kjoretoyTable.setPreferredScrollableViewportSize(new Dimension(0, 0));
		kjoretoyScroll.setViewportView(kjoretoyTable);
		
		JButton okButton = new JButton(getOkAction());
		JButton cancelButton = new JButton(getCancelAction());
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(okButton, new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0,0));
		buttonPanel.add(cancelButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0,0));
		
		JPanel panel = new JPanel(new GridBagLayout());
		panel.add(kjoretoyScroll, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0,0));
		panel.add(buttonPanel, 	  new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0,0));
		
		return panel;
	}
    private JXHeader getHeader(){
        JXHeader header = new JXHeader();
        header.setPreferredSize(new Dimension(360, 70));
        header.setTitle(GuiTexts.getText(GuiTexts.KJORETOY_TITLE));
        header.setDescription(GuiTexts.getText(GuiTexts.KJORETOY_DESCRIPTION));
        header.setIcon(IconResources.BIL);
        return header;
    }
    
    private void loadData(FriksjonQueryParams params){
		model.setBeanMethodArgValues(new Object[]{params});
		model.loadData();
	}
    
	private void createTable(){
		model = new MipssBeanTableModel<KjoretoyDfuVO>(KjoretoyDfuVO.class, "eksterntNavn", "fraDato", "tilDato");
		
		model.setBeanInstance(plugin.getController());
		model.setBeanInterface(FriksjonController.class);
		model.setBeanMethod("getDfuMedFriksjon");
		model.setBeanMethodArguments(new Class[]{FriksjonQueryParams.class});
		model.addTableLoaderListener(this);
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(KjoretoyDfuVO.class, model.getColumns());
		kjoretoyTable = new JMipssBeanTable<KjoretoyDfuVO>(model, colModel);
		kjoretoyTable.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		
		kjoretoyTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		kjoretoyTable.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				int rowAtPoint = kjoretoyTable.rowAtPoint(e.getPoint());
				kjoretoyTable.getSelectionModel().addSelectionInterval(rowAtPoint, rowAtPoint);
				System.out.println(kjoretoyTable.getSelectionModel().isSelectedIndex(rowAtPoint));
			}
		});
		kjoretoyTable.setFillsViewportWidth(true);
		kjoretoyTable.setDoWidthHacks(false);
//		friksjonTable.getColumn(9).setPreferredWidth(40);
//		friksjonTable.getColumn(10).setPreferredWidth(40);
	}
    private Action getOkAction(){
    	return new AbstractAction("Ok", IconResources.OK2_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=false;
				List<Long> selectedEntities = new ArrayList<Long>();
				for (KjoretoyDfuVO d:kjoretoyTable.getSelectedEntities()){
					selectedEntities.add(d.getDfuId());
				}
				paramspanel.setValgtKjoretoy(selectedEntities);
				dispose();
				
			}
    		
    	};
    }
    
	private Action getCancelAction(){
    	return new AbstractAction("Avbryt", IconResources.CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
    		
    	};
    }
    public boolean isCancelled(){
    	return cancelled;
    }
    
    
    @Override
	public void loadingFinished(MipssBeanLoaderEvent event) {
		busyLabel.setBusy(false);
		kjoretoyScroll.setViewportView(kjoretoyTable);
		
	}

	@Override
	public void loadingStarted(MipssBeanLoaderEvent event) {
		busyLabel.setBusy(true);
		kjoretoyScroll.setViewportView(busyLabel);
		
	}
}
