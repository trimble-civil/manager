package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.friksjon.GuiTexts;
import no.mesta.mipss.messages.MipssMapFriksjonStrekningMessage;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class KontinuerligButtonPanel extends JPanel{

	private final FriksjonModule plugin;
	private final KontinuerligTablePanel tablePanel;

	public KontinuerligButtonPanel(FriksjonModule plugin, KontinuerligTablePanel tablePanel){
		this.plugin = plugin;
		this.tablePanel = tablePanel;
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JButton reportButton = new JButton(new ReportAction(GuiTexts.getText(GuiTexts.EXCEL), IconResources.EXCEL_ICON16));
		JButton visButton = new JButton(new VisIKartAction(GuiTexts.getText(GuiTexts.VIS_I_KART), IconResources.NORGE_ICON));
		final DetailAction detailAction = new DetailAction(GuiTexts.getText(GuiTexts.DETALJ), IconResources.GRAF_TAB_16);
		JButton detaljButton = new JButton(detailAction);
		
		Binding reportActivate = 	BindingHelper.createbinding(tablePanel.getFriksjonTable(), "${noOfSelectedRows >= 1}", reportButton, "enabled");
		Binding visiKartActivate =	BindingHelper.createbinding(tablePanel.getFriksjonTable(), "${noOfSelectedRows == 1}", visButton, "enabled");
		Binding detaljActivate = 	BindingHelper.createbinding(tablePanel.getFriksjonTable(), "${noOfSelectedRows == 1}", detaljButton, "enabled");
		
		tablePanel.getFriksjonTable().addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount()==2){
					detailAction.actionPerformed(null);
				}
			}
		});
		visiKartActivate.bind();
		reportActivate.bind();
		detaljActivate.bind();
		
		this.add(reportButton, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
		this.add(visButton, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		this.add(detaljButton, 	new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
	}
	
	private class ReportAction extends AbstractAction{
		public ReportAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
//			Prodstrekning ps = tablePanel.getSelectedStrekning();
//			if (ps!=null)
//				plugin.getController().openExcelFile(ps);
//			
			final List<Prodstrekning> strekningList = tablePanel.getSelectedStrekningList();
			if (strekningList!=null&&strekningList.size()>0){
				final JDialog d = new JDialog();
				JXBusyLabel label = new JXBusyLabel();
				label.setText("Lager rapport, venligst vent");
				label.setBusy(true);
				d.add(label);
				d.pack();
				d.setLocationRelativeTo(plugin.getLoader().getApplicationFrame());
				d.setVisible(true);
				new SwingWorker<Void, Void>(){
					public Void doInBackground(){
						try{
							plugin.getController().openExcelFile(strekningList);
						} catch (Throwable e){
							e.printStackTrace();
							d.dispose();
						}
						d.dispose();
						return null;
					}
				}.execute();
			}
		}
	}
	
	private class VisIKartAction extends AbstractAction{
		public VisIKartAction(String text, Icon icon){
			super(text, icon);
		}
		@SuppressWarnings("unchecked")
		@Override
		public void actionPerformed(ActionEvent e) {
			Prodstrekning ps = tablePanel.getSelectedStrekning();
			if (ps!=null){
				List<Prodstrekning> psList = new ArrayList<Prodstrekning>();
				psList.add(ps);
				MipssPlugin target;
				try {
					target = plugin.getLoader().getPlugin((Class<? extends MipssPlugin>)Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));
					MipssMapFriksjonStrekningMessage msg = new MipssMapFriksjonStrekningMessage(psList);	
					msg.setTargetPlugin(target);
					plugin.getLoader().sendMessage(msg);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
			}
				
//			List<Friksjonsdetalj> selectedEntities = tablePanel.getFriksjonTable().getSelectedEntities();
//			Friksjonsdetalj f = selectedEntities.get(0);
//			List<Friksjonsdetalj> friksjonsdetaljer = new ArrayList<Friksjonsdetalj>();
//			friksjonsdetaljer.add(f);
//			MipssPlugin target;
//			try {
//				target = plugin.getLoader().getPlugin((Class<? extends MipssPlugin>)Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));
//				MipssMapFriksjonMessage msg = new MipssMapFriksjonMessage(friksjonsdetaljer);
//				msg.setTargetPlugin(target);
//				plugin.getLoader().sendMessage(msg);
//			} catch (ClassNotFoundException e1) {
//				e1.printStackTrace();
//			}
		}
	}
	private class DetailAction extends AbstractAction{
		public DetailAction(String text, Icon icon){
			super(text, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			Prodstrekning ps = tablePanel.getSelectedStrekning();
			if (ps!=null){
				FriksjonChartPanel panel = new FriksjonChartPanel(plugin, tablePanel);
				panel.updateChartData(ps);
				JDialog f = new JDialog(plugin.getLoader().getApplicationFrame());
				f.setContentPane(panel);
				f.pack();
				f.setLocationRelativeTo(plugin.getLoader().getApplicationFrame());
		        f.setVisible(true);
			}
			
		}
	}
}
