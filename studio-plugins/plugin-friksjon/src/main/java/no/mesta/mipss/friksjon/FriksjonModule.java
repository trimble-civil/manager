package no.mesta.mipss.friksjon;

import java.util.List;

import javax.swing.JComponent;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.friksjon.r7.ReportManager;
import no.mesta.mipss.friksjon.swing.FriksjonPanel;
import no.mesta.mipss.friksjonservice.FriksjonsdetaljService;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kvernetf1.Fore;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;
import no.mesta.mipss.persistence.kvernetf1.Nedbor;
import no.mesta.mipss.plugin.MipssPlugin;

public class FriksjonModule extends MipssPlugin{
	private FriksjonPanel panel;
	private FriksjonController controller = new FriksjonController(this);
	private List<Fore> foreList;
	private List<Nedbor> nedborList;
	private Driftkontrakt valgtKontrakt;
	
	@Override
	public JComponent getModuleGUI() {
		return panel;
	}

	@Override
	protected void initPluginGUI() {
		panel = new FriksjonPanel(this);
		setValgtKontrakt(getLoader().getSelectedDriftkontrakt());
		
	}

	@Override
	public void shutdown() {
		panel = null;
		
	}

	public FriksjonController getController(){
		return controller;
	}
	/**
	 * Alle føreentitetene
	 * @return
	 */
	public List<Fore> getForeList(){
		if (foreList == null){
			foreList = BeanUtil.lookup(FriksjonsdetaljService.BEAN_NAME, FriksjonsdetaljService.class).getForeList();
		}
		return foreList;
	}
	/**
	 * Alle nedbørsentitetene
	 * @return
	 */
	public List<Nedbor> getNedborList(){
		if (nedborList == null){
			nedborList = BeanUtil.lookup(FriksjonsdetaljService.BEAN_NAME, FriksjonsdetaljService.class).getNedborList();
		}
		return nedborList;
	}
	public void createReport(){
		if (valgtKontrakt==null){
			Driftkontrakt d = getLoader().getSelectedDriftkontrakt();
			if (d!=null)
				valgtKontrakt = d;
		}
		if (valgtKontrakt!=null){
			new ReportManager(getFriksjonsdetaljer()).generateReport(valgtKontrakt.getKontraktnummer()+" "+valgtKontrakt.getKontraktnavn());
		}else{
			System.out.println("kontrakt ikke valgt:"+valgtKontrakt);
		}
	}
	
	public List<Friksjonsdetalj> getFriksjonsdetaljer(){
		return panel.getFriksjonsdetaljerRetardasjon();
	}

	public void setValgtKontrakt(Driftkontrakt valgtKontrakt) {
		Driftkontrakt old = this.valgtKontrakt;
		this.valgtKontrakt = valgtKontrakt;
		props.firePropertyChange("valgtKontrakt", old, valgtKontrakt);
	}

	public Driftkontrakt getValgtKontrakt() {
		return valgtKontrakt;
	}
}
