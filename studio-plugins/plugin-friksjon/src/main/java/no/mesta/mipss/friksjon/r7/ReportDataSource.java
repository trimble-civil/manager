package no.mesta.mipss.friksjon.r7;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JasperReport;
import no.mesta.mipss.persistence.kvernetf1.Friksjonsdetalj;

public class ReportDataSource implements JRDataSource{
    
    private List friksjonList;
    private int index = -1;
    private JasperReport merknaderReport;
    private MerknaderDataSource merknaderDatasource;
    public ReportDataSource(List friksjonList, JasperReport merknaderReport, MerknaderDataSource merknaderDatasource) {
        this.friksjonList = friksjonList;
        this.merknaderReport = merknaderReport;
        this.merknaderDatasource = merknaderDatasource;
    }
    

    public boolean next() {
        index++;
        return index<friksjonList.size();
    }

    public Object getFieldValue(JRField field) {
        String fieldName = field.getName();
        
        return getValue(fieldName, (Friksjonsdetalj)friksjonList.get(index));
    }
    
    private Object getValue(String fieldName, Friksjonsdetalj value){
        System.out.println(fieldName);
        try {
            Method m = value.getClass().getDeclaredMethod(getFieldGetterName(fieldName), null);
            return  m.invoke(value, null);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
             e.printStackTrace();
        } catch (InvocationTargetException e) {
             e.printStackTrace();
        }
        return null;
    }
    
    private String getFieldGetterName(String fieldName){
        return "get"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
    }
}
