package no.mesta.mipss.friksjon.kontinuerlig.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JPanel;

import no.mesta.mipss.friksjon.FriksjonModule;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;

public class KjoretoyTab extends JPanel{
	
	private KontinuerligTablePanel tablePanel;
	private KontinuerligButtonPanel buttonPanel;
	private final FriksjonModule plugin;
	private final String eksterntNavn;
	
	public KjoretoyTab(FriksjonModule plugin, String eksterntNavn){
		this.plugin = plugin;
		this.eksterntNavn = eksterntNavn;
		initGui();
	}

	private void initGui() {
		tablePanel = new KontinuerligTablePanel(plugin);
		buttonPanel = new KontinuerligButtonPanel(plugin, tablePanel);		
		setLayout(new GridBagLayout());
		
		this.add(tablePanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		this.add(buttonPanel, 	 new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

	}
	
	public void setProdstrekninger(List<Prodstrekning> ps){
		tablePanel.setProdstrekninger(ps);
	}

	public String getEksterntNavn() {
		return eksterntNavn;
	}
	
}
