package no.mesta.mipss.chart;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.chart.ChartActions.ChartActionType;
import no.mesta.mipss.chart.messages.OpenSingleChartMessage;
import no.mesta.mipss.chart.swing.ChartPanel;
import no.mesta.mipss.chart.swing.MipssChartPanel;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;

import org.jdesktop.beansbinding.Binding;

@SuppressWarnings("serial")
public class ChartModule extends MipssPlugin{
	public static final String KEY_MODUL_KONFIG = "studio.mipss.graf";
	
	private ChartPanel mainPanel;
	private ChartQueryParams queryParams;
	private FieldToolkit fieldToolkit;
	private SingleChartMode waitForRunning;
	private Driftkontrakt valgtKontrakt;
	
	@Override
	public JPanel getModuleGUI() {
		return mainPanel;
	}

	@Override
	protected void initPluginGUI() {
		queryParams = new ChartQueryParams();

		mainPanel = new ChartPanel(this);
		fieldToolkit.bind();
	}

	@Override
	public void shutdown() {
		mainPanel.dispose();
		mainPanel = null;
		queryParams = null;
		fieldToolkit.unbindAndRemove();
		fieldToolkit = null;
		if (waitForRunning!=null){
			waitForRunning.dispose();
			waitForRunning.cancel(true);
			waitForRunning = null;
		}
		for (PropertyChangeListener l:props.getPropertyChangeListeners()){
			props.removePropertyChangeListener(l);
		}
	}

	public FieldToolkit getFieldToolkit() {
        if (fieldToolkit==null){
            fieldToolkit = FieldToolkit.createToolkit();
        }
        return fieldToolkit;
    }
	public void addBinding(Binding<?,?,?,?> b){
		getFieldToolkit().addBinding(b);
	}
	public ChartQueryParams getQueryParams() {
		return queryParams;
	}
	
	public void setValgtKontrakt(Driftkontrakt valgtKontrakt) {
		Driftkontrakt old = this.valgtKontrakt;
		this.valgtKontrakt = valgtKontrakt;
		props.firePropertyChange("valgtKontrakt", old, valgtKontrakt);

	}

	public Driftkontrakt getValgtKontrakt() {
		return valgtKontrakt;
	}

	/** {@inheritDoc} */
	@Override
	public void receiveMessage(PluginMessage<?, ?> message){
		if (!(message instanceof OpenSingleChartMessage)){
			throw new IllegalArgumentException("Unsupported message type");
		}
		message.processMessage();
	}

	public void setSingleChartMode(final ChartQueryParams params, final ChangeListener listener) {
		waitForRunning = new SingleChartMode(params, listener);
		waitForRunning.execute();
	}
	
	class SingleChartMode extends SwingWorker<Void, Void>{
		
		private final ChartQueryParams params;
		private final ChangeListener listener;
		private MipssChartPanel singleChartPanel;
		private ChartActions action;
		private JFrame newDialog;
		public SingleChartMode(ChartQueryParams params, ChangeListener listener){
			this.params = params;
			this.listener = listener;
			
		}
		@Override
		protected Void doInBackground() throws Exception {
			
			while(getStatus() != MipssPlugin.Status.RUNNING) {
				Thread.yield();
			}
			singleChartPanel = new MipssChartPanel(ChartModule.this, null, ChartActions.ChartActionType.SINGLE, null);
			singleChartPanel.setQueryParams(params);
			singleChartPanel.setTitlePainter(null);
			singleChartPanel.setUI(null);
			singleChartPanel.setToolbarVisible(false);
			action = new ChartActions(ChartModule.this, params, singleChartPanel);
			action.getChartAction(ChartActionType.SINGLE).actionPerformed(null);
			
			JPanel newPanel = new JPanel();
            newPanel.setLayout(new BorderLayout());
            newPanel.add(singleChartPanel, BorderLayout.CENTER);
            
            newDialog = new JFrame("Graf");
            newDialog.setAlwaysOnTop(true);
            newDialog.add(newPanel);
            
            newDialog.pack();
            newDialog.setLocationRelativeTo(null);
            newDialog.setVisible(true);
            
            newDialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            newDialog.addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e) {                                    
                       newDialog.dispose();
                }
            });
            if(listener != null) {
				singleChartPanel.addChangeListener(listener);
			}
            singleChartPanel.addChangeListener(new ChangeListener(){
				@Override
				public void stateChanged(ChangeEvent e) {
					singleChartPanel.setToolbarVisible(true);
					newDialog.pack();
					newDialog.setLocationRelativeTo(null);
				}
            });
            
			return null;
		}
		/**
		 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
		 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
		 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
		 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
		 * etter at modulen er lukket. (mem-leaks)
		 */
		public void dispose(){
			singleChartPanel.clearChangeListeners();
			action.dispose();
			action = null;
			singleChartPanel=null;
			
			for (WindowListener l:newDialog.getWindowListeners()){
				newDialog.removeWindowListener(l);
			}
			newDialog = null;
		}
	}
	
}
