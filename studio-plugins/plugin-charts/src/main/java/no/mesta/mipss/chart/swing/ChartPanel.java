package no.mesta.mipss.chart.swing;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerDateModel;

import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.KjoretoySokPanel;
import no.mesta.mipss.ui.UIUtils;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

import org.jdesktop.beansbinding.Binding;

@SuppressWarnings("serial")
public class ChartPanel extends JPanel{
	
	private static final String KEY_FRADATO_KONFIG = "standard.fradato.daysago";
	private static final String KEY_TILDATO_KONFIG = "standard.tildato.daysago";
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("grafText");
	
	private ChartModule plugin;
	
	private JMipssDatePicker fraPicker;
    private JMipssDatePicker tilPicker;
    private JSpinner fraSpinner;
    private JSpinner tilSpinner;
    private MipssDateTimePickerHolder fraDato;
    private MipssDateTimePickerHolder tilDato;
    private JMipssContractPicker contractPicker;
    private SingleVehiclePanel singlePanel;
    private MultipleVehiclePanel multiPanel;
    private JTabbedPane tab;
    
    private int fraDatoDaysAgo;
    private int tilDatoDaysAgo;
    
	public ChartPanel(ChartModule plugin) {
		this.plugin = plugin;
		
		fraDatoDaysAgo = Integer.valueOf(KonfigparamPreferences.getInstance().hentEnForApp(ChartModule.KEY_MODUL_KONFIG, KEY_FRADATO_KONFIG).getVerdi());
		tilDatoDaysAgo = Integer.valueOf(KonfigparamPreferences.getInstance().hentEnForApp(ChartModule.KEY_MODUL_KONFIG, KEY_TILDATO_KONFIG).getVerdi());
		initGui();
	}

	private void initGui() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		contractPicker = new JMipssContractPicker(plugin, false);
		contractPicker.setPreferredSize(new Dimension(200, 20));
		contractPicker.getDropdown().setMaximumRowCount(30);
		contractPicker.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				plugin.setValgtKontrakt((Driftkontrakt)evt.getNewValue());			
			}
		});
		
		JPanel contractPanel = UIUtils.getHorizPanel();
		contractPanel.add(getLabel("Velg kontrakt", 82));
		contractPanel.add(contractPicker);
		contractPanel.add(Box.createHorizontalGlue());
		
		fraPicker = new JMipssDatePicker(CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), fraDatoDaysAgo));
		tilPicker = new JMipssDatePicker();
		fraPicker.pairWith(tilPicker);
		DatePickerConstraintsUtils.setNotBeforeDate(plugin.getLoader(), fraPicker, tilPicker);
		
		Dimension spinnerSize = new Dimension(70, 22);
        fraSpinner = new javax.swing.JSpinner(new SpinnerDateModel(fraPicker.getDate(), null, null, Calendar.MINUTE));
        fraSpinner.setPreferredSize(spinnerSize);
        fraSpinner.setMinimumSize(spinnerSize);
        fraSpinner.setMaximumSize(spinnerSize);
        fraSpinner.setEditor(new JSpinner.DateEditor(fraSpinner, "HH:mm:ss"));
        
        
        Date midnight = CalendarUtil.getDaysAgo(CalendarUtil.getMidnight(Clock.now()), tilDatoDaysAgo);
		Calendar c = Calendar.getInstance();
		c.setTime(midnight);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		
        tilSpinner = new javax.swing.JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        tilSpinner.setPreferredSize(spinnerSize);
        tilSpinner.setMinimumSize(spinnerSize);
        tilSpinner.setMaximumSize(spinnerSize);
        tilSpinner.setEditor(new JSpinner.DateEditor(tilSpinner, "HH:mm:ss"));
        
        
        fraDato = new MipssDateTimePickerHolder(fraPicker, fraSpinner);
        tilDato = new MipssDateTimePickerHolder(tilPicker, tilSpinner);
        
        
        JPanel fraPanel = UIUtils.getHorizPanel();
        fraPanel.add(getLabel(resources.getGuiString("chartpanel.fraDato"), 78));
        fraPanel.add(UIUtils.getHStrut(4));
        fraPanel.add(fraPicker);
        fraPanel.add(UIUtils.getHStrut(10));
        fraPanel.add(fraSpinner);
        fraPanel.add(Box.createHorizontalGlue());
        
        JPanel tilPanel = UIUtils.getHorizPanel();
        tilPanel.add(new JLabel(resources.getGuiString("chartpanel.tilDato")));
        tilPanel.add(UIUtils.getHStrut(10));
        tilPanel.add(tilPicker);
        tilPanel.add(UIUtils.getHStrut(10));
        tilPanel.add(tilSpinner);
        tilPanel.add(Box.createHorizontalGlue());
        
        contractPanel.setBorder(BorderFactory.createEmptyBorder(0, 7, 5, 0));
        fraPanel.setBorder(BorderFactory.createEmptyBorder(0, 7, 0, 0));
        tilPanel.setBorder(BorderFactory.createEmptyBorder(0, 7, 0, 0));
        
		tab = new JTabbedPane();
		singlePanel = new SingleVehiclePanel(plugin);
		multiPanel = new MultipleVehiclePanel(plugin);
		tab.addTab(resources.getGuiString("chartpanel.singlechart"), getSinglePanel());
		tab.addTab(resources.getGuiString("chartpanel.multiplechart"), multiPanel);
		if (plugin.getLoader().getSelectedDriftkontrakt()==null)
			tab.setEnabledAt(1, false);
		contractPicker.addPropertyChangeListener(new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getNewValue()==null){
					tab.setEnabledAt(1, false);
					tab.setSelectedIndex(0);
					plugin.getQueryParams().setDriftkontraktId(null);
				}else{
					tab.setEnabledAt(1, true);
					plugin.getQueryParams().setDriftkontraktId(((Driftkontrakt)evt.getNewValue()).getId());
				}
			}
		});
		if (plugin.getLoader().getSelectedDriftkontrakt()!=null){
        	int index = contractPicker.getModel().getKontrakter().indexOf(plugin.getLoader().getSelectedDriftkontrakt());
        	contractPicker.getDropdown().setSelectedIndex(index);
        	plugin.getQueryParams().setDriftkontraktId(plugin.getLoader().getSelectedDriftkontrakt().getId());
        	plugin.setValgtKontrakt(plugin.getLoader().getSelectedDriftkontrakt());
        }
		add(contractPanel);
        add(fraPanel);
        add(UIUtils.getVStrut(5));
		add(tilPanel);
		add(UIUtils.getVStrut(10));
		add(tab);
		
		setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
		
		bind();
		
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		for (PropertyChangeListener l:contractPicker.getPropertyChangeListeners()){
			contractPicker.removePropertyChangeListener(l);
		}
		singlePanel.dispose();
		multiPanel.dispose();
		tab = null;
		contractPicker = null;
		plugin = null;
	}
	public void activateSingleChartTab(){
		tab.setSelectedIndex(0);	
	}
	public MipssChartPanel getSingleChartPanel(){
		return singlePanel.getSingleChartPanel();
	}
	
	private void bind() {
		Binding<MipssDateTimePickerHolder, Date, ChartQueryParams, Date> fraDatoBind = BindingHelper.createbinding(fraDato, "date", plugin.getQueryParams(), "fraDate");
		Binding<MipssDateTimePickerHolder, Date, ChartQueryParams, Date> tilDatoBind = BindingHelper.createbinding(tilDato, "date", plugin.getQueryParams(), "tilDate");
		Binding<JMipssContractPicker, Driftkontrakt, KjoretoySokPanel, Driftkontrakt> driftkontraktBind = BindingHelper.createbinding(contractPicker, "valgtKontrakt", getSinglePanel().getKjoretoyPanel(), "valgtKontrakt");
		
		plugin.addBinding(fraDatoBind);
		plugin.addBinding(tilDatoBind);
		plugin.addBinding(driftkontraktBind);
		
		plugin.addBinding(BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", fraPicker, "enabled"));
		plugin.addBinding(BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", fraSpinner, "enabled"));
		plugin.addBinding(BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", tilPicker, "enabled"));
		plugin.addBinding(BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", tilSpinner, "enabled"));
		plugin.addBinding(BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", tilDato, "enabled"));
		plugin.addBinding(BindingHelper.createbinding(plugin, "${valgtKontrakt!=null}", singlePanel, "enabled"));

	}
	
	
	private JLabel getLabel(String text, int w){
		JLabel label = new JLabel(text);
		UIUtils.setComponentSize(new Dimension(w, 20), label);
		return label;
	}

	public SingleVehiclePanel getSinglePanel() {
		return singlePanel;
	}
	
}
