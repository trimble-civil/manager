package no.mesta.mipss.chart.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.chart.ChartActions;
import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chart.ChartActions.ChartActionType;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.chartservice.dataset.CategoryChartPlot;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.ArrowButton;
import no.mesta.mipss.ui.UIUtils;
import no.mesta.mipss.ui.picturepanel.ImageSelection;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTitledPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;

@SuppressWarnings("serial")
public class MipssChartPanel extends JXTitledPanel {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("grafText");
	
	private JScrollPane contentPane;
	private JFreeChart chart;
	private org.jfree.chart.ChartPanel chartPanel;
	private JPanel toolbarPanel;
	private final ChartModule plugin;
	private ChartQueryParams params;
	private final ChartActionType chartType;
	private final VehicleChartPanel parent;
	private boolean docked = true;
	public JScrollPane getContentPane() {
		return contentPane;
	}

	public MipssChartPanel(ChartModule plugin, String title, ChartActions.ChartActionType chartType, VehicleChartPanel parent) {
		super(title);
		this.plugin = plugin;
		this.chartType = chartType;
		this.parent = parent;
			
		initGui();
	}
	
	public void waitForChart(){
		if (docked){
			JXBusyLabel wait = new JXBusyLabel();
			wait.setText("Vennligst vent mens grafen genereres");
			wait.setBusy(true);
			if (contentPane==null){
				contentPane = new JScrollPane();
				add(contentPane, BorderLayout.CENTER);
			}
			contentPane.setViewportView(wait);
		}else{
			wait.setVisible(true);
			wait.setBusy(true);
		}
		
	}
	private void setDocked(boolean docked){
		this.docked = docked;
	}
	public void unWaitForChart(){
		contentPane.setViewportView(null);
	}
	
	public void setContentPane(JScrollPane contentPane){
		
		add(contentPane, BorderLayout.CENTER);
	}
	private void initGui() {
		wait = new JXBusyLabel();
		wait.setText("Vennligst vent mens grafen genereres");
		wait.setVisible(false);
		
		contentPane = new JScrollPane();
		add(contentPane, BorderLayout.CENTER);
		toolbarPanel = createToolbarPanel();
		add(toolbarPanel, BorderLayout.SOUTH);
		
		Dimension d = new Dimension(17, 17);
        ArrowButton floatButton = new ArrowButton(d, resources.getGuiString("chartpanel.extractbutton.text"));
        floatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getUI().setRightDecoration(null);
                setDocked(false);
                final JFrame newDialog = new JFrame(getTitle());
                newDialog.add(MipssChartPanel.this);
                newDialog.setSize(MipssChartPanel.this.getSize());
                newDialog.setLocationRelativeTo(null);
                newDialog.setVisible(true);
                
                newDialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                newDialog.addWindowListener(new WindowAdapter(){
                    public void windowClosing(WindowEvent e) {                                    
                           newDialog.dispose();
                    }
                });
                parent.setChartPanel(new MipssChartPanel(plugin, getTitle(), chartType, parent));
                parent.repaint();
        		repaint();
            }
        });

        JPanel buttonPnl = new JPanel(new FlowLayout());
        buttonPnl.setOpaque(false);
        buttonPnl.add(floatButton);

        getUI().setRightDecoration(buttonPnl);
        setBorder(null);
		
	}
	public void setQueryParams(ChartQueryParams params){
		this.params = params;
	}
	/**
	 * Litt feil bruk av ChangeListener men ettersom PropertyListener ikke fungerer
	 * som det skal er dette en lett utvei..
	 */
	private List<ChangeListener> list = new ArrayList<ChangeListener>();
	private JXBusyLabel wait;
	public void addChangeListener(ChangeListener l){
		list.add(l);
	}
	public void removeChangeListener(ChangeListener l){
		list.remove(l);
	}
	
	public void clearChangeListeners(){
		list.clear();
	}
	protected void fireStateChanged(Object source){
		ChangeEvent e = new ChangeEvent(source);
		for (ChangeListener l:list){
			l.stateChanged(e);
		}
	}
	
	public void setChart(JFreeChart chart){
		if (!docked){
			wait.setVisible(false);
			wait.setBusy(false);
			if (this.chart!=null){
				ValueAxis axis = ((XYPlot)this.chart.getPlot()).getDomainAxis();
				((XYPlot)chart.getPlot()).getDomainAxis().setRange(axis.getRange());
			}
		}
		this.chart = chart;
		if (chart!=null){
			chartPanel = new org.jfree.chart.ChartPanel(chart);
			contentPane.setViewportView(chartPanel);	
			contentPane.getVerticalScrollBar().setUnitIncrement(10);
		}else{
			contentPane.setViewportView(new JLabel("Ingen data funnet..."));
			chartPanel = null;
			return;
		}
		
		int head = 55;
		int foot = 40;
		int space = 5;
		
		CombinedDomainXYPlot plot = (CombinedDomainXYPlot) chart.getPlot();
		
		List<XYPlot> subplots = plot.getSubplots();
		int size = head+foot+((subplots.size()-1)*space);
		for (XYPlot p:subplots){
			size += p.getWeight();
		}
		
		Dimension panelSize = chartPanel.getPreferredSize();
		panelSize.height=size;
		chartPanel.setMinimumDrawHeight(0);
		chartPanel.setMaximumDrawHeight(2000);
		chartPanel.setMinimumDrawWidth(0);
		chartPanel.setMaximumDrawWidth(2000);
		
		chartPanel.setPreferredSize(panelSize);
		fireStateChanged(chartPanel);
	}
	
	
	private JPanel createToolbarPanel(){
		JPanel panel = UIUtils.getHorizPanel();
		
		JButton button = new JButton(new ClipboardAction("", IconResources.COPY_ICON_BIG));
		JButton refreshButton = new JButton(new RefreshAction("", IconResources.REFRESH_VIEW_ICON));
		button.setToolTipText(resources.getGuiString("clipboard.tooltip"));
		refreshButton.setToolTipText(resources.getGuiString("refresh.tooltip"));
		setButtonSize(button);
		setButtonSize(refreshButton);

		panel.add(UIUtils.getHStrut(3));
		panel.add(button);
		panel.add(UIUtils.getHStrut(5));
		panel.add(refreshButton);
		panel.add(UIUtils.getHStrut(25));
		panel.add(wait);
		panel.add(Box.createHorizontalGlue());
		
		JPanel p = UIUtils.getVertPanel();
		p.add(UIUtils.getVStrut(3));
		p.add(panel);
		p.add(UIUtils.getVStrut(3));
		return p;
	}
	private void setButtonSize(JComponent b){
        Dimension s = new Dimension(30,30);
        b.setMaximumSize(s);
        b.setPreferredSize(s);
        b.setMinimumSize(s);
    }
	
	public void setToolbarVisible(boolean visible){
		toolbarPanel.setVisible(visible);
	}
	
	class ClipboardAction extends AbstractAction{
		
		public ClipboardAction(String name, Icon icon){
			super(name, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			
			Dimension d = contentPane.getSize();
			d = chartPanel.getSize();
			ImageSelection.copyImageToClipboard(chart.createBufferedImage(d.width, d.height));
		}
	}
	
	class RefreshAction extends AbstractAction{
		
		public RefreshAction(String name, Icon icon){
			super(name, icon);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			if (params==null)
				params = plugin.getQueryParams().clone();
			if (params!=null){
				ChartActions chartActions = new ChartActions(plugin, params, MipssChartPanel.this);
				chartActions.getChartAction(chartType).actionPerformed(e);
			}
			
		}
		
	}
}

