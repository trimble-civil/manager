package no.mesta.mipss.chart.messages;

import java.awt.Component;
import java.util.Date;
import java.util.Map;

import javax.swing.event.ChangeListener;

import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.plugin.PluginMessage;

import org.jfree.chart.JFreeChart;

public class OpenSingleChartMessage extends PluginMessage<String, JFreeChart>{

	private ChartQueryParams params;
	private final ChangeListener listener;
	public OpenSingleChartMessage(Long kjoretoyId, Date fra, Date til, ChangeListener listener){
		this.listener = listener;
		params = new ChartQueryParams();
		params.setKjoretoyId(kjoretoyId);
		params.setFraDate(fra);
		params.setTilDate(til);
		params.setHastighet(true);
		params.setProduksjon(true);
		params.setRode(true);
		params.setSprederdata(true);
		params.setVei(true);
		params.setVeinummer(false);
	}
	@Override
	public Component getMessageGUI() {
		return null;
	}

	@Override
	public Map<String, JFreeChart> getResult() {
		
		return null;
	}

	@Override
	public void processMessage() {
		if (getTargetPlugin() instanceof ChartModule){
			ChartModule plugin = (ChartModule)getTargetPlugin();
			plugin.setSingleChartMode(params, listener);
		}
		setConsumed(true);
	}

}
