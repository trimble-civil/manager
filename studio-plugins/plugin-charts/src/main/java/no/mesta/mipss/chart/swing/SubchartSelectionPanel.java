package no.mesta.mipss.chart.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import no.mesta.mipss.chart.ChartActions;
import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chart.ChartActions.ChartActionType;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;

@SuppressWarnings("serial")
public class SubchartSelectionPanel extends JPanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("grafText");
	private JCheckBox hastighetCheck;
	private JCheckBox veiCheck;
	private JCheckBox veinummerCheck;
	private JCheckBox rodeCheck;
	private JCheckBox produksjonCheck;
	private JCheckBox sprederCheck;
	private JCheckBox skjulIdCheck;
	
	private ChartModule plugin;
	private MipssChartPanel chartPanel;
	private AutoBinding<Object, Object, Object, Object> binding;
	private ChartActions chartAction;
	private JButton button;
	
	public SubchartSelectionPanel(ChartModule plugin, MipssChartPanel chartPanel) {
		this.plugin = plugin;
		this.chartPanel = chartPanel;
		initGui();
	}

	public void setChartPanel(MipssChartPanel chartPanel){
		this.chartPanel = chartPanel;
		chartAction.setContentPane(chartPanel);
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		plugin = null;
		chartPanel = null;
		binding.unbind();
		binding = null;
		chartAction.dispose();
		chartAction = null;
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		hastighetCheck = new JCheckBox(resources.getGuiString("criteria.hastighetCheck.text"));
		veiCheck = new JCheckBox(resources.getGuiString("criteria.vei.text"));
		veinummerCheck = new JCheckBox(resources.getGuiString("criteria.veinummer.text"));
		rodeCheck = new JCheckBox(resources.getGuiString("criteria.rode.text"));
		produksjonCheck = new JCheckBox(resources.getGuiString("criteria.produksjon.text"));
		sprederCheck = new JCheckBox(resources.getGuiString("criteria.spreder.text"));
		skjulIdCheck = new JCheckBox(resources.getGuiString("criteria.hideId"));
		
		hastighetCheck.setSelected(true);
		veiCheck.setSelected(true);
		rodeCheck.setSelected(true);
		produksjonCheck.setSelected(true);
		sprederCheck.setSelected(true);
		
		binding = Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ, veiCheck, BeanProperty.create("selected"), veinummerCheck, BeanProperty.create("enabled"));
		binding.bind();
		
		chartAction = new ChartActions(plugin, chartPanel);
		button = new JButton(chartAction.getChartAction(ChartActionType.SINGLE));
		
		add(hastighetCheck, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,0,0), 0,0));
		add(veiCheck, 		new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(veinummerCheck, new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(rodeCheck, 		new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(produksjonCheck,new GridBagConstraints(0,3, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(sprederCheck, 	new GridBagConstraints(0,4, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(skjulIdCheck, 	new GridBagConstraints(0,5, 1,1, 1.0,1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		add(button, 		new GridBagConstraints(0,6, 1,1, 1.0,1.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, new Insets(0,0,5,0), 0,0));
		
		bind();
	}
	
	@Override
	public void setEnabled(boolean enabled){
		hastighetCheck.setEnabled(enabled);
		veiCheck.setEnabled(enabled);
		veinummerCheck.setEnabled(enabled);
		rodeCheck.setEnabled(enabled);
		produksjonCheck.setEnabled(enabled);
		sprederCheck.setEnabled(enabled);
		button.setEnabled(enabled);
		skjulIdCheck.setEnabled(enabled);
	}

	private void bind() {
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> hastighetBind = BindingHelper.createbinding(hastighetCheck, "selected", plugin.getQueryParams(), "hastighet");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> veiBind = BindingHelper.createbinding(veiCheck, "selected", plugin.getQueryParams(), "vei");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> veinummerBind = BindingHelper.createbinding(veinummerCheck, "selected", plugin.getQueryParams(), "veinummer");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> rodeBind = BindingHelper.createbinding(rodeCheck, "selected", plugin.getQueryParams(), "rode");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> produksjonBind = BindingHelper.createbinding(produksjonCheck, "selected", plugin.getQueryParams(), "produksjon");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> sprederBind = BindingHelper.createbinding(sprederCheck, "selected", plugin.getQueryParams(), "sprederdata");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> hideIdBind = BindingHelper.createbinding(skjulIdCheck, "selected", plugin.getQueryParams(), "hideId");

		plugin.getFieldToolkit().addBinding(hastighetBind);
		plugin.getFieldToolkit().addBinding(veiBind);
		plugin.getFieldToolkit().addBinding(veinummerBind);
		plugin.getFieldToolkit().addBinding(rodeBind);
		plugin.getFieldToolkit().addBinding(produksjonBind);
		plugin.getFieldToolkit().addBinding(sprederBind);
		plugin.getFieldToolkit().addBinding(hideIdBind);
	}
}
