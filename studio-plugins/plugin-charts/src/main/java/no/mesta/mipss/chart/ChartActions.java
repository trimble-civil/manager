package no.mesta.mipss.chart;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.chart.swing.MipssChartPanel;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.chartservice.MipssCharts;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;

public class ChartActions {
	private PropertyResourceBundleUtil bundle = PropertyResourceBundleUtil.getPropertyBundle("grafText");

	public enum ChartActionType{
		SINGLE,
		MULTIPLE
	}

	private ChartModule plugin;
	private MipssChartPanel contentPane;
	private ChartQueryParams params;
	
	public ChartActions(ChartModule plugin, MipssChartPanel contentPane) {
		this.plugin = plugin;
		this.contentPane = contentPane;
	}
	public ChartActions(ChartModule plugin, ChartQueryParams params, MipssChartPanel contentPane) {
		this.plugin = plugin;
		this.contentPane = contentPane;
		this.params = params;
	}
	public void setContentPane(MipssChartPanel contentPane){
		this.contentPane = contentPane;
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		this.plugin = null;
		this.params = null;
		this.contentPane = null;
	}
	@SuppressWarnings("serial")
	private class GetSingleVechicleChartAction extends AbstractAction{

		public GetSingleVechicleChartAction(String text, Icon icon){
			super(text, icon);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Long kjoretoyId = null;
			if (params!=null){
				kjoretoyId = params.getKjoretoyId();
			}else
				kjoretoyId = plugin.getQueryParams().getKjoretoyId();
			if(kjoretoyId!=null){
				contentPane.waitForChart();
				final ChartWorker worker = new ChartWorker();
				worker.execute();
				new Thread(){
					public void run(){
						try {
							contentPane.setChart(worker.get());
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						}
					}
				}.start();
			}else{
				JOptionPane.showMessageDialog(plugin.getModuleGUI(), bundle.getGuiString("kjoretoy.ikkevalgt"), bundle.getGuiString("kjoretoy.ikkevalgt.title"), JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
	}
	private class ChartWorker extends SwingWorker<JFreeChart, Integer>{

		@Override
		protected JFreeChart doInBackground() throws Exception {
			try{
				ChartQueryParams p = params==null?plugin.getQueryParams():params;
				MipssCharts bean = BeanUtil.lookup(MipssCharts.BEAN_NAME, MipssCharts.class);
				JFreeChart chart = bean.getChart(p);
				if (chart!=null)
					chart.setAntiAlias(false);
				return chart;
			}catch (Throwable t){
				plugin.getLoader().handleException(t, this, "En feil oppstod ved henting av data til grafen");
			}
			return null;
		}
	}
	
	public AbstractAction getChartAction(ChartActionType type){
		AbstractAction action;
		switch(type){
			case SINGLE: action = new GetSingleVechicleChartAction("Hent graf", null);
				break;
			case MULTIPLE: action = new GetMultipleVechicleChartAction("Hent graf", null);
				break;
			default:action = null;
				break;
		}
		return action;
		
	}
	
	@SuppressWarnings("serial")
	private class GetMultipleVechicleChartAction extends AbstractAction{

		public GetMultipleVechicleChartAction(String text, Icon icon){
			super(text, icon);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			contentPane.waitForChart();
			final KjoretoyChartWorker worker = new KjoretoyChartWorker();
			worker.execute();
			new Thread(){
				public void run(){
					try {
						contentPane.setChart(worker.get());
					} catch (Exception e) {
						e.printStackTrace();
						plugin.getLoader().handleException(e, this, "En feil oppstod ved henting av data til grafen");
						contentPane.unWaitForChart();
					} 
				}
			}.start();
			
		}
		
	}
	
	private class KjoretoyChartWorker extends SwingWorker<JFreeChart, Integer>{

		@Override
		protected JFreeChart doInBackground() throws Exception {
			MipssCharts bean = BeanUtil.lookup(MipssCharts.BEAN_NAME, MipssCharts.class);
			JFreeChart chart = bean.getKjoretoyChart(params==null?plugin.getQueryParams():params);
			return chart;
		}
	}
}
