package no.mesta.mipss.chart.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import no.mesta.mipss.chart.ChartActions;
import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.KjoretoySokPanel;
import no.mesta.mipss.ui.UIUtils;
import no.mesta.mipss.ui.table.KjoretoyTableObject;

import org.jdesktop.swingx.decorator.HighlighterFactory;

@SuppressWarnings("serial")
public class SingleVehiclePanel extends JPanel implements VehicleChartPanel{

	private KjoretoySokPanel kjoretoyPanel;
	private SubchartSelectionPanel subchartPanel;
	private ChartModule plugin;
	private MipssChartPanel singleChartPanel;
	private JPanel topPanel;
	private JPanel chartPanel;
	private PropertyResourceBundleUtil bundle = PropertyResourceBundleUtil.getPropertyBundle("grafText");
	
	public SingleVehiclePanel(ChartModule plugin) {
		this.plugin = plugin;
		initGui();
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		plugin = null;
		for (MouseListener m:kjoretoyPanel.getKjoretoyTable().getMouseListeners()){
			kjoretoyPanel.getKjoretoyTable().removeMouseListener(m);
		}
		kjoretoyPanel = null;
		subchartPanel.dispose();
		subchartPanel = null;
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		Driftkontrakt k = plugin.getLoader().getSelectedDriftkontrakt();
		kjoretoyPanel = new KjoretoySokPanel(k, false, true, true, new String[] {"enhetId", "beskrivelse", "type", "modell", "leverandor", "regnr" });
		kjoretoyPanel.getKjoretoyTable().setGridColor(new Color(0,0,0,0));

		kjoretoyPanel.addPropertyChangeListener("kjoretoy", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				KjoretoyTableObject obj = (KjoretoyTableObject)evt.getNewValue();
				if (obj!=null && obj.getKjoretoy()!=null){
					plugin.getQueryParams().setKjoretoyId(obj.getKjoretoy().getId());
				}else{
					plugin.getQueryParams().setKjoretoyId(null);
				}
			}
			
		});
		plugin.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getNewValue()!=null)
					kjoretoyPanel.getKjoretoy();			
			}
		});
		kjoretoyPanel.getKjoretoyTable().setHighlighters(HighlighterFactory.createAlternateStriping());
		kjoretoyPanel.setBorder(BorderFactory.createTitledBorder(bundle.getGuiString("kjoretoyutvalg")));
		Dimension size = new Dimension(500, 200);
		kjoretoyPanel.setPreferredSize(size);
		kjoretoyPanel.setMinimumSize(size);
		kjoretoyPanel.setMaximumSize(size);
		singleChartPanel = new MipssChartPanel(plugin, "Graf", ChartActions.ChartActionType.SINGLE, this);
		
		subchartPanel = new SubchartSelectionPanel(plugin, singleChartPanel);
		subchartPanel.setBorder(BorderFactory.createTitledBorder("Grafutvalg"));
//		topPanel = UIUtils.getHorizPanel();
		topPanel = new JPanel(new GridBagLayout());
		
		topPanel.add(kjoretoyPanel, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		topPanel.add(subchartPanel, new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		
		chartPanel = UIUtils.getHorizPanel();
		chartPanel.add(singleChartPanel);
		add(topPanel, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));
		add(chartPanel, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));

	}
	@Override
	public void setEnabled(boolean enabled){
		kjoretoyPanel.setEnabled(enabled);
		subchartPanel.setEnabled(enabled);
		
	}
	public void setChart(ChartPanel panel){
		singleChartPanel.getContentPane().setViewportView(panel);
	}
	
	public MipssChartPanel getSingleChartPanel(){
		return singleChartPanel;
	}
	public KjoretoySokPanel getKjoretoyPanel() {
		return kjoretoyPanel;
	}
	@Override
	public void setChartPanel(MipssChartPanel panel) {
		singleChartPanel = panel;
		subchartPanel.setChartPanel(singleChartPanel);
		if (chartPanel!=null)
			remove(chartPanel);
		
		chartPanel = UIUtils.getHorizPanel();
		chartPanel.add(singleChartPanel);
		add(chartPanel, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		validate();
	}
}
