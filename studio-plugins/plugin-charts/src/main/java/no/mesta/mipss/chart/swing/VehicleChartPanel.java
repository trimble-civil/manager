package no.mesta.mipss.chart.swing;

public interface VehicleChartPanel {

	void setChartPanel(MipssChartPanel panel);
	void repaint();
}
