package no.mesta.mipss.chart.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.KjoretoygruppeService;
import no.mesta.mipss.chart.ChartActions;
import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chart.ChartActions.ChartActionType;
import no.mesta.mipss.chartservice.ChartQueryParams;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

import org.jdesktop.beansbinding.Binding;

@SuppressWarnings("serial")
public class MultipleVehiclePanel extends JPanel implements VehicleChartPanel{
	private JCheckBox kunProdCheck;
	private JCheckBox hideIdCheck;
	private JCheckBox kunKontraktKjortoyCheck;
	
	private JLabel lblKjoretoygruppe;
	private MipssComboBoxModel<Kjoretoygruppe> modelGruppe;
	private JComboBox cmbGruppe;
	
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("grafText");
	private ChartModule plugin;
	private MipssChartPanel multiChartPanel;
	private ChartActions chartActions;
	private JPanel chartPanel;
	private JPanel topPanel;
	
	public MultipleVehiclePanel(ChartModule plugin) {
		this.plugin = plugin;
		initComponents();
		initGui();
	}
	private void initComponents() {
		plugin.addPropertyChangeListener("valgtKontrakt", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				modelGruppe.setEntities(getKjoretoygruppeForKontrakt());
			}
		});
		
		modelGruppe = new MipssComboBoxModel<Kjoretoygruppe>(getKjoretoygruppeForKontrakt(), true);
		cmbGruppe = new JComboBox(modelGruppe);
		Dimension fieldSize = new Dimension(153, 20);
		cmbGruppe.setPreferredSize(fieldSize);
		cmbGruppe.setMaximumSize(fieldSize);
		cmbGruppe.setMinimumSize(fieldSize);
        MipssListCellRenderer<Kjoretoygruppe> renderer = new MipssListCellRenderer<Kjoretoygruppe>();
        cmbGruppe.setRenderer(renderer);
		lblKjoretoygruppe = new JLabel(resources.getGuiString("label.kjoretoygruppe"));
	}
	private List<Kjoretoygruppe> getKjoretoygruppeForKontrakt(){
		KjoretoygruppeService gruppeBean = BeanUtil.lookup(KjoretoygruppeService.BEAN_NAME, KjoretoygruppeService.class);
		if (plugin.getValgtKontrakt()==null){
			return new ArrayList<Kjoretoygruppe>();
		}
		List<Kjoretoygruppe> kjoretoygruppe = gruppeBean.getKjoretoygruppe(plugin.getValgtKontrakt().getId());
		return kjoretoygruppe;
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
	public void dispose(){
		plugin = null;
		chartActions.dispose();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		kunProdCheck = new JCheckBox(resources.getGuiString("criteria.medprod"));
		hideIdCheck = new JCheckBox(resources.getGuiString("criteria.hideId"));
		kunKontraktKjortoyCheck = new JCheckBox(resources.getGuiString("criteria.kunKontraktKjoretoy"));
		
		multiChartPanel = new MipssChartPanel(plugin, resources.getGuiString("multiplevehiclepanel.title"), ChartActions.ChartActionType.MULTIPLE, this);
		setChartPanel(multiChartPanel);
		bind();
		kunProdCheck.setSelected(true);
		kunKontraktKjortoyCheck.setSelected(true);
	}

	private void bind() {
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> kunProdBind = BindingHelper.createbinding(kunProdCheck, "selected", plugin.getQueryParams(), "kunProduksjon");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> kunKontraktKjoretoy = BindingHelper.createbinding(kunKontraktKjortoyCheck, "selected", plugin.getQueryParams(), "kunKontraktKjoretoy");
		Binding<JCheckBox, Boolean, ChartQueryParams, Boolean> hideIdBind = BindingHelper.createbinding(hideIdCheck, "selected", plugin.getQueryParams(), "hideId");
		Binding<JComboBox, Kjoretoygruppe, ChartQueryParams, Kjoretoygruppe> gruppeBind = BindingHelper.createbinding(cmbGruppe, "selectedItem", plugin.getQueryParams(), "kjoretoygruppe");
		plugin.getFieldToolkit().addBinding(kunProdBind);
		plugin.getFieldToolkit().addBinding(kunKontraktKjoretoy);
		plugin.getFieldToolkit().addBinding(hideIdBind);
		plugin.getFieldToolkit().addBinding(gruppeBind);
	}
	

	@Override
	public void setChartPanel(MipssChartPanel panel) {
		multiChartPanel = panel;
		if (chartPanel!=null)
			remove(chartPanel);
		if (topPanel!=null)
			remove(topPanel);
		
		
		chartActions = new ChartActions(plugin, panel);
		JButton getChartButton = new JButton(chartActions.getChartAction(ChartActionType.MULTIPLE));
		
		topPanel = new JPanel(new GridBagLayout());
		topPanel.add(kunProdCheck, 			  new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		topPanel.add(kunKontraktKjortoyCheck, new GridBagConstraints(1,0, 2,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		topPanel.add(hideIdCheck, 			  new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		topPanel.add(lblKjoretoygruppe, 	  new GridBagConstraints(1,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,10,0,0), 0,0));
		topPanel.add(cmbGruppe, 			  new GridBagConstraints(2,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		
		topPanel.add(getChartButton, 			new GridBagConstraints(0,2, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		
		chartPanel = new JPanel(new GridBagLayout()); 
		chartPanel.add(multiChartPanel, new GridBagConstraints(0,0, 1,1,1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		
		add(topPanel, 	new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0,0,0), 0,0));
		add(chartPanel, new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 0,0,0), 0,0));
	}
}
