package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssEntityRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import org.jdesktop.swingx.decorator.SortOrder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class RegistreringTabPanel extends JPanel implements LoadingPanel {

    private MipssBeanTableModel<Avvik> avvikTableModel;
    private JMipssBeanTable<Avvik> avvikTable;

    private MipssBeanTableModel<Hendelse> hendelseTableModel;
    private JMipssBeanTable<Hendelse> hendelseTable;

    private MipssBeanTableModel<Forsikringsskade> forsikringsskadeTableModel;
    private JMipssBeanTable<Forsikringsskade> forsikringsskadeTable;

    private MipssBeanTableModel<Skred> skredTableModel;
    private JMipssBeanTable<Skred> skredTable;

    private JTabbedPane tabRegistreringer;
    private final FeltController controller;

    public RegistreringTabPanel(FeltController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        List<Avvik> avvikList = new ArrayList<Avvik>();
        List<Hendelse> hendelseList = new ArrayList<Hendelse>();
        List<Forsikringsskade> fsList = new ArrayList<Forsikringsskade>();
        List<Skred> skredList = new ArrayList<Skred>();
        for (Sak s : inspeksjon.getSakList()) {
            avvikList.addAll(s.getAvvik());
            if (s.getHendelse() != null)
                hendelseList.add(s.getHendelse());
            if (s.getForsikringsskade() != null)
                fsList.add(s.getForsikringsskade());
            if (s.getSkred() != null)
                skredList.add(s.getSkred());
        }
        avvikTableModel.setEntities(avvikList);
        hendelseTableModel.setEntities(hendelseList);
        forsikringsskadeTableModel.setEntities(fsList);
        skredTableModel.setEntities(skredList);
        tabRegistreringer.setTitleAt(0, "Avvik (" + avvikList.size() + ")");
        tabRegistreringer.setTitleAt(1, "Hendelse (" + hendelseList.size() + ")");
        tabRegistreringer.setTitleAt(2, "Forsikringsskade (" + fsList.size() + ")");
        tabRegistreringer.setTitleAt(3, "Skred (" + skredList.size() + ")");
    }

    private void initAvvikTable() {
        avvikTableModel = new MipssBeanTableModel<Avvik>(Avvik.class, "refnummer", "${sak.refnummer}", "sisteStatusDato", "status", "tiltaksDato", "prosess", "tilstand", "aarsak", "veiref");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Avvik.class, avvikTableModel.getColumns());
        avvikTable = new JMipssBeanTable<Avvik>(avvikTableModel, columnModel);
        initTable(avvikTable);
        avvikTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    Avvik avvik = avvikTable.getSelectedEntities().get(0);
                    AvvikSokResult rs = new AvvikSokResult();
                    rs.setGuid(avvik.getGuid());
                    rs.setSakGuid(avvik.getSakGuid());
                    rs.setSlettetDato(avvik.getSlettetDato());
                    controller.getAvvikController().openAvvik(rs);
                }
            }
        });
    }


    private void initHendelseTable() {
        hendelseTableModel = new MipssBeanTableModel<Hendelse>(Hendelse.class, "refnummer", "${sak.refnummer}", "dato", "ownedMipssEntity", "aarsak", "tiltakstekster", "veiref", "antallBilder", "henvendelseFra", "henvendelseMottattAv");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Hendelse.class, hendelseTableModel.getColumns());
        hendelseTable = new JMipssBeanTable<Hendelse>(hendelseTableModel, columnModel);
        initTable(hendelseTable);
        hendelseTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    Hendelse hendelse = hendelseTable.getSelectedEntities().get(0);
                    RskjemaSokResult rs = new RskjemaSokResult();
                    rs.setGuid(hendelse.getGuid());
                    rs.setSakGuid(hendelse.getSakGuid());
                    rs.setSlettetDato(hendelse.getSlettetDato());
                    controller.getRskjemaController().openRskjema(rs);
                }
            }
        });
    }

    private void initForsikringsskadeTable() {
        forsikringsskadeTableModel = new MipssBeanTableModel<Forsikringsskade>(Forsikringsskade.class, "refnummer", "${sak.refnummer}", "${sak.skadested}", "${sak.veiref.textForGUI}", "entrepenorNavn", "meldtAv", "skadeDato", "antattTidsromForSkaden", "fritekst");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Forsikringsskade.class, forsikringsskadeTableModel.getColumns());
        forsikringsskadeTable = new JMipssBeanTable<Forsikringsskade>(forsikringsskadeTableModel, columnModel);
        initTable(forsikringsskadeTable);
        forsikringsskadeTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    Forsikringsskade fs = forsikringsskadeTable.getSelectedEntities().get(0);
                    RskjemaSokResult rs = new RskjemaSokResult();
                    rs.setGuid(fs.getGuid());
                    rs.setSakGuid(fs.getSakGuid());
                    rs.setSlettetDato(fs.getSlettetDato());
                    controller.getRskjemaController().openRskjema(rs);
                }
            }
        });

    }

    private void initSkredTable() {
        skredTableModel = new MipssBeanTableModel<Skred>(Skred.class, "refnummer", "${sak.refnummer}", "${sak.skadested}", "${sak.veiref.textForGUI}", "opprettetAv", "skredDato", "antallBilder");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Skred.class, skredTableModel.getColumns());
        skredTable = new JMipssBeanTable<Skred>(skredTableModel, columnModel);
        initTable(skredTable);
        skredTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    Skred skr = skredTable.getSelectedEntities().get(0);
                    RskjemaSokResult rs = new RskjemaSokResult();
                    rs.setGuid(skr.getGuid());
                    rs.setSakGuid(skr.getSakGuid());
                    rs.setSlettetDato(skr.getSlettetDato());
                    controller.getRskjemaController().openRskjema(rs);
                }
            }
        });
    }

    private void initTable(JMipssBeanTable<?> table) {
        MipssEntityRenderer entityRendrer = new MipssEntityRenderer();
        table.setDefaultRenderer(Punktveiref.class, entityRendrer);
        table.setDefaultRenderer(Date.class, new DateTimeRenderer(MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
        table.setDefaultRenderer(OwnedMipssEntity.class, entityRendrer);
        table.setDefaultRenderer(Hendelseaarsak.class, entityRendrer);
        table.setDefaultRenderer(HendelseTrafikktiltak.class, entityRendrer);

        table.setDefaultRenderer(Prosess.class, entityRendrer);
        table.setDefaultRenderer(Avviktilstand.class, entityRendrer);
        table.setDefaultRenderer(Avvikaarsak.class, entityRendrer);
        table.setDefaultRenderer(AvvikstatusType.class, entityRendrer);
        table.setDefaultRenderer(AvvikStatus.class, entityRendrer);

        table.setDefaultRenderer(Prosess.class, entityRendrer);

        table.setFillsViewportHeight(true);
        table.setFillsViewportWidth(true);
        table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        table.setSortable(true);
        table.setSortOrder(0, SortOrder.ASCENDING);
        table.setHorizontalScrollEnabled(true);

    }

    private void initComponents() {
        tabRegistreringer = new JTabbedPane();

        initAvvikTable();
        initHendelseTable();
        initForsikringsskadeTable();
        initSkredTable();

//		JScrollPane scrAvvik = getScrollPane(avvikTable);
        tabRegistreringer.addTab("Avvik", new JScrollPane(avvikTable));
        tabRegistreringer.addTab("Hendelse", new JScrollPane(hendelseTable));
        tabRegistreringer.addTab("Forsikringsskade", new JScrollPane(forsikringsskadeTable));
        tabRegistreringer.addTab("Skred", new JScrollPane(skredTable));
    }

    private void initGui() {
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.registreringer")));
        setLayout(new BorderLayout());
        add(tabRegistreringer, BorderLayout.CENTER);
    }

    @Override
    public boolean isLoading() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

}
