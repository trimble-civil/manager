package no.mesta.mipss.felt.brukere;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class BrukereDialog extends JDialog {
    private BrukerePanel pnlBrukere;
    private final Driftkontrakt kontrakt;
    private boolean cancelled = true;
    private final QueryParams params;

    public BrukereDialog(QueryParams params, JFrame owner, String title) {
        super(owner, title, true);
        this.params = params;
        this.kontrakt = params.getValgtKontrakt();

        initGui();
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        pnlBrukere = new BrukerePanel(params, kontrakt);

//		JButton nullstillButton = new JButton(getNullstillAction());
        JButton okButton = new JButton(getOkAction());
        JButton cancelButton = new JButton(getCancelAction());
        JCheckBox chkAlle = new JCheckBox(Resources.getResource("label.velgAlle"));
        chkAlle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JCheckBox box = (JCheckBox) e.getSource();
                if (box.isSelected()) {
                    pnlBrukere.setAlleValgt(true);
                } else {
                    pnlBrukere.setAlleValgt(false);
                }

            }
        });
        JPanel buttonPanel = new JPanel(new GridBagLayout());
//		buttonPanel.add(nullstillButton,new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,0,5,10), 0,0));
        buttonPanel.add(chkAlle, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 10), 0, 0));
        buttonPanel.add(okButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 10), 0, 0));
        buttonPanel.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));

        add(pnlBrukere, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        pack();
    }

    public String[] getValgteBrukere() {
        if (cancelled) {
            return null;
        }
        return pnlBrukere.getAlleValgte();
    }

    public boolean isCancelled() {
        return cancelled;
    }

    private Action getOkAction() {
        return new AbstractAction("Ok", IconResources.OK2_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = false;
                BrukereDialog.this.dispose();
            }
        };
    }

    private Action getCancelAction() {
        return new AbstractAction("Avbryt", IconResources.CANCEL_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = true;
                BrukereDialog.this.dispose();
            }
        };
    }


    public void setSelectedBrukere(String[] brukerList) {
        pnlBrukere.setSelectedBrukere(brukerList);

    }

}
