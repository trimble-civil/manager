package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.mipssfelt.SakSokResult;
import no.mesta.mipss.persistence.Clock;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;
import java.util.Date;

public final class FristdatoSakHighlighter implements HighlightPredicate {

    private final SakController controller;

    public FristdatoSakHighlighter(SakController controller) {
        this.controller = controller;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        SakSokResult srs = controller.getSakTableModel().get(controller.getSakTable().convertRowIndexToModel(adapter.row));
        if (srs.getTiltaksDato() != null) {
            Date mangelIntrefferDato = CalendarUtil.round(Clock.now(), true);
            Date mangel3Dager = CalendarUtil.getDaysAgo(mangelIntrefferDato, -3);
            if (CalendarUtil.isOverlappende(mangelIntrefferDato, mangel3Dager, srs.getTiltaksDato(), srs.getTiltaksDato(), true)) {
                if (srs.getSisteStatus() != null && srs.getSisteStatus().toLowerCase().startsWith("lukket")) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }
}