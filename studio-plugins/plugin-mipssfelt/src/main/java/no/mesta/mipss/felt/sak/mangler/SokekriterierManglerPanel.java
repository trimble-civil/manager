package no.mesta.mipss.felt.sak.mangler;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class SokekriterierManglerPanel extends JPanel {
    /**
     * 1.	Mangler der avviket ikke er lukket ved utløpet av angitt periode (”Mangler med åpent avvik”?). Denne kommer opp som default.
     */
    private JRadioButton radioAapentAvvik;
    /**
     * 2.	Alle mangler som har oppstått i angitt periode (“Nye mangler i perioden”?).
     */
    private JRadioButton radioNyeMangler;

    /**
     * Viser alle avvik som kan bli mangler.
     */
    private JRadioButton radioKommendeMangler;

    /**
     * Viser alle mangler
     */
    private JRadioButton radioAlleMangler;


    private final AvvikController controller;

    public SokekriterierManglerPanel(AvvikController controller) {
        this.controller = controller;

        initComponents();
        initGui();
        bind();
    }

    private void bind() {
        Binding<JRadioButton, Object, AvvikQueryParams, Object> aapenAvvikBind = BindingHelper.createbinding(radioAapentAvvik, "selected", controller.getQueryParams(), "aapentAvvik");
        Binding<JRadioButton, Object, AvvikQueryParams, Object> nyeManglerBind = BindingHelper.createbinding(radioNyeMangler, "selected", controller.getQueryParams(), "nyeMangler");
        Binding<JRadioButton, Object, AvvikQueryParams, Object> kommendeManglerBind = BindingHelper.createbinding(radioKommendeMangler, "selected", controller.getQueryParams(), "kommendeMangler");
        Binding<JRadioButton, Object, AvvikQueryParams, Object> alleManglerBind = BindingHelper.createbinding(radioAlleMangler, "selected", controller.getQueryParams(), "alleMangler");
        controller.getFeltController().getBindingGroup().addBinding(aapenAvvikBind);
        controller.getFeltController().getBindingGroup().addBinding(nyeManglerBind);
        controller.getFeltController().getBindingGroup().addBinding(kommendeManglerBind);
        controller.getFeltController().getBindingGroup().addBinding(alleManglerBind);
        radioAapentAvvik.setSelected(true);
    }

    private void initComponents() {
        radioAapentAvvik = new JRadioButton(Resources.getResource("check.aapentAvvik"));
        radioAapentAvvik.setToolTipText(Resources.getResource("check.aapentAvvik.tooltip"));

        radioNyeMangler = new JRadioButton(Resources.getResource("check.nyeMangler"));
        radioNyeMangler.setToolTipText(Resources.getResource("check.nyeMangler.tooltip"));

        radioKommendeMangler = new JRadioButton(Resources.getResource("check.kommendeMangler"));
        radioKommendeMangler.setToolTipText(Resources.getResource("check.kommendeMangler.tooltip"));

        radioAlleMangler = new JRadioButton(Resources.getResource("check.alleMangler"));
        radioAlleMangler.setToolTipText(Resources.getResource("check.alleMangler.tooltip"));
        ButtonGroup bg = new ButtonGroup();
        bg.add(radioAapentAvvik);
        bg.add(radioNyeMangler);
        bg.add(radioKommendeMangler);
        bg.add(radioAlleMangler);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(radioAapentAvvik, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(radioNyeMangler, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        add(radioAlleMangler, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        add(radioKommendeMangler, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    }
}
