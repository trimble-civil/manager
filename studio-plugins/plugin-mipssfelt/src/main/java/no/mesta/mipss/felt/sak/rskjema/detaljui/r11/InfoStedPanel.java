package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DateStringConverter;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.service.veinett.VeirefVO;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssSpinnerDateModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressWarnings("serial")
public class InfoStedPanel extends JPanel {
    private Font bold;
    private final R11Controller controller;
    private JTextField koordinatFraField;
    private MipssComboBoxModel<HpWrapper> fraHpModel;
    private MipssComboBoxModel<HpWrapper> tilHpModel;
    private JComboBox veiCombo;
    private JComboBox fraHpCombo;
    private JComboBox tilHpCombo;
    private JTextField mFraField;
    private JTextField mTilField;
    private MipssComboBoxModel<VeirefVO> veiModel;
    private JTextField koordinatTilField;
    private JLabel fkInfoLabel;
    private JLabel navnInfoLabel;
    private JLabel regAvInfoLabel;

    private JComboBox feltCombo;
    private Egenskap felt;
    private JTextField stedField;
    //	private JCheckBox stengtCheck;
    private VeirefVO fraVeirefUtenforKontraktsveinett;

//	private JLabel skredDatoInfoLabel;

    private JMipssDatePicker dateSkred;
    private JSpinner klSkred;
    private MipssDateTimePickerHolder skredDato;

    private JFormattedTextField fldElrappEmne;
    private JFormattedTextField fldElrappId;
    private JFormattedTextField fldElrappVersjon;
    private JFormattedTextField fldElrappDato;

    private BindingGroup bindings = new BindingGroup();

    public InfoStedPanel(R11Controller controller) {
        this.controller = controller;
        initGui();
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(createElrappPanel(), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(3, 5, 0, 5), 0, 0));
        add(createHeaderPanel(), new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
//		add(createEmnePanel(), 	new GridBagConstraints(0,2, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,5), 0,0));
        add(createStedPanel(), new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));

    }

    private void checkStedfesting(Skred skred) {
        List<Punktstedfest> stedfestingList = skred.getSak().getStedfesting();
        if (stedfestingList.size() < 2) {
            Punktstedfest sted = stedfestingList.get(0);
            stedfestingList.add(controller.cloneStedfest(sted));
        }
        Punktstedfest tilPunkt = stedfestingList.get(1);

        if (tilPunkt.getX() == null & tilPunkt.getY() == null && tilPunkt.getSnappetX() == null && tilPunkt.getSnappetY() == null) {
            JOptionPane.showMessageDialog(null, Resources.getResource("message.manglerKoordinater"), Resources.getResource("message.manglerKoordinater.title"), JOptionPane.INFORMATION_MESSAGE);
            stedfestingList.remove(1);
            stedfestingList.add(controller.cloneStedfest(stedfestingList.get(0)));
        }
        if (tilPunkt.getVeiref() == null) {
            Punktveiref punktveiref = controller.getVeireferanse(tilPunkt);
            if (punktveiref != null) {
                tilPunkt.addPunktveiref(punktveiref);
            } else {
                //TODO show MESSAGE!!
            }
        }
    }

    public void setSkred(Skred skred) {
        checkStedfesting(skred);
        veiCombo.setModel(createVeiModel());
        Driftkontrakt kontrakt = skred.getSak().getKontrakt();
        String nr = kontrakt.getKontraktnummer();
        String navn = kontrakt.getKontraktnavn();
        String fraDato = MipssDateFormatter.formatDate(kontrakt.getGyldigFraDato(), "yyyy");
        String tilDato = MipssDateFormatter.formatDate(kontrakt.getGyldigTilDato(), "yyyy");
        String regnavn = skred.getOpprettetAv();
//		String dato = MipssDateFormatter.formatDate(skred.getSkredDato(), "dd.MM.yyyy HH:mm");
        fkInfoLabel.setText("FK" + nr);
        navnInfoLabel.setText(navn + " " + fraDato + "-" + tilDato);
        regAvInfoLabel.setText(regnavn);


//		skredDatoInfoLabel.setText(dato);

        final EgenskapItemListener listener = new EgenskapItemListener(felt, skred);
        feltCombo.addItemListener(listener);
        feltCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getItem() instanceof Egenskapverdi) {
                    Egenskapverdi verdi = (Egenskapverdi) e.getItem();
                    controller.getFraHp().setKjorefelt(verdi.getVerdi());
                    controller.getTilHp().setKjorefelt(verdi.getVerdi());
                }
                if (e.getItem() == null) {
                    controller.getFraHp().setKjorefelt(null);
                    controller.getTilHp().setKjorefelt(null);
                }
                listener.getClass();
            }
        });
        feltCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(felt));
        //bind guivalg mot modellobjekter
        bind();
        setVeiValues();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void bind() {
        bindings.addBinding(BindingHelper.createbinding(mFraField, "text", this, "koordinatFra", BindingHelper.READ_WRITE));
        bindings.addBinding(BindingHelper.createbinding(mTilField, "text", this, "koordinatTil", BindingHelper.READ_WRITE));

        bindings.addBinding(BindingHelper.createbinding(controller.getSkred().getSak(), "skadested", stedField, "text", BindingHelper.READ_WRITE));
        bindings.addBinding(BindingHelper.createbinding(fraHpModel, "${selectedItem!=null}", mFraField, "enabled"));
        bindings.addBinding(BindingHelper.createbinding(tilHpModel, "${selectedItem!=null}", mTilField, "enabled"));
//		bindings.addBinding(BindingHelper.createbinding(controller.getSkred(), "stengtPgaSkredfare", stengtCheck, "selected", BindingHelper.READ_WRITE));

        bindings.addBinding(BindingHelper.createbinding(controller.getSkred(), "skredDato", skredDato, "date", BindingHelper.READ_WRITE));


        Binding<Skred, Object, JFormattedTextField, Object> emneBind = BindingHelper.createbinding(controller.getSkred(), "skjemaEmne", fldElrappEmne, "text", UpdateStrategy.READ_WRITE);
        Binding<Skred, Object, JFormattedTextField, Object> elrappIdBind = BindingHelper.createbinding(controller.getSkred(), "elrappDokumentIdent", fldElrappId, "text");
        Binding<Skred, Object, JFormattedTextField, Object> elrappVersjonBind = BindingHelper.createbinding(controller.getSkred(), "elrappVersjon", fldElrappVersjon, "text");
        Binding rapportertDatoBind = BindingHelper.createbinding(controller.getSkred(), "rapportertDato", fldElrappDato, "text");
        rapportertDatoBind.setConverter(new DateStringConverter(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

        bindings.addBinding(emneBind);
        bindings.addBinding(elrappIdBind);
        bindings.addBinding(elrappVersjonBind);
        bindings.addBinding(rapportertDatoBind);

        bindings.bind();
    }

    private JPanel createElrappPanel() {
        JPanel pnl = new JPanel(new GridBagLayout());

        JLabel lblEmne = new JLabel(Resources.getResource("label.elrappEmne"));
        JLabel lblId = new JLabel(Resources.getResource("label.elrappId"));
        JLabel lblVersjon = new JLabel(Resources.getResource("label.elrappVersjon"));
        JLabel lblDato = new JLabel(Resources.getResource("label.elrappRapportertDato"));

        fldElrappEmne = new JFormattedTextField();
        fldElrappId = new JFormattedTextField();
        fldElrappVersjon = new JFormattedTextField();
        fldElrappDato = new JFormattedTextField();
        fldElrappDato.setEditable(false);
        fldElrappId.setEditable(false);
        fldElrappVersjon.setEditable(false);

        pnl.add(lblEmne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(fldElrappEmne, new GridBagConstraints(1, 0, 3, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));

        pnl.add(lblId, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(fldElrappId, new GridBagConstraints(1, 1, 1, 1, 0.2, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        pnl.add(lblVersjon, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnl.add(fldElrappVersjon, new GridBagConstraints(3, 1, 1, 1, 0.2, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));

        pnl.add(lblDato, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(fldElrappDato, new GridBagConstraints(1, 2, 1, 1, 0.2, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

        pnl.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.elrapp")));

        return pnl;
    }

    private JPanel createHeaderPanel() {

//		Skred skred = controller.getSkred();

        JPanel panel = new JPanel(new GridBagLayout());

        JLabel fkLabel = new JLabel(Resources.getResource("label.r11.fk"));
        JLabel navnLabel = new JLabel(Resources.getResource("label.r11.navn"));
        JLabel regAvLabel = new JLabel(Resources.getResource("label.r11.regAv"));
        JLabel skredDatoLabel = new JLabel(Resources.getResource("label.r11.skreddato"));

        bold = fkLabel.getFont();
        bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
        fkLabel.setFont(bold);
        navnLabel.setFont(bold);
        regAvLabel.setFont(bold);
        skredDatoLabel.setFont(bold);


        fkInfoLabel = new JLabel();
        navnInfoLabel = new JLabel();
        regAvInfoLabel = new JLabel();
//		skredDatoInfoLabel = new JLabel();

        dateSkred = new JMipssDatePicker();
        klSkred = new JSpinner(new MipssSpinnerDateModel(null, null, null, Calendar.MINUTE));
        klSkred.setEditor(new JSpinner.DateEditor(klSkred, "HH:mm"));

        skredDato = new MipssDateTimePickerHolder(dateSkred, klSkred);

        JPanel pnlSkredDato = new JPanel(new GridBagLayout());
        pnlSkredDato.add(dateSkred, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlSkredDato.add(klSkred, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 2, 0, 0), 0, 0));

        panel.add(fkLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        panel.add(fkInfoLabel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));

        panel.add(navnLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        panel.add(navnInfoLabel, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));

        panel.add(regAvLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        panel.add(regAvInfoLabel, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));

        panel.add(skredDatoLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        panel.add(pnlSkredDato, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));

        panel.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.headerTitle")));
        return panel;
    }

    private JPanel createStedPanel() {
        JPanel panel = new JPanel(new GridBagLayout());
        JLabel stedLabel = new JLabel(Resources.getResource("label.r11.stedsnavn"));
        JLabel veiLabel = new JLabel(Resources.getResource("label.r11.vei"));
        JLabel fraHpLabel = new JLabel(Resources.getResource("label.r11.fraHp"));
        JLabel tilHpLabel = new JLabel(Resources.getResource("label.r11.tilHp"));
        JLabel m1Label = new JLabel(Resources.getResource("label.r11.m"));
        JLabel m2Label = new JLabel(Resources.getResource("label.r11.m"));
        JLabel feltLabel = new JLabel(Resources.getResource("label.r11.felt"));
        JLabel koordinatFraLabel = new JLabel(Resources.getResource("label.r11.koordinatFra"));
        JLabel koordinatTilLabel = new JLabel(Resources.getResource("label.r11.koordinatTil"));

        stedField = new JTextField();
        veiCombo = createVeiCombo();
        fraHpCombo = createFraHpCombo();
        tilHpCombo = createTilHpCombo();
        feltCombo = new JComboBox();
//		feltCombo.setRenderer(new MipssListCellRenderer<IRenderableMipssEntity>("<Velg felt>"));

//		stengtCheck = new JCheckBox(Resources.getResource("label.r11.stengtRas"));
        koordinatFraField = new JTextField();
        koordinatFraField.setEditable(false);
        koordinatTilField = new JTextField();
        koordinatTilField.setEditable(false);
        mFraField = new JTextField();
        mTilField = new JTextField();

        //koble gui komponenter mot datamodeller
        Dimension size = fraHpCombo.getPreferredSize();
        size.width = 50;
        fraHpCombo.setPreferredSize(size);
        tilHpCombo.setPreferredSize(size);

        fraHpModel = new MipssComboBoxModel<HpWrapper>(new ArrayList<HpWrapper>());
        tilHpModel = new MipssComboBoxModel<HpWrapper>(new ArrayList<HpWrapper>());


        fraHpCombo.setModel(fraHpModel);
        tilHpCombo.setModel(tilHpModel);

        felt = controller.findEgenskap(R11Panel.FELT_ID);
        MipssComboBoxModel<Egenskapverdi> feltModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(felt), true);
        feltCombo.setModel(feltModel);

        //plasser ut gui
        panel.add(stedLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(stedField, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));

        panel.add(veiLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(veiCombo, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(fraHpLabel, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(fraHpCombo, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(m1Label, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(mFraField, new GridBagConstraints(6, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(tilHpLabel, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(tilHpCombo, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(m2Label, new GridBagConstraints(9, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(mTilField, new GridBagConstraints(10, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));

        panel.add(feltLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(feltCombo, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
//		panel.add(stengtCheck, 	new GridBagConstraints(3,2, 5,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,0,5), 0,0));

        panel.add(koordinatFraLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(koordinatFraField, new GridBagConstraints(1, 3, 10, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));

        panel.add(koordinatTilLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(koordinatTilField, new GridBagConstraints(1, 4, 10, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));


        panel.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.stedTitle")));
        return panel;
    }

    public void setKoordinatFra(String txtIgnoreThis) {
        new SwingWorker<Void, Void>() {
            private String value;

            public Void doInBackground() {

                Veinettveireferanse c1 = null;
                if (!mFraField.getText().equals("")) {
                    HpWrapper fraHp = fraHpModel.getSelectedItem();
                    Integer fra = Integer.valueOf(mFraField.getText());
                    c1 = new Veinettveireferanse();
                    c1.setFraKm(Double.valueOf((double) fra / 1000));
                    c1.setTilKm(Double.valueOf((double) fra / 1000));
                    c1.setFylkesnummer(fraHp.getVeiref().getFylkesnummer());
                    c1.setKommunenummer(fraHp.getVeiref().getKommunenummer());
                    c1.setHp(fraHp.getVeiref().getHp());
                    c1.setVeikategori(fraHp.getVeiref().getVeitype().charAt(0) + "");
                    c1.setVeistatus(fraHp.getVeiref().getVeitype().charAt(1) + "");
                    c1.setVeinummer(fraHp.getVeiref().getVeinummer());
                }
                value = controller.getKoordinater(c1, controller.getFraHp().getPunktstedfest(), controller.getFraHp());
                return null;
            }

            @Override
            public void done() {
                koordinatFraField.setText(value);
            }
        }.execute();
    }

    public void setKoordinatTil(String txtIgnoreThis) {
        new SwingWorker<Void, Void>() {
            private String value;

            public Void doInBackground() {
                HpWrapper tilHp = tilHpModel.getSelectedItem();

                Veinettveireferanse c2 = null;
                if (!mTilField.getText().equals("")) {
                    Integer til = Integer.valueOf(mTilField.getText());
                    c2 = new Veinettveireferanse();
                    c2.setFraKm(Double.valueOf((double) til / 1000));
                    c2.setTilKm(Double.valueOf((double) til / 1000));
                    c2.setFylkesnummer(tilHp.getVeiref().getFylkesnummer());
                    c2.setKommunenummer(tilHp.getVeiref().getKommunenummer());
                    c2.setHp(tilHp.getVeiref().getHp());
                    c2.setVeikategori(tilHp.getVeiref().getVeitype().charAt(0) + "");
                    c2.setVeistatus(tilHp.getVeiref().getVeitype().charAt(1) + "");
                    c2.setVeinummer(tilHp.getVeiref().getVeinummer());
                }
                value = controller.getKoordinater(c2, controller.getTilHp().getPunktstedfest(), controller.getTilHp());
                return null;
            }

            @Override
            public void done() {
                koordinatTilField.setText(value);
            }
        }.execute();
    }

    private void setVeiValues() {

        VeirefVO selectedFraVei = null;
        Punktveiref fraRef = controller.getFraHp();
        Punktveiref tilRef = controller.getTilHp();

        for (VeirefVO veiref : veiModel.getEntities()) {
            if (isVeirefEqual(veiref, fraRef)) {
                selectedFraVei = veiref;
            }
        }
        veiCombo.setSelectedItem(selectedFraVei);

        HpWrapper fraHp = null;
        HpWrapper tilHp = null;
        for (HpWrapper w : fraHpModel.getEntities()) {
            if (w.getVeiref().getHp().intValue() == fraRef.getHp()) {
                fraHp = w;
            }
        }
        if (fraHp == null) {
            fraVeirefUtenforKontraktsveinett = createVeirefVO(fraRef);
            veiModel.getEntities().add(fraVeirefUtenforKontraktsveinett);
            veiCombo.setSelectedItem(fraVeirefUtenforKontraktsveinett);

            fraHp = new HpWrapper(fraVeirefUtenforKontraktsveinett);
            fraHp.setMarked(true);
            fraHpModel.getEntities().add(fraHp);


        }
        for (HpWrapper w : tilHpModel.getEntities()) {
            if (w.getVeiref().getHp().intValue() == tilRef.getHp()) {
                tilHp = w;
            }
        }
        //ikke en del av kontraktsveinettet
        if (tilHp == null) {
            VeirefVO til = createVeirefVO(tilRef);
            tilHp = new HpWrapper(til);
            tilHp.setMarked(true);
            tilHpModel.getEntities().add(tilHp);
        }

        fraHpCombo.setSelectedItem(fraHp);
        tilHpCombo.setSelectedItem(tilHp);

        BindingHelper.createbinding(fraHpModel, "selectedItem", this, "fraHp", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(tilHpModel, "selectedItem", this, "tilHp", BindingHelper.READ_WRITE).bind();
        //disse feltene kan ikke bindes ettersom den faktisk meterverdien kun kan settes etter at
        //vi har hentet reflinkseksjoner fra nvdb i controlleren.
        mFraField.setText(controller.getFraHp().getMeter() + "");
        mTilField.setText(controller.getTilHp().getMeter() + "");
    }

    private VeirefVO createVeirefVO(Punktveiref vr) {
        VeirefVO v = new VeirefVO();
        v.setFylkesnummer(Long.valueOf(vr.getFylkesnummer()));
        v.setKommunenummer(Long.valueOf(vr.getKommunenummer()));
        v.setVeitype(vr.getVeikategori() + vr.getVeistatus());
        v.setVeinummer(Long.valueOf(vr.getVeinummer()));
        v.setHp(Long.valueOf(vr.getHp()));
        return v;

    }

    /**
     * custom bindingsmetode for å sette verdien på punktveirefobjektet når bruker velger i comboboxen
     *
     * @param hp
     */
    public void setFraHp(HpWrapper hp) {
        if (hp != null)
            controller.getFraHp().setHp(hp.getVeiref().getHp().intValue());
    }

    /**
     * custom bindingsmetode for å sette verdien på punktveirefobjektet når bruker velger i comboboxen
     *
     * @param hp
     */
    public void setTilHp(HpWrapper hp) {
        if (hp != null)
            controller.getTilHp().setHp(hp.getVeiref().getHp().intValue());
    }

    private boolean isVeirefEqual(VeirefVO ref, Punktveiref vei) {
        return new EqualsBuilder()
                .append(ref.getFylkesnummer().intValue(), vei.getFylkesnummer())
                .append(ref.getKommunenummer().intValue(), vei.getKommunenummer())
                .append(ref.getVeitype(), vei.getVeikategori() + vei.getVeistatus())
                .append(ref.getVeinummer().intValue(), vei.getVeinummer())
                //.append(ref.getHp().intValue(), vei.getHp())
                .isEquals();
    }

    private MipssComboBoxModel<VeirefVO> createVeiModel() {
        List<VeirefVO> veirefList = new ArrayList<VeirefVO>();
        veiModel = new MipssComboBoxModel<VeirefVO>(veirefList);
        for (VeirefVO v : controller.getVeirefList()) {
            if (!veirefList.contains(v)) {
                veirefList.add(v);
            }
        }
        return veiModel;
    }

    private JComboBox createVeiCombo() {
        JComboBox veiCombo = new JComboBox();
        veiCombo.setRenderer(new VeirefComboRenderer());
        veiCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getItem() instanceof VeirefVO) {
                    VeirefVO v = (VeirefVO) e.getItem();

                    List<HpWrapper> entities = new ArrayList<HpWrapper>();
                    for (VeirefVO vo : controller.getVeirefList()) {
                        if (vo.equals(v)) {
                            entities.add(new HpWrapper(vo));
                        }
                    }
                    fraHpModel.setEntities(entities);
                    tilHpModel.setEntities(entities);
                } else {
                    fraHpModel.setEntities(new ArrayList<HpWrapper>());
                    tilHpModel.setEntities(new ArrayList<HpWrapper>());
                }
                setKoordinatFra("");
                setKoordinatTil("");
            }
        });
        return veiCombo;
    }

    private JComboBox createFraHpCombo() {
        JComboBox fraHpCombo = new JComboBox();
        fraHpCombo.setRenderer(new HpComboRenderer());
        fraHpCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setKoordinatFra("");
                setKoordinatTil("");
            }

        });
        return fraHpCombo;
    }

    private JComboBox createTilHpCombo() {
        JComboBox tilHpCombo = new JComboBox();
        tilHpCombo.setRenderer(new HpComboRenderer());
        tilHpCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setKoordinatFra("");
                setKoordinatTil("");
            }
        });
        return tilHpCombo;
    }

    class VeirefComboRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (value != null) {
                VeirefVO w = (VeirefVO) value;
                if (w.equals(fraVeirefUtenforKontraktsveinett)) {
                    label.setBackground(Color.red);
                } else {
                    if (isSelected) {
                        setBackground(list.getSelectionBackground());
                        setForeground(list.getSelectionForeground());
                    } else {
                        setBackground(list.getBackground());
                        setForeground(list.getForeground());
                    }
                }
            }
            return label;
        }
    }

    class HpComboRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value != null) {
                HpWrapper w = (HpWrapper) value;
                if (w.isMarked())
                    label.setBackground(Color.red);
                else {
                    if (isSelected) {
                        setBackground(list.getSelectionBackground());
                        setForeground(list.getSelectionForeground());
                    } else {
                        setBackground(list.getBackground());
                        setForeground(list.getForeground());
                    }
                }
            }
            return label;
        }
    }

    class HpWrapper implements IRenderableMipssEntity {
        private VeirefVO veiref;
        private boolean marked;

        public HpWrapper(VeirefVO veiref) {
            this.veiref = veiref;
        }

        public void setMarked(boolean marked) {
            this.marked = marked;
        }

        public boolean isMarked() {
            return this.marked;
        }

        public VeirefVO getVeiref() {
            return veiref;
        }

        @Override
        public String toString() {
            return String.valueOf(veiref.getHp());
        }

        @Override
        public String getTextForGUI() {
            return String.valueOf(veiref.getHp());
        }
    }
}
