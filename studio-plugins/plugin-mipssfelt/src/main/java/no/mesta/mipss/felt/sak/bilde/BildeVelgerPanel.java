package no.mesta.mipss.felt.sak.bilde;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.resources.images.IconResources;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class BildeVelgerPanel extends JXPanel {

    private final List<Bilde> bilder;

    private List<BildePanel> bildePanels = new ArrayList<BildePanel>();

    public BildeVelgerPanel(List<Bilde> bilder) {
        this.bilder = bilder;
        initComponents();
        initGui();

    }

    private void initComponents() {
        for (Bilde b : bilder) {
            bildePanels.add(new BildePanel(b, b.getFilnavn()));
        }
    }

    private JXHeader createHeader() {
        JXHeader head = new JXHeader(Resources.getResource("message.velgBilder.title"), Resources.getResource("message.velgBilder"), IconResources.IMPORT_EXPORT_64);
        return head;
    }

    public List<Bilde> getSelectedImages() {
        List<Bilde> selected = new ArrayList<Bilde>();
        for (BildePanel p : bildePanels) {
            if (p.isSelected()) {
                selected.add(p.getBilde());
            }
        }
        return selected;
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        if (!bildePanels.isEmpty()) {
            JPanel panel = new JPanel(new WrapLayout());
            panel.setSize(new Dimension(300, 1));
            JScrollPane scroll = new JScrollPane(panel);

            for (BildePanel b : bildePanels) {
                panel.add(b);
            }
            add(createHeader(), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            add(scroll, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        } else {
            JPanel panel = new JPanel();
            JLabel label = new JLabel(Resources.getResource("message.ingenBilder"));
            panel.add(label);
            JLabel lblImg = new JLabel(IconResources.EMPTY_FOLDER_256);
            add(panel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            add(lblImg, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
    }


}
