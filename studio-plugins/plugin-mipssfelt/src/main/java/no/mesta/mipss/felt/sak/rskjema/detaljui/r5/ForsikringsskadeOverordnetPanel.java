package no.mesta.mipss.felt.sak.rskjema.detaljui.r5;

import com.jgoodies.validation.view.ValidationComponentUtils;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DateStringConverter;
import no.mesta.mipss.core.DoubleStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssSpinnerDateModel;
import no.mesta.mipss.ui.UIUtils;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.jxlayer.JXLayer;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class ForsikringsskadeOverordnetPanel extends JPanel {
    private JFormattedTextField skadestedField;
    private JFormattedTextField kommuneField;
    private JFormattedTextField vegnrField;
    private JFormattedTextField hpField;
    private JFormattedTextField kmField;
    private JFormattedTextField meldtavField;
    private JFormattedTextField avhvemField;
    private JFormattedTextField langtidField;
    private JFormattedTextField krField;
    private JFormattedTextField fldSkadeAntatt;

    private JMipssDatePicker skademeldDato;
    private JMipssDatePicker skadeDato;
    private JMipssDatePicker oppdagetDato;
    private JMipssDatePicker repDato;

    private JSpinner skademeldDatoSpinner;
    private JSpinner oppdagetDatoSpinner;
    private JSpinner skadeDatoSpinner;
    private JSpinner repDatoSpinner;

    private JFormattedTextField fldElrappEmne;
    private JFormattedTextField fldElrappId;
    private JFormattedTextField fldElrappVersjon;
    private JFormattedTextField fldElrappDato;

    private JTextArea omfangText;

    private BindingGroup bg = new BindingGroup();

    private Forsikringsskade forsikringsskade;
    private final GridBagConstraints c;

    public ForsikringsskadeOverordnetPanel(GridBagConstraints c) {
//		this.forsikringsskade = forsikringsskade;
        this.c = c;
        initGui();
    }

    public void setForsikringsskade(Forsikringsskade forsikringsskade) {
        this.forsikringsskade = forsikringsskade;
        bind();
    }

    private void initComponentAnnotations() {
        ValidationComponentUtils.setMandatory(fldElrappEmne, true);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        skadestedField = new JFormattedTextField();
        kommuneField = new JFormattedTextField();
        vegnrField = new JFormattedTextField();
        hpField = new JFormattedTextField();
        kmField = new JFormattedTextField();
        skadestedField.setEnabled(false);
        kommuneField.setEnabled(false);
        vegnrField.setEnabled(false);
        hpField.setEnabled(false);
        kmField.setEnabled(false);

        skademeldDato = new JMipssDatePicker();
        meldtavField = new JFormattedTextField();

        oppdagetDato = new JMipssDatePicker();
        avhvemField = new JFormattedTextField();

        skadeDato = new JMipssDatePicker();
        fldSkadeAntatt = new JFormattedTextField();

        omfangText = new JTextArea();

        krField = new JFormattedTextField();
        krField.setDocument(new DocumentFilter(9, "0123456789.", false));
        repDato = new JMipssDatePicker();
        repDato.setDate(null);

        langtidField = new JFormattedTextField();

        oppdagetDatoSpinner = new JSpinner(new MipssSpinnerDateModel(null, null, null, Calendar.MINUTE));
        oppdagetDatoSpinner.setEditor(new JSpinner.DateEditor(oppdagetDatoSpinner, "HH:mm"));
        skadeDatoSpinner = new JSpinner(new MipssSpinnerDateModel(null, null, null, Calendar.MINUTE));
        skadeDatoSpinner.setEditor(new JSpinner.DateEditor(skadeDatoSpinner, "HH:mm"));
        skademeldDatoSpinner = new JSpinner(new MipssSpinnerDateModel(null, null, null, Calendar.MINUTE));
        skademeldDatoSpinner.setEditor(new JSpinner.DateEditor(skademeldDatoSpinner, "HH:mm"));
        repDatoSpinner = new JSpinner(new MipssSpinnerDateModel(null, null, null, Calendar.MINUTE));
        repDatoSpinner.setEditor(new JSpinner.DateEditor(repDatoSpinner, "HH:mm"));

        createOverorndetPanel();
        initComponentAnnotations();
    }

    //	public void setAntattCheck(boolean value){
//		antattCheck.setSelected(value);
//		if (value){
//			System.out.println("value before"+forsikringsskade.getSkadeDato());
//			Date dato = forsikringsskade.getSkadeDato();
//			skadeDato.setDate(dato);
//			skadeDatoSpinner.setValue(dato);
////			setSkadeDatoKl(forsikringsskade.getSkadeDato());
//			System.out.println("value."+forsikringsskade.getSkadeDato()+" "+dato);
//		}
//		
//	}
//	public boolean isAntattCheckSelected(){
//		return antattCheck.isSelected();
//	}
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void bind() {
        DoubleStringConverter dsc = new DoubleStringConverter();
        Binding entreprenorDatoEnable = BindingHelper.createbinding(oppdagetDato, "${date!=null}", oppdagetDatoSpinner, "enabled");
        Binding skadeDatoEnable = BindingHelper.createbinding(skadeDato, "${date!=null}", skadeDatoSpinner, "enabled");
        Binding meldtDatoEnable = BindingHelper.createbinding(skademeldDato, "${date!=null}", skademeldDatoSpinner, "enabled");
        Binding repDatoEnable = BindingHelper.createbinding(repDato, "${date!=null}", repDatoSpinner, "enabled");

        bg.addBinding(entreprenorDatoEnable);
        bg.addBinding(skadeDatoEnable);
        bg.addBinding(meldtDatoEnable);
        bg.addBinding(repDatoEnable);

        bg.addBinding(BindingHelper.createbinding(forsikringsskade.getSak(), "skadested", skadestedField, "text"));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade.getSak(), "kommune", kommuneField, "text"));
        if (forsikringsskade.getSak().getVeiref() != null) {
            String veinummer = forsikringsskade.getSak().getVeiref().getVeikategori() + forsikringsskade.getSak().getVeiref().getVeistatus() + forsikringsskade.getSak().getVeiref().getVeinummer();
            vegnrField.setText(veinummer);
        }
        bg.addBinding(BindingHelper.createbinding(forsikringsskade.getSak().getVeiref(), "hp", hpField, "text"));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade.getSak().getVeiref(), "km", kmField, "text"));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "meldtAv", meldtavField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "entrepenorNavn", avhvemField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "fritekst", omfangText, "text", BindingHelper.READ_WRITE));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "entrepenorDato", oppdagetDato, "date", BindingHelper.READ));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "entrepenorDato", oppdagetDatoSpinner, "value", BindingHelper.READ));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadeDato", skadeDato, "date", BindingHelper.READ));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadeDato", skadeDatoSpinner, "value", BindingHelper.READ));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "meldtDato", skademeldDato, "date", BindingHelper.READ));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "meldtDato", skademeldDatoSpinner, "value", BindingHelper.READ));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "antattTidsromForSkaden", fldSkadeAntatt, "text", BindingHelper.READ_WRITE));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "reparertDato", repDato, "date", BindingHelper.READ));

        bg.addBinding(BindingHelper.createbinding(this, "entrepenorDato", oppdagetDato, "date", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(this, "skadeDato", skadeDato, "date", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(this, "meldtDato", skademeldDato, "date", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(this, "repDato", repDato, "date", BindingHelper.READ_WRITE));

        bg.addBinding(BindingHelper.createbinding(this, "entrepenorDatoKl", oppdagetDatoSpinner, "value", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(this, "skadeDatoKl", skadeDatoSpinner, "value", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(this, "meldtDatoKl", skademeldDatoSpinner, "value", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(this, "repDatoKl", repDatoSpinner, "value", BindingHelper.READ_WRITE));

        Binding kostnadBinding = BindingHelper.createbinding(forsikringsskade, "estimatKostnad", krField, "text", BindingHelper.READ_WRITE);
        kostnadBinding.setConverter(dsc);
        bg.addBinding(kostnadBinding);
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "reparertDato", repDato, "date", BindingHelper.READ));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "utsattRepKommentar", langtidField, "text", BindingHelper.READ_WRITE));


        Binding<Forsikringsskade, Object, JFormattedTextField, Object> emneBind = BindingHelper.createbinding(forsikringsskade, "skjemaEmne", fldElrappEmne, "text", UpdateStrategy.READ_WRITE);
        Binding<Forsikringsskade, Object, JFormattedTextField, Object> elrappIdBind = BindingHelper.createbinding(forsikringsskade, "elrappDokumentIdent", fldElrappId, "text");
        Binding<Forsikringsskade, Object, JFormattedTextField, Object> elrappVersjonBind = BindingHelper.createbinding(forsikringsskade, "elrappVersjon", fldElrappVersjon, "text");
        Binding rapportertDatoBind = BindingHelper.createbinding(forsikringsskade, "rapportertDato", fldElrappDato, "text");
        rapportertDatoBind.setConverter(new DateStringConverter(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

        bg.addBinding(emneBind);
        bg.addBinding(elrappIdBind);
        bg.addBinding(elrappVersjonBind);
        bg.addBinding(rapportertDatoBind);


        bg.bind();
    }

    public void setEntrepenorDato(Date date) {
        if (date != null) {
            forsikringsskade.setEntrepenorDato(mergeDates(date, (Date) oppdagetDatoSpinner.getValue()));
        }
    }

    public void setEntrepenorDatoKl(Date kl) {
        if (kl != null && oppdagetDato.getDate() != null) {
            oppdagetDatoSpinner.setValue(mergeDates(oppdagetDato, kl));
            forsikringsskade.setEntrepenorDato((Date) oppdagetDatoSpinner.getValue());
        }
    }

    public void setMeldtDato(Date date) {
        if (date != null) {
            forsikringsskade.setMeldtDato(mergeDates(date, (Date) skademeldDatoSpinner.getValue()));
        }
    }

    public void setMeldtDatoKl(Date kl) {
        if (kl != null && skademeldDato.getDate() != null) {
            skademeldDatoSpinner.setValue(mergeDates(skademeldDato, kl));
            forsikringsskade.setMeldtDato((Date) skademeldDatoSpinner.getValue());
        }
    }

    public void setRepDato(Date date) {
        if (date != null)
            forsikringsskade.setReparertDato(mergeDates(date, (Date) repDatoSpinner.getValue()));
    }

    public void setRepDatoKl(Date kl) {
        if (kl != null && repDato.getDate() != null) {
            repDatoSpinner.setValue(mergeDates(repDato, kl));
            forsikringsskade.setReparertDato((Date) repDatoSpinner.getValue());
        }
    }

    public void setSkadeDato(Date date) {
        if (date != null) {
            forsikringsskade.setSkadeDato(mergeDates(date, (Date) skadeDatoSpinner.getValue()));
        }
        if (date == null && fldSkadeAntatt != null) {
            forsikringsskade.setSkadeDato(null);
        }
    }

    public void setSkadeDatoKl(Date kl) {
        if (kl != null && skadeDato.getDate() != null) {
            skadeDatoSpinner.setValue(mergeDates(skadeDato, kl));
            forsikringsskade.setSkadeDato((Date) skadeDatoSpinner.getValue());
        }
    }

    private Date mergeDates(JMipssDatePicker date, Date kl) {
        Calendar c = Calendar.getInstance();
        c.setTime(kl);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(date.getDate());
        c2.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE));
        c2.set(Calendar.SECOND, c.get(Calendar.SECOND));
        date.setDate(c2.getTime());
        return c2.getTime();
    }

    private Date mergeDates(Date date, Date kl) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(kl);
        c.set(Calendar.HOUR_OF_DAY, c2.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c2.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, c2.get(Calendar.SECOND));
        return c.getTime();
    }

    private JPanel createElrappPanel() {
        JPanel pnl = new JPanel(new GridBagLayout());

        JLabel lblEmne = new JLabel(Resources.getResource("label.elrappEmne"));
        lblEmne.setForeground(ValidationComponentUtils.getMandatoryForeground());
        JLabel lblId = new JLabel(Resources.getResource("label.elrappId"));
        JLabel lblVersjon = new JLabel(Resources.getResource("label.elrappVersjon"));
        JLabel lblDato = new JLabel(Resources.getResource("label.elrappRapportertDato"));

        fldElrappEmne = new JFormattedTextField();
        fldElrappId = new JFormattedTextField();
        fldElrappVersjon = new JFormattedTextField();
        fldElrappDato = new JFormattedTextField();
        fldElrappDato.setEditable(false);
        fldElrappId.setEditable(false);
        fldElrappVersjon.setEditable(false);

        pnl.add(lblEmne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(fldElrappEmne, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(lblDato, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(fldElrappDato, new GridBagConstraints(3, 0, 1, 1, 0.2, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));

        pnl.add(lblId, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(fldElrappId, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        pnl.add(lblVersjon, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnl.add(fldElrappVersjon, new GridBagConstraints(3, 1, 1, 1, 0.2, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));


        pnl.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.elrapp")));

        return pnl;
    }

    private void createOverorndetPanel() {
        JPanel elrapp = createElrappPanel();

        JLabel skadestedLabel = new JLabel(Resources.getResource("label.skadested"));
        JLabel kommuneLabel = new JLabel(Resources.getResource("label.kommune") + "         ");
        JLabel vegnrLabel = new JLabel(Resources.getResource("label.vegnr"));
        JLabel hpLabel = new JLabel(Resources.getResource("label.hp"));
        JLabel kmLabel = new JLabel(Resources.getResource("label.km"));
        JLabel skademeldLabel = new JLabel(Resources.getResource("label.skadeMeld"));
        JLabel meldtavLabel = new JLabel(Resources.getResource("label.meldAv"));
        JLabel oppdagetLabel = new JLabel(Resources.getResource("label.oppdagetDato"));
        JLabel avhvemLabel = new JLabel(Resources.getResource("label.avHvem"));
        JLabel skadedatoLabel = new JLabel(Resources.getResource("label.skadeDato"));
        JLabel antattLabel = new JLabel(Resources.getResource("label.skadeAntatt"));
        JLabel omfangLabel = new JLabel(Resources.getResource("label.skadeOmfang"));
        JLabel krLabel = new JLabel(Resources.getResource("label.kostnad"));
        JLabel repLabel = new JLabel(Resources.getResource("label.reparert"));
        JLabel langtidLabel = new JLabel(Resources.getResource("label.langTid"));

//		add(elrapp, new GridBagConstraints(0,0,1,1,1.0,0.0,GridBagConstraints.NONE, GridBagConstraints.NORTHWEST,new Insets(0,0,0,0),0,0));
        c.gridwidth = 5;
        add(elrapp, c);
        c.gridwidth = 1;
        c.gridy++;
        add(UIUtils.getTwoComponentPanelVert(skadestedLabel, skadestedField, false), c);
        c.gridx++;
        add(UIUtils.getTwoComponentPanelVert(kommuneLabel, kommuneField, false), c);
        c.gridx++;
        add(UIUtils.getTwoComponentPanelVert(vegnrLabel, vegnrField, false), c);
        c.gridx++;
        add(UIUtils.getTwoComponentPanelVert(hpLabel, hpField, false), c);
        c.gridx++;
        add(UIUtils.getTwoComponentPanelVert(kmLabel, kmField, false), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        JPanel meldtPanel = UIUtils.getHorizPanel();
        meldtPanel.add(skademeldDato);
        meldtPanel.add(skademeldDatoSpinner);
        meldtPanel.add(Box.createHorizontalGlue());
        add(UIUtils.getTwoComponentPanelVert(skademeldLabel, meldtPanel, true), c);
        c.gridx += 1;
        c.gridwidth = 4;
        add(UIUtils.getTwoComponentPanelVert(meldtavLabel, decorateField(meldtavField, "validateMeldtAv"), false), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        JPanel oppdagetPanel = UIUtils.getHorizPanel();
        oppdagetPanel.add(oppdagetDato);
        oppdagetPanel.add(oppdagetDatoSpinner);
        oppdagetPanel.add(Box.createHorizontalGlue());
        add(UIUtils.getTwoComponentPanelVert(oppdagetLabel, oppdagetPanel, true), c);
        c.gridx++;
        c.gridwidth = 1;
        add(UIUtils.getTwoComponentPanelVert(avhvemLabel, decorateField(avhvemField, "validateAvHvem"), false), c);
        c.gridwidth = 2;
        c.gridx++;

        JPanel skadetPanel = UIUtils.getHorizPanel();
        skadetPanel.add(skadeDato);
        skadetPanel.add(skadeDatoSpinner);
        skadetPanel.add(Box.createHorizontalGlue());

        add(UIUtils.getTwoComponentPanelVert(skadedatoLabel, skadetPanel, true), c);
        c.gridx += 2;
        c.gridwidth = 1;
        add(UIUtils.getTwoComponentPanelVert(antattLabel, decorateField(fldSkadeAntatt, "validateFldSkadeAntatt"), true), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        c.ipady = 60;
        add(UIUtils.getTwoComponentPanelVert(omfangLabel, decorateField(new JScrollPane(omfangText), "validateOmfang"), false), c);
        c.ipady = 0;

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        add(UIUtils.getTwoComponentPanelVert(krLabel, krField, false), c);
        c.gridx += 2;
        c.gridwidth = 3;
        JPanel repPanel = UIUtils.getHorizPanel();
        repPanel.add(repDato);
        repPanel.add(repDatoSpinner);
        repPanel.add(Box.createHorizontalGlue());
        add(UIUtils.getTwoComponentPanelVert(repLabel, repPanel, true), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(UIUtils.getTwoComponentPanelVert(langtidLabel, langtidField, false), c);
        c.gridx = 0;
        c.gridy++;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        add(new JPanel(), c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.weighty = 0.0;
    }

    public boolean isValidFields() {
        boolean valid = true;
        if (!validateMeldtAv())
            valid = false;
        if (!validateAvHvem())
            valid = false;
        if (!validateOmfang()) {
            valid = false;
        }
        if (!validateFldSkadeAntatt()) {
            valid = false;
        }
        return valid;
    }

    public boolean validateMeldtAv() {
        return !meldtavField.getText().equals("");
    }

    public boolean validateAvHvem() {
        return !avhvemField.getText().equals("");
    }

    public boolean validateOmfang() {
        return !omfangText.getText().equals("");
    }

    public boolean validateFldSkadeAntatt() {
        if (fldSkadeAntatt.getText() == "" && skadeDato.getDate() == null) {
            return false;
        }
        return true;
    }

    private JXLayer<JComponent> decorateField(JComponent c, String validationMethod) {
        JXLayer<JComponent> layer = new JXLayer<JComponent>(c);
        layer.setUI(new ValidationLayerUI(validationMethod, this));
        return layer;
    }
}
