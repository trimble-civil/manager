package no.mesta.mipss.felt;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.felt.inspeksjon.InspeksjonController;
import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.messages.MipssMapOpenMipssFieldItem;
import no.mesta.mipss.mipssfelt.*;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.persistence.veinett.Veinettreflinkseksjon;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.reports.*;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.webservice.NvdbWebservice;
import no.mesta.mipss.webservice.ReflinkStrekning;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXBusyLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FeltController {

    private static final Logger log = LoggerFactory.getLogger(FeltController.class);

    private ButtonController buttonController;
    private final FeltModule plugin;


    private final SakController sakController;
    private final AvvikController avvikController;
    private final RskjemaController rskjemaController;
    private final InspeksjonController inspeksjonController;

    private QueryParams commonQueryParams = new QueryParams();

    private BindingGroup bindings = new BindingGroup();
    private MipssFelt feltBean;
    private NvdbWebservice nvdb;

    public FeltController(FeltModule plugin) {
        this.plugin = plugin;
        buttonController = new ButtonController();
        sakController = new SakController(this);
        avvikController = new AvvikController(this);
        rskjemaController = new RskjemaController(this);
        inspeksjonController = new InspeksjonController(this);
        feltBean = BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class);
        nvdb = BeanUtil.lookup(NvdbWebservice.BEAN_NAME, NvdbWebservice.class);
    }

    public void resetAllTables() {
        sakController.getSakTableModel().setEntities(new ArrayList<SakSokResult>());
        avvikController.getAvvikTableModel().setEntities(new ArrayList<AvvikSokResult>());
        avvikController.getManglerTableModel().setEntities(new ArrayList<AvvikSokResult>());

        rskjemaController.getRskjemaTableModel().setEntities(new ArrayList<RskjemaSokResult>());
        inspeksjonController.getInspeksjonTableModel().setEntities(new ArrayList<InspeksjonSokResult>());
    }

    public BindingGroup getBindingGroup() {
        return bindings;
    }

    public void bind() {
        bindings.bind();
    }

    public JDialog showWaitDialog(String text) {
        JDialog d = new JDialog();
        JPanel labelPanel = new JPanel(new FlowLayout());
        JXBusyLabel busyLabel = new JXBusyLabel();
        busyLabel.setBusy(true);
        labelPanel.add(busyLabel);
        labelPanel.add(new JLabel(text));
        d.add(labelPanel);
        d.pack();
        d.setLocationRelativeTo(plugin.getLoader().getApplicationFrame());
        d.setVisible(true);
        return d;
    }

    public FeltModule getPlugin() {
        return plugin;
    }

    public String getLoggedOnUserSign() {
        return getPlugin().getLoader().getLoggedOnUserSign();
    }

    public QueryParams getCommonQueryParams() {
        return commonQueryParams;
    }

    public SakController getSakController() {
        return sakController;
    }

    public AvvikController getAvvikController() {
        return avvikController;
    }

    public RskjemaController getRskjemaController() {
        return rskjemaController;
    }

    public InspeksjonController getInspeksjonController() {
        return inspeksjonController;
    }

    public NokkeltallFelt getMangelStatistikk() {
        if (getCommonQueryParams().getValgtKontrakt() != null) {
            Long id = getCommonQueryParams().getValgtKontrakt().getId();
            try {
                return feltBean.hentMangelStatistikk(id);
            } catch (Exception e) {
                log.error("Unable to find statistics for contract with id {}", id);
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public void openItemsInMap(List<? extends MipssFieldDetailItem> items) {
        MipssPlugin target;
        try {
            target = plugin.getLoader().getPlugin((Class<? extends MipssPlugin>) Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));
            MipssMapOpenMipssFieldItem message = new MipssMapOpenMipssFieldItem(items);
            message.setTargetPlugin(target);
            plugin.getLoader().sendMessage(message);
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }

    public <T extends IRenderableMipssEntity> void openExcel(JMipssBeanTable<T> table) {
        JMipssBeanTable.EXPORT_TYPE exportType = JMipssBeanTable.EXPORT_TYPE.SELECTED_ROWS_ONLY;
        if (table.getSelectedRowCount() == 0) {
            exportType = JMipssBeanTable.EXPORT_TYPE.ALL_ROWS;
        }
        File excelFile = table.exportToExcel(exportType);
        DesktopHelper.openFile(excelFile);
    }

    public void openRapportAvvikSak(String sakGuid) {
        try {
            List<FeltEntityInfo> aInfoList = feltBean.hentEntityInfoAvvik(sakGuid);

            List<String> ids = new ArrayList<String>();
            for (FeltEntityInfo i : aInfoList) {
                ids.add(i.getGuid());
            }

            openRapportAvvik(ids.toArray(new String[]{}));

        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportAvvik(String... avvikGuids) {
        try {
            PunktregDS dataSource = new PunktregDS(Arrays.asList(avvikGuids));
            dataSource.setFieldBean(feltBean);
            JReportBuilder builder = new JReportBuilder(Report.DISCOVERY_DETAIL_PRINT, Report.DISCOVERY_DETAIL_PRINT.getDefaultParameters(),
                    dataSource, ReportHelper.EXPORT_PDF, plugin.getLoader().getApplicationFrame());
            File report = builder.getGeneratedReportFile();

            if (report != null) {
                DesktopHelper.openFile(report);
            }

        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportAvvikList(List<AvvikSokResult> avvik) {
        try {
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(avvik);
            JReportBuilder builder = new JReportBuilder(Report.DISCOVERY_OVERVIEW, Report.DISCOVERY_OVERVIEW
                    .getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, plugin.getLoader().getApplicationFrame());
            File report = builder.getGeneratedReportFile();

            if (report != null) {
                DesktopHelper.openFile(report);
            }
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportR2Sak(String sakGuid) {
        try {
            List<FeltEntityInfo> hInfoList = feltBean.hentEntityInfoHendelse(sakGuid);
            if (hInfoList.size() != 1) {
                //TODO throw exception??...
                return;
            }
            openRapportR2(hInfoList.get(0).getGuid());
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportR2(String hendelseGuid) {
        try {
            HendelseDS dataSource = new HendelseDS(Collections.singletonList(hendelseGuid));
            dataSource.setFieldBean(feltBean);
            JReportBuilder builder = new JReportBuilder(Report.R2_FORM, Report.R2_FORM.getDefaultParameters(),
                    dataSource, ReportHelper.EXPORT_PDF, plugin.getLoader().getApplicationFrame());
            File report = builder.getGeneratedReportFile();
            if (report != null) {
                DesktopHelper.openFile(report);
            }
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportR5Sak(String sakGuid) {
        try {
            List<FeltEntityInfo> hInfoList = feltBean.hentEntityInfoForsikringsskade(sakGuid);
            if (hInfoList.size() != 1) {
                //TODO throw exception??...
                return;
            }
            openRapportR5(hInfoList.get(0).getGuid());
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportR5(String forsikringsskadeGuid) {
        try {
            MipssFeltDS<Forsikringsskade> dataSource = new MipssFeltDS<Forsikringsskade>(Forsikringsskade.class, Collections.singletonList(forsikringsskadeGuid));
            dataSource.setFieldBean(feltBean);
            JReportBuilder builder = new JReportBuilder(Report.R5_FORM, Report.R5_FORM.getDefaultParameters(),
                    dataSource, ReportHelper.EXPORT_PDF, plugin.getLoader().getApplicationFrame());
            File report = builder.getGeneratedReportFile();
            if (report != null) {
                DesktopHelper.openFile(report);
            }
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportR11Sak(String sakGuid) {
        try {
            List<FeltEntityInfo> hInfoList = feltBean.hentEntityInfoSkred(sakGuid);
            if (hInfoList.size() != 1) {
                //TODO throw exception??...
                return;
            }
            openRapportR11(hInfoList.get(0).getGuid());
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public void openRapportR11(String skredGuid) {
        try {
            R11DS dataSource = new R11DS(Skred.class, Collections.singletonList(skredGuid), feltBean);
            JReportBuilder builder = new JReportBuilder(Report.R11_FORM, Report.R11_FORM.getDefaultParameters(),
                    dataSource, ReportHelper.EXPORT_PDF, plugin.getLoader().getApplicationFrame());
            File report = builder.getGeneratedReportFile();
            if (report != null) {
                DesktopHelper.openFile(report);
            }
        } catch (Throwable t) {
            ErrorHandler.showError(t, getRapportFeilWarning());
        }
    }

    public boolean updateStedfestingFromVeiInfo(VeiInfo veiInfo, Punktstedfest sted) {
        Veinettveireferanse vr = new Veinettveireferanse();
        vr.setFylkesnummer(Long.valueOf(veiInfo.getFylkesnummer()));
        vr.setKommunenummer(Long.valueOf(veiInfo.getKommunenummer()));
        vr.setVeikategori(veiInfo.getVeikategori());
        vr.setVeinummer(Long.valueOf(veiInfo.getVeinummer()));
        vr.setVeistatus(veiInfo.getVeistatus());
        vr.setFraKm(Double.valueOf((double) veiInfo.getMeter() / 1000d));
        vr.setTilKm(Double.valueOf((double) veiInfo.getMeter() / 1000d));
        vr.setHp(Long.valueOf(veiInfo.getHp()));


        List<Veinettreflinkseksjon> reflinks = nvdb.getReflinkSectionsWithinRoadref(vr);
        if (reflinks == null || reflinks.size() == 0) {
            return false;
        }
        Veinettreflinkseksjon vref = reflinks.get(0);
        sted.setReflinkIdent(vref.getReflinkIdent());
        sted.setReflinkPosisjon(vref.getFra());

        List<Veinettreflinkseksjon> rl = new ArrayList<Veinettreflinkseksjon>();
        rl.add(vref);
        List<ReflinkStrekning> geo = nvdb.getGeometryWithinReflinkSectionsFromVeinettreflinkseksjon(rl);
        Point point = geo.get(0).getVectors()[0];
        sted.setSnappetX(point.getX());
        sted.setSnappetY(point.getY());
        sted.setX(point.getX());
        sted.setY(point.getY());

        Punktveiref prev = sted.getVeiref();
        if (!veirefIsNull(prev)) {
            //kopier den gamle veireffen og oppdater til de nye verdier
            //.for å unngå binding-problemer i gui
            prev.getPunktstedfest().getVeireferanser().remove(prev);
            Punktveiref old = copyVeiref(prev);
            old.setPunktstedfest(sted);
            prev.getPunktstedfest().getVeireferanser().add(old);
            prev.getPunktstedfest().getVeireferanser().add(prev);
        }
        prev.setFraDato(Clock.now());
        prev.setFylkesnummer(veiInfo.getFylkesnummer());
        prev.setKommunenummer(veiInfo.getKommunenummer());
        prev.setVeikategori(veiInfo.getVeikategori());
        prev.setVeistatus(veiInfo.getVeistatus());
        prev.setVeinummer(veiInfo.getVeinummer());
        prev.setHp(veiInfo.getHp());
        prev.setMeter(veiInfo.getMeter());
        return true;
    }

    public Punktveiref getPunktveirefFromXY(Double x, Double y) {
        if (x == null || y == null)
            return null;
        Point p = new Point();
        p.setLocation(x, y);
        Veinettreflinkseksjon veiref = nvdb.getReflinkSectionFromGeometry(p);
        if (veiref == null)
            return null;
        List<Veinettveireferanse> veiList = nvdb.getRoadrefsWithinReflinkSectionsFromVeinettreflinkseksjon(Collections.singletonList(veiref));

        if (veiList.size() == 0)
            return null;
        Veinettveireferanse v = veiList.get(0);
        Punktveiref pv = new Punktveiref();
        pv.setFylkesnummer(v.getFylkesnummer().intValue());
        pv.setKommunenummer(v.getKommunenummer().intValue());
        pv.setVeikategori(v.getVeikategori());
        pv.setVeistatus(v.getVeistatus());
        pv.setVeinummer(v.getVeinummer().intValue());
        pv.setHp(v.getHp().intValue());
        pv.setMeter((int) (v.getFraKm() * 1000d));
        pv.setNewEntity(true);
        pv.setFraDato(Clock.now());
        return pv;

    }

    public Punktveiref copyVeiref(Punktveiref vr) {
        Punktveiref v = new Punktveiref();
        v.setFraDato(vr.getFraDato());
        v.setFylkesnummer(vr.getFylkesnummer());
        v.setHp(vr.getHp());
        v.setKjorefelt(vr.getKjorefelt());
        v.setKommunenummer(vr.getKommunenummer());
        v.setMeter(vr.getMeter());
        v.setVeikategori(vr.getVeikategori());
        v.setVeinummer(vr.getVeinummer());
        v.setVeistatus(vr.getVeistatus());


        return v;
    }

    public void copyVeirefInfo(Punktveiref src, Punktveiref dest) {
        dest.setFraDato(src.getFraDato());
        dest.setFylkesnummer(src.getFylkesnummer());
        dest.setHp(src.getHp());
        dest.setKjorefelt(src.getKjorefelt());
        dest.setKommunenummer(src.getKommunenummer());
        dest.setMeter(src.getMeter());
        dest.setVeikategori(src.getVeikategori());
        dest.setVeinummer(src.getVeinummer());
        dest.setVeistatus(src.getVeistatus());
        dest.setMeter(src.getMeter());
    }

    public Punktstedfest copyStedfesting(Punktstedfest ps) {
        Punktstedfest p = new Punktstedfest();
        p.setGuid(ServerUtils.getInstance().fetchGuid());
        p.setReflinkIdent(ps.getReflinkIdent());
        p.setReflinkPosisjon(ps.getReflinkPosisjon());
        p.setSnappetH(ps.getSnappetH());
        p.setSnappetX(ps.getSnappetX());
        p.setSnappetY(ps.getSnappetY());
        p.setX(ps.getX());
        p.setY(ps.getY());

        Punktveiref veiref = copyVeiref(ps.getVeiref());
        veiref.setPunktstedfest(p);
        p.getVeireferanser().add(veiref);
        return p;

    }

    public VeiInfo createVeiInfoFromPunktstedfest(Punktstedfest sted) {
        Punktveiref v = sted.getVeiref();
        //mulig feil sjekk av punktveiref ettersom studio oppretter en veiref for å binde til
        //gui komponenter dersom det mangler.
        if (v == null) {
            v = getPunktveirefFromXY(sted.getX(), sted.getY());
            v.setPunktstedfest(sted);
            sted.getVeireferanser().add(v);
        }
        //avvikets stedfesting mangler snappetX og Y dette kan indikere at det mangler en
        //veireferanse.. Mulig dette blir en konflikt med det over. Testes.
        if (sted.getSnappetX() == null && sted.getSnappetY() == null) {
            if (sted.getX() != null && sted.getY() != null)
                copyVeirefInfo(getPunktveirefFromXY(sted.getX(), sted.getY()), v);
        }
        VeiInfo veiInfo = new VeiInfo();
        veiInfo.setFylkesnummer(v.getFylkesnummer());
        veiInfo.setKommunenummer(v.getKommunenummer());
        veiInfo.setVeikategori(v.getVeikategori());
        veiInfo.setVeistatus(v.getVeistatus());
        veiInfo.setVeinummer(v.getVeinummer());
        veiInfo.setHp(v.getHp());
        veiInfo.setMeter(v.getMeter());
        return veiInfo;
    }

    /**
     * Kopierer snappede referanser fra src til dest, ingen relasjoner mellom stedfesting og veiref kopieres
     * funksjonen kan kun benyttes dersom src og dest inneholder identiske veireferanser.
     *
     * @param src
     * @param dest
     * @return
     */
    public Punktstedfest copyStedfestingInto(Punktstedfest src, Punktstedfest dest) {
        dest.setReflinkIdent(src.getReflinkIdent());
        dest.setReflinkPosisjon(src.getReflinkPosisjon());
        dest.setSnappetH(src.getSnappetH());
        dest.setSnappetX(src.getSnappetX());
        dest.setSnappetY(src.getSnappetY());
        dest.setX(src.getX());
        dest.setY(src.getY());
        return dest;

    }

    public boolean veirefIsNull(Punktveiref vr) {
        return vr.getFylkesnummer() == 0 && vr.getKommunenummer() == 0 && vr.getVeinummer() == 0 && vr.getHp() == 0 && vr.getMeter() == 0;
    }

    public Action getVeiButtonAction() {
        return buttonController.getVeiButtonAction(commonQueryParams, plugin);
    }

    public Action getPdaButtonAction() {
        return buttonController.getPdaButtonAction(commonQueryParams, plugin);
    }

    public Action getBrukerButtonAction() {
        return buttonController.getBrukerButtonAction(commonQueryParams, plugin);
    }

    /**
     * Viser pda dialogen og binder valgene til angitt QueryParam
     *
     * @param params
     * @return
     */
    public Action getPdaButtonAction(QueryParams params) {
        return buttonController.getPdaButtonAction(params, plugin);
    }

    /**
     * Viser bruker dialogen og binder valgene til angitt QueryParam
     *
     * @param params
     * @return
     */
    public Action getBrukerButtonAction(QueryParams params) {
        return buttonController.getBrukerButtonAction(params, plugin);
    }

    public Action getVelgProsessAction(QueryParams params) {
        return buttonController.getVelgProsessAction(params, plugin);
    }

    public Action getTilbakestillKriterierAction() {
        return buttonController.getTilbakestillKriterierAction(commonQueryParams);
    }

    public void openItems(final List<MipssFieldDetailItem> items) {
        plugin.enforceReadAccess();

        SwingWorker<Void, Void> waitForRunning = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                while (plugin.getStatus() != MipssPlugin.Status.RUNNING) {
                    Thread.yield();
                }
                MipssFieldDetailItem item = items.get(0);
                if (item instanceof Avvik) {
                    Avvik avvik = (Avvik) item;
                    AvvikSokResult rs = new AvvikSokResult();
                    rs.setGuid(avvik.getGuid());
                    rs.setSakGuid(avvik.getSakGuid());
                    rs.setSlettetDato(avvik.getSlettetDato());
                    getAvvikController().openAvvik(rs);
                }
                if (item instanceof Hendelse) {
                    RskjemaSokResult rs = getRskjemaSokResultFromHendelse((Hendelse) item);
                    getRskjemaController().openRskjema(rs);
                }
                if (item instanceof Forsikringsskade) {
                    RskjemaSokResult rs = getRskjemaSokResultFromForsikringsskade((Forsikringsskade) item);
                    getRskjemaController().openRskjema(rs);
                }
                if (item instanceof Skred) {
                    RskjemaSokResult rs = getRskjemaSokResultFromSkred((Skred) item);
                    getRskjemaController().openRskjema(rs);
                }
                return null;
            }
        };
        waitForRunning.execute();
    }


    public RskjemaSokResult getRskjemaSokResultFromHendelse(Hendelse h) {
        RskjemaSokResult rs = new RskjemaSokResult();
        rs.setGuid(h.getGuid());
        rs.setSakGuid(h.getSakGuid());
        rs.setSlettetDato(h.getSlettetDato());
        return rs;
    }

    public RskjemaSokResult getRskjemaSokResultFromForsikringsskade(Forsikringsskade f) {
        RskjemaSokResult rs = new RskjemaSokResult();
        rs.setGuid(f.getGuid());
        rs.setSakGuid(f.getSakGuid());
        rs.setSlettetDato(f.getSlettetDato());
        return rs;
    }

    public RskjemaSokResult getRskjemaSokResultFromSkred(Skred s) {
        RskjemaSokResult rs = new RskjemaSokResult();
        rs.setGuid(s.getGuid());
        rs.setSakGuid(s.getSakGuid());
        rs.setSlettetDato(s.getSlettetDato());
        return rs;
    }

    /**
     * Lagrer en mipss-felt entitet.
     * Disse entitetene omfatter:
     * <ul>
     * <li>Skred</li>
     * <li>Forsikringsskade</li>
     * <li>Hendelse</li>
     * <li>Avvik</li>
     * </ul>
     *
     * @param entity
     */
    public void saveEntity(MipssFieldDetailItem entity) {
        if (entity instanceof Skred) {
            getRskjemaController().persistSkred((Skred) entity);
        }
        if (entity instanceof Forsikringsskade) {
            getRskjemaController().persistForsikringsskade((Forsikringsskade) entity);
        }
        if (entity instanceof Hendelse) {
            getRskjemaController().persistHendelse((Hendelse) entity);
        }
        if (entity instanceof Avvik) {
            getAvvikController().persistAvvik((Avvik) entity);
        }
        if (entity instanceof Sak) {
            getSakController().persistSak((Sak) entity);
        }

    }

    public List<Bilde> getBilderFromSak(String sakGuid) {
        return feltBean.getBilderFromSak(sakGuid);
    }

    public void persistSynkbilde(final List<String> bildeGuids) {
        new Thread() {
            public void run() {
                feltBean.persistSynkbilde(bildeGuids, getLoggedOnUserSign());
            }
        }.start();

    }

    private String getRapportFeilWarning() {
        return Resources.getResource("warning.rapportfeil", EpostUtils.getSupportEpostAdresse());
    }

}
