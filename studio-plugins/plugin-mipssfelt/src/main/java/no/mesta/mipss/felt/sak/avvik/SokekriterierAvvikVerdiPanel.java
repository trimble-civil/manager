package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.util.StringConcatHelper;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class SokekriterierAvvikVerdiPanel extends JPanel {

    private final AvvikController controller;

    private JLabel lblKriterier;
    private JLabel lblKriterierVerdi;

    private JLabel lblProsess;
    private JLabel lblProsessVerdi;

    private JLabel lblStatus;
    private JLabel lblStatusVerdi;

    private JLabel lblAarsak;
    private JLabel lblAarsakVerdi;

    private List<String> sokebegrensning = new ArrayList<String>();

    public SokekriterierAvvikVerdiPanel(AvvikController controller) {
        this.controller = controller;
        initComponents();
        initGui();
        bind();
    }

    private void bind() {
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> valgtProsessBind = BindingHelper.createbinding(controller.getQueryParams(), "prosesskoder", this, "valgteProsesskoder");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> valgtStatusBind = BindingHelper.createbinding(controller.getQueryParams(), "avvikstatus", this, "valgteStatus");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> valgtAarsakBind = BindingHelper.createbinding(controller.getQueryParams(), "avvikaarsakStr", this, "valgteAarsak");

        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> etterslepBind = BindingHelper.createbinding(controller.getQueryParams(), "etterslep", this, "etterslep");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> tilleggsarbeidBind = BindingHelper.createbinding(controller.getQueryParams(), "tilleggsarbeid", this, "tilleggsarbeid");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> trafikksikkerhetBind = BindingHelper.createbinding(controller.getQueryParams(), "trafikksikkerhet", this, "trafikksikkerhet");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> kunSlettedeBind = BindingHelper.createbinding(controller.getQueryParams(), "kunSlettede", this, "kunSlettede");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> fristdatoOverskredetBind = BindingHelper.createbinding(controller.getQueryParams(), "fristdatoOverskredet", this, "fristdatoOverskredet");
        Binding<AvvikQueryParams, Object, SokekriterierAvvikVerdiPanel, Object> innenforPeriodeBind = BindingHelper.createbinding(controller.getQueryParams(), "innenforPeriode", this, "innenforPeriode");


        controller.getFeltController().getBindingGroup().addBinding(valgtProsessBind);
        controller.getFeltController().getBindingGroup().addBinding(valgtStatusBind);
        controller.getFeltController().getBindingGroup().addBinding(valgtAarsakBind);
        controller.getFeltController().getBindingGroup().addBinding(etterslepBind);
        controller.getFeltController().getBindingGroup().addBinding(tilleggsarbeidBind);
        controller.getFeltController().getBindingGroup().addBinding(trafikksikkerhetBind);
        controller.getFeltController().getBindingGroup().addBinding(kunSlettedeBind);
        controller.getFeltController().getBindingGroup().addBinding(fristdatoOverskredetBind);
        controller.getFeltController().getBindingGroup().addBinding(innenforPeriodeBind);
    }

    public void setValgteStatus(List<String> status) {
        if (status == null || status.size() == 0) {
            lblStatusVerdi.setText(Resources.getResource("label.valgteStatus.default"));
            lblStatusVerdi.setToolTipText(Resources.getResource("label.valgteStatus.default"));
            return;
        }
        String[] s = status.toArray(new String[]{});
        lblStatusVerdi.setText(StringConcatHelper.concatProsess(s));
        lblStatusVerdi.setToolTipText(StringConcatHelper.createTooltip(s));
    }

    public void setValgteAarsak(List<String> aarsakStr) {
        if (aarsakStr == null || aarsakStr.size() == 0) {
            lblAarsakVerdi.setText(Resources.getResource("label.valgteAarsaker.default"));
            lblAarsakVerdi.setToolTipText(Resources.getResource("label.valgteAarsaker.default"));
            return;
        }
        String[] s = aarsakStr.toArray(new String[]{});
        lblAarsakVerdi.setText(StringConcatHelper.concatProsess(s));
        lblAarsakVerdi.setToolTipText(StringConcatHelper.createTooltip(s));

    }

    public void setValgteProsesskoder(String[] prosesskoder) {
        lblProsessVerdi.setText(StringConcatHelper.concatProsess(prosesskoder));
        lblProsessVerdi.setToolTipText(StringConcatHelper.createTooltip(prosesskoder));
    }


    private void initComponents() {
        lblKriterier = new JLabel(Resources.getResource("label.sokevalg"));
        lblProsess = new JLabel(Resources.getResource("label.valgteProsesser"));
        lblStatus = new JLabel(Resources.getResource("label.valgteStatus"));
        lblAarsak = new JLabel(Resources.getResource("label.valgteAarsaker"));
        Font font = lblKriterier.getFont();
        font = new Font(font.getFontName(), Font.BOLD, font.getSize());
        lblKriterier.setFont(font);
        lblProsess.setFont(font);
        lblStatus.setFont(font);
        lblAarsak.setFont(font);

        lblProsessVerdi = new JLabel(Resources.getResource("label.valgteProsesser.default"));
        lblKriterierVerdi = new JLabel("ingen");
        lblStatusVerdi = new JLabel(Resources.getResource("label.valgteStatus.default"));
        lblAarsakVerdi = new JLabel(Resources.getResource("label.valgteAarsaker.default"));
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.sokebegrensninger")));

        add(lblKriterier, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2, 5, 0, 0), 0, 0));
        add(lblProsess, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblStatus, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblAarsak, new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));


        add(lblKriterierVerdi, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2, 5, 0, 0), 0, 0));
        add(lblProsessVerdi, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblStatusVerdi, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblAarsakVerdi, new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    }

    private void updateText() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        if (sokebegrensning.size() > 0) {
            sb.append("Kun ");
            sb.append(sokebegrensning.get(0) + " ");
            if (sokebegrensning.size() > 1) {
                sb.append("<b>");
                sb.append(" og ");
                sb.append("</b>");
            }
            String s = StringUtils.join(Arrays.copyOfRange(sokebegrensning.toArray(new String[]{}), 1, sokebegrensning.size()), " <b>og</b> ");
            sb.append(s);
        } else
            sb.append("ingen");
        sb.append("</html>");
        lblKriterierVerdi.setText(sb.toString());
    }

    public void setEtterslep(boolean etterslep) {
        if (etterslep)
            sokebegrensning.add(Resources.getResource("sok.etterslep"));
        else
            sokebegrensning.remove(Resources.getResource("sok.etterslep"));
        updateText();
    }

    public void setTilleggsarbeid(boolean tilleggsarbeid) {
        if (tilleggsarbeid)
            sokebegrensning.add(Resources.getResource("sok.tilleggsarbeid"));
        else
            sokebegrensning.remove(Resources.getResource("sok.tilleggsarbeid"));
        updateText();
    }

    public void setTrafikksikkerhet(boolean trafikksikkerhet) {
        if (trafikksikkerhet)
            sokebegrensning.add(Resources.getResource("sok.trafikksikkerhet"));
        else
            sokebegrensning.remove(Resources.getResource("sok.trafikksikkerhet"));
        updateText();
    }

    public void setKunSlettede(boolean kunSlettede) {
        if (kunSlettede)
            sokebegrensning.add(Resources.getResource("sok.kunSlettede"));
        else
            sokebegrensning.remove(Resources.getResource("sok.kunSlettede"));
        updateText();
    }

    public void setFristdatoOverskredet(boolean fristdatoOverskredet) {
        if (fristdatoOverskredet)
            sokebegrensning.add(Resources.getResource("sok.fristdatoOverskredet"));
        else
            sokebegrensning.remove(Resources.getResource("sok.fristdatoOverskredet"));
        updateText();
    }

    public void setInnenforPeriode(boolean innenforPeriode) {
        if (innenforPeriode)
            sokebegrensning.add(Resources.getResource("sok.innenforPeriode"));
        else
            sokebegrensning.remove(Resources.getResource("sok.innenforPeriode"));
        updateText();
    }
}
