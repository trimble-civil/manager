package no.mesta.mipss.felt.sak.rskjema;

import net.sf.jasperreports.engine.JRException;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.elrapp.ElrappResponse;
import no.mesta.mipss.elrapp.ElrappService;
import no.mesta.mipss.elrapp.ElrappValidationException;
import no.mesta.mipss.felt.OpenItemDialog;
import no.mesta.mipss.felt.OpenItemDialog.TYPE;
import no.mesta.mipss.felt.ProsessDialog;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.ProcessRelatedPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.HendelseDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.TiltakPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.SkredDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r5.ForsikringsskadeDetaljPanel;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.MipssFeltDS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssProgressDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

@SuppressWarnings("serial")
public class RskjemaButtonController {

    private static final Logger log = LoggerFactory.getLogger(RskjemaButtonController.class);

    private RskjemaService rskjemaService;
    private final RskjemaController controller;

    public RskjemaButtonController(RskjemaController controller) {
        this.controller = controller;
        rskjemaService = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
    }

    public Action getEndreProsessAction(final ProcessRelatedPanel vurderingPanel, final JPanel panel, final RskjemaController controller) {
        return new AbstractAction(Resources.getResource("button.endre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessRelated processRelatedObject = vurderingPanel.getProcessRelatedObject();
                ProsessDialog prosessDialog = new ProsessDialog(controller.getQueryParams(), (JDialog) SwingUtilities.windowForComponent(panel), Resources.getResource("button.velgProsess"));
                prosessDialog.setSingleSelectionMode(true);
                prosessDialog.setSize(400, 450);
                prosessDialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
                prosessDialog.setSelectedProsesser(new Long[]{processRelatedObject.getProsessId()});
                prosessDialog.setVisible(true);
                if (!prosessDialog.isCancelled()) {
                    Set<Prosess> prosesser = prosessDialog.getProsesser();
                    if (prosesser.size() > 0) {
                        Prosess prosess = prosesser.iterator().next();
                        processRelatedObject.setProsess(prosess);
                        vurderingPanel.refreshProcess(prosess);
                    }
                }
            }
        };
    }

    public Action getRskjemaNyAction() {
        return new AbstractAction(Resources.getResource("button.nyttRskjema"), IconResources.NEW_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().getSakController().opprettNySak();
            }
        };

    }

    public Action getRskjemaSlettAction() {
        return new AbstractAction(Resources.getResource("button.slettRskjema"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<RskjemaSokResult> selectedEntities = controller.getRskjemaTable().getSelectedEntities();
                String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();
                String msg = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.rskjema", dagerForSletting) : Resources.getResource("warning.slett.rskjemaer", dagerForSletting);
                String tit = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.rskjema.title") : Resources.getResource("warning.slett.rskjemaer.title");
                int v = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, tit, JOptionPane.OK_CANCEL_OPTION);
                if (v == JOptionPane.YES_OPTION) {
                    rskjemaService.deleteRskjema(selectedEntities, controller.getFeltController().getPlugin().getLoader().getLoggedOnUserSign());
                    for (RskjemaSokResult rs : selectedEntities) {
                        controller.getRskjemaTableModel().removeItem(rs);
                    }
                }
            }
        };
    }

    public Action getRskjemaExcelAction() {
        return new AbstractAction(Resources.getResource("button.excel"), IconResources.EXCEL_ICON16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openExcel(controller.getRskjemaTable());
            }
        };
    }

    public Action getRskjemaRapportAction() {
        return new AbstractAction(Resources.getResource("button.rapport2"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<RskjemaSokResult> rskjema = controller.getRskjemaTable().getSelectedEntities();
                RskjemaSokResult r = rskjema.get(0);
                if (r.getTypeR().equals("R2")) {
                    controller.getFeltController().openRapportR2(r.getGuid());
                }
                if (r.getTypeR().equals("R5")) {
                    controller.getFeltController().openRapportR5(r.getGuid());
                }
                if (r.getTypeR().equals("R11")) {
                    controller.getFeltController().openRapportR11(r.getGuid());
                }
            }
        };
    }

    public Action getR2RapportAction(final HendelseDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.rapport2"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openRapportR2(pnl.getHendelse().getGuid());
            }
        };
    }

    public Action getR11RapportAction(final SkredDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.rapport2"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openRapportR11(pnl.getSkred().getGuid());
            }
        };
    }

    public Action getRskjemaDetaljerAction() {
        return new AbstractAction(Resources.getResource("button.detaljer"), IconResources.DETALJER_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<RskjemaSokResult> rskjema = controller.getRskjemaTable().getSelectedEntities();
                RskjemaSokResult rskjemaResult = rskjema.get(0);
                controller.openRskjema(rskjemaResult);
            }
        };
    }

    public Action getRskjemaKartAction() {
        return new AbstractAction(Resources.getResource("button.visIKart"), IconResources.NORGE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<RskjemaSokResult> selectedEntities = controller.getRskjemaTable().getSelectedEntities();
                List<MipssFieldDetailItem> rksjemmaList = new ArrayList<MipssFieldDetailItem>();
                for (RskjemaSokResult rs : selectedEntities) {
                    rksjemmaList.add(controller.getDetailItemFromRskjemaSokResult(rs));
                }
                controller.getFeltController().openItemsInMap(rksjemmaList);
            }
        };
    }

    public Action getRskjemaSokButtonAction(final RskjemaController controller) {
        return new AbstractAction(Resources.getResource("button.sok"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<RskjemaSokResult> model = controller.getRskjemaTableModel();
                model.setBeanMethodArgValues(new Object[]{controller.getQueryParams()});
                model.loadData();
            }
        };
    }

    public Action getHendelseMedIdAction() {
        return new AbstractAction(Resources.getResource("label.elrappType.r2"), IconResources.DETALJER_SMALL_ICON) {
            private Long prevHendelseRefnummer;

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.HENDELSE, controller.getFeltController());
                dialog.setRefnummer(prevHendelseRefnummer);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevHendelseRefnummer = dialog.getRefnummer();
            }
        };
    }

    public Action getForsikringsskadeMedIdAction() {
        return new AbstractAction(Resources.getResource("label.elrappType.r5"), IconResources.DETALJER_SMALL_ICON) {
            private Long prevForsikringsskadeRefnummer;

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.FORSIKRINGSSKADE, controller.getFeltController());
                dialog.setRefnummer(prevForsikringsskadeRefnummer);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevForsikringsskadeRefnummer = dialog.getRefnummer();
            }
        };
    }

    public Action getSkredMedIdAction() {
        return new AbstractAction(Resources.getResource("label.elrappType.r11"), IconResources.DETALJER_SMALL_ICON) {
            private Long prevSkredRefnummer;

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.SKRED, controller.getFeltController());
                dialog.setRefnummer(prevSkredRefnummer);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevSkredRefnummer = dialog.getRefnummer();
            }
        };
    }

    public Action getHendelseMedElrappIdAction() {
        return new AbstractAction(Resources.getResource("label.elrappType.r2"), IconResources.DETALJER_SMALL_ICON) {
            private Long prevHendelseElrappId;

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.HENDELSE_ELRAPP, controller.getFeltController());
                dialog.setRefnummer(prevHendelseElrappId);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevHendelseElrappId = dialog.getRefnummer();
            }
        };
    }

    public Action getForsikringsskadeMedElrappIdAction() {
        return new AbstractAction(Resources.getResource("label.elrappType.r5"), IconResources.DETALJER_SMALL_ICON) {
            private Long prevForsikringsskadeElrappId;

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.FORSIKRINGSSKADE_ELRAPP, controller.getFeltController());
                dialog.setRefnummer(prevForsikringsskadeElrappId);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevForsikringsskadeElrappId = dialog.getRefnummer();
            }
        };
    }

    public Action getSkredMedElrappIdAction() {
        return new AbstractAction(Resources.getResource("label.elrappType.r11"), IconResources.DETALJER_SMALL_ICON) {
            private Long prevSkredElrappId;

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.SKRED_ELRAPP, controller.getFeltController());
                dialog.setRefnummer(prevSkredElrappId);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevSkredElrappId = dialog.getRefnummer();
            }
        };
    }

    public Action getRskjemaSendElrappAction() {
        return new AbstractAction(Resources.getResource("button.sendTilElrapp"), IconResources.ELRAPP_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                RskjemaSokResult rskjema = controller.getRskjemaTable().getSelectedEntities().get(0);
                sendTilElrapp(rskjema, null);
            }
        };
    }

    public Action getR2SendElrappAction(final HendelseDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.sendTilElrapp"), IconResources.ELRAPP_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Hendelse h = pnl.getHendelse();
                RskjemaSokResult rs = new RskjemaSokResult();
                rs.setGuid(h.getGuid());
                rs.setSakGuid(h.getSakGuid());
                rs.setTypeR("R2");
                sendTilElrapp(rs, pnl);
            }
        };
    }

    public Action getR5SendElrappAction(final ForsikringsskadeDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.sendTilElrapp"), IconResources.ELRAPP_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Forsikringsskade fs = pnl.getForsikringsskade();
                RskjemaSokResult rs = new RskjemaSokResult();
                rs.setGuid(fs.getGuid());
                rs.setSakGuid(fs.getSakGuid());
                rs.setTypeR("R5");
                sendTilElrapp(rs, pnl);
            }
        };
    }

    public Action getR11SendElrappAction(final SkredDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.sendTilElrapp"), IconResources.ELRAPP_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Skred s = pnl.getSkred();
                RskjemaSokResult rs = new RskjemaSokResult();
                rs.setGuid(s.getGuid());
                rs.setSakGuid(s.getSakGuid());
                rs.setTypeR("R11");
                sendTilElrapp(rs, pnl);
            }
        };
    }

    private void sendTilElrapp(final RskjemaSokResult rs, final JPanel pnlElrappEntitet) {
        int v = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("message.sendTilElrapp"), Resources.getResource("message.sendTilElrapp.title"), JOptionPane.YES_NO_OPTION);
        if (v != JOptionPane.YES_OPTION)
            return;

        final Bruker bruker = controller.getFeltController().getPlugin().getLoader().getLoggedOnUser(true);
        String msg = controller.checkContractReadyForElrapp(controller.getQueryParams().getValgtKontrakt(), bruker);
        if (msg != null) {
            JOptionPane.showMessageDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), msg, "Kan ikke sende data til Elrapp", JOptionPane.ERROR_MESSAGE);
            return;
        }
        final MipssProgressDialog progress = new MipssProgressDialog(Resources.getResource("message.sendElrapp"), controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
        progress.setVisible(true);

        new SwingWorker<Void, Void>() {
            @Override
            public Void doInBackground() {
                sendTilElrapp(rs, pnlElrappEntitet, bruker, progress);
                return null;
            }

            @Override
            public void done() {
            }
        }.execute();

    }

    void sendTilElrapp(RskjemaSokResult rs, JPanel pnlElrappEntitet, Bruker bruker, MipssProgressDialog progress) {
        try {
            controller.validateElrappSchema(rs);
            ElrappService b = BeanUtil.lookup(ElrappService.BEAN_NAME, ElrappService.class);

            ElrappResponse res = b.sendRSchema(rs, bruker);
            progress.dispose();
            if (res != null) {
                Long elrappDokumentId = res.getElrappDokumentId();
                Long elrappVersjon = res.getElrappVersjon();
                String message = Resources.getResource("message.sendElrappMessageOk", res.getRtype(), String.valueOf(elrappDokumentId), String.valueOf(elrappVersjon));
                String title = Resources.getResource("message.sendElrappMessageOk.title", res.getRtype());

                oppdaterElrappKule(pnlElrappEntitet);
                oppdaterElrappDokumentId(pnlElrappEntitet, elrappDokumentId, elrappVersjon);
                JOptionPane.showMessageDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), message, title, JOptionPane.INFORMATION_MESSAGE);
                controller.getFeltController().getSakController().closeDialogForSak(rs.getSakGuid());
            }

        } catch (ElrappValidationException e) {
            progress.dispose();
            String tit = Resources.getResource("warning.elrapp.ikkesendt.title");
            String msg = Resources.getResource("warning.elrapp.ikkesendt", e.getMessage());
            JOptionPane.showMessageDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, tit, JOptionPane.WARNING_MESSAGE);
        } catch (Throwable t) {
            progress.dispose();
            ErrorInfo info = new ErrorInfo("Feil ved sending til elrapp", "Skjema ble IKKE sendt\n" + t.getMessage(), null, "Feil", t, Level.SEVERE, null);
            t.printStackTrace();
            JXErrorPane.showDialog(null, info);
        }
    }

    private void oppdaterElrappKule(JPanel pnlElrappEntitet) {
        if (pnlElrappEntitet instanceof HendelseDetaljPanel) {
            ((HendelseDetaljPanel) pnlElrappEntitet).oppdaterElrappKule();
        }
        if (pnlElrappEntitet instanceof ForsikringsskadeDetaljPanel) {
            ((ForsikringsskadeDetaljPanel) pnlElrappEntitet).oppdaterElrappKule();
        }
        if (pnlElrappEntitet instanceof SkredDetaljPanel) {
            ((SkredDetaljPanel) pnlElrappEntitet).oppdaterElrappKule();
        }
        controller.reloadTable();
    }

    private void oppdaterElrappDokumentId(JPanel pnlElrappEntitet, Long dokumentIdent, Long elrappVersjon) {
        if(pnlElrappEntitet instanceof HendelseDetaljPanel) {
            Hendelse hendelse = ((HendelseDetaljPanel) pnlElrappEntitet).getHendelse();
            hendelse.setElrappDokumentIdent(dokumentIdent);
            hendelse.setElrappVersjon(elrappVersjon);
        }
        if(pnlElrappEntitet instanceof ForsikringsskadeDetaljPanel) {
            Forsikringsskade forsikringsskade = ((ForsikringsskadeDetaljPanel) pnlElrappEntitet).getForsikringsskade();
            forsikringsskade.setElrappDokumentIdent(dokumentIdent);
            forsikringsskade.setElrappVersjon(elrappVersjon);
        }
        if(pnlElrappEntitet instanceof SkredDetaljPanel) {
            Skred skred = ((SkredDetaljPanel) pnlElrappEntitet).getSkred();
            skred.setElrappDokumentIdent(dokumentIdent);
            skred.setElrappVersjon(elrappVersjon);
        }
    }

    public Action getLeggTilTrafikktiltakAction(final JMipssBeanTable<HendelseTrafikktiltak> trafikktiltakTable, final TiltakPanel tiltakPanel) {
        return new AbstractAction(Resources.getResource("button.leggTil"), IconResources.ADD_ITEM_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                trafikktiltakTable.clearSelection();
                controller.leggTilTrafikktiltak(tiltakPanel);
            }
        };
    }

    public Action getSlettTrafikktiltak(final JMipssBeanTable<HendelseTrafikktiltak> trafikktiltakTable, final TiltakPanel tiltakPanel) {
        return new AbstractAction(Resources.getResource("button.slett"), IconResources.REMOVE_ITEM_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<HendelseTrafikktiltak> selection = trafikktiltakTable.getSelectedEntities();
                Hendelse hendelse = tiltakPanel.getHendelse();
                if (selection != null && selection.size() > 0) {
                    trafikktiltakTable.clearSelection();
                    for (HendelseTrafikktiltak t : selection) {
                        hendelse.removeTrafikktiltak(t);
                    }
                }
            }
        };
    }

    public Action getForsikringsskadeLagreAction(final ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel, final String brukerSign) {
        return new AbstractAction(Resources.getResource("button.lagre"), IconResources.SAVE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Forsikringsskade forsikringsskade = forsikringsskadeDetaljPanel.getForsikringsskade();
                if (checkForsikringsskade(forsikringsskade)) {
                    List<SkadeKontakt> sk = forsikringsskade.getSkadeKontakter();
                    if (sk != null) {
                        List<SkadeKontakt> toRemove = new ArrayList<SkadeKontakt>();
                        for (SkadeKontakt s : sk) {
                            if (s.isBlank()) {
                                toRemove.add(s);
                                continue;
                            }
                            if (s.getGuid() == null) {
                                s.setGuid(ServerUtils.getInstance().fetchGuid());
                            }
                            if (s.getSkade() == null) {
                                s.setSkade(forsikringsskade);
                            }
                        }
                        forsikringsskade.getSkadeKontakter().removeAll(toRemove);
                    }
                    try {
                        Forsikringsskade f = controller.persistForsikringsskade(forsikringsskade);

                        if (forsikringsskade.getRefnummer() == null)
                            forsikringsskade.setRefnummer(f.getRefnummer());
                        if (f != null) {
                            forsikringsskadeDetaljPanel.getSaveStatusRegistry().reset();
                            forsikringsskadeDetaljPanel.setTitle(String.valueOf(f.getRefnummer()));
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
    }


    public Action getForsikringsskadeRapportAction(final ForsikringsskadeDetaljPanel r5Panel) {
        return new AbstractAction(Resources.getResource("button.rapport.r5"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssFeltDS<Forsikringsskade> ds = new MipssFeltDS<Forsikringsskade>(null, null) {
                    @Override
                    protected void loadPageData() {
                    }

                    int c = 0;

                    public boolean next() throws JRException {
                        if (c == 0) {
                            c++;
                            return true;
                        } else
                            return false;
                    }
                };
                Forsikringsskade fs = r5Panel.getForsikringsskade();
                if (fs.isNewEntity()) {
                    Forsikringsskade hent = controller.hentForsikringsskade(fs.getGuid());
                    if (hent == null) {
                        JOptionPane.showMessageDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("message.lagreR5ForRapport"), Resources.getResource("message.lagreR5ForRapport.title"), JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                    fs = hent;
                }
                ds.setEntity(fs);
                ds.setFieldBean(BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class));

                JReportBuilder builder = new JReportBuilder(Report.R5_FORM, Report.R5_FORM.getDefaultParameters(), ds, ReportHelper.EXPORT_PDF, new JFrame());
                File report = builder.getGeneratedReportFile();
                if (report != null) {
                    DesktopHelper.openFile(report);
                }
            }
        };
    }

    public Action getForsikringsskadeRestoreAction(final ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.gjenopprett"), IconResources.UNTRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Forsikringsskade forsikringsskade = forsikringsskadeDetaljPanel.getForsikringsskade();
                try {
                    Forsikringsskade f = rskjemaService.restoreForsikringsskade(forsikringsskade, brukerSign);
                    if (f != null) {
                        forsikringsskade.setSlettetAv(null);
                        forsikringsskade.setSlettetDato(null);
                        ActionEvent ev = new ActionEvent(f, 0, "gjenopprett");
                        callback.actionPerformed(ev);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public Action getForsikringsskadeSlettAction(final ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.slett"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Forsikringsskade forsikringsskade = forsikringsskadeDetaljPanel.getForsikringsskade();
                try {
                    Forsikringsskade f = slettForsikringsskade(forsikringsskade, brukerSign);
                    if (f != null) {
                        ActionEvent ev = new ActionEvent(f, 0, "gjenopprett");
                        callback.actionPerformed(ev);
                        removeWithGuidAndType(forsikringsskade.getGuid(), "R5");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public Action getSkredLagreAction(final SkredDetaljPanel skredDetaljPanel, final String brukerSign) {
        return new AbstractAction(Resources.getResource("button.lagre"), IconResources.SAVE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Skred skred = skredDetaljPanel.getSkred();
                try {

                    Skred s = controller.persistSkred(skred);
                    if (skred.getRefnummer() == null)
                        skred.setRefnummer(s.getRefnummer());
                    if (s != null) {
                        skredDetaljPanel.getSaveStatusRegistry().reset();
                        skredDetaljPanel.setTitle(String.valueOf(skred.getRefnummer()));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public Action getSkredRestoreAction(final SkredDetaljPanel skredDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.gjenopprett"), IconResources.UNTRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Skred skred = skredDetaljPanel.getSkred();
                try {
                    Skred s = rskjemaService.restoreSkred(skred, brukerSign);
                    if (s != null) {
                        skred.setSlettetAv(null);
                        skred.setSlettetDato(null);
                        ActionEvent ev = new ActionEvent(s, 0, "gjenopprett");
                        callback.actionPerformed(ev);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public Action getSkredSlettAction(final SkredDetaljPanel skredDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.slett"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Skred skred = skredDetaljPanel.getSkred();
                try {
                    Skred s = slettSkred(skred, brukerSign);
                    if (s != null) {
                        ActionEvent ev = new ActionEvent(s, 0, "gjenopprett");
                        callback.actionPerformed(ev);
                        removeWithGuidAndType(skred.getGuid(), "R11");

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    private void removeWithGuidAndType(String guid, String type) {
        RskjemaSokResult toRemove = null;
        for (RskjemaSokResult rs : controller.getRskjemaTableModel().getEntities()) {
            if (type.equals(rs.getTypeR()) && guid.equals(rs.getGuid())) {
                toRemove = rs;
                break;
            }
        }
        if (toRemove != null) {
            controller.getRskjemaTableModel().removeItem(toRemove);
        }
    }

    public Action getHendelseLagreAction(final HendelseDetaljPanel hendelseDetaljPanel, final String brukerSign) {
        return new AbstractAction(Resources.getResource("button.lagre"), IconResources.SAVE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Hendelse hendelse = hendelseDetaljPanel.getHendelse();
                try {
                    Hendelse h = controller.persistHendelse(hendelse);
                    if (hendelse.getRefnummer() == null)
                        hendelse.setRefnummer(h.getRefnummer());
                    if (h != null) {
                        hendelseDetaljPanel.getSaveStatusRegistry().reset();
                        hendelseDetaljPanel.setTitle(String.valueOf(h.getRefnummer()));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }


    public Action getHendelseRestoreAction(final HendelseDetaljPanel hendelseDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.gjenopprett"), IconResources.UNTRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Hendelse hendelse = hendelseDetaljPanel.getHendelse();
                try {
                    Hendelse h = rskjemaService.restoreHendelse(hendelse, brukerSign);
                    if (h != null) {
                        hendelse.setSlettetAv(null);
                        hendelse.setSlettetDato(null);
                        ActionEvent ev = new ActionEvent(h, 0, "gjenopprett");
                        callback.actionPerformed(ev);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public Action getHendelseSlettAction(final HendelseDetaljPanel hendelseDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.slett"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Hendelse hendelse = hendelseDetaljPanel.getHendelse();
                try {
                    Hendelse h = slettHendelse(hendelse, brukerSign);
                    if (h != null) {
                        ActionEvent ev = new ActionEvent(h, 0, "slettet");
                        callback.actionPerformed(ev);
                        removeWithGuidAndType(hendelse.getGuid(), "R2");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }


    private Hendelse slettHendelse(Hendelse hendelse, String brukerSign) throws Exception {
        String refnummer = hendelse.getRefnummer() != null ? hendelse.getRefnummer().toString() : "";
        String msg = getSlettMsg(refnummer, "warning.slett.hendelse");
        String title = getSlettTitle(refnummer, "warning.slett.hendelse.title");

        int response = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, title, JOptionPane.YES_NO_CANCEL_OPTION);
        if (response == JOptionPane.YES_OPTION) {
            return rskjemaService.deleteHendelse(hendelse, brukerSign);
        }
        return null;
    }

    private Forsikringsskade slettForsikringsskade(Forsikringsskade forsikringsskade, String brukerSign) throws Exception {
        String refnummer = forsikringsskade.getRefnummer() != null ? forsikringsskade.getRefnummer().toString() : "";
        String msg = getSlettMsg(refnummer, "warning.slett.forsikringsskade");
        String title = getSlettTitle(refnummer, "warning.slett.forsikringsskade.title");
        int response = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, title, JOptionPane.YES_NO_CANCEL_OPTION);
        if (response == JOptionPane.YES_OPTION) {
            return rskjemaService.deleteForsikringsskade(forsikringsskade, brukerSign);
        }
        return null;
    }

    /**
     * Sjekker om en forsikringsskade har verdier i enten skadedatofelt eller i felt for antatt tidsrom.
     * Hvis begge er tomme, skal feilmelding gis og lagring avbrytes.
     *
     * @param forsikringsskade
     * @return
     */
    private boolean checkForsikringsskade(Forsikringsskade forsikringsskade) {
        if (forsikringsskade.getSkadeDato() == null &&
                (forsikringsskade.getAntattTidsromForSkaden() == null || forsikringsskade.getAntattTidsromForSkaden().equals(""))) {
            String msg = (Resources.getResource("warning.slett.forsikringsskade.feil"));
            JOptionPane.showMessageDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg);
            return false;
        }
        return true;

    }

    private Skred slettSkred(Skred skred, String brukerSign) throws Exception {
        String refnummer = skred.getRefnummer() != null ? skred.getRefnummer().toString() : "";
        String msg = getSlettMsg(refnummer, "warning.slett.skred");
        String title = getSlettTitle(refnummer, "warning.slett.skred.title");

        int response = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, title, JOptionPane.YES_NO_CANCEL_OPTION);
        if (response == JOptionPane.YES_OPTION) {
            return rskjemaService.deleteSkred(skred, brukerSign);
        }
        return null;
    }

    private String getSlettMsg(String refnummer, String messageKey) {
        String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();
        String msg = Resources.getResource(messageKey, dagerForSletting, refnummer);
        return msg;
    }

    private String getSlettTitle(String refnummer, String titleKey) {
        String title = Resources.getResource(titleKey, refnummer);
        return title;

    }
}
