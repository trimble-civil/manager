package no.mesta.mipss.felt;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.util.StringConcatHelper;
import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class CommonSelectedCriteriaPanel extends JPanel {

    private final FeltController controller;
    private JLabel lblVei;
    private JLabel lblPda;
    private JLabel lblBruker;

    private JLabel lblVeiValue;
    private JLabel lblPdaValue;
    private JLabel lblBrukerValue;
    private JButton btnReset;


    public CommonSelectedCriteriaPanel(FeltController controller) {
        this.controller = controller;
        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        lblVei = new JLabel(Resources.getResource("label.valgteVeier"));
        lblPda = new JLabel(Resources.getResource("label.valgtePda"));
        lblBruker = new JLabel(Resources.getResource("label.valgteBrukere"));

        Font font = lblVei.getFont();
        font = new Font(font.getFontName(), Font.BOLD, font.getSize());
        lblVei.setFont(font);
        lblPda.setFont(font);
        lblBruker.setFont(font);

        lblVeiValue = new JLabel(Resources.getResource("label.alle"));
        lblPdaValue = new JLabel(Resources.getResource("label.alle"));
        lblBrukerValue = new JLabel(Resources.getResource("label.alle"));

        btnReset = new JButton(controller.getTilbakestillKriterierAction());
        btnReset.setMargin(new Insets(0, 0, 0, 0));
        btnReset.setToolTipText(Resources.getResource("button.tilbakestill.tooltip"));
    }

    private void bind() {
        QueryParams qp = controller.getCommonQueryParams();
        Binding<QueryParams, List<Veinettveireferanse>, CommonSelectedCriteriaPanel, List<Veinettveireferanse>> valgteVeier = BindingHelper.createbinding(
                qp, "veiListe", this, "veiListe");
        Binding<QueryParams, Long[], CommonSelectedCriteriaPanel, Long[]> valgtePda = BindingHelper.createbinding(
                qp, "pdaDfuIdentList", this, "pdaDfuIdentList");
        Binding<QueryParams, String[], CommonSelectedCriteriaPanel, String[]> valgteBrukere = BindingHelper.createbinding(
                qp, "brukerList", this, "brukerList");

        controller.getBindingGroup().addBinding(valgteVeier);
        controller.getBindingGroup().addBinding(valgtePda);
        controller.getBindingGroup().addBinding(valgteBrukere);
    }

    public void setVeiListe(List<Veinettveireferanse> veiListe) {
        if (veiListe == null || veiListe.size() == 0) {
            lblVeiValue.setText(Resources.getResource("label.alle"));
            return;
        }
        List<String> veiStringList = new ArrayList<String>();
        for (Veinettveireferanse v : veiListe) {
            veiStringList.add(getVeiString(v));
        }
        String veiString = StringUtils.join(veiStringList, ", ");
        lblVeiValue.setText(veiString);


        lblVeiValue.setToolTipText(StringConcatHelper.createTooltip(veiStringList.toArray()));
    }

    private String getVeiString(Veinettveireferanse v) {
        StringBuilder sb = new StringBuilder();
        sb.append(v.getVeikategori());
        sb.append(v.getVeistatus());
        sb.append(v.getVeinummer());
        sb.append("hp");
        sb.append(v.getHp());
        return sb.toString();
    }

    public void setPdaDfuIdentList(Long[] pdaDfuIdentList) {
        if (pdaDfuIdentList == null || pdaDfuIdentList.length == 0) {
            lblPdaValue.setText(Resources.getResource("label.alle"));
            return;
        }
        String pdaList = StringUtils.join(pdaDfuIdentList, ", ");
        lblPdaValue.setText(pdaList);
        lblPdaValue.setToolTipText(StringConcatHelper.createTooltip(pdaDfuIdentList));

    }

    public void setBrukerList(String[] brukerList) {
        if (brukerList == null || brukerList.length == 0) {
            lblBrukerValue.setText(Resources.getResource("label.alle"));
            return;
        }
        String brukere = StringUtils.join(brukerList, ", ");
        lblBrukerValue.setText(brukere);
        lblBrukerValue.setToolTipText(StringConcatHelper.createTooltip(brukerList));
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.sokekriterier")));

        add(lblVei, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2, 5, 0, 0), 0, 0));
        add(lblPda, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblBruker, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0), 0, 0));

        add(lblVeiValue, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2, 5, 0, 0), 0, 0));
        add(lblPdaValue, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblBrukerValue, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0), 0, 0));
        add(btnReset, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 10, 0), 0, 0));
    }
}
