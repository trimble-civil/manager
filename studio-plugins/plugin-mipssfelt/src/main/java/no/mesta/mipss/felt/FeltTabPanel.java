package no.mesta.mipss.felt;

import no.mesta.mipss.felt.inspeksjon.InspeksjonPanel;
import no.mesta.mipss.felt.sak.SakTabPanel;
import no.mesta.mipss.felt.sak.mangler.ManglerPanel;
import no.mesta.mipss.resources.images.IconResources;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

@SuppressWarnings("serial")
public class FeltTabPanel extends JPanel {

    private final FeltController controller;

    private JTabbedPane tabFelt;

    private SakTabPanel tabSak;
    private InspeksjonPanel tabInspeksjon;
//	private PdaTabPanel tabPda;

    public FeltTabPanel(FeltController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    private void initComponents() {
        tabFelt = new JTabbedPane();
        tabSak = new SakTabPanel(controller);
        tabInspeksjon = new InspeksjonPanel(controller);
//		tabPda = new PdaTabPanel(controller);
        tabFelt.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                stateIsChanged(e);
            }
        });
    }


    /**
     * Workaround for å kunne disable og enable periodevelger dersom brukeren har valgt "kommende mangler" under mangler fanen.
     *
     * @param e
     */
    private void stateIsChanged(ChangeEvent e) {
        JTabbedPane source = (JTabbedPane) e.getSource();
        Component selectedComponent = source.getSelectedComponent();
        if (selectedComponent instanceof SakTabPanel) {
            Component selectedInSak = ((SakTabPanel) selectedComponent).getTabSak().getSelectedComponent();
            if (selectedInSak instanceof ManglerPanel) {
                if (controller.getAvvikController().getQueryParams().isKommendeMangler()) {
                    controller.getCommonQueryParams().setDateEnabled(false);
                }
            }
        } else {
            controller.getCommonQueryParams().setDateEnabled(true);
        }
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        tabFelt.addTab(Resources.getResource("maintab.sak"), IconResources.SAK_ICON_16, tabSak);
        tabFelt.addTab(Resources.getResource("maintab.inspeksjon"), IconResources.BIL_SMALL_ICON, tabInspeksjon);
//		tabFelt.addTab(Resources.getResource("maintab.pda"), IconResources.PDA_ICON, tabPda);

        add(tabFelt, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

}
