package no.mesta.mipss.felt.sak.bilde;

import no.mesta.mipss.felt.sak.rskjema.detaljui.BildePictureModel;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;
import org.jdesktop.swingx.JXImageView;
import org.jdesktop.swingx.JXPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;

@SuppressWarnings("serial")
public class BildePanel extends JXPanel {

    private boolean selected;
    private final Bilde bilde;
    @SuppressWarnings("unused")
    private final String text;
    private JXImageView view;

    public BildePanel(Bilde bilde, String text) {
        this.bilde = bilde;
        this.text = text;

        initComponents();
        initGui();
        view.setPreferredSize(new Dimension(150, 130));
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public Bilde getBilde() {
        return bilde;
    }

    private void initComponents() {

        view = new JXImageView();
        view.setToolTipText("<html><pre>" + bilde.getBeskrivelse() != null ? bilde.getBeskrivelse() : "" + "<br>" + bilde.getFilnavn() + "</pre></html>");

        JLabel lblInfo = new JLabel(bilde.getFilnavn());
        lblInfo.setBackground(new Color(0f, 0f, 0f, 0.75f));
        lblInfo.setOpaque(true);
        lblInfo.setForeground(Color.WHITE);

        view.add(lblInfo);
        view.setImage(bilde.getImage());
        view.setDragEnabled(false);
        view.setEditable(false);
        view.addMouseListener(new ImageClickListener() {
            public void singleClick(MouseEvent e) {
                selectImage();
            }

            public void doubleClick(MouseEvent e) {
                openImage();
            }
        });
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(view, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        setBorder(BorderFactory.createTitledBorder(""));
    }

    private void openImage() {
        BildePictureModel model = new BildePictureModel();
        model.setBilder(Collections.singletonList(bilde));
        JDialog dialog = (JDialog) SwingUtilities.windowForComponent(this);
        JPictureDialogue picture = new JPictureDialogue(dialog, model);
        picture.showBilde(0);
        picture.setVisible(true);
    }

    private void selectImage() {
        if (!isSelected()) {
            view.setBorder(BorderFactory.createLineBorder(Color.GREEN, 3));
            setSelected(true);
        } else {
            view.setBorder(null);
            setSelected(false);
        }
    }

    private class ImageClickListener extends MouseAdapter implements ActionListener {
        private MouseEvent lastEvent;
        private Timer timer;

        private ImageClickListener() {
            timer = new Timer(200, this);
        }

        public void singleClick(MouseEvent e) {
        }

        public void doubleClick(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if (e.getClickCount() > 2) return;
                lastEvent = e;

                if (timer.isRunning()) {
                    timer.stop();
                    doubleClick(lastEvent);
                } else {
                    timer.restart();
                }
            }
        }

        public void actionPerformed(ActionEvent e) {
            timer.stop();
            singleClick(lastEvent);
        }
    }
}
