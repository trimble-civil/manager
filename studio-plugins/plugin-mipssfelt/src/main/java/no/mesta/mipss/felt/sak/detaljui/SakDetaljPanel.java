package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.common.PropertyChangeSource;
import no.mesta.mipss.core.*;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.felt.sak.avvik.detaljui.AvvikDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.HendelseDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.SkredDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r5.ForsikringsskadeDetaljPanel;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.valueobjects.ElrappStatus;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jdesktop.swingx.JXBusyLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class SakDetaljPanel extends JPanel {

    private static final Logger log = LoggerFactory.getLogger(SakDetaljPanel.class);

    private JLabel lblRegDato;
    private JLabel lblAv;
    private JLabel lblPda;
    private JLabel lblVei;
    private JLabel lblSted;
    private JLabel lblKommune;
    private JLabel lblInspeksjon;

    private JFormattedTextField txtRegDato;
    private JFormattedTextField txtAv;
    private JFormattedTextField txtPda;
    private JFormattedTextField txtVei;
    private JFormattedTextField txtSted;
    private JFormattedTextField txtKommune;
    private JFormattedTextField txtInspeksjon;

    private JButton btnVeiEndre;
    private JButton btnInspeksjonApne;
    private JButton btnLagreSak;
    private JButton btnInternnotat;
    private JPopupListMenu btnOpprettR;

    private JTabbedPane tabPane;

    private final Sak sak;
    private final SakController controller;

    private JXBusyLabel lblLoading;

    private BindingGroup bg = new BindingGroup();

    private SakDetaljTabPanelList panels = new SakDetaljTabPanelList();
    private boolean ready;
    private JPanel pnlLoading;

    private SaveStatusRegistry saveStatus;
    private SaveStatusMonitor<Sak> sakMonitor;
    // lytter som tar vare på entiteter/skjemaer som er endret.
    private SaveStatusListener saveStatusListener;

    private boolean lastSlettede;
    private String[] lastSlettedeGuid;
    private String selectedGuid;
    private DeleteRestoreEntityListener deleteListener;
    private JMenuItem opprettAvvik;
    private JMenuItem opprettR2;
    private JMenuItem opprettR5;
    private JMenuItem opprettR11;
    private JXLayer<JButton> btnInternnotatDec;

    // register for å lytte på endringer for saken
    public SaveStatusRegistry getSaveStatusRegistry() {
        return saveStatus;
    }

    public SaveStatusListener getSaveStatusListener() {
        return saveStatusListener;
    }

    public SakDetaljPanel(SakController controller, Sak sak, boolean lastSlettede, String[] lastSlettedeGuid, String selectedGuid, JDialog dialog) {
        log.debug("SakDetaljPanel()");
        this.controller = controller;
        this.sak = sak;
        this.lastSlettede = lastSlettede;
        this.lastSlettedeGuid = lastSlettedeGuid;
        this.selectedGuid = selectedGuid;

        saveStatus = new SaveStatusRegistry();
        sakMonitor = new SaveStatusMonitor<Sak>(Sak.class, this, "sak", saveStatus.getBindingGroup());
        saveStatus.addMonitor(sakMonitor);

        initComponents();
        initGui();
        bind();
        load();
        waitWhileLoading();
    }

    private void checkStedfesting() {
        log.debug("checkStedfesting()");
        if (sak.getStedfesting().size() == 0) {
            JOptionPane.showMessageDialog(this, Resources.getResource("warning.sak.manglerStedfest", EpostUtils.getSupportEpostAdresse()), Resources.getResource("warning.sak.manglerStedfest.title"), JOptionPane.WARNING_MESSAGE);
            Punktstedfest ps = new Punktstedfest();
            ps.setSak(sak);
            ps.setNewEntity(true);
            ps.setGuid(ServerUtils.getInstance().fetchGuid());

            Punktveiref pv = new Punktveiref();
            pv.setFraDato(Clock.now());
            pv.setPunktstedfest(ps);
            pv.setNewEntity(true);
            ps.getVeireferanser().add(pv);

            List<Punktstedfest> psList = new ArrayList<Punktstedfest>();
            psList.add(ps);
            sak.setStedfesting(psList);
        } else {
            //har stedfesting men mangler veiref
            if (sak.getStedfesting().get(0).getVeiref() == null) {
                Punktstedfest sted = sak.getStedfesting().get(0);
                Punktveiref nyVeiref = controller.getFeltController().getPunktveirefFromXY(sted.getSnappetX(), sted.getSnappetY());
                if (nyVeiref == null) {
                    nyVeiref = new Punktveiref();
                    nyVeiref.setNewEntity(true);
                }
                nyVeiref.setPunktstedfest(sted);
                sted.getVeireferanser().add(nyVeiref);
                controller.persistSak(sak);
            }
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void bind() {
        log.debug("bind()");
        Binding regdatoBind = BindingHelper.createbinding(sak, "opprettetDato", txtRegDato, "text");
        Binding<Sak, Object, JFormattedTextField, Object> regAvBind = BindingHelper.createbinding(sak, "opprettetAv", txtAv, "text");
        Binding<Sak, Object, JFormattedTextField, Object> pdaBind = BindingHelper.createbinding(sak, "pdaDfuIdent", txtPda, "text");

        checkStedfesting();
        Binding<Punktveiref, Object, JFormattedTextField, Object> veiBind = BindingHelper.createbinding(sak.getStedfesting().get(0).getVeiref(), "textForGUI", txtVei, "text");


        Binding<Sak, String, JFormattedTextField, String> stedBind = BindingHelper.createbinding(sak, "skadested", txtSted, "text", UpdateStrategy.READ_WRITE);
        Binding<Sak, String, JFormattedTextField, String> kommuneBind = BindingHelper.createbinding(sak, "kommune", txtKommune, "text", UpdateStrategy.READ_WRITE);
        regdatoBind.setConverter(new DateStringConverter(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
        Binding<Inspeksjon, Object, JFormattedTextField, Object> inspeksjonBind = BindingHelper.createbinding(sak.getInspeksjon(), "textForGUI", txtInspeksjon, "text");

        Binding<Sak, Object, JButton, Object> internnotatTooltipBind = BindingHelper.createbinding(sak, "internnotatFormatted", btnInternnotat, "toolTipText");

        Binding<Sak, Object, JButton, Object> openInspBind = BindingHelper.createbinding(sak, "${inspeksjon!=null}", btnInspeksjonApne, "enabled");


        Binding<SakDetaljTabPanelList, Object, JMenuItem, Object> hendelseBind = BindingHelper.createbinding(panels, "${antallHendelser==0&&antallAvvik>0}", opprettR2, "enabled");
        Binding<SakDetaljTabPanelList, Object, JMenuItem, Object> fsBind = BindingHelper.createbinding(panels, "${antallForsikringsskader==0&&antallAvvik>0}", opprettR5, "enabled");
        Binding<SakDetaljTabPanelList, Object, JMenuItem, Object> skredBind = BindingHelper.createbinding(panels, "${antallSkred<1}", opprettR11, "enabled");

        StringNullConverter sn = new StringNullConverter();
        stedBind.setConverter(sn);
        kommuneBind.setConverter(sn);

        bg.addBinding(regdatoBind);
        bg.addBinding(regAvBind);
        bg.addBinding(pdaBind);
        bg.addBinding(veiBind);
        bg.addBinding(stedBind);
        bg.addBinding(kommuneBind);
        bg.addBinding(inspeksjonBind);
        bg.addBinding(internnotatTooltipBind);
        bg.addBinding(openInspBind);

        bg.addBinding(hendelseBind);
        bg.addBinding(fsBind);
        bg.addBinding(skredBind);
        bg.bind();

        saveStatus.getBindingGroup().addBinding(BindingHelper.createbinding(saveStatus, "changed", btnLagreSak, "enabled"));
        sakMonitor.registerField("skadested");
        sakMonitor.registerField("kommune");
        saveStatus.doBind();
    }

    public SaveStatusMonitor<Sak> getSaveMonitor() {
        return sakMonitor;
    }

    public BindingGroup getBindingGroup() {
        return bg;
    }

    public void dispose() {
        for (SakDetaljTabPanel pnl : panels.getPanels()) {
            pnl.dispose();
        }
    }

    public Sak getSak() {
        return sak;
    }

    public void removeTabFor(SakDetaljTabPanel sakDetaljTabPanel) {
        panels.remove(sakDetaljTabPanel);

        if (sakDetaljTabPanel instanceof HendelseDetaljPanel) {
            sak.setHendelse(null);
        }
        if (sakDetaljTabPanel instanceof ForsikringsskadeDetaljPanel) {
            sak.setForsikringsskade(null);
        }
        if (sakDetaljTabPanel instanceof SkredDetaljPanel) {
            sak.setSkred(null);
        }

        int tabCount = tabPane.getTabCount();
        for (int i = 0; i < tabCount; i++) {
            if (tabPane.getComponentAt(i).equals(sakDetaljTabPanel)) {
                tabPane.remove(i);
                return;
            }
        }
    }

    private boolean evaluate(FeltEntityInfo info) {
        if (lastSlettede)
            return true;
        if (lastSlettedeGuid == null)
            return info.getSlettetDato() == null ? true : false;
        else {
            if (info.getSlettetDato() == null)
                return true;
            return Arrays.asList(lastSlettedeGuid).contains(info.getGuid()) ? true : false;
        }
    }

    private Avvik getAvvikFromSak(String avvikGuid) {
        for (Avvik avvik : sak.getAvvik()) {
            if (avvik.getGuid().equals(avvikGuid))
                return avvik;
        }
        return null;
    }

    private void load() {
        log.debug("load()");
        new SwingWorker<Void, Void>() {
            public Void doInBackground() {
                try {
                    saveStatusListener = new SaveStatusListener();
                    deleteListener = new DeleteRestoreEntityListener(SakDetaljPanel.this);

                    //TODO refactor dette...
                    List<FeltEntityInfo> infoList = controller.hentEntityInfoForSakAvvik(sak.getGuid());
                    for (FeltEntityInfo info : infoList) {
                        if (!evaluate(info))
                            continue;
                        //ikke noe poeng å laste denne i egen tråd ettersom sak laster attributtene til de tilknyttede avvikene
                        AvvikDetaljPanel panel = new AvvikDetaljPanel(controller.getFeltController().getAvvikController(), getAvvikFromSak(info.getGuid()), info, SakDetaljPanel.this);
                        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
                        panel.addPropertyChangeListener("changed", saveStatusListener);
                        panel.setDeleteRestoreEntityListener(deleteListener);
                        panels.add(panel);
                    }

                    infoList = controller.hentEntityInfoForSakHendelse(sak.getGuid());
                    for (FeltEntityInfo info : infoList) {
                        if (!evaluate(info))
                            continue;
                        HendelseDetaljPanel panel = new HendelseDetaljPanel(controller.getFeltController().getRskjemaController(), info);
                        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
                        panel.addPropertyChangeListener("changed", saveStatusListener);
                        panel.setDeleteRestoreEntityListener(deleteListener);
                        panels.add(panel);
                    }

                    infoList = controller.hentEntityInfoForSakForsikringsskade(sak.getGuid());
                    for (FeltEntityInfo info : infoList) {
                        if (!evaluate(info))
                            continue;
                        ForsikringsskadeDetaljPanel panel = new ForsikringsskadeDetaljPanel(controller.getFeltController().getRskjemaController(), info);
                        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
                        panel.addPropertyChangeListener("changed", saveStatusListener);
                        panel.setDeleteRestoreEntityListener(deleteListener);
                        panels.add(panel);
                    }

                    infoList = controller.hentEntityInfoForSakSkred(sak.getGuid());
                    for (FeltEntityInfo info : infoList) {
                        if (!evaluate(info))
                            continue;
                        SkredDetaljPanel panel = new SkredDetaljPanel(controller.getFeltController().getRskjemaController(), info);
                        panel.setSak(sak);
                        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
                        panel.addPropertyChangeListener("changed", saveStatusListener);
                        panel.setDeleteRestoreEntityListener(deleteListener);
                        panels.add(panel);
                    }
                    //for å fange opp at sak er endret når bruker lukker vindet
                    sakMonitor.addPropertyChangeListener("changed", saveStatusListener);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                return null;
            }

            @Override
            protected void done() {
                int index = 0;
                int selectedIndex = 0;
                for (SakDetaljTabPanel panel : panels.getPanels()) {
                    if (selectedGuid != null) {
                        if (panel.getFeltEntityInfo().getGuid().equals(selectedGuid)) {
                            selectedIndex = index;
                        }
                    }
                    tabPane.addTab(panel.getTitle(), panel);
                    tabPane.setTabComponentAt(index++, panel.getTabHeader());
                }
                if (tabPane.getTabCount() > selectedIndex)
                    tabPane.setSelectedIndex(selectedIndex);
                ready = true;
            }
        }.execute();
    }

    public void setSelectedGuid(String selectedGuid) {
        this.selectedGuid = selectedGuid;
        if (selectedGuid == null)
            return;
        int i = 0;
        for (SakDetaljTabPanel p : panels.getPanels()) {
            if (p.getFeltEntityInfo().getGuid().equals(selectedGuid)) {
                tabPane.setSelectedIndex(i);
                return;
            }
            i++;
        }
    }

    public void addAvvik(Avvik avvik) {
        if (sak.getAvvik() == null) {
            sak.setAvvik(new ArrayList<Avvik>());
        }
        sak.getAvvik().add(avvik);
        AvvikDetaljPanel panel = new AvvikDetaljPanel(controller.getFeltController().getAvvikController(), avvik, null, this);
        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
        panel.addPropertyChangeListener("changed", saveStatusListener);
        panel.setDeleteRestoreEntityListener(deleteListener);
        panels.add(panel);
        updateTab(panel);
    }

    public void addHendelse(Hendelse hendelse) {
        sak.setHendelse(hendelse);
        HendelseDetaljPanel panel = new HendelseDetaljPanel(controller.getFeltController().getRskjemaController(), hendelse);
        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
        panel.addPropertyChangeListener("changed", saveStatusListener);
        panel.setDeleteRestoreEntityListener(deleteListener);
        panels.add(panel);
        updateTab(panel);
    }

    public void addForsikringsskade(Forsikringsskade forsikringsskade) {
        sak.setForsikringsskade(forsikringsskade);
        ForsikringsskadeDetaljPanel panel = new ForsikringsskadeDetaljPanel(controller.getFeltController().getRskjemaController(), forsikringsskade);
        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
        panel.addPropertyChangeListener("changed", saveStatusListener);
        panel.setDeleteRestoreEntityListener(deleteListener);
        panels.add(panel);
        updateTab(panel);
    }

    public void addSkred(Skred skred) {
        sak.setSkred(skred);
        SkredDetaljPanel panel = new SkredDetaljPanel(controller.getFeltController().getRskjemaController(), skred, sak);
        panel.addPropertyChangeListener("changed", new ChangePropertyListener(panel, tabPane));
        panel.addPropertyChangeListener("changed", saveStatusListener);
        panel.setDeleteRestoreEntityListener(deleteListener);
        panels.add(panel);
        updateTab(panel);
    }

    private void updateTab(SakDetaljTabPanel panel) {
        tabPane.addTab(panel.getTitle(), panel);
        int index = tabPane.getTabCount() - 1;
        tabPane.setTabComponentAt(index, panel.getTabHeader());
        tabPane.setSelectedIndex(index);

    }

    protected void waitWhileLoading() {
        log.debug("waitWhileLoading()");
        new SwingWorker<Void, Void>() {
            @Override
            public Void doInBackground() {
                while (!isAllPanelsLoaded()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            public void done() {
                setLoading(false);
            }
        }.execute();
    }

    private boolean isAllPanelsLoaded() {
        if (panels == null || !ready)
            return false;
        int loaded = panels.getPanels().size();
        for (SakDetaljTabPanel p : panels.getPanels()) {
            if (!p.isLoading()) {
                loaded--;
            }
        }
        return (loaded == 0);
    }

    private void initComponents() {
        log.debug("initComponents()");
        lblRegDato = new JLabel(Resources.getResource("label.regdato"));
        lblAv = new JLabel(Resources.getResource("label.av"));
        lblPda = new JLabel(Resources.getResource("label.pda"));
        lblVei = new JLabel(Resources.getResource("label.vei"));
        lblSted = new JLabel(Resources.getResource("label.sted"));
        lblKommune = new JLabel(Resources.getResource("label.kommune"));
        lblInspeksjon = new JLabel(Resources.getResource("label.inspeksjon"));

        txtRegDato = new JFormattedTextField();
        txtAv = new JFormattedTextField();
        txtPda = new JFormattedTextField();
        txtVei = new JFormattedTextField();
        txtSted = new JFormattedTextField();
        txtKommune = new JFormattedTextField();
        txtInspeksjon = new JFormattedTextField();

        txtRegDato.setEditable(false);
        txtAv.setEditable(false);
        txtPda.setEditable(false);
        txtVei.setEditable(false);
        txtInspeksjon.setEditable(false);

        btnVeiEndre = new JButton(controller.getEndreVeiAction(this));
        btnInspeksjonApne = new JButton(controller.getOpenInspeksjonAction(this));
        btnLagreSak = new JButton(controller.getLagreSakAction(this));
        btnLagreSak.setToolTipText(Resources.getResource("button.lagreSak.tooltip"));

        btnInternnotat = new JButton(controller.getVisInternnotatAction(this));
        btnInternnotatDec = new JXLayer<JButton>(btnInternnotat);
        AbstractLayerUI<JButton> decoratorUI = new AbstractLayerUI<JButton>() {
            @Override
            protected void paintLayer(Graphics2D g2, JXLayer<JButton> l) {
                super.paintLayer(g2, l);
                if (!StringUtils.isBlank(sak.getInternnotat())) {
                    Graphics2D g = (Graphics2D) g2.create();
                    Font f = g.getFont();
                    g.setFont(new Font(f.getName(), Font.PLAIN, 15));
                    g.setPaint(new Color(0, 150, 0));
                    g.drawString("*", 5, 17);
                    g.dispose();
                }
            }
        };

        btnInternnotatDec.setUI(decoratorUI);

        List<JMenuItem> popupList = new ArrayList<JMenuItem>();

        opprettAvvik = new JMenuItem(controller.getOpprettAvvikAcion(this));
        opprettR2 = new JMenuItem(controller.getOpprettRskjemaAction(this, ElrappTypeR.Type.R2));
        opprettR5 = new JMenuItem(controller.getOpprettRskjemaAction(this, ElrappTypeR.Type.R5));
        opprettR11 = new JMenuItem(controller.getOpprettRskjemaAction(this, ElrappTypeR.Type.R11));

        popupList.add(opprettAvvik);
        popupList.add(opprettR2);
        popupList.add(opprettR5);
        popupList.add(opprettR11);
        btnOpprettR = new JPopupListMenu(Resources.getResource("button.opprettR"), IconResources.ATTACHMENT, popupList);


        lblLoading = new JXBusyLabel();
        lblLoading.setText(Resources.getResource("label.lasterSak"));
        setLoading(true);

        tabPane = new JTabbedPane();

        //TODO implementer..
        btnInspeksjonApne.setEnabled(false);
//		btnOpprettR.setEnabled(false);
    }

    public void setLoading(boolean loading) {
        lblLoading.setBusy(loading);
        lblLoading.setVisible(loading);
    }

    public void btnVeiEndreClick(Action callback) {
        btnVeiEndre.doClick();
        callback.actionPerformed(null);
    }

    private void initGui() {
        log.debug("initGui()");
        setLayout(new GridBagLayout());

        JPanel pnlSted = new JPanel(new GridBagLayout());
        pnlSted.add(txtSted, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        pnlSted.add(lblKommune, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        pnlSted.add(txtKommune, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));

        pnlLoading = new JPanel(new GridBagLayout());
        pnlLoading.add(lblLoading, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        add(lblRegDato, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(txtRegDato, new GridBagConstraints(1, 0, 1, 1, 2.5, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        add(lblAv, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(txtAv, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        add(lblPda, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(txtPda, new GridBagConstraints(5, 0, 1, 1, 0.5, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        add(lblVei, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(txtVei, new GridBagConstraints(7, 0, 1, 1, 0.5, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
        add(btnVeiEndre, new GridBagConstraints(8, 0, 1, 1, 0.3, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));

        add(lblSted, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
        add(pnlSted, new GridBagConstraints(1, 1, 5, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
        add(lblInspeksjon, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(txtInspeksjon, new GridBagConstraints(7, 1, 1, 1, 0.7, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
        add(btnInspeksjonApne, new GridBagConstraints(8, 1, 1, 1, 0.3, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
        add(btnLagreSak, new GridBagConstraints(8, 2, 1, 1, 0.3, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

        JPanel p = new JPanel();
        p.add(btnInternnotatDec);
        p.add(btnOpprettR);

        add(p, new GridBagConstraints(5, 2, 3, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        add(pnlLoading, new GridBagConstraints(0, 3, 10, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
        add(tabPane, new GridBagConstraints(0, 4, 10, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    public void oppdaterElrappKule(ElrappStatus status) {
        HendelseDetaljPanel hendelseDetaljPanel = getHendelseDetaljPanel();
        if(hendelseDetaljPanel != null) {
            hendelseDetaljPanel.getTabHeader().setElrappStatus(status);
        }
    }

    public HendelseDetaljPanel getHendelseDetaljPanel() {
        for(SakDetaljTabPanel panel : panels.getPanels()) {
            if(panel instanceof HendelseDetaljPanel) {
                return (HendelseDetaljPanel) panel;
            }
        }
        return null;
    }

    public class SakDetaljTabPanelList implements PropertyChangeSource {
        private List<SakDetaljTabPanel> panels = new ArrayList<SakDetaljTabPanel>();
        private PropertyChangeSupport props;

        public SakDetaljTabPanelList() {
            props = new PropertyChangeSupport(this);
        }

        public void add(SakDetaljTabPanel panel) {
            Integer oldAntallHendelser = getAntallHendelser();
            int oldAntallForsikringsskader = getAntallForsikringsskader();
            int oldAntallSkred = getAntallSkred();
            int oldAntallAvvik = getAntallAvvik();
            panels.add(panel);
            props.firePropertyChange("antallHendelser", oldAntallHendelser, getAntallHendelser());
            props.firePropertyChange("antallForsikringsskader", oldAntallForsikringsskader, getAntallForsikringsskader());
            props.firePropertyChange("antallSkred", oldAntallSkred, getAntallSkred());
            props.firePropertyChange("antallAvvik", oldAntallAvvik, getAntallAvvik());
        }

        public void remove(SakDetaljTabPanel panel) {
            Integer oldAntallHendelser = getAntallHendelser();
            int oldAntallForsikringsskader = getAntallForsikringsskader();
            int oldAntallSkred = getAntallSkred();
            int oldAntallAvvik = getAntallAvvik();
            panels.remove(panel);
            props.firePropertyChange("antallHendelser", oldAntallHendelser, getAntallHendelser());
            props.firePropertyChange("antallForsikringsskader", oldAntallForsikringsskader, getAntallForsikringsskader());
            props.firePropertyChange("antallSkred", oldAntallSkred, getAntallSkred());
            props.firePropertyChange("antallAvvik", oldAntallAvvik, getAntallAvvik());

        }

        public List<SakDetaljTabPanel> getPanels() {
            return panels;
        }

        private int getPanelCount(Class<?> c) {
            int count = 0;
            for (SakDetaljTabPanel p : panels) {
                if (p.getClass().equals(c)) {
                    count++;
                }
            }
            return count;
        }

        public int getAntallSkred() {
            return getPanelCount(SkredDetaljPanel.class);
        }

        public int getAntallForsikringsskader() {
            return getPanelCount(ForsikringsskadeDetaljPanel.class);
        }

        public Integer getAntallHendelser() {
            return getPanelCount(HendelseDetaljPanel.class);
        }

        public int getAntallAvvik() {
            return getPanelCount(AvvikDetaljPanel.class);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener l) {
            props.addPropertyChangeListener(l);
        }

        @Override
        public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
            props.addPropertyChangeListener(propName, l);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener l) {
            props.removePropertyChangeListener(l);
        }

        @Override
        public void removePropertyChangeListener(String propName, PropertyChangeListener l) {
            props.removePropertyChangeListener(propName, l);
        }
    }
}
