package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.inspeksjon.detaljui.InspeksjonDetaljPanel;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

@SuppressWarnings("serial")
public class InspeksjonDetaljDialog extends JDialog {

    private final InspeksjonSokResult inspeksjonSokResult;
    private final InspeksjonController controller;

    private InspeksjonDetaljPanel pnlDetaljer;
    private final Inspeksjon inspeksjon;

    public InspeksjonDetaljDialog(InspeksjonController controller, InspeksjonSokResult inspeksjonSokResult) {
        this(controller, inspeksjonSokResult, null);
    }

    public InspeksjonDetaljDialog(InspeksjonController controller, InspeksjonSokResult inspeksjonSokResult, Inspeksjon inspeksjon) {
        super(controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
        this.controller = controller;
        this.inspeksjonSokResult = inspeksjonSokResult;
        this.inspeksjon = inspeksjon;
        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlDetaljer = new InspeksjonDetaljPanel(controller, inspeksjonSokResult, inspeksjon);
        setTitle("Inspeksjon id:" + inspeksjonSokResult.getRefnummer());
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (pnlDetaljer.getSaveStatusRegistry().isChanged()) {
                    int v = JOptionPane.showConfirmDialog(pnlDetaljer, Resources.getResource("message.inspeksjon.ikkelagret"), Resources.getResource("message.inspeksjon.ikkelagret.title"), JOptionPane.YES_NO_OPTION);
                    if (v == JOptionPane.YES_OPTION) {
                        controller.persistInspeksjon(pnlDetaljer.getInspeksjon());
                    }
                }
            }
        });
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(pnlDetaljer, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 5, 5, 5), 0, 0));
        setSize(840, 690);
    }
}
