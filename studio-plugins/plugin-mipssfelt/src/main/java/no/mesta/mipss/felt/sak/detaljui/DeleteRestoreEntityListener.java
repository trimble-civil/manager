package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.felt.sak.SakDetaljTabPanel;

public class DeleteRestoreEntityListener {

    private final SakDetaljPanel sakDetaljPanel;

    public DeleteRestoreEntityListener(SakDetaljPanel sakDetaljPanel) {
        this.sakDetaljPanel = sakDetaljPanel;
    }

    public void entityDeleted(SakDetaljTabPanel sakDetaljTabPanel) {
        sakDetaljPanel.removeTabFor(sakDetaljTabPanel);

    }

    public void entityRestored(SakDetaljTabPanel sakDetaljTabPanel) {
        //TODO implement fuzzy logic...

    }

}
