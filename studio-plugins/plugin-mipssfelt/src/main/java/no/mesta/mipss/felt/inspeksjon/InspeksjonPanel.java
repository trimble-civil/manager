package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.SokekriterierTaskPane;
import no.mesta.mipss.ui.JMipssBeanScrollPane;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class InspeksjonPanel extends JPanel {

    private final InspeksjonController controller;

    private SokekriterierTaskPane taskPaneSok;
    private SokekriterierInspeksjonPanel pnlSokekriterier;

    private JButton btnSok;
    private JButton btnHentMedId;
    private JPanel pnlButton;
    private InspeksjonButtonPanel pnlInspeksjonButtons;

    private JMipssBeanScrollPane scrTable;


    public InspeksjonPanel(FeltController controller) {
        this.controller = controller.getInspeksjonController();
        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlSokekriterier = new SokekriterierInspeksjonPanel(controller);
        taskPaneSok = new SokekriterierTaskPane(pnlSokekriterier);

        btnSok = new JButton(controller.getInspeksjonSokButtonAction());
        btnHentMedId = new JButton(controller.getInspeksjonHentMedIdAction());

        scrTable = new JMipssBeanScrollPane(controller.getInspeksjonTable(), controller.getInspeksjonTableModel());

        pnlButton = new JPanel(new GridBagLayout());
        pnlInspeksjonButtons = new InspeksjonButtonPanel(controller);
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        pnlButton.add(btnSok, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnHentMedId, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(taskPaneSok, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));
        add(scrTable, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 0, 5, 0), 0, 0));
        add(pnlInspeksjonButtons, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));

    }
}
