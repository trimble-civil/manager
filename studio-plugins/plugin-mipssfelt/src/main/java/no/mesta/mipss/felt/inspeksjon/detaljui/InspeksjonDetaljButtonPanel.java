package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.felt.inspeksjon.InspeksjonController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class InspeksjonDetaljButtonPanel extends JPanel {

    private final InspeksjonController controller;

    private JButton btnSlett;
    private JButton btnLagre;

    private final InspeksjonDetaljPanel inspeksjonDetaljPanel;

    public InspeksjonDetaljButtonPanel(InspeksjonController controller, InspeksjonDetaljPanel inspeksjonDetaljPanel) {
        this.controller = controller;
        this.inspeksjonDetaljPanel = inspeksjonDetaljPanel;
        initComponents();
        initGui();

    }

    public void setGjenopprett(boolean gjenopprett) {
        //TODO ..
//		if (gjenopprett){
//			btnSlett.setAction(controller.getInspeksjonGjenopprettAction(avvikDetaljPanel, getRestoreAvvikCallback()));
//		}else{
//			btnSlett.setAction(controller.getAvvikSlettAction(avvikDetaljPanel, getSlettAvvikCallback()));
//		}
    }

    public void setLagreEnabled(boolean enabled) {
        btnLagre.setEnabled(enabled);
    }

    private void initComponents() {
        btnSlett = new JButton(controller.getInspeksjonSlettAction(inspeksjonDetaljPanel, getSlettInspeksjonCallback()));
        btnLagre = new JButton(controller.getInspeksjonLagreAction(inspeksjonDetaljPanel));
        btnLagre.setEnabled(false);
    }

    private Action getSlettInspeksjonCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inspeksjonDetaljPanel.entityDeleted();
            }
        };
    }


    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnSlett, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(btnLagre, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));


    }
}
