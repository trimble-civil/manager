package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.felt.sak.rskjema.RskjemaController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class HendelseDetaljButtonPanel extends JPanel {
    private JButton btnLagre;
    private JButton btnSlett;
    private JButton btnRapport;
    private JButton btnElrapp;

    private RskjemaController controller;
    private HendelseDetaljPanel hendelseDetaljPanel;

    public HendelseDetaljButtonPanel(RskjemaController controller, HendelseDetaljPanel hendelseDetaljPanel) {
        this.controller = controller;
        this.hendelseDetaljPanel = hendelseDetaljPanel;
        initComponents();
        initGui();

    }

    public void setGjenopprett(boolean gjenopprett) {
        if (gjenopprett) {
            btnSlett.setAction(controller.getHendelseRestoreAction(hendelseDetaljPanel, getRestoreHendelseCallback()));
            btnElrapp.setEnabled(false);
        } else {
            btnSlett.setAction(controller.getHendelseSlettAction(hendelseDetaljPanel, getSlettHendelseCallback()));
            btnElrapp.setEnabled(true);
        }
    }

    private Action getSlettHendelseCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hendelseDetaljPanel.entityDeleted();
            }
        };
    }

    private Action getRestoreHendelseCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hendelseDetaljPanel.entityRestored();
                setGjenopprett(false);
            }
        };
    }

    private void initComponents() {
        btnLagre = new JButton(controller.getHendelseLagreAction(hendelseDetaljPanel));
        btnLagre.setEnabled(false);
        btnSlett = new JButton(controller.getHendelseSlettAction(hendelseDetaljPanel, getSlettHendelseCallback()));
        btnRapport = new JButton(controller.getR2RapportAction(hendelseDetaljPanel));
        btnElrapp = new JButton(controller.getR2SendElrappAction(hendelseDetaljPanel));
    }

    public void setEnabled(boolean enabled) {
        btnLagre.setEnabled(enabled);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnSlett, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(btnRapport, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnElrapp, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnLagre, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    }

}