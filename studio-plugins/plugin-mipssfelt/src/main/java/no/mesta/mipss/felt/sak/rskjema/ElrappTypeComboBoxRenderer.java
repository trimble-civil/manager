package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.valueobjects.ElrappTypeR;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ElrappTypeComboBoxRenderer extends DefaultListCellRenderer {

    private boolean allOnNull;

    public ElrappTypeComboBoxRenderer() {

    }

    /**
     * @param allOnNull Skriver "alle" dersom value er null
     */
    public ElrappTypeComboBoxRenderer(boolean allOnNull) {
        this.allOnNull = allOnNull;

    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        label.setText(" ");
        ElrappTypeR type = (ElrappTypeR) value;
        if (type != null && type.getType() != null) {
            switch (type.getType()) {
                case R2:
                    label.setText(Resources.getResource("label.elrappType.r2"));
                    break;
                case R5:
                    label.setText(Resources.getResource("label.elrappType.r5"));
                    break;
                case R11:
                    label.setText(Resources.getResource("label.elrappType.r11"));
                    break;
            }
        } else if (allOnNull) {
            label.setText(Resources.getResource("label.alle"));
        }

        return label;
    }
}
