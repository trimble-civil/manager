package no.mesta.mipss.felt.sak.rskjema.detaljui.r5;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.felt.util.ImageDropper;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;
import no.mesta.mipss.valueobjects.ElrappStatus;
import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class ForsikringsskadeDetaljPanel extends SakDetaljTabPanel {

    private String title;
    private RskjemaController controller;
    private FeltEntityInfo info;
    private R5Panel pnlR5;
    private Forsikringsskade forsikringsskade;
    private ForsikringsskadeBildePanel pnlBilder;
    private ForsikringsskadeButtonPanel pnlButtons;

    public ForsikringsskadeDetaljPanel(RskjemaController controller, FeltEntityInfo info) {
        this.controller = controller;
        this.info = info;
        this.title = "R5 (" + info.getRefnummer() + ")";
        initComponents();
        initGui();
        doLoad();
        waitWhileLoading();
    }

    public ForsikringsskadeDetaljPanel(RskjemaController controller, Forsikringsskade forsikringsskade) {
        this.controller = controller;
        this.title = "R5 (" + forsikringsskade.getRefnummer() + ")";
        initComponents();
        initGui();
        setForsikringsskade(forsikringsskade);
        bind();
    }

    private void initComponents() {
        pnlR5 = new R5Panel(controller);
        pnlBilder = new ForsikringsskadeBildePanel(controller);
        new ImageDropper(pnlBilder, new ImageDropper.ImageDropListener() {
            @Override
            public void imageDropped(Bilde[] images) {
                for (Bilde i : images) {
                    pnlBilder.addBilde(i);
                }
            }
        }, controller.getFeltController().getLoggedOnUserSign());
        pnlR5.addTab(pnlBilder, "Bilder");
        pnlButtons = new ForsikringsskadeButtonPanel(controller, this);

        loadingPanels = new ArrayList<LoadingPanel>();
        loadingPanels.add(pnlR5);
        loadingPanels.add(pnlBilder);

    }

    @Override
    public void setChanged(boolean changed) {
        super.setChanged(changed);
        pnlButtons.setEnabled(changed);
    }

    public Forsikringsskade getForsikringsskade() {
        List<SkadeKontakt> sk = forsikringsskade.getSkadeKontakter();
        List<SkadeKontakt> toRemove = new ArrayList<SkadeKontakt>();
        if (sk != null) {
            for (SkadeKontakt s : sk) {
                if (isEmptySkadeKontakt(s)) {
                    toRemove.add(s);
                }
            }
        }
        sk.removeAll(toRemove);
        return forsikringsskade;
    }

    private boolean isEmptySkadeKontakt(SkadeKontakt sk) {
        return StringUtils.isBlank(sk.getAdresse())
                && StringUtils.isBlank(sk.getEpostAdresse())
                && StringUtils.isBlank(sk.getNavn())
                && StringUtils.isBlank(sk.getPoststed())
                && StringUtils.isBlank(sk.getKontaktNavn())
                && StringUtils.isBlank(sk.getResultat())
                && sk.getDato() == null
                && sk.getPostnummer() == null
                && sk.getTlfnummer() == null
                && sk.getKontaktTlfnummer() == null
                && (sk.getPostnummer() == null || sk.getPostnummer().intValue() == 0);
    }

    @Override
    protected void initGui() {
        super.initGui();
        setLayout(new BorderLayout());
        add(pnlSlettetInfo, BorderLayout.NORTH);
        add(pnlR5, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);
    }

    @Override
    protected void bind() {
        SaveStatusMonitor<Forsikringsskade> fsMonitor = new SaveStatusMonitor<Forsikringsskade>(Forsikringsskade.class, this, "forsikringsskade", getSaveStatusRegistry().getBindingGroup());
        fsMonitor.registerField("anmeldt");
        fsMonitor.registerField("navnBerg1");
        fsMonitor.registerField("tlfBerg1");
        fsMonitor.registerField("kontaktNavnBerg1");
        fsMonitor.registerField("kontaktTlfBerg1");
        fsMonitor.registerField("resultatBerg1");
        fsMonitor.registerField("navnBerg2");
        fsMonitor.registerField("tlfBerg2");
        fsMonitor.registerField("kontaktNavnBerg2");
        fsMonitor.registerField("kontaktTlfBerg2");
        fsMonitor.registerField("resultatBerg2");
        fsMonitor.registerField("antattTidsromForSkaden");
        fsMonitor.registerField("eierAdresse");
        fsMonitor.registerField("eierAvKjoretoy");
        fsMonitor.registerField("eierTelefonNr");
        fsMonitor.registerField("forerNavn");
        fsMonitor.registerField("forerTlf");
        fsMonitor.registerField("forsikringsselskap");
        fsMonitor.registerField("politiKontaktNavn");
        fsMonitor.registerField("politiKontaktTlf");
        fsMonitor.registerField("politiKontaktResultat");
        fsMonitor.registerField("politiKontaktDato");
        fsMonitor.registerField("vitneNavn");
        fsMonitor.registerField("vitneDato");
        fsMonitor.registerField("tilleggsopplysninger");
        fsMonitor.registerField("entrepenorDato");
        fsMonitor.registerField("entrepenorNavn");
        fsMonitor.registerField("estimatKostnad");
        fsMonitor.registerField("fritekst");
        fsMonitor.registerField("meldtAv");
        fsMonitor.registerField("meldtDato");
        fsMonitor.registerField("reparertDato");
        fsMonitor.registerField("skadeDato");
        fsMonitor.registerField("skadevolderBilmerke");
        fsMonitor.registerField("skadevolderHenger");
        fsMonitor.registerField("skadevolderRegnr");
        fsMonitor.registerField("utsattRepKommentar");
        fsMonitor.registerField("skjemaEmne");
        fsMonitor.registerField("bilder");
        fsMonitor.registerField("antattTidsromForSkaden");
        fsMonitor.registerField("endretDato");

        getSaveStatusRegistry().addMonitor(fsMonitor);

        getSaveStatusRegistry().getBindingGroup().addBinding(BindingHelper.createbinding(getSaveStatusRegistry(), "changed", this, "changed"));
        getSaveStatusRegistry().doBind();
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
    }

    @Override
    protected void doneLoading() {
        setForsikringsskade(forsikringsskade);

    }

    public void setForsikringsskade(Forsikringsskade forsikringsskade) {
        if (forsikringsskade.getSlettetDato() != null) {
            pnlSlettetInfo.setVisible(true);
            lblSlettet.setText("Forsikringsskaden er slettet, " + MipssDateFormatter.formatDate(forsikringsskade.getSlettetDato()) + " av " + forsikringsskade.getSlettetAv());
            pnlButtons.setGjenopprett(true);
            panelDisabledIndicator = new PanelDisabledIndicator(pnlR5);
        }
        this.forsikringsskade = forsikringsskade;
        pnlR5.setForsikringsskade(forsikringsskade);
        pnlBilder.setForsikringsskade(forsikringsskade);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    protected void load() {
        forsikringsskade = controller.hentForsikringsskade(info.getGuid());
    }

    @Override
    public MipssFieldDetailItem getDetailEntity() {
        return forsikringsskade;
    }

    @Override
    public FeltEntityInfo getFeltEntityInfo() {
        return info;
    }

    public void setTitle(String refnummer) {
        this.title = "R5 (" + refnummer + ")";
        getTabHeader().setTitle(title);
    }

    @Override
    public ElrappStatus getElrappStatus() {
        return ElrappStatus.getElrappStatus(forsikringsskade.getRapportertDato(), forsikringsskade.getEndretDato());
    }

    public void oppdaterElrappKule() {
        getTabHeader().setElrappStatus(getElrappStatus());
    }
}
