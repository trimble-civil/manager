package no.mesta.mipss.felt.sak.mangler;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.taskpane.LinkLabel;
import no.mesta.mipss.ui.taskpane.MipssTaskPane;
import no.mesta.mipss.ui.taskpane.MipssTaskPaneContainer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
/**
 * Flyttet til egen fane
 */
@Deprecated
public class ManglerTaskPane extends MipssTaskPaneContainer {
    private MipssTaskPane paneMangler;
    private ManglerPanel pnlMangler;
    private final SakController controller;
    private JLabel lblAntallMangler;
    private JLabel lblAntallManglerValue;

    public ManglerTaskPane(SakController controller) {
        this.controller = controller;

        initComponents();
    }

    @Override
    public void setSize(int x, int y) {
        super.setSize(x, y);
        System.out.println("setsize..");
    }

    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
        pnlMangler.setPreferredSize(new Dimension(preferredSize.width - 5, preferredSize.height));
        pnlMangler.setSize(new Dimension(preferredSize.width - 5, preferredSize.height));

    }

    private void initComponents() {
        lblAntallMangler = new JLabel(Resources.getResource("label.antallMangler"));
        lblAntallManglerValue = new JLabel("0");
        pnlMangler = new ManglerPanel(controller.getFeltController().getAvvikController());

//		pnlMangler.setPreferredSize(new Dimension(940, 300));
        paneMangler = new MipssTaskPane();

//		paneMangler.setTitleLeftComponent(new JLabel(Resources.getResource("label.avansertSok")));
        paneMangler.setContent(pnlMangler);
        paneMangler.setTitleContent(createSearchPaneTitle());

        addTaskPane(paneMangler);
        paneMangler.setCollapsed(true);

        controller.getFeltController().getAvvikController().getManglerTableModel().addTableLoaderListener(new MipssBeanLoaderListener() {
            @Override
            public void loadingStarted(MipssBeanLoaderEvent event) {
                lblAntallManglerValue.setText("vent litt...");
            }

            @Override
            public void loadingFinished(MipssBeanLoaderEvent event) {
                setAntall(controller.getFeltController().getAvvikController().getManglerTableModel().getEntities().size());
            }
        });
    }

    public void setAntall(int antall) {
        lblAntallManglerValue.setText(String.valueOf(antall));
    }

    private JPanel createSearchPaneTitle() {
        final LinkLabel label = new LinkLabel(Resources.getResource("label.visMangler"));

        Action labelAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (paneMangler.isCollapsed()) {
                    paneMangler.setCollapsed(false);
                    label.setText(Resources.getResource("label.skjulMangler"));
                } else {
                    paneMangler.setCollapsed(true);
                    label.setText(Resources.getResource("label.visMangler"));
                }
            }
        };
        final JCheckBox chkVisAlle = new JCheckBox(Resources.getResource("label.visAlle"));
        chkVisAlle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO implementer action for å vise alle mangler siden tidenes morgen..(eller 01.09.2010 fra konfigparam...)
                if (chkVisAlle.isSelected()) {
                    controller.getManglerQueryParams().setAlle(Boolean.TRUE);
                } else {
                    controller.getManglerQueryParams().setAlle(Boolean.FALSE);
                }
                controller.getFeltController().getAvvikController().getManglerTableModel().loadData();
            }
        });
        chkVisAlle.setOpaque(false);
        label.setAction(labelAction);
        JPanel titleContent = new JPanel();
        titleContent.setLayout(new BoxLayout(titleContent, BoxLayout.LINE_AXIS));
        titleContent.add(Box.createHorizontalStrut(10));
        titleContent.add(lblAntallMangler);
        titleContent.add(lblAntallManglerValue);
        titleContent.add(Box.createHorizontalStrut(20));
        titleContent.add(label);
        titleContent.add(Box.createHorizontalGlue());

        titleContent.add(chkVisAlle);


        return titleContent;
    }
}
