package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.SokekriterierTaskPane;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.Sphere;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class RskjemaPanel extends JPanel {

    private final RskjemaController controller;
    private SokekriterierTaskPane taskPaneSok;
    private SokekriterierRskjemaPanel pnlSokekriterier;

    private JButton btnSok;

    private JPanel pnlButton;
    private RskjemaButtonPanel pnlRskjemaButtons;
    private JMipssBeanScrollPane scrTable;

    private JMenuItem hentR2;
    private JMenuItem hentR5;
    private JMenuItem hentR11;
    private JMenuItem hentElrappR2;
    private JMenuItem hentElrappR5;
    private JMenuItem hentElrappR11;
    private JPopupListMenu btnHentMedId;
    private JPopupListMenu btnHentMedElrappId;

    public RskjemaPanel(FeltController controller) {
        this.controller = controller.getRskjemaController();

        initComponents();
        initGui();
    }

    private JPanel createLegend() {
        Sphere red = new Sphere(Color.red, 20);
        Sphere yellow = new Sphere(Color.yellow, 20);
        Sphere green = new Sphere(Color.green, 20);

        JLabel ikkeSendt = new JLabel("Ikke sendt", red.getIcon(), JLabel.LEFT);
        ikkeSendt.setToolTipText(Resources.getResource("label.elrappStatus.ikkeSendt"));
        JLabel endret = new JLabel("Endret", yellow.getIcon(), JLabel.LEFT);
        endret.setToolTipText(Resources.getResource("label.elrappStatus.endret"));
        JLabel rappOk = new JLabel("Rapportert Ok", green.getIcon(), JLabel.LEFT);
        rappOk.setToolTipText(Resources.getResource("label.elrappStatus.rapportertOk"));

        JPanel panel = new JPanel();
        panel.add(ikkeSendt);
        panel.add(endret);
        panel.add(rappOk);
        return panel;
    }

    private void initComponents() {
        pnlSokekriterier = new SokekriterierRskjemaPanel(controller);
        taskPaneSok = new SokekriterierTaskPane(pnlSokekriterier);

        btnSok = new JButton(controller.getRskjemaSokButtonAction());

        List<JMenuItem> popupList = new ArrayList<JMenuItem>();
        hentR2 = new JMenuItem(controller.getHendelseMedIdAction());
        hentR5 = new JMenuItem(controller.getForsikringsskadeMedIdAction());
        hentR11 = new JMenuItem(controller.getSkredMedIdAction());
        popupList.add(hentR2);
        popupList.add(hentR5);
        popupList.add(hentR11);
        btnHentMedId = new JPopupListMenu(Resources.getResource("button.hentRskjemaMedId"), IconResources.DETALJER_SMALL_ICON, popupList);

        List<JMenuItem> popupListElrapp = new ArrayList<JMenuItem>();
        hentElrappR2 = new JMenuItem(controller.getHendelseMedElrappIdAction());
        hentElrappR5 = new JMenuItem(controller.getForsikringsskadeMedElrappIdAction());
        hentElrappR11 = new JMenuItem(controller.getSkredMedElrappIdAction());
        popupListElrapp.add(hentElrappR2);
        popupListElrapp.add(hentElrappR5);
        popupListElrapp.add(hentElrappR11);
        btnHentMedElrappId = new JPopupListMenu(Resources.getResource("button.hentRskjemaMedElrappId"), IconResources.DETALJER_SMALL_ICON, popupListElrapp);


        pnlButton = new JPanel(new GridBagLayout());
        scrTable = new JMipssBeanScrollPane(controller.getRskjemaTable(), controller.getRskjemaTableModel());

        pnlRskjemaButtons = new RskjemaButtonPanel(controller);


    }

    private void initGui() {
        setLayout(new GridBagLayout());

        pnlButton.add(btnSok, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
        pnlButton.add(btnHentMedId, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlButton.add(btnHentMedElrappId, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(taskPaneSok, new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(createLegend(), new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(scrTable, new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 0, 5, 0), 0, 0));
        add(pnlRskjemaButtons, new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));

    }
}
