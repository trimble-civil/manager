package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.ui.table.AvvikstatusTableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import org.jdesktop.swingx.JXTable;

import java.util.ArrayList;
import java.util.List;

public class AvvikstatusTypePanel extends TableObjectPanel<AvvikstatusTableObject, AvvikstatusType> {

    public AvvikstatusTypePanel(List<AvvikstatusType> alleAvvikstatus) {
        super(alleAvvikstatus);
    }

    @Override
    protected JXTable getTable() {
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(AvvikstatusTableObject.class, columns);
        tableModel = new MipssRenderableEntityTableModel<AvvikstatusTableObject>(AvvikstatusTableObject.class, new int[]{0}, columns);
        tableModel.getEntities().clear();
        List<AvvikstatusTableObject> list = new ArrayList<AvvikstatusTableObject>();
        for (AvvikstatusType t : alleAvvikstatus) {
            list.add(new AvvikstatusTableObject(t));
        }
        tableModel.getEntities().addAll(list);
        JXTable table = new JXTable(tableModel, columnModel);
        table.getColumn(0).setMaxWidth(30);

        return table;
    }

    public void setSelected(List<String> status) {
        if (status == null)
            return;
        for (AvvikstatusTableObject o : tableModel.getEntities()) {
            if (status.contains(o.getStatus())) {
                o.setValgt(Boolean.TRUE);
            }
        }
    }

    public List<String> getSelected() {
        List<String> ids = new ArrayList<String>();
        for (AvvikstatusTableObject t : checkTable.getAlleValgte()) {
            ids.add(t.getStatus());
        }
        return ids;
    }

}
