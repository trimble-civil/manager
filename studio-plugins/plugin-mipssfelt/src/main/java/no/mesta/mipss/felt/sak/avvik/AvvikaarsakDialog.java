package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.persistence.mipssfield.Avvikaarsak;
import no.mesta.mipss.ui.table.AvvikaarsakTableObject;

import javax.swing.*;
import java.util.List;

@SuppressWarnings("serial")
public class AvvikaarsakDialog extends TableObjectDialog<AvvikaarsakTableObject, Avvikaarsak> {

    public AvvikaarsakDialog(JFrame owner, List<Avvikaarsak> alleStatuser) {
        super(owner, alleStatuser);
    }

    @Override
    protected TableObjectPanel<AvvikaarsakTableObject, Avvikaarsak> getPanel() {
        return new AvvikaarsakPanel(alleStatuser);
    }

    public void setSelected(List<Long> status) {
        ((AvvikaarsakPanel) pnlStatus).setSelected(status);
    }

    public List<Long> getSelectedIds() {
        return ((AvvikaarsakPanel) pnlStatus).getSelected();
    }

    public List<String> getSelectedNames() {
        return ((AvvikaarsakPanel) pnlStatus).getSelectedNames();
    }
}