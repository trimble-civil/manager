package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.ProcessRelatedPanel;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class VurderingPanel extends JPanel implements LoadingPanel, ProcessRelatedPanel<Hendelse> {

    private JLabel lblAarsak;
    private JLabel lblProsess;
    private JLabel lblObjektType;
    private JLabel lblObjektAvvikType;
    private JLabel lblObjektAvvikKategori;
    private JLabel lblObjektStedTypeAngivelse;
    private JLabel lblArbeidType;
    private JLabel lblMerknad;
    private JLabel lblPlanlagtUtbedret;
    private JLabel lblUtbedringplan;
    private JLabel lblUtbedret;

    private JComboBox cmbAarsak;
    private JFormattedTextField fldProsess;
    private JComboBox cmbObjektType;
    private JComboBox cmbObjektAvvikType;
    private JComboBox cmbObjektAvvikKategori;
    private JComboBox cmbObjektStedTypeAngivelse;
    private JComboBox cmbArbeidType;
    private JTextField fldMerknad;
    private JMipssDatePicker jtpPlanlagtUtbedret;
    private JTextArea fldUtbedringplan;
    private JCheckBox checkUtbedret;
    private JPanel panelUtbedret;

    private JButton btnEndreProsess;
    private List<ObjektType> objektTyper;
    private List<ObjektAvvikType> objektAvvikTyper;
    private List<ObjektAvvikKategori> objektAvvikKategorier;
    private List<ObjektStedTypeAngivelse> objektStedTypeAngivelser;
    private List<ArbeidType> arbeidTyper;
    private List<Hendelseaarsak> aarsaker;

    private final RskjemaController controller;
    private BindingGroup bindings = new BindingGroup();
    private Hendelse hendelse;

    public VurderingPanel(RskjemaController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    private void initComponents() {
        lblAarsak = new JLabel(Resources.getResource("label.aarsak"));
        lblProsess = new JLabel(Resources.getResource("label.prosess"));
        lblObjektType = new JLabel(Resources.getResource("label.objektType"));
        lblObjektAvvikType = new JLabel(Resources.getResource("label.objektAvvikType"));
        lblObjektAvvikKategori = new JLabel(Resources.getResource("label.objektAvvikKategori"));
        lblObjektStedTypeAngivelse = new JLabel(Resources.getResource("label.objektStedTypeAngivelse"));
        lblArbeidType = new JLabel(Resources.getResource("label.arbeidType"));
        lblMerknad = new JLabel(Resources.getResource("label.merknad"));
        lblPlanlagtUtbedret = new JLabel(Resources.getResource("label.utbedring"));
        lblUtbedringplan = new JLabel(Resources.getResource("label.utbedringplan"));
        lblUtbedret = new JLabel(Resources.getResource("label.utbedret"));

        fldProsess = new JFormattedTextField();
        fldProsess.setEditable(false);

        fldMerknad = new JTextField(null, 50);
        fldMerknad.setEditable(true);

        cmbAarsak = new JComboBox();
        MipssListCellRenderer<Hendelseaarsak> aarsakRenderer = new MipssListCellRenderer<Hendelseaarsak>();
        cmbAarsak.setRenderer(aarsakRenderer);
        cmbObjektType = new JComboBox();
        MipssListCellRenderer<ObjektType> objektTypeRenderer = new MipssListCellRenderer<ObjektType>();
        cmbObjektType.setRenderer(objektTypeRenderer);

        cmbObjektAvvikType = new JComboBox();
        MipssListCellRenderer<ObjektAvvikType> objektAvvikTypeRenderer = new MipssListCellRenderer<ObjektAvvikType>();
        cmbObjektAvvikType.setRenderer(objektAvvikTypeRenderer);

        cmbObjektAvvikKategori = new JComboBox();
        MipssListCellRenderer<ObjektAvvikKategori> objektAvvikKategoriRenderer = new MipssListCellRenderer<ObjektAvvikKategori>();
        cmbObjektAvvikKategori.setRenderer(objektAvvikKategoriRenderer);

        cmbObjektStedTypeAngivelse = new JComboBox();
        MipssListCellRenderer<ObjektStedTypeAngivelse> objektStedTypeAngivelseRenderer = new MipssListCellRenderer<ObjektStedTypeAngivelse>();
        cmbObjektStedTypeAngivelse.setRenderer(objektStedTypeAngivelseRenderer);

        cmbArbeidType = new JComboBox();
        MipssListCellRenderer<ArbeidType> arbeidTypeRenderer = new MipssListCellRenderer<ArbeidType>();
        cmbArbeidType.setRenderer(arbeidTypeRenderer);

        btnEndreProsess = new JButton(controller.getEndreProsessAction(this));

        jtpPlanlagtUtbedret = new JMipssDatePicker();

        fldUtbedringplan = new JTextArea();
        fldUtbedringplan.setWrapStyleWord(true);
        fldUtbedringplan.setLineWrap(true);
        AbstractDocument doc = (AbstractDocument) fldUtbedringplan.getDocument();
        doc.setDocumentFilter(new DocumentSizeFilter(1000));

        checkUtbedret = new JCheckBox();
        panelUtbedret = new JPanel();

    }

    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
        arbeidTyper = controller.getArbeidTyper(false);
        aarsaker = controller.getHendelseaarsaker(hendelse);
        if (hendelse.getProsess() != null) {
            Long kontraktId = hendelse.getSak().getKontrakt().getId();
            Long prosessId = hendelse.getProsess().getId();
            objektTyper = controller.getObjektTyper(false, prosessId, kontraktId);
            objektAvvikTyper = controller.getObjektAvvikTyper(false, prosessId, kontraktId);
            objektAvvikKategorier = controller.getObjektAvvikKategorier(false, prosessId, kontraktId);
            objektStedTypeAngivelser = controller.getObjektStedTypeAngivelser(false, prosessId, kontraktId);
        } else {
            objektTyper = new ArrayList<ObjektType>();
            objektAvvikTyper = new ArrayList<ObjektAvvikType>();
            objektAvvikKategorier = new ArrayList<ObjektAvvikKategori>();
            objektStedTypeAngivelser = new ArrayList<ObjektStedTypeAngivelse>();
        }
        bind();
    }

    private void bind() {
        Binding<Hendelse, Object, JFormattedTextField, Object> prosessBind = BindingHelper.createbinding(hendelse, "prosessString", fldProsess, "text");

        JComboBoxBinding<?, List<Hendelseaarsak>, JComboBox> comboAarsakBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, aarsaker, cmbAarsak);
        Binding<Hendelse, Object, JComboBox, Object> aarsakBind = BindingHelper.createbinding(hendelse, "aarsak", cmbAarsak, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektType>, JComboBox> comboObjektTypeBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektTyper, cmbObjektType);
        Binding<Hendelse, Object, JComboBox, Object> objektTypeBind = BindingHelper.createbinding(hendelse, "objektType", cmbObjektType, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektAvvikType>, JComboBox> comboObjektAvvikTypeBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektAvvikTyper, cmbObjektAvvikType);
        Binding<Hendelse, Object, JComboBox, Object> objektAvvikTypeBind = BindingHelper.createbinding(hendelse, "objektAvvikType", cmbObjektAvvikType, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektAvvikKategori>, JComboBox> comboObjektAvvikKategoriBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektAvvikKategorier, cmbObjektAvvikKategori);
        Binding<Hendelse, Object, JComboBox, Object> objektAvvikKategoriBind = BindingHelper.createbinding(hendelse, "objektAvvikKategori", cmbObjektAvvikKategori, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektStedTypeAngivelse>, JComboBox> comboObjektStedTypeAngivelseBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektStedTypeAngivelser, cmbObjektStedTypeAngivelse);
        Binding<Hendelse, Object, JComboBox, Object> objektStedTypeAngivelseBind = BindingHelper.createbinding(hendelse, "objektStedTypeAngivelse", cmbObjektStedTypeAngivelse, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ArbeidType>, JComboBox> comboArbeidTypeBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, arbeidTyper, cmbArbeidType);
        Binding<Hendelse, Object, JComboBox, Object> arbeidTypeBind = BindingHelper.createbinding(hendelse, "arbeidType", cmbArbeidType, "selectedItem", UpdateStrategy.READ_WRITE);

        Binding<Hendelse, Object, JTextField, Object> merknadBind = BindingHelper.createbinding(hendelse, "merknad", fldMerknad, "text", UpdateStrategy.READ_WRITE);

        Binding<Hendelse, Object, JMipssDatePicker, Object> dateBind = BindingHelper.createbinding(hendelse, "planlagtUtbedret", jtpPlanlagtUtbedret, "date", UpdateStrategy.READ_WRITE);

        Binding<Hendelse, Object, JTextArea, Object> utbedringPlanBind = BindingHelper.createbinding(hendelse, "utbedringplan", fldUtbedringplan, "text", UpdateStrategy.READ_WRITE);

        Binding<Hendelse, Object, JCheckBox, Object> utbedretBind = BindingHelper.createbinding(hendelse, "utbedret", checkUtbedret, "selected", UpdateStrategy.READ_WRITE);

        bindings.addBinding(prosessBind);
        bindings.addBinding(comboAarsakBind);
        bindings.addBinding(aarsakBind);
        bindings.addBinding(comboObjektTypeBind);
        bindings.addBinding(objektTypeBind);
        bindings.addBinding(comboObjektAvvikTypeBind);
        bindings.addBinding(objektAvvikTypeBind);
        bindings.addBinding(comboObjektAvvikKategoriBind);
        bindings.addBinding(objektAvvikKategoriBind);
        bindings.addBinding(comboObjektStedTypeAngivelseBind);
        bindings.addBinding(objektStedTypeAngivelseBind);
        bindings.addBinding(merknadBind);
        bindings.addBinding(dateBind);
        bindings.addBinding(utbedringPlanBind);
        bindings.addBinding(utbedretBind);
        bindings.addBinding(comboArbeidTypeBind);
        bindings.addBinding(arbeidTypeBind);

        bindings.bind();
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.vurdering")));

        add(lblAarsak, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbAarsak, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblProsess, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(fldProsess, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(btnEndreProsess, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 1), 0, 0));

        add(lblObjektType, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektType, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblObjektAvvikType, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektAvvikType, new GridBagConstraints(1, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblObjektAvvikKategori, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektAvvikKategori, new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblObjektStedTypeAngivelse, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektStedTypeAngivelse, new GridBagConstraints(1, 5, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblMerknad, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(fldMerknad, new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

        add(lblArbeidType, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbArbeidType, new GridBagConstraints(1, 7, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblPlanlagtUtbedret, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(jtpPlanlagtUtbedret, new GridBagConstraints(1, 8, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

        add(lblUtbedringplan, new GridBagConstraints(0, 9, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(new JScrollPane(fldUtbedringplan), new GridBagConstraints(0, 10, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 0), 0, 0));

        panelUtbedret.add(checkUtbedret);
        panelUtbedret.add(lblUtbedret);
        add(panelUtbedret, new GridBagConstraints(0, 11, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        // Do nothing
    }

    @Override
    public Hendelse getProcessRelatedObject() {
        return hendelse;
    }

    @Override
    public void setProcessRelatedObject(Hendelse hendelse) {
        this.hendelse = hendelse;
    }

    @Override
    public void refreshProcess(Prosess prosess) {
        Long kontraktId = hendelse.getSak().getKontrakt().getId();
        Long prosessId = prosess.getId();

        hendelse.setObjektType(null);
        hendelse.setObjektAvvikType(null);
        hendelse.setObjektAvvikKategori(null);
        hendelse.setObjektStedTypeAngivelse(null);

        updateList(objektTyper, cmbObjektType, controller.getObjektTyper(true, prosessId, kontraktId));
        updateList(objektAvvikTyper, cmbObjektAvvikType, controller.getObjektAvvikTyper(true, prosessId, kontraktId));
        updateList(objektAvvikKategorier, cmbObjektAvvikKategori, controller.getObjektAvvikKategorier(true, prosessId, kontraktId));
        updateList(objektStedTypeAngivelser, cmbObjektStedTypeAngivelse, controller.getObjektStedTypeAngivelser(true, prosessId, kontraktId));
    }

    private <T> void updateList(List<T> localList, JComboBox comboBox, List<T> newContent) {
        localList.clear();
        localList.addAll(newContent);
        if(localList.size()>0) {
            comboBox.setSelectedIndex(0);
        }
    }
}
