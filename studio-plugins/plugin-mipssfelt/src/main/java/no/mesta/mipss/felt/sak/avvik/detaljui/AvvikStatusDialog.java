package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.resources.images.IconResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AvvikStatusDialog extends JDialog {
    private static final Logger logger = LoggerFactory.getLogger(AvvikStatusDialog.class);
    private final Avvik avvik;
    private AvvikStatusPanel panel;
    private JButton cancel;
    private JButton add;
    private final FeltController controller;

    public AvvikStatusDialog(JDialog owner, final Avvik avvik, FeltController controller) {
        super(owner, Resources.getResource("label.status"), true);
        this.controller = controller;
        setIconImage(IconResources.EDIT_ICON.getImage());
        this.avvik = avvik;

        initComponents();
        initGui();
        bind();
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        JPanel buttonPanel = new JPanel(new GridBagLayout());
        buttonPanel.add(cancel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        buttonPanel.add(add, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

        add(panel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
        add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    }

    private void initComponents() {
        panel = new AvvikStatusPanel(controller, avvik);
        cancel = new JButton(new CancelAction(IconResources.CANCEL_ICON, Resources.getResource("button.avbryt")));
        add = new JButton(new AddAction(IconResources.ADD_ITEM_ICON, Resources.getResource("button.leggTil")));
    }

    private void bind() {
        BindingHelper.createbinding(panel, "${status.punktregstatusType != null}", add, "enabled").bind();
    }


    /**
     * Viser dialogen
     *
     * @param center
     */
    public void showRelativeTo(Component center) {
        pack();
        setLocationRelativeTo(center);
        setVisible(true);
    }

    /**
     * Legger til statusen på funnet
     *
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    private class AddAction extends AbstractAction {

        public AddAction(Icon icon, String text) {
            super(text, icon);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            logger.debug("actionPerformed() status: " + panel.getStatus());
            AvvikStatus status = panel.getStatus();
            if (status.getAvvikstatusType() != null) {
                avvik.addStatus(panel.getStatus());
                setVisible(false);
            }
        }
    }

    /**
     * Avbryter dialogen
     *
     * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
     */
    private class CancelAction extends AbstractAction {

        public CancelAction(Icon icon, String text) {
            super(text, icon);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
        }
    }
}
