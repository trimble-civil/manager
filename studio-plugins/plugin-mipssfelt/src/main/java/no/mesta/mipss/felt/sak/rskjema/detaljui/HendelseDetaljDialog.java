package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.persistence.mipssfield.Hendelse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

@SuppressWarnings("serial")
@Deprecated//skal ikke benyttes alene, saken skal alltid vises
public class HendelseDetaljDialog extends JDialog {

    private final RskjemaController controller;

    private HendelseDetaljPanel pnlDetalj;

    private final Hendelse hendelse;

    public HendelseDetaljDialog(RskjemaController controller, Hendelse hendelse) {
        super(controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
        this.controller = controller;
        this.hendelse = hendelse;
        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlDetalj = new HendelseDetaljPanel(controller, hendelse);
        setTitle("R2 id:" + hendelse.getRefnummer());
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
//				pnlDetalj.getBindingGroup().unbind();
                pnlDetalj.dispose();
            }
        });
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(pnlDetalj, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 5, 5, 5), 0, 0));
        setSize(840, 590);
    }
}
