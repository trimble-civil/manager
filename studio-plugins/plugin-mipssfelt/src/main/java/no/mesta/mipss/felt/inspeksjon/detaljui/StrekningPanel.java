package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import org.jdesktop.swingx.decorator.SortOrder;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;

@SuppressWarnings("serial")
public class StrekningPanel extends JPanel implements LoadingPanel {

    private MipssBeanTableModel<Prodstrekning> tableModel;
    private JMipssBeanTable<Prodstrekning> table;

    public StrekningPanel() {
        initComponents();
        initGui();
    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        tableModel.setSourceObject(inspeksjon);
    }

    public void initTable() {
        tableModel = new MipssBeanTableModel<Prodstrekning>("strekninger",
                Prodstrekning.class, "fylkesnummer", "kommunenummer", "vei", "hp", "fraKm", "tilKm");

        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Prodstrekning.class,
                tableModel.getColumns());
        table = new JMipssBeanTable<Prodstrekning>(tableModel, columnModel);
        table.setDefaultRenderer(Double.class, new DoubleRenderer());
        table.setFillsViewportHeight(true);
        table.setFillsViewportWidth(true);
        table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        table.setSortable(true);
        table.setSortOrder(0, SortOrder.DESCENDING);
        table.setHorizontalScrollEnabled(true);
        table.setEditable(false);
//		parent.getFieldToolkit().controlEnabledField(table);

        setColumnWidth(table.getColumn(0), 50);
        setColumnWidth(table.getColumn(1), 50);
        setColumnWidth(table.getColumn(2), 50);
        setColumnWidth(table.getColumn(3), 50);
    }

    private void setColumnWidth(TableColumn column, int width) {
        column.setPreferredWidth(width);
        column.setMinWidth(width);
    }

    private void initComponents() {
        initTable();

    }

    private void initGui() {
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.strekninger")));
        setLayout(new BorderLayout());
        JScrollPane scroll = new JScrollPane(table);
        add(scroll, BorderLayout.CENTER);
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {

    }

}
