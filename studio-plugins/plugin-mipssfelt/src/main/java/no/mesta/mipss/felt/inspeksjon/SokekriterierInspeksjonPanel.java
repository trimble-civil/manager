package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.util.StringConcatHelper;
import no.mesta.mipss.mipssfelt.InspeksjonQueryParams;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class SokekriterierInspeksjonPanel extends JPanel {
    private final InspeksjonController controller;

    private JCheckBox chkKunEtterslep;
    private JCheckBox chkKunSlettede;

    private JButton btnProsess;
    private JLabel lblValgeProsesser;
    private JLabel lblProsesserValue;

    public SokekriterierInspeksjonPanel(InspeksjonController controller) {
        this.controller = controller;

        initComponents();
        initGui();
    }

    private void initComponents() {
        chkKunEtterslep = new JCheckBox(Resources.getResource("check.etterslep"));
        chkKunEtterslep.setToolTipText(Resources.getResource("check.etterslep.tooltip"));

        chkKunSlettede = new JCheckBox(Resources.getResource("check.kunSlettede"));
        chkKunSlettede.setToolTipText(Resources.getResource("check.kunSlettede.tooltip"));

        btnProsess = new JButton(controller.getFeltController().getVelgProsessAction(controller.getQueryParams()));
        lblValgeProsesser = new JLabel(Resources.getResource("label.valgteProsesser"));
        lblProsesserValue = new JLabel(Resources.getResource("label.valgteProsesser.default"));
        Font fontProsess = lblProsesserValue.getFont();
        fontProsess = new Font(fontProsess.getName(), Font.BOLD, fontProsess.getSize());
        lblProsesserValue.setFont(fontProsess);
        bind();
    }


    private void bind() {
        Binding<InspeksjonQueryParams, Object, SokekriterierInspeksjonPanel, Object> prosessBind = BindingHelper.createbinding(controller.getQueryParams(), "prosesskoder", this, "valgteProsesskoder");
        Binding<JCheckBox, Object, InspeksjonQueryParams, Object> kunSlettetBind = BindingHelper.createbinding(chkKunSlettede, "selected", controller.getQueryParams(), "kunSlettede");
        Binding<JCheckBox, Object, InspeksjonQueryParams, Object> kunEtterslepBind = BindingHelper.createbinding(chkKunEtterslep, "selected", controller.getQueryParams(), "kunEtterslep");

        controller.getFeltController().getBindingGroup().addBinding(prosessBind);
        controller.getFeltController().getBindingGroup().addBinding(kunSlettetBind);
        controller.getFeltController().getBindingGroup().addBinding(kunEtterslepBind);

    }

    public void setValgteProsesskoder(String[] prosesskoder) {
        if (prosesskoder == null || prosesskoder.length == 0) {
            lblProsesserValue.setText(Resources.getResource("label.valgteProsesser.default"));
            return;
        }
        String prosesskoderString = StringUtils.join(prosesskoder, ", ");
        lblProsesserValue.setText(prosesskoderString);
        lblProsesserValue.setToolTipText(StringConcatHelper.createTooltip(prosesskoder));

    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(chkKunEtterslep, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(chkKunSlettede, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));


        add(btnProsess, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
        add(lblValgeProsesser, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 15, 0, 0), 0, 0));
        add(lblProsesserValue, new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    }
}