package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.inspeksjon.detaljui.InspeksjonDetaljPanel;
import no.mesta.mipss.mipssfelt.*;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.*;
import no.mesta.mipss.util.EpostUtils;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.Highlighter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;

public class InspeksjonController {

    private final FeltController controller;
    private final InspeksjonButtonController buttonController;

    private MipssBeanTableModel<InspeksjonSokResult> mdlInspeksjon;
    private JMipssBeanTable<InspeksjonSokResult> tblInspeksjon;
    private InspeksjonQueryParams queryParams;
    private InspeksjonService inspeksjonService;
    private MipssFelt feltService;


    public InspeksjonController(FeltController controller) {
        this.controller = controller;
        buttonController = new InspeksjonButtonController(this);
        inspeksjonService = BeanUtil.lookup(InspeksjonService.BEAN_NAME, InspeksjonService.class);
        feltService = BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class);
        queryParams = new InspeksjonQueryParams();
        bindQueryParams(controller.getCommonQueryParams());
        initInspeksjonTable();

    }

    private void bindQueryParams(QueryParams commonQueryParams) {
        BindingHelper.createbinding(commonQueryParams, "dateFra", queryParams, "dateFra").bind();
        BindingHelper.createbinding(commonQueryParams, "dateTil", queryParams, "dateTil").bind();
        BindingHelper.createbinding(commonQueryParams, "brukerList", queryParams, "brukerList").bind();
        BindingHelper.createbinding(commonQueryParams, "pdaDfuIdentList", queryParams, "pdaDfuIdentList").bind();
        BindingHelper.createbinding(commonQueryParams, "valgtKontrakt", queryParams, "valgtKontrakt").bind();
        BindingHelper.createbinding(commonQueryParams, "veiListe", queryParams, "veiListe").bind();
        BindingHelper.createbinding(commonQueryParams, "prosesser", queryParams, "prosesser").bind();
    }

    public MipssBeanTableModel<InspeksjonSokResult> getInspeksjonTableModel() {
        return mdlInspeksjon;
    }

    public JMipssBeanTable<InspeksjonSokResult> getInspeksjonTable() {
        return tblInspeksjon;
    }

    private void initInspeksjonTable() {
        mdlInspeksjon = new MipssBeanTableModel<InspeksjonSokResult>(InspeksjonSokResult.class, "refnummer", "fraTidspunkt", "tilTidspunkt",
                "etterslep", "antallSaker", "hpInspText", "prosessInspText", "vaerNavn", "vindNavn", "temperaturNavn",
                "opprettetAv", "kmEvInspisert", "kmRvInspisert", "kmFvInspisert", "kmKvInspisert", "pdaDfuNavn",
                "beskrivelse");
        MipssRenderableEntityTableColumnModel inspectionColumnModel = new MipssRenderableEntityTableColumnModel(
                InspeksjonSokResult.class, mdlInspeksjon.getColumns());

        mdlInspeksjon.setBeanInstance(inspeksjonService);
        mdlInspeksjon.setBeanMethod("sokInspeksjoner");
        mdlInspeksjon.setBeanInterface(InspeksjonService.class);
        mdlInspeksjon.setBeanMethodArguments(new Class<?>[]{InspeksjonQueryParams.class});

        tblInspeksjon = new JMipssBeanTable<InspeksjonSokResult>(mdlInspeksjon, inspectionColumnModel);
        tblInspeksjon.setDefaultRenderer(Date.class, new DateTimeRenderer());
        tblInspeksjon.setDefaultRenderer(Boolean.class, new BooleanRenderer());
        tblInspeksjon.setDefaultRenderer(Double.class, new DoubleRenderer());
        tblInspeksjon.setFillsViewportHeight(true);
        tblInspeksjon.setFillsViewportWidth(true);
        tblInspeksjon.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        tblInspeksjon.setColumnControlVisible(true);
        tblInspeksjon.addColumnStateSupport("inspeksjonTable", controller.getPlugin());
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(0), 60);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(1), 90);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(2), 90);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(3), 30);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(4), 30);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(5), 117);

        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(7), 65);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(8), 65);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(9), 65);

        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(11), 45);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(12), 45);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(13), 45);
        TableHelper.setColumnWidth(inspectionColumnModel.getColumn(14), 45);

        tblInspeksjon.setHighlighters(
                new Highlighter[]{
                        new ColorHighlighter(
                                new InspeksjonHighlightPredicate(mdlInspeksjon, tblInspeksjon),
                                TableHelper.getCellBackground(),
                                Color.RED,
                                TableHelper.getSelectedBackground(),
                                TableHelper.getSelectedForeground())
                }
        );
        tblInspeksjon.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    openInspeksjon(mdlInspeksjon.get(tblInspeksjon.convertRowIndexToModel(tblInspeksjon.getSelectedRow())));
                }
            }
        });
        tblInspeksjon.loadUserSettings();
    }

    /**
     * Henter id til kontrakten som tilhører inspeksjonen med angitt refnummer
     *
     * @param refnummer
     * @return
     */
    public Long getDriftkontraktIdForInspeksjon(Long refnummer) {
        return feltService.getDriftkontraktIdForInspeksjon(refnummer);
    }

    public FeltController getFeltController() {
        return controller;
    }

    public Inspeksjon hentInspeksjon(String guid) {
        return inspeksjonService.hentInspeksjon(guid);
    }

    public Inspeksjon hentInspeksjon(Long refnummer) {
        return inspeksjonService.hentInspeksjon(refnummer);
    }

    public void openInspeksjon(InspeksjonSokResult result) {
        InspeksjonDetaljDialog dialog = new InspeksjonDetaljDialog(this, result);
        dialog.setLocationRelativeTo(getFeltController().getPlugin().getLoader().getApplicationFrame());
        dialog.setVisible(true);
    }

    public void openInspeksjon(final Inspeksjon inspeksjon) {
        final JDialog waitDialog = controller.showWaitDialog(Resources.getResource("label.henterInspeksjon"));
        new SwingWorker<Void, Void>() {
            private Inspeksjon i;

            public Void doInBackground() {
                i = hentInspeksjon(inspeksjon.getGuid());
                return null;
            }

            @Override
            public void done() {
                waitDialog.dispose();
                if (i != null) {
                    InspeksjonDetaljDialog dialog = new InspeksjonDetaljDialog(InspeksjonController.this, createResultFromInspeksjon(i), i);
                    dialog.setLocationRelativeTo(getFeltController().getPlugin().getLoader().getApplicationFrame());
                    dialog.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(controller.getPlugin().getLoader().getApplicationFrame(), Resources.getResource("warning.hentInspeksjonFeil", inspeksjon.getRefnummer() + "", EpostUtils.getSupportEpostAdresse()), Resources.getResource("warning.hentInspeksjonFeil.title"), JOptionPane.WARNING_MESSAGE);
                }

            }
        }.execute();


    }

    /**
     * Oppretter et InspeksjonSokResult objekt basert på Inspeksjon
     * Setter de parametere som er nødvendig for å gjenbruke eksisterende kode
     *
     * @param i
     * @return
     */
    public InspeksjonSokResult createResultFromInspeksjon(Inspeksjon i) {
        InspeksjonSokResult r = new InspeksjonSokResult();
        r.setKmEvInspisert(getKmInspisert(i, "E"));
        r.setKmFvInspisert(getKmInspisert(i, "F"));
        r.setKmKvInspisert(getKmInspisert(i, "K"));
        r.setKmRvInspisert(getKmInspisert(i, "R"));
        return r;
    }

    private Double getKmInspisert(Inspeksjon i, String veikategori) {
        List<Prodstrekning> st = i.getStrekninger();
        double temp = 0;
        for (Prodstrekning s : st) {
            if (veikategori.equals(s.getVeikategori()) && !(s.getFraKm() == 0 && s.getTilKm() == 99999)) {
                temp += Math.abs(s.getTilKm() - s.getFraKm());
            }
        }
        return Double.valueOf(temp);
    }

    private void reloadTable() {
        final int selectedRow = getInspeksjonTable().getSelectedRow();
        getInspeksjonTableModel().loadData();
        getInspeksjonTableModel().addTableLoaderListener(new MipssBeanLoaderListener() {
            @Override
            public void loadingStarted(MipssBeanLoaderEvent event) {
            }

            @Override
            public void loadingFinished(MipssBeanLoaderEvent event) {
                getInspeksjonTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
            }
        });
    }

    public Inspeksjon persistInspeksjon(Inspeksjon in) {
        try {
            inspeksjonService.persistInspeksjon(in);
            reloadTable();
            return in;
        } catch (Exception ex) {
            ErrorHandler.showError(ex, Resources.getResource("inspeksjon.feilVedLagring", EpostUtils.getSupportEpostAdresse()));
            ex.printStackTrace();
        }
        return null;

    }

    /***********************************
     * Delegatorer for knappeactions
     ***********************************/

    public Action getInspeksjonSokButtonAction() {
        return buttonController.getInspeksjonSokButtonAction(this);
    }

    public Action getInspeksjonHentMedIdAction() {
        return buttonController.getInspeksjonHentMedIdAction();
    }

    public Action getInspeksjonSlettAction(InspeksjonDetaljPanel inspeksjonDetaljPanel, Action callback) {
        return buttonController.getInspeksjonSlettAction(inspeksjonDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getInspeksjonSlettAction() {
        return buttonController.getInspeksjonSlettAction();
    }


    public Action getInspeksjonExcelAction() {
        return buttonController.getInspeksjonExcelAction();
    }

    public Action getInspeksjonRapportAction() {
        return buttonController.getInspeksjonRapportAction();
    }

    public Action getInspeksjonDetaljerAction() {
        return buttonController.getInspeksjonDetaljerAction();
    }

    public Action getInspeksjonKartAction() {
        return buttonController.getInspeksjonKartAction();
    }

    public InspeksjonQueryParams getQueryParams() {
        return queryParams;
    }

    public Action getInspeksjonLagreAction(InspeksjonDetaljPanel inspeksjonDetaljPanel) {
        return buttonController.getInspeksjonLagreAction(inspeksjonDetaljPanel);
    }


}
