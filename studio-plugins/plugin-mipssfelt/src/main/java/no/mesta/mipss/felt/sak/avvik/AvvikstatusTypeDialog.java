package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.ui.table.AvvikstatusTableObject;

import javax.swing.*;
import java.util.List;

@SuppressWarnings("serial")
public class AvvikstatusTypeDialog extends TableObjectDialog<AvvikstatusTableObject, AvvikstatusType> {

    public AvvikstatusTypeDialog(JFrame owner, List<AvvikstatusType> alleStatuser) {
        super(owner, alleStatuser);
    }

    @Override
    protected TableObjectPanel<AvvikstatusTableObject, AvvikstatusType> getPanel() {
        return new AvvikstatusTypePanel(alleStatuser);
    }

    public void setSelected(List<String> status) {
        ((AvvikstatusTypePanel) pnlStatus).setSelected(status);
    }

    public List<String> getSelectedIds() {
        return ((AvvikstatusTypePanel) pnlStatus).getSelected();
    }


//	private final List<AvvikstatusType> alleStatuser;
//	private AvvikstatusTypePanel pnlStatus;
//	private JButton btnOk;
//	private JButton btnCancel;
//	private boolean cancel=true;
//	
//	public AvvikstatusTypeDialog(JFrame owner, List<AvvikstatusType> alleStatuser){
//		super(owner,Resources.getResource("dialog.title.avvikstatus"), true);
//		this.alleStatuser = alleStatuser;
//		initComponents();
//		initGui();
//	}
//
//	private void initComponents() {
//		pnlStatus = new AvvikstatusTypePanel(alleStatuser);
//		btnOk = new JButton(getOkAction());
//		btnCancel = new JButton(getAvbrytAction());
//		
//	}
//	public void setSelected(List<String> status){
//		pnlStatus.setSelected(status);
//	}
//	public List<String> getSelectedIds(){
//		return pnlStatus.getSelected();
//	}
//	public boolean isCancelled(){
//		return cancel;
//	}
//	private Action getOkAction(){
//		return new AbstractAction(Resources.getResource("button.ok")){
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				cancel = false;
//				dispose();
//			}
//		};
//	}
//	private Action getAvbrytAction(){
//		return new AbstractAction(Resources.getResource("button.avbryt")){
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				cancel = true;
//				dispose();
//			}
//		};
//	}
//	
//	private void initGui() {
//		setLayout(new BorderLayout());
//		add(pnlStatus);
//		JCheckBox chkAlle = new JCheckBox(Resources.getResource("label.velgAlle"));
//		chkAlle.addActionListener(new ActionListener(){
//			public void actionPerformed(ActionEvent e){
//				JCheckBox box = (JCheckBox)e.getSource();
//				if (box.isSelected()){
//					pnlStatus.setAlleValgt(true);
//				}else{
//					pnlStatus.setAlleValgt(false);
//				}
//			}
//		});
//		JPanel pnlButton = new JPanel();
//		pnlButton.add(btnCancel);
//		pnlButton.add(btnOk);
//		JPanel pnlB = new JPanel(new BorderLayout());
//		pnlB.add(pnlButton, BorderLayout.EAST);
//		
//		JPanel pnlW = new JPanel(new BorderLayout());
//		pnlW.add(chkAlle, BorderLayout.WEST);
//		pnlB.add(pnlW, BorderLayout.WEST);
//		add(pnlB, BorderLayout.SOUTH);
//	}
}
