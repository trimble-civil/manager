package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseTrafikktiltak;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings({"serial", "unchecked"})
public class TiltakPanel extends JPanel implements LoadingPanel {
    private final RskjemaController controller;
    private JMipssBeanTable<HendelseTrafikktiltak> trafikktiltakTable;
    private JMipssBeanScrollPane scrTable;
    private JButton btnSlett;
    private JButton btnLeggTil;
    private Hendelse hendelse;

    public TiltakPanel(RskjemaController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    private void initComponents() {
        trafikktiltakTable = controller.createTrafikktiltakTable();
        trafikktiltakTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    trafikktiltakTable.selectRow(e);
                    controller.endreValgteTrafikktiltak(TiltakPanel.this, trafikktiltakTable);
                }
            }
        });
        btnSlett = new JButton(controller.getSlettTrafikktiltak(trafikktiltakTable, this));
        btnLeggTil = new JButton(controller.getLeggTilTrafikktiltak(trafikktiltakTable, this));
        scrTable = new JMipssBeanScrollPane(trafikktiltakTable, (MipssBeanTableModel<AvvikStatus>) trafikktiltakTable.getModel());
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.tiltak")));

        JPanel pnlButton = new JPanel(new GridBagLayout());
        pnlButton.add(btnSlett, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnLeggTil, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(scrTable, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 3, 3), 0, 0));
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
    }

    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
        MipssBeanTableModel<HendelseTrafikktiltak> model = (MipssBeanTableModel<HendelseTrafikktiltak>) trafikktiltakTable.getModel();
        model.setSourceObject(hendelse);
    }

    public Hendelse getHendelse() {
        return hendelse;
    }
}
