package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DateStringConverter;
import no.mesta.mipss.core.StringNullConverter;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings({"serial", "unchecked", "rawtypes"})
public class TidStedPanel extends JPanel implements LoadingPanel {
    private JLabel lblId;
    private JLabel lblRegdato;
    private JLabel lblAv;
    private JLabel lblPda;
    private JLabel lblElrappEmne;
    private JLabel lblElrappId;
    private JLabel lblElrappVersjon;
    private JLabel lblElrappDato;

    private JFormattedTextField fldId;
    private JFormattedTextField fldRegdato;
    private JFormattedTextField fldAv;
    private JFormattedTextField fldPda;

    private JFormattedTextField fldElrappEmne;
    private JFormattedTextField fldElrappId;
    private JFormattedTextField fldElrappVersjon;
    private JFormattedTextField fldElrappDato;

    private BindingGroup bindings = new BindingGroup();
    private Hendelse hendelse;

    public TidStedPanel() {
        initComponents();
        initGui();

    }

    private void initComponents() {
        lblId = new JLabel(Resources.getResource("label.id"));
        lblRegdato = new JLabel(Resources.getResource("label.regdato"));
        lblAv = new JLabel(Resources.getResource("label.av"));
        lblPda = new JLabel(Resources.getResource("label.pda"));
        lblElrappEmne = new JLabel(Resources.getResource("label.elrappEmne"));
        lblElrappId = new JLabel(Resources.getResource("label.elrappId"));
        lblElrappVersjon = new JLabel(Resources.getResource("label.elrappVersjon"));
        lblElrappDato = new JLabel(Resources.getResource("label.elrappRapportertDato"));

        fldId = new JFormattedTextField();
        fldRegdato = new JFormattedTextField();
        fldAv = new JFormattedTextField();
        fldPda = new JFormattedTextField();
        fldElrappEmne = new JFormattedTextField();
        fldElrappId = new JFormattedTextField();
        fldElrappVersjon = new JFormattedTextField();
        fldElrappDato = new JFormattedTextField();

        fldId.setEditable(false);
        fldRegdato.setEditable(false);
        fldAv.setEditable(false);
        fldPda.setEditable(false);
        fldElrappDato.setEditable(false);
        fldElrappId.setEditable(false);
        fldElrappVersjon.setEditable(false);
    }

    private void bind() {
        Binding regdatoBind = BindingHelper.createbinding(hendelse.getOwnedMipssEntity(), "opprettetDato", fldRegdato, "text");
        Binding<Hendelse, Object, JFormattedTextField, Object> idBind = BindingHelper.createbinding(hendelse, "refnummer", fldId, "text");
        Binding<OwnedMipssEntity, Object, JFormattedTextField, Object> regAvBind = BindingHelper.createbinding(hendelse.getOwnedMipssEntity(), "opprettetAv", fldAv, "text");
        Binding<Hendelse, Object, JFormattedTextField, Object> pdaBind = BindingHelper.createbinding(hendelse, "${sak.pdaDfuIdent}", fldPda, "text");
        regdatoBind.setConverter(new DateStringConverter(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

        Binding<Hendelse, String, JFormattedTextField, String> emneBind = BindingHelper.createbinding(hendelse, "skjemaEmne", fldElrappEmne, "text", UpdateStrategy.READ_WRITE);
        Binding<Hendelse, Object, JFormattedTextField, Object> elrappIdBind = BindingHelper.createbinding(hendelse, "elrappDokumentIdent", fldElrappId, "text");
        Binding<Hendelse, Object, JFormattedTextField, Object> elrappVersjonBind = BindingHelper.createbinding(hendelse, "elrappVersjon", fldElrappVersjon, "text");
        Binding rapportertDatoBind = BindingHelper.createbinding(hendelse, "rapportertDato", fldElrappDato, "text");
        rapportertDatoBind.setConverter(new DateStringConverter(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));

        emneBind.setConverter(new StringNullConverter());

        bindings.addBinding(idBind);
        bindings.addBinding(regdatoBind);
        bindings.addBinding(regAvBind);
        bindings.addBinding(pdaBind);
        bindings.addBinding(emneBind);
        bindings.addBinding(elrappIdBind);
        bindings.addBinding(elrappVersjonBind);
        bindings.addBinding(rapportertDatoBind);
        bindings.bind();
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.tidSted")));
        add(lblId, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldId, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

        add(lblRegdato, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldRegdato, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblAv, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        add(fldAv, new GridBagConstraints(3, 1, 1, 1, 0.2, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblPda, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        add(fldPda, new GridBagConstraints(5, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));

        add(lblElrappEmne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldElrappEmne, new GridBagConstraints(1, 2, 5, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));

        add(lblElrappId, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldElrappId, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblElrappVersjon, new GridBagConstraints(2, 3, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        add(fldElrappVersjon, new GridBagConstraints(4, 3, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));

        add(lblElrappDato, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldElrappDato, new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();

    }

    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
        bind();
    }
}
