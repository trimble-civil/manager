package no.mesta.mipss.felt;

import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.process.JProcessPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Set;

@SuppressWarnings("serial")
public class ProsessDialog extends JDialog {

    private JProcessPanel pnlProsess;
    private Driftkontrakt kontrakt;
    private boolean cancelled = true;

    public ProsessDialog(QueryParams params, JFrame owner, String title) {
        super(owner, title, true);
        this.kontrakt = params.getValgtKontrakt();
        initGui();
    }

    public ProsessDialog(QueryParams params, JDialog owner, String title) {
        super(owner, title, true);
        this.kontrakt = params.getValgtKontrakt();
        initGui();
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setSingleSelectionMode(boolean singleSelection) {
        pnlProsess.setSingleSelectionMode(singleSelection);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        pnlProsess = new JProcessPanel(kontrakt, "id", false);

        JButton nullstillButton = new JButton(getNullstillAction());
        JButton okButton = new JButton(getOkAction());
        JButton cancelButton = new JButton(getCancelAction());
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        buttonPanel.add(nullstillButton, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 5, 10), 0, 0));
        buttonPanel.add(okButton, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 10), 0, 0));
        buttonPanel.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));

        add(pnlProsess, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

    }

    public Long[] getValgteProsesser() {
        Set<Prosess> valgteProsesser = pnlProsess.getValgteProsesser();
        Long[] prosesser = new Long[valgteProsesser.size()];
        int i = 0;
        for (Prosess p : valgteProsesser) {
            prosesser[i] = p.getId();
            i++;
        }
        return prosesser;
    }

    public String[] getValgteProsesskoder() {
        Set<Prosess> valgteProsesser = pnlProsess.getValgteProsesser();
        String[] prosessKoder = new String[valgteProsesser.size()];
        int i = 0;
        for (Prosess p : valgteProsesser) {
            prosessKoder[i] = p.getProsessKode();
            i++;
        }
        return prosessKoder;
    }

    public Set<Prosess> getProsesser() {
        return pnlProsess.getValgteProsesser();
    }

    public void setSelectedProsesser(final Long[] prosessId) {
        new SwingWorker<Void, Void>() {
            public Void doInBackground() {
                while (!pnlProsess.isProsessLoaded()) {
                    Thread.yield();
                }
                if (prosessId == null || prosessId.length == 0) {
                    pnlProsess.selectAll(false);
                    return null;
                }
                for (Long p : prosessId) {
                    pnlProsess.setSelectedProsessId(p);
                }

                return null;
            }

            public void done() {
                pnlProsess.repaint();
            }
        }.execute();
    }

    private Action getOkAction() {
        return new AbstractAction("Ok", IconResources.OK2_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = false;
                ProsessDialog.this.dispose();
            }
        };
    }

    private Action getCancelAction() {
        return new AbstractAction("Avbryt", IconResources.CANCEL_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = true;
                ProsessDialog.this.dispose();
            }
        };
    }

    private Action getNullstillAction() {
        return new AbstractAction("Nullstill", IconResources.RESET_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedProsesser(new Long[]{});
            }
        };
    }

}
