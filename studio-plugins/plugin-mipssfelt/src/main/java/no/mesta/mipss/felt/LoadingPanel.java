package no.mesta.mipss.felt;

public interface LoadingPanel {
    /**
     * Returnerer true så lenge panelet lastes.
     *
     * @return
     */
    boolean isLoading();

    /**
     * Unbind komponentene og løs opp i evt. gjenliggende gui referanser
     */
    void dispose();
}
