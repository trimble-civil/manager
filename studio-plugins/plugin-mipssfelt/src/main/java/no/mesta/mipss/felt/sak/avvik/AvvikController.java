package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.sak.avvik.detaljui.*;
import no.mesta.mipss.felt.tableHighlighter.ColorHighlighterFactory;
import no.mesta.mipss.mipssfelt.*;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.*;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SortOrder;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

public class AvvikController {

    private final FeltController controller;
    private final AvvikButtonController buttonController;
    private AvvikQueryParams queryParams;
    private MipssBeanTableModel<AvvikSokResult> mdlAvvik;
    private JMipssBeanTable<AvvikSokResult> tblAvvik;
    private MipssBeanTableModel<AvvikSokResult> mdlMangler;
    private JMipssBeanTable<AvvikSokResult> tblMangler;
    private AvvikService avvikService;


    private List<AvvikstatusType> statusTyperCache;
    private List<Avviktilstand> avviktilstandCache;
    private List<ArbeidType> arbeidTypeCache;
    private MipssFelt feltBean;


    public AvvikController(FeltController controller) {
        this.controller = controller;
        buttonController = new AvvikButtonController(this);
        avvikService = BeanUtil.lookup(AvvikService.BEAN_NAME, AvvikService.class);
        feltBean = BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class);
        queryParams = new AvvikQueryParams();
        bindQueryParams(controller.getCommonQueryParams());
        initAvvikTable();
        initManglerTable();
    }

    private void bindQueryParams(QueryParams commonQueryParams) {
        BindingHelper.createbinding(commonQueryParams, "dateFra", queryParams, "dateFra").bind();
        BindingHelper.createbinding(commonQueryParams, "dateTil", queryParams, "dateTil").bind();
        BindingHelper.createbinding(commonQueryParams, "brukerList", queryParams, "brukerList").bind();
        BindingHelper.createbinding(commonQueryParams, "pdaDfuIdentList", queryParams, "pdaDfuIdentList").bind();
        BindingHelper.createbinding(commonQueryParams, "valgtKontrakt", queryParams, "valgtKontrakt").bind();
        BindingHelper.createbinding(commonQueryParams, "veiListe", queryParams, "veiListe").bind();
        BindingHelper.createbinding(commonQueryParams, "prosesser", queryParams, "prosesser").bind();

        BindingHelper.createbinding(queryParams, "dateEnabled", commonQueryParams, "dateEnabled").bind();
    }

    public MipssBeanTableModel<AvvikSokResult> getAvvikTableModel() {
        return mdlAvvik;
    }

    public JMipssBeanTable<AvvikSokResult> getAvvikTable() {
        return tblAvvik;
    }

    public MipssBeanTableModel<AvvikSokResult> getManglerTableModel() {
        return mdlMangler;
    }

    public JMipssBeanTable<AvvikSokResult> getManglerTable() {
        return tblMangler;
    }

    /**
     * Viser en ventedialog og åpner saken i en dialog
     *
     * @param avvikResult
     */
    public void openAvvik(final AvvikSokResult avvikResult) {
        controller.getSakController().openSak(avvikResult.getSakGuid(), avvikResult.getSlettetDato() != null ? new String[]{avvikResult.getGuid()} : null, avvikResult.getGuid());
    }

    public void openAvvik(Avvik avvik) {
        AvvikSokResult rs = new AvvikSokResult();
        rs.setGuid(avvik.getGuid());
        rs.setSakGuid(avvik.getSakGuid());
        rs.setSlettetDato(avvik.getSlettetDato());
        openAvvik(rs);
    }

    private void initAvvikTable() {
        mdlAvvik = new MipssBeanTableModel<AvvikSokResult>(AvvikSokResult.class, "refnummer", "opprettetAv", "opprettetDato", "sisteStatus", "sisteStatusDato",
                "tiltaksDato", "prosessKode", "aarsakNavn", "tilleggsarbeid", "etterslep", "trafikksikkerhet", "fylkesnummer", "kommunenummer",
                "veitype", "veinummer", "hp", "meter", "kjorefelt", "beskrivelse", "antallBilder", "pdaDfuNavn", "pdaDfuId", "historikkStreng");
        MipssRenderableEntityTableColumnModel avvikColumnModel = new MipssRenderableEntityTableColumnModel(
                AvvikSokResult.class, mdlAvvik.getColumns());

        avvikColumnModel.getColumn(4).setCellRenderer(new DateTimeRenderer(MipssDateFormatter.DATE_FORMAT));

        mdlAvvik.setBeanInstance(avvikService);
        mdlAvvik.setBeanMethod("sokAvvik");
        mdlAvvik.setBeanInterface(AvvikService.class);
        mdlAvvik.setBeanMethodArguments(new Class<?>[]{AvvikQueryParams.class});

        tblAvvik = new JMipssBeanTable<AvvikSokResult>(mdlAvvik, avvikColumnModel);
        tblAvvik.setDefaultRenderer(Date.class, new DateTimeRenderer());
        tblAvvik.setDefaultRenderer(Boolean.class, new BooleanRenderer());
        tblAvvik.setDefaultRenderer(Double.class, new DoubleRenderer());
        tblAvvik.setFillsViewportHeight(true);
        tblAvvik.setFillsViewportWidth(true);
        tblAvvik.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        tblAvvik.setColumnControlVisible(true);
        tblAvvik.addColumnStateSupport("avvikTable", controller.getPlugin());

        tblAvvik.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    openAvvik(mdlAvvik.get(tblAvvik.convertRowIndexToModel(tblAvvik.getSelectedRow())));
                }
            }
        });

        ColorHighlighterFactory fact = new ColorHighlighterFactory();
        tblAvvik.setHighlighters(new Highlighter[]{
                fact.getFristdatoWarningHighlighter(this),
                fact.getFristdatoOverskredetHighlighter(this)
        });
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(0), 60);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(1), 90);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(2), 90);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(3), 30);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(4), 30);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(5), 117);
//
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(7), 65);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(8), 65);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(9), 65);
//
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(11), 45);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(12), 45);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(13), 45);
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(14), 45);

//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(0), 60); // sisteStatusDato
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(1), 90); // sisteStatusDato
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(3), 90); // tiltaksDato
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(4), 130); // prosessTxt
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(5), 110); // tilstandtypeNavn
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(7), 30); // harHendelse
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(8), 30); // harInspeksjon
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(9), 30); // etterslep
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(10), 45); // antallBilder
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(11), 30); // fylkesnummer
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(12), 30); // kommunenummer
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(13), 30); // vei
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(14), 40); // veinummer
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(15), 30); // hp
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(16), 40); // meter
//		TableHelper.setColumnWidth(avvikColumnModel.getColumn(17), 90); // registrertAv

        tblAvvik.loadUserSettings();
    }

    private void initManglerTable() {
        mdlMangler = new MipssBeanTableModel<AvvikSokResult>(AvvikSokResult.class, "refnummer", "opprettetDato", "sisteStatus", "sisteStatusDato",
                "tiltaksDato", "prosessKode", "aarsakNavn", "tilleggsarbeid", "etterslep", "fylkesnummer", "kommunenummer",
                "veitype", "veinummer", "hp", "meter", "kjorefelt", "beskrivelse", "antallBilder", "historikkStreng");

        MipssRenderableEntityTableColumnModel avvikColumnModel = new MipssRenderableEntityTableColumnModel(
                AvvikSokResult.class, mdlMangler.getColumns());
        //Viser KUN dato for tiltaksdato
        avvikColumnModel.getColumn(4).setCellRenderer(new DateTimeRenderer(MipssDateFormatter.DATE_FORMAT));

        mdlMangler.setBeanInstance(avvikService);
        mdlMangler.setBeanMethod("sokMangler");
        mdlMangler.setBeanInterface(AvvikService.class);
        mdlMangler.setBeanMethodArguments(new Class<?>[]{AvvikQueryParams.class});

        tblMangler = new JMipssBeanTable<AvvikSokResult>(mdlMangler, avvikColumnModel);

        tblMangler.setDefaultRenderer(Date.class, new DateTimeRenderer());
        tblMangler.setDefaultRenderer(Boolean.class, new BooleanRenderer());
        tblMangler.setDefaultRenderer(Double.class, new DoubleRenderer());
        tblMangler.setDoWidthHacks(true);
        tblMangler.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        tblMangler.setColumnControlVisible(true);
        tblMangler.addColumnStateSupport("manglerTable", controller.getPlugin());

        tblMangler.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    openAvvik(mdlMangler.get(tblMangler.convertRowIndexToModel(tblMangler.getSelectedRow())));
                }
            }
        });
        ColorHighlighterFactory fact = new ColorHighlighterFactory();
        tblMangler.setHighlighters(
                fact.getLukketMangelHighlighter(this),
                fact.getAvvikSomIkkeErManglerHighlighter(this));
        tblMangler.loadUserSettings();
        tblMangler.setSortOrder(4, SortOrder.ASCENDING);
    }

    public JMipssBeanTable<AvvikStatus> createAvvikStatusTable() {
        MipssBeanTableModel<AvvikStatus> tableModel = new MipssBeanTableModel<AvvikStatus>("statuser", AvvikStatus.class, "opprettetDato", "avvikstatusType", "fritekst", "opprettetAv");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(AvvikStatus.class, tableModel.getColumns());

        final JMipssBeanTable<AvvikStatus> table = new JMipssBeanTable<AvvikStatus>(tableModel, columnModel);
        table.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
                    if (e.getClickCount() == 2) {
                        List<AvvikStatus> statuses = table.getSelectedEntities();
                        AvvikStatus status = (statuses != null && statuses.size() > 0 ? statuses.get(0) : null);

                        if (status != null) {
                            AvvikStatusBeskrivelseDialog dialog = new AvvikStatusBeskrivelseDialog((JDialog) SwingUtilities.windowForComponent(table), status);
                            dialog.show(table);
                            if (!dialog.isCancelled()) {
                                status.setFritekst(dialog.getAvvikStatusFritekst());
                                //TODO notify savemanager!..
                            }
                        }
                    }
                }

            }
        });
        table.setFillsViewportWidth(true);
        table.setFillsViewportHeight(true);
        table.setDefaultRenderer(AvvikstatusType.class, new MipssEntityRenderer());
        table.setDefaultRenderer(Date.class, new DateTimeRenderer());
        table.setSortOrder(0, SortOrder.DESCENDING);
        return table;

    }

    public Avvik opprettAvvik(Sak sak) {
        Avvik a = new Avvik();
        a.setGuid(ServerUtils.getInstance().fetchGuid());
        a.setEtterslepFlagg(Boolean.FALSE);
        a.setSak(sak);
        a.setOpprettetAv(controller.getLoggedOnUserSign());
        a.setOpprettetDato(Clock.now());
        //TODO avvikets stedfesting være den samme som sak sin?

        Punktstedfest copyStedfesting = getFeltController().copyStedfesting(sak.getStedfesting().get(0));
        a.setStedfesting(copyStedfesting);

        AvvikStatus as = new AvvikStatus();
        as.setOpprettetAv(controller.getLoggedOnUserSign());
        as.setOpprettetDato(Clock.now());
        as.setAvvikstatusType(getStatus(AvvikstatusType.NY_STATUS));
        as.setAvvik(a);
        a.addStatus(as);
        a.setNewEntity(true);
        return a;
    }

    /**
     * Henter et avvik med angitt guid
     *
     * @param guid
     * @return
     */
    public Avvik hentAvvik(String guid) {
        return avvikService.hentAvvik(guid);
    }

    /**
     * Henter et avvik med angitt refnummer
     *
     * @param refnummer
     * @return
     */
    public Avvik hentAvvik(Long refnummer) {
        Avvik avvik = avvikService.hentAvvik(refnummer);
        return avvik;
    }

    /**
     * Lagrer/oppdaterer et avvik i databasen
     *
     * @param avvik
     */
    public Avvik persistAvvik(Avvik avvik) {
        String brukerSign = controller.getLoggedOnUserSign();
        Avvik av = avvikService.persistAvvik(avvik, brukerSign);
        List<String> bilder = new ArrayList<String>();
        for (AvvikBilde ab : av.getBilder()) {
            bilder.add(ab.getBilde().getGuid());
        }
        controller.persistSynkbilde(bilder);

        reloadTable();
        reloadManglerTable();
        return av;
    }

    public void reloadManglerTable() {
//		final int selectedRow= getManglerTable().getSelectedRow();
        getManglerTableModel().loadData();
//		getManglerTableModel().addTableLoaderListener(new MipssBeanLoaderListener(){
//			@Override
//			public void loadingStarted(MipssBeanLoaderEvent event) {}
//			@Override
//			public void loadingFinished(MipssBeanLoaderEvent event) {
//				getManglerTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
//			}
//		});
    }

    public void reloadTable() {
        final int selectedRow = getAvvikTable().getSelectedRow();
        getAvvikTableModel().loadData();
        getAvvikTableModel().addTableLoaderListener(new MipssBeanLoaderListener() {
            @Override
            public void loadingStarted(MipssBeanLoaderEvent event) {
            }

            @Override
            public void loadingFinished(MipssBeanLoaderEvent event) {
                getAvvikTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
            }
        });
        getFeltController().getSakController().reloadTable();
    }

    /**
     * Henter alle AvvikstatusTyper fra databasen
     *
     * @param refresh true hvis nye instanser skal hentes.
     * @return false returnerer cache.
     */
    public List<AvvikstatusType> getStatusTyper(boolean refresh, boolean alle) {
        if (refresh || statusTyperCache == null) {
            statusTyperCache = avvikService.hentAlleStatusTyper();
        }
        if (!alle) {
            List<AvvikstatusType> kunValgbar = new ArrayList<AvvikstatusType>();
            for (AvvikstatusType t : statusTyperCache) {
                if (t.getValgbar())
                    kunValgbar.add(t);
            }
            return kunValgbar;
        }
        return statusTyperCache;
    }

    /**
     * Henter AvvikstatusTyper fra databasen
     *
     * @param G3 true hvis kun satuser som tilhører tilleggsarbeid skal hentes. false hvis man ønsker alle statuser med G2 flagget satt.
     * @return Liste med statuser.
     */
    public List<AvvikstatusType> getStatusTyper(boolean G3) {

        List<AvvikstatusType> alleStatuser = avvikService.hentAlleStatusTyper();
        List<AvvikstatusType> G2statuser = new ArrayList<AvvikstatusType>();
        List<AvvikstatusType> G3statuser = new ArrayList<AvvikstatusType>();

        for (AvvikstatusType avvikstatusType : alleStatuser) {
            if (avvikstatusType.getValgbar()) {
                if (G3 && avvikstatusType.getG3()) {
                    G3statuser.add(avvikstatusType);
                } else if (!G3 && avvikstatusType.getG2()) {
                    G2statuser.add(avvikstatusType);
                }
            }
        }

        if (G3) {
            return G3statuser;
        }
        return G2statuser;
    }

    public List<AvvikstatusType> getAlleStatuser() {
        return avvikService.hentAlleStatusTyper();
    }


    public AvvikstatusType getStatus(String status) {
        AvvikstatusType type = null;
        for (AvvikstatusType sok : getStatusTyper(false, true)) {
            if (StringUtils.equals(sok.getStatus(), status)) {
                type = sok;
                break;
            }
        }

        return type;
    }

    /**
     * Henter alle Avvikaarsaker fra databasen
     * Legger til avvikets aarsak, dersom den ikke allerede er med i lista.
     */
    public List<Avvikaarsak> getAvvikaarsaker(Avvik avvik) {
        List<Avvikaarsak> aarsaker = getAvvikaarsaker();
        if(avvik != null && !aarsaker.contains(avvik.getAarsak())) {
            aarsaker.add(avvik.getAarsak());
        }
        return aarsaker;
    }

    public List<Avvikaarsak> getAvvikaarsaker() {
        return  avvikService.hentAlleAvvikaarsaker();
    }

    public List<AvvikAvstand> sokAvvikINarheten(Avvik avvik) {
        List<AvvikAvstand> avvikList = avvikService.sokAvvikINarheten(avvik, getQueryParams().getValgtKontrakt().getId());
        Collections.sort(avvikList, new Comparator<AvvikAvstand>() {
            @Override
            public int compare(AvvikAvstand o1, AvvikAvstand o2) {
                return o1.getAvstand().compareTo(o2.getAvstand());
            }
        });
        return avvikList;

    }

    /**
     * Henter alle Avviktilstander fra databasen
     *
     * @param refresh true hvis nye instanser skal hentes.
     * @return false returnerer cache.
     */
    public List<Avviktilstand> getAvviktilstander(boolean refresh) {
        if (refresh || avviktilstandCache == null) {
            avviktilstandCache = avvikService.hentAlleAvviktilstander();
        }
        return avviktilstandCache;
    }

    /**
     * Henter alle Objekttyper fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektType> getObjektTyper(boolean refresh, Long prosessId, Long kontraktId) {
        return avvikService.hentAlleObjektTyper(prosessId, kontraktId);
    }

    /**
     * Henter alle ObjektAvvikTyper fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektAvvikType> getObjektAvvikTyper(boolean refresh, Long prosessId, Long kontraktId) {
        return avvikService.hentAlleObjektAvvikTyper(prosessId, kontraktId);
    }

    /**
     * Henter alle ObjektAvvikKategorir fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektAvvikKategori> getObjektAvvikKategorier(boolean refresh, Long prosessId, Long kontraktId) {
        return avvikService.hentAlleObjektAvvikKategorier(prosessId, kontraktId);
    }

    /**
     * Henter alle ObjektStedTypeAngivelser fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektStedTypeAngivelse> getObjektStedTypeAngivelser(boolean refresh, Long prosessId, Long kontraktId) {
        return avvikService.hentAlleObjektStedTypeAngivelser(prosessId, kontraktId);
    }

    /**
     * Henter alle ArbeidTyper fra databasen
     *
     * @param refresh true hvis nye instanser skal hentes.
     * @return false returnerer cache.
     */
    public List<ArbeidType> getArbeidTyper(boolean refresh) {
        if (refresh || arbeidTypeCache == null) {
            arbeidTypeCache = avvikService.hentAlleArbeidTyper();
            arbeidTypeCache.add(null); // this ensures that we display "< ikke valgt>" in the drop-down list
        }
        return arbeidTypeCache;
    }

    public Avvik leggTilKommentar(String avvikGuid, String kommentar) {
        return avvikService.leggTilKommentar(avvikGuid, kommentar, controller.getLoggedOnUserSign());
    }

    /**
     * Henter id til kontrakten som tilhører avviket med angitt refnummer
     *
     * @param refnummer
     * @return
     */
    public Long getDriftkontraktIdForAvvik(Long refnummer) {
        return feltBean.getDriftkontraktIdForAvvik(refnummer);
    }

    public FeltController getFeltController() {
        return controller;
    }

    public Action getAvvikNyAction() {
        return buttonController.getAvvikNyAction();
    }


    public Action getAvvikExcelAction() {
        return buttonController.getAvvikExcelAction();
    }

    public Action getAvvikRapportAction() {
        return buttonController.getAvvikRapportAction();
    }

    public Action getAvvikListRapportAction() {
        return buttonController.getAvvikListRapportAction();
    }

    public Action getAvvikRapportAction(final AvvikDetaljPanel pnl) {
        return buttonController.getAvvikRapportAction(pnl);
    }

    public Action getLeggTilKommentarAction(final AvvikDetaljPanel pnl) {
        return buttonController.getLeggTilKommentarAction(pnl);
    }

    public Action getVisAvvikINarhetenAction(final AvvikDetaljPanel pnl) {
        return buttonController.getVisAvvikINarhetenAction(pnl);
    }

    public Action getAvvikDetaljerAction() {
        return buttonController.getAvvikDetaljerAction();
    }

    public Action getAvvikKartAction() {
        return buttonController.getAvvikKartAction();
    }

    public Action getAvvikSokOpprettetButtonAction() {
        return buttonController.getAvvikSokOpprettetButtonAction(this);
    }

    public Action getAvvikSokApnePrTilDatoButtonAction() {
        return buttonController.getAvvikSokApnePrTilDatoButtonAction(this);
    }

    public Action getAvvikSokStatusendringButtonAction() {
        return buttonController.getAvvikSokStatusendringButtonAction(this);
    }

    public Action getManglerSokButtonAction() {
        return buttonController.getManglerSokButtonAction(this);
    }

    public Action getAvvikHentMedIdAction() {
        return buttonController.getAvvikHentMedIdAction();
    }

    public Action getSlettAvvikStatus(JMipssBeanTable<AvvikStatus> table, StatusHistoryPanel statusHistoryPanel) {
        return buttonController.getSlettAvvikStatus(table, statusHistoryPanel);
    }

    public Action getLeggTilAvvikStatus(StatusHistoryPanel historyPanel) {
        return buttonController.getLeggTilAvvikStatus(historyPanel);
    }

    public AvvikQueryParams getQueryParams() {
//		QueryParams qp = getFeltController().getCommonQueryParams();
//		qp.merge(queryParams);
        return queryParams;
    }

    public Action getEndreVeiAction(TidStedPanel tidStedPanel) {
        return buttonController.getEndreVeiAction(tidStedPanel, this);
    }

    public Action getEndreProsessAction(VurderingPanel vurderingPanel) {
        return buttonController.getEndreProsessAction(vurderingPanel, vurderingPanel, this);
    }

    public Action getAvvikLagreAction(AvvikDetaljPanel avvikDetaljPanel) {
        return buttonController.getAvvikLagreAction(avvikDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign());
    }

    public Action getAvvikSlettAction(AvvikDetaljPanel avvikDetaljPanel, Action callback) {
        return buttonController.getAvvikSlettAction(avvikDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getAvvikSlettAction(final AvvikButtonPanel panel) {
        return buttonController.getAvvikSlettAction(panel);
    }

    public Action getAvvikGjenopprettAction(AvvikDetaljPanel avvikDetaljPanel, Action callback) {
        return buttonController.getAvvikGjenopprettAction(avvikDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getLukkAvvikAction(StatusHistoryPanel statusHistoryPanel, AvvikDetaljPanel avvikDetaljPanel) {
        return buttonController.getLukkAvvikAction(statusHistoryPanel, avvikDetaljPanel);
    }

    public Action getGjenapneAvvikAction(StatusHistoryPanel statusHistoryPanel) {
        return buttonController.getGjenapneAvvikAction(statusHistoryPanel);
    }

    public Action getManglerExcelAction() {
        return buttonController.getManglerExcelAction();
    }

    public Action getVelgAvvikstatusAction(AvvikQueryParams queryParams) {
        return buttonController.getVelgAvvikstatusAction(queryParams);
    }

    public Action getVelgAarsakAction(AvvikQueryParams queryParams) {
        return buttonController.getVelgAarsakAction(queryParams);
    }
}
