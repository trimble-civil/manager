package no.mesta.mipss.felt.util;

import no.mesta.mipss.felt.Resources;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringConcatHelper {

    public static String concatProsess(String[] prosesskoder) {
        if (prosesskoder == null || prosesskoder.length == 0) {
            return Resources.getResource("label.valgteProsesser.default");
        }
        if (prosesskoder.length > 10) {
            StringBuilder prosesskoderString = new StringBuilder();
            prosesskoderString.append("<html>");
            String[] koderL1 = Arrays.copyOfRange(prosesskoder, 0, 10);
            String[] koderL2 = Arrays.copyOfRange(prosesskoder, 10, prosesskoder.length);
            for (int i = 0; i < 8; i++) {
                koderL1[i] = prosesskoder[i];
            }
            prosesskoderString.append(StringUtils.join(koderL1, "<b>,</b> "));
            prosesskoderString.append("<br>");
            prosesskoderString.append(StringUtils.join(koderL2, "<b>,</b> "));
            prosesskoderString.append("</html>");
            return prosesskoderString.toString();
        } else {
            StringBuilder prosesskoderString = new StringBuilder();
            prosesskoderString.append("<html>");
            prosesskoderString.append(StringUtils.join(prosesskoder, "<b>,</b> "));
            prosesskoderString.append("</html>");
            return prosesskoderString.toString();
        }
    }

    public static String createTooltip(Object[] values) {
        if (values == null)
            return null;
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        List<Object> line = new ArrayList<Object>();
        for (Object s : values) {
            line.add(s);
            if (line.size() == 6) {
                sb.append(StringUtils.join(line, ", "));
                sb.append("<br>");
                line.clear();
            }
        }
        if (line.size() != 0) {
            sb.append(StringUtils.join(line, ", "));
        }
        sb.append("</html>");
        return sb.toString();
    }
}
