package no.mesta.mipss.felt.elrapp;

import no.mesta.mipss.elrapp.ElrappValidationException;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.R11Controller;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.R11Panel;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class R11Validator extends ElrappValidator {

    private final Sak sak;

    private final R11Controller controller;

    public R11Validator(Sak sak, R11Controller controller) {
        this.sak = sak;
        this.controller = controller;

    }

    @Override
    public List<String> validate() throws ElrappValidationException {
        List<String> errMsg = new ArrayList<String>();
        Skred skred = sak.getSkred();
        if (skred.getStengtPgaSkredfare() != null && skred.getStengtPgaSkredfare()) {
            errMsg = validateSkredfare(skred);
        } else {
            errMsg = validateSkred(skred);
        }
        if (errMsg.size() != 0) {
            throw new ElrappValidationException(StringUtils.join(errMsg, "\n"));
        }
        return errMsg;
    }

    private List<String> validateSkredfare(Skred skred) {
        List<String> errMsg = new ArrayList<String>();
        Egenskap typeStenging = controller.findEgenskap(R11Panel.STENGING_ID);
        Egenskap stengtRetning = controller.findEgenskap(R11Panel.STENGNINGSRETNING_ID);

        if (!controller.containsEgenskap(typeStenging, skred)) {
            errMsg.add(Resources.getResource("feilmelding.manglerVerdi", typeStenging.getNavn()));
        }
        if (!controller.containsEgenskap(stengtRetning, skred)) {
            errMsg.add(Resources.getResource("feilmelding.manglerVerdi", stengtRetning.getNavn()));
        }
        if (skred.getStengtDato() == null) {
            errMsg.add(Resources.getResource("feilmelding.manglerVerdi", Resources.getResource("label.r11.stengt")));
        }
        if (skred.getAapnetDato() == null) {
            errMsg.add(Resources.getResource("feilmelding.manglerVerdi", Resources.getResource("label.r11.apnet")));
        }
        return errMsg;
    }

    private List<String> validateSkred(Skred skred) {
        List<String> errMsg = new ArrayList<String>();
        Egenskap type = controller.findEgenskap(R11Panel.SKREDTYPE_ID);
        Egenskap stenging = controller.findEgenskap(R11Panel.STENGNINGSKRED_ID);
        if (!controller.containsEgenskap(type, skred)) {
            errMsg.add(Resources.getResource("feilmelding.manglerVerdi", type.getNavn()));
        }
        if (!controller.containsEgenskap(stenging, skred)) {
            errMsg.add(Resources.getResource("feilmelding.manglerVerdi", stenging.getNavn()));
        }
        return errMsg;
    }

}