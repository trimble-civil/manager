package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.util.ImageDropper;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class SkredKommentarBildePanel extends JPanel {
    private JTextArea kommentarArea;
    private SkredBildePanel pnlBilder;
    private final R11Controller controller;

    public SkredKommentarBildePanel(R11Controller controller) {
        this.controller = controller;

        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlBilder = new SkredBildePanel(controller.getRskjemaController());
        new ImageDropper(pnlBilder, new ImageDropper.ImageDropListener() {
            @Override
            public void imageDropped(Bilde[] images) {
                for (Bilde i : images) {
                    pnlBilder.addBilde(i);
                }
            }
        }, controller.getRskjemaController().getFeltController().getLoggedOnUserSign());


    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(pnlBilder, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.4, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 5), 0, 0));
        add(createBildekommentarPanel(), new GridBagConstraints(0, 1, 1, 1, 1.0, 0.6, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(15, 5, 0, 5), 0, 0));
    }

    public void setSkred(Skred skred) {
        BindingHelper.createbinding(skred, "kommentar", kommentarArea, "text", BindingHelper.READ_WRITE).bind();
        pnlBilder.setSkred(skred);
    }

    private JPanel createBildekommentarPanel() {
        JPanel panel = new JPanel(new GridBagLayout());
        JLabel kommentarLabel = new JLabel(Resources.getResource("label.r11.kommentarer"));
        Font bold = kommentarLabel.getFont();
        bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
        kommentarLabel.setFont(bold);
        kommentarArea = new JTextArea();
        kommentarArea.setWrapStyleWord(true);
        kommentarArea.setLineWrap(true);
        JScrollPane kommentarScroll = new JScrollPane(kommentarArea);

        panel.add(kommentarLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        panel.add(kommentarScroll, new GridBagConstraints(0, 1, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));


        panel.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.bildekommentarTitle")));
        return panel;
    }
}
