package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.persistence.Clock;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;
import java.util.Date;

public class FristdatoAvvikHighlighter implements HighlightPredicate {

    private final AvvikController controller;

    public FristdatoAvvikHighlighter(AvvikController controller) {
        this.controller = controller;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        AvvikSokResult srs = controller.getAvvikTableModel().get(controller.getAvvikTable().convertRowIndexToModel(adapter.row));
        if (srs.getTiltaksDato() != null) {
            Date mangelIntrefferDato = CalendarUtil.round(Clock.now(), true);
            Date mangel3Dager = CalendarUtil.getDaysAgo(mangelIntrefferDato, -3);
            if (CalendarUtil.isOverlappende(mangelIntrefferDato, mangel3Dager, srs.getTiltaksDato(), srs.getTiltaksDato(), true)) {
                if (srs.getSisteStatus() != null && srs.getSisteStatus().toLowerCase().startsWith("lukket")) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }
}