package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.StringNullConverter;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class BeskrivelsePanel extends JPanel implements LoadingPanel {

    private JTextArea txtBeskrivelse;
    private JScrollPane scrBeskrivelse;
    private Hendelse hendelse;
    private BindingGroup bindings = new BindingGroup();

    public BeskrivelsePanel() {
        initComponents();
        initGui();
    }

    private void initComponents() {
        txtBeskrivelse = new JTextArea();
        txtBeskrivelse.setWrapStyleWord(true);
        txtBeskrivelse.setLineWrap(true);
        scrBeskrivelse = new JScrollPane(txtBeskrivelse);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.beskrivelseHendelse")));
        add(scrBeskrivelse, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    private void bind() {
        Binding<Hendelse, String, JTextArea, String> beskrivelseBind = BindingHelper.createbinding(hendelse, "beskrivelse", txtBeskrivelse, "text", UpdateStrategy.READ_WRITE);
        beskrivelseBind.setConverter(new StringNullConverter());
        bindings.addBinding(beskrivelseBind);
        bindings.bind();
    }

    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
        bind();

    }

    @Override
    public boolean isLoading() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

}
