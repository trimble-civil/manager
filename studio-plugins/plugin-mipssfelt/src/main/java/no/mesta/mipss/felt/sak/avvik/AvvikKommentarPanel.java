package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.resources.images.IconResources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AvvikKommentarPanel extends JPanel {

    private JTextArea txtKommentar;
    private JScrollPane scrKommentar;

    private JButton btnOk;
    private JButton btnAvbryt;
    private boolean cancelled = true;

    public AvvikKommentarPanel() {
        initComponents();
        initGui();
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public String getKommentar() {
        return txtKommentar.getText();
    }

    private void initComponents() {
        txtKommentar = new JTextArea();
        txtKommentar.setLineWrap(true);
        txtKommentar.setWrapStyleWord(true);

        scrKommentar = new JScrollPane(txtKommentar);

        btnOk = new JButton(getOkAction());
        btnAvbryt = new JButton(getAvbrytAction());
    }

    private AbstractAction getOkAction() {
        return new AbstractAction(Resources.getResource("button.ok"), IconResources.OK2_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = false;
                SwingUtilities.windowForComponent(AvvikKommentarPanel.this).dispose();
            }
        };
    }

    private AbstractAction getAvbrytAction() {
        return new AbstractAction(Resources.getResource("button.avbryt"), IconResources.CANCEL_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {

                SwingUtilities.windowForComponent(AvvikKommentarPanel.this).dispose();
            }
        };
    }

    private void initGui() {
        setLayout(new BorderLayout());


        JPanel pnlButton = new JPanel(new GridBagLayout());
        pnlButton.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlButton.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        JPanel pnlKommentar = new JPanel(new BorderLayout());
        pnlKommentar.add(scrKommentar);
        pnlKommentar.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.avvikKommentar")));

        add(pnlKommentar);
        add(pnlButton, BorderLayout.SOUTH);
    }

}
