package no.mesta.mipss.felt;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.rapportfunksjoner.NokkeltallFelt;
import no.mesta.mipss.resources.images.IconResources;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class MangelStatistikkPanel extends JPanel {

    private final FeltController controller;
    private JLabel lblTotalAntallMangler;
    private JLabel lblAntallManglerMedLukketAvvik;
    private JLabel lblAntallManglerMedAapentAvvik;

    private JLabel lblTotalAntallManglerValue;
    private JLabel lblAntallManglerMedLukketAvvikValue;
    private JLabel lblAntallManglerMedAapentAvvikValue;

    private JButton btnOppdater;

    public MangelStatistikkPanel(FeltController controller) {

        this.controller = controller;
        initComponents();
        initGui();
        bind();
        updateData();
    }

    private void initComponents() {
        lblTotalAntallMangler = new JLabel(Resources.getResource("label.totaltAntallMangler"));
        lblTotalAntallMangler.setToolTipText(Resources.getResource("label.totaltAntallMangler.tooltip"));
        lblAntallManglerMedLukketAvvik = new JLabel(Resources.getResource("label.manglerMedLukketAvvik"));
        lblAntallManglerMedLukketAvvik.setToolTipText(Resources.getResource("label.manglerMedLukketAvvik.tooltip"));
        lblAntallManglerMedAapentAvvik = new JLabel(Resources.getResource("label.manglerMedAapentAvvik"));
        lblAntallManglerMedAapentAvvik.setToolTipText(Resources.getResource("label.manglerMedAapentAvvik.tooltip"));

        lblTotalAntallManglerValue = new JLabel("0");
        lblAntallManglerMedLukketAvvikValue = new JLabel("0");
        lblAntallManglerMedAapentAvvikValue = new JLabel("0");

        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.manglerstat")));

        Dimension iconSize = new Dimension(22, 22);
        btnOppdater = new JButton(getOppdaterAction());
        btnOppdater.setVisible(false);
        btnOppdater.setMaximumSize(iconSize);
        btnOppdater.setMinimumSize(iconSize);
        btnOppdater.setPreferredSize(iconSize);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(lblTotalAntallMangler, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        add(lblTotalAntallManglerValue, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        add(lblAntallManglerMedLukketAvvik, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        add(lblAntallManglerMedLukketAvvikValue, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        add(lblAntallManglerMedAapentAvvik, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblAntallManglerMedAapentAvvikValue, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(btnOppdater, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    }

    private void bind() {
        Binding<QueryParams, Object, MangelStatistikkPanel, Object> kontraktBind = BindingHelper.createbinding(controller.getCommonQueryParams(), "valgtKontrakt", this, "valgtKontrakt", UpdateStrategy.READ);
        controller.getBindingGroup().addBinding(kontraktBind);
    }

    public void setValgtKontrakt(Driftkontrakt valgtKontrakt) {
        updateData();
    }

    private Action getOppdaterAction() {
        return new AbstractAction("", IconResources.REFRESH_VIEW_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateData();
            }
        };
    }

    public void updateData() {
        NokkeltallFelt stats = controller.getMangelStatistikk();
        if (stats != null) {
            lblTotalAntallManglerValue.setText(stats.getAntManglerAlle() != null ? String.valueOf(stats.getAntManglerAlle()) : "");
            lblAntallManglerMedLukketAvvikValue.setText(stats.getAntManglerLukketAlle() != null ? String.valueOf(stats.getAntManglerLukketAlle()) : "");
            lblAntallManglerMedAapentAvvikValue.setText(stats.getAntManglerAapne() != null ? String.valueOf(stats.getAntManglerAapne()) : "");
        } else {
            lblTotalAntallManglerValue.setText("..");
            lblAntallManglerMedLukketAvvikValue.setText("..");
            lblAntallManglerMedAapentAvvikValue.setText("..");
        }
    }
}
