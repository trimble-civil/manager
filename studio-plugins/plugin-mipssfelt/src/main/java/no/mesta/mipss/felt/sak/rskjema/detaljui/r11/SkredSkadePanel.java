package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DoubleStringConverter;
import no.mesta.mipss.core.IntegerStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.R11Controller.EgenskapCheck;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class SkredSkadePanel extends JPanel {
    private Font bold;
    private R11Controller controller;
    private JComboBox regnCombo;
    private JComboBox snoCombo;
    private JComboBox typeCombo;
    private JComboBox losneCombo;
    private JComboBox volumVeiCombo;
    private JComboBox blokkertCombo;
    private JComboBox stengingSkredCombo;

    private Egenskap skredtype;
    private Egenskap losneomrade;
    private Egenskap volum;
    private Egenskap stengtSkred;
    private JFormattedTextField volumTotField;
    private Egenskap hoyde;
    private Egenskap regn;
    private Egenskap sno;
    private Egenskap vind;
    private Egenskap blokkert;

    private JComboBox hoydeCombo;
    private JComboBox vindCombo;
    private JCheckBox nedborCheck;
    private JCheckBox ukjentNedborCheck;

    private JTextField temperaturField;
    private List<Egenskapverdi> skadeTypeList;
    private List<R11Controller.EgenskapCheck> skadeCheckList;

    public SkredSkadePanel(R11Controller controller) {
        this.controller = controller;
        initGui();
    }

    public void clearSelections() {
        hoydeCombo.setSelectedItem(null);
        typeCombo.setSelectedItem(null);
        losneCombo.setSelectedItem(null);
        blokkertCombo.setSelectedItem(null);
        stengingSkredCombo.setSelectedItem(null);
        volumVeiCombo.setSelectedItem(null);
        for (R11Controller.EgenskapCheck e : skadeCheckList) {
            e.setSelected(false);
        }
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(createTypePanel(), new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
        add(createVaerSkadePanel(), new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void setSkred(Skred skred) {

        EgenskapItemListener typeListener = new EgenskapItemListener(skredtype, controller.getSkred());
        EgenskapItemListener losneListener = new EgenskapItemListener(losneomrade, controller.getSkred());
        EgenskapItemListener volumListener = new EgenskapItemListener(volum, controller.getSkred());

        typeCombo.addItemListener(typeListener);
        losneCombo.addItemListener(losneListener);
        volumVeiCombo.addItemListener(volumListener);


        typeCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(skredtype));
        losneCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(losneomrade));
        volumVeiCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(volum));

        BindingGroup bg = new BindingGroup();
        DoubleStringConverter doubleStringConverter = new DoubleStringConverter();
        IntegerStringConverter integerStringConverter = new IntegerStringConverter();

        Binding storrelseBind = BindingHelper.createbinding(controller.getSkred(), "anslagTotalSkredstorrelse", volumTotField, "text", BindingHelper.READ_WRITE);
        storrelseBind.setConverter(integerStringConverter);

        EgenskapItemListener hoydeListener = new EgenskapItemListener(hoyde, controller.getSkred());
        EgenskapItemListener regnListener = new EgenskapItemListener(regn, controller.getSkred());
        EgenskapItemListener snoListener = new EgenskapItemListener(sno, controller.getSkred());
        EgenskapItemListener vindListener = new EgenskapItemListener(vind, controller.getSkred());
        EgenskapItemListener blokkertListener = new EgenskapItemListener(blokkert, controller.getSkred());
        EgenskapItemListener stengingSkredListener = new EgenskapItemListener(stengtSkred, controller.getSkred());

        hoydeCombo.addItemListener(hoydeListener);
        regnCombo.addItemListener(regnListener);
        snoCombo.addItemListener(snoListener);
        vindCombo.addItemListener(vindListener);
        blokkertCombo.addItemListener(blokkertListener);
        stengingSkredCombo.addItemListener(stengingSkredListener);

        hoydeCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(hoyde));

        Egenskapverdi regnVerdi = controller.getEgenskapveriSkredSingle(regn);
        if (regnVerdi != null)
            regnCombo.setSelectedItem(regnVerdi);

        Egenskapverdi snoVerdi = controller.getEgenskapveriSkredSingle(sno);
        if (snoVerdi != null)
            snoCombo.setSelectedItem(snoVerdi);

        vindCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(vind));

        Egenskapverdi blokkertVerdi = controller.getEgenskapveriSkredSingle(blokkert);
        if (blokkertVerdi != null) {
            blokkertCombo.setSelectedItem(blokkertVerdi);
        }
        stengingSkredCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(stengtSkred));

        Binding tempBind = BindingHelper.createbinding(controller.getSkred(), "temperatur", temperaturField, "text", BindingHelper.READ_WRITE);
        tempBind.setConverter(doubleStringConverter);

        bg.addBinding(storrelseBind);
        bg.addBinding(tempBind);
        bg.bind();

        BindingHelper.createbinding(controller.getSkred(), "ingenNedbor", nedborCheck, "selected", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(this, "ingenNedbor", nedborCheck, "${selected}", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(this, "ukjentNedbor", ukjentNedborCheck, "${selected}", BindingHelper.READ_WRITE).bind();

        for (R11Controller.EgenskapCheck e : skadeCheckList) {
            if (controller.isEgenskapVerdiPrensent(e.getVerdi())) {
                e.setSelected(true);
            }
        }
        if (skred.getIngenNedbor() == null || skred.getIngenNedbor() == false) {
            if (controller.getEgenskapveriSkredSingle(sno) == null && controller.getEgenskapveriSkredSingle(regn) == null) {
                ukjentNedborCheck.setSelected(true);
            }
//			skred.get
        }
    }

    private JPanel createTypePanel() {
        JPanel panel = new JPanel(new GridBagLayout());

        JLabel typeLabel = new JLabel(Resources.getResource("label.r11.typeSkred"));
        JLabel beskrivelseLabel = new JLabel(Resources.getResource("label.r11.beskrivelse"));

        JLabel volumLabel = new JLabel(Resources.getResource("label.r11.volum"));
        JLabel volumVeiLabel = new JLabel(Resources.getResource("label.r11.volumVeg"));
        JLabel volumTotLabel = new JLabel(Resources.getResource("label.r11.volumTot"));
        JLabel volumM2Label = new JLabel(Resources.getResource("label.r11.volumM2"));
        bold = volumLabel.getFont();
        bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
        volumLabel.setFont(bold);
        typeLabel.setFont(bold);
        beskrivelseLabel.setFont(bold);

        typeCombo = new JComboBox();
        losneCombo = new JComboBox();
        volumVeiCombo = new JComboBox();
        volumTotField = new JFormattedTextField();
        volumTotField.setDocument(new DocumentFilter(10));
        Dimension size = volumTotField.getPreferredSize();
        size.width = 100;
        volumTotField.setPreferredSize(size);

        skredtype = controller.findEgenskap(R11Panel.SKREDTYPE_ID);
        losneomrade = controller.findEgenskap(R11Panel.LOSNEOMRADE_ID);
        volum = controller.findEgenskap(R11Panel.VOLUM_ID);

        MipssComboBoxModel<Egenskapverdi> typeModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(skredtype), true);
        MipssComboBoxModel<Egenskapverdi> losneModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(losneomrade), true);
        MipssComboBoxModel<Egenskapverdi> volumModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(volum), true);

        typeCombo.setModel(typeModel);
        losneCombo.setModel(losneModel);
        volumVeiCombo.setModel(volumModel);

        JPanel volumPanel = new JPanel(new GridBagLayout());
        volumPanel.add(volumTotField, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        volumPanel.add(volumM2Label, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        panel.add(typeLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(beskrivelseLabel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(volumLabel, new GridBagConstraints(2, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));

        panel.add(typeCombo, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(losneCombo, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(volumVeiLabel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(volumVeiCombo, new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(volumTotLabel, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(volumPanel, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));

        panel.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.skredTitle")));
        return panel;
    }

    private JPanel createVaerSkadePanel() {
        JPanel panel = new JPanel(new GridBagLayout());

        JLabel hoydeLabel = new JLabel(Resources.getResource("label.r11.hoyde"));
        JLabel skadeLabel = new JLabel(Resources.getResource("label.r11.skade"));
        JLabel vaerLabel = new JLabel(Resources.getResource("label.r11.vaerforhold"));
        JLabel blokkertLabel = new JLabel(Resources.getResource("label.r11.blokkering"));
        JLabel stengingSkredLabel = new JLabel(Resources.getResource("label.r11.stengingFolgeAvSkred"));

        hoydeLabel.setFont(bold);
        skadeLabel.setFont(bold);
        vaerLabel.setFont(bold);
        blokkertLabel.setFont(bold);
        stengingSkredLabel.setFont(bold);

        JLabel regnLabel = new JLabel(Resources.getResource("label.r11.regn"));
        JLabel snoLabel = new JLabel(Resources.getResource("label.r11.sno"));
        JLabel vindLabel = new JLabel(Resources.getResource("label.r11.vindretning"));
        JLabel temperaturLabel = new JLabel(Resources.getResource("label.r11.temperatur"));
        JLabel graderLabel = new JLabel(Resources.getResource("label.r11.grader"));


        hoydeCombo = new JComboBox();
        regnCombo = new JComboBox();
        snoCombo = new JComboBox();
        vindCombo = new JComboBox();
        blokkertCombo = new JComboBox();
        stengingSkredCombo = new JComboBox();

        nedborCheck = new JCheckBox(Resources.getResource("label.r11.nedbor"));
        ukjentNedborCheck = new JCheckBox("Ukjent");
        temperaturField = new JTextField();

        hoyde = controller.findEgenskap(R11Panel.HOYDE_ID);
        regn = controller.findEgenskap(R11Panel.REGN_ID);
        sno = controller.findEgenskap(R11Panel.SNO_ID);
        vind = controller.findEgenskap(R11Panel.VIND_ID);
        blokkert = controller.findEgenskap(R11Panel.BLOKKERTVEI_ID);
        stengtSkred = controller.findEgenskap(R11Panel.STENGNINGSKRED_ID);

        MipssComboBoxModel<Egenskapverdi> hoydeModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(hoyde), true);
        MipssComboBoxModel<Egenskapverdi> regnModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(regn), true);
        MipssComboBoxModel<Egenskapverdi> snoModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(sno), true);
        MipssComboBoxModel<Egenskapverdi> vindModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(vind), true);
        MipssComboBoxModel<Egenskapverdi> blokkertModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(blokkert), true);
        MipssComboBoxModel<Egenskapverdi> stengingSkredModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(stengtSkred), true);

        hoydeCombo.setModel(hoydeModel);
        regnCombo.setModel(regnModel);
        snoCombo.setModel(snoModel);
        vindCombo.setModel(vindModel);
        blokkertCombo.setModel(blokkertModel);
        stengingSkredCombo.setModel(stengingSkredModel);

        Egenskap skade = controller.findEgenskap(R11Panel.SKADE_ID);

        skadeTypeList = controller.getEgenskapverdi(skade);
        skadeCheckList = new ArrayList<R11Controller.EgenskapCheck>();
        for (Egenskapverdi e : skadeTypeList) {
            EgenskapCheck wrapper = new EgenskapCheck(e);
            skadeCheckList.add(wrapper);

            wrapper.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    EgenskapCheck egenskap = (EgenskapCheck) e.getItem();
                    if (egenskap.isSelected()) {
                        controller.addEgenskapverdi(egenskap.getVerdi());
                    } else {
                        controller.slettEgenskapverdi(egenskap.getVerdi());
                    }
                }
            });
        }

        JPanel tempPanel = new JPanel(new GridBagLayout());
        tempPanel.add(temperaturField, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        tempPanel.add(graderLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));

        panel.add(hoydeLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(skadeLabel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(vaerLabel, new GridBagConstraints(2, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));

        panel.add(hoydeCombo, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 0, 0, 5), 0, 0));
        panel.add(blokkertLabel, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(blokkertCombo, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 0, 0, 5), 0, 0));
        panel.add(stengingSkredLabel, new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(stengingSkredCombo, new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 0, 0, 5), 0, 0));

        int i = 1;
        for (R11Controller.EgenskapCheck e : skadeCheckList) {
            panel.add(e, new GridBagConstraints(1, i++, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        }
        panel.add(nedborCheck, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(ukjentNedborCheck, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(regnLabel, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(regnCombo, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(snoLabel, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(snoCombo, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(vindLabel, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(vindCombo, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(temperaturLabel, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(tempPanel, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.veiinfoTitle")));
        return panel;
    }

    public void setUkjentNedbor(Boolean enabled) {

        if (enabled) {
            if (nedborCheck.isSelected()) {
                nedborCheck.setSelected(false);
            }
            snoCombo.setEnabled(false);
            regnCombo.setEnabled(false);
            removeSelectionRegnSnoCombo();
        } else {
            regnCombo.setEnabled(true);
            snoCombo.setEnabled(true);
        }
    }

    private void removeSelectionRegnSnoCombo() {
        regnCombo.setSelectedItem(null);

//		Egenskapverdi regnVerdi = controller.getEgenskapveriSkredSingle(regn);
//		controller.slettEgenskapverdi(regnVerdi);
        snoCombo.setSelectedItem(null);
//		Egenskapverdi snoVerdi = controller.getEgenskapveriSkredSingle(sno);
//		controller.slettEgenskapverdi(snoVerdi);

    }

    public void setIngenNedbor(Boolean enabled) {
        if (enabled) {
            if (ukjentNedborCheck.isSelected()) {
                ukjentNedborCheck.setSelected(false);
            }
            regnCombo.setEnabled(false);
            snoCombo.setEnabled(false);
            removeSelectionRegnSnoCombo();
        } else {
            regnCombo.setEnabled(true);
            snoCombo.setEnabled(true);
        }
    }
}
