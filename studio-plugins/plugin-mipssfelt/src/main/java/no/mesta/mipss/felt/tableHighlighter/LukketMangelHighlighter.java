package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class LukketMangelHighlighter implements HighlightPredicate {
    private final AvvikController controller;

    public LukketMangelHighlighter(AvvikController controller) {
        this.controller = controller;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        AvvikSokResult srs = controller.getManglerTableModel().get(controller.getManglerTable().convertRowIndexToModel(adapter.row));
        if (srs.getSisteStatus().toLowerCase().startsWith("lukket")) {
            return true;
        }
        return false;
    }
}
