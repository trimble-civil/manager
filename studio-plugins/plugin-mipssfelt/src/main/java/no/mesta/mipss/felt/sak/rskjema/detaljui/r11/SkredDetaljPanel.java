package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;
import no.mesta.mipss.valueobjects.ElrappStatus;

import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SkredDetaljPanel extends SakDetaljTabPanel {

    private RskjemaController controller;
    private FeltEntityInfo info;
    private String title;
    private R11Panel pnlR11;
    private Skred skred;
//	private SkredBildePanel pnlSkredBilder;
//	private SkredKommentarBildePanel pnlKommentarBilder;

    private SkredDetaljButtonPanel pnlButtons;

    public Sak sak;

    public SkredDetaljPanel(RskjemaController controller, FeltEntityInfo info) {
        this.controller = controller;
        this.info = info;
        this.title = "R11 (" + info.getRefnummer() + ")";
        initComponents();
        initGui();
        doLoad();
        waitWhileLoading();
    }

    public SkredDetaljPanel(RskjemaController controller, Skred skred, Sak sak) {
        this.controller = controller;
        this.title = "R11 (" + skred.getRefnummer() + ")";
        this.sak = sak;
        initComponents();
        initGui();
        setSkred(skred);
        bind();
    }

    public void setSak(Sak sak) {
        this.sak = sak;
    }

    @Override
    public void setChanged(boolean changed) {
        super.setChanged(changed);
        pnlButtons.setEnabled(changed);
    }

    private void initComponents() {
        pnlR11 = new R11Panel(controller);
//		pnlSkredBilder = new SkredBildePanel(controller);
//		pnlKommentarBilder = new SkredKommentarBildePanel(controller);

//		pnlR11.addTab(pnlSkredBilder, Resources.getResource("label.r11.tabHead.bilder"));
        pnlButtons = new SkredDetaljButtonPanel(controller, this);

        loadingPanels = new ArrayList<LoadingPanel>();
        loadingPanels.add(pnlR11);
//		loadingPanels.add(pnlSkredBilder);

    }

    public Skred getSkred() {
        return skred;
    }

    public Sak getSak() {
        return skred.getSak();
    }

    @Override
    protected void initGui() {
        super.initGui();
        setLayout(new BorderLayout());
        add(pnlSlettetInfo, BorderLayout.NORTH);
        add(pnlR11, BorderLayout.CENTER);//new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0),0,0));
        add(pnlButtons, BorderLayout.SOUTH);//new GridBagConstraints(0,1,1,1,1.0,0.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5),0,0));
    }

    @Override
    protected void bind() {
        SaveStatusMonitor<Skred> sMonitor = new SaveStatusMonitor<Skred>(Skred.class, this, "skred", getSaveStatusRegistry().getBindingGroup());
        sMonitor.registerField("anmeldt");
        sMonitor.registerField("stengtPgaSkredfare");
        sMonitor.registerField("skredEgenskapList");
        sMonitor.registerField("temperatur");
        sMonitor.registerField("anslagTotalSkredstorrelse");
        sMonitor.registerField("gjelderGS");
        sMonitor.registerField("kommentar");
        sMonitor.registerField("stengtDato");
        sMonitor.registerField("aapnetDato");
        sMonitor.registerField("bilder");
        sMonitor.registerField("skjemaEmne");
        sMonitor.registerField("endretDato");
        //TODO utgår fra registreringen
//		SaveStatusMonitor<VaerFraVaerstasjon> vMonitor = new SaveStatusMonitor<VaerFraVaerstasjon>(VaerFraVaerstasjon.class, skred, "vaerFraVaerstasjon", getSaveStatusRegistry().getBindingGroup());
//		vMonitor.registerField("mmNedborSiste3Dogn");
//		vMonitor.registerField("regnMmPrDag");
//		vMonitor.registerField("sluddMmPrDag");
//		vMonitor.registerField("snoMmPrDag");
//		vMonitor.registerField("temperatur");
//		vMonitor.registerField("vindretning");
//		vMonitor.registerField("vindstyrkeMPrSek");

        SaveStatusMonitor<Sak> sakMonitor = new SaveStatusMonitor<Sak>(Sak.class, this, "sak", getSaveStatusRegistry().getBindingGroup());
        sakMonitor.registerField("skadested");

        SaveStatusMonitor<Punktveiref> fraHpMonitor = new SaveStatusMonitor<Punktveiref>(Punktveiref.class, pnlR11.getController(), "fraHp", getSaveStatusRegistry().getBindingGroup());
        SaveStatusMonitor<Punktveiref> tilHpMonitor = new SaveStatusMonitor<Punktveiref>(Punktveiref.class, pnlR11.getController(), "tilHp", getSaveStatusRegistry().getBindingGroup());
        fraHpMonitor.registerField("fylkesnummer");
        fraHpMonitor.registerField("kommunenummer");
        fraHpMonitor.registerField("veikategori");
        fraHpMonitor.registerField("veistatus");
        fraHpMonitor.registerField("veinummer");
        fraHpMonitor.registerField("hp");
        fraHpMonitor.registerField("meter");
        fraHpMonitor.registerField("kjorefelt");

        tilHpMonitor.registerField("fylkesnummer");
        tilHpMonitor.registerField("kommunenummer");
        tilHpMonitor.registerField("veikategori");
        tilHpMonitor.registerField("veistatus");
        tilHpMonitor.registerField("veinummer");
        tilHpMonitor.registerField("hp");
        tilHpMonitor.registerField("meter");


        getSaveStatusRegistry().addMonitor(sMonitor);
//		getSaveStatusRegistry().addMonitor(vMonitor);
        getSaveStatusRegistry().addMonitor(sakMonitor);
        getSaveStatusRegistry().addMonitor(fraHpMonitor);
        getSaveStatusRegistry().addMonitor(tilHpMonitor);

        getSaveStatusRegistry().getBindingGroup().addBinding(BindingHelper.createbinding(getSaveStatusRegistry(), "changed", this, "changed"));
        getSaveStatusRegistry().doBind();
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void doneLoading() {
        setSkred(skred);

    }

    private void setSkred(Skred skred) {
        skred.setSak(sak);
        if (skred.getSlettetDato() != null) {
            pnlSlettetInfo.setVisible(true);
            lblSlettet.setText("Skredet er slettet, " + MipssDateFormatter.formatDate(skred.getSlettetDato()) + " av " + skred.getSlettetAv());
            pnlButtons.setGjenopprett(true);
            panelDisabledIndicator = new PanelDisabledIndicator(pnlR11);
        }
        this.skred = skred;
        pnlR11.setSkred(skred);
//		pnlSkredBilder.setSkred(skred);
//		pnlKommentarBilder.setSkred(skred);
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String refnummer) {
        this.title = "R11 (" + refnummer + ")";
        getTabHeader().setTitle(title);
    }

    @Override
    protected void load() {
        skred = controller.hentSkred(info.getGuid());

    }

    @Override
    public MipssFieldDetailItem getDetailEntity() {
        return skred;
    }

    @Override
    public FeltEntityInfo getFeltEntityInfo() {
        return info;
    }

    @Override
    public ElrappStatus getElrappStatus() {
        return ElrappStatus.getElrappStatus(skred.getRapportertDato(), skred.getEndretDato());
    }

    public void oppdaterElrappKule() {
        getTabHeader().setElrappStatus(getElrappStatus());
    }

}
