package no.mesta.mipss.felt.brukere;

import no.mesta.mipss.ui.table.CheckBoxTableObject;
import org.apache.commons.lang.builder.CompareToBuilder;

@SuppressWarnings("serial")
public class BrukerTableObject implements CheckBoxTableObject, Comparable<BrukerTableObject> {
    private Boolean valgt;
    private String navn;

    @Override
    public String getTextForGUI() {
        return null;
    }

    @Override
    public int compareTo(BrukerTableObject o) {
        return new CompareToBuilder().append(o.getNavn(), navn).toComparison();
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public String getNavn() {
        return navn;
    }

    @Override
    public void setValgt(Boolean valgt) {
        this.valgt = valgt;
    }

    @Override
    public Boolean getValgt() {
        return valgt;
    }

}
