package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.persistence.Clock;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;
import java.util.Date;

public class FristdatoOverskredetAvvikHighlighter implements HighlightPredicate {
    private final AvvikController controller;

    public FristdatoOverskredetAvvikHighlighter(AvvikController controller) {
        this.controller = controller;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        AvvikSokResult srs = controller.getAvvikTableModel().get(controller.getAvvikTable().convertRowIndexToModel(adapter.row));
        Date fristDatoOverskredetDato = CalendarUtil.round(Clock.now(), false);
        Date fristDato = srs.getTiltaksDato() != null ? CalendarUtil.round(srs.getTiltaksDato(), false) : null;
        if (fristDato != null && fristDato.before(fristDatoOverskredetDato)) {
            //fristdato er overskredet, sjekk siste statusdato og status
            if (srs.getSisteStatus() != null && srs.getSisteStatus().toLowerCase().startsWith("lukket")) {
                Date statusDato = CalendarUtil.round(srs.getSisteStatusDato(), true);
                if (statusDato.before(fristDato))//avviket er lukket før fristen
                    return false;
            }
            return true; //siste status er ikke: Lukket avvik
        }
        return false;
    }
}