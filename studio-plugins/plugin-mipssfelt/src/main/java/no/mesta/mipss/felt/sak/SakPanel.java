package no.mesta.mipss.felt.sak;

import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.SokekriterierTaskPane;
import no.mesta.mipss.ui.JMipssBeanScrollPane;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class SakPanel extends JPanel {

    private final SakController controller;
    private SokekriterierTaskPane taskPaneSok;
    private SokekriterierSakPanel pnlSokekriterier;
    private JButton btnSok;
    private JButton btnHentMedId;
    private JPanel pnlButton;

    private JMipssBeanScrollPane scrTable;

    private SakButtonPanel pnlSakButtons;


    public SakPanel(FeltController controller) {
        this.controller = controller.getSakController();
        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlSokekriterier = new SokekriterierSakPanel(controller);
        taskPaneSok = new SokekriterierTaskPane(pnlSokekriterier);
        btnSok = new JButton(controller.getSakSokButtonAction());
        btnHentMedId = new JButton(controller.getSakHentMedIdAction());
        pnlButton = new JPanel(new GridBagLayout());
        pnlSakButtons = new SakButtonPanel(controller, this);

        scrTable = new JMipssBeanScrollPane(controller.getSakTable(), controller.getSakTableModel());

    }


    private void initGui() {
        setLayout(new GridBagLayout());
        pnlButton.add(btnSok, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
        pnlButton.add(btnHentMedId, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(taskPaneSok, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(scrTable, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 0, 5, 0), 0, 0));
        add(pnlSakButtons, new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
    }
}
