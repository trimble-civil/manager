package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.RskjemaQueryParams;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.valueobjects.ElrappStatus;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class SokekriterierRskjemaPanel extends JPanel {

    private final RskjemaController controller;
    private JCheckBox chkKunSlettede;
    private JLabel lblType;
    private JLabel lblElrapp;

    private JComboBox comboType;
    private JComboBox comboElrapp;

    public SokekriterierRskjemaPanel(RskjemaController controller) {
        this.controller = controller;

        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        chkKunSlettede = new JCheckBox(Resources.getResource("check.kunSlettede"));
        chkKunSlettede.setToolTipText(Resources.getResource("check.kunSlettede.tooltip"));

        lblType = new JLabel(Resources.getResource("label.typeRskjema"));
        lblElrapp = new JLabel(Resources.getResource("label.elrappStatus"));

        comboType = new JComboBox();
        List<ElrappTypeR> typeList = new ArrayList<ElrappTypeR>();
        typeList.add(new ElrappTypeR(ElrappTypeR.Type.R2));
        typeList.add(new ElrappTypeR(ElrappTypeR.Type.R5));
        typeList.add(new ElrappTypeR(ElrappTypeR.Type.R11));
        MipssComboBoxModel<ElrappTypeR> mdlType = new MipssComboBoxModel<ElrappTypeR>(typeList, true);
        comboType.setModel(mdlType);
        comboType.setRenderer(new ElrappTypeComboBoxRenderer(true));


        comboElrapp = new JComboBox();
        List<ElrappStatus> statusList = new ArrayList<ElrappStatus>();
        statusList.add(new ElrappStatus(ElrappStatus.Status.IKKE_SENDT));
        statusList.add(new ElrappStatus(ElrappStatus.Status.ENDRET));
        statusList.add(new ElrappStatus(ElrappStatus.Status.SENDT));
        MipssComboBoxModel<ElrappStatus> mdlStatus = new MipssComboBoxModel<ElrappStatus>(statusList, true);
        comboElrapp.setModel(mdlStatus);
        comboElrapp.setRenderer(new ElrappStatusComboBoxRenderer(true));
    }

    public void setSelectedType(ElrappTypeR type) {
        if (type != null)
            controller.getQueryParams().setTypeR(type.getType());
        else {
            controller.getQueryParams().setTypeR(null);
        }
    }

    public void setSelectedElrapp(ElrappStatus status) {
        if (status != null) {
            controller.getQueryParams().setElrappStatus(status.getStatus());
        } else {
            controller.getQueryParams().setElrappStatus(null);
        }
    }

    private void bind() {
        Binding<JComboBox, Object, SokekriterierRskjemaPanel, Object> typeBind = BindingHelper.createbinding(comboType, "selectedItem", this, "selectedType");
        Binding<JComboBox, Object, SokekriterierRskjemaPanel, Object> statusBind = BindingHelper.createbinding(comboElrapp, "selectedItem", this, "selectedElrapp");
        Binding<JCheckBox, Object, RskjemaQueryParams, Object> kunSlettetBind = BindingHelper.createbinding(chkKunSlettede, "selected", controller.getQueryParams(), "kunSlettede");

        controller.getFeltController().getBindingGroup().addBinding(typeBind);
        controller.getFeltController().getBindingGroup().addBinding(statusBind);
        controller.getFeltController().getBindingGroup().addBinding(kunSlettetBind);
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        add(lblType, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(lblElrapp, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));

        add(comboType, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
        add(comboElrapp, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

        add(chkKunSlettede, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    }
}
