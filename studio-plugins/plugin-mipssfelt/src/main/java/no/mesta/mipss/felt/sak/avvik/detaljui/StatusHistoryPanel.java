package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings({"unchecked", "serial"})
public class StatusHistoryPanel extends JPanel implements LoadingPanel {

    private final AvvikController controller;
    private JMipssBeanTable<AvvikStatus> avvikStatusTable;
    private JMipssBeanScrollPane scrTable;
    private JButton btnSlett;
    private JButton btnLeggTil;
    private JButton btnLukkAvvik;
    private JButton btnGjenapne;

    private Avvik avvik;
    private final AvvikDetaljPanel parentPanel;

    public StatusHistoryPanel(AvvikController controller, AvvikDetaljPanel parentPanel) {
        this.controller = controller;
        this.parentPanel = parentPanel;
        initComponents();
        initGui();

        Dimension dim = getPreferredSize();
        dim.setSize(dim.getWidth(), 100);
        setPreferredSize(dim);
    }

    public AvvikDetaljPanel getDetaljPanel() {
        return parentPanel;
    }

    public void setAvvik(Avvik avvik) {
        this.avvik = avvik;

        MipssBeanTableModel<AvvikStatus> model = (MipssBeanTableModel<AvvikStatus>) avvikStatusTable.getModel();
        model.setSourceObject(avvik);
    }

    public Avvik getAvvik() {
        return this.avvik;
    }

    public void removeSelection() {
        avvikStatusTable.clearSelection();
    }

    private void initComponents() {
        avvikStatusTable = controller.createAvvikStatusTable();

        btnSlett = new JButton(controller.getSlettAvvikStatus(avvikStatusTable, this));
        btnLeggTil = new JButton(controller.getLeggTilAvvikStatus(this));
        scrTable = new JMipssBeanScrollPane(avvikStatusTable, (MipssBeanTableModel<AvvikStatus>) avvikStatusTable.getModel());

        btnLukkAvvik = new JButton(controller.getLukkAvvikAction(this, parentPanel));
        btnGjenapne = new JButton(controller.getGjenapneAvvikAction(this));
        btnGjenapne.setVisible(false);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.statushistorikk")));

        JPanel pnlButton = new JPanel(new GridBagLayout());
        pnlButton.add(btnLukkAvvik, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        pnlButton.add(btnGjenapne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        pnlButton.add(btnSlett, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnLeggTil, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(scrTable, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 3, 3), 0, 0));

    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
    }

    public void setLukket(boolean lukketAvvik) {
        btnLukkAvvik.setVisible(!lukketAvvik);
        btnGjenapne.setVisible(lukketAvvik);
        btnSlett.setEnabled(!lukketAvvik);
        btnLeggTil.setEnabled(!lukketAvvik);
    }
}
