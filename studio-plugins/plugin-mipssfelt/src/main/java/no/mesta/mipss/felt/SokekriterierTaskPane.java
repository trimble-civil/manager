package no.mesta.mipss.felt;

import no.mesta.mipss.ui.taskpane.LinkLabel;
import no.mesta.mipss.ui.taskpane.MipssTaskPane;
import no.mesta.mipss.ui.taskpane.MipssTaskPaneContainer;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class SokekriterierTaskPane extends MipssTaskPaneContainer {
    private MipssTaskPane paneSearch;
    private JPanel pnlSokekriterier;

    public SokekriterierTaskPane(JPanel pnlSokekriterier) {
        this.pnlSokekriterier = pnlSokekriterier;
        initComponents();
    }

    private void initComponents() {
        paneSearch = new MipssTaskPane();
        paneSearch.setTitleLeftComponent(new JLabel(Resources.getResource("label.avansertSok")));
        paneSearch.setContent(pnlSokekriterier);

        paneSearch.setTitleContent(createSearchPaneTitle());

        addTaskPane(paneSearch);
        paneSearch.setCollapsed(true);

    }

    private JPanel createSearchPaneTitle() {
        final LinkLabel label = new LinkLabel(Resources.getResource("label.visSokekriterier"));
        Action labelAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (paneSearch.isCollapsed()) {
                    paneSearch.setCollapsed(false);
                    label.setText(Resources.getResource("label.skjulSokekriterier"));
                } else {
                    paneSearch.setCollapsed(true);
                    label.setText(Resources.getResource("label.visSokekriterier"));
                }
            }
        };
        label.setAction(labelAction);
        JPanel titleContent = new JPanel();
        titleContent.setLayout(new BoxLayout(titleContent, BoxLayout.LINE_AXIS));
        titleContent.add(Box.createHorizontalStrut(30));
        titleContent.add(label);
        return titleContent;
    }
}
