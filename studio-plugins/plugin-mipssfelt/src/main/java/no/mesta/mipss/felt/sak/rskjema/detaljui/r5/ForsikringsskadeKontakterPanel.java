package no.mesta.mipss.felt.sak.rskjema.detaljui.r5;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.UIUtils;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.jxlayer.JXLayer;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ForsikringsskadeKontakterPanel extends JPanel {
    private BindingGroup bg = new BindingGroup();

    private JCheckBox vitnerJa;
    private JFormattedTextField vitnerHvemField;
    private JMipssDatePicker vitnerDato;
    private JFormattedTextField politiField;
    private JFormattedTextField politiTlfField;
    private JMipssDatePicker politiDato;
    private JFormattedTextField politiResultatField;
    private JFormattedTextField bergingNavnField;
    private JFormattedTextField bergingTlfField;
    private JFormattedTextField bergingKontakt1NavnField;
    private JFormattedTextField bergingKontakt1TlfField;
    private JFormattedTextField bergingKontakt1ResultatField;
    private JFormattedTextField berging2NavnField;
    private JFormattedTextField berging2TlfField;
    private JFormattedTextField bergingKontakt2NavnField;
    private JFormattedTextField bergingKontakt2ResultatField;
    private JFormattedTextField bergingKontakt2TlfField;
    private JRadioButton politiJa;
    private JRadioButton politiNei;
    private JFormattedTextField tilleggField;

    private Forsikringsskade forsikringsskade;
    private final GridBagConstraints c;

    @SuppressWarnings("unused")
    private final R5Panel r5;

    public ForsikringsskadeKontakterPanel(GridBagConstraints c, R5Panel r5) {
        this.c = c;
        this.r5 = r5;
        initGui();
    }

    public void setForsikringsskade(Forsikringsskade forsikringsskade) {
        this.forsikringsskade = forsikringsskade;
        bind();
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        vitnerJa = new JCheckBox(Resources.getResource("label.ja"));
        vitnerHvemField = new JFormattedTextField();
        vitnerDato = new JMipssDatePicker();
        vitnerDato.setDate(null);

        politiField = new JFormattedTextField();
        politiTlfField = new JFormattedTextField();
        politiTlfField.setDocument(new DocumentFilter(20));

        politiDato = new JMipssDatePicker();
        politiDato.setDate(null);

        politiJa = new JRadioButton(Resources.getResource("label.ja"));
        politiNei = new JRadioButton(Resources.getResource("label.nei"));
        politiNei.setSelected(true);
        ButtonGroup pGroup = new ButtonGroup();
        pGroup.add(politiJa);
        pGroup.add(politiNei);

        politiResultatField = new JFormattedTextField();
        bergingNavnField = new JFormattedTextField();
        bergingTlfField = new JFormattedTextField();
        bergingTlfField.setDocument(new DocumentFilter(20));
        berging2TlfField = new JFormattedTextField();
        berging2TlfField.setDocument(new DocumentFilter(20));

        bergingKontakt1NavnField = new JFormattedTextField();
        bergingKontakt1TlfField = new JFormattedTextField();
        bergingKontakt1TlfField.setDocument(new DocumentFilter(20));
        bergingKontakt1ResultatField = new JFormattedTextField();

        berging2NavnField = new JFormattedTextField();
        bergingKontakt2NavnField = new JFormattedTextField();
        bergingKontakt2TlfField = new JFormattedTextField();
        bergingKontakt2TlfField.setDocument(new DocumentFilter(20));
        bergingKontakt2ResultatField = new JFormattedTextField();

        tilleggField = new JFormattedTextField();
        createKontakterPanel();
    }

    public void setVitnerChecked(boolean checked) {
        if (!checked) {
            forsikringsskade.setVitneDato(null);
            forsikringsskade.setVitneNavn(null);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void bind() {

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "${vitneNavn!=null}", this, "vitne"));

        Binding vitnerDatoEnable = BindingHelper.createbinding(vitnerJa, "selected", vitnerDato, "enabled");
        Binding vitnerEnable = BindingHelper.createbinding(vitnerJa, "selected", vitnerHvemField, "enabled");
        Binding vitnerChecked = BindingHelper.createbinding(vitnerJa, "selected", this, "vitnerChecked");

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "anmeldt", politiJa, "selected", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "${anmeldt==false}", politiNei, "selected"));

        LongStringConverter slc = new LongStringConverter();

        bg.addBinding(vitnerDatoEnable);
        bg.addBinding(vitnerEnable);
        bg.addBinding(vitnerChecked);

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "tilleggsopplysninger", tilleggField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "vitneNavn", vitnerHvemField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "vitneDato", vitnerDato, "date", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "politiKontaktNavn", politiField, "text", BindingHelper.READ_WRITE));
        Binding politiKontaktTlfBind = BindingHelper.createbinding(forsikringsskade, "politiKontaktTlf", politiTlfField, "text", BindingHelper.READ_WRITE);
        politiKontaktTlfBind.setConverter(slc);
        bg.addBinding(politiKontaktTlfBind);
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "politiKontaktResultat", politiResultatField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "politiKontaktDato", politiDato, "date", BindingHelper.READ_WRITE));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "navnBerg1", bergingNavnField, "text", BindingHelper.READ_WRITE));
        Binding tlfBerg1Bind = BindingHelper.createbinding(forsikringsskade, "tlfBerg1", bergingTlfField, "text", BindingHelper.READ_WRITE);
        tlfBerg1Bind.setConverter(slc);
        bg.addBinding(tlfBerg1Bind);
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "kontaktNavnBerg1", bergingKontakt1NavnField, "text", BindingHelper.READ_WRITE));
        Binding kontaktTlfBerg1Bind = BindingHelper.createbinding(forsikringsskade, "kontaktTlfBerg1", bergingKontakt1TlfField, "text", BindingHelper.READ_WRITE);
        kontaktTlfBerg1Bind.setConverter(slc);
        bg.addBinding(kontaktTlfBerg1Bind);
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "resultatBerg1", bergingKontakt1ResultatField, "text", BindingHelper.READ_WRITE));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "navnBerg2", berging2NavnField, "text", BindingHelper.READ_WRITE));
        Binding tlfBerg2Bind = BindingHelper.createbinding(forsikringsskade, "tlfBerg2", berging2TlfField, "text", BindingHelper.READ_WRITE);
        tlfBerg2Bind.setConverter(slc);
        bg.addBinding(tlfBerg2Bind);
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "kontaktNavnBerg2", bergingKontakt2NavnField, "text", BindingHelper.READ_WRITE));
        Binding kontaktTlfBerg2Bind = BindingHelper.createbinding(forsikringsskade, "kontaktTlfBerg2", bergingKontakt2TlfField, "text", BindingHelper.READ_WRITE);
        kontaktTlfBerg2Bind.setConverter(slc);
        bg.addBinding(kontaktTlfBerg2Bind);
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "resultatBerg2", bergingKontakt2ResultatField, "text", BindingHelper.READ_WRITE));
        bg.bind();
    }

    private void createKontakterPanel() {
        JLabel skadevolderKjentLabel = new JLabel(Resources.getResource("label.skadevolderKjent"));
        JLabel vitnerLabel = new JLabel(Resources.getResource("label.vitner"));
        JLabel vitnerHvemLabel = new JLabel(Resources.getResource("label.vitnerHvem"));
        JLabel vitnerDatoLabel = new JLabel(Resources.getResource("label.dato"));
        JLabel politiLabel = new JLabel(Resources.getResource("label.politi"));
        JLabel politiTlfLabel = new JLabel(Resources.getResource("label.politiTlf"));
        JLabel politiDatoLabel = new JLabel(Resources.getResource("label.dato"));
        JLabel politiResultatLabel = new JLabel(Resources.getResource("label.resultatPoliti"));
        JLabel bergingselskLabel = new JLabel(Resources.getResource("label.bergingselsk"));
        JLabel navnBergingLabel = new JLabel(Resources.getResource("label.navnBerging"));
        JLabel kontaktpersonLabel = new JLabel(Resources.getResource("label.kontaktPerson"));
        JLabel kontaktpersonTlfLabel = new JLabel(Resources.getResource("label.kontaktPersonTlf"));
        JLabel fire1Label = new JLabel(Resources.getResource("label.fire1"));
        JLabel fire2Label = new JLabel(Resources.getResource("label.fire2"));
        JLabel politiAnmeldLabel = new JLabel(Resources.getResource("label.anmeldtPoliti"));
        JLabel kopianmeldLabel = new JLabel(Resources.getResource("label.kopiAnmeld"));
        JLabel tilleggLabel = new JLabel(Resources.getResource("label.tillegg"));

        Font f = skadevolderKjentLabel.getFont();
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        skadevolderKjentLabel.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
        add(skadevolderKjentLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        JPanel vitnerPanel = UIUtils.getHorizPanel();
        vitnerPanel.add(vitnerLabel);
        vitnerPanel.add(UIUtils.getHStrut(5));
        vitnerPanel.add(vitnerJa);
        c.anchor = GridBagConstraints.SOUTH;
        add(vitnerPanel, c);
        c.anchor = GridBagConstraints.CENTER;
        c.gridx += 2;
        add(UIUtils.getTwoComponentPanelVert(vitnerHvemLabel, vitnerHvemField, false), c);
        c.gridx += 2;
        c.gridwidth = 1;
        add(UIUtils.getTwoComponentPanelVert(vitnerDatoLabel, vitnerDato, true), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 3;
        add(UIUtils.getTwoComponentPanelVert(politiLabel, politiField, false), c);

        c.gridx += 3;
        c.gridwidth = 1;
        add(UIUtils.getTwoComponentPanelVert(politiTlfLabel, politiTlfField, true), c);

        c.gridx += 1;
        c.gridwidth = 1;
        add(UIUtils.getTwoComponentPanelVert(politiDatoLabel, politiDato, true), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(UIUtils.getTwoComponentPanelVert(politiResultatLabel, politiResultatField, false), c);

        Insets i = c.insets;
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        c.insets = new Insets(10, 0, 10, 20);
        add(bergingselskLabel, c);

        c.insets = i;
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 3;
        add(navnBergingLabel, c);

        c.gridx += 3;
        c.gridwidth = 1;
        add(kontaktpersonLabel, c);
        c.gridx++;
        add(kontaktpersonTlfLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 3;
        JPanel fire1Pnl = UIUtils.getHorizPanel();
        fire1Pnl.add(fire1Label);
        fire1Pnl.add(UIUtils.getHStrut(5));
        fire1Pnl.add(decorateField(bergingNavnField, "validateBergingsselskap1"));
        fire1Pnl.add(bergingTlfField);
        Dimension bsize = new Dimension(75, 20);
        bergingTlfField.setPreferredSize(bsize);
        bergingTlfField.setSize(bsize);
        bergingTlfField.setMaximumSize(bsize);
        c.anchor = GridBagConstraints.NORTH;
        add(fire1Pnl, c);
        c.gridx += 3;

        c.gridwidth = 1;
        add(bergingKontakt1NavnField, c);
        c.gridx++;
        add(bergingKontakt1TlfField, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        JPanel resultat1Panel = UIUtils.getHorizPanel();
        resultat1Panel.add(new JLabel("Resultat:"));
        resultat1Panel.add(bergingKontakt1ResultatField);
        add(resultat1Panel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 3;
        JPanel fire2Pnl = UIUtils.getHorizPanel();
        fire2Pnl.add(fire2Label);
        fire2Pnl.add(UIUtils.getHStrut(5));
        fire2Pnl.add(decorateField(berging2NavnField, "validateBergingsselskap2"));
        fire2Pnl.add(berging2TlfField);
        berging2TlfField.setPreferredSize(bsize);
        berging2TlfField.setSize(bsize);
        berging2TlfField.setMaximumSize(bsize);

        add(fire2Pnl, c);
        c.gridx += 3;
        c.gridwidth = 1;
        add(bergingKontakt2NavnField, c);
        c.gridx++;
        add(bergingKontakt2TlfField, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        JPanel resultat2Panel = UIUtils.getHorizPanel();
        resultat2Panel.add(new JLabel("Resultat:"));
        resultat2Panel.add(bergingKontakt2ResultatField);
        add(resultat2Panel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        JPanel politiPanel = UIUtils.getHorizPanel();
        politiPanel.add(politiAnmeldLabel);
        politiPanel.add(UIUtils.getHStrut(5));
        politiPanel.add(politiJa);
        politiPanel.add(politiNei);
        politiPanel.add(UIUtils.getHStrut(5));
        politiPanel.add(kopianmeldLabel);
        add(politiPanel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(UIUtils.getTwoComponentPanelVert(tilleggLabel, tilleggField, false), c);

        c.gridx = 0;
        c.gridy++;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        add(new JPanel(), c);
    }

    private JXLayer<JComponent> decorateField(JComponent c, String validationMethod) {
        JXLayer<JComponent> layer = new JXLayer<JComponent>(c);
        layer.setUI(new ValidationLayerUI(validationMethod, this));
        return layer;
    }

    public boolean isVitnerJaSelected() {
        return vitnerJa.isSelected();
    }

    public boolean isPolitiJaSelected() {
        return politiJa.isSelected();
    }

    public void setVitne(boolean value) {
        vitnerJa.setSelected(value);
    }

    public void setPolitiJa(boolean value) {
        politiJa.setSelected(value);
    }

    public void setPolitiNei(boolean value) {
        politiNei.setSelected(value);
    }

    public boolean isValidFields() {
        boolean valid = true;
        if (!validateVitne()) {
            valid = false;
        }
        if (!validatePoliti()) {
            valid = false;
        }
        if (!validateBergingsselskap1()) {
            valid = false;
        }
        if (!validateBergingsselskap2()) {
            valid = false;
        }
        return valid;
    }

    public boolean validateBergingsselskap1() {
        String navn = bergingNavnField.getText();
        String kontakt = bergingKontakt1NavnField.getText();
        String tlfBerg = bergingTlfField.getText();
        String tlf = bergingKontakt1TlfField.getText();
        String resultat = bergingKontakt1ResultatField.getText();
        if (navn.equals("") && kontakt.equals("") && tlf.equals("") && resultat.equals("") && tlfBerg.equals("")) {
            return true;
        }
        if (navn.equals("")) {//navn kan ikke være "" når vi andre andre data på beringsselskapene
            return false;
        }
        return true;
    }


    public boolean validateBergingsselskap2() {
        String navn = berging2NavnField.getText();
        String kontakt = bergingKontakt2NavnField.getText();
        String tlfBerg = berging2TlfField.getText();
        String tlf = bergingKontakt2TlfField.getText();
        String resultat = bergingKontakt2ResultatField.getText();
        if (navn.equals("") && kontakt.equals("") && tlf.equals("") && resultat.equals("") && tlfBerg.equals("")) {
            return true;
        }
        if (navn.equals("")) {//navn kan ikke være "" når vi andre andre data på beringsselskapene
            return false;
        }
        return true;
    }

    public boolean validatePoliti() {
        String politi = politiField.getText();
        String resultat = politiResultatField.getText();
        if (StringUtils.isBlank(politi) && StringUtils.isBlank(resultat) && politiDato.getDate() == null) {
            return true;
        }
        if (StringUtils.isBlank(politi)) {
            return false;//politinavn kan ikke være "" når andre felter på politi er satt
        }
        return true;
    }

    public boolean validateVitne() {
        if (!vitnerJa.isSelected()) {
            return true;
        }

        String vitneNavn = vitnerHvemField.getText();
        if (vitneNavn.equals("")) {//navn kan ikke være "" når vi har en dato
            return false;
        }
        return true;
    }

}
