package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IntegerStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class AvvikButtonPanel extends JPanel {

    private final AvvikController controller;
    private JButton btnNyttAvvik;
    private JButton btnSlettAvvik;
    private JButton btnExcel;
    private JButton btnKart;
    private JPopupListMenu btnRapport;
    private JButton btnDetaljer;
    private JSeparator separator;
    private JMenuItem rapportList;
    private JMenuItem rapportDetalj;

    private JLabel lblAntallTreff;
    private JLabel lblTreffValue;

    public AvvikButtonPanel(AvvikController controller) {
        this.controller = controller;
        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        btnNyttAvvik = new JButton(controller.getAvvikNyAction());
        btnSlettAvvik = new JButton(controller.getAvvikSlettAction(this));
        btnExcel = new JButton(controller.getAvvikExcelAction());
        btnKart = new JButton(controller.getAvvikKartAction());

        List<JMenuItem> popupList = new ArrayList<JMenuItem>();
        rapportList = new JMenuItem(controller.getAvvikListRapportAction());
        rapportDetalj = new JMenuItem(controller.getAvvikRapportAction());
        popupList.add(rapportList);
        popupList.add(rapportDetalj);

        btnRapport = new JPopupListMenu(Resources.getResource("button.rapport"), IconResources.ADOBE_16, popupList);

        btnDetaljer = new JButton(controller.getAvvikDetaljerAction());
        separator = new JSeparator(SwingConstants.VERTICAL);
        btnExcel.setToolTipText(Resources.getResource("button.excel.tooltip"));
        lblAntallTreff = new JLabel(Resources.getResource("label.antallTreff"));
        lblTreffValue = new JLabel();

        //TODO implementer
        btnSlettAvvik.setEnabled(false);
        btnExcel.setEnabled(false);
        btnKart.setEnabled(false);
        btnRapport.setEnabled(false);
        btnDetaljer.setEnabled(false);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void bind() {
        BindingHelper.createbinding(controller.getAvvikTableModel(), "${size>0}", btnExcel, "enabled").bind();
//		BindingHelper.createbinding(controller.getAvvikTable(), "${noOfSelectedRows==1}", rapportDetalj, "enabled").bind();
        BindingHelper.createbinding(controller.getAvvikTableModel(), "${size>0}", btnRapport, "enabled").bind();
        BindingHelper.createbinding(controller.getAvvikTable(), "${noOfSelectedRows==1}", btnDetaljer, "enabled").bind();
        BindingHelper.createbinding(controller.getAvvikTable(), "${noOfSelectedRows>0}", btnKart, "enabled").bind();
        BindingHelper.createbinding(controller.getAvvikTable(), "${noOfSelectedRows<4&&noOfSelectedRows>0}", btnSlettAvvik, "enabled").bind();
        Binding antallBind = BindingHelper.createbinding(controller.getAvvikTableModel(), "size", lblTreffValue, "text");
        IntegerStringConverter isc = new IntegerStringConverter();
        antallBind.setConverter(isc);
        antallBind.bind();

    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnNyttAvvik, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnSlettAvvik, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
        add(separator, new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 10), 0, 0));
        add(btnExcel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnRapport, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnKart, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnDetaljer, new GridBagConstraints(6, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(lblAntallTreff, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(lblTreffValue, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

    }
}
