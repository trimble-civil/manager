package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IntegerStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class RskjemaButtonPanel extends JPanel {

    private final RskjemaController controller;
    private JButton btnNyttRskjema;
    private JButton btnSlettRskjema;
    private JButton btnSendElrapp;
    private JButton btnExcel;
    private JButton btnKart;
    private JButton btnRapport;
    private JButton btnDetaljer;
    private JSeparator separator;
    private BindingGroup bindings = new BindingGroup();
    private JLabel lblAntallTreff;
    private JLabel lblTreffValue;

    public RskjemaButtonPanel(RskjemaController controller) {
        this.controller = controller;
        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        btnNyttRskjema = new JButton(controller.getRskjemaNyAction());
        btnNyttRskjema.setToolTipText(Resources.getResource("button.nyttRskjema.title"));
        btnSlettRskjema = new JButton(controller.getRskjemaSlettAction());
        btnSendElrapp = new JButton(controller.getRskjemaSendElrappAction());
        btnExcel = new JButton(controller.getRskjemaExcelAction());
        btnKart = new JButton(controller.getRskjemaKartAction());
        btnRapport = new JButton(controller.getRskjemaRapportAction());
        btnDetaljer = new JButton(controller.getRskjemaDetaljerAction());
        separator = new JSeparator(SwingConstants.VERTICAL);

        btnExcel.setToolTipText(Resources.getResource("button.excel.tooltip"));
        lblAntallTreff = new JLabel(Resources.getResource("label.antallTreff"));
        lblTreffValue = new JLabel();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void bind() {
        JMipssBeanTable<RskjemaSokResult> rskjemaTable = controller.getRskjemaTable();
        MipssBeanTableModel<RskjemaSokResult> rskjemaTableModel = controller.getRskjemaTableModel();
        Binding<JMipssBeanTable<RskjemaSokResult>, Object, JButton, Object> detaljerBind = BindingHelper.createbinding(rskjemaTable, "${noOfSelectedRows == 1}", btnDetaljer, "enabled");
        Binding<JMipssBeanTable<RskjemaSokResult>, Object, JButton, Object> kartBind = BindingHelper.createbinding(rskjemaTable, "${noOfSelectedRows > 0}", btnKart, "enabled");
        Binding<JMipssBeanTable<RskjemaSokResult>, Object, JButton, Object> elrappBind = BindingHelper.createbinding(rskjemaTable, "${noOfSelectedRows == 1}", btnSendElrapp, "enabled");
        Binding<JMipssBeanTable<RskjemaSokResult>, Object, JButton, Object> rapportBind = BindingHelper.createbinding(rskjemaTable, "${noOfSelectedRows == 1}", btnRapport, "enabled");

        Binding<MipssBeanTableModel<RskjemaSokResult>, Object, JButton, Object> excelBind = BindingHelper.createbinding(rskjemaTableModel, "${size > 0}", btnExcel, "enabled");

        Binding<JMipssBeanTable<RskjemaSokResult>, Object, JButton, Object> slettBind = BindingHelper.createbinding(rskjemaTable, "${noOfSelectedRows > 0 && noOfSelectedRows < 4}", btnSlettRskjema, "enabled");

        Binding antallBind = BindingHelper.createbinding(controller.getRskjemaTableModel(), "size", lblTreffValue, "text");
        IntegerStringConverter isc = new IntegerStringConverter();
        antallBind.setConverter(isc);


        bindings.addBinding(detaljerBind);
        bindings.addBinding(kartBind);
        bindings.addBinding(elrappBind);
        bindings.addBinding(excelBind);
        bindings.addBinding(slettBind);
        bindings.addBinding(rapportBind);
        bindings.addBinding(antallBind);
        bindings.bind();
    }


    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnNyttRskjema, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnSlettRskjema, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
        add(separator, new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
        add(btnSendElrapp, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnExcel, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnRapport, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnKart, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnDetaljer, new GridBagConstraints(7, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(lblAntallTreff, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(lblTreffValue, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

    }
}
