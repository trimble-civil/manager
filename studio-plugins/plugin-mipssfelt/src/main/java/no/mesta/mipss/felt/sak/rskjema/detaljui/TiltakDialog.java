package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IBlankFieldValidator;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.HendelseTrafikktiltak;
import no.mesta.mipss.persistence.mipssfield.Trafikktiltak;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JNoBlankLabel;
import no.mesta.mipss.ui.MipssListCellRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Dialog for å legge til estimat. Ikke gjenbrukbar
 *
 * @author <a href="mailto:arnljot.arntsen@avenir.no">Arnljot Arntsen</a>
 */
@SuppressWarnings("serial")
public class TiltakDialog extends JDialog {
    private String beskrivelse;
    private JTextArea beskrivelseField;
    private boolean cancelled = true;
    private JComboBox comboBox;
    private Trafikktiltak type;
    private BindingGroup group = new BindingGroup();
    private final Logger logger = LoggerFactory.getLogger(TiltakDialog.class);
    private boolean missingType;
    private final Hendelse hendelse;
    private JLabel validation;

    public TiltakDialog(final JDialog owner, final Hendelse hendelse, final List<Trafikktiltak> typer) {
        super(owner, true);

        this.hendelse = hendelse;

        setTitle(Resources.getResource("label.tiltak"));

        comboBox = new JComboBox();
        comboBox.setRenderer(new MipssListCellRenderer<Trafikktiltak>());
        JComboBoxBinding<Trafikktiltak, List<Trafikktiltak>, JComboBox> cb = SwingBindings
                .createJComboBoxBinding(AutoBinding.UpdateStrategy.READ_WRITE, typer, comboBox);
        cb.bind();

        validation = new JLabel((String) null) {
            @Override
            public void setText(String txt) {
                if (txt == null) {
                    setIcon(null);
                } else {
                    setIcon(IconResources.ERROR_ICON_SMALL);
                }
                super.setText(txt);
            }
        };
        validation.setForeground(Color.RED);
        Box validationBox = Box.createHorizontalBox();
        validationBox.add(validation);
        validationBox.add(Box.createHorizontalGlue());

        comboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                Trafikktiltak t = (Trafikktiltak) cb.getSelectedItem();
                if (t != null && missingType) {
                    validation.setText(null);
                    pack();
                    missingType = false;
                }
            }
        });

        beskrivelseField = new JTextArea();
        beskrivelseField.setLineWrap(true);
        beskrivelseField.setWrapStyleWord(true);
        JScrollPane beskrivelseScroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        beskrivelseScroll.setViewportView(beskrivelseField);
        ComponentSizeResources.setComponentSize(comboBox, new Dimension(280, 22));
        ComponentSizeResources.setComponentSize(beskrivelseScroll, new Dimension(218, 86));

        Box inputBox = Box.createVerticalBox();
        inputBox.add(BoxUtil.createVerticalStrut(2));
        inputBox.add(new LineBox<String>(Resources.getResource("label.kommentar"), beskrivelseScroll, null));

        JButton cancel = new JButton(new AbstractAction(Resources.getResource("button.avbryt")) {
            public void actionPerformed(ActionEvent e) {
                close(true);
            }
        });
        cancel.setMargin(new Insets(1, 1, 1, 1));
        ComponentSizeResources.setComponentSize(cancel, new Dimension(45, 22));

        JButton add = new JButton(new AbstractAction(Resources.getResource("button.leggTil")) {
            public void actionPerformed(ActionEvent e) {
                Trafikktiltak t = getTrafikktiltakType();

                boolean errors = false;
                if (t == null) {
                    missingType = true;
                    validation.setText(Resources.getResource("message.velgTiltakType"));
                    errors = true;
                }

                if (errors) {
                    // Feilmelding er skrevet ut, resize vindu slik at alt
                    // vises
                    pack();
                } else {
                    close(false);
                }
            }
        });
        add.setMargin(new Insets(1, 1, 1, 1));
        ComponentSizeResources.setComponentSize(add, new Dimension(45, 22));

        Box buttons = Box.createHorizontalBox();
        buttons.add(Box.createHorizontalGlue());
        buttons.add(cancel);
        buttons.add(Box.createHorizontalStrut(2));
        buttons.add(add);

        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        add(BoxUtil.createVerticalStrut(2));
        add(BoxUtil.createHorizontalBox(2, comboBox));
        add(Box.createVerticalStrut(2));
        add(validationBox);
        add(Box.createVerticalStrut(2));
        add(BoxUtil.createHorizontalBox(2, inputBox));
        add(Box.createVerticalStrut(2));
        add(BoxUtil.createHorizontalBox(2, buttons));
        add(Box.createVerticalStrut(2));

        ComponentSizeResources.setComponentSize(this, new Dimension(290, 170));
        setResizable(false);
        bindGroup();
    }

    /**
     * Lukker dialogen
     *
     * @param f
     */
    private void close(boolean f) {
        cancelled = f;
        setVisible(false);
    }


    public void doDialogue(JComponent center) {
        missingType = false;
        cancelled = true;

        pack();
        setLocationRelativeTo((Component) center);
        setVisible(true);
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public Trafikktiltak getTrafikktiltakType() {
        return type;
    }

    public HendelseTrafikktiltak getNewValue() {
        HendelseTrafikktiltak tt = new HendelseTrafikktiltak();
        tt.setHendelse(hendelse);
        tt.setTrafikktiltak(getTrafikktiltakType());
        tt.setBeskrivelse(getBeskrivelse());

        unbindGroup();
        return tt;
    }

    public Hendelse getHendelse() {
        return hendelse;
    }

    /**
     * Lager newEstimate objektet og setter det opp
     */
    private void bindGroup() {
        logger.debug("bindGroup() start");

        Binding<TiltakDialog, String, JTextArea, String> commentBinding = BindingHelper.createbinding(this,
                "beskrivelse", beskrivelseField, "text", BindingHelper.READ_WRITE);

        // knytter type dropdown til objektet
        Binding<TiltakDialog, Trafikktiltak, JComboBox, Trafikktiltak> typeBinding = BindingHelper
                .createbinding(this, "type", comboBox, "selectedItem", BindingHelper.READ_WRITE);

        group.addBinding(typeBinding);
        group.addBinding(commentBinding);
        group.bind();
        logger.debug("bindGroup() done");
    }

    public boolean isCancelled() {
        return cancelled;
    }


    public void setBeskrivelse(String beskrivelse) {
        String old = this.beskrivelse;
        this.beskrivelse = beskrivelse;

        if (beskrivelse != null && beskrivelse.length() > 255) {
            this.beskrivelse = old;
            beskrivelseField.setText(old);
        }

        firePropertyChange("beskrivelse", old, beskrivelse);
    }

    public void setType(Trafikktiltak type) {
        Trafikktiltak old = this.type;
        this.type = type;
        firePropertyChange("type", old, type);
    }

    /**
     *
     */
    private void unbindGroup() {
        logger.debug("unbindGroup()");
        group.unbind();
        for (Binding<?, ?, ?, ?> b : group.getBindings()) {
            group.removeBinding(b);
        }
    }

    static class LineBox<T> extends Box {
        private static int STRUT = 2;
        private JComponent component;
        private JLabel label;
        private String labelTxt;

        public LineBox(String labelTxt, JComponent component, IBlankFieldValidator<T> validator) {
            super(BoxLayout.X_AXIS);
            if (validator == null) {
                label = new JLabel(labelTxt);
            } else {
                label = new JNoBlankLabel<T>(labelTxt, validator);
            }

            this.labelTxt = labelTxt;
            this.component = component;
            layoutComponents();
        }

        @SuppressWarnings("unchecked")
        public IBlankFieldValidator<T> getValidator() {
            if (label instanceof JNoBlankLabel) {
                return ((JNoBlankLabel<T>) label).getValidator();
            } else {
                return null;
            }
        }

        private void layoutComponents() {
            ComponentSizeResources.setComponentSizes(component);
            label.setHorizontalAlignment(JLabel.RIGHT);
            label.setHorizontalTextPosition(JLabel.RIGHT);
            label.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
            ComponentSizeResources.setComponentSize(label, new Dimension(66, 20));

            add(label);
            add(BoxUtil.createHorizontalStrut(STRUT));
            add(component);

            Dimension min = new Dimension(59 + STRUT + component.getMinimumSize().width, component.getMinimumSize().height);
            int prefWidth = Math.max(Short.MAX_VALUE, 59 + STRUT + component.getPreferredSize().width);
            Dimension pref = new Dimension(prefWidth, component.getPreferredSize().height);
            int maxWidth = Math.max(Short.MAX_VALUE, 59 + STRUT + component.getMaximumSize().width);
            Dimension max = new Dimension(maxWidth, component.getMaximumSize().height);
            ComponentSizeResources.setComponentSizes(this, min, pref, max);
        }

        public void setValidator(IBlankFieldValidator<T> validator) {
            label = new JNoBlankLabel<T>(labelTxt, validator);
            removeAll();
            layoutComponents();
        }
    }
}
