package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.ui.table.TableHelper;
import org.jdesktop.swingx.decorator.ColorHighlighter;

import java.awt.*;

public class ColorHighlighterFactory {
    private Color warningCellBackground = new Color(255, 255, 200);
    private Color overskredetCellBackground = Color.pink;

    private Color lukketMangelBackground = new Color(230, 230, 230);
    private Color ikkeMangelForeground = new Color(80, 80, 80);

    public ColorHighlighter getSakUtenAvvikHighlighter(SakController controller) {
        return new ColorHighlighter(new SakUtenAvvikHighlighter(controller),
                TableHelper.getCellBackground(),
                new Color(255, 0, 0),
                TableHelper.getSelectedBackground(),
                new Color(255, 0, 0));
    }

    public ColorHighlighter getFristdatoWarningHighlighter(SakController controller) {
        return new ColorHighlighter(new FristdatoSakHighlighter(controller),
                warningCellBackground,
                TableHelper.getCellForeground(),
                TableHelper.getSelectedBackground(),
                TableHelper.getSelectedForeground());
    }

    public ColorHighlighter getFristdatoOverskredetHighlighter(SakController controller) {
        return new ColorHighlighter(new FristdatoOverskredetSakHighlighter(controller),
                overskredetCellBackground,
                TableHelper.getCellForeground(),
                TableHelper.getSelectedBackground(),
                TableHelper.getSelectedForeground());
    }

    public ColorHighlighter getFristdatoWarningHighlighter(AvvikController controller) {
        return new ColorHighlighter(new FristdatoAvvikHighlighter(controller),
                warningCellBackground,
                TableHelper.getCellForeground(),
                TableHelper.getSelectedBackground(),
                TableHelper.getSelectedForeground());
    }

    public ColorHighlighter getFristdatoOverskredetHighlighter(AvvikController controller) {
        return new ColorHighlighter(new FristdatoOverskredetAvvikHighlighter(controller),
                overskredetCellBackground,
                TableHelper.getCellForeground(),
                TableHelper.getSelectedBackground(),
                TableHelper.getSelectedForeground());
    }

    public ColorHighlighter getLukketMangelHighlighter(AvvikController controller) {
        return new ColorHighlighter(new LukketMangelHighlighter(controller),
                lukketMangelBackground,
                TableHelper.getCellForeground(),
                TableHelper.getSelectedBackground(),
                TableHelper.getSelectedForeground());
    }

    public ColorHighlighter getAvvikSomIkkeErManglerHighlighter(AvvikController controller) {
        return new ColorHighlighter(new AvvikSomIkkeErManglerHighlighter(controller),
                TableHelper.getCellBackground(),
                ikkeMangelForeground,
                TableHelper.getSelectedBackground(),
                TableHelper.getSelectedForeground());
    }
}
