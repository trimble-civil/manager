package no.mesta.mipss.felt.sak;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IntegerStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.SakSokResult;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class SakButtonPanel extends JPanel {

    private final SakController controller;
    private JButton btnNySak;
    private JButton btnSlettSak;
    private JButton btnExcel;
    private JButton btnDetaljer;
    private JSeparator separator;
    private final SakPanel panel;


    private JMenuItem rapportAvvik;
    private JMenuItem rapportR2;
    private JMenuItem rapportR5;
    private JMenuItem rapportR11;
    private JPopupListMenu btnRapport;

    private JLabel lblAntallTreff;
    private JLabel lblTreffValue;

    public SakButtonPanel(SakController controller, SakPanel panel) {
        this.controller = controller;
        this.panel = panel;
        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        btnNySak = new JButton(controller.getSakNyAction());
        btnSlettSak = new JButton(controller.getSakSlettAction(panel));
        btnExcel = new JButton(controller.getSakExcelAction());
        btnDetaljer = new JButton(controller.getSakDetaljerAction());
        separator = new JSeparator(SwingConstants.VERTICAL);

        List<JMenuItem> popupList = new ArrayList<JMenuItem>();
        rapportAvvik = new JMenuItem(controller.getSakAvvikRapportAction());
        rapportR2 = new JMenuItem(controller.getSakRskjemaRapportAction(ElrappTypeR.Type.R2));
        rapportR5 = new JMenuItem(controller.getSakRskjemaRapportAction(ElrappTypeR.Type.R5));
        rapportR11 = new JMenuItem(controller.getSakRskjemaRapportAction(ElrappTypeR.Type.R11));
        popupList.add(rapportAvvik);
        popupList.add(rapportR2);
        popupList.add(rapportR5);
        popupList.add(rapportR11);
        btnRapport = new JPopupListMenu(Resources.getResource("button.rapport"), IconResources.ADOBE_16, popupList);
        btnExcel.setToolTipText(Resources.getResource("button.excel.tooltip"));
        lblAntallTreff = new JLabel(Resources.getResource("label.antallTreff"));
        lblTreffValue = new JLabel();

        btnSlettSak.setEnabled(false);
        btnExcel.setEnabled(false);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void bind() {
        BindingHelper.createbinding(controller.getSakTable(), "selectedEntities", this, "selectedRows").bind();
        BindingHelper.createbinding(controller.getSakTable(), "selectedEntities", this, "slettText").bind();
        BindingHelper.createbinding(controller.getSakTableModel(), "${size>0}", btnExcel, "enabled").bind();
        BindingHelper.createbinding(controller.getSakTable(), "${noOfSelectedRows==1}", btnRapport, "enabled").bind();
        BindingHelper.createbinding(controller.getSakTable(), "${noOfSelectedRows==1}", btnDetaljer, "enabled").bind();
        BindingHelper.createbinding(controller.getSakTable(), "selectedEntities", this, "avvikRapportAktiv").bind();
        BindingHelper.createbinding(controller.getSakTable(), "selectedEntities", this, "r2RapportAktiv").bind();
        BindingHelper.createbinding(controller.getSakTable(), "selectedEntities", this, "r5RapportAktiv").bind();
        BindingHelper.createbinding(controller.getSakTable(), "selectedEntities", this, "r11RapportAktiv").bind();
        Binding antallBind = BindingHelper.createbinding(controller.getSakTableModel(), "size", lblTreffValue, "text");
        IntegerStringConverter isc = new IntegerStringConverter();
        antallBind.setConverter(isc);
        antallBind.bind();
    }

    public void setAvvikRapportAktiv(List<SakSokResult> selected) {
        if (selected.size() == 1) {
            SakSokResult s = selected.get(0);
            if (s.getAntallAvvik() > 0)
                rapportAvvik.setEnabled(true);
            else
                rapportAvvik.setEnabled(false);
        } else
            rapportAvvik.setEnabled(false);
    }

    public void setR2RapportAktiv(List<SakSokResult> selected) {
        if (selected.size() == 1) {
            SakSokResult s = selected.get(0);
            if (s.getR2())
                rapportR2.setEnabled(true);
            else
                rapportR2.setEnabled(false);
        } else
            rapportR2.setEnabled(false);
    }

    public void setR5RapportAktiv(List<SakSokResult> selected) {
        if (selected.size() == 1) {
            SakSokResult s = selected.get(0);
            if (s.getR5())
                rapportR5.setEnabled(true);
            else
                rapportR5.setEnabled(false);
        } else
            rapportR5.setEnabled(false);
    }

    public void setR11RapportAktiv(List<SakSokResult> selected) {
        if (selected.size() == 1) {
            SakSokResult s = selected.get(0);
            if (s.getR11())
                rapportR11.setEnabled(true);
            else
                rapportR11.setEnabled(false);
        } else
            rapportR11.setEnabled(false);
    }

    public void setSlettText(List<SakSokResult> selected) {
        if (selected == null || selected.size() == 0)
            return;
        if (selected.size() == 1) {
            btnSlettSak.setText(Resources.getResource("button.slettSak"));
        }
        if (selected.size() > 1) {
            btnSlettSak.setText(Resources.getResource("button.slettSaker"));
        }
    }

    public void setSelectedRows(List<SakSokResult> sakList) {
        boolean slettable = sakList == null || sakList.size() == 0 ? false : true;
        for (SakSokResult s : sakList) {
            if (!s.isSlettbar()) {
                slettable = false;
                break;
            }
        }
        btnSlettSak.setEnabled(slettable);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnNySak, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnSlettSak, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
        add(separator, new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 10), 0, 0));
        add(btnExcel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnRapport, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnDetaljer, new GridBagConstraints(5, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(lblAntallTreff, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(lblTreffValue, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
    }
}
