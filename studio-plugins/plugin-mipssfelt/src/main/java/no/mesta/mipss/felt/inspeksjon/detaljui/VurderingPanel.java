package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.Temperatur;
import no.mesta.mipss.persistence.mipssfield.Vaer;
import no.mesta.mipss.persistence.mipssfield.Vind;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class VurderingPanel extends JPanel implements LoadingPanel {

    private JLabel lblVaer;
    private JLabel lblVind;
    private JLabel lblTemp;

    private JFormattedTextField fldVaer;
    private JFormattedTextField fldVind;
    private JFormattedTextField fldTemp;

    private JCheckBox chkEtterslep;
    private Inspeksjon inspeksjon;
    private BindingGroup bindings = new BindingGroup();

    public VurderingPanel() {
        initComponents();
        initGui();
    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
        bind();

    }

    private void bind() {
        Binding<Vaer, Object, JFormattedTextField, Object> vaerBind = BindingHelper.createbinding(inspeksjon.getVaer(), "navn", fldVaer, "text");
        Binding<Vind, Object, JFormattedTextField, Object> vindBind = BindingHelper.createbinding(inspeksjon.getVind(), "navn", fldVind, "text");
        Binding<Temperatur, Object, JFormattedTextField, Object> tempBind = BindingHelper.createbinding(inspeksjon.getTemperatur(), "navn", fldTemp, "text");

        Binding<Inspeksjon, Object, JCheckBox, Object> etterslepBind = BindingHelper.createbinding(inspeksjon, "etterslep", chkEtterslep, "selected", UpdateStrategy.READ_WRITE);

        bindings.addBinding(vaerBind);
        bindings.addBinding(vindBind);
        bindings.addBinding(tempBind);
        bindings.addBinding(etterslepBind);

        bindings.bind();

    }

    private void initComponents() {
        lblVaer = new JLabel(Resources.getResource("label.vaer"));
        lblVind = new JLabel(Resources.getResource("label.vind"));
        lblTemp = new JLabel(Resources.getResource("label.temp"));

        fldVaer = new JFormattedTextField();
        fldVind = new JFormattedTextField();
        fldTemp = new JFormattedTextField();

        chkEtterslep = new JCheckBox(Resources.getResource("label.etterslep"));

        fldVaer.setEditable(false);
        fldVind.setEditable(false);
        fldTemp.setEditable(false);
    }

    private void initGui() {
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.vurdering")));
        setLayout(new GridBagLayout());

        add(lblVaer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldVaer, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(lblVind, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldVind, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(lblTemp, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldTemp, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(chkEtterslep, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }

    @Override
    public boolean isLoading() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();

    }

}
