package no.mesta.mipss.felt.elrapp;

import no.mesta.mipss.elrapp.ElrappValidationException;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class R2Validator extends ElrappValidator {

    private static final List<String> allowedCauses;

    static {
        // These values match the enumeration in elrapp_form_r2_2.xsd
        allowedCauses =
                Arrays.asList("Hærverk", "Trafikkskade", "NormalNedbrytning",
                "AkuttSkade", "Annet");
    }
    private final Hendelse hendelse;

    public R2Validator(Hendelse hendelse) {
        this.hendelse = hendelse;

    }

    public List<String> validate() throws ElrappValidationException {
        List<String> errMsg = new ArrayList<String>();

        if (hendelse.getSak().getStedfesting() == null) {
            errMsg.add(Resources.getResource("feilmelding.manglerStedfesting"));
        }
        if (hendelse.getBeskrivelse() == null) {
            errMsg.add(Resources.getResource("feilmelding.manglerBeskrivelse"));
        }
        if (StringUtils.isBlank(hendelse.getSkjemaEmne())) {
            errMsg.add(Resources.getResource("feilmelding.manglerSkjemaEmne"));
        }

        if (hendelse.getAarsak() != null) {
            String cause = hendelse.getAarsak().getElrappId();

            if (!allowedCauses.contains(cause)) {
                errMsg.add(Resources.getResource("feilmelding.ugyldigAarsak"));
            }
        }

        if (hendelse.getArbeidType() == null) {
            errMsg.add(Resources.getResource("feilmelding.manglerArbeidtype"));
        } else if (hendelse.getArbeidType().getElrappId().equals("Funksjonsansvar")
                && hendelse.getPlanlagtUtbedret() == null) {
            errMsg.add(Resources.getResource("feilmelding.manglerArbeidPlanlagtDato"));
        }
        if (errMsg.size() != 0) {
            throw new ElrappValidationException(StringUtils.join(errMsg, "\n"));
        }
        return errMsg;
    }

}
