package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.felt.OpenItemDialog;
import no.mesta.mipss.felt.OpenItemDialog.TYPE;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.inspeksjon.detaljui.InspeksjonDetaljPanel;
import no.mesta.mipss.mipssfelt.InspeksjonService;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

@SuppressWarnings("serial")
public class InspeksjonButtonController {

    private final InspeksjonController controller;
    private InspeksjonService inspeksjonService;

    public InspeksjonButtonController(InspeksjonController controller) {
        this.controller = controller;
        inspeksjonService = BeanUtil.lookup(InspeksjonService.BEAN_NAME, InspeksjonService.class);
    }

    /**
     * Action for å søke frem avvik
     *
     * @param inspeksjonController
     * @return
     */

    public Action getInspeksjonSokButtonAction(final InspeksjonController inspeksjonController) {
        return new AbstractAction(Resources.getResource("button.sok"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<InspeksjonSokResult> model = inspeksjonController.getInspeksjonTableModel();
                model.setBeanMethodArgValues(new Object[]{inspeksjonController.getQueryParams()});
                model.loadData();
            }
        };
    }

    private Long prevRefnummer;

    /**
     * Action for å åpne et avvik med en gitt id(refnummer)
     *
     * @return
     */
    public Action getInspeksjonHentMedIdAction() {
        return new AbstractAction(Resources.getResource("button.hentInspeksjonMedId"), IconResources.DETALJER_SMALL_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.INSPEKSJON, controller.getFeltController());
                dialog.setRefnummer(prevRefnummer);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevRefnummer = dialog.getRefnummer();
            }
        };
    }

    /**
     * Action for å slette en Inspeksjon
     *
     * @return
     */
    public Action getInspeksjonSlettAction(final InspeksjonDetaljPanel panel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.slettInspeksjonen"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Inspeksjon inspeksjon = panel.getInspeksjon();
                try {
                    Inspeksjon i = slettInspeksjon(inspeksjon, brukerSign);
                    if (i != null) {
                        ActionEvent ev = new ActionEvent(i, 0, "slettet");
                        callback.actionPerformed(ev);
                        InspeksjonSokResult toRemove = null;
                        for (InspeksjonSokResult ir : controller.getInspeksjonTableModel().getEntities()) {
                            if (inspeksjon.getGuid().equals(ir.getGuid())) {
                                toRemove = ir;
                                break;
                            }
                        }
                        if (toRemove != null)
                            controller.getInspeksjonTableModel().removeItem(toRemove);
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        };
    }

    public Action getInspeksjonSlettAction() {
        return new AbstractAction(Resources.getResource("button.slettInspeksjon"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<InspeksjonSokResult> selectedEntities = controller.getInspeksjonTable().getSelectedEntities();
                String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();

                String msg = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.inspeksjon", dagerForSletting) : Resources.getResource("warning.slett.inspeksjoner", dagerForSletting);
                String tit = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.inspeksjon.title") : Resources.getResource("warning.slett.inspeksjoner.title");
                int v = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, tit, JOptionPane.OK_CANCEL_OPTION);
                if (v == JOptionPane.YES_OPTION) {
                    inspeksjonService.deleteInspeksjon(selectedEntities, controller.getFeltController().getLoggedOnUserSign());
                    for (InspeksjonSokResult in : selectedEntities) {
                        controller.getInspeksjonTableModel().removeItem(in);
                    }
                }
            }
        };
    }

    private Inspeksjon slettInspeksjon(Inspeksjon inspeksjon, String brukerSign) throws Exception {
        String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();
        String refnummer = inspeksjon.getRefnummer() != null ? inspeksjon.getRefnummer().toString() : "";
        String msg = Resources.getResource("warning.slett.inspeksjon", dagerForSletting, refnummer);
        String title = Resources.getResource("warning.slett.inspeksjon.title", refnummer);
        int response = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, title, JOptionPane.YES_NO_CANCEL_OPTION);
        if (response == JOptionPane.YES_OPTION) {
            return inspeksjonService.deleteInspeksjon(inspeksjon, brukerSign);
        }
        return null;
    }

    /**
     * Action for å eksportere søkeresultatene til Inspeksjon til Excel
     *
     * @return
     */
    public Action getInspeksjonExcelAction() {
        return new AbstractAction(Resources.getResource("button.excel"), IconResources.EXCEL_ICON16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openExcel(controller.getInspeksjonTable());
            }
        };
    }

    /**
     * Action for å vise frem en rapportmeny for Inspeksjon
     *
     * @return
     */
    public Action getInspeksjonRapportAction() {
        return new AbstractAction(Resources.getResource("button.rapport2"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                new InspeksjonrapportHandler(controller.getFeltController());
            }
        };
    }

    /**
     * Action for å vise frem detaljer om en Inspeksjon
     *
     * @return
     */
    public Action getInspeksjonDetaljerAction() {
        return new AbstractAction(Resources.getResource("button.detaljer"), IconResources.DETALJER_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<InspeksjonSokResult> inspeksjon = controller.getInspeksjonTable().getSelectedEntities();
                InspeksjonSokResult in = inspeksjon.get(0);
                controller.openInspeksjon(in);
            }
        };
    }

    /**
     * Action for å vise en Inspeksjon i kart
     *
     * @return
     */
    public Action getInspeksjonKartAction() {
        return new AbstractAction(Resources.getResource("button.visIKart"), IconResources.NORGE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO
            }
        };
    }

    public Action getInspeksjonLagreAction(final InspeksjonDetaljPanel inspeksjonDetaljPanel) {
        return new AbstractAction(Resources.getResource("button.lagre"), IconResources.SAVE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Inspeksjon in = inspeksjonDetaljPanel.getInspeksjon();
                Inspeksjon i = controller.persistInspeksjon(in);
                if (i != null) {
                    inspeksjonDetaljPanel.getSaveStatusRegistry().reset();
                }
            }
        };
    }
}
