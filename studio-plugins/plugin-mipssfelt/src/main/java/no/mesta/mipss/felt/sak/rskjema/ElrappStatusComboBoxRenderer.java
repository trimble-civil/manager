package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.ui.Sphere;
import no.mesta.mipss.valueobjects.ElrappStatus;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ElrappStatusComboBoxRenderer extends DefaultListCellRenderer {
    private Sphere red;
    private Sphere green;
    private Sphere yellow;
    private boolean allOnNull;

    public ElrappStatusComboBoxRenderer() {
        red = new Sphere(Color.red, 14);
        green = new Sphere(Color.green, 14);
        yellow = new Sphere(Color.yellow, 14);
    }

    public ElrappStatusComboBoxRenderer(boolean allOnNull) {
        this();
        this.allOnNull = allOnNull;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        label.setText(" ");
        ElrappStatus status = (ElrappStatus) value;
        if (status != null && status.getStatus() != null) {
            switch (status.getStatus()) {
                case IKKE_SENDT:
                    label.setIcon(red.getIcon());
                    label.setText(Resources.getResource("label.elrappStatus.ikkeSendt"));
                    break;
                case ENDRET:
                    label.setIcon(yellow.getIcon());
                    label.setText(Resources.getResource("label.elrappStatus.endret"));
                    break;
                case SENDT:
                    label.setIcon(green.getIcon());
                    label.setText(Resources.getResource("label.elrappStatus.rapportertOk"));
                    break;
            }
        } else if (allOnNull) {
            label.setText(Resources.getResource("label.alle"));
        }
        return label;
    }
}