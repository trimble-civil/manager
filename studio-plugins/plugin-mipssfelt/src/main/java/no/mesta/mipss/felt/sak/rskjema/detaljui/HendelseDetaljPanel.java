package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.felt.FeltMapPanel;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.felt.util.ImageDropper;
import no.mesta.mipss.persistence.OwnedMipssEntity;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;
import no.mesta.mipss.valueobjects.ElrappStatus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class HendelseDetaljPanel extends SakDetaljTabPanel {

    private final RskjemaController controller;
    private FeltEntityInfo info;
    private String title;
    private Hendelse hendelse;
    private TidStedPanel pnlTidSted;
    private VurderingPanel pnlVurdering;
    private BeskrivelsePanel pnlBeskrivelse;
    private HendelseBildePanel pnlBilder;
    private HendelseDetaljButtonPanel pnlButtons;

    private FeltMapPanel pnlMapPoint;
    private JPanel pnlContent;

    public HendelseDetaljPanel(RskjemaController controller, FeltEntityInfo info) {
        this.controller = controller;
        this.info = info;
        this.title = "R2 (" + info.getRefnummer() + ")";
        initComponents();
        initGui();
        doLoad();
        waitWhileLoading();
    }

    public HendelseDetaljPanel(RskjemaController controller, Hendelse hendelse) {
        this.controller = controller;
        this.title = "R2 (" + hendelse.getRefnummer() + ")";
        initComponents();
        initGui();
        setHendelse(hendelse);
        bind();
    }

    @Override
    public void setChanged(boolean changed) {
        super.setChanged(changed);
        pnlButtons.setEnabled(changed);
    }

    @Override
    protected void initGui() {
        super.initGui();
        setLayout(new BorderLayout());
        final GridBagLayout layout = new GridBagLayout();
        pnlContent = new JPanel(layout);

        // rad 1
        addComponentToGrid(pnlTidSted, 0, 0, 0, 0);
        addComponentToGrid(pnlVurdering, 0, 1, 1, 2, 0, 0.25); // strekker seg med rad 2

        // rad 2
        addComponentToGrid(pnlBeskrivelse, 1, 0, 0, 0.25);

        // rad 3
        addComponentToGrid(pnlBilder, 2, 0, 1, 0.75);
        addComponentToGrid(pnlMapPoint, 2, 1, 1, 0.75);

        add(pnlSlettetInfo, BorderLayout.NORTH);
        add(pnlContent, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);

        // lytteren sørger for at de to kolonnene i dette panelet alltid har lik bredde
        addComponentListener(new java.awt.event.ComponentAdapter() {
            @Override
            public void componentResized(java.awt.event.ComponentEvent evt) {
                int width = getWidth();
                int wGap = getInsets().left + getInsets().right;

                layout.columnWidths = new int[]{width / 2 - wGap, width / 2 - wGap};
            }
        });
    }

    private void addComponentToGrid(Component component, int x, int y, int colspan, int rowspan, double weightx, double weighty) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = y;
        c.gridy = x;
        c.gridwidth = colspan;
        c.gridheight = rowspan;
        c.weightx = weightx;
        c.weighty = weighty;
        c.fill = GridBagConstraints.BOTH;
        addComponentToGrid(component, c);
    }

    private void addComponentToGrid(Component component, int x, int y, double weightx, double weighty) {
        addComponentToGrid(component, x, y, 1, 1, weightx, weighty);
    }

    private void addComponentToGrid(Component component, GridBagConstraints c) {
        pnlContent.add(component, c);
    }

    private void initComponents() {
        pnlTidSted = new TidStedPanel();
        pnlVurdering = new VurderingPanel(controller);
        pnlBeskrivelse = new BeskrivelsePanel();
        pnlBilder = new HendelseBildePanel(controller);
        new ImageDropper(pnlBilder, new ImageDropper.ImageDropListener() {
            @Override
            public void imageDropped(Bilde[] images) {
                for (Bilde i : images) {
                    pnlBilder.addBilde(i);
                }
            }
        }, controller.getFeltController().getLoggedOnUserSign());
        pnlMapPoint = new FeltMapPanel(controller.getFeltController());
        pnlButtons = new HendelseDetaljButtonPanel(controller, this);
        loadingPanels = new ArrayList<LoadingPanel>();
        loadingPanels.add(pnlTidSted);
        loadingPanels.add(pnlVurdering);
        loadingPanels.add(pnlBeskrivelse);
        loadingPanels.add(pnlBilder);
        loadingPanels.add(pnlMapPoint);
    }

    @Override
    public void dispose() {
        for (LoadingPanel panel : loadingPanels) {
            panel.dispose();
        }
        removeAll();
        loadingPanels.clear();
    }

    private void setHendelse(Hendelse hendelse) {
        if (hendelse.getSlettetDato() != null) {
            pnlSlettetInfo.setVisible(true);
            lblSlettet.setText("Hendelsen er slettet, " + MipssDateFormatter.formatDate(hendelse.getSlettetDato()) + " av " + hendelse.getSlettetAv());
            pnlButtons.setGjenopprett(true);
            panelDisabledIndicator = new PanelDisabledIndicator(pnlContent);
        }
        this.hendelse = hendelse;
        pnlTidSted.setHendelse(hendelse);
        pnlVurdering.setHendelse(hendelse);
        pnlBeskrivelse.setHendelse(hendelse);
        pnlBilder.setHendelse(hendelse);
        pnlMapPoint.setHendelse(hendelse);

    }

    public Hendelse getHendelse() {
        return this.hendelse;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String refnummer) {
        title = "R2 (" + refnummer + ")";
        getTabHeader().setTitle(title);
    }

    @Override
    public void load() {
        this.hendelse = controller.hentHendelse(info.getGuid());
    }

    public void oppdaterElrappKule() {
        getTabHeader().setElrappStatus(getElrappStatus());
    }

    @Override
    protected void bind() {
        SaveStatusMonitor<Hendelse> hendelseMonitor = new SaveStatusMonitor<Hendelse>(Hendelse.class, this, "hendelse", getSaveStatusRegistry().getBindingGroup());
        hendelseMonitor.registerField("aarsak");
        hendelseMonitor.registerField("henvendelseMottattAv");
        hendelseMonitor.registerField("henvendelseFra");
        hendelseMonitor.registerField("henvendelseDato");
        hendelseMonitor.registerField("tiltak");
        hendelseMonitor.registerField("tiltakTekster");
        hendelseMonitor.registerField("beskrivelse");
        hendelseMonitor.registerField("bilder");
        hendelseMonitor.registerField("skjemaEmne");
        hendelseMonitor.registerField("objektType");
        hendelseMonitor.registerField("objektAvvikType");
        hendelseMonitor.registerField("objektAvvikKategori");
        hendelseMonitor.registerField("objektStedTypeAngivelse");
        hendelseMonitor.registerField("merknad");
        hendelseMonitor.registerField("utbedringplan");
        hendelseMonitor.registerField("planlagtUtbedret");
        hendelseMonitor.registerField("prosess");
        hendelseMonitor.registerField("utbedret");
        hendelseMonitor.registerField("arbeidType");

        SaveStatusMonitor<OwnedMipssEntity> endretMonitor = new SaveStatusMonitor<OwnedMipssEntity>(OwnedMipssEntity.class, this, "${hendelse.ownedMipssEntity}", getSaveStatusRegistry().getBindingGroup());
        endretMonitor.registerField("endretDato");

        getSaveStatusRegistry().addMonitor(endretMonitor);
        getSaveStatusRegistry().addMonitor(hendelseMonitor);

        getSaveStatusRegistry().getBindingGroup().addBinding(BindingHelper.createbinding(getSaveStatusRegistry(), "changed", this, "changed"));
        getSaveStatusRegistry().doBind();

    }

    @Override
    protected void doneLoading() {
        setHendelse(hendelse);
    }

    @Override
    public MipssFieldDetailItem getDetailEntity() {
        return hendelse;
    }

    @Override
    public FeltEntityInfo getFeltEntityInfo() {
        return info;
    }

    @Override
    public ElrappStatus getElrappStatus() {
        return ElrappStatus.getElrappStatus(hendelse.getRapportertDato(), hendelse.getOwnedMipssEntity().getEndretDato());
    }
}
