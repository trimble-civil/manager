package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import org.jdesktop.swingx.JXHeader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class SaveEntitiesPanel extends JPanel {

    private final FeltController controller;
    private final List<MipssFieldDetailItem> changedEntities;
    private List<EntityPanel> entityPanels = new ArrayList<EntityPanel>();
    private boolean disposed;

    public SaveEntitiesPanel(FeltController controller, List<MipssFieldDetailItem> changedEntities) {
        this.controller = controller;
        this.changedEntities = changedEntities;
        initGui();
    }

    public boolean isDisposed() {
        return disposed;
    }

    private JPanel createHeader() {
        JXHeader header = new JXHeader();
        header.setTitle(Resources.getResource("warning.lagreFaneark"));
        header.setDescription(Resources.getResource("warning.lagreFaneark.description"));

        Dimension size = header.getPreferredSize();
        size.height = 75;
        size.width = 350;
        header.setPreferredSize(size);
        return header;
    }

    private JPanel createButtons() {
        JPanel pnl = new JPanel(new GridBagLayout());
        JButton btnLagre = new JButton(getLagreAction());
        JButton btnAvbryt = new JButton(getAvbrytAction());
        JButton btnLukk = new JButton(getLukkAction());
        btnLagre.setToolTipText(Resources.getResource("button.lagreValgte.tooltip"));
        btnLukk.setToolTipText(Resources.getResource("button.lukkUtenLagre.tooltip"));


        pnl.add(btnLukk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnl.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnl.add(btnLagre, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        return pnl;
    }

    private Action getAvbrytAction() {
        return new AbstractAction(Resources.getResource("button.avbryt")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                disposed = false;
                SwingUtilities.windowForComponent(SaveEntitiesPanel.this).dispose();
            }
        };
    }

    private Action getLagreAction() {
        return new AbstractAction(Resources.getResource("button.lagreValgte")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                disposed = true;
                saveSelected();
                SwingUtilities.windowForComponent(SaveEntitiesPanel.this).dispose();
            }
        };
    }

    private Action getLukkAction() {
        return new AbstractAction(Resources.getResource("button.lukkUtenLagre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                disposed = true;
                SwingUtilities.windowForComponent(SaveEntitiesPanel.this).dispose();
            }
        };
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        JPanel pnlHeader = createHeader();
        add(pnlHeader, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        int i = 0;
        JPanel pnlEntities = new JPanel(new GridBagLayout());
        pnlEntities.setBorder(BorderFactory.createTitledBorder(""));
        for (MipssFieldDetailItem item : changedEntities) {
            EntityPanel ep = new EntityPanel(item);
            entityPanels.add(ep);
            pnlEntities.add(ep, new GridBagConstraints(0, i++, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        add(pnlEntities, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 0), 0, 0));
        add(createButtons(), new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(15, 5, 5, 0), 0, 0));
    }

    private void saveSelected() {
        for (EntityPanel ep : entityPanels) {
            if (ep.isSaveSelected()) {
                saveEntity(ep.getEntity());
            }
        }
    }

    private void saveEntity(MipssFieldDetailItem entity) {
        controller.saveEntity(entity);
    }

    class EntityPanel extends JPanel {
        private final MipssFieldDetailItem entity;
        private JCheckBox chkSave;

        public EntityPanel(MipssFieldDetailItem entity) {
            this.entity = entity;
            initGui();
        }

        public boolean isSaveSelected() {
            return chkSave.isSelected();
        }

        public MipssFieldDetailItem getEntity() {
            return entity;
        }

        private void initGui() {
            setBackground(Color.white);
            chkSave = new JCheckBox("Lagre");
            chkSave.setSelected(true);
            chkSave.setOpaque(false);
            String labelString = "";
            if (entity instanceof Skred) {
                labelString = "R11 (id=" + ((Skred) entity).getRefnummer() + ")";
            }
            if (entity instanceof Forsikringsskade) {
                labelString = "R5 (id=" + ((Forsikringsskade) entity).getRefnummer() + ")";
            }
            if (entity instanceof Hendelse) {
                labelString = "R2 (id=" + ((Hendelse) entity).getRefnummer() + ")";
            }
            if (entity instanceof Avvik) {
                labelString = "Avvik(id=" + ((Avvik) entity).getRefnummer() + ")";
            }
            if (entity instanceof Sak) {
                labelString = "Sak(id=" + ((Sak) entity).getRefnummer() + ")";
            }
            JLabel label = new JLabel(labelString);

            setLayout(new GridBagLayout());
            add(chkSave, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
            add(label, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }

    }
}
