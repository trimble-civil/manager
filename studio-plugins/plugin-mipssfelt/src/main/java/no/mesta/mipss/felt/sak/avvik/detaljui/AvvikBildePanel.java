package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.MaxLengthEnforcer;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.felt.sak.bilde.BildeVelgerDialog;
import no.mesta.mipss.felt.util.ImageNameFilter;
import no.mesta.mipss.felt.util.ImageRotator;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikBilde;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.ComponentSizeResources;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import org.apache.commons.io.IOUtils;
import org.jdesktop.swingx.JXImageView;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.CheckerboardPainter;
import org.jdesktop.swingx.painter.Painter;

import javax.persistence.Column;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;

@SuppressWarnings("serial")
public class AvvikBildePanel extends JXPanel implements LoadingPanel {

    private JScrollPane picturesPane = new JScrollPane();
    private JXPanel pnlImage;
    private boolean loading = true;
    private JLabel lblAntallBilder;
    private final AvvikController controller;
    private AvvikBilde activeImage;
    private Avvik avvik;


    public AvvikBildePanel(AvvikController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    public void setAvvik(Avvik avvik) {
        this.avvik = avvik;
        setAvvikBilder(avvik.getBilder());
    }

    private List<JXImageView> imageViews = new ArrayList<JXImageView>();
    private JComponent buttonPanel;
    private JLayeredPane layeredPane;
    private JButton btnEdit;
    private JButton btnDelete;
    private JPopupListMenu btnAdd;
    private JButton btnOpen;
    private JButton btnRoter;

    private void setAvvikBilder(List<AvvikBilde> bildeList) {
        for (JXImageView v : imageViews) {
            pnlImage.remove(v);
        }
        for (AvvikBilde avvikBilde : bildeList) {
            Bilde bilde = avvikBilde.getBilde();
            Image image = bilde.getSmaaImage();
            final JXImageView view = new JXImageView();
            imageViews.add(view);
            view.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
            if (image == null) {
                image = bilde.getImage();
                if (image == null)
                    image = IconResources.MISSING_ICON.getImage();
            }
            if (bilde.getBeskrivelse() != null) {
                view.setToolTipText("<html><pre>" + bilde.getBeskrivelse() + "</pre></html>");
            }
            view.setPreferredSize(new Dimension(200, 180));
            view.setImage(image);
            view.setDragEnabled(false);
            view.setEditable(false);
            view.addMouseListener(new ImageClickListener(view, avvikBilde));

            view.setLayout(new BorderLayout());
            JComponent info = createImageInfo(avvikBilde, Color.WHITE);
            view.add(info, BorderLayout.NORTH);

            pnlImage.add(view);
        }
        lblAntallBilder.setText(" Antall bilder: " + bildeList.size());
        picturesPane.setViewportView(pnlImage);
        picturesPane.getHorizontalScrollBar().setUnitIncrement(205);
        updateSize();
        loading = false;
    }

    public JComponent createImageInfo(AvvikBilde pb, Color color) {
        Box box = Box.createVerticalBox();
        JLabel dateLabel = new JLabel(" " + MipssDateFormatter.formatDate(pb.getBilde().getNyDato(),
                MipssDateFormatter.SHORT_DATE_TIME_FORMAT));
        dateLabel.setForeground(color);
        dateLabel.setBackground(new Color(0f, 0f, 0f, 0.75f));
        dateLabel.setOpaque(true);
        JLabel statusLabel = new JLabel(" " + pb.getStatusType().getNavn());

        statusLabel.setForeground(color);
        statusLabel.setBackground(new Color(0f, 0f, 0f, 0.75f));
        statusLabel.setOpaque(true);

        box.add(dateLabel);
        box.add(BoxUtil.createVerticalStrut(1));
        box.add(statusLabel);

        return box;
    }

    public void dispose() {
        for (JXImageView v : imageViews) {
            for (MouseListener ml : v.getMouseListeners())
                v.removeMouseListener(ml);
            v.removeAll();
        }
        imageViews.clear();
        picturesPane.removeAll();
        pnlImage.removeAll();
    }

    protected void clearBorders() {
        Component[] components = pnlImage.getComponents();
        for (Component c : components) {
            if (c instanceof JXImageView) {
                JXImageView imageView = (JXImageView) c;
                imageView.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
            }
        }
        repaint();
    }

    private void initComponents() {
        pnlImage = new JXPanel(new FlowLayout());
        pnlImage.setBackground(Color.WHITE);
        pnlImage.setScrollableTracksViewportWidth(false);

        lblAntallBilder = new JLabel(" Antall bilder:");
        lblAntallBilder.setBackground(new Color(0f, 0f, 0f, 0.75f));
        lblAntallBilder.setOpaque(true);
        lblAntallBilder.setForeground(Color.WHITE);

        buttonPanel = createButtonsPanel();

        layeredPane = new JLayeredPane();
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                updateSize();
            }

            public void componentMoved(ComponentEvent e) {
                updateSize();
            }
        });
        updateSize();
        layeredPane.add(picturesPane, new Integer(0));
        layeredPane.add(lblAntallBilder, new Integer(1));
        layeredPane.add(buttonPanel, new Integer(2));
    }

    private void updateSize() {
        int w = getWidth();
        int h = layeredPane.getHeight();
        picturesPane.setBounds(new Rectangle(0, 0, w, h));
        lblAntallBilder.setBounds(5, h - 40, 90, 15);
        buttonPanel.setBounds(w - 305, h - 44, 300, 20);
//		
        if (avvik != null) {
            if (avvik.getBilder() == null || avvik.getBilder().size() < 2) {
                pnlImage.setPreferredSize(new Dimension(w, pnlImage.getHeight()));
            } else
                pnlImage.setPreferredSize(null);
        } else
            pnlImage.setPreferredSize(null);
    }

    private JComponent createButtonsPanel() {
        Box buttonPanel = Box.createHorizontalBox();

        btnEdit = new JButton(getEditBildeAction());
        btnDelete = new JButton(getSlettBildeAction());
//		btnAdd = new JButton(getAddBildeAction());

        List<JMenuItem> menuItems = new ArrayList<JMenuItem>();
        menuItems.add(new JMenuItem(getAddBildeFraFilAction()));
        menuItems.add(new JMenuItem(getAddBildeFraSakAction()));
        btnAdd = new JPopupListMenu(Resources.getResource("button.leggTil"), null, menuItems);

        btnOpen = new JButton(getOpenBildeAction());
        btnRoter = new JButton(getRoterBildeAction());

        buttonPanel.add(Box.createHorizontalGlue());
        if (controller.getFeltController().getPlugin().hasWriteAccess()) {
            buttonPanel.add(btnRoter);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
            buttonPanel.add(btnDelete);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
            buttonPanel.add(btnEdit);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
            buttonPanel.add(btnAdd);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
        }
        buttonPanel.add(btnOpen);

        return buttonPanel;
    }

    private AbstractAction getRoterBildeAction() {
        return new AbstractAction("", IconResources.ROTATECW_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                rotateImage();
            }
        };
    }

    private AbstractAction getEditBildeAction() {
        return new AbstractAction(Resources.getResource("button.endre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                editPicture();
            }
        };
    }

    private AbstractAction getAddBildeFraFilAction() {
        return new AbstractAction(Resources.getResource("button.fraFil")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                addPicture();
            }
        };
    }

    private AbstractAction getAddBildeFraSakAction() {
        return new AbstractAction(Resources.getResource("button.fraSak")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Bilde> bilderFromSak = controller.getFeltController().getBilderFromSak(avvik.getSakGuid());
                List<Bilde> cleaned = new ArrayList<Bilde>();
                for (Bilde b : bilderFromSak) {
                    List<AvvikBilde> bilder = avvik.getBilder();
                    boolean found = false;
                    for (AvvikBilde bb : bilder) {
                        if (bb.getBilde().equals(b)) {
                            found = true;
                        }
                    }
                    if (!found) {
                        cleaned.add(b);
                    }
                }
                Window windowForComponent = SwingUtilities.windowForComponent(AvvikBildePanel.this);
                BildeVelgerDialog dialog = new BildeVelgerDialog((JDialog) windowForComponent, cleaned);

                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
                List<Bilde> selectedImages = dialog.getSelectedImages();
                if (!selectedImages.isEmpty()) {
                    for (Bilde b : selectedImages) {
                        AvvikBilde avvikBilde = createAvvikBilde(b);
                        if (avvikBilde != null) {
                            avvik.addBilde(avvikBilde);
                        }
                        activeImage = null;
                        setAvvikBilder(avvik.getBilder());
                    }
                }
            }
        };
    }

    private void rotateImage() {
        if (activeImage != null) {
            AvvikBilde active = activeImage;
            Bilde bilde = active.getBilde();
            ImageRotator.rotate90(bilde, controller.getFeltController().getLoggedOnUserSign());
            setAvvikBilder(avvik.getBilder());

            for (JXImageView v : imageViews) {
                if (v.getImage().equals(bilde.getImage()) || v.getImage().equals(bilde.getSmaaImage())) {
                    selectView(v);
                }
            }
            activeImage = active;
            avvik.setEndretAv(controller.getFeltController().getLoggedOnUserSign());
            avvik.setEndretDato(Clock.now());
        }
    }

    private void addPicture() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setLocale(new Locale("no", "NO"));
        fileChooser.setFileFilter(new ImageNameFilter());
        int what = fileChooser.showOpenDialog(this);
        if (what == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            Bilde b = createBilde(file);
            AvvikBilde pb = createAvvikBilde(b);
            if (pb == null) {
                return;
            }

            avvik.addBilde(pb);
            activeImage = null;
            setAvvikBilder(avvik.getBilder());
        }
    }

    private AbstractAction getOpenBildeAction() {
        return new AbstractAction(Resources.getResource("button.apne")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (activeImage != null) {
                    openImage();
                }
            }
        };
    }

    protected void openImage() {
        AvvikBildePictureModel model = new AvvikBildePictureModel();
        model.setPunktregBilder(avvik.getBilder());
        int imageIndex = model.indexOf(activeImage);
        JDialog dialog = (JDialog) SwingUtilities.windowForComponent(this);
        JPictureDialogue picture = new JPictureDialogue(dialog, model);
        picture.showBilde(imageIndex);
        picture.setVisible(true);
    }

    private AbstractAction getSlettBildeAction() {
        return new AbstractAction(Resources.getResource("button.slett")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (activeImage != null) {
                    int a = JOptionPane.showConfirmDialog(AvvikBildePanel.this, Resources.getResource("warning.slettBilde"), Resources.getResource("warning.slettBilde.title"), JOptionPane.YES_NO_OPTION);
                    if (a == JOptionPane.YES_OPTION) {
                        slettActiveImage();
                    }
                }
            }
        };
    }

    private void slettActiveImage() {
        avvik.removeBilde(activeImage);
        activeImage = null;
        setAvvikBilder(avvik.getBilder());
    }

    private void initGui() {

        setLayout(new GridBagLayout());
        add(layeredPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    private Bilde createBilde(File file) {
        Bilde b = new Bilde();
        b.setGuid(ServerUtils.getInstance().fetchGuid());
        b.setNyDato(Clock.now());
        b.setOppretter(controller.getFeltController().getPlugin().getLoader().getLoggedOnUserSign());
        b.setFilnavn(file.getName());
        try {
            byte[] imageLob = IOUtils.toByteArray(new FileInputStream(file));
            b.setBildeLob(imageLob);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        b.setSmaabildeLob(ImageUtil.getScaledInstance(new ByteArrayInputStream(b.getBildeLob()), 320, 240, 0.5f));
        b.setBredde(b.getImage().getWidth());
        b.setHoyde(b.getImage().getHeight());

        return b;
    }

    public void addBilde(Bilde b) {
        AvvikBilde avvikBilde = createAvvikBilde(b);
        if (avvikBilde == null) {
            return;
        }
        avvik.addBilde(avvikBilde);
        activeImage = null;
        setAvvikBilder(avvik.getBilder());
    }

    private AvvikBilde createAvvikBilde(final Bilde b) {
        final AvvikBilde pb = new AvvikBilde();
        pb.setBilde(b);
        pb.setAvvik(avvik);

        final JDialog dialog = new JDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("label.leggTilBilde"), true);

        List<AvvikstatusType> statuser = setToList(avvik.getStatusTyper());
        MipssComboBoxModel<AvvikstatusType> model = new MipssComboBoxModel<AvvikstatusType>(statuser);
        final JComboBox combo = new JComboBox(model);
        combo.setRenderer(new MipssListCellRenderer<AvvikstatusType>());
        ComponentSizeResources.setComponentSize(combo, new Dimension(160, 22));

        final JMipssDatePicker pkrDato = new JMipssDatePicker(Clock.now());
        JLabel lblDato = new JLabel("Bildedato:");

        final JTextArea text = new JTextArea();
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.setViewportView(text);
        ComponentSizeResources.setComponentSize(pane, new Dimension(160, 120));

        Integer maxLength = (Integer) b.getAnnotationValue("getBeskrivelse", Column.class, "length");
        MaxLengthEnforcer enforcer = new MaxLengthEnforcer(b, text, "beskrivelse", maxLength);
        enforcer.start();

        JXImageView view = new JXImageView();
        ComponentSizeResources.setComponentSize(view, new Dimension(160, 140));
        view.setSize(new Dimension(180, 160));
        view.setImage(b.getImage());
        view.setDragEnabled(false);
        view.setEditable(false);
        view.setScale(calculateScale(b.getImage(), view.getSize()));

        JButton ok = new JButton(Resources.getResource("button.leggTil"));
        ok.setMargin(new Insets(1, 1, 1, 1));
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (combo.getSelectedItem() != null) {
                    pb.setStatusType((AvvikstatusType) combo.getSelectedItem());
                    b.setBeskrivelse(text.getText());
                    b.setNyDato(pkrDato.getDate());
                    dialog.setVisible(false);
                }
            }
        });

        JButton cancel = new JButton(Resources.getResource("button.avbryt"));
        cancel.setMargin(new Insets(1, 1, 1, 1));
        cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                pb.setBilde(null);
                dialog.setVisible(false);
            }
        });
        JPanel pnlDato = new JPanel(new GridBagLayout());
        pnlDato.add(lblDato, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlDato.add(pkrDato, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        ComponentSizeResources.setComponentSize(pnlDato, new Dimension(160, 22));
        Box buttons = BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(), cancel, BoxUtil
                .createHorizontalStrut(2), ok);

        Box gui = BoxUtil.createVerticalBox(2, BoxUtil.createHorizontalBox(2, BoxUtil.createVerticalBox(2, view),
                BoxUtil.createHorizontalStrut(2), BoxUtil.createVerticalBox(2, combo, pnlDato, pane)), buttons);

        dialog.add(gui);

        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.setResizable(false);
        dialog.pack();
        dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
        dialog.setVisible(true);

        if (pb.getBilde() != null) {
            return pb;
        } else {
            return null;
        }
    }

    private void editPicture() {
        if (activeImage == null)
            return;
        final JDialog dialog = new JDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("button.endre"), true);
        final AvvikstatusType originalStatus = activeImage.getStatusType();
        List<AvvikstatusType> statuser = setToList(avvik.getStatusTyper());
        MipssComboBoxModel<AvvikstatusType> model = new MipssComboBoxModel<AvvikstatusType>(statuser);
        final JComboBox combo = new JComboBox(model);
        combo.setRenderer(new MipssListCellRenderer<AvvikstatusType>());
        ComponentSizeResources.setComponentSize(combo, new Dimension(160, 22));
        combo.setSelectedItem(originalStatus);

        final JTextArea text = new JTextArea();
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        text.setText(activeImage.getBilde().getBeskrivelse());
        JScrollPane pane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.setViewportView(text);
        ComponentSizeResources.setComponentSize(pane, new Dimension(160, 120));

        Integer maxLength = (Integer) activeImage.getBilde().getAnnotationValue("getBeskrivelse", Column.class,
                "length");
        MaxLengthEnforcer enforcer = new MaxLengthEnforcer(activeImage.getBilde(), text, "beskrivelse", maxLength);
        enforcer.start();

        JButton ok = new JButton(Resources.getResource("button.endre"));
        ok.setMargin(new Insets(1, 1, 1, 1));
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (combo.getSelectedItem() != null) {
                    activeImage.setStatusType((AvvikstatusType) combo.getSelectedItem());
                    activeImage.getBilde().setBeskrivelse(text.getText());
                    dialog.setVisible(false);

                }
            }
        });

        JButton cancel = new JButton(Resources.getResource("button.avbryt"));
        cancel.setMargin(new Insets(1, 1, 1, 1));
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        });

        Box buttons = BoxUtil.createHorizontalBox(0, Box.createHorizontalGlue(), cancel, BoxUtil
                .createHorizontalStrut(2), ok);

        Box gui = BoxUtil.createVerticalBox(2, BoxUtil
                .createHorizontalBox(2, BoxUtil.createVerticalBox(2, combo, pane)), buttons);

        dialog.add(gui);

        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.setResizable(false);
        dialog.pack();
        dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
        dialog.setVisible(true);
        setAvvikBilder(avvik.getBilder());

    }

    private List<AvvikstatusType> setToList(Set<AvvikstatusType> set) {
        List<AvvikstatusType> list = new ArrayList<AvvikstatusType>();
        Iterator<AvvikstatusType> it = set.iterator();
        while (it.hasNext()) {
            list.add((AvvikstatusType) it.next());
        }
        Collections.sort(list);
        return list;
    }

    private double calculateScale(Image image, Dimension size) {
        double scale = 1;

        if (image == null) {
            return scale;
        }

        double imageWidth = image.getWidth(null);
        double imageHeight = image.getHeight(null);
        double viewWidth = size.getWidth();
        double viewHeight = size.getHeight();

        if (viewWidth > viewHeight) {
            if (imageWidth < imageHeight) {
                scale = viewWidth / imageWidth;
            } else {
                scale = viewHeight / imageHeight;
            }
        } else {
            if (imageWidth > imageHeight) {
                scale = viewHeight / imageHeight;
            } else {
                scale = viewWidth / imageWidth;
            }
        }

        return scale;
    }

    private void selectView(JXImageView view) {
        clearBorders();
        view.setBorder(BorderFactory.createLineBorder(Color.GREEN, 3));
    }

    private final class ImageClickListener extends MouseAdapter {
        private final JXImageView view;
        private final AvvikBilde bilde;

        private ImageClickListener(JXImageView view, AvvikBilde bilde) {
            this.view = view;
            this.bilde = bilde;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                activeImage = bilde;
                if (e.getClickCount() == 1) {
                    selectView(view);
                } else if (e.getClickCount() == 2) {
                    openImage();
                }
            }
        }
    }

    public void setLukket(boolean lukketAvvik) {
        btnEdit.setEnabled(!lukketAvvik);
        btnAdd.setEnabled(!lukketAvvik);
        btnDelete.setEnabled(!lukketAvvik);
        btnOpen.setEnabled(!lukketAvvik);

    }
}
