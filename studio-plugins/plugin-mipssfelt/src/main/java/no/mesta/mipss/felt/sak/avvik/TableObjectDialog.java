package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.ui.table.CheckBoxTableObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SuppressWarnings({"serial", "rawtypes"})
public abstract class TableObjectDialog<T extends CheckBoxTableObject, E extends MipssEntityBean> extends JDialog {

    protected final List<E> alleStatuser;
    protected TableObjectPanel<T, E> pnlStatus;
    private JButton btnOk;
    private JButton btnCancel;
    private boolean cancel = true;

    public TableObjectDialog(JFrame owner, List<E> alleStatuser) {
        super(owner, Resources.getResource("dialog.title.avvikstatus"), true);
        this.alleStatuser = alleStatuser;
        initComponents();
        initGui();
    }

    protected abstract TableObjectPanel<T, E> getPanel();

    private void initComponents() {
        pnlStatus = getPanel();
        btnOk = new JButton(getOkAction());
        btnCancel = new JButton(getAvbrytAction());

    }

    public boolean isCancelled() {
        return cancel;
    }

    private Action getOkAction() {
        return new AbstractAction(Resources.getResource("button.ok")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancel = false;
                dispose();
            }
        };
    }

    private Action getAvbrytAction() {
        return new AbstractAction(Resources.getResource("button.avbryt")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancel = true;
                dispose();
            }
        };
    }

    private void initGui() {
        setLayout(new BorderLayout());
        add(pnlStatus);
        JCheckBox chkAlle = new JCheckBox(Resources.getResource("label.velgAlle"));
        chkAlle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JCheckBox box = (JCheckBox) e.getSource();
                if (box.isSelected()) {
                    pnlStatus.setAlleValgt(true);
                } else {
                    pnlStatus.setAlleValgt(false);
                }
            }
        });
        JPanel pnlButton = new JPanel();
        pnlButton.add(btnCancel);
        pnlButton.add(btnOk);
        JPanel pnlB = new JPanel(new BorderLayout());
        pnlB.add(pnlButton, BorderLayout.EAST);

        JPanel pnlW = new JPanel(new BorderLayout());
        pnlW.add(chkAlle, BorderLayout.WEST);
        pnlB.add(pnlW, BorderLayout.WEST);
        add(pnlB, BorderLayout.SOUTH);
    }
}
