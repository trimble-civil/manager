package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Sak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

@SuppressWarnings("serial")
public class SakDetaljDialog extends JDialog {

    private static final Logger log = LoggerFactory.getLogger(SakDetaljDialog.class);

    private final SakController controller;

    private SakDetaljPanel pnlDetalj;

    private final Sak sak;

    private boolean lastSlettede;

    private String[] lastSlettedeGuid;

    private String selectedGuid;

    public SakDetaljDialog(SakController controller, Sak sak, boolean lastSlettede) {
        super(controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
        log.debug("SakDetaljDialog(sak={})", sak == null ? "null" : sak.getGuid());
        this.controller = controller;
        this.sak = sak;
        this.lastSlettede = lastSlettede;
        initComponents();
        initGui();
    }

    public SakDetaljDialog(SakController controller, Sak sak, String[] lastSlettedeGuid, String selectedGuid) {
        super(controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
        log.debug("SakDetaljDialog(sak={})", sak == null ? "null" : sak.getGuid());
        this.controller = controller;
        this.sak = sak;
        this.lastSlettedeGuid = lastSlettedeGuid;
        this.selectedGuid = selectedGuid;
        initComponents();
        initGui();
    }

    public void setSelectedGuid(String selectedGuid) {
        pnlDetalj.setSelectedGuid(selectedGuid);
    }

    public SakDetaljPanel getSakDetaljPanel() {
        return pnlDetalj;
    }

    private void initComponents() {
        pnlDetalj = new SakDetaljPanel(controller, sak, lastSlettede, lastSlettedeGuid, selectedGuid, this);
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setTitle("Sak id:" + sak.getRefnummer());
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {

                SaveStatusListener sl = pnlDetalj.getSaveStatusListener();
                List<MipssFieldDetailItem> changedEntities = sl.getChangedEntities();
                if (changedEntities.size() == 0) {
                    closeWindow();
                } else {
                    JDialog d = new JDialog();
                    SaveEntitiesPanel saveEntitiesPanel = new SaveEntitiesPanel(controller.getFeltController(), changedEntities);
                    d.add(saveEntitiesPanel);
                    d.pack();
                    d.setLocationRelativeTo(SakDetaljDialog.this);
                    d.setModal(true);
                    d.setVisible(true);
                    if (saveEntitiesPanel.isDisposed()) {
                        closeWindow();
                    }
                }

            }
        });
    }

    private void closeWindow() {
        pnlDetalj.getBindingGroup().unbind();
        pnlDetalj.dispose();
        dispose();
    }

    private void initGui() {
        add(pnlDetalj);
        setSize(1600, 900);
    }
}
