package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.persistence.Clock;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;
import java.util.Date;

public class AvvikSomIkkeErManglerHighlighter implements HighlightPredicate {
    private final AvvikController controller;

    public AvvikSomIkkeErManglerHighlighter(AvvikController controller) {
        this.controller = controller;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        AvvikSokResult avvikSok = controller.getManglerTableModel().get(controller.getManglerTable().convertRowIndexToModel(adapter.row));
        Date tiltaksDato = avvikSok.getTiltaksDato();

        Date mangelIntrefferDato = CalendarUtil.round(Clock.now(), true);
        if (tiltaksDato.after(mangelIntrefferDato) || tiltaksDato.getTime() == mangelIntrefferDato.getTime()) {
            return true;
        }
        return false;
    }
}
