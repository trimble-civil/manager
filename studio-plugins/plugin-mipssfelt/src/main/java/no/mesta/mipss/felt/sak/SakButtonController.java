package no.mesta.mipss.felt.sak;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.felt.OpenItemDialog;
import no.mesta.mipss.felt.OpenItemDialog.TYPE;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.detaljui.InternnotatDialog;
import no.mesta.mipss.felt.sak.detaljui.SakDetaljPanel;
import no.mesta.mipss.mipssfelt.SakService;
import no.mesta.mipss.mipssfelt.SakSokResult;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.roadpicker.JRoadSelectDialog;
import no.mesta.mipss.valueobjects.ElrappTypeR.Type;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

@SuppressWarnings("serial")
public class SakButtonController {

    private final SakController controller;
    private SakService sakService;

    public SakButtonController(SakController controller) {
        this.controller = controller;
        sakService = BeanUtil.lookup(SakService.BEAN_NAME, SakService.class);
    }

    /**
     * Action for knappen som søker i sak-skjermbildet
     *
     * @return
     */
    public Action getSakSokButtonAction(final SakController sakController) {
        return new AbstractAction(Resources.getResource("button.sok"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<SakSokResult> model = sakController.getSakTableModel();
                model.setBeanMethodArgValues(new Object[]{sakController.getQueryParams()});
                model.loadData();

            }
        };
    }

    private Long prevRefnummer;

    /**
     * Action for knappen for å hente en sak med gitt Id i sak-skjermbildet
     *
     * @return
     */
    public Action getSakHentMedIdAction() {
        return new AbstractAction(Resources.getResource("button.hentSakMedId"), IconResources.DETALJER_SMALL_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.SAK, controller.getFeltController());
                dialog.setRefnummer(prevRefnummer);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevRefnummer = dialog.getRefnummer();
            }
        };
    }

    /**
     * Action for å opprette en ny sak
     *
     * @return
     */
    public Action getSakNyAction() {
        return new AbstractAction(Resources.getResource("button.nySak"), IconResources.NEW_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.opprettNySak();
            }
        };
    }

    /**
     * Action for å slette en sak
     * <p/>
     * Saker kan kun slettes dersom det ikke er tilknyttet noen avvik, r2, r5 eller r11
     *
     * @param panel
     * @return
     */

    public Action getSakSlettAction(final SakPanel panel) {
        return new AbstractAction(Resources.getResource("button.slettSak"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<SakSokResult> selectedEntities = controller.getSakTable().getSelectedEntities();
                String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();
                String msg = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.sak", dagerForSletting) : Resources.getResource("warning.slett.saker", dagerForSletting);
                String tit = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.sak.title") : Resources.getResource("warning.slett.saker.title");
                int v = JOptionPane.showConfirmDialog(panel, msg, tit, JOptionPane.OK_CANCEL_OPTION);
                if (v == JOptionPane.YES_OPTION) {
                    sakService.deleteSaker(selectedEntities, controller.getFeltController().getPlugin().getLoader().getLoggedOnUserSign());
                    for (SakSokResult rs : selectedEntities) {
                        controller.getSakTableModel().removeItem(rs);
                    }
                }
            }
        };
    }

    /**
     * Action for å eksportere søkeresultatene til sak til Excel
     *
     * @return
     */
    public Action getSakExcelAction() {
        return new AbstractAction(Resources.getResource("button.excel"), IconResources.EXCEL_ICON16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openExcel(controller.getSakTable());
            }
        };
    }

    /**
     * Action for å lage en avvikrapport
     *
     * @return
     */
    public Action getSakAvvikRapportAction() {
        return new AbstractAction(Resources.getResource("label.rapportAvvik"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<SakSokResult> entities = controller.getSakTable().getSelectedEntities();
                SakSokResult sakSokResult = entities.get(0);
                controller.getFeltController().openRapportAvvikSak(sakSokResult.getGuid());
            }
        };
    }

    /**
     * Action for å lage en r-skjema rapport, r2,r5,r11
     *
     * @param rSkjema
     * @return
     */
    public Action getSakRskjemaRapportAction(final Type rSkjema) {
        return new AbstractAction(rSkjema.name(), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<SakSokResult> entities = controller.getSakTable().getSelectedEntities();
                SakSokResult sakSokResult = entities.get(0);
                switch (rSkjema) {
                    case R2:
                        controller.getFeltController().openRapportR2Sak(sakSokResult.getGuid());
                        break;
                    case R5:
                        controller.getFeltController().openRapportR5Sak(sakSokResult.getGuid());
                        break;
                    case R11:
                        controller.getFeltController().openRapportR11Sak(sakSokResult.getGuid());
                        break;
                }
            }
        };
    }


    /**
     * Action for å vise detaljer om en sak
     *
     * @return
     */
    public Action getSakDetaljerAction() {
        return new AbstractAction(Resources.getResource("button.detaljer"), IconResources.DETALJER_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<SakSokResult> mdlSak = controller.getSakTableModel();
                JMipssBeanTable<SakSokResult> tblSak = controller.getSakTable();
                controller.openSak(mdlSak.get(tblSak.convertRowIndexToModel(tblSak.getSelectedRow())));
            }
        };
    }

    public Action getEndreVeiAction(final SakDetaljPanel sakPanel) {
        return new AbstractAction(Resources.getResource("button.endre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Sak sak = sakPanel.getSak();
                Punktstedfest sted = sak.getStedfesting().get(0);
                if (sted.getVeiref() == null) {
                    Punktveiref pv = new Punktveiref();
                    pv.setPunktstedfest(sted);
                    sted.addPunktveiref(pv);
//					sted.addPunktveiref(punktveiref)
                }
                VeiInfo veiInfo = controller.getFeltController().createVeiInfoFromPunktstedfest(sted);

                JRoadSelectDialog dialog = new JRoadSelectDialog((JDialog) SwingUtilities.windowForComponent(sakPanel), true, false);
                dialog.setDriftkontrakt(controller.getQueryParams().getValgtKontrakt());
                dialog.setVeiSok(veiInfo);
                dialog.setVisible(true);

                if (dialog.isSet()) {
                    while (!updateStedFromVeiInfo(sakPanel, dialog, sted) && dialog.isSet()) {
                        dialog.setVisible(true);
                    }
                }
            }
        };
    }

    private boolean updateStedFromVeiInfo(SakDetaljPanel sakPanel, JRoadSelectDialog dialog, Punktstedfest sted) {
        VeiInfo veiInfo = dialog.getVeiSok();
        if (veiInfo != null) {
            boolean updated = controller.getFeltController().updateStedfestingFromVeiInfo(veiInfo, sted);
            if (!updated) {
                JOptionPane.showMessageDialog(sakPanel, Resources.getResource("message.ugyldigVeireferanse"), Resources.getResource("message.ugyldigVeireferanse.title"), JOptionPane.WARNING_MESSAGE);
                return false;
            } else {
                sakPanel.getSaveMonitor().setOverriddenChange(true);
            }
        }
        return true;
    }

    public Action getOpenInspeksjonAction(final SakDetaljPanel sakPanel) {
        return new AbstractAction(Resources.getResource("button.openInspeksjon")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().getInspeksjonController().openInspeksjon(sakPanel.getSak().getInspeksjon());
            }
        };
    }

    public Action getLagreSakAction(final SakDetaljPanel sakPanel, final String brukerSign) {
        return new AbstractAction(Resources.getResource("button.lagreSak"), IconResources.SAVE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Sak sak = sakPanel.getSak();
                try {
                    lagreSak(sakPanel, sak);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public Action getOpprettRskjemaAction(final SakDetaljPanel sakPanel, final String brukerSign, final Type rSkjema) {
        return new AbstractAction(Resources.getResource("button.opprett", rSkjema.toString())) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (lagreSakPrompt(sakPanel)) {
                    switch (rSkjema) {
                        case R2:
                            if (!isAvvikLagret(sakPanel.getSak().getAvvik())) {
                                showAvvikMessage(sakPanel);
                                return;
                            }
                            Hendelse hendelse = controller.getFeltController().getRskjemaController().opprettR2(sakPanel.getSak());
                            sakPanel.addHendelse(hendelse);
                            break;
                        case R5:
                            if (!isAvvikLagret(sakPanel.getSak().getAvvik())) {
                                showAvvikMessage(sakPanel);
                                return;
                            }
                            Forsikringsskade forsikringsskade = controller.getFeltController().getRskjemaController().opprettR5(sakPanel.getSak());
                            sakPanel.addForsikringsskade(forsikringsskade);
                            break;
                        case R11:
                            Skred skred = controller.getFeltController().getRskjemaController().opprettR11(sakPanel.getSak());
                            sakPanel.addSkred(skred);
                            break;
                    }
                }
            }
        };
    }

    private void showAvvikMessage(SakDetaljPanel sakPanel) {
        JOptionPane.showMessageDialog(sakPanel, Resources.getResource("message.avvik.ma.lagres"), Resources.getResource("message.avvik.ma.lagres.title"), JOptionPane.INFORMATION_MESSAGE);
    }

    private boolean isAvvikLagret(List<Avvik> aList) {
        for (Avvik a : aList) {
            if (!a.isNewEntity() && a.getSlettetDato() == null)
                return true;
        }
        return false;
    }

    private boolean lagreSak(SakDetaljPanel sakPanel, Sak sak) {
        Sak s = controller.persistSak(sak);
        if (s != null) {
            if (sak.getRefnummer() == null)
                ((JDialog) SwingUtilities.windowForComponent(sakPanel)).setTitle("Sak id:" + s.getRefnummer());
            sakPanel.getSaveStatusRegistry().reset();
            List<MipssFieldDetailItem> changedEntities = sakPanel.getSaveStatusListener().getChangedEntities();
            if (changedEntities.contains(sak)) {
                changedEntities.remove(sak);
            }
            sak.setRefnummer(s.getRefnummer());
            sak.setNewEntity(s.isNewEntity());
            return true;
        }
        return false;
    }

    private boolean lagreSakPrompt(SakDetaljPanel sakPanel) {
        Sak sak = sakPanel.getSak();
        if (sak.isNewEntity()) {
            int res = JOptionPane.showConfirmDialog(sakPanel, Resources.getResource("warning.lagreSakForRegistrering"), Resources.getResource("warning.lagreSakForRegistrering.title"), JOptionPane.OK_CANCEL_OPTION);
            if (res == JOptionPane.OK_OPTION) {
                if (lagreSak(sakPanel, sak))
                    return true;
                return false;
            }
            return false;
        }
        return true;
    }

    public Action getOpprettAvvikAction(final SakDetaljPanel sakPanel, final String brukerSign) {
        return new AbstractAction(Resources.getResource("button.opprettAvvik")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (lagreSakPrompt(sakPanel)) {
                    Avvik avvik = controller.getFeltController().getAvvikController().opprettAvvik(sakPanel.getSak());
                    sakPanel.addAvvik(avvik);
                }

            }
        };
    }

    public Action getVisInternnotatAction(final SakDetaljPanel sakDetaljPanel) {
        return new AbstractAction(Resources.getResource("button.internnotat"), IconResources.EDIT_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                String title = Resources.getResource("label.internnotat.title");
                Window window = SwingUtilities.windowForComponent(sakDetaljPanel);

                InternnotatDialog dialog = new InternnotatDialog((JDialog) window, title, sakDetaljPanel, controller);
                dialog.setSize(400, 300);
                dialog.setLocationRelativeTo(sakDetaljPanel);
                dialog.setVisible(true);
            }
        };
    }


}
