package no.mesta.mipss.felt;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class FeltPanel extends JPanel {

    private final FeltController controller;
    private CommonCriteriaPanel pnlCriteria;
    private MangelStatistikkPanel pnlMangelStats;

    private FeltTabPanel pnlFeltTab;
    private PanelDisabledIndicator panelDisabledIndicator;

    public FeltPanel(FeltController controller) {
        this.controller = controller;
        setPreferredSize(new Dimension(900, 500));
        initComponents();
        initGui();
        bind();
    }

    private void bind() {
        BindingHelper.createbinding(controller.getCommonQueryParams(), "${valgtKontrakt != null}", this, "valgtKontrakt").bind();
    }

    private void initComponents() {
        pnlCriteria = new CommonCriteriaPanel(controller);
        pnlMangelStats = new MangelStatistikkPanel(controller);
        pnlFeltTab = new FeltTabPanel(controller);
    }

    public void setValgtKontrakt(boolean kontraktValgt) {
        if (kontraktValgt) {
            if (panelDisabledIndicator != null) {
                panelDisabledIndicator.dispose();
                panelDisabledIndicator = null;
            }
        } else {
            panelDisabledIndicator = new PanelDisabledIndicator(pnlFeltTab);
        }
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(pnlCriteria, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
        add(pnlMangelStats, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL, new Insets(5, 5, 0, 5), 0, 0));
        add(pnlFeltTab, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 0, 0, 0), 0, 0));
    }


}
