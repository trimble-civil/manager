package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.r.r11.SkredEgenskapverdi;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Legger til eller sletter en Egenskap med verdi til Skredet.
 *
 * @author harkul
 */
public class EgenskapItemListener implements ItemListener {

    private final Egenskap egenskap;
    private final Skred skred;

    public EgenskapItemListener(Egenskap egenskap, Skred skred) {
        this.egenskap = egenskap;
        this.skred = skred;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

        if (e.getItem() instanceof Egenskapverdi) {
            Egenskapverdi verdi = (Egenskapverdi) e.getItem();
            int stateChange = e.getStateChange();
            if (stateChange == ItemEvent.DESELECTED) {
                SkredEgenskapverdi toRemove = null;
                for (SkredEgenskapverdi s : skred.getSkredEgenskapList()) {
                    if (s.getEgenskapverdi().equals(verdi)) {
                        toRemove = s;
                        break;
                    }
                }
                if (toRemove != null) {
                    skred.removeSkredEgenskap(toRemove);
                }
            }
            if (stateChange == ItemEvent.SELECTED) {
                addNewSkredEgenskap(verdi);
            }
        }
    }

    private void addNewSkredEgenskap(Egenskapverdi nyVerdi) {
        Egenskap egenskap = nyVerdi.getEgenskap();
        if (egenskap == null) {
            egenskap = this.egenskap;
        }
        removeSkredEgenskap(egenskap);

        if (nyVerdi.getEgenskap() != null) {
            SkredEgenskapverdi ny = new SkredEgenskapverdi();
            ny.setEgenskapverdi(nyVerdi);
            ny.setSkred(skred);
            ny.setOpprettetDato(Clock.now());

            skred.addSkredEgenskap(ny);
        }
    }

    private void removeSkredEgenskap(Egenskap egenskap) {
        SkredEgenskapverdi toRemove = null;
        for (SkredEgenskapverdi v : skred.getSkredEgenskapList()) {
            if (v.getEgenskapverdi().getEgenskap().equals(egenskap)) {
                toRemove = v;
                break;
            }
        }
        if (toRemove != null) {
            skred.removeSkredEgenskap(toRemove);
        }
    }
}
