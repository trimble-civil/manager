package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AvvikStatusBeskrivelseDialog extends JDialog {

    private final AvvikStatus avvikStatus;

    private JButton btnOk;
    private JButton btnAvbryt;
    private JTextArea txtFritekst;
    private JScrollPane scrFritekst;
    private boolean cancelled = true;

    public AvvikStatusBeskrivelseDialog(JDialog parent, AvvikStatus avvikStatus) {
        super(parent, true);
        this.avvikStatus = avvikStatus;
        initComponents();
        initGui();
    }

    private void initComponents() {
        btnOk = new JButton(getOkAction());
        btnAvbryt = new JButton(getAvbrytAction());
        txtFritekst = new JTextArea();
        scrFritekst = new JScrollPane(txtFritekst);
        txtFritekst.setText(avvikStatus.getFritekst());
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        JPanel pnlButton = new JPanel(new GridBagLayout());
        pnlButton.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlButton.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(scrFritekst, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 10, 5), 0, 0));

    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public String getAvvikStatusFritekst() {
        return txtFritekst.getText();
    }

    public void show(JComponent center) {
        pack();
        setLocationRelativeTo(center);
        setVisible(true);
    }

    private AbstractAction getOkAction() {
        return new AbstractAction(Resources.getResource("button.lagre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = false;
                AvvikStatusBeskrivelseDialog.this.dispose();

            }

        };
    }

    private AbstractAction getAvbrytAction() {
        return new AbstractAction(Resources.getResource("button.avbryt")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                AvvikStatusBeskrivelseDialog.this.dispose();
            }

        };
    }

}
