package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.StringNullConverter;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class RessursPanel extends JPanel implements LoadingPanel {

    private Avvik avvik;
    private JTextArea txtRessurs;
    private JScrollPane scrRessurs;
    private BindingGroup bindings = new BindingGroup();

    public RessursPanel() {
        initComponents();
        initGui();
    }

    private void initComponents() {
        txtRessurs = new JTextArea();
        txtRessurs.setWrapStyleWord(true);
        txtRessurs.setLineWrap(true);

        scrRessurs = new JScrollPane(txtRessurs);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        JPanel pnlRessurs = new JPanel(new BorderLayout());
        pnlRessurs.setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.anslagRessurs")));
        pnlRessurs.add(scrRessurs, BorderLayout.CENTER);

        add(pnlRessurs, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }

    public void setAvvik(Avvik avvik) {
        this.avvik = avvik;
        bind();
    }

    private void bind() {
        Binding<Avvik, String, JTextArea, String> estimatBind = BindingHelper.createbinding(avvik, "estimat", txtRessurs, "text", UpdateStrategy.READ_WRITE);
        StringNullConverter sn = new StringNullConverter();
        estimatBind.setConverter(sn);
        bindings.addBinding(estimatBind);
        bindings.bind();
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();
    }

    public void setLukket(boolean lukketAvvik) {
        txtRessurs.setEditable(!lukketAvvik);
        txtRessurs.setEnabled(!lukketAvvik);
    }

}