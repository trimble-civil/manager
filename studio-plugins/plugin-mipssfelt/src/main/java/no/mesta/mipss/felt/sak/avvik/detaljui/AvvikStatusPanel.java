package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.AvvikStatus;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JDateTimePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.list.MipssListSelectionModel;
import no.mesta.mipss.util.DatePickerConstraintsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JListBinding;
import org.jdesktop.swingbinding.SwingBindings;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class AvvikStatusPanel extends JPanel {
    private static final Logger logger = LoggerFactory.getLogger(AvvikStatusPanel.class);
    private Avvik funn;
    private AvvikStatus status = new AvvikStatus();
    private final BindingGroup bindingGroup;
    private JLabel lblInfo;

    private JPanel pnlComment;
    private JTextArea txtComment;
    private JScrollPane scrComment;
    private JDateTimePicker pkrDateTime;
    private JList lstStatustype;
    private MipssListSelectionModel<AvvikStatus> mdlStatustype;
    private JScrollPane scrStatustype;
    private JLabel lblDato;
    private final FeltController controller;

    public AvvikStatusPanel(FeltController controller, Avvik funn) {
        this.controller = controller;
        bindingGroup = new BindingGroup();
        this.funn = funn;
        status.setAvvik(funn);
        status.setOpprettetAv(controller.getPlugin().getLoader().getLoggedOnUserSign());
        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        txtComment = new JTextArea();
        txtComment.setLineWrap(true);
        txtComment.setWrapStyleWord(true);
        txtComment.setRows(4);

        scrComment = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrComment.setViewportView(txtComment);


        pnlComment = new JPanel(new BorderLayout());
        pnlComment.setBorder(BorderFactory.createTitledBorder("Kommentar"));
        pnlComment.add(scrComment);

        Date notBeforeDate = funn.getStatus().getOpprettetDato();
        Date now = Clock.now();
        Date anchorDate = (!now.before(notBeforeDate) ? now : notBeforeDate);
        pkrDateTime = new JDateTimePicker(anchorDate);
        pkrDateTime.setNotBeforeDate(notBeforeDate);
        DatePickerConstraintsUtils.setNotBeforeDate(notBeforeDate, pkrDateTime.getDatePicker());
        lblDato = new JLabel("Statusdato :");

        lstStatustype = new JList();
        lstStatustype.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstStatustype.setCellRenderer(new MipssListCellRenderer<AvvikstatusType>());


        mdlStatustype = new MipssListSelectionModel<AvvikStatus>(lstStatustype);
        mdlStatustype.setSelectionMode(MipssListSelectionModel.SINGLE_SELECTION);

        scrStatustype = new JScrollPane(lstStatustype);

        lblInfo = new JLabel(IconResources.HELP_ICON);
        lblInfo.setText(Resources.getResource("label.infoStatusDato"));
        lblInfo.setVerticalAlignment(JLabel.TOP);
        lblInfo.setHorizontalAlignment(JLabel.LEFT);
        lblInfo.setBorder(BorderFactory.createTitledBorder(""));
        lblInfo.setBackground(new java.awt.Color(255, 242, 171));
        lblInfo.setIconTextGap(15);
        lblInfo.setOpaque(true);

    }

    protected void initGui() {
        logger.debug("initGUI()");
        setLayout(new GridBagLayout());
        add(scrStatustype, new GridBagConstraints(0, 0, 1, 2, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 2, 0, 0), 0, 0));
        add(lblDato, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
        add(pkrDateTime, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
        add(lblInfo, new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(55, 10, 0, 2), 0, 0));
        add(pnlComment, new GridBagConstraints(0, 2, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
    }

    @SuppressWarnings("rawtypes")
    private void bind() {
        List<AvvikstatusType> statusTypes = new ArrayList<AvvikstatusType>(controller.getAvvikController().getStatusTyper(funn.getTilleggsarbeid()));
        statusTypes.removeAll(getTypeHistory());
        Collections.sort(statusTypes);

        Binding bndComment = BindingHelper.createbinding(txtComment, "text", status, "fritekst");
        Binding bndDate = BindingHelper.createbinding(pkrDateTime, "date", status, "opprettetDato");

        JListBinding<AvvikstatusType, List<AvvikstatusType>, JList> bndStatustypeList = SwingBindings.createJListBinding(AutoBinding.UpdateStrategy.READ, statusTypes, lstStatustype);
        Binding bndStatustype = BindingHelper.createbinding(mdlStatustype, "selectedValue", status, "avvikstatusType");
        bindingGroup.addBinding(bndComment);
        bindingGroup.addBinding(bndDate);
        bindingGroup.addBinding(bndStatustypeList);
        bindingGroup.addBinding(bndStatustype);
        bindingGroup.bind();
    }

    private List<AvvikstatusType> getTypeHistory() {
        List<AvvikstatusType> statuser = new ArrayList<AvvikstatusType>();

        for (AvvikStatus p : funn.getStatusHistory()) {
            statuser.add(p.getAvvikstatusType());
        }

        return statuser;
    }

    /**
     * @return the funn
     */
    public Avvik getFunn() {
        return funn;
    }

    /**
     * @return the status
     */
    public AvvikStatus getStatus() {
        return status;
    }

    /**
     * Setter opp gui for panelet
     *
     */


    /**
     * @param funn the funn to set
     */
    public void setFunn(Avvik funn) {
        this.funn = funn;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(AvvikStatus status) {
        this.status = status;
    }
}
