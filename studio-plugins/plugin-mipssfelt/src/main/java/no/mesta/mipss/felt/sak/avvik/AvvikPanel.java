package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.SokekriterierTaskPane;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class AvvikPanel extends JPanel {

    private static final Logger log = LoggerFactory.getLogger(AvvikPanel.class);

    private final AvvikController controller;
    private SokekriterierTaskPane taskPaneSok;
    private SokekriterierAvvikPanel pnlSokekriterier;

    private JButton btnSokopprettet;
    private JButton btnSokInnenforPeriode;
    private JButton btnSokStatusendring;
    private JButton btnHentMedId;
    private JPanel pnlButton;
    private AvvikButtonPanel pnlAvvikButtons;
    private JMipssBeanScrollPane scrTable;

    public AvvikPanel(FeltController controller) {
        this.controller = controller.getAvvikController();

        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlSokekriterier = new SokekriterierAvvikPanel(controller);
        taskPaneSok = new SokekriterierTaskPane(pnlSokekriterier);

        btnSokopprettet = new JButton(controller.getAvvikSokOpprettetButtonAction());
        btnSokInnenforPeriode = new JButton(controller.getAvvikSokApnePrTilDatoButtonAction());
        btnSokStatusendring = new JButton(controller.getAvvikSokStatusendringButtonAction());
        btnSokopprettet.setToolTipText(Resources.getResource("button.sokOpprettet.tooltip"));
        btnSokInnenforPeriode.setToolTipText(Resources.getResource("button.sokApnePrTilDato.tooltip"));
        btnSokStatusendring.setToolTipText(Resources.getResource("button.sokStatusendring.tooltip"));

        btnHentMedId = new JButton(controller.getAvvikHentMedIdAction());
        pnlButton = new JPanel(new GridBagLayout());
        pnlAvvikButtons = new AvvikButtonPanel(controller);

        scrTable = new JMipssBeanScrollPane(controller.getAvvikTable(), controller.getAvvikTableModel());
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        pnlButton.add(btnSokopprettet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
        pnlButton.add(btnSokInnenforPeriode, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnSokStatusendring, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnHentMedId, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(taskPaneSok, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(scrTable, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 0, 5, 0), 0, 0));
        add(pnlAvvikButtons, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
    }
}
