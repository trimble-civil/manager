package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.elrapp.ElrappValidationException;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.elrapp.R11Validator;
import no.mesta.mipss.felt.elrapp.R2Validator;
import no.mesta.mipss.felt.elrapp.R5Validator;
import no.mesta.mipss.felt.sak.rskjema.detaljui.HendelseDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.TiltakDialog;
import no.mesta.mipss.felt.sak.rskjema.detaljui.TiltakPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.VurderingPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.R11Controller;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r11.SkredDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.r5.ForsikringsskadeDetaljPanel;
import no.mesta.mipss.mipssfelt.*;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.*;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.valueobjects.ElrappStatus;
import no.mesta.mipss.valueobjects.ElrappTypeR;
import no.mesta.mipss.valueobjects.ElrappTypeR.Type;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.SortOrder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RskjemaController {

    private final FeltController controller;
    private final RskjemaButtonController buttonController;
    private RskjemaQueryParams queryParams;
    private MipssBeanTableModel<RskjemaSokResult> mdlRskjema;
    private JMipssBeanTable<RskjemaSokResult> tblRskjema;
    private RskjemaService rskjemaService;
    private List<Hendelseaarsak> hendelseaarsakCache;
    private List<Trafikktiltak> trafikktiltakCache;
    private List<ObjektType> objektTypeCache;
    private List<ObjektAvvikType> objektAvvikTypeCache;
    private List<ObjektAvvikKategori> objektAvvikKategoriCache;
    private List<ObjektStedTypeAngivelse> objektStedTypeAngivelseCache;
    private List<ArbeidType> arbeidTypeCache;
    private MipssFelt feltService;


    public RskjemaController(FeltController controller) {
        this.controller = controller;
        buttonController = new RskjemaButtonController(this);
        rskjemaService = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
        feltService = BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class);

        queryParams = new RskjemaQueryParams();
        bindQueryParams(controller.getCommonQueryParams());
        initRskjemaTable();
    }

    private void bindQueryParams(QueryParams commonQueryParams) {
        BindingHelper.createbinding(commonQueryParams, "dateFra", queryParams, "dateFra").bind();
        BindingHelper.createbinding(commonQueryParams, "dateTil", queryParams, "dateTil").bind();
        BindingHelper.createbinding(commonQueryParams, "brukerList", queryParams, "brukerList").bind();
        BindingHelper.createbinding(commonQueryParams, "pdaDfuIdentList", queryParams, "pdaDfuIdentList").bind();
        BindingHelper.createbinding(commonQueryParams, "valgtKontrakt", queryParams, "valgtKontrakt").bind();
        BindingHelper.createbinding(commonQueryParams, "veiListe", queryParams, "veiListe").bind();
        BindingHelper.createbinding(commonQueryParams, "prosesser", queryParams, "prosesser").bind();
    }

    public MipssBeanTableModel<RskjemaSokResult> getRskjemaTableModel() {
        return mdlRskjema;
    }

    public JMipssBeanTable<RskjemaSokResult> getRskjemaTable() {
        return tblRskjema;
    }

    private void initRskjemaTable() {
        mdlRskjema = new MipssBeanTableModel<RskjemaSokResult>(RskjemaSokResult.class, "refnummer", "sakRefnummer", "typeR", "opprettetDato",
                "fylkesnummer", "kommunenummer", "veitype", "veinummer", "hp", "meter", "skadested", "beskrivelse", "elrappStatus", "elrappId",
                "elrappVersjon", "rapportertDato", "skjemaEmne");
        MipssRenderableEntityTableColumnModel rskjemaColumnModel = new MipssRenderableEntityTableColumnModel(
                RskjemaSokResult.class, mdlRskjema.getColumns());

        mdlRskjema.setBeanInstance(rskjemaService);
        mdlRskjema.setBeanMethod("sokRskjema");
        mdlRskjema.setBeanInterface(RskjemaService.class);
        mdlRskjema.setBeanMethodArguments(new Class<?>[]{RskjemaQueryParams.class});

        tblRskjema = new JMipssBeanTable<RskjemaSokResult>(mdlRskjema, rskjemaColumnModel);
        tblRskjema.setHighlighters(new Highlighter[]{
                new ColorHighlighter(new RskjemaHighlightPredicate(Type.R2, mdlRskjema, tblRskjema),
                        new Color(238, 240, 255),
                        TableHelper.getCellForeground(),
                        TableHelper.getSelectedBackground(),
                        TableHelper.getSelectedDefaultForeground()),
                new ColorHighlighter(new RskjemaHighlightPredicate(Type.R5, mdlRskjema, tblRskjema),
                        new Color(255, 250, 225),
                        TableHelper.getCellForeground(),
                        TableHelper.getSelectedBackground(),
                        TableHelper.getSelectedDefaultForeground()),
                new ColorHighlighter(new RskjemaHighlightPredicate(Type.R11, mdlRskjema, tblRskjema),
                        new Color(244, 234, 225),
                        TableHelper.getCellForeground(),
                        TableHelper.getSelectedBackground(),
                        TableHelper.getSelectedDefaultForeground())});

        tblRskjema.setDefaultRenderer(Date.class, new DateTimeRenderer());
        tblRskjema.setDefaultRenderer(Boolean.class, new BooleanRenderer());
        tblRskjema.setDefaultRenderer(Double.class, new DoubleRenderer());
        tblRskjema.setDefaultRenderer(ElrappStatus.class, new ElrappStatusTableCellRenderer());
        tblRskjema.setFillsViewportHeight(true);
        tblRskjema.setFillsViewportWidth(true);
        tblRskjema.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        tblRskjema.setColumnControlVisible(true);
        tblRskjema.addColumnStateSupport("rskjemaTable", controller.getPlugin());
        tblRskjema.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    openRskjema(mdlRskjema.get(tblRskjema.convertRowIndexToModel(tblRskjema.getSelectedRow())));
                }
            }
        });

        tblRskjema.loadUserSettings();
    }

    /**
     * Viser en ventedialog og åpner saken i en dialog
     *
     * @param rskjemaResult
     */
    public void openRskjema(final RskjemaSokResult rskjemaResult) {
        controller.getSakController().openSak(rskjemaResult.getSakGuid(), rskjemaResult.getSlettetDato() != null ? new String[]{rskjemaResult.getGuid()} : null, rskjemaResult.getGuid());
    }

    public void openRskjema(Long refnummer, ElrappTypeR.Type typeR) {

    }

    public Hendelse opprettR2(Sak sak) {
        Hendelse h = new Hendelse();
        if (sak.getAvvik() != null && sak.getAvvik().size() > 0) {
            Avvik avvik = sak.getAvvik().get(0);
            h.setBeskrivelse(avvik.getBeskrivelse());
            for (AvvikBilde ab : avvik.getBilder()) {
                h.addBilde(ab.getBilde());
            }
            h.setArbeidType(avvik.getArbeidType());
            h.setProsess(avvik.getProsess());
            h.setObjektType(avvik.getObjektType());
            h.setObjektAvvikType(avvik.getObjektAvvikType());
            h.setObjektAvvikKategori(avvik.getObjektAvvikKategori());
            h.setObjektStedTypeAngivelse(avvik.getObjektStedTypeAngivelse());
            h.setMerknad(avvik.getMerknad());

            Avvikaarsak aarsak = avvik.getAarsak();
            if(aarsak != null) {
                List<Hendelseaarsak> hendelseaarsaks = rskjemaService.hentAlleHendelseaarsaker();
                for(Hendelseaarsak ha : hendelseaarsaks) {
                    if(ha.getNavn().equals(aarsak.getNavn())) {
                        h.setAarsak(ha);
                        break;
                    }
                }
            }
        }

        h.setGuid(ServerUtils.getInstance().fetchGuid());
        h.setSak(sak);
        h.getOwnedMipssEntity().setOpprettetAv(controller.getLoggedOnUserSign());
        h.getOwnedMipssEntity().setOpprettetDato(Clock.now());
        h.setNewEntity(true);
        return h;
    }

    public Forsikringsskade opprettR5(Sak sak) {
        Forsikringsskade f = new Forsikringsskade();
        f.setGuid(ServerUtils.getInstance().fetchGuid());
        if (sak.getAvvik() != null && sak.getAvvik().size() > 0) {
            Avvik avvik = sak.getAvvik().get(0);
            f.setFritekst(avvik.getBeskrivelse());
            for (AvvikBilde ab : avvik.getBilder()) {
                Bilde b = ab.getBilde();
//				Bilde ny = copyBilde(b);
                ForsikringsskadeBilde fsb = new ForsikringsskadeBilde();
                fsb.setForsikringsskade(f);
                fsb.setBilde(b);
                f.addBilde(fsb);
            }
        }

        f.setSak(sak);
        f.setOpprettetAv(controller.getLoggedOnUserSign());
        f.setOpprettetDato(Clock.now());

        f.setAnmeldt(Boolean.FALSE);
        f.setNewEntity(true);

        return f;
    }

    public Skred opprettR11(Sak sak) {
        Skred s = new Skred();
        if (sak.getAvvik() != null && sak.getAvvik().size() > 0) {
            Avvik avvik = sak.getAvvik().get(0);
            s.setKommentar(avvik.getBeskrivelse());
            for (AvvikBilde ab : avvik.getBilder()) {
                s.addBilde(ab.getBilde());
            }
        }
        s.setGuid(ServerUtils.getInstance().fetchGuid());
        s.setSak(sak);
        s.setOpprettetAv(controller.getLoggedOnUserSign());
        s.setOpprettetDato(Clock.now());
        s.setStengtPgaSkredfare(Boolean.FALSE);
        s.setIngenNedbor(Boolean.FALSE);
        s.setGjelderGS(Boolean.FALSE);
        s.setNewEntity(true);
        return s;
    }

    public Hendelse hentHendelse(String guid) {
        Hendelse hendelse = rskjemaService.hentHendelse(guid);
        return hendelse;
    }

    public Forsikringsskade hentForsikringsskade(String guid) {
        Forsikringsskade forsikringsskade = rskjemaService.hentForsikringsskade(guid);
        return forsikringsskade;
    }

    public Skred hentSkred(String guid) {
        Skred skred = rskjemaService.hentSkred(guid);
        return skred;
    }

    public Hendelse hentHendelse(Long refnummer) {
        Hendelse hendelse = rskjemaService.hentHendelse(refnummer);
        return hendelse;
    }

    public Forsikringsskade hentForsikringsskade(Long refnummer) {
        Forsikringsskade forsikringsskade = rskjemaService.hentForsikringsskade(refnummer);
        return forsikringsskade;
    }

    public Skred hentSkred(Long refnummer) {
        Skred skred = rskjemaService.hentSkred(refnummer);
        return skred;
    }

    public Skred hentSkredElrapp(Long elrappId, Long driftkontraktId) {
        Skred skred = rskjemaService.hentSkredElrapp(elrappId, driftkontraktId);
        return skred;
    }

    public Hendelse hentHendelseElrapp(Long elrappId, Long driftkontraktId) {
        Hendelse hendelse = rskjemaService.hentHendelseElrapp(elrappId, driftkontraktId);
        return hendelse;
    }

    public Forsikringsskade hentForsikringsskadeElrapp(Long elrappId, Long driftkontraktId) {
        Forsikringsskade forsikringsskade = rskjemaService.hentForsikringsskadeElrapp(elrappId, driftkontraktId);
        return forsikringsskade;
    }


    /**
     * Henter alle Hendelseaarsak fra databasen
     *
     * @return false returnerer cache.
     */
    public List<Hendelseaarsak> getHendelseaarsaker(Hendelse hendelse) {
        List<Hendelseaarsak> aarsaker = getHendelseaarsaker();
        if(hendelse != null && !aarsaker.contains(hendelse.getAarsak())) {
            aarsaker.add(hendelse.getAarsak());
        }
        return aarsaker;
    }

    public List<Hendelseaarsak> getHendelseaarsaker() {
        return rskjemaService.hentAlleHendelseaarsaker();
    }

    public List<Trafikktiltak> getTrafikktiltak(boolean refresh) {
        if (refresh || trafikktiltakCache == null) {
            trafikktiltakCache = rskjemaService.hentAlleTrafikktiltak();
        }
        return trafikktiltakCache;
    }

    /**
     * Henter alle Objekttyper fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektType> getObjektTyper(boolean refresh, Long prosessId, Long kontraktId) {
        return rskjemaService.hentAlleObjektTyper(prosessId, kontraktId);
    }

    /**
     * Henter alle ObjektAvvikTyper fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektAvvikType> getObjektAvvikTyper(boolean refresh, Long prosessId, Long kontraktId) {
        return rskjemaService.hentAlleObjektAvvikTyper(prosessId, kontraktId);
    }

    /**
     * Henter alle ObjektAvvikKategorir fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektAvvikKategori> getObjektAvvikKategorier(boolean refresh, Long prosessId, Long kontraktId) {
        return rskjemaService.hentAlleObjektAvvikKategorier(prosessId, kontraktId);
    }

    /**
     * Henter alle ObjektStedTypeAngivelser fra databasen
     *
     * @param refresh    true hvis nye instanser skal hentes.
     * @param kontraktId
     * @return false returnerer cache.
     */
    public List<ObjektStedTypeAngivelse> getObjektStedTypeAngivelser(boolean refresh, Long prosessId, Long kontraktId) {
        return rskjemaService.hentAlleObjektStedTypeAngivelser(prosessId, kontraktId);
    }

    /**
     * Henter alle ArbeidTyper fra databasen
     *
     * @param refresh true hvis nye instanser skal hentes.
     * @return false returnerer cache.
     */
    public List<ArbeidType> getArbeidTyper(boolean refresh) {
        if (refresh || arbeidTypeCache == null) {
            arbeidTypeCache = rskjemaService.hentAlleArbeidTyper();
            arbeidTypeCache.add(null); // this ensures that we display "< ikke valgt>" in the drop-down list
        }
        return arbeidTypeCache;
    }

    public JMipssBeanTable<HendelseTrafikktiltak> createTrafikktiltakTable() {
        MipssBeanTableModel<HendelseTrafikktiltak> tableModel = new MipssBeanTableModel<HendelseTrafikktiltak>("tiltak", HendelseTrafikktiltak.class, "trafikktiltak", "beskrivelse");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(HendelseTrafikktiltak.class, tableModel.getColumns());
        final JMipssBeanTable<HendelseTrafikktiltak> table = new JMipssBeanTable<HendelseTrafikktiltak>(tableModel, columnModel);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && table.rowAtPoint(e.getPoint()) != -1) {
                    if (e.getClickCount() == 2) {
                        List<HendelseTrafikktiltak> statuses = table.getSelectedEntities();
                        HendelseTrafikktiltak status = (statuses != null && statuses.size() > 0 ? statuses.get(0) : null);

                        if (status != null) {
                            //TODO vis gui for endring..
                        }
                    }
                }
            }
        });

        table.setFillsViewportWidth(true);
        table.setFillsViewportHeight(true);
        table.setDefaultRenderer(Trafikktiltak.class, new MipssEntityRenderer());
        table.setSortOrder(0, SortOrder.ASCENDING);
        return table;
    }

    public Forsikringsskade persistForsikringsskade(Forsikringsskade forsikringsskade) {
        String brukerSign = controller.getLoggedOnUserSign();
        Forsikringsskade f = rskjemaService.persistForsikringsskade(forsikringsskade, brukerSign);
        List<String> bilder = new ArrayList<String>();
        for (ForsikringsskadeBilde b : f.getBilder()) {
            bilder.add(b.getBilde().getGuid());
        }
        controller.persistSynkbilde(bilder);
        reloadTable();
        return f;
    }

    public Skred persistSkred(Skred skred) {
        String brukerSign = controller.getLoggedOnUserSign();
        if (skred.getVaerFraVaerstasjon() != null && skred.getVaerFraVaerstasjon().getSkredGuid() == null) {
            skred.getVaerFraVaerstasjon().setSkred(skred);
        }
        Skred s = rskjemaService.persistSkred(skred, brukerSign);
        List<String> bilder = new ArrayList<String>();
        for (Bilde b : s.getBilder()) {
            bilder.add(b.getGuid());
        }
        controller.persistSynkbilde(bilder);
        reloadTable();
        return s;
    }

    public Hendelse persistHendelse(Hendelse hendelse) {
        String brukerSign = controller.getLoggedOnUserSign();
        Hendelse h = rskjemaService.persistHendelse(hendelse, brukerSign);
        List<String> bilder = new ArrayList<String>();
        for (Bilde b : h.getBilder()) {
            bilder.add(b.getGuid());
        }
        controller.persistSynkbilde(bilder);
        reloadTable();
        return h;
    }

    public void reloadTable() {
        final int selectedRow = getRskjemaTable().getSelectedRow();
        getRskjemaTableModel().loadData();
        getRskjemaTableModel().addTableLoaderListener(new MipssBeanLoaderListener() {
            @Override
            public void loadingStarted(MipssBeanLoaderEvent event) {
            }

            @Override
            public void loadingFinished(MipssBeanLoaderEvent event) {
                getRskjemaTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
            }
        });
        getFeltController().getSakController().reloadTable();
    }

    public void leggTilTrafikktiltak(TiltakPanel tiltakPanel) {
        Hendelse hendelse = tiltakPanel.getHendelse();
        TiltakDialog dialog = new TiltakDialog((JDialog) SwingUtilities.windowForComponent(tiltakPanel), hendelse, getTrafikktiltak(false));
        dialog.doDialogue(tiltakPanel);

        if (!dialog.isCancelled()) {
            HendelseTrafikktiltak nyttTiltak = dialog.getNewValue();
            if (hendelse.getTiltak().contains(nyttTiltak)) {
                List<HendelseTrafikktiltak> tiltak = hendelse.getTiltak();
                HendelseTrafikktiltak eksisterendeTiltak = tiltak.get(tiltak.indexOf(nyttTiltak));
                eksisterendeTiltak.setBeskrivelse(nyttTiltak.getBeskrivelse());
            } else {
                hendelse.addTrafikktiltak(nyttTiltak);
            }
        }
    }

    public void endreValgteTrafikktiltak(TiltakPanel tiltakPanel, JMipssBeanTable<HendelseTrafikktiltak> trafikktiltakTable) {
        Hendelse hendelse = tiltakPanel.getHendelse();
        HendelseTrafikktiltak tiltak = trafikktiltakTable.getSelectedEntities().get(0);
        TiltakDialog dialog = new TiltakDialog((JDialog) SwingUtilities.windowForComponent(tiltakPanel), hendelse, getTrafikktiltak(false));
        dialog.setType(tiltak.getTrafikktiltak());
        dialog.setBeskrivelse(tiltak.getBeskrivelse());
        dialog.doDialogue(trafikktiltakTable);
        HendelseTrafikktiltak editTiltak = dialog.getNewValue();
        if (!dialog.isCancelled()) {
            trafikktiltakTable.clearSelection();
            hendelse.removeTrafikktiltak(tiltak);
            if (StringUtils.isBlank(editTiltak.getBeskrivelse())) {
                editTiltak.setBeskrivelse(null);
            }
            hendelse.addTrafikktiltak(editTiltak);
        }
    }

    public FeltController getFeltController() {
        return controller;
    }

    public Hendelse opprettHendelse(Sak sak) {
        Hendelse h = new Hendelse();
        h.setGuid(ServerUtils.getInstance().fetchGuid());
        h.setNewEntity(true);
        h.setSak(sak);
        return h;
    }

    public Forsikringsskade opprettForsikringsskade(Sak sak) {
        Forsikringsskade f = new Forsikringsskade();
        f.setGuid(ServerUtils.getInstance().fetchGuid());
        f.setNewEntity(true);
        f.setSak(sak);

        return f;

    }

    public Skred opprettSkred(Sak sak) {
        Skred s = new Skred();
        s.setGuid(ServerUtils.getInstance().fetchGuid());
        s.setNewEntity(true);
        s.setSak(sak);

        return s;
    }

    public Long getDriftkontraktIdForRskjema(Long refnummer, ElrappTypeR.Type typeR) {
        return feltService.getDriftkontraktIdForRskjema(refnummer, typeR);
    }

    //flyttet fra "gammel" elrapp-kode
    public String checkContractReadyForElrapp(Driftkontrakt d, Bruker bruker) {
        List<String> str = new ArrayList<String>();
        if (bruker.getElrappIdent() == null) {
            str.add(Resources.getResource("rskjema.manglerBruker", bruker.getSignatur()));
        }

        List<String> kontraktErr = new ArrayList<String>();
        kontraktErr.add(Resources.getResource("rskjema.manglerKonfigurasjon"));
        if (d.getElrappKontraktIdent() == null) {
            kontraktErr.add(Resources.getResource("rskjema.elrappKontraktident"));
        }
        if (d.getElrappSluttDato() == null) {
            kontraktErr.add(Resources.getResource("rskjema.elrappSluttdato"));
        }
        if (d.getElrappStartDato() == null) {
            kontraktErr.add(Resources.getResource("rskjema.elrappStartdato"));
        }
        if (kontraktErr.size() > 1) {
            kontraktErr.add(Resources.getResource("rskjema.elrappSendEpost", EpostUtils.getSupportEpostAdresse()));
            String errmsg = StringUtils.join(kontraktErr.toArray(), "\n");
            str.add(errmsg);
        }
        if (str.size() > 0) {
            return StringUtils.join(str.toArray(), "\n");
        }
        return null;
    }

    public void validateElrappSchema(RskjemaSokResult rs) throws ElrappValidationException {
        Sak sak = controller.getSakController().hentSak(rs.getSakGuid());
        switch (rs.getElrappTypeR()) {
            case R2:
                new R2Validator(sak.getHendelse()).validate();
                break;
            case R5:
                sak.setForsikringsskade(hentForsikringsskade(rs.getGuid()));
                new R5Validator(sak).validate();
                break;
            case R11:
                sak.setSkred(hentSkred(rs.getGuid()));
                new R11Validator(sak, new R11Controller(this)).validate();
                break;
        }
    }

    public MipssFieldDetailItem getDetailItemFromRskjemaSokResult(RskjemaSokResult rs) {
        if (rs.getTypeR().equals("R2")) {
            return rskjemaService.hentHendelse(rs.getGuid());
        }
        if (rs.getTypeR().equals("R5")) {
            return rskjemaService.hentForsikringsskade(rs.getGuid());
        }
        if (rs.getTypeR().equals("R11")) {
            return rskjemaService.hentSkred(rs.getGuid());
        }
        return null;
    }

    public Action getRskjemaNyAction() {
        return buttonController.getRskjemaNyAction();
    }

    public Action getRskjemaSlettAction() {
        return buttonController.getRskjemaSlettAction();
    }

    public Action getRskjemaExcelAction() {
        return buttonController.getRskjemaExcelAction();
    }

    public Action getRskjemaRapportAction() {
        return buttonController.getRskjemaRapportAction();
    }

    public Action getR2RapportAction(HendelseDetaljPanel pnl) {
        return buttonController.getR2RapportAction(pnl);
    }

    public Action getR11RapportAction(SkredDetaljPanel pnl) {
        return buttonController.getR11RapportAction(pnl);
    }

    public Action getRskjemaDetaljerAction() {
        return buttonController.getRskjemaDetaljerAction();
    }

    public Action getRskjemaKartAction() {
        return buttonController.getRskjemaKartAction();
    }

    public Action getRskjemaSokButtonAction() {
        return buttonController.getRskjemaSokButtonAction(this);
    }

    public Action getHendelseMedIdAction() {
        return buttonController.getHendelseMedIdAction();
    }

    public Action getForsikringsskadeMedIdAction() {
        return buttonController.getForsikringsskadeMedIdAction();
    }

    public Action getSkredMedIdAction() {
        return buttonController.getSkredMedIdAction();
    }

    public Action getHendelseMedElrappIdAction() {
        return buttonController.getHendelseMedElrappIdAction();
    }

    public Action getForsikringsskadeMedElrappIdAction() {
        return buttonController.getForsikringsskadeMedElrappIdAction();
    }

    public Action getSkredMedElrappIdAction() {
        return buttonController.getSkredMedElrappIdAction();
    }

    public Action getRskjemaSendElrappAction() {
        return buttonController.getRskjemaSendElrappAction();
    }

    public Action getR2SendElrappAction(HendelseDetaljPanel pnl) {
        return buttonController.getR2SendElrappAction(pnl);
    }

    public Action getR5SendElrappAction(ForsikringsskadeDetaljPanel pnl) {
        return buttonController.getR5SendElrappAction(pnl);
    }

    public Action getR11SendElrappAction(SkredDetaljPanel pnl) {
        return buttonController.getR11SendElrappAction(pnl);
    }

    public RskjemaQueryParams getQueryParams() {
        QueryParams qp = getFeltController().getCommonQueryParams();
        qp.merge(queryParams);
        return queryParams;
    }

    public Action getLeggTilTrafikktiltak(final JMipssBeanTable<HendelseTrafikktiltak> trafikktiltakTable, TiltakPanel tiltakPanel) {
        return buttonController.getLeggTilTrafikktiltakAction(trafikktiltakTable, tiltakPanel);
    }

    public Action getSlettTrafikktiltak(JMipssBeanTable<HendelseTrafikktiltak> trafikktiltakTable, TiltakPanel tiltakPanel) {
        return buttonController.getSlettTrafikktiltak(trafikktiltakTable, tiltakPanel);
    }

    public Action getForsikringsskadeLagreAction(ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel) {
        return buttonController.getForsikringsskadeLagreAction(forsikringsskadeDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign());
    }

    public Action getForsikringsskadeRapportAction(ForsikringsskadeDetaljPanel r5Panel) {
        return buttonController.getForsikringsskadeRapportAction(r5Panel);
    }

    public Action getForsikringsskadeRestoreAction(ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel, Action callback) {
        return buttonController.getForsikringsskadeRestoreAction(forsikringsskadeDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getForsikringsskadeSlettAction(ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel, Action callback) {
        return buttonController.getForsikringsskadeSlettAction(forsikringsskadeDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getSkredLagreAction(SkredDetaljPanel skredDetaljPanel) {
        return buttonController.getSkredLagreAction(skredDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign());
    }

    public Action getSkredRestoreAction(SkredDetaljPanel skredDetaljPanel, Action callback) {
        return buttonController.getSkredRestoreAction(skredDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getSkredSlettAction(SkredDetaljPanel skredDetaljPanel, Action callback) {
        return buttonController.getSkredSlettAction(skredDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getHendelseLagreAction(HendelseDetaljPanel hendelseDetaljPanel) {
        return buttonController.getHendelseLagreAction(hendelseDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign());
    }

    public Action getHendelseRestoreAction(HendelseDetaljPanel hendelseDetaljPanel, Action callback) {
        return buttonController.getHendelseRestoreAction(hendelseDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getHendelseSlettAction(HendelseDetaljPanel hendelseDetaljPanel, Action callback) {
        return buttonController.getHendelseSlettAction(hendelseDetaljPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), callback);
    }

    public Action getEndreProsessAction(VurderingPanel vurderingPanel) {
        return buttonController.getEndreProsessAction(vurderingPanel, vurderingPanel, this);
    }

}
