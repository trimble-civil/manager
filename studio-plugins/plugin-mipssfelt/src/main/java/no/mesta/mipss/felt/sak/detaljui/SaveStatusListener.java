package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.persistence.mipssfield.Sak;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class SaveStatusListener implements PropertyChangeListener {
    List<MipssFieldDetailItem> changedEntitiesList = new ArrayList<MipssFieldDetailItem>();

    @SuppressWarnings("rawtypes")
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("changed".equals(evt.getPropertyName())) {
            MipssFieldDetailItem entity = null;
            if (evt.getSource() instanceof SakDetaljTabPanel) {
                SakDetaljTabPanel pnl = (SakDetaljTabPanel) evt.getSource();
                entity = pnl.getDetailEntity();
            }
            if (evt.getSource() instanceof SaveStatusMonitor) {
                SaveStatusMonitor sm = (SaveStatusMonitor) evt.getSource();
                if (sm.getBeanInstance() instanceof Sak) {
                    entity = (MipssFieldDetailItem) sm.getBeanInstance();
                }
            }

            if (evt.getNewValue().equals(Boolean.TRUE)) {
                changedEntitiesList.add(entity);
            }
            if (evt.getNewValue().equals(Boolean.FALSE)) {
                changedEntitiesList.remove(entity);
            }
        }
    }

    public List<MipssFieldDetailItem> getChangedEntities() {
        return changedEntitiesList;
    }
}