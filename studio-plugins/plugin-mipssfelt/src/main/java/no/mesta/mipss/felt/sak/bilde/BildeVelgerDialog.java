package no.mesta.mipss.felt.sak.bilde;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.resources.images.IconResources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class BildeVelgerDialog extends JDialog {

    private BildeVelgerPanel panel;
    private boolean canceled = true;

    public BildeVelgerDialog(JDialog owner, List<Bilde> bilder) {
        super(owner, true);

        panel = new BildeVelgerPanel(bilder);
        setLayout(new BorderLayout());
        add(panel);
        add(createButtonPanel(), BorderLayout.SOUTH);
        setSize(500, 500);
        setTitle(Resources.getResource("dialog.title.bilder"));

    }

    private JPanel createButtonPanel() {
        JPanel pnlBtn = new JPanel(new GridBagLayout());
        JButton btnOk = new JButton(getOkAction());
        JButton btnCancel = new JButton(getCancelAction());

        pnlBtn.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 10, 5), 0, 0));
        pnlBtn.add(btnCancel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 10, 10), 0, 0));
        return pnlBtn;
    }

    private Action getCancelAction() {
        return new AbstractAction(Resources.getResource("button.avbryt"), IconResources.ABORT_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                canceled = true;
                BildeVelgerDialog.this.dispose();
            }
        };
    }

    private Action getOkAction() {
        return new AbstractAction(Resources.getResource("button.ok"), IconResources.OK2_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                canceled = false;
                BildeVelgerDialog.this.dispose();
            }
        };
    }

    public List<Bilde> getSelectedImages() {
        if (canceled) {
            return new ArrayList<Bilde>();
        }
        return panel.getSelectedImages();
    }
}
