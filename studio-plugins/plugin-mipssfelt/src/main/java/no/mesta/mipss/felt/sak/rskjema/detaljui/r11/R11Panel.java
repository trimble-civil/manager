package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import net.sf.jasperreports.engine.JRException;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.mipssfield.MipssField;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.persistence.mipssfield.r.r11.SkredEgenskapverdi;
import no.mesta.mipss.reports.MipssFieldDS;
import no.mesta.mipss.reports.R11RapportObjekt;
import no.mesta.mipss.resources.images.IconResources;
import org.apache.commons.lang.StringUtils;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Panel for å legge inn/redigere R11 skjema
 *
 * @author <a href="mailto:haraldk@mesan.no">Harald A. Kulø</a>
 */
@SuppressWarnings("serial")
public class R11Panel extends JPanel implements LoadingPanel {

    public static final Long SKREDTYPE_ID = Long.valueOf(1);
    public static final Long LOSNEOMRADE_ID = Long.valueOf(2);
    public static final Long VOLUM_ID = Long.valueOf(3);
    public static final Long HOYDE_ID = Long.valueOf(4);
    public static final Long SKADE_ID = Long.valueOf(5);
    public static final Long STENGING_ID = Long.valueOf(6);
    public static final Long BLOKKERTVEI_ID = Long.valueOf(7);
    public static final Long VIND_ID = Long.valueOf(8);
    public static final Long SNO_ID = Long.valueOf(9);
    public static final Long REGN_ID = Long.valueOf(10);
    public static final Long FELT_ID = Long.valueOf(11);
    public static final Long STENGNINGSRETNING_ID = Long.valueOf(12);
    public static final Long STENGNINGSKRED_ID = Long.valueOf(13);
    private boolean cancelled = true;

    private R11Controller controller;
    private Skred skred;
    private InfoStedPanel pnlInfoSted;
    private SkredSkadePanel pnlSkredSkade;
    private BlokkeringVaerPanel pnlSkredfare;
    private SkredKommentarBildePanel pnlKommentarBilder;

    private JRadioButton radioSkred;
    private JRadioButton radioSkredfare;
    private JLabel lblType;

    private boolean loading = true;
    private JTabbedPane tab;

    public R11Panel(RskjemaController rController) {
        controller = new R11Controller(rController);
        initComponents();
        initGui();
    }

    private void initComponents() {
        pnlInfoSted = new InfoStedPanel(controller);
        pnlSkredSkade = new SkredSkadePanel(controller);
        pnlSkredfare = new BlokkeringVaerPanel(controller);
        pnlKommentarBilder = new SkredKommentarBildePanel(controller);

        tab = new JTabbedPane();
        tab.addTab(Resources.getResource("label.r11.tabHead.info"), pnlInfoSted);
        tab.addTab(Resources.getResource("label.r11.tabHead.blokkering"), pnlSkredfare);
        tab.addTab(Resources.getResource("label.r11.tabHead.bilder"), pnlKommentarBilder);

        lblType = new JLabel(Resources.getResource("label.r11.type"));
        lblType.setFont(new Font(lblType.getFont().getName(), Font.BOLD, lblType.getFont().getSize()));
        radioSkred = new JRadioButton(Resources.getResource("label.skred"));
        radioSkredfare = new JRadioButton(Resources.getResource("label.skredfare"));

        BindingHelper.createbinding(radioSkred, "selected", this, "skredSelected").bind();
        BindingHelper.createbinding(radioSkredfare, "selected", this, "skredfareSelected").bind();


        ButtonGroup bg = new ButtonGroup();
        bg.add(radioSkred);
        bg.add(radioSkredfare);
    }


    public void setSkredSelected(boolean selected) throws PropertyVetoException {
        if (radioSkred.isSelected()) {
            if (!validateChange()) {
                radioSkred.setSelected(false);
                radioSkredfare.setSelected(true);
                return;
            }
            int selectedIndex = tab.getSelectedIndex();
            if (tab.getTabCount() == 3) {
                tab.removeTabAt(1);
            }
            tab.insertTab(Resources.getResource("label.r11.tabHead.skredskade"), null, pnlSkredSkade, null, 1);
            controller.getSkred().setStengtPgaSkredfare(Boolean.FALSE);

            if (selectedIndex == 1) {
                tab.setSelectedIndex(selectedIndex);
            }

        }
    }

    private void cleanSkred() {
        controller.slettSkredEgenskaper(controller.getSkredEgenskaper(), skred);
        skred.setAnslagTotalSkredstorrelse(null);
        pnlSkredSkade.clearSelections();
    }

    private void cleanSkredfare() {
        controller.slettSkredEgenskaper(controller.getSkredfareEgenskaper(), skred);
        skred.setAapnetDato(null);
        skred.setStengtDato(null);
        pnlSkredfare.clearSelections();
    }

    public void setSkredfareSelected(boolean selected) throws PropertyVetoException {
        if (radioSkredfare.isSelected()) {
            if (!validateChange()) {
                radioSkredfare.setSelected(false);
                radioSkred.setSelected(true);
                return;
            }
            int selectedIndex = tab.getSelectedIndex();
            if (tab.getTabCount() == 3) {
                tab.removeTabAt(1);
            }

            tab.insertTab(Resources.getResource("label.r11.tabHead.blokkering"), null, pnlSkredfare, null, 1);
            controller.getSkred().setStengtPgaSkredfare(Boolean.TRUE);
            if (selectedIndex == 1) {
                tab.setSelectedIndex(selectedIndex);
            }
        }

    }

    private boolean validateChange() {
        if (radioSkred.isSelected() && skred.getStengtPgaSkredfare() == Boolean.TRUE) {
            List<String> verdier = getSkredfareVerdier();
            String v = "\n  * " + StringUtils.join(verdier, "\n  * ") + "\n";
            boolean shouldRemoveSkred = displayConfirmChange(verdier, Resources.getResource("message.r11.endreTilSkred.title"), Resources.getResource("message.r11.endreTilSkred", v));
            if (shouldRemoveSkred) {
                cleanSkredfare();
            }
            return shouldRemoveSkred;
        }
        if (radioSkredfare.isSelected() && skred.getStengtPgaSkredfare() == Boolean.FALSE) {
            List<String> verdier = getSkredVerdier();
            String v = "\n  * " + StringUtils.join(verdier, "\n  * ") + "\n";
            boolean shouldRemoveSkredfare = displayConfirmChange(verdier, Resources.getResource("message.r11.endreTilSkredfare.title"), Resources.getResource("message.r11.endreTilSkredfare", v));
            if (shouldRemoveSkredfare) {
                cleanSkred();
            }
            return shouldRemoveSkredfare;

        }
        return true;
    }

    private boolean displayConfirmChange(List<String> verdier, String title, String message) {
        if (verdier.isEmpty()) {
            return true;
        }
        int confirmed = JOptionPane.showConfirmDialog(this, message, title, JOptionPane.OK_CANCEL_OPTION);
        return confirmed == JOptionPane.OK_OPTION;
    }


    private List<String> getSkredVerdier() {
        List<String> verdier = getVerdier(controller.getSkredEgenskaper());
        if (skred.getAnslagTotalSkredstorrelse() != null) {
            String anslag = Resources.getResource("label.r11.anslag");
            verdier.add(anslag + " (" + skred.getAnslagTotalSkredstorrelse() + ")");
        }
        return verdier;
    }

    private List<String> getSkredfareVerdier() {
        List<String> verdier = getVerdier(controller.getSkredfareEgenskaper());
        if (skred.getAapnetDato() != null) {
            String aapnet = Resources.getResource("label.r11.apnet");
            verdier.add(aapnet + " dato (" + MipssDateFormatter.formatDate(skred.getAapnetDato()) + ")");
        }
        if (skred.getStengtDato() != null) {
            String stengt = Resources.getResource("label.r11.stengt");
            verdier.add(stengt + " dato (" + MipssDateFormatter.formatDate(skred.getStengtDato()) + ")");
        }

        return verdier;

//		BindingHelper.createbinding(controller.getSkred(), "aapnetDato", apnetDato, "date", BindingHelper.READ_WRITE).bind();
//		BindingHelper.createbinding(controller.getSkred(), "stengtDato", stengtDato, "date", BindingHelper.READ_WRITE).bind();
    }

    private List<String> getVerdier(List<Egenskap> egenskaper) {
        List<String> verdier = new ArrayList<String>();
        for (Egenskap e : egenskaper) {
            for (SkredEgenskapverdi v : skred.getSkredEgenskapList()) {
                if (v.getEgenskapverdi().getEgenskap().equals(e)) {
                    verdier.add(e.getNavn() + " (" + v.getEgenskapverdi().getVerdi() + ")");
                    break;
                }
            }
        }
        return verdier;
    }


    public R11Controller getController() {
        return controller;
    }

    public void addTab(JPanel pnl, String title) {
        tab.addTab(title, pnl);
    }

    public Skred getSkred() {
        return controller.getSkred();
    }
//	public Skred getOriginalSkred(){
//		return controller.getOriginalSkred();
//	}

    public void setSkred(Skred skred) {
        this.skred = skred;
        controller.setSkred(skred);
        pnlInfoSted.setSkred(skred);
        pnlSkredSkade.setSkred(skred);
        pnlSkredfare.setSkred(skred);
        pnlKommentarBilder.setSkred(skred);
        loading = false;
        if (skred.getStengtPgaSkredfare() != null) {
            if (skred.getStengtPgaSkredfare()) {
                radioSkredfare.setSelected(true);
                cleanSkred();
            } else {
                radioSkred.setSelected(true);
                cleanSkredfare();

            }
        }
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        JLabel tittelLabel = new JLabel(Resources.getResource("label.r11.tittel"));
        Font f = tittelLabel.getFont();
        tittelLabel.setFont(new Font(f.getName(), Font.BOLD, 12));


        JPanel pnlType = new JPanel(new GridBagLayout());
        pnlType.add(lblType, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlType.add(radioSkred, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlType.add(radioSkredfare, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(tittelLabel, new GridBagConstraints(0, 0, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10, 5, 10, 5), 0, 0));
        add(pnlType, new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 5), 0, 0));
        add(tab, new GridBagConstraints(0, 2, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void runWizard() {
        InfoStedPanel inf = new InfoStedPanel(controller);
        SkredSkadePanel skred = new SkredSkadePanel(controller);
        BlokkeringVaerPanel blok = new BlokkeringVaerPanel(controller);

        PageInfo i = new PageInfo(inf);
        PageSkred s = new PageSkred(skred);
        PageBlokkering b = new PageBlokkering(blok);

        WizardPage[] pages = {i, s, b};
        Wizard wizard = WizardPage.createWizard(pages);
        UIManager.put("wizard.sidebar.image", ImageUtil.convertImageToBufferedImage(IconResources.WIZARD_ICON.getImage()));
        new WizardDialog(null, wizard).startWizard();
    }

    class PreviewDS extends MipssFieldDS<Skred> {

        int c = 0;

        public PreviewDS(Skred skred) {
            super(Skred.class, new ArrayList<String>());
            setFieldBean(BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class));
            setEntity(new R11RapportObjekt(skred));
        }

        protected void loadPageData() {
        }

        public boolean next() throws JRException {
            if (c == 0) {
                c++;
                return true;
            } else
                return false;
        }
    }

    static class PageInfo extends WizardPage {
        public static String getDescription() {
            return "Info/Sted";
        }

        public PageInfo(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    static class PageSkred extends WizardPage {
        public static String getDescription() {
            return "Skred og skade";
        }

        public PageSkred(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    static class PageBlokkering extends WizardPage {
        public static String getDescription() {
            return Resources.getResource("label.r11.blokkeringTitle");
        }

        public PageBlokkering(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    private class WizardDialog extends JDialog {
        private final Wizard wizard;

        public WizardDialog(JDialog parent, Wizard wizard) {
            super(parent, true);
            this.wizard = wizard;
        }

        public void startWizard() {
            GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0);
            this.setLayout(new GridBagLayout());
            WizardDisplayer.installInContainer(WizardDialog.this, gbc, wizard, null, null, new WizardResultReceiver() {
                @SuppressWarnings("rawtypes")
                @Override
                public void cancelled(Map arg0) {
                    cancelled = true;
                    dispose();
                }

                @Override
                public void finished(Object arg0) {
//					if (validateForm()){
//						cancelled=false;
                    cancelled = false;
                    dispose();
//					}else{
//						showError(GUITexts.getText(GUITexts.MISSING_FIELD));
//					}
                }
            });
            setSize(new Dimension(825, 500));
            setLocationRelativeTo(null);
            setVisible(true);
        }
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }
}
