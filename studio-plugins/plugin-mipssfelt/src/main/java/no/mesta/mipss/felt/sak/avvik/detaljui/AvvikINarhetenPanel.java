package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.mipssfelt.AvvikAvstand;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import org.jdesktop.swingx.JXHeader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

@SuppressWarnings("serial")
public class AvvikINarhetenPanel extends JPanel {

    private final AvvikController controller;

    private JMipssBeanTable<AvvikAvstand> tblAvvik;
    private MipssBeanTableModel<AvvikAvstand> mdlAvvik;
    private JMipssBeanScrollPane scrAvvik;
    private JButton btnLukk;
    private JButton btnApne;

    public AvvikINarhetenPanel(AvvikController controller, Avvik avvik) {
        this.controller = controller;

        initComponents();
        initGui();
        mdlAvvik.setBeanMethodArgValues(new Object[]{avvik});
        mdlAvvik.loadData();
    }

    private void initTable() {
        mdlAvvik = new MipssBeanTableModel<AvvikAvstand>(AvvikAvstand.class, "refnummer", "avstand", "registrertDato", "registrertAv", "prosessKode", "veireferanse", "status", "x", "y", "beskrivelse");
        MipssRenderableEntityTableColumnModel avvikColumnModel = new MipssRenderableEntityTableColumnModel(AvvikAvstand.class, mdlAvvik.getColumns());
        avvikColumnModel.getColumn(2).setCellRenderer(new DateTimeRenderer(MipssDateFormatter.DATE_FORMAT));

        mdlAvvik.setBeanInstance(controller);
        mdlAvvik.setBeanMethod("sokAvvikINarheten");
        mdlAvvik.setBeanInterface(AvvikController.class);
        mdlAvvik.setBeanMethodArguments(new Class<?>[]{Avvik.class});

        tblAvvik = new JMipssBeanTable<AvvikAvstand>(mdlAvvik, avvikColumnModel);
        tblAvvik.setDefaultRenderer(Date.class, new DateTimeRenderer());
        tblAvvik.setFillsViewportHeight(true);
        tblAvvik.setFillsViewportWidth(true);
        tblAvvik.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        tblAvvik.setColumnControlVisible(true);
        tblAvvik.addColumnStateSupport("avvikINarhetenTable", controller.getFeltController().getPlugin());
        tblAvvik.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    openValgte();
                }
            }
        });
        scrAvvik = new JMipssBeanScrollPane(tblAvvik, mdlAvvik);
    }

    private void openValgte() {
        AvvikAvstand avvik = mdlAvvik.get(tblAvvik.convertRowIndexToModel(tblAvvik.getSelectedRow()));
        AvvikSokResult asr = new AvvikSokResult();
        asr.setGuid(avvik.getGuid());
        asr.setSakGuid(avvik.getSakGuid());
        asr.setSlettetDato(avvik.getSlettetDato());
        controller.openAvvik(asr);
    }

    private void initComponents() {
        initTable();

        btnLukk = new JButton(getLukkAction());
        btnApne = new JButton(getOpenAction());
    }

    private Action getLukkAction() {
        return new AbstractAction(Resources.getResource("button.lukk"), IconResources.CANCEL_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.windowForComponent(AvvikINarhetenPanel.this).dispose();
            }
        };
    }

    private Action getOpenAction() {
        return new AbstractAction(Resources.getResource("button.apne"), IconResources.OK2_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                openValgte();
            }
        };
    }

    private JXHeader createHeader() {
        String avstand = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.field", "avstandAvvikINarheten").getVerdi();
        return new JXHeader(Resources.getResource("header.avvikINarheten.title"), Resources.getResource("header.avvikINarheten.description", avstand), IconResources.FUNN_ICON);
    }

    private void initGui() {
        setLayout(new BorderLayout());

        JPanel pnlButtons = new JPanel(new GridBagLayout());
        pnlButtons.add(btnApne, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
        pnlButtons.add(btnLukk, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
        JXHeader header = createHeader();
        add(header, BorderLayout.NORTH);
        add(scrAvvik, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);


    }
}
