package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.felt.sak.avvik.AvvikController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AvvikDetaljButtonPanel extends JPanel {
    private JButton btnLagre;
    private JButton btnSlett;
    private JButton btnRapport;
    private JButton btnLeggTilKommentar;
    private JButton btnAvvikINarheten;

    private final AvvikController controller;
    private final AvvikDetaljPanel avvikDetaljPanel;

    public AvvikDetaljButtonPanel(AvvikController controller, AvvikDetaljPanel avvikDetaljPanel) {
        this.controller = controller;
        this.avvikDetaljPanel = avvikDetaljPanel;
        initComponents();
        initGui();
    }

    public void setKommentarButtonVisible(boolean visible) {
        btnLeggTilKommentar.setVisible(visible);
    }

    public void setGjenopprett(boolean gjenopprett) {
        if (gjenopprett) {
            btnSlett.setAction(controller.getAvvikGjenopprettAction(avvikDetaljPanel, getRestoreAvvikCallback()));
        } else {
            btnSlett.setAction(controller.getAvvikSlettAction(avvikDetaljPanel, getSlettAvvikCallback()));
        }
    }

    private void initComponents() {
        btnLagre = new JButton(controller.getAvvikLagreAction(avvikDetaljPanel));
        btnLagre.setEnabled(false);
        btnSlett = new JButton(controller.getAvvikSlettAction(avvikDetaljPanel, getSlettAvvikCallback()));
        btnRapport = new JButton(controller.getAvvikRapportAction(avvikDetaljPanel));
        btnLeggTilKommentar = new JButton(controller.getLeggTilKommentarAction(avvikDetaljPanel));
        btnAvvikINarheten = new JButton(controller.getVisAvvikINarhetenAction(avvikDetaljPanel));

    }

    private Action getSlettAvvikCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                avvikDetaljPanel.entityDeleted();
            }
        };
    }

    private Action getRestoreAvvikCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                avvikDetaljPanel.entityRestored();
                setGjenopprett(false);
            }
        };
    }

    public void setEnabled(boolean enabled) {
        btnLagre.setEnabled(enabled);
    }

    public void setLukket(boolean lukketAvvik) {
        btnSlett.setEnabled(!lukketAvvik);
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        add(btnSlett, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(btnLeggTilKommentar, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnAvvikINarheten, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnRapport, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnLagre, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    }
}
