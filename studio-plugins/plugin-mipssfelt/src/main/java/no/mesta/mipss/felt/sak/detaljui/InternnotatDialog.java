package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.StringNullConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.mipssfelt.SakService;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.resources.images.IconResources;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

@SuppressWarnings("serial")
public class InternnotatDialog extends JDialog {

    private final SakDetaljPanel sakDetaljPanel;
    private JTextArea txtNotat;
    private JButton btnLukk;
    private JScrollPane scrNotat;
    private Binding<Sak, String, JTextArea, String> notatBind;
    private final SakController controller;


    public InternnotatDialog(JDialog owner, String title, SakDetaljPanel sakDetaljPanel, SakController controller) {
        super(owner, title);
        this.sakDetaljPanel = sakDetaljPanel;
        this.controller = controller;
        initComponents();
        initGui();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                notatBind.unbind();
            }
        });
    }

    private void initComponents() {
        txtNotat = new JTextArea();
        btnLukk = new JButton(getLukkAction());
        scrNotat = new JScrollPane(txtNotat);

        notatBind = BindingHelper.createbinding(sakDetaljPanel.getSak(), "internnotat", txtNotat, "text", UpdateStrategy.READ_WRITE);
        notatBind.setConverter(new StringNullConverter());
        notatBind.bind();


    }

    private Action getLukkAction() {
        return new AbstractAction(Resources.getResource("button.lukk"), IconResources.OK2_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                notatBind.unbind();
                InternnotatDialog.this.dispose();
                BeanUtil.lookup(SakService.BEAN_NAME, SakService.class).persistInternnotat(sakDetaljPanel.getSak().getGuid(), txtNotat.getText());
                controller.reloadTable();
            }
        };
    }

    private void initGui() {
        setLayout(new BorderLayout());
        JPanel pnlButton = new JPanel(new BorderLayout());
        pnlButton.add(btnLukk, BorderLayout.EAST);
        pnlButton.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));


        add(scrNotat, BorderLayout.CENTER);
        add(pnlButton, BorderLayout.SOUTH);

    }


}
