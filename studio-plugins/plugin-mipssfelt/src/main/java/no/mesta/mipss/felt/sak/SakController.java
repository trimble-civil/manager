package no.mesta.mipss.felt.sak;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.detaljui.SakDetaljDialog;
import no.mesta.mipss.felt.sak.detaljui.SakDetaljPanel;
import no.mesta.mipss.felt.tableHighlighter.ColorHighlighterFactory;
import no.mesta.mipss.mipssfelt.*;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.*;
import no.mesta.mipss.valueobjects.ElrappStatus;
import no.mesta.mipss.valueobjects.ElrappTypeR.Type;
import org.jdesktop.swingx.decorator.Highlighter;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;

public class SakController {


    private final FeltController controller;
    private final SakButtonController buttonController;
    private SakQueryParams queryParams;
    private ManglerQueryParams manglerQueryParams;

    private MipssBeanTableModel<SakSokResult> mdlSak;
    private JMipssBeanTable<SakSokResult> tblSak;
    private SakService sakService;
    private MipssFelt feltBean;

    public SakController(FeltController controller) {
        this.controller = controller;
        buttonController = new SakButtonController(this);
        sakService = BeanUtil.lookup(SakService.BEAN_NAME, SakService.class);
        feltBean = BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class);
        queryParams = new SakQueryParams();
        bindQueryParams(controller.getCommonQueryParams());

//		initManglerQuery();
        initSakTable();
    }

    public ManglerQueryParams getManglerQueryParams() {
        return manglerQueryParams;
    }
//	private void initManglerQuery(){
//		manglerQueryParams = new ManglerQueryParams();
//		manglerQueryParams.setAlle(Boolean.FALSE);
//		BindingHelper.createbinding(controller.getCommonQueryParams(), "valgtKontrakt", this, "driftkontraktForMangler").bind();
//	}
//	public void setDriftkontraktForMangler(Driftkontrakt driftkontrakt){
//		manglerQueryParams.setDriftkontrakt(driftkontrakt);
//		if (getFeltController().getAvvikController()==null)
//			return;
//		MipssBeanTableModel<AvvikSokResult> tbm = getFeltController().getAvvikController().getManglerTableModel();
//		if (driftkontrakt!=null){
//			tbm.setBeanMethodArgValues(new Object[]{manglerQueryParams});
//			tbm.loadData();
//		}else{
//			if (tbm.getEntities()!=null&&tbm.getEntities().size()>0)
//				tbm.getEntities().clear();
//		}
//	}

    private void bindQueryParams(QueryParams commonQueryParams) {
        BindingHelper.createbinding(commonQueryParams, "dateFra", queryParams, "dateFra").bind();
        BindingHelper.createbinding(commonQueryParams, "dateTil", queryParams, "dateTil").bind();
        BindingHelper.createbinding(commonQueryParams, "brukerList", queryParams, "brukerList").bind();
        BindingHelper.createbinding(commonQueryParams, "pdaDfuIdentList", queryParams, "pdaDfuIdentList").bind();
        BindingHelper.createbinding(commonQueryParams, "valgtKontrakt", queryParams, "valgtKontrakt").bind();
        BindingHelper.createbinding(commonQueryParams, "veiListe", queryParams, "veiListe").bind();
        BindingHelper.createbinding(commonQueryParams, "prosesser", queryParams, "prosesser").bind();
    }

    /**
     * Returnerer modellen til tabellen som inneholder sakene fra søk
     *
     * @return
     */
    public MipssBeanTableModel<SakSokResult> getSakTableModel() {
        return mdlSak;
    }

    /**
     * Returnerer tabellen som inneholder sakene fra søk
     */
    public JMipssBeanTable<SakSokResult> getSakTable() {
        return tblSak;
    }

    /**
     * Henter id til kontrakten som tilhører saken med angitt refnummer
     *
     * @param refnummer
     * @return
     */
    public Long getDriftkontraktIdForSak(Long refnummer) {
        return feltBean.getDriftkontraktIdForSak(refnummer);
    }

    /**
     * Viser en ventedialog og åpner saken i en dialog
     *
     * @param sakResult
     */
    public void openSak(final SakSokResult sakResult) {
        openSak(sakResult.getGuid(), false);
    }

    public void openSak(final String sakGuid, final boolean lastSlettede) {
        final JDialog waitDialog = controller.showWaitDialog(Resources.getResource("label.henterSak"));
        new SwingWorker<Void, Void>() {
            private Sak sak;

            public Void doInBackground() {
                sak = hentSak(sakGuid);
                return null;
            }

            @Override
            public void done() {
                openSak(sak, lastSlettede);
                waitDialog.dispose();
            }
        }.execute();
    }

    private Map<String, SakDetaljDialog> dialogManager = new HashMap<String, SakDetaljDialog>();

    public void openSak(final String sakGuid, final String[] lastSlettedeGuid, final String selectedGuid) {
        if (dialogManager.get(sakGuid) != null) {
            SakDetaljDialog det = dialogManager.get(sakGuid);
            det.setSelectedGuid(selectedGuid);
            det.requestFocus();
            return;
        }
        final JDialog waitDialog = controller.showWaitDialog(Resources.getResource("label.henterSak"));
        new SwingWorker<Void, Void>() {
            private Sak sak;

            public Void doInBackground() {
                try {
                    sak = hentSak(sakGuid);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;

            }

            @Override
            public void done() {
                openSak(sak, lastSlettedeGuid, selectedGuid);
                waitDialog.dispose();
            }
        }.execute();
    }

    public void closeDialogForSak(String sakGuid) {
        SakDetaljDialog sakDialog = dialogManager.get(sakGuid);
        if (sakDialog != null) {
            sakDialog.dispose();
            dialogManager.remove(sakGuid);
        }

    }

    /**
     * Viser en ventedialog og åpner saken i en dialog
     *
     * @param refnummer
     */
    public void openSak(final Long refnummer) {
        final JDialog waitDialog = controller.showWaitDialog(Resources.getResource("label.henterSak"));
        new SwingWorker<Void, Void>() {
            private Sak sak;

            public Void doInBackground() {
                sak = hentSak(refnummer);
                return null;
            }

            @Override
            public void done() {
                waitDialog.dispose();
                if (sak != null) {
                    openSak(sak, false);
                } else {
                    JOptionPane.showMessageDialog(controller.getPlugin().getLoader().getApplicationFrame(), Resources.getResource("warning.sakFinnesIkke", refnummer + ""), Resources.getResource("warning.sakFinnesIkke.title"), JOptionPane.WARNING_MESSAGE);
                }

            }
        }.execute();
    }

    /**
     * Åpner saken i en dialog
     *
     * @param sak
     */
    public void openSak(Sak sak, boolean lastSlettede) {
        SakDetaljDialog dialog = new SakDetaljDialog(this, sak, lastSlettede);
        dialog.setLocationRelativeTo(getFeltController().getPlugin().getLoader().getApplicationFrame());
        dialog.setVisible(true);
    }

    /**
     * Åpner saken i en dialog og velger fanen som inneholder registreringen med guid = selectedGuid
     *
     * @param sak
     */
    private void openSak(final Sak sak, String[] lastSlettedeGuid, String selectedGuid) {
        SakDetaljDialog dialog = new SakDetaljDialog(this, hentSak(sak.getGuid()), lastSlettedeGuid, selectedGuid);
        dialog.setLocationRelativeTo(getFeltController().getPlugin().getLoader().getApplicationFrame());
        dialog.setVisible(true);
        dialogManager.put(sak.getGuid(), dialog);
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dialogManager.remove(sak.getGuid());
            }
        });
    }

    /**
     * Henter en sak med angitt guid
     *
     * @param guid
     * @return
     */
    public Sak hentSak(String guid) {
        try {
            Sak sak = sakService.hentSak(guid);
            return sak;
        } catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }

    /**
     * Henter en sak med angitt refnummer
     *
     * @param refnummer
     * @return
     */
    public Sak hentSak(Long refnummer) {
        Sak sak = sakService.hentSak(refnummer);
        return sak;
    }

    public SakDetaljDialog aapneNySakDialog(Sak nySak) {
        SakDetaljDialog dialog = new SakDetaljDialog(this, nySak, false);
        dialog.setLocationRelativeTo(getFeltController().getPlugin().getLoader().getApplicationFrame());
        dialog.setVisible(true);
        return dialog;
    }

    public void opprettNySak() {
        SakDetaljDialog dialog = new SakDetaljDialog(this, opprettSak(), false);
        dialog.setLocationRelativeTo(getFeltController().getPlugin().getLoader().getApplicationFrame());
        dialog.setVisible(true);
    }

    public Sak opprettSak() {
        Sak sak = new Sak();
        sak.setNewEntity(true);
        sak.setKontrakt(controller.getPlugin().getController().getCommonQueryParams().getValgtKontrakt());
        sak.setOpprettetAv(controller.getPlugin().getLoader().getLoggedOnUserSign());
        sak.setOpprettetDato(Clock.now());
        sak.setGuid(ServerUtils.getInstance().fetchGuid());
        sak.setPdaDfuIdent(Long.valueOf(-1));
        List<Punktstedfest> stedfesting = new ArrayList<Punktstedfest>();
        Punktstedfest ps = new Punktstedfest();
        ps.setSak(sak);
        ps.setGuid(ServerUtils.getInstance().fetchGuid());

        Punktveiref pv = new Punktveiref();
        pv.setPunktstedfest(ps);
        pv.setFraDato(Clock.now());

        ps.getVeireferanser().add(pv);
        stedfesting.add(ps);
        sak.setStedfesting(stedfesting);
        return sak;
    }

    /**
     * Henter minimalt med informasjon om knytningene til en sak.
     *
     * @param sakGuid
     * @return
     */
    public List<FeltEntityInfo> hentEntityInfoForSakAvvik(String sakGuid) {
        return feltBean.hentEntityInfoAvvik(sakGuid);
    }

    /**
     * Henter minimalt med informasjon om knytningene til en sak.
     *
     * @param sakGuid
     * @return
     */
    public List<FeltEntityInfo> hentEntityInfoForSakHendelse(String sakGuid) {
        return feltBean.hentEntityInfoHendelse(sakGuid);
    }

    /**
     * Henter minimalt med informasjon om knytningene til en sak.
     *
     * @param sakGuid
     * @return
     */
    public List<FeltEntityInfo> hentEntityInfoForSakForsikringsskade(String sakGuid) {
        return feltBean.hentEntityInfoForsikringsskade(sakGuid);
    }

    /**
     * Henter minimalt med informasjon om knytningene til en sak.
     *
     * @param sakGuid
     * @return
     */
    public List<FeltEntityInfo> hentEntityInfoForSakSkred(String sakGuid) {
        return feltBean.hentEntityInfoSkred(sakGuid);
    }

    private void initSakTable() {
        mdlSak = new MipssBeanTableModel<SakSokResult>(SakSokResult.class, "refnummer", "opprettetDato", "opprettetAv", "sisteStatusDato",
                "sisteStatus", "tiltaksDato", "antallAvvik", "r2Status",
                "r5Status", "r11Status", "tilleggsarbeid", "etterslep",
                "fylkesnummer", "kommunenummer", "veitype", "veinummer", "hp", "meter", "skadested",
                "prosesserText", "pdaDfuIdent", "internnotat");
        MipssRenderableEntityTableColumnModel avvikColumnModel = new MipssRenderableEntityTableColumnModel(
                SakSokResult.class, mdlSak.getColumns());

        mdlSak.setBeanInstance(sakService);
        mdlSak.setBeanMethod("sokSaker");
        mdlSak.setBeanInterface(SakService.class);
        mdlSak.setBeanMethodArguments(new Class<?>[]{SakQueryParams.class});

        tblSak = new JMipssBeanTable<SakSokResult>(mdlSak, avvikColumnModel);
        tblSak.setDefaultRenderer(Date.class, new DateTimeRenderer());
        tblSak.setDefaultRenderer(Boolean.class, new BooleanRenderer());
        tblSak.setDefaultRenderer(Double.class, new DoubleRenderer());
        tblSak.setDefaultRenderer(ElrappStatus.class, new ElrappStatusTableCellRenderer());
        tblSak.setFillsViewportHeight(true);
        tblSak.setFillsViewportWidth(true);
        tblSak.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        tblSak.setColumnControlVisible(true);
        tblSak.addColumnStateSupport("sakTable", controller.getPlugin());
        tblSak.loadUserSettings();
        tblSak.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (tblSak.getSelectedRow() == -1)
                    return;
                if (e.getClickCount() == 2) {
                    openSak(mdlSak.get(tblSak.convertRowIndexToModel(tblSak.getSelectedRow())));
                }
            }
        });
        ColorHighlighterFactory fact = new ColorHighlighterFactory();
        tblSak.setHighlighters(new Highlighter[]{
                fact.getSakUtenAvvikHighlighter(this),
                fact.getFristdatoWarningHighlighter(this),
                fact.getFristdatoOverskredetHighlighter(this)
        });
        tblSak.loadUserSettings();
    }


    public Sak persistSak(Sak sak) {
        if (!validateSak(sak)) {
            return null;
        }
        String brukerSign = controller.getLoggedOnUserSign();
        Sak s = sakService.persistSak(sak, brukerSign);
        s.setNewEntity(false);
        reloadTable();
        return s;
    }

    public void reloadTable() {
        final int selectedRow = getSakTable().getSelectedRow();
        getSakTableModel().loadData();
        getSakTableModel().addTableLoaderListener(new MipssBeanLoaderListener() {
            @Override
            public void loadingStarted(MipssBeanLoaderEvent event) {
            }

            @Override
            public void loadingFinished(MipssBeanLoaderEvent event) {
                getSakTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
            }
        });
    }

    private boolean validateSak(Sak sak) {
        if (sak.getVeiref() == null || sak.getVeiref().getVeikategori() == null) {
            JOptionPane.showMessageDialog(null, "Saken mangler veireferanse", "Mangler veireferanse", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public FeltController getFeltController() {
        return controller;
    }

    public Action getSakSokButtonAction() {
        return buttonController.getSakSokButtonAction(this);
    }

    public Action getSakHentMedIdAction() {
        return buttonController.getSakHentMedIdAction();
    }

    public Action getSakNyAction() {
        return buttonController.getSakNyAction();
    }

    public Action getSakSlettAction(SakPanel panel) {
        return buttonController.getSakSlettAction(panel);
    }

    public Action getSakExcelAction() {
        return buttonController.getSakExcelAction();
    }

    public Action getSakDetaljerAction() {
        return buttonController.getSakDetaljerAction();
    }

    public Action getEndreVeiAction(SakDetaljPanel sakPanel) {
        return buttonController.getEndreVeiAction(sakPanel);
    }

    public Action getOpenInspeksjonAction(SakDetaljPanel sakPanel) {
        return buttonController.getOpenInspeksjonAction(sakPanel);
    }

    public Action getLagreSakAction(SakDetaljPanel sakPanel) {
        return buttonController.getLagreSakAction(sakPanel, controller.getPlugin().getLoader().getLoggedOnUserSign());
    }

    public SakQueryParams getQueryParams() {
//		QueryParams qp = getFeltController().getCommonQueryParams();
//		qp.merge(queryParams);
        return queryParams;
    }

    public Action getOpprettRskjemaAction(SakDetaljPanel sakPanel, Type rSkjema) {
        return buttonController.getOpprettRskjemaAction(sakPanel, controller.getPlugin().getLoader().getLoggedOnUserSign(), rSkjema);
    }

    public Action getOpprettAvvikAcion(SakDetaljPanel sakPanel) {
        return buttonController.getOpprettAvvikAction(sakPanel, controller.getPlugin().getLoader().getLoggedOnUserSign());
    }

    public Action getVisInternnotatAction(SakDetaljPanel sakDetaljPanel) {
        return buttonController.getVisInternnotatAction(sakDetaljPanel);
    }

    public Action getSakAvvikRapportAction() {
        return buttonController.getSakAvvikRapportAction();
    }

    public Action getSakRskjemaRapportAction(Type rSkjema) {
        return buttonController.getSakRskjemaRapportAction(rSkjema);
    }


}
