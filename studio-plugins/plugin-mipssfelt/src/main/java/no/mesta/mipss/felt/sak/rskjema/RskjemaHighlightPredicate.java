package no.mesta.mipss.felt.sak.rskjema;

import no.mesta.mipss.mipssfelt.RskjemaSokResult;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import no.mesta.mipss.valueobjects.ElrappTypeR.Type;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class RskjemaHighlightPredicate implements HighlightPredicate {

    private MipssRenderableEntityTableModel<RskjemaSokResult> tableModel;
    private JMipssBeanTable<RskjemaSokResult> table;
    private final Type typeR;

    public RskjemaHighlightPredicate(Type typeR, MipssRenderableEntityTableModel<RskjemaSokResult> tableModel, JMipssBeanTable<RskjemaSokResult> table) {
        this.typeR = typeR;
        this.tableModel = tableModel;
        this.table = table;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        int row = adapter.row;

        int rowModel = table.convertRowIndexToModel(row);

        RskjemaSokResult rskjema = tableModel.get(rowModel);

        boolean highlight = false;
        if (rskjema != null) {
            switch (typeR) {
                case R2:
                    highlight = rskjema.getTypeR().equals("R2");
                    break;
                case R5:
                    highlight = rskjema.getTypeR().equals("R5");
                    break;
                case R11:
                    highlight = rskjema.getTypeR().equals("R11");
                    break;
            }
        }
        return highlight;
    }
}
