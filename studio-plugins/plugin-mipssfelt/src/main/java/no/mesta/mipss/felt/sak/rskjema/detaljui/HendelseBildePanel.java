package no.mesta.mipss.felt.sak.rskjema.detaljui;

import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.bilde.BildeDatoPanel;
import no.mesta.mipss.felt.sak.bilde.BildeVelgerDialog;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.felt.util.ImageNameFilter;
import no.mesta.mipss.felt.util.ImageRotator;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.AvvikstatusType;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;
import no.mesta.mipss.ui.popuplistmenu.JPopupListMenu;
import org.apache.commons.io.IOUtils;
import org.jdesktop.swingx.JXImageView;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.CheckerboardPainter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;

@SuppressWarnings("serial")
public class HendelseBildePanel extends JXPanel implements LoadingPanel {

    protected JScrollPane picturesPane = new JScrollPane();
    protected JXPanel pnlImage;
    protected boolean loading = true;
    protected JLabel lblAntallBilder;
    protected final RskjemaController controller;
    protected Bilde activeImage;
    private Hendelse hendelse;

    protected String sakGuid;


    public HendelseBildePanel(RskjemaController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    public void setHendelse(Hendelse hendelse) {
        this.hendelse = hendelse;
        setHendelseBilder(hendelse.getBilder());
        sakGuid = hendelse.getSakGuid();
    }

    protected List<JXImageView> imageViews = new ArrayList<JXImageView>();
    protected JComponent buttonPanel;
    protected JLayeredPane layeredPane;

    private void setHendelseBilder(List<Bilde> bildeList) {
        for (JXImageView v : imageViews) {
            pnlImage.remove(v);
        }
        for (Bilde bilde : bildeList) {
            Image image = bilde.getSmaaImage();
            final JXImageView view = new JXImageView();
            imageViews.add(view);
            view.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
            if (image == null) {
                image = bilde.getImage();
                if (image == null)
                    image = IconResources.MISSING_ICON.getImage();
            }
            if (bilde.getBeskrivelse() != null) {
                view.setToolTipText("<html><pre>" + bilde.getBeskrivelse() + "</pre></html>");
            }
            view.setPreferredSize(new Dimension(200, 180));
            view.setImage(image);
            view.setDragEnabled(false);
            view.setEditable(false);
            view.addMouseListener(new ImageClickListener(view, bilde));

            view.setLayout(new BorderLayout());

            pnlImage.add(view);
        }
        lblAntallBilder.setText(" Antall bilder: " + bildeList.size());
        picturesPane.setViewportView(pnlImage);
        picturesPane.getHorizontalScrollBar().setUnitIncrement(205);
        updateSize();
        loading = false;
    }


    public void dispose() {
        for (JXImageView v : imageViews) {
            for (MouseListener ml : v.getMouseListeners())
                v.removeMouseListener(ml);
            v.removeAll();
        }
        imageViews.clear();
        picturesPane.removeAll();
        pnlImage.removeAll();
    }

    protected void clearBorders() {
        Component[] components = pnlImage.getComponents();
        for (Component c : components) {
            if (c instanceof JXImageView) {
                JXImageView imageView = (JXImageView) c;
                imageView.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
            }
        }
        repaint();
    }

    private void initComponents() {
        pnlImage = new JXPanel(new FlowLayout());
        pnlImage.setBackground(Color.WHITE);
        pnlImage.setScrollableTracksViewportWidth(false);

        lblAntallBilder = new JLabel(" Antall bilder:");
        lblAntallBilder.setBackground(new Color(0f, 0f, 0f, 0.75f));
        lblAntallBilder.setOpaque(true);
        lblAntallBilder.setForeground(Color.WHITE);

        buttonPanel = createButtonsPanel();

        layeredPane = new JLayeredPane();
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                updateSize();
            }

            public void componentMoved(ComponentEvent e) {
                updateSize();
            }
        });
        updateSize();
        layeredPane.add(picturesPane, new Integer(0));
        layeredPane.add(lblAntallBilder, new Integer(1));
        layeredPane.add(buttonPanel, new Integer(2));
    }

    protected void updateSize() {
        int w = getWidth();
        int h = layeredPane.getHeight();
        picturesPane.setBounds(new Rectangle(0, 0, w, h));
        lblAntallBilder.setBounds(5, h - 40, 90, 15);
        buttonPanel.setBounds(w - 305, h - 44, 300, 20);
//		
        if (hendelse != null) {
            if (hendelse.getBilder() == null || hendelse.getBilder().size() < 2) {
                pnlImage.setPreferredSize(new Dimension(w, pnlImage.getHeight()));
            } else
                pnlImage.setPreferredSize(null);
        } else
            pnlImage.setPreferredSize(null);
    }

    private JComponent createButtonsPanel() {
        Box buttonPanel = Box.createHorizontalBox();

        JButton delete = new JButton(getSlettBildeAction());
        JButton open = new JButton(getOpenBildeAction());
        JButton rotate = new JButton(getRoterBildeAction());
        List<JMenuItem> menuItems = new ArrayList<JMenuItem>();
        menuItems.add(new JMenuItem(getAddBildeFraFilAction()));
        menuItems.add(new JMenuItem(getAddBildeFraSakAction()));
        JPopupListMenu btnAdd = new JPopupListMenu(Resources.getResource("button.leggTil"), null, menuItems);

        buttonPanel.add(Box.createHorizontalGlue());
        if (controller.getFeltController().getPlugin().hasWriteAccess()) {
            buttonPanel.add(rotate);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
            buttonPanel.add(delete);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
            buttonPanel.add(btnAdd);
            buttonPanel.add(BoxUtil.createHorizontalStrut(2));
        }
        buttonPanel.add(open);

        return buttonPanel;
    }

    private AbstractAction getAddBildeFraFilAction() {
        return new AbstractAction(Resources.getResource("button.fraFil")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                addPicture();
            }
        };
    }

    protected AbstractAction getAddBildeFraSakAction() {
        return new AbstractAction(Resources.getResource("button.fraSak")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Bilde> bilderFromSak = controller.getFeltController().getBilderFromSak(sakGuid);
                List<Bilde> cleaned = new ArrayList<Bilde>();
                for (Bilde b : bilderFromSak) {
                    if (!hendelse.getBilder().contains(b)) {
                        cleaned.add(b);
                    }
                }
                Window windowForComponent = SwingUtilities.windowForComponent(HendelseBildePanel.this);
                BildeVelgerDialog dialog = new BildeVelgerDialog((JDialog) windowForComponent, cleaned);

                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
                List<Bilde> selectedImages = dialog.getSelectedImages();
                if (!selectedImages.isEmpty()) {
                    for (Bilde b : selectedImages) {
                        hendelse.addBilde(b);
                        activeImage = null;
                        setBilder();
                    }
                }
            }
        };
    }

    private void rotateImage() {
        if (activeImage != null) {
            Bilde active = activeImage;
            ImageRotator.rotate90(activeImage, controller.getFeltController().getLoggedOnUserSign());
            setBilder();
            for (JXImageView v : imageViews) {
                if (v.getImage().equals(activeImage.getImage()) || v.getImage().equals(activeImage.getSmaaImage())) {
                    selectView(v);
                }
            }
            activeImage = active;
            oppdaterEndrer();
        }
    }

    protected void setBilder() {
        setHendelseBilder(hendelse.getBilder());
    }

    protected void oppdaterEndrer() {
        hendelse.getOwnedMipssEntity().setEndretAv(controller.getFeltController().getLoggedOnUserSign());
        hendelse.getOwnedMipssEntity().setEndretDato(Clock.now());
    }

    private AbstractAction getRoterBildeAction() {
        return new AbstractAction("", IconResources.ROTATECW_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                rotateImage();
            }
        };
    }

    @SuppressWarnings("unused")
    private AbstractAction getAddBildeAction() {
        return new AbstractAction(Resources.getResource("button.leggTil")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                addPicture();
            }
        };
    }

    protected void addPicture() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setLocale(new Locale("no", "NO"));
        fileChooser.setFileFilter(new ImageNameFilter());
        int what = fileChooser.showOpenDialog(this);
        if (what == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            Bilde b = createBilde(file);
            if (b == null) {
                setHendelseBilder(hendelse.getBilder());
                return;
            }
            hendelse.addBilde(b);
            activeImage = null;
            setHendelseBilder(hendelse.getBilder());
        }
    }

    public void addBilde(final Bilde b) {
        BildeDatoPanel pnlDato = new BildeDatoPanel();
        JDialog d = new JDialog(SwingUtilities.windowForComponent(this));
        d.setTitle("Velg bildedato");
        d.setModal(true);
        d.add(pnlDato);
        d.setSize(220, 120);
        d.setLocationRelativeTo(this);
        d.setVisible(true);
        if (pnlDato.isCancelled())
            return;

        Date dato = pnlDato.getDato();
        b.setNyDato(dato);

        hendelse.addBilde(b);
        activeImage = null;
        setHendelseBilder(hendelse.getBilder());
    }

    private AbstractAction getOpenBildeAction() {
        return new AbstractAction(Resources.getResource("button.apne")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (activeImage != null) {
                    openImage();
                }
            }
        };
    }

    protected void openImage() {
        BildePictureModel model = new BildePictureModel();
        model.setBilder(hendelse.getBilder());
        int imageIndex = model.indexOf(activeImage);
        JDialog dialog = (JDialog) SwingUtilities.windowForComponent(this);
        JPictureDialogue picture = new JPictureDialogue(dialog, model);
        picture.showBilde(imageIndex);
        picture.setVisible(true);
    }

    private AbstractAction getSlettBildeAction() {
        return new AbstractAction(Resources.getResource("button.slett")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (activeImage != null) {
                    int a = JOptionPane.showConfirmDialog(HendelseBildePanel.this, Resources.getResource("warning.slettBilde"), Resources.getResource("warning.slettBilde.title"), JOptionPane.YES_NO_OPTION);
                    if (a == JOptionPane.YES_OPTION) {
                        slettActiveImage();
                    }
                }
            }
        };
    }

    protected void slettActiveImage() {
        hendelse.removeBilde(activeImage);
        activeImage = null;
        setHendelseBilder(hendelse.getBilder());
    }

    private void initGui() {

        setLayout(new GridBagLayout());
        add(layeredPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    protected Bilde createBilde(File file) {
        Bilde b = new Bilde();
        b.setGuid(ServerUtils.getInstance().fetchGuid());
        b.setNyDato(Clock.now());
        b.setOppretter(controller.getFeltController().getPlugin().getLoader().getLoggedOnUserSign());
        b.setFilnavn(file.getName());
        try {
            byte[] imageLob = IOUtils.toByteArray(new FileInputStream(file));
            b.setBildeLob(imageLob);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        b.setSmaabildeLob(ImageUtil.getScaledInstance(new ByteArrayInputStream(b.getBildeLob()), 320, 240, 0.5f));
        b.setBredde(b.getImage().getWidth());
        b.setHoyde(b.getImage().getHeight());
        BildeDatoPanel pnlDato = new BildeDatoPanel();
        JDialog d = new JDialog(SwingUtilities.windowForComponent(this));
        d.setTitle("Velg bildedato");
        d.setModal(true);
        d.add(pnlDato);
        d.setSize(220, 120);
        d.setLocationRelativeTo(this);
        d.setVisible(true);
        if (pnlDato.isCancelled())
            return null;

        Date dato = pnlDato.getDato();
        b.setNyDato(dato);
        return b;
    }


    @SuppressWarnings("unused")
    private List<AvvikstatusType> setToList(Set<AvvikstatusType> set) {
        List<AvvikstatusType> list = new ArrayList<AvvikstatusType>();
        Iterator<AvvikstatusType> it = set.iterator();
        while (it.hasNext()) {
            list.add((AvvikstatusType) it.next());
        }
        Collections.sort(list);
        return list;
    }

    private void selectView(JXImageView view) {
        clearBorders();
        view.setBorder(BorderFactory.createLineBorder(Color.GREEN, 3));
    }

    public class ImageClickListener extends MouseAdapter {
        private final JXImageView view;
        private final Bilde bilde;

        public ImageClickListener(JXImageView view, Bilde bilde) {
            this.view = view;
            this.bilde = bilde;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                activeImage = bilde;
                if (e.getClickCount() == 1) {
                    selectView(view);

                } else if (e.getClickCount() == 2) {
                    openImage();
                }
            }
        }
    }
}