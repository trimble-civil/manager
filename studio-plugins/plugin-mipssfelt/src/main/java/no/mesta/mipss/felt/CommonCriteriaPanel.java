package no.mesta.mipss.felt;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.JMipssDatePicker;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

@SuppressWarnings("serial")
public class CommonCriteriaPanel extends JPanel {

    private final FeltController controller;

    private JLabel lblKontrakt;
    private JLabel lblPeriode;
    private JLabel lblAlternativer;
    private JLabel lblDash;

    private JMipssContractPicker contractPicker;
    private JMipssDatePicker dateFra;
    private JMipssDatePicker dateTil;
    private JPanel pnlPeriode;

    private JButton btnVei;
    private JButton btnPda;
    private JButton btnBruker;
    private JPanel pnlButton;

    private CommonSelectedCriteriaPanel pnlValg;

    public CommonCriteriaPanel(FeltController controller) {
        this.controller = controller;

        initComponents();
        initGui();
        bind();

    }

    private void initComponents() {
        lblKontrakt = new JLabel(Resources.getResource("label.velgKontrakt"));
        lblPeriode = new JLabel(Resources.getResource("label.periodeFraTil"));
        lblAlternativer = new JLabel(Resources.getResource("label.alternativer"));
        lblDash = new JLabel(Resources.getResource("label.dash"));

        contractPicker = new JMipssContractPicker(controller.getPlugin(), false);
        contractPicker.getDropdown().setPreferredSize(null);
        dateFra = new JMipssDatePicker();
        dateFra.setTimeFormat(JMipssDatePicker.TIME_FORMAT.START_OF_DAY);
        dateTil = new JMipssDatePicker();
        dateTil.setTimeFormat(JMipssDatePicker.TIME_FORMAT.END_OF_DAY);
        dateFra.pairWith(dateTil);
//		DatePickerConstraintsUtils.setNotBeforeDate(controller.getPlugin().getLoader(), dateFra, dateTil);
        pnlPeriode = new JPanel(new GridBagLayout());

        btnVei = new JButton(controller.getVeiButtonAction());
        btnPda = new JButton(controller.getPdaButtonAction());
        btnBruker = new JButton(controller.getBrukerButtonAction());
        pnlButton = new JPanel(new GridBagLayout());

        pnlValg = new CommonSelectedCriteriaPanel(controller);

    }

    private void initGui() {
        setLayout(new GridBagLayout());

        pnlPeriode.add(dateFra, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        pnlPeriode.add(lblDash, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));
        pnlPeriode.add(dateTil, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        pnlButton.add(btnVei, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnPda, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(btnBruker, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        add(lblKontrakt, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 20), 0, 0));
        add(contractPicker, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(6, 0, 5, 0), 0, 0));

        add(lblPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 20), 0, 0));
        add(pnlPeriode, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(lblAlternativer, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 20), 0, 0));
        add(pnlButton, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        add(pnlValg, new GridBagConstraints(2, 0, 1, 3, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
//		add(lblLogo, 		new GridBagConstraints(3, 0, 1, 3, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,5,5,0),0,0));
    }

    private void bind() {
        Binding<JMipssContractPicker, Boolean, CommonCriteriaPanel, Boolean> enableThis = BindingHelper.createbinding(contractPicker, "${valgtKontrakt != null}", this, "enabled");
        Binding<JMipssContractPicker, Driftkontrakt, QueryParams, Driftkontrakt> valgtKontrakt = BindingHelper.createbinding(contractPicker, "valgtKontrakt", controller.getCommonQueryParams(), "valgtKontrakt");
        Binding<JMipssContractPicker, Driftkontrakt, CommonCriteriaPanel, Driftkontrakt> valgtKontraktBind = BindingHelper.createbinding(contractPicker, "valgtKontrakt", this, "driftkontrakt");
        Binding<JMipssDatePicker, Date, QueryParams, Date> fraDate = BindingHelper.createbinding(dateFra, "date", controller.getCommonQueryParams(), "dateFra");
        Binding<JMipssDatePicker, Date, QueryParams, Date> tilDate = BindingHelper.createbinding(dateTil, "date", controller.getCommonQueryParams(), "dateTil");
        Binding<QueryParams, Date, JMipssDatePicker, Date> tilDateEnabled = BindingHelper.createbinding(controller.getCommonQueryParams(), "dateEnabled", dateTil, "editable");
        Binding<QueryParams, Date, JMipssDatePicker, Date> fraDateEnabled = BindingHelper.createbinding(controller.getCommonQueryParams(), "dateEnabled", dateFra, "editable");
        controller.getBindingGroup().addBinding(enableThis);
        controller.getBindingGroup().addBinding(valgtKontrakt);
        controller.getBindingGroup().addBinding(fraDate);
        controller.getBindingGroup().addBinding(tilDate);
        controller.getBindingGroup().addBinding(tilDateEnabled);
        controller.getBindingGroup().addBinding(fraDateEnabled);
        controller.getBindingGroup().addBinding(valgtKontraktBind);

    }

    public void setEnabled(boolean enabled) {
        dateFra.setEnabled(enabled);
        dateTil.setEnabled(enabled);
        btnVei.setEnabled(enabled);
        btnPda.setEnabled(enabled);
        btnBruker.setEnabled(enabled);
    }

    public void setDriftkontrakt(Driftkontrakt kontrakt) {
        controller.resetAllTables();
    }
}
