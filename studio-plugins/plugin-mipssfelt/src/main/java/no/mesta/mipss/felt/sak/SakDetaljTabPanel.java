package no.mesta.mipss.felt.sak;

import no.mesta.mipss.core.SaveStatusRegistry;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.TabHeader;
import no.mesta.mipss.felt.sak.detaljui.DeleteRestoreEntityListener;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.valueobjects.ElrappStatus;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.util.List;

@SuppressWarnings("serial")
public abstract class SakDetaljTabPanel extends JPanel {
    private boolean changed;
    protected boolean loading = true;
    protected List<LoadingPanel> loadingPanels;
    private SaveStatusRegistry saveStatusRegistry = new SaveStatusRegistry();
    private TabHeader tabHeader;
    protected JPanel pnlSlettetInfo = new JPanel();
    protected JPanel pnlLukketInfo = new JPanel();
    protected JLabel lblSlettet;
    protected JLabel lblLukket;
    protected PanelDisabledIndicator panelDisabledIndicator;

    private DeleteRestoreEntityListener deleteEntityListener;

    public abstract MipssFieldDetailItem getDetailEntity();

    protected abstract void bind();

    public abstract void dispose();

    public abstract ElrappStatus getElrappStatus();

    /**
     * Kalles på EDT for sette verdiene for gui
     */
    protected abstract void doneLoading();

    public abstract FeltEntityInfo getFeltEntityInfo();

    public abstract String getTitle();

    protected void initGui() {
        lblSlettet = new JLabel();
        Font old = lblSlettet.getFont();
        Font f = new Font(old.getName(), Font.BOLD, 12);
        lblSlettet.setFont(f);
        lblSlettet.setOpaque(false);
        lblSlettet.setForeground(Color.white);

        lblLukket = new JLabel();
        lblLukket.setFont(f);
        lblLukket.setOpaque(false);
        lblLukket.setForeground(Color.black);

        pnlSlettetInfo.add(lblSlettet);
        pnlSlettetInfo.setVisible(false);
        pnlSlettetInfo.setBackground(Color.red);
        pnlSlettetInfo.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        pnlLukketInfo.add(lblLukket);
        pnlLukketInfo.setVisible(false);
        pnlLukketInfo.setBackground(new Color(255, 255, 150));
        pnlLukketInfo.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

    }

    public void setDeleteRestoreEntityListener(DeleteRestoreEntityListener deleteEntityListener) {
        this.deleteEntityListener = deleteEntityListener;
    }

    public void entityDeleted() {
        setChanged(false);
        deleteEntityListener.entityDeleted(this);
    }

    public void entityRestored() {
        if (panelDisabledIndicator != null)
            panelDisabledIndicator.dispose();
        pnlSlettetInfo.setVisible(false);
        deleteEntityListener.entityRestored(this);
    }

    /**
     * Henter data til panelene, kjøres i egen tråd utenfor EDT
     */
    protected abstract void load();

    public void doLoad() {
        new SwingWorker<Void, Void>() {
            public Void doInBackground() {
                try {
                    load();
                    if (getElrappStatus() != null) {
                        getTabHeader().setElrappStatus(getElrappStatus());
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                return null;
            }

            @Override
            public void done() {
                try {

                    doneLoading();
                    bind();
                } catch (Throwable t) {
                    ErrorHandler.showError(t, Resources.getResource("error.feilvedLasting", EpostUtils.getSupportEpostAdresse()));
                    t.printStackTrace();
                }
            }
        }.execute();
    }

    public SaveStatusRegistry getSaveStatusRegistry() {
        return saveStatusRegistry;
    }

    private boolean isAllPanelsLoaded() {
        if (loadingPanels == null)
            return false;
        int loaded = 0;
        for (LoadingPanel p : loadingPanels) {
            if (p.isLoading())
                loaded++;
        }
        return (loaded == 0);
    }

    public boolean isChanged() {
        return this.changed;
    }

    public final boolean isLoading() {
        return this.loading;
    }

    public void setChanged(boolean changed) {
        boolean old = this.changed;
        this.changed = changed;
        firePropertyChange("changed", old, changed);
    }

    protected void waitWhileLoading() {
        new SwingWorker<Void, Void>() {
            @Override

            public Void doInBackground() {
                while (!isAllPanelsLoaded()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                loading = false;
                return null;
            }

            @Override
            public void done() {
//				bind();
            }
        }.execute();
    }

    public void setTabHeader(TabHeader tabHeader) {
        this.tabHeader = tabHeader;
    }

    public TabHeader getTabHeader() {
        if (tabHeader == null) {
            tabHeader = new TabHeader(getTitle());
        }
        return tabHeader;
    }

}
