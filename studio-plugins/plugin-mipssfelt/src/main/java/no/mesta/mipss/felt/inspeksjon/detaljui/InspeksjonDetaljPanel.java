package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.felt.FeltMapPanel;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.inspeksjon.InspeksjonController;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.persistence.mipssfield.FeltEntityInfo;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.MipssFieldDetailItem;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;
import no.mesta.mipss.valueobjects.ElrappStatus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class InspeksjonDetaljPanel extends SakDetaljTabPanel {

    private final InspeksjonSokResult inspeksjonSokResult;
    private final InspeksjonController controller;
    private String title;
    private Inspeksjon inspeksjon;

    private InspeksjonDetaljButtonPanel pnlButtons;
    private JPanel pnlContent;

    private TidStedPanel pnlTidSted;
    private VurderingPanel pnlVurdering;
    private KommentarPanel pnlKommentar;
    private FeltMapPanel pnlMap;
    private ProsessAvvikPanel pnlProsessAvvik;
    private StrekningPanel pnlStrekninger;
    private RegistreringTabPanel pnlRegistreringer;

    private SaveStatusMonitor<Inspeksjon> inspeksjonMonitor;

    public InspeksjonDetaljPanel(InspeksjonController controller, InspeksjonSokResult inspeksjonSokResult, Inspeksjon inspeksjon) {
        this.controller = controller;
        this.inspeksjonSokResult = inspeksjonSokResult;
        this.title = "Inspeksjon(" + inspeksjonSokResult.getRefnummer() + ")";

        inspeksjonMonitor = new SaveStatusMonitor<Inspeksjon>(Inspeksjon.class, this, "inspeksjon", getSaveStatusRegistry().getBindingGroup());
        getSaveStatusRegistry().addMonitor(inspeksjonMonitor);

        initComponents();
        initGui();
        if (inspeksjon == null) {
            doLoad();
            waitWhileLoading();
        } else {
            setInspeksjon(inspeksjon);
            bind();
            loading = false;
        }
    }

    public Inspeksjon getInspeksjon() {
        return inspeksjon;
    }

    private void initComponents() {
        pnlButtons = new InspeksjonDetaljButtonPanel(controller, this);
        pnlTidSted = new TidStedPanel(inspeksjonSokResult);
        pnlVurdering = new VurderingPanel();
        pnlKommentar = new KommentarPanel();
        pnlMap = new FeltMapPanel(controller.getFeltController());
        pnlProsessAvvik = new ProsessAvvikPanel();
        pnlStrekninger = new StrekningPanel();
        pnlRegistreringer = new RegistreringTabPanel(controller.getFeltController());

        loadingPanels = new ArrayList<LoadingPanel>();
        loadingPanels.add(pnlTidSted);
        loadingPanels.add(pnlVurdering);
        loadingPanels.add(pnlKommentar);
        loadingPanels.add(pnlMap);
        loadingPanels.add(pnlProsessAvvik);
        loadingPanels.add(pnlStrekninger);
        loadingPanels.add(pnlRegistreringer);

    }

    @Override
    protected void initGui() {
        super.initGui();
        setLayout(new BorderLayout());
        JPanel pnlLoadingPanels = new JPanel(new GridLayout(3, 2));
        pnlLoadingPanels.add(pnlTidSted);
        pnlLoadingPanels.add(pnlVurdering);
        pnlLoadingPanels.add(pnlKommentar);
        pnlLoadingPanels.add(pnlMap);
        pnlLoadingPanels.add(pnlProsessAvvik);
        pnlLoadingPanels.add(pnlStrekninger);


        pnlContent = new JPanel(new GridBagLayout());
        pnlContent.add(pnlLoadingPanels, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        pnlContent.add(pnlRegistreringer, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.5, GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        add(pnlSlettetInfo, BorderLayout.NORTH);
        add(pnlContent, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);
    }

    @Override
    public MipssFieldDetailItem getDetailEntity() {
        return inspeksjon;
    }

    @Override
    protected void bind() {
        getSaveStatusRegistry().getBindingGroup().addBinding(BindingHelper.createbinding(getSaveStatusRegistry(), "changed", pnlButtons, "lagreEnabled"));
        inspeksjonMonitor.registerField("beskrivelse");
        inspeksjonMonitor.registerField("etterslepsregFlagg");
        getSaveStatusRegistry().doBind();

    }

    @Override
    public void dispose() {
        getSaveStatusRegistry().reset();

    }

    @Override
    public ElrappStatus getElrappStatus() {
        return null;
    }

    @Override
    protected void doneLoading() {
        setInspeksjon(inspeksjon);

    }

    private void setInspeksjon(Inspeksjon inspeksjon) {
        if (inspeksjon.getSlettetDato() != null) {
            pnlSlettetInfo.setVisible(true);
            lblSlettet.setText("Avviket er slettet, " + MipssDateFormatter.formatDate(inspeksjon.getSlettetDato()) + " av " + inspeksjon.getSlettetAv());
            pnlButtons.setGjenopprett(true);
            panelDisabledIndicator = new PanelDisabledIndicator(pnlContent);
        }

        this.inspeksjon = inspeksjon;
        pnlTidSted.setInspeksjon(inspeksjon);
        pnlVurdering.setInspeksjon(inspeksjon);
        pnlKommentar.setInspeksjon(inspeksjon);
        pnlMap.setInspeksjon(inspeksjon);
        pnlProsessAvvik.setInspeksjon(inspeksjon);
        pnlStrekninger.setInspeksjon(inspeksjon);
        pnlRegistreringer.setInspeksjon(inspeksjon);
    }

    @Override
    public FeltEntityInfo getFeltEntityInfo() {
        return null;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    protected void load() {
        inspeksjon = controller.hentInspeksjon(inspeksjonSokResult.getGuid());
    }


}
