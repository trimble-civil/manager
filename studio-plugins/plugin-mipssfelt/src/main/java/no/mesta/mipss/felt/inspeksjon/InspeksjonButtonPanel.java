package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.core.BindingHelper;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class InspeksjonButtonPanel extends JPanel {

    private final InspeksjonController controller;
    private JButton btnSlettInspeksjon;
    private JButton btnExcel;
    private JButton btnKart;
    private JButton btnRapport;
    private JButton btnDetaljer;
    private JSeparator separator;

    public InspeksjonButtonPanel(InspeksjonController controller) {
        this.controller = controller;
        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        btnSlettInspeksjon = new JButton(controller.getInspeksjonSlettAction());
        btnExcel = new JButton(controller.getInspeksjonExcelAction());
        btnKart = new JButton(controller.getInspeksjonKartAction());
        btnRapport = new JButton(controller.getInspeksjonRapportAction());
        btnDetaljer = new JButton(controller.getInspeksjonDetaljerAction());
        separator = new JSeparator(SwingConstants.VERTICAL);

        btnSlettInspeksjon.setEnabled(false);
        btnExcel.setEnabled(false);
        btnKart.setEnabled(false);
        btnDetaljer.setEnabled(false);
    }

    private void bind() {
        BindingHelper.createbinding(controller.getInspeksjonTableModel(), "${size>0}", btnExcel, "enabled").bind();
//		BindingHelper.createbinding(controller.getInspeksjonTable(), "${noOfSelectedRows==1}", btnRapport, "enabled").bind();
        BindingHelper.createbinding(controller.getInspeksjonTable(), "${noOfSelectedRows==1}", btnSlettInspeksjon, "enabled").bind();
        BindingHelper.createbinding(controller.getInspeksjonTable(), "${noOfSelectedRows==1}", btnDetaljer, "enabled").bind();
//		BindingHelper.createbinding(controller.getInspeksjonTable(), "${noOfSelectedRows>0}", btnKart, "enabled").bind();

    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnSlettInspeksjon, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
        add(separator, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 10), 0, 0));
        add(btnExcel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        add(btnRapport, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
//TODO		add(btnKart, 			new GridBagConstraints(4,0,1,1,0.0,0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,5),0,0));
        add(btnDetaljer, new GridBagConstraints(5, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

    }
}