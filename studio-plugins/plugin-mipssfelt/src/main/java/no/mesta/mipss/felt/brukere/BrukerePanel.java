package no.mesta.mipss.felt.brukere;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mipssfelt.MipssFelt;
import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

@SuppressWarnings("serial")
public class BrukerePanel extends JPanel {

    private final Driftkontrakt kontrakt;
    private JXTable table;
    private MipssRenderableEntityTableModel<BrukerTableObject> tableModel;
    private Set<BrukerTableObject> allBrukere;
    private final QueryParams params;

    public BrukerePanel(QueryParams params, Driftkontrakt kontrakt) {
        this.params = params;
        this.kontrakt = kontrakt;
        initGui();
    }

    public void setAlleValgt(boolean valgt) {
        for (BrukerTableObject b : allBrukere) {
            b.setValgt(valgt);
        }
        repaint();
    }

    private void initGui() {
        table = getTable();
        JScrollPane scroll = new JScrollPane(table);

        setLayout(new BorderLayout());
        add(scroll, BorderLayout.CENTER);
    }

    private JXTable getTable() {
        String[] columns = new String[]{"valgt", "navn"};
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(BrukerTableObject.class, columns);
        tableModel = new MipssRenderableEntityTableModel<BrukerTableObject>(BrukerTableObject.class, new int[]{0}, columns);
        tableModel.getEntities().clear();
        tableModel.getEntities().addAll(getEnheter());
        JXTable table = new JXTable(tableModel, columnModel);
        table.getColumn(0).setMaxWidth(30);

        return table;
    }

    public String[] getAlleValgte() {
        List<String> valgte = new ArrayList<String>();
        for (BrukerTableObject b : allBrukere) {
            if (b.getValgt() != null && b.getValgt()) {
                valgte.add(b.getNavn());
            }
        }
        return valgte.toArray(new String[]{});
    }

    private Set<BrukerTableObject> getEnheter() {
        allBrukere = new TreeSet<BrukerTableObject>();
        List<String> alle = null;

        if (kontrakt != null) {
            alle = BeanUtil.lookup(MipssFelt.BEAN_NAME, MipssFelt.class).getFeltBrukere(kontrakt.getId(), params.getPdaDfuIdentList());
        } else {
            alle = new ArrayList<String>();
        }

        for (String p : alle) {
            BrukerTableObject dto = new BrukerTableObject();
            dto.setNavn(p);
            allBrukere.add(dto);
        }

        return allBrukere;
    }

    public void setSelectedBrukere(String[] brukerList) {
        if (brukerList == null)
            return;
        Arrays.sort(brukerList);
        for (BrukerTableObject b : allBrukere) {
            if (Arrays.binarySearch(brukerList, b.getNavn()) >= 0) {
                b.setValgt(Boolean.TRUE);
            } else {
                b.setValgt(Boolean.FALSE);
            }
        }
        table.repaint();
    }

}
