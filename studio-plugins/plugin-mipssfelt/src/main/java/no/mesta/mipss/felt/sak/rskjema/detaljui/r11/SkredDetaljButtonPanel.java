package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.felt.sak.rskjema.RskjemaController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")

public class SkredDetaljButtonPanel extends JPanel {
    private JButton btnLagre;
    private JButton btnSlett;
    private JButton btnRapport;
    private JButton btnElrapp;

    private RskjemaController controller;
    private SkredDetaljPanel skredDetaljPanel;

    public SkredDetaljButtonPanel(RskjemaController controller, SkredDetaljPanel skredDetaljPanel) {
        this.controller = controller;
        this.skredDetaljPanel = skredDetaljPanel;
        initComponents();
        initGui();

    }

    private void initComponents() {
        btnLagre = new JButton(controller.getSkredLagreAction(skredDetaljPanel));
        btnLagre.setEnabled(false);
        btnSlett = new JButton(controller.getSkredSlettAction(skredDetaljPanel, getSlettSkredCallback()));
        btnRapport = new JButton(controller.getR11RapportAction(skredDetaljPanel));
        btnElrapp = new JButton(controller.getR11SendElrappAction(skredDetaljPanel));

    }

    public void setGjenopprett(boolean gjenopprett) {
        if (gjenopprett) {
            btnSlett.setAction(controller.getSkredRestoreAction(skredDetaljPanel, getRestoreSkredCallback()));
            btnElrapp.setEnabled(false);
        } else {
            btnSlett.setAction(controller.getSkredSlettAction(skredDetaljPanel, getSlettSkredCallback()));
            btnElrapp.setEnabled(true);
        }
    }

    private Action getSlettSkredCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                skredDetaljPanel.entityDeleted();
            }
        };
    }

    private Action getRestoreSkredCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                skredDetaljPanel.entityRestored();
                setGjenopprett(false);
            }
        };
    }

    public void setEnabled(boolean enabled) {
        btnLagre.setEnabled(enabled);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnSlett, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(btnRapport, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnElrapp, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(btnLagre, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    }

}
