package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.CoordinateUtils;
import no.mesta.mipss.core.DateStringConverter;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Punktstedfest;
import no.mesta.mipss.persistence.mipssfield.Punktveiref;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;

@SuppressWarnings("serial")
public class TidStedPanel extends JPanel implements LoadingPanel {
    private final AvvikController controller;
    private Avvik avvik;
    private JLabel lblId;
    private JLabel lblRegdato;
    private JLabel lblAv;
    private JLabel lblPda;
    private JLabel lblVei;
    private JLabel lblKoordinat;
    private JButton btnEndreVei;
    private JFormattedTextField fldId;
    private JFormattedTextField fldRegdato;
    private JFormattedTextField fldAv;
    private JFormattedTextField fldPda;
    private JFormattedTextField fldVei;
    private JTextArea txtKoordinat;
    private BindingGroup bindings = new BindingGroup();

    public TidStedPanel(AvvikController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    private void initComponents() {
        lblId = new JLabel(Resources.getResource("label.id"));
        lblRegdato = new JLabel(Resources.getResource("label.regdato"));
        lblAv = new JLabel(Resources.getResource("label.av"));
        lblPda = new JLabel(Resources.getResource("label.pda"));
        lblVei = new JLabel(Resources.getResource("label.vei"));
        lblKoordinat = new JLabel(Resources.getResource("label.koordinat"));

        fldId = new JFormattedTextField();
        fldRegdato = new JFormattedTextField();
        fldAv = new JFormattedTextField();
        fldPda = new JFormattedTextField();
        fldVei = new JFormattedTextField();
        txtKoordinat = new JTextArea();

        fldId.setEditable(false);
        fldRegdato.setEditable(false);
        fldAv.setEditable(false);
        fldPda.setEditable(false);
        fldVei.setEditable(false);
        txtKoordinat.setEditable(false);

        btnEndreVei = new JButton(controller.getEndreVeiAction(this));
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.tidSted")));
        JPanel pnlVei = new JPanel(new GridBagLayout());
        pnlVei.add(fldVei, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        pnlVei.add(btnEndreVei, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

        add(lblId, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldId, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblRegdato, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(fldRegdato, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblAv, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        add(fldAv, new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblPda, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        add(fldPda, new GridBagConstraints(5, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblVei, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(pnlVei, new GridBagConstraints(1, 2, 5, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(lblKoordinat, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        add(new JScrollPane(txtKoordinat), new GridBagConstraints(1, 3, 5, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 5, 10, 0), 0, 0));
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    private void bind() {
        Binding regdatoBind = BindingHelper.createbinding(avvik, "opprettetDato", fldRegdato, "text");
        Binding<Avvik, Object, JFormattedTextField, Object> idBind = BindingHelper.createbinding(avvik, "refnummer", fldId, "text");
        Binding<Avvik, Object, JFormattedTextField, Object> regAvBind = BindingHelper.createbinding(avvik, "opprettetAv", fldAv, "text");
        Binding<Avvik, Object, JFormattedTextField, Object> pdaBind = BindingHelper.createbinding(avvik, "${sak.pdaDfuIdent}", fldPda, "text");
        if (avvik.getStedfesting() != null) {
            Binding<Punktveiref, Object, JFormattedTextField, Object> veiBind = BindingHelper.createbinding(avvik.getStedfesting().getVeiref(), "textForGUI", fldVei, "text");
            bindings.addBinding(veiBind);

        }
        Binding<Punktstedfest, String, TidStedPanel, String> koordinatBind = BindingHelper.createbinding(avvik.getStedfesting(), "koordinatString", this, "koordinatString");
        regdatoBind.setConverter(new DateStringConverter(MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
        bindings.addBinding(idBind);
        bindings.addBinding(regdatoBind);
        bindings.addBinding(regAvBind);
        bindings.addBinding(pdaBind);
        bindings.addBinding(koordinatBind);
        bindings.bind();
    }

    public void setKoordinatString(String koordinatString) {
        Punktstedfest ps = avvik.getStedfesting();
        StringBuilder sb = new StringBuilder();

        sb.append("GPS: ");
        DecimalFormat f = new DecimalFormat("#.#####");
        try {
            Point2D.Double wgs84 = CoordinateUtils.convertFromUTM33toWGS84(ps.getX(), ps.getY());

            // dette gjør det mulig for brukeren å klikke på lblKoordinat for å
            // få opp koordinatene markert på Google Maps i standardnettleseren
            createLinkLabel(wgs84.y, wgs84.x);

            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
            dfs.setDecimalSeparator('.');
            f.setDecimalFormatSymbols(dfs);

            sb.append(f.format(wgs84.y));
            sb.append(" ");
            sb.append(f.format(wgs84.x));
        } catch (Exception e) {
            e.printStackTrace();
        }

        sb.append("\n");

        f = new DecimalFormat("#.#");
        if (ps.getSnappetX() != null && ps.getSnappetY() != null) {
            sb.append(Resources.getResource("label.pavei"));
            sb.append(" N:");
            sb.append(f.format(ps.getSnappetY()));
            sb.append(" ");
            sb.append(Resources.getResource("label.east"));
            sb.append(f.format(ps.getSnappetX()));
        } else {
            sb.append(Resources.getResource("label.ikkesnappet"));
            sb.append("\n");
            sb.append("N:");
            sb.append(f.format(ps.getY()));
            sb.append(" ");
            sb.append(Resources.getResource("label.east"));
            sb.append(f.format(ps.getX()));
        }
        sb.append(" (UTM33)");

        txtKoordinat.setText(sb.toString());
    }

    private void createLinkLabel(final double lat, final double lon) {
        final Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;

        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            // farger teksten blå og setter strek under den
            lblKoordinat.setForeground(Color.BLUE);

            Font font = lblKoordinat.getFont();
            Map attributes = font.getAttributes();
            attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
            lblKoordinat.setFont(font.deriveFont(attributes));

            // lytteren åpner standardnettleseren
            lblKoordinat.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    try {
                        URI uri = new URI("http://www.google.com/maps/place/" + lat + "," + lon);
                        desktop.browse(uri);
                    } catch (URISyntaxException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }
    }

    public BindingGroup getBindingGroup() {
        return this.bindings;
    }

    public void setAvvik(Avvik avvik) {
        this.avvik = avvik;
        bind();
    }

    public Avvik getAvvik() {
        return this.avvik;
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();

    }

    public void setLukket(boolean lukketAvvik) {
        btnEndreVei.setEnabled(!lukketAvvik);
        fldAv.setEnabled(!lukketAvvik);
        fldId.setEnabled(!lukketAvvik);
        fldPda.setEnabled(!lukketAvvik);
        fldRegdato.setEnabled(!lukketAvvik);
        fldVei.setEnabled(!lukketAvvik);
    }

}