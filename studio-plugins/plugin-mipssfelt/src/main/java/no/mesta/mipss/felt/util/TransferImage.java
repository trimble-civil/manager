package no.mesta.mipss.felt.util;

import java.io.File;
import java.io.Serializable;

@SuppressWarnings("serial")
public class TransferImage implements Serializable {
    private byte[] data;
    private File file;

    public TransferImage(byte[] data, File file) {
        this.data = data;
        this.file = file;
    }

    public byte[] getData() {
        return data;
    }

    public File getFile() {
        return file;
    }
}
