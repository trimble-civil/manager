package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.persistence.MipssEntityBean;
import no.mesta.mipss.ui.table.CheckBoxTableObject;
import no.mesta.mipss.ui.table.JCheckTablePanel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import java.awt.*;
import java.util.List;

@SuppressWarnings({"serial", "rawtypes"})
public abstract class TableObjectPanel<T extends CheckBoxTableObject, E extends MipssEntityBean> extends JPanel {

    private JCheckBox alleCheck;
    protected JCheckTablePanel<T> checkTable;
    protected String[] columns = new String[]{"valgt", "textForGUI"};
    protected MipssRenderableEntityTableModel<T> tableModel;

    protected final List<E> alleAvvikstatus;
    protected JXTable table;

    public TableObjectPanel(List<E> alleAvvikstatus) {
        this.alleAvvikstatus = alleAvvikstatus;
        initComponents();
        initGui();
    }

    public void setAlleValgt(boolean alleValgt) {
        for (T o : tableModel.getEntities()) {
            o.setValgt(alleValgt);
        }
        repaint();
    }

    private void initComponents() {
        table = getTable();
        alleCheck = new JCheckBox("Alle");
        checkTable = new JCheckTablePanel<T>(table, tableModel, alleCheck);
    }

    //TODO implement in subclass
//	public List<String> getSelected(){
//		List<String> ids = new ArrayList<String>();
//		for (AvvikstatusTableObject t:checkTable.getAlleValgte()){
//			ids.add(t.getStatus());
//		}
//		return ids;
//	}
    protected abstract JXTable getTable();

    //	{
//		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(AvvikstatusTableObject.class, columns);
//		tableModel = new MipssRenderableEntityTableModel<T>(T.class, new int[] { 0 }, columns);
//		tableModel.getEntities().clear();
//		List<AvvikstatusTableObject> list = new ArrayList<AvvikstatusTableObject>();
//		for (AvvikstatusType t:alleAvvikstatus){
//			list.add(new AvvikstatusTableObject(t));
//		}
//		tableModel.getEntities().addAll(list);
//		JXTable table = new JXTable(tableModel, columnModel);
//		table.getColumn(0).setMaxWidth(30);
//
//		return table;
//	}
    private void initGui() {
        setLayout(new BorderLayout());
        add(checkTable);
    }
    //TODO implemented in subclass
//	public void setSelected(List<String> status) {
//		if (status==null)
//			return;
//		for (AvvikstatusTableObject o:tableModel.getEntities()){
//			if (status.contains(o.getStatus())){
//				o.setValgt(Boolean.TRUE);
//			}
//		}
//		
//		
//	}
}
