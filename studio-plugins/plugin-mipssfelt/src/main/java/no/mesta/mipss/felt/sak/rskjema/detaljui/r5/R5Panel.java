package no.mesta.mipss.felt.sak.rskjema.detaljui.r5;

import net.sf.jasperreports.engine.JRException;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.persistence.mipssfield.Skadekontakttype;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.MipssFeltDS;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.UIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class R5Panel extends JPanel implements LoadingPanel {


    private boolean cancelled = true;
    private Forsikringsskade forsikringsskade;
    @SuppressWarnings("unused")
    private boolean isNew = true;
    private List<Skadekontakttype> kontaktList = BeanUtil.lookup(MipssEntityService.BEAN_NAME, MipssEntityService.class).getSkadekontakttypeList();
    @SuppressWarnings("unused")
    private Logger log = LoggerFactory.getLogger(this.getClass());

    private ForsikringsskadeOverordnetPanel overordnetPanel;
    private ForsikringsskadeSkadevolderPanel skadevolderPanel;
    private ForsikringsskadeKontakterPanel kontakterPanel;
    @SuppressWarnings("unused")
    private final RskjemaController controller;


    public R5Panel(RskjemaController controller) {
        this.controller = controller;
        initGui();
    }

    public List<SkadeKontakt> getKontakter(Forsikringsskade skade, String type) {
        List<SkadeKontakt> kontakter = new ArrayList<SkadeKontakt>();
        for (SkadeKontakt k : skade.getSkadeKontakter()) {
            if (k.getType().getNavn().equals(type)) {
                kontakter.add(k);
            }
        }
        return kontakter;
    }

    private void initGui() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 20), 0, 0);
        overordnetPanel = new ForsikringsskadeOverordnetPanel(c);
        skadevolderPanel = new ForsikringsskadeSkadevolderPanel(c, this);
        kontakterPanel = new ForsikringsskadeKontakterPanel(c, this);

        tab = new JTabbedPane();
        tab.addTab("Overordnet", overordnetPanel);
        tab.addTab("Skadevolder", skadevolderPanel);
        tab.addTab("Kontakter og resultater", kontakterPanel);

        overordnetPanel.setBorder(BorderFactory.createEmptyBorder(3, 20, 0, 0));
        skadevolderPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 20, 0));
        kontakterPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 20, 0));

        JLabel tittelLabel = new JLabel(Resources.getResource("label.r5.tittel"));
        Font f = tittelLabel.getFont();
        tittelLabel.setFont(new Font(f.getName(), Font.BOLD, 11));

        JPanel tittelPanel = UIUtils.getHorizPanel();
        tittelPanel.add(UIUtils.getHStrut(5));
        tittelPanel.add(tittelLabel);
        tittelPanel.add(Box.createHorizontalGlue());

        add(UIUtils.getVStrut(5));
        add(tittelPanel);
        add(UIUtils.getVStrut(5));
        add(tab);
        add(UIUtils.getVStrut(5));
    }

    public void addTab(JPanel pnl, String title) {
        tab.addTab(title, pnl);
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void runWizard() {
        PageOverordnet o = new PageOverordnet(overordnetPanel);
        PageSkadevolder s = new PageSkadevolder(skadevolderPanel);
        PageKontakter k = new PageKontakter(kontakterPanel);

        WizardPage[] pages = {o, s, k};
        Wizard wizard = WizardPage.createWizard(pages);
        UIManager.put("wizard.sidebar.image", ImageUtil.convertImageToBufferedImage(IconResources.WIZARD_ICON.getImage()));
        new WizardDialog(null, wizard).startWizard();
    }

    private JTabbedPane tab;

    /**
     * Setter forsikringsskaden
     */
    public void setForsikringsskade(Forsikringsskade forsikringsskade) {
        this.forsikringsskade = forsikringsskade;
        this.forsikringsskade.setSkadekontakttyper(kontaktList);

        skadevolderPanel.setForsikringsskade(forsikringsskade);
        overordnetPanel.setForsikringsskade(forsikringsskade);
        kontakterPanel.setForsikringsskade(forsikringsskade);
    }

    public Forsikringsskade getForsikringsskade() {
        return this.forsikringsskade;
    }

    /**
     * Validerer at alle feltene er ihht r5 modellen
     */
    public boolean validateForm() {
        boolean valid = true;
        if (!skadevolderPanel.isValidFields())
            valid = false;
        if (!kontakterPanel.isValidFields())
            valid = false;
        if (!overordnetPanel.isValidFields()) {
            valid = false;
        }
        return valid;
    }

    private void showError(String error) {
        repaint();
        JOptionPane.showMessageDialog(getParent(), error, "Feil i skjema", JOptionPane.ERROR_MESSAGE);
    }

    class AvbrytAction extends AbstractAction {

        public AvbrytAction(String txt, ImageIcon icon) {
            super(txt, icon);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            cancelled = true;
            SwingUtilities.windowForComponent(R5Panel.this).dispose();
        }
    }

    class OkAction extends AbstractAction {

        public OkAction(String txt, ImageIcon icon) {
            super(txt, icon);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            cancelled = false;
            if (validateForm()) {
                SwingUtilities.windowForComponent(R5Panel.this).dispose();
            } else
                showError(Resources.getResource("warning.missingField"));
        }
    }

    static class PageKontakter extends WizardPage {
        public static String getDescription() {
            return "Kontakter";
        }

        public PageKontakter(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    static class PageOverordnet extends WizardPage {
        public static String getDescription() {
            return "Overordnet";
        }

        public PageOverordnet(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    static class PageSkadevolder extends WizardPage {
        public static String getDescription() {
            return "Skadevolder";
        }

        public PageSkadevolder(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    static class PageVisning extends WizardPage {
        public static String getDescription() {
            return "Forhåndsvisning";
        }

        public PageVisning(JPanel content) {
            setLayout(new BorderLayout());
            add(content);
        }
    }

    public AbstractAction getPreviewAction(String txt, ImageIcon icon) {
        return new AbstractAction(txt, icon) {
            @Override
            public void actionPerformed(ActionEvent e) {
                PreviewDS dataSource = new PreviewDS(forsikringsskade);
                JReportBuilder builder = new JReportBuilder(Report.R5_FORM, Report.R5_FORM.getDefaultParameters(), dataSource, ReportHelper.EXPORT_PDF, new JFrame());
                File report = builder.getGeneratedReportFile();
                if (report != null) {
                    DesktopHelper.openFile(report);
                }
            }
        };
    }

    class PreviewDS extends MipssFeltDS<Forsikringsskade> {

        int c = 0;

        public PreviewDS(Forsikringsskade f) {
            super(Forsikringsskade.class, new ArrayList<String>());
//			setFieldBean(BeanUtil.lookup(MipssField.BEAN_NAME, MipssField.class));
            //TODO implementer
            setEntity(f);
        }

        protected void loadPageData() {
        }

        public boolean next() throws JRException {
            if (c == 0) {
                c++;
                return true;
            } else
                return false;
        }
    }

    private class WizardDialog extends JDialog {
        private final Wizard wizard;

        public WizardDialog(JDialog parent, Wizard wizard) {
            super(parent, true);
            this.wizard = wizard;
        }

        public void startWizard() {
            GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0);
            this.setLayout(new GridBagLayout());
            //Map gatheredSettings = (Map) WizardDisplayer.showWizard(wiz);
            WizardDisplayer.installInContainer(WizardDialog.this, gbc, wizard, null, null, new WizardResultReceiver() {
                @SuppressWarnings("rawtypes")
                @Override
                public void cancelled(Map arg0) {
                    cancelled = true;
                    dispose();
                }

                @Override
                public void finished(Object arg0) {
                    if (validateForm()) {
                        cancelled = false;
                        dispose();
                    } else {
                        showError(Resources.getResource("warning.missingField"));
                    }
                }
            });
            setSize(new Dimension(775, 550));
            setLocationRelativeTo(null);
            setVisible(true);
        }
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

}
