package no.mesta.mipss.felt;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.mapsgen.*;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.ImageUtil;
import no.mesta.mipss.persistence.kvernetf1.Prodstrekning;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.service.produksjon.ProduksjonService;
import no.mesta.mipss.ui.BoxUtil;
import no.mesta.mipss.ui.JMipssImageView;
import no.mesta.mipss.ui.picturepanel.ClipboardImageSupport;
import no.mesta.mipss.webservice.ReflinkStrekning;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
/**
 * Delvis kopiert og tilpasset klasse fra JMapPanel i den gamle mipssfield modulen 
 */
public class FeltMapPanel extends JXPanel implements LoadingPanel {
    private JMipssImageView mapView = new JMipssImageView();
    private static PointImage avvikImage;
    private boolean loading = true;
    private final Box messages = Box.createHorizontalBox();
    private JButton copybutton;
    private JButton mapbutton;
    private MipssFieldDetailItem feltEntity;
    private JButton savebutton;
    private final FeltController controller;


    public FeltMapPanel(FeltController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    private void initComponents() {
        mapView.setLayout(new BorderLayout());
        mapView.setExportFormat("png");
        mapView.setTransferHandler(new ClipboardImageSupport());
        mapView.add(messages, BorderLayout.NORTH);
        mapView.setImage(IconResources.MAP_NORGE_DARK.getImage());
        setBusyMessage("Laster kart");

        copybutton = new JButton(getCopyAction());
        mapbutton = new JButton(getMapAction());
        savebutton = new JButton();
        savebutton.setAction(getSaveAction());
        savebutton.setText(Resources.getResource("button.lagre"));

        copybutton.setMargin(new Insets(1, 1, 1, 1));
        mapbutton.setMargin(new Insets(1, 1, 1, 1));
        savebutton.setMargin(new Insets(1, 1, 1, 1));
    }

    private AbstractAction getMapAction() {
        return new AbstractAction(Resources.getResource("button.kart")) {
            public void actionPerformed(ActionEvent e) {
                List<MipssFieldDetailItem> list = new ArrayList<MipssFieldDetailItem>();
                if (feltEntity instanceof Inspeksjon) {
                    InspeksjonSokResult isr = convertInspeksjonToSokResult((Inspeksjon) feltEntity);
                    list.add((MipssFieldDetailItem) isr);
                } else {
                    list.add((MipssFieldDetailItem) feltEntity);
                }
                controller.openItemsInMap(list);
            }
        };
    }

    private InspeksjonSokResult convertInspeksjonToSokResult(Inspeksjon i) {
        InspeksjonSokResult is = controller.getInspeksjonController().createResultFromInspeksjon(i);
        is.setGuid(i.getGuid());
        is.setEtterslep(i.getEtterslep());
        is.setFraTidspunkt(i.getFraTidspunkt());
        is.setOpprettetDato(i.getRegistreringsDato());
        is.setPdaDfuIdent(i.getPdaDfuIdent());
        is.setPdaDfuNavn(i.getPdaDfuIdent() + "");
        is.setProsessInspText(i.getInspiserteProsesserGUITxt());
        is.setRefnummer(i.getRefnummer());
        is.setTemperaturId(i.getTemperaturId());
        is.setTemperaturNavn(i.getTemperatur().getNavn());
        is.setTilTidspunkt(i.getTilTidspunkt());
        is.setVaerId(i.getVaerId());
        is.setVaerNavn(i.getVaer().getNavn());
        is.setVindId(i.getVindId());
        is.setVindNavn(i.getVind().getNavn());
        is.setBeskrivelse(i.getBeskrivelse());
        return is;
    }

    private AbstractAction getCopyAction() {
        return new AbstractAction(Resources.getResource("button.kopier")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                TransferHandler handler = mapView.getTransferHandler();
                handler.exportToClipboard(mapView, getToolkit().getSystemClipboard(), TransferHandler.COPY);
            }
        };
    }

    private void initGui() {
        mapView.setBorder(BorderFactory.createTitledBorder(""));
        mapView.add(BoxUtil.createVerticalBox(2, BoxUtil.createHorizontalBox(2, Box.createHorizontalGlue(),
                savebutton, copybutton, mapbutton)), BorderLayout.SOUTH);

        setLayout(new GridBagLayout());
        add(mapView, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    }

    public Action getSaveAction() {
        Action action = new AbstractAction() {
            public void actionPerformed(ActionEvent evt) {
                Image img = mapView.getImage();
                BufferedImage dst = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
                Graphics2D g = (Graphics2D) dst.getGraphics();
                // smooth scaling
                g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                g.drawImage(img, 0, 0, null);
                g.dispose();
                FileDialog fd = new FileDialog((Dialog) SwingUtilities.windowForComponent(FeltMapPanel.this));
                fd.setMode(FileDialog.SAVE);
                String date = MipssDateFormatter.formatDate(Clock.now(), MipssDateFormatter.SHORT_DATE_TIME_FORMAT);
                date = date.replace(":", "");
                date = date.replace(" ", "_");
                date = date.replace(".", "");
                fd.setFile("kart" + date + ".png");
                fd.setFilenameFilter(new PNGNameFilter());
                fd.setVisible(true);
                if (fd.getFile() != null) {
                    String fileName = fd.getFile();
                    if (!StringUtils.endsWithIgnoreCase(fileName, ".png")) {
                        fileName += ".png";
                    }
                    try {
                        ImageIO.write(dst, "png", new File(fd.getDirectory(), fileName));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };

        return action;
    }

    public void setAvvik(Avvik avvik) {
        this.feltEntity = avvik;
        mapView.setExportName(Resources.getResource("export.avvikKartName"));
        MapPointWorker worker = new MapPointWorker(avvik.getStedfesting(), getAvvikImage(), IconResources.MAP_PIN_LEFT_SM);
        worker.execute();
    }

    public void setHendelse(Hendelse hendelse) {
        this.feltEntity = hendelse;
        mapView.setExportName(Resources.getResource("export.hendelseKartName"));
        MapPointWorker worker = new MapPointWorker(hendelse.getSak().getStedfesting().get(0), getAvvikImage(), IconResources.MAP_PIN_LEFT_SM);
        worker.execute();
    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.feltEntity = inspeksjon;
        mapView.setExportName(Resources.getResource("export.inspeksjonKartName"));
        MapLinesWorker worker = new MapLinesWorker(inspeksjon.getStrekninger());
        worker.execute();
    }

    private static PointImage getAvvikImage() {
        if (avvikImage == null) {
            avvikImage = new PointImage();
            avvikImage.setHotspot(new Point(22, 40));
            avvikImage.setImageData(ImageUtil.imageToBytes(ImageUtil
                    .convertImageToBufferedImage(IconResources.MAP_PIN_LEFT.getImage())));
        }
        return avvikImage;
    }

    private void setBusyMessage(String msgTxt) {
        JXBusyLabel busy = new JXBusyLabel();
        messages.removeAll();
        messages.add(busy);
        messages.add(BoxUtil.createHorizontalStrut(2));
        messages.add(new JLabel(msgTxt));
        messages.add(Box.createHorizontalGlue());
        busy.setBusy(true);
        mapView.revalidate();
    }

    private void setMessage(final String msg, final ImageIcon msgIcon) {
        messages.removeAll();
        messages.add(new JLabel(msg, msgIcon, JLabel.LEFT));
        messages.add(Box.createHorizontalGlue());
    }

    public void dispose() {
        if (mapView != null)
            mapView.removeAll();
        removeAll();
        mapView = null;
    }

    private class MapPointWorker extends SwingWorker<Void, Void> {
        private final PointImage image;
        private final ImageIcon msgIcon;
        private final Punktstedfest sted;
        private Image mapImage;

        public MapPointWorker(final Punktstedfest stedfesting, final PointImage image, final ImageIcon msgIcon) {
            this.sted = stedfesting;
            this.image = image;
            this.msgIcon = msgIcon;
        }

        @Override
        protected Void doInBackground() {
            try {
                if (sted != null) {
                    Double bredde = sted.getSnappetY() != null ? sted.getSnappetY() : sted.getY();
                    Double lengde = sted.getSnappetX() != null ? sted.getSnappetX() : sted.getX();

                    if (bredde != null && lengde != null && bredde != 0 && lengde != 0) {
                        PointItem pos = new PointItem();
                        Point point = new Point();
                        point.setLocation(lengde, bredde);
                        pos.setPos(point);
                        pos.setPointImageIndex(0);

                        MapsGen mapsGen = BeanUtil.lookup(MapsGen.BEAN_NAME, MapsGen.class);
                        try {
                            byte[] mapImagebytes = mapsGen.generateMap(Collections.singletonList((MapItem) pos),
                                    Collections.singletonList(image), new Dimension(900, 768), 4);
                            mapImage = ImageUtil.bytesToImage(mapImagebytes);
                            setMessage(null, null);
                        } catch (Throwable t) {
                            setMessage(Resources.getResource("label.lastingFeilet"), IconResources.WARNING_ICON);
                            mapImage = IconResources.MAP_NORGE.getImage();
                        }
                    } else {
                        setMessage(Resources.getResource("label.manglerStedfesting"), msgIcon);
                        mapImage = IconResources.MAP_NORGE.getImage();
                    }
                } else {
                    setMessage(Resources.getResource("label.manglerPosisjon"), IconResources.WARNING_ICON);
                    mapImage = IconResources.MAP_NORGE.getImage();
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        @Override
        protected void done() {
            if (mapView != null) {
                mapView.setImage(mapImage);
            }
            loading = false;

        }
    }

    private class MapLinesWorker extends SwingWorker<Void, Void> {
        private final List<Prodstrekning> strekninger;
        private Image mapImage;

        public MapLinesWorker(final List<Prodstrekning> strekninger) {
            this.strekninger = strekninger != null ? strekninger : new ArrayList<Prodstrekning>();
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                setBusyMessage("Laster kart");
                if (strekninger.size() == 0) {
                    mapImage = IconResources.MAP_NORGE.getImage();
                    setMessage(Resources.getResource("warning.ingenStrekninger"), null);
                    return null;
                }

                List<MapItem> items = new ArrayList<MapItem>();
                List<ReflinkStrekning> reflinkStrekninger = getReflinkStrekninger(strekninger);
                for (ReflinkStrekning rs : reflinkStrekninger) {
                    LineItem li = new LineItem();
                    li.setColor(Color.BLUE);
                    li.setCoords(rs.getVectors());
                    items.add(li);
                }

                Bbox bbox = new Bbox();
                for (MapItem li : items) {
                    bbox.addBounds(li.getBbox());
                }
                double width = bbox.getWidth();
                double height = bbox.getHeight();
                double screenWidth = mapView.getWidth() - 10;
                double screenHeight = mapView.getHeight() - 10;
                double pixelsNeeded = Math.max((width / screenWidth), (height / screenHeight));
                if (items.size() == 0) {
                    mapImage = IconResources.MAP_NORGE.getImage();
                    setMessage(null, null);
                    return null;
                }
                try {
                    MapsGen mapsGen = BeanUtil.lookup(MapsGen.BEAN_NAME, MapsGen.class);
                    byte[] mapImagebytes = mapsGen.generateMap(items, new ArrayList<PointImage>(), new Dimension(
                            (int) (mapView.getWidth() * 1.5), (int) (mapView.getWidth() * 1.5)), pixelsNeeded);
                    mapImage = ImageUtil.bytesToImage(mapImagebytes);
                    setMessage(null, null);
                } catch (Throwable t) {
                    setMessage(Resources.getResource("label.lastingFeilet"), IconResources.WARNING_ICON);
                    t.printStackTrace();
                }

            } catch (Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        @Override
        protected void done() {
            if (mapView != null) {
                mapView.setImage(mapImage);
            }
            loading = false;
        }
    }

    private List<ReflinkStrekning> getReflinkStrekninger(List<Prodstrekning> strekninger) {
        List<ReflinkStrekning> reflinkStrekninger = getProdBean().getReflinkStrekningFromProdstrekning(strekninger, true);
        return reflinkStrekninger;
    }

    private ProduksjonService getProdBean() {
        ProduksjonService bean = BeanUtil.lookup(ProduksjonService.BEAN_NAME, ProduksjonService.class);
        return bean;
    }

    private class PNGNameFilter implements FilenameFilter {

        @Override
        public boolean accept(File dir, String name) {
            if (StringUtils.endsWithIgnoreCase(name, ".png")) {
                return true;
            } else {
                return false;
            }
        }

    }

    @Override
    public boolean isLoading() {
        return loading;
    }
}
