package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class KommentarPanel extends JPanel implements LoadingPanel {

    private JTextArea txtKommentar;
    private JScrollPane scrKommentar;

    private BindingGroup bindings = new BindingGroup();
    private Inspeksjon inspeksjon;

    public KommentarPanel() {
        initComponents();
        initGui();
    }

    private void initComponents() {
        txtKommentar = new JTextArea();
        txtKommentar.setLineWrap(true);
        txtKommentar.setWrapStyleWord(true);
        scrKommentar = new JScrollPane(txtKommentar);

    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
        bind();

    }

    private void bind() {
        Binding<Inspeksjon, Object, JTextArea, Object> kommentarBind = BindingHelper.createbinding(inspeksjon, "beskrivelse", txtKommentar, "text", UpdateStrategy.READ_WRITE);
        bindings.addBinding(kommentarBind);

        bindings.bind();
    }

    private void initGui() {
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.kommentar")));
        setLayout(new BorderLayout());

        add(scrKommentar, BorderLayout.CENTER);

    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();

    }

}
