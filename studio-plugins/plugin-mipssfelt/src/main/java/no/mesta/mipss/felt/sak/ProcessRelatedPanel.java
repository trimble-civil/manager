package no.mesta.mipss.felt.sak;

import no.mesta.mipss.persistence.mipssfield.ProcessRelated;
import no.mesta.mipss.persistence.mipssfield.Prosess;

public interface ProcessRelatedPanel<T extends ProcessRelated> {

    T getProcessRelatedObject();

    void setProcessRelatedObject(T t);

    void refreshProcess(Prosess prosess);
}
