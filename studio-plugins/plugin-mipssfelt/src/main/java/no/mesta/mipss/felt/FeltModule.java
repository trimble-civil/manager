package no.mesta.mipss.felt;

import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

import javax.swing.*;

@SuppressWarnings("serial")
public class FeltModule extends MipssPlugin {

    private FeltPanel pnlFelt;
    private FeltController controller;

    @Override
    public JComponent getModuleGUI() {
        return pnlFelt;
    }

    @Override
    protected void initPluginGUI() {
        initGeoTools();
        controller = new FeltController(this);
        pnlFelt = new FeltPanel(controller);

        controller.bind();
    }

    public FeltController getController() {
        return controller;
    }

    /**
     * Initialiserer geotools, gjør kun et enkelt kall mot api som
     * fører til at FaktoryRegistry scanner etter plugins..
     * <p/>
     * Dette kan ta noen sekunder, dumt å gjøre det på EDT..
     */
    private void initGeoTools() {
        new Thread("GeoTools init worker") {
            public void run() {
                try {
                    CRS.decode("EPSG:32633");
                } catch (NoSuchAuthorityCodeException e) {
                    e.printStackTrace();
                } catch (FactoryException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public void receiveMessage(PluginMessage<?, ?> message) {
        if (!(message instanceof OpenDetailMessage)) {
            throw new IllegalArgumentException("Unsupported message type");
        }

        if (this != message.getTargetPlugin()) {
            throw new IllegalStateException("Invalid plugin receiver for target");
        }

        message.processMessage();
    }

    @Override
    public void shutdown() {

    }

}
