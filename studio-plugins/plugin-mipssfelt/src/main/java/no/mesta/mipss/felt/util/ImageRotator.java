package no.mesta.mipss.felt.util;

import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import java.awt.image.BufferedImage;

public class ImageRotator {

    public static void rotate90(Bilde bilde, String loggedOnUserSign) {
        BufferedImage img = ImageUtil.bytesToImage(bilde.getBildeLob());
        BufferedImage imgSmall = ImageUtil.bytesToImage(bilde.getSmaabildeLob());
        img = ImageUtil.rotate90(img);
        imgSmall = ImageUtil.rotate90(imgSmall);
        bilde.setBildeLob(ImageUtil.imageToBytes(img));
        bilde.setSmaabildeLob(ImageUtil.imageToBytes(imgSmall));
        bilde.setEndrer(loggedOnUserSign);
        bilde.setEndringsDato(Clock.now());
    }
}
