package no.mesta.mipss.felt.sak.mangler;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.IntegerStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ManglerPanel extends JPanel {

    private final AvvikController controller;
    private SokekriterierManglerPanel pnlSokekriterier;
    private JButton btnSok;

    private JMipssBeanScrollPane scrTable;
    private JPanel pnlButton;
    private JButton btnExcel;
    private JLabel lblAntallTreff;
    private JLabel lblTreffValue;
    private JPanel pnlSokButton;

    public ManglerPanel(AvvikController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void initComponents() {
        pnlSokekriterier = new SokekriterierManglerPanel(controller);
        scrTable = new JMipssBeanScrollPane(controller.getManglerTable(), controller.getManglerTableModel());
        btnExcel = new JButton(controller.getManglerExcelAction());
        lblAntallTreff = new JLabel(Resources.getResource("label.antallTreff"));
        lblTreffValue = new JLabel();
        btnSok = new JButton(controller.getManglerSokButtonAction());

        pnlButton = new JPanel(new GridBagLayout());
        pnlButton.add(btnExcel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
        pnlButton.add(lblAntallTreff, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        pnlButton.add(lblTreffValue, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

        pnlSokButton = new JPanel(new GridBagLayout());
        pnlSokButton.add(btnSok, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));

        Binding antallBind = BindingHelper.createbinding(controller.getManglerTableModel(), "size", lblTreffValue, "text");
        IntegerStringConverter isc = new IntegerStringConverter();
        antallBind.setConverter(isc);
        antallBind.bind();

    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(pnlSokekriterier, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
        add(pnlSokButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(scrTable, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(10, 0, 2, 0), 0, 0));
        add(pnlButton, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

    }
}
