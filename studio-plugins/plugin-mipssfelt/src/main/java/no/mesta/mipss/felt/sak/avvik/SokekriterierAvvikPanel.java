package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class SokekriterierAvvikPanel extends JPanel {
    private final AvvikController controller;

    private JCheckBox chkEtterslep;
    private JCheckBox chkTilleggsarbeid;
    private JCheckBox chkTrafikksikkerhet;
    private JCheckBox chkKunSlettede;
    private JCheckBox chkFristdatoOverskredet;

    private JButton btnProsess;
    private JButton btnStatus;
    private JButton btnAarsak;
    private JLabel lblProsesserValue;

    private SokekriterierAvvikVerdiPanel pnlValg;

    public SokekriterierAvvikPanel(AvvikController controller) {
        this.controller = controller;

        initComponents();
        initGui();
        bind();
    }

    private void bind() {
        Binding<JCheckBox, Object, AvvikQueryParams, Object> etterslepBind = BindingHelper.createbinding(chkEtterslep, "selected", controller.getQueryParams(), "etterslep");
        Binding<JCheckBox, Object, AvvikQueryParams, Object> tilleggsarbeidBind = BindingHelper.createbinding(chkTilleggsarbeid, "selected", controller.getQueryParams(), "tilleggsarbeid");
        Binding<JCheckBox, Object, AvvikQueryParams, Object> trafikksikkerhetBind = BindingHelper.createbinding(chkTrafikksikkerhet, "selected", controller.getQueryParams(), "trafikksikkerhet");
        Binding<JCheckBox, Object, AvvikQueryParams, Object> kunSlettedeBind = BindingHelper.createbinding(chkKunSlettede, "selected", controller.getQueryParams(), "kunSlettede");
        Binding<JCheckBox, Object, AvvikQueryParams, Object> fristdatoOverskredetBind = BindingHelper.createbinding(chkFristdatoOverskredet, "selected", controller.getQueryParams(), "fristdatoOverskredet");

        controller.getFeltController().getBindingGroup().addBinding(etterslepBind);
        controller.getFeltController().getBindingGroup().addBinding(tilleggsarbeidBind);
        controller.getFeltController().getBindingGroup().addBinding(trafikksikkerhetBind);
        controller.getFeltController().getBindingGroup().addBinding(kunSlettedeBind);
        controller.getFeltController().getBindingGroup().addBinding(fristdatoOverskredetBind);
    }

    private void initComponents() {
        chkEtterslep = new JCheckBox(Resources.getResource("check.etterslep"));
        chkEtterslep.setToolTipText(Resources.getResource("check.etterslep.tooltip"));

        chkTilleggsarbeid = new JCheckBox(Resources.getResource("check.tilleggsarbeid"));
        chkTilleggsarbeid.setToolTipText(Resources.getResource("check.tilleggsarbeid.tooltip"));

        chkTrafikksikkerhet = new JCheckBox(Resources.getResource("check.trafikksikkerhet"));
        chkTrafikksikkerhet.setToolTipText(Resources.getResource("check.trafikksikkerhet.tooltip"));

        chkKunSlettede = new JCheckBox(Resources.getResource("check.kunSlettede"));
        chkKunSlettede.setToolTipText(Resources.getResource("check.kunSlettede.tooltip"));

        chkFristdatoOverskredet = new JCheckBox(Resources.getResource("check.fristdatoOverskredet"));
        chkFristdatoOverskredet.setToolTipText(Resources.getResource("check.fristdatoOverskredet.tooltip"));

        btnProsess = new JButton(controller.getFeltController().getVelgProsessAction(controller.getQueryParams()));
        btnStatus = new JButton(controller.getVelgAvvikstatusAction(controller.getQueryParams()));
        btnAarsak = new JButton(controller.getVelgAarsakAction(controller.getQueryParams()));
        lblProsesserValue = new JLabel(Resources.getResource("label.valgteProsesser.default"));
        Font fontProsess = lblProsesserValue.getFont();
        fontProsess = new Font(fontProsess.getName(), Font.BOLD, fontProsess.getSize());
        lblProsesserValue.setFont(fontProsess);

        pnlValg = new SokekriterierAvvikVerdiPanel(controller);
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        JPanel pnlButton = new JPanel(new GridBagLayout());
        pnlButton.add(btnProsess, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 0, 0), 0, 0));
        pnlButton.add(btnStatus, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 0, 0), 0, 0));
        pnlButton.add(btnAarsak, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 0, 0), 0, 0));

        add(chkEtterslep, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(chkTilleggsarbeid, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(chkTrafikksikkerhet, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(chkKunSlettede, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(chkFristdatoOverskredet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

        add(pnlButton, new GridBagConstraints(2, 0, 1, 3, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(pnlValg, new GridBagConstraints(3, 0, 1, 3, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));
    }
}