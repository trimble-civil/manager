package no.mesta.mipss.felt.elrapp;

import no.mesta.mipss.elrapp.ElrappValidationException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.List;

public abstract class ElrappValidator {

    public abstract List<String> validate() throws ElrappValidationException;

    protected String createXml(Object o) {
        StringWriter w = new StringWriter();
        try {
            JAXBContext ctx = JAXBContext.newInstance(o.getClass());
            Marshaller m = ctx.createMarshaller();
            m.marshal(o, w);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return w.toString();
    }
}
