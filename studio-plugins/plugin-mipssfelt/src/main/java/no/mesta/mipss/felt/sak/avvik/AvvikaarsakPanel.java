package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.persistence.mipssfield.Avvikaarsak;
import no.mesta.mipss.ui.table.AvvikaarsakTableObject;
import no.mesta.mipss.ui.table.AvvikstatusTableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import org.jdesktop.swingx.JXTable;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class AvvikaarsakPanel extends TableObjectPanel<AvvikaarsakTableObject, Avvikaarsak> {

    public AvvikaarsakPanel(List<Avvikaarsak> alleAvvikstatus) {
        super(alleAvvikstatus);
    }

    @Override
    protected JXTable getTable() {
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(AvvikstatusTableObject.class, columns);
        tableModel = new MipssRenderableEntityTableModel<AvvikaarsakTableObject>(AvvikaarsakTableObject.class, new int[]{0}, columns);
        tableModel.getEntities().clear();
        List<AvvikaarsakTableObject> list = new ArrayList<AvvikaarsakTableObject>();
        for (Avvikaarsak t : alleAvvikstatus) {
            list.add(new AvvikaarsakTableObject(t));
        }
        tableModel.getEntities().addAll(list);
        JXTable table = new JXTable(tableModel, columnModel);
        table.getColumn(0).setMaxWidth(30);

        return table;
    }

    public void setSelected(List<Long> status) {
        if (status == null)
            return;
        for (AvvikaarsakTableObject o : tableModel.getEntities()) {
            if (status.contains(o.getId())) {
                o.setValgt(Boolean.TRUE);
            }
        }
    }

    public List<Long> getSelected() {
        List<Long> ids = new ArrayList<Long>();
        for (AvvikaarsakTableObject t : checkTable.getAlleValgte()) {
            ids.add(t.getId());
        }
        return ids;
    }

    public List<String> getSelectedNames() {
        List<String> names = new ArrayList<String>();
        for (AvvikaarsakTableObject t : checkTable.getAlleValgte()) {
            names.add(t.getStatus());
        }
        return names;
    }

}