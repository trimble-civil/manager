package no.mesta.mipss.felt.inspeksjon;

import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableModel;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class InspeksjonHighlightPredicate implements HighlightPredicate {

    private final MipssRenderableEntityTableModel<InspeksjonSokResult> tableModel;
    private final JMipssBeanTable<InspeksjonSokResult> table;

    public InspeksjonHighlightPredicate(MipssRenderableEntityTableModel<InspeksjonSokResult> tableModel, JMipssBeanTable<InspeksjonSokResult> table) {
        this.tableModel = tableModel;
        this.table = table;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        int row = adapter.row;

        int rowModel = table.convertRowIndexToModel(row);

        InspeksjonSokResult inspeksjon = tableModel.get(rowModel);

        boolean highlight = false;
        if (inspeksjon != null) {
            highlight = inspeksjon.getSlettetDato() != null;
        }
        return highlight;
    }
}