package no.mesta.mipss.felt.sak.avvik;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.CoordinateUtils;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.felt.OpenItemDialog;
import no.mesta.mipss.felt.OpenItemDialog.TYPE;
import no.mesta.mipss.felt.ProsessDialog;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.ProcessRelatedPanel;
import no.mesta.mipss.felt.sak.avvik.detaljui.*;
import no.mesta.mipss.felt.sak.detaljui.SakDetaljPanel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.HendelseDetaljPanel;
import no.mesta.mipss.mipssfelt.AvvikQueryParams;
import no.mesta.mipss.mipssfelt.AvvikService;
import no.mesta.mipss.mipssfelt.AvvikSokResult;
import no.mesta.mipss.mipssfelt.RskjemaService;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.persistence.mipssfield.vo.VeiInfo;
import no.mesta.mipss.produksjonsrapport.excel.ExcelPOIHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.roadpicker.JRoadSelectDialog;
import no.mesta.mipss.valueobjects.ElrappStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@SuppressWarnings("serial")
public class AvvikButtonController {

    private static final String FUNKSJONSANSVAR = "Funksjonsansvar";
    private static final String TRAFIKKFARLIG = "Trafikkfarlig";

    private final AvvikController controller;
    private AvvikService avvikService;
    private RskjemaService rskjemaService;

    public AvvikButtonController(AvvikController controller) {
        this.controller = controller;
        avvikService = BeanUtil.lookup(AvvikService.BEAN_NAME, AvvikService.class);
        rskjemaService = BeanUtil.lookup(RskjemaService.BEAN_NAME, RskjemaService.class);
    }

    /**
     * Action for å opprette et nytt avvik
     *
     * @return
     */
    public Action getAvvikNyAction() {
        return new AbstractAction(Resources.getResource("button.nyttAvvik"), IconResources.NEW_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().getSakController().opprettNySak();
            }
        };
    }

    @SuppressWarnings("unused")
    private void afterVeirefSelected(final SakDetaljPanel sakPanel, final Avvik avvik) {
        if (controller.getFeltController().veirefIsNull(sakPanel.getSak().getVeiref())) {
            sakPanel.btnVeiEndreClick(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    afterVeirefSelected(sakPanel, avvik);
                }
            });
        } else {
            Punktveiref veiref = avvik.getVeiref();
            Punktstedfest sted = avvik.getStedfesting();
            controller.getFeltController().copyVeirefInfo(sakPanel.getSak().getVeiref(), veiref);
            controller.getFeltController().copyStedfestingInto(sakPanel.getSak().getStedfesting().get(0), sted);
        }
    }


    /**
     * Action for å eksportere søkeresultatene til avvik til Excel
     *
     * @return
     */
    public Action getAvvikExcelAction() {
        return new AbstractAction(Resources.getResource("button.excel"), IconResources.EXCEL_ICON16) {
            @SuppressWarnings("unused")
            @Override
            public void actionPerformed(ActionEvent e) {

                ExcelPOIHelper excel = new ExcelPOIHelper();
                excel.setDateFormat(MipssDateFormatter.DATE_FORMAT);
                try {
                    File tempFile = File.createTempFile("tabellExport", ".xlsx");
                    Workbook workbook = excel.createWorkbook(tempFile, excel.getExcelTemplate("funn_excel.xlsx"));
                    Sheet sheet = excel.getSheet(0, workbook);
                    List<AvvikSokResult> entities = controller.getAvvikTable().getSelectedEntities();
                    if (entities.size() == 0) {
                        entities = controller.getAvvikTableModel().getEntities();
                    }
                    int rowCount = 1;
                    DecimalFormat koordinatFormat = new DecimalFormat("###.#####");
                    for (AvvikSokResult sok : entities) {
                        Row row = excel.createRow(sheet, rowCount);

                        int c = 0;
                        excel.writeData(row, c++, sok.getRefnummer());
                        excel.setDateFormat(MipssDateFormatter.DATE_TIME_FORMAT);
                        excel.writeData(row, c++, getNewDateFromDate(sok.getOpprettetDato()));
                        excel.writeData(row, c++, sok.getSisteStatus());
                        excel.writeData(row, c++, getNewDateFromDate(sok.getSisteStatusDato()));
                        excel.setDateFormat(MipssDateFormatter.DATE_FORMAT);
                        excel.writeData(row, c++, getNewDateFromDate(sok.getTiltaksDato()));
                        //overskredet fristdato
                        if (sok.getTiltaksDato() != null && sok.getTiltaksDato().before(sok.getSisteStatusDato()))
                            excel.writeData(row, c++, "X");
                        else {
                            c++;
                        }
                        excel.writeData(row, c++, sok.getProsessKode(), true);
//						excel.writeData(row, c++, sok.getTilstandtypeNavn());
                        excel.writeData(row, c++, sok.getAarsakNavn());
//						excel.writeData(row, c++, sok.getHarHendelse());
//						excel.writeData(row, c++, sok.getHarInspeksjon());
                        excel.writeData(row, c++, sok.getTilleggsarbeid());
                        excel.writeData(row, c++, sok.getEtterslep());
                        excel.writeData(row, c++, sok.getAntallBilder());
                        excel.writeData(row, c++, sok.getFylkesnummer());
                        excel.writeData(row, c++, sok.getKommunenummer());
                        excel.writeData(row, c++, sok.getVeitype());
                        excel.writeData(row, c++, sok.getVeinummer());
                        excel.writeData(row, c++, sok.getHp());
                        excel.writeData(row, c++, sok.getMeter());
                        excel.writeData(row, c++, sok.getKjorefelt());

                        excel.writeData(row, c++, sok.getOpprettetAv());
//						excel.writeData(row, c++, sok.getPdaDfuNavn(), true);
                        excel.writeData(row, c++, sok.getBeskrivelse(), true);
                        excel.writeData(row, c++, CoordinateUtils.convertFromXYToWGS84String(sok.getX(), sok.getY()));
                        excel.writeData(row, c++, sok.getHistorikkStreng().replaceAll(", ", ",\n"));
//						excel.writeData(row, c++, sok.getHistorikk(), true);
//						excel.writeData(row, c, sok.getEstimat());
                        rowCount++;
                    }

                    excel.closeWorkbook(workbook);
                    DesktopHelper.openFile(excel.getExcelFile());
                } catch (Throwable t) {
                    t.printStackTrace();
                }
//				controller.getFeltController().openExcel(controller.getAvvikTable());
            }
        };
    }

    private Date getNewDateFromDate(Date date) {
        return date != null ? new Date(date.getTime()) : null;
    }

    /**
     * Action for å vise frem en rapportmeny for avvik
     *
     * @return
     */
    public Action getAvvikRapportAction() {
        return new AbstractAction(Resources.getResource("button.rapport.avvik"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<AvvikSokResult> avvik = controller.getAvvikTable().getSelectedEntities();
//				AvvikSokResult a = avvik.get(0);
                List<String> guids = new ArrayList<String>();
                for (AvvikSokResult a : avvik) {
                    guids.add(a.getGuid());
                }
                controller.getFeltController().openRapportAvvik(guids.toArray(new String[]{}));
            }
        };
    }

    /**
     * Action for å vise frem en rapportmeny for avvik
     *
     * @return
     */
    public Action getAvvikListRapportAction() {
        return new AbstractAction(Resources.getResource("button.rapport.avvikList"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<AvvikSokResult> avvik = null;
                if (controller.getAvvikTable().getSelectedEntities().size() == 0) {
                    avvik = controller.getAvvikTableModel().getEntities();
                } else {
                    avvik = controller.getAvvikTable().getSelectedEntities();
                }
                controller.getFeltController().openRapportAvvikList(avvik);

            }
        };
    }

    /**
     * Action for å vise frem en rapportmeny for avvik
     *
     * @return
     */
    public Action getAvvikRapportAction(final AvvikDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.rapport2"), IconResources.ADOBE_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openRapportAvvik(pnl.getAvvik().getGuid());
            }
        };
    }

    /**
     * Action for å legge til en kommentar på avviket
     *
     * @return
     */
    public Action getLeggTilKommentarAction(final AvvikDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.leggTilKommentar"), IconResources.ATTACHMENT) {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO implement
                JDialog d = new JDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), true);
                AvvikKommentarPanel avvikKommentarPanel = new AvvikKommentarPanel();
                d.add(avvikKommentarPanel);
                d.setSize(400, 200);
                d.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                d.setVisible(true);
                if (!avvikKommentarPanel.isCancelled()) {
                    Avvik avvik = controller.leggTilKommentar(pnl.getAvvik().getGuid(), avvikKommentarPanel.getKommentar());
                    if (avvik.getGuid() == null) {//avviket er ikke lagret enda
                        pnl.getAvvik().setBeskrivelse(avvik.getBeskrivelse());
                        return;
                    }
                    if (pnl.getAvvik().isLukket()) {
                        controller.getFeltController().getSakController().closeDialogForSak(pnl.getAvvik().getSakGuid());
                        controller.openAvvik(avvik);
                    } else {
                        pnl.getAvvik().setBeskrivelse(avvik.getBeskrivelse());
                    }
                }
            }
        };
    }

    /**
     * Action for å vise frem detaljer om et avvik
     *
     * @return
     */
    public Action getAvvikDetaljerAction() {
        return new AbstractAction(Resources.getResource("button.detaljer"), IconResources.DETALJER_ICON_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<AvvikSokResult> avvik = controller.getAvvikTable().getSelectedEntities();
                AvvikSokResult a = avvik.get(0);
                controller.openAvvik(a);
            }
        };
    }

    /**
     * Action for å vise et avvik i kart
     *
     * @return
     */
    public Action getAvvikKartAction() {
        return new AbstractAction(Resources.getResource("button.visIKart"), IconResources.NORGE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<AvvikSokResult> selectedEntities = controller.getAvvikTable().getSelectedEntities();
                List<Avvik> avvikList = new ArrayList<Avvik>();
                for (AvvikSokResult av : selectedEntities) {
                    avvikList.add(avvikService.hentAvvik(av.getGuid()));
                }
                controller.getFeltController().openItemsInMap(avvikList);
            }
        };
    }

    /**
     * Action for å søke frem avvik basert på opprettet_dato
     *
     * @return
     */
    public Action getAvvikSokOpprettetButtonAction(final AvvikController avvikController) {
        return new AbstractAction(Resources.getResource("button.sokOpprettet"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<AvvikSokResult> model = avvikController.getAvvikTableModel();
                AvvikQueryParams params = avvikController.getQueryParams();
                params.setRapportType(AvvikQueryParams.RapportType.OPPRETTET_I_PERIODEN);
                model.setBeanMethodArgValues(new Object[]{params});
                model.loadData();

            }
        };
    }

    /**
     * Action for å søke frem avvik basert på om de er åpne frem til til_dato eller lukket i perioden
     *
     * @return
     */
    public Action getAvvikSokApnePrTilDatoButtonAction(final AvvikController avvikController) {
        return new AbstractAction(Resources.getResource("button.sokApnePrTilDato"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<AvvikSokResult> model = avvikController.getAvvikTableModel();
                AvvikQueryParams params = avvikController.getQueryParams();
                params.setRapportType(AvvikQueryParams.RapportType.APNE_PR_TILDATO_EL_LUKKET_I_PERIODEN);
                model.setBeanMethodArgValues(new Object[]{params});
                model.loadData();
            }
        };
    }

    /**
     * Action for å søke frem avvik basert på om de har statusendring i perioden
     *
     * @return
     */
    public Action getAvvikSokStatusendringButtonAction(final AvvikController avvikController) {
        return new AbstractAction(Resources.getResource("button.sokStatusendring"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<AvvikSokResult> model = avvikController.getAvvikTableModel();
                AvvikQueryParams params = avvikController.getQueryParams();
                params.setRapportType(AvvikQueryParams.RapportType.STATUSENDRING_I_PERIODEN);
                model.setBeanMethodArgValues(new Object[]{params});
                model.loadData();
            }
        };
    }

    public Action getManglerSokButtonAction(final AvvikController avvikController) {
        return new AbstractAction(Resources.getResource("button.sok"), IconResources.SEARCH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                MipssBeanTableModel<AvvikSokResult> model = avvikController.getManglerTableModel();
                AvvikQueryParams params = avvikController.getQueryParams();
                AvvikQueryParams p = new AvvikQueryParams();
                p.setValgtKontrakt(params.getValgtKontrakt());
                p.setDateFra(params.getDateFra());
                p.setDateTil(params.getDateTil());
                p.setMangler(true);
                p.setNyeMangler(params.isNyeMangler());
                p.setAapentAvvik(params.isAapentAvvik());
                model.setBeanMethodArgValues(new Object[]{params});
                model.loadData();
            }
        };
    }

    private Long prevRefnummer;

    /**
     * Action for å åpne et avvik med en gitt id(refnummer)
     *
     * @return
     */
    public Action getAvvikHentMedIdAction() {
        return new AbstractAction(Resources.getResource("button.hentAvvikMedId"), IconResources.DETALJER_SMALL_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog dialog = new OpenItemDialog(TYPE.AVVIK, controller.getFeltController());
                dialog.setRefnummer(prevRefnummer);
                dialog.pack();
                dialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                dialog.setVisible(true);
                prevRefnummer = dialog.getRefnummer();
            }
        };
    }

    public Action getSlettAvvikStatus(final JMipssBeanTable<AvvikStatus> table, final StatusHistoryPanel statusHistoryPanel) {
        return new AbstractAction(Resources.getResource("button.slett"), IconResources.REMOVE_ITEM_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.clearSelection();
                Avvik avvik = statusHistoryPanel.getAvvik();
                AvvikStatus ps = avvik.getStatus();
                if (AvvikstatusType.GJENAPNET_STATUS.equals(ps.getAvvikstatusType().getStatus()) ||
                        AvvikstatusType.LUKKET_STATUS.equals(ps.getAvvikstatusType().getStatus()) ||
                        AvvikstatusType.NY_STATUS.equals(ps.getAvvikstatusType().getStatus())) {

                    JOptionPane.showMessageDialog(statusHistoryPanel, Resources.getResource("warning.kanIkkeSletteStatus", ps.getAvvikstatusType().getNavn()), Resources.getResource("warning.kanIkkeSletteStatus.title"), JOptionPane.WARNING_MESSAGE);
                    return;
                }
                avvik.removeStatus(ps);
                if (StringUtils.equalsIgnoreCase(ps.getAvvikstatusType().getStatus(), AvvikstatusType.LUKKET_STATUS)) {
                    avvik.setLukketDato(null);
                }

            }
        };
    }

    public Action getLeggTilAvvikStatus(final StatusHistoryPanel historyPanel) {
        return new AbstractAction(Resources.getResource("button.leggTil"), IconResources.ADD_ITEM_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog owner = (JDialog) SwingUtilities.windowForComponent(historyPanel);
                AvvikStatusDialog dialog = new AvvikStatusDialog(owner, historyPanel.getAvvik(), controller.getFeltController());
                dialog.showRelativeTo(owner);

            }
        };
    }


    public Action getEndreVeiAction(final TidStedPanel tidStedPanel, final AvvikController controller) {
        return new AbstractAction(Resources.getResource("button.endre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Avvik avvik = tidStedPanel.getAvvik();
                Punktstedfest sted = avvik.getStedfesting();
                VeiInfo veiInfo = controller.getFeltController().createVeiInfoFromPunktstedfest(sted);

                JRoadSelectDialog dialog = new JRoadSelectDialog((JDialog) SwingUtilities.windowForComponent(tidStedPanel), true, false);
                dialog.setDriftkontrakt(controller.getQueryParams().getValgtKontrakt());
                dialog.setVeiSok(veiInfo);
                dialog.setVisible(true);

                if (dialog.isSet()) {
                    veiInfo = dialog.getVeiSok();
                    if (veiInfo != null) {
                        controller.getFeltController().updateStedfestingFromVeiInfo(veiInfo, sted);
                    }
                }
            }
        };
    }

    public Action getEndreProsessAction(final ProcessRelatedPanel vurderingPanel, final JPanel panel, final AvvikController controller) {
        return new AbstractAction(Resources.getResource("button.endre")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessRelated processRelatedObject = vurderingPanel.getProcessRelatedObject();
                ProsessDialog prosessDialog = new ProsessDialog(controller.getQueryParams(), (JDialog) SwingUtilities.windowForComponent(panel), Resources.getResource("button.velgProsess"));
                prosessDialog.setSingleSelectionMode(true);
                prosessDialog.setSize(400, 450);
                prosessDialog.setLocationRelativeTo(controller.getFeltController().getPlugin().getLoader().getApplicationFrame());
                prosessDialog.setSelectedProsesser(new Long[]{processRelatedObject.getProsessId()});
                prosessDialog.setVisible(true);
                if (!prosessDialog.isCancelled()) {
                    Set<Prosess> prosesser = prosessDialog.getProsesser();
                    if (prosesser.size() > 0) {
                        Prosess prosess = prosesser.iterator().next();
                        processRelatedObject.setProsess(prosess);
                        vurderingPanel.refreshProcess(prosess);
                    }
                }
            }
        };
    }

    public Action getAvvikLagreAction(final AvvikDetaljPanel avvikDetaljPanel, final String brukerSign) {
        return new AbstractAction(Resources.getResource("button.lagre"), IconResources.SAVE_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Avvik avvik = avvikDetaljPanel.getAvvik();

                boolean avvikOk = validerAvvikInfo(avvik, avvikDetaljPanel);

                if (!avvikOk) {
                    return;
                }

                try {
                    Avvik a = controller.persistAvvik(avvik);
                    avvikDetaljPanel.setTitle(String.valueOf(a.getRefnummer()));
                    if (avvik.getRefnummer() == null)
                        avvik.setRefnummer(a.getRefnummer());
                    if (a != null) {
                        avvikDetaljPanel.getSaveStatusRegistry().reset();
                        avvik.setNewEntity(false);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    private boolean validerAvvikInfo(Avvik avvik, Component avvikDetaljPanel) {

        if (avvik.getBeskrivelse() != null && avvik.getBeskrivelse().length() >= 1000) {
            JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.descriptionTooLong"), Resources.getResource("message.valideringFeilet.title"), JOptionPane.WARNING_MESSAGE);
            return false;
        }

        if (avvik.getMerknad() != null && avvik.getMerknad().length() >= 1000) {
            JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.commentTooLong"), Resources.getResource("message.valideringFeilet.title"), JOptionPane.WARNING_MESSAGE);
            return false;
        }

        if (avvik.getArbeidType() != null && FUNKSJONSANSVAR.equals(avvik.getArbeidType().getNavn()) && avvik.getTiltaksDato() == null) {
            JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.arbeidtype"), Resources.getResource("message.trafikksikkerhettiltak.tittel"), JOptionPane.WARNING_MESSAGE);
            return false;
        }

        if (avvik.getObjektAvvikType() != null && TRAFIKKFARLIG.equals(avvik.getObjektAvvikType().getNavn()) && avvik.getTiltaksDato() == null) {
            JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.trafikkfarlig"), Resources.getResource("message.trafikksikkerhettiltak.tittel"), JOptionPane.WARNING_MESSAGE);
            return false;
        }

        if ((!getBool(avvik.getEtterslepFlagg()) && !getBool(avvik.getTilleggsarbeid())) && avvik.getTiltaksDato() == null) {
            JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.fristdatoMangler"), Resources.getResource("message.trafikksikkerhettiltak.tittel"), JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }

    private boolean getBool(Boolean bool) {
        if (bool == null)
            return false;
        return bool.booleanValue();
    }

    public Action getAvvikSlettAction(final AvvikDetaljPanel avvikDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.slett"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Avvik avvik = avvikDetaljPanel.getAvvik();
                if (isAvvikSlettbar(avvik)) {
                    try {
                        Avvik av = slettAvvik(avvik, brukerSign);
                        if (av != null) {
                            ActionEvent ev = new ActionEvent(av, 0, "slettet");
                            callback.actionPerformed(ev);
                            AvvikSokResult toRemove = null;
                            for (AvvikSokResult rs : controller.getAvvikTableModel().getEntities()) {
                                if (avvik.equals(rs.getGuid())) {
                                    toRemove = rs;
                                    break;
                                }
                            }
                            if (toRemove != null)
                                controller.getAvvikTableModel().removeItem(toRemove);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.avvik.kanikke.slettes"), Resources.getResource("message.avvik.kanikke.slettes.title"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
        };
    }

    /**
     * Action for å slette et avvik
     *
     * @return
     */
    public Action getAvvikSlettAction(final AvvikButtonPanel panel) {
        return new AbstractAction(Resources.getResource("button.slettAvvik"), IconResources.TRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<AvvikSokResult> selectedEntities = controller.getAvvikTable().getSelectedEntities();
                String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();
                String msg = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.avvik2", dagerForSletting) : Resources.getResource("warning.slett.avvikList", dagerForSletting);
                String tit = selectedEntities.size() == 1 ? Resources.getResource("warning.slett.avvik2.title") : Resources.getResource("warning.slett.avvikList.title");
                int v = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, tit, JOptionPane.OK_CANCEL_OPTION);
                if (v == JOptionPane.YES_OPTION) {
                    List<AvvikSokResult> notDeletedAvvik = avvikService.deleteAvvik(selectedEntities, controller.getFeltController().getPlugin().getLoader().getLoggedOnUserSign());
                    selectedEntities.removeAll(notDeletedAvvik);
//					avvikService.deleteAvvik(selectedEntities, controller.getFeltController().getPlugin().getLoader().getLoggedOnUserSign());
//					for (AvvikSokResult rs:selectedEntities){
//						controller.getAvvikTableModel().removeItem(rs);
//					}


                    if (notDeletedAvvik.size() != 0) {
                        String ids = getIdFromAvvikSokResultList(notDeletedAvvik);
                        String mes = Resources.getResource("message.avvikList.kanikke.slettes", ids);
                        String til = Resources.getResource("message.avvikList.kanikke.slettes.title");
                        JOptionPane.showMessageDialog(panel.getParent(), mes, til, JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        };
    }

    private String getIdFromAvvikSokResultList(List<AvvikSokResult> notDeleted) {
        List<Long> ids = new ArrayList<Long>();
        for (AvvikSokResult rs : notDeleted) {
            ids.add(rs.getRefnummer());
        }
        return StringUtils.join(ids, ',');
    }

    private boolean isAvvikSlettbar(Avvik avvik) {
        Sak sak = avvik.getSak();
        if (antallAvvikSomIkkeErSlettet(sak) > 1)
            return true;

        if ((sak.getHendelse() != null && sak.getHendelse().getSlettetDato() == null) ||
                (sak.getForsikringsskade() != null && sak.getForsikringsskade().getSlettetDato() == null))
            return false;

        return true;
    }

    private int antallAvvikSomIkkeErSlettet(Sak sak) {
        int c = 0;
        for (Avvik av : sak.getAvvik()) {
            if (av.getSlettetDato() == null)
                c++;
        }
        return c;
    }

    private Avvik slettAvvik(Avvik avvik, String brukerSign) throws Exception {
        String dagerForSletting = KonfigparamPreferences.getInstance().hentEnForApp("MIPSS_FELT", "dagerForSletting").getVerdi();
        String refnummer = avvik.getRefnummer() != null ? avvik.getRefnummer().toString() : "";
        String msg = Resources.getResource("warning.slett.avvik", dagerForSletting, refnummer);
        String title = Resources.getResource("warning.slett.avvik.title", refnummer);
        int response = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getModuleGUI(), msg, title, JOptionPane.YES_NO_CANCEL_OPTION);
        if (response == JOptionPane.YES_OPTION) {
            return avvikService.deleteAvvik(avvik, brukerSign);
        }
        return null;
    }

    public Action getAvvikGjenopprettAction(final AvvikDetaljPanel avvikDetaljPanel, final String brukerSign, final Action callback) {
        return new AbstractAction(Resources.getResource("button.gjenopprett"), IconResources.UNTRASH_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Avvik avvik = avvikDetaljPanel.getAvvik();
                if (avvik.getSak().getSlettetDato() != null) {
                    JOptionPane.showMessageDialog(avvikDetaljPanel, Resources.getResource("message.avvik.gjenoppretting.ikkemulig"), Resources.getResource("message.avvik.gjenoppretting.ikkemulig.title"), JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                try {
                    Avvik av = avvikService.restoreAvvik(avvik, brukerSign);
                    if (av != null) {
                        avvik.setSlettetAv(null);
                        avvik.setSlettetDato(null);
                        ActionEvent ev = new ActionEvent(av, 0, "gjenopprett");
                        callback.actionPerformed(ev);
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        };
    }

    public Action getLukkAvvikAction(final StatusHistoryPanel statusHistoryPanel, final AvvikDetaljPanel avvikDetaljPanel) {
        return new AbstractAction(Resources.getResource("button.lukkAvvik"), IconResources.YELLOW_HELMET_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                int v = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("message.lukkAvvik"), Resources.getResource("message.lukkAvvik.title"), JOptionPane.YES_NO_OPTION);
                if (v != JOptionPane.YES_OPTION)
                    return;

                statusHistoryPanel.removeSelection();
                Avvik avvik = statusHistoryPanel.getAvvik();
                AvvikstatusType oldStatus = avvik.getStatus().getAvvikstatusType();
                avvik.setLukketDato(Clock.now());
                avvik.setEndretAv(controller.getFeltController().getLoggedOnUserSign());
                Avvik av = avvikService.persistAvvik(avvik, controller.getFeltController().getLoggedOnUserSign());
                avvik.setEndretAv(av.getEndretAv());
                avvik.setEndretDato(av.getEndretDato());
                if (!av.getStatus().getAvvikstatusType().equals(oldStatus)) {
                    avvik.addStatus(av.getStatus());
                }
                if(avvik.getSak() != null && avvik.getSak().getHendelse() !=null) {
                    Hendelse hendelse = avvik.getSak().getHendelse();
                    hendelse.setUtbedret(true);
                    HendelseDetaljPanel hendelsePanel = avvikDetaljPanel.getSakDetaljPanel().getHendelseDetaljPanel();
                    if(hendelsePanel != null) {
                        avvikDetaljPanel.getSakDetaljPanel().oppdaterElrappKule(new ElrappStatus(ElrappStatus.Status.ENDRET));
                        rskjemaService.persistHendelse(hendelse, controller.getFeltController().getLoggedOnUserSign());
                        hendelsePanel.getSaveStatusRegistry().reset();
                    }
                }

                avvikDetaljPanel.getSaveStatusRegistry().reset();
                controller.reloadTable();
                controller.reloadManglerTable();
            }
        };
    }

    public Action getGjenapneAvvikAction(final StatusHistoryPanel statusHistoryPanel) {
        return new AbstractAction(Resources.getResource("button.gjenapne"), IconResources.YELLOW_HELMET_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {

                Boolean blirMangel = blirAvviketMangel(statusHistoryPanel.getAvvik());

                if (blirMangel) {
                    int j = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("message.gjenapneBlirMangel"), Resources.getResource("message.gjenapneBlirMangel.title"), JOptionPane.YES_NO_OPTION);
                    if (j != JOptionPane.YES_OPTION) {
                        return;
                    }
                } else {
                    int v = JOptionPane.showConfirmDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), Resources.getResource("message.gjenapneAvvik"), Resources.getResource("message.gjenapneAvvik.title"), JOptionPane.YES_NO_OPTION);
                    if (v != JOptionPane.YES_OPTION)
                        return;
                }

                statusHistoryPanel.removeSelection();
                Avvik avvik = statusHistoryPanel.getAvvik();
                AvvikstatusType oldStatus = avvik.getStatus().getAvvikstatusType();
                avvik.setLukketDato(null);
                Avvik av = avvikService.persistAvvik(avvik, controller.getFeltController().getLoggedOnUserSign());
                avvik.setEndretAv(av.getEndretAv());
                avvik.setEndretDato(av.getEndretDato());
                if (!av.getStatus().getAvvikstatusType().equals(oldStatus)) {
                    avvik.addStatus(av.getStatus());
                }
            }
        };
    }

    /**
     * Sjekker om avviket er eller blir en mangel.
     * Denne metoden tar ikke hensyn til om avviket er lukket eller ikke.
     * Benytt funksjonen mangel_sjekk i feltstudio_pk for dette.
     *
     * @param avvik
     * @return
     */
    private boolean blirAvviketMangel(Avvik avvik) {

        if (avvik.getTiltaksDato() == null) {
            return false;
        }

        if (avvik.getTiltaksDato().before(new Date())) {
            if (!avvik.getTrafikksikkerhetFlagg() && (avvik.getEtterslepFlagg() || avvik.getTilleggsarbeid())) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    public Action getManglerExcelAction() {
        return new AbstractAction(Resources.getResource("button.excel.mangler"), IconResources.EXCEL_ICON16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getFeltController().openExcel(controller.getManglerTable());
            }
        };
    }

    public Action getVelgAvvikstatusAction(AvvikQueryParams queryParams) {
        return new AbstractAction(Resources.getResource("button.velgstatus"), IconResources.PROSESS_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<AvvikstatusType> statusTyper = controller.getAlleStatuser();
                AvvikstatusTypeDialog d = new AvvikstatusTypeDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), statusTyper);
                d.setSelected(controller.getQueryParams().getAvvikstatus());
                d.setSize(300, 350);
                d.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                d.setVisible(true);
                if (!d.isCancelled()) {
                    List<String> selectedIds = d.getSelectedIds();
                    if (selectedIds.size() > 0) {
                        controller.getQueryParams().setAvvikstatus(selectedIds);
                    } else {
                        controller.getQueryParams().setAvvikstatus(null);
                    }
                }
            }
        };
    }

    public Action getVelgAarsakAction(AvvikQueryParams queryParams) {
        return new AbstractAction(Resources.getResource("button.velgaarsak"), IconResources.PROSESS_16) {
            @Override
            public void actionPerformed(ActionEvent e) {

                List<Avvikaarsak> avvikaarsaker = controller.getAvvikaarsaker();
                AvvikaarsakDialog d = new AvvikaarsakDialog(controller.getFeltController().getPlugin().getLoader().getApplicationFrame(), avvikaarsaker);
                d.setSelected(controller.getQueryParams().getAvvikaarsak());
                d.setSize(300, 350);
                d.setLocationRelativeTo(controller.getFeltController().getPlugin().getModuleGUI());
                d.setVisible(true);
                if (!d.isCancelled()) {
                    List<Long> selectedIds = d.getSelectedIds();
                    List<String> selectedNames = d.getSelectedNames();
                    if (selectedIds.size() > 0) {
                        controller.getQueryParams().setAvvikaarsak(selectedIds);
                        controller.getQueryParams().setAvvikaarsakStr(selectedNames);
                    } else {
                        controller.getQueryParams().setAvvikaarsak(null);
                        controller.getQueryParams().setAvvikaarsakStr(null);
                    }
                }
            }
        };
    }

    public Action getVisAvvikINarhetenAction(final AvvikDetaljPanel pnl) {
        return new AbstractAction(Resources.getResource("button.visAvvikINarheten"), IconResources.RADAR_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
//				List<AvvikAvstand> sokAvvikINarheten = controller.sokAvvikINarheten(pnl.getAvvik());
                JDialog dialog = new JDialog(SwingUtilities.windowForComponent(pnl));
                AvvikINarhetenPanel pnlAvvik = new AvvikINarhetenPanel(controller, pnl.getAvvik());
                dialog.add(pnlAvvik);
                dialog.setSize(650, 400);
                dialog.setLocationRelativeTo(pnl);
                dialog.setVisible(true);


            }
        };
    }
}
