package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.ProcessRelatedPanel;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class VurderingPanel extends JPanel implements LoadingPanel, ProcessRelatedPanel<Avvik> {

    private static final Logger log = LoggerFactory.getLogger(VurderingPanel.class);

    private JLabel lblProsess;
    private JLabel lblAarsak;
    private JLabel lblFrist;
    private JLabel lblObjektType;
    private JLabel lblObjektAvvikType;
    private JLabel lblObjektAvvikKategori;
    private JLabel lblObjektStedTypeAngivelse;
    private JLabel lblArbeidType;
    private JLabel lblMerknad;

    private JFormattedTextField fldProsess;
    private JComboBox cmbAarsak;
    private JComboBox cmbObjektType;
    private JComboBox cmbObjektAvvikType;
    private JComboBox cmbObjektAvvikKategori;
    private JComboBox cmbObjektStedTypeAngivelse;
    private JComboBox cmbArbeidType;
    private JTextField fldMerknad;

    private JCheckBox chkEtterslep;

    private JMipssDatePicker dateFrist;

    private JButton btnEndreProsess;
    private final AvvikController controller;
    private Avvik avvik;
    private BindingGroup bindings = new BindingGroup();
    private List<Avvikaarsak> aarsaker;
    private List<ObjektType> objektTyper;
    private List<ObjektAvvikType> objektAvvikTyper;
    private List<ObjektAvvikKategori> objektAvvikKategorier;
    private List<ObjektStedTypeAngivelse> objektStedTypeAngivelser;
    private List<ArbeidType> arbeidTyper;

    public VurderingPanel(AvvikController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    public void setAvvik(Avvik avvik) {
        this.avvik = avvik;
        aarsaker = controller.getAvvikaarsaker(avvik);
        arbeidTyper = controller.getArbeidTyper(false);
        if(avvik.getProsess()!=null) {
            Long kontraktId = avvik.getSak().getKontraktId();
            Long prosessId = avvik.getProsess().getId();
            objektTyper = controller.getObjektTyper(false, prosessId, kontraktId);
            objektAvvikTyper = controller.getObjektAvvikTyper(false, prosessId, kontraktId);
            objektAvvikKategorier = controller.getObjektAvvikKategorier(false, prosessId, kontraktId);
            objektStedTypeAngivelser = controller.getObjektStedTypeAngivelser(false, prosessId, kontraktId);
        } else {
            objektTyper = new ArrayList<ObjektType>();
            objektAvvikTyper = new ArrayList<ObjektAvvikType>();
            objektAvvikKategorier = new ArrayList<ObjektAvvikKategori>();
            objektStedTypeAngivelser = new ArrayList<ObjektStedTypeAngivelse>();
        }
        bind();

        if (avvik.isMangel()) {
            chkEtterslep.setEnabled(false);
            dateFrist.setEditable(false);
            dateFrist.setEnabled(false);
            dateFrist.setDate(avvik.getTiltaksDato());
            dateFrist.setBackground(Color.red);

        }

    }

    public Avvik getAvvik() {
        return this.avvik;
    }

    private void bind() {
        Binding<Avvik, Object, JFormattedTextField, Object> prosessBind = BindingHelper.createbinding(avvik, "prosessString", fldProsess, "text");
        Binding<Avvik, Object, JTextField, Object> merknadBind = BindingHelper.createbinding(avvik, "merknad", fldMerknad, "text", UpdateStrategy.READ_WRITE);

        Binding<Avvik, Object, JCheckBox, Object> etterslepBind = BindingHelper.createbinding(avvik, "etterslepFlagg", chkEtterslep, "selected", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<Avvikaarsak>, JComboBox> comboAarsakBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, aarsaker, cmbAarsak);
        Binding<Avvik, Object, JComboBox, Object> aarsakBind = BindingHelper.createbinding(avvik, "aarsak", cmbAarsak, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektType>, JComboBox> comboObjektTypeBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektTyper, cmbObjektType);
        Binding<Avvik, Object, JComboBox, Object> objektTypeBind = BindingHelper.createbinding(avvik, "objektType", cmbObjektType, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektAvvikType>, JComboBox> comboObjektAvvikTypeBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektAvvikTyper, cmbObjektAvvikType);
        Binding<Avvik, Object, JComboBox, Object> objektAvvikTypeBind = BindingHelper.createbinding(avvik, "objektAvvikType", cmbObjektAvvikType, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektAvvikKategori>, JComboBox> comboObjektAvvikKategoriBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektAvvikKategorier, cmbObjektAvvikKategori);
        Binding<Avvik, Object, JComboBox, Object> objektAvvikKategoriBind = BindingHelper.createbinding(avvik, "objektAvvikKategori", cmbObjektAvvikKategori, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ObjektStedTypeAngivelse>, JComboBox> comboObjektStedTypeAngivelseBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, objektStedTypeAngivelser, cmbObjektStedTypeAngivelse);
        Binding<Avvik, Object, JComboBox, Object> objektStedTypeAngivelseBind = BindingHelper.createbinding(avvik, "objektStedTypeAngivelse", cmbObjektStedTypeAngivelse, "selectedItem", UpdateStrategy.READ_WRITE);

        JComboBoxBinding<?, List<ArbeidType>, JComboBox> comboArbeidTypeBind = SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE, arbeidTyper, cmbArbeidType);
        Binding<Avvik, Object, JComboBox, Object> arbeidTypeBind = BindingHelper.createbinding(avvik, "arbeidType", cmbArbeidType, "selectedItem", UpdateStrategy.READ_WRITE);

        Binding<Avvik, Object, JMipssDatePicker, Object> fristBind = BindingHelper.createbinding(avvik, "tiltaksDato", dateFrist, "date", UpdateStrategy.READ_WRITE);

        Binding<Avvik, Object, VurderingPanel, Object> fristEtterslepBind = BindingHelper.createbinding(avvik, "etterslepFlagg", this, "etterslepTillegg", UpdateStrategy.READ_WRITE);
        Binding<Avvik, Object, VurderingPanel, Object> fristTilleggBind = BindingHelper.createbinding(avvik, "tilleggsarbeid", this, "etterslepTillegg", UpdateStrategy.READ_WRITE);
        Binding<Avvik, Object, VurderingPanel, Object> fristTrafikksikkerhetBind = BindingHelper.createbinding(avvik, "trafikksikkerhetFlagg", this, "etterslepTillegg", UpdateStrategy.READ_WRITE);

        bindings.addBinding(prosessBind);
        bindings.addBinding(merknadBind);
        if (!avvik.isMangel()) { //skal ikke kunne endres dersom avviket er en mangel
            bindings.addBinding(etterslepBind);
            bindings.addBinding(fristBind);
            bindings.addBinding(fristTilleggBind);
            bindings.addBinding(fristEtterslepBind);
            bindings.addBinding(fristTrafikksikkerhetBind);

        } else {
            chkEtterslep.setSelected(avvik.getEtterslepFlagg());
        }

        bindings.addBinding(comboAarsakBind);
        bindings.addBinding(aarsakBind);
        bindings.addBinding(comboObjektTypeBind);
        bindings.addBinding(objektTypeBind);
        bindings.addBinding(comboObjektAvvikTypeBind);
        bindings.addBinding(objektAvvikTypeBind);
        bindings.addBinding(comboObjektAvvikKategoriBind);
        bindings.addBinding(objektAvvikKategoriBind);
        bindings.addBinding(comboObjektStedTypeAngivelseBind);
        bindings.addBinding(objektStedTypeAngivelseBind);
        bindings.addBinding(comboArbeidTypeBind);
        bindings.addBinding(arbeidTypeBind);

        bindings.bind();
    }

    public BindingGroup getBindingGroup() {
        return this.bindings;
    }

    /**
     * Setter fristdato-feltet inaktivt dersom etterslep eller tilleggsarbeid er valgt, dersom trafikksikkerhet skal frist aktiveres.
     *
     * @param etterslepTillegg ikke i bruk, må være tilstede pga bindingrammeverket.
     */
    public void setEtterslepTillegg(boolean etterslepTillegg) {
        if (avvik.isMangel())
            return;

        if (avvik.getTrafikksikkerhetFlagg()) {
            dateFrist.setEnabled(true);
            return;
        }

        //etterslep eller tilleggsarbeid er valgt, fjern fristdato (dersom ikke trafikkfare er angitt)
        if ((avvik.getEtterslepFlagg() != null && avvik.getEtterslepFlagg() == true) &&
                (avvik.getTrafikksikkerhetFlagg() == null || avvik.getTrafikksikkerhetFlagg() == false)) {
            avvik.setTiltaksDato(null);
            dateFrist.setEnabled(false);
        } else {
            dateFrist.setEnabled(true);
        }
    }

    private void initComponents() {
        lblProsess = new JLabel(Resources.getResource("label.prosess"));
        lblAarsak = new JLabel(Resources.getResource("label.aarsak"));
        lblFrist = new JLabel(Resources.getResource("label.frist"));
        lblObjektType = new JLabel(Resources.getResource("label.objektType"));
        lblObjektAvvikType = new JLabel(Resources.getResource("label.objektAvvikType"));
        lblObjektAvvikKategori = new JLabel(Resources.getResource("label.objektAvvikKategori"));
        lblObjektStedTypeAngivelse = new JLabel(Resources.getResource("label.objektStedTypeAngivelse"));
        lblArbeidType = new JLabel(Resources.getResource("label.arbeidType"));
        lblMerknad = new JLabel(Resources.getResource("label.merknad"));

        fldProsess = new JFormattedTextField();
        fldProsess.setEditable(false);

        fldMerknad = new JTextField();
        fldMerknad.setEditable(true);

        cmbAarsak = new JComboBox();
        MipssListCellRenderer<Avvikaarsak> aarsakRenderer = new MipssListCellRenderer<Avvikaarsak>();
        cmbAarsak.setRenderer(aarsakRenderer);

        cmbObjektType = new JComboBox();
        MipssListCellRenderer<ObjektType> objektTypeRenderer = new MipssListCellRenderer<ObjektType>();
        cmbObjektType.setRenderer(objektTypeRenderer);

        cmbObjektAvvikType = new JComboBox();
        MipssListCellRenderer<ObjektAvvikType> objektAvvikTypeRenderer = new MipssListCellRenderer<ObjektAvvikType>();
        cmbObjektAvvikType.setRenderer(objektAvvikTypeRenderer);

        cmbObjektAvvikKategori = new JComboBox();
        MipssListCellRenderer<ObjektAvvikKategori> objektAvvikKategoriRenderer = new MipssListCellRenderer<ObjektAvvikKategori>();
        cmbObjektAvvikKategori.setRenderer(objektAvvikKategoriRenderer);

        cmbObjektStedTypeAngivelse = new JComboBox();
        MipssListCellRenderer<ObjektStedTypeAngivelse> objektStedTypeAngivelseRenderer = new MipssListCellRenderer<ObjektStedTypeAngivelse>();
        cmbObjektStedTypeAngivelse.setRenderer(objektStedTypeAngivelseRenderer);

        cmbArbeidType = new JComboBox();
        MipssListCellRenderer<ArbeidType> arbeidTypeRenderer = new MipssListCellRenderer<ArbeidType>();
        cmbArbeidType.setRenderer(arbeidTypeRenderer);

        chkEtterslep = new JCheckBox(Resources.getResource("label.etterslep"));

        dateFrist = new JMipssDatePicker((Date) null);

        btnEndreProsess = new JButton(controller.getEndreProsessAction(this));
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.vurdering")));
        JPanel pnlCheck = new JPanel(new FlowLayout());
        pnlCheck.add(chkEtterslep);

        add(lblAarsak, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbAarsak, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblProsess, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(fldProsess, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        add(btnEndreProsess, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 1), 0, 0));

        add(lblObjektType, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektType, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblObjektAvvikType, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektAvvikType, new GridBagConstraints(1, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblObjektAvvikKategori, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektAvvikKategori, new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblObjektStedTypeAngivelse, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbObjektStedTypeAngivelse, new GridBagConstraints(1, 5, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(lblMerknad, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(fldMerknad, new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));

        add(lblArbeidType, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 2, 5, 0), 0, 0));
        add(cmbArbeidType, new GridBagConstraints(1, 8, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 2), 0, 0));

        add(pnlCheck, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        add(lblFrist, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 2, 0, 0), 0, 0));
        add(dateFrist, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        // Do nothing
    }

    public void setLukket(boolean lukketAvvik) {
        cmbAarsak.setEnabled(!lukketAvvik);
        cmbArbeidType.setEnabled(!lukketAvvik);
        cmbObjektAvvikKategori.setEnabled(!lukketAvvik);
        cmbObjektAvvikType.setEnabled(!lukketAvvik);
        cmbObjektStedTypeAngivelse.setEnabled(!lukketAvvik);
        cmbObjektType.setEnabled(!lukketAvvik);
        fldMerknad.setEnabled(!lukketAvvik);
        if (!avvik.isMangel()) {
            chkEtterslep.setEnabled(!lukketAvvik);
            if (!lukketAvvik) {
                setEtterslepTillegg(false);
            } else {
                dateFrist.setEnabled(!lukketAvvik);
            }
        }

        btnEndreProsess.setEnabled(!lukketAvvik);
    }

    public void refreshProcess(Prosess prosess) {
        Long kontraktId = avvik.getSak().getKontrakt().getId();
        Long prosessId = prosess.getId();

        avvik.setObjektType(null);
        avvik.setObjektAvvikType(null);
        avvik.setObjektAvvikKategori(null);
        avvik.setObjektStedTypeAngivelse(null);

        updateList(objektTyper, cmbObjektType, controller.getObjektTyper(true, prosessId, kontraktId));
        updateList(objektAvvikTyper, cmbObjektAvvikType, controller.getObjektAvvikTyper(true, prosessId, kontraktId));
        updateList(objektAvvikKategorier, cmbObjektAvvikKategori, controller.getObjektAvvikKategorier(true, prosessId, kontraktId));
        updateList(objektStedTypeAngivelser, cmbObjektStedTypeAngivelse, controller.getObjektStedTypeAngivelser(true, prosessId, kontraktId));
    }

    private <T> void updateList(List<T> localList, JComboBox comboBox, List<T> newContent) {
        localList.clear();
        localList.addAll(newContent);
        if(localList.size()>0) {
            comboBox.setSelectedIndex(0);
        }
    }

    @Override
    public Avvik getProcessRelatedObject() {
        return avvik;
    }

    @Override
    public void setProcessRelatedObject(Avvik avvik) {
        this.avvik = avvik;
    }
}
