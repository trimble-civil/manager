package no.mesta.mipss.felt.sak.bilde;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.ui.JMipssDatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

@SuppressWarnings("serial")
public class BildeDatoPanel extends JPanel {
    private boolean cancelled = true;
    private JMipssDatePicker pkrDato;
    private JLabel lblDato;
    private JButton ok;
    private JButton cancel;
    private JPanel pnlDato;
    private Container pnlButtons;

    public BildeDatoPanel() {
        initComponents();
        initGui();
    }

    private void initComponents() {
        ok = new JButton(Resources.getResource("button.leggTil"));
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = false;
                SwingUtilities.windowForComponent(BildeDatoPanel.this).dispose();
            }
        });
        cancel = new JButton(Resources.getResource("button.avbryt"));
        cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.windowForComponent(BildeDatoPanel.this).dispose();
            }
        });

        pkrDato = new JMipssDatePicker(Clock.now());

        lblDato = new JLabel("Bildedato:");
        pnlDato = new JPanel(new GridBagLayout());
        pnlDato.add(lblDato, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
        pnlDato.add(pkrDato, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 0, 10), 0, 0));

        pnlButtons = new JPanel(new GridBagLayout());
        pnlButtons.add(ok, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
        pnlButtons.add(cancel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 10, 10), 0, 0));

    }

    private void initGui() {
        setLayout(new BorderLayout());
        add(pnlDato, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public Date getDato() {
        return pkrDato.getDate();
    }

}
