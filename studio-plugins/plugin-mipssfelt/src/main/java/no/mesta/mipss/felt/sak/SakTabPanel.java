package no.mesta.mipss.felt.sak;

import no.mesta.mipss.felt.FeltController;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.avvik.AvvikPanel;
import no.mesta.mipss.felt.sak.mangler.ManglerPanel;
import no.mesta.mipss.felt.sak.rskjema.RskjemaPanel;
import no.mesta.mipss.resources.images.IconResources;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

@SuppressWarnings("serial")
public class SakTabPanel extends JPanel {

    private JTabbedPane tabSak;
    private SakPanel pnlSak;
    private AvvikPanel pnlAvvik;
    private RskjemaPanel pnlRskjema;
    private final FeltController controller;
    private ManglerPanel pnlMangler;

    public SakTabPanel(FeltController controller) {
        this.controller = controller;
        initComponents();
        initGui();
    }

    private void initComponents() {
        tabSak = new JTabbedPane();
        pnlSak = new SakPanel(controller);
        pnlAvvik = new AvvikPanel(controller);
        pnlRskjema = new RskjemaPanel(controller);
        pnlMangler = new ManglerPanel(controller.getAvvikController());
        tabSak.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                stateIsChanged(e);
            }
        });
    }


    public JTabbedPane getTabSak() {
        return tabSak;
    }

    /**
     * Workaround for å kunne disable og enable periodevelger dersom brukeren har valgt "kommende mangler" under mangler fanen.
     *
     * @param e
     */
    private void stateIsChanged(ChangeEvent e) {
        JTabbedPane source = (JTabbedPane) e.getSource();
        Component selectedComponent = source.getSelectedComponent();
        if (selectedComponent instanceof ManglerPanel) {
            if (controller.getAvvikController().getQueryParams().isKommendeMangler()) {
                controller.getCommonQueryParams().setDateEnabled(false);
            }
        } else {
            controller.getCommonQueryParams().setDateEnabled(true);
        }
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        tabSak.addTab(Resources.getResource("saktab.sakbehandling"), IconResources.SAKSBEHANDLING_ICON_16, pnlSak);
        tabSak.addTab(Resources.getResource("saktab.avvik"), IconResources.FUNN_SMALL_ICON_16, pnlAvvik);
        tabSak.addTab(Resources.getResource("saktab.rskjema"), IconResources.ELRAPP_ICON_16, pnlRskjema);
        tabSak.addTab(Resources.getResource("saktab.mangler"), IconResources.WARNING16, pnlMangler);

        add(tabSak, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

}
