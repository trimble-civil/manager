package no.mesta.mipss.felt.util;

import no.mesta.mipss.common.ImageUtil;
import no.mesta.mipss.core.ServerUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Bilde;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TooManyListenersException;

public class ImageDropper implements DropTargetListener {

    private final ImageDropListener listener;
    private final String loggedOnUserSign;
//	private DataFlavor urlFlavor;

    public ImageDropper(JComponent c, ImageDropListener listener, String loggedOnUserSign) {
        this.listener = listener;
//		try {
//			urlFlavor = new DataFlavor ("application/x-java-url; class=java.net.URL");
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
        this.loggedOnUserSign = loggedOnUserSign;

        makeDroppable(c);
    }

    private void makeDroppable(final JComponent c) {
        DropTarget dt = new DropTarget();
        try {
            dt.addDropTargetListener(this);
        } catch (TooManyListenersException e) {
            e.printStackTrace();
        }

        c.addHierarchyListener(new HierarchyListener() {
            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                java.awt.Component parent = c.getParent();
                if (parent == null) {
                    c.setDropTarget(null);
                } else {
                    new DropTarget(c, ImageDropper.this);
                }
            }
        });
        if (c.getParent() != null)
            new java.awt.dnd.DropTarget(c, this);

    }

    /**
     * Aksepterer kun drops av bilder
     *
     * @param evt
     * @return
     */
    public boolean isDropAccepted(DropTargetDragEvent evt) {
        List<DataFlavor> flavors = evt.getCurrentDataFlavorsAsList();
        for (DataFlavor f : flavors) {
            System.out.println(f);
            if (f.equals(DataFlavor.imageFlavor)) {
                return true;
            }
            if (f.equals(DataFlavor.javaFileListFlavor)) {
                return true;
            }
//			if (f.equals(urlFlavor)){
//				return true;
//			}
        }
        return true;
    }

    @Override
    public void dragEnter(DropTargetDragEvent evt) {
        if (!isDropAccepted(evt)) {
            evt.rejectDrag();
        }

    }

    @Override
    public void dragOver(DropTargetDragEvent evt) {
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent evt) {
    }

    @Override
    public void dragExit(DropTargetEvent evt) {

    }

    @Override
    public void drop(DropTargetDropEvent evt) {
        try {
            Transferable transferable = evt.getTransferable();
            if (transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                evt.acceptDrop(DnDConstants.ACTION_COPY);
                BufferedImage data = (BufferedImage) transferable.getTransferData(DataFlavor.imageFlavor);
                byte[] imageBytes = ImageUtil.imageToBytes(data);
                TransferImage image = new TransferImage(imageBytes, new File("draggedFile.jpg"));
                Bilde bilde = createBildeFromTransferImage(image);
                listener.imageDropped(new Bilde[]{bilde});
            } else if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                //les fil til buffered image
                evt.acceptDrop(DnDConstants.ACTION_COPY);
                @SuppressWarnings("unchecked")
                List<File> data = (List<File>) transferable.getTransferData(DataFlavor.javaFileListFlavor);
                List<Bilde> list = new ArrayList<Bilde>();
                for (File f : data) {
                    byte[] bilde = ImageUtil.readFromDisk(f);
                    TransferImage ti = new TransferImage(bilde, f);
                    Bilde nyttbilde = createBildeFromTransferImage(ti);
                    list.add(nyttbilde);
                }
                listener.imageDropped(list.toArray(new Bilde[list.size()]));
            }
//			else if (transferable.isDataFlavorSupported(urlFlavor)){
//				evt.acceptDrop(DnDConstants.ACTION_COPY);
//				URL url = (URL)transferable.getTransferData(urlFlavor);
//				InputStream stream = url.openStream();
//				byte[] imageBytes = IOUtils.toByteArray(stream);
//				TransferImage image = new TransferImage(imageBytes, new File("draggedFileUrl.jpg"));
//				listener.imageDropped(new TransferImage[]{image});
//			}
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(evt);


    }

    private Bilde createBildeFromTransferImage(TransferImage i) {
        Bilde bilde = new Bilde();
        bilde.setGuid(ServerUtils.getInstance().fetchGuid());
        bilde.setOppretter(loggedOnUserSign);
        bilde.setBildeLob(i.getData());
        bilde.setSmaabildeLob(ImageUtil.getScaledInstance(new ByteArrayInputStream(bilde.getBildeLob()), 320, 240, 0.5f));
        bilde.setNyDato(Clock.now());
        BufferedImage image = ImageUtil.bytesToImage(i.getData());
        bilde.setBredde(image.getWidth());
        bilde.setHoyde(image.getHeight());
        bilde.setFilnavn(i.getFile().getName());
        return bilde;
    }

    /**
     * Interface for komponenter som skal kunne motta bilder via DnD
     *
     * @author harkul
     */
    public static interface ImageDropListener {
        void imageDropped(Bilde[] images);
    }
}
