package no.mesta.mipss.felt.sak.detaljui;

import no.mesta.mipss.felt.TabHeader;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ChangePropertyListener implements PropertyChangeListener {
    public ChangePropertyListener(SakDetaljTabPanel tabPanel, JTabbedPane tabPane) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        SakDetaljTabPanel pnl = (SakDetaljTabPanel) evt.getSource();
        TabHeader head = pnl.getTabHeader();
        if (evt.getNewValue() == Boolean.TRUE) {
            head.setChanged(true);
        }
        if (evt.getNewValue() == Boolean.FALSE) {
            head.setChanged(false);
        }
    }
}