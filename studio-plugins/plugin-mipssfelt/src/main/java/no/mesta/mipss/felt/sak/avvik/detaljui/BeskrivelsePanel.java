package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.StringNullConverter;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Avvik;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class BeskrivelsePanel extends JPanel implements LoadingPanel {

    private Avvik avvik;
    private JTextArea txtBeskrivelse;
    private JScrollPane scrBeskrivelse;
    private BindingGroup bindings = new BindingGroup();

    public BeskrivelsePanel() {
        initComponents();
        initGui();

        /*Dimension dim = getPreferredSize();
        dim.setSize(dim.getWidth(), 83);
        setPreferredSize(dim);*/
    }

    private void initComponents() {
        txtBeskrivelse = new JTextArea();
        txtBeskrivelse.setWrapStyleWord(true);
        txtBeskrivelse.setLineWrap(true);

        scrBeskrivelse = new JScrollPane(txtBeskrivelse);
    }

    private void initGui() {
        setLayout(new GridBagLayout());

        JPanel pnlBeskrivelse = new JPanel(new BorderLayout());
        pnlBeskrivelse.setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.beskrivelseAvvik")));
        pnlBeskrivelse.add(scrBeskrivelse, BorderLayout.CENTER);

        Dimension dim = pnlBeskrivelse.getPreferredSize();
        dim.setSize(dim.getWidth(), 75);
        pnlBeskrivelse.setPreferredSize(dim);

        add(pnlBeskrivelse, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }

    public void setAvvik(Avvik avvik) {
        this.avvik = avvik;
        bind();
    }

    private void bind() {
        Binding<Avvik, String, JTextArea, String> beskrivelseBind = BindingHelper.createbinding(avvik, "beskrivelse", txtBeskrivelse, "text", UpdateStrategy.READ_WRITE);
        StringNullConverter sn = new StringNullConverter();
        beskrivelseBind.setConverter(sn);
        bindings.addBinding(beskrivelseBind);
        bindings.bind();
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();
    }

    public void setLukket(boolean lukketAvvik) {
        txtBeskrivelse.setEditable(!lukketAvvik);
        txtBeskrivelse.setEnabled(!lukketAvvik);
    }

}