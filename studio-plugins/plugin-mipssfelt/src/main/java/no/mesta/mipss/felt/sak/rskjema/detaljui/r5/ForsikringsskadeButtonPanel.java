package no.mesta.mipss.felt.sak.rskjema.detaljui.r5;

import no.mesta.mipss.felt.sak.rskjema.RskjemaController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ForsikringsskadeButtonPanel extends JPanel {
    private JButton btnLagre;
    private JButton btnSlett;
    private JButton btnRapportR5;
    private JButton btnElrapp;


    private final RskjemaController controller;
    private final ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel;

    public ForsikringsskadeButtonPanel(RskjemaController controller, ForsikringsskadeDetaljPanel forsikringsskadeDetaljPanel) {
        this.controller = controller;
        this.forsikringsskadeDetaljPanel = forsikringsskadeDetaljPanel;
        initComponents();
        initGui();

    }

    public void setGjenopprett(boolean gjenopprett) {
        if (gjenopprett) {
            btnSlett.setAction(controller.getForsikringsskadeRestoreAction(forsikringsskadeDetaljPanel, getRestoreForsikringsskadeCallback()));
            btnElrapp.setEnabled(false);
        } else {
            btnSlett.setAction(controller.getForsikringsskadeSlettAction(forsikringsskadeDetaljPanel, getSlettForsikringsskadeCallback()));
            btnElrapp.setEnabled(true);
        }
    }

    private Action getSlettForsikringsskadeCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                forsikringsskadeDetaljPanel.entityDeleted();
            }
        };
    }

    private Action getRestoreForsikringsskadeCallback() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                forsikringsskadeDetaljPanel.entityRestored();
                setGjenopprett(false);
            }
        };
    }

    private void initComponents() {
        btnLagre = new JButton(controller.getForsikringsskadeLagreAction(forsikringsskadeDetaljPanel));
        btnLagre.setEnabled(false);
        btnSlett = new JButton(controller.getForsikringsskadeSlettAction(forsikringsskadeDetaljPanel, getSlettForsikringsskadeCallback()));

        btnRapportR5 = new JButton(controller.getForsikringsskadeRapportAction(forsikringsskadeDetaljPanel));
        btnElrapp = new JButton(controller.getR5SendElrappAction(forsikringsskadeDetaljPanel));
    }

    public void setEnabled(boolean enabled) {
        btnLagre.setEnabled(enabled);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(btnSlett, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(btnRapportR5, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(3, 5, 2, 0), 0, 0));
        add(btnElrapp, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(3, 5, 2, 5), 0, 0));
        add(btnLagre, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(3, 5, 2, 5), 0, 0));
    }

}
