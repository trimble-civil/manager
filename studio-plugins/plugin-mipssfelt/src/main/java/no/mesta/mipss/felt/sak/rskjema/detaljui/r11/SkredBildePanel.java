package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.bilde.BildeDatoPanel;
import no.mesta.mipss.felt.sak.bilde.BildeVelgerDialog;
import no.mesta.mipss.felt.sak.rskjema.RskjemaController;
import no.mesta.mipss.felt.sak.rskjema.detaljui.BildePictureModel;
import no.mesta.mipss.felt.sak.rskjema.detaljui.HendelseBildePanel;
import no.mesta.mipss.felt.util.ImageNameFilter;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.picturepanel.JPictureDialogue;
import org.jdesktop.swingx.JXImageView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("serial")
public class SkredBildePanel extends HendelseBildePanel {

    private Skred skred;


    public SkredBildePanel(RskjemaController controller) {
        super(controller);
    }

    public void setSkred(Skred skred) {
        this.skred = skred;
        setSkredBilder(skred.getBilder());
        sakGuid = skred.getSakGuid();
    }

    private void setSkredBilder(List<Bilde> bildeList) {
        for (JXImageView v : imageViews) {
            pnlImage.remove(v);
        }
        for (Bilde bilde : bildeList) {
            Image image = bilde.getSmaaImage();
            final JXImageView view = new JXImageView();
            imageViews.add(view);
            view.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
            if (image == null) {
                image = bilde.getImage();
                if (image == null)
                    image = IconResources.MISSING_ICON.getImage();
            }
            if (bilde.getBeskrivelse() != null) {
                view.setToolTipText("<html><pre>" + bilde.getBeskrivelse() + "</pre></html>");
            }
            view.setPreferredSize(new Dimension(200, 180));
            view.setImage(image);
            view.setDragEnabled(false);
            view.setEditable(false);
            view.addMouseListener(new ImageClickListener(view, bilde));

            view.setLayout(new BorderLayout());
            pnlImage.add(view);
        }
        lblAntallBilder.setText(" Antall bilder: " + bildeList.size());
        picturesPane.setViewportView(pnlImage);
        picturesPane.getHorizontalScrollBar().setUnitIncrement(205);
        updateSize();
        loading = false;
    }

    protected AbstractAction getAddBildeFraSakAction() {
        return new AbstractAction(Resources.getResource("button.fraSak")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Bilde> bilderFromSak = controller.getFeltController().getBilderFromSak(sakGuid);
                List<Bilde> cleaned = new ArrayList<Bilde>();
                for (Bilde b : bilderFromSak) {
                    if (!skred.getBilder().contains(b)) {
                        cleaned.add(b);
                    }
                }
                Window windowForComponent = SwingUtilities.windowForComponent(SkredBildePanel.this);
                BildeVelgerDialog dialog = new BildeVelgerDialog((JDialog) windowForComponent, cleaned);

                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
                List<Bilde> selectedImages = dialog.getSelectedImages();
                if (!selectedImages.isEmpty()) {
                    for (Bilde b : selectedImages) {
                        skred.addBilde(b);
                        activeImage = null;
                        setBilder();
                    }
                }
            }
        };
    }

    @Override
    protected void setBilder() {
        setSkredBilder(skred.getBilder());
    }

    @Override
    protected void oppdaterEndrer() {
        skred.setEndretAv(controller.getFeltController().getLoggedOnUserSign());
        skred.setEndretDato(Clock.now());
    }

    protected void slettActiveImage() {
        skred.removeBilde(activeImage);
        activeImage = null;
        setSkredBilder(skred.getBilder());
    }

    @Override
    protected void updateSize() {
        int w = getWidth();
        int h = layeredPane.getHeight();
        picturesPane.setBounds(new Rectangle(0, 0, w, h));
        lblAntallBilder.setBounds(5, h - 40, 90, 15);
        buttonPanel.setBounds(w - 265, h - 44, 258, 20);
//		
        if (skred != null) {
            if (skred.getBilder() == null || skred.getBilder().size() < 2) {
                pnlImage.setPreferredSize(new Dimension(w, pnlImage.getHeight()));
            } else
                pnlImage.setPreferredSize(null);
        } else
            pnlImage.setPreferredSize(null);
    }

    @Override
    protected void addPicture() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setLocale(new Locale("no", "NO"));
        fileChooser.setFileFilter(new ImageNameFilter());
        int what = fileChooser.showOpenDialog(this);
        if (what == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            Bilde b = createBilde(file);
            if (b == null) {
                setSkredBilder(skred.getBilder());
                return;
            }
            skred.addBilde(b);
            activeImage = null;
            setSkredBilder(skred.getBilder());
        }
    }

    public void addBilde(Bilde b) {
        BildeDatoPanel pnlDato = new BildeDatoPanel();
        JDialog d = new JDialog(SwingUtilities.windowForComponent(this));
        d.setTitle("Velg bildedato");
        d.setModal(true);
        d.add(pnlDato);
        d.setSize(220, 120);
        d.setLocationRelativeTo(this);
        d.setVisible(true);
        if (pnlDato.isCancelled())
            return;

        Date dato = pnlDato.getDato();
        b.setNyDato(dato);

        skred.addBilde(b);
        activeImage = null;
        setSkredBilder(skred.getBilder());
    }

    @Override
    protected void openImage() {
        BildePictureModel model = new BildePictureModel();
        model.setBilder(skred.getBilder());
        int imageIndex = model.indexOf(activeImage);
        JDialog dialog = (JDialog) SwingUtilities.windowForComponent(this);
        JPictureDialogue picture = new JPictureDialogue(dialog, model);
        picture.showBilde(imageIndex);
        picture.setVisible(true);
    }

    //TODO dupe av HendelseBildePanel.ImageClickListener..
    public class ImageClickListener extends MouseAdapter {
        private final JXImageView view;
        private final Bilde bilde;

        public ImageClickListener(JXImageView view, Bilde bilde) {
            this.view = view;
            this.bilde = bilde;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                activeImage = bilde;
                if (e.getClickCount() == 1) {
                    clearBorders();
                    view.setBorder(BorderFactory.createLineBorder(Color.GREEN, 3));

                } else if (e.getClickCount() == 2) {
                    openImage();
                }
            }
        }
    }
}