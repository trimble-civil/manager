package no.mesta.mipss.felt;

import no.mesta.mipss.persistence.mipssfield.Avvik;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Hendelse;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.DocumentFilter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class OpenItemDialog extends JDialog {
    public enum TYPE {
        SAK,
        AVVIK,
        HENDELSE,
        FORSIKRINGSSKADE,
        SKRED,
        HENDELSE_ELRAPP,
        FORSIKRINGSSKADE_ELRAPP,
        SKRED_ELRAPP,
        INSPEKSJON
    }

    ;

    public enum VALUE {
        OK,
        CANCEL
    }

    private VALUE value;
    private final TYPE type;
    private JTextField numberField;
    private JButton okButton;
    private JButton cancelButton;
    private final FeltController controller;
    private Long refnummer;

    public void setRefnummer(Long refnummer) {
        this.refnummer = refnummer;
        numberField.setText(refnummer != null ? refnummer + "" : "");
        numberField.setSelectionStart(0);
        numberField.setSelectionEnd(numberField.getText().length());
    }

    public Long getRefnummer() {
        return refnummer;
    }

    public OpenItemDialog(TYPE type, FeltController controller) {
        super(controller.getPlugin().getLoader().getApplicationFrame());
        this.controller = controller;
        setModal(true);
        setTitle(Resources.getResource("button.apne") + " " + type.toString() + " med id");
        this.type = type;
        initGui();
    }

    public VALUE getValue() {
        return value;
    }

    private void initGui() {
        setLayout(new BorderLayout());
        JPanel panel = new JPanel(new GridBagLayout());
        okButton = new JButton(getOkAction());
        cancelButton = new JButton(getCancelAction());

        JLabel idLabel = new JLabel("ID:");
        numberField = new JTextField();
        numberField.setDocument(new DocumentFilter(10));
        numberField.setAction(getOkAction());
        panel.add(idLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 5, 5), 0, 0));
        panel.add(numberField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10, 5, 5, 5), 0, 0));

        JPanel buttonPanel = new JPanel(new GridBagLayout());
        buttonPanel.add(okButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        buttonPanel.add(cancelButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));

        panel.add(buttonPanel, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 10, 5), 0, 0));

        add(panel);
    }

    private AbstractAction getOkAction() {
        return new AbstractAction(Resources.getResource("button.apne"), IconResources.OK2_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                refnummer = Long.valueOf(numberField.getText());
                Long driftkontraktId = controller.getCommonQueryParams().getValgtKontrakt().getId();
                boolean fantEntitet = false;
                OpenItemDialog.this.dispose();
                switch (type) {
                    case SAK:
                        fantEntitet = openSak(driftkontraktId);
                        break;
                    case HENDELSE:
                        fantEntitet = openHendelse(driftkontraktId);
                        break;
                    case SKRED:
                        fantEntitet = openSkred(driftkontraktId);
                        break;
                    case FORSIKRINGSSKADE:
                        fantEntitet = openForsikringsskade(driftkontraktId);
                        break;
                    case INSPEKSJON:
                        fantEntitet = openInspeksjon(driftkontraktId);
                        break;
                    case HENDELSE_ELRAPP:
                        fantEntitet = openHendelseElrapp(driftkontraktId);
                        break;
                    case SKRED_ELRAPP:
                        fantEntitet = openSkredElrapp(driftkontraktId);
                        break;
                    case FORSIKRINGSSKADE_ELRAPP:
                        fantEntitet = openForsikringsskadeElrapp(driftkontraktId);
                        break;
                    case AVVIK:
                        fantEntitet = openAvvik(driftkontraktId);
                        break;
                    default:
                        break;
                }
                if (!fantEntitet) {
                    JOptionPane.showMessageDialog(controller.getPlugin().getLoader().getApplicationFrame(), Resources.getResource("warning.ugyldigId"), Resources.getResource("warning.ugyldigId.title"), JOptionPane.WARNING_MESSAGE);
                }

            }

            /**
             * åpner en inspeksjon, sjekker om inspeksjonen tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom inspeksjonen ikke tilhører riktig kontrakt
             */
            private boolean openInspeksjon(Long driftkontraktId) {
                Long dkid = controller.getInspeksjonController().getDriftkontraktIdForInspeksjon(refnummer);
                if (isCorrectKontrakt(dkid, driftkontraktId)) {
                    Inspeksjon i = controller.getInspeksjonController().hentInspeksjon(refnummer);
                    if (i == null) {
                        fantIkkeRegistrering(refnummer);
                    } else {
                        controller.getInspeksjonController().openInspeksjon(i);
                    }
                    return true;
                }
                return false;
            }

            /**
             * åpner en sak, sjekker om saken tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom saken ikke tilhører riktig kontrakt
             */
            private boolean openSak(Long driftkontraktId) {
                Long dkid = controller.getSakController().getDriftkontraktIdForSak(refnummer);
                if (isCorrectKontrakt(dkid, driftkontraktId)) {
                    controller.getSakController().openSak(refnummer);
                    return true;
                }
                return false;
            }

            /**
             * åpner en hendelse, sjekker om hendelsen tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom hendelsen ikke tilhører riktig kontrakt
             */
            private boolean openHendelse(Long driftkontraktId) {
                Long dkid = controller.getRskjemaController().getDriftkontraktIdForRskjema(refnummer, no.mesta.mipss.valueobjects.ElrappTypeR.Type.R2);
                if (isCorrectKontrakt(dkid, driftkontraktId)) {
                    Hendelse h = controller.getRskjemaController().hentHendelse(refnummer);
                    if (h == null) {
                        fantIkkeRegistrering(refnummer);
                    } else {
                        controller.getRskjemaController().openRskjema(controller.getRskjemaSokResultFromHendelse(h));
                    }
                    return true;
                }
                return false;
            }

            /**
             * åpner et skred, sjekker om skredet tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom skredet ikke tilhører riktig kontrakt
             */
            private boolean openSkred(Long driftkontraktId) {
                Long dkid = controller.getRskjemaController().getDriftkontraktIdForRskjema(refnummer, no.mesta.mipss.valueobjects.ElrappTypeR.Type.R11);
                if (isCorrectKontrakt(dkid, driftkontraktId)) {
                    Skred s = controller.getRskjemaController().hentSkred(refnummer);
                    if (s == null) {
                        fantIkkeRegistrering(refnummer);
                    } else {
                        controller.getRskjemaController().openRskjema(controller.getRskjemaSokResultFromSkred(s));
                    }
                    return true;
                }
                return false;
            }

            /**
             * åpner en forsikringsskade, sjekker om forsikringsskaden tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom forsikringsskaden ikke tilhører riktig kontrakt
             */
            private boolean openForsikringsskade(Long driftkontraktId) {
                Long dkid = controller.getRskjemaController().getDriftkontraktIdForRskjema(refnummer, no.mesta.mipss.valueobjects.ElrappTypeR.Type.R5);
                if (isCorrectKontrakt(dkid, driftkontraktId)) {
                    Forsikringsskade f = controller.getRskjemaController().hentForsikringsskade(refnummer);
                    if (f == null) {
                        fantIkkeRegistrering(refnummer);
                    } else {
                        controller.getRskjemaController().openRskjema(controller.getRskjemaSokResultFromForsikringsskade(f));
                    }
                    return true;
                }
                return false;
            }

            /**
             * åpner en hendelse, sjekker om hendelsen tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom hendelsen ikke tilhører riktig kontrakt
             */
            private boolean openHendelseElrapp(Long driftkontraktId) {
                //TODO
                Hendelse h = controller.getRskjemaController().hentHendelseElrapp(refnummer, driftkontraktId);
                if (h == null) {
                    fantIkkeRegistrering(refnummer);
                } else {
                    controller.getRskjemaController().openRskjema(controller.getRskjemaSokResultFromHendelse(h));
                }
                return true;
            }

            /**
             * åpner et skred, sjekker om skredet tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom skredet ikke tilhører riktig kontrakt
             */
            private boolean openSkredElrapp(Long driftkontraktId) {
                //TODO
                Skred s = controller.getRskjemaController().hentSkredElrapp(refnummer, driftkontraktId);
                if (s == null) {
                    fantIkkeRegistrering(refnummer);
                } else {
                    controller.getRskjemaController().openRskjema(controller.getRskjemaSokResultFromSkred(s));
                }
                return true;
            }

            /**
             * åpner en forsikringsskade, sjekker om forsikringsskaden tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom forsikringsskaden ikke tilhører riktig kontrakt
             */
            private boolean openForsikringsskadeElrapp(Long driftkontraktId) {
                //TODO
                Forsikringsskade f = controller.getRskjemaController().hentForsikringsskadeElrapp(refnummer, driftkontraktId);
                if (f == null) {
                    fantIkkeRegistrering(refnummer);
                } else {
                    controller.getRskjemaController().openRskjema(controller.getRskjemaSokResultFromForsikringsskade(f));
                }
                return true;
            }

            /**
             * åpner et avvik, sjekker om avviket tilhører valgte kontrakt
             *
             * @param driftkontraktId
             * @return false dersom avviket ikke tilhører riktig kontrakt
             */
            private boolean openAvvik(Long driftkontraktId) {
                Long dkid = controller.getAvvikController().getDriftkontraktIdForAvvik(refnummer);
                if (isCorrectKontrakt(dkid, driftkontraktId)) {
                    Avvik avvik = controller.getAvvikController().hentAvvik(refnummer);
                    if (avvik == null) {
                        fantIkkeRegistrering(refnummer);
                    } else {
                        controller.getAvvikController().openAvvik(avvik);
                    }
                    return true;
                }
                return false;
            }
        };
    }

    private void fantIkkeRegistrering(Long refnummer) {
        JOptionPane.showMessageDialog(controller.getPlugin().getLoader().getApplicationFrame(), Resources.getResource("message.fantIkkeRegistrering", String.valueOf(refnummer)), Resources.getResource("message.fantIkkeRegistrering.title"), JOptionPane.INFORMATION_MESSAGE);
    }

    private boolean isCorrectKontrakt(Long dkid, Long driftkontraktId) {
        if (dkid == null)
            return true;
        return dkid.equals(driftkontraktId);
    }

    private AbstractAction getCancelAction() {
        return new AbstractAction("Avbryt", IconResources.NO_ICON) {

            @Override
            public void actionPerformed(ActionEvent e) {
                OpenItemDialog.this.dispose();
            }
        };
    }

}
