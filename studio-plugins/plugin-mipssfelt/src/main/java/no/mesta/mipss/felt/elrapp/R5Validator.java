package no.mesta.mipss.felt.elrapp;

import no.mesta.mipss.elrapp.ElrappValidationException;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.Sak;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class R5Validator extends ElrappValidator {

    private final Sak sak;

    public R5Validator(Sak sak) {
        this.sak = sak;

    }

    @Override
    public List<String> validate() throws ElrappValidationException {
        List<String> errMsg = new ArrayList<String>();
        if (sak.getStedfesting().size() == 0) {
            errMsg.add(Resources.getResource("feilmelding.manglerStedfesting"));
        }

        if (manglerMeldtAvEllerHvem()) {
            errMsg.add(Resources.getResource("feilmelding.manglerAvHvem"));
        }
        if (manglerSkadeDato()) {
            errMsg.add(Resources.getResource("feilmelding.manglerSkadedato"));
        }
        if (sak.getForsikringsskade().getBilder().size() == 0) {
            errMsg.add(Resources.getResource("feilmelding.manglerVedlegg"));
        }
        if (manglerEntreprenorDato()) {
            errMsg.add(Resources.getResource("feilmelding.manglerEntreprenorDato"));
        }
        if (manglerNavnKontaktpersonBerginsselskap()) {
            errMsg.add(Resources.getResource("feilmelding.manglerNavnKontaktpersonBergingsselskap"));
        }
        if (StringUtils.isBlank(sak.getForsikringsskade().getSkjemaEmne())) {
            errMsg.add(Resources.getResource("feilmelding.manglerSkjemaEmne"));
        }
        if (manglerRegnr()) {
            errMsg.add(Resources.getResource("feilmelding.manglerRegnr"));
        }
        if (errMsg.size() != 0) {
            throw new ElrappValidationException(StringUtils.join(errMsg, "\n"));
        }
        return errMsg;
    }

    private boolean manglerNavnKontaktpersonBerginsselskap() {
        Forsikringsskade f = sak.getForsikringsskade();
        for (SkadeKontakt sk : f.getBergingsSelskaper()) {
            String kNavn = sk.getKontaktNavn();
            String skNavn = sk.getNavn();
            if (kNavn == null && skNavn != null) {
                return true;
            }
            if (skNavn == null && kNavn != null) {
                return true;
            }
        }
        return false;
    }

    private boolean manglerMeldtAvEllerHvem() {
        Forsikringsskade f = sak.getForsikringsskade();
        return (StringUtils.isBlank(f.getEntrepenorNavn()) && StringUtils.isBlank(f.getMeldtAv()));
    }

    private boolean manglerSkadeDato() {
        Forsikringsskade f = sak.getForsikringsskade();
        return (f.getSkadeDato() == null && (f.getAntattTidsromForSkaden() == null || StringUtils.isEmpty(f.getAntattTidsromForSkaden())));
    }

    private boolean manglerEntreprenorDato() {
        Forsikringsskade f = sak.getForsikringsskade();
        return f.getEntrepenorDato() == null;
    }

    private boolean manglerRegnr() {
        Forsikringsskade f = sak.getForsikringsskade();
        if (!StringUtils.isBlank(f.getSkadevolderBilmerke()) ||
                !StringUtils.isBlank(f.getSkadevolderHenger()) ||
                !StringUtils.isBlank(f.getForsikringsselskap()) ||
                !StringUtils.isBlank(f.getEierAdresse()) ||
                !StringUtils.isBlank(f.getEierAvKjoretoy()) ||
                f.getEierTelefonNr() != null ||
                !StringUtils.isBlank(f.getForerNavn()) ||
                f.getForerTlf() != null) {

            if (StringUtils.isBlank(f.getSkadevolderRegnr())) {
                return true;
            }
        }
        return false;


    }

}
