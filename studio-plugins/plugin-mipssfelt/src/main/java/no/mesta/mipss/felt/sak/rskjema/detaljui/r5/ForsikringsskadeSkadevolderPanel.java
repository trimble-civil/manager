package no.mesta.mipss.felt.sak.rskjema.detaljui.r5;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Forsikringsskade;
import no.mesta.mipss.persistence.mipssfield.SkadeKontakt;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.UIUtils;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.jxlayer.JXLayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SuppressWarnings("serial")
public class ForsikringsskadeSkadevolderPanel extends JPanel {
    private JFormattedTextField regnrField;
    private JFormattedTextField hengerField;
    private JFormattedTextField merkeField;
    private JFormattedTextField forsikringsseskField;
    private JFormattedTextField eiernavnField;
    private JFormattedTextField tlf1Field;
    private JFormattedTextField tlf2Field;

    private JRadioButton snobroytJa;
    private JRadioButton snobroytNei;
    private JFormattedTextField navnField;
    private JFormattedTextField adresseField;
    private Forsikringsskade forsikringsskade;
    private final GridBagConstraints c;

    private JCheckBox ukjentSkadevolderCheck;
    private BindingGroup bg = new BindingGroup();
    private final R5Panel mainForm;


    public ForsikringsskadeSkadevolderPanel(GridBagConstraints c, R5Panel mainForm) {
        this.c = c;
        this.mainForm = mainForm;
        initGui();
    }

    public void setForsikringsskade(Forsikringsskade forsikringsskade) {
        this.forsikringsskade = forsikringsskade;
        bind();
        if (isSkadevolderNotPresent()) {
            ukjentSkadevolderCheck.setSelected(true);
            enableSkadevolderField(false);
        }
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        regnrField = new JFormattedTextField();
        regnrField.setDocument(new DocumentFilter(10, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", true));
        hengerField = new JFormattedTextField();
        hengerField.setDocument(new DocumentFilter(10, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", true));
        merkeField = new JFormattedTextField();

        forsikringsseskField = new JFormattedTextField();

        eiernavnField = new JFormattedTextField();
        tlf1Field = new JFormattedTextField();
        tlf1Field.setDocument(new DocumentFilter(20));

        adresseField = new JFormattedTextField();
        navnField = new JFormattedTextField();

        tlf2Field = new JFormattedTextField();
        tlf2Field.setDocument(new DocumentFilter(20));

        snobroytJa = new JRadioButton(Resources.getResource("label.ja"));
        snobroytNei = new JRadioButton(Resources.getResource("label.nei"));
        snobroytJa.setEnabled(false);
        snobroytNei.setEnabled(false);

        ukjentSkadevolderCheck = new JCheckBox("Ukjent skadevolder");
        ukjentSkadevolderCheck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Boolean enabled = true;
                if (ukjentSkadevolderCheck.isSelected()) {
                    enabled = false;
                    if (!isSkadevolderNotPresent()) {
                        int a = JOptionPane.showConfirmDialog(ForsikringsskadeSkadevolderPanel.this, Resources.getResource("warning.skadevolderwarning"), Resources.getResource("warning.skadevolderwarning.title"), JOptionPane.YES_NO_OPTION);
                        if (a == JOptionPane.YES_OPTION) {
                            nukeSkadevolder();
                        } else {
                            ukjentSkadevolderCheck.setSelected(false);
                            enabled = null;
                        }
                    }

                }
                enableSkadevolderField(enabled);
            }
        });

        ButtonGroup rGroup = new ButtonGroup();
        rGroup.add(snobroytNei);
        rGroup.add(snobroytJa);
        createSkadevolderPanel();


    }

    private void nukeSkadevolder() {
        List<SkadeKontakt> forsikringsselskap = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORSIKRINGSELSKAP);
        List<SkadeKontakt> eier = mainForm.getKontakter(forsikringsskade, SkadeKontakt.EIER_AV_KJORETOY);
        List<SkadeKontakt> forer = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORER_AV_KJORETOY);
        //TODO trenger vel metoder for dette i Forsikringsskade slik at propertychanged blir avfyrt
        forsikringsskade.getSkadeKontakter().removeAll(forsikringsselskap);
        forsikringsskade.getSkadeKontakter().removeAll(eier);
        forsikringsskade.getSkadeKontakter().removeAll(forer);
        forsikringsskade.setSkadevolderBilmerke(null);
        forsikringsskade.setSkadevolderHenger(null);
        forsikringsskade.setSkadevolderRegnr(null);
//		forsikringsseskField.setText(null);
//		eiernavnField.setText(null);
//		tlf1Field.setText(null);
//		adresseField.setText(null);
//		navnField.setText(null);
//		tlf2Field.setText(null);
    }

    private boolean isSkadevolderNotPresent() {
        List<SkadeKontakt> forsikringsselskap = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORSIKRINGSELSKAP);
        List<SkadeKontakt> eier = mainForm.getKontakter(forsikringsskade, SkadeKontakt.EIER_AV_KJORETOY);
        List<SkadeKontakt> forer = mainForm.getKontakter(forsikringsskade, SkadeKontakt.FORER_AV_KJORETOY);
        String bilmerke = forsikringsskade.getSkadevolderBilmerke();
        String henger = forsikringsskade.getSkadevolderHenger();
        String regnr = forsikringsskade.getSkadevolderRegnr();
        return (forsikringsselskap.size() == 0 &&
                eier.size() == 0 &&
                forer.size() == 0 &&
                bilmerke == null &&
                henger == null &&
                regnr == null);
    }

    private void enableSkadevolderField(Boolean enabled) {
        if (enabled == null)
            return;
        regnrField.setEnabled(enabled);
        hengerField.setEnabled(enabled);
        merkeField.setEnabled(enabled);
        forsikringsseskField.setEnabled(enabled);
        eiernavnField.setEnabled(enabled);
        tlf1Field.setEnabled(enabled);
        adresseField.setEnabled(enabled);
        navnField.setEnabled(enabled);
        tlf2Field.setEnabled(enabled);


        if (enabled) {
            regnrField.setText("");
            hengerField.setText("");
            merkeField.setText("");
            forsikringsseskField.setText("");
            eiernavnField.setText("");
            tlf1Field.setText("");
            adresseField.setText("");
            navnField.setText("");
            tlf2Field.setText("");
        } else {
            forsikringsskade.setSkadevolderRegnr(null);
            forsikringsskade.setSkadevolderHenger(null);
            forsikringsskade.setSkadevolderBilmerke(null);
            forsikringsskade.setForsikringsselskap(null);
            forsikringsskade.setEierAvKjoretoy(null);
            forsikringsskade.setEierTelefonNr(null);
            forsikringsskade.setEierAdresse(null);
            forsikringsskade.setForerNavn(null);
            forsikringsskade.setForerTlf(null);

        }

    }

    private void bind() {
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadevolderRegnr", regnrField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadevolderHenger", hengerField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "skadevolderBilmerke", merkeField, "text", BindingHelper.READ_WRITE));

        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "broyteskade", snobroytJa, "selected"));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "${broyteskade==false}", snobroytNei, "selected"));

        //for en komplett bindingmodell legges disse også inn
        //Entiteten sørger for rikitg håndtering av relasjonene mot skadekontakter
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "forsikringsselskap", forsikringsseskField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "forerNavn", navnField, "text", BindingHelper.READ_WRITE));
        Binding<Forsikringsskade, Long, JFormattedTextField, String> forerTlf = BindingHelper.createbinding(forsikringsskade, "forerTlf", tlf2Field, "text", BindingHelper.READ_WRITE);//Long
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "eierAdresse", adresseField, "text", BindingHelper.READ_WRITE));
        bg.addBinding(BindingHelper.createbinding(forsikringsskade, "eierAvKjoretoy", eiernavnField, "text", BindingHelper.READ_WRITE));
        Binding<Forsikringsskade, Long, JFormattedTextField, String> eierTlf = BindingHelper.createbinding(forsikringsskade, "eierTelefonNr", tlf1Field, "text", BindingHelper.READ_WRITE);//Long
        LongStringConverter sl = new LongStringConverter();
        forerTlf.setConverter(sl);
        eierTlf.setConverter(sl);
        bg.addBinding(forerTlf);
        bg.addBinding(eierTlf);
        bg.bind();
    }

    private void createSkadevolderPanel() {
        JLabel skadevolderLabel = new JLabel(Resources.getResource("label.skadevolder"));
        JLabel regnrLabel = new JLabel(Resources.getResource("label.regnr"));
        JLabel hengerLabel = new JLabel(Resources.getResource("label.henger"));
        JLabel merkeLabel = new JLabel(Resources.getResource("label.merke") + "                 ");
        JLabel forsikringsseskLabel = new JLabel(Resources.getResource("label.forsikringsselsk"));
        JLabel eiernavnLabel = new JLabel(Resources.getResource("label.navnEier"));
        JLabel tlf1Label = new JLabel(Resources.getResource("label.telefonnr"));
        JLabel adresseLabel = new JLabel(Resources.getResource("label.adresseEier"));
        JLabel navnLabel = new JLabel(Resources.getResource("label.navnForer"));
        JLabel tlf2Label = new JLabel(Resources.getResource("label.telefonnr"));
        JLabel snobroytLabel = new JLabel(Resources.getResource("label.snobroyt"));


        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        Font f = skadevolderLabel.getFont();
        skadevolderLabel.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
        add(skadevolderLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(ukjentSkadevolderCheck, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;
        add(UIUtils.getTwoComponentPanelVert(regnrLabel, decorateField(regnrField, "validateSkadevolder"), false), c);
        c.gridx += 1;
        c.gridwidth = 2;
        add(UIUtils.getTwoComponentPanelVert(hengerLabel, hengerField, false), c);
        c.gridx += 2;
        c.gridwidth = 2;
        add(UIUtils.getTwoComponentPanelVert(merkeLabel, merkeField, false), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(UIUtils.getTwoComponentPanelVert(forsikringsseskLabel, decorateField(forsikringsseskField, "validateForsikringsselskap"), false), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 3;
        add(UIUtils.getTwoComponentPanelVert(eiernavnLabel, decorateField(eiernavnField, "validateEier"), false), c);
        c.gridx += 3;
        c.gridwidth = 2;
        add(UIUtils.getTwoComponentPanelVert(tlf1Label, tlf1Field, false), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(UIUtils.getTwoComponentPanelVert(adresseLabel, adresseField, false), c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 3;
        add(UIUtils.getTwoComponentPanelVert(navnLabel, decorateField(navnField, "validateForer"), false), c);
        c.gridx += 3;
        c.gridwidth = 2;
        add(UIUtils.getTwoComponentPanelVert(tlf2Label, tlf2Field, false), c);

        JPanel chkPanel = UIUtils.getHorizPanel();
        chkPanel.add(snobroytLabel);
        chkPanel.add(UIUtils.getHStrut(5));
        chkPanel.add(snobroytJa);
        chkPanel.add(snobroytNei);
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 5;
        add(chkPanel, c);

        c.gridx = 0;
        c.gridy++;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        add(new JPanel(), c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.weighty = 0.0;
    }

    public boolean validateEier() {
        String eier = eiernavnField.getText();
        String eierTlf = tlf1Field.getText();
        String eierAdresse = adresseField.getText();
        if (eier.equals("") && eierTlf.equals("") && eierAdresse.equals("")) {
            return true;
        }
        if (eier.equals("")) //eiernavn kan ikke være "" når andre felter er utfylt
            return false;

        return true;

    }

    public boolean validateForer() {
        String forerNavn = navnField.getText();//fører
        String forerTlf = tlf2Field.getText();

        if (forerNavn.equals("") && forerTlf.equals("")) {
            return true;
        }
        if (forerNavn.equals(""))//fører navn kan ikke være "" når tlf er utfylt
            return false;
        return true;
    }

    public boolean isValidFields() {
        boolean valid = true;
        if (!validateEier()) {
            valid = false;
        }
        if (!validateForer()) {
            valid = false;
        }
        if (!validateForsikringsselskap()) {
            valid = false;
        }
        if (!validateSkadevolder()) {
            valid = false;
        }
        return valid;
    }

    public boolean validateForsikringsselskap() {
        return true;
    }

    public boolean validateSkadevolder() {
        if (!ukjentSkadevolderCheck.isSelected()) {
            if (regnrField.getText().equals("")) {
                return false;
            }
        }
        return true;
    }

    private JXLayer<JComponent> decorateField(JComponent c, String validationMethod) {
        JXLayer<JComponent> layer = new JXLayer<JComponent>(c);
        layer.setUI(new ValidationLayerUI(validationMethod, this));
        return layer;
    }

//	public String getEierNavn(){
//		return eiernavnField.getText();
//	}
//	public String getTlf1(){
//		return tlf1Field.getText();
//	}
//	public String getTlf2(){
//		return tlf2Field.getText();
//	}
//	public String getAdresse(){
//		return adresseField.getText();
//	}
//	public String getForerNavn(){
//		return navnField.getText();
//	}
//	public String getForsikringsselskap(){
//		return forsikringsseskField.getText();
//	}
//	public void setForsikringsselskap(String navn){
//		forsikringsseskField.setText(navn);	
//	}
//	public void setEierNavn(String navn){
//		eiernavnField.setText(navn);
//	}
//	public void setTlf1(String tlf1){
//		tlf1Field.setText(tlf1);		
//	}
//	public void setAdresse(String adresse){
//		adresseField.setText(adresse);
//	}
//	public void setForerNavn(String navn){
//		navnField.setText(navn);
//	}
//	public void setTlf2(String tlf2){
//		tlf2Field.setText(tlf2);
//	}
}
