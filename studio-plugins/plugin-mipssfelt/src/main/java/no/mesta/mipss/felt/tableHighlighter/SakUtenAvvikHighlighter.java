package no.mesta.mipss.felt.tableHighlighter;

import no.mesta.mipss.felt.sak.SakController;
import no.mesta.mipss.mipssfelt.SakSokResult;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public final class SakUtenAvvikHighlighter implements HighlightPredicate {
    private final SakController controller;

    public SakUtenAvvikHighlighter(SakController controller) {
        this.controller = controller;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        SakSokResult srs = controller.getSakTableModel().get(controller.getSakTable().convertRowIndexToModel(adapter.row));
        if (srs.isSlettbar())
            return true;
        return false;
    }
}