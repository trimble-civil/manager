package no.mesta.mipss.felt;

import no.mesta.mipss.ui.Sphere;
import no.mesta.mipss.valueobjects.ElrappStatus;
import org.jdesktop.swingx.JXBusyLabel;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class TabHeader extends JPanel {
    private JLabel lblChanged;
    private JLabel lblTitle;
    private JLabel lblElrappStatus;

    private Sphere red;
    private Sphere green;
    private Sphere yellow;


    private JXBusyLabel lblBusy;

    public TabHeader(String title) {

        setLayout(new BorderLayout());
        lblTitle = new JLabel(title);
        lblChanged = new JLabel("*");
        lblChanged.setVisible(false);
        lblChanged.setForeground(Color.red);

        lblElrappStatus = new JLabel();

        lblBusy = new JXBusyLabel();
        lblBusy.setVisible(false);
        add(lblBusy, BorderLayout.WEST);
        add(lblTitle, BorderLayout.CENTER);

        JPanel pnlEast = new JPanel();
        pnlEast.add(lblElrappStatus, BorderLayout.WEST);
        pnlEast.add(lblChanged, BorderLayout.EAST);
        add(pnlEast, BorderLayout.EAST);


        red = new Sphere(Color.red, 14);
        green = new Sphere(Color.green, 14);
        yellow = new Sphere(Color.yellow, 14);
    }

    public void setElrappStatus(ElrappStatus status) {
        switch (status.getStatus()) {
            case IKKE_SENDT:
                lblElrappStatus.setIcon(red.getIcon());
                break;
            case ENDRET:
                lblElrappStatus.setIcon(yellow.getIcon());
                break;
            case SENDT:
                lblElrappStatus.setIcon(green.getIcon());
                break;
        }
    }

    public void setTitle(String title) {
        lblTitle.setText(title);
    }

    public void setBusy(boolean busy) {
        lblBusy.setVisible(busy);
        lblBusy.setBusy(busy);
    }

    public void setChanged(boolean changed) {
        lblChanged.setVisible(changed);
    }
}