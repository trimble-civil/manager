package no.mesta.mipss.felt.sak.avvik.detaljui;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.felt.FeltMapPanel;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.felt.sak.SakDetaljTabPanel;
import no.mesta.mipss.felt.sak.avvik.AvvikController;
import no.mesta.mipss.felt.sak.detaljui.SakDetaljPanel;
import no.mesta.mipss.felt.util.ImageDropper;
import no.mesta.mipss.persistence.dokarkiv.Bilde;
import no.mesta.mipss.persistence.mipssfield.*;
import no.mesta.mipss.ui.components.PanelDisabledIndicator;
import no.mesta.mipss.valueobjects.ElrappStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class AvvikDetaljPanel extends SakDetaljTabPanel {

    private static final Logger log = LoggerFactory.getLogger(AvvikDetaljPanel.class);

    private final AvvikController controller;
    private final SakDetaljPanel sakDetaljPanel;
    private FeltEntityInfo info;
    private String title;

    private AvvikBildePanel pnlBilder;
    private FeltMapPanel pnlmapPoint;
    private StatusHistoryPanel pnlStatusHistory;
    private RessursPanel pnlRessurs;
    private BeskrivelsePanel pnlBeskrivelse;
    private TidStedPanel pnlTidSted;
    private VurderingPanel pnlVurdering;
    private AvvikDetaljButtonPanel pnlButtons;
    private Avvik avvik;
    private JPanel pnlContent;

    public AvvikDetaljPanel(AvvikController controller, Avvik avvik, FeltEntityInfo info, SakDetaljPanel sakDetaljPanel) {
        this.controller = controller;
        this.title = "Avvik (" + avvik.getRefnummer() + ")";
        this.info = info;
        initComponents();
        initGui();
        setAvvik(avvik);
        bind();
        loading = false;
        this.sakDetaljPanel = sakDetaljPanel;
    }

    public SakDetaljPanel getSakDetaljPanel() {
        return sakDetaljPanel;
    }

    @Override
    public void setChanged(boolean changed) {
        super.setChanged(changed);
        pnlButtons.setEnabled(changed);
    }

    private void initComponents() {
        pnlBilder = new AvvikBildePanel(controller);
        new ImageDropper(pnlBilder, new ImageDropper.ImageDropListener() {
            @Override
            public void imageDropped(Bilde[] images) {
                for (Bilde i : images) {
                    pnlBilder.addBilde(i);
                }
            }
        }, controller.getFeltController().getLoggedOnUserSign());

        pnlmapPoint = new FeltMapPanel(controller.getFeltController());
        pnlStatusHistory = new StatusHistoryPanel(controller, this);
        pnlRessurs = new RessursPanel();
        pnlBeskrivelse = new BeskrivelsePanel();
        pnlTidSted = new TidStedPanel(controller);
        pnlVurdering = new VurderingPanel(controller);
        pnlButtons = new AvvikDetaljButtonPanel(controller, this);

        loadingPanels = new ArrayList<LoadingPanel>();
        loadingPanels.add(pnlBilder);
        loadingPanels.add(pnlmapPoint);
        loadingPanels.add(pnlStatusHistory);
        loadingPanels.add(pnlRessurs);
        loadingPanels.add(pnlBeskrivelse);
        loadingPanels.add(pnlTidSted);
        loadingPanels.add(pnlVurdering);
    }

    @Override
    protected void initGui() {
        super.initGui();
        final GridBagLayout layout = new GridBagLayout();
        pnlContent = new JPanel(layout);
        setLayout(new BorderLayout());

        // rad 1
        addComponentToGrid(pnlTidSted, 0, 0, 1, 0.0);
        addComponentToGrid(pnlVurdering, 0, 1, 1, 0.0);

        // rad 2
        addComponentToGrid(pnlRessurs, 1, 0, 1, 0.0);
        addComponentToGrid(pnlStatusHistory, 1, 1, 2, 0.25); // strekker seg med rad 3

        // rad 3
        addComponentToGrid(pnlBeskrivelse, 2, 0, 1, 0.25);

        // rad 4
        addComponentToGrid(pnlBilder, 3, 0, 1, 0.75);
        addComponentToGrid(pnlmapPoint, 3, 1, 1, 0.75);

        add(pnlSlettetInfo, BorderLayout.NORTH);
        add(pnlLukketInfo, BorderLayout.NORTH);
        add(pnlContent, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);

        // lytteren sørger for at de to kolonnene i dette panelet alltid har lik bredde
        addComponentListener(new java.awt.event.ComponentAdapter() {
            @Override
            public void componentResized(java.awt.event.ComponentEvent evt) {
                int width = getWidth();
                int wGap = getInsets().left + getInsets().right;

                layout.columnWidths = new int[]{width / 2 - wGap, width / 2 - wGap};
            }
        });
    }

    private void addComponentToGrid(
            Component component, int x, int y, int colspan, int rowspan, double weightx, double weighty) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = y;
        c.gridy = x;
        c.gridwidth = colspan;
        c.gridheight = rowspan;
        c.weightx = weightx;
        c.weighty = weighty;
        c.fill = GridBagConstraints.BOTH;
        addComponentToGrid(component, c);
    }

    private void addComponentToGrid(Component component, int x, int y, int rowspan, double weighty) {
        addComponentToGrid(component, x, y, 1, rowspan, 1, weighty);
    }

    private void addComponentToGrid(Component component, GridBagConstraints c) {
        pnlContent.add(component, c);
    }

    public void dispose() {
        for (LoadingPanel panel : loadingPanels) {
            panel.dispose();
        }
        pnlBilder = null;
        pnlmapPoint = null;
        removeAll();
        loadingPanels.clear();
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String refnummer) {
        title = "Avvik (" + refnummer + ")";
        getTabHeader().setTitle(title);
    }

    protected void bind() {
        SaveStatusMonitor<Avvik> avvikMonitor = new SaveStatusMonitor<Avvik>(Avvik.class, this, "avvik", getSaveStatusRegistry().getBindingGroup());
        getSaveStatusRegistry().addMonitor(avvikMonitor);
        avvikMonitor.registerField("beskrivelse");
        avvikMonitor.registerField("estimat");
        avvikMonitor.registerField("prosess");
        avvikMonitor.registerField("aarsak");
        avvikMonitor.registerField("tilstand");
        avvikMonitor.registerField("etterslepFlagg");
        avvikMonitor.registerField("trafikksikkerhetFlagg");
        avvikMonitor.registerField("tilleggsarbeid");
        avvikMonitor.registerField("tiltaksDato");
        avvikMonitor.registerField("statuser");
        avvikMonitor.registerField("bilder");
        avvikMonitor.registerField("endretDato");
        avvikMonitor.registerField("objektType");
        avvikMonitor.registerField("objektAvvikType");
        avvikMonitor.registerField("objektAvvikKategori");
        avvikMonitor.registerField("objektStedTypeAngivelse");
        avvikMonitor.registerField("arbeidType");
        avvikMonitor.registerField("merknad");


        SaveStatusMonitor<Punktstedfest> stedMonitor = new SaveStatusMonitor<Punktstedfest>(Punktstedfest.class, avvik, "stedfesting", getSaveStatusRegistry().getBindingGroup());
        getSaveStatusRegistry().addMonitor(stedMonitor);
        stedMonitor.registerField("koordinatString");

        getSaveStatusRegistry().getBindingGroup().addBinding(BindingHelper.createbinding(getSaveStatusRegistry(), "changed", this, "changed"));
        getSaveStatusRegistry().doBind();

        BindingHelper.createbinding(avvik, "${lukketDato!=null}", this, "lukket").bind();
    }

    public void setLukket(boolean lukketAvvik) {
        pnlBilder.setLukket(lukketAvvik);
        pnlStatusHistory.setLukket(lukketAvvik);
        pnlRessurs.setLukket(lukketAvvik);
        pnlBeskrivelse.setLukket(lukketAvvik);
        pnlTidSted.setLukket(lukketAvvik);
        pnlVurdering.setLukket(lukketAvvik);
        pnlButtons.setLukket(lukketAvvik);
        if (lukketAvvik) {
            if (avvik.isMangel()) {
                lblLukket.setText(Resources.getResource("message.lukketAvvikMangel", MipssDateFormatter.formatDate(avvik.getLukketDato()) + " av " + avvik.getStatus().getOpprettetAv()));
            } else {
                lblLukket.setText(Resources.getResource("message.lukketAvvik", MipssDateFormatter.formatDate(avvik.getLukketDato()) + " av " + avvik.getStatus().getOpprettetAv()));
            }
            pnlLukketInfo.setVisible(true);
        } else {
            if (!avvik.isMangel())
                pnlLukketInfo.setVisible(false);
        }
    }

    private void checkStedfesting() {
        if (avvik.getStedfesting() != null) {
            //har stedfesting men mangler veiref
            if (avvik.getStedfesting().getVeiref() == null) {
                Punktstedfest sted = avvik.getStedfesting();
                Punktveiref nyVeiref = controller.getFeltController().getPunktveirefFromXY(sted.getSnappetX(), sted.getSnappetY());
                boolean isEmpty = false;
                if (nyVeiref == null) {
                    nyVeiref = new Punktveiref();
                    nyVeiref.setNewEntity(true);
                    isEmpty = true;
                }
                nyVeiref.setPunktstedfest(sted);
                sted.getVeireferanser().add(nyVeiref);
                if (!isEmpty) {
                    controller.persistAvvik(avvik);
                }
            }
        }
    }

    public void setAvvik(Avvik avvik) {
        if (avvik.getSlettetDato() != null) {
            pnlSlettetInfo.setVisible(true);
            lblSlettet.setText("Avviket er slettet, " + MipssDateFormatter.formatDate(avvik.getSlettetDato()) + " av " + avvik.getSlettetAv());
            pnlButtons.setGjenopprett(true);
            panelDisabledIndicator = new PanelDisabledIndicator(pnlContent);
        }
        pnlButtons.setKommentarButtonVisible(avvik.getGuid() != null);


        this.avvik = avvik;
        checkStedfesting();
        pnlBilder.setAvvik(avvik);
        pnlmapPoint.setAvvik(avvik);
        pnlStatusHistory.setAvvik(avvik);
        pnlRessurs.setAvvik(avvik);
        pnlBeskrivelse.setAvvik(avvik);
        pnlTidSted.setAvvik(avvik);
        pnlVurdering.setAvvik(avvik);

        if (avvik.isMangel()) {
            lblLukket.setText("Avviket har blitt en mangel");
            pnlLukketInfo.setVisible(true);
        }
    }

    public Avvik getAvvik() {
        return this.avvik;
    }

    @Override
    public void load() {
        controller.getAvvikaarsaker(avvik);
        controller.getAvviktilstander(false);
        avvik = controller.hentAvvik(info.getGuid());
    }

    @Override
    public void doneLoading() {
        setAvvik(avvik);
    }

    @Override
    public MipssFieldDetailItem getDetailEntity() {
        return avvik;
    }

    @Override
    public FeltEntityInfo getFeltEntityInfo() {
        return info;
    }

    @Override
    public ElrappStatus getElrappStatus() {
        return null;
    }

}