package no.mesta.mipss.felt.sak;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.SakQueryParams;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class SokekriterierSakPanel extends JPanel {

    private final SakController controller;

    private JRadioButton radioTilBehandling;
    private JRadioButton radioFerdigBehandlet;
    private JRadioButton radioAlle;
    private ButtonGroup radioButtonGroup;

    private JCheckBox chkIkkeEtterslep;
    private JCheckBox chkIkkeTilleggsarbeid;
    private JCheckBox chkKunSlettede;

    public SokekriterierSakPanel(SakController controller) {
        this.controller = controller;

        initComponents();
        initGui();
        bind();
    }

    private void initComponents() {
        radioTilBehandling = new JRadioButton(Resources.getResource("radio.tilBehandling"));
        radioTilBehandling.setToolTipText(Resources.getResource("radio.tilBehandling.tooltip"));
        radioTilBehandling.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getQueryParams().setType(SakQueryParams.TYPE.TIL_BEHANDLING);
            }
        });

        radioFerdigBehandlet = new JRadioButton(Resources.getResource("radio.ferdigBehandlet"));
        radioFerdigBehandlet.setToolTipText(Resources.getResource("radio.ferdigBehandlet.tooltip"));
        radioFerdigBehandlet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getQueryParams().setType(SakQueryParams.TYPE.FERDIG_BEHANDLET);
            }
        });

        radioAlle = new JRadioButton(Resources.getResource("radio.alle"));
        radioAlle.setToolTipText(Resources.getResource("radio.alle.tooltip"));
        radioAlle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getQueryParams().setType(SakQueryParams.TYPE.ALLE);
            }
        });

        radioButtonGroup = new ButtonGroup();
        radioButtonGroup.add(radioTilBehandling);
        radioButtonGroup.add(radioFerdigBehandlet);
        radioButtonGroup.add(radioAlle);


        chkIkkeEtterslep = new JCheckBox(Resources.getResource("check.ikkeEtterslep"));
        chkIkkeEtterslep.setToolTipText(Resources.getResource("check.ikkeEtterslep.tooltip"));
        chkIkkeTilleggsarbeid = new JCheckBox(Resources.getResource("check.ikkeTilleggsarbeid"));
        chkIkkeTilleggsarbeid.setToolTipText(Resources.getResource("check.ikkeTilleggsarbeid.tooltip"));
        chkKunSlettede = new JCheckBox(Resources.getResource("check.kunSlettede"));
        chkKunSlettede.setToolTipText(Resources.getResource("check.kunSlettede.tooltip"));

    }

    private void bind() {
        Binding<JCheckBox, Object, SakQueryParams, Object> bindEtterslep = BindingHelper.createbinding(chkIkkeEtterslep, "selected", controller.getQueryParams(), "ikkeEtterslep");
        Binding<JCheckBox, Object, SakQueryParams, Object> bindTilleggsarbeid = BindingHelper.createbinding(chkIkkeTilleggsarbeid, "selected", controller.getQueryParams(), "ikkeTilleggsarbeid");
        Binding<JCheckBox, Object, SakQueryParams, Object> kunSlettetBind = BindingHelper.createbinding(chkKunSlettede, "selected", controller.getQueryParams(), "kunSlettede");

        controller.getFeltController().getBindingGroup().addBinding(bindEtterslep);
        controller.getFeltController().getBindingGroup().addBinding(bindTilleggsarbeid);
        controller.getFeltController().getBindingGroup().addBinding(kunSlettetBind);
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(radioTilBehandling, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(radioFerdigBehandlet, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        add(radioAlle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

        add(chkIkkeEtterslep, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
        add(chkIkkeTilleggsarbeid, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        add(chkKunSlettede, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    }
}
