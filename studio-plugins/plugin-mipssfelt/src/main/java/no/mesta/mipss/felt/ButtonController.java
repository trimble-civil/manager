package no.mesta.mipss.felt;

import no.mesta.mipss.felt.brukere.BrukereDialog;
import no.mesta.mipss.mipssfelt.QueryParams;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JPDADialog;
import no.mesta.mipss.ui.roadpicker.KontraktveinettDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ButtonController {

    /**
     * Action for å velge veier til å bruke som kriterier for søk.
     *
     * @return
     */

    public Action getVeiButtonAction(final QueryParams params, final MipssPlugin owner) {
        return new AbstractAction(Resources.getResource("button.vei"), IconResources.VEI_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                KontraktveinettDialog veiVelger = new KontraktveinettDialog(owner.getLoader().getApplicationFrame(), Resources.getResource("label.velgVei"), params.getValgtKontrakt(), false, false, false, true, false);
                veiVelger.setLocationRelativeTo(owner.getLoader().getApplicationFrame());
                veiVelger.setSelectedVeireferanser(params.getVeiListe());
                veiVelger.setVisible(true);
                if (!veiVelger.isCancelled())
                    params.setVeiListe(veiVelger.getSelectedVeireferanser());
            }
        };
    }

    /**
     * Action for å velge PDA til å bruker som kriterier for søk
     *
     * @return
     */
    public Action getPdaButtonAction(final QueryParams params, final MipssPlugin owner) {
        return new AbstractAction(Resources.getResource("button.pda"), IconResources.PDA_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                JPDADialog dialog = new JPDADialog(owner.getLoader().getApplicationFrame(), Resources.getResource("label.velgPda"), params.getValgtKontrakt());
                dialog.setSize(400, 300);
                dialog.setLocationRelativeTo(owner.getLoader().getApplicationFrame());
                dialog.setSelectedPdaIds(params.getPdaDfuIdentList());
                dialog.setVisible(true);
                if (!dialog.isCancelled())
                    params.setPdaDfuIdentList(dialog.getValgtePDAdfuIdent());

            }
        };
    }

    /**
     * Action for å velge Bruker til å bruke som kriterier for søk
     *
     * @return
     */
    public Action getBrukerButtonAction(final QueryParams params, final MipssPlugin owner) {
        return new AbstractAction(Resources.getResource("button.bruker"), IconResources.ROLES_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                BrukereDialog dialog = new BrukereDialog(params, owner.getLoader().getApplicationFrame(), Resources.getResource("label.velgBruker"));
                dialog.setSize(300, 250);
                dialog.setLocationRelativeTo(owner.getLoader().getApplicationFrame());
                dialog.setSelectedBrukere(params.getBrukerList());
                dialog.setVisible(true);
                if (!dialog.isCancelled())
                    params.setBrukerList(dialog.getValgteBrukere());
            }
        };
    }


    /**
     * Action som viser frem prosessene tilknyttet en kontrakt
     *
     * @param callback
     * @return
     */
    public Action getVelgProsessAction(final QueryParams params, final MipssPlugin owner) {
        return new AbstractAction(Resources.getResource("button.velgProsess"), IconResources.PROSESS_16) {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProsessDialog dialog = new ProsessDialog(params, owner.getLoader().getApplicationFrame(), Resources.getResource("label.velgProsess"));
                dialog.setSize(400, 450);
                dialog.setLocationRelativeTo(owner.getLoader().getApplicationFrame());
                dialog.setSelectedProsesser(params.getProsesser());
                dialog.setVisible(true);
                if (!dialog.isCancelled()) {
                    params.setProsesser(dialog.getValgteProsesser());
                    params.setProsesskoder(dialog.getValgteProsesskoder());
                }
            }
        };
    }

    public Action getTilbakestillKriterierAction(final QueryParams params) {
        return new AbstractAction("", IconResources.DELETE_LAYERS) {
            @Override
            public void actionPerformed(ActionEvent e) {
                params.setBrukerList(null);
                params.setPdaDfuIdentList(null);
                params.setVeiListe(null);
            }
        };
    }


}
