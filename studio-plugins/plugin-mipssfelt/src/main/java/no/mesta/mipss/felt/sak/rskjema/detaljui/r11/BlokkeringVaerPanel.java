package no.mesta.mipss.felt.sak.rskjema.detaljui.r11;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DoubleStringConverter;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskap;
import no.mesta.mipss.persistence.mipssfield.r.common.Egenskapverdi;
import no.mesta.mipss.persistence.mipssfield.r.r11.Skred;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import org.jdesktop.beansbinding.Binding;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class BlokkeringVaerPanel extends JPanel {
    private Font bold;
    private R11Controller controller;
    private JMipssDatePicker stengtPicker;
    private JSpinner stengtSpinner;
    private JMipssDatePicker apnetPicker;
    private JSpinner apnetSpinner;
    private Egenskap typeStenging;
    private Egenskap stengtRetning;
    private JComboBox stengingCombo;
    private JComboBox retningCombo;

    private Egenskap regn;
    private Egenskap sno;
    private Egenskap vind;
    private JCheckBox nedborCheck;
    private JCheckBox ukjentNedborCheck;
    private JTextField temperaturField;
    private JComboBox regnCombo;
    private JComboBox snoCombo;
    private JComboBox vindCombo;

    public BlokkeringVaerPanel(R11Controller controller) {
        this.controller = controller;
        initGui();
    }

    private void initGui() {
        setLayout(new GridBagLayout());
        add(createBlokkeringPanel(), new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public void setSkred(Skred skred) {
        MipssDateTimePickerHolder stengtDato = new MipssDateTimePickerHolder(stengtPicker, stengtSpinner);
        BindingHelper.createbinding(controller.getSkred(), "stengtDato", stengtDato, "date", BindingHelper.READ_WRITE).bind();

        MipssDateTimePickerHolder apnetDato = new MipssDateTimePickerHolder(apnetPicker, apnetSpinner);
        BindingHelper.createbinding(controller.getSkred(), "aapnetDato", apnetDato, "date", BindingHelper.READ_WRITE).bind();


        EgenskapItemListener stengingListener = new EgenskapItemListener(typeStenging, controller.getSkred());
        EgenskapItemListener retningListener = new EgenskapItemListener(stengtRetning, controller.getSkred());
        EgenskapItemListener regnListener = new EgenskapItemListener(regn, controller.getSkred());
        EgenskapItemListener snoListener = new EgenskapItemListener(sno, controller.getSkred());
        EgenskapItemListener vindListener = new EgenskapItemListener(vind, controller.getSkred());

        stengingCombo.addItemListener(stengingListener);
        retningCombo.addItemListener(retningListener);
        regnCombo.addItemListener(regnListener);
        snoCombo.addItemListener(snoListener);
        vindCombo.addItemListener(vindListener);

        stengingCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(typeStenging));
        retningCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(stengtRetning));

        Egenskapverdi regnVerdi = controller.getEgenskapveriSkredSingle(regn);
        if (regnVerdi != null)
            regnCombo.setSelectedItem(regnVerdi);

        Egenskapverdi snoVerdi = controller.getEgenskapveriSkredSingle(sno);
        if (snoVerdi != null)
            snoCombo.setSelectedItem(snoVerdi);

        vindCombo.setSelectedItem(controller.getEgenskapveriSkredSingle(vind));

        DoubleStringConverter doubleStringConverter = new DoubleStringConverter();
        Binding tempBind = BindingHelper.createbinding(controller.getSkred(), "temperatur", temperaturField, "text", BindingHelper.READ_WRITE);
        tempBind.setConverter(doubleStringConverter);
        tempBind.bind();

        BindingHelper.createbinding(controller.getSkred(), "ingenNedbor", nedborCheck, "selected", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(this, "ingenNedbor", nedborCheck, "${selected}", BindingHelper.READ_WRITE).bind();
        BindingHelper.createbinding(this, "ukjentNedbor", ukjentNedborCheck, "${selected}", BindingHelper.READ_WRITE).bind();

        if (skred.getIngenNedbor() == null || skred.getIngenNedbor() == false) {
            if (controller.getEgenskapveriSkredSingle(sno) == null && controller.getEgenskapveriSkredSingle(regn) == null) {
                ukjentNedborCheck.setSelected(true);
            }
        }
    }

    public void setIngenNedbor(Boolean enabled) {
        if (enabled) {
            if (ukjentNedborCheck.isSelected()) {
                ukjentNedborCheck.setSelected(false);
            }
            regnCombo.setEnabled(false);
            snoCombo.setEnabled(false);
            removeSelectionRegnSnoCombo();
        } else {
            regnCombo.setEnabled(true);
            snoCombo.setEnabled(true);
        }
    }

    private void removeSelectionRegnSnoCombo() {
        regnCombo.setSelectedItem(null);
        snoCombo.setSelectedItem(null);
    }

    public void setUkjentNedbor(Boolean enabled) {
        if (enabled) {
            if (nedborCheck.isSelected()) {
                nedborCheck.setSelected(false);
            }
            snoCombo.setEnabled(false);
            regnCombo.setEnabled(false);
            removeSelectionRegnSnoCombo();
        } else {
            regnCombo.setEnabled(true);
            snoCombo.setEnabled(true);
        }
    }

    private JPanel createBlokkeringPanel() {
        JPanel panel = new JPanel(new GridBagLayout());

        JLabel stengingLabel = new JLabel(Resources.getResource("label.r11.stenging"));
        JLabel retningLabel = new JLabel(Resources.getResource("label.r11.stengtRetning"));
        JLabel vaerLabel = new JLabel(Resources.getResource("label.r11.vaerforhold"));

        JLabel stengtLabel = new JLabel(Resources.getResource("label.r11.stengt"));
        JLabel apnetLabel = new JLabel(Resources.getResource("label.r11.apnet"));
        JLabel kl1 = new JLabel(Resources.getResource("label.r11.kl"));
        JLabel kl2 = new JLabel(Resources.getResource("label.r11.kl"));

        JLabel regnLabel = new JLabel(Resources.getResource("label.r11.regn"));
        JLabel snoLabel = new JLabel(Resources.getResource("label.r11.sno"));
        JLabel vindLabel = new JLabel(Resources.getResource("label.r11.vindretning"));
        JLabel temperaturLabel = new JLabel(Resources.getResource("label.r11.temperatur"));
        JLabel graderLabel = new JLabel(Resources.getResource("label.r11.grader"));


        stengingCombo = new JComboBox();
        retningCombo = new JComboBox();
        regnCombo = new JComboBox();
        snoCombo = new JComboBox();
        vindCombo = new JComboBox();

        nedborCheck = new JCheckBox(Resources.getResource("label.r11.nedbor"));
        ukjentNedborCheck = new JCheckBox("Ukjent");
        temperaturField = new JTextField();

        createStengtDato();
        createApnetDato(stengtPicker);


        bold = stengingLabel.getFont();
        bold = new Font(bold.getName(), Font.BOLD, bold.getSize());
        stengingLabel.setFont(bold);
        Dimension size = stengtLabel.getSize();
        size.width = 37;
        size.height = 20;
        stengtLabel.setPreferredSize(size);
        apnetLabel.setPreferredSize(size);

        retningLabel.setFont(bold);
        vaerLabel.setFont(bold);

        typeStenging = controller.findEgenskap(R11Panel.STENGING_ID);
        stengtRetning = controller.findEgenskap(R11Panel.STENGNINGSRETNING_ID);
        regn = controller.findEgenskap(R11Panel.REGN_ID);
        sno = controller.findEgenskap(R11Panel.SNO_ID);
        vind = controller.findEgenskap(R11Panel.VIND_ID);

        List<Egenskapverdi> stengingVerdier = controller.getEgenskapverdi(typeStenging);
        List<Egenskapverdi> retningVerdier = controller.getEgenskapverdi(stengtRetning);

        MipssComboBoxModel<Egenskapverdi> stengingModel = new MipssComboBoxModel<Egenskapverdi>(stengingVerdier, true);
        MipssComboBoxModel<Egenskapverdi> retningModel = new MipssComboBoxModel<Egenskapverdi>(retningVerdier, true);
        MipssComboBoxModel<Egenskapverdi> regnModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(regn), true);
        MipssComboBoxModel<Egenskapverdi> snoModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(sno), true);
        MipssComboBoxModel<Egenskapverdi> vindModel = new MipssComboBoxModel<Egenskapverdi>(controller.getEgenskapverdi(vind), true);

        stengingCombo.setModel(stengingModel);
        retningCombo.setModel(retningModel);
        regnCombo.setModel(regnModel);
        snoCombo.setModel(snoModel);
        vindCombo.setModel(vindModel);

        JPanel tempPanel = new JPanel(new GridBagLayout());
        tempPanel.add(temperaturField, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        tempPanel.add(graderLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));

        JPanel stengtPanel = new JPanel(new GridBagLayout());
        stengtPanel.add(stengtLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        stengtPanel.add(stengtPicker, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        stengtPanel.add(kl1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        stengtPanel.add(stengtSpinner, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));

        JPanel apnetPanel = new JPanel(new GridBagLayout());
        apnetPanel.add(apnetLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        apnetPanel.add(apnetPicker, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        apnetPanel.add(kl2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        apnetPanel.add(apnetSpinner, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));

        panel.add(stengingLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(stengingCombo, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 10, 5), 0, 0));
        panel.add(retningLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        panel.add(retningCombo, new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 5), 0, 0));

        panel.add(vaerLabel, new GridBagConstraints(3, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(nedborCheck, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(ukjentNedborCheck, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(regnLabel, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(regnCombo, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(snoLabel, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(snoCombo, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(vindLabel, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(vindCombo, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(temperaturLabel, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        panel.add(tempPanel, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 5), 0, 0));

        panel.add(stengtPanel, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
        panel.add(apnetPanel, new GridBagConstraints(1, 5, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));


        panel.setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.r11.blokkeringTitle")));
        return panel;
    }

    private void createStengtDato() {
        stengtPicker = new JMipssDatePicker();

        Date midnight = stengtPicker.getDate();
        Calendar c = Calendar.getInstance();
        c.setTime(midnight);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        Dimension spinnerSize = new Dimension(55, 22);
        stengtSpinner = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        stengtSpinner.setPreferredSize(spinnerSize);
        stengtSpinner.setMinimumSize(spinnerSize);
        stengtSpinner.setMaximumSize(spinnerSize);
        stengtSpinner.setEditor(new JSpinner.DateEditor(stengtSpinner, "HH:mm"));

    }

    private void createApnetDato(JMipssDatePicker stengtPicker) {
        apnetPicker = new JMipssDatePicker();
        stengtPicker.pairWith(apnetPicker);

        Calendar c = Calendar.getInstance();
        c.setTime(Clock.now());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        if (stengtPicker.getDate() != null) {
            c.setTime(stengtPicker.getDate());
        }

        Dimension spinnerSize = new Dimension(55, 22);
        apnetSpinner = new JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
        apnetSpinner.setPreferredSize(spinnerSize);
        apnetSpinner.setMinimumSize(spinnerSize);
        apnetSpinner.setMaximumSize(spinnerSize);
        apnetSpinner.setEditor(new JSpinner.DateEditor(apnetSpinner, "HH:mm"));
    }

    public void clearSelections() {
        stengingCombo.setSelectedItem(null);
        retningCombo.setSelectedItem(null);
    }
}
