package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import no.mesta.mipss.persistence.mipssfield.vo.InspisertProsess;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DoubleRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import org.jdesktop.swingx.decorator.SortOrder;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;

@SuppressWarnings("serial")
public class ProsessAvvikPanel extends JPanel implements LoadingPanel {

    private MipssBeanTableModel<InspisertProsess> tableModel;
    private JMipssBeanTable<InspisertProsess> table;

    public ProsessAvvikPanel() {
        initComponents();
        initGui();
    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        tableModel.setSourceObject(inspeksjon);
    }

    private void initTable() {
        tableModel = new MipssBeanTableModel<InspisertProsess>(
                "inspiserteProsesser", InspisertProsess.class, "adHoc", "kode", "navn", "antallFunn");
        MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(
                InspisertProsess.class, tableModel.getColumns());
        table = new JMipssBeanTable<InspisertProsess>(tableModel, columnModel);
        table.setDefaultRenderer(Double.class, new DoubleRenderer());
        table.setDefaultRenderer(Boolean.class, new BooleanRenderer());
        table.setFillsViewportHeight(true);
        table.setFillsViewportWidth(true);
        table.setAutoResizeMode(JMipssBeanTable.AUTO_RESIZE_OFF);
        table.setSortable(true);
        table.setSortOrder(1, SortOrder.ASCENDING);
        table.setHorizontalScrollEnabled(true);
        table.setEditable(false);
//		table.setEnabled(false);
//		parent.getFieldToolkit().controlEnabledField(table);

        setColumnWidth(table.getColumn(0), 50);
        setColumnWidth(table.getColumn(1), 50);
        setColumnWidth(table.getColumn(2), 150);
        setColumnWidth(table.getColumn(3), 50);
    }

    private class BooleanRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

        public BooleanRenderer() {
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column) {
            Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            JPanel panel = new JPanel(new BorderLayout());
            panel.setBackground(component.getBackground());
            panel.setForeground(component.getForeground());

            if (value != null && value instanceof Boolean) {
                Boolean b = (Boolean) value;
                JLabel label;
                if (!b) {
                    label = new JLabel(Resources.getResource("label.ja"));
                    panel.add(label, BorderLayout.CENTER);
                } else {
                    label = new JLabel(Resources.getResource("label.nei"));
                    panel.add(label, BorderLayout.CENTER);
                }
                label.setBackground(component.getBackground());
                label.setForeground(component.getForeground());
            }

            return panel;
        }
    }

    private void setColumnWidth(TableColumn column, int width) {
        column.setPreferredWidth(width);
        column.setMinWidth(width);
    }

    private void initComponents() {
        initTable();

    }

    private void initGui() {
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("border.prosessAvvik")));
        setLayout(new BorderLayout());
        JScrollPane scroll = new JScrollPane(table);
        add(scroll, BorderLayout.CENTER);
    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {

    }

}
