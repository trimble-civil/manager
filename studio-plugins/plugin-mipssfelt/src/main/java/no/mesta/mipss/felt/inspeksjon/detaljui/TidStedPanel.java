package no.mesta.mipss.felt.inspeksjon.detaljui;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.felt.LoadingPanel;
import no.mesta.mipss.felt.Resources;
import no.mesta.mipss.mipssfelt.InspeksjonSokResult;
import no.mesta.mipss.persistence.mipssfield.Inspeksjon;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

import javax.swing.*;
import java.awt.*;
import java.util.Formatter;

@SuppressWarnings("serial")
public class TidStedPanel extends JPanel implements LoadingPanel {

    private JLabel lblKontrakt;
    private JLabel lblId;
    private JLabel lblUtfort;
    private JLabel lblVarighet;
    private JLabel lblInspisert;

    private JFormattedTextField fldKontrakt;
    private JFormattedTextField fldId;
    private JFormattedTextField fldUtfort;
    private JFormattedTextField fldVarighet;
    private JFormattedTextField fldInspisert;

    private Inspeksjon inspeksjon;
    private final InspeksjonSokResult inspeksjonSok;
    private BindingGroup bindings = new BindingGroup();

    public TidStedPanel(InspeksjonSokResult inspeksjonSok) {
        this.inspeksjonSok = inspeksjonSok;
        initComponents();
        initGui();
    }

    public void setInspeksjon(Inspeksjon inspeksjon) {
        this.inspeksjon = inspeksjon;
        bind();

    }

    private void bind() {
        fldInspisert.setText(getInspisertVeiInfo());
        fldVarighet.setText(getVarighet());
        fldKontrakt.setText(inspeksjon.getKontrakt().getTextForGUI());

        Binding<Inspeksjon, Object, JFormattedTextField, Object> idBind = BindingHelper.createbinding(inspeksjon, "refnummer", fldId, "text");
        Binding<Inspeksjon, Object, JFormattedTextField, Object> utfortBind = BindingHelper.createbinding(inspeksjon, "textForGUI", fldUtfort, "text");
        bindings.addBinding(idBind);
        bindings.addBinding(utfortBind);

        bindings.bind();
    }

    private void initComponents() {
        lblKontrakt = new JLabel(Resources.getResource("label.kontrakt"));
        lblId = new JLabel(Resources.getResource("label.id"));
        lblUtfort = new JLabel(Resources.getResource("label.utfort"));
        lblVarighet = new JLabel(Resources.getResource("label.varighet"));
        lblInspisert = new JLabel(Resources.getResource("label.inspisert"));

        fldKontrakt = new JFormattedTextField();
        fldId = new JFormattedTextField();
        fldUtfort = new JFormattedTextField();
        fldVarighet = new JFormattedTextField();
        fldInspisert = new JFormattedTextField();

        fldKontrakt.setEditable(false);
        fldId.setEditable(false);
        fldUtfort.setEditable(false);
        fldVarighet.setEditable(false);
        fldInspisert.setEditable(false);

    }


    private void initGui() {
        setBorder(BorderFactory.createTitledBorder(Resources.getResource("label.tidSted")));
        setLayout(new GridBagLayout());

        add(lblKontrakt, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldKontrakt, new GridBagConstraints(1, 0, 1, 1, 0.75, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        add(lblId, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldId, new GridBagConstraints(3, 0, 1, 1, 0.25, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(lblUtfort, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldUtfort, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(lblVarighet, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldVarighet, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        add(lblInspisert, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 3, 5, 2), 0, 0));
        add(fldInspisert, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public void dispose() {
        bindings.unbind();

    }


    private String getVarighet() {
        long varighet;
        String varighetString;
        if (inspeksjon.getTilTidspunkt() != null) {
            varighet = inspeksjon.getTilTidspunkt().getTime() - inspeksjon.getFraTidspunkt().getTime();
            Time time = new Time(varighet + 30000); // Legger til 30sekunder for å avrunde opp til hele minutt
            varighetString = time.toString();
        } else {
            varighetString = "Ikke avsluttet!";
        }
        return varighetString;
    }

    private String format(Double d) {
        Formatter formatter = new Formatter();
        formatter = formatter.format("%1$,1.3f", (Double) d);
        String txt = formatter.toString();
        formatter.close();
        return txt;
    }

    private String getInspisertVeiInfo() {
        Double totalInsp = inspeksjonSok.getKmEvInspisert() + inspeksjonSok.getKmRvInspisert() + inspeksjonSok.getKmFvInspisert() + inspeksjonSok.getKmKvInspisert();
        String evTxt = format(inspeksjonSok.getKmEvInspisert());
        String rvTxt = format(inspeksjonSok.getKmRvInspisert());
        String fvTxt = format(inspeksjonSok.getKmFvInspisert());
        String kvTxt = format(inspeksjonSok.getKmKvInspisert());
        String totTxt = format(totalInsp);

        StringBuilder sb = new StringBuilder();
        sb.append(Resources.getResource("label.totalt", totTxt));
        if (totalInsp != null && totalInsp > 0) {

            sb.append(Resources.getResource("label.fordelt"));

            if (inspeksjonSok.getKmEvInspisert() != null && inspeksjonSok.getKmEvInspisert() > 0) {
                sb.append("EV:" + evTxt);
            }
            if (inspeksjonSok.getKmRvInspisert() != null && inspeksjonSok.getKmRvInspisert() > 0) {
                sb.append("RV:" + rvTxt);
            }
            if (inspeksjonSok.getKmFvInspisert() != null && inspeksjonSok.getKmFvInspisert() > 0) {
                sb.append("FV:" + fvTxt);
            }
            if (inspeksjonSok.getKmKvInspisert() != null && inspeksjonSok.getKmKvInspisert() > 0) {
                sb.append("KV:" + kvTxt);
            }
        }
        return sb.toString();
    }

    private class Time {
        private static final String BLANK = " ";
        private static final int SECONDS_DIVIDER = 1000;
        private static final int MINUTES_DIVIDER = SECONDS_DIVIDER * 60;
        private static final int HOURS_DIVIDER = MINUTES_DIVIDER * 60;
        private static final int DAYS_DIVIDER = HOURS_DIVIDER * 24;
        private static final String EMPTY = "";
        private final long time;

        public Time(long time) {
            this.time = time;
        }

        private long days() {
            long days = getDays() * DAYS_DIVIDER;
            return days;
        }

        public int getDays() {
            return (int) time / DAYS_DIVIDER;
        }

        public int getHours() {
            return (int) (time - days()) / HOURS_DIVIDER;
        }

        public int getMinutes() {
            return (int) (time - days() - hours()) / MINUTES_DIVIDER;
        }

//		public int getSeconds() {
//			return (int) (time - days() - hours() - minutes()) / SECONDS_DIVIDER;
//		}

        private long hours() {
            return getHours() * HOURS_DIVIDER;
        }

//		private long minutes() {
//			return getMinutes() * MINUTES_DIVIDER;
//		}

        public String toString() {
            String days = getDays() != 0 ? getDays() + "d" + BLANK : EMPTY;
            String hours = getHours() + "t" + BLANK;
            String minutes = getMinutes() + "min";

            return days + hours + minutes;
        }
    }
}
