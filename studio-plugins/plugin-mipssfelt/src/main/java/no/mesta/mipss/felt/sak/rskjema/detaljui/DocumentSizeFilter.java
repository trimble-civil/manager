package no.mesta.mipss.felt.sak.rskjema.detaljui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class DocumentSizeFilter extends DocumentFilter {

    private final int numberOfCharacters;

    public DocumentSizeFilter(int numberOfCharacters) {
        this.numberOfCharacters=numberOfCharacters;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        if(fb.getDocument().getLength() + string.length() <= numberOfCharacters) {
            super.insertString(fb, offset, string, attr);
        }
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        if(fb.getDocument().getLength() + text.length() <= numberOfCharacters) {
            super.replace(fb, offset, length, text, attrs);
        }
    }
}
