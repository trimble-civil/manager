package no.mesta.mipss.mipsslogg.swing.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import no.mesta.mipss.chartservice.dataset.XYMipssLoggChartData;

/**
 * Tabelmodel som benyttes til visning av tabellen i Mipss Logg.
 * @author larnym
 */

@SuppressWarnings("serial")
public class MipssLoggTableModel extends AbstractTableModel implements TableModel{

	private String [] columnNames = {"Tidspunkt", "Breddegrad", "Lengdegrad", "Hastighet", "Digi", "Cen"};
	private List<XYMipssLoggChartData> data;

	public MipssLoggTableModel(List<XYMipssLoggChartData> data) {
		if (data != null) {
			this.data = data;
		} else{
			this.data = new ArrayList<XYMipssLoggChartData>();
			XYMipssLoggChartData dummy = new XYMipssLoggChartData();
			this.data.add(dummy);
		}
	}

	public int getColumnCount(){
		return columnNames.length;
	}

	public int getRowCount(){
		return this.data.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex){
		
		switch (columnIndex) {
		case 0:
			return data.get(rowIndex).getTidspunkt();
		case 1:
			return data.get(rowIndex).getBreddegrad();
		case 2:
			return data.get(rowIndex).getLengdegrad();
		case 3:
			return data.get(rowIndex).getHastighet();
		case 4:
			return data.get(rowIndex).getHexDigi();
		case 5:
			return data.get(rowIndex).getCen();
		default:
			return data.get(rowIndex).getTidspunkt();
		}		
	}

	public String getColumnName(int column) {
		return columnNames[column];
	}
	
	
}