package no.mesta.mipss.mipsslogg.swing.table;

import java.awt.Dimension;
import java.util.Date;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.chartservice.dataset.XYMipssLoggChartData;
import no.mesta.mipss.mipsslogg.MipssLoggController;
import no.mesta.mipss.mipsslogg.Resources;
import no.mesta.mipss.mipsslogg.swing.MipssLoggPanel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledPanel;

/**
 * Panel som benyttes for å vise dataene i tabellen
 * @author larnym
 */

@SuppressWarnings("serial")
public class MipssLoggTablePanel extends JXTitledPanel implements ListSelectionListener{
       
    private MipssLoggTableModel model;   
    private MipssLoggController controller;
    private JXTable table;
	private final MipssLoggPanel parent;
	private JXBusyLabel wait;
	private JScrollPane contentPane;
  
    public MipssLoggTablePanel(MipssLoggController controller, MipssLoggPanel parent, String title) {   
        super(title);   
        this.controller = controller;
		this.parent = parent;      
        table = new JXTable(new MipssLoggTableModel(null)); 
        table.getSelectionModel().addListSelectionListener(this);
        contentPane = new JScrollPane(table);
		add(contentPane);
        this.setVisible(false);
        
        wait = new JXBusyLabel();
		wait.setText("");
		wait.setVisible(false);
    }
    
    public void buildTable() {
    	
		wait.setText(Resources.getResource("label.vennligstVentTabell"));
		wait.setBusy(true);
		wait.setVisible(true);
		contentPane.setViewportView(wait);
		final LoggTableWorker worker = new LoggTableWorker();
		worker.execute();
		new Thread(){
			public void run(){
				try {
					setTable(worker.get());					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
		this.setVisible(true);
	}
	
	private class LoggTableWorker extends SwingWorker<List<XYMipssLoggChartData>, Integer>{

		@Override
		protected List<XYMipssLoggChartData> doInBackground() throws Exception {
			return controller.getTableData();
		}
	}
	
	private void setTable(List<XYMipssLoggChartData> data){
		
		if (data != null) {
			contentPane.setViewportView(table);
		}else{
			contentPane.setViewportView(new JLabel(Resources.getResource("label.ingenDataFunnet")));
			parent.emptyTable();
			return;
		}
		
		model = new MipssLoggTableModel(data);
		table.setModel(model);
		table.packAll(); //Gjøres for å automatisk justere bredden på innholdet i hver kolonne					
		model.fireTableDataChanged();
		this.setVisible(true);
	}

	
	@Override
	public Dimension getPreferredSize() {		
		return new Dimension(super.getWidth(), 100);
	}
	
	public void setCurrentRow(Date plotDate){
		
		for (int i = 0; i < table.getRowCount(); i++) {
			Date date = (Date)table.getValueAt(i, 0);
			if (date.getTime() == plotDate.getTime()) {
				table.changeSelection(i, 0, false, false);				
				return;
			}
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {		
		try {
			parent.setDomainCrosshairInGraph((Date)table.getValueAt(table.getSelectedRow(), 0));
		} catch (Exception e2) {
			//Får ArrayIndexOutOfBoundsException når man gjør ett nytt søk
			//Denne catches her, for å unngå at programmet feiler.
		}
		
	}

	public void emptyTable() {
		table.setModel(new MipssLoggTableModel(null));
	}
}