package no.mesta.mipss.mipsslogg;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;

import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.mipsslogg.messages.OpenMipssloggMessage;
import no.mesta.mipss.mipsslogg.swing.MipssLoggPanel;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;

/**
 * Modul som viser grafer og tabell over produserte data.
 * @author larnym
 */

@SuppressWarnings("serial")
public class MipssLoggModule extends MipssPlugin{

	public static final String KEY_MODUL_KONFIG = "studio.mipss.mipsslogg";

	private MipssLoggController controller;	
	private MipssLoggPanel mipssLoggPanel;
	
	@Override
	public JComponent getModuleGUI() {
		return mipssLoggPanel;
	}

	@Override
	protected void initPluginGUI() {
		controller = new MipssLoggController(this);
		mipssLoggPanel = new MipssLoggPanel(controller);
		controller.bind();
	}

	@Override
	public void shutdown() {

	}

	public void openModuleExternally(MipssLoggQueryParams params){
		controller = new MipssLoggController(this);
		
		final JFrame frame = new JFrame("Mipss Logg");
		MipssLoggPanel panel = new MipssLoggPanel(controller, params);
		
		frame.add(panel);		
		frame.setAlwaysOnTop(true);        
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
        
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {                                    
            	frame.dispose();
            }
        });
		controller.bind();
	}
	
	/** {@inheritDoc} */
	@Override
	public void receiveMessage(PluginMessage<?, ?> message){
		if (!(message instanceof OpenMipssloggMessage)){
			throw new IllegalArgumentException("Unsupported message type");
		}
		message.processMessage();
	}
}
