package no.mesta.mipss.mipsslogg.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.MipssDateTimePickerHolder;
import no.mesta.mipss.mipsslogg.MipssLoggController;
import no.mesta.mipss.mipsslogg.Resources;
import no.mesta.mipss.mipsslogg.swing.graph.MipssLoggGraphPanel;
import no.mesta.mipss.mipsslogg.swing.table.MipssLoggTablePanel;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.UIUtils;

import org.jdesktop.beansbinding.Binding;

/**
 * Hovedpanelet som oppretter underpanelene som viser grafene og tabellen.
 * @author larnym
 */

@SuppressWarnings("serial")
public class MipssLoggPanel extends JPanel {
		
	private JRadioButton rbtnDfuId;
    private JRadioButton rbtnRegNr;
    private JRadioButton rbtnSerieNr;
    private JRadioButton rbtnMaskinNr;
    private ButtonGroup filterBySelector;
    private JTextField txtInputField;
    private JButton btnHentData;
    
    private JMipssDatePicker fraDatoPicker;
    private JMipssDatePicker tilDatoPicker;
    private JSpinner fraTidspunktSpinner;
    private JSpinner tilTidspunktSpinner;
    private MipssDateTimePickerHolder fraDato;
    private MipssDateTimePickerHolder tilDato;
    
    private MipssLoggGraphPanel graphPanel;

	private final MipssLoggController controller; 
	
	private MipssLoggTablePanel tablePanel;
    
    public MipssLoggPanel(MipssLoggController controller){
		this.controller = controller;
    	initGui();
    }

    //Konstruktør som benyttes når Mipss Logg åpnes fra kjøretøysoversikten.
	public MipssLoggPanel(MipssLoggController controller, MipssLoggQueryParams params) {
		this.controller = controller;
		this.controller.setParams(params);
		initSimpleGUI();
		btnHentData.doClick();
	}

	private void initGui() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		//Filterpanel hvor alle søkekriterier legges
		JPanel filterPanel = new JPanel();
		
		rbtnDfuId = new JRadioButton(Resources.getResource("radioButton.dfu"));
		rbtnRegNr = new JRadioButton(Resources.getResource("radioButton.regNr"));
		rbtnSerieNr = new JRadioButton(Resources.getResource("radioButton.serieNr"));
		rbtnMaskinNr = new JRadioButton(Resources.getResource("radioButton.maskinNr"));
		filterBySelector = new ButtonGroup();
		filterBySelector.add(rbtnDfuId);
		filterBySelector.add(rbtnRegNr);
		filterBySelector.add(rbtnSerieNr);
		filterBySelector.add(rbtnMaskinNr);
		rbtnSerieNr.setSelected(true);
		
		txtInputField = new JTextField(15);
		
		//Panel for fra dato og tidspunkt
		JPanel fraPanel = new JPanel();
		
		fraDatoPicker = new JMipssDatePicker();
		Dimension spinnerSize = new Dimension(70, 22);
		fraTidspunktSpinner = new javax.swing.JSpinner(new SpinnerDateModel(fraDatoPicker.getDate(), null, null, Calendar.MINUTE));
		fraTidspunktSpinner.setPreferredSize(spinnerSize);
		fraTidspunktSpinner.setMinimumSize(spinnerSize);
		fraTidspunktSpinner.setMaximumSize(spinnerSize);
		fraTidspunktSpinner.setEditor(new JSpinner.DateEditor(fraTidspunktSpinner, "HH:mm:ss"));
        
		fraPanel.add(getLabel(Resources.getResource("label.fra"), 20));
		fraPanel.add(fraDatoPicker);
		fraPanel.add(fraTidspunktSpinner);
		
		//Panel for til dato og tidspunkt
		JPanel tilPanel = new JPanel();
		tilDatoPicker = new JMipssDatePicker();
		fraDatoPicker.pairWith(tilDatoPicker);
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);

		tilTidspunktSpinner = new javax.swing.JSpinner(new SpinnerDateModel(c.getTime(), null, null, Calendar.MINUTE));
		tilTidspunktSpinner.setPreferredSize(spinnerSize);
		tilTidspunktSpinner.setMinimumSize(spinnerSize);
		tilTidspunktSpinner.setMaximumSize(spinnerSize);
		tilTidspunktSpinner.setEditor(new JSpinner.DateEditor(tilTidspunktSpinner, "HH:mm:ss"));
		
		tilPanel.add(getLabel(Resources.getResource("label.til"), 20));
		tilPanel.add(tilDatoPicker);
		tilPanel.add(tilTidspunktSpinner);
		
		fraDato = new MipssDateTimePickerHolder(fraDatoPicker, fraTidspunktSpinner);
		tilDato = new MipssDateTimePickerHolder(tilDatoPicker, tilTidspunktSpinner);
		
		//Tabell-panelet
		tablePanel = new MipssLoggTablePanel(controller, this, Resources.getResource("title.datagrunnlag"));
		
		//Graf-panelet
		graphPanel = new MipssLoggGraphPanel(controller, this, Resources.getResource("title.graf"));
		
		//Knapp som henter dataene
		btnHentData = new JButton(getVehicleChart());
		btnHentData.setText(Resources.getResource("button.hentData"));
		
		//Alle underpanel og elementer legges til i filterpanelet
		filterPanel.add(rbtnSerieNr);
		filterPanel.add(rbtnRegNr);
		filterPanel.add(rbtnMaskinNr);
		filterPanel.add(rbtnDfuId);
		filterPanel.add(txtInputField);
		filterPanel.add(fraPanel);
		filterPanel.add(tilPanel);
		filterPanel.add(btnHentData);
			
		//Filter- og grafpanelt legges til hovedpanelet
		add(filterPanel);
		add(graphPanel);
		add(tablePanel);
		
		bind();
	}
	
	private void initSimpleGUI(){
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		//Knapp som henter dataene
		btnHentData = new JButton(getVehicleChart());
		btnHentData.setText(Resources.getResource("button.hentData"));
		
		//Tabell-panelet
		tablePanel = new MipssLoggTablePanel(controller, this, Resources.getResource("title.datagrunnlag"));
		
		//Graf-panelet
		graphPanel = new MipssLoggGraphPanel(controller, this, Resources.getResource("title.graf"));
		graphPanel.setSize(900, 500);
		add(graphPanel);
		add(tablePanel);		
	}
	
	private JLabel getLabel(String text, int w){
		JLabel label = new JLabel(text);
		UIUtils.setComponentSize(new Dimension(w, 20), label);
		return label;
	}
	
	public Action getVehicleChart (){
		return new AbstractAction(Resources.getResource("button.hentGraf"), null) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				tablePanel.buildTable();
				graphPanel.buildGraph();
			}
		};
	}

	private void bind() {
		Binding<MipssDateTimePickerHolder, Date, MipssLoggQueryParams, Date> fraDatoBind = BindingHelper.createbinding(fraDato, "date", controller.getQueryParams(), "fraDato");
		Binding<MipssDateTimePickerHolder, Date, MipssLoggQueryParams, Date> tilDatoBind = BindingHelper.createbinding(tilDato, "date", controller.getQueryParams(), "tilDato");		
		Binding<JTextField, String, MipssLoggQueryParams, String> sokeTekstBind = BindingHelper.createbinding(txtInputField, "text", controller.getQueryParams(), "sokeTekst");
		Binding<JRadioButton, Boolean, MipssLoggQueryParams, Boolean> valgtDFUBinding = BindingHelper.createbinding(rbtnDfuId, "selected", controller.getQueryParams(), "dfuValgt");
		Binding<JRadioButton, Boolean, MipssLoggQueryParams, Boolean> valgtRegNrBinding = BindingHelper.createbinding(rbtnRegNr, "selected", controller.getQueryParams(), "regNrValgt");
		Binding<JRadioButton, Boolean, MipssLoggQueryParams, Boolean> valgtSerieNrBinding = BindingHelper.createbinding(rbtnSerieNr, "selected", controller.getQueryParams(), "serieNrValgt");
		Binding<JRadioButton, Boolean, MipssLoggQueryParams, Boolean> valgtMaskinNrBinding = BindingHelper.createbinding(rbtnMaskinNr, "selected", controller.getQueryParams(), "maskinNrValgt");

		controller.addBinding(fraDatoBind);
		controller.addBinding(tilDatoBind);
		controller.addBinding(sokeTekstBind);
		controller.addBinding(valgtRegNrBinding);
		controller.addBinding(valgtDFUBinding);		
		controller.addBinding(valgtSerieNrBinding);
		controller.addBinding(valgtMaskinNrBinding);
	}

	public void setCurrentTableRow(Date date) {
		tablePanel.setCurrentRow(date);		
	}
	
	public void setDomainCrosshairInGraph(Date date){
		graphPanel.updateCrosshairValue(date);
	}

	public void emptyTable() {
		tablePanel.emptyTable();	
	}
}
