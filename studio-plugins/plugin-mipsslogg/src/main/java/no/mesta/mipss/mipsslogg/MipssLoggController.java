package no.mesta.mipss.mipsslogg;

import java.util.List;

import no.mesta.mipss.chartservice.MipssCharts;
import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.chartservice.dataset.XYMipssLoggChartData;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.FieldToolkit;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jfree.chart.JFreeChart;

/**
 * Controller som henter data bra backend.
 * @author larnym
 */
public class MipssLoggController {
	
	private BindingGroup bindings;
	private MipssLoggModule plugin;
	private MipssLoggQueryParams params;
	private FieldToolkit fieldToolkit;
	private MipssCharts bean = BeanUtil.lookup(MipssCharts.BEAN_NAME, MipssCharts.class);
	
	public MipssLoggController(MipssLoggModule plugin){
		this.plugin = plugin;
		params = new MipssLoggQueryParams();
	}

	public void bind() {
		getFieldToolkit().bind();
	}
	
	public BindingGroup getBindingGroup(){
		return bindings;
	}
	
	public MipssLoggModule getPlugin() {
		return plugin;
	}

	public MipssLoggQueryParams getQueryParams() {
		return params;
	}
	
	public void setParams(MipssLoggQueryParams params) {
		this.params = params;
	}

	public FieldToolkit getFieldToolkit() {
        if (fieldToolkit==null){
            fieldToolkit = FieldToolkit.createToolkit();
        }
        return fieldToolkit;
    }
	
	public void addBinding(Binding<?,?,?,?> b){
		getFieldToolkit().addBinding(b);
	}

	public JFreeChart getGraph() {
		List<XYMipssLoggChartData> data = getTableData();
		if (!data.isEmpty()) {
			JFreeChart graf = bean.getMipssLoggGraph(getQueryParams(), data);
			return graf;
		}
		return null;
	}
	
	public List<XYMipssLoggChartData> getTableData() {
		List<XYMipssLoggChartData> data = bean.getMipssLoggTableData(getQueryParams());
		return data;
	}
}
