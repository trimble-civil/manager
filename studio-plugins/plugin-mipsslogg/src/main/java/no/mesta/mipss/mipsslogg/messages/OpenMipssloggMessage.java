package no.mesta.mipss.mipsslogg.messages;

import java.awt.Component;
import java.util.Date;
import java.util.Map;

import no.mesta.mipss.chartservice.MipssLoggQueryParams;
import no.mesta.mipss.mipsslogg.MipssLoggModule;
import no.mesta.mipss.plugin.PluginMessage;

import org.jfree.chart.JFreeChart;

/** 
 * Melding som benyttes når grafen opprettes via driftskontrakt-modulen
 * @author larnym
 */

public class OpenMipssloggMessage extends PluginMessage<String, JFreeChart>{

	private MipssLoggQueryParams params;
	
	public OpenMipssloggMessage(Long id, String regnr, String serienr, Date fraDato, Date tilDato) {
		params = new MipssLoggQueryParams();
				
		if (regnr != null) {
			params.setRegNrValgt(true);
			params.setSokeTekst(regnr);						
		} else if(serienr != null){
			params.setSerieNrValgt(true);
			params.setSokeTekst(serienr);
		} else if (id != null) {
			params.setDfuId(id);
			params.setDfuValgt(true);
			params.setSokeTekst(id + "");
		}
		params.setFraDato(fraDato);
		params.setTilDato(tilDato);
		
	}

	@Override
	public void processMessage() {
		if (getTargetPlugin() instanceof MipssLoggModule){
			MipssLoggModule plugin = (MipssLoggModule)getTargetPlugin();
			if (params.getSokeTekst() != null) {
				plugin.openModuleExternally(params);
			}			
		}
		setConsumed(true);		
	}

	@Override
	public Map<String, JFreeChart> getResult() {
		return null;
	}

	@Override
	public Component getMessageGUI() {
		return null;
	}
}
