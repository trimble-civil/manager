package no.mesta.mipss.mipsslogg.swing.graph;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Date;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;

import no.mesta.mipss.mipsslogg.MipssLoggController;
import no.mesta.mipss.mipsslogg.Resources;
import no.mesta.mipss.mipsslogg.swing.MipssLoggPanel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTitledPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;

/**
 * Panel som sørger for å vise frem grafene.
 * Alle grafer genereres backend og sendes "ferdig" laget hit.
 * Dette panelet sørger for å sette crosshair-verdiene i grafen, 
 * samt sender signal til tabellen om å sette fokus på riktig rad.
 * @author larnym
 */

@SuppressWarnings("serial")
public class MipssLoggGraphPanel extends JXTitledPanel implements ChartProgressListener {

	private JScrollPane contentPane;
	private JFreeChart chart;
	private ChartPanel chartPanel;
	private JXBusyLabel wait;
	private final MipssLoggController controller;	
	private List<XYPlot> subplots;
	private Double crosshairValue = 0.0;
	private final MipssLoggPanel parent;
	
	
	public MipssLoggGraphPanel (MipssLoggController controller, MipssLoggPanel parent, String title){
		super(title);
		this.controller = controller;
		this.parent = parent;
		initGui();
	}

	private void initGui() {
		this.setSize(500, 500);
		wait = new JXBusyLabel();
		wait.setText(Resources.getResource("label.grafGenereres"));
		wait.setVisible(false);
		
		contentPane = new JScrollPane();
		add(contentPane);
	}
	
	public void setChart (JFreeChart chart) {
		this.chart = chart;
				
		if (chart != null) {
			chartPanel = new ChartPanel(chart);
			contentPane.setViewportView(chartPanel);
			contentPane.getVerticalScrollBar().setUnitIncrement(10);
		}else{
			JLabel tilbakemelding = new JLabel(Resources.getResource("label.ingenDataFunnet"));
			if (controller.getQueryParams().getRegNrValgt()) {
				tilbakemelding.setText(Resources.getResource("label.flereKjoretoy"));
			}
			contentPane.setViewportView(tilbakemelding);
			parent.emptyTable();
			chartPanel = null;
			return;
		}
		
		int head = 55;
		int foot = 40;
		int space = 5;
		
		CombinedDomainXYPlot plot = (CombinedDomainXYPlot) chart.getPlot();
		
		subplots = plot.getSubplots();
		int size = head+foot+((subplots.size()-1)*space);
		for (XYPlot p:subplots){
			size += p.getWeight();
		}
		
		Dimension panelSize = chartPanel.getPreferredSize();
		panelSize.height=size;
		chartPanel.setMinimumDrawHeight(0);
		chartPanel.setMaximumDrawHeight(2000);
		chartPanel.setMinimumDrawWidth(0);
		chartPanel.setMaximumDrawWidth(2000);
		
		chartPanel.setPreferredSize(panelSize);
		
		this.chart.addProgressListener(this);
	}
	
	
	public JScrollPane getContentPane() {
		return contentPane;
	}
	
	public void setContentPane(JScrollPane contentPane){		
		add(contentPane, BorderLayout.CENTER);
	}
	
	public void buildGraph() {
		wait.setText(Resources.getResource("label.vennligstVent"));
		wait.setBusy(true);
		wait.setVisible(true);
		contentPane.setViewportView(wait);
		final LoggChartWorker worker = new LoggChartWorker();
		worker.execute();
		new Thread(){
			public void run(){
				try {
					setChart(worker.get());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	private class LoggChartWorker extends SwingWorker<JFreeChart, Integer>{

		@Override
		protected JFreeChart doInBackground() throws Exception {
			return controller.getGraph();
		}
	}
	
	@Override
	public Dimension getPreferredSize() {		
		return new Dimension(super.getWidth(), 400);
	}

	@Override
	public void chartProgress(ChartProgressEvent event) {		
		
		if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {   
            return;   
        } 
			
        if (chartPanel != null) {      
            if (chart != null) {
            	
            	for(XYPlot plot : subplots){
            		if (plot.getDomainCrosshairValue() != crosshairValue) {
            			crosshairValue = plot.getDomainCrosshairValue();
            			Date date = new Date(((Double)plot.getDomainCrosshairValue()).longValue());
            			updateCrosshairValue(date);				
						parent.setCurrentTableRow(date);
					}
            	}     	
            }   
        }	
	}	
	
	public void updateCrosshairValue(Date date){
		
		for(XYPlot plot : subplots){			
			plot.setDomainCrosshairValue(date.getTime());
		}
		chart.fireChartChanged();
	}
}
