package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.driftkontrakt.bean.LevStedQueryFilter;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.DateUtil;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.SokPanel;

@SuppressWarnings("serial")
public class LeggTilKontraktLeverandorPanel extends AbstractRelasjonPanel<List<KontraktLeverandor>> implements PropertyChangeListener {
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JLabel lblFraDato = new JLabel();
    private JMipssDatePicker dtPckrDatoFra;
    private JLabel lblTilDato = new JLabel();
    private JMipssDatePicker dtPckrDatoTil;
    private JPanel pnlDatoer = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    
	private DriftkontraktController driftkontraktController;
	private SokPanel<LevStedView> sokPanel;
	
	public LeggTilKontraktLeverandorPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		
		sokPanel = new SokPanel<LevStedView>(LevStedView.class,
												new LevStedQueryFilter(),
												driftkontraktController,
												"getLevStedViewListUnique",
												DriftkontraktController.class,
												true,
												"levNr",
												"levNavn",
												"orgNr");
		initDatePckrs();
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
    private void initDatePckrs() {
    	Driftkontrakt kontrakt = driftkontraktController.getCurrentDriftkontrakt();
    	
		Locale loc = new Locale("no", "NO");
		
		dtPckrDatoFra = new JMipssDatePicker(loc);
		dtPckrDatoTil = new JMipssDatePicker(loc);
		dtPckrDatoFra.pairWith(dtPckrDatoTil);

		dtPckrDatoFra.addPropertyChangeListener(this);
		dtPckrDatoTil.addPropertyChangeListener(this);
		
    	if(kontrakt != null) {
    		dtPckrDatoFra.setDate(kontrakt.getGyldigFraDato());
    		dtPckrDatoTil.setDate(kontrakt.getGyldigTilDato());
    	}
    }
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        lblFraDato.setText(ResourceUtils.getResource("label.fraDato"));
        lblTilDato.setText(ResourceUtils.getResource("label.tilDatoKanEndres"));
        pnlDatoer.setLayout(gridBagLayout3);
        pnlDatoer.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgTidsperiode")));
        pnlMain.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlDatoer.add(lblFraDato, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoer.add(lblTilDato, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoer.add(dtPckrDatoFra, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoer.add(dtPckrDatoTil, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlMain.add(pnlDatoer, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
	private void updateComplete() {
		List<LevStedView> levStedViewList = sokPanel.getSelectedEntityList();
		boolean datesCorrect = checkDates();
		setComplete(levStedViewList != null && datesCorrect);
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(800,500);
	}

	@Override
	public List<KontraktLeverandor> getNyRelasjon() {
		List<LevStedView> levStedViewList = sokPanel.getSelectedEntityList();
		Date datoFra = dtPckrDatoFra.getDate();
    	Date datoTil = dtPckrDatoTil.getDate();
    	
		if(levStedViewList != null && datoFra != null && datoTil != null) {
			List<KontraktLeverandor> retList = new ArrayList<KontraktLeverandor>();
			for(LevStedView levStedView : levStedViewList) {
				if(isLegal(levStedView)) {
					KontraktLeverandor kontraktLeverandor = new KontraktLeverandor();
					kontraktLeverandor.setDriftkontrakt(driftkontraktController.getCurrentDriftkontrakt());
					kontraktLeverandor.setLeverandorNr(levStedView.getLevNr());
					kontraktLeverandor.setFraDato(datoFra);
					kontraktLeverandor.setTilDato(datoTil);
					retList.add(kontraktLeverandor);
				}
			}
			return retList;
		} else {
			return null;
		}
	}

	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilLeverandor");
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateComplete();
	}
	
	private boolean checkDates() {
    	Date datoFra = dtPckrDatoFra.getDate();
    	Date datoTil = dtPckrDatoTil.getDate();
    	
    	if(datoFra != null && datoTil != null) {
    		return datoTil.after(datoFra);
    	}
    	
    	return false;
	}

	@Override
	public boolean isLegal() {
		boolean oneLegal = false;
		
		List<LevStedView> levStedViewList = sokPanel.getSelectedEntityList();
    	
    	for(LevStedView levStedView : levStedViewList) {
			if(isLegal(levStedView)) {
				oneLegal = true;
			} 
    	}
		
		if(!oneLegal) {
			JOptionPane.showMessageDialog(getParentDialog(), ResourceUtils.getResource("message.leverandorOverlappendePeriode"));
		}
		
		return oneLegal;
	}
	
	private boolean isLegal(LevStedView levStedView) {
		Driftkontrakt driftkontrakt = driftkontraktController.getCurrentDriftkontrakt();
		Date datoFra = dtPckrDatoFra.getDate();
    	Date datoTil = dtPckrDatoTil.getDate();
    	
//    	if (driftkontraktController.isLeverandorOverlappendePeriode(levStedView, datoFra, datoTil)){
//    		return false;
//    	}
    	
    	if(driftkontrakt.getKontraktLeverandorList() != null && driftkontrakt.getKontraktLeverandorList().size() > 0) {
			for(KontraktLeverandor kontraktLeverandor : driftkontrakt.getKontraktLeverandorList()) {
				if(levStedView.getLevNr().equals(kontraktLeverandor.getLeverandorNr()) 
						&& DateUtil.overlappingPeriods(kontraktLeverandor.getFraDato(), kontraktLeverandor.getTilDato(), datoFra, datoTil)) {
					return false;
				}
			}
		}
    	return true;
	}
}
