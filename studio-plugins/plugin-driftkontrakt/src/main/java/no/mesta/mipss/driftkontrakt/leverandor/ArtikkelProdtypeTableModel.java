package no.mesta.mipss.driftkontrakt.leverandor;

import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import no.mesta.mipss.persistence.kontrakt.Samproduksjon;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import java.util.List;

public class ArtikkelProdtypeTableModel extends MipssBeanTableModel<ArtikkelProdtype>  {

    public static final int COL_ARTIKKEL = 0;
    public static final int COL_UNIT = 1;
    public static final int COL_PRICE = 2;
    public static final int COL_COPRODUCTION = 3;
    public static final int COL_PRODUCTION_TYPE = 4;
    public static final int COL_SPREADING_PRODUCT = 5;
    public static final int COL_EXPIRED = 6;

    public ArtikkelProdtypeTableModel(List<ArtikkelProdtype> artikler) {
        super(ArtikkelProdtype.class, new int[]{3, 4, 5}, "artikkel", "oppgjorsenhet", "enhetspris", "samproduksjon", "prodtype", "stroprodukt", "utgatt");
        setEntities(artikler);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        ArtikkelProdtype selectedArticle = getEntities().get(rowIndex);

        if (selectedArticle.isUtgatt()) {
            return false;
        }

        if (columnIndex == COL_COPRODUCTION || columnIndex == COL_PRODUCTION_TYPE) {
            return true;
        } else if (columnIndex == COL_SPREADING_PRODUCT) {
            return selectedArticle.getProdtype() != null && selectedArticle.getProdtype().getStroFlagg();
        }

        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ArtikkelProdtype selectedArticle = getEntities().get(rowIndex);

        switch(columnIndex) {
            case COL_ARTIKKEL: return selectedArticle.getArtikkel();
            case COL_UNIT: return selectedArticle.getOppgjorsenhet();
            case COL_PRICE: return selectedArticle.getEnhetspris();
            case COL_COPRODUCTION: return selectedArticle.getSamproduksjon();
            case COL_PRODUCTION_TYPE: return selectedArticle.getProdtype();
            case COL_SPREADING_PRODUCT: return selectedArticle.getStroprodukt();
            case COL_EXPIRED: return selectedArticle.isUtgatt() ? "Ja" : "";
            default:
                return "<UKJENT KOLONNE>";
        }
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch(column) {
            case COL_ARTIKKEL: return String.class;
            case COL_UNIT: return String.class;
            case COL_PRICE: return Double.class;
            case COL_COPRODUCTION: return Samproduksjon.class;
            case COL_PRODUCTION_TYPE: return Prodtype.class;
            case COL_SPREADING_PRODUCT: return Stroprodukt.class;
            case COL_EXPIRED: return String.class;
            default:
                return Object.class;
        }
    }

}