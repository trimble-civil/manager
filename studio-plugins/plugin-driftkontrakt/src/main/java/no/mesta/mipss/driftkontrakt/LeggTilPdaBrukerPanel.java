package no.mesta.mipss.driftkontrakt;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.mipssfield.Pdabruker;
import no.mesta.mipss.persistence.mipssfield.PdabrukerKontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.JMipssBeanScrollPane;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class LeggTilPdaBrukerPanel extends AbstractRelasjonPanel<List<PdabrukerKontrakt>> implements PropertyChangeListener{

	private final DriftkontraktController controller;
	
	private JLabel lblNavn;
	private JTextField fldNavn;
	private JButton btnSok;
	private JButton btnLeggTil;
	private JLabel lblNoResult;
	private JPanel pnlNyBruker;
	
	private MipssBeanTableModel<Pdabruker> mdlPdabruker;
	private JMipssBeanTable<Pdabruker> tblPdabruker;
	private JMipssBeanScrollPane scrPdabruker;

	private List<Pdabruker> selectedUsers;
	
	private BindingGroup bindings = new BindingGroup();
	
	public LeggTilPdaBrukerPanel(DriftkontraktController controller){
		this.controller = controller;
		initComponents();
		initGui();
		bind();
	}
	
	
	private void initComponents() {
		lblNavn = new JLabel(ResourceUtils.getResource("label.pdabrukernavn"));
		fldNavn = new JTextField();
		
		mdlPdabruker = new MipssBeanTableModel<Pdabruker>(Pdabruker.class, "signatur", "navn", "tlfnummer", "epostAdresse");
		mdlPdabruker.setBeanInstance(controller);
		mdlPdabruker.setBeanInterface(DriftkontraktController.class);
		mdlPdabruker.setBeanMethod("sokPdabruker");
		mdlPdabruker.setBeanMethodArguments(new Class<?>[]{String.class});

		
		MipssRenderableEntityTableColumnModel pdabrukerColumnModel = new MipssRenderableEntityTableColumnModel(Pdabruker.class, mdlPdabruker.getColumns());

		tblPdabruker = new JMipssBeanTable<Pdabruker>(mdlPdabruker, pdabrukerColumnModel);
		tblPdabruker.getColumn(0).setPreferredWidth(60);
		tblPdabruker.getColumn(1).setPreferredWidth(140);
		tblPdabruker.getColumn(2).setPreferredWidth(80);
		tblPdabruker.getColumn(3).setPreferredWidth(140);
		tblPdabruker.setFillsViewportWidth(true);
		tblPdabruker.setDoWidthHacks(false);
		tblPdabruker.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrPdabruker = new JMipssBeanScrollPane(tblPdabruker, mdlPdabruker);
		
		mdlPdabruker.addTableLoaderListener(new MipssBeanLoaderListener() {
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {}
			
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				if (mdlPdabruker.getEntities()==null||mdlPdabruker.getEntities().isEmpty()){//fant ingen brukere ved søk
					scrPdabruker.setViewportView(pnlNyBruker);
				}
			}
		});
		
		btnSok = new JButton(getSokAction());
		btnLeggTil = new JButton(getNyBrukerAction());
		
		pnlNyBruker = new JPanel(new BorderLayout());
		lblNoResult = new JLabel(ResourceUtils.getResource("label.noResults"));
		pnlNyBruker.add(lblNoResult);
		pnlNyBruker.add(btnLeggTil, BorderLayout.SOUTH);
		
	}
	
	private void bind(){
		bindings.addBinding(BindingHelper.createbinding(tblPdabruker, "selectedEntities", this, "selectedUsers"));
		bindings.bind();
	}
	public void setSelectedUsers(List<Pdabruker> selectedUsers){
		this.selectedUsers = selectedUsers;
		setComplete(Boolean.valueOf(selectedUsers != null && !selectedUsers.isEmpty()));
	}
	
	private Action getSokAction(){
		return new AbstractAction(ResourceUtils.getResource("sokbutton"), IconResources.SEARCH_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				mdlPdabruker.setBeanMethodArgValues(new Object[]{fldNavn.getText()});
				mdlPdabruker.loadData();
			}
		};
	}
	private Action getNyBrukerAction(){
		return new AbstractAction(ResourceUtils.getResource("button.leggtilNybruker"), IconResources.ADD_ONE_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				NyPdabrukerPanel pdabrukerPanel = new NyPdabrukerPanel();
				LeggTilRelasjonDialog<Pdabruker> relDialog = new LeggTilRelasjonDialog<Pdabruker>(controller.getDriftkontraktAdminModule().getLoader().getApplicationFrame(), pdabrukerPanel);
				Pdabruker pdabruker = relDialog.showDialog();
				pdabruker.setOpprettetAv(controller.getDriftkontraktAdminModule().getLoader().getLoggedOnUserSign());
				pdabruker.setOpprettetDato(Clock.now());
				mdlPdabruker.addItem(pdabruker);
				scrPdabruker.loadingFinished(new MipssBeanLoaderEvent(this, false));

			}
		};
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		
		add(lblNavn,	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 5, 5, 0), 0, 0));
		add(fldNavn,	new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11, 5, 5, 0), 0, 0));
		add(btnSok,		new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(11, 5, 5, 11), 0, 0));
		
		add(scrPdabruker, new GridBagConstraints(0, 1, 3, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(11, 11, 19, 11), 0, 0));
		
	}


	@Override
	public Dimension getDialogSize() {
		return new Dimension(450, 345);
	}

	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilPdabruker");
	}

	@Override
	public List<PdabrukerKontrakt> getNyRelasjon() {
		if (selectedUsers!=null&&selectedUsers.size()>0){
			
			Pdabruker pdabruker = selectedUsers.get(0);
			if (pdabruker.getOpprettetDato()==null){//NY bruker
				pdabruker.setOpprettetAv(controller.getDriftkontraktAdminModule().getLoader().getLoggedOnUserSign());
				pdabruker.setOpprettetDato(Clock.now());
			}
			PdabrukerKontrakt rel = new PdabrukerKontrakt();
			rel.setDriftkontrakt(controller.getCurrentDriftkontrakt());
			rel.setPdabruker(pdabruker);
			rel.setInaktivFlagg(Boolean.FALSE);
			List<PdabrukerKontrakt> list = new ArrayList<PdabrukerKontrakt>();
			list.add(rel);
			return list;
			
		}
		return null;
	}

	@Override
	public boolean isLegal() {
		return true;
	}
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
//		updateComplete();
	}

}
