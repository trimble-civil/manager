package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.TextAreaProvider;
import no.mesta.mipss.util.DatePickerConstraintsUtils;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.table.TableColumnExt;

@SuppressWarnings("serial")
public class KjoretoyListPanel extends JPanel {
	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneKjoretoyList = new JScrollPane();
	private JMipssBeanTable<KjoretoyslisteTableObject> tblKjoretoyList;
	private JButton btnEndreKjoretoy = new JButton();
	private JButton btnKnyttKjoretoy = new JButton();
	private JButton btnKobleFra = new JButton();
	private JButton btnVisRaaData = new JButton();
	private JButton btnGraf = new JButton();
	private JButton btnRapport = new JButton();
	private JButton btnMap = new JButton();
	private JButton btnUkjentStrometode = new JButton();
    private JXBusyLabel lblBusy = new JXBusyLabel();
    private JPanel pnlVelgPeriode = new JPanel();
    private JRadioButton rbtnIDag = new JRadioButton();
    private JRadioButton rbtnSisteSyvDogn = new JRadioButton();
    private JRadioButton rbtnSisteTredveDogn = new JRadioButton();
    private JRadioButton rbtnValgfriPeriode = new JRadioButton();
    private JLabel lblFra = new JLabel();
    private JLabel lblTil = new JLabel();
    private JXDatePicker datePckrValgfriPerFra = new JXDatePicker();
    private JXDatePicker datePckrValgfriPerTil = new JXDatePicker();
    private ButtonGroup btnGroup = new ButtonGroup();
	
	private DriftkontraktController driftkontraktController;
	private BindingGroup bindingGroup;
	private MipssBeanTableModel<KjoretoyslisteTableObject> kontraktKjoretoyModel;
	
	/**
	 * Inneholder hvilken fane som skal åpnes når man klikker på en gitt celle i tabellen 
	 */
	private Map<Integer, Integer> kolonneFaneMapping = new HashMap<Integer,Integer>();
	
	public KjoretoyListPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		
		bindingGroup = new BindingGroup();
		int c =0;
		kolonneFaneMapping.put(c++, 0);//kjøretøyId
		kolonneFaneMapping.put(c++, 0);//regnummer
		kolonneFaneMapping.put(c++, 0);//type og fabrikat
		kolonneFaneMapping.put(c++, 5);//leverandør	
		kolonneFaneMapping.put(c++, 5);//Eier	
		kolonneFaneMapping.put(c++, 0);//bestillingstatus
		kolonneFaneMapping.put(c++, 2);//utstyr
		kolonneFaneMapping.put(c++, 3);//strøinformasjon
		kolonneFaneMapping.put(c++, 4);//klippeinformasjon
		kolonneFaneMapping.put(c++, 0);//RTCU tlf
		kolonneFaneMapping.put(c++, 0);//RTCU serienummer
		kolonneFaneMapping.put(c++, 0);//ukjent produksjon
		kolonneFaneMapping.put(c++, 0);//Siste livstegn
		kolonneFaneMapping.put(c++, 0);//Siste GPS
		kolonneFaneMapping.put(c++, 0);//tomkj km
		kolonneFaneMapping.put(c++, 0);//tot km
		kolonneFaneMapping.put(c++, 0);//tomkj %
		
		initLabels();
		initTable();
		initButtons();
		initDatePickers();
		initGui();
		
		bindingGroup.bind();
	}
	
	public void setKontrakt(Driftkontrakt driftkontrakt) {
		lastData(driftkontrakt);
	}
	
	private void initTable() {
    	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
    	String advarselCol = params.hentEnForApp("studio.mipss.driftkontrakt", "fargeKjoretoylisteAdvarsel").getVerdi();
    	String advarselSelectedCol = params.hentEnForApp("studio.mipss.driftkontrakt", "fargeKjoretoylisteAdvarselSel").getVerdi();
    	String mangelCol = params.hentEnForApp("studio.mipss.driftkontrakt", "fargeKjoretoylisteMangel").getVerdi();
    	String mangelSelectedCol = params.hentEnForApp("studio.mipss.driftkontrakt", "fargeKjoretoylisteMangelSel").getVerdi();
    	Color colorAdv = ColorUtil.fromHexString(advarselCol);
    	Color colorAdvSel = ColorUtil.fromHexString(advarselSelectedCol);
    	Color colorMangel = ColorUtil.fromHexString(mangelCol);
    	Color colorMangelSel = ColorUtil.fromHexString(mangelSelectedCol);
		
		kontraktKjoretoyModel = new MipssBeanTableModel<KjoretoyslisteTableObject>(KjoretoyslisteTableObject.class, "eksterntNavn", "regnr", "typeOgFabrikat", "leverandor", "kjoretoyEier", "bestillingStatus", "aktuellKonfigKlartekst", "aktuellStroperiodeKlartekst", "klippeinformasjon", "dfuTlfnummer", "dfuSerienummer", "ukjentProduksjon", "ukjentStrometode", "dfuSisteLivtegnDato", "dfuSistePosisjonDatoCET", "tomkjoringKm", "totalKm", "tomkjoringProsent");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", this, "kontrakt", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelDfuKontrakt = new MipssRenderableEntityTableColumnModel(KjoretoyslisteTableObject.class, kontraktKjoretoyModel.getColumns());
		tblKjoretoyList = new JMipssBeanTable<KjoretoyslisteTableObject>(kontraktKjoretoyModel, columnModelDfuKontrakt);
		tblKjoretoyList.setFillsViewportWidth(true);
		tblKjoretoyList.setDoWidthHacks(false);
		
		tblKjoretoyList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Integer faneIndex = kolonneFaneMapping.get(tblKjoretoyList.getSelectedColumn());
				
				if(e.getClickCount() >= 2) {
					Action a = driftkontraktController.getEndreKjoretoyAction(tblKjoretoyList, kontraktKjoretoyModel, new AbstractAction() {
						@Override
						public void actionPerformed(ActionEvent e) {
							reloadData();
						}}, faneIndex);
					a.actionPerformed(null);
				}
			}
		});
		
		tblKjoretoyList.addHighlighter(new ColorHighlighter(new HighlightPredicate() {
			@Override
			public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
				if(adapter.getValue() instanceof ColorableTableValue) {
					ColorableTableValue colorableTableValue = (ColorableTableValue)adapter.getValue();
					return colorableTableValue.isRed();
				} else {
					return false;
				}
			}
		}, colorMangel, Color.BLACK, colorMangelSel, Color.BLACK));
		
		tblKjoretoyList.addHighlighter(new ColorHighlighter(new HighlightPredicate() {
			@Override
			public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
				if(adapter.getValue() instanceof ColorableTableValue) {
					ColorableTableValue colorableTableValue = (ColorableTableValue)adapter.getValue();
					return colorableTableValue.isYellow();
				} else {
					return false;
				}
			}
		}, colorAdv, Color.BLACK, colorAdvSel, Color.BLACK));
		
		tblKjoretoyList.setRowHeightEnabled(true);
		tblKjoretoyList.setDefaultRenderer(Object.class, new DefaultTableRenderer(new TextAreaProvider()) {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				
				if(component instanceof JTextArea && value instanceof ColorableTableValue) {
					JTextArea txtArea = (JTextArea)component;
					ColorableTableValue colorableTableValue = (ColorableTableValue)value;
					
					if (colorableTableValue.getValue() instanceof Boolean){
						Boolean b = colorableTableValue.isRed()||colorableTableValue.isYellow();
						txtArea.setText(b?" X ":"");
					}
					
					if (colorableTableValue.getValue() instanceof Date){
						Date d = (Date)colorableTableValue.getValue();
						txtArea.setText(MipssDateFormatter.formatDate(d, MipssDateFormatter.LONG_YEARDATE_TIME_FORMAT));
					}
					txtArea.setToolTipText(colorableTableValue.getTooltipText());
				}
				
				return component;
			}
		});
		
		tblKjoretoyList.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnAdded(TableColumnModelEvent e) {}
            public void columnRemoved(TableColumnModelEvent e) {}
            public void columnMoved(TableColumnModelEvent e) {}
            public void columnSelectionChanged(ListSelectionEvent e) {}
 
            public void columnMarginChanged(ChangeEvent e) {
                calculateRowHeights();
            }
        });
		
		
		
		TableColumnExt tableColumnExt = tblKjoretoyList.getColumnExt(KjoretoyslisteTableObject.class.getName() + ".tomkjoringProsent");
		tableColumnExt.setComparator(new Comparator<ColorableTableValue>() {
			@Override
			public int compare(ColorableTableValue o1, ColorableTableValue o2) {
				if(o1.getValue() instanceof Double) {
					if(o1.getValue() != null && o2.getValue() != null) {
						return ((Double)o1.getValue()).compareTo((Double)o2.getValue());
					} else if (o1.getValue() != null) {
						return 1;
					} else if(o2.getValue() != null) {
						return -1;
					} else {
						return 0;
					}
				} else {
					return o1.toString().compareTo(o2.toString());
				}
			}
		});
	}
	
	private void calculateRowHeights() {
		for (int row = 0; row < tblKjoretoyList.getRowCount(); row++) {
            int rowHeight = 0;
            for (int column = 0; column < tblKjoretoyList.getColumnCount(); column++) {
                Component comp = tblKjoretoyList.prepareRenderer(
                		tblKjoretoyList.getCellRenderer(row, column), row, column);
                rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
            }
            tblKjoretoyList.setRowHeight(row, rowHeight); 
        }
	}
	
	
	private void initButtons() {
		AbstractAction reloadAction = new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				reloadData();
			}
		};
		btnKnyttKjoretoy.setAction(driftkontraktController.getKnyttKjoretoyAction(tblKjoretoyList, kontraktKjoretoyModel, reloadAction));
		btnKobleFra.setAction(driftkontraktController.getKobleFraKjoretoyAction(tblKjoretoyList, kontraktKjoretoyModel, reloadAction));
		
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnKnyttKjoretoy, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(tblKjoretoyList, "${noOfSelectedRows == 1}", btnKobleFra, "enabled"));
		
		btnKnyttKjoretoy.setIcon(IconResources.BIL_SMALL_ICON);
		btnKobleFra.setIcon(IconResources.REMOVE_ITEM_ICON);
		btnKobleFra.setToolTipText(ResourceUtils.getResource("button.kobleFraKjoretoy.tooltip"));
		
		bindingGroup.addBinding(BindingHelper.createbinding(tblKjoretoyList, "${noOfSelectedRows == 1}", btnEndreKjoretoy, "enabled"));
		
		btnEndreKjoretoy.setAction(driftkontraktController.getEndreKjoretoyAction(tblKjoretoyList, kontraktKjoretoyModel, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reloadData();
			}}, 0));
		
		btnEndreKjoretoy.setIcon(IconResources.DETALJER_SMALL_ICON);
		
		btnVisRaaData.setAction(driftkontraktController.getVisKjoretoyRaadataAction(tblKjoretoyList, kontraktKjoretoyModel));	
		btnVisRaaData.setIcon(IconResources.MIPSS_LOGG);
		bindingGroup.addBinding(BindingHelper.createbinding(tblKjoretoyList, "${noOfSelectedRows == 1}", btnVisRaaData, "enabled"));
		
		btnGraf.setAction(driftkontraktController.getVisKjoretoyGrafAction(tblKjoretoyList, kontraktKjoretoyModel, datePckrValgfriPerFra));
		btnGraf.setIcon(IconResources.GRAF_ICON_SMALL);
		bindingGroup.addBinding(BindingHelper.createbinding(tblKjoretoyList, "${noOfSelectedRows == 1}", btnGraf, "enabled"));
	
		btnMap.setAction(driftkontraktController.getVisKjoretoyIKartAction(tblKjoretoyList, kontraktKjoretoyModel));
		btnMap.setIcon(IconResources.NORGE_ICON);
		btnMap.setToolTipText(ResourceUtils.getResource("tooltip.visKjoretoyIKart"));		
		bindingGroup.addBinding(BindingHelper.createbinding(tblKjoretoyList, "${noOfSelectedRows == 1}", btnMap, "enabled"));
		
		btnUkjentStrometode.setAction(driftkontraktController.getVisUkjentStrometodeAction(tblKjoretoyList, kontraktKjoretoyModel));
		btnUkjentStrometode.setIcon(IconResources.SPREDER_ICON_16);
		btnUkjentStrometode.setToolTipText(ResourceUtils.getResource("button.ukjentStrometode.tooltip"));
		bindingGroup.addBinding(BindingHelper.createbinding(tblKjoretoyList, "${noOfSelectedRows == 1}", btnUkjentStrometode, "enabled"));
		
		btnRapport.setAction(driftkontraktController.getVisKjoretoyslisteIExcelAction(tblKjoretoyList, kontraktKjoretoyModel));
		btnRapport.setIcon(IconResources.EXCEL_ICON16);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnRapport, "enabled"));
	
		btnGroup.add(rbtnIDag);
		btnGroup.add(rbtnSisteSyvDogn);
		btnGroup.add(rbtnSisteTredveDogn);
		btnGroup.add(rbtnValgfriPeriode);
		rbtnIDag.setSelected(true);
		
		rbtnIDag.setText(ResourceUtils.getResource("button.kjoretoylisteIDag"));
		rbtnSisteSyvDogn.setText(ResourceUtils.getResource("button.kjoretoyliste7Dager"));
		rbtnSisteTredveDogn.setText(ResourceUtils.getResource("button.kjoretoyliste30Dager"));
		rbtnValgfriPeriode.setText(ResourceUtils.getResource("button.kjoretoylisteValgfritt"));
		
		ActionListener action = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				datePckrValgfriPerFra.setEnabled(rbtnValgfriPeriode.isSelected());
				datePckrValgfriPerTil.setEnabled(rbtnValgfriPeriode.isSelected());
				lblFra.setEnabled(rbtnValgfriPeriode.isSelected());
				lblTil.setEnabled(rbtnValgfriPeriode.isSelected());
				
				if(datePckrValgfriPerFra.getDate().after(datePckrValgfriPerTil.getDate())) {
					if(e.getSource().equals(datePckrValgfriPerFra)) {
						datePckrValgfriPerTil.setDate(datePckrValgfriPerFra.getDate());
					} else {
						datePckrValgfriPerFra.setDate(datePckrValgfriPerTil.getDate());
					}
				}
								
				reloadData();
			}
		};
		
		rbtnIDag.addActionListener(action);
		rbtnSisteSyvDogn.addActionListener(action);
		rbtnSisteTredveDogn.addActionListener(action);
		rbtnValgfriPeriode.addActionListener(action);
		
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", rbtnIDag, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", rbtnSisteSyvDogn, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", rbtnSisteTredveDogn, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", rbtnValgfriPeriode, "enabled"));
		
		datePckrValgfriPerFra.addActionListener(action);
		datePckrValgfriPerTil.addActionListener(action);
	}
	
	private void initLabels() {
		lblFra.setText(ResourceUtils.getResource("label.fraDato"));
		lblTil.setText(ResourceUtils.getResource("label.tilDato"));
		lblFra.setEnabled(false);
		lblTil.setEnabled(false);
	}
	
	private void initDatePickers() {
		datePckrValgfriPerFra.setDate(Clock.now());
		datePckrValgfriPerFra.setEnabled(false);
		datePckrValgfriPerTil.setDate(Clock.now());
		datePckrValgfriPerTil.setEnabled(false);
		
		DatePickerConstraintsUtils.setNotBeforeDate(driftkontraktController.getDriftkontraktAdminModule().getLoader(), datePckrValgfriPerFra, datePckrValgfriPerTil);
	}
	
	private void initGui() {
		scrlPaneKjoretoyList.setViewportView(tblKjoretoyList);
		
		pnlVelgPeriode.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kjoretoyProduksjon")));
		
		pnlVelgPeriode.setLayout(new GridBagLayout());
		pnlVelgPeriode.add(rbtnIDag, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		pnlVelgPeriode.add(rbtnSisteSyvDogn, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		pnlVelgPeriode.add(rbtnSisteTredveDogn, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		pnlVelgPeriode.add(rbtnValgfriPeriode, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		pnlVelgPeriode.add(lblFra, new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		pnlVelgPeriode.add(datePckrValgfriPerFra, new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
		pnlVelgPeriode.add(lblTil, new GridBagConstraints(0, 6, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		pnlVelgPeriode.add(datePckrValgfriPerTil, new GridBagConstraints(0, 7, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 5), 0, 0));
				
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneKjoretoyList, 	new GridBagConstraints(0, 0, 1, 9, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(btnKnyttKjoretoy, 		new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnKobleFra, 			new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnEndreKjoretoy, 		new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnVisRaaData,			new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnGraf, 				new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnMap, 				new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnUkjentStrometode,	new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(btnRapport, 			new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		pnlMain.add(pnlVelgPeriode, 		new GridBagConstraints(1, 8, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
	
    private void setBusy(boolean busy) {
    	lblBusy.setVisible(busy);
		lblBusy.setBusy(busy);
		tblKjoretoyList.setVisible(!busy);
		scrlPaneKjoretoyList.setViewportView(busy?lblBusy:tblKjoretoyList);
		scrlPaneKjoretoyList.repaint();
    }
    
    private Date getFraDatoForProduksjon() {
    	Date now = getTilDatoForProduksjon();
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(now);
    	if(rbtnIDag.isSelected()) {
    		cal.add(Calendar.DATE, -1);
    	} else if(rbtnSisteSyvDogn.isSelected()) {
    		cal.add(Calendar.DATE, -7);
    	} else if(rbtnSisteTredveDogn.isSelected()) {
    		cal.add(Calendar.DATE, -30);
    	} else if(rbtnValgfriPeriode.isSelected()) {
    		cal.setTime(datePckrValgfriPerFra.getDate());
    		cal.set(Calendar.HOUR_OF_DAY, 0);
    		cal.set(Calendar.MINUTE, 0);
    		cal.set(Calendar.SECOND, 0);
    	}
    	driftkontraktController.setSearchFromDate(cal.getTime());
    	return cal.getTime();	
    }
    
    private Date getTilDatoForProduksjon() {
    	if(rbtnValgfriPeriode.isSelected()) {
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(datePckrValgfriPerTil.getDate());
    		cal.set(Calendar.HOUR_OF_DAY, 23);
    		cal.set(Calendar.MINUTE, 59);
    		cal.set(Calendar.SECOND, 59);
    		driftkontraktController.setSearchToDate(cal.getTime());
    		return cal.getTime();
    	} else {
    		driftkontraktController.setSearchToDate(Clock.now());
    		return Clock.now();
    	}
    }
    
    private void reloadData() {
    	lastData(driftkontraktController.getCurrentDriftkontrakt());
    }
    
    @SuppressWarnings("unchecked")
    private void lastData(final Driftkontrakt driftkontrakt) {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
				setBusy(true);
				List<KjoretoyslisteTableObject> newList = null;
				
				if(driftkontrakt != null) {
					newList = driftkontraktController.getKontraktKjoretoyInfo(driftkontrakt.getId(), getFraDatoForProduksjon(), getTilDatoForProduksjon());
				} else {
					newList = new ArrayList<KjoretoyslisteTableObject>();
				}
				
				tblKjoretoyList.clearSelection();
				kontraktKjoretoyModel.setEntities(newList);
				calculateRowHeights();
				setBusy(false);
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				} 
    		}
    	}.start();
    }
    
    public Action getReloadAction() {
    	return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reloadData();
			}
		};
    }
}
