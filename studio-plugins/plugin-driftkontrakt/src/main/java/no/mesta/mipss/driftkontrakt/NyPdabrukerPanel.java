package no.mesta.mipss.driftkontrakt;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.mipssfield.Pdabruker;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.util.EpostUtils;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXHeader;

@SuppressWarnings("serial")
public class NyPdabrukerPanel extends AbstractRelasjonPanel<Pdabruker>{

	private JLabel lblSignatur;
	private JLabel lblNavn;
	private JLabel lblTelefon;
	private JLabel lblEpost;
	
	private JTextField fldSignatur;
	private JTextField fldNavn;
	private JTextField fldTelefon;
	private JTextField fldEpost;
	private Pdabruker pdabruker;
	
	public NyPdabrukerPanel(){
		this(null);
	}
	public NyPdabrukerPanel(Pdabruker pdabruker){
		this.pdabruker = pdabruker;
		initComponents();
		initGui();
	}
	private void initComponents() {
		lblSignatur = new JLabel(ResourceUtils.getResource("label.pda.signatur"));
		lblNavn = new JLabel(ResourceUtils.getResource("label.pda.navn"));
		lblTelefon = new JLabel(ResourceUtils.getResource("label.pda.telefon"));
		lblEpost = new JLabel(ResourceUtils.getResource("label.pda.epost"));
		
		fldSignatur = new JTextField();
		fldNavn = new JTextField();
		fldTelefon = new JTextField();
		fldEpost = new JTextField();
		
		KeyAdapter listener = new KeyAdapter(){
			@Override
			public void keyReleased(KeyEvent e) {
				complete();
			}
		};
		fldSignatur.addKeyListener(listener);
		fldNavn.addKeyListener(listener);
		fldTelefon.addKeyListener(listener);
		fldEpost.addKeyListener(listener);
		if (pdabruker==null)
			fldSignatur.setDocument(new DocumentFilter(5, "abcdefghijklmnopqrstuvwxyz", false));
		fldTelefon.setDocument(new DocumentFilter(8));
		
		if (pdabruker!=null){
			fldSignatur.setText(pdabruker.getSignatur());
			fldNavn.setText(pdabruker.getNavn());
			fldTelefon.setText(String.valueOf(pdabruker.getTlfnummer()));
			fldEpost.setText(pdabruker.getEpostAdresse());
			fldSignatur.setEditable(false);
			fldNavn.setEditable(false);
		}
	}
	
	
	private void initGui() {
		setLayout(new BorderLayout());
		
		JPanel pnl = new JPanel(new GridBagLayout());
		pnl.add(lblSignatur,new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		 new Insets(11, 11, 0, 5), 0, 0));
		pnl.add(fldSignatur,new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11, 5, 0, 11), 0, 0));
		pnl.add(lblNavn, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		 new Insets(5, 11, 0, 5), 0, 0));
		pnl.add(fldNavn, 	new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 11), 0, 0));
		pnl.add(lblTelefon, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		 new Insets(5, 11, 0, 5), 0, 0));
		pnl.add(fldTelefon, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 11), 0, 0));
		pnl.add(lblEpost, 	new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 		 new Insets(5, 11, 11, 5), 0, 0));
		pnl.add(fldEpost, 	new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 11, 11), 0, 0));
		
		add(getHeader(), BorderLayout.NORTH);
		add(pnl, BorderLayout.CENTER);
	}
	
	private JXHeader getHeader(){
		return new JXHeader(ResourceUtils.getResource("head.nybruker.title"), ResourceUtils.getResource("head.nybruker.description"), IconResources.INFO_ICON);
	}
	@Override
	public Dimension getDialogSize() {
		return new Dimension(450, 345);
	}

	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.nyPdabruker");
	}

	@Override
	public Pdabruker getNyRelasjon() {
		if (pdabruker==null){
			Pdabruker p = new Pdabruker();
			p.setSignatur("#"+fldSignatur.getText());
			p.setNavn(fldNavn.getText());
			p.setTlfnummer(Long.valueOf(fldTelefon.getText()));
			p.setEpostAdresse(fldEpost.getText());
			return p;
		}else{
			pdabruker.setTlfnummer(Long.valueOf(fldTelefon.getText()));
			pdabruker.setEpostAdresse(fldEpost.getText());
			return pdabruker;
		}
	}

	private boolean complete(){
		String sign = fldSignatur.getText();
		String navn = fldNavn.getText();
		String tele = fldTelefon.getText();
		String epos = fldEpost.getText();
		try{
			Long.parseLong(tele);
		} catch (NumberFormatException e){
			setComplete(false);
			return false;
		}
		
		if (StringUtils.isEmpty(sign)||
			StringUtils.isEmpty(navn)||
			(!StringUtils.isEmpty(epos)&&!EpostUtils.verifyEpost(epos))){
			setComplete(false);
			return false;
		}else{
			setComplete(true);
			return true;
		}
		
	}
	
	@Override
	public boolean isLegal() {
		return complete();
	}

}
