package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class StabPanel extends JPanel{
	private final DriftkontraktController driftkontraktController;
	private JScrollPane scrlStab = new JScrollPane();
	private JMipssBeanTable<KontraktStab> tblStab;
	private MipssBeanTableModel<KontraktStab> stabModel;
	private BindingGroup bindingGroup;
	private JPanel pnlStabButtons = new JPanel();
	private JButton btnNyStab = new JButton();
	private JButton btnSlettStab = new JButton();
	private JButton btnElrappBruker = new JButton();
	
	public StabPanel(DriftkontraktController driftkontraktController){
		this.driftkontraktController = driftkontraktController;
		bindingGroup = new BindingGroup();
		
		initComponents();
		initGui();
		bindingGroup.bind();
	}
	
	private void initComponents(){
		driftkontraktController.registerChangableField("kontraktStabList", "prosjektKontraktList");

		stabModel = new MipssBeanTableModel<KontraktStab>("kontraktStabList", KontraktStab.class, "arbeidsfunksjon", "bruker", "elrappBruker");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", stabModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelStab = new MipssRenderableEntityTableColumnModel(KontraktStab.class, stabModel.getColumns());

		tblStab = new JMipssBeanTable<KontraktStab>(stabModel, columnModelStab);
		tblStab.setFillsViewportWidth(true);
		tblStab.setDoWidthHacks(false);
		tblStab.setDefaultRenderer(Arbeidsfunksjon.class, new RenderableMipssEntityTableCellRenderer<Arbeidsfunksjon>());
		tblStab.setDefaultRenderer(Bruker.class, new RenderableMipssEntityTableCellRenderer<Bruker>());
		tblStab.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() >= 2) {
					Action a = driftkontraktController.getEndreStabAction(stabModel, tblStab);
					a.actionPerformed(null);
				}
			}
		});
		tblStab.setPreferredScrollableViewportSize(new Dimension(0, 0));
		scrlStab.getViewport().add(tblStab, null);

		
		btnNyStab.setAction(driftkontraktController.getLeggTilKontraktStabAction());
		btnNyStab.setIcon(IconResources.ADD_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnNyStab, "enabled"));

		btnSlettStab.setAction(driftkontraktController.getSlettKontraktStabAction(stabModel, tblStab));
		btnSlettStab.setIcon(IconResources.REMOVE_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblStab, "${noOfSelectedRows > 0}", btnSlettStab, "enabled"));

		btnElrappBruker.setAction(driftkontraktController.getEndreStabAction(stabModel, tblStab));
		btnElrappBruker.setIcon(IconResources.ELRAPP_ICON_16);
		bindingGroup.addBinding(BindingHelper.createbinding(tblStab, "${noOfSelectedRows > 0}", btnElrappBruker, "enabled"));

	}
	
	private void initGui(){
		pnlStabButtons.setLayout(new GridBagLayout());
		setLayout(new GridBagLayout());
		add(scrlStab, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 0), 0, 0));
		pnlStabButtons.add(btnNyStab, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlStabButtons.add(btnSlettStab, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlStabButtons.add(btnElrappBruker, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		add(pnlStabButtons, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 5, 5, 5), 0, 0));

		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.brukereIKontrakt")));

	}
}
