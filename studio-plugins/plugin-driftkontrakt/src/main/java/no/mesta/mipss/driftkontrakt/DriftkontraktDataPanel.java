package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.HashingUtils;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Kontraktlogg;
import no.mesta.mipss.persistence.kontrakt.Kontrakttype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.DateTimeRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class DriftkontraktDataPanel extends JPanel {
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
	private JPanel pnlMain = new JPanel();
	private GridBagLayout gridBagLayout2 = new GridBagLayout();
	private JPanel pnlTop = new JPanel();
	private JPanel pnlBottom = new JPanel();
	private GridBagLayout gridBagLayout3 = new GridBagLayout();
	private JLabel lblType = new JLabel();
	private JLabel lblDistrikt = new JLabel();
	private JLabel lblVeinett = new JLabel();
	private JLabel lblAreal = new JLabel();
	private JLabel lblKontraktsnr = new JLabel();
	private JLabel lblKontraktsnavn = new JLabel();
	private JLabel lblDatoFra = new JLabel();
	private JLabel lblDatoTil = new JLabel();
	private JLabel lblBeskrivelse = new JLabel();
	private JLabel lblKontraktspassord = new JLabel();
	private JLabel lblKontraktspassordHashed = new JLabel();

	private JComboBox cbxType = new JComboBox();
	private JComboBox cbxDistrikt = new JComboBox();
	private JTextField txtVeinett = new JTextField();
	private JTextField txtAreal = new JTextField();
	private JTextField txtKontraktsnr = new JTextField();
	private JTextField txtKontraktsnavn = new JTextField();
	private JTextField txtKontraktspassord = new JTextField();
	private JTextField txtKontraktspassordHashed = new JTextField();

	private JXDatePicker dtPckrDatoFra;
	private JXDatePicker dtPckrDatoTil;
	private JTextArea txtAreaBeskrivelse = new JTextArea();
	private JScrollPane scrlPaneBeskrivelse = new JScrollPane();
	private GridBagLayout gridBagLayout5 = new GridBagLayout();
	private JXTable tblKontraktslogg = new JXTable();
	private JScrollPane scrollPaneTbl = new JScrollPane();
	private JPanel pnlSideButtons = new JPanel();
	private GridBagLayout gridBagLayout6 = new GridBagLayout();
	private JButton btnNyLogg = new JButton();
	private JLabel lblKontraktslogg = new JLabel();
	private JButton btnVisLogg = new JButton();

	private DriftkontraktController driftkontraktController;
	private BindingGroup bindingGroup;

	public DriftkontraktDataPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;

		bindingGroup = new BindingGroup();

		initDatePickers();
		initGui();
		initComboBoxes();
		initButtons();
		initTable();
		initTextFields();

		bindingGroup.bind();
	}

	private void initTextFields() {
		txtKontraktsnr.setEditable(false);
		txtKontraktsnavn.setEditable(false);
		dtPckrDatoFra.setEditable(false);
		dtPckrDatoTil.setEditable(false);
		txtKontraktsnr.setEditable(false);

		driftkontraktController.registerChangableField("kontraktnummer", "kontraktnavn", "gyldigFraDato", "gyldigTilDato", "beskrivelse", "driftdistrikt", "kontrakttype", "kontraktpassord");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.kontraktnummer}", txtKontraktsnr, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.kontraktnavn}", txtKontraktsnavn, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.gyldigFraDato}", dtPckrDatoFra, "date", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.gyldigTilDato}", dtPckrDatoTil, "date", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null && currentDriftkontrakt.veinett != null ? currentDriftkontrakt.veinett.navn : null}", txtVeinett, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null && currentDriftkontrakt.areal != null ? currentDriftkontrakt.areal.navn : null}", txtAreal, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.beskrivelse}", txtAreaBeskrivelse, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", txtAreaBeskrivelse, "editable", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", txtKontraktspassord, "editable", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.kontraktpassord}", txtKontraktspassord, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.kontraktpassordHashed}", txtKontraktspassordHashed, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.kontraktpassord}", this, "kontraktpassord", BindingHelper.READ_WRITE));
	}

	public void setKontraktpassord(String kontraktpassord){
		if (StringUtils.isEmpty(kontraktpassord)){
			driftkontraktController.getCurrentDriftkontrakt().setKontraktpassordHashed(null);
			return;
		}
		String hash = new HashingUtils().toHash(kontraktpassord);
		driftkontraktController.getCurrentDriftkontrakt().setKontraktpassordHashed(hash);
	}
	@SuppressWarnings("rawtypes")
	private void initComboBoxes() {
		List<Driftdistrikt> distriktTyper = driftkontraktController.getDriftdistriktList();

		cbxDistrikt.setRenderer((ListCellRenderer) new MipssListCellRenderer(ResourceUtils.getResource("comboboxItem.ingenDistrikt")));
		JComboBoxBinding distriktCBBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ, distriktTyper, cbxDistrikt);
		bindingGroup.addBinding(distriktCBBinding);
		Binding distriktBinding = BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.driftdistrikt}", cbxDistrikt, "selectedItem", BindingHelper.READ_WRITE);
		bindingGroup.addBinding(distriktBinding);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", cbxDistrikt, "enabled", BindingHelper.READ));

		List<Kontrakttype> kontraktTyper = driftkontraktController.getKontrakttypeList();
		cbxType.setRenderer((ListCellRenderer) new MipssListCellRenderer(ResourceUtils.getResource("comboboxItem.ingenType")));
		JComboBoxBinding typeCBBinding = SwingBindings.createJComboBoxBinding(AutoBinding.UpdateStrategy.READ, kontraktTyper, cbxType);
		bindingGroup.addBinding(typeCBBinding);
		Binding typeBinding = BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.kontrakttype}", cbxType, "selectedItem", BindingHelper.READ_WRITE);
		bindingGroup.addBinding(typeBinding);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", cbxType, "enabled", BindingHelper.READ));
	}

	private void initDatePickers() {
		Locale loc = new Locale("no", "NO");
		String[] formats = new String[] { "dd.MM.yyyy" };

		dtPckrDatoFra = new JXDatePicker(loc);
		dtPckrDatoTil = new JXDatePicker(loc);

		dtPckrDatoFra.setFormats(formats);
		dtPckrDatoTil.setFormats(formats);
	}

	private void initButtons() {
		driftkontraktController.registerChangableField("kontraktloggList");
		btnNyLogg.setAction(driftkontraktController.getNyLoggAction());
		btnNyLogg.setIcon(IconResources.ADD_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnNyLogg, "enabled"));

		btnVisLogg.setAction(driftkontraktController.getVisLoggDetaljerAction());
		btnVisLogg.setIcon(IconResources.DETALJER_SMALL_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnVisLogg, "enabled"));
	}

	private void initTable() {
		MipssBeanTableModel<Kontraktlogg> model = new MipssBeanTableModel<Kontraktlogg>("kontraktloggList", Kontraktlogg.class, "opprettetAv", "opprettetDato", "fritekst");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", model, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Kontraktlogg.class, model.getColumns());

		tblKontraktslogg.setModel(model);
		tblKontraktslogg.setColumnModel(columnModel);
		tblKontraktslogg.setDefaultRenderer(Timestamp.class, new DateTimeRenderer());
	}

	private void initGui() {
		this.setLayout(gridBagLayout1);
		pnlMain.setLayout(gridBagLayout2);
		pnlTop.setLayout(gridBagLayout3);
		pnlBottom.setLayout(gridBagLayout5);
		lblType.setText(ResourceUtils.getResource("label.kontraktstype"));
		lblDistrikt.setText(ResourceUtils.getResource("label.mestaDistrikt"));
		lblVeinett.setText(ResourceUtils.getResource("label.veinett"));
		lblAreal.setText(ResourceUtils.getResource("label.areal"));
		lblKontraktsnr.setText(ResourceUtils.getResource("label.kontraktsnummer"));
		lblKontraktsnavn.setText(ResourceUtils.getResource("label.kontraktsnavn"));
		lblDatoFra.setText(ResourceUtils.getResource("label.fraDato"));
		lblDatoTil.setText(ResourceUtils.getResource("label.tilDato"));
		lblBeskrivelse.setText(ResourceUtils.getResource("label.beskrivelse"));
		lblKontraktspassord.setText(ResourceUtils.getResource("label.kontraktspassord"));
		lblKontraktspassordHashed.setText(ResourceUtils.getResource("label.kontraktspassordHashed"));
		cbxType.setPreferredSize(new Dimension(180, 21));
		cbxDistrikt.setPreferredSize(new Dimension(180, 21));
		txtVeinett.setPreferredSize(new Dimension(180, 21));
		txtVeinett.setEditable(false);
		txtAreal.setPreferredSize(new Dimension(180, 21));
		txtAreal.setEditable(false);
		txtKontraktsnr.setPreferredSize(new Dimension(180, 21));
		txtKontraktsnavn.setPreferredSize(new Dimension(180, 21));
		dtPckrDatoFra.setPreferredSize(new Dimension(180, 21));
		dtPckrDatoTil.setPreferredSize(new Dimension(180, 21));
		txtAreaBeskrivelse.setLineWrap(true);
		txtAreaBeskrivelse.setWrapStyleWord(true);
		scrlPaneBeskrivelse.setPreferredSize(new Dimension(180, 105));
		scrlPaneBeskrivelse.setMinimumSize(new Dimension(180, 105));
		
		txtKontraktspassord.setPreferredSize(new Dimension(180, 21));
		txtKontraktspassordHashed.setPreferredSize(new Dimension(180, 21));
		txtKontraktspassordHashed.setEditable(false);
		
		scrlPaneBeskrivelse.getViewport().add(txtAreaBeskrivelse, null);
		tblKontraktslogg.setPreferredScrollableViewportSize(new Dimension(0, 0));
		scrollPaneTbl.getViewport().add(tblKontraktslogg, null);
		pnlSideButtons.setLayout(gridBagLayout6);
		lblKontraktslogg.setText(ResourceUtils.getResource("label.kontraktslogg"));
		
		pnlTop.add(lblType, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 0), 0, 0));
		pnlTop.add(lblDistrikt, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 0), 0, 0));
		pnlTop.add(lblVeinett, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 0), 0, 0));
		pnlTop.add(lblAreal, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 19, 0), 0, 0));
		pnlTop.add(lblKontraktsnr, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlTop.add(lblKontraktsnavn, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlTop.add(lblDatoFra, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlTop.add(lblDatoTil, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 19, 0), 0, 0));
		pnlTop.add(lblBeskrivelse, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlTop.add(cbxType, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(cbxDistrikt, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(txtVeinett, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(txtAreal, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 19, 0), 0, 0));
		pnlTop.add(txtKontraktsnr, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(txtKontraktsnavn, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(dtPckrDatoFra, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(dtPckrDatoTil, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 19, 0), 0, 0));
		pnlTop.add(scrlPaneBeskrivelse, new GridBagConstraints(1, 4, 3, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 5, 0), 0, 0));
		
		pnlTop.add(lblKontraktspassord, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlTop.add(txtKontraktspassord, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));
		pnlTop.add(lblKontraktspassordHashed, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 19, 5, 0), 0, 0));
		pnlTop.add(txtKontraktspassordHashed, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 11, 5, 0), 0, 0));

		pnlMain.add(pnlTop, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 19, 0), 0, 0));
		pnlMain.add(lblKontraktslogg, new GridBagConstraints(1, 1, 2, 1, 0, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlBottom.add(scrollPaneTbl, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(pnlBottom, new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlSideButtons.add(btnNyLogg, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlSideButtons.add(btnVisLogg, new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(pnlSideButtons, new GridBagConstraints(2, 2, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 5, 0, 0), 0, 0));
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
