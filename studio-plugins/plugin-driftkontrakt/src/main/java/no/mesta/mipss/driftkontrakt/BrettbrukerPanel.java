package no.mesta.mipss.driftkontrakt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.mipssfield.Pdabruker;
import no.mesta.mipss.persistence.mipssfield.PdabrukerKontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class BrettbrukerPanel extends JPanel{

	private final DriftkontraktController controller;
	private JMipssBeanTable<PdabrukerKontrakt> tblPdabruker;
	private MipssBeanTableModel<PdabrukerKontrakt> mdlPdabruker;
	private BindingGroup bindingGroup = new BindingGroup();
	private JButton btnNy = new JButton();
	private JButton btnSlett = new JButton();
	private JButton btnEndre = new JButton();

	public BrettbrukerPanel(DriftkontraktController controller){
		this.controller = controller;
		
		initComponents();
		initGui();
		bind();
		
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		JScrollPane scrBruker = new JScrollPane(tblPdabruker);
		
		JPanel pnlBrukerButtons = new JPanel(new GridBagLayout());
		pnlBrukerButtons.add(btnNy, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlBrukerButtons.add(btnSlett, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlBrukerButtons.add(btnEndre, 	new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

		add(scrBruker, 			new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 0), 0, 0));
		add(pnlBrukerButtons, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 5, 5, 5), 0, 0));

		setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.brettBrukereIKontrakt")));

	}

	private void initComponents(){
		controller.registerChangableField("pdabrukerList");
		
		mdlPdabruker = new MipssBeanTableModel<PdabrukerKontrakt>("pdabrukerList", PdabrukerKontrakt.class, "signatur", "${pdabruker.navn}", "${pdabruker.tlfnummer}", "${pdabruker.epostAdresse}");
		bindingGroup.addBinding(BindingHelper.createbinding(controller, "currentDriftkontrakt", mdlPdabruker, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelPdabruker = new MipssRenderableEntityTableColumnModel(PdabrukerKontrakt.class, mdlPdabruker.getColumns());

		tblPdabruker = new JMipssBeanTable<PdabrukerKontrakt>(mdlPdabruker, columnModelPdabruker);
		tblPdabruker.setFillsViewportWidth(true);
		tblPdabruker.setDoWidthHacks(false);
		tblPdabruker.setDefaultRenderer(Pdabruker.class, new RenderableMipssEntityTableCellRenderer<Pdabruker>("navn"));
		
		btnNy.setAction(controller.getLeggTilPdabrukerAction());
		btnNy.setIcon(IconResources.ADD_ITEM_ICON);

		btnSlett.setAction(controller.getSlettPdabrukerAction(mdlPdabruker, tblPdabruker));
		btnSlett.setIcon(IconResources.REMOVE_ITEM_ICON);
		
		btnEndre.setAction(controller.getEndrePdabrukerAction(mdlPdabruker, tblPdabruker));
		btnEndre.setIcon(IconResources.EDIT_ICON);
		

	}
	
	private void bind(){
		bindingGroup.addBinding(BindingHelper.createbinding(controller, "${currentDriftkontrakt != null}", btnNy, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(tblPdabruker, "${noOfSelectedRows > 0}", btnSlett, "enabled"));
		bindingGroup.bind();
	}
}
