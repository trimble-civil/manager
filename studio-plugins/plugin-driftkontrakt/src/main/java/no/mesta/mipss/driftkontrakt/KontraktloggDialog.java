package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;

public class KontraktloggDialog extends JDialog {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlLogg = new JPanel();
    private JPanel pbnlButtons = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JButton btnLukk = new JButton();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    private JScrollPane scrollPaneLogg = new JScrollPane();
    private JTextArea txtAreaLogg = new JTextArea();
    private JLabel lblLogg = new JLabel();
    
    private DriftkontraktController driftkontraktController;
    
    public KontraktloggDialog(JFrame owner, DriftkontraktController driftkontraktController) {
    	super(owner);
    	
    	this.driftkontraktController = driftkontraktController;
    	
    	initGui();
    	initTextArea();
    	initButtons();
    	
    	setLocationRelativeTo(null);
    	setVisible(true);
    }
    
    private void initTextArea() {
    	txtAreaLogg.setEditable(false);
    	txtAreaLogg.setFocusable(true);
    	txtAreaLogg.setDocument(driftkontraktController.getKontraktloggDocument());
    }
    
    private void initButtons() {
    	btnLukk.setAction(new AbstractAction(ResourceUtils.getResource("button.lukk")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
    	});
    }
    
    private void initGui() {
        this.setSize(new Dimension(634, 579));
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlLogg.setLayout(gridBagLayout4);
        pbnlButtons.setLayout(gridBagLayout3);
        txtAreaLogg.setLineWrap(true);
        txtAreaLogg.setWrapStyleWord(true);
        lblLogg.setText(ResourceUtils.getResource("label.kontraktslogg"));
        scrollPaneLogg.getViewport().add(txtAreaLogg, null);
        pnlLogg.add(scrollPaneLogg, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        pnlLogg.add(lblLogg, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        pnlMain.add(pnlLogg, 
                    new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                           GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        pbnlButtons.add(btnLukk, 
                        new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, 
                                               GridBagConstraints.NONE, 
                                               new Insets(0, 0, 0, 0), 0, 0));
        pnlMain.add(pbnlButtons, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, 
                                           GridBagConstraints.HORIZONTAL, 
                                           new Insets(11, 0, 0, 0), 0, 0));
        this.add(pnlMain, 
                 new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(11, 11, 11, 11), 0, 0));
    }
}
