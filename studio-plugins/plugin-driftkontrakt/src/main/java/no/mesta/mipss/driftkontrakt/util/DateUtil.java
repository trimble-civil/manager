package no.mesta.mipss.driftkontrakt.util;

import java.util.Date;

public class DateUtil {
	public static boolean overlappingPeriods(Date period1Start, Date period1End, Date period2Start, Date period2End) {
		long l1 = period1End.getTime() - period2Start.getTime();
		long l2 = period2End.getTime() - period1Start.getTime();
		long l3 = period1End.getTime() - period1Start.getTime() + period2End.getTime() - period2Start.getTime();
		return l1 < l3 && l2 < l3;
	}
}
