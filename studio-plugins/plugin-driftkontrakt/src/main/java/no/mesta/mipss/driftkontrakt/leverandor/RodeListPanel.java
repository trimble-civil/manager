package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.LevProdtypeRodeTableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class RodeListPanel extends JPanel {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlRodeList = new JPanel();
    private GridBagLayout gridBagLayout6 = new GridBagLayout();
    private JScrollPane scrlPaneRoder = new JScrollPane();
    private JPanel pnlButtons = new JPanel();
    private GridBagLayout gridBagLayout7 = new GridBagLayout();
    private JButton btnLeggTilRode = new JButton();
    private JButton btnSlettRode = new JButton();
    private JLabel lblRoder = new JLabel();
    private JMipssBeanTable<LevProdtypeRodeTableObject> tblLevProdtypeRode;
    private MipssBeanTableModel<LevProdtypeRodeTableObject> leverandorModel;
    
    private LeverandorController leverandorController;
    private BindingGroup bindingGroup;
    
    public RodeListPanel(LeverandorController leverandorController) {
    	this.leverandorController = leverandorController;
    	
    	bindingGroup = new BindingGroup();
    	
    	initTable();
    	initButtons();
    	initGui();
    	
    	bindingGroup.bind();
    }
    
    private void initButtons() {
		btnLeggTilRode.setAction(leverandorController.getLeggTilRodeAction());
		btnLeggTilRode.setIcon(IconResources.ADD_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", btnLeggTilRode, "enabled"));
		
		btnSlettRode.setAction(leverandorController.getSlettKontaktAction(leverandorModel, tblLevProdtypeRode));
		btnSlettRode.setIcon(IconResources.REMOVE_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblLevProdtypeRode, "${noOfSelectedRows > 0}", btnSlettRode, "enabled"));
    }
    
	private void initTable() {
		leverandorController.registerChangableField("leverandor.levProdtypeRodeList");
		
		leverandorModel = new MipssBeanTableModel<LevProdtypeRodeTableObject>(LevProdtypeRodeTableObject.class, "rode", "prodtyperForGui", "fraDato", "tilDato");
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "levProdtypeRodeTableObjectList", leverandorModel, "entities", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(LevProdtypeRodeTableObject.class, leverandorModel.getColumns());
		
		tblLevProdtypeRode = new JMipssBeanTable<LevProdtypeRodeTableObject>(leverandorModel, columnModel);
		tblLevProdtypeRode.setFillsViewportWidth(true);
		tblLevProdtypeRode.setDoWidthHacks(false);
		
		tblLevProdtypeRode.setDefaultRenderer(Rode.class, new RenderableMipssEntityTableCellRenderer<Rode>());
	}
    
    private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlButtons.setLayout(gridBagLayout6);
        pnlButtons.setDoubleBuffered(false);
        pnlRodeList.setLayout(gridBagLayout7);
        lblRoder.setText(ResourceUtils.getResource("label.roder"));
        scrlPaneRoder.getViewport().add(tblLevProdtypeRode, null);
        tblLevProdtypeRode.setPreferredScrollableViewportSize(new Dimension(0, 0));
        pnlRodeList.add(scrlPaneRoder, 
                        new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                               new Insets(0, 0, 0, 0), 0, 0));
        pnlButtons.add(btnLeggTilRode, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 5, 0), 0, 0));
        pnlButtons.add(btnSlettRode, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        pnlRodeList.add(pnlButtons, 
                        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,GridBagConstraints.NORTH, GridBagConstraints.NONE, 
                                               new Insets(0, 5, 0, 0), 0, 0));
        this.add(lblRoder, 
                new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                       new Insets(0, 0, 5, 0), 0, 0));
        this.add(pnlRodeList, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 0, 0), 0, 0));
    }
}
