package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;

public class LeverandorPanel extends JPanel {
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
	private LeverandorDetaljPanel pnlLeverandorDetalj;
	private RodeListPanel pnlRodeList;
	
	private LeverandorController leverandorController;
	
	public LeverandorPanel(LeverandorController leverandorController) {
		this.leverandorController = leverandorController;
		initGui();
	}
	
	private void initGui() {
		pnlLeverandorDetalj = new LeverandorDetaljPanel(leverandorController);
		pnlRodeList = new RodeListPanel(leverandorController);
        this.setLayout(gridBagLayout1);
//        this.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("dialogTitle.leverandor")));
        this.add(pnlLeverandorDetalj, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
        this.add(pnlRodeList, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
	}
}
