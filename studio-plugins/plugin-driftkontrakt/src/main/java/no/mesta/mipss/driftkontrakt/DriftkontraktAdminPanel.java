package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.resources.images.IconResources;

public class DriftkontraktAdminPanel extends JPanel {
	private JPanel pnlMain = new JPanel();
	private JPanel pnlTabs = new JPanel();
	private JTabbedPane tbdPane = new JTabbedPane();
	private JPanel tabKontraktsdata = new JPanel();
	private JPanel tabStabOgProsjekt = new JPanel();
	private JPanel tabOppsett = new JPanel();
	private JComboBox cbxVelgKontrakt = new JComboBox();
	private JPanel tabByggherre = new JPanel();
	private JPanel tabFavoritter = new JPanel();
	private JPanel tabKontraktProsess = new JPanel();
	private JPanel tabAktivering = new JPanel();
	
    private DriftkontraktDataPanel pnlKontraktsdata;
	private StabOgProsjektTabPanel pnlStabOgProsjekt;
	private ByggherrePanel pnlByggherre;
	private DriftkontraktOppsettPanel pnlOppsett;
	private FavorittlisterPanel pnlFavorittlister;
	private KontraktProsessPanel pnlKontraktProsess;
	private AktiveringPanel pnlAktivering;

	public DriftkontraktAdminPanel(DriftkontraktController driftkontraktController) {
        pnlKontraktsdata = new DriftkontraktDataPanel(driftkontraktController);
        pnlStabOgProsjekt = new StabOgProsjektTabPanel(driftkontraktController);
        pnlByggherre = new ByggherrePanel(driftkontraktController);
        pnlOppsett = new DriftkontraktOppsettPanel(driftkontraktController);
        pnlFavorittlister = new FavorittlisterPanel(driftkontraktController);
        pnlKontraktProsess = new KontraktProsessPanel(driftkontraktController);
        pnlAktivering = new AktiveringPanel();
		initGui();
	}

	private void initGui() {
        this.setLayout(new GridBagLayout());
        pnlMain.setLayout(new GridBagLayout());
        pnlTabs.setLayout(new GridBagLayout());
        tabKontraktsdata.setLayout(new GridBagLayout());
        tabStabOgProsjekt.setLayout(new GridBagLayout());
        tabByggherre.setLayout(new GridBagLayout());
        tabOppsett.setLayout(new GridBagLayout());
        tabFavoritter.setLayout(new GridBagLayout());
        tabKontraktProsess.setLayout(new GridBagLayout());
        tabAktivering.setLayout(new GridBagLayout());
        cbxVelgKontrakt.setPreferredSize(new Dimension(180, 21));
        
        tabKontraktsdata.add(pnlKontraktsdata,   	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabStabOgProsjekt.add(pnlStabOgProsjekt, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabByggherre.add(pnlByggherre, 			 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabOppsett.add(pnlOppsett, 				 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabFavoritter.add(pnlFavorittlister, 	 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabKontraktProsess.add(pnlKontraktProsess, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabAktivering.add(pnlAktivering, 			new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        tbdPane.addTab(ResourceUtils.getResource("tab.kontraktsdata"), IconResources.DETALJER_SMALL_ICON , tabKontraktsdata);
        tbdPane.addTab(ResourceUtils.getResource("tab.stabOgProsjekt"), IconResources.ROLES_ICON, tabStabOgProsjekt);
        tbdPane.addTab(ResourceUtils.getResource("tab.byggherre"), IconResources.FORTRESS_ICON, tabByggherre);
        tbdPane.addTab(ResourceUtils.getResource("tab.oppsett"), IconResources.PREFERENCES_SMALL_ICON, tabOppsett);
        tbdPane.addTab(ResourceUtils.getResource("tab.kontraktProsess"), IconResources.PROSESS_16, tabKontraktProsess);
        tbdPane.addTab(ResourceUtils.getResource("tab.favoritter"), IconResources.BOOKMARK_EDIT, tabFavoritter);
        tbdPane.addTab(ResourceUtils.getResource("tab.aktiver"), IconResources.ACTIVATE_16, tabAktivering);
        
        pnlTabs.add(tbdPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        pnlMain.add(pnlTabs, new GridBagConstraints(0, 1, 2, 1, 1.0, 1000.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 0, 0, 0), 0, 0));
	}
}
