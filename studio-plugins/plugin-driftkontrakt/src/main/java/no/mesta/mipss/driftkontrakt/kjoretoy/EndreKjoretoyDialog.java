package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.netbeans.spi.wizard.Wizard;

public class EndreKjoretoyDialog extends JDialog {
	private DriftkontraktController driftkontraktController;
	
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	private JTabbedPane tbdPaneKjoretoy = new JTabbedPane();
	private Kjoretoy kjoretoy;
	private Wizard wizard;
	private JFrame parent;
	
	public EndreKjoretoyDialog(DriftkontraktController driftkontraktController, JFrame parent, Kjoretoy kjoretoy, Wizard wizard) {
		super(parent, ResourceUtils.getResource("dialogTitle.endreKjoretoy"), true);
		
		this.driftkontraktController = driftkontraktController;
		this.parent = parent;
		this.kjoretoy = kjoretoy;
		this.wizard = wizard;
		
		initGui();
		initTabbedPane();
		initButtons();
		
		setSize(new Dimension(800, 600));
	}
	
	public Kjoretoy showDialog() {
		setLocationRelativeTo(null);
		setVisible(true);
		return kjoretoy;
	}
	
	private void initTabbedPane() {
		String[] ids = wizard.getAllSteps();
		for(String id : ids) {
			JComponent jComponent = wizard.navigatingTo(id, null);
			String title = wizard.getStepDescription(id);
			tbdPaneKjoretoy.add(title, jComponent);
		}
	}
	public void setSelectedTabIndex(Integer tabIndex){
		tbdPaneKjoretoy.setSelectedIndex(tabIndex);
	}
	private void initButtons() {
		btnOk.setAction(new AbstractAction(ResourceUtils.getResource("button.lagre")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(kjoretoy.isComplete()) {
					kjoretoy = driftkontraktController.oppdaterKjoretoy(kjoretoy);
					dispose();
				} else {
					MipssDialogue dialogue = new MipssDialogue(parent, IconResources.INFO_ICON, 
							ResourceUtils.getResource("dialogTitle.uferdigKjoretoy"), 
							ResourceUtils.getResource("message.uferdigKjoretoy"),
							MipssDialogue.Type.INFO, 
							new MipssDialogue.Button[] { MipssDialogue.Button.OK });
					dialogue.askUser();
				}
			}
		});
		btnAvbryt.setAction(new AbstractAction(ResourceUtils.getResource("button.avbryt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}
	
	private void initGui() {
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(tbdPaneKjoretoy, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 0, 11), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 0, 11, 0), 0, 0));
	}
}
