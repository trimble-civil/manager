package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.FavorittProsess;
import no.mesta.mipss.persistence.mipssfield.Favorittliste;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class FavorittlisterPanel extends JPanel {
	private enum Visningsmodus {LASTER, VIS_TRE, SKJUL_TRE};
	
	private JPanel pnlMain = new JPanel();
	private JPanel pnlFavorittListe = new JPanel();
	private JPanel pnlProsessTre = new JPanel();
	private JPanel pnlForhaandsvisning = new JPanel();
	private JMipssBeanTable<Favorittliste> tblFavorittliste;
	private JScrollPane scrlFavorittliste = new JScrollPane();
	private JList lstForhaandsvisning = new JList();
	private JScrollPane scrlForhaandsvisning = new JScrollPane();
	private JTree treeProsess = new JTree();
	private JScrollPane scrlProsess = new JScrollPane();
	private JLabel lblFavorittliste = new JLabel();
	private JPanel pnlButtons = new JPanel();
	private JButton btnNy = new JButton();
	private JButton btnEndre = new JButton();
	private JButton btnSlett = new JButton();
	private JLabel lblIngenFavorittlisteValgt = new JLabel();
	private JLabel lblIngenFavorittlisteValgtKopi = new JLabel();
	private JXBusyLabel lblBusy = new JXBusyLabel();
	private JXBusyLabel lblBusyKopi = new JXBusyLabel();
	
	private DriftkontraktController driftkontraktController;
	private FavorittlisteRecursiveProsessTreeModel treeModelProsess;
	private BindingGroup bindingGroup;
	private MipssBeanTableModel<Favorittliste> favorittlisteModel;
	
	public FavorittlisterPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		treeModelProsess = new FavorittlisteRecursiveProsessTreeModel(treeProsess, driftkontraktController);
		
		bindingGroup = new BindingGroup();
		initLabels();
		initTree();
		initTables();
		initLists();
		initButtons();
		initGui();
		bindingGroup.bind();
		
		driftkontraktController.addPropertyChangeListener("currentDriftkontrakt", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				byttKontrakt();
			}
		});
	}
	
	private void byttKontrakt() {
		Driftkontrakt driftkontrakt = driftkontraktController.getCurrentDriftkontrakt();
		if(driftkontrakt != null) {
			List<Prosess> prosess = driftkontraktController.getProsesserForKontrakt(driftkontrakt.getId());
			treeModelProsess.setProsesser(prosess);
		} else {
			treeModelProsess.setProsesser(null);
		}
		setVisProsesseditering(Visningsmodus.SKJUL_TRE);
	}
	
	public synchronized void setVisProsesseditering(Visningsmodus modus) {
		switch (modus) {
		case LASTER:
			lblBusy.setBusy(true);
			lblBusyKopi.setBusy(true);
			scrlProsess.setViewportView(lblBusy);
			scrlForhaandsvisning.setViewportView(lblBusyKopi);
			break;
		case SKJUL_TRE:
			lblBusy.setBusy(false);
			lblBusyKopi.setBusy(false);
			scrlProsess.setViewportView(lblIngenFavorittlisteValgt);
			scrlForhaandsvisning.setViewportView(lblIngenFavorittlisteValgtKopi);
			break;
		case VIS_TRE:
			lblBusy.setBusy(false);
			lblBusyKopi.setBusy(false);
			scrlProsess.setViewportView(treeProsess);
			scrlForhaandsvisning.setViewportView(lstForhaandsvisning);
			break;
		default:
			break;
		}
	}
	
	private void initTree() {
		treeProsess.setModel(treeModelProsess);
	}
	
	private void initLists() {
		lstForhaandsvisning.setModel(treeModelProsess);
	}

	private void initTables() {
		driftkontraktController.registerChangableField("favorittlisteList");
		
		favorittlisteModel = new MipssBeanTableModel<Favorittliste>("favorittlisteList", Favorittliste.class, "navn", "beskrivelse");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", favorittlisteModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelFavorittliste = new MipssRenderableEntityTableColumnModel(Favorittliste.class, favorittlisteModel.getColumns());
		
		tblFavorittliste = new JMipssBeanTable<Favorittliste>(favorittlisteModel, columnModelFavorittliste);
		tblFavorittliste.setFillsViewportWidth(true);
		tblFavorittliste.setDoWidthHacks(false);
		tblFavorittliste.setPreferredScrollableViewportSize(new Dimension(0, 0));
		
		tblFavorittliste.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() >= 2) {
					Action a = driftkontraktController.getEndreFavorittlisteAction(favorittlisteModel, tblFavorittliste);
					a.actionPerformed(null);
				}
			}
		});
		
		tblFavorittliste.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int[] selectedRows = tblFavorittliste.getSelectedRows();
				if(selectedRows.length == 1) {
					final int selectedRow = tblFavorittliste.convertRowIndexToModel(selectedRows[0]);
					if(selectedRow >= 0 && selectedRow < tblFavorittliste.getRowCount()) {
						final SwingWorker w = new SwingWorker(){
							@Override
							protected Object doInBackground() throws Exception {
								setVisProsesseditering(Visningsmodus.LASTER);
								Favorittliste favorittliste = favorittlisteModel.getEntities().get(selectedRow);
								if(!favorittliste.equals(treeModelProsess.getFavorittliste())) {
									if(!favorittliste.getLagreFavorittProsesser()) {
										List<FavorittProsess> list = driftkontraktController.getFavorittProsessListForFavorittliste(favorittliste.getId());
										favorittliste.setFavorittProsessList(list);
										favorittliste.setLagreFavorittProsesser(Boolean.TRUE);
									}
									treeModelProsess.setFavorittliste(favorittliste);
								}
								return null;
							}
							@Override
							protected void done(){
								setVisProsesseditering(Visningsmodus.VIS_TRE);
							}
				    	};
				    	w.execute();
				    	new Thread(){
				    		public void run(){
				    			try {
									w.get();
								} catch (Exception e) {
									e.printStackTrace();
								} 
				    		}
				    	}.start();
					} else {
						setVisProsesseditering(Visningsmodus.SKJUL_TRE);
						treeModelProsess.setFavorittliste(null);
					}
				} else {
					setVisProsesseditering(Visningsmodus.SKJUL_TRE);
					treeModelProsess.setFavorittliste(null);
				}
			}
		});
		
//		bindingGroup.addBinding(BindingHelper.createbinding(tblFavorittliste, "${noOfSelectedRows == 1}", this, "visProsesseditering", BindingHelper.READ));
	}
	
	private void initLabels() {
		lblFavorittliste.setText(ResourceUtils.getResource("label.favorittliste"));
		lblIngenFavorittlisteValgt.setText(ResourceUtils.getResource("label.velgFavorittliste"));
		lblIngenFavorittlisteValgtKopi.setText(lblIngenFavorittlisteValgt.getText());
	}
	
	private void initButtons() {
		btnNy.setAction(driftkontraktController.getNyFavorittlisteAction());
		btnEndre.setAction(driftkontraktController.getEndreFavorittlisteAction(favorittlisteModel, tblFavorittliste));
		btnSlett.setAction(driftkontraktController.getSlettFavorittlisteAction(favorittlisteModel, tblFavorittliste));
		
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnNy, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(tblFavorittliste, "${noOfSelectedRows > 0}", btnSlett, "enabled"));
		bindingGroup.addBinding(BindingHelper.createbinding(tblFavorittliste, "${noOfSelectedRows == 1}", btnEndre, "enabled"));
	}
	
	private void initGui() {
		pnlProsessTre.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgProsesser")));
		pnlForhaandsvisning.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.forhaandsvisListe")));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnNy, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnEndre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnSlett, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		
		scrlFavorittliste.setViewportView(tblFavorittliste);
		pnlFavorittListe.setPreferredSize(new Dimension(100, 200));
		pnlFavorittListe.setMinimumSize(new Dimension(100, 200));
		pnlFavorittListe.setLayout(new GridBagLayout());
		pnlFavorittListe.add(lblFavorittliste, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlFavorittListe.add(scrlFavorittliste, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
		pnlFavorittListe.add(pnlButtons, new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
		
		scrlProsess.setViewportView(treeProsess);
		pnlProsessTre.setLayout(new GridBagLayout());
		pnlProsessTre.add(scrlProsess, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		scrlForhaandsvisning.setViewportView(lstForhaandsvisning);
		pnlForhaandsvisning.setLayout(new GridBagLayout());
		pnlForhaandsvisning.add(scrlForhaandsvisning, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(pnlFavorittListe, 	 new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 11, 0), 0, 0));
		pnlMain.add(pnlProsessTre, 		 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 11), 0, 0));
		pnlMain.add(pnlForhaandsvisning, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
