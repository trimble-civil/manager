package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.datafangsutstyr.Dfuindivid;
import no.mesta.mipss.persistence.datafangsutstyr.Dfumodell;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXTable;

public class DfuKontraktListPanel extends JPanel {
	private JPanel pnlMain = new JPanel();
	private JScrollPane scrlPaneDfuKontraktList = new JScrollPane();
	private JMipssBeanTable<DfuKontrakt> tblDfuKontraktList;
	private JButton btnEndreDfuKontrakt = new JButton();
	
	private MipssBeanTableModel<DfuKontrakt> dfuKontraktModel;
	
	private DriftkontraktController driftkontraktController;
	private BindingGroup bindingGroup;
	
	public DfuKontraktListPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		
		bindingGroup = new BindingGroup();
		
		initTable();
		initButtons();
		initGui();
		
		bindingGroup.bind();
	}
	
	private void initTable() {
		driftkontraktController.registerChangableField("dfuKontraktListGyldig");
		
		dfuKontraktModel = new MipssBeanTableModel<DfuKontrakt>("dfuKontraktList", DfuKontrakt.class, "dfuId", "modelLeverandor", "modelNavn", "navn", "fraDato", "tilDato", "sisteDfuStatus");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", dfuKontraktModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelDfuKontrakt = new MipssRenderableEntityTableColumnModel(DfuKontrakt.class, dfuKontraktModel.getColumns());
		
		tblDfuKontraktList = new JMipssBeanTable<DfuKontrakt>(dfuKontraktModel, columnModelDfuKontrakt);
		tblDfuKontraktList.setFillsViewportWidth(true);
		tblDfuKontraktList.setDoWidthHacks(false);
		tblDfuKontraktList.setDefaultRenderer(Dfuindivid.class, new RenderableMipssEntityTableCellRenderer<Dfuindivid>());
		tblDfuKontraktList.setDefaultRenderer(Dfumodell.class, new RenderableMipssEntityTableCellRenderer<Dfumodell>());
		
		tblDfuKontraktList.getColumn(0).setMaxWidth(75);
		
		tblDfuKontraktList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tblDfuKontraktList.addMouseListener(new MouseAdapter() {
			/** {@inheritDoc} */
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && tblDfuKontraktList.rowAtPoint(e.getPoint()) != -1) {
					if (e.getClickCount() == 2) {
						driftkontraktController.getEndreDfuKontraktAction(dfuKontraktModel, tblDfuKontraktList).actionPerformed(null);
					}
				}
			}
		});
	}
	
	private void initButtons() {
		bindingGroup.addBinding(BindingHelper.createbinding(tblDfuKontraktList, "${noOfSelectedRows == 1}", btnEndreDfuKontrakt, "enabled"));
		
		btnEndreDfuKontrakt.setAction(driftkontraktController.getEndreDfuKontraktAction(dfuKontraktModel, tblDfuKontraktList));
		btnEndreDfuKontrakt.setIcon(IconResources.DETALJER_SMALL_ICON);
	}
	
	private void initGui() {
		scrlPaneDfuKontraktList.setViewportView(tblDfuKontraktList);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrlPaneDfuKontraktList, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlMain.add(btnEndreDfuKontrakt, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
