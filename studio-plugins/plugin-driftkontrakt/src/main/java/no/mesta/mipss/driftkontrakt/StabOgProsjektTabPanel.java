package no.mesta.mipss.driftkontrakt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;

@SuppressWarnings("serial")
public class StabOgProsjektTabPanel extends JPanel{

	private StabOgProsjektPanel pnlProsjekt;
	private BrukerPanel pnlBrukere;
	private JTabbedPane tabMain;
	private final DriftkontraktController controller;
	
	public StabOgProsjektTabPanel(DriftkontraktController controller){
		this.controller = controller;
		initComponents();
		initGui();
	}

	private void initComponents() {
		tabMain = new JTabbedPane();
		
		pnlBrukere = new BrukerPanel(controller);
		pnlProsjekt = new StabOgProsjektPanel(controller);
		
		tabMain.addTab(ResourceUtils.getResource("tab.prosjekt"), pnlProsjekt);
		tabMain.addTab(ResourceUtils.getResource("tab.brukere"), pnlBrukere);
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		
		add(tabMain, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

		
	}

	
}
