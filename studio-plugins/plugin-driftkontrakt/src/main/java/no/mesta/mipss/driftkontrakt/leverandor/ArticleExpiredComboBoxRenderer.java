package no.mesta.mipss.driftkontrakt.leverandor;

import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import no.mesta.mipss.ui.table.MipssTableComboBoxRenderer;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ArticleExpiredComboBoxRenderer extends MipssTableComboBoxRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (!table.isCellEditable(row, column) && comp instanceof JLabel && table.getModel() instanceof ArtikkelProdtypeTableModel) {
            ArtikkelProdtypeTableModel model = (ArtikkelProdtypeTableModel) table.getModel();
            ArtikkelProdtype article = model.get(table.convertRowIndexToModel(row));

            if (article.isUtgatt()) {
                JLabel label = (JLabel) comp;
                label.setForeground(Color.GRAY);
                label.setBorder(new CompoundBorder(new EmptyBorder(0, 1, 0, 0), label.getBorder()));

                Box cell = Box.createHorizontalBox();
                cell.setOpaque(true);
                cell.setBackground(Color.LIGHT_GRAY);
                cell.add(label);
                cell.add(Box.createHorizontalGlue());

                return cell;
            }
        }

        return comp;
    }

}