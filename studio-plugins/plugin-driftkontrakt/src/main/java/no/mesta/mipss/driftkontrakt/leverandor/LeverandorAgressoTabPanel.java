package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;

@SuppressWarnings("serial")
public class LeverandorAgressoTabPanel extends JPanel{

	private final LeverandorController leverandorController;
	
	private LeverandorPanel leverandorPanel;
	private AgressoKontraktPanel agressoPanel;
	
	private JTabbedPane tab;
	
	public LeverandorAgressoTabPanel(LeverandorController leverandorController){
		this.leverandorController = leverandorController;
		initComponents();
		initGui();
	}

	private void initComponents() {
		tab = new JTabbedPane();
		leverandorPanel = new LeverandorPanel(leverandorController);
		agressoPanel = new AgressoKontraktPanel(leverandorController);
		
		tab.addTab(ResourceUtils.getResource("dialogTitle.leverandor"), leverandorPanel);
		tab.addTab(ResourceUtils.getResource("dialogTitle.leverandorkontrakter"), agressoPanel);
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		add(tab, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		
	}
}
