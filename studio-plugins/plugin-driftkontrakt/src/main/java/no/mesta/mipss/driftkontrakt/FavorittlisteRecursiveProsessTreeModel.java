package no.mesta.mipss.driftkontrakt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JTree;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import no.mesta.mipss.persistence.mipssfield.FavorittProsess;
import no.mesta.mipss.persistence.mipssfield.Favorittliste;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.ui.tree.RecursiveProsessTreeModel;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class FavorittlisteRecursiveProsessTreeModel extends RecursiveProsessTreeModel implements ListModel{
	private DriftkontraktController driftkontraktController;
	private Set<ProsessWrapper> valgteProsesserSet;
	private Favorittliste favorittliste;
	private List<ListDataListener> listDataListenerList = new ArrayList<ListDataListener>();


	public FavorittlisteRecursiveProsessTreeModel(JTree tree, DriftkontraktController driftkontraktController) {
		super(tree, false, false, false);
		this.driftkontraktController = driftkontraktController;
//		tree.setRootVisible(false);
	}
	public Favorittliste getFavorittliste() {
		return favorittliste;
	}

	public synchronized void setFavorittliste(Favorittliste favorittliste) {
		this.favorittliste = favorittliste;
		initSeleksjon();
		fireListDataChanged();
		expandFirstLeaves();
	}
	@Override
	protected void nodeSelected(Prosess node, boolean selected) {
		prosessEnabled(node, selected);
		fireListDataChanged();
	}
	@Override
	public void setProsesser(List<Prosess> prosesser) {
		super.setProsesser(prosesser);
		initSeleksjon();
	}
	
	
	private void initSeleksjon() {
		valgteProsesserSet = new HashSet<ProsessWrapper>();		
		if(favorittliste != null && favorittliste.getFavorittProsessList() != null) {
			for(FavorittProsess favorittProsess : favorittliste.getFavorittProsessList()) {
				valgteProsesserSet.add(new ProsessWrapper(favorittProsess.getProsess()));
			}
		}
		
		//Looper på noder i treet. Slår opp prosess mot nevnte sett og "velger" dem som får treff i settet.
		initTreeNodes(rootNode, valgteProsesserSet);
	}
	public void expandFirstLeaves() {
		  for (int i = 0;i<rootNode.getChildCount();i++){
			  DefaultMutableTreeNode childAt = (DefaultMutableTreeNode)rootNode.getChildAt(i);
			  tree.expandPath(new TreePath(childAt.getPath()));
		  }
	}
	private void prosessEnabled(Prosess prosess, boolean selected) {
		if(favorittliste != null) {		
			//Dersom dette er en ny prosess må den legges til
			if(selected) {
				if(prosess != null) {
					ProsessWrapper prosessWrapper;
					FavorittProsess nyFavorittProsess = new FavorittProsess();
					nyFavorittProsess.setFavorittliste(favorittliste);
					nyFavorittProsess.setProsess((Prosess)prosess);
					prosessWrapper = new ProsessWrapper((Prosess)prosess);
					if(!valgteProsesserSet.contains(prosessWrapper)) {
						favorittliste.getFavorittProsessList().add(nyFavorittProsess);
						valgteProsesserSet.add(prosessWrapper);
					}
				}
			} else {
				if(prosess != null) {
					for(int i = 0; i < favorittliste.getFavorittProsessList().size(); i++) {
						FavorittProsess favorittProsess = favorittliste.getFavorittProsessList().get(i);
						ProsessWrapper prosessWrapper;
						
						if(prosess.equals(favorittProsess.getProsess())) {
							favorittliste.getFavorittProsessList().remove(i);
							prosessWrapper = new ProsessWrapper(prosess);
							valgteProsesserSet.remove(prosessWrapper);
							break;
						}
					}
				}
			}
			
			driftkontraktController.setOverriddenChange(Boolean.TRUE);
		}
	}
	private boolean initTreeNodes(TreeNode treeNode, Set<ProsessWrapper> valgteProsesser) {
		boolean denneNodeEkspandert = false;
		if(treeNode instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treeNode;
            Object userObject = defaultMutableTreeNode.getUserObject();
            if(userObject instanceof SelectableProsessNode) {
            	SelectableProsessNode selectableProsessNode =(SelectableProsessNode)userObject;
            	Prosess prosess = selectableProsessNode.getProsess();
            	ProsessWrapper prosessWrapper = new ProsessWrapper(prosess);
				
            	if(valgteProsesser.contains(prosessWrapper)) {
            		selectableProsessNode.setSelected(true);
            		List<Object> list = new LinkedList<Object>();
            		//list.add(treeNode);
            		TreeNode parent = treeNode.getParent();
            		while(parent != null) {
            			list.add(0, parent);
            			parent = parent.getParent();
            		}
            		tree.expandPath(new TreePath(list.toArray()));
            		denneNodeEkspandert = true;
            	} else {
            		selectableProsessNode.setSelected(false);
            	}
            }
		}
		
		boolean undernodeEkspandert = false;
		for(int i = 0; i < treeNode.getChildCount(); i++) {
			if(initTreeNodes(treeNode.getChildAt(i), valgteProsesser)) {
				undernodeEkspandert = true;
			}
		}
		
		if(!undernodeEkspandert) {
    		List<Object> list = new LinkedList<Object>();
    		list.add(treeNode);
    		TreeNode parent = treeNode.getParent();
    		while(parent != null) {
    			list.add(0, parent);
    			parent = parent.getParent();
    		}
    		
    		if(list.size() > 0) {
    			tree.collapsePath(new TreePath(list.toArray()));
    		}
		}
		
		return denneNodeEkspandert || undernodeEkspandert;
	}
	@Override
	public void removeListDataListener(ListDataListener l) {
		listDataListenerList.remove(l);
	}
	@Override
	public void addListDataListener(ListDataListener l) {
		listDataListenerList.add(l);
	}
	private void fireListDataChanged() {
		ListDataEvent e = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize());
		for(ListDataListener l : listDataListenerList) {
			l.contentsChanged(e);
		}
	}
	private class ProsessWrapper {
		private Prosess prosess;
		
		public ProsessWrapper(Prosess prosess) {
			this.prosess = prosess;
		}
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			if (o == this) {
				return true;
			}
			if (!(o instanceof ProsessWrapper)) {
				return false;
			}
			ProsessWrapper other = (ProsessWrapper) o;
			EqualsBuilder builder = new EqualsBuilder();
			builder.append(prosess, other.prosess);
			return builder.isEquals();
		}
		
		@Override
		public int hashCode() {
			return new HashCodeBuilder(199, 93).append(prosess).toHashCode();
		}
	}
	private boolean isSelectedProsess(TreeNode treeNode) {
		if(treeNode instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) treeNode;
            Object userObject = defaultMutableTreeNode.getUserObject();
            if(userObject instanceof SelectableProsessNode) {
            	SelectableProsessNode selectableProsessNode =(SelectableProsessNode)userObject;
            	if(selectableProsessNode.getSelected()) {
            		return true;
            	}
            }
		}
		return false;
	}
	private int subTreeSize(TreeNode t) {
		int size = isSelectedProsess(t) ? 1 : 0;
		
		for(int i = 0; i < t.getChildCount(); i++) {
			size += subTreeSize(t.getChildAt(i));
		}
		
		return size;
	}
	private TreeNode selectedNodeAt(TreeNode t, int index, int cumulativIndex) {
		if(isSelectedProsess(t)) {
			if(index == cumulativIndex) {
				return t;
			}
			cumulativIndex++;
		}
		TreeNode res = null;
		for(int i = 0; i < t.getChildCount(); i++) {
			res = selectedNodeAt(t.getChildAt(i), index, cumulativIndex);
			if(res != null) {
				break;
			} else {
				cumulativIndex += subTreeSize(t.getChildAt(i));
			}
		}
		return res;
	}
	@Override
	public Object getElementAt(int index) {
		TreeNode node = selectedNodeAt(rootNode, index, 0);
		String ret = "#";
		
		if(node instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) node;
            Object userObject = defaultMutableTreeNode.getUserObject();
            if(userObject instanceof SelectableProsessNode) {
            	SelectableProsessNode selectableProsessNode =(SelectableProsessNode)userObject;
            	ret = selectableProsessNode.getText();
            }
		}
		
		return ret; 
	}

	@Override
	public int getSize() {		
		return subTreeSize(rootNode);
	}
}
