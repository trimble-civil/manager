package no.mesta.mipss.driftkontrakt;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.JMipssContractPicker;

import org.jdesktop.swingx.JXBusyLabel;

public class DriftkontraktAdminModulePanel extends JPanel {
	private static final String BUSY_KEY = "busy";
	
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlPicker = new JPanel();
    private JPanel pnlContent = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private CardLayout cardLayout = new CardLayout();
    private JPanel pnlBottomButtons = new JPanel();
    private GridBagLayout gridBagLayout6 = new GridBagLayout();
    private JButton btnLagre = new JButton();
    private JPanel busyPanel = new JPanel();
    private JXBusyLabel busyLabel = new JXBusyLabel();
    private String busyText;
	
	private JMipssContractPicker contractPicker;
	private String currentKey;
	
	public DriftkontraktAdminModulePanel(JMipssContractPicker contractPicker) {
		this.contractPicker = contractPicker;
		
		busyText = ResourceUtils.getResource("label.henterKontrakt");
		
		initGui();
		registrerPanel(busyPanel, BUSY_KEY);
	}
	
	public void registrerPanel(JPanel panel, String key) {
		pnlContent.add(panel, key);
	}
	
	public void visPanel(String key) {
		currentKey = key;
		if(!busyLabel.isBusy()) {
			cardLayout.show(pnlContent, key);
		}
	}
	
	public void setBusy(boolean busy, boolean visTekst) {
		busyLabel.setBusy(busy);
		busyLabel.setText(visTekst ? busyText : "");
		
		if(busy) {
			contractPicker.setEnabled(false);
			cardLayout.show(pnlContent, BUSY_KEY);
			btnLagre.setVisible(false);
		} else {
			contractPicker.setEnabled(true);
			cardLayout.show(pnlContent, currentKey);
			btnLagre.setVisible(true);
		}
	}
	
	public void initLagreButton(DriftkontraktController driftkontraktController) {
		btnLagre.setAction(driftkontraktController.getLagreAction(btnLagre));
		btnLagre.setIcon(IconResources.SAVE_ICON);
	}
	
	private void initGui() {
		busyPanel.setLayout(new GridBagLayout());
		busyPanel.add(busyLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
        this.setLayout(gridBagLayout1);
        pnlBottomButtons.setLayout(gridBagLayout6);
        pnlPicker.setLayout(gridBagLayout2);
        pnlContent.setLayout(cardLayout);
        pnlPicker.add(contractPicker, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
        pnlBottomButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlPicker, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 0, 11), 0, 0));
        this.add(pnlContent, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlBottomButtons, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 11, 11), 0, 0));
	}
}
