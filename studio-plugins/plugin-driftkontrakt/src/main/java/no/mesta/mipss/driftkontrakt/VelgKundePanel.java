package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.driftkontrakt.bean.KundeQueryFilter;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.KundeView;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.SokPanel;

public class VelgKundePanel extends AbstractRelasjonPanel<Long> {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    
    private SokPanel<KundeView> sokPanel;
	
	public VelgKundePanel(DriftkontraktController driftkontraktController) {
		sokPanel = new SokPanel<KundeView>(KundeView.class, 
											new KundeQueryFilter(), 
											driftkontraktController, 
											"getKundeViewList", 
											DriftkontraktController.class,
											false,
											"kundeNr", 
											"kundeNavn");
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension getDialogSize() {
		return new Dimension(400,500);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getNyRelasjon() {
		KundeView selectedKundeView = sokPanel.getSelectedEntity();
		return selectedKundeView != null ? Long.valueOf(selectedKundeView.getKundeNr()) : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.velgKunde");
	}
	
	private void updateComplete() {
		setComplete(Boolean.valueOf(sokPanel.getSelectedEntity() != null));
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlMain.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
