package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class StabOgProsjektPanel extends JPanel {
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
	private JPanel pnlMain = new JPanel();
	private GridBagLayout gridBagLayout2 = new GridBagLayout();
	private JMipssBeanTable<KontraktProsjekt> tblProsjekt;
	private JScrollPane scrlProsjekt = new JScrollPane();
	private JPanel pnlProsjektButtons = new JPanel();
	private GridBagLayout gridBagLayout4 = new GridBagLayout();
	private JButton btnNyProsjekt = new JButton();
	private JButton btnSlettProsjekt = new JButton();
	private JButton btnSettStandardProsjekt = new JButton();
	private JPanel pnlProsjekt = new JPanel();
	private KontaktListPanel kontaktListPanel;

	private MipssBeanTableModel<KontraktProsjekt> prosjektModel;
	private DriftkontraktController driftkontraktController;
	private BindingGroup bindingGroup;

//	private StabPanel pnlStab;
	
	public StabOgProsjektPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		kontaktListPanel = new KontaktListPanel(driftkontraktController);

		bindingGroup = new BindingGroup();

		initTables();
		initButtons();
		initGui();

		bindingGroup.bind();
	}

	private void initTables() {

		prosjektModel = new MipssBeanTableModel<KontraktProsjekt>("prosjektKontraktList", KontraktProsjekt.class, "prosjektNr", "StandardFlaggBoolean");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", prosjektModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModelProsjekt = new MipssRenderableEntityTableColumnModel(KontraktProsjekt.class, prosjektModel.getColumns());

		tblProsjekt = new JMipssBeanTable<KontraktProsjekt>(prosjektModel, columnModelProsjekt);
		tblProsjekt.setFillsViewportWidth(true);
		tblProsjekt.setDoWidthHacks(false);
	}

	private void initButtons() {
		btnNyProsjekt.setAction(driftkontraktController.getLeggTilProsjektAction());
		btnNyProsjekt.setIcon(IconResources.ADD_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnNyProsjekt, "enabled"));

		btnSettStandardProsjekt.setAction(driftkontraktController.getSettStandardKontraktProsjektAction(prosjektModel, tblProsjekt));
		btnSettStandardProsjekt.setIcon(IconResources.OK2_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblProsjekt, "${noOfSelectedRows == 1}", btnSettStandardProsjekt, "enabled"));

		btnSlettProsjekt.setAction(driftkontraktController.getSlettKontraktProsjektAction(prosjektModel, tblProsjekt));
		btnSlettProsjekt.setIcon(IconResources.REMOVE_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblProsjekt, "${noOfSelectedRows > 0}", btnSlettProsjekt, "enabled"));

	}

	private void initGui() {
		this.setLayout(gridBagLayout1);
		pnlMain.setLayout(gridBagLayout2);
		pnlProsjekt.setLayout(new GridBagLayout());
		
		pnlProsjektButtons.setLayout(gridBagLayout4);
		tblProsjekt.setPreferredScrollableViewportSize(new Dimension(0, 0));
		scrlProsjekt.getViewport().add(tblProsjekt, null);
		pnlProsjekt.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.prosjektliste")));

		pnlProsjekt.add(scrlProsjekt, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 0), 0, 0));
//		pnlStab = new StabPanel(driftkontraktController);
		pnlProsjektButtons.add(btnNyProsjekt, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlProsjektButtons.add(btnSettStandardProsjekt, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlProsjektButtons.add(btnSlettProsjekt, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		pnlProsjekt.add(pnlProsjektButtons, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlMain.add(kontaktListPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
//		pnlMain.add(pnlStab, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(pnlProsjekt, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
