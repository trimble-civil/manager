package no.mesta.mipss.driftkontrakt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BrukerPanel extends JPanel{

	private final DriftkontraktController driftkontraktController;
	private StabPanel pnlStab;
	private BrettbrukerPanel pnlBrukere;
	
	public BrukerPanel(DriftkontraktController driftkontraktController){
		this.driftkontraktController = driftkontraktController;
		initComponents();
		initGui();
	}
	
	private void initComponents(){
		pnlStab = new StabPanel(driftkontraktController);
		pnlBrukere = new BrettbrukerPanel(driftkontraktController);
	}
	
	private void initGui(){
		setLayout(new GridBagLayout());
		add(pnlStab, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(11, 0, 0, 5), 0, 0));
		add(pnlBrukere, new GridBagConstraints(1, 0, 1, 1, 0.5, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(11, 0, 0, 0), 0, 0));

	}
}
