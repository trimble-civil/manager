package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class EndreKjoretoygrupperingDialog extends JDialog{
	private JTextField fieldNavn;
	private JTextArea textBeskrivelse;
	private JButton btnOk;
	private JButton btnAvbryt;
	private boolean cancelled = true;
	private Kjoretoygruppe kjoretoygruppe;
	
	public EndreKjoretoygrupperingDialog(DriftkontraktController controller, Kjoretoygruppe kjoretoygruppe){
		super(controller.getDriftkontraktAdminModule().getLoader().getApplicationFrame(), true);
		this.kjoretoygruppe = kjoretoygruppe;
		initGui();
		if (kjoretoygruppe==null){
			this.kjoretoygruppe = new Kjoretoygruppe();
			this.kjoretoygruppe.setDriftkontrakt(controller.getCurrentDriftkontrakt());
			this.kjoretoygruppe.setOpprettetAv(controller.getDriftkontraktAdminModule().getLoader().getLoggedOnUserSign());
			this.kjoretoygruppe.setOpprettetDato(Clock.now());
		}
		fieldNavn.setText(this.kjoretoygruppe.getNavn());
		textBeskrivelse.setText(this.kjoretoygruppe.getBeskrivelse());
	}
	

	private void initGui() {
		fieldNavn = new JTextField();
		textBeskrivelse = new JTextArea();
		
		btnOk = new JButton(getOkAction());
		btnAvbryt = new JButton(getAvbrytAction());
		
		JLabel lblNavn = new JLabel(ResourceUtils.getResource("label.gruppenavn"));
		
		JPanel pnlBeskrivelse = new JPanel(new BorderLayout());
		pnlBeskrivelse.add(new JScrollPane(textBeskrivelse));
		pnlBeskrivelse.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("label.beskrivelse")));
		
		setLayout(new GridBagLayout());
		
		JPanel pnlButton = new JPanel(new GridBagLayout());
		pnlButton.add(btnOk, 		new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 5), 0, 0));
		pnlButton.add(btnAvbryt, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		add(lblNavn,   		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 5, 5), 0, 0));
		add(fieldNavn, 	  	new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 10), 0, 0));
		add(pnlBeskrivelse, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		add(pnlButton, 		new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		
	}
	
	private Action getOkAction(){
		return new AbstractAction("Lagre", IconResources.SAVE_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = false;
				kjoretoygruppe.setNavn(fieldNavn.getText());
				kjoretoygruppe.setBeskrivelse(textBeskrivelse.getText());
				dispose();
			}
		};
	}
	
	private Action getAvbrytAction(){
		return new AbstractAction("Avbryt", IconResources.BUTTON_CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = true;
				dispose();
			}
		};
	}
	
	public Kjoretoygruppe getKjoretoygruppe(){
		return kjoretoygruppe;
	}

	public boolean isCancelled(){
		return cancelled;
	}

}
