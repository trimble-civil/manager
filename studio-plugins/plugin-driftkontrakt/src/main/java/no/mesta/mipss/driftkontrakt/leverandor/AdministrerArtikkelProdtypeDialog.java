package no.mesta.mipss.driftkontrakt.leverandor;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter.ArticleExpiredCurrencyRenderer;
import no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter.HighlightFactory;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.driftslogg.DriftsloggService;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import no.mesta.mipss.persistence.kontrakt.Samproduksjon;
import no.mesta.mipss.persistence.stroing.Stroprodukt;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.table.CurrencyRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.MipssTableComboBoxEditor;
import no.mesta.mipss.ui.table.MipssTableComboBoxRenderer;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static no.mesta.mipss.driftkontrakt.leverandor.ArtikkelProdtypeTableModel.COL_PRICE;
import static no.mesta.mipss.driftkontrakt.leverandor.ArtikkelProdtypeTableModel.COL_PRODUCTION_TYPE;

public class AdministrerArtikkelProdtypeDialog extends JDialog implements TableModelListener {

	private JPanel pnlHeader = new JPanel();
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JMipssBeanTable<ArtikkelProdtype> tblArtikler;
	private JScrollPane scrollPane = new JScrollPane();
	private JLabel lblForklaring = new JLabel();
	private JButton btnLagre = new JButton();
	private JButton btnAvbryt = new JButton();

	private List<ArtikkelProdtype> artikler = new ArrayList<>();
	private List<Samproduksjon> samproduksjon;
	private List<Prodtype> prodtyper;
	private List<Stroprodukt> stroprodukter;

    private JTextField driftkontraktField;
    private JTextField leverandorField;
    private JTextField levkontraktField;
    private final DriftsloggService driftsloggBean;

    public AdministrerArtikkelProdtypeDialog(JFrame parent, Levkontrakt levkontrakt) {
		super(parent, ResourceUtils.getResource("dialogTitle.artikler"), true);

		MaintenanceContract maintenanceBean = BeanUtil.lookup("MaintenanceContractBean", MaintenanceContract.class);
        driftsloggBean = BeanUtil.lookup("DriftsloggService", DriftsloggService.class);

        samproduksjon = driftsloggBean.getSamproduksjon();
		prodtyper = maintenanceBean.getProdtypeList();
		stroprodukter = driftsloggBean.getAllStroprodukter();

        samproduksjon.add(0,null);
		prodtyper.add(0, null);
		stroprodukter.add(0, null);

        driftkontraktField = getReadOnlyField(levkontrakt.getDriftkontrakt().getTextForGUI());
        leverandorField = getReadOnlyField(levkontrakt.getLeverandor().getTextForGUI());
        levkontraktField = getReadOnlyField(levkontrakt.getTextForGUI());


		List<AgrArtikkelV> articles = driftsloggBean.getArtikler(levkontrakt);

		for(AgrArtikkelV article : articles) {
            ArtikkelProdtype ap = findMapping(levkontrakt, article);
			artikler.add(ap);
		}

		initTable();
		initButtons();
		initGui();
		
		setLocationRelativeTo(null);
		setVisible(true);
	}

    private ArtikkelProdtype findMapping(Levkontrakt levkontrakt, AgrArtikkelV article) {
        ArtikkelProdtype ap = driftsloggBean.getArtikkelProdtypeMapping(levkontrakt.getLeverandorNr(), levkontrakt.getDriftkontraktId(), levkontrakt.getLevkontraktIdent(), article.getArtikkelIdent());

        if(ap == null) {
            ap = new ArtikkelProdtype();
            ap.setLeverandorNr(levkontrakt.getLeverandorNr());
            ap.setKontraktId(levkontrakt.getDriftkontraktId());
            ap.setLevkontraktId(levkontrakt.getLevkontraktIdent());
            ap.setArtikkelId(article.getArtikkelIdent());
            ap.setArtikkel(article.getArtikkelNavn());
            ap.setOppgjorsenhet(article.getEnhet());
            ap.setAgrArtikkelV(article);
            ap.setEnhetspris(article.getPris());
            driftsloggBean.saveArtikkelProdtypeMapping(ap);
        }
        return ap;
    }

    private JTextField getReadOnlyField(String value) {
        JTextField field = new JTextField(value);
        field.setEditable(false);
        return field;
    }
	
	private void initTable() {
        ArtikkelProdtypeTableModel tblModelArtikler = new ArtikkelProdtypeTableModel(artikler);

		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(ArtikkelProdtype.class, tblModelArtikler.getColumns());
		tblArtikler = new JMipssBeanTable<>(tblModelArtikler, colModel);
        tblArtikler.getModel().addTableModelListener(this);

		tblArtikler.setFillsViewportWidth(true);
		tblArtikler.setDoWidthHacks(false);

		tblArtikler.getColumn(COL_PRICE).setCellRenderer(new ArticleExpiredCurrencyRenderer());

		tblArtikler.setDefaultRenderer(Samproduksjon.class, new ArticleExpiredComboBoxRenderer());
		tblArtikler.setDefaultEditor(Samproduksjon.class, new MipssTableComboBoxEditor(samproduksjon));

		tblArtikler.setDefaultRenderer(Prodtype.class, new ArticleExpiredComboBoxRenderer());
		tblArtikler.setDefaultEditor(Prodtype.class, new MipssTableComboBoxEditor(prodtyper));

		tblArtikler.setDefaultRenderer(Stroprodukt.class, new ArticleExpiredComboBoxRenderer());
		tblArtikler.setDefaultEditor(Stroprodukt.class, new MipssTableComboBoxEditor(stroprodukter));

		tblArtikler.setHighlighters(HighlightFactory.getArticleExpiredHighlighter(tblArtikler, tblModelArtikler));
	}
	
	private void initButtons() {
		btnLagre.setAction(new AbstractAction(ResourceUtils.getResource("button.lagre")) {
			@Override
			public void actionPerformed(ActionEvent e) {

                for(ArtikkelProdtype ap : artikler) {
                    driftsloggBean.saveArtikkelProdtypeMapping(ap);
                }
                dispose();
			}
		});
		btnAvbryt.setAction(new AbstractAction(ResourceUtils.getResource("button.avbryt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}
	
	private void initGui() {
		this.setSize(new Dimension(900, 500));
		this.setLayout(new GridBagLayout());

		tblArtikler.setPreferredScrollableViewportSize(new Dimension(0, 0));
        scrollPane.getViewport().add(tblArtikler, null);

        pnlHeader.setLayout(new GridBagLayout());

        pnlHeader.add(new JLabel(ResourceUtils.getResource("label.driftkontrakt")), new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        pnlHeader.add(new JLabel(ResourceUtils.getResource("label.leverandor")), new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        pnlHeader.add(new JLabel(ResourceUtils.getResource("label.levkontrakt")), new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        pnlHeader.add(driftkontraktField, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        pnlHeader.add(leverandorField, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        pnlHeader.add(levkontraktField, new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));

		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(scrollPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnLagre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

		lblForklaring.setText(ResourceUtils.getResource("label.samprodFoklaring"));

        this.add(pnlHeader, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlMain, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(lblForklaring, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 11, 11), 0, 0));
	}

    @Override
    public void tableChanged(TableModelEvent e) {
        int row = e.getFirstRow();
        int column = e.getColumn();

        if(column==COL_PRODUCTION_TYPE) {
            ArtikkelProdtype artikkelProdtype = artikler.get(row);
            if(artikkelProdtype==null || artikkelProdtype.getProdtype()==null || !artikkelProdtype.getProdtype().getStroFlagg()) {
                artikkelProdtype.setStroprodukt(null);
            }
        }

    }
}