package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.resources.images.IconResources;

import org.jdesktop.beansbinding.BindingGroup;

public class ByggherrePanel extends JPanel {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlVelgKunde = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblKundeNr = new JLabel();
    private JLabel lblKundeNavn = new JLabel();
    private JTextField txtKundeNr = new JTextField();
    private JTextField txtKundeNavn = new JTextField();
    private JButton btnVelgKunde = new JButton();
	
	private DriftkontraktController driftkontraktController;
    private BindingGroup bindingGroup;
	
	public ByggherrePanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		
		bindingGroup = new BindingGroup();
		
		initGui();
		initButtons();
		initTextFields();
		
		bindingGroup.bind();
	}
	
	private void initTextFields() {
		driftkontraktController.registerChangableField("kundeNr");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null ? currentDriftkontrakt.kundeNr : null}", txtKundeNr, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${kundeView != null ? kundeView.kundeNavn : null}", txtKundeNavn, "text", BindingHelper.READ));
	}
	
	private void initButtons() {
		btnVelgKunde.setAction(driftkontraktController.getVelgKundeAction());
		btnVelgKunde.setIcon(IconResources.DETALJER_SMALL_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnVelgKunde, "enabled"));
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlVelgKunde.setLayout(gridBagLayout3);
        lblKundeNr.setText(ResourceUtils.getResource("label.kundeNummer"));
        lblKundeNavn.setText(ResourceUtils.getResource("label.kundeNavn"));
        txtKundeNr.setPreferredSize(new Dimension(60, 21));
        txtKundeNr.setEditable(false);
        txtKundeNavn.setPreferredSize(new Dimension(180, 21));
        txtKundeNavn.setEditable(false);
        pnlVelgKunde.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kunde")));
        pnlVelgKunde.add(lblKundeNr, 
                         new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                                new Insets(0, 5, 5, 0), 0, 0));
        pnlVelgKunde.add(lblKundeNavn, 
                         new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                                new Insets(0, 5, 5, 0), 0, 
                                                0));
        pnlVelgKunde.add(txtKundeNr, 
                         new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                                new Insets(0, 5, 5, 5), 0, 0));
        pnlVelgKunde.add(txtKundeNavn, 
                         new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                                new Insets(0, 5, 5, 5), 0, 
                                                0));
        pnlMain.add(btnVelgKunde, 
                         new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                                new Insets(0, 0, 11, 0), 0, 
                                                0));
        pnlMain.add(pnlVelgKunde, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, 
                 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(11, 11, 11, 11), 0, 0));
	}
}
