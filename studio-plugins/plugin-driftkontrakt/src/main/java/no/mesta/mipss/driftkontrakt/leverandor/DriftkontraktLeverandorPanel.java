package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DriftkontraktLeverandorPanel extends JPanel {
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
	private JPanel pnlMain = new JPanel();
	
	private LeverandorController leverandorController;
	private LeverandorListPanel leverandorListPanel;
	private LeverandorAgressoTabPanel leverandorPanel;
	
	public DriftkontraktLeverandorPanel(LeverandorController leverandorController) {
		this.leverandorController = leverandorController;
		
		initGui();
	}
	
	private void initGui() {
		leverandorListPanel = new LeverandorListPanel(leverandorController);
		leverandorPanel = new LeverandorAgressoTabPanel(leverandorController);
		
		this.setLayout(gridBagLayout1);
		pnlMain.setLayout(new GridLayout(2, 1, 11, 11));
		pnlMain.add(leverandorListPanel);
		pnlMain.add(leverandorPanel);
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));	
	}
}
