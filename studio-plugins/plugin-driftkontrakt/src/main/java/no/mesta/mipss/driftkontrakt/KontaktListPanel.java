package no.mesta.mipss.driftkontrakt;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.EpostadresseTableCellRenderer;
import no.mesta.mipss.ui.table.EpostadresseTableHelper;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.valueobjects.Epostadresse;

import org.jdesktop.beansbinding.BindingGroup;

public class KontaktListPanel extends JPanel {
    private JPanel pnlKontakter = new JPanel();
    private JScrollPane scrlPaneKontakter = new JScrollPane();
    private JPanel pnlKontakterBtns = new JPanel();
    private JButton btnLeggTilKontakt = new JButton();
    private JButton btnEndreKontakt = new JButton();
    private JButton btnSlettKontakt = new JButton();
    private JMipssBeanTable<KontraktKontakttype> tblKontakter;
    
    private MipssBeanTableModel<KontraktKontakttype> kontakterModel;
	private DriftkontraktController driftkontraktController;
    private BindingGroup bindingGroup;
    
    public KontaktListPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		
		bindingGroup = new BindingGroup();
		
		initTable();
		initButtons();
		initGui();
		
		bindingGroup.bind();
    }
    
    private void initTable() {
		kontakterModel = new MipssBeanTableModel<KontraktKontakttype>("kontraktKontaktypeList" ,KontraktKontakttype.class, "kontakttypeNavn", "kontaktnavn", "tlfnummer1String", "tlfnummer2String", "epost", "faksnummerString");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", kontakterModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(KontraktKontakttype.class, kontakterModel.getColumns());
		
		tblKontakter = new JMipssBeanTable<KontraktKontakttype>(kontakterModel, columnModel);
		tblKontakter.setFillsViewportWidth(true);
		tblKontakter.setDoWidthHacks(false);
		EpostadresseTableHelper.prepareTable(tblKontakter);
		tblKontakter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() >= 2) {
					Action a = driftkontraktController.getEndreKontaktAction(kontakterModel, tblKontakter);
					a.actionPerformed(null);
				}
			}
		});
    }
    
    private void initButtons() {
		btnLeggTilKontakt.setAction(driftkontraktController.getLeggTilKontaktAction());
		btnLeggTilKontakt.setIcon(IconResources.ADD_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnLeggTilKontakt, "enabled"));
		
		btnSlettKontakt.setAction(driftkontraktController.getSlettKontaktAction(kontakterModel, tblKontakter));
		btnSlettKontakt.setIcon(IconResources.REMOVE_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblKontakter, "${noOfSelectedRows > 0}", btnSlettKontakt, "enabled"));
		
		btnEndreKontakt.setAction(driftkontraktController.getEndreKontaktAction(kontakterModel, tblKontakter));
		btnEndreKontakt.setIcon(IconResources.DETALJER_SMALL_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblKontakter, "${noOfSelectedRows == 1}", btnEndreKontakt, "enabled"));
    }
    
    private void initGui() {
    	this.setLayout(new GridBagLayout());
    	pnlKontakter.setLayout(new GridBagLayout());
        pnlKontakter.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kontaktinformasjon")));
        pnlKontakterBtns.setLayout(new GridBagLayout());
        tblKontakter.setPreferredScrollableViewportSize(new Dimension(0, 0));
        scrlPaneKontakter.getViewport().add(tblKontakter, null);
        pnlKontakter.add(scrlPaneKontakter, 
                new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                       new Insets(0, 5, 5, 5), 0, 0));
	    pnlKontakterBtns.add(btnLeggTilKontakt, 
	                         new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, 
	                                                new Insets(0, 0, 5, 0), 0, 
	                                                0));
	    pnlKontakterBtns.add(btnEndreKontakt, 
	            new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, 
	                                   new Insets(0, 0, 5, 0), 0, 
	                                   0));
	    pnlKontakterBtns.add(btnSlettKontakt, 
	                         new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, 
	                                                new Insets(0, 0, 0, 0), 0, 
	                                                0));
	    pnlKontakter.add(pnlKontakterBtns, 
	                     new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, 
	                                            new Insets(0, 0, 5, 5), 0, 0));
	    this.add(pnlKontakter, 
	                new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
	                                       GridBagConstraints.BOTH, 
	                                       new Insets(0, 0, 0, 0), 0, 0));
    }
}
