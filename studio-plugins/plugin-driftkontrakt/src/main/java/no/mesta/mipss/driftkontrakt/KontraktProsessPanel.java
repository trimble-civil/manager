package no.mesta.mipss.driftkontrakt;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.KontraktProsess;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Prosessett;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.list.MipssRenderableEntityModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;
import no.mesta.mipss.ui.tree.MipssTreeCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXBusyLabel;

/**
 * Gui for å opprette kontraktens prosessliste. 
 * 
 * @author harkul
 *
 */
@SuppressWarnings("serial")
public class KontraktProsessPanel extends JPanel{

	private final KontraktProsessController controller;
	
	private JMipssBeanTable<Prosessett> prosessettTable;
	
	private JTree prosessTree;

	private JMipssBeanTable<KontraktProsess> kontraktProsessTable;
	private MipssBeanTableModel<KontraktProsess> kontraktProsessTableModel;
	
	private DefaultTreeModel treeModel;
	
	private MipssBeanTableModel<Prosessett> prosessettTableModel;

	private JScrollPane prosessettTableScroll;
	private JScrollPane prosessTreeScroll;
	private JScrollPane kontraktProsessScroll;
	
	private JButton addOne;
	private JButton addAll;
	private JButton removeOne;

	private BindingGroup bindingGroup;

	private Prosessett prosessett;
	
	public KontraktProsessPanel(DriftkontraktController controller){
		this.controller = new KontraktProsessController(controller);
		
		controller.addPropertyChangeListener("currentDriftkontrakt", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				byttKontrakt();
			}
		});
		bindingGroup = new BindingGroup();
		initGui();
		bindingGroup.bind();
	}
	
	private void initGui() {
		initTable();
		initTree();
		initKontraktProsessTable();
		initButtons();
		
		setLayout(new GridBagLayout());
		JLabel infoLabel = new JLabel(ResourceUtils.getResource("label.prosessett.info"));
		
		JPanel prosessettPanel = new JPanel(new GridBagLayout());
		prosessettPanel.setPreferredSize(new Dimension(100, 200));
		prosessettPanel.setMinimumSize(new Dimension(100, 200));
		prosessettPanel.add(infoLabel, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		prosessettPanel.add(prosessettTableScroll, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
		
		JPanel prosessPanel = new JPanel(new GridBagLayout());
		prosessPanel.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgProsesser")));
		prosessPanel.add(prosessTreeScroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

		JPanel kontraktProsessPanel = new JPanel(new GridBagLayout());
		kontraktProsessPanel.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kontraktProsess")));
		kontraktProsessPanel.add(kontraktProsessScroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
		
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		
		buttonPanel.add(addOne, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		buttonPanel.add(addAll, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 15, 5), 0, 0));
		buttonPanel.add(removeOne, 	new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
		JPanel mainPanel = new JPanel(new GridBagLayout());
		mainPanel.add(prosessettPanel, 		new GridBagConstraints(0, 0, 3, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 11, 0), 0, 0));
		mainPanel.add(prosessPanel, 		new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 11), 0, 0));
		mainPanel.add(buttonPanel, 			new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
		mainPanel.add(kontraktProsessPanel, new GridBagConstraints(2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		this.add(mainPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}

	private void initTable() {
		prosessettTableModel= new MipssBeanTableModel<Prosessett>(Prosessett.class, "navn", "beskrivelse");
		prosessettTableModel.setBeanInstance(controller);
		prosessettTableModel.setBeanInterface(KontraktProsessController.class);
		prosessettTableModel.setBeanMethod("getProsessettList");
		
		MipssRenderableEntityTableColumnModel prosessettColumnModel = new MipssRenderableEntityTableColumnModel(Prosessett.class, prosessettTableModel.getColumns());
		prosessettTable = new JMipssBeanTable<Prosessett>(prosessettTableModel, prosessettColumnModel);
		prosessettTable.setFillsViewportWidth(true);
		prosessettTable.setDoWidthHacks(false);
		prosessettTable.setPreferredScrollableViewportSize(new Dimension(0, 0));
		prosessettTable.getColumn(0).setPreferredWidth(50);
		prosessettTable.getColumn(1).setPreferredWidth(350);
//		prosessettTable.getColumn(2).setPreferredWidth(10);
		prosessettTableScroll = new JScrollPane(prosessettTable);
		
		prosessettTableModel.addTableLoaderListener(new ProsessettLoaderListener(prosessettTableScroll));
		
		
		prosessettTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		prosessettTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int row = prosessettTable.getSelectedRow();
				final int selectedRow = prosessettTable.convertRowIndexToModel(row);
				Prosessett prosessett = prosessettTableModel.getEntities().get(selectedRow);
				setProsessett(prosessett);
			}
		});
	}
	
	private void initTree(){
		prosessTree = new JTree();
		treeModel = new DefaultTreeModel(new DefaultMutableTreeNode("Prosesser"));
		prosessTree.setModel(treeModel);
		prosessTree.setCellRenderer(new MipssTreeCellRenderer(IconResources.PROSESS_16));
		prosessTreeScroll = new JScrollPane(prosessTree);
		
		prosessTree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener(){
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)prosessTree.getLastSelectedPathComponent();
				if (node==null){
					addOne.setEnabled(false);
				}else if (node.getUserObject() instanceof Prosess){
					addOne.setEnabled(!isProsessInKontraktListe((Prosess)node.getUserObject()));
				} else{
					addOne.setEnabled(false);
				}
			}
		});
	}
	public void setCurrentDriftkontrakt(Driftkontrakt d){
		Collections.sort(kontraktProsessTableModel.getEntities(), new Comparator<KontraktProsess>() {
			@Override
			public int compare(KontraktProsess o1, KontraktProsess o2) {
				return o1.getProsess().getProsessKode().compareTo(o2.getProsess().getProsessKode());
			}
		});
	}
	private void initKontraktProsessTable(){		
		kontraktProsessTableModel = new MipssBeanTableModel<KontraktProsess>("kontraktProsessList", KontraktProsess.class, new int[]{1, 2}, "prosess", "gyldigForInspeksjon", "gyldigForArbeid");
		bindingGroup.addBinding(BindingHelper.createbinding(controller.getDriftkontraktController(), "currentDriftkontrakt", kontraktProsessTableModel, "sourceObject", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(controller.getDriftkontraktController(), "currentDriftkontrakt", this, "currentDriftkontrakt", BindingHelper.READ));
		
		MipssRenderableEntityTableColumnModel kontraktProsessColumnModel = new MipssRenderableEntityTableColumnModel(KontraktProsess.class, kontraktProsessTableModel.getColumns());
		kontraktProsessTableModel.addTableModelListener(new TableModelListener(){
			@Override
			public void tableChanged(TableModelEvent e) {
				if (e.getType()==TableModelEvent.UPDATE){
					int row = e.getFirstRow();
					boolean changed = false;
					if (e.getColumn()==1){
						if (row<kontraktProsessTableModel.getSize()){
							changed=true;
							KontraktProsess kp = kontraktProsessTableModel.get(row);
							if (!kp.getGyldigForInspeksjon()&&!kp.getGyldigForArbeid()){
								kp.setGyldigForInspeksjon(Boolean.TRUE);
								changed=false;
							}
							//inspeksjon
						}
					}
					if (e.getColumn()==2){
						if (row<kontraktProsessTableModel.getSize()){
							changed=true;
							KontraktProsess kp = kontraktProsessTableModel.get(row);
							if (!kp.getGyldigForInspeksjon()&&!kp.getGyldigForArbeid()){
								kp.setGyldigForArbeid(Boolean.TRUE);
								changed = false;
							}
						}
					}
					if (changed){
						controller.getDriftkontraktController().setOverriddenChange(Boolean.TRUE);
					}
				}
			}
			
		});
		kontraktProsessTable = new JMipssBeanTable<KontraktProsess>(kontraktProsessTableModel, kontraktProsessColumnModel );
		kontraktProsessTable.setFillsViewportWidth(true);
		kontraktProsessTable.setDoWidthHacks(false);
		kontraktProsessTable.getColumn(0).setPreferredWidth(235);
		kontraktProsessTable.getColumn(1).setPreferredWidth(5);
		kontraktProsessTable.getColumn(2).setPreferredWidth(5);
//		kontraktProsessTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		kontraktProsessScroll = new JScrollPane(kontraktProsessTable);
		kontraktProsessTable.setDefaultRenderer(Prosess.class, new RenderableMipssEntityTableCellRenderer<Prosess>());
		BindingHelper.createbinding(kontraktProsessTable, "${noOfSelectedRows == 1}", removeOne, "enabled").bind();
		
	}
	
	private void initButtons(){
		addOne = new JButton(createAddOneAction(IconResources.ADD_ONE_ICON));
		removeOne = new JButton(createRemoveOneAction(IconResources.REMOVE_ONE_ICON));
		addAll = new JButton(createAddAllAction(IconResources.ADD_ALL_ICON));
		addOne.setToolTipText(ResourceUtils.getResource("tooltip.leggTilEnProsess"));
		removeOne.setToolTipText(ResourceUtils.getResource("tooltip.fjernEnProsess"));
		
		addOne.setEnabled(false);
		removeOne.setEnabled(true);
		
		Dimension size = new Dimension(50, 25);
		addOne.setPreferredSize(size);
		removeOne.setPreferredSize(size);
		
		
	}
	
	private AbstractAction createAddOneAction(Icon icon){
		return new AbstractAction("",icon) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)prosessTree.getLastSelectedPathComponent();
				if (node != null && node.getUserObject() instanceof Prosess)
					leggTilProsessIListe((Prosess)node.getUserObject());
			}
		};
	}
	private AbstractAction createAddAllAction(Icon icon){
		return new AbstractAction("",icon) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (prosessett!=null){
					int r = JOptionPane.showConfirmDialog(controller.getDriftkontraktController().getDriftkontraktAdminModule().getModuleGUI(), ResourceUtils.getResource("message.leggTilAlleProsesser.message"), ResourceUtils.getResource("message.leggTilAlleProsesser.title"), JOptionPane.YES_NO_OPTION);
					if (r==JOptionPane.YES_OPTION){
						for (Prosess p:getAlleProsserFraTre()){
							leggTilProsessIListe(p);
						}
					}
				}
			}
		};
	}
	private AbstractAction createRemoveOneAction(Icon icon){
		return new AbstractAction("", icon) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				List<KontraktProsess> selectedEntities = kontraktProsessTable.getSelectedEntities();
				if (selectedEntities.size()==1){
					fjernProsessFraListe(selectedEntities.get(0));
				}
			}
		};
	}
	
	
	private void leggTilProsessIListe(Prosess p){
		
		KontraktProsess kp = controller.createKontraktProsess(p);
		if (!isProsessInKontraktListe(p)){
			kontraktProsessTableModel.addItem(kp);
			controller.getCurrentDriftkontrakt().getKontraktProsessList().add(kp);
			addOne.setEnabled(false);
			controller.getDriftkontraktController().setOverriddenChange(Boolean.TRUE);
		}
		Collections.sort(kontraktProsessTableModel.getEntities(), new Comparator<KontraktProsess>() {
			@Override
			public int compare(KontraktProsess o1, KontraktProsess o2) {
				// TODO Auto-generated method stub
				return o1.getProsess().getProsessKode().compareTo(o2.getProsess().getProsessKode());
			}
		});
	}
	
	private boolean isProsessInKontraktListe(Prosess p){
		
		for (KontraktProsess pp:kontraktProsessTableModel.getEntities()){
			if (pp.getProsess().equals(p)){
				return true;
			}
		}
		return false;
	}
	private void fjernProsessFraListe(KontraktProsess p){
		boolean remove = true;
		if (controller.isProsessIBrukPaaValgtKontrakt(p.getProsess().getId())){
			int rv = JOptionPane.showConfirmDialog(controller.getDriftkontraktController().getDriftkontraktAdminModule().getModuleGUI(), ResourceUtils.getResource("message.prosessenErIBruk.message"), ResourceUtils.getResource("message.prosessenErIBruk.title"), JOptionPane.YES_NO_OPTION);
			if (rv==JOptionPane.NO_OPTION){
				remove =false;
			}	
		}
		else if(controller.isProsessInFavorittListe(p.getProsessId())){
			JOptionPane.showMessageDialog(controller.getDriftkontraktController().getDriftkontraktAdminModule().getModuleGUI(), ResourceUtils.getResource("message.prosessErFavoritt.message"), ResourceUtils.getResource("message.prosessenErIBruk.title"), JOptionPane.WARNING_MESSAGE);
			remove = false;
		}
		if (remove){
			kontraktProsessTableModel.removeItem(p);
			controller.getDriftkontraktController().setOverriddenChange(Boolean.TRUE);
			controller.getCurrentDriftkontrakt().getKontraktProsessList().remove(p);
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)prosessTree.getLastSelectedPathComponent();
			if (node==null)
				addOne.setEnabled(false);
			else{
				if (node.getUserObject() instanceof Prosess){
					if (((Prosess)node.getUserObject()).equals(p.getProsess())){
						addOne.setEnabled(true);
					}
					else{
						addOne.setEnabled(false);
					}
				}
			}
		}
	}
	
	private void setProsessett(Prosessett prosessett){
		this.prosessett = prosessett;
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Prosesser");
		for (Prosess pp:prosessett.getProsessList()){
			createNodes(pp, root);
		}
		treeModel.setRoot(root);
		expandFirstLeaves();
	}
	
	private void expandFirstLeaves() {
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
		for (int i = 0;i<root.getChildCount();i++){
			DefaultMutableTreeNode childAt = (DefaultMutableTreeNode)root.getChildAt(i);
			prosessTree.expandPath(new TreePath(childAt.getPath()));
		}
	}
	private List<Prosess> getAlleProsserFraTre(){
		List<Prosess> prosessList = new ArrayList<Prosess>();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
		getAlleProsesserFraTre(prosessList, root);
		return prosessList;
	}
	private void getAlleProsesserFraTre(List<Prosess> prosessList, DefaultMutableTreeNode root){
		int c = root.getChildCount();
		for (int i=0;i<c;i++){
			DefaultMutableTreeNode child = (DefaultMutableTreeNode)root.getChildAt(i);
			if (child.getUserObject() instanceof Prosess){
				prosessList.add((Prosess)child.getUserObject());
			}
			getAlleProsesserFraTre(prosessList, child);
		}
	}
	private void createNodes(Prosess p, DefaultMutableTreeNode root){
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(p);
		root.add(node);
		for (Prosess pp:p.getProsesser()){
			createNodes(pp, node);
		}
	}
	public static void printChild(Prosess a, String tab){
		for (Prosess pp:a.getProsesser()){
			System.out.println(tab+pp.getProsessKode()+" "+pp.getNavn());
			printChild(pp, tab+"  ");
		}
		
	}
	private void byttKontrakt() {
		prosessettTableModel.loadData();
//		kontraktProsessTableModel.setBeanMethodArgValues(new Object[]{controller.getCurrentDriftkontrakt().getId()});
//		kontraktProsessTableModel.loadData();
		
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setSize(400, 400);
		f.setLocationRelativeTo(null);
		
		KontraktProsess p = new KontraktProsess();
		Prosess pp = new Prosess();
		pp.setNavn("1");
		p.setProsess(pp);
		
		KontraktProsess p1 = new KontraktProsess();
		Prosess pp1 = new Prosess();
		pp1.setNavn("2");
		p1.setProsess(pp1);
		
		KontraktProsess p2 = new KontraktProsess();
		Prosess pp2 = new Prosess();
		pp2.setNavn("3");
		p2.setProsess(pp2);
		
		List<KontraktProsess> kl = new ArrayList<KontraktProsess>();
		kl.add(p1);
		kl.add(p);
		
		MipssRenderableEntityModel<KontraktProsess> m = new MipssRenderableEntityModel<KontraktProsess>(kl);
		m.addElement(p2);
		JList l = new JList(m);
		l.setCellRenderer(new MipssListCellRenderer("Ingen i liste..."));
		f.add(l);
		f.setVisible(true);
		
	}
	
	class ProsessettLoaderListener implements MipssBeanLoaderListener{
		private JXBusyLabel busyLabel;
		private final JScrollPane scroll;
		private Component view;
		
		public ProsessettLoaderListener(JScrollPane scroll){
			this.scroll = scroll;
			busyLabel = new JXBusyLabel();
			busyLabel.setText(ResourceUtils.getResource("label.prosessett.laster"));
			view = scroll.getViewport().getView();
		}
		
		@Override
		public void loadingFinished(MipssBeanLoaderEvent event) {
			busyLabel.setBusy(false);
			scroll.setViewportView(view);
		}

		@Override
		public void loadingStarted(MipssBeanLoaderEvent event) {
			view = scroll.getViewport().getView();
			busyLabel.setBusy(true);
			scroll.setViewportView(busyLabel);
			
		}
	}
}
