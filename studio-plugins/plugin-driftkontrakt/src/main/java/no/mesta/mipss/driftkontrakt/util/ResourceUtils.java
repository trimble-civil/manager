package no.mesta.mipss.driftkontrakt.util;

import no.mesta.mipss.common.PropertyResourceBundleUtil;

public class ResourceUtils {
	private static PropertyResourceBundleUtil properties;
	private static final String BUNDLE_NAME = "plugin-driftkontrakt";
	
	
	public static String getResource(String key){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key);
	}
	public static String getResource(String key, String... values){
		if (properties==null)
			properties = PropertyResourceBundleUtil.getPropertyBundle(BUNDLE_NAME);
		return properties.getGuiString(key, values);
	}
}
