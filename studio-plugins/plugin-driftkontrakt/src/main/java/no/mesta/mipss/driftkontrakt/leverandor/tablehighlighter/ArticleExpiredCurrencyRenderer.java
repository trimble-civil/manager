package no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter;

import no.mesta.mipss.driftkontrakt.leverandor.ArtikkelProdtypeTableModel;
import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import no.mesta.mipss.ui.table.CurrencyRenderer;

import javax.swing.*;
import java.awt.*;

public class ArticleExpiredCurrencyRenderer extends CurrencyRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,  boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (comp instanceof Box && table.getModel() instanceof ArtikkelProdtypeTableModel) {
            Box panel = (Box) comp;
            Component[] comps = panel.getComponents();

            ArtikkelProdtypeTableModel model = (ArtikkelProdtypeTableModel) table.getModel();
            ArtikkelProdtype article = model.get(table.convertRowIndexToModel(row));

            if (comps.length > 1 && comps[1] instanceof JLabel && article.isUtgatt()) {
                JLabel label = (JLabel) comps[1];
                label.setBackground(Color.LIGHT_GRAY);
                label.setForeground(Color.GRAY);
            }
        }

        return comp;
    }

}