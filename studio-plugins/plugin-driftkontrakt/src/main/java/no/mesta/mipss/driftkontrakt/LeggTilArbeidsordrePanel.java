package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.driftkontrakt.bean.ProsjektQueryFilter;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.organisasjon.ArbeidsordreView;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.SokPanel;

public class LeggTilArbeidsordrePanel extends AbstractRelasjonPanel<List<KontraktProsjekt>> {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
	
	private SokPanel<ArbeidsordreView> sokPanel;
	private DriftkontraktController driftkontraktController;
	
	public LeggTilArbeidsordrePanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		sokPanel = new SokPanel<ArbeidsordreView>(ArbeidsordreView.class, 
												new ProsjektQueryFilter(), 
												driftkontraktController, 
												"getArbeidsordreViewList", 
												DriftkontraktController.class, 
												true,
												"arbeidsordrenr", 
												"arbeidsordrenavn", 
												"ansvar");
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension getDialogSize() {
		return new Dimension(400,500);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<KontraktProsjekt> getNyRelasjon() {
		List<ArbeidsordreView> prosjektViewList = sokPanel.getSelectedEntityList();
		if(prosjektViewList != null) {
			List<KontraktProsjekt> retList = new ArrayList<KontraktProsjekt>();
			for(ArbeidsordreView prosjektView : prosjektViewList) {
				if(isLegal(prosjektView)) {
					KontraktProsjekt kontraktProsjekt = new KontraktProsjekt();
					kontraktProsjekt.setDriftkontrakt(driftkontraktController.getCurrentDriftkontrakt());
					kontraktProsjekt.setProsjektNr(prosjektView.getArbeidsordrenr());
					kontraktProsjekt.setSelskapskode("10");
					kontraktProsjekt.setStandardFlagg(Long.valueOf(0));
					retList.add(kontraktProsjekt);
				}
			}
			return retList;
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilProsjekt");
	}
	
	private void updateComplete() {
		setComplete(Boolean.valueOf(sokPanel.getSelectedEntityList() != null));
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlMain.add(sokPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}

	@Override
	public boolean isLegal() {
		boolean oneLegal = false;
		boolean oneIllegal = false;
		
		List<ArbeidsordreView> prosjektViewList = sokPanel.getSelectedEntityList();
		
		for(ArbeidsordreView prosjektView : prosjektViewList) {
			if(isLegal(prosjektView)) {
				oneLegal = true;
			} else {
				oneIllegal = true;
			}
		}
		
		if(!oneLegal) {
			JOptionPane.showMessageDialog(getParentDialog(), ResourceUtils.getResource("message.prosjektDuplikat"));
		}
		
		return oneLegal;
	}
	
	private boolean isLegal(ArbeidsordreView prosjektView) {
		List<KontraktProsjekt> kontraktProsjektList = driftkontraktController.getCurrentDriftkontrakt().getProsjektKontraktList();
		for(KontraktProsjekt kontraktProsjekt : kontraktProsjektList) {
			if(kontraktProsjekt.getProsjektNr().equals(prosjektView.getProsjektnr())) {
				return false;
			}
		}
		return true;
	}
}
