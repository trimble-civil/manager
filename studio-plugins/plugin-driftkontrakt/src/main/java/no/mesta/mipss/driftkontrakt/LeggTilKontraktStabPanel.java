package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.driftkontrakt.bean.StabQueryFilter;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.SokPanel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

public class LeggTilKontraktStabPanel extends AbstractRelasjonPanel<List<KontraktStab>> {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlVelgArbeidsfunksjon = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblArbeidsfunksjon = new JLabel();
    private JComboBox cbxArbeidsfunksjon = new JComboBox();
    private JPanel pnlBrukerList = new JPanel();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    
    private DriftkontraktController driftkontraktController;
    private SokPanel<Bruker> sokPanel;
    private List<Arbeidsfunksjon> arbeidsfunksjonList;
	
	public LeggTilKontraktStabPanel(DriftkontraktController driftkontraktController, List<Arbeidsfunksjon> arbeidsfunksjonList) {
		this.driftkontraktController = driftkontraktController;
		this.arbeidsfunksjonList = arbeidsfunksjonList;
		
		sokPanel = new SokPanel<Bruker>(Bruker.class,
										new StabQueryFilter(), 
										driftkontraktController, 
										"getBrukerList", 
										DriftkontraktController.class, 
										true,
										"navn", 
										"signatur");
		initGui();
		
		sokPanel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateComplete();
			}
		});
		
		
		initComboBox();
	}
	
	private void initComboBox() {
		MipssComboBoxModel<Arbeidsfunksjon> model = new MipssComboBoxModel<Arbeidsfunksjon>(arbeidsfunksjonList, false);
		cbxArbeidsfunksjon.setRenderer((ListCellRenderer)new MipssListCellRenderer<Arbeidsfunksjon>());
		cbxArbeidsfunksjon.setModel(model);
		cbxArbeidsfunksjon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateComplete();
			}
		});
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlVelgArbeidsfunksjon.setLayout(gridBagLayout3);
        pnlVelgArbeidsfunksjon.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgArbeidsfunksjon")));
        lblArbeidsfunksjon.setText(ResourceUtils.getResource("label.arbeidsfunksjon"));
        cbxArbeidsfunksjon.setPreferredSize(new Dimension(120, 21));
        pnlBrukerList.setLayout(gridBagLayout4);
        pnlBrukerList.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgBruker")));
        pnlVelgArbeidsfunksjon.add(lblArbeidsfunksjon, 
                                   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                                          new Insets(0, 5, 5, 
                                                                     0), 0, 
                                                          0));
        pnlVelgArbeidsfunksjon.add(cbxArbeidsfunksjon, 
                                   new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                                          new Insets(0, 5, 5, 
                                                                     0), 0, 
                                                          0));
        pnlMain.add(pnlVelgArbeidsfunksjon, 
                    new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        pnlBrukerList.add(sokPanel, 
                          new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                                                 new Insets(0, 0, 0, 0), 0, 
                                                 0));
        pnlMain.add(pnlBrukerList, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                           GridBagConstraints.BOTH, 
                                           new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlMain, 
                 new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(11, 11, 11, 11), 0, 0));
	}

	private void updateComplete() {
		Arbeidsfunksjon arbeidsfunksjon = getSelectedArbeidsfunksjon();
		List<Bruker> brukerList = getSelectedBrukerList();
		setComplete(Boolean.valueOf(brukerList != null && arbeidsfunksjon != null));
	}
	
	private List<Bruker> getSelectedBrukerList() {
		return sokPanel.getSelectedEntityList();
	}
	
	private Arbeidsfunksjon getSelectedArbeidsfunksjon() {
		return (Arbeidsfunksjon)cbxArbeidsfunksjon.getSelectedItem();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<KontraktStab> getNyRelasjon() {
		Arbeidsfunksjon arbeidsfunksjon = getSelectedArbeidsfunksjon();
		List<Bruker> brukerList = getSelectedBrukerList();
		if(brukerList != null && arbeidsfunksjon != null) {
			List<KontraktStab> retList = new ArrayList<KontraktStab>();
			for(Bruker bruker : brukerList) {
				if(isLegal(bruker)) {
					KontraktStab kontraktStab = new KontraktStab();
					kontraktStab.setArbeidsfunksjon(arbeidsfunksjon);
					kontraktStab.setBruker(bruker);
					kontraktStab.setDriftkontrakt(driftkontraktController.getCurrentDriftkontrakt());
					retList.add(kontraktStab);
				}
			}
			return retList;
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilStab");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension getDialogSize() {
		return new Dimension(400,500);
	}

	@Override
	public boolean isLegal() {
		boolean oneLegal = false;
		boolean oneIllegal = false; 
		
		List<Bruker> brukerList = getSelectedBrukerList();
		
		for(Bruker bruker : brukerList) {
			if(isLegal(bruker)) {
				oneLegal = true;
			} else {
				oneIllegal = true;
			}
		}
		
		if(!oneLegal) {
			JOptionPane.showMessageDialog(getParentDialog(), ResourceUtils.getResource("message.brukerDuplikat"));		
		}
		
		return oneLegal;
	}
	
	private boolean isLegal(Bruker bruker) {
		List<KontraktStab> kontraktStabList = driftkontraktController.getCurrentDriftkontrakt().getKontraktStabList();
		Arbeidsfunksjon arbeidsfunksjon = getSelectedArbeidsfunksjon();
		for(KontraktStab kontraktStab : kontraktStabList) {
			if(kontraktStab.getArbeidsfunksjon().getNavn().equals(arbeidsfunksjon.getNavn())
					&& kontraktStab.getBruker().getSignatur().equals(bruker.getSignatur())) {
				return false;
			}
		}
		return true;
	}
}
