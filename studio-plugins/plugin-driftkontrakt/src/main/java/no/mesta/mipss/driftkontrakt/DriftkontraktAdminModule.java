package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;

import javax.swing.JPanel;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.SaveStatusRegistry;
import no.mesta.mipss.driftkontrakt.kjoretoy.DriftkontraktKjoretoyPanel;
import no.mesta.mipss.driftkontrakt.leverandor.DriftkontraktLeverandorPanel;
import no.mesta.mipss.driftkontrakt.leverandor.LeverandorController;
import no.mesta.mipss.driftkontrakt.messages.OpenDriftAdminMessage;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.messages.LevHarUlagredeEndringerMessage;
import no.mesta.mipss.messages.RefreshLeverandorMessage;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.plugin.PluginMessage;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.dialogue.MipssDialogue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriftkontraktAdminModule extends MipssPlugin {
	public static final String PANEL_DRIFT_DATA = "kontraktsdata";
	public static final String PANEL_DRIFT_LEVERANDOR = "kontraktsleverandør";
	public static final String PANEL_DRIFT_KJORETOY = "kontraktskjøretøy";
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private DriftkontraktAdminModulePanel driftkontraktAdminModulePanel;
	private DriftkontraktAdminPanel driftkontraktAdminPanel;
	private DriftkontraktLeverandorPanel driftkontraktLeverandorPanel;
	private DriftkontraktKjoretoyPanel driftkontraktKjoretoyPanel;
	private MaintenanceContract contractBean;
	private MipssEntityService entityServiceBean;
	private DriftkontraktController driftkontraktController;
	private LeverandorController leverandorController;
	private JMipssContractPicker contractPicker;
	private SaveStatusRegistry saveStatusRegistry;
	private String panel;
	
	public DriftkontraktAdminModule() {
		logger.debug("new DriftkontraktAdminModule instanciated");
	}
	
	@Override
	public JPanel getModuleGUI() {
		return driftkontraktAdminModulePanel;
	}

	@Override
	protected void initPluginGUI() {
		saveStatusRegistry = new SaveStatusRegistry();
		contractPicker = new JMipssContractPicker(this, true);
		driftkontraktAdminModulePanel = new DriftkontraktAdminModulePanel(contractPicker);

		
		driftkontraktController = new DriftkontraktController(saveStatusRegistry, this, getMaintenanceContractBean(), getMipssEntityServiceBean(), contractPicker, driftkontraktAdminModulePanel);
		leverandorController = new LeverandorController(saveStatusRegistry, driftkontraktController, this);
		
		driftkontraktAdminModulePanel.initLagreButton(driftkontraktController);
		
		driftkontraktAdminPanel = new DriftkontraktAdminPanel(driftkontraktController);
		driftkontraktController.registrerDriftPanel(driftkontraktAdminPanel, PANEL_DRIFT_DATA);
		
		driftkontraktLeverandorPanel = new DriftkontraktLeverandorPanel(leverandorController);
		driftkontraktLeverandorPanel.setPreferredSize(new Dimension(800, 500));
		driftkontraktController.registrerDriftPanel(driftkontraktLeverandorPanel, PANEL_DRIFT_LEVERANDOR);
		
		driftkontraktKjoretoyPanel = new DriftkontraktKjoretoyPanel(driftkontraktController);
		driftkontraktController.registrerDriftPanel(driftkontraktKjoretoyPanel, PANEL_DRIFT_KJORETOY);
		
		saveStatusRegistry.doBind();
		
		driftkontraktController.visPanel(panel);
		
		if(contractPicker.getValgtKontrakt() != null) {
			driftkontraktController.lastKontraktForAdministrasjon(contractPicker.getValgtKontrakt(), true);
		}
	}

    public void readyForShutdown(MipssDialogue dialog) {
    	if(saveStatusRegistry.isChanged()) {
    		dialog.askUser();
    	}
    }
	
	@Override
	public void shutdown() {
		logger.debug("DriftkontraktAdminModule shudown");
	}
	
	private MaintenanceContract getMaintenanceContractBean() {
        if(contractBean == null) {
            contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
        }
        
        return contractBean;
    }
	
	private MipssEntityService getMipssEntityServiceBean() {
        if(entityServiceBean == null) {
        	entityServiceBean = BeanUtil.lookup(MipssEntityService.BEAN_NAME, MipssEntityService.class);
        }
        
        return entityServiceBean;
    }
	
	/** {@inheritDoc} */
	@Override
	@SuppressWarnings("unchecked")
    public void receiveMessage(PluginMessage message) {
        if(message instanceof LevHarUlagredeEndringerMessage) {
        	LevHarUlagredeEndringerMessage levHarUlagredeEndringerMessage = (LevHarUlagredeEndringerMessage)message;
        	levHarUlagredeEndringerMessage.setUlagredeEndringer(saveStatusRegistry.isChanged());
        	levHarUlagredeEndringerMessage.processMessage();
        	return;
        } else if(message instanceof RefreshLeverandorMessage) {
        	Driftkontrakt driftkontrakt = driftkontraktController.getCurrentDriftkontrakt();
        	driftkontraktController.setCurrentDriftkontrakt(null);
       		driftkontraktController.lastKontraktForAdministrasjon(driftkontrakt, false);
        	message.processMessage();
        	return;
        } else if(!(message instanceof OpenDriftAdminMessage)) {
            throw new IllegalArgumentException("Unsupported message type");
        }
        
        if(this != message.getTargetPlugin()) {
            throw new IllegalStateException("Invalid plugin receiver for target");
        }
        
        message.processMessage();
    }
	
	public void setPanel(String panel) {
		if(getStatus().equals(Status.RUNNING)) {
			driftkontraktController.visPanel(panel);
		}
		
		this.panel = panel;
	}
}
