package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.driftkontrakt.util.DateUtil;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.JMipssDatePicker;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.ProdtypeSelectableObject;
import no.mesta.mipss.ui.table.RodeSelectableObject;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class LeggTilLevProsessRodePanel extends AbstractRelasjonPanel<List<LevProdtypeRode>> implements PropertyChangeListener {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlFiltrerRodetype = new JPanel();
    private JPanel pnlVelgRode = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    private JScrollPane scrlPaneRode = new JScrollPane();
    private JLabel lblRodetype = new JLabel();
    private JComboBox cbxRodetype = new JComboBox();
    private JPanel pnlvelgProsess = new JPanel();
    private GridBagLayout gridBagLayout5 = new GridBagLayout();
    private JScrollPane scrlPaneProsess = new JScrollPane();
    private JCheckBox chkVelgAlleRoder = new JCheckBox();
    private JCheckBox chkVelgAlleProsesser = new JCheckBox();
    private JXTable tblRode = new JXTable();
    private JXTable tblProsess = new JXTable();
    private JLabel lblFraDato = new JLabel();
    private JMipssDatePicker dtPckrDatoFra;
    private JLabel lblTilDato = new JLabel();
    private JMipssDatePicker dtPckrDatoTil;
    private JPanel pnlDatoer = new JPanel();
    private GridBagLayout gridBagLayout6 = new GridBagLayout();
    
    private LeverandorController leverandorController;
    private BindingGroup bindingGroup;
    private MipssBeanTableModel<RodeSelectableObject> rodeModel;
    private MipssBeanTableModel<ProdtypeSelectableObject> prodtypeModel;
    
    public LeggTilLevProsessRodePanel(LeverandorController leverandorController) {
    	this.leverandorController = leverandorController;
    	
    	bindingGroup = new BindingGroup();
    	
    	initComponents();
    	initDatePckrs();
    	initGui();
    	
    	bindingGroup.bind();
    }
    
    private void initDatePckrs() {
    	KontraktLeverandor kontraktLeverandor = leverandorController.getCurrentLeverandor();
    	
		Locale loc = new Locale("no", "NO");
		
		dtPckrDatoFra = new JMipssDatePicker(loc);
		dtPckrDatoTil = new JMipssDatePicker(loc);
		dtPckrDatoFra.pairWith(dtPckrDatoTil);
		
		dtPckrDatoFra.addPropertyChangeListener(this);
		dtPckrDatoTil.addPropertyChangeListener(this);
		
		if(kontraktLeverandor != null) {
			dtPckrDatoFra.setDate(kontraktLeverandor.getFraDato());
			dtPckrDatoTil.setDate(kontraktLeverandor.getTilDato());
		}
    }
    
    private void initComponents() {
    	rodeModel = new MipssBeanTableModel<RodeSelectableObject>(RodeSelectableObject.class, new int[]{0}, "valgt", "navn");
    	MipssRenderableEntityTableColumnModel columnModelRoder = new MipssRenderableEntityTableColumnModel(RodeSelectableObject.class, rodeModel.getColumns());
    	
    	tblRode.setModel(rodeModel);
    	tblRode.setColumnModel(columnModelRoder);
    	
    	tblRode.getColumn(0).setMaxWidth(50);
    	rodeModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				updateComplete();
			}
    	});
    	
    	prodtypeModel = new MipssBeanTableModel<ProdtypeSelectableObject>(ProdtypeSelectableObject.class, new int[]{0}, "valgt", "navn", "sesong");
    	MipssRenderableEntityTableColumnModel columnModelProdtyper = new MipssRenderableEntityTableColumnModel(ProdtypeSelectableObject.class, prodtypeModel.getColumns());
    	
    	prodtypeModel.setEntities(leverandorController.getProdtypeList());
    	tblProsess.setModel(prodtypeModel);
    	tblProsess.setColumnModel(columnModelProdtyper);
    	
    	tblProsess.getColumn(0).setMaxWidth(50);
    	prodtypeModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				updateComplete();
			}
    	});
    	
    	List<Rodetype> rodetyper = leverandorController.getRodetypeForKontrakt();
        MipssComboBoxModel<Rodetype> cbxModelRodetypeModel = new MipssComboBoxModel<Rodetype>(rodetyper);
        cbxRodetype.setModel(cbxModelRodetypeModel);
        cbxRodetype.setRenderer(new MipssListCellRenderer<Rodetype>());
        cbxRodetype.setAction(leverandorController.getRodetypeAction(rodeModel, cbxModelRodetypeModel));
        cbxRodetype.getAction().actionPerformed(null);
        
        chkVelgAlleRoder.setAction(leverandorController.getVelgAlleRoderAction(chkVelgAlleRoder, rodeModel));
        chkVelgAlleProsesser.setAction(leverandorController.getVelgAlleProdtyperAction(chkVelgAlleProsesser, prodtypeModel));
    }
    
    private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlFiltrerRodetype.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgRodetype")));
        pnlFiltrerRodetype.setLayout(gridBagLayout3);
        pnlVelgRode.setLayout(gridBagLayout4);
        pnlVelgRode.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgRode")));
        lblRodetype.setText(ResourceUtils.getResource("label.rodetype"));
        cbxRodetype.setPreferredSize(new Dimension(120, 21));
        pnlvelgProsess.setLayout(gridBagLayout5);
        pnlvelgProsess.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgProdtype")));
        chkVelgAlleRoder.setText(ResourceUtils.getResource("label.alleRoder"));
        chkVelgAlleProsesser.setText(ResourceUtils.getResource("label.alleProdtyper"));
        tblRode.setPreferredScrollableViewportSize(new Dimension(0, 0));
        scrlPaneRode.getViewport().add(tblRode, null);
        tblProsess.setPreferredScrollableViewportSize(new Dimension(0, 0));
        scrlPaneProsess.getViewport().add(tblProsess, null);
        lblFraDato.setText(ResourceUtils.getResource("label.fraDato"));
        lblTilDato.setText(ResourceUtils.getResource("label.tilDato"));
        pnlDatoer.setLayout(gridBagLayout6);
        pnlDatoer.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.velgTidsperiode")));
        pnlFiltrerRodetype.add(lblRodetype, 
                new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                       new Insets(0, 5, 5, 0), 0, 
                                       0));
		pnlFiltrerRodetype.add(cbxRodetype, 
		                new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
		                                       new Insets(0, 5, 5, 0), 0, 
		                                       0));
		pnlMain.add(pnlFiltrerRodetype, 
		         new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
		                                new Insets(0, 0, 0, 0), 0, 0));
		pnlVelgRode.add(scrlPaneRode, 
		             new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
		                                    new Insets(0, 5, 5, 5), 0, 0));
		pnlVelgRode.add(chkVelgAlleRoder, 
		             new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
		                                    new Insets(0, 5, 5, 5), 0, 0));
		pnlMain.add(pnlVelgRode, 
		         new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
		                                GridBagConstraints.BOTH, 
		                                new Insets(0, 0, 0, 0), 0, 0));
		pnlvelgProsess.add(scrlPaneProsess, 
		                new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
		                                       new Insets(0, 5, 5, 5), 0, 
		                                       0));
		pnlvelgProsess.add(chkVelgAlleProsesser, 
		                new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
		                                       new Insets(0, 5, 5, 5), 0, 
		                                       0));
		pnlMain.add(pnlvelgProsess, 
		         new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, 
		                                GridBagConstraints.CENTER, 
		                                GridBagConstraints.BOTH, 
		                                new Insets(0, 0, 0, 0), 0, 0));
		pnlDatoer.add(lblFraDato, 
                new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                       new Insets(0, 5, 5, 5), 0, 
                                       0));
		pnlDatoer.add(lblTilDato, 
                new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                       new Insets(0, 5, 5, 5), 0, 
                                       0));
		pnlDatoer.add(dtPckrDatoFra, 
                new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                       new Insets(0, 5, 5, 5), 0, 
                                       0));
		pnlDatoer.add(dtPckrDatoTil, 
                new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                       new Insets(0, 5, 5, 5), 0, 
                                       0));
		pnlMain.add(pnlDatoer, 
		         new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, 
		                                GridBagConstraints.CENTER, 
		                                GridBagConstraints.BOTH, 
		                                new Insets(0, 0, 0, 0), 0, 0));
		this.add(pnlMain, 
		      new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
		                             GridBagConstraints.BOTH, 
		                             new Insets(11, 11, 11, 11), 0, 0));
    }
    
    private void updateComplete() {
    	List<Rode> rodeList = getSelectedRoder();
    	List<Prodtype> prodtypeList = getSelectedProdtyper();
    	boolean datesCorrect = checkDates();
    	
    	setComplete(rodeList != null && rodeList.size() > 0 && prodtypeList != null && prodtypeList.size() > 0 && datesCorrect);
    }
    
    private List<Rode> getSelectedRoder() {
    	List<RodeSelectableObject> rodeSelectableObjactList = rodeModel.getEntities();
    	List<Rode> rodeList = new ArrayList<Rode>();
    	if(rodeList != null) {
    		for(RodeSelectableObject rode : rodeSelectableObjactList) {
    			if(rode.getValgt().booleanValue()) {
    				rodeList.add(rode.getRode());
    			}
    		}
    	}
    	
    	return rodeList;
    }
    
    private List<Prodtype> getSelectedProdtyper() {
    	List<ProdtypeSelectableObject> prodtypeSelectableObjactList = prodtypeModel.getEntities();
    	List<Prodtype> prodtypeList = new ArrayList<Prodtype>();
    	if(prodtypeList != null) {
    		for(ProdtypeSelectableObject prodtype : prodtypeSelectableObjactList) {
    			if(prodtype.getValgt().booleanValue()) {
    				prodtypeList.add(prodtype.getProdtype());
    			}
    		}
    	}
    	
    	return prodtypeList;
    }
    
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension getDialogSize() {
		return new Dimension(500,700);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<LevProdtypeRode> getNyRelasjon() {
		List<LevProdtypeRode> levProdtypeRodeList = new ArrayList<LevProdtypeRode>();
		List<Rode> rodeList = getSelectedRoder();
		List<Prodtype> prodtypeList = getSelectedProdtyper();
    	Date datoFra = dtPckrDatoFra.getDate();
    	Date datoTil = dtPckrDatoTil.getDate();
    	
		for(Rode rode : rodeList) {
			for(Prodtype prodtype : prodtypeList) {
				LevProdtypeRode levProdtypeRode = new LevProdtypeRode();
				levProdtypeRode.setLeverandor(leverandorController.getCurrentLeverandor().getLeverandor());
				levProdtypeRode.setRode(rode);
				levProdtypeRode.setProdtype(prodtype);
				levProdtypeRode.setFraDato(new Timestamp(datoFra.getTime()));
				levProdtypeRode.setTilDato(new Timestamp(datoTil.getTime()));
				levProdtypeRode.getOwnedMipssEntity().setOpprettetDato(Clock.now());
				levProdtypeRode.getOwnedMipssEntity().setOpprettetAv(leverandorController.getLoggedOnUserSign());
				levProdtypeRodeList.add(levProdtypeRode);
			}
		}
		
		return levProdtypeRodeList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilRode");
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateComplete();
	}
	
	private boolean checkDates() {
    	Date datoFra = dtPckrDatoFra.getDate();
    	Date datoTil = dtPckrDatoTil.getDate();
    	
    	if(datoFra != null && datoTil != null) {
    		return datoTil.after(datoFra);
    	}
    	
    	return false;
	}

	@Override
	public boolean isLegal() {
		boolean isLegal = true;
		List<LevProdtypeRode> nyRelasjonList = getNyRelasjon();
		List<LevProdtypeRode> eksisterendeList = leverandorController.getCurrentLeverandor().getLeverandor().getLevProdtypeRodeList();
		
		class Key {
			String leverandorNr;
			Long rodeId;
			Long prodtypeId;
			
			public Key(String leverandorNr, Long rodeId, Long prodtypeId) {
				this.leverandorNr = leverandorNr;
				this.rodeId = rodeId;
				this.prodtypeId = prodtypeId;
			}
			
			@Override
		    public int hashCode() {
		        return new HashCodeBuilder().append(leverandorNr).append(rodeId).append(prodtypeId).toHashCode();
		    }
			
			public boolean equals(Object o) {
		        if(o == null) {return false;}
		        if(o == this) {return true;}
		        if(!(o instanceof Key)) {return false;}
		        Key other = (Key) o;
		        return new EqualsBuilder().append(leverandorNr, other.leverandorNr).append(rodeId, other.rodeId).append(prodtypeId, other.prodtypeId).isEquals();
		    }
		}
		
		HashMap<Key, LevProdtypeRode> hashMap = new HashMap<Key, LevProdtypeRode>();
		for(LevProdtypeRode levProdtypeRode: eksisterendeList) {
			Key key = new Key(levProdtypeRode.getLeverandorNr(), levProdtypeRode.getRodeId(), levProdtypeRode.getProdtypeId());
			hashMap.put(key, levProdtypeRode);
		}
		
		for(LevProdtypeRode levProdtypeRode : nyRelasjonList) {
			Key key = new Key(levProdtypeRode.getLeverandorNr(), levProdtypeRode.getRodeId(), levProdtypeRode.getProdtypeId());
			if(hashMap.containsKey(key)) {
				LevProdtypeRode eksLevProdtypeRode = hashMap.get(key);
				if(DateUtil.overlappingPeriods(eksLevProdtypeRode.getFraDato(), 
						eksLevProdtypeRode.getTilDato(), 
						levProdtypeRode.getFraDato(), 
						levProdtypeRode.getTilDato())) {
					isLegal = false;
					break;
				}
				
			}
		}
		
		if(!isLegal) {
			JOptionPane.showMessageDialog(getParentDialog(), ResourceUtils.getResource("message.rodeOverlappendePeriode"));
		}
		
		return isLegal;
	}
}
