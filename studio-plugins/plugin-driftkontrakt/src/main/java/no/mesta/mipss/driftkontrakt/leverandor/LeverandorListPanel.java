package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter.HighlightFactory;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.EpostadresseTableHelper;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.decorator.Filter;
import org.jdesktop.swingx.decorator.FilterPipeline;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.PatternFilter;

@SuppressWarnings("serial")
public class LeverandorListPanel extends JPanel implements ListSelectionListener {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JScrollPane scrlpaneKontraktLeverandor = new JScrollPane();
    private JPanel pnlButtons = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JButton btnLeggTil = new JButton();
    private JButton btnSlett = new JButton();
    private JCheckBox chkSkjulInaktiveLeverandorer;
    private JMipssBeanTable<KontraktLeverandor> tblLeverandor;
    private PropertyChangeSupport props = new PropertyChangeSupport(this);
    MipssBeanTableModel<KontraktLeverandor> leverandorModel;
	
	private DriftkontraktController driftkontraktController;
	private LeverandorController leverandorController;
	private BindingGroup bindingGroup;
	
	public LeverandorListPanel(LeverandorController leverandorController) {
		this.driftkontraktController = leverandorController.getDriftkontraktController();
		this.leverandorController = leverandorController;
		
		bindingGroup = new BindingGroup();
		
		initTable();
		initComponents();
		initGui();
		
		bindingGroup.bind();		
	
	}
	
	private void initComponents() {
		driftkontraktController.registerChangableField("kontraktLeverandorList");
		btnLeggTil.setAction(driftkontraktController.getLeggTilLeverandorAction());
		btnLeggTil.setIcon(IconResources.ADD_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnLeggTil, "enabled"));
		
		btnSlett.setAction(driftkontraktController.getSlettLeverandorAction(leverandorModel, tblLeverandor));
		btnSlett.setIcon(IconResources.REMOVE_ITEM_ICON);
		bindingGroup.addBinding(BindingHelper.createbinding(tblLeverandor, "${noOfSelectedRows > 0}", btnSlett, "enabled"));
		
		chkSkjulInaktiveLeverandorer = new JCheckBox(ResourceUtils.getResource("label.skjulInaktiveLeverandorer"));
		chkSkjulInaktiveLeverandorer.setSelected(true);
		bindingGroup.addBinding(BindingHelper.createbinding(chkSkjulInaktiveLeverandorer, "selected", this, "inaktiveLeverandorerCheckbox"));
		
	}
	
	private void initTable() {
		leverandorModel = new MipssBeanTableModel<KontraktLeverandor>("kontraktLeverandorList", KontraktLeverandor.class, "leverandorNr", "leverandor", "epost", "kopiEpost", "fraDato", "tilDato", "tlfnummer", "antallKjoretoy", "inaktivDato");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "currentDriftkontrakt", leverandorModel, "sourceObject", BindingHelper.READ));
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(KontraktLeverandor.class, leverandorModel.getColumns());
		tblLeverandor = new JMipssBeanTable<KontraktLeverandor>(leverandorModel, columnModel);
		tblLeverandor.setFillsViewportWidth(true);
		tblLeverandor.setDoWidthHacks(false);
				
		tblLeverandor.getColumnExt(8).setToolTipText(ResourceUtils.getResource("tooltip.agressoTilDato"));
		
		tblLeverandor.setDefaultRenderer(Leverandor.class, new RenderableMipssEntityTableCellRenderer<Leverandor>());
		
		Comparator<String> numberComparator = new Comparator<String>() {
	        public int compare(String o1, String o2) {
	            Double d1 = null;
	            Double d2 = null;
	            try{ d1 = Double.valueOf(o1 == null ? "0" : o1);} catch (NumberFormatException e){d1 = Double.valueOf(0);}
	            try{ d2 = Double.valueOf(o2 == null ? "0" : o2);} catch (NumberFormatException e){d2 = Double.valueOf(0);}
	            return d1.compareTo(d2);
	        }
	    };
	    tblLeverandor.getColumnExt(0).setComparator(numberComparator);
	    
	    Comparator<IRenderableMipssEntity> leverandorComparator = new Comparator<IRenderableMipssEntity>() {
	        public int compare(IRenderableMipssEntity o1, IRenderableMipssEntity o2) {
	            String text1 = null;
	            String text2 = null;
	        	if (o1!=null)
	        		text1 = o1.getTextForGUI();
	        	else
	        		text1 = "";
	        	if (o2!=null){
	        		text2 = o2.getTextForGUI();
	        	}else
	        		text2 = "";
	            return text1.compareTo(text2);
	        }
	    };
	    tblLeverandor.getColumnExt(1).setComparator(leverandorComparator);

		tblLeverandor.getSelectionModel().addListSelectionListener(this);
		bindingGroup.addBinding(BindingHelper.createbinding(this, "selectedLeverandor", leverandorController, "currentLeverandor", BindingHelper.READ));
		
		EpostadresseTableHelper.prepareTable(tblLeverandor);
		
		tblLeverandor.setHighlighters(new Highlighter[] { 
				HighlightFactory.getLeverandorUtenKjoretoyHighlighter(tblLeverandor, leverandorModel),
				HighlightFactory.getLeverandorMipssInaktivHighlighter(tblLeverandor, leverandorModel),
				HighlightFactory.getLeverandorInaktivHighlighter(tblLeverandor, leverandorModel)
				
		});
		
		//Lytter som benyttes for å kjøre igang filteret på tabellen.
		leverandorModel.addPropertyChangeListener(new PropertyChangeListener() {		
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				chkSkjulInaktiveLeverandorer.setSelected(false);
				chkSkjulInaktiveLeverandorer.setSelected(true);
			}
		});
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlButtons.setLayout(gridBagLayout3);
        scrlpaneKontraktLeverandor.getViewport().add(tblLeverandor, null);
        this.add(scrlpaneKontraktLeverandor,  new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        pnlButtons.add(btnLeggTil, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlButtons.add(btnSlett, new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        pnlButtons.add(chkSkjulInaktiveLeverandorer, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        this.add(pnlButtons, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 5, 0, 0), 0, 0));
	}
	
	public KontraktLeverandor getSelectedLeverandor() {
		if(tblLeverandor.getSelectedRowCount() == 1) {
			int i = tblLeverandor.getSelectedRow();
			if(i < tblLeverandor.getRowCount()) {
				return leverandorModel.get(tblLeverandor.convertRowIndexToModel(i));
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public void setSelectedLeverandor(KontraktLeverandor kontraktLeverandor) {
		if(kontraktLeverandor == null) {
			tblLeverandor.clearSelection();
		}
	}
	
	public void setInaktiveLeverandorerCheckbox(boolean valgt){
		tblLeverandor.clearSelection();

		if (valgt) {
			//Skjuler alle inaktive leverandører
			//Filteret fungerer slik at det beholder alle Leverandører som har Mipss til-dato satt til etter dagens dato.

			ArrayList<String> patternList = new ArrayList<String>();

			for (int i = 0; i < tblLeverandor.getRowCount(); i++) {

				String leverandorNummer = (String) tblLeverandor.getValueAt(i, 0);

				//Agresso til-dato
				if (tblLeverandor.getValueAt(i, 8) != null) {
					if (checkDateAndReturnLevNr(new Date(((Timestamp) tblLeverandor.getValueAt(i, 8)).getTime()))) {
						patternList.add(leverandorNummer);
					}
				}
				//Mipss til-dato
				else if (tblLeverandor.getValueAt(i, 5) != null) {
					if (checkDateAndReturnLevNr((Date)tblLeverandor.getValueAt(i, 5))){
						patternList.add(leverandorNummer);
					}
				}
			}		

			StringBuilder pattern = new StringBuilder();

			if (patternList.isEmpty()) {
				pattern.append("ingen");
			}
			for (int i = 0; i < patternList.size(); i++) {
				if (i != 0) {
					pattern.append("|");
				}
				pattern.append(patternList.get(i));
			}			

			PatternFilter filter = new PatternFilter(pattern.toString(), 0, 0);
			Filter[] filterArray = { filter };
			FilterPipeline filters = new FilterPipeline(filterArray);
			tblLeverandor.setFilters(filters);
		}
		else {
			//Viser alle leverandører
			tblLeverandor.setFilters(null);
		}
	}

	private Boolean checkDateAndReturnLevNr(Date date){
		if (date != null) {
			if (date.after(Clock.now())) {
				return true;
			}
		}		
		return false;	
	}
	

	@Override
	public void valueChanged(ListSelectionEvent e) {
		props.firePropertyChange("selectedLeverandor", getSelectedLeverandor(), null);
	}
	
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
