package no.mesta.mipss.driftkontrakt;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.kontrakt.Kontraktlogg;

import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;

public class KontraktloggDocument extends PlainDocument implements ObservableListListener {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private List<Kontraktlogg> loggList;
	
	public KontraktloggDocument() {
	}
	
	public List<Kontraktlogg> getLoggList() {
		return loggList;
	}

	public void setLoggList(List<Kontraktlogg> loggList) {
		Object old = this.loggList;
		unListen();
		this.loggList = loggList;
		listen();
		buildString();
		props.firePropertyChange("loggList", old, loggList);
	}
	
	private void listen() {
		if(loggList != null && loggList instanceof MipssObservableList) {
			((MipssObservableList<Kontraktlogg>)loggList).addObservableListListener(this);
		}
	}
	
	private void unListen() {
		if(loggList != null && loggList instanceof MipssObservableList) {
			((MipssObservableList<Kontraktlogg>)loggList).removeObservableListListener(this);
		}
	}
	
	private void buildString() {
		StringBuffer strBuff = new StringBuffer();
		
		if(loggList != null) {
			for(Kontraktlogg k : loggList) {
				strBuff.append(stringForLoggEntry(k));
			}
		} else {
			
		}
		
		try {
			remove(0, getLength());
			insertString(0, strBuff.toString(), null);
		} catch (BadLocationException ble) {
			ble.printStackTrace();
		}
	}
	
	private StringBuffer stringForLoggEntry(Kontraktlogg k) {
		StringBuffer sb = new StringBuffer();
		sb.append(MipssDateFormatter.formatDate(k.getOpprettetDato(), true) + "\n");
		sb.append(k.getOpprettetAv() + "\n");
		sb.append(k.getFritekst() + "\n\n");
		
		return sb;
	}
	
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }

	@SuppressWarnings("unchecked")
	@Override
	public void listElementPropertyChanged(ObservableList arg0, int arg1) {
		buildString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void listElementReplaced(ObservableList arg0, int arg1, Object arg2) {
		buildString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void listElementsAdded(ObservableList arg0, int arg1, int arg2) {
		buildString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void listElementsRemoved(ObservableList arg0, int arg1, List arg2) {
		buildString();
	}
}
