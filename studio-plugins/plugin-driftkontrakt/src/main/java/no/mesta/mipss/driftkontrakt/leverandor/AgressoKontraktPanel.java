package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class AgressoKontraktPanel extends JPanel{
	
	private final LeverandorController leverandorController;
	
	private MipssBeanTableModel<Levkontrakt> mdlKontrakt;
	private JMipssBeanTable<Levkontrakt> tblKontrakt;
	private JScrollPane scrKontrakt;
	
    private JButton btnLeggTil;
    private JButton btnSlett;
	private JButton btnArtikler;

    private BindingGroup bindings = new BindingGroup();
    
	public AgressoKontraktPanel(LeverandorController leverandorController){
		this.leverandorController = leverandorController;
		initComponents();
		
		initGui();
		bind();
		
	}

	private void bind() {
		leverandorController.registerChangableField("levkontraktList");
		Binding<LeverandorController, Object, MipssBeanTableModel<Levkontrakt>, Object> kontraktBind = BindingHelper.createbinding(leverandorController, "currentLeverandor", mdlKontrakt, "sourceObject", BindingHelper.READ);
		Binding<JMipssBeanTable<Levkontrakt>, Object, JButton, Object> enableSlett = BindingHelper.createbinding(tblKontrakt, "${noOfSelectedRows > 0}", btnSlett, "enabled");
		Binding<LeverandorController, Object, JButton, Object> enableLeggtil = BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", btnLeggTil, "enabled");
		Binding<JMipssBeanTable<Levkontrakt>, Object, JButton, Object> enableArtikler = BindingHelper.createbinding(tblKontrakt, "${noOfSelectedRows == 1}", btnArtikler, "enabled");
		bindings.addBinding(kontraktBind);
		bindings.addBinding(enableLeggtil);
		bindings.addBinding(enableSlett);
		bindings.addBinding(enableArtikler);
		bindings.bind();
	}
	
	private void initComponents() {
		mdlKontrakt = new MipssBeanTableModel<Levkontrakt>("levkontraktList", Levkontrakt.class, "levkontraktIdent", "fraDato");
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(Levkontrakt.class, mdlKontrakt.getColumns());
		tblKontrakt = new JMipssBeanTable<Levkontrakt>(mdlKontrakt, columnModel);
		tblKontrakt.setFillsViewportWidth(true);
		tblKontrakt.setDoWidthHacks(false);
		scrKontrakt  = new JScrollPane(tblKontrakt);
		
		btnLeggTil = new JButton(leverandorController.leggTilLevkontrakt());
		btnSlett = new JButton(leverandorController.getSlettLevkontrakt(mdlKontrakt, tblKontrakt));
		btnArtikler = new JButton(leverandorController.administrerArtikkelProdtype(mdlKontrakt, tblKontrakt));
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
        pnlButtons.add(btnLeggTil, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        pnlButtons.add(btnSlett, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnArtikler, new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        this.add(scrKontrakt,  new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(pnlButtons, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 5, 0, 5), 0, 0));

	}

}
