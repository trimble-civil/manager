package no.mesta.mipss.driftkontrakt.kjoretoy;


public interface ColorableTableValue {
	boolean isRed();
	boolean isYellow();
	String getTooltipText();
	Object getValue();
}
