package no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter;

import java.awt.Component;

import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

public class LeverandorUtenKjoretoyHighlighter implements HighlightPredicate{

	private final JMipssBeanTable<KontraktLeverandor> table;
	private final MipssBeanTableModel<KontraktLeverandor> model;

	public LeverandorUtenKjoretoyHighlighter(JMipssBeanTable<KontraktLeverandor> table, MipssBeanTableModel<KontraktLeverandor> model){
		this.table = table;
		this.model = model;
		
	}

	@Override
	public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
		KontraktLeverandor kl = model.get(table.convertRowIndexToModel(adapter.row));
		if (kl.getAntallKjoretoy()!=null&&kl.getAntallKjoretoy()==0){
			return true;
		}
		return false;
	}
}