package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

public class DriftkontraktOppsettPanel extends JPanel {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlRapportOppsett = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblEpostRodetype = new JLabel();
    private JComboBox cbxEpostRodetype = new JComboBox();
    private JLabel lblOvertekst = new JLabel();
    private JLabel lblUndertekst = new JLabel();
    private JScrollPane scrlPaneOvertekst = new JScrollPane();
    private JScrollPane scrlPaneUndertekst = new JScrollPane();
    private JTextArea txtAreaOvertekst = new JTextArea();
    private JTextArea txtAreaUndertekst = new JTextArea();
	
	private DriftkontraktController driftkontraktController;
    private BindingGroup bindingGroup;
	
	public DriftkontraktOppsettPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		
		bindingGroup = new BindingGroup();
		
		initGui();
		initTextFields();
		initComboBox();
		
		bindingGroup.bind();
		if (!UserUtils.isAdmin(driftkontraktController.getDriftkontraktAdminModule().getLoader().getLoggedOnUser(true))){
			cbxEpostRodetype.setVisible(false);
		}

	}
	
	private void initTextFields() {
		driftkontraktController.registerChangableField("rodetype", "epostEkstraOvertekstHtml", "epostEkstraUndertekstHtml", "kontraktKontaktypeList");
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.epostEkstraOvertekstHtml}", txtAreaOvertekst, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", txtAreaOvertekst, "enabled", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt.epostEkstraUndertekstHtml}", txtAreaUndertekst, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", txtAreaUndertekst, "enabled", BindingHelper.READ));
	}
	
	private void initComboBox() {
		final MipssComboBoxModel<Rodetype> cbxModelEpostRodetype = new MipssComboBoxModel<Rodetype>(new ArrayList<Rodetype>(), true);
		
		final PropertyChangeListener updateKontraktListener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(driftkontraktController.getCurrentDriftkontrakt() != null) {
					driftkontraktController.getCurrentDriftkontrakt().setRodetype((Rodetype)evt.getNewValue());
				}
			}
		};
		
		cbxModelEpostRodetype.addPropertyChangeListener(updateKontraktListener);
		
		driftkontraktController.addPropertyChangeListener("currentDriftkontrakt", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(driftkontraktController.getCurrentDriftkontrakt() != null) {
					cbxModelEpostRodetype.removePropertyChangeListener(updateKontraktListener);
					cbxModelEpostRodetype.setEntities(driftkontraktController.getRodetypeList(driftkontraktController.getCurrentDriftkontrakt().getId()));
					cbxModelEpostRodetype.setSelectedItem(driftkontraktController.getCurrentDriftkontrakt().getRodetype());
					cbxModelEpostRodetype.addPropertyChangeListener(updateKontraktListener);
					if (!UserUtils.isAdmin(driftkontraktController.getDriftkontraktAdminModule().getLoader().getLoggedOnUser(true))){
						if (driftkontraktController.getCurrentDriftkontrakt().getRodetype()!=null){
							lblEpostRodetype.setText(ResourceUtils.getResource("label.rodetypeForEpost")+" "+driftkontraktController.getCurrentDriftkontrakt().getRodetype().getNavn());
						}
					}
				}
			}
		});
		
		cbxEpostRodetype.setModel(cbxModelEpostRodetype);
		cbxEpostRodetype.setRenderer(new MipssListCellRenderer<Rodetype>());
		
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", cbxEpostRodetype, "enabled", BindingHelper.READ));
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlRapportOppsett.setLayout(new GridBagLayout());
        pnlRapportOppsett.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.rapportoppsett")));
        lblEpostRodetype.setText(ResourceUtils.getResource("label.rodetypeForEpost"));
        cbxEpostRodetype.setPreferredSize(new Dimension(160, 21));
        cbxEpostRodetype.setMinimumSize(new Dimension(160, 21));
        lblOvertekst.setText(ResourceUtils.getResource("label.html1"));
        lblUndertekst.setText(ResourceUtils.getResource("label.html2"));
        
        txtAreaOvertekst.setWrapStyleWord(true);
        txtAreaOvertekst.setLineWrap(true);
        txtAreaUndertekst.setWrapStyleWord(true);
        txtAreaUndertekst.setLineWrap(true);
        
        scrlPaneOvertekst.setViewportView(txtAreaOvertekst);
        scrlPaneUndertekst.setViewportView(txtAreaUndertekst);
        scrlPaneOvertekst.setPreferredSize(new Dimension(160, 100));
        scrlPaneOvertekst.setMinimumSize(new Dimension(160, 100));
        scrlPaneUndertekst.setPreferredSize(new Dimension(160, 100));
        scrlPaneUndertekst.setMinimumSize(new Dimension(160, 100));
        
        pnlRapportOppsett.add(lblEpostRodetype, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 11, 5), 0, 0));
        pnlRapportOppsett.add(cbxEpostRodetype, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 11, 5), 0, 0));
        pnlRapportOppsett.add(lblOvertekst, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlRapportOppsett.add(lblUndertekst, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
        pnlRapportOppsett.add(scrlPaneOvertekst, new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 11, 5), 0, 0));
        pnlRapportOppsett.add(scrlPaneUndertekst, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
        
        pnlMain.add(pnlRapportOppsett, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}
}
