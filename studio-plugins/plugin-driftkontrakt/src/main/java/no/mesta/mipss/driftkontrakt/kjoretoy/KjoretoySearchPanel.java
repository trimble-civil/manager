package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class KjoretoySearchPanel extends JPanel{

	private boolean cancelled = true;
	private JButton btnSearch = new JButton();
	private JButton btnVelg = new JButton();
	private JButton btnAvbryt = new JButton();
	private JTextField fieldSearch = new JTextField();
	private JXTable searchResults = new JXTable();
	private JXBusyLabel busyLabel = new JXBusyLabel();
	private JScrollPane scrollResults = new JScrollPane();
	private JLabel labelSearch = new JLabel();
	private MipssBeanTableModel<KjoretoyListV> searchModel;
	private final DriftkontraktController controller;
	
	public static void main(String[] args) {
		JDialog d = new JDialog();
		d.add(new KjoretoySearchPanel(null));
		d.pack();
		d.setLocationRelativeTo(null);
		d.setVisible(true);
	}
	public KjoretoySearchPanel(DriftkontraktController controller){
		
		this.controller = controller;
		searchModel = new MipssBeanTableModel<KjoretoyListV>(KjoretoyListV.class, "regnr", "maskinnummerString", "typeNavn", "eierNavn", "modell", "ansKontrakt");
		searchModel.setBeanInstance(controller);
		searchModel.setBeanMethod("searchForKjoretoy");
		searchModel.setBeanInterface(DriftkontraktController.class);
		searchModel.setBeanMethodArguments(new Class[]{String.class});
		searchModel.addTableLoaderListener(getTableLoaderListener());
		searchResults.setModel(searchModel);
		MipssRenderableEntityTableColumnModel searchColumnModel = new MipssRenderableEntityTableColumnModel(KjoretoyListV.class, searchModel.getColumns());
		searchResults.setColumnModel(searchColumnModel);
		searchResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		initGui();
	}

	private void initGui() {
		setLayout(new GridBagLayout());
		busyLabel.setText(ResourceUtils.getResource("label.sokerKjoretoy"));
		labelSearch.setText(ResourceUtils.getResource("label.sokFritekst"));
		btnSearch.setAction(getSearchAction());
		btnAvbryt.setAction(getAvbrytAction());
		btnVelg.setAction(getVelgAction());
		fieldSearch.setPreferredSize(new Dimension(180, 21));
		fieldSearch.setMinimumSize(new Dimension(180, 21));
		fieldSearch.setAction(getSearchAction());
		
		JPanel pnlScroll = new JPanel(new BorderLayout());
		pnlScroll.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.sokResultat")));
		pnlScroll.add(scrollResults, BorderLayout.CENTER);
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		pnlButtons.add(btnVelg,	new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		pnlButtons.add(btnAvbryt,		new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		
		add(labelSearch, 	new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 5, 5), 0, 0));
		add(fieldSearch, 	new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0, 5, 15), 0, 0));
		add(btnSearch, 	 	new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0, 5, 0), 0, 0));
		add(pnlScroll,		new GridBagConstraints(0, 1, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		add(pnlButtons,		new GridBagConstraints(0, 2, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
	}
	
	public void reload(){
		btnSearch.doClick();
	}
	
	public void setBusy(boolean busy){
		busyLabel.setVisible(busy);
		busyLabel.setBusy(busy);
		searchResults.setVisible(!busy);
		scrollResults.setViewportView(busy?busyLabel:searchResults);
		repaint();
	}
	public boolean isCancelled(){
		return cancelled;
	}
	public KjoretoyListV getValgtKjoretoy(){
		int idx = searchResults.getSelectedRow();
		if (idx==-1)
			return null;
		idx = searchResults.convertRowIndexToModel(idx);
		return searchModel.getEntities().get(idx);
	}
	
	private MipssBeanLoaderListener getTableLoaderListener(){
		return new MipssBeanLoaderListener(){
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				setBusy(false);
			}
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {
				setBusy(true);
			}
		};
	}
	
	private Action getAvbrytAction(){
		return new AbstractAction(ResourceUtils.getResource("button.avbryt")){
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled=true;
				JDialog d = (JDialog)SwingUtilities.windowForComponent(KjoretoySearchPanel.this);
				d.dispose();
			}
			
		};
	}
	
	private Action getVelgAction(){
		return new AbstractAction(ResourceUtils.getResource("button.velg")){
			@Override
			public void actionPerformed(ActionEvent e) {
				int idx = searchResults.getSelectedRow();
				cancelled=false;
				if (idx==-1){
					int v = JOptionPane.showConfirmDialog(controller.getDriftkontraktAdminModule().getModuleGUI(), ResourceUtils.getResource("message.ingenKjoretoyValgt.message"), ResourceUtils.getResource("message.ingenKjoretoyValgt.title"), JOptionPane.OK_CANCEL_OPTION);
					if (v!=JOptionPane.OK_OPTION){
						return;
					}
				}
				if (!opprettKontraktKontakt()){
					return;
				}
				JDialog d = (JDialog)SwingUtilities.windowForComponent(KjoretoySearchPanel.this);
				d.dispose();
			}
			
		};
	}
	private boolean opprettKontraktKontakt(){
		KjoretoyListV valgtKjoretoy = getValgtKjoretoy();
		Long kid = valgtKjoretoy.getId();
		Long did = controller.getCurrentDriftkontrakt().getId();
		Long ansvarlig = valgtKjoretoy.getAnsKontrakt().equals("")?Long.valueOf(1):Long.valueOf(0);
		boolean r = true;
		if (!controller.isKontraktKontaktPresent(did, kid)){
			controller.opprettKontraktKjoretoy(did, kid, ansvarlig);
			
		}else{
			JOptionPane.showMessageDialog(controller.getDriftkontraktAdminModule().getModuleGUI(), ResourceUtils.getResource("message.koblingEksisterer.message"), ResourceUtils.getResource("message.koblingEksisterer.title"), JOptionPane.WARNING_MESSAGE);
			r=false;
		}
		return r;
	}
	
	private Action getSearchAction(){
		return new AbstractAction(ResourceUtils.getResource("sokbutton")){
			@Override
			public void actionPerformed(ActionEvent e) {
				searchModel.setBeanMethodArgValues(new Object[]{fieldSearch.getText()});
				searchModel.loadData();
			}
			
		};
	}
	
	
}
