package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.jdesktop.beansbinding.BindingGroup;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.mipssfield.Favorittliste;
import no.mesta.mipss.ui.DocumentFilter;

@SuppressWarnings("serial")
public class FavorittlisteDialog extends JDialog {
	private JPanel pnlMain = new JPanel();
	private JLabel lblNavn = new JLabel();
	private JLabel lblBeskrivelse = new JLabel();
	private JFormattedTextField txtNavn = new JFormattedTextField();
	private JScrollPane scrlPaneBeskrivelse = new JScrollPane();
	private JTextArea txtAreaBeskrivelse = new JTextArea();
	private JPanel pnlButtons = new JPanel();
	private JButton btnOk = new JButton();
	private JButton btnAvbryt = new JButton();
	
	private BindingGroup bindingGroup;
	private Favorittliste favorittliste;
	
	public FavorittlisteDialog(Window owner, Favorittliste favorittliste) {
		super(owner, ResourceUtils.getResource("dialogTitle.favorittliste"), ModalityType.APPLICATION_MODAL);
		
		bindingGroup = new BindingGroup();
		
		this.favorittliste = new Favorittliste();
		this.favorittliste.setId(favorittliste.getId());
		this.favorittliste.merge(favorittliste);

		initLabels();
		initButtons();
		initTextFields();
		initGui();
	}
	
	public Favorittliste showDialog() {
		bindingGroup.bind();
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		bindingGroup.unbind();

		if(this.favorittliste != null && 
				this.favorittliste.getBeskrivelse() != null && 
				this.favorittliste.getBeskrivelse().length() > 255) {
			this.favorittliste.setBeskrivelse(this.favorittliste.getBeskrivelse().substring(0, 255));
		}
		
		return this.favorittliste;
	}
	
	private void initLabels() {
		lblNavn.setText(ResourceUtils.getResource("label.favorittlisteNavn"));
		lblBeskrivelse.setText(ResourceUtils.getResource("label.favorittlisteBeskrivelse"));
	}

	private void initButtons() {
		btnOk.setText(ResourceUtils.getResource("button.ok"));
		btnAvbryt.setText(ResourceUtils.getResource("button.avbryt"));
		
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnAvbryt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FavorittlisteDialog.this.favorittliste = null;
				dispose();
			}
		});
	}
	
	private void initTextFields() {
		txtNavn.setDocument(new DocumentFilter(30, null, false));
		
		bindingGroup.addBinding(BindingHelper.createbinding(favorittliste, "navn", txtNavn, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(favorittliste, "beskrivelse", txtAreaBeskrivelse, "text", BindingHelper.READ_WRITE));
	}
	
	private void initGui() {
		txtNavn.setPreferredSize(new Dimension(150, 19));		
		scrlPaneBeskrivelse.setPreferredSize(new Dimension(150, 50));
		
		scrlPaneBeskrivelse.setViewportView(txtAreaBeskrivelse);
		
		pnlMain.setLayout(new GridBagLayout());
		pnlMain.add(lblNavn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlMain.add(txtNavn, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlMain.add(lblBeskrivelse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlMain.add(scrlPaneBeskrivelse, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
		pnlButtons.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
        this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
        this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 11, 11, 11), 0, 0));
	}
}
