package no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter;

import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.*;

public class ArticleExpiredHighlighter implements HighlightPredicate{

	private final JMipssBeanTable<ArtikkelProdtype> table;
	private final MipssBeanTableModel<ArtikkelProdtype> model;

	public ArticleExpiredHighlighter(JMipssBeanTable<ArtikkelProdtype> table, MipssBeanTableModel<ArtikkelProdtype> model){
		this.table = table;
		this.model = model;
	}

	@Override
	public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
		ArtikkelProdtype artikkel = model.get(table.convertRowIndexToModel(adapter.row));
		return artikkel.isUtgatt();
	}

}