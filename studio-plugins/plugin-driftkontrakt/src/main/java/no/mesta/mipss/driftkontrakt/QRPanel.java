package no.mesta.mipss.driftkontrakt;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.util.DateTransformer;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;

@SuppressWarnings("serial")
public class QRPanel extends JPanel{

	private Date date;
	private String dateStr;
	private MultiFormatWriter writer = new MultiFormatWriter();
	private DateTransformer trafo = new DateTransformer();
	private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
	private String generatedCode;
	private BufferedImage bufferedQrkode;
	
	public QRPanel(){
		
	}
	public QRPanel(Date date){
		setDate(date);
	}
	
	@Override
	public void paint(Graphics g){
		super.paint(g);
		if (date==null){
			return;
		}
		int w = getWidth()-10;
		int h = getHeight()-10;
		int size =Math.min(w, h);
		try {
			bufferedQrkode = MatrixToImageWriter.toBufferedImage(writer.encode(generatedCode, BarcodeFormat.QR_CODE, size, size));
			g.drawImage(bufferedQrkode, (w/2)-(size/2), 5, size, size, null);
			
		} catch (WriterException e) {
			e.printStackTrace();
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.red);
			g2.setStroke(new BasicStroke(3));
			g2.drawLine(0, 0, size, size);
			g2.drawLine(size, 0, 0, size);
			String feilMld = ResourceUtils.getResource("qrkode.feil", e.getMessage());
			FontMetrics f = g2.getFontMetrics();
			int sw = f.stringWidth(feilMld);
			g2.drawString(feilMld, (size/2)-(sw/2), size/2);
		}
		
	}
	
	public void setDate(Date date){
		this.date = date;
		if (date==null){
			generatedCode = null;
			bufferedQrkode = null;
		}else{
			generatedCode = trafo.dateToString(date);
			dateStr = format.format(date);
		}
		repaint();
	}
	public BufferedImage getQrkode(){
		return bufferedQrkode;
	}
	public String getDateString(){
		return dateStr;
	}
	public String getGeneratedCode(){
		return generatedCode;
	}
}
