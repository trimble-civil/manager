package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.AgrArtikkelSelectableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class LeggTilAgressoKontraktPanel extends AbstractRelasjonPanel<List<Levkontrakt>>{

	private final LeverandorController controller;

	private JLabel lblLeverandorNr;
	private JLabel lblLeverandorNavn;
	private JLabel lblLeverandorNrValue;
	private JLabel lblLeverandorNavnValue;
	
	private MipssBeanTableModel<AgrArtikkelSelectableObject> mdlAgr;
	private JXTable tblAgr;
	private JScrollPane scrAgr;
	
	private BindingGroup bindings = new BindingGroup();
	
	public LeggTilAgressoKontraktPanel(LeverandorController controller){
		this.controller = controller;
		
		initComponents();
		initGui();
		bind();
	}

	private void bind() {
		
		
	}

	private void initComponents() {
		mdlAgr = new MipssBeanTableModel<AgrArtikkelSelectableObject>(AgrArtikkelSelectableObject.class, new int[]{0}, "valgt", "levkontraktIdent", "levkontraktBeskrivelse", "fraDato", "tilDato");
		MipssRenderableEntityTableColumnModel columnModelAgr = new MipssRenderableEntityTableColumnModel(AgrArtikkelSelectableObject.class, mdlAgr.getColumns());
		tblAgr = new JXTable(mdlAgr, columnModelAgr);
		List<AgrArtikkelSelectableObject> agrArtikkelForKontrakt = controller.getAgrArtikkelForKontrakt();
		if (agrArtikkelForKontrakt==null||agrArtikkelForKontrakt.isEmpty()){
			//TODO tom liste.. informer brukeren.
			String txt = ResourceUtils.getResource("label.ingenKontrakter");
			JLabel lblInfo = new JLabel(txt);
			scrAgr = new JScrollPane(lblInfo);
		}else{
			mdlAgr.setEntities(agrArtikkelForKontrakt);
			scrAgr = new JScrollPane(tblAgr);
		}
		mdlAgr.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				updateComplete();
			}
    	});
		lblLeverandorNr = new JLabel(ResourceUtils.getResource("label.leverandorNummer"));
		lblLeverandorNavn = new JLabel(ResourceUtils.getResource("label.leverandorNavn"));
		
		lblLeverandorNrValue = new JLabel(controller.getCurrentLeverandor().getLeverandorNr());
		lblLeverandorNavnValue = new JLabel(controller.getCurrentLeverandor().getLeverandor().getNavn());
	}
	
	private List<AgrArtikkelV> getSelectedKontrakter() {
    	List<AgrArtikkelSelectableObject> selected = mdlAgr.getEntities();
    	List<AgrArtikkelV> agrList = new ArrayList<AgrArtikkelV>();
    	if(agrList != null) {
    		for(AgrArtikkelSelectableObject agr : selected) {
    			if(agr.getValgt().booleanValue()) {
    				agrList.add(agr.getAgrArtikkel());
    			}
    		}
    	}
    	return agrList;
    }
	
	public void dispose(){
		bindings.unbind();
	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		
		JPanel pnlInfo = new JPanel(new GridBagLayout());
		pnlInfo.add(lblLeverandorNr,   		new GridBagConstraints(0,0, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11,11,5,5),0,0));
		pnlInfo.add(lblLeverandorNavn, 		new GridBagConstraints(0,1, 1,1, 0.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,11,11,5),0,0));
		pnlInfo.add(lblLeverandorNrValue,   new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11,0,5,0),0,0));
		pnlInfo.add(lblLeverandorNavnValue, new GridBagConstraints(1,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,11,0),0,0));
		pnlInfo.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("dialogTitle.leverandorinfo")));
		
		JPanel pnlAgr = new JPanel(new GridBagLayout());
		pnlAgr.add(scrAgr,   		new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(11,11,11,11),0,0));
		pnlAgr.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("dialogTitle.kontrakter")));
		add(pnlInfo, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(11,11,11,11),0,0));
		add(pnlAgr , new GridBagConstraints(0,1, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(11,11,11,11),0,0));
	}

	@Override
	public Dimension getDialogSize() {
		return new Dimension(550,600);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilLeverandorkontrakt");
	}
	private void updateComplete() {
		List<AgrArtikkelV> selectedKontrakter = getSelectedKontrakter();
    	setComplete(selectedKontrakter != null && selectedKontrakter.size() > 0 );
    }
	@Override
	public List<Levkontrakt> getNyRelasjon() {
		List<AgrArtikkelV> selected = getSelectedKontrakter();
		KontraktLeverandor currentLeverandor = controller.getCurrentLeverandor();
		Driftkontrakt currentDriftkontrakt = controller.getDriftkontraktController().getCurrentDriftkontrakt();
		List<Levkontrakt> kontrakter = new ArrayList<Levkontrakt>(); 
		for (AgrArtikkelV a:selected){
			Levkontrakt l = new Levkontrakt();
			l.setDriftkontrakt(currentDriftkontrakt);
			l.setLeverandor(currentLeverandor.getLeverandor());
			l.setFraDato(currentLeverandor.getFraDato());
			l.setLevkontraktIdent(a.getLevkontraktIdent());
			l.setNewEntity(true);
			kontrakter.add(l);
		}
		return kontrakter;
	}

	@Override
	public boolean isLegal() {
		return true;
	}
}
