package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import no.mesta.driftkontrakt.bean.KjoretoygruppeService;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.driftkontrakt.DriftkontraktAdminModule;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyGruppering;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class KjoretoygruppeController {

	private DriftkontraktAdminModule plugin;
	private KjoretoygruppeService kjoretoygruppeBean;
	private final DriftkontraktController driftkontraktController;

	public KjoretoygruppeController(DriftkontraktController driftkontraktController){
		this.plugin = driftkontraktController.getDriftkontraktAdminModule();
		this.driftkontraktController = driftkontraktController;
		kjoretoygruppeBean = BeanUtil.lookup(KjoretoygruppeService.BEAN_NAME, KjoretoygruppeService.class);

	}
	public List<KontraktKjoretoyGruppering> getKjoretoyIGruppe(Kjoretoygruppe gruppe){
    	return kjoretoygruppeBean.getKontraktKjoretoyGruppering(gruppe.getId());
    }
	public List<KontraktKjoretoy> getKjoretoyUtenforGruppering(Kjoretoygruppe gruppe){
		return kjoretoygruppeBean.getKjoretoyUtenforGruppering(gruppe.getId(), gruppe.getKontraktId());
	}
    /**
     * Henter kjøretøygruppene til en gitt kontrakt
     * @param kontraktId
     * @return
     */
    public List<Kjoretoygruppe> getKjoretoygrupperForDriftkontrakt(Long kontraktId){
    	return kjoretoygruppeBean.getKjoretoygruppe(kontraktId);
    }
    /**
     * Henter kjøretøygruppene til valgt driftkontrakt, hvis ingen kontrakt er valgt returneres null
     * @return
     */
    public List<Kjoretoygruppe> getKjoretoygrupperForValgtDriftkontrakt(){
    	Driftkontrakt dr = driftkontraktController.getCurrentDriftkontrakt();
    	if (dr!=null){
    		return getKjoretoygrupperForDriftkontrakt(dr.getId());
    	}
    	return new ArrayList<Kjoretoygruppe>();
    }
    
	public void showKjoretoygrupperingDialog(Kjoretoygruppe gruppe, Action callback){
    	EndreKjoretoygrupperingDialog dialog = new EndreKjoretoygrupperingDialog(driftkontraktController, gruppe);
		dialog.setSize(400, 350);
		dialog.setLocationRelativeTo(plugin.getModuleGUI());
		dialog.setVisible(true);
		if (!dialog.isCancelled()){
			Kjoretoygruppe kjoretoygruppe = dialog.getKjoretoygruppe();
			plugin.enforceWriteAccess();
			kjoretoygruppe = kjoretoygruppeBean.persistKjoretoygruppe(kjoretoygruppe);
			ActionEvent ev = new ActionEvent(kjoretoygruppe, 0, "reload");
			callback.actionPerformed(ev);
		}
    }
    
    public Action getNyKjoretoygruppeAction(final Action callback){
    	return new AbstractAction(null, IconResources.NEW_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.enforceWriteAccess();
				showKjoretoygrupperingDialog(null, callback);
			}
    	};
    }
    
    public Action getEndreKjoretoygruppeAction(final JComboBox cbxKjoretoygruppe, final Action callback){
    	return new AbstractAction(null, IconResources.EDIT_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.enforceWriteAccess();
				Kjoretoygruppe gruppe = (Kjoretoygruppe)cbxKjoretoygruppe.getSelectedItem();
				showKjoretoygrupperingDialog(gruppe, callback);
			}
    	};
    }
    public Action getSlettKjoretoygruppeAction(final JComboBox cbxKjoretoygruppe, final Action callback){
    	return new AbstractAction(null, IconResources.BUTTON_CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.enforceWriteAccess();
				Kjoretoygruppe gruppe = (Kjoretoygruppe)cbxKjoretoygruppe.getSelectedItem();
				if (gruppe==null)
					return;
				int r = JOptionPane.showConfirmDialog(plugin.getLoader().getApplicationFrame(), 
						ResourceUtils.getResource("message.slettGruppe", gruppe.getNavn()), 
						ResourceUtils.getResource("message.slettGruppe.title"), 
						JOptionPane.YES_NO_OPTION);
				if (r==JOptionPane.YES_OPTION){
					kjoretoygruppeBean.slettKjoretoygruppe(gruppe.getId());
					callback.actionPerformed(null);
				}
			}
    	};
    }
    public Action getFjernKjoretoyFraGruppeAction(final JXTable tblKjoretoygruppe, final MipssBeanTableModel<KontraktKjoretoyGruppering> tblModelKjoretoygruppe, final Action callback){
    	return new AbstractAction(ResourceUtils.getResource("button.fjernFraKjoretoygruppe"), IconResources.REMOVE_ITEM_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.enforceWriteAccess();
				int selectedRow = tblKjoretoygruppe.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblKjoretoygruppe.getRowCount()) {
					KontraktKjoretoyGruppering kjoretoy= tblModelKjoretoygruppe.getEntities().get(tblKjoretoygruppe.convertRowIndexToModel(selectedRow));
					if(kjoretoy != null) {
						int r = JOptionPane.showConfirmDialog(plugin.getLoader().getApplicationFrame(), 
								ResourceUtils.getResource("message.slettKontraktKjoretoyGruppering", kjoretoy.getKjoretoygruppe().getNavn()), 
								ResourceUtils.getResource("message.slettKontraktKjoretoyGruppering.title"), 
								JOptionPane.YES_NO_OPTION);
						if (r==JOptionPane.YES_OPTION){
							kjoretoygruppeBean.slettKontraktKjoretoyGruppering(kjoretoy);
							ActionEvent ev = new ActionEvent(kjoretoy.getKjoretoygruppe(), 0, "reload");
							callback.actionPerformed(ev);
						}
					}
				}
			}
    	};
    }
    
    
	public Action getLeggTilKjoretoyiGruppeAction(final JComboBox cbxKjoretoygruppe, final Action callback){
    	return new AbstractAction(ResourceUtils.getResource("button.leggTiliGruppe"), IconResources.ADD_ITEM_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.enforceWriteAccess();
				Kjoretoygruppe gruppe = (Kjoretoygruppe)cbxKjoretoygruppe.getSelectedItem();
				LeggTilKjoretoyiGruppeDialog dialog = new LeggTilKjoretoyiGruppeDialog(plugin.getLoader().getApplicationFrame(), KjoretoygruppeController.this, gruppe);
				dialog.setSize(600, 450);
				dialog.setLocationRelativeTo(plugin.getModuleGUI());
				dialog.setVisible(true);
				if (!dialog.isCancelled()){
					List<KontraktKjoretoyGruppering> kjoretoyList = dialog.getKontraktKjoretoyGrupperingList();
					gruppe.setKjoretoygruppe(kjoretoyList);
					kjoretoygruppeBean.updateKjoretoyGruppe(gruppe);
					ActionEvent ev = new ActionEvent(gruppe, 0, "reload");
					callback.actionPerformed(ev);
				}
			}
    	};
    }
	   
	public Action getLeggTilKjoretoyiGruppeListAction(
			final JMipssBeanTable<KontraktKjoretoy> tblKontraktKjoretoy,
			final MipssBeanTableModel<KontraktKjoretoy> tblModelKontraktKjoretoy,
			final MipssBeanTableModel<KontraktKjoretoyGruppering> tblModelKjoretoyGruppe, 
			final Kjoretoygruppe kjoretoygruppe) {
		return new AbstractAction("", IconResources.ADD_ONE_ICON){
			@Override
			public void actionPerformed(ActionEvent e){
				plugin.enforceWriteAccess();
				int selectedRow = tblKontraktKjoretoy.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblKontraktKjoretoy.getRowCount()) {
					KontraktKjoretoy kontraktKjoretoy= tblModelKontraktKjoretoy.getEntities().get(tblKontraktKjoretoy.convertRowIndexToModel(selectedRow));
					if(kontraktKjoretoy != null) {
						tblModelKontraktKjoretoy.removeItem(kontraktKjoretoy);
						KontraktKjoretoyGruppering k = new KontraktKjoretoyGruppering();
						k.setKjoretoy(kontraktKjoretoy);
						k.setKjoretoygruppe(kjoretoygruppe);
						k.setOpprettetDato(Clock.now());
						k.setOpprettetAv(plugin.getLoader().getLoggedOnUserSign());
						tblModelKjoretoyGruppe.addItem(k);
					}
				}
			}
		};
	}
	
	public Action getFjernKjoretoyFraGruppeListAction(
			final JMipssBeanTable<KontraktKjoretoyGruppering> tblKjoretoyGruppe,
			final MipssBeanTableModel<KontraktKjoretoyGruppering> tblModelKjoretoyGruppe,
			final MipssBeanTableModel<KontraktKjoretoy> tblModelKontraktKjoretoy){
		return new AbstractAction("", IconResources.REMOVE_ONE_ICON){
			@Override
			public void actionPerformed(ActionEvent e){
				plugin.enforceWriteAccess();
				int selectedRow = tblKjoretoyGruppe.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblKjoretoyGruppe.getRowCount()) {
					KontraktKjoretoyGruppering kontraktKjoretoyGruppering= tblModelKjoretoyGruppe.getEntities().get(tblKjoretoyGruppe.convertRowIndexToModel(selectedRow));
					if(kontraktKjoretoyGruppering != null) {
						tblModelKjoretoyGruppe.removeItem(kontraktKjoretoyGruppering);
						KontraktKjoretoy kontraktKjoretoy = kjoretoygruppeBean.getKontraktKjoretoy(kontraktKjoretoyGruppering.getKontraktId(), kontraktKjoretoyGruppering.getKjoretoyId());
						tblModelKontraktKjoretoy.addItem(kontraktKjoretoy);
					}
				}
			}
		};
	}
	
	/**
	 * Vidresender propertylistener til driftkontraktcontrolleren
	 * @param l
	 */
	public void addPropertyChangeListener(String propName, PropertyChangeListener l) {
		driftkontraktController.addPropertyChangeListener(propName, l);
	}
}
