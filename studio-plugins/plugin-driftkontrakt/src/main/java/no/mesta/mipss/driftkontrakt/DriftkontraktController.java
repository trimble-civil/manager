package no.mesta.mipss.driftkontrakt;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.naming.NamingException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;
import javax.swing.text.Document;

import jxl.format.Colour;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import no.mesta.driftkontrakt.bean.KundeQueryFilter;
import no.mesta.driftkontrakt.bean.LevStedQueryFilter;
import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.driftkontrakt.bean.ProsessHelper;
import no.mesta.driftkontrakt.bean.ProsjektQueryFilter;
import no.mesta.driftkontrakt.bean.StabQueryFilter;
import no.mesta.mipss.accesscontrol.AccessControl;
import no.mesta.mipss.bestilling.BestillingSkjema;
import no.mesta.mipss.bestilling.BestillingSkjemaType;
import no.mesta.mipss.chart.ChartModule;
import no.mesta.mipss.chart.messages.OpenSingleChartMessage;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.DesktopHelper;
import no.mesta.mipss.core.ExcelHelper;
import no.mesta.mipss.core.IMipssStudioLoader;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.core.SaveStatusRegistry;
import no.mesta.mipss.driftkontrakt.kjoretoy.ColorableTableValue;
import no.mesta.mipss.driftkontrakt.kjoretoy.EndreKjoretoyDialog;
import no.mesta.mipss.driftkontrakt.kjoretoy.KjoretoySearchPanel;
import no.mesta.mipss.driftkontrakt.kjoretoy.KjoretoyslisteTableObject;
import no.mesta.mipss.driftkontrakt.leverandor.LeggTilKontraktLeverandorPanel;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.driftslogg.DriftsloggService;
import no.mesta.mipss.driftslogg.Resources;
import no.mesta.mipss.entityservice.MipssEntityService;
import no.mesta.mipss.exceptions.ErrorHandler;
import no.mesta.mipss.instrumentering.wizard.WizardPanel;
import no.mesta.mipss.kjoretoy.MipssKjoretoy;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.ldap.LdapException;
import no.mesta.mipss.ldap.LdapUser;
import no.mesta.mipss.ldap.LdapUserFactory;
import no.mesta.mipss.messages.MipssMapPosKjoretoyMessage;
import no.mesta.mipss.messages.RefreshLeverandorMessage;
import no.mesta.mipss.mipsslogg.MipssLogg;
import no.mesta.mipss.mipsslogg.MipssLoggModule;
import no.mesta.mipss.mipsslogg.messages.OpenMipssloggMessage;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.kjoretoy.Kjoretoy;
import no.mesta.mipss.persistence.kjoretoy.KjoretoyListV;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.Arbeidsfunksjon;
import no.mesta.mipss.persistence.kontrakt.Driftdistrikt;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyDogn;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.persistence.kontrakt.Kontraktlogg;
import no.mesta.mipss.persistence.kontrakt.Kontrakttype;
import no.mesta.mipss.persistence.kontrakt.KundeView;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.persistence.kontrakt.kontakt.Kontraktkontakttype;
import no.mesta.mipss.persistence.mipssfield.FavorittProsess;
import no.mesta.mipss.persistence.mipssfield.Favorittliste;
import no.mesta.mipss.persistence.mipssfield.Pdabruker;
import no.mesta.mipss.persistence.mipssfield.PdabrukerKontrakt;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.organisasjon.ArbeidsordreView;
import no.mesta.mipss.persistence.organisasjon.KontraktProsjekt;
import no.mesta.mipss.persistence.organisasjon.ProsjektView;
import no.mesta.mipss.persistence.rapportfunksjoner.UkjentStroprodukt;
import no.mesta.mipss.persistence.tilgangskontroll.Bruker;
import no.mesta.mipss.persistence.veinett.Veinett;
import no.mesta.mipss.plugin.MipssPlugin;
import no.mesta.mipss.produksjonsrapport.Prodrapport;
import no.mesta.mipss.reports.BeanDataSource;
import no.mesta.mipss.reports.JReportBuilder;
import no.mesta.mipss.reports.Report;
import no.mesta.mipss.reports.ReportHelper;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.EndreDfuKontraktDialog;
import no.mesta.mipss.ui.JMipssContractPicker;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.MipssTextAreaInputDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.dialogue.MipssDialogue;
import no.mesta.mipss.util.DateFormatter;
import no.mesta.mipss.util.EpostUtils;
import no.mesta.mipss.util.ukjentproduksjon.UkjentStroproduktRapport;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
@SuppressWarnings("serial")
public class DriftkontraktController implements PropertyChangeListener, VetoableChangeListener {
	private MaintenanceContract contractBean;
	private MipssEntityService entityServiceBean;
	private MipssKjoretoy kjoretoyBean;
	private Driftkontrakt currentDriftkontrakt;
	private KundeView kundeView;
	private JMipssContractPicker contractPicker;
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private DriftkontraktAdminModule driftkontraktAdminModule;
	private DriftkontraktAdminModulePanel driftkontraktAdminModulePanel;
	private SaveStatusMonitor<Driftkontrakt> kontraktSaveMonitor;
	private SaveStatusRegistry saveStatusRegistry;
	
	private Date searchFromDate;
	private Date searchToDate;
	
	public DriftkontraktController(SaveStatusRegistry saveStatusRegistry, DriftkontraktAdminModule driftkontraktAdminModule, MaintenanceContract contractBean, MipssEntityService entityServiceBean, JMipssContractPicker contractPicker, DriftkontraktAdminModulePanel driftkontraktAdminModulePanel) {
		this.driftkontraktAdminModule = driftkontraktAdminModule;
		this.contractBean = contractBean;
		kjoretoyBean = BeanUtil.lookup(MipssKjoretoy.BEAN_NAME, MipssKjoretoy.class);
		this.entityServiceBean = entityServiceBean;
		this.contractPicker = contractPicker;
		this.driftkontraktAdminModulePanel = driftkontraktAdminModulePanel;
		this.saveStatusRegistry = saveStatusRegistry;
		kontraktSaveMonitor = new SaveStatusMonitor<Driftkontrakt>(Driftkontrakt.class, this, "currentDriftkontrakt", saveStatusRegistry.getBindingGroup());
		saveStatusRegistry.addMonitor(kontraktSaveMonitor);
		
		contractPicker.addVetoableChangeListener(this);
	}

	public Driftkontrakt getCurrentDriftkontrakt() {
		return currentDriftkontrakt;
	}

	public void setCurrentDriftkontrakt(Driftkontrakt currentDriftkontrakt) {
		Object oldContract = this.currentDriftkontrakt;
		this.currentDriftkontrakt = currentDriftkontrakt;
		
		if(currentDriftkontrakt != null && currentDriftkontrakt.getKontraktloggList() != null) {
			Collections.sort(currentDriftkontrakt.getKontraktloggList());
			Collections.reverse(currentDriftkontrakt.getKontraktloggList());
		}
		
		props.firePropertyChange("currentDriftkontrakt", oldContract, currentDriftkontrakt);
		oppdaterKundeView();
		saveStatusRegistry.reset();
	}
	
	public KundeView getKundeView() {
		return kundeView;
	}

	public void setKundeView(KundeView kundeView) {
		Object oldValue = this.kundeView;
		this.kundeView = kundeView;
		props.firePropertyChange("kundeView", oldValue, kundeView);
	}

	private void oppdaterKundeView() {
		Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
		KundeView kundeView = null;
		if(driftkontrakt != null && driftkontrakt.getKundeNr() != null) {
			KundeQueryFilter kundeQueryFilter = new KundeQueryFilter();
			kundeQueryFilter.setKundeNr(driftkontrakt.getKundeNr());
			List<KundeView> kundeViewList = null;
			try {
				kundeViewList = getKundeViewList(kundeQueryFilter);
			} catch (Exception e) {
				e.printStackTrace();
				kundeViewList = new ArrayList<KundeView>();
			}
			if(kundeViewList.size() > 0) {
				kundeView = kundeViewList.get(0);
			}
		}
		
		setKundeView(kundeView);
	}
	
	public void registrerDriftPanel(JPanel panel, String key) {
		driftkontraktAdminModulePanel.registrerPanel(panel, key);
	}
	
	public void visPanel(String key) {
		driftkontraktAdminModulePanel.visPanel(key);
	}
	
	public DriftkontraktAdminModule getDriftkontraktAdminModule() {
		return driftkontraktAdminModule;
	}

	public List<Driftkontrakt> getDriftkontraktList() {
		return contractBean.getDriftkontraktList();
	}
	
	public List<Driftdistrikt> getDriftdistriktList() {
		return contractBean.getDriftdistriktList();
	}
	
	public List<Kontrakttype> getKontrakttypeList() {
		return contractBean.getKontrakttypeList();
	}
	
	public Veinett getVeinettForDriftkontrakt(Long id) {
		return contractBean.getVeinettForDriftkontrakt(id);
	}
	
	public Driftkontrakt getDriftkontraktForAdministration(Long id) {
		return contractBean.getDriftkontraktForAdministration(id);
	}
	
	public Driftkontrakt oppdaterDriftkontrakt(Driftkontrakt driftkontrakt) {
		return contractBean.oppdaterDriftkontrakt(driftkontrakt, driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
	}

	public Driftkontrakt getDriftkontrakt(Long id) {
		return contractBean.getDriftkontrakt(id);
	}
	
    public List<KundeView> getKundeViewList(KundeQueryFilter kundeQueryFilter) {
    	return contractBean.getKundeViewList(kundeQueryFilter);
    }
    
    public List<ProsjektView> getProsjektViewList(ProsjektQueryFilter prosjektQueryFilter) {
    	return contractBean.getProsjektViewList(prosjektQueryFilter);
    }
    
    public List<ArbeidsordreView> getArbeidsordreViewList(ProsjektQueryFilter prosjektQueryFilter) {
    	return contractBean.getArbeidsordreViewList(prosjektQueryFilter);
    }
    
    public List<Bruker> getBrukerList(StabQueryFilter stabQueryFilter) {
    	return contractBean.getBrukerList(stabQueryFilter);
    }
	
    public List<LevStedView> getLevStedViewListUnique(LevStedQueryFilter levStedQueryFilter) {
    	return contractBean.getLevStedViewList(levStedQueryFilter, true);
    }
    
    public List<LevStedView> getLevStedViewList(LevStedQueryFilter levStedQueryFilter) {
    	return contractBean.getLevStedViewList(levStedQueryFilter, false);
    }
    
    public List<Rodetype> getRodetypeList(Long kontraktId) {
    	return contractBean.getRodetypeForKontrakt(kontraktId);
    }
    
    public List<Rode> getRodeList(Long kontraktId, Long typeId) {
    	return contractBean.getRodeList(kontraktId, typeId);
    }
    
    public Leverandor opprettLeverandor(String leverandorNr) {
    	return contractBean.opprettLeverandor(leverandorNr);
    }
    
    public List<Prodtype> getProdtypeList() {
    	return contractBean.getProdtypeList();
    }
    
    public List<Kontraktkontakttype> getKontraktkontakttypeList() {
    	return contractBean.getKontraktkontakttypeList();
    }
    
    public Kjoretoy oppdaterKjoretoy(Kjoretoy kjoretoy) {
    	return contractBean.oppdaterKjoretoy(kjoretoy);
    }
    
    public Kjoretoy getKjoretoy(Long kjoretoyId) {
    	return contractBean.getKjoretoy(kjoretoyId);
    }
    public List<Prosess> getProsesserForKontrakt(Long kontraktId){
    	List<Prosess> pl = contractBean.getProsessForKontrakt(kontraktId, false);
    	return new ProsessHelper().createTree(pl);
    }
    public List<FavorittProsess> getFavorittProsessListForFavorittliste(Long listeId) {
    	return contractBean.getFavorittProsessListForFavorittliste(listeId);
    }
    
    public List<LevProdtypeRode> getLevProdtypeRodeListForLeverandorAndKontrakt(String leverandorNr, Long kontraktId) {
    	return contractBean.getLevProdtypeRodeListForLeverandorAndKontrakt(leverandorNr, kontraktId);
    }
    
    public void opprettKontraktKjoretoy(Long did, Long kid, Long ansvarlig){
    	contractBean.opprettKontraktKjoretoy(did, kid, ansvarlig, driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
    	sendRefreshMeldingTilLeverandor();
    }
    
    public Boolean isKontraktKontaktPresent(Long driftkontraktId, Long kjoretoyId){
    	return contractBean.sjekkOmKontraktKjoretoyFinnes(driftkontraktId, kjoretoyId);
    }
    
    public void fjernKontraktKjoretoy(Long did, Long kid){
    	contractBean.fjernKontraktKjoretoy(kid, kid, driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
    }
    
    public List<KjoretoyslisteTableObject> getKontraktKjoretoyInfo(Long kontraktId, Date prodFromDate, Date prodToDate) {
    	List<KjoretoyslisteTableObject> kjoretoyslist = new ArrayList<KjoretoyslisteTableObject>();
    	List<KontraktKjoretoyInfo> infoList = contractBean.getKontraktKjoretoyInfo(kontraktId, prodFromDate, prodToDate);
    	List<KontraktKjoretoyDogn> produksjonList = contractBean.getKontraktKjoretoyDognListAkk(kontraktId, prodFromDate, prodToDate);
    	
    	//Bygger opp en hashmap med lister av kjoretoyDogn per kjøretøy
    	Map<Long, List<KontraktKjoretoyDogn>> kjoretoyDognMap = new HashMap<Long, List<KontraktKjoretoyDogn>>();
    	
    	for(KontraktKjoretoyDogn kontraktKjoretoyDogn: produksjonList) {
    		if(kjoretoyDognMap.containsKey(kontraktKjoretoyDogn.getKjoretoyId())) {
    			kjoretoyDognMap.get(kontraktKjoretoyDogn.getKjoretoyId()).add(kontraktKjoretoyDogn);
    		} else {
    			List<KontraktKjoretoyDogn> tmpList = new ArrayList<KontraktKjoretoyDogn>();
    			tmpList.add(kontraktKjoretoyDogn);
    			kjoretoyDognMap.put(kontraktKjoretoyDogn.getKjoretoyId(), tmpList);
    		}
    	}
    	String ordrestatusDelerSendt = KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss.montering", "ordrestatusDelerSendt").getVerdi();
    	Set<Long> kjoretoyFraFunksjonSet = new HashSet<Long>();
    	for(KontraktKjoretoyInfo kontraktKjoretoyInfo : infoList) {
    		kjoretoyslist.add(new KjoretoyslisteTableObject(kontraktKjoretoyInfo, kjoretoyDognMap.get(kontraktKjoretoyInfo.getKjoretoyId()), ordrestatusDelerSendt));
    		kjoretoyFraFunksjonSet.add(kontraktKjoretoyInfo.getKjoretoyId());
    	}
    	
    	return kjoretoyslist;
    }
    
	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}
	
	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		if(StringUtils.equals("valgtKontrakt", evt.getPropertyName())) {
			Driftkontrakt valgtKontrakt = (Driftkontrakt)evt.getNewValue(); //contractPicker.getValgtKontrakt();
			
			if(valgtKontrakt != null && valgtKontrakt.equals(currentDriftkontrakt)) {
				return;
			}
			
			if(saveStatusRegistry.isChanged()) {
				MipssDialogue dialogue = new MipssDialogue(
						driftkontraktAdminModule.getLoader().getApplicationFrame(), IconResources.INFO_ICON, ResourceUtils.getResource("dialogTitle.bekreft"),
						ResourceUtils.getResource("message.lagreVedKontraktbytte"),
			            MipssDialogue.Type.QUESTION,
			            new MipssDialogue.Button[] {MipssDialogue.Button.YES,
						MipssDialogue.Button.NO,
			            MipssDialogue.Button.CANCEL});
				dialogue.askUser();
				if(dialogue.getPressedButton() == MipssDialogue.Button.YES) {
					Driftkontrakt nyDriftkontrakt = oppdaterDriftkontrakt(getCurrentDriftkontrakt());
					List<Driftkontrakt> kontrakter = contractPicker.getModel().getKontrakter();
					int index = kontrakter.indexOf(getCurrentDriftkontrakt());
					kontrakter.set(index, nyDriftkontrakt);
				} else if(dialogue.getPressedButton() == MipssDialogue.Button.CANCEL) {
					throw new PropertyVetoException("Ulagrede endringer", evt);
				}
			}
			
			lastKontraktForAdministrasjon(valgtKontrakt,  true);
		}
	}
	
	/**
	 * Hovedmetode for lasting av kontrakt
	 * @param valgtKontrakt
	 */
	@SuppressWarnings("unchecked")
	public void lastKontraktForAdministrasjon(final Driftkontrakt valgtKontrakt, final boolean visTekst) {
    	final SwingWorker w = new SwingWorker(){
			@Override
			protected Object doInBackground() throws Exception {
				driftkontraktAdminModulePanel.setBusy(true, visTekst);
				if(valgtKontrakt != null) {
					List<Driftkontrakt> kontrakter = contractPicker.getModel().getKontrakter();
					int index = kontrakter.indexOf(valgtKontrakt);
					Driftkontrakt tmpKontrakt = getDriftkontraktForAdministration(valgtKontrakt.getId());
					kontrakter.set(index, tmpKontrakt);
					setCurrentDriftkontrakt(tmpKontrakt);
				} else {
					setCurrentDriftkontrakt(valgtKontrakt);
				}
				driftkontraktAdminModulePanel.setBusy(false, visTekst);
				
				return null;
			}
    	};
    	w.execute();
    	new Thread(){
    		public void run(){
    			try {
					w.get();
				} catch (Exception e) {
					e.printStackTrace();
				}
    		}
    	}.start();
	}
	
	@SuppressWarnings("unchecked")
	public Document getKontraktloggDocument() {
		KontraktloggDocument doc = new KontraktloggDocument();
		Binding binding = BindingHelper.createbinding(this, "${currentDriftkontrakt.kontraktloggList}", doc, "loggList", BindingHelper.READ);
		binding.bind();
		return doc;
	}
	
	private DriftkontraktController getInstance() {
		return this;
	}
	
	public void registerChangableField(String... fields) {
		for(String s : fields) {
			kontraktSaveMonitor.registerField(s);
		}
	}
	
    private void createAndOpenPdf(Kjoretoyordre ordre){
    	BeanDataSource<Kjoretoyordre> ds = new BeanDataSource<Kjoretoyordre>(ordre);
    	JReportBuilder builder = new JReportBuilder(Report.KJORETOYORDRE,
				Report.KJORETOYORDRE.getDefaultParameters(), ds, ReportHelper.EXPORT_PDF,
				driftkontraktAdminModule.getLoader().getApplicationFrame());
		File report = builder.getGeneratedReportFile();

		if (report != null) {
			DesktopHelper.openFile(report);
		}
    }
	
    public List<Kjoretoyordre> getKjoretoyordreIKontrakt(Long kontraktId) {
    	List<Kjoretoyordre> list = contractBean.getOrdreListForKontraktId(kontraktId);
    	
    	//Viser kun ordre som har riktige statuser her
    	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
		String synligeStatuser = params.hentEnForApp("studio.mipss.driftkontrakt", "synligeKjoretoyordrestatus").getVerdi();
		
    	StringTokenizer st = new StringTokenizer(synligeStatuser, ",");
		Set<String> statusIdenter = new HashSet<String>();
		while(st.hasMoreTokens()) {
			String s = st.nextToken();
			statusIdenter.add(s);
		}
		
		for(Iterator<Kjoretoyordre> it = list.iterator(); it.hasNext();) {
			Kjoretoyordrestatus ks = it.next().getSisteKjoretoyordrestatus(); 
			if(ks != null && !statusIdenter.contains(ks.getIdent())) {
				it.remove();
			}
		}
    	
    	return list;
    }
    
    /**
     * LagreAction
     * @return
     */
    public Action getLagreAction(final JButton btnLagre) {
    	saveStatusRegistry.getBindingGroup().addBinding(BindingHelper.createbinding(saveStatusRegistry, "changed", btnLagre, "enabled"));
    	
    	return new AbstractAction(ResourceUtils.getResource("button.lagre")) {
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				final Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				final SwingWorker w = new SwingWorker(){
					@Override
					protected Object doInBackground() throws Exception {
						driftkontraktAdminModulePanel.setBusy(true, false);
						Driftkontrakt nyDriftkontrakt = oppdaterDriftkontrakt(driftkontrakt);
						setCurrentDriftkontrakt(null);
						lastKontraktForAdministrasjon(nyDriftkontrakt, false);
						driftkontraktAdminModulePanel.setBusy(false, false);
						return null;
					}
		    	};
		    	w.execute();
		    	new Thread(){
		    		public void run(){
		    			try {
							w.get();
						} catch (Exception e) {
							e.printStackTrace();
						} 
		    		}
		    	}.start();
			}
    	};
    }
	
    public Action getVisLoggDetaljerAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.visKontraktslogg")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				new KontraktloggDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), getInstance());
			}
    	};
    }
    
    public Action getNyLoggAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.ny")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					//String tekst = JOptionPane.showInputDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), "Ny melding (maks 20 tegn)", "Ny melding", JOptionPane.PLAIN_MESSAGE);
					MipssTextAreaInputDialog inputDialog = new MipssTextAreaInputDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), "Ny melding");
					String tekst = inputDialog.getInput();
					if(tekst != null) {
						tekst = tekst.length() > 4000 ? tekst.substring(0, 4000) : tekst;
						Kontraktlogg nyEntry = new Kontraktlogg();
						nyEntry.setFritekst(tekst);
						nyEntry.setDriftkontrakt(driftkontrakt);
						nyEntry.setOpprettetAv(driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
						nyEntry.setOpprettetDato(new Timestamp(Calendar.getInstance().getTimeInMillis()));
						driftkontrakt.getKontraktloggList().add(0, nyEntry);
					}
				}
			}
    	};
    }
    
    public Action getLeggTilKontraktStabAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.leggTil")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<Arbeidsfunksjon> arbeidfunksjonList = entityServiceBean.getArbeidsfunksjonList();
					LeggTilKontraktStabPanel stabPanel = new LeggTilKontraktStabPanel(getInstance(), arbeidfunksjonList);
					LeggTilRelasjonDialog<List<KontraktStab>> relDialog = new LeggTilRelasjonDialog<List<KontraktStab>>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), stabPanel);
					List<KontraktStab> kontraktStabList = relDialog.showDialog();
					
					if(kontraktStabList != null) {
						for(KontraktStab kontraktStab : kontraktStabList) {
							driftkontrakt.getKontraktStabList().add(kontraktStab);
						}
					}
				}
			}
    	};
    }
    
    public Action getLeggTilPdabrukerAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.leggTil")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					LeggTilPdaBrukerPanel stabPanel = new LeggTilPdaBrukerPanel(getInstance());
					LeggTilRelasjonDialog<List<PdabrukerKontrakt>> relDialog = new LeggTilRelasjonDialog<List<PdabrukerKontrakt>>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), stabPanel);
					List<PdabrukerKontrakt> pdabrukerList = relDialog.showDialog();
					
					if(pdabrukerList != null) {
						for(PdabrukerKontrakt pdabruker : pdabrukerList) {
							if (!driftkontrakt.getPdabrukerList().contains(pdabruker)){
								driftkontrakt.getPdabrukerList().add(pdabruker);
							}else{
								JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getModuleGUI(), ResourceUtils.getResource("message.alleredelagttil.message"), ResourceUtils.getResource("message.alleredelagttil.title"), JOptionPane.INFORMATION_MESSAGE);
							}
						}
					}
				}
			}
    	};
    }
    public Action getSlettPdabrukerAction(final MipssBeanTableModel<PdabrukerKontrakt> brukerModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<PdabrukerKontrakt> brukerList = new ArrayList<PdabrukerKontrakt>();
					int[] selectedIndexes = table.getSelectedRows();
					for(int i=0; i<selectedIndexes.length; i++) {
						PdabrukerKontrakt bruker = brukerModel.get(table.convertRowIndexToModel(selectedIndexes[i]));
						brukerList.add(bruker);
					}
					
					if(brukerList.size() > 0) {
						driftkontrakt.getPdabrukerList().removeAll(brukerList);
					}
				}
			}
    	};
    }
    public Action getEndrePdabrukerAction(final MipssBeanTableModel<PdabrukerKontrakt> brukerModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.endre")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					PdabrukerKontrakt pdabrukerKontrakt = brukerModel.get(table.convertRowIndexToModel(table.getSelectedRows()[0]));
					NyPdabrukerPanel pdabrukerPanel = new NyPdabrukerPanel(pdabrukerKontrakt.getPdabruker());
					LeggTilRelasjonDialog<Pdabruker> relDialog = new LeggTilRelasjonDialog<Pdabruker>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), pdabrukerPanel);
					Pdabruker pdabruker = relDialog.showDialog();
					contractBean.updatePdabruker(pdabruker);

				}
			}
    	};
    }
    public Action getSlettKontraktStabAction(final MipssBeanTableModel<KontraktStab> stabModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<KontraktStab> kontraktStabList = new ArrayList<KontraktStab>();
					int[] selectedIndexes = table.getSelectedRows();
					for(int i=0; i<selectedIndexes.length; i++) {
						KontraktStab kontraktStab = stabModel.get(table.convertRowIndexToModel(selectedIndexes[i]));
						kontraktStabList.add(kontraktStab);
					}
					
					if(kontraktStabList.size() > 0) {
						driftkontrakt.getKontraktStabList().removeAll(kontraktStabList);
					}
				}
			}
    	};
    }
    
    public Action getVelgKundeAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.velgKunde")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					VelgKundePanel kundePanel = new VelgKundePanel(getInstance());
					LeggTilRelasjonDialog<Long> relDialog = new LeggTilRelasjonDialog<Long>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), kundePanel);
					Long kundeId = relDialog.showDialog();
					if(kundeId != null) {
						driftkontrakt.setKundeNr(kundeId.toString());
						oppdaterKundeView();
					}
				}
			}
    	};
    }
    
    public Action getLeggTilProsjektAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.leggTil")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					LeggTilArbeidsordrePanel prosjektPanel = new LeggTilArbeidsordrePanel(getInstance());
					LeggTilRelasjonDialog<List<KontraktProsjekt>> relDialog = new LeggTilRelasjonDialog<List<KontraktProsjekt>>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), prosjektPanel);
					List<KontraktProsjekt> prosjektKontraktList = relDialog.showDialog();
					if(prosjektKontraktList != null) {
						for(KontraktProsjekt prosjektKontrakt : prosjektKontraktList) { 
							driftkontrakt.getProsjektKontraktList().add(prosjektKontrakt);
						}
					}
				}
			}
    	};
    }
    
    public Action getSettStandardKontraktProsjektAction(final MipssBeanTableModel<KontraktProsjekt> prosjektModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.settSomStandard")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					int selectedIndex = table.getSelectedRow();
					if(selectedIndex >= 0) {
						KontraktProsjekt kontraktProsjekt = prosjektModel.get(table.convertRowIndexToModel(selectedIndex));
						kontraktProsjekt.setStandardFlagg(Long.valueOf(1));
						
						for(KontraktProsjekt kontraktProsjekt2 : prosjektModel.getEntities()) {
							if(!kontraktProsjekt.equals(kontraktProsjekt2)) {
								kontraktProsjekt2.setStandardFlagg(Long.valueOf(0));
							}
						}
						
						prosjektModel.fireTableModelEvent(0, prosjektModel.getRowCount(), TableModelEvent.UPDATE);
						kontraktSaveMonitor.setOverriddenChange(Boolean.TRUE);
					}
				}
			}
    	};
    }
    
    public Action getSlettKontraktProsjektAction(final MipssBeanTableModel<KontraktProsjekt> prosjektModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<KontraktProsjekt> kontraktProsjektList = new ArrayList<KontraktProsjekt>();
					int[] selectedIndexes = table.getSelectedRows();
					for(int i=0; i<selectedIndexes.length; i++) {
						KontraktProsjekt kontraktProsjekt = prosjektModel.get(table.convertRowIndexToModel(selectedIndexes[i]));
						kontraktProsjektList.add(kontraktProsjekt);
					}
					
					if(kontraktProsjektList.size() > 0) {
						driftkontrakt.getProsjektKontraktList().removeAll(kontraktProsjektList);
					}
				}
			}
    	};
    }
    
    public Action getLeggTilLeverandorAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.leggTil")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				
				if(driftkontrakt != null) {
					LeggTilKontraktLeverandorPanel levStedPanel = new LeggTilKontraktLeverandorPanel(getInstance());
					LeggTilRelasjonDialog<List<KontraktLeverandor>> relDialog = new LeggTilRelasjonDialog<List<KontraktLeverandor>>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), levStedPanel);
					List<KontraktLeverandor> kontraktLeverandorList = relDialog.showDialog();
					if(kontraktLeverandorList != null) {
						for(KontraktLeverandor kontraktLeverandor : kontraktLeverandorList) {
							//Oppretter leverandøren om denne ikke finnes
							Leverandor leverandor = opprettLeverandor(kontraktLeverandor.getLeverandorNr());
							if(leverandor != null) {
								kontraktLeverandor.setLeverandor(leverandor);
								driftkontrakt.getKontraktLeverandorList().add(kontraktLeverandor);
							} else {
								JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.klarteIkkeOppretteLeverandor"));
							}
						}
					}
				}
			}
    	};
    }
    
    public Action getSlettLeverandorAction(final MipssBeanTableModel<KontraktLeverandor> leverandorModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<KontraktLeverandor> kontraktLeverandorList = new ArrayList<KontraktLeverandor>();
					int[] selectedIndexes = table.getSelectedRows();
					StringBuilder bodyText = new StringBuilder();
					boolean kanNoenSlettes = false;
					
					for(int i=0; i<selectedIndexes.length; i++) {
						KontraktLeverandor kontraktLeverandor = leverandorModel.get(table.convertRowIndexToModel(selectedIndexes[i]));
						List<Kjoretoy> kjoretoyForKontraktLeverandor = contractBean.getKjoretoyForKontraktLeverandor(kontraktLeverandor);	
						
						//Hvis levereandøren har levkontrakter-/agresso-tilknytning
						if (kontraktLeverandor.getLevkontraktList().size() > 0) {
							
							StringBuilder header = new StringBuilder();
							StringBuilder msg = new StringBuilder();
							
							header.append(ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.levkontrakt.head"));
							
							msg.append(ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.levkontrakt"));
							
							Boolean inaktivDatoErSatt = kontraktLeverandor.getInaktivDato() == null ? false : true;
							Boolean tilDatoEtterDagensDato = kontraktLeverandor.getTilDato().after(Clock.now());
							
							//Hvis inaktivdato er satt
							if (inaktivDatoErSatt) {
								msg.append(ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.levkontrakt.inaktivDatoErSatt", EpostUtils.getSupportEpostAdresse()));
								kontraktLeverandor.setTilDato(kontraktLeverandor.getInaktivDato());
								JOptionPane.showMessageDialog(table, msg.toString(), header.toString(), JOptionPane.OK_OPTION);
								return;
							}
							else if (tilDatoEtterDagensDato) {
								msg.append(ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.levkontrakt.tilDatoEtterDagensDato"));
								
								int confirm = JOptionPane.showConfirmDialog(table, msg.toString(), header.toString(), JOptionPane.YES_NO_OPTION);
								
								if (confirm == JOptionPane.YES_OPTION){
									table.clearSelection(); //Fjerner selection for å unngå indeks-problemer
									kontraktLeverandor.setTilDato(Clock.now());
									JOptionPane.showMessageDialog(table, 	ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.levkontrakt.erSattSomInaktiv", EpostUtils.getSupportEpostAdresse()), 
											ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.levkontrakt.erSattSomInaktiv.head"), 
											JOptionPane.YES_NO_OPTION);
								}
								return;
							}
							else {
								JOptionPane.showMessageDialog(table, msg.toString(), header.toString(), JOptionPane.OK_OPTION);
							}
						}
						
						//Hvis leverandøren har kjøretøy tilknyttet seg
						if (kjoretoyForKontraktLeverandor.isEmpty()){
							kontraktLeverandorList.add(kontraktLeverandor);
							kanNoenSlettes = true;
						} 
						else{//kan ikke fjerne leverandøren før kjøretøyet er fjernet.
							List<String> regnr = new ArrayList<String>();
							int lineBreakCount = 10;
							int c =0;
							for (Kjoretoy k:kjoretoyForKontraktLeverandor){
								regnr.add(k.getEksterntNavn());
								if (c>1&&c%lineBreakCount==0){ 
									regnr.add("<br>");
								}
								c++;
							}
							String r = StringUtils.join(regnr, ',');
							String msg = ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.kjoretoy", r, kontraktLeverandor.getLeverandor().getNavn());
							bodyText.append(msg);
						}
					}
					String body = bodyText.toString();
					
					if (!"".equals(body)){
						body = "<ul>"+body+"</ul>";
						String msg = ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.head");
						String footer = ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.footer");
						String end = ResourceUtils.getResource("message.kanIkkeFjerneLeverandor.end");
						if (kanNoenSlettes){
							int	confirm = JOptionPane.showConfirmDialog(table, msg+body+footer+"<br><br>"+end, "title", JOptionPane.YES_NO_OPTION);
							if (confirm == JOptionPane.YES_OPTION){
								driftkontrakt.getKontraktLeverandorList().removeAll(kontraktLeverandorList);
							}
						}else{
							JOptionPane.showMessageDialog(table, msg+body+footer+"</html>", "title", JOptionPane.YES_NO_OPTION);
						}
					}
					else if(kontraktLeverandorList.size() > 0) {
						driftkontrakt.getKontraktLeverandorList().removeAll(kontraktLeverandorList);
					}
				}
			}
    	};
    }
    
    public Action getLeggTilKontaktAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.leggTil")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					LeggTilKontaktPanel kontaktPanel = new LeggTilKontaktPanel(getInstance(), driftkontrakt.getKontraktKontaktypeList());
					LeggTilRelasjonDialog<KontraktKontakttype> relDialog = new LeggTilRelasjonDialog<KontraktKontakttype>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), kontaktPanel);
					KontraktKontakttype kontraktKontakt = relDialog.showDialog();
					if(kontraktKontakt != null) {
						driftkontrakt.getKontraktKontaktypeList().add(kontraktKontakt);
					}
				}
			}
    	};
    }
    
    public Action getSlettKontaktAction(final MipssBeanTableModel<KontraktKontakttype> kontaktModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<KontraktKontakttype> kontraktKontakttypeList = new ArrayList<KontraktKontakttype>();
					int[] selectedIndexes = table.getSelectedRows();
					for(int i=0; i<selectedIndexes.length; i++) {
						KontraktKontakttype kontraktKontakttype = kontaktModel.get(table.convertRowIndexToModel(selectedIndexes[i]));
						kontraktKontakttypeList.add(kontraktKontakttype);
					}
					if(kontraktKontakttypeList.size() > 0) {
						driftkontrakt.getKontraktKontaktypeList().removeAll(kontraktKontakttypeList);
					}
				}
			}
    	};
    }
    
    public Action getEndreKontaktAction(final MipssBeanTableModel<KontraktKontakttype> kontakterModel, final JXTable kontaktTable) {
    	return new AbstractAction(ResourceUtils.getResource("button.endre")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = kontaktTable.getSelectedRow(); 
				if (selectedIndex != -1){
					KontraktKontakttype kontraktKontaktype = kontakterModel.getEntities().get(kontaktTable.convertRowIndexToModel(selectedIndex));
					if(kontraktKontaktype != null) {
						Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
						if(driftkontrakt != null) {
							LeggTilKontaktPanel kontaktPanel = new LeggTilKontaktPanel(getInstance(), driftkontrakt.getKontraktKontaktypeList(), kontraktKontaktype);
							LeggTilRelasjonDialog<KontraktKontakttype> relDialog = new LeggTilRelasjonDialog<KontraktKontakttype>(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), kontaktPanel);
							kontraktKontaktype = relDialog.showDialog();
							if(kontraktKontaktype != null) {
								kontraktSaveMonitor.setOverriddenChange(Boolean.TRUE);
							}
							kontakterModel.fireTableModelEvent(kontaktTable.convertRowIndexToModel(selectedIndex), kontaktTable.convertRowIndexToModel(selectedIndex), TableModelEvent.UPDATE);
						}
					}
				}
			}
    	};
    }
    public Action getEndreStabAction(final MipssBeanTableModel<KontraktStab> stabModel, final JXTable stabTable){
    	return new AbstractAction(ResourceUtils.getResource("button.endre")){
    		@Override
    		public void actionPerformed(ActionEvent e){
    			int idx = stabTable.getSelectedRow();
    			if (idx!=-1){
    				KontraktStab stab = stabModel.getEntities().get(stabTable.convertRowIndexToModel(idx));
    				if (stab!=null){
    					RedigerElrappBrukerFraKontraktStabDialog dialog = new RedigerElrappBrukerFraKontraktStabDialog(driftkontraktAdminModule.getLoader().getApplicationFrame(), stab);
    					dialog.setLocationRelativeTo(driftkontraktAdminModule.getLoader().getApplicationFrame());
    					dialog.setVisible(true);
    					if (!dialog.isCancelled()){
    						//TODO
    						String elrappIdent = dialog.getElrappBruker();
    						if ("".equals(elrappIdent)){
    							elrappIdent = null;
    						}
    						stab.getBruker().setElrappIdent(elrappIdent);
    						AccessControl a = BeanUtil.lookup(AccessControl.BEAN_NAME, AccessControl.class);
    						a.oppdaterElrappNavn(stab.getBruker(), driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
    						stabModel.fireTableModelEvent(idx, idx, TableModelEvent.UPDATE);
    					}
    				}
    			}
    		}
    	};
    }
    
    public Action getEndreDfuKontraktAction(final MipssBeanTableModel<DfuKontrakt> dfuKontraktModel, final JXTable tblDfuKontraktList) {
    	return new AbstractAction(ResourceUtils.getResource("button.endreDfuKontrakt")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblDfuKontraktList.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblDfuKontraktList.getRowCount()) {
					DfuKontrakt dfuKontrakt = dfuKontraktModel.getEntities().get(tblDfuKontraktList.convertRowIndexToModel(selectedRow));
					EndreDfuKontraktDialog endreDfuKontraktDialog = new EndreDfuKontraktDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), dfuKontrakt);
					boolean endret = endreDfuKontraktDialog.showDialog();
					
					if(endret) {
						dfuKontraktModel.fireTableModelEvent(selectedRow, selectedRow, TableModelEvent.UPDATE);
						kontraktSaveMonitor.setOverriddenChange(Boolean.TRUE);
					}
				}
			}
    	};
    }
    
    
    public Action getNyttKjoretoyAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy, final Action reloadKjoretoyListAction) {
    	return new AbstractAction(ResourceUtils.getResource("button.nyttKjoretoy")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(currentDriftkontrakt != null) {
					//Viser dialog for nytt kjøretøy
					WizardPanel wizardPanel = new WizardPanel(driftkontraktAdminModule.getLoader().getApplicationFrame(), driftkontraktAdminModule, true, currentDriftkontrakt, null, false, false);
					Kjoretoy kjoretoy = wizardPanel.startWizard();
					if(kjoretoy != null) {
						KontraktKjoretoy kk = null;
						for(KontraktKjoretoy kontrkj : kjoretoy.getKontraktKjoretoyList()) {
							if(kontrkj.getKontraktId().equals(currentDriftkontrakt.getId())) {
								kk = kontrkj;
								break;
							}
						}
						
						if(kk == null) {
							kk = new KontraktKjoretoy();
							kk.setDriftkontrakt(currentDriftkontrakt);
							kk.setKjoretoy(kjoretoy);
							kk.setAnsvarligFlagg(Long.valueOf(0));
							kjoretoy.getKontraktKjoretoyList().add(kk);
						}
						
						kjoretoy = oppdaterKjoretoy(kjoretoy);	
						tblKjoretoy.clearSelection();						
						reloadKjoretoyListAction.actionPerformed(null);
					}
				}
			}
    	};
    }
    
	@SuppressWarnings("unchecked")
	public void sendRefreshMeldingTilLeverandor(){
		Class<? extends MipssPlugin> pluginClass;
		try {
			pluginClass = (Class<? extends MipssPlugin>) Class.forName("no.mesta.mipss.driftkontrakt.DriftkontraktAdminModule");
			boolean isRunning = driftkontraktAdminModule.getLoader().isPluginRunning(pluginClass);
			if (isRunning){
				RefreshLeverandorMessage msg = new RefreshLeverandorMessage();
				msg.setTargetPlugin(driftkontraktAdminModule.getLoader().getPlugin(pluginClass));
				driftkontraktAdminModule.getLoader().sendMessage(msg);
			}
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
    
	public Action getKnyttKjoretoyAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy, final Action reloadKjoretoyListAction){
    	return new AbstractAction(ResourceUtils.getResource("button.knyttKjoretoy")){
			private KjoretoySearchPanel kjoretoySearchPanel;

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new JDialog(driftkontraktAdminModule.getLoader().getApplicationFrame(), true);
				dialog.setTitle(ResourceUtils.getResource("dialogTitle.sokKjoretoy"));
				if (kjoretoySearchPanel==null)
					kjoretoySearchPanel = new KjoretoySearchPanel(DriftkontraktController.this);
				else{
					kjoretoySearchPanel.reload();
				}
				dialog.add(kjoretoySearchPanel);
				dialog.setSize(new Dimension(700, 400));
				dialog.setLocationRelativeTo(driftkontraktAdminModule.getLoader().getApplicationFrame());
				dialog.setVisible(true);
				
				if (!kjoretoySearchPanel.isCancelled()){
					tblKjoretoy.clearSelection();
					reloadKjoretoyListAction.actionPerformed(null);
				}
				
			}
    	};
    }
    
    public Action getKobleFraKjoretoyAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy, final Action reloadKjoretoyListAction){
    	return new AbstractAction(ResourceUtils.getResource("button.kobleFraKjoretoy")){
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = tblKjoretoy.getSelectedRow();
				if (index!=-1){
					KjoretoyslisteTableObject kjoretoy= modelKjoretoy.get(tblKjoretoy.convertRowIndexToModel(index));
					
					Object serienummer = kjoretoy.getDfuSerienummer().getValue();
					Object telefon = kjoretoy.getDfuTlfnummer().getValue();
					String dfuSN = serienummer != null ? kjoretoy.getDfuSerienummer().getValue().toString() : "";
					String dfuTLF = telefon != null ? kjoretoy.getDfuTlfnummer().getValue().toString() : "";
					
					String message = "";
					if ((!dfuSN.equals("") || !dfuTLF.equals("")) && kjoretoy.getAnsvarligFlagg().equals(Long.valueOf(1))) {
						message = ResourceUtils.getResource("message.kobleFraHarDfu.message", EpostUtils.getSupportEpostAdresse());
						JOptionPane.showMessageDialog(driftkontraktAdminModulePanel, message, ResourceUtils.getResource("message.kobleFra.title"), JOptionPane.WARNING_MESSAGE);
					}else{
						message = ResourceUtils.getResource("message.kobleFra.message");
						int r = JOptionPane.showConfirmDialog(driftkontraktAdminModulePanel, message, ResourceUtils.getResource("message.kobleFra.title"), JOptionPane.YES_NO_OPTION);
						if (r!=JOptionPane.YES_OPTION){
							return;
						}else{
							contractBean.fjernKontraktKjoretoy(currentDriftkontrakt.getId(), kjoretoy.getKjoretoyId(), driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
							tblKjoretoy.clearSelection();
							reloadKjoretoyListAction.actionPerformed(null);
						}
					}
				}
			}
    	};
    }
    
    public List<KjoretoyListV> searchForKjoretoy(String searchString){
    	List<KjoretoyListV> kjoretoyListVList = kjoretoyBean.getKjoretoyListVList(searchString);
    	return kjoretoyListVList;
    }
    
    
    public Action getEndreKjoretoyAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy, final Action reloadKjoretoyListAction, final Integer faneIndex) {
    	return new AbstractAction(ResourceUtils.getResource("button.endreKjoretoy")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKjoretoy.getSelectedRow();
				if(currentDriftkontrakt != null) {
					if(selectedRow >= 0 && selectedRow < tblKjoretoy.getRowCount()) {
						tblKjoretoy.setCursor(new Cursor(Cursor.WAIT_CURSOR));
						KjoretoyslisteTableObject kjoretoyslisteTableObject = modelKjoretoy.getEntities().get(tblKjoretoy.convertRowIndexToModel(selectedRow)); 
						KontraktKjoretoyInfo kontraktKjoretoyInfo = kjoretoyslisteTableObject.getKontraktKjoretoyInfo();
						if(kontraktKjoretoyInfo != null) {
							Kjoretoy kjoretoy = getKjoretoy(kontraktKjoretoyInfo.getKjoretoyId()); 
							if(kjoretoy != null) {
								WizardPanel wizardPanel = new WizardPanel(driftkontraktAdminModule.getLoader().getApplicationFrame(), driftkontraktAdminModule, true, currentDriftkontrakt, kjoretoy, true, true);							
								EndreKjoretoyDialog endreKjoretoyDialog = new EndreKjoretoyDialog(getInstance(), driftkontraktAdminModule.getLoader().getApplicationFrame(), kjoretoy, wizardPanel.getWizard());
								endreKjoretoyDialog.setSelectedTabIndex(faneIndex);
								String title = kontraktKjoretoyInfo.getRegnr()+" "+kontraktKjoretoyInfo.getLeverandorNavn()+"("+kontraktKjoretoyInfo.getLeverandorNr()+")";
								endreKjoretoyDialog.setTitle(ResourceUtils.getResource("dialogTitle.endreKjoretoy")+" "+title);
								kjoretoy = endreKjoretoyDialog.showDialog();
								reloadKjoretoyListAction.actionPerformed(null);
							}
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.finnerIkkeKjoretoyknytning"));
						}
						tblKjoretoy.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					}
				}
			}
    	};
    }
    
    public Action getVisKjoretoyGrafAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy, final JXDatePicker datePicker) {
    	return new AbstractAction(ResourceUtils.getResource("button.kjoretoyGraf")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKjoretoy.getSelectedRow();
				if(currentDriftkontrakt != null) {
					if(selectedRow >= 0 && selectedRow < tblKjoretoy.getRowCount()) {
						KjoretoyslisteTableObject kjoretoyslisteTableObject = modelKjoretoy.getEntities().get(tblKjoretoy.convertRowIndexToModel(selectedRow)); 
						KontraktKjoretoyInfo kontraktKjoretoyInfo = kjoretoyslisteTableObject.getKontraktKjoretoyInfo();
						if(kontraktKjoretoyInfo != null) {
							Date startDate = null;
							Date stopDate = null;
							if(datePicker.isEnabled()) {
								startDate = datePicker.getDate();
								Calendar cal = Calendar.getInstance();
								cal.setTime(startDate);
								cal.add(Calendar.DATE, 1);
								stopDate = cal.getTime();
							} else {
								stopDate = Clock.now();
								Calendar cal = Calendar.getInstance();
								cal.setTime(stopDate);
								cal.add(Calendar.DATE, -1);
								startDate = cal.getTime();
							}
							
							IMipssStudioLoader loader = driftkontraktAdminModule.getLoader();
							MipssPlugin plugin = loader.getPlugin(ChartModule.class);
							OpenSingleChartMessage m = new OpenSingleChartMessage(kontraktKjoretoyInfo.getKjoretoyId(), startDate, stopDate, null);
							m.setTargetPlugin(plugin);
							loader.sendMessage(m);
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.finnerIkkeKjoretoyknytning"));
						}
					}
				}
			}
    	};
    }
    
    /**
     * Action som åpner MipssLogg-modulen og sender inn parametre basert på valgt kjøretøy i listen.
     * @param tblKjoretoy
     * @param modelKjoretoy
     * @param datePickerFra
     * @param datePickerTil
     * @return Action
     */
    public Action getVisKjoretoyRaadataAction (final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy){
    	return new AbstractAction(ResourceUtils.getResource("button.kjoretoyRaadata")) {		
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKjoretoy.getSelectedRow();
				if(currentDriftkontrakt != null) {
					if(selectedRow >= 0 && selectedRow < tblKjoretoy.getRowCount()) {
						KjoretoyslisteTableObject kjoretoyslisteTableObject = modelKjoretoy.getEntities().get(tblKjoretoy.convertRowIndexToModel(selectedRow)); 
						KontraktKjoretoyInfo kontraktKjoretoyInfo = kjoretoyslisteTableObject.getKontraktKjoretoyInfo();
						if(kontraktKjoretoyInfo != null) {
							IMipssStudioLoader loader = driftkontraktAdminModule.getLoader();
							MipssPlugin plugin = loader.getPlugin(MipssLoggModule.class);
							OpenMipssloggMessage m = new OpenMipssloggMessage(kontraktKjoretoyInfo.getDfuId(), kontraktKjoretoyInfo.getRegnr(), kontraktKjoretoyInfo.getDfuSerienummer(), getSearchFromDate(), getSearchToDate());
							m.setTargetPlugin(plugin);
							loader.sendMessage(m);
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.finnerIkkeKjoretoyknytning"));
						}
					}
				}				
			}
		};
    }
    

    public Action getVisUkjentStrometodeAction (final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy){
    	return new AbstractAction("Ukjent strømetode") {		
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKjoretoy.getSelectedRow();
				if(currentDriftkontrakt != null) {
					if(selectedRow >= 0 && selectedRow < tblKjoretoy.getRowCount()) {
						KjoretoyslisteTableObject kjoretoyslisteTableObject = modelKjoretoy.getEntities().get(tblKjoretoy.convertRowIndexToModel(selectedRow)); 
						KontraktKjoretoyInfo kontraktKjoretoyInfo = kjoretoyslisteTableObject.getKontraktKjoretoyInfo();
						kontraktKjoretoyInfo.getTextForGUI();
						if(kontraktKjoretoyInfo != null) {					
							createUkjentStroproduktRapport(getSearchFromDate(), getSearchToDate(), kontraktKjoretoyInfo.getKjoretoyId());							
						}
					}
				}				
			}
		};
    }
    
    private void createUkjentStroproduktRapport(Date fraDatoTid, Date tilDatoTid, Long kjoretoyId) {
		try {
			Prodrapport rapport = BeanUtil.lookup(Prodrapport.BEAN_NAME, Prodrapport.class);
			List<UkjentStroprodukt> ukjenteStroprodukter = rapport.genererUkjentStroproduktRapport(null, fraDatoTid, tilDatoTid, kjoretoyId);
			if (ukjenteStroprodukter.isEmpty()) {
				JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(driftkontraktAdminModule.getModuleGUI()), "Fant ingen data for utvalget", "Ingen data funnet", JOptionPane.INFORMATION_MESSAGE);
			} else{
				new UkjentStroproduktRapport(ukjenteStroprodukter, fraDatoTid, tilDatoTid, kjoretoyId).finalizeAndOpen();
			}
		} catch (Throwable t) {
			ErrorHandler.showError(t, Resources.getResource("message.feilVedHentingAvRapporten", EpostUtils.getSupportEpostAdresse()));
			t.printStackTrace();
		}
	}
    
	public Date getSearchFromDate() {
		return searchFromDate;
	}

	public void setSearchFromDate(Date fromDate) {
		this.searchFromDate = fromDate;
	}

	public Date getSearchToDate() {
		return searchToDate;
	}

	public void setSearchToDate(Date toDate) {
		this.searchToDate = toDate;
	}

	private Colour getColor(ColorableTableValue k){
    	if (k.isRed())
    		return Colour.RED;
    	if (k.isYellow())
    		return Colour.YELLOW;
    	return Colour.WHITE;
    }
    public Action getVisKjoretoyslisteIExcelAction(final JMipssBeanTable<KjoretoyslisteTableObject> tblKjoretoyList, final MipssBeanTableModel<KjoretoyslisteTableObject> model) {
    	return new AbstractAction(ResourceUtils.getResource("button.kjoretoylisteRapport")) {
    		private String removeHtmlFormatting(String title){
    			if (title==null)
    				return null;
    			String htmlRemoved = title.replaceAll("\\<.*?\\>", "");
    			return htmlRemoved;
    		}
			@Override
			public void actionPerformed(ActionEvent e) {
				if(currentDriftkontrakt != null) {
					ExcelHelper excel = new ExcelHelper();
					excel.setDateFormat(MipssDateFormatter.SHORT_DATE_TIME_FORMAT);
					WritableWorkbook workbook = excel.createNewWorkbook("kjoretoyExport");
					WritableSheet kjoretoySheet = excel.createNewSheet("kjoretoy", workbook, 0);
					
					String[] headers = ResourceUtils.getResource("excel.reportHeaders").split(",");
					excel.writeHeaders(kjoretoySheet, 0, 0, true, headers);
					
					List<KjoretoyslisteTableObject> kjoretoy = model.getEntities();
					int r = 1;
					
					for (KjoretoyslisteTableObject k:kjoretoy){
						int c = 0;
						excel.writeData(kjoretoySheet, r, c++, k.getEksterntNavn(), Colour.WHITE, "");
						excel.writeData(kjoretoySheet, r, c++, k.getRegnr(), Colour.WHITE, "");
						excel.writeData(kjoretoySheet, r, c++, k.getTypeOgFabrikat().getValue(), getColor(k.getTypeOgFabrikat()), k.getTypeOgFabrikat().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getLeverandor().getValue(), getColor(k.getLeverandor()), k.getLeverandor().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getBestillingStatus().getValue(), getColor(k.getBestillingStatus()), k.getBestillingStatus().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getAktuellKonfigKlartekst().getValue(), getColor(k.getAktuellKonfigKlartekst()), k.getAktuellKonfigKlartekst().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getAktuellStroperiodeKlartekst().getValue(), getColor(k.getAktuellStroperiodeKlartekst()), k.getAktuellStroperiodeKlartekst().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getDfuTlfnummer().getValue(), getColor(k.getDfuTlfnummer()), k.getDfuTlfnummer().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getDfuSerienummer().getValue(), getColor(k.getDfuSerienummer()), k.getDfuSerienummer().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getUkjentProduksjon().getValue(), getColor(k.getUkjentProduksjon()), removeHtmlFormatting(k.getUkjentProduksjon().getTooltipText()));
						excel.writeData(kjoretoySheet, r, c++, k.getUkjentStrometode().getValue(), getColor(k.getUkjentStrometode()), removeHtmlFormatting(k.getUkjentStrometode().getTooltipText()));
						excel.writeData(kjoretoySheet, r, c++, k.getDfuSisteLivtegnDato().getValue(), getColor(k.getDfuSisteLivtegnDato()), k.getDfuSisteLivtegnDato().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getDfuSistePosisjonDatoCET().getValue(), getColor(k.getDfuSistePosisjonDatoCET()),k.getDfuSistePosisjonDatoCET().getTooltipText());
						excel.writeData(kjoretoySheet, r, c++, k.getTomkjoringKm(), Colour.WHITE, "");
						excel.writeData(kjoretoySheet, r, c++, k.getTotalKm(), Colour.WHITE, "");
						excel.writeData(kjoretoySheet, r, c++, k.getTomkjoringProsent(), Colour.WHITE, "");
						r++;
					}
					int c=0;
					for (String head:headers){
						excel.setColumnView(kjoretoySheet, c++, head);
					}
					excel.closeWorkbook(workbook);
					DesktopHelper.openFile(excel.getExcelFile());
				}
			}
    	};
    }
    
    public Action getVisKjoretoyIKartAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy) {
    	return new AbstractAction(ResourceUtils.getResource("button.kjoretoyMap")) {
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKjoretoy.getSelectedRow();
				if(currentDriftkontrakt != null) {
					if(selectedRow >= 0 && selectedRow < tblKjoretoy.getRowCount()) {
						KjoretoyslisteTableObject kjoretoyslisteTableObject = modelKjoretoy.getEntities().get(tblKjoretoy.convertRowIndexToModel(selectedRow)); 
						KontraktKjoretoyInfo kontraktKjoretoyInfo = kjoretoyslisteTableObject.getKontraktKjoretoyInfo();
						if(kontraktKjoretoyInfo != null) {
							if(kontraktKjoretoyInfo.getDfuSisteProdPosisjonDato() == null) {
								JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.manglerSisteGPSPos"));
								return;
							}
							try {
								IMipssStudioLoader loader = driftkontraktAdminModule.getLoader();
								MipssPlugin target = loader.getPlugin((Class<? extends MipssPlugin>)Class.forName("no.mesta.mipss.mipssmap.MipssMapModule"));
								MipssMapPosKjoretoyMessage m = new MipssMapPosKjoretoyMessage(kontraktKjoretoyInfo.getKjoretoyId(), DateFormatter.convertFromGMTtoCET(kontraktKjoretoyInfo.getDfuSisteProdPosisjonDato()), kontraktKjoretoyInfo.getLeverandorNavn(), kontraktKjoretoyInfo.getEksterntNavn());
								m.setTargetPlugin(target);
								loader.sendMessage(m);
							} catch (ClassNotFoundException e1) {
								e1.printStackTrace();
							}
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.finnerIkkeKjoretoyknytning"));
						}
					}
				}
			}
    	};
    }
    
    public Action getVisKjoretoyProduksjonAction(final JXTable tblKjoretoy, final MipssBeanTableModel<KjoretoyslisteTableObject> modelKjoretoy, final JXDatePicker datePicker) {
    	return new AbstractAction(ResourceUtils.getResource("button.kjoretoyProduksjon"), IconResources.YELLOW_HELMET_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblKjoretoy.getSelectedRow();
				if(currentDriftkontrakt != null) {
					if(selectedRow >= 0 && selectedRow < tblKjoretoy.getRowCount()) {
						KjoretoyslisteTableObject kjoretoyslisteTableObject = modelKjoretoy.getEntities().get(tblKjoretoy.convertRowIndexToModel(selectedRow)); 
						KontraktKjoretoyInfo kontraktKjoretoyInfo = kjoretoyslisteTableObject.getKontraktKjoretoyInfo();
						if(kontraktKjoretoyInfo != null) {
							Kjoretoy kjoretoy = getKjoretoy(kontraktKjoretoyInfo.getKjoretoyId());
							SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
							SimpleDateFormat clockFormatter = new SimpleDateFormat("HH:mm:ss");
							
							Date startDate = null;
							Date stopDate = null;
							if(datePicker.isEnabled()) {
								startDate = datePicker.getDate();
								Calendar cal = Calendar.getInstance();
								cal.setTime(startDate);
								cal.add(Calendar.DATE, 1);
								stopDate = cal.getTime();
							} else {
								stopDate = Clock.now();
								Calendar cal = Calendar.getInstance();
								cal.setTime(stopDate);
								cal.add(Calendar.DATE, -1);
								startDate = cal.getTime();
							}
							
							if(kjoretoy.getEksterntNavn() != null) {
								MipssLogg.startMipssLogg("Reg", 
										kjoretoy.getEksterntNavn(), 
										dateFormatter.format(startDate), 
										clockFormatter.format(startDate),
										dateFormatter.format(stopDate), 
										clockFormatter.format(stopDate),
										"ReadOnly");
							}
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), ResourceUtils.getResource("message.finnerIkkeKjoretoyknytning"));
						}
					}
				}
			}
    	};
    }
    
    public void setOverriddenChange(Boolean overriddenChange) {
    	kontraktSaveMonitor.setOverriddenChange(overriddenChange);
    }
    
    public Action getNyFavorittlisteAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.ny"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					Favorittliste favorittliste = new Favorittliste();
					favorittliste.setDriftkontrakt(driftkontrakt);
					favorittliste.setLagreFavorittProsesser(Boolean.TRUE);
					FavorittlisteDialog favorittlisteDialog = new FavorittlisteDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), favorittliste);
					
					favorittliste = favorittlisteDialog.showDialog();
					if(favorittliste != null) {
						favorittliste.setOpprettetAv(driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
						favorittliste.setOpprettetDato(new Timestamp(Calendar.getInstance().getTimeInMillis()));
						driftkontrakt.getFavorittlisteList().add(favorittliste);
					}
				}
			}
		};
    }
    
    public Action getEndreFavorittlisteAction(final MipssBeanTableModel<Favorittliste> favorittlisteModel, final JXTable favorittlisteTable) {
    	return new AbstractAction(ResourceUtils.getResource("button.endreFavorittliste"), IconResources.DETALJER_SMALL_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = favorittlisteTable.getSelectedRow(); 
				if (selectedIndex != -1){
					Favorittliste favorittliste = favorittlisteModel.getEntities().get(favorittlisteTable.convertRowIndexToModel(selectedIndex));
					if(favorittliste != null) {
						Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
						if(driftkontrakt != null) {
							FavorittlisteDialog favorittlisteDialog = new FavorittlisteDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), favorittliste);
							Favorittliste endretFavorittliste = favorittlisteDialog.showDialog();
							if(endretFavorittliste != null) {
								favorittliste.merge(endretFavorittliste);
								favorittliste.setEndretAv(driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
								favorittliste.setEndretDato(new Timestamp(Calendar.getInstance().getTimeInMillis()));
								kontraktSaveMonitor.setOverriddenChange(Boolean.TRUE);
							}
							favorittlisteModel.fireTableModelEvent(favorittlisteTable.convertRowIndexToModel(selectedIndex), favorittlisteTable.convertRowIndexToModel(selectedIndex), TableModelEvent.UPDATE);
						}
					}
				}
			}
		};
    }
    
    public Action getSlettFavorittlisteAction(final MipssBeanTableModel<Favorittliste> favorittlisteModel, final JXTable favorittlisteTable) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett"), IconResources.REMOVE_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				Driftkontrakt driftkontrakt = getCurrentDriftkontrakt();
				if(driftkontrakt != null) {
					List<Favorittliste> favorittlisteList = new ArrayList<Favorittliste>();
					int[] selectedIndexes = favorittlisteTable.getSelectedRows();
					for(int i=0; i<selectedIndexes.length; i++) {
						Favorittliste favorittliste = favorittlisteModel.get(favorittlisteTable.convertRowIndexToModel(selectedIndexes[i]));
						favorittlisteList.add(favorittliste);
					}
					if(favorittlisteList.size() > 0) {
						driftkontrakt.getFavorittlisteList().removeAll(favorittlisteList);
					}
				}
			}
		};
    }
        
    public Action getNymonteringAction(final Action reloadAction) {
    	return new AbstractAction(ResourceUtils.getResource("button.nymontering"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				final BestillingSkjema bestillingSkjema = new BestillingSkjema(BestillingSkjemaType.OPPRETTING, getCurrentDriftkontrakt(),
						null, driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
				bestillingSkjema.showWizard();
				reloadAction.actionPerformed(null);
			}
    	};
    }
    
    public Action getEndreMonteringAction(final JXTable tblOrdre, final MipssBeanTableModel<Kjoretoyordre> tblModelOrdre, final Action reloadAction) {
    	return new AbstractAction(ResourceUtils.getResource("button.endreMontering"), IconResources.EDIT_ICON) {
    		@Override
			public void actionPerformed(ActionEvent e) {
    			int selectedRow = tblOrdre.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblOrdre.getRowCount()) {
					Kjoretoyordre kjoretoyordre = tblModelOrdre.getEntities().get(tblOrdre.convertRowIndexToModel(selectedRow));
					if(kjoretoyordre != null) {
						KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
						String opprettetStatusVerdi = params.hentEnForApp("studio.mipss.montering", "ordrestatusOverlevertKoord").getVerdi();
						
						Kjoretoyordre ko = kjoretoyBean.getKjoretoyordreForAdmin(kjoretoyordre.getId());
						Kjoretoyordrestatus sisteKjoretoyordrestatus = ko.getSisteKjoretoyordrestatus();
						if(sisteKjoretoyordrestatus.getIdent().equals(opprettetStatusVerdi)) {
							final BestillingSkjema bestillingSkjema = new BestillingSkjema(BestillingSkjemaType.ENDRING, getCurrentDriftkontrakt(),
									kjoretoyordre.getId(), driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
							bestillingSkjema.showWizard();
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), 
									ResourceUtils.getResource("message.feilStatusForEndring.message", EpostUtils.getSupportEpostAdresse()), 
									ResourceUtils.getResource("message.feilStatusForEndring.title"),
									JOptionPane.INFORMATION_MESSAGE);
						}
						reloadAction.actionPerformed(null);
					}
				}
    		}
    	};
    }
    
    public Action getFerdigstillBestillingAction(final JXTable tblOrdre, final MipssBeanTableModel<Kjoretoyordre> tblModelOrdre, final Action reloadAction) {
    	return new AbstractAction(ResourceUtils.getResource("button.ferdigstillBestilling"), IconResources.OK2_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblOrdre.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblOrdre.getRowCount()) {
					Kjoretoyordre kjoretoyordre = tblModelOrdre.getEntities().get(tblOrdre.convertRowIndexToModel(selectedRow));
					if(kjoretoyordre != null) {
						KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
						String behandletAdminStatusNavn = params.hentEnForApp("studio.mipss.montering", "ordrestatusFullfortEkstLev").getVerdi();
						int indexOf = tblModelOrdre.getEntities().indexOf(kjoretoyordre);
						kjoretoyordre = kjoretoyBean.getKjoretoyordreForAdmin(kjoretoyordre.getId());
						tblModelOrdre.getEntities().set(indexOf, kjoretoyordre);
						String regnr = kjoretoyordre.getKjoretoy().getRegnr();
						Long maskinnummer = kjoretoyordre.getKjoretoy().getMaskinnummer();
						String eksterntNavn = kjoretoyordre.getKjoretoy().getEksterntNavn();
						if (regnr==null&&maskinnummer==null&&eksterntNavn==null){
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), 
									ResourceUtils.getResource("message.manglerRegnrEllerMaskinnummer.message"), 
									ResourceUtils.getResource("message.manglerRegnrEllerMaskinnummer.title"),
									JOptionPane.INFORMATION_MESSAGE);
							return;
						}
						if(kjoretoyordre.getSisteKjoretoyordrestatus().getIdent().equals(behandletAdminStatusNavn)) {
							final BestillingSkjema bestillingSkjema = new BestillingSkjema(BestillingSkjemaType.KONTRAKT_BEHANDLING, 
									getCurrentDriftkontrakt(),
									kjoretoyordre.getId(), 
									driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
							bestillingSkjema.showWizard();
							reloadAction.actionPerformed(null);
						} else {
							JOptionPane.showMessageDialog(getDriftkontraktAdminModule().getLoader().getApplicationFrame(), 
									ResourceUtils.getResource("message.feilStatusForBehandling.message"), 
									ResourceUtils.getResource("message.feilStatusForBehandling.title"),
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}
			}
    	};
    }
    
    public Action getVisBestillingsskjemaAction(final JXTable tblOrdre, final MipssBeanTableModel<Kjoretoyordre> tblModelOrdre) {
    	return new AbstractAction(ResourceUtils.getResource("button.visBestillingsskjema"), IconResources.ADOBE_16) {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = tblOrdre.getSelectedRow();
				if(selectedRow >= 0 && selectedRow < tblOrdre.getRowCount()) {
					Kjoretoyordre kjoretoyordre = tblModelOrdre.getEntities().get(tblOrdre.convertRowIndexToModel(selectedRow));
					if(kjoretoyordre != null) {
						int indexOf = tblModelOrdre.getEntities().indexOf(kjoretoyordre);
						kjoretoyordre = kjoretoyBean.getKjoretoyordreForAdmin(kjoretoyordre.getId());
						tblModelOrdre.getEntities().set(indexOf, kjoretoyordre);
						final BestillingSkjema bestillingSkjema = new BestillingSkjema(BestillingSkjemaType.RAPPORTUTTAK, 
								getCurrentDriftkontrakt(),
								kjoretoyordre.getId(), 
								driftkontraktAdminModule.getLoader().getLoggedOnUserSign());
						bestillingSkjema.genererRapport();
					}
				}
			}
    	};
    }
    
    
    public List<Pdabruker> sokPdabruker(String navn){
    	List<Pdabruker> sokPdabruker = contractBean.sokPdabruker(navn);
    	if (sokPdabruker==null){
    		sokPdabruker = new ArrayList<Pdabruker>();
    	}
    	try {
    		
    		Boolean useAD = Boolean.valueOf(KonfigparamPreferences.getInstance().hentEnForApp("studio.mipss", "useADLogIn").getVerdi());
    		if (useAD) {
    			List<LdapUser> search = LdapUserFactory.getInstanceForSearch().search(navn);
    			for (LdapUser u:search){
    				Pdabruker p = new Pdabruker();
    				p.setNavn(u.getCommonName());
    				p.setSignatur(u.getSAMAccountName());
    				p.setEpostAdresse(u.getMail());
    				try{
    					p.setTlfnummer(Long.valueOf(u.getMobile()));
    				} catch (Exception e){
    					
    				}
    				if (!sokPdabruker.contains(p)){
    					sokPdabruker.add(p);
    				}
    			}
			} else{
				StabQueryFilter stabQueryFilter = new StabQueryFilter();
				stabQueryFilter.setNavn(navn);
				List<Bruker> brukerList = contractBean.getBrukerList(stabQueryFilter);
				for (Bruker bruker : brukerList) {
					Pdabruker p = new Pdabruker();
					p.setNavn(bruker.getNavn());
					p.setSignatur(bruker.getSignatur());
					p.setEpostAdresse(bruker.getEpost());
					
					if (!sokPdabruker.contains(p)){
    					sokPdabruker.add(p);
    				}
				}
			}
			
		} catch (LdapException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sokPdabruker;
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
}
