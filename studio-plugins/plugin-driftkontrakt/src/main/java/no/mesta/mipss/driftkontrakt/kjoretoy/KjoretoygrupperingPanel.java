package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyGruppering;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.DateTableCellRenderer;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class KjoretoygrupperingPanel extends JPanel implements PropertyChangeListener{

	private final KjoretoygruppeController controller;

	private MipssComboBoxModel<Kjoretoygruppe> modelGruppe;
	private JComboBox cmbGruppe;
	
	private JTextArea txtBeskrivelse;
	private JScrollPane scrollBeskrivelse;
	private JScrollPane scrollKjoretoy;
	
	private JButton btnNyGruppering;
	private JButton btnEndreGruppering;
	private JButton btnSlettGruppering;
	private JButton btnFjernKjoretoy;
	private JButton btnNyttKjoretoy;

	private JLabel lblGruppe;
	private JXBusyLabel busyLabel;
	
	private JMipssBeanTable<KontraktKjoretoyGruppering> tblKjoretoy;
	private MipssBeanTableModel<KontraktKjoretoyGruppering> tblModelKjoretoy;
	
	public KjoretoygrupperingPanel(KjoretoygruppeController controller){
		this.controller = controller;
		
		controller.addPropertyChangeListener("currentDriftkontrakt", this);
		
		initComponents();
		initGui();
		bind();
		
	}
	
	private void bind() {
		BindingHelper.createbinding(cmbGruppe, "selectedItem", this, "kjoretoygruppe").bind();
	}

	private void initComponents(){
		modelGruppe = new MipssComboBoxModel<Kjoretoygruppe>(controller.getKjoretoygrupperForValgtDriftkontrakt(), true);
		cmbGruppe = new JComboBox(modelGruppe);
        MipssListCellRenderer<Kjoretoygruppe> renderer = new MipssListCellRenderer<Kjoretoygruppe>();
        cmbGruppe.setRenderer(renderer);
        cmbGruppe.setMinimumSize(new Dimension(250,20));
		
		tblModelKjoretoy = new MipssBeanTableModel<KontraktKjoretoyGruppering>(KontraktKjoretoyGruppering.class, "kjoretoynavn", "type", "merke", "leverandor", "regnr");
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(KontraktKjoretoyGruppering.class, tblModelKjoretoy.getColumns());
		tblKjoretoy = new JMipssBeanTable<KontraktKjoretoyGruppering>(tblModelKjoretoy, colModel);
		tblKjoretoy.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblKjoretoy.setFillsViewportWidth(true);
		tblKjoretoy.setDoWidthHacks(false);
		
		scrollKjoretoy = new JScrollPane(tblKjoretoy);
		
		btnNyGruppering = new JButton(controller.getNyKjoretoygruppeAction(getReloadAction()));
		btnNyGruppering.setToolTipText(ResourceUtils.getResource("button.nyKjoretoygruppe.tooltip"));
		btnNyGruppering.setMargin(new Insets(0,0,0,0));
		btnEndreGruppering = new JButton(controller.getEndreKjoretoygruppeAction(cmbGruppe, getReloadAction()));
		btnEndreGruppering.setToolTipText(ResourceUtils.getResource("button.endreKjoretoygruppe.tooltip"));
		btnEndreGruppering.setMargin(new Insets(0,0,0,0));
		btnSlettGruppering = new JButton(controller.getSlettKjoretoygruppeAction(cmbGruppe, getReloadAction()));
		btnSlettGruppering.setToolTipText(ResourceUtils.getResource("button.slettKjoretoygruppe.tooltip"));
		btnSlettGruppering.setMargin(new Insets(0,0,0,0));
		btnNyttKjoretoy = new JButton(controller.getLeggTilKjoretoyiGruppeAction(cmbGruppe, getReloadAction()));
		btnNyttKjoretoy.setToolTipText(ResourceUtils.getResource("button.leggTiliGruppe.tooltip"));
		btnFjernKjoretoy = new JButton(controller.getFjernKjoretoyFraGruppeAction(tblKjoretoy, tblModelKjoretoy, getReloadTableAction()));
		btnFjernKjoretoy.setToolTipText(ResourceUtils.getResource("button.fjernFraKjoretoygruppe.tooltip"));
		
		lblGruppe = new JLabel(ResourceUtils.getResource("label.kjoretoygruppe"));
		
		txtBeskrivelse = new JTextArea();
		scrollBeskrivelse = new JScrollPane(txtBeskrivelse);
		txtBeskrivelse.setEditable(false);
		
		busyLabel = new JXBusyLabel();
		busyLabel.setText(ResourceUtils.getResource("label.henterKjoretoy"));
		
		BindingHelper.createbinding(tblKjoretoy, "${noOfSelectedRows >= 1}", btnFjernKjoretoy, "enabled").bind();
		BindingHelper.createbinding(cmbGruppe, "${selectedItem != null}", btnEndreGruppering, "enabled").bind();
		BindingHelper.createbinding(cmbGruppe, "${selectedItem != null}", btnSlettGruppering, "enabled").bind();
		BindingHelper.createbinding(cmbGruppe, "${selectedItem != null}", btnNyttKjoretoy, "enabled").bind();

	}
	
	private void initGui() {
		setLayout(new GridBagLayout());

		JPanel pnlButton = new JPanel(new GridBagLayout());
		pnlButton.add(btnNyttKjoretoy, 	new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlButton.add(btnFjernKjoretoy, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		
		JPanel beskrivelsePanel = new JPanel(new BorderLayout());
		beskrivelsePanel.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.gruppebeskrivelse")));
		beskrivelsePanel.add(scrollBeskrivelse);
		beskrivelsePanel.setMinimumSize(new Dimension(300, 100));
		
		JLabel icon = new JLabel(IconResources.LASTEBIL_ICON, JLabel.RIGHT);
		
		add(lblGruppe, 			new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15, 10, 5, 5), 0, 0));
		add(cmbGruppe, 			new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(15, 10, 5, 5), 0, 0));
		add(btnEndreGruppering, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15, 5, 5, 3), 0, 0));
		add(btnNyGruppering, 	new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15, 5, 5, 3), 0, 0));
		add(btnSlettGruppering, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15, 5, 5, 3), 0, 0));
		add(beskrivelsePanel, 	new GridBagConstraints(0, 1, 5, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 10, 5, 5), 0, 0));
		add(icon, 				new GridBagConstraints(5, 0, 1, 2, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(5, 10, 5, 5), 0, 0));
		add(scrollKjoretoy, 	new GridBagConstraints(0, 2, 6, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 10, 5, 5), 0, 0));

		add(pnlButton, 			new GridBagConstraints(0, 3, 6, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
	}

	/**
	 * Setter valgt Kjøretøygruppe
	 * @param kj
	 */
	public void setKjoretoygruppe(final Kjoretoygruppe kj){
		if (kj!=null){
			txtBeskrivelse.setText(kj.getBeskrivelse());
			busyLabel.setBusy(true);
			scrollKjoretoy.setViewportView(busyLabel);
			new SwingWorker<Void, Void>(){
				private List<KontraktKjoretoyGruppering> kjoretoyIGruppe;
				@Override
				public Void doInBackground(){
					kjoretoyIGruppe = controller.getKjoretoyIGruppe(kj);
					return null;
				}
				public void done(){
					tblModelKjoretoy.setEntities(kjoretoyIGruppe);
					scrollKjoretoy.setViewportView(tblKjoretoy);
					busyLabel.setBusy(false);
				}
			}.execute();
			
		}else{
			txtBeskrivelse.setText("");
			tblModelKjoretoy.setEntities(new ArrayList<KontraktKjoretoyGruppering>());
		}
	}
	
	private Action getReloadTableAction(){
		return new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				Kjoretoygruppe gr = (Kjoretoygruppe)e.getSource();
				setKjoretoygruppe(gr);
			}
		};
	}
	private Action getReloadAction(){
		return new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				reload();
				if (e!=null){
					Kjoretoygruppe gr = (Kjoretoygruppe)e.getSource();
					cmbGruppe.setSelectedItem(gr);
				}else{
					cmbGruppe.setSelectedItem(null);
				}
			}
		};
	}

	private void reload() {
		System.out.println("reload");
		modelGruppe.setEntities(controller.getKjoretoygrupperForValgtDriftkontrakt());
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		reload();
		
	}
}
