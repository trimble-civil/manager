package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordre;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordrestatus;
import no.mesta.mipss.persistence.kjoretoyordre.Kjoretoyordretype;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.RenderableMipssEntityTableCellRenderer;

import org.jdesktop.beansbinding.BindingGroup;

@SuppressWarnings("serial")
public class KjoretoyordreListPanel extends JPanel implements PropertyChangeListener {
	private DriftkontraktController driftkontraktController;
	private MipssBeanTableModel<Kjoretoyordre> tblModelOrdre;
	private BindingGroup bindingGroup;
	
	private JMipssBeanTable<Kjoretoyordre> tblOrdre;
	private JScrollPane scrlPaneOrdre = new JScrollPane();
	private JPanel pnlButtons = new JPanel();
	private JButton btnNymontering = new JButton();
	private JButton btnEndre = new JButton();
	
	private JButton btnFerdigstillBestilling = new JButton();
	private JButton btnVisBestillingsskjema = new JButton();
	private final Action reloadKjoretoylisteAction;
	
	public KjoretoyordreListPanel(final DriftkontraktController driftkontraktController, final Action reloadKjoretoylisteAction) {
		this.driftkontraktController = driftkontraktController;
		this.reloadKjoretoylisteAction = reloadKjoretoylisteAction;
		
		bindingGroup = new BindingGroup();
		
		driftkontraktController.addPropertyChangeListener("currentDriftkontrakt", this);
		
		initTable();
		initButtons();
		initGui();
		
		bindingGroup.bind();
	}
	
	private void initTable() {
		tblModelOrdre = new MipssBeanTableModel<Kjoretoyordre>(Kjoretoyordre.class, "id", "kjoretoyEksterntNavn", "kjoretoyRegnr", "kjoretoyMaskinnummer", "kjoretoyordretype", "dfuSerienummer", "sisteKjoretoyordrestatus", "sisteKjoretoyordrestatusEndring", "prosjektNr", "signatur");
		MipssRenderableEntityTableColumnModel colModel = new MipssRenderableEntityTableColumnModel(Kjoretoyordre.class, tblModelOrdre.getColumns());
		tblOrdre = new JMipssBeanTable<Kjoretoyordre>(tblModelOrdre, colModel);
		tblOrdre.setFillsViewportWidth(true);
		tblOrdre.setDoWidthHacks(false);
		tblOrdre.setDefaultRenderer(Kjoretoyordretype.class, new RenderableMipssEntityTableCellRenderer<Kjoretoyordretype>());
		tblOrdre.setDefaultRenderer(Kjoretoyordrestatus.class, new RenderableMipssEntityTableCellRenderer<Kjoretoyordrestatus>());
		
		//Brukeren skal inntil videre ikke kunne endre ordre
		
//		tblOrdre.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				if(e.getClickCount() == 2) {
//					int selectedRow = tblOrdre.getSelectedRow();
//					if(selectedRow >= 0 && selectedRow < tblOrdre.getRowCount()) {
//						Action action = driftkontraktController.getEndreKjoretoyordreAction(tblOrdre, tblModelOrdre);
//						action.actionPerformed(null);
//					}
//				}
//			}
//		});
	}
	
	private void initButtons() {
		
		btnNymontering.setAction(driftkontraktController.getNymonteringAction(getReloadAction()));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnNymontering, "enabled"));
		
		btnEndre.setAction(driftkontraktController.getEndreMonteringAction(tblOrdre, tblModelOrdre, getReloadAction()));
		bindingGroup.addBinding(BindingHelper.createbinding(driftkontraktController, "${currentDriftkontrakt != null}", btnEndre, "enabled"));
		
		bindingGroup.addBinding(BindingHelper.createbinding(tblOrdre, "${noOfSelectedRows == 1}", btnFerdigstillBestilling, "enabled"));
		btnFerdigstillBestilling.setAction(driftkontraktController.getFerdigstillBestillingAction(tblOrdre, tblModelOrdre, getReloadAction()));
		
		bindingGroup.addBinding(BindingHelper.createbinding(tblOrdre, "${noOfSelectedRows == 1}", btnVisBestillingsskjema, "enabled"));
		btnVisBestillingsskjema.setAction(driftkontraktController.getVisBestillingsskjemaAction(tblOrdre, tblModelOrdre));
	}
	
	private Action getReloadAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reloadList();
				reloadKjoretoylisteAction.actionPerformed(null);
			}
		};
	}

	private void initGui() {
		tblOrdre.setPreferredScrollableViewportSize(new Dimension(0, 0));
		scrlPaneOrdre.setViewportView(tblOrdre);
		
		pnlButtons.setLayout(new GridBagLayout());
		pnlButtons.add(btnNymontering, 			 new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnEndre, 				 new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnFerdigstillBestilling, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
		pnlButtons.add(btnVisBestillingsskjema,  new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		
		this.setLayout(new GridBagLayout());
		this.add(scrlPaneOrdre, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 5), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(11, 0, 11, 11), 0, 0));
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		reloadList();
	}
	
	private void reloadList() {
		List<Kjoretoyordre> ordreList = new ArrayList<Kjoretoyordre>();
		Driftkontrakt driftkontrakt = driftkontraktController.getCurrentDriftkontrakt();
		
		if(driftkontrakt != null) {
			ordreList = driftkontraktController.getKjoretoyordreIKontrakt(driftkontrakt.getId());
		}
		
		tblModelOrdre.setEntities(ordreList);
	}
}
