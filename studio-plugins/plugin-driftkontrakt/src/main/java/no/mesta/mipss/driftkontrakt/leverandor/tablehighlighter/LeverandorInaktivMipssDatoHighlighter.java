package no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter;

import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

public class LeverandorInaktivMipssDatoHighlighter implements HighlightPredicate {

	private final JMipssBeanTable<KontraktLeverandor> table;
	private final MipssBeanTableModel<KontraktLeverandor> model;

	private Date defaultDate;
	
	public LeverandorInaktivMipssDatoHighlighter(
			JMipssBeanTable<KontraktLeverandor> table,
			MipssBeanTableModel<KontraktLeverandor> model) {
		this.table = table;
		this.model = model;
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		try {
			defaultDate = f.parse("01.01.1900 00:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
		
		KontraktLeverandor kl = model.get(table.convertRowIndexToModel(adapter.row));
		if (defaultDate.equals(kl.getTilDato())){
			return false;
		}
		if (kl.getTilDato()==null){
			return false;
		}
		if (kl.getTilDato().before(Clock.now())){
			return true;
		}
		return false;
		
	}

}
