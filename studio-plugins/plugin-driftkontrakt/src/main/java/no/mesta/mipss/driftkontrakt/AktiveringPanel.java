package no.mesta.mipss.driftkontrakt;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.picturepanel.ImageSelection;

import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.border.DropShadowBorder;

public class AktiveringPanel extends JXPanel{

	private JLabel lblUtvalg;
	private JLabel lblKode;
	private JLabel lblGyldigTil;
	private JLabel lblGyldigTilValue;
	private JTextField txtKode;
	
	
	
	private JComboBox cmbUtvalg;
	private QRPanel pnlQr;
	private DefaultComboBoxModel modelUtvalg;
	private JButton btnClipboard;
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setSize(500,500);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
		f.add(new AktiveringPanel());
		f.setVisible(true);
	}
	public AktiveringPanel(){
		initComponents();
		initGui();
	}
	
	private JXHeader getHeader(){
		JXHeader head =new JXHeader(ResourceUtils.getResource("aktivering.header.title"), ResourceUtils.getResource("aktivering.header.description"));
		head.setIcon(IconResources.INFO_ICON);
		return head;
	}

	private void initComponents() {
		lblUtvalg = new JLabel(ResourceUtils.getResource("label.aktivering"));
		pnlQr = new QRPanel();
		pnlQr.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));
		cmbUtvalg = new JComboBox();
		
		lblKode = new JLabel(ResourceUtils.getResource("label.kode"));
		lblGyldigTil = new JLabel(ResourceUtils.getResource("label.gyldigTil"));
		txtKode = new JTextField();
		lblGyldigTilValue = new JLabel();
		modelUtvalg = new DefaultComboBoxModel();
		modelUtvalg.addElement("<Velg periode>");
		modelUtvalg.addElement(new QRPeriode("ca 1 mnd", CalendarUtil.toNearestFirstInMonth(CalendarUtil.getMonthsAgo(-1))));
		modelUtvalg.addElement(new QRPeriode("ca 1/2 år", CalendarUtil.toNearestFirstInMonth(CalendarUtil.getMonthsAgo(-6))));
		modelUtvalg.addElement(new QRPeriode("ca 1 år", CalendarUtil.toNearestFirstInMonth(CalendarUtil.getYearsAgo(-1))));
		cmbUtvalg.setModel(modelUtvalg);
		cmbUtvalg.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				Object selectedItem = modelUtvalg.getSelectedItem();
				update(selectedItem);
			}
		});
		btnClipboard = new JButton(getClipboardAction());
	}
	
	private void update(Object periode){
		if (periode instanceof QRPeriode){
			QRPeriode p = (QRPeriode)periode;
			Date endDate = p.getEndDate();
			pnlQr.setDate(endDate);
			txtKode.setText(pnlQr.getGeneratedCode());
			lblGyldigTilValue.setText(pnlQr.getDateString());
		}else{
			pnlQr.setDate(null);
			txtKode.setText("");
			lblGyldigTilValue.setText("");
		}
	}
	private Action getClipboardAction(){
		return new AbstractAction(ResourceUtils.getResource("button.clipboard"), IconResources.COPY_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pnlQr.getQrkode()!=null){
					ImageSelection.copyImageToClipboard(pnlQr.getQrkode());
				}else{
					JOptionPane.showMessageDialog(AktiveringPanel.this, ResourceUtils.getResource("message.qrmissing"), ResourceUtils.getResource("message.qrmissing.title"), JOptionPane.WARNING_MESSAGE);
				}
			}
		};
	}
	private void initGui() {
		setLayout(new GridBagLayout());
		JPanel pnlKode = new JPanel(new GridBagLayout());
		JPanel pnlDato = new JPanel(new GridBagLayout());
		pnlKode.add(lblKode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0, 0));
		pnlKode.add(txtKode, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0, 0));
		
		pnlDato.add(lblGyldigTil, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,0,5), 0, 0));
		pnlDato.add(lblGyldigTilValue, 	new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,5), 0, 0));
		
		
		add(getHeader(),	new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,5), 0, 0));
		add(lblUtvalg, 		new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5,5,5), 0, 0));
		add(cmbUtvalg, 		new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,5,0), 0, 0));
		add(pnlQr,		 	new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,5,0), 0, 0));
		add(pnlKode, 		new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,0), 0, 0));
		add(pnlDato, 		new GridBagConstraints(0, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,5,0), 0, 0));
		add(btnClipboard,	new GridBagConstraints(0, 5, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
	}	
	
	class QRPeriode{
		private String tekst;
		private Date endDate;
		
		public QRPeriode(String tekst, Date endDate){
			this.tekst = tekst;
			this.endDate = endDate;
		}
		public String toString(){
			return tekst;
		}
		public Date getEndDate(){
			return endDate;
		}
	}
	
}
