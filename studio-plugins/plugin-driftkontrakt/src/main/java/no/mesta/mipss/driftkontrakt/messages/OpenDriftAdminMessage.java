package no.mesta.mipss.driftkontrakt.messages;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Collections;
import java.util.Map;

import no.mesta.mipss.driftkontrakt.DriftkontraktAdminModule;
import no.mesta.mipss.plugin.PluginMessage;
/**
 * Melding for å styre modulen fra treet
 * @author lareid
 *
 */
public class OpenDriftAdminMessage extends PluginMessage {

	/**
	 * Ikke i bruk
	 */
	@Override
	public Component getMessageGUI() {
		return null;
	}
	
	/**
	 * Ikke i bruk
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Map getResult() {
		return Collections.EMPTY_MAP;
	}

	@Override
	public void processMessage() {
        if(isConsumed()) {
            return;
        }
        
        String mode = null;
        if(this instanceof OpenDriftDataMessage) {
            mode = DriftkontraktAdminModule.PANEL_DRIFT_DATA;
        } else if(this instanceof OpenDriftLeverandorMessage) {
            mode = DriftkontraktAdminModule.PANEL_DRIFT_LEVERANDOR;
        } else if(this instanceof OpenDriftKjoretoyMessage) {
            mode = DriftkontraktAdminModule.PANEL_DRIFT_KJORETOY;
        } else {
            throw new IllegalStateException("Unkown message type");
        }
        
        DriftkontraktAdminModule plugin = (DriftkontraktAdminModule) getTargetPlugin();
        plugin.setPanel(mode);
        
        if(this instanceof OpenDriftDataMessage) {
        	plugin.getModuleGUI().setPreferredSize(new Dimension(630,500));
        } else if(this instanceof OpenDriftLeverandorMessage) {
        	plugin.getModuleGUI().setPreferredSize(new Dimension(800,500));
        } else if(this instanceof OpenDriftKjoretoyMessage) {
        	plugin.getModuleGUI().setPreferredSize(new Dimension(660,540));
        }
        
        
        setConsumed(true);
	}
}
