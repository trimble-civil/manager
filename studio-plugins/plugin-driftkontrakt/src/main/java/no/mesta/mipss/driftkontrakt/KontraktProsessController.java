package no.mesta.mipss.driftkontrakt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.mesta.driftkontrakt.bean.MaintenanceContract;
import no.mesta.driftkontrakt.bean.ProsessHelper;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.UserUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.mipssfield.KontraktProsess;
import no.mesta.mipss.persistence.mipssfield.Prosess;
import no.mesta.mipss.persistence.mipssfield.Prosessett;

public class KontraktProsessController {
	private final DriftkontraktController controller;
	private final MaintenanceContract contractBean;
	
	public KontraktProsessController(DriftkontraktController controller){
		this.controller = controller;
		contractBean = BeanUtil.lookup(MaintenanceContract.BEAN_NAME, MaintenanceContract.class);
	}
	
	
	public Driftkontrakt getCurrentDriftkontrakt() {
		return controller.getCurrentDriftkontrakt();
	}
	
	public List<Prosessett> getProsessettList(){
		List<Prosessett> alleProsessett = contractBean.getAlleProsessett();
		
		//Kun admin har tilgang til alle prosessett. 
		if (!UserUtils.isAdmin(controller.getDriftkontraktAdminModule().getLoader().getLoggedOnUser(true))){
			Long prosessettId = getCurrentDriftkontrakt().getProsessettId();
			if (prosessettId==null){
				alleProsessett.clear();
			}
			List<Prosessett> kunForKontrakt = new ArrayList<Prosessett>();
			for (Prosessett prosessett:alleProsessett){
				if (prosessett.getId().equals(prosessettId))
					kunForKontrakt.add(prosessett);
			}
			alleProsessett = kunForKontrakt;
		}
		
		
		for (Prosessett p:alleProsessett){
			p.setProsessList(new ProsessHelper().createTree(p.getProsessList()));
			sort(p.getProsessList());
		}
		return alleProsessett;
	}
	
	private void sort(List<Prosess> prosesser){
		Collections.sort(prosesser, new Comparator<Prosess>(){
			@Override
			public int compare(Prosess o1, Prosess o2) {
				return o1.getProsessKode().compareTo(o2.getProsessKode());
			}
			
		});
		for (Prosess p:prosesser){
			sort(p.getProsesser());
		}
	}
	public List<KontraktProsess> getKontraktProsess(Long kontraktId){
		return contractBean.getKontraktProsessForKontrakt(kontraktId);
	}
	public DriftkontraktController getDriftkontraktController(){
		return controller;
	}
	
	public KontraktProsess createKontraktProsess(Prosess p){
		KontraktProsess kp = new KontraktProsess();
		kp.setDriftkontrakt(controller.getCurrentDriftkontrakt());
		kp.setProsess(p);
		kp.setGyldigForArbeid(p.getGyldigForArbeid());
		kp.setGyldigForInspeksjon(p.getGyldigForInspeksjon());
		kp.setOpprettetAv(controller. getDriftkontraktAdminModule().getLoader().getLoggedOnUserSign());
		kp.setOpprettetDato(Clock.now());
		return kp;
	}
	
	public boolean isProsessIBrukPaaValgtKontrakt(Long prosessId){
		return contractBean.getProsessIBrukPaKontrakt(getCurrentDriftkontrakt().getId(), prosessId).booleanValue();
	}
	public boolean isProsessInFavorittListe(Long prosessId){
		return contractBean.getProsessErDelAvFavorittListe(getCurrentDriftkontrakt().getId(), prosessId).booleanValue();
	}
}
