package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.datafangsutstyr.DfuKontrakt;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

public class DriftkontraktKjoretoyPanel extends JPanel {
	private JTabbedPane tbdPane = new JTabbedPane();
	private DfuKontraktListPanel dfuKontraktListPanel;
	private KjoretoyListPanel kjoretoyListPanel;
	private KjoretoyordreListPanel kjoretoyordreListPanel;
	private KjoretoygrupperingPanel kjoretoygrupperingPanel;
	
	private DriftkontraktController driftkontraktController;
	
	public DriftkontraktKjoretoyPanel(DriftkontraktController driftkontraktController) {
		this.driftkontraktController = driftkontraktController;
		dfuKontraktListPanel = new DfuKontraktListPanel(driftkontraktController);
		kjoretoyListPanel = new KjoretoyListPanel(driftkontraktController);
		kjoretoyordreListPanel = new KjoretoyordreListPanel(driftkontraktController, kjoretoyListPanel.getReloadAction()); 
		KjoretoygruppeController kjoretoygruppeController = new KjoretoygruppeController(driftkontraktController);
		kjoretoygrupperingPanel = new KjoretoygrupperingPanel(kjoretoygruppeController);
		initGui();
	}
	
	private void initGui() {
		tbdPane.addTab(ResourceUtils.getResource("tab.kjoretoy"), IconResources.BIL_SMALL_ICON, kjoretoyListPanel);
		tbdPane.addTab(ResourceUtils.getResource("tab.dfuKontrakt"), IconResources.PDA_ICON, dfuKontraktListPanel);
		tbdPane.addTab(ResourceUtils.getResource("tab.kjoretoyordre"), IconResources.EDIT_ICON, kjoretoyordreListPanel);
		tbdPane.addTab(ResourceUtils.getResource("tab.kjoretoygruppering"), IconResources.BIL_SMALL_GRUPPE_ICON, kjoretoygrupperingPanel);
		
		this.setLayout(new GridBagLayout());
		this.add(tbdPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 0, 0, 0), 0, 0));
	}
}
