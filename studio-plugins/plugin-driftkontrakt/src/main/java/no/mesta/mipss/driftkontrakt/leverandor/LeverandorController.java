package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;

import no.mesta.driftkontrakt.bean.LevStedQueryFilter;
import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.core.SaveStatusMonitor;
import no.mesta.mipss.core.SaveStatusRegistry;
import no.mesta.mipss.driftkontrakt.DriftkontraktAdminModule;
import no.mesta.mipss.driftkontrakt.DriftkontraktController;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.driftslogg.DriftsloggService;
import no.mesta.mipss.persistence.MipssObservableList;
import no.mesta.mipss.persistence.driftslogg.AgrArtikkelV;
import no.mesta.mipss.persistence.driftslogg.Levkontrakt;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.LeggTilRelasjonDialog;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;
import no.mesta.mipss.ui.table.AgrArtikkelSelectableObject;
import no.mesta.mipss.ui.table.LevProdtypeRodeTableObject;
import no.mesta.mipss.ui.table.ProdtypeSelectableObject;
import no.mesta.mipss.ui.table.RodeSelectableObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;
import org.jdesktop.swingx.JXTable;
@SuppressWarnings("serial")
public class LeverandorController implements ObservableListListener {
	private PropertyChangeSupport props = new PropertyChangeSupport(this);
	private DriftkontraktController driftkontraktController;
	private KontraktLeverandor currentLeverandor;
	private List<LevProdtypeRodeTableObject> levProdtypeRodeTableObjectList;
	private SaveStatusMonitor<KontraktLeverandor> leverandorSaveMonitor;
	private final DriftkontraktAdminModule driftkontraktAdminModule;
	private DriftsloggService driftslogg;
	
	public LeverandorController(SaveStatusRegistry saveStatusRegistry, DriftkontraktController driftkontraktController, DriftkontraktAdminModule driftkontraktAdminModule) {
		driftslogg = BeanUtil.lookup(DriftsloggService.BEAN_NAME, DriftsloggService.class);
		this.driftkontraktController = driftkontraktController;
		this.driftkontraktAdminModule = driftkontraktAdminModule;
		leverandorSaveMonitor = new SaveStatusMonitor<KontraktLeverandor>(KontraktLeverandor.class, this, "currentLeverandor", saveStatusRegistry.getBindingGroup());
		saveStatusRegistry.addMonitor(leverandorSaveMonitor);
	}
	
	public KontraktLeverandor getCurrentLeverandor() {
		return currentLeverandor;
	}

	public String getLoggedOnUserSign(){
		return driftkontraktAdminModule.getLoader().getLoggedOnUserSign();
	}
	public void setCurrentLeverandor(KontraktLeverandor currentLeverandor) {
		KontraktLeverandor oldValue = this.currentLeverandor;
		this.currentLeverandor = currentLeverandor;
		byggLevProdtypeRodeList();
		props.firePropertyChange("currentLeverandor", oldValue, currentLeverandor);

		unobserveLevProdtypeRodeList(oldValue);
		observeLevProdtypeRodeList(currentLeverandor);
	}

	public void registerChangableField(String... fields) {
		for(String s : fields) {
			leverandorSaveMonitor.registerField(s);
		}
	}
	
	public DriftkontraktController getDriftkontraktController() {
		return driftkontraktController;
	}
	
    public List<Rodetype> getRodetypeForKontrakt() {
    	if(driftkontraktController.getCurrentDriftkontrakt() != null) {
    		return driftkontraktController.getRodetypeList(driftkontraktController.getCurrentDriftkontrakt().getId());
    	} else {
    		return new ArrayList<Rodetype>();
    	}
    }
	
    public List<RodeSelectableObject> getRodeList(Long typeId) {
    	if(driftkontraktController.getCurrentDriftkontrakt() != null) {
	    	List<Rode> rodeList = driftkontraktController.getRodeList(driftkontraktController.getCurrentDriftkontrakt().getId(), typeId);
	    	
	    	List<RodeSelectableObject> rodeSelectableObjectList = new ArrayList<RodeSelectableObject>();
	    	for(Rode rode : rodeList) {
	    		rodeSelectableObjectList.add(new RodeSelectableObject(rode));
	    	}
	    	
	    	return rodeSelectableObjectList;
    	} else {
    		return new ArrayList<RodeSelectableObject>();
    	}
    }
    
    public List<ProdtypeSelectableObject> getProdtypeList() {
    	if(driftkontraktController.getCurrentDriftkontrakt() != null) {
	    	List<Prodtype> prodtypeList = driftkontraktController.getProdtypeList();
	    	
	    	List<ProdtypeSelectableObject> prodtypeSelectableObjectList = new ArrayList<ProdtypeSelectableObject>();
	    	for(Prodtype prodtype : prodtypeList) {
	    		prodtypeSelectableObjectList.add(new ProdtypeSelectableObject(prodtype));
	    	}
	    	
	    	return prodtypeSelectableObjectList;
    	} else {
    		return new ArrayList<ProdtypeSelectableObject>();
    	}
    }
    
	private LeverandorController getInstance() {
		return this;
	}
	
	public List<LevProdtypeRodeTableObject> getLevProdtypeRodeTableObjectList() {
		return levProdtypeRodeTableObjectList;
	}
	
	public void setLevProdtypeRodeTableObjectList(List<LevProdtypeRodeTableObject> levProdtypeRodeTableObjectList) {
		Object oldValue = this.levProdtypeRodeTableObjectList;
		this.levProdtypeRodeTableObjectList = levProdtypeRodeTableObjectList;
		props.firePropertyChange("levProdtypeRodeTableObjectList", oldValue, levProdtypeRodeTableObjectList);
	}
	
	public Action getLeggTilRodeAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.leggTil")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				KontraktLeverandor kontraktLeverandor = getCurrentLeverandor();
				if(kontraktLeverandor != null) {
					LeggTilLevProsessRodePanel levStedPanel = new LeggTilLevProsessRodePanel(getInstance());
					LeggTilRelasjonDialog<List<LevProdtypeRode>> relDialog = new LeggTilRelasjonDialog<List<LevProdtypeRode>>(driftkontraktController.getDriftkontraktAdminModule().getLoader().getApplicationFrame(), levStedPanel);
					List<LevProdtypeRode> levProsessRodeList = relDialog.showDialog();
					if(levProsessRodeList != null) {
						if(kontraktLeverandor.getLeverandor().getLevProdtypeRodeList() == null) {
							kontraktLeverandor.getLeverandor().setLevProdtypeRodeList(levProsessRodeList);
						} else {
							kontraktLeverandor.getLeverandor().getLevProdtypeRodeList().addAll(levProsessRodeList);
						}
					}
				}
			}
    	};
	}
	
	public Action getRodetypeAction(final MipssBeanTableModel<RodeSelectableObject> model, final MipssComboBoxModel<Rodetype> cbxModelRodetypeModel) {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Rodetype rodetype = cbxModelRodetypeModel.getSelectedItem();
				if(rodetype != null) {
					model.setBeanInterface(LeverandorController.class);
			    	model.setBeanInstance(getInstance());
			    	model.setBeanMethod("getRodeList");
			    	model.setBeanMethodArguments(new Class<?>[] {Long.class});
			    	model.setBeanMethodArgValues(new Object[] {rodetype.getId()});
			    	model.loadData();
				}
			}
		};
	}
	
	public Action getVelgAlleRoderAction(final JCheckBox chkVelgAlleRoder, final MipssBeanTableModel<RodeSelectableObject> model) {
		return new AbstractAction(ResourceUtils.getResource("button.alleRoder")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<RodeSelectableObject> rodeList = model.getEntities();
				if(rodeList != null) {
					for(RodeSelectableObject rode : rodeList) {
						rode.setValgt(Boolean.valueOf(chkVelgAlleRoder.isSelected()));
					}
					
					model.fireTableModelEvent(0, model.getRowCount()-1, TableModelEvent.UPDATE);
				}
			}
		};
	}
	
	public Action getVelgAlleProdtyperAction(final JCheckBox chkVelgAlleProdtyper, final MipssBeanTableModel<ProdtypeSelectableObject> model) {
		return new AbstractAction(ResourceUtils.getResource("button.alleProsesser")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<ProdtypeSelectableObject> prodtypeList = model.getEntities();
				if(prodtypeList != null) {
					for(ProdtypeSelectableObject prodtype : prodtypeList) {
						prodtype.setValgt(Boolean.valueOf(chkVelgAlleProdtyper.isSelected()));
					}
					
					model.fireTableModelEvent(0, model.getRowCount()-1, TableModelEvent.UPDATE);
				}
			}
		};
	}
	
	public Action getVisAdresserForLeverandorAction() {
    	return new AbstractAction(ResourceUtils.getResource("button.visAdresser")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				KontraktLeverandor kontraktLeverandor = getCurrentLeverandor();
				if(kontraktLeverandor != null) {
					LevStedQueryFilter levStedQueryFilter = new LevStedQueryFilter();
					levStedQueryFilter.setLevNr(kontraktLeverandor.getLeverandorNr());
					List<LevStedView> levStedViewList = driftkontraktController.getLevStedViewList(levStedQueryFilter);
					new LeverandorAdresserDialog(driftkontraktController.getDriftkontraktAdminModule().getLoader().getApplicationFrame(), levStedViewList);
				}
			}
    	};
	}
	
    public Action getSlettKontaktAction(final MipssBeanTableModel<LevProdtypeRodeTableObject> leverandorModel, final JXTable table) {
    	return new AbstractAction(ResourceUtils.getResource("button.slett")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				KontraktLeverandor kontraktLeverandor = getCurrentLeverandor();
				if(kontraktLeverandor != null) {
					List<LevProdtypeRode> toBeRemoved = new ArrayList<LevProdtypeRode>();
					int[] selectedIndexes = table.getSelectedRows();
					for(int i=0; i<selectedIndexes.length; i++) {
						LevProdtypeRodeTableObject levProdtypeRodeTableObject = leverandorModel.get(table.convertRowIndexToModel(selectedIndexes[i]));
						for(Iterator<LevProdtypeRode> it = kontraktLeverandor.getLeverandor().getLevProdtypeRodeList().iterator(); it.hasNext();) {
							LevProdtypeRode levProdtypeRode = it.next();
							if(levProdtypeRode.getRodeId().equals(levProdtypeRodeTableObject.getRode().getId())
									&& levProdtypeRodeTableObject.getProdtypeList().contains(levProdtypeRode.getProdtype())
									&& levProdtypeRodeTableObject.getFraDato().equals(levProdtypeRode.getFraDato())
									&& levProdtypeRodeTableObject.getTilDato().equals(levProdtypeRode.getTilDato())) {
								toBeRemoved.add(levProdtypeRode);
							}
						}
					}
					
					if(toBeRemoved.size() > 0) {
						kontraktLeverandor.getLeverandor().getLevProdtypeRodeList().removeAll(toBeRemoved);
						kontraktLeverandor.getLeverandor().getSlettedeLevProdtypeRodeList().addAll(toBeRemoved);
						byggLevProdtypeRodeList();
					}
				}
			}
    	};
    }
	
    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        props.addPropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void addPropertyChangeListener(String propName, 
                                          PropertyChangeListener l) {
        props.addPropertyChangeListener(propName, l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        props.removePropertyChangeListener(l);
    }

    /**
     * Delegate
     * 
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
     * @param l
     */
    public void removePropertyChangeListener(String propName, 
                                             PropertyChangeListener l) {
        props.removePropertyChangeListener(propName, l);
    }
    
    private void byggLevProdtypeRodeList() {
    	List<LevProdtypeRodeTableObject> levProdtypeRodeTableObjectList = new MipssObservableList<LevProdtypeRodeTableObject>();
    	KontraktLeverandor kontraktLeverandor = getCurrentLeverandor();
    	if(kontraktLeverandor != null) {
    		hentLevProdtypeRode(kontraktLeverandor);
    		if(kontraktLeverandor.getLeverandor().getLevProdtypeRodeList() != null) {
	    		class Key {
	    			String leverandorNr;
	    			Long rodeId;
	    			Timestamp fraDato;
	    			Timestamp tilDato;
	    			
	    			public Key(String leverandorNr, Long rodeId, Timestamp fraDato, Timestamp tilDato) {
	    				this.leverandorNr = leverandorNr;
	    				this.rodeId = rodeId;
	    				this.fraDato = fraDato;
	    				this.tilDato = tilDato;
	    			}
	    			
	    			@Override
	    		    public int hashCode() {
	    		        return new HashCodeBuilder().append(leverandorNr).append(rodeId).append(fraDato).append(tilDato).toHashCode();
	    		    }
	    			
	    			public boolean equals(Object o) {
	    		        if(o == null) {return false;}
	    		        if(o == this) {return true;}
	    		        if(!(o instanceof Key)) {return false;}
	    		        Key other = (Key) o;
	    		        return new EqualsBuilder().append(leverandorNr, other.leverandorNr).append(rodeId, other.rodeId).append(fraDato, other.fraDato).append(tilDato, other.tilDato).isEquals();
	    		    }
	    		}
	    		HashMap<Key, LevProdtypeRodeTableObject> hashMap = new HashMap<Key, LevProdtypeRodeTableObject>();
	    		for(LevProdtypeRode levProdtypeRode : kontraktLeverandor.getLeverandor().getLevProdtypeRodeList()) {
	    			if(levProdtypeRode.getRode().getKontraktId().equals(getDriftkontraktController().getCurrentDriftkontrakt().getId())) {
		    			Key key = new Key(levProdtypeRode.getLeverandorNr(), levProdtypeRode.getRodeId(), levProdtypeRode.getFraDato(), levProdtypeRode.getTilDato());
		    			if(hashMap.containsKey(key)) {
		    				hashMap.get(key).getProdtypeList().add(levProdtypeRode.getProdtype());
		    			} else {
		    				LevProdtypeRodeTableObject levProdtypeRodeTableObject = new LevProdtypeRodeTableObject();
		    				levProdtypeRodeTableObject.setLeverandor(levProdtypeRode.getLeverandor());
		    				levProdtypeRodeTableObject.setRode(levProdtypeRode.getRode());
		    				levProdtypeRodeTableObject.setFraDato(levProdtypeRode.getFraDato());
		    				levProdtypeRodeTableObject.setTilDato(levProdtypeRode.getTilDato());
		    				levProdtypeRodeTableObject.getProdtypeList().add(levProdtypeRode.getProdtype());
		    				hashMap.put(key, levProdtypeRodeTableObject);
		    			}
	    			}
	    		}
	    		
	    		levProdtypeRodeTableObjectList.addAll(hashMap.values());
    		}
    	}
    	
    	setLevProdtypeRodeTableObjectList(levProdtypeRodeTableObjectList);
    }

    private void hentLevProdtypeRode(KontraktLeverandor kontraktLeverandor) {
    	if(kontraktLeverandor.getLeverandor().getSlettedeLevProdtypeRodeList().size() == 0) {
    		//Følgende er ekstremt stygt, men andre metoder er like stygge...
    		try {
    			kontraktLeverandor.getLeverandor().getLevProdtypeRodeList().size();
    		} catch (Exception e) {
		    	List<LevProdtypeRode> list = driftkontraktController.getLevProdtypeRodeListForLeverandorAndKontrakt(
						kontraktLeverandor.getLeverandorNr(), 
						kontraktLeverandor.getKontraktId());
		    	
		    	kontraktLeverandor.getLeverandor().setLevProdtypeRodeList(list);
			}
    	}
    }
    
    @SuppressWarnings("unchecked")
    private void observeLevProdtypeRodeList(KontraktLeverandor kontraktLeverandor) {
    	if(kontraktLeverandor != null && kontraktLeverandor.getLeverandor().getLevProdtypeRodeList() != null) {
    		if(!(kontraktLeverandor.getLeverandor().getLevProdtypeRodeList() instanceof ObservableList)) {
    			MipssObservableList<LevProdtypeRode> observableList = new MipssObservableList<LevProdtypeRode>(kontraktLeverandor.getLeverandor().getLevProdtypeRodeList());
    			observableList.addObservableListListener(this);
    			kontraktLeverandor.getLeverandor().setLevProdtypeRodeList(observableList);
    		} else {
    			((ObservableList)kontraktLeverandor.getLeverandor().getLevProdtypeRodeList()).addObservableListListener(this);
    		}
    	}
    }
    
    @SuppressWarnings("unchecked")
    private void unobserveLevProdtypeRodeList(KontraktLeverandor kontraktLeverandor) {
    	if(kontraktLeverandor != null && kontraktLeverandor.getLeverandor().getLevProdtypeRodeList() != null) {
    		if(kontraktLeverandor.getLeverandor().getLevProdtypeRodeList() instanceof ObservableList) {
    			((ObservableList)kontraktLeverandor.getLeverandor().getLevProdtypeRodeList()).removeObservableListListener(this);
    		}
    	}
    }
    
	@Override
	public void listElementPropertyChanged(ObservableList arg0, int arg1) {
		byggLevProdtypeRodeList();
	}

	@Override
	public void listElementReplaced(ObservableList arg0, int arg1, Object arg2) {
		byggLevProdtypeRodeList();
	}

	@Override
	public void listElementsAdded(ObservableList arg0, int arg1, int arg2) {
		byggLevProdtypeRodeList();
	}

	@Override
	public void listElementsRemoved(ObservableList arg0, int arg1, List arg2) {
		byggLevProdtypeRodeList();
	}

	public Action leggTilLevkontrakt() {
		return new AbstractAction(ResourceUtils.getResource("button.leggTil"), IconResources.ADD_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				LeggTilAgressoKontraktPanel agrPanel = new LeggTilAgressoKontraktPanel(getInstance());
				LeggTilRelasjonDialog<List<Levkontrakt>> relDialog = new LeggTilRelasjonDialog<List<Levkontrakt>>(driftkontraktController.getDriftkontraktAdminModule().getLoader().getApplicationFrame(), agrPanel);
				List<Levkontrakt> agrList = relDialog.showDialog();
//				agrPanel.dispose();
				if(agrList != null) {
					KontraktLeverandor curLev = getCurrentLeverandor();
					for (Levkontrakt l:agrList){
						curLev.getLevkontraktList().add(l);
					}
				}

			}
		};
	}

	public Action getSlettLevkontrakt(final MipssBeanTableModel<Levkontrakt> mdlKontrakt, final JMipssBeanTable<Levkontrakt> tblKontrakt) {
		return new AbstractAction(ResourceUtils.getResource("button.slett"), IconResources.REMOVE_ITEM_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Levkontrakt> toBeRemoved = new ArrayList<Levkontrakt>();
				int[] selectedIndexes = tblKontrakt.getSelectedRows();
				List<String> levkontraktMedData = new ArrayList<String>();
				for(int i=0; i<selectedIndexes.length; i++) {
					Levkontrakt levkontrakt = mdlKontrakt.get(tblKontrakt.convertRowIndexToModel(selectedIndexes[i]));
					boolean dataFinnes = driftslogg.sjekkRegistrering(levkontrakt);
					if (dataFinnes){
						levkontraktMedData.add(levkontrakt.getLevkontraktIdent());
					}else{
						toBeRemoved.add(levkontrakt);
					}
				}
				
				if(toBeRemoved.size() > 0) {
					getCurrentLeverandor().getLevkontraktList().removeAll(toBeRemoved);
				}
				if (levkontraktMedData.size()!=0){
					String join = StringUtils.join(levkontraktMedData, ", ");
					String msg = ResourceUtils.getResource("message.levkontraktKanIkkeSlettes", join);
					JOptionPane.showMessageDialog(getDriftkontraktController().getDriftkontraktAdminModule().getModuleGUI(), msg, ResourceUtils.getResource("message.levkontraktKanIkkeSlettes.title"), JOptionPane.WARNING_MESSAGE);
				}
			}
		};
	}

	public Action administrerArtikkelProdtype(final MipssBeanTableModel<Levkontrakt> mdlKontrakt, final JMipssBeanTable<Levkontrakt> tblKontrakt) {
		return new AbstractAction(ResourceUtils.getResource("button.artikler"), IconResources.HISTORY_ICON) {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!leverandorSaveMonitor.isChanged()) {
					Levkontrakt levkontrakt = mdlKontrakt.get(tblKontrakt.convertRowIndexToModel(tblKontrakt.getSelectedRow()));
					new AdministrerArtikkelProdtypeDialog(driftkontraktController.getDriftkontraktAdminModule().getLoader().getApplicationFrame(), levkontrakt);
				} else {
					JOptionPane.showMessageDialog(
							getDriftkontraktController().getDriftkontraktAdminModule().getModuleGUI(),
							ResourceUtils.getResource("message.manageArticlesNotSaved.message"),
							ResourceUtils.getResource("message.manageArticlesNotSaved.title"),
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		};
	}

	public List<AgrArtikkelSelectableObject> getAgrArtikkelForKontrakt() {
		List<AgrArtikkelV> sokAgrArtikkel = driftslogg.sokAgrLevkontrakt(driftkontraktController.getCurrentDriftkontrakt(), currentLeverandor.getLeverandor());
		List<AgrArtikkelSelectableObject> list = new ArrayList<AgrArtikkelSelectableObject>();
		for (AgrArtikkelV a:sokAgrArtikkel){
			list.add(new AgrArtikkelSelectableObject(a));
		}
		return list;
	}
}
