package no.mesta.mipss.driftkontrakt.leverandor.tablehighlighter;

import java.awt.Color;

import no.mesta.mipss.persistence.kontrakt.ArtikkelProdtype;
import no.mesta.mipss.persistence.kontrakt.KontraktLeverandor;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.TableHelper;

import org.jdesktop.swingx.decorator.ColorHighlighter;

public class HighlightFactory {
	private static Color warningCellBackground = new Color(255, 255, 200);
	
	public static ColorHighlighter getLeverandorUtenKjoretoyHighlighter(JMipssBeanTable<KontraktLeverandor> table, MipssBeanTableModel<KontraktLeverandor> model){
		return new ColorHighlighter(new LeverandorUtenKjoretoyHighlighter(table, model),	
				warningCellBackground,
				TableHelper.getCellForeground(),
				TableHelper.getSelectedBackground(),
				TableHelper.getSelectedForeground());
	}
	
	public static ColorHighlighter getLeverandorInaktivHighlighter(JMipssBeanTable<KontraktLeverandor> table, MipssBeanTableModel<KontraktLeverandor> model){
		return new ColorHighlighter(new LeverandorInaktivHighlighter(table, model),	
				TableHelper.getCellBackground(),
				new Color(255, 0, 0),
				TableHelper.getSelectedBackground(),
				new Color(255, 0, 0));
	}
	
	public static ColorHighlighter getLeverandorMipssInaktivHighlighter(JMipssBeanTable<KontraktLeverandor> table, MipssBeanTableModel<KontraktLeverandor> model){
		return new ColorHighlighter(new LeverandorInaktivMipssDatoHighlighter(table, model),	
				TableHelper.getCellBackground(),
				Color.GRAY,
				TableHelper.getSelectedBackground(),
				Color.GRAY);
	}

	public static ColorHighlighter getArticleExpiredHighlighter(JMipssBeanTable<ArtikkelProdtype> table, MipssBeanTableModel<ArtikkelProdtype> model){
		return new ColorHighlighter(new ArticleExpiredHighlighter(table, model),
				Color.LIGHT_GRAY,
				Color.GRAY,
				Color.LIGHT_GRAY,
				Color.GRAY);
	}

}