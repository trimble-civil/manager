package no.mesta.mipss.driftkontrakt;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.KontraktStab;
import no.mesta.mipss.resources.images.IconResources;

@SuppressWarnings("serial")
public class RedigerElrappBrukerFraKontraktStabDialog extends JDialog{
	private final KontraktStab stab;
//	private final KontraktStab orginalStab;
	
	private boolean cancelled=true;
	private JTextField elrappField;
	public RedigerElrappBrukerFraKontraktStabDialog(JFrame owner, KontraktStab stab){
		super(owner, true);
		this.stab = stab;
//		this.stab = new KontraktStab();
//		this.stab.merge(stab);
//		this.orginalStab=stab;
//		this.stab.setFunksjonNavn(stab.getFunksjonNavn());
//		this.stab.setKontraktId(stab.getKontraktId());
//		this.stab.setSignatur(stab.getSignatur());
		initGui();
	}
	private void initGui() {
		setLayout(new BorderLayout());
		setTitle(ResourceUtils.getResource("dialogTitle.leggTilElrappBruker"));
		JLabel elrappLabel = new JLabel("Elrapp brukerId :");
		elrappField = new JTextField();
		
		elrappField.setText(stab.getElrappBruker());
		
		JButton okButton = new JButton(getOkAction());
		JButton cancelButton = new JButton(getCancelAction());
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		buttonPanel.add(okButton, new GridBagConstraints(0,0, 1,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));
		buttonPanel.add(cancelButton, new GridBagConstraints(1,0, 1,1, 0.0,0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,5,0,0), 0,0));
		
		JPanel mainPanel = new JPanel(new GridBagLayout());
		mainPanel.add(elrappLabel, new GridBagConstraints(0,0, 1,1, 0.0,1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(15,5,5,5), 0,0));
		mainPanel.add(elrappField , new GridBagConstraints(1,0, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
		mainPanel.add(buttonPanel, new GridBagConstraints(0,1, 2,1, 1.0,0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(15,5,5,5), 0,0));
		
		
		add(mainPanel);
		setSize(250, 120);
	}
	
	private AbstractAction getOkAction(){
		return new AbstractAction("Ok", IconResources.OK_ICON){
			@Override
			public void actionPerformed(ActionEvent e){
				cancelled=false;
				dispose();
			}
		};
	}
	
	private AbstractAction getCancelAction(){
		return new AbstractAction("Avbryt", IconResources.CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e){
				dispose();
			}
		};
	}
	
	public boolean isCancelled(){
		return cancelled;
	}
	
	public String getElrappBruker(){
		return elrappField.getText();
	}
}
