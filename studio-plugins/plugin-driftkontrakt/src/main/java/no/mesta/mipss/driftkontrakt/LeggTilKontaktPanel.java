package no.mesta.mipss.driftkontrakt;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.core.LongStringConverter;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.kontrakt.KontraktKontakttype;
import no.mesta.mipss.persistence.kontrakt.kontakt.Kontraktkontakttype;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.DocumentFilter;
import no.mesta.mipss.ui.MipssListCellRenderer;
import no.mesta.mipss.ui.combobox.MipssComboBoxModel;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

public class LeggTilKontaktPanel extends AbstractRelasjonPanel<KontraktKontakttype> implements PropertyChangeListener {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlKontakttype = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JComboBox cbxKontaktType = new JComboBox();
    private JLabel lblKontaktType = new JLabel();
    private JPanel pnlKontaktinformasjon = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblKontaktNavn = new JLabel();
    private JTextField txtKontaktNavn = new JTextField();
    private JLabel lblTelefon1 = new JLabel();
    private JFormattedTextField txtTelefon1 = new JFormattedTextField();
    private JLabel lblTelefon2 = new JLabel();
    private JLabel lblEpost = new JLabel();
    private JFormattedTextField txtTelefon2 = new JFormattedTextField();
    private JTextField txtEpost = new JTextField();
    private JLabel lblFaks = new JLabel();
    private JLabel lblBeskrivelse = new JLabel();
    private JFormattedTextField txtFaks = new JFormattedTextField();
    private JTextField txtBeskrivelse = new JTextField();
	
	private DriftkontraktController driftkontraktController;
	private BindingGroup bindingGroup;
	private KontraktKontakttype kontraktKontaktype;
	private KontraktKontakttype originalKontraktKontaktype;
	private List<KontraktKontakttype> ekisterendeList;
	private boolean endreKontakt;

	public LeggTilKontaktPanel(DriftkontraktController driftkontraktController, List<KontraktKontakttype> ekisterendeList) {
		this.driftkontraktController = driftkontraktController;
		this.ekisterendeList = ekisterendeList;
		kontraktKontaktype = new KontraktKontakttype();
		kontraktKontaktype.addPropertyChangeListener(this);
		
		kontraktKontaktype.setDriftkontrakt(driftkontraktController.getCurrentDriftkontrakt());
		kontraktKontaktype.getOwnedMipssEntity().setOpprettetDato(Clock.now());
		
		endreKontakt = false;
		
		init();
	}
	
	public LeggTilKontaktPanel(DriftkontraktController driftkontraktController, List<KontraktKontakttype> ekisterendeList, KontraktKontakttype kontraktKontaktype) {
		this.driftkontraktController = driftkontraktController;
		this.ekisterendeList = ekisterendeList;
		this.kontraktKontaktype = new KontraktKontakttype();
		this.kontraktKontaktype.setDriftkontrakt(kontraktKontaktype.getDriftkontrakt());
		this.kontraktKontaktype.setKontraktkontakttype(kontraktKontaktype.getKontraktkontakttype());
		this.kontraktKontaktype.mergeAll(kontraktKontaktype);
		this.kontraktKontaktype.addPropertyChangeListener(this);
		originalKontraktKontaktype = kontraktKontaktype;
		
		endreKontakt = true;
		
		init();
	}
	
	private void init() {
		bindingGroup = new BindingGroup();
		
		initComboBox();
		initGui();
		initTextFields();
		
		bindingGroup.bind();
	}
	
	@SuppressWarnings("unchecked")
	private void initTextFields() {
		LongStringConverter longStringConverter = new LongStringConverter();
		
		Binding tlf1Binding = BindingHelper.createbinding(kontraktKontaktype, "tlfnummer1", txtTelefon1, "text", BindingHelper.READ_WRITE);
		Binding tlf2Binding = BindingHelper.createbinding(kontraktKontaktype, "tlfnummer2", txtTelefon2, "text", BindingHelper.READ_WRITE);
		Binding faksBinding = BindingHelper.createbinding(kontraktKontaktype, "faksnummer", txtFaks, "text", BindingHelper.READ_WRITE);
		
		tlf1Binding.setConverter(longStringConverter);
		tlf2Binding.setConverter(longStringConverter);
		faksBinding.setConverter(longStringConverter);
		
		bindingGroup.addBinding(tlf1Binding);
		bindingGroup.addBinding(tlf2Binding);
		bindingGroup.addBinding(faksBinding);
		
		txtTelefon1.setDocument(new DocumentFilter(8, "0123456789", false));
		txtTelefon2.setDocument(new DocumentFilter(8, "0123456789", false));
		txtFaks.setDocument(new DocumentFilter(8, "0123456789", false));
		
		bindingGroup.addBinding(BindingHelper.createbinding(kontraktKontaktype, "kontaktnavn", txtKontaktNavn, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(kontraktKontaktype, "epostAdresse", txtEpost, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(kontraktKontaktype, "beskrivelse", txtBeskrivelse, "text", BindingHelper.READ_WRITE));
	}
	
	private void initComboBox() {
		Set<String> set = new HashSet<String>();
		for(KontraktKontakttype kontraktKontaktype : ekisterendeList) {
			set.add(kontraktKontaktype.getKontakttypeNavn());
		}
		
		List<Kontraktkontakttype> kontraktkontakttyper = driftkontraktController.getKontraktkontakttypeList();
		
		for(Iterator<Kontraktkontakttype> it = kontraktkontakttyper.iterator(); it.hasNext();) {
			Kontraktkontakttype kontraktkontakttype = it.next();
			if(kontraktkontakttype.getKunEnKontaktFlagg() && set.contains(kontraktkontakttype.getNavn())) {
				it.remove();
			}
		}
		
        MipssComboBoxModel<Kontraktkontakttype> cbxModelKontakttype = new MipssComboBoxModel<Kontraktkontakttype>(kontraktkontakttyper, false);
        cbxKontaktType.setModel(cbxModelKontakttype);
        cbxKontaktType.setRenderer(new MipssListCellRenderer<Kontraktkontakttype>());
        
        if(endreKontakt) {
        	cbxModelKontakttype.setSelectedItem(kontraktKontaktype.getKontraktkontakttype());
        	cbxKontaktType.setEnabled(false);
        	cbxKontaktType.setFocusable(false);
        } else {
            if(kontraktkontakttyper.size() > 0) {
            	kontraktKontaktype.setKontraktkontakttype(kontraktkontakttyper.get(0));
            }
            
            bindingGroup.addBinding(BindingHelper.createbinding(kontraktKontaktype, "kontraktkontakttype", cbxKontaktType, "selectedItem", BindingHelper.READ_WRITE));
        }
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlKontakttype.setLayout(gridBagLayout2);
        pnlKontakttype.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.type")));
        cbxKontaktType.setPreferredSize(new Dimension(120, 21));
        lblKontaktType.setText(ResourceUtils.getResource("label.kontakttype"));
        pnlKontaktinformasjon.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kontaktinformasjon")));
        pnlKontaktinformasjon.setLayout(gridBagLayout3);
        lblKontaktNavn.setText(ResourceUtils.getResource("label.kontaktnavn"));
        txtKontaktNavn.setPreferredSize(new Dimension(150, 21));
        txtKontaktNavn.setSize(new Dimension(150, 21));
        lblTelefon1.setText(ResourceUtils.getResource("label.telefon1"));
        txtTelefon1.setPreferredSize(new Dimension(150, 21));
        lblTelefon2.setText(ResourceUtils.getResource("label.telefon2"));
        lblEpost.setText(ResourceUtils.getResource("label.epost"));
        txtTelefon2.setPreferredSize(new Dimension(150, 21));
        txtEpost.setPreferredSize(new Dimension(150, 21));
        lblFaks.setText(ResourceUtils.getResource("label.faks"));
        lblBeskrivelse.setText(ResourceUtils.getResource("label.merknad"));
        txtFaks.setPreferredSize(new Dimension(150, 21));
        txtBeskrivelse.setPreferredSize(new Dimension(150, 21));
        pnlKontakttype.add(cbxKontaktType, 
                    new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontakttype.add(lblKontaktType, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 0), 0, 0));
        this.add(pnlKontakttype, 
                 new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                                        new Insets(11, 11, 5, 11), 0, 0));
        pnlKontaktinformasjon.add(lblKontaktNavn, 
                    new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtKontaktNavn, 
                    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblTelefon1, 
                    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtTelefon1, 
                    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblTelefon2, 
                    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblEpost, 
                    new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtTelefon2, 
                    new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtEpost, 
                    new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblFaks, 
                    new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(lblBeskrivelse, 
                    new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                           new Insets(0, 5, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtFaks, 
                    new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        pnlKontaktinformasjon.add(txtBeskrivelse, 
                    new GridBagConstraints(1, 5, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, 
                                           new Insets(0, 0, 5, 5), 0, 0));
        this.add(pnlKontaktinformasjon, 
                 new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, 
                                        GridBagConstraints.BOTH, 
                                        new Insets(0, 11, 11, 11), 0, 0));
	}
	
	private void updateComplete() {
		String navn = kontraktKontaktype.getKontaktnavn();
		String kontraktkontakttypeNavn = kontraktKontaktype.getKontakttypeNavn();
		setComplete(Boolean.valueOf(navn != null && navn.length() > 0 && kontraktkontakttypeNavn != null && kontraktkontakttypeNavn.length() > 0));
	}
	
	@Override
	public Dimension getDialogSize() {
		return new Dimension(295, 345);
	}

	@Override
	public KontraktKontakttype getNyRelasjon() {
		if(endreKontakt) {
			originalKontraktKontaktype.merge(kontraktKontaktype);
			return originalKontraktKontaktype;
		} else {
			return kontraktKontaktype;
		}
	}

	@Override
	public String getTitle() {
		return ResourceUtils.getResource("dialogTitle.leggTilKontakt");
	}

	@Override
	public boolean isLegal() {
		return true;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateComplete();
	}
}
