package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.ui.DocumentFilter;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXDatePicker;

public class LeverandorDetaljPanel extends JPanel {
    private GridBagLayout gridBagLayout1 = new GridBagLayout();
    private JPanel pnlMain = new JPanel();
    private GridBagLayout gridBagLayout2 = new GridBagLayout();
    private JPanel pnlLeverandorInfo = new JPanel();
    private GridBagLayout gridBagLayout3 = new GridBagLayout();
    private JLabel lblLeverandorNr = new JLabel();
    private JLabel lblLeverandorNavn = new JLabel();
    private JTextField txtLeverandorNr = new JTextField();
    private JTextField txtLeverandorNavn = new JTextField();
    private JPanel pnlDato = new JPanel();
    private GridBagLayout gridBagLayout4 = new GridBagLayout();
    private JPanel pnlEpost = new JPanel();
    private GridBagLayout gridBagLayout5 = new GridBagLayout();
    private JLabel lblEpost = new JLabel();
    private JLabel lblEpostKopi = new JLabel();
    private JLabel lblSendEpost = new JLabel();
    private JTextField txtEpost = new JTextField();
    private JTextField txtEpostKopi = new JTextField();
    private JCheckBox chkSendEpost = new JCheckBox();
    private JLabel lblFraDato = new JLabel();
    private JTextField txtDatoFra = new JTextField();
    private JLabel lblTilDato = new JLabel();
    private JXDatePicker dtPckrDatoTil;
    private JLabel lblTlfnummer = new JLabel();
    private JFormattedTextField  txtTlfnummer = new JFormattedTextField ();
    
    private JButton btnAdresser = new JButton();
	
	private LeverandorController leverandorController;
    private BindingGroup bindingGroup;
	
	public LeverandorDetaljPanel(LeverandorController leverandorController) {
		this.leverandorController = leverandorController;
		bindingGroup = new BindingGroup();
		
		initDatePickers();
		initGui();
		initTextFields();
		initButtons();
		
		bindingGroup.bind();
	}
	
	private void initTextFields() {
		leverandorController.registerChangableField("epostAdresse", "kopiEpostAdresse", "sendeEpostFlagg", "tilDato", "tlfnummer");
		
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null ? currentLeverandor.leverandorNr : null}", txtLeverandorNr, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null && currentLeverandor.leverandor != null ? currentLeverandor.leverandor.navn : null}", txtLeverandorNavn, "text", BindingHelper.READ));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null ? currentLeverandor.fraDatoString : null}", txtDatoFra, "text", BindingHelper.READ));
		
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", txtEpost, "editable"));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor.epostAdresse}", txtEpost, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", txtEpostKopi, "editable"));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor.kopiEpostAdresse}", txtEpostKopi, "text", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", chkSendEpost, "enabled")); 
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", txtTlfnummer, "editable"));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor.tlfnummer}", txtTlfnummer, "text", BindingHelper.READ_WRITE));
		
		Binding<LeverandorController, Long, JCheckBox, Boolean> b = BindingHelper.createbinding(leverandorController, "${currentLeverandor.sendeEpost}", chkSendEpost, "selected", BindingHelper.READ_WRITE);
		bindingGroup.addBinding(b);
	}
	
	private void initDatePickers() {
		Locale loc = new Locale("no", "NO");
		String[] formats = new String[]{"dd.MM.yyyy"};
		
		dtPckrDatoTil = new JXDatePicker(loc);
		dtPckrDatoTil.setFormats(formats);
		
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor.tilDato}", dtPckrDatoTil, "date", BindingHelper.READ_WRITE));
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", dtPckrDatoTil, "editable", BindingHelper.READ));
	}
	
	private void initButtons() {
		btnAdresser.setAction(leverandorController.getVisAdresserForLeverandorAction());
		bindingGroup.addBinding(BindingHelper.createbinding(leverandorController, "${currentLeverandor != null}", btnAdresser, "enabled", BindingHelper.READ));
	}
	
	private void initGui() {
        this.setLayout(gridBagLayout1);
        pnlMain.setLayout(gridBagLayout2);
        pnlLeverandorInfo.setLayout(gridBagLayout3);
        lblLeverandorNr.setText(ResourceUtils.getResource("label.leverandorNummer"));
        lblLeverandorNavn.setText(ResourceUtils.getResource("label.leverandorNavn"));
        txtLeverandorNr.setPreferredSize(new Dimension(180, 21));
        txtLeverandorNr.setEditable(false);
        txtLeverandorNavn.setPreferredSize(new Dimension(180, 21));
        txtLeverandorNavn.setEditable(false);
        pnlDato.setLayout(gridBagLayout4);
        pnlEpost.setLayout(gridBagLayout5);
        lblEpost.setText(ResourceUtils.getResource("label.epost"));
        lblEpostKopi.setText(ResourceUtils.getResource("label.epostKopi"));
        lblSendEpost.setText(ResourceUtils.getResource("label.sendEpost"));
        txtEpost.setPreferredSize(new Dimension(180, 21));
        txtEpostKopi.setPreferredSize(new Dimension(180, 21));
        lblFraDato.setText(ResourceUtils.getResource("label.fraDato"));
        txtDatoFra.setPreferredSize(new Dimension(120, 21));
        txtDatoFra.setEditable(false);
        lblTilDato.setText(ResourceUtils.getResource("label.tilDato"));
        dtPckrDatoTil.setPreferredSize(new Dimension(120, 21));
        lblTlfnummer.setText(ResourceUtils.getResource("label.tlfnummer"));
        txtTlfnummer.setPreferredSize(new Dimension(120, 21));
        txtTlfnummer.setDocument(new DocumentFilter(20));
        
        pnlLeverandorInfo.add(lblLeverandorNr,   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlLeverandorInfo.add(lblLeverandorNavn, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlLeverandorInfo.add(txtLeverandorNr,   new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnlLeverandorInfo.add(txtLeverandorNavn, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnlLeverandorInfo.add(btnAdresser, 		 new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
        
        pnlDato.add(lblFraDato, 		new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlDato.add(txtDatoFra, 		new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnlDato.add(lblTilDato, 		new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlDato.add(dtPckrDatoTil, 		new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        pnlDato.add(lblTlfnummer, 		new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        pnlDato.add(txtTlfnummer, 		new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
        
        pnlEpost.add(lblEpost, 	   new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0, 0, 5, 0), 0,0));
        pnlEpost.add(lblEpostKopi, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0, 0, 5, 0), 0,0));
        pnlEpost.add(lblSendEpost, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0, 0, 5, 0), 0,0));
        pnlEpost.add(txtEpost, 	   new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0, 5, 5, 0), 0,0));
        pnlEpost.add(txtEpostKopi, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0, 5, 5, 0), 0,0));
        pnlEpost.add(chkSendEpost, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0, 5, 5, 0), 0,0));
        
        pnlMain.add(pnlLeverandorInfo, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        pnlMain.add(pnlEpost,    	   new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 11, 0, 11), 0, 0));
        pnlMain.add(pnlDato, 	       new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        
        this.add(pnlMain, 			   new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0, 0, 0, 0), 0, 0));
	}
}
