package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import no.mesta.mipss.common.MipssDateFormatter;
import no.mesta.mipss.core.CalendarUtil;
import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.persistence.IRenderableMipssEntity;
import no.mesta.mipss.persistence.kjoretoy.KontraktKjoretoyInfo;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyDogn;

public class KjoretoyslisteTableObject implements IRenderableMipssEntity {
	private Boolean nyttKjoretoy;
	private KontraktKjoretoyInfo kontraktKjoretoyInfo;
	private Double tomkjoringKm;
	private Double totalKm;
	private Double tomkjoringProsent;
	
	private final String ordrestatusDelerSendt;	
	
	public KjoretoyslisteTableObject(KontraktKjoretoyInfo kontraktKjoretoyInfo, List<KontraktKjoretoyDogn> produksjonList, String ordrestatusDelerSendt) {
		this.kontraktKjoretoyInfo = kontraktKjoretoyInfo;
		this.ordrestatusDelerSendt = ordrestatusDelerSendt;
		this.nyttKjoretoy = Boolean.FALSE;
		
		initproduksjon(produksjonList);
	}
	
	private void initproduksjon(List<KontraktKjoretoyDogn> produksjonList) {
		if(produksjonList != null) {
			double sumTomkjoring = 0;
			double sumTotalt = 0;

			for(KontraktKjoretoyDogn kontraktKjoretoyDogn : produksjonList) {
				sumTomkjoring += kontraktKjoretoyDogn.getTomkjoringKm() != null ? kontraktKjoretoyDogn.getTomkjoringKm() : 0;
				sumTotalt += kontraktKjoretoyDogn.getTotalKm() != null ? kontraktKjoretoyDogn.getTotalKm() : 0;
			}

			tomkjoringKm = sumTomkjoring;
			totalKm = sumTotalt;
			
			tomkjoringProsent = totalKm != 0 ? ((tomkjoringKm * 100) / totalKm) : (tomkjoringKm != 0 ? 100 : 0);
			tomkjoringProsent = Double.valueOf(Math.round(tomkjoringProsent));
		} else {
			tomkjoringKm = null;
			totalKm = null;
			tomkjoringProsent = null;
		}
	}
	
	public KontraktKjoretoyInfo getKontraktKjoretoyInfo() {
		return kontraktKjoretoyInfo;
	}

	@Override
	public String getTextForGUI() {
		return kontraktKjoretoyInfo.getTextForGUI();
	}
	
	public Long getKjoretoyId() {
		return kontraktKjoretoyInfo.getKjoretoyId();
	}
	
	public Long getAnsvarligFlagg() {
		return kontraktKjoretoyInfo.getAnsvarligFlagg();
	}
	
	public String getRegnr() {
		return kontraktKjoretoyInfo.getRegnr();
	}
	
	public String getMaskinnummer() {
		return kontraktKjoretoyInfo.getMaskinnummer() != null ? kontraktKjoretoyInfo.getMaskinnummer().toString() : "";
	}
	
	public String getEksterntNavn() {
		return kontraktKjoretoyInfo.getEksterntNavn();
	}
	
	public ColorableTableValue getDfuTlfnummer() {
		return new KjoretoyValue(kontraktKjoretoyInfo.getDfuTlfnummer()) {
			@Override
			public boolean isRed() {
				return value == null || value.toString().equals("");
			}

			@Override
			public boolean isYellow() {
				return false;
			}

			@Override
			public String getTooltipText() {
				return isRed() ? ResourceUtils.getResource("tooltip.manglerDfu") : null;
			}
		};
	}
	
	public ColorableTableValue getDfustatus() {
		return new KjoretoyValue(kontraktKjoretoyInfo.getDfustatus()) {
			@Override
			public boolean isRed() {
				return value == null || !value.toString().equals("I drift");
			}

			@Override
			public boolean isYellow() {
				return false;
			}

			@Override
			public String getTooltipText() {
				if(isRed()) {
					if(value != null && value.toString().length() > 0) {
						return ResourceUtils.getResource("tooltip.feilDfuStatus");
					} else {
						return ResourceUtils.getResource("tooltip.manglerDfuStatus");
					}
				} else {
					return null;
				}
			}
		};
	}
	
	public ColorableTableValue getDfuSisteLivtegnDato() {
		return new KjoretoyValue(kontraktKjoretoyInfo.getDfuSisteLivtegnDato()) {
			@Override
			public boolean isRed() {
				return olderThanFourteenDays(kontraktKjoretoyInfo.getDfuSisteLivtegnDato());
			}

			@Override
			public boolean isYellow() {
				return betweenThreeAndFourteenDays(kontraktKjoretoyInfo.getDfuSisteLivtegnDato());
			}

			@Override
			public String getTooltipText() {
				if(isRed()) {
					return ResourceUtils.getResource("tooltip.feilSisteLivstegn");
				} else if (isYellow()) {
					return ResourceUtils.getResource("tooltip.varselSisteLivstegn");
				} else {
					return null;
				}
			}
		};
	}
	
	public ColorableTableValue getDfuSistePosisjonDatoCET() {
		return new KjoretoyValue(kontraktKjoretoyInfo.getDfuSistePosisjonDatoCET()) {
			@Override
			public boolean isRed() {
				return olderThanFourteenDays(kontraktKjoretoyInfo.getDfuSistePosisjonDatoCET());
			}

			@Override
			public boolean isYellow() {
				return betweenThreeAndFourteenDays(kontraktKjoretoyInfo.getDfuSistePosisjonDatoCET());
			}

			@Override
			public String getTooltipText() {
				if(isRed()) {
					return ResourceUtils.getResource("tooltip.feilSistePosisjon");
				} else if (isYellow()) {
					return ResourceUtils.getResource("tooltip.varselSistePosisjon");
				} else {
					return null;
				}
			}
		};
	}
	
	public ColorableTableValue getAktuellKonfigKlartekst() {
		return new KjoretoyValue(kontraktKjoretoyInfo.getAktuellKonfigKlartekst()) {
			@Override
			public boolean isRed() {
				return value == null || value.toString().equals("");
			}

			@Override
			public boolean isYellow() {
				boolean harSprederMedDatafangst = kontraktKjoretoyInfo.getSprMedDatafangstFlagg() != null && kontraktKjoretoyInfo.getSprMedDatafangstFlagg() == 1L;
				boolean harSprederUtenDatafangst = kontraktKjoretoyInfo.getSprUtenDatafangstFlagg() != null && kontraktKjoretoyInfo.getSprUtenDatafangstFlagg() == 1L;
				
				return harSprederMedDatafangst && harSprederUtenDatafangst;
			}

			@Override
			public String getTooltipText() {
				if(isRed()) {
					return ResourceUtils.getResource("tooltip.manglerUtstyr");
				} else if(isYellow()) {
					return ResourceUtils.getResource("tooltip.varselSprederMedOgUtenDatafangst");
				} else {
					return null;
				}
			}
		};
	}
	
	public ColorableTableValue getKlippeinformasjon() {
		Date today = new Date();
		
		
		final boolean harKlippeutstyr = kontraktKjoretoyInfo.getKantklippFlagg() != null && kontraktKjoretoyInfo.getKantklippFlagg() == 1L;
		
		final boolean harKlippeperiode = kontraktKjoretoyInfo.getAktuellKlippeperiodeFraDato() != null;
		
		final boolean utloptKlippeperiode = harKlippeperiode && kontraktKjoretoyInfo.getAktuellKlippeperiodeTilDato() != null && kontraktKjoretoyInfo.getAktuellKlippeperiodeTilDato().before(today);
		
		return new KjoretoyValue(kontraktKjoretoyInfo.getAktuellKlippeperiodeTekst()) {
			
			@Override
			public boolean isRed() {
				return (utloptKlippeperiode && harKlippeutstyr);
			}

			@Override
			public boolean isYellow() {	
				return ((harKlippeutstyr && !harKlippeperiode) || (!harKlippeutstyr && harKlippeperiode && !utloptKlippeperiode));
			}

			@Override
			public String getTooltipText() {
				if(isRed()) {
					return ResourceUtils.getResource("tooltip.varselHarKlippeutstyrUtloptPeriode");
				} else if(isYellow()) {
					
					if (harKlippeutstyr && !harKlippeperiode) {
						return ResourceUtils.getResource("tooltip.varselHarKlippeutstyrIkkePeriode");
					} else if(!harKlippeutstyr && harKlippeperiode) {
						return ResourceUtils.getResource("tooltip.varselHarKlippeperiodeIkkeUtstyr");
					}
					return null;
				} else {
					return null;
				}
			}
		};
	}
	
	public ColorableTableValue getBestillingStatus(){
		StringBuilder sb = new StringBuilder();
		if (kontraktKjoretoyInfo.getBestillingStatus()!=null){
			sb.append(kontraktKjoretoyInfo.getBestillingStatus());
			sb.append("\n(");
			if (kontraktKjoretoyInfo.getBestillingStatusDato()!=null){
				sb.append(MipssDateFormatter.formatDate(kontraktKjoretoyInfo.getBestillingStatusDato(), MipssDateFormatter.DATE_FORMAT));
			}else sb.append("???");
			sb.append(")");
		}
		return new KjoretoyValue(sb.toString()){
			@Override
			public String getTooltipText() {
				return isYellow()?ResourceUtils.getResource("tooltip.underBestilling"):isRed()?ResourceUtils.getResource("tooltip.prodsettKjoretoy"):null;
			}

			@Override
			public boolean isRed() {
				if (ordrestatusDelerSendt.equals(kontraktKjoretoyInfo.getBestillingStatus())){
					if (kontraktKjoretoyInfo.getBestillingDato().before(CalendarUtil.getAgo(Clock.now(), 0, 14, 0))){
						return true;
					}
				}
				return false;
			}

			@Override
			public boolean isYellow() {
				return kontraktKjoretoyInfo.getBestillingStatus()!=null&&!isRed();
			}
		};
	}
	
	public ColorableTableValue getUkjentProduksjon(){
		return new KjoretoyValue(kontraktKjoretoyInfo.getUkjentProduksjon()){
			@Override
			public String getTooltipText() {
				return isYellow()?ResourceUtils.getResource("tooltip.ukjentProduksjon"):null;
			}
			@Override
			public boolean isRed() {
				return false;
			}
			@Override
			public boolean isYellow() {
				return kontraktKjoretoyInfo.getUkjentProduksjon();
			}
		};
	}
	
	public ColorableTableValue getUkjentStrometode(){
		return new KjoretoyValue(kontraktKjoretoyInfo.getUkjentStrometode()){
			@Override
			public String getTooltipText() {
				return isYellow()?ResourceUtils.getResource("tooltip.ukjentStrometode"):null;
			}
			@Override
			public boolean isRed() {
				return false;
			}
			@Override
			public boolean isYellow() {
				return kontraktKjoretoyInfo.getUkjentStrometode();
			}
		};
	}
	
	public ColorableTableValue getDfuSerienummer(){
		return new KjoretoyValue(kontraktKjoretoyInfo.getDfuSerienummer()) {
			@Override
			public boolean isRed() {
				return value == null || value.toString().equals("");
			}

			@Override
			public boolean isYellow() {
				return false;
			}

			@Override
			public String getTooltipText() {
				return isRed() ? ResourceUtils.getResource("tooltip.manglerDfu") : null;
			}
		};
	}
	private boolean isPeriodValid(Date fra, Date til){
		if (fra==null&&til==null)
			return true;
		
		Date today = Clock.now();
		if (fra!=null){
			if (fra.after(today))
				return false;
		}
		if (til!=null)
			if (til.before(today))
				return false;
		
		return true;
	}
	private String getFeilmeldingStroperiode(Date fra, Date til){
		Date today = Clock.now();
		if (fra!=null){
			if (fra.after(today))
				return ResourceUtils.getResource("tooltip.stroperiodeIkkeStartet");
		}
		if (til!=null)
			if (til.before(today))
				return ResourceUtils.getResource("tooltip.stroperiodeUtgatt");
		return "Ukjent feil";
	}
	public ColorableTableValue getAktuellStroperiodeKlartekst() {
		return new KjoretoyValue(kontraktKjoretoyInfo.getAktuellStroperiodeKlartekst()) {
			@Override
			public boolean isRed() {
				boolean harSprederMedDatafangst = kontraktKjoretoyInfo.getSprMedDatafangstFlagg() != null && kontraktKjoretoyInfo.getSprMedDatafangstFlagg() == 1L;
				boolean harSprederUtenDatafangst = kontraktKjoretoyInfo.getSprUtenDatafangstFlagg() != null && kontraktKjoretoyInfo.getSprUtenDatafangstFlagg() == 1L;
				boolean harStroperioder = kontraktKjoretoyInfo.getAktuellStroperiodeKlartekst() != null && kontraktKjoretoyInfo.getAktuellStroperiodeKlartekst().length() > 0;
				return harSprederUtenDatafangst && !harStroperioder && !harSprederMedDatafangst;
			}

			@Override
			public boolean isYellow() {
				
				Date fra = kontraktKjoretoyInfo.getAktuellStroperiodeFraDato();
				Date til = kontraktKjoretoyInfo.getAktuellStroperiodeTilDato();
				boolean stroperiodeOk = isPeriodValid(fra, til);
				
				if (stroperiodeOk){
					boolean harSprederMedDatafangst = kontraktKjoretoyInfo.getSprMedDatafangstFlagg() != null && kontraktKjoretoyInfo.getSprMedDatafangstFlagg() == 1L;
					boolean harStroperioder = kontraktKjoretoyInfo.getAktuellStroperiodeKlartekst() != null && kontraktKjoretoyInfo.getAktuellStroperiodeKlartekst().length() > 0;				
					return harSprederMedDatafangst && harStroperioder;
				}else
					return true;
			}

			@Override
			public String getTooltipText() {
				if(isRed()) {
					return ResourceUtils.getResource("tooltip.manglerStroperiode");
				} else if(isYellow()) {
					Date fra = kontraktKjoretoyInfo.getAktuellStroperiodeFraDato();
					Date til = kontraktKjoretoyInfo.getAktuellStroperiodeTilDato();
					boolean stroperiodeOk = isPeriodValid(fra, til);
					if (stroperiodeOk){
						return ResourceUtils.getResource("tooltip.overflodigStroperiode");
					}else{
						return getFeilmeldingStroperiode(fra, til);
					}
				} else {
					return null;
				}
			}
		};
	}
	
	public ColorableTableValue getTypeOgFabrikat() {
		String typeOgFabrikat = "";
		
		if(kontraktKjoretoyInfo.getType() != null) {
			typeOgFabrikat += kontraktKjoretoyInfo.getType() + " ";
		}
		
		if(kontraktKjoretoyInfo.getMerke() != null) {
			typeOgFabrikat += kontraktKjoretoyInfo.getMerke() + " ";
		}
		
		if(kontraktKjoretoyInfo.getModell() != null) {
			typeOgFabrikat += kontraktKjoretoyInfo.getModell() + " ";
		}
		
		if(kontraktKjoretoyInfo.getAar() != null) {
			typeOgFabrikat += kontraktKjoretoyInfo.getAar();
		}
		
		return new KjoretoyValue(typeOgFabrikat) {
			@Override
			public boolean isRed() {
				return value == null || value.toString().equals("");
			}

			@Override
			public boolean isYellow() {
				return false;
			}

			@Override
			public String getTooltipText() {
				return isRed() ? ResourceUtils.getResource("tooltip.manglerTypeOgFabrikat") : null;
			}
		};
	}
	
	public ColorableTableValue getKjoretoyEier(){
		String eier = kontraktKjoretoyInfo.getEierNavn();
		return new KjoretoyValue(eier){
			@Override
			public boolean isRed() {
				return false;
			}
			@Override
			public boolean isYellow() {
				return value==null||value.toString().equals("");
			}

			@Override
			public String getTooltipText() {
				return isYellow() ? ResourceUtils.getResource("tooltip.manglerEier") : null;
			}
			
		};
	}
	public ColorableTableValue getLeverandor() {
		String leverandor = "";
		Long vf = kontraktKjoretoyInfo.getLeverandorValgbarFlagg();
		final Boolean valgbarFlagg=(vf!=null&&vf.intValue()==1)?Boolean.TRUE:Boolean.FALSE;
		
		
		if(kontraktKjoretoyInfo.getLeverandorNavn() != null) {
			leverandor += kontraktKjoretoyInfo.getLeverandorNavn();
		}
		if(kontraktKjoretoyInfo.getLeverandorNr() != null) {
			leverandor += " ("+kontraktKjoretoyInfo.getLeverandorNr() + ")";
		}
		
		return new KjoretoyValue(leverandor) {
			@Override
			public boolean isRed() {
				return (value == null || value.toString().equals(""))||!valgbarFlagg;
			}

			@Override
			public boolean isYellow() {
				return false;
			}

			@Override
			public String getTooltipText() {
				if (value!=null&&!value.toString().equals("")){
					if (!valgbarFlagg){
						return ResourceUtils.getResource("tooltip.leverandorIkkeValgbar");
					}
				}
				return isRed() ? ResourceUtils.getResource("tooltip.manglerLeverandor") : null;
			}
		};
	}

	public Double getTomkjoringKm() {
		return tomkjoringKm;
	}

	public Double getTotalKm() {
		return totalKm;
	}

	/**
	 * Dersom tomkjøring er 30%-40% gis gul markering.
	 * Om den er 0 eller over 50 gis rød markering
	 * 
	 * @return
	 */
	public Double getTomkjoringProsent() {
		return tomkjoringProsent;
	}
	
	private boolean betweenThreeAndFourteenDays(Date date) {
		if(date == null) return false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(Clock.now());
		cal.add(Calendar.DATE, -3);
		Date treDagerSiden = cal.getTime();
		cal.add(Calendar.DATE, -11);
		Date fjortenDagerSiden = cal.getTime();
		return date.after(fjortenDagerSiden) && date.before(treDagerSiden);
	}
	
	private boolean olderThanFourteenDays(Date date) {
		if(date == null) return true;
		Calendar cal = Calendar.getInstance();
		cal.setTime(Clock.now());
		cal.add(Calendar.DATE, -14);
		Date fjortenDagerSiden = cal.getTime();
		return date.before(fjortenDagerSiden);
	}
	
	public Boolean getNyttKjoretoy() {
		return nyttKjoretoy;
	}

	private abstract class KjoretoyValue implements ColorableTableValue {
		protected Object value;
		
		public KjoretoyValue(Object value) {
			this.value = value;
		}
		
		public Object getValue() {
			return value;
		}
		
		public String toString() {
			if(value != null) {
				return value.toString();
			} else {
				return "";
			}
		}
	}
}
