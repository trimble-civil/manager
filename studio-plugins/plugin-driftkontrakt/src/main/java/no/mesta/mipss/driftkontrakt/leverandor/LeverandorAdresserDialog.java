package no.mesta.mipss.driftkontrakt.leverandor;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.LevStedView;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXTable;

public class LeverandorAdresserDialog extends JDialog {
	private JPanel pnlMain = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private JXTable tblAdresser = new JXTable();
	private JScrollPane scrlPaneAdresser = new JScrollPane();
	private JButton btnOk = new JButton();
	
	private List<LevStedView> levStedViewList;
	
	public LeverandorAdresserDialog(JFrame parent, List<LevStedView> levStedViewList) {
		super(parent, ResourceUtils.getResource("dialogTitle.leverandorAdresser"), true);
		this.levStedViewList = levStedViewList;
		
		initGui();
		initTable();
		initButtins();
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	private void initTable() {
		MipssBeanTableModel<LevStedView> tblModelAdresser = new MipssBeanTableModel<LevStedView>(LevStedView.class, "levNr", "levNavn", "orgNr", "adresse1", "adresse2", "adresse3", "postnr", "poststed");
		tblModelAdresser.setEntities(levStedViewList);
		MipssRenderableEntityTableColumnModel columnModel = new MipssRenderableEntityTableColumnModel(LevStedView.class, tblModelAdresser.getColumns());
		
		tblAdresser.setModel(tblModelAdresser);
		tblAdresser.setColumnModel(columnModel);
	}
	
	private void initButtins() {
		btnOk.setAction(new AbstractAction(ResourceUtils.getResource("button.ok")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}
	
	private void initGui() {
		this.setSize(new Dimension(900, 500));
		this.setLayout(new GridBagLayout());
		pnlMain.setLayout(new GridBagLayout());
		pnlButtons.setLayout(new GridBagLayout());
        tblAdresser.setPreferredScrollableViewportSize(new Dimension(0, 0));
        scrlPaneAdresser.getViewport().add(tblAdresser, null);
		pnlMain.add(scrlPaneAdresser, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		pnlButtons.add(btnOk, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(pnlMain, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
		this.add(pnlButtons, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 11, 11, 11), 0, 0));
	}
}
