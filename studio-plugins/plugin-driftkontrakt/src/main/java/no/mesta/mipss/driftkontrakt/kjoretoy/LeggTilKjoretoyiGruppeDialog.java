package no.mesta.mipss.driftkontrakt.kjoretoy;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import no.mesta.mipss.driftkontrakt.util.ResourceUtils;
import no.mesta.mipss.persistence.kontrakt.Kjoretoygruppe;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoy;
import no.mesta.mipss.persistence.kontrakt.KontraktKjoretoyGruppering;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.ui.MipssBeanLoaderEvent;
import no.mesta.mipss.ui.MipssBeanLoaderListener;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.jdesktop.swingx.JXBusyLabel;

@SuppressWarnings("serial")
public class LeggTilKjoretoyiGruppeDialog extends JDialog{

	private final KjoretoygruppeController controller;
	private JButton btnLeggTil;
	private JButton btnFjern;
	private JButton btnOk;
	private JButton btnAvbryt;
	
	private JMipssBeanTable<KontraktKjoretoy> tblKontraktKjoretoy;
	private MipssBeanTableModel<KontraktKjoretoy> tblModelKontraktKjoretoy;
	private JScrollPane scrKontraktKjoretoy;
	
	private JMipssBeanTable<KontraktKjoretoyGruppering> tblKjoretoygruppe;
	private MipssBeanTableModel<KontraktKjoretoyGruppering> tblModelKjoretoygruppe;
	private JScrollPane scrKjoretoygruppe;
	
	private JXBusyLabel bsyKontraktKjoretoy;
	private JXBusyLabel bsyKjoretoygruppe;
	
	private final Kjoretoygruppe kjoretoygruppe;

	private boolean cancelled;
	
	public LeggTilKjoretoyiGruppeDialog(JFrame parent, KjoretoygruppeController controller, Kjoretoygruppe kjoretoygruppe){
		super(parent, true);
		this.controller = controller;
		this.kjoretoygruppe = kjoretoygruppe;
		
		initComponents();
		initGui();
		loadTables();
	}

	private void initComponents() {
		
		bsyKontraktKjoretoy = new JXBusyLabel();
		bsyKontraktKjoretoy.setText(ResourceUtils.getResource("label.henterKontraktKjoretoy"));
		bsyKjoretoygruppe = new JXBusyLabel();
		bsyKjoretoygruppe.setText(ResourceUtils.getResource("label.henterGruppensKjoretoy"));
		
		tblModelKontraktKjoretoy = new MipssBeanTableModel<KontraktKjoretoy>(KontraktKjoretoy.class, "kjoretoynavn", "type", "leverandor");
		tblModelKontraktKjoretoy.setBeanInstance(controller);
		tblModelKontraktKjoretoy.setBeanInterface(KjoretoygruppeController.class);
		tblModelKontraktKjoretoy.setBeanMethod("getKjoretoyUtenforGruppering");
		tblModelKontraktKjoretoy.setBeanMethodArguments(new Class<?>[]{Kjoretoygruppe.class});
		tblModelKontraktKjoretoy.addTableLoaderListener(new MipssBeanLoaderListener(){
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				bsyKontraktKjoretoy.setBusy(false);
				scrKontraktKjoretoy.setViewportView(tblKontraktKjoretoy);
			}
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {
				bsyKontraktKjoretoy.setBusy(true);
				scrKontraktKjoretoy.setViewportView(bsyKontraktKjoretoy);
			}
		});
		MipssRenderableEntityTableColumnModel colModelKontraktKjoretoy = new MipssRenderableEntityTableColumnModel(KontraktKjoretoy.class, tblModelKontraktKjoretoy.getColumns());
		tblKontraktKjoretoy = new JMipssBeanTable<KontraktKjoretoy>(tblModelKontraktKjoretoy, colModelKontraktKjoretoy);
		tblKontraktKjoretoy.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblKontraktKjoretoy.setFillsViewportWidth(true);
		tblKontraktKjoretoy.setDoWidthHacks(false);

		tblModelKjoretoygruppe = new MipssBeanTableModel<KontraktKjoretoyGruppering>(KontraktKjoretoyGruppering.class, "kjoretoynavn", "type", "leverandor");
		tblModelKjoretoygruppe.setBeanInstance(controller);
		tblModelKjoretoygruppe.setBeanInterface(KjoretoygruppeController.class);
		tblModelKjoretoygruppe.setBeanMethod("getKjoretoyIGruppe");
		tblModelKjoretoygruppe.setBeanMethodArguments(new Class<?>[]{Kjoretoygruppe.class});
		tblModelKjoretoygruppe.addTableLoaderListener(new MipssBeanLoaderListener(){
			@Override
			public void loadingFinished(MipssBeanLoaderEvent event) {
				bsyKjoretoygruppe.setBusy(false);
				scrKjoretoygruppe.setViewportView(tblKjoretoygruppe);
			}
			@Override
			public void loadingStarted(MipssBeanLoaderEvent event) {
				bsyKjoretoygruppe.setBusy(true);
				scrKjoretoygruppe.setViewportView(bsyKjoretoygruppe);
			}
		});
		MipssRenderableEntityTableColumnModel colModelKjoretoyGruppe = new MipssRenderableEntityTableColumnModel(KontraktKjoretoyGruppering.class, tblModelKjoretoygruppe.getColumns());
		tblKjoretoygruppe = new JMipssBeanTable<KontraktKjoretoyGruppering>(tblModelKjoretoygruppe, colModelKjoretoyGruppe);
		tblKjoretoygruppe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblKjoretoygruppe.setFillsViewportWidth(true);
		tblKjoretoygruppe.setDoWidthHacks(false);

		scrKontraktKjoretoy = new JScrollPane(tblKontraktKjoretoy);
		scrKjoretoygruppe = new JScrollPane(tblKjoretoygruppe);
		
		btnLeggTil = new JButton(controller.getLeggTilKjoretoyiGruppeListAction(tblKontraktKjoretoy, tblModelKontraktKjoretoy, tblModelKjoretoygruppe, kjoretoygruppe));
		btnLeggTil.setToolTipText(ResourceUtils.getResource("button.leggTiliGruppeDialog.tooltip"));
		btnFjern = new JButton(controller.getFjernKjoretoyFraGruppeListAction(tblKjoretoygruppe, tblModelKjoretoygruppe, tblModelKontraktKjoretoy));
		btnFjern.setToolTipText(ResourceUtils.getResource("button.fjernFraGruppeDialog.tooltip"));
		
		btnOk = new JButton(getOkAction());
		btnOk.setToolTipText(ResourceUtils.getResource("button.lagreGruppe.tooltip"));
		btnAvbryt = new JButton(getAvbrytAction());
		btnAvbryt.setToolTipText(ResourceUtils.getResource("button.avbrytGruppe.tooltip"));

	}
	
	private void initGui() {
		setLayout(new GridBagLayout());
		
		JPanel pnlKontraktKjoretoy = new JPanel(new BorderLayout());
		pnlKontraktKjoretoy.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kontraktKjoretoy")));
		
		JPanel pnlKjoretoyGruppe = new JPanel(new BorderLayout());
		pnlKjoretoyGruppe.setBorder(BorderFactory.createTitledBorder(ResourceUtils.getResource("border.kjoretoyGruppering", kjoretoygruppe.getNavn())));
		
		JPanel pnlAddButtons = new JPanel(new GridBagLayout());
		pnlAddButtons.add(btnLeggTil, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.SOUTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		pnlAddButtons.add(btnFjern, 	new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		
		JPanel pnlButton = new JPanel(new GridBagLayout());
		pnlButton.add(btnOk, 	 new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		pnlButton.add(btnAvbryt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));

		pnlKontraktKjoretoy.add(scrKontraktKjoretoy);
		pnlKjoretoyGruppe.add(scrKjoretoygruppe);

		add(pnlKontraktKjoretoy,new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
		add(pnlAddButtons, 		new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		add(pnlKjoretoyGruppe,  new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
		add(pnlButton, 			new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
		
	}
	private void loadTables(){
		tblModelKjoretoygruppe.setBeanMethodArgValues(new Object[]{kjoretoygruppe});
		tblModelKontraktKjoretoy.setBeanMethodArgValues(new Object[]{kjoretoygruppe});
		
		tblModelKjoretoygruppe.loadData();
		tblModelKontraktKjoretoy.loadData();
	}
	public boolean isCancelled(){
		return cancelled;
	}
	
	private Action getOkAction(){
		return new AbstractAction("Lagre", IconResources.SAVE_ICON) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = false;
				dispose();
			}
		};
	}
	
	private Action getAvbrytAction(){
		return new AbstractAction("Avbryt", IconResources.BUTTON_CANCEL_ICON){
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelled = true;
				dispose();
			}
		};
	}
	
	public List<KontraktKjoretoyGruppering> getKontraktKjoretoyGrupperingList(){
		return tblModelKjoretoygruppe.getEntities();
	}
	
}
