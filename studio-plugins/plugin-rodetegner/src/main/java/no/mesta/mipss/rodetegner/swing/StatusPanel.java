package no.mesta.mipss.rodetegner.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.util.CoordinateEvent;
import no.mesta.mipss.mapplugin.util.CoordinateMessageListener;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.JXProgressBar;
import no.mesta.mipss.ui.progressbar.ProgressController;
import no.mesta.mipss.ui.progressbar.ProgressPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * StatusPanel for the map
 * 
 * Implements a PaintMessageListener for receiving information on the progress of painting the map
 * and CoordinateMessageListener for receiving information about the actual coordinate under the mouse cursor
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class StatusPanel extends JPanel implements CoordinateMessageListener{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private JXProgressBar progressBar = new JXProgressBar();
    private JLabel eastingLabel;
    private JLabel northingLabel;
    private JLabel eastingValue;
    private JLabel northingValue;
    
    private ProgressPanel progressPanel ;
	private ProgressController progressController;
	private JDialog workerDialog;
    //private JXBusyLabel mapInfoLabel;
    private JLabel infoLabel;
    private RodetegnerModule plugin;
    public StatusPanel(RodetegnerModule plugin) {
    	this.plugin = plugin;
    	
        progressBar.setPreferredSize(new Dimension(150, 20));
        progressBar.setSize(150, 20);
        
        progressPanel = new ProgressPanel(progressBar);
        progressController = new ProgressController(progressPanel);
        
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        
        eastingLabel = new JLabel(resources.getGuiString("statuspanel.east"));
        northingLabel = new JLabel(resources.getGuiString("statuspanel.north"));
        eastingValue = new JLabel();
        northingValue = new JLabel();
        
        infoLabel = new JLabel();
        
        JPanel coordinatePanel = new JPanel();
        coordinatePanel.setLayout(new BoxLayout(coordinatePanel, BoxLayout.LINE_AXIS));
        
        Dimension valueSize = new Dimension(120,20);
        Dimension sepSize = new Dimension(2, 20);
        
        coordinatePanel.add(northingLabel);
        coordinatePanel.add(Box.createRigidArea(new Dimension(5,0)));
        setComponentSize(valueSize, northingValue);
        coordinatePanel.add(northingValue);
        
        JSeparator sep = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep);
        coordinatePanel.add(sep);
        
        coordinatePanel.add(eastingLabel);
        coordinatePanel.add(Box.createRigidArea(new Dimension(5,0)));
        setComponentSize(valueSize, eastingValue);
        coordinatePanel.add(eastingValue);
        
        
        add(coordinatePanel);
        
        JSeparator sep1 = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep1);
        add(sep1);
        
        add(Box.createHorizontalGlue());
        JSeparator sep2 = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep2);
        add(sep2);
        Dimension infoSize = new Dimension(100, 20);
        setComponentSize(infoSize, infoLabel);
        add(infoLabel);
        
        add(Box.createRigidArea(new Dimension(5,0)));
        
        
        JSeparator sep3 = new JSeparator(JSeparator.VERTICAL);
        setComponentSize(sepSize, sep3);
        add(sep3);
        
        Dimension progressSize = new Dimension(150, 20);
        setComponentSize(progressSize, progressBar);
        add(progressBar);
        
        progressBar.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount()==2){
					if (workerDialog == null){
						Component c = getParent();
						while (!(c instanceof Window)){
							c = c.getParent();
						}
						workerDialog = new JDialog((Window)c);
						workerDialog.add(progressPanel);
						workerDialog.setTitle(resources.getGuiString("JStatusPanel.workerDialog.title"));
						workerDialog.pack();
						workerDialog.setLocationRelativeTo(null);
						workerDialog.setVisible(true);
						progressPanel.setDialog(workerDialog);
						
					}
					else{
						workerDialog.setVisible(true);
						workerDialog.requestFocus();
					}
					
				}
			}
		});
        startProgressThread();
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	plugin = null;
    }
    private void startProgressThread(){
    	
    	new Thread("JStatusPanel update thread"){
    		
			public void run(){
				while (plugin!=null&&plugin.isRunning()){
					try{
						if (progressPanel.getUiList().isEmpty()){
							progressBar.setString(resources.getGuiString("JStatusPanel.progressBar.defaultText"));
							if (progressBar.isIndeterminate())
								progressBar.setIndeterminate(false);
						}else{
							progressBar.setString(resources.getGuiString("JStatusPanel.progressBar.workingText1")+progressPanel.getUiList().size()+resources.getGuiString("JStatusPanel.progressBar.workingText2"));
							if (!progressBar.isIndeterminate())
								progressBar.setIndeterminate(true);
						}
						Thread.sleep(500);
					}catch (Exception e){
					}
				}
			}
		}.start();
    }
    /**
     * Set the component size
     * @param dim size
     * @param comp JComponent
     */
    private void setComponentSize(Dimension dim, JComponent comp){
        comp.setPreferredSize(dim);
        comp.setMinimumSize(dim);
        comp.setMaximumSize(dim);
    }
    /**
     * Returns the progressbar from the statuspanel
     * @return
     */
    public JXProgressBar getProgressBar(){
        return progressBar;
    }
    
    /**
     * Set the value to be displayed as north coordinate
     * @param value
     */
    public void setNorthingValue(String value){
        northingValue.setText(value);
    }
    /**
     * Set the value to be displayed as east coordinate
     * @param value
     */
    public void setEastingValue(String value){
        eastingValue.setText(value);
    }
    
    /**
     * Set a text on the infolabel and start the JXBusyLabel 
     * @param value
     */
    public void startMapInfo(final String value){
        infoLabel.setText(value);
    }
    /**
     * Set a text on the infoLabel and stopp the JXBusyLabel from spinning
     * @param value
     */
    public void stopMapInfo(String value){
        infoLabel.setText(value);
    }
    
    /**
     * Event received 
     * @param e
     */
    public void coordinateChanged(CoordinateEvent e) {
        setNorthingValue(e.getNorthing());
        setEastingValue(e.getEasting());
    }
    
    /**
	 * @return the controller
	 */
	public ProgressController getProgressController() {
		return progressController;
	}
}

