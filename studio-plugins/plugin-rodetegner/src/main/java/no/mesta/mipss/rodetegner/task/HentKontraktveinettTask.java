package no.mesta.mipss.rodetegner.task;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.LayerBuilder;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.service.veinett.KontraktveinettVO;
import no.mesta.mipss.service.veinett.VeinettService;
import no.mesta.mipss.ui.progressbar.ExecutorTaskType;

public class HentKontraktveinettTask extends RodegeneratorTask{
	private Driftkontrakt kontrakt;
	private RodetegnerModule plugin;
	private final boolean veityper;
	
	public HentKontraktveinettTask(RodetegnerModule plugin, Driftkontrakt kontrakt, boolean veityper){
		super(ExecutorTaskType.QUEUE);
		this.plugin = plugin;
		this.kontrakt = kontrakt;
		this.veityper = veityper;
		
	}
	
	@Override
	protected Layer doInBackground() throws Exception {
		if (isCancelled()){
			return null;
		}
		getProgressWorkerPanel().setBarIndeterminate(true);
		getProgressWorkerPanel().setBarString(getProgressWorkerPanel().getLabel());
		VeinettService veinettBean = BeanUtil.lookup(VeinettService.BEAN_NAME, VeinettService.class);
		
		KontraktveinettVO veinett = veinettBean.getKontraktveinett(kontrakt.getId(), veityper);
		if (veityper){
			return new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getKontraktveiLayer(veinett);
		}
        return new LayerBuilder(plugin.getLoader(), plugin.getMainMap()).getLayerFromKontraktveinettVO(veinett);
	}
}
