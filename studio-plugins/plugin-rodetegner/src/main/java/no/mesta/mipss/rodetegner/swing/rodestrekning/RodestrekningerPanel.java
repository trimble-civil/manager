package no.mesta.mipss.rodetegner.swing.rodestrekning;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;

import no.mesta.mipss.common.BeanUtil;
import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.persistence.veinett.Veinettveireferanse;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodeservice.Rodegenerator;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.ui.beantable.JMipssBeanTable;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.roadpicker.KontraktveinettDialog;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;

import org.apache.commons.lang.StringUtils;

/**
 * Panel for å velge, legge til og slette rodestrekninger
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class RodestrekningerPanel extends AbstractRodePanel implements PropertyChangeListener{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    private RodetegnerModule plugin;
    private JMipssBeanTable<Veinettveireferanse> strekningTable;
    private MipssBeanTableModel<Veinettveireferanse> strekningTableModel;
    private JLabel infoLabel;
    private boolean disabled;
    
    public RodestrekningerPanel(RodetegnerModule plugin) {
        this.plugin = plugin;
        initGui();
    }
    
    private void initGui(){
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        createStrekningTable();
        strekningTable.addPropertyChangeListener("selectedEntities", this);
        strekningTable.setModel(strekningTableModel);
        Dimension tableSize = new Dimension(415, 200);
        strekningTable.setPreferredScrollableViewportSize(tableSize);
        JScrollPane scroll = new JScrollPane(strekningTable);
        
        strekningTable.getColumn(0).setPreferredWidth(20);
        strekningTable.getColumn(1).setPreferredWidth(35);
        strekningTable.getColumn(2).setPreferredWidth(30);
        strekningTable.getColumn(3).setPreferredWidth(25);
        strekningTable.getColumn(4).setPreferredWidth(20);
        strekningTable.getColumn(5).setPreferredWidth(10);
        strekningTable.getColumn(6).setPreferredWidth(30);
        strekningTable.getColumn(7).setPreferredWidth(30);
        
        add(scroll);
        add(Box.createVerticalStrut(10));
        add(getOptionsPanel());
        add(Box.createVerticalStrut(10));
        add(getButtonPanel());
    }
    
    private JPanel getOptionsPanel(){
        JPanel panel = new JPanel(){
            public Dimension getMaximumSize() {
                Dimension size = getPreferredSize();
                size.width = Short.MAX_VALUE;
                return size;
            }
        };
        Dimension size = new Dimension(465, 25);
        setComponentSize(size, panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        
        JButton deleteButton = new JButton(new DeleteStrekningerAction(resources.getGuiString("rodestrekningpanel.deletebutton"), IconResources.BUTTON_CANCEL_ICON));
        JButton newButton = new JButton(new NewStrekningAction(resources.getGuiString("rodestrekningpanel.newbutton"), IconResources.NEW_ICON));
        JButton pickButton = new JButton(new PickAction(resources.getGuiString("rodestrekningpanel.pickbutton"), null));
        
        deleteButton.setToolTipText(resources.getGuiString("rodestrekningpanel.deletebutton.tooltip"));
        newButton.setToolTipText(resources.getGuiString("rodestrekningpanel.newbutton.tooltip"));
        pickButton.setToolTipText(resources.getGuiString("rodestrekningpanel.pickbutton.tooltip"));
        
        
        panel.add(deleteButton);
        panel.add(Box.createHorizontalStrut(5));
        panel.add(newButton);
        panel.add(Box.createHorizontalStrut(5));
        panel.add(pickButton);
        panel.add(Box.createHorizontalGlue());
        return panel;
    }
    
    private JPanel getButtonPanel(){
        JPanel panel = new JPanel(){
            public Dimension getMaximumSize() {
                Dimension size = getPreferredSize();
                size.width = Short.MAX_VALUE;
                return size;
            }

        };
        Dimension size = new Dimension(415, 25);
        setComponentSize(size, panel);
        
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        
        JButton cancelButton = new JButton(new CancelButtonAction("Avbryt", IconResources.CANCEL_ICON));
        JButton okButton = new JButton(new OkButtonAction("Lagre", IconResources.OK2_ICON));        
        infoLabel = new JLabel();
        infoLabel.setText(":");
        panel.add(infoLabel);
        panel.add(Box.createHorizontalGlue());
        panel.add(cancelButton);
        panel.add(Box.createHorizontalStrut(5));
        panel.add(okButton);
        return panel;
    }

    public int getVerticalStrut() {
        return 0;
    }

    /**
     * Oppretter strekningtabell med modell og columnmodel
     */
    private void createStrekningTable(){
        strekningTableModel = new MipssBeanTableModel<Veinettveireferanse>(Veinettveireferanse.class, new int[]{0,1,2,3,4,5,6,7}, "fylkesnummer", "kommunenummer", "veikategori", "veistatus", "veinummer", "hp", "fraKm", "tilKm");
        strekningTableModel.setBeanInterface(Rodegenerator.class);
        strekningTableModel.setBeanInstance(BeanUtil.lookup(Rodegenerator.BEAN_NAME, Rodegenerator.class));
        strekningTableModel.setBeanMethod("getVeireferanseForRode");
        strekningTableModel.setBeanMethodArguments(new Class[]{Rode.class});
        strekningTableModel.setBeanMethodArgValues(new Object[]{plugin.getRodeController().getRode()});
        strekningTableModel.loadData();
        
        MipssRenderableEntityTableColumnModel strekningerColumnModel = new MipssRenderableEntityTableColumnModel(
            Veinettveireferanse.class, "fylkesnummer", "kommunenummer", "veikategori", "veistatus", "veinummer", "hp", "fraKm", "tilKm");
                
        strekningTable = new JMipssBeanTable<Veinettveireferanse>(strekningTableModel, strekningerColumnModel);
        strekningTable.setFillsViewportWidth(true);

    }
    /**
     * Actionklasse for å klikke ok på rodestrekningvinduet
     */
    class OkButtonAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public OkButtonAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	plugin.getRodeController().mergeStekninger(strekningTableModel.getEntities());
        	SwingUtilities.windowForComponent(RodestrekningerPanel.this).dispose();
        }
    }
    /**
     * Actionklasse for å klikke ok på rodestrekningvinduet
     */
    class CancelButtonAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CancelButtonAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	SwingUtilities.windowForComponent(RodestrekningerPanel.this).dispose();
        }
    }
    
    /**
     * Actionklasse for å klikke ok på rodestrekningvinduet
     */
    class DeleteStrekningerAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public DeleteStrekningerAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
//        	int[] selectedRows = strekningTable.getSelectedRows();
//        	List<Veinettveireferanse> veiList =new ArrayList<Veinettveireferanse>();
//        	for (int i:selectedRows){
//        		int index = strekningTable.convertRowIndexToModel(i);
//        		veiList.add((Veinettveireferanse)plugin.getStrekningTableModel().getEntities().get(index));
//        	}
        	List<Veinettveireferanse> strekninger = strekningTable.getSelectedEntities();
        	strekningTable.clearSelection();
        	for (Veinettveireferanse v:strekninger){
        		strekningTableModel.removeItem(v);
        	}
        }
    }
    
    /**
     * Actionklasse for å klikke ok på rodestrekningvinduet
     */
    class NewStrekningAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public NewStrekningAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	strekningTableModel.addItem(new Veinettveireferanse());
        }
    }
    
    class PickAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public PickAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	KontraktveinettDialog dialog = new KontraktveinettDialog((Dialog)SwingUtilities.windowForComponent(RodestrekningerPanel.this), resources.getGuiString("rodestrekningpanel.kontraktvei.title"), plugin.getRodeController().getDriftkontrakt(), true, false, false, false, false);
    		dialog.setLocationRelativeTo(plugin.getModuleGUI());
        	dialog.setVisible(true);
//        	KontraktveistrekningerPanel kontraktPanel = new KontraktveistrekningerPanel(plugin);
//        	dialog.add(kontraktPanel);
//        	dialog.pack();
//        	dialog.setLocationRelativeTo(null);
//        	dialog.setVisible(true);
        	strekningTable.clearSelection();
        	List<Veinettveireferanse> valgte = dialog.getSelectedVeireferanser();
        	if (valgte!=null){
        		strekningTableModel.getEntities().addAll(valgte);
        	}
        	strekningTableModel.fireTableModelEvent(0, 0, TableModelEvent.INSERT);
        }
    }

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == strekningTable && StringUtils.equals("selectedEntities", evt.getPropertyName())) {
			infoLabel.setText("");
			List<Veinettveireferanse> strekninger = strekningTable.getSelectedEntities();
			if (strekninger.size()==1){
				Veinettveireferanse v = strekninger.get(0);
				if (isNull(v)){
					return;
				}
			}
			for (Veinettveireferanse v:strekninger){
				v.setVeikategori(v.getVeikategori().toUpperCase());
				v.setVeistatus(v.getVeistatus().toUpperCase());
			}
			if (strekninger!=null&&strekninger.size()!=0){
				plugin.getRodeController().markerStrekninger(strekninger, infoLabel, false);
			}
		}
	}
	private boolean isNull(Veinettveireferanse v){
		if (v.getFraKm()==null&&
			v.getFylkesnummer()==null&&
			v.getHp()==null&&
			v.getKommunenummer()==null&&
			v.getTilKm()==null&&
			v.getVeikategori()==null&&
			v.getVeinummer()==null&&
			v.getVeistatus()==null)
			return true;
		return false;
	}
}
