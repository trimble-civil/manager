package no.mesta.mipss.rodetegner.swing;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

import no.mesta.mipss.mapplugin.MapPanel;
import no.mesta.mipss.rodetegner.RodetegnerModule;

import org.jdesktop.swingx.border.DropShadowBorder;

/**
 * Panel for gui til Rodegenerator
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class MipssRodegeneratorPanel extends JPanel{
    
    private RodetegnerModule plugin;
    private StatusPanel statusPanel;
    private MainMapPanel mapPanel;
    private MainLeftPanel leftPanel;
    public MipssRodegeneratorPanel(RodetegnerModule plugin) {
        this.plugin = plugin;
        setLayout(new BorderLayout());
        initGui();
    }
    private void initGui(){
        leftPanel = new MainLeftPanel(plugin);
        leftPanel.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));
        plugin.setRodePanel(leftPanel.getRodePanel());
        mapPanel = plugin.getMapPanel();
        mapPanel.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));
        
        mapPanel.getMapViewPanel().activateCoordinateEvents();
        
        statusPanel = new StatusPanel(plugin);
        statusPanel.setBorder(new DropShadowBorder(new Color(0,0,153), 5, .5f, 12, false, false, true, true));
        
//        mapPanel.getMapViewPanel().getLayerHandler().addMessageListener(statusPanel);
        mapPanel.getMapViewPanel().addCoordinateMessageListener(statusPanel);
//        plugin.setMainMap(mapPanel.getMapViewPanel());
        add(leftPanel, BorderLayout.WEST);
        add(mapPanel, BorderLayout.CENTER);
        add(statusPanel, BorderLayout.PAGE_END);
    }
    
    public StatusPanel getStatusPanel(){
    	return statusPanel;
    }
    
    public MapPanel getMapPanel(){
    	return mapPanel.getMapViewPanel();
    }
    
    public ToolbarPanel getToolbarPanel(){
    	return mapPanel.getToolbarPanel();
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	leftPanel.dispose();
    	statusPanel.dispose();
    	plugin = null;
    }
}
