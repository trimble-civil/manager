package no.mesta.mipss.rodetegner.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import no.mesta.mipss.core.ColorUtil;
import no.mesta.mipss.core.KonfigparamPreferences;
import no.mesta.mipss.konfigparam.KonfigParam;
import no.mesta.mipss.mapplugin.JMapViewPanel;
import no.mesta.mipss.mapplugin.tools.MoveLayerTool;
import no.mesta.mipss.mapplugin.tools.PanMapTool;
import no.mesta.mipss.mapplugin.util.LayerType;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.mapplugin.util.MouseListenerManager;
import no.mesta.mipss.mapplugin.utm.UTMProjectionTileFactory;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.tools.SelectTool;

 /**
  * Panel for å velge, legge til og slette rodetyper.
  * 
  * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
  */
public class MainMapPanel extends JPanel{

    private JMapViewPanel mapViewPanel;
    private ToolbarPanel toolbarPanel;
    
    public MainMapPanel(RodetegnerModule plugin) {
        setLayout(new BorderLayout());
        mapViewPanel = new JMapViewPanel();
        if (mapViewPanel.getTileFactory() instanceof UTMProjectionTileFactory){
        	KonfigParam params  = KonfigparamPreferences.getInstance().getKonfigParam();
        	String blendCol = params.hentEnForApp("studio.mipss.rodegenerator", "mapBlendColor").getVerdi();
        	String blendVal = params.hentEnForApp("studio.mipss.rodegenerator", "mapBlendValue").getVerdi();
        	Color color = ColorUtil.fromHexString(blendCol);
        	Float val = Float.valueOf(blendVal);
        	((UTMProjectionTileFactory)mapViewPanel.getTileFactory()).setColorFilter(color, val);
        	mapViewPanel.setBackground(new Color(127, 127, 127));
        }
        toolbarPanel = new ToolbarPanel(plugin);
        
      //set opp kartet til å brukte gitte kontrollere
        MouseListenerManager mouseListenerManager = new MouseListenerManager();
        SelectTool selectListener = new SelectTool(mapViewPanel, plugin.getRodePanel());
        
        MoveLayerTool moveListener = new MoveLayerTool(mapViewPanel);
        
        mouseListenerManager.addListener(0, new PanMapTool(mapViewPanel));
        mouseListenerManager.addListener(0, selectListener);
        mouseListenerManager.addListener(0, moveListener);
        mapViewPanel.setMouseListenerManager(mouseListenerManager);
        List<LayerType> layerTypes = new ArrayList<LayerType>();
        for (MipssMapLayerType l: MipssMapLayerType.values()){
        	layerTypes.add(l.getType());
        }
        mapViewPanel.getLayerHandler().setLayerTypes(layerTypes);
        
        add(mapViewPanel);
        add(toolbarPanel, BorderLayout.PAGE_START);
    }
    /**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	toolbarPanel.dispose();
    }
    
    public JMapViewPanel getMapViewPanel(){
        return mapViewPanel;
    }
    
    public ToolbarPanel getToolbarPanel(){
    	return toolbarPanel;
    }
}
