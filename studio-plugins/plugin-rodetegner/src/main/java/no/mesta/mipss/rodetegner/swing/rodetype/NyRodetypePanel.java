package no.mesta.mipss.rodetegner.swing.rodetype;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.BindingHelper;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.ui.DocumentFilter;

import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;

/**
 * Panel for rodetype spesifikke komponenter.
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class NyRodetypePanel extends AbstractRodePanel{
    private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    private RodetegnerModule plugin;
    private Rodetype rodetype;
	private boolean viewMode;
    public NyRodetypePanel(RodetegnerModule plugin, Rodetype rodetype, boolean viewMode) {
        this.plugin = plugin;
        this.rodetype = rodetype;
        this.viewMode = viewMode;
        initGui();
    }
    
    private void initGui(){
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        JLabel nameLabel = new JLabel(resources.getGuiString("rodetypepanel.namelabel"));
        JFormattedTextField nameField = new JFormattedTextField();
        nameField.setDocument(new DocumentFilter(30, null, false));
        
        JCheckBox overlappingAllowedCheck = new JCheckBox(resources.getGuiString("rodetypepanel.checkbox.overlapping"));
        overlappingAllowedCheck.setEnabled(false);
        JCheckBox oneInstanceCheck = new JCheckBox(resources.getGuiString("rodetypepanel.checkbox.en"));
        JCheckBox generateReportDataCheck = new JCheckBox(resources.getGuiString("rodetypepanel.checkbox.rapportdata"));
        
        JEditorPane descriptionArea = new JEditorPane();
        
        descriptionArea.setDocument(new DocumentFilter(255, null, false));
        JScrollPane descriptionScroll = new JScrollPane(descriptionArea);
        
        JPanel namePanel = new JPanel();
        namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.LINE_AXIS));
        namePanel.add(nameLabel);
        namePanel.add(Box.createRigidArea(new Dimension(10,0)));
        namePanel.add(nameField);
        namePanel.add(Box.createHorizontalGlue());
        
        JPanel descriptionPanel = new JPanel();
        descriptionPanel.setLayout(new BoxLayout(descriptionPanel, BoxLayout.LINE_AXIS));
        descriptionPanel.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("rodetypepanel.description.border")));
        descriptionPanel.add(descriptionScroll);
        descriptionPanel.setMinimumSize(new Dimension(350, 100));
        descriptionPanel.setPreferredSize(new Dimension(350, 100));
        
        setNameFieldSize(nameField);
        
        BindingGroup group = new BindingGroup();
        group.addBinding(BindingHelper.createbinding(rodetype, "navn", nameField, "text", UpdateStrategy.READ_WRITE));
        group.addBinding(BindingHelper.createbinding(rodetype, "beskrivelse", descriptionArea, "text", UpdateStrategy.READ_WRITE));
        
        if (viewMode){
	        group.addBinding(BindingHelper.createbinding(rodetype, "kanHaOverlappFlagg", overlappingAllowedCheck, "selected", UpdateStrategy.READ_WRITE));
	        group.addBinding(BindingHelper.createbinding(rodetype, "bareEnRodeFlagg", oneInstanceCheck, "selected", UpdateStrategy.READ_WRITE));
	        group.addBinding(BindingHelper.createbinding(rodetype, "genererProduksjonFlagg", generateReportDataCheck, "selected", UpdateStrategy.READ_WRITE));
        }else{
        	group.addBinding(BindingHelper.createbinding(overlappingAllowedCheck, "selected", rodetype, "kanHaOverlappFlagg"));
            group.addBinding(BindingHelper.createbinding(oneInstanceCheck, "selected", rodetype, "bareEnRodeFlagg"));
            group.addBinding(BindingHelper.createbinding(generateReportDataCheck, "selected", rodetype, "genererProduksjonFlagg"));
        }
        group.bind();
        if (!viewMode){
        	generateReportDataCheck.setSelected(true);
        }
        if (viewMode){
        	if (rodetype.getDriftkontrakt()==null){
        		nameField.setEditable(false);
        		descriptionArea.setEditable(false);
        		oneInstanceCheck.setEnabled(false);
        		generateReportDataCheck.setEnabled(false);
        		overlappingAllowedCheck.setEnabled(false);
        	}
        }
        add(namePanel);
        add(Box.createRigidArea(new Dimension(0,10)));
        add(createCheckPanel(overlappingAllowedCheck));
        add(createCheckPanel(oneInstanceCheck));
        add(createCheckPanel(generateReportDataCheck));
        add(Box.createRigidArea(new Dimension(0,10)));
        add(descriptionPanel);
    }
    
    /**
     * Lager et panel som inneholder en JCheckBox
     * @param check
     * @return
     */
    private JPanel createCheckPanel(JCheckBox check){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.add(check);
        panel.add(Box.createHorizontalGlue());
        return panel;
        
    }
    
    /**
     * Setter størrelsen på nameField
     * @param nameField
     */
    private void setNameFieldSize(JTextField nameField){
        Dimension size = new Dimension(200, 22);
        setComponentSize(size, nameField);
    }

    public int getVerticalStrut() {
        return 0;
    }
}
