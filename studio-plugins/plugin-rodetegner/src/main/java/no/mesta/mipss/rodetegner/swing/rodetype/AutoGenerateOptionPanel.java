package no.mesta.mipss.rodetegner.swing.rodetype;

import java.awt.Component;
import java.awt.Frame;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;

/**
 * Panel for å velge autogenerering av roder
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class AutoGenerateOptionPanel extends AbstractRodePanel{
    private RodetegnerModule plugin;
    private ButtonGroup group;
    
    
    /**
     * Konstruktør
     * @param plugin RodetegnerModulen som styrer dette panelet
     */
    public AutoGenerateOptionPanel(RodetegnerModule plugin) {
    	this.plugin = plugin;
        initGui();
    }
    
    /**
     * Initialiserer gui komponentene
     */
    private void initGui(){
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createTitledBorder(""));
        
        group = new ButtonGroup();
        
        JRadioButton none = new JRadioButton("Ingen");
        none.setSelected(true);
        JRadioButton copy = new JRadioButton("Kopier eksisterende rode");
        JRadioButton adt = new JRadioButton("Generer fra ÅDT");
        JRadioButton kategori = new JRadioButton("Veikategori");
        JRadioButton strategi = new JRadioButton("Veistrategi");
        JRadioButton slitasje = new JRadioButton("Sporslitasje");
        
        group.add(none);
        group.add(copy);
        group.add(adt);
        group.add(kategori);
        group.add(strategi);
        group.add(slitasje);
        
        add(createRadioPanel(none));
        add(createRadioPanel(copy));
        add(createRadioPanel(adt));
        add(createRadioPanel(kategori));
        add(createRadioPanel(strategi));
        add(createRadioPanel(slitasje));
        add(Box.createVerticalStrut(10));
        add(createDatePanel("Gyldig fra", 10));
        add(Box.createVerticalStrut(5));
        add(createDatePanel("Gyldig til", 16));
    }
    
    /**
     * Lager et panel som inneholder JRadioButton
     * @param radio
     * @return
     */
    private JPanel createRadioPanel(JRadioButton radio){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.add(Box.createHorizontalStrut(10));
        panel.add(radio);
        panel.add(Box.createHorizontalGlue());
        return panel;
    }
    /**
     * Aktiverer eller deaktiverer ALLE komponentene i dette panelet
     * @param enabled true hvis alle skal aktiverers, false hvis de skal deaktiveres
     */
    @Override
    public void setEnabled(boolean enabled){
        setComponentsEnabled(enabled, this);
    }
    
    /**
     * Aktiverer eller deaktiverer ALLE komponentene i componenten som er satt som parameter
     * @param enabled true hvis alle skal aktiverers, false hvis de skal deaktiveres
     * @param component komponenten som inneholder de andre komponentene
     */
    private void setComponentsEnabled(boolean enabled, Component component){
        if (component instanceof JPanel){
            for (Component c: ((JPanel)component).getComponents()){
                setComponentsEnabled(enabled, c);    
            }
        }else{
            component.setEnabled(enabled);
        }
    }

    public int getVerticalStrut() {
        return 0;
    }
}
