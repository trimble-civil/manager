package no.mesta.mipss.rodetegner.swing;

import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Panel som wrapper andre paneler og lager en standard avstand mellom
 * kantene av panelet.
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class DefaultWrapPanel extends JPanel{
    
    public DefaultWrapPanel(List<AbstractRodePanel> panels) {
        initGui(panels);
    }
    
    private void initGui(List<AbstractRodePanel> panels){
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        JPanel wrapPanel = new JPanel();
        wrapPanel.setLayout(new BoxLayout(wrapPanel, BoxLayout.LINE_AXIS));
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        
        
        for (AbstractRodePanel panel:panels){
            if (panel.getVerticalStrut()>0)
                mainPanel.add(Box.createVerticalStrut(panel.getVerticalStrut()));
            mainPanel.add(panel);
            
        }
        
        wrapPanel.add(Box.createHorizontalStrut(10));
        wrapPanel.add(mainPanel);
        wrapPanel.add(Box.createHorizontalStrut(10));
        
        add(Box.createVerticalStrut(10));
        add(wrapPanel);
        add(Box.createVerticalStrut(10));
    }
}
