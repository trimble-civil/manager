package no.mesta.mipss.rodetegner.swing.rode;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.persistence.kjoretoyutstyr.Prodtype;
import no.mesta.mipss.persistence.kontrakt.LevProdtypeRode;
import no.mesta.mipss.persistence.kontrakt.Leverandor;
import no.mesta.mipss.persistence.kontrakt.Rode;
import no.mesta.mipss.rodetegner.RodeController;
import no.mesta.mipss.ui.AbstractRelasjonPanel;
import no.mesta.mipss.ui.beantable.MipssBeanTableModel;
import no.mesta.mipss.ui.table.LeverandorSelectableObject;
import no.mesta.mipss.ui.table.MipssRenderableEntityTableColumnModel;
import no.mesta.mipss.ui.table.ProdtypeSelectableObject;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

@SuppressWarnings("serial")
public class LeggTilLevProdtypePanel extends AbstractRelasjonPanel<List<LevProdtypeRode>> implements PropertyChangeListener {
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");

	
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
	private JPanel pnlMain = new JPanel();
	private GridBagLayout gridBagLayout2 = new GridBagLayout();
	private JPanel pnlVelgRode = new JPanel();
	private GridBagLayout gridBagLayout4 = new GridBagLayout();
	private JScrollPane scrlPaneRode = new JScrollPane();
	private JPanel pnlvelgProsess = new JPanel();
	private GridBagLayout gridBagLayout5 = new GridBagLayout();
	private JScrollPane scrlPaneProsess = new JScrollPane();
	private JXTable tblLeverandor = new JXTable();
	private JXTable tblProsess = new JXTable();
	private JLabel lblFraDato = new JLabel();
	private JXDatePicker dtPckrDatoFra;
	private JLabel lblTilDato = new JLabel();
	private JXDatePicker dtPckrDatoTil;
	private JPanel pnlDatoer = new JPanel();
	private GridBagLayout gridBagLayout6 = new GridBagLayout();

	private RodeController rodeController;
	private BindingGroup bindingGroup;
	private MipssBeanTableModel<LeverandorSelectableObject> leverandorModel;
	private MipssBeanTableModel<ProdtypeSelectableObject> prodtypeModel;

	public LeggTilLevProdtypePanel(RodeController rodeController) {
		this.rodeController = rodeController;

		bindingGroup = new BindingGroup();

		initComponents();
		initDatePckrs();
		initGui();

		bindingGroup.bind();
	}

	private void initDatePckrs() {
		Rode rode = rodeController.getRode();

		Locale loc = new Locale("no", "NO");
		String[] formats = new String[] { "dd.MM.yyyy" };

		dtPckrDatoFra = new JXDatePicker(loc);
		dtPckrDatoTil = new JXDatePicker(loc);

		dtPckrDatoFra.setFormats(formats);
		dtPckrDatoTil.setFormats(formats);

		dtPckrDatoFra.addPropertyChangeListener(this);
		dtPckrDatoTil.addPropertyChangeListener(this);

		if (rode != null) {
			dtPckrDatoFra.setDate(rode.getGyldigFraDato());
			dtPckrDatoTil.setDate(rode.getGyldigTilDato());
		}
	}

	private void initComponents() {
		leverandorModel = new MipssBeanTableModel<LeverandorSelectableObject>(
				LeverandorSelectableObject.class, new int[] { 0 }, "valgt", "nr", "navn");
		MipssRenderableEntityTableColumnModel columnModelRoder = new MipssRenderableEntityTableColumnModel(LeverandorSelectableObject.class, leverandorModel.getColumns());
		leverandorModel.setBeanInterface(RodeController.class);
		leverandorModel.setBeanInstance(rodeController);
		leverandorModel.setBeanMethod("getLeverandorForKontraktList");
		leverandorModel.setBeanMethodArguments(null);
		leverandorModel.setBeanMethodArgValues(null);
		leverandorModel.loadData();
    	
		tblLeverandor.setModel(leverandorModel);
		tblLeverandor.setColumnModel(columnModelRoder);

		tblLeverandor.getColumn(0).setMaxWidth(50);
		leverandorModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				updateComplete();
			}
		});

		prodtypeModel = new MipssBeanTableModel<ProdtypeSelectableObject>(ProdtypeSelectableObject.class, new int[] { 0 }, "valgt", "navn", "sesong");
		MipssRenderableEntityTableColumnModel columnModelProdtyper = new MipssRenderableEntityTableColumnModel(ProdtypeSelectableObject.class, prodtypeModel.getColumns());

		prodtypeModel.setEntities(rodeController.getProdtypeList());
		tblProsess.setModel(prodtypeModel);
		tblProsess.setColumnModel(columnModelProdtyper);

		tblProsess.getColumn(0).setMaxWidth(50);
		prodtypeModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				updateComplete();
			}
		});
	}

	private void initGui() {
		this.setLayout(gridBagLayout1);
		pnlMain.setLayout(gridBagLayout2);
		
		pnlVelgRode.setLayout(gridBagLayout4);
		pnlVelgRode.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("border.velgLeverandor")));
		pnlvelgProsess.setLayout(gridBagLayout5);
		pnlvelgProsess.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("border.velgProsesser")));
		tblLeverandor.setPreferredScrollableViewportSize(new Dimension(0, 0));
		scrlPaneRode.getViewport().add(tblLeverandor, null);
		tblProsess.setPreferredScrollableViewportSize(new Dimension(0, 0));
		scrlPaneProsess.getViewport().add(tblProsess, null);
		lblFraDato.setText(resources.getGuiString("label.fraDato"));
		lblTilDato.setText(resources.getGuiString("label.tilDato"));
		pnlDatoer.setLayout(gridBagLayout6);
		pnlDatoer.setBorder(BorderFactory.createTitledBorder(resources.getGuiString("border.velgTidsperiode")));
		
		pnlVelgRode.add(scrlPaneRode, 		new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlvelgProsess.add(scrlPaneProsess, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));

		pnlDatoer.add(lblFraDato,			new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5, 5, 5), 0, 0));
		pnlDatoer.add(lblTilDato, 			new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,5, 5, 5), 0, 0));
		pnlDatoer.add(dtPckrDatoFra, 		new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		pnlDatoer.add(dtPckrDatoTil, 		new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
		
		pnlMain.add(pnlVelgRode, 			new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));		
		pnlMain.add(pnlvelgProsess, 		new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));		
		pnlMain.add(pnlDatoer, 				new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.add(pnlMain, 					new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(11, 11, 11, 11), 0, 0));
	}

	private void updateComplete() {
		List<Leverandor> leverandorList = getSelectedLeverandorer();
		List<Prodtype> prodtypeList = getSelectedProdtyper();
		boolean datesCorrect = checkDates();

		setComplete(leverandorList != null && leverandorList.size() > 0
				&& prodtypeList != null && prodtypeList.size() > 0
				&& datesCorrect);
	}

	private List<Leverandor> getSelectedLeverandorer() {
		List<LeverandorSelectableObject> leveradnorSelectableObjectList = leverandorModel.getEntities();
		List<Leverandor> leverandorList = new ArrayList<Leverandor>();
			for (LeverandorSelectableObject leverandor : leveradnorSelectableObjectList) {
				if (leverandor.getValgt().booleanValue()) {
					leverandorList.add(leverandor.getLeverandor());
				}
			}

		return leverandorList;
	}

	private List<Prodtype> getSelectedProdtyper() {
		List<ProdtypeSelectableObject> prodtypeSelectableObjactList = prodtypeModel
				.getEntities();
		List<Prodtype> prodtypeList = new ArrayList<Prodtype>();
		if (prodtypeList != null) {
			for (ProdtypeSelectableObject prodtype : prodtypeSelectableObjactList) {
				if (prodtype.getValgt().booleanValue()) {
					prodtypeList.add(prodtype.getProdtype());
				}
			}
		}

		return prodtypeList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension getDialogSize() {
		return new Dimension(400, 500);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<LevProdtypeRode> getNyRelasjon() {
		List<LevProdtypeRode> levProdtypeRodeList = new ArrayList<LevProdtypeRode>();
		List<Leverandor> leverandorList = getSelectedLeverandorer();
		List<Prodtype> prodtypeList = getSelectedProdtyper();
		Date datoFra = dtPckrDatoFra.getDate();
		Date datoTil = dtPckrDatoTil.getDate();

		for (Leverandor leverandor : leverandorList) {
			for (Prodtype prodtype : prodtypeList) {
				LevProdtypeRode levProdtypeRode = new LevProdtypeRode();
				levProdtypeRode.setRode(rodeController.getRode());
				levProdtypeRode.setLeverandor(leverandor);
				levProdtypeRode.setProdtype(prodtype);
				levProdtypeRode.setFraDato(new Timestamp(datoFra.getTime()));
				levProdtypeRode.setTilDato(new Timestamp(datoTil.getTime()));
				levProdtypeRodeList.add(levProdtypeRode);
			}
		}

		return levProdtypeRodeList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return resources.getGuiString("dialogTitle.leggTilRode");
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		updateComplete();
	}

	private boolean checkDates() {
		Date datoFra = dtPckrDatoFra.getDate();
		Date datoTil = dtPckrDatoTil.getDate();

		if (datoFra != null && datoTil != null) {
			return datoTil.after(datoFra);
		}

		return false;
	}

	@Override
	public boolean isLegal() {
		boolean isLegal = true;
		List<LevProdtypeRode> nyRelasjonList = getNyRelasjon();
		
		List<LevProdtypeRode> eksisterendeList = rodeController.getRode().getLevProdtypeRodeList();

		class Key {
			String leverandorNr;
			Long rodeId;
			Long prodtypeId;

			public Key(String leverandorNr, Long rodeId, Long prodtypeId) {
				this.leverandorNr = leverandorNr;
				this.rodeId = rodeId;
				this.prodtypeId = prodtypeId;
			}

			@Override
			public int hashCode() {
				return new HashCodeBuilder().append(leverandorNr)
						.append(rodeId).append(prodtypeId).toHashCode();
			}

			public boolean equals(Object o) {
				if (o == null) {
					return false;
				}
				if (o == this) {
					return true;
				}
				if (!(o instanceof Key)) {
					return false;
				}
				Key other = (Key) o;
				return new EqualsBuilder().append(leverandorNr,
						other.leverandorNr).append(rodeId, other.rodeId)
						.append(prodtypeId, other.prodtypeId).isEquals();
			}
		}

		HashMap<Key, LevProdtypeRode> hashMap = new HashMap<Key, LevProdtypeRode>();
		for (LevProdtypeRode levProdtypeRode : eksisterendeList) {
			Key key = new Key(levProdtypeRode.getLeverandorNr(),
					levProdtypeRode.getRodeId(), levProdtypeRode
							.getProdtypeId());
			hashMap.put(key, levProdtypeRode);
		}

		for (LevProdtypeRode levProdtypeRode : nyRelasjonList) {
			Key key = new Key(levProdtypeRode.getLeverandorNr(),
					levProdtypeRode.getRodeId(), levProdtypeRode
							.getProdtypeId());
			if (hashMap.containsKey(key)) {
				LevProdtypeRode eksLevProdtypeRode = hashMap.get(key);
				if (DateUtil.overlappingPeriods(eksLevProdtypeRode.getFraDato(), eksLevProdtypeRode.getTilDato(), levProdtypeRode.getFraDato(),levProdtypeRode.getTilDato())) {
					isLegal = false;
					break;
				}

			}
		}

		if (!isLegal) {
			JOptionPane.showMessageDialog(getParentDialog(), resources.getGuiString("message.rodeOverlappendePeriode"));
		}

		return isLegal;
	}
	
	static class DateUtil {
		public static boolean overlappingPeriods(Date period1Start, Date period1End, Date period2Start, Date period2End) {
			long l1 = period1End.getTime() - period2Start.getTime();
			long l2 = period2End.getTime() - period1Start.getTime();
			long l3 = period1End.getTime() - period1Start.getTime() + period2End.getTime() - period2Start.getTime();
			return l1 < l3 && l2 < l3;
		}
	}

}
