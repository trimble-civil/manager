package no.mesta.mipss.rodetegner.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.VeitypePanel;

public class KontraktsveinettKonfigPanel extends JPanel{
	private VeitypePanel pnlVeitype;
	private JButton btnRestore;
	private final RodetegnerModule plugin;
	
	public KontraktsveinettKonfigPanel(RodetegnerModule plugin){
		this.plugin = plugin;
		initComponents();
		initGui();
	}

	private void initComponents() {
		pnlVeitype = new VeitypePanel();
		
		btnRestore = new JButton("Tilbakestill");
		btnRestore.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.KONTRAKTVEI.getType());
				plugin.getRodeController().visKontraktveinett(true, false, null);
			}
		});
		
	}
	public void addChangeListener(ChangeListener l) {
		pnlVeitype.addChangeListener(l);
    }
	private void initGui() {
		setLayout(new GridBagLayout());
		add(pnlVeitype, new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		add(btnRestore, new GridBagConstraints(0,1, 1,1, 1.0,0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,5,5,5), 0,0));
	}

}
