package no.mesta.mipss.rodetegner.swing.rodestrekning;

import java.awt.BorderLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.rodetegner.swing.DefaultWrapPanel;

/**
 * Dialog for å velge, legge til og slette rodestrekninger
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class RodestrekningerDialog extends JDialog {
    
    private RodetegnerModule plugin;
    
    public RodestrekningerDialog(RodetegnerModule plugin) {
    	super(plugin.getLoader().getApplicationFrame(), false);
        setTitle("Rodestrekninger");
        this.plugin = plugin;
        initGui();
    }   
    
    private void initGui(){
        setLayout(new BorderLayout());
        
        List<AbstractRodePanel> panels = new ArrayList<AbstractRodePanel>();
        RodestrekningerPanel rodePanel = new RodestrekningerPanel(plugin);
        
        panels.add(rodePanel);
        
        DefaultWrapPanel wrap = new DefaultWrapPanel(panels);
        add(wrap);
    }
}
