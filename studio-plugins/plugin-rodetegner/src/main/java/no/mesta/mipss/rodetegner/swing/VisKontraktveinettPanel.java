package no.mesta.mipss.rodetegner.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.mapplugin.layer.KontraktveiCollectionLayer;
import no.mesta.mipss.mapplugin.layer.Layer;
import no.mesta.mipss.mapplugin.util.MipssMapLayerType;
import no.mesta.mipss.persistence.kontrakt.Driftkontrakt;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.ui.VeitypePanel;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

/**
 * Panel for å gi brukere mulighet for å skru av og på kontraktveinettet
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
@SuppressWarnings("serial")
public class VisKontraktveinettPanel extends AbstractRodePanel{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");

    private JCheckBox showContractRoads;
    private JLabel dekningsProsent;
    private JButton btnValg;
	private RodetegnerModule plugin;
	private KontraktsveinettKonfigPanel kontraktsveinettKonfigPanel;
	
    public VisKontraktveinettPanel(final RodetegnerModule plugin) {
        this.plugin = plugin;
		setBorder(BorderFactory.createTitledBorder("Kontraktveinett"));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        kontraktsveinettKonfigPanel = new KontraktsveinettKonfigPanel(plugin);
        
        showContractRoads = new JCheckBox("Vis kontraktveinett", null);
        showContractRoads.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.getRodeController().visKontraktveinett(showContractRoads.isSelected(), false, null);
			}
        });
        
        btnValg = new JButton("Valg");
        btnValg.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog d = new JDialog(plugin.getLoader().getApplicationFrame(), true);
				d.add(kontraktsveinettKonfigPanel);
				d.pack();
				d.setSize(200, 250);
				d.setLocationRelativeTo(null);
				d.setVisible(true);
			}
        });
        
        dekningsProsent = new JLabel("0%");
        
        kontraktsveinettKonfigPanel.addChangeListener(getChangeListener());
        Binding enableShowRoadsBind = Bindings.createAutoBinding(UpdateStrategy.READ, plugin.getRodeController(), ELProperty.create("${driftkontrakt != null}"),  showContractRoads, BeanProperty.create("enabled"));
        Binding selectContractRoads = Bindings.createAutoBinding(UpdateStrategy.READ, plugin.getRodeController(), BeanProperty.create("driftkontrakt"), this, BeanProperty.create("driftkontrakt"));
        Binding selectRodetype = Bindings.createAutoBinding(UpdateStrategy.READ, plugin.getRodeController(), BeanProperty.create("rodetype"), this, BeanProperty.create("rodetype"));
        Binding updateAktive = Bindings.createAutoBinding(UpdateStrategy.READ, plugin.getRodeController(), BeanProperty.create("visKunAktiveRoder"), this, BeanProperty.create("aktive"));

        plugin.getFieldToolkit().addBinding(enableShowRoadsBind);
        plugin.getFieldToolkit().addBinding(selectContractRoads);
        plugin.getFieldToolkit().addBinding(selectRodetype);
        plugin.getFieldToolkit().addBinding(updateAktive);
        
        JPanel showContractRoadsPanel = new JPanel();
        showContractRoadsPanel.setLayout(new BoxLayout(showContractRoadsPanel, BoxLayout.LINE_AXIS));
        
        showContractRoadsPanel.add(showContractRoads);
        showContractRoadsPanel.add(btnValg);
        showContractRoadsPanel.add(Box.createHorizontalGlue());
        showContractRoadsPanel.add(new JLabel("Dekningsgrad:"));
        showContractRoadsPanel.add(dekningsProsent);
        add(showContractRoadsPanel);
    }
    
    private ChangeListener getChangeListener() {
    	return new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				Layer l=null;
				List<Layer> kontraktveiLayer = plugin.getMainMap().getLayerHandler().getLayers(MipssMapLayerType.KONTRAKTVEI.getType());
				if (kontraktveiLayer!=null&&kontraktveiLayer.size()>0){
                	l = kontraktveiLayer.get(0);
                }
				if (e.getSource().getClass().equals(VeitypePanel.class)){
            		VeitypePanel p = (VeitypePanel)e.getSource();
            		if (l!=null){
                		if (l.getClass().equals(KontraktveiCollectionLayer.class)){
                			KontraktveiCollectionLayer kl = (KontraktveiCollectionLayer)l;
                			kl.showEv(p.isEv());
                			kl.showRv(p.isRv());
                			kl.showFv(p.isFv());
                			kl.showKv(p.isKv());
                		}
                		else{
                			int v = JOptionPane.showConfirmDialog(plugin.getModuleGUI(), resources.getGuiString("message.henteKontraktsveinettIgjen"), "Hente kontraktsveinett", JOptionPane.OK_CANCEL_OPTION);
                			if (v==JOptionPane.OK_OPTION){
            					plugin.getMainMap().getLayerHandler().removeLayerType(MipssMapLayerType.KONTRAKTVEI.getType());
            					plugin.getRodeController().visKontraktveinett(true, true, p);
                			}
                		}
                		plugin.getMainMap().getLayerHandler().changeLayer(l);
            		}
            		if (l==null){
            			plugin.getRodeController().visKontraktveinett(true, true, p);
            			showContractRoads.setSelected(true);
            		}
            	}
			}
		};
	}
	/**
	 * Denne metoden vil bli kalt når modulen lukkes, og må ikke kalles under noen
	 * andre omstendigheter, ettersom dette vil føre til ustabilitet i modulen. 
	 * Metoden er til for å rydde opp i referanser til de forskjellige klassene
	 * som blir instansiert av modulen slik at det ikke ligger igjen noen instanser
	 * etter at modulen er lukket. (mem-leaks)
	 */
    public void dispose(){
    	for (ActionListener l:showContractRoads.getActionListeners()){
    		showContractRoads.removeActionListener(l);
    	}
    	showContractRoads = null;
    	plugin = null;
    }
    public int getVerticalStrut() {
        return 0;
    }
    
    public void setDriftkontrakt(Driftkontrakt kontrakt){
    	showContractRoads.setSelected(false);
    }
    
    public void setRodetype(final Rodetype rodetype){
    	if (rodetype!=null&&plugin.getRodeController().getDriftkontrakt()!=null){
    		if (plugin.getRodeController().getDriftkontrakt().getVeinettId()!=null){
    			new Thread(){
			    	public void run(){
	    				Double prosent = plugin.getRodegeneratorBean().getDekningsgradRodetypeKontraktveinett(plugin.getRodeController().getDriftkontrakt().getId(),rodetype.getId(), plugin.getRodeController().getVisKunAktiveRoder());
	    				String p = "0%";
	    				if (prosent!=null){
	    					DecimalFormat f = new DecimalFormat("###.#");
	    					p = f.format(prosent);
	    				}
	    				final String prosentText = p+"%";
	    				SwingUtilities.invokeLater(new Runnable(){
	    					public void run(){
	    						dekningsProsent.setText(prosentText);
	    					}
	    				});
	    				
			    	}
    			}.start();
    		}
    	}
    }
    public void setAktive(Boolean aktive){
    	setRodetype(plugin.getRodeController().getRodetype());
    }
}
