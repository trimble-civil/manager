package no.mesta.mipss.rodetegner.swing.rodetype;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import no.mesta.mipss.common.PropertyResourceBundleUtil;
import no.mesta.mipss.core.FieldToolkit;
import no.mesta.mipss.persistence.kontrakt.Rodetype;
import no.mesta.mipss.resources.images.IconResources;
import no.mesta.mipss.rodetegner.RodetegnerModule;
import no.mesta.mipss.rodetegner.swing.AbstractRodePanel;
import no.mesta.mipss.rodetegner.swing.DefaultWrapPanel;

/**
 * Dialog for å legge til nye rodetyper
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public class NyRodetypeDialog  extends JDialog{
	private PropertyResourceBundleUtil resources = PropertyResourceBundleUtil.getPropertyBundle("rodegeneratorText");
    private NyRodetypePanel rodetypePanel;
    private RodetegnerModule plugin ;
    private Rodetype rodetype= new Rodetype();
    private Rodetype bakRodetype;
    private boolean viewMode;
	private JButton okButton;
	private JButton cancelButton;
    
    public NyRodetypeDialog(RodetegnerModule plugin ) {
    	super(plugin.getLoader().getApplicationFrame(), true);
        setTitle("Opprett ny rodetype");
        this.plugin = plugin;
        initGui();
    }
    
    public NyRodetypeDialog(RodetegnerModule plugin, Rodetype rodetype ) {
    	super(plugin.getLoader().getApplicationFrame(), true);
        setTitle("Egenskaper for rodetype:"+rodetype.getNavn());
        this.plugin = plugin;
        this.viewMode = true;
        this.bakRodetype = rodetype;
        
        this.rodetype.setNavn(rodetype.getNavn());
        this.rodetype.setBeskrivelse(rodetype.getBeskrivelse());
        this.rodetype.setBareEnRodeFlagg(rodetype.getBareEnRodeFlagg());
        this.rodetype.setKanHaOverlappFlagg(rodetype.getKanHaOverlappFlagg());
        this.rodetype.setGenererProduksjonFlagg(rodetype.getGenererProduksjonFlagg());
        this.rodetype.setDriftkontrakt(rodetype.getDriftkontrakt());
        
        initGui();
    }
    
    public void setEditable(boolean editable){
    	okButton.setEnabled(editable);
    }
    
    private void initGui(){
        setResizable(false);
        setLayout(new BorderLayout());
        
        rodetypePanel= new NyRodetypePanel(plugin, rodetype, viewMode);
        
        JCheckBox autogenerateCheck = new JCheckBox(resources.getGuiString("rodetypedialog.autogenerer"));
        AutoGenerateOptionPanel optionPanel = new AutoGenerateOptionPanel(plugin);
        
        //bind komponentene i autogenerer-panelet til å være aktive hvis checkboxen er valgt
        FieldToolkit toolkit = FieldToolkit.createToolkit();
        toolkit.bindCheckBox(autogenerateCheck, AutoGenerateOptionPanel.class, optionPanel, "enabled", null);
        toolkit.bind();
        autogenerateCheck.setSelected(false);
        autogenerateCheck.setEnabled(false);
        
        List<AbstractRodePanel> panels = new ArrayList<AbstractRodePanel>();
        panels.add(rodetypePanel);
        if (!viewMode){
        	panels.add(createAutogenerateCheckPanel(autogenerateCheck));
        	panels.add(optionPanel);
        }
        panels.add(getButtonPanel());
        
        DefaultWrapPanel wrap = new DefaultWrapPanel(panels);
        add(wrap);
    }
    
    private AbstractRodePanel createAutogenerateCheckPanel(JCheckBox autogenerateCheck){
        AbstractRodePanel panel = new AbstractRodePanel(){
            public int getVerticalStrut(){
                return 5;
            }
        };
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.add(autogenerateCheck);
        panel.add(Box.createHorizontalGlue());
        return panel;
    }
    
    private AbstractRodePanel getButtonPanel(){
        AbstractRodePanel panel = new AbstractRodePanel(){
            public int getVerticalStrut(){
                return 5;
            }
        };
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        
        cancelButton = new JButton(new CancelButtonAction("Avbryt", IconResources.CANCEL_ICON));
        okButton = new JButton(new OkButtonAction("Lagre", IconResources.OK2_ICON));
        
        panel.add(Box.createHorizontalGlue());
        panel.add(cancelButton);
        panel.add(Box.createHorizontalStrut(5));
        panel.add(okButton);
        return panel;
    }
    
    /**
     * Actionklasse for å opprette en ny rodetype
     */
    class OkButtonAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public OkButtonAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
        	String msg = null;
    		if (viewMode){
    			if (rodetype.getBareEnRodeFlagg()){
    				if (plugin.getRodeController().getRoder().size()>1){
    					JOptionPane.showMessageDialog(NyRodetypeDialog.this, resources.getGuiString("rodetypedialog.update.error"), resources.getGuiString("rodetypedialog.error"), JOptionPane.ERROR_MESSAGE);
    					return;
    				}
    			}
    			bakRodetype.setNavn(rodetype.getNavn());
    			bakRodetype.setBeskrivelse(rodetype.getBeskrivelse());
    			bakRodetype.setBareEnRodeFlagg(rodetype.getBareEnRodeFlagg());
    			bakRodetype.setKanHaOverlappFlagg(rodetype.getKanHaOverlappFlagg());
    			bakRodetype.setGenererProduksjonFlagg(rodetype.getGenererProduksjonFlagg());
    			bakRodetype.setDriftkontrakt(rodetype.getDriftkontrakt());
    			plugin.getRodeController().mergeRodetype(bakRodetype);
        	}
        	else{
        		msg = plugin.getRodeController().persistRodetype(rodetype);
        	}
        		
	        if (msg!=null){
	        	JOptionPane.showMessageDialog(NyRodetypeDialog.this, msg, resources.getGuiString("rodetypedialog.error"), JOptionPane.ERROR_MESSAGE);
	        }else{
	        	NyRodetypeDialog.this.dispose();
	        }
        }
    }
    
    /**
     * Actionklasse for å avbryte opprettingen av ny rodetype
     */
    class CancelButtonAction extends AbstractAction{
        
        /**
         * Konstruktør
         * @param text tekst til knappen
         * @param image ikonet på knappen
         */
        public CancelButtonAction(String text, Icon image){
            super(text, image);
        }
        /**
         * Action
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
            NyRodetypeDialog.this.dispose();
        }
    }
}
