package no.mesta.mipss.rodetegner.swing;

import java.awt.Dimension;

import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.mesta.mipss.persistence.Clock;
import no.mesta.mipss.ui.JDateTimePicker;

import org.jdesktop.swingx.JXDatePicker;

/**
 * Abstrakt klasse som inneholder forskjellige set metoder som 
 * brukes av subklassene til å sette størrelse på komponentene.
 * 
 * @author <a href="mailto:harald.kulo@mesta.no">Harald Alexander Kulø</a>
 */
public abstract class AbstractRodePanel extends JPanel{
    
    public AbstractRodePanel() {
        
    }
    
    protected void setRodetypePanelSize(JPanel panel){
        Dimension size = new Dimension(300, 120);
        setComponentSize(size, panel);
    }
    
    protected void setButtonPanelSize(JPanel panel){
        Dimension size = new Dimension(300, 30);
        setComponentSize(size, panel);
    }
    
    protected void setRoderButtonSize(JButton button){
        Dimension size = new Dimension (110, 25);
        setComponentSize(size, button);
    }
    protected void setButtonSize(JButton button){
        Dimension size = new Dimension(80, 25);
        setComponentSize(size, button);
    }
    
    protected void setComboSize(JComboBox box){
        Dimension size = new Dimension(300, 20);
        setComponentSize(size, box);
    }
    protected void setComponentSize(Dimension size, JComponent comp){
        comp.setPreferredSize(size);
        comp.setMinimumSize(size);
        comp.setMaximumSize(size);
    }
    
    /**
     * Lager et panel som inneholder en JXDatePicker med en label
     * @param text teksten som skal så i label
     * @param strut avstand mellom label og picker
     * @return
     */
    protected JPanel createDatePanel(String text, int strut){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        JLabel label = new JLabel(text);
        JXDatePicker date = new JDateTimePicker(Clock.now()).getDatePicker();
        //panel.add(Box.createHorizontalGlue());
         panel.add(Box.createHorizontalStrut(10));
        panel.add(label);
        panel.add(Box.createHorizontalStrut(strut));
        panel.add(date);
        panel.add(Box.createHorizontalGlue());
        return panel;
    }
    
    /**
     * Lager et panel som inneholder en JXDatePicker med en label
     * @param text teksten som skal så i label
     * @param strut avstand mellom label og picker
     * @param date datovelgeren som skal puttes i panelet
     * @return
     */
    protected JPanel createDatePanel(String text, int strut, JXDatePicker date){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        JLabel label = new JLabel(text);
//         panel.add(Box.createHorizontalStrut(10));
        panel.add(label);
        panel.add(Box.createHorizontalStrut(strut));
        panel.add(date);
        panel.add(Box.createHorizontalGlue());
        return panel;
    }
    
    /**
     * Returnerer avstanden til forrige panel før dette panelet tegnes
     * @return
     */
    public abstract int getVerticalStrut();
    
}
